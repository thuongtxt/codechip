import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0011_RD_GLBPMC(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["hold_reg0"] = _AF6CNC0011_RD_GLBPMC._hold_reg0()
        allRegisters["upen_txpwcnt_rc"] = _AF6CNC0011_RD_GLBPMC._upen_txpwcnt_rc()
        allRegisters["upen_txpwcnt_ro"] = _AF6CNC0011_RD_GLBPMC._upen_txpwcnt_ro()
        allRegisters["upen_rxpwcnt_info0_rc"] = _AF6CNC0011_RD_GLBPMC._upen_rxpwcnt_info0_rc()
        allRegisters["upen_rxpwcnt_info0_ro"] = _AF6CNC0011_RD_GLBPMC._upen_rxpwcnt_info0_ro()
        allRegisters["upen_rxpwcnt_info1_rc"] = _AF6CNC0011_RD_GLBPMC._upen_rxpwcnt_info1_rc()
        allRegisters["upen_rxpwcnt_info1_ro"] = _AF6CNC0011_RD_GLBPMC._upen_rxpwcnt_info1_ro()
        allRegisters["upen_rxpwcnt_info2_rc"] = _AF6CNC0011_RD_GLBPMC._upen_rxpwcnt_info2_rc()
        allRegisters["upen_rxpwcnt_info2_ro"] = _AF6CNC0011_RD_GLBPMC._upen_rxpwcnt_info2_ro()
        allRegisters["upen_pwcntbyte_rc"] = _AF6CNC0011_RD_GLBPMC._upen_pwcntbyte_rc()
        allRegisters["upen_pwcntbyte_ro"] = _AF6CNC0011_RD_GLBPMC._upen_pwcntbyte_ro()
        allRegisters["upen_poh_pmr_cnt0_rc"] = _AF6CNC0011_RD_GLBPMC._upen_poh_pmr_cnt0_rc()
        allRegisters["upen_poh_pmr_cnt0_ro"] = _AF6CNC0011_RD_GLBPMC._upen_poh_pmr_cnt0_ro()
        allRegisters["upen_pohpmdat_sts_ber_erdi1"] = _AF6CNC0011_RD_GLBPMC._upen_pohpmdat_sts_ber_erdi1()
        allRegisters["upen_pohpmdat_vt_ber_erdi1_ro"] = _AF6CNC0011_RD_GLBPMC._upen_pohpmdat_vt_ber_erdi1_ro()
        allRegisters["upen_pohpmdat_vt_ber_erdi1_ro"] = _AF6CNC0011_RD_GLBPMC._upen_pohpmdat_vt_ber_erdi1_ro()
        allRegisters["upen_poh_vt_cnt_rc"] = _AF6CNC0011_RD_GLBPMC._upen_poh_vt_cnt_rc()
        allRegisters["upen_poh_vt_cnt_ro"] = _AF6CNC0011_RD_GLBPMC._upen_poh_vt_cnt_ro()
        allRegisters["upen_poh_sts_cnt_rc"] = _AF6CNC0011_RD_GLBPMC._upen_poh_sts_cnt_rc()
        allRegisters["upen_poh_sts_cnt_ro"] = _AF6CNC0011_RD_GLBPMC._upen_poh_sts_cnt_ro()
        allRegisters["upen_pdh_de1cntval30_rc"] = _AF6CNC0011_RD_GLBPMC._upen_pdh_de1cntval30_rc()
        allRegisters["upen_pdh_de1cntval30_ro"] = _AF6CNC0011_RD_GLBPMC._upen_pdh_de1cntval30_ro()
        allRegisters["upen_pdh_de3cnt0_rc"] = _AF6CNC0011_RD_GLBPMC._upen_pdh_de3cnt0_rc()
        allRegisters["upen_pdh_de3cnt0_ro"] = _AF6CNC0011_RD_GLBPMC._upen_pdh_de3cnt0_ro()
        allRegisters["upen_de1prm_cnt0_rc"] = _AF6CNC0011_RD_GLBPMC._upen_de1prm_cnt0_rc()
        allRegisters["upen_de1prm_cnt0_ro"] = _AF6CNC0011_RD_GLBPMC._upen_de1prm_cnt0_ro()
        allRegisters["upen_de1prm_cnt1_rc"] = _AF6CNC0011_RD_GLBPMC._upen_de1prm_cnt1_rc()
        allRegisters["upen_de1prm_cnt1_ro"] = _AF6CNC0011_RD_GLBPMC._upen_de1prm_cnt1_ro()
        allRegisters["upen_cla0_rc"] = _AF6CNC0011_RD_GLBPMC._upen_cla0_rc()
        allRegisters["upen_cla0_ro"] = _AF6CNC0011_RD_GLBPMC._upen_cla0_ro()
        allRegisters["upen_rxeth00_rc"] = _AF6CNC0011_RD_GLBPMC._upen_rxeth00_rc()
        allRegisters["upen_rxeth00_ro"] = _AF6CNC0011_RD_GLBPMC._upen_rxeth00_ro()
        allRegisters["upen_rxeth1_pkt_rc"] = _AF6CNC0011_RD_GLBPMC._upen_rxeth1_pkt_rc()
        allRegisters["upen_rxeth1_pkt_ro"] = _AF6CNC0011_RD_GLBPMC._upen_rxeth1_pkt_ro()
        allRegisters["upen_rxeth1_byte_rc"] = _AF6CNC0011_RD_GLBPMC._upen_rxeth1_byte_rc()
        allRegisters["upen_rxeth1_byte_ro"] = _AF6CNC0011_RD_GLBPMC._upen_rxeth1_byte_ro()
        allRegisters["upen_txeth00_rc"] = _AF6CNC0011_RD_GLBPMC._upen_txeth00_rc()
        allRegisters["upen_txeth00_ro"] = _AF6CNC0011_RD_GLBPMC._upen_txeth00_ro()
        allRegisters["upen_txeth1_pkt_rc"] = _AF6CNC0011_RD_GLBPMC._upen_txeth1_pkt_rc()
        allRegisters["upen_txeth1_pkt_ro"] = _AF6CNC0011_RD_GLBPMC._upen_txeth1_pkt_ro()
        allRegisters["upen_txeth1_byte_rc"] = _AF6CNC0011_RD_GLBPMC._upen_txeth1_byte_rc()
        allRegisters["upen_txeth1_byte_ro"] = _AF6CNC0011_RD_GLBPMC._upen_txeth1_byte_ro()
        allRegisters["upen_cnttxoam_rc"] = _AF6CNC0011_RD_GLBPMC._upen_cnttxoam_rc()
        allRegisters["upen_cnttxoam_ro"] = _AF6CNC0011_RD_GLBPMC._upen_cnttxoam_ro()
        allRegisters["upen_cls_bcnt_rc"] = _AF6CNC0011_RD_GLBPMC._upen_cls_bcnt_rc()
        allRegisters["upen_cls_bcnt_ro"] = _AF6CNC0011_RD_GLBPMC._upen_cls_bcnt_ro()
        allRegisters["upen_cls_opcnt0_rc"] = _AF6CNC0011_RD_GLBPMC._upen_cls_opcnt0_rc()
        allRegisters["upen_cls_opcnt0_ro"] = _AF6CNC0011_RD_GLBPMC._upen_cls_opcnt0_ro()
        allRegisters["upen_cls_opcnt1_rc"] = _AF6CNC0011_RD_GLBPMC._upen_cls_opcnt1_rc()
        allRegisters["upen_cls_opcnt1_ro"] = _AF6CNC0011_RD_GLBPMC._upen_cls_opcnt1_ro()
        allRegisters["upen_cls_ipcnt_rc"] = _AF6CNC0011_RD_GLBPMC._upen_cls_ipcnt_rc()
        allRegisters["upen_cls_ipcnt_ro"] = _AF6CNC0011_RD_GLBPMC._upen_cls_ipcnt_ro()
        allRegisters["upen_eth_bytecnt_rc"] = _AF6CNC0011_RD_GLBPMC._upen_eth_bytecnt_rc()
        allRegisters["upen_eth_bytecnt_ro"] = _AF6CNC0011_RD_GLBPMC._upen_eth_bytecnt_ro()
        allRegisters["upen_eth_bytecnt_rc"] = _AF6CNC0011_RD_GLBPMC._upen_eth_bytecnt_rc()
        allRegisters["upen_eth_bytecnt_ro"] = _AF6CNC0011_RD_GLBPMC._upen_eth_bytecnt_ro()
        allRegisters["upen_de1_tdmvld_cnt_rc"] = _AF6CNC0011_RD_GLBPMC._upen_de1_tdmvld_cnt_rc()
        allRegisters["upen_de1_tdmvld_cnt_ro"] = _AF6CNC0011_RD_GLBPMC._upen_de1_tdmvld_cnt_ro()
        allRegisters["upen_de3_tdmvld_cnt_rc"] = _AF6CNC0011_RD_GLBPMC._upen_de3_tdmvld_cnt_rc()
        allRegisters["upen_de3_tdmvld_cnt_ro"] = _AF6CNC0011_RD_GLBPMC._upen_de3_tdmvld_cnt_ro()
        return allRegisters

    class _hold_reg0(AtRegister.AtRegister):
        def name(self):
            return "CPU Reg Hold 0"
    
        def description(self):
            return "The register provides hold register from [63:32]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b319
            
        def endAddress(self):
            return 0xffffffff

        class _hold0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "hold0"
            
            def description(self):
                return "Hold from  [63:32]bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["hold0"] = _AF6CNC0011_RD_GLBPMC._hold_reg0._hold0()
            return allFields

    class _upen_txpwcnt_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_0000 + 1024*$offset + $pwid"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x00000bff

        class _txpwcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txpwcnt"
            
            def description(self):
                return "in case of txpwcnt0 side: + offset = 0 : txrbit + offset = 1 : txnbit + offset = 2 : Unused in case of txpwcnt1 side: + offset = 0 : txpbit + offset = 1 : txlbit + offset = 2 : txpkt"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txpwcnt"] = _AF6CNC0011_RD_GLBPMC._upen_txpwcnt_rc._txpwcnt()
            return allFields

    class _upen_txpwcnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_1000 + 1024*$offset + $pwid"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0x00001bff

        class _txpwcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txpwcnt"
            
            def description(self):
                return "in case of txpwcnt0 side: + offset = 0 : txrbit + offset = 1 : txnbit + offset = 2 : Unused in case of txpwcnt1 side: + offset = 0 : txpbit + offset = 1 : txlbit + offset = 2 : txpkt"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txpwcnt"] = _AF6CNC0011_RD_GLBPMC._upen_txpwcnt_ro._txpwcnt()
            return allFields

    class _upen_rxpwcnt_info0_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info0"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_8000 + 1024*$offset + $pwid"
            
        def startAddress(self):
            return 0x00008000
            
        def endAddress(self):
            return 0x00008bff

        class _rxpwcnt_info0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info0"
            
            def description(self):
                return "in case of rxpwcnt_info0_cnt0 side: + offset = 0 : rxlate + offset = 1 : rxpbit + offset = 2 : rxlbit in case of rxpwcnt_info0_cnt1 side: + offset = 0 : rxrbit + offset = 1 : rxnbit + offset = 2 : rxpkt"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info0"] = _AF6CNC0011_RD_GLBPMC._upen_rxpwcnt_info0_rc._rxpwcnt_info0()
            return allFields

    class _upen_rxpwcnt_info0_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info0"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_9000 + 1024*$offset + $pwid"
            
        def startAddress(self):
            return 0x00009000
            
        def endAddress(self):
            return 0x00009bff

        class _rxpwcnt_info0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info0"
            
            def description(self):
                return "in case of rxpwcnt_info0_cnt0 side: + offset = 0 : rxlate + offset = 1 : rxpbit + offset = 2 : rxlbit in case of rxpwcnt_info0_cnt1 side: + offset = 0 : rxrbit + offset = 1 : rxnbit + offset = 2 : rxpkt"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info0"] = _AF6CNC0011_RD_GLBPMC._upen_rxpwcnt_info0_ro._rxpwcnt_info0()
            return allFields

    class _upen_rxpwcnt_info1_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1_0000 + 1024*$offset + $pwid"
            
        def startAddress(self):
            return 0x00010000
            
        def endAddress(self):
            return 0x00010bff

        class _rxpwcnt_info1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info1"
            
            def description(self):
                return "in case of rxpwcnt_info1_cnt0 side: + offset = 0 : rxearly + offset = 1 : rxlops + offset = 2 : Unused in case of rxpwcnt_info1_cnt1 side: + offset = 0 : rxlost + offset = 1 : Unused + offset = 2 : Unused"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info1"] = _AF6CNC0011_RD_GLBPMC._upen_rxpwcnt_info1_rc._rxpwcnt_info1()
            return allFields

    class _upen_rxpwcnt_info1_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1_1000 + 1024*$offset + $pwid"
            
        def startAddress(self):
            return 0x00011000
            
        def endAddress(self):
            return 0x00011bff

        class _rxpwcnt_info1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info1"
            
            def description(self):
                return "in case of rxpwcnt_info1_cnt0 side: + offset = 0 : rxearly + offset = 1 : rxlops + offset = 2 : Unused in case of rxpwcnt_info1_cnt1 side: + offset = 0 : rxlost + offset = 1 : Unused + offset = 2 : Unused"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info1"] = _AF6CNC0011_RD_GLBPMC._upen_rxpwcnt_info1_ro._rxpwcnt_info1()
            return allFields

    class _upen_rxpwcnt_info2_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info2"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1_8000 + 1024*$offset + $pwid"
            
        def startAddress(self):
            return 0x00018000
            
        def endAddress(self):
            return 0x00018bff

        class _rxpwcnt_info2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info2"
            
            def description(self):
                return "in case of rxpwcnt_info2_cnt0 side: + offset = 0 : rxoverrun + offset = 1 : rxunderrun + offset = 2 : rxmalform in case of rxpwcnt_info2_cnt1 side: + offset = 0 : rxoam + offset = 1 : rxduplicate + offset = 2 : rxstray"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info2"] = _AF6CNC0011_RD_GLBPMC._upen_rxpwcnt_info2_rc._rxpwcnt_info2()
            return allFields

    class _upen_rxpwcnt_info2_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info2"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1_9000 + 1024*$offset + $pwid"
            
        def startAddress(self):
            return 0x00019000
            
        def endAddress(self):
            return 0x00019bff

        class _rxpwcnt_info2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info2"
            
            def description(self):
                return "in case of rxpwcnt_info2_cnt0 side: + offset = 0 : rxoverrun + offset = 1 : rxunderrun + offset = 2 : rxmalform in case of rxpwcnt_info2_cnt1 side: + offset = 0 : rxoam + offset = 1 : rxduplicate + offset = 2 : rxstray"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info2"] = _AF6CNC0011_RD_GLBPMC._upen_rxpwcnt_info2_ro._rxpwcnt_info2()
            return allFields

    class _upen_pwcntbyte_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Byte Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x6_8000 + $pwid"
            
        def startAddress(self):
            return 0x00068000
            
        def endAddress(self):
            return 0x000683ff

        class _pwcntbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pwcntbyte"
            
            def description(self):
                return "txpwcntbyte (rxpwcntbyte)"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pwcntbyte"] = _AF6CNC0011_RD_GLBPMC._upen_pwcntbyte_rc._pwcntbyte()
            return allFields

    class _upen_pwcntbyte_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Byte Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x6_8400 + $pwid"
            
        def startAddress(self):
            return 0x00068400
            
        def endAddress(self):
            return 0x000687ff

        class _pwcntbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pwcntbyte"
            
            def description(self):
                return "txpwcntbyte (rxpwcntbyte)"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pwcntbyte"] = _AF6CNC0011_RD_GLBPMC._upen_pwcntbyte_ro._pwcntbyte()
            return allFields

    class _upen_poh_pmr_cnt0_rc(AtRegister.AtRegister):
        def name(self):
            return "PMR Error Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_B000 + 32*$offset + $stsid"
            
        def startAddress(self):
            return 0x0003b000
            
        def endAddress(self):
            return 0x0003b05f

        class _pmr_err_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pmr_err_cnt"
            
            def description(self):
                return "in case of pmr_err_cnt0 side: + offset = 0 : rei_l + offset = 1 : b1 + offset = 2 : Unused in case of pmr_err_cnt1 side: + offset = 0 : b2 + offset = 1 : Unused + offset = 2 : Unused"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pmr_err_cnt"] = _AF6CNC0011_RD_GLBPMC._upen_poh_pmr_cnt0_rc._pmr_err_cnt()
            return allFields

    class _upen_poh_pmr_cnt0_ro(AtRegister.AtRegister):
        def name(self):
            return "PMR Error Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_B080 + 32*$offset + $stsid"
            
        def startAddress(self):
            return 0x0003b080
            
        def endAddress(self):
            return 0x0003b0df

        class _pmr_err_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pmr_err_cnt"
            
            def description(self):
                return "in case of pmr_err_cnt0 side: + offset = 0 : rei_l + offset = 1 : b1 + offset = 2 : Unused in case of pmr_err_cnt1 side: + offset = 0 : b2 + offset = 1 : Unused + offset = 2 : Unused"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pmr_err_cnt"] = _AF6CNC0011_RD_GLBPMC._upen_poh_pmr_cnt0_ro._pmr_err_cnt()
            return allFields

    class _upen_pohpmdat_sts_ber_erdi1(AtRegister.AtRegister):
        def name(self):
            return "POH path sts ERDI Counter1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_D800 + 32*$offset +  $stsid"
            
        def startAddress(self):
            return 0x0003d800
            
        def endAddress(self):
            return 0x0003d85f

        class _pohpath_ber_erdi(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pohpath_ber_erdi"
            
            def description(self):
                return "in case of pohpath_ber_erdi_cnt2 side: + offset = 0 : sts_rei + offset = 1 : Unused + offset = 2 : Unused in case of pohpath_ber_erdi_cnt3 side: + offset = 0 : sts_bip(B3) + offset = 1 : Unused + offset = 2 : Unused"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pohpath_ber_erdi"] = _AF6CNC0011_RD_GLBPMC._upen_pohpmdat_sts_ber_erdi1._pohpath_ber_erdi()
            return allFields

    class _upen_pohpmdat_vt_ber_erdi1_ro(AtRegister.AtRegister):
        def name(self):
            return "POH path vt ERDI Counter1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x6_4000 + 672*$offset + 28*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x00064000
            
        def endAddress(self):
            return 0x000647df

        class _pohpath_ber_erdi(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pohpath_ber_erdi"
            
            def description(self):
                return "in case of pohpath_ber_erdi_cnt2 side: + offset = 0 : vt_rei + offset = 1 : Unused + offset = 2 : Unused in case of pohpath_ber_erdi_cnt3 side: + offset = 0 : vt_bip + offset = 1 : Unused + offset = 2 : Unused"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pohpath_ber_erdi"] = _AF6CNC0011_RD_GLBPMC._upen_pohpmdat_vt_ber_erdi1_ro._pohpath_ber_erdi()
            return allFields

    class _upen_pohpmdat_vt_ber_erdi1_ro(AtRegister.AtRegister):
        def name(self):
            return "POH path vt ERDI Counter1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xE_4000 + 672*$offset + 28*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x000e4000
            
        def endAddress(self):
            return 0x000e47df

        class _pohpath_ber_erdi(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pohpath_ber_erdi"
            
            def description(self):
                return "in case of pohpath_ber_erdi_cnt2 side: + offset = 0 : vt_rei + offset = 1 : Unused + offset = 2 : Unused in case of pohpath_ber_erdi_cnt3 side: + offset = 0 : vt_bip + offset = 1 : Unused + offset = 2 : Unused"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pohpath_ber_erdi"] = _AF6CNC0011_RD_GLBPMC._upen_pohpmdat_vt_ber_erdi1_ro._pohpath_ber_erdi()
            return allFields

    class _upen_poh_vt_cnt_rc(AtRegister.AtRegister):
        def name(self):
            return "POH vt pointer Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x5_0000 + 672*$offset +  28*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x00050000
            
        def endAddress(self):
            return 0x000507df

        class _poh_vt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_vt_cnt"
            
            def description(self):
                return "in case of poh_vt_cnt0 side: + offset = 0 : pohtx_vt_dec + offset = 1 : pohrx_vt_dec + offset = 2 : Unused in case of poh_vt_cnt1 side: + offset = 0 : pohtx_vt_inc + offset = 1 : pohrx_vt_inc + offset = 2 : Unused"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_vt_cnt"] = _AF6CNC0011_RD_GLBPMC._upen_poh_vt_cnt_rc._poh_vt_cnt()
            return allFields

    class _upen_poh_vt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "POH vt pointer Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x5_4000 + 672*$offset +  28*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x00054000
            
        def endAddress(self):
            return 0x000547df

        class _poh_vt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_vt_cnt"
            
            def description(self):
                return "in case of poh_vt_cnt0 side: + offset = 0 : pohtx_vt_dec + offset = 1 : pohrx_vt_dec + offset = 2 : Unused in case of poh_vt_cnt1 side: + offset = 0 : pohtx_vt_inc + offset = 1 : pohrx_vt_inc + offset = 2 : Unused"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_vt_cnt"] = _AF6CNC0011_RD_GLBPMC._upen_poh_vt_cnt_ro._poh_vt_cnt()
            return allFields

    class _upen_poh_sts_cnt_rc(AtRegister.AtRegister):
        def name(self):
            return "POH sts pointer Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_F000 + 32*$offset +  $stsid"
            
        def startAddress(self):
            return 0x0003f000
            
        def endAddress(self):
            return 0x0003f05f

        class _poh_sts_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_sts_cnt"
            
            def description(self):
                return "in case of poh_sts_cnt0 side: + offset = 0 :  pohtx_sts_dec + offset = 1 :  pohrx_sts_dec + offset = 2 :  Unused in case of poh_sts_cnt1 side: + offset = 0 :  pohtx_sts_inc + offset = 1 :  pohrx_sts_inc + offset = 2 :  Unused"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_sts_cnt"] = _AF6CNC0011_RD_GLBPMC._upen_poh_sts_cnt_rc._poh_sts_cnt()
            return allFields

    class _upen_poh_sts_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "POH sts pointer Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_F800 + 32*$offset +  $stsid"
            
        def startAddress(self):
            return 0x0003f800
            
        def endAddress(self):
            return 0x0003f85f

        class _poh_sts_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_sts_cnt"
            
            def description(self):
                return "in case of poh_sts_cnt0 side: + offset = 0 :  pohtx_sts_dec + offset = 1 :  pohrx_sts_dec + offset = 2 :  Unused in case of poh_sts_cnt1 side: + offset = 0 :  pohtx_sts_inc + offset = 1 :  pohrx_sts_inc + offset = 2 :  Unused"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_sts_cnt"] = _AF6CNC0011_RD_GLBPMC._upen_poh_sts_cnt_ro._poh_sts_cnt()
            return allFields

    class _upen_pdh_de1cntval30_rc(AtRegister.AtRegister):
        def name(self):
            return "PDH ds1 cntval Counter Bus1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_0000 + 672*$offset + 28*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x00030000
            
        def endAddress(self):
            return 0x000307df

        class _ds1_cntval_bus1_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ds1_cntval_bus1_cnt"
            
            def description(self):
                return "in case of ds1_cntval_bus1_cnt0 side: + offset = 0 : fe + offset = 1 : rei + offset = 2 : unused in case of ds1_cntval_bus1_cnt1 side: + offset = 0 : crc + offset = 1 : lcv + offset = 2 : unused"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ds1_cntval_bus1_cnt"] = _AF6CNC0011_RD_GLBPMC._upen_pdh_de1cntval30_rc._ds1_cntval_bus1_cnt()
            return allFields

    class _upen_pdh_de1cntval30_ro(AtRegister.AtRegister):
        def name(self):
            return "PDH ds1 cntval Counter Bus1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_0800 + 672*$offset + 28*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x00030800
            
        def endAddress(self):
            return 0x00030fdf

        class _ds1_cntval_bus1_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ds1_cntval_bus1_cnt"
            
            def description(self):
                return "in case of ds1_cntval_bus1_cnt0 side: + offset = 0 : fe + offset = 1 : rei + offset = 2 : unused in case of ds1_cntval_bus1_cnt1 side: + offset = 0 : crc + offset = 1 : lcv + offset = 2 : unused"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ds1_cntval_bus1_cnt"] = _AF6CNC0011_RD_GLBPMC._upen_pdh_de1cntval30_ro._ds1_cntval_bus1_cnt()
            return allFields

    class _upen_pdh_de3cnt0_rc(AtRegister.AtRegister):
        def name(self):
            return "PDH ds3 cntval Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_8000 + 32*$offset + $stsid"
            
        def startAddress(self):
            return 0x00038000
            
        def endAddress(self):
            return 0x0003805f

        class _ds3_cntval_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ds3_cntval_cnt"
            
            def description(self):
                return "in case of ds3_cntval_cnt0 side: + offset = 0 : fe + offset = 1 : pb + offset = 2 : lcv in case of ds3_cntval_cnt1 side: + offset = 0 : rei + offset = 1 : cb + offset = 2 : unused"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ds3_cntval_cnt"] = _AF6CNC0011_RD_GLBPMC._upen_pdh_de3cnt0_rc._ds3_cntval_cnt()
            return allFields

    class _upen_pdh_de3cnt0_ro(AtRegister.AtRegister):
        def name(self):
            return "PDH ds3 cntval Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_8800 + 32*$offset + $stsid"
            
        def startAddress(self):
            return 0x00038800
            
        def endAddress(self):
            return 0x0003885f

        class _ds3_cntval_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ds3_cntval_cnt"
            
            def description(self):
                return "in case of ds3_cntval_cnt0 side: + offset = 0 : fe + offset = 1 : pb + offset = 2 : lcv in case of ds3_cntval_cnt1 side: + offset = 0 : rei + offset = 1 : cb + offset = 2 : unused"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ds3_cntval_cnt"] = _AF6CNC0011_RD_GLBPMC._upen_pdh_de3cnt0_ro._ds3_cntval_cnt()
            return allFields

    class _upen_de1prm_cnt0_rc(AtRegister.AtRegister):
        def name(self):
            return "PDH ds1 prm0 Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4_0000 + 672*$offset + 28*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x00040000
            
        def endAddress(self):
            return 0x000407df

        class _ds1_prm0_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ds1_prm0_cnt"
            
            def description(self):
                return "in case of ds1_prm0_cnt0 side: + offset = 0 : SL + offset = 1 : SE + offset = 2 : G5 in case of ds1_prm0_cnt1 side: + offset = 0 : LV + offset = 1 : G6 + offset = 2 : G4"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ds1_prm0_cnt"] = _AF6CNC0011_RD_GLBPMC._upen_de1prm_cnt0_rc._ds1_prm0_cnt()
            return allFields

    class _upen_de1prm_cnt0_ro(AtRegister.AtRegister):
        def name(self):
            return "PDH ds1 prm0 Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4_4000 + 672*$offset + 28*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x00044000
            
        def endAddress(self):
            return 0x000447df

        class _ds1_prm0_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ds1_prm0_cnt"
            
            def description(self):
                return "in case of ds1_prm0_cnt0 side: + offset = 0 : SL + offset = 1 : SE + offset = 2 : G5 in case of ds1_prm0_cnt1 side: + offset = 0 : LV + offset = 1 : G6 + offset = 2 : G4"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ds1_prm0_cnt"] = _AF6CNC0011_RD_GLBPMC._upen_de1prm_cnt0_ro._ds1_prm0_cnt()
            return allFields

    class _upen_de1prm_cnt1_rc(AtRegister.AtRegister):
        def name(self):
            return "PDH ds1 prm1 Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4_8000 + 672*$offset + 28*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x00048000
            
        def endAddress(self):
            return 0x000487df

        class _ds1_prm1_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ds1_prm1_cnt"
            
            def description(self):
                return "in case of ds1_prm1_cnt0 side: + offset = 0 : G3 + offset = 1 : G1 + offset = 2 : unused in case of ds1_prm1_cnt1 side: + offset = 0 : G2 + offset = 1 : unused + offset = 2 : unused"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ds1_prm1_cnt"] = _AF6CNC0011_RD_GLBPMC._upen_de1prm_cnt1_rc._ds1_prm1_cnt()
            return allFields

    class _upen_de1prm_cnt1_ro(AtRegister.AtRegister):
        def name(self):
            return "PDH ds1 prm1 Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4_C000 + 672*$offset + 28*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x0004c000
            
        def endAddress(self):
            return 0x0004c7df

        class _ds1_prm1_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ds1_prm1_cnt"
            
            def description(self):
                return "in case of ds1_prm1_cnt0 side: + offset = 0 : G3 + offset = 1 : G1 + offset = 2 : unused in case of ds1_prm1_cnt1 side: + offset = 0 : G2 + offset = 1 : unused + offset = 2 : unused"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ds1_prm1_cnt"] = _AF6CNC0011_RD_GLBPMC._upen_de1prm_cnt1_ro._ds1_prm1_cnt()
            return allFields

    class _upen_cla0_rc(AtRegister.AtRegister):
        def name(self):
            return "CLA Counter00"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b320
            
        def endAddress(self):
            return 0xffffffff

        class _cla_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cla_err"
            
            def description(self):
                return "cla_err"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cla_err"] = _AF6CNC0011_RD_GLBPMC._upen_cla0_rc._cla_err()
            return allFields

    class _upen_cla0_ro(AtRegister.AtRegister):
        def name(self):
            return "CLA Counter00"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b321
            
        def endAddress(self):
            return 0xffffffff

        class _cla_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cla_err"
            
            def description(self):
                return "cla_err"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cla_err"] = _AF6CNC0011_RD_GLBPMC._upen_cla0_ro._cla_err()
            return allFields

    class _upen_rxeth00_rc(AtRegister.AtRegister):
        def name(self):
            return "ETH Counter00"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b328
            
        def endAddress(self):
            return 0xffffffff

        class _rxpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpkt"
            
            def description(self):
                return "rxpkt"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpkt"] = _AF6CNC0011_RD_GLBPMC._upen_rxeth00_rc._rxpkt()
            return allFields

    class _upen_rxeth00_ro(AtRegister.AtRegister):
        def name(self):
            return "ETH Counter00"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b329
            
        def endAddress(self):
            return 0xffffffff

        class _rxpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpkt"
            
            def description(self):
                return "rxpkt"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpkt"] = _AF6CNC0011_RD_GLBPMC._upen_rxeth00_ro._rxpkt()
            return allFields

    class _upen_rxeth1_pkt_rc(AtRegister.AtRegister):
        def name(self):
            return "ETH Packet RX Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b370
            
        def endAddress(self):
            return 0xffffffff

        class _cntrxpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntrxpkt"
            
            def description(self):
                return "ETH rx Packet Counter"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntrxpkt"] = _AF6CNC0011_RD_GLBPMC._upen_rxeth1_pkt_rc._cntrxpkt()
            return allFields

    class _upen_rxeth1_pkt_ro(AtRegister.AtRegister):
        def name(self):
            return "ETH Packet RX Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b371
            
        def endAddress(self):
            return 0xffffffff

        class _cntrxpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntrxpkt"
            
            def description(self):
                return "ETH rx Packet Counter"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntrxpkt"] = _AF6CNC0011_RD_GLBPMC._upen_rxeth1_pkt_ro._cntrxpkt()
            return allFields

    class _upen_rxeth1_byte_rc(AtRegister.AtRegister):
        def name(self):
            return "ETH Byte Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b372
            
        def endAddress(self):
            return 0xffffffff

        class _cntrxbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntrxbyte"
            
            def description(self):
                return "ETH rx Byte Counter"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntrxbyte"] = _AF6CNC0011_RD_GLBPMC._upen_rxeth1_byte_rc._cntrxbyte()
            return allFields

    class _upen_rxeth1_byte_ro(AtRegister.AtRegister):
        def name(self):
            return "ETH Byte Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b373
            
        def endAddress(self):
            return 0xffffffff

        class _cntrxbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntrxbyte"
            
            def description(self):
                return "ETH rx Byte Counter"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntrxbyte"] = _AF6CNC0011_RD_GLBPMC._upen_rxeth1_byte_ro._cntrxbyte()
            return allFields

    class _upen_txeth00_rc(AtRegister.AtRegister):
        def name(self):
            return "ETH Counter00"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b338
            
        def endAddress(self):
            return 0xffffffff

        class _txpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txpkt"
            
            def description(self):
                return "txpkt"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txpkt"] = _AF6CNC0011_RD_GLBPMC._upen_txeth00_rc._txpkt()
            return allFields

    class _upen_txeth00_ro(AtRegister.AtRegister):
        def name(self):
            return "ETH Counter00"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b339
            
        def endAddress(self):
            return 0xffffffff

        class _txpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txpkt"
            
            def description(self):
                return "txpkt"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txpkt"] = _AF6CNC0011_RD_GLBPMC._upen_txeth00_ro._txpkt()
            return allFields

    class _upen_txeth1_pkt_rc(AtRegister.AtRegister):
        def name(self):
            return "ETH Packet TX Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b374
            
        def endAddress(self):
            return 0xffffffff

        class _cnttxpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnttxpkt"
            
            def description(self):
                return "ETH tx Packet Counter"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnttxpkt"] = _AF6CNC0011_RD_GLBPMC._upen_txeth1_pkt_rc._cnttxpkt()
            return allFields

    class _upen_txeth1_pkt_ro(AtRegister.AtRegister):
        def name(self):
            return "ETH Packet TX Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b375
            
        def endAddress(self):
            return 0xffffffff

        class _cnttxpkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnttxpkt"
            
            def description(self):
                return "ETH tx Packet Counter"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnttxpkt"] = _AF6CNC0011_RD_GLBPMC._upen_txeth1_pkt_ro._cnttxpkt()
            return allFields

    class _upen_txeth1_byte_rc(AtRegister.AtRegister):
        def name(self):
            return "ETH Byte TX Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b376
            
        def endAddress(self):
            return 0xffffffff

        class _cnttxbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnttxbyte"
            
            def description(self):
                return "ETH tx Byte Counter"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnttxbyte"] = _AF6CNC0011_RD_GLBPMC._upen_txeth1_byte_rc._cnttxbyte()
            return allFields

    class _upen_txeth1_byte_ro(AtRegister.AtRegister):
        def name(self):
            return "ETH Byte TX Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b377
            
        def endAddress(self):
            return 0xffffffff

        class _cnttxbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnttxbyte"
            
            def description(self):
                return "ETH tx Byte Counter"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnttxbyte"] = _AF6CNC0011_RD_GLBPMC._upen_txeth1_byte_ro._cnttxbyte()
            return allFields

    class _upen_cnttxoam_rc(AtRegister.AtRegister):
        def name(self):
            return "ETH TX Packet OAM CNT"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b336
            
        def endAddress(self):
            return 0xffffffff

        class _cnttxoamt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnttxoamt"
            
            def description(self):
                return "TX Packet OAM CNT"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnttxoamt"] = _AF6CNC0011_RD_GLBPMC._upen_cnttxoam_rc._cnttxoamt()
            return allFields

    class _upen_cnttxoam_ro(AtRegister.AtRegister):
        def name(self):
            return "ETH TX Packet OAM CNT"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b337
            
        def endAddress(self):
            return 0xffffffff

        class _cnttxoamt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnttxoamt"
            
            def description(self):
                return "TX Packet OAM CNT"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnttxoamt"] = _AF6CNC0011_RD_GLBPMC._upen_cnttxoam_ro._cnttxoamt()
            return allFields

    class _upen_cls_bcnt_rc(AtRegister.AtRegister):
        def name(self):
            return "Classifier Bytes Counter vld"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_B308 + $clsid"
            
        def startAddress(self):
            return 0x0003b308
            
        def endAddress(self):
            return 0x0003b30b

        class _bytecnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "bytecnt"
            
            def description(self):
                return "Classifier bytecnt"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["bytecnt"] = _AF6CNC0011_RD_GLBPMC._upen_cls_bcnt_rc._bytecnt()
            return allFields

    class _upen_cls_bcnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Classifier Bytes Counter vld"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_B30C + $clsid"
            
        def startAddress(self):
            return 0x0003b30c
            
        def endAddress(self):
            return 0x0003b30f

        class _bytecnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "bytecnt"
            
            def description(self):
                return "Classifier bytecnt"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["bytecnt"] = _AF6CNC0011_RD_GLBPMC._upen_cls_bcnt_ro._bytecnt()
            return allFields

    class _upen_cls_opcnt0_rc(AtRegister.AtRegister):
        def name(self):
            return "Classifier output counter pkt0"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_B348 + $offset"
            
        def startAddress(self):
            return 0x0003b348
            
        def endAddress(self):
            return 0x0003b34b

        class _opktcnt0_opktcnt1_(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "opktcnt0_opktcnt1_"
            
            def description(self):
                return "in case of opktcnt0 + offset = 0 : DS1/E1 good pkt counter + offset = 1 : Control pkt good counter + offset = 2 : FCs err cnt in case of opktcnt1 + offset = 0 : DS3/E3/EC1 good pkt counter + offset = 1 : Sequence err  counter + offset = 2 : len pkt err cnt"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["opktcnt0_opktcnt1_"] = _AF6CNC0011_RD_GLBPMC._upen_cls_opcnt0_rc._opktcnt0_opktcnt1_()
            return allFields

    class _upen_cls_opcnt0_ro(AtRegister.AtRegister):
        def name(self):
            return "Classifier output counter pkt0"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_B34C + $offset"
            
        def startAddress(self):
            return 0x0003b34c
            
        def endAddress(self):
            return 0x0003b34f

        class _opktcnt0_opktcnt1_(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "opktcnt0_opktcnt1_"
            
            def description(self):
                return "in case of opktcnt0 + offset = 0 : DS1/E1 good pkt counter + offset = 1 : Control pkt good counter + offset = 2 : FCs err cnt in case of opktcnt1 + offset = 0 : DS3/E3/EC1 good pkt counter + offset = 1 : Sequence err  counter + offset = 2 : len pkt err cnt"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["opktcnt0_opktcnt1_"] = _AF6CNC0011_RD_GLBPMC._upen_cls_opcnt0_ro._opktcnt0_opktcnt1_()
            return allFields

    class _upen_cls_opcnt1_rc(AtRegister.AtRegister):
        def name(self):
            return "Classifier output counter pkt1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_B350 + $offset"
            
        def startAddress(self):
            return 0x0003b350
            
        def endAddress(self):
            return 0x0003b353

        class _opktcnt0_opktcnt1_(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "opktcnt0_opktcnt1_"
            
            def description(self):
                return "in case of opktcnt0 + offset = 0 : DA error pkt counter + offset = 1 : E-type err pkt counter + offset = 2 : Unused in case of opktcnt1 + offset = 0 : SA error pkt counter + offset = 1 : Length field err counter + offset = 2 : Unused"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["opktcnt0_opktcnt1_"] = _AF6CNC0011_RD_GLBPMC._upen_cls_opcnt1_rc._opktcnt0_opktcnt1_()
            return allFields

    class _upen_cls_opcnt1_ro(AtRegister.AtRegister):
        def name(self):
            return "Classifier output counter pkt1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_B354 + $offset"
            
        def startAddress(self):
            return 0x0003b354
            
        def endAddress(self):
            return 0x0003b357

        class _opktcnt0_opktcnt1_(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "opktcnt0_opktcnt1_"
            
            def description(self):
                return "in case of opktcnt0 + offset = 0 : DA error pkt counter + offset = 1 : E-type err pkt counter + offset = 2 : Unused in case of opktcnt1 + offset = 0 : SA error pkt counter + offset = 1 : Length field err counter + offset = 2 : Unused"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["opktcnt0_opktcnt1_"] = _AF6CNC0011_RD_GLBPMC._upen_cls_opcnt1_ro._opktcnt0_opktcnt1_()
            return allFields

    class _upen_cls_ipcnt_rc(AtRegister.AtRegister):
        def name(self):
            return "Classifier input counter pkt"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b306
            
        def endAddress(self):
            return 0xffffffff

        class _ipktcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ipktcnt"
            
            def description(self):
                return "input packet cnt"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ipktcnt"] = _AF6CNC0011_RD_GLBPMC._upen_cls_ipcnt_rc._ipktcnt()
            return allFields

    class _upen_cls_ipcnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Classifier input counter pkt"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b307
            
        def endAddress(self):
            return 0xffffffff

        class _ipktcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ipktcnt"
            
            def description(self):
                return "input packet cnt"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ipktcnt"] = _AF6CNC0011_RD_GLBPMC._upen_cls_ipcnt_ro._ipktcnt()
            return allFields

    class _upen_eth_bytecnt_rc(AtRegister.AtRegister):
        def name(self):
            return "PDH Mux Ethernet Byte Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b300
            
        def endAddress(self):
            return 0xffffffff

        class _eth_bytecnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "eth_bytecnt"
            
            def description(self):
                return "ETH Byte Counter"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["eth_bytecnt"] = _AF6CNC0011_RD_GLBPMC._upen_eth_bytecnt_rc._eth_bytecnt()
            return allFields

    class _upen_eth_bytecnt_ro(AtRegister.AtRegister):
        def name(self):
            return "PDH Mux Ethernet Byte Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b301
            
        def endAddress(self):
            return 0xffffffff

        class _eth_bytecnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "eth_bytecnt"
            
            def description(self):
                return "ETH Byte Counter"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["eth_bytecnt"] = _AF6CNC0011_RD_GLBPMC._upen_eth_bytecnt_ro._eth_bytecnt()
            return allFields

    class _upen_eth_bytecnt_rc(AtRegister.AtRegister):
        def name(self):
            return "PDH Mux Ethernet Packet Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b340
            
        def endAddress(self):
            return 0xffffffff

        class _eth_pktcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "eth_pktcnt"
            
            def description(self):
                return "ETH Packet Counter"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["eth_pktcnt"] = _AF6CNC0011_RD_GLBPMC._upen_eth_bytecnt_rc._eth_pktcnt()
            return allFields

    class _upen_eth_bytecnt_ro(AtRegister.AtRegister):
        def name(self):
            return "PDH Mux Ethernet Packet Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0003b341
            
        def endAddress(self):
            return 0xffffffff

        class _eth_pktcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "eth_pktcnt"
            
            def description(self):
                return "ETH Packet Counter"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["eth_pktcnt"] = _AF6CNC0011_RD_GLBPMC._upen_eth_bytecnt_ro._eth_pktcnt()
            return allFields

    class _upen_de1_tdmvld_cnt_rc(AtRegister.AtRegister):
        def name(self):
            return "DE1 TDM bit counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_B200 + $lid"
            
        def startAddress(self):
            return 0x0003b200
            
        def endAddress(self):
            return 0x0003b27f

        class _de1_bit_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "de1_bit_cnt"
            
            def description(self):
                return "bit_cnt"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["de1_bit_cnt"] = _AF6CNC0011_RD_GLBPMC._upen_de1_tdmvld_cnt_rc._de1_bit_cnt()
            return allFields

    class _upen_de1_tdmvld_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "DE1 TDM bit counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_B280 + $lid"
            
        def startAddress(self):
            return 0x0003b280
            
        def endAddress(self):
            return 0x0003b2ff

        class _de1_bit_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "de1_bit_cnt"
            
            def description(self):
                return "bit_cnt"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["de1_bit_cnt"] = _AF6CNC0011_RD_GLBPMC._upen_de1_tdmvld_cnt_ro._de1_bit_cnt()
            return allFields

    class _upen_de3_tdmvld_cnt_rc(AtRegister.AtRegister):
        def name(self):
            return "DE3 TDM bit counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_B180 + $lid"
            
        def startAddress(self):
            return 0x0003b180
            
        def endAddress(self):
            return 0x0003b19f

        class _de3_bit_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "de3_bit_cnt"
            
            def description(self):
                return "bit_cnt"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["de3_bit_cnt"] = _AF6CNC0011_RD_GLBPMC._upen_de3_tdmvld_cnt_rc._de3_bit_cnt()
            return allFields

    class _upen_de3_tdmvld_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "DE3 TDM bit counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_B1A0 + $lid"
            
        def startAddress(self):
            return 0x0003b1a0
            
        def endAddress(self):
            return 0x0003b1bf

        class _de3_bit_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "de3_bit_cnt"
            
            def description(self):
                return "bit_cnt"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["de3_bit_cnt"] = _AF6CNC0011_RD_GLBPMC._upen_de3_tdmvld_cnt_ro._de3_bit_cnt()
            return allFields

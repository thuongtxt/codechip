import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0022_RD_CDR_v3_TimGen(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["cdr_cpu_hold_ctrl"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_cpu_hold_ctrl()
        allRegisters["cdr_line_mode_ctrl"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_line_mode_ctrl()
        allRegisters["cdr_timing_extref_ctrl"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_timing_extref_ctrl()
        allRegisters["cdr_sts_timing_ctrl"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_sts_timing_ctrl()
        allRegisters["cdr_vt_timing_ctrl"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_vt_timing_ctrl()
        allRegisters["cdr_top_timing_frmsync_ctrl"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_top_timing_frmsync_ctrl()
        allRegisters["cdr_refsync_master_octrl"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_refsync_master_octrl()
        allRegisters["cdr_refsync_slaver_octrl"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_refsync_slaver_octrl()
        allRegisters["RAM_TimingGen_Parity_Force_Control"] = _AF6CNC0022_RD_CDR_v3_TimGen._RAM_TimingGen_Parity_Force_Control()
        allRegisters["RAM_TimingGen_Parity_Disbale_Control"] = _AF6CNC0022_RD_CDR_v3_TimGen._RAM_TimingGen_Parity_Disbale_Control()
        allRegisters["RAM_TimingGen_Parity_Error_Sticky"] = _AF6CNC0022_RD_CDR_v3_TimGen._RAM_TimingGen_Parity_Error_Sticky()
        allRegisters["rdha3_0_control"] = _AF6CNC0022_RD_CDR_v3_TimGen._rdha3_0_control()
        allRegisters["rdha7_4_control"] = _AF6CNC0022_RD_CDR_v3_TimGen._rdha7_4_control()
        allRegisters["rdha11_8_control"] = _AF6CNC0022_RD_CDR_v3_TimGen._rdha11_8_control()
        allRegisters["rdha15_12_control"] = _AF6CNC0022_RD_CDR_v3_TimGen._rdha15_12_control()
        allRegisters["rdha19_16_control"] = _AF6CNC0022_RD_CDR_v3_TimGen._rdha19_16_control()
        allRegisters["rdha23_20_control"] = _AF6CNC0022_RD_CDR_v3_TimGen._rdha23_20_control()
        allRegisters["rdha24data_control"] = _AF6CNC0022_RD_CDR_v3_TimGen._rdha24data_control()
        allRegisters["rdha_hold63_32"] = _AF6CNC0022_RD_CDR_v3_TimGen._rdha_hold63_32()
        allRegisters["rdindr_hold95_64"] = _AF6CNC0022_RD_CDR_v3_TimGen._rdindr_hold95_64()
        allRegisters["rdindr_hold127_96"] = _AF6CNC0022_RD_CDR_v3_TimGen._rdindr_hold127_96()
        return allRegisters

    class _cdr_cpu_hold_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR CPU  Reg Hold Control"
    
        def description(self):
            return "The register provides hold register for three word 32-bits MSB when CPU access to engine."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x030000 + HoldId"
            
        def startAddress(self):
            return 0x00030000
            
        def endAddress(self):
            return 0x00030002

        class _HoldReg0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HoldReg0"
            
            def description(self):
                return "Hold 32 bits"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HoldReg0"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_cpu_hold_ctrl._HoldReg0()
            return allFields

    class _cdr_line_mode_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR line mode control"
    
        def description(self):
            return "The STS configuration registers are used to configure kinds of STS-1s and the kinds of VT/TUs in each STS-1."
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00000200 +  STS"
            
        def startAddress(self):
            return 0x00000200
            
        def endAddress(self):
            return 0x0000022f

        class _VC3Mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "VC3Mode"
            
            def description(self):
                return "VC3 or TU3 mode, each bit is used configured for each STS (only valid in TU3 CEP) 1: TU3 CEP 0: Other mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3Mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DE3Mode"
            
            def description(self):
                return "DS3 or E3 mode, each bit is used configured for each STS. 1: DS3 mode 0: E3 mod"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS1STSMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "STS1STSMode"
            
            def description(self):
                return "STS mode of STS1 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS1VTType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "STS1VTType"
            
            def description(self):
                return "VT Type of 7 VTG in STS1, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VC3Mode"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_line_mode_ctrl._VC3Mode()
            allFields["DE3Mode"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_line_mode_ctrl._DE3Mode()
            allFields["STS1STSMode"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_line_mode_ctrl._STS1STSMode()
            allFields["STS1VTType"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_line_mode_ctrl._STS1VTType()
            return allFields

    class _cdr_timing_extref_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR Timing External reference control"
    
        def description(self):
            return "This register is used to configure for the 2Khz coefficient of two external reference signal in order to generate timing. The mean that, if the reference signal is 8Khz, the  coefficient must be configured 4 (I.e 8Khz  = 4x2Khz)"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _Ext3N2k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 32
        
            def name(self):
                return "Ext3N2k"
            
            def description(self):
                return "The 2Khz coefficient of the third external reference signal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Ext2N2k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Ext2N2k"
            
            def description(self):
                return "The 2Khz coefficient of the second external reference signal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Ext1N2k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Ext1N2k"
            
            def description(self):
                return "The 2Khz coefficient of the first external reference signal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Ext3N2k"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_timing_extref_ctrl._Ext3N2k()
            allFields["Ext2N2k"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_timing_extref_ctrl._Ext2N2k()
            allFields["Ext1N2k"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_timing_extref_ctrl._Ext1N2k()
            return allFields

    class _cdr_sts_timing_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR STS Timing control"
    
        def description(self):
            return "This register is used to configure timing mode for per STS"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0001000+STS"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0x0000102f

        class _CDRChid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 20
        
            def name(self):
                return "CDRChid"
            
            def description(self):
                return "CDR channel ID in CDR mode."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VC3EParEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "VC3EParEn"
            
            def description(self):
                return "VC3 EPAR mode 0 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapSTSMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "MapSTSMode"
            
            def description(self):
                return "Map STS mode 0: Payload STS mode 1: Payload DE3 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3TimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DE3TimeMode"
            
            def description(self):
                return "DE3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR timing mode 9: OCN System timing mode 10: DCR timing mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VC3TimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VC3TimeMode"
            
            def description(self):
                return "VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR timing mode for CEP mode 9: OCN System timing mode 10: DCR timing mode for CEP mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRChid"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_sts_timing_ctrl._CDRChid()
            allFields["VC3EParEn"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_sts_timing_ctrl._VC3EParEn()
            allFields["MapSTSMode"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_sts_timing_ctrl._MapSTSMode()
            allFields["DE3TimeMode"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_sts_timing_ctrl._DE3TimeMode()
            allFields["VC3TimeMode"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_sts_timing_ctrl._VC3TimeMode()
            return allFields

    class _cdr_vt_timing_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR VT Timing control"
    
        def description(self):
            return "This register is used to configure timing mode for per VT"
            
        def width(self):
            return 24
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0001800 + STS*32 + TUG*4 + VT"
            
        def startAddress(self):
            return 0x00001800
            
        def endAddress(self):
            return 0x00001fff

        class _CDRChid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 12
        
            def name(self):
                return "CDRChid"
            
            def description(self):
                return "CDR channel ID in CDR mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VTEParEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "VTEParEn"
            
            def description(self):
                return "VT EPAR mode 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapVTMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "MapVTMode"
            
            def description(self):
                return "Map VT mode 0: Payload VT15/VT2 mode 1: Payload DS1/E1 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1TimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DE1TimeMode"
            
            def description(self):
                return "DE1 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR timing mode 9: OCN System timing mode 10: DCR timing mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VTTimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VTTimeMode"
            
            def description(self):
                return "VT time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR timing mode for CEP mode 9: OCN System timing mode 10: DCR timing mode for CEP mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRChid"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_vt_timing_ctrl._CDRChid()
            allFields["VTEParEn"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_vt_timing_ctrl._VTEParEn()
            allFields["MapVTMode"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_vt_timing_ctrl._MapVTMode()
            allFields["DE1TimeMode"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_vt_timing_ctrl._DE1TimeMode()
            allFields["VTTimeMode"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_vt_timing_ctrl._VTTimeMode()
            return allFields

    class _cdr_top_timing_frmsync_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR ToP Timing Frame sync 8K control"
    
        def description(self):
            return "This register is used to configure timing source to generate the 125us signal frame sync for ToP"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001040
            
        def endAddress(self):
            return 0xffffffff

        class _ToPPDHLineType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ToPPDHLineType"
            
            def description(self):
                return "Line type of DS1/E1 loop time or PDH CDR 0: E1 1: DS1 2: VT2 3: VT15 4: E3 5: DS3 6: STS1 7: Reserve"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ToPPDHLineID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 4
        
            def name(self):
                return "ToPPDHLineID"
            
            def description(self):
                return "Line ID of DS1/E1 loop time or PDH/CEP CDR"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ToPTimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ToPTimeMode"
            
            def description(self):
                return "Time mode  for ToP 0: System timing mode 1: DS1/E1 Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: PDH CDR timing mode 9: OCN System timing mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ToPPDHLineType"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_top_timing_frmsync_ctrl._ToPPDHLineType()
            allFields["ToPPDHLineID"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_top_timing_frmsync_ctrl._ToPPDHLineID()
            allFields["ToPTimeMode"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_top_timing_frmsync_ctrl._ToPTimeMode()
            return allFields

    class _cdr_refsync_master_octrl(AtRegister.AtRegister):
        def name(self):
            return "CDR Reference Sync 8K Master Output control"
    
        def description(self):
            return "This register is used to configure timing source to generate the reference sync master output signal"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001041
            
        def endAddress(self):
            return 0xffffffff

        class _RefOut1PDHLineType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RefOut1PDHLineType"
            
            def description(self):
                return "Line type of DS1/E1 loop time or PDH/CEP CDR 0: E1 1: DS1 2: VT2 3: VT15 4: E3 5: DS3 6: STS1 7: Reserve"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RefOut1PDHLineID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RefOut1PDHLineID"
            
            def description(self):
                return "Line ID of DS1/E1 loop time or PDH CDR"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RefOut1TimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RefOut1TimeMode"
            
            def description(self):
                return "Time mode for RefOut1 0: System timing mode 1: DS1/E1 Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: PDH CDR timing mode 9: External Clock sync"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RefOut1PDHLineType"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_refsync_master_octrl._RefOut1PDHLineType()
            allFields["RefOut1PDHLineID"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_refsync_master_octrl._RefOut1PDHLineID()
            allFields["RefOut1TimeMode"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_refsync_master_octrl._RefOut1TimeMode()
            return allFields

    class _cdr_refsync_slaver_octrl(AtRegister.AtRegister):
        def name(self):
            return "CDR Reference Sync 8k Slaver Output control"
    
        def description(self):
            return "This register is used to configure timing source to generate the reference sync slaver output signal"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001042
            
        def endAddress(self):
            return 0xffffffff

        class _RefOut2PDHLineType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RefOut2PDHLineType"
            
            def description(self):
                return "Line type of DS1/E1 loop time or PDH/CEP CDR 0: E1 1: DS1 2: VT2 3: VT15 4: E3 5: DS3 6: STS1 7: Reserve"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RefOut2PDHLineID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RefOut2PDHLineID"
            
            def description(self):
                return "Line ID of DS1/E1 loop time or PDH CDR"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RefOut2TimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RefOut2TimeMode"
            
            def description(self):
                return "Time mode for RefOut1 0: System timing mode 1: DS1/E1 Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: PDH CDR timing mode 9: External Clock sync"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RefOut2PDHLineType"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_refsync_slaver_octrl._RefOut2PDHLineType()
            allFields["RefOut2PDHLineID"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_refsync_slaver_octrl._RefOut2PDHLineID()
            allFields["RefOut2TimeMode"] = _AF6CNC0022_RD_CDR_v3_TimGen._cdr_refsync_slaver_octrl._RefOut2TimeMode()
            return allFields

    class _RAM_TimingGen_Parity_Force_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM TimingGen Parity Force Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000104c
            
        def endAddress(self):
            return 0xffffffff

        class _CDRVTTimingCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "CDRVTTimingCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"CDR VT Timing control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRSTSTimingCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRSTSTimingCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"CDR STS Timing control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRVTTimingCtrl_ParErrFrc"] = _AF6CNC0022_RD_CDR_v3_TimGen._RAM_TimingGen_Parity_Force_Control._CDRVTTimingCtrl_ParErrFrc()
            allFields["CDRSTSTimingCtrl_ParErrFrc"] = _AF6CNC0022_RD_CDR_v3_TimGen._RAM_TimingGen_Parity_Force_Control._CDRSTSTimingCtrl_ParErrFrc()
            return allFields

    class _RAM_TimingGen_Parity_Disbale_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM TimingGen Parity Disable Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000104d
            
        def endAddress(self):
            return 0xffffffff

        class _CDRVTTimingCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "CDRVTTimingCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"CDR VT Timing control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRSTSTimingCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRSTSTimingCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"CDR STS Timing control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRVTTimingCtrl_ParErrDis"] = _AF6CNC0022_RD_CDR_v3_TimGen._RAM_TimingGen_Parity_Disbale_Control._CDRVTTimingCtrl_ParErrDis()
            allFields["CDRSTSTimingCtrl_ParErrDis"] = _AF6CNC0022_RD_CDR_v3_TimGen._RAM_TimingGen_Parity_Disbale_Control._CDRSTSTimingCtrl_ParErrDis()
            return allFields

    class _RAM_TimingGen_Parity_Error_Sticky(AtRegister.AtRegister):
        def name(self):
            return "RAM TimingGen parity Error Sticky"
    
        def description(self):
            return "This register configures disable parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000104e
            
        def endAddress(self):
            return 0xffffffff

        class _CDRVTTimingCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "CDRVTTimingCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"CDR VT Timing control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRSTSTimingCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRSTSTimingCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"CDR STS Timing control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRVTTimingCtrl_ParErrStk"] = _AF6CNC0022_RD_CDR_v3_TimGen._RAM_TimingGen_Parity_Error_Sticky._CDRVTTimingCtrl_ParErrStk()
            allFields["CDRSTSTimingCtrl_ParErrStk"] = _AF6CNC0022_RD_CDR_v3_TimGen._RAM_TimingGen_Parity_Error_Sticky._CDRSTSTimingCtrl_ParErrStk()
            return allFields

    class _rdha3_0_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit3_0 Control"
    
        def description(self):
            return "This register is used to send HA read address bit3_0 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x30200 + HaAddr3_0"
            
        def startAddress(self):
            return 0x00030200
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr3_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr3_0"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr3_0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr3_0"] = _AF6CNC0022_RD_CDR_v3_TimGen._rdha3_0_control._ReadAddr3_0()
            return allFields

    class _rdha7_4_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit7_4 Control"
    
        def description(self):
            return "This register is used to send HA read address bit7_4 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x30210 + HaAddr7_4"
            
        def startAddress(self):
            return 0x00030210
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr7_4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr7_4"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr7_4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr7_4"] = _AF6CNC0022_RD_CDR_v3_TimGen._rdha7_4_control._ReadAddr7_4()
            return allFields

    class _rdha11_8_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit11_8 Control"
    
        def description(self):
            return "This register is used to send HA read address bit11_8 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x30220 + HaAddr11_8"
            
        def startAddress(self):
            return 0x00030220
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr11_8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr11_8"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr11_8"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr11_8"] = _AF6CNC0022_RD_CDR_v3_TimGen._rdha11_8_control._ReadAddr11_8()
            return allFields

    class _rdha15_12_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit15_12 Control"
    
        def description(self):
            return "This register is used to send HA read address bit15_12 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x30230 + HaAddr15_12"
            
        def startAddress(self):
            return 0x00030230
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr15_12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr15_12"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr15_12"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr15_12"] = _AF6CNC0022_RD_CDR_v3_TimGen._rdha15_12_control._ReadAddr15_12()
            return allFields

    class _rdha19_16_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit19_16 Control"
    
        def description(self):
            return "This register is used to send HA read address bit19_16 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x30240 + HaAddr19_16"
            
        def startAddress(self):
            return 0x00030240
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr19_16(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr19_16"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr19_16"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr19_16"] = _AF6CNC0022_RD_CDR_v3_TimGen._rdha19_16_control._ReadAddr19_16()
            return allFields

    class _rdha23_20_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit23_20 Control"
    
        def description(self):
            return "This register is used to send HA read address bit23_20 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x30250 + HaAddr23_20"
            
        def startAddress(self):
            return 0x00030250
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr23_20(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr23_20"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr23_20"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr23_20"] = _AF6CNC0022_RD_CDR_v3_TimGen._rdha23_20_control._ReadAddr23_20()
            return allFields

    class _rdha24data_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit24 and Data Control"
    
        def description(self):
            return "This register is used to send HA read address bit24 to HA engine to read data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x30260 + HaAddr24"
            
        def startAddress(self):
            return 0x00030260
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData31_0"
            
            def description(self):
                return "HA read data bit31_0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData31_0"] = _AF6CNC0022_RD_CDR_v3_TimGen._rdha24data_control._ReadHaData31_0()
            return allFields

    class _rdha_hold63_32(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data63_32"
    
        def description(self):
            return "This register is used to read HA dword2 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00030270
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData63_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData63_32"
            
            def description(self):
                return "HA read data bit63_32"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData63_32"] = _AF6CNC0022_RD_CDR_v3_TimGen._rdha_hold63_32._ReadHaData63_32()
            return allFields

    class _rdindr_hold95_64(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data95_64"
    
        def description(self):
            return "This register is used to read HA dword3 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00030271
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData95_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData95_64"
            
            def description(self):
                return "HA read data bit95_64"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData95_64"] = _AF6CNC0022_RD_CDR_v3_TimGen._rdindr_hold95_64._ReadHaData95_64()
            return allFields

    class _rdindr_hold127_96(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data127_96"
    
        def description(self):
            return "This register is used to read HA dword4 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00030272
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData127_96(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData127_96"
            
            def description(self):
                return "HA read data bit127_96"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData127_96"] = _AF6CNC0022_RD_CDR_v3_TimGen._rdindr_hold127_96._ReadHaData127_96()
            return allFields

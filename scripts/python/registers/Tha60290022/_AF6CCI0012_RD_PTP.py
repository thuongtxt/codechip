import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0012_RD_PTP(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_hold1"] = _AF6CCI0012_RD_PTP._upen_hold1()
        allRegisters["upen_hold2"] = _AF6CCI0012_RD_PTP._upen_hold2()
        allRegisters["upen_hold3"] = _AF6CCI0012_RD_PTP._upen_hold3()
        allRegisters["upen_ptp_en"] = _AF6CCI0012_RD_PTP._upen_ptp_en()
        allRegisters["upen_ptp_stmode"] = _AF6CCI0012_RD_PTP._upen_ptp_stmode()
        allRegisters["upen_ptp_ms"] = _AF6CCI0012_RD_PTP._upen_ptp_ms()
        allRegisters["upen_ptp_dev"] = _AF6CCI0012_RD_PTP._upen_ptp_dev()
        allRegisters["upen_ms_srcpid"] = _AF6CCI0012_RD_PTP._upen_ms_srcpid()
        allRegisters["upen_sl_reqpid"] = _AF6CCI0012_RD_PTP._upen_sl_reqpid()
        allRegisters["upen_chsv4_bypass"] = _AF6CCI0012_RD_PTP._upen_chsv4_bypass()
        allRegisters["upen_ptp_rdy"] = _AF6CCI0012_RD_PTP._upen_ptp_rdy()
        allRegisters["upen_tx_ptp_cnt_r2c"] = _AF6CCI0012_RD_PTP._upen_tx_ptp_cnt_r2c()
        allRegisters["upen_tx_ptp_cnt_ro"] = _AF6CCI0012_RD_PTP._upen_tx_ptp_cnt_ro()
        allRegisters["upen_rx_ptp_cnt_r2c"] = _AF6CCI0012_RD_PTP._upen_rx_ptp_cnt_r2c()
        allRegisters["upen_rx_ptp_cnt_ro"] = _AF6CCI0012_RD_PTP._upen_rx_ptp_cnt_ro()
        allRegisters["upen_rx_sync_cnt_r2c"] = _AF6CCI0012_RD_PTP._upen_rx_sync_cnt_r2c()
        allRegisters["upen_rx_sync_cnt_ro"] = _AF6CCI0012_RD_PTP._upen_rx_sync_cnt_ro()
        allRegisters["upen_rx_folu_cnt_r2c"] = _AF6CCI0012_RD_PTP._upen_rx_folu_cnt_r2c()
        allRegisters["upen_rx_folu_cnt_ro"] = _AF6CCI0012_RD_PTP._upen_rx_folu_cnt_ro()
        allRegisters["upen_rx_dreq_cnt_r2c"] = _AF6CCI0012_RD_PTP._upen_rx_dreq_cnt_r2c()
        allRegisters["upen_rx_dreq_cnt_ro"] = _AF6CCI0012_RD_PTP._upen_rx_dreq_cnt_ro()
        allRegisters["upen_rx_dres_cnt_r2c"] = _AF6CCI0012_RD_PTP._upen_rx_dres_cnt_r2c()
        allRegisters["upen_rx_dres_cnt_ro"] = _AF6CCI0012_RD_PTP._upen_rx_dres_cnt_ro()
        allRegisters["upen_rx_extr_err_syncnt_r2c"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_syncnt_r2c()
        allRegisters["upen_rx_extr_err_syncnt_ro"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_syncnt_ro()
        allRegisters["upen_rx_extr_err_folucnt_r2c"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_folucnt_r2c()
        allRegisters["upen_rx_extr_err_folucnt_ro"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_folucnt_ro()
        allRegisters["upen_rx_extr_err_dreqcnt_r2c"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_dreqcnt_r2c()
        allRegisters["upen_rx_extr_err_dreqcnt_ro"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_dreqcnt_ro()
        allRegisters["upen_rx_extr_err_drescnt_r2c"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_drescnt_r2c()
        allRegisters["upen_rx_extr_err_drescnt_ro"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_drescnt_ro()
        allRegisters["upen_rx_chs_err_syncnt_r2c"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_syncnt_r2c()
        allRegisters["upen_rx_chs_err_syncnt_ro"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_syncnt_ro()
        allRegisters["upen_rx_chs_err_folucnt_r2c"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_folucnt_r2c()
        allRegisters["upen_rx_chs_err_folucnt_ro"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_folucnt_ro()
        allRegisters["upen_rx_chs_err_dreqcnt_r2c"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_dreqcnt_r2c()
        allRegisters["upen_rx_chs_err_dreqcnt_ro"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_dreqcnt_ro()
        allRegisters["upen_rx_chs_err_drescnt_r2c"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_drescnt_r2c()
        allRegisters["upen_rx_chs_err_drescnt_ro"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_drescnt_ro()
        allRegisters["upen_tx_sync_cnt_r2c"] = _AF6CCI0012_RD_PTP._upen_tx_sync_cnt_r2c()
        allRegisters["upen_tx_sync_cnt_ro"] = _AF6CCI0012_RD_PTP._upen_tx_sync_cnt_ro()
        allRegisters["upen_tx_folu_cnt_r2c"] = _AF6CCI0012_RD_PTP._upen_tx_folu_cnt_r2c()
        allRegisters["upen_tx_folu_cnt_ro"] = _AF6CCI0012_RD_PTP._upen_tx_folu_cnt_ro()
        allRegisters["upen_tx_dreq_cnt_r2c"] = _AF6CCI0012_RD_PTP._upen_tx_dreq_cnt_r2c()
        allRegisters["upen_tx_dreq_cnt_ro"] = _AF6CCI0012_RD_PTP._upen_tx_dreq_cnt_ro()
        allRegisters["upen_tx_dres_cnt_r2c"] = _AF6CCI0012_RD_PTP._upen_tx_dres_cnt_r2c()
        allRegisters["upen_tx_dres_cnt_ro"] = _AF6CCI0012_RD_PTP._upen_tx_dres_cnt_ro()
        allRegisters["upen_cla_stk"] = _AF6CCI0012_RD_PTP._upen_cla_stk()
        allRegisters["upen_chk_stk"] = _AF6CCI0012_RD_PTP._upen_chk_stk()
        allRegisters["upen_getinf_stk"] = _AF6CCI0012_RD_PTP._upen_getinf_stk()
        allRegisters["upen_trans_stk"] = _AF6CCI0012_RD_PTP._upen_trans_stk()
        allRegisters["upen_cpuque_stk"] = _AF6CCI0012_RD_PTP._upen_cpuque_stk()
        allRegisters["upen_t1t3mode"] = _AF6CCI0012_RD_PTP._upen_t1t3mode()
        allRegisters["upen_cpu_flush"] = _AF6CCI0012_RD_PTP._upen_cpu_flush()
        allRegisters["upen_cpuque"] = _AF6CCI0012_RD_PTP._upen_cpuque()
        allRegisters["upen_int_en"] = _AF6CCI0012_RD_PTP._upen_int_en()
        allRegisters["upen_int"] = _AF6CCI0012_RD_PTP._upen_int()
        allRegisters["upen_sta"] = _AF6CCI0012_RD_PTP._upen_sta()
        allRegisters["upen_umac_glb"] = _AF6CCI0012_RD_PTP._upen_umac_glb()
        allRegisters["upen_mmac1_glb"] = _AF6CCI0012_RD_PTP._upen_mmac1_glb()
        allRegisters["upen_mmac2_glb"] = _AF6CCI0012_RD_PTP._upen_mmac2_glb()
        allRegisters["upen_anymac_en"] = _AF6CCI0012_RD_PTP._upen_anymac_en()
        allRegisters["upen_macglb_en"] = _AF6CCI0012_RD_PTP._upen_macglb_en()
        allRegisters["upen_v4mip1_glb"] = _AF6CCI0012_RD_PTP._upen_v4mip1_glb()
        allRegisters["upen_v4mip2_glb"] = _AF6CCI0012_RD_PTP._upen_v4mip2_glb()
        allRegisters["upen_v6mip1_glb"] = _AF6CCI0012_RD_PTP._upen_v6mip1_glb()
        allRegisters["upen_v6mip2_glb"] = _AF6CCI0012_RD_PTP._upen_v6mip2_glb()
        allRegisters["upen_anyip_en"] = _AF6CCI0012_RD_PTP._upen_anyip_en()
        allRegisters["upen_cla_rdy"] = _AF6CCI0012_RD_PTP._upen_cla_rdy()
        allRegisters["upen_force_ptplayer"] = _AF6CCI0012_RD_PTP._upen_force_ptplayer()
        allRegisters["upen_mru_val"] = _AF6CCI0012_RD_PTP._upen_mru_val()
        allRegisters["upen_mac"] = _AF6CCI0012_RD_PTP._upen_mac()
        allRegisters["upen_geip1"] = _AF6CCI0012_RD_PTP._upen_geip1()
        allRegisters["upen_geip2"] = _AF6CCI0012_RD_PTP._upen_geip2()
        allRegisters["upen_geip3"] = _AF6CCI0012_RD_PTP._upen_geip3()
        allRegisters["upen_geip4"] = _AF6CCI0012_RD_PTP._upen_geip4()
        allRegisters["upen_ptl"] = _AF6CCI0012_RD_PTP._upen_ptl()
        allRegisters["upen_geipen"] = _AF6CCI0012_RD_PTP._upen_geipen()
        allRegisters["upen_eth2cla_stk"] = _AF6CCI0012_RD_PTP._upen_eth2cla_stk()
        allRegisters["upen_cla2epa_stk"] = _AF6CCI0012_RD_PTP._upen_cla2epa_stk()
        allRegisters["upen_cla2ptp_stk"] = _AF6CCI0012_RD_PTP._upen_cla2ptp_stk()
        allRegisters["upen_parser_stk0"] = _AF6CCI0012_RD_PTP._upen_parser_stk0()
        allRegisters["upen_parser_stk1"] = _AF6CCI0012_RD_PTP._upen_parser_stk1()
        allRegisters["upen_lkup_stk"] = _AF6CCI0012_RD_PTP._upen_lkup_stk()
        allRegisters["upen_datashidt_stk"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk()
        allRegisters["upen_allc_stk"] = _AF6CCI0012_RD_PTP._upen_allc_stk()
        allRegisters["upen_allc_blkfre_cnt"] = _AF6CCI0012_RD_PTP._upen_allc_blkfre_cnt()
        allRegisters["upen_allc_blkiss_cnt"] = _AF6CCI0012_RD_PTP._upen_allc_blkiss_cnt()
        allRegisters["upen_todinit_cfg"] = _AF6CCI0012_RD_PTP._upen_todinit_cfg()
        allRegisters["upen_tod_cur"] = _AF6CCI0012_RD_PTP._upen_tod_cur()
        allRegisters["upen_tod_mode"] = _AF6CCI0012_RD_PTP._upen_tod_mode()
        allRegisters["upen_bp_latency_mode"] = _AF6CCI0012_RD_PTP._upen_bp_latency_mode()
        allRegisters["upen_face_latency_mode"] = _AF6CCI0012_RD_PTP._upen_face_latency_mode()
        allRegisters["upen_hold_40g1"] = _AF6CCI0012_RD_PTP._upen_hold_40g1()
        allRegisters["upen_hold_40g2"] = _AF6CCI0012_RD_PTP._upen_hold_40g2()
        allRegisters["upen_hold_40g3"] = _AF6CCI0012_RD_PTP._upen_hold_40g3()
        allRegisters["upen_ptp40_stmode"] = _AF6CCI0012_RD_PTP._upen_ptp40_stmode()
        allRegisters["upen_ptp40_ms"] = _AF6CCI0012_RD_PTP._upen_ptp40_ms()
        allRegisters["upen_ptp40_dev"] = _AF6CCI0012_RD_PTP._upen_ptp40_dev()
        allRegisters["upen_cfg_ptp_bypass"] = _AF6CCI0012_RD_PTP._upen_cfg_ptp_bypass()
        allRegisters["upen_cfg_mac_sa"] = _AF6CCI0012_RD_PTP._upen_cfg_mac_sa()
        allRegisters["upen_cfg_mac_da"] = _AF6CCI0012_RD_PTP._upen_cfg_mac_da()
        allRegisters["upen_cfg_ip4_sa1"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_sa1()
        allRegisters["upen_cfg_ip4_sa2"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_sa2()
        allRegisters["upen_cfg_ip4_sa3"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_sa3()
        allRegisters["upen_cfg_ip4_sa4"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_sa4()
        allRegisters["upen_cfg_ip4_da1"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_da1()
        allRegisters["upen_cfg_ip4_da2"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_da2()
        allRegisters["upen_cfg_ip4_da3"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_da3()
        allRegisters["upen_cfg_ip4_da4"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_da4()
        allRegisters["upen_cfg_ip6_sa1"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_sa1()
        allRegisters["upen_cfg_ip6_sa2"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_sa2()
        allRegisters["upen_cfg_ip6_sa3"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_sa3()
        allRegisters["upen_cfg_ip6_sa4"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_sa4()
        allRegisters["upen_cfg_ip6_da1"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_da1()
        allRegisters["upen_cfg_ip6_da2"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_da2()
        allRegisters["upen_cfg_ip6_da3"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_da3()
        allRegisters["upen_cfg_ip6_da4"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_da4()
        allRegisters["upen_cnt_pkt_in_r2c"] = _AF6CCI0012_RD_PTP._upen_cnt_pkt_in_r2c()
        allRegisters["upen_cnt_pkt_in_ro"] = _AF6CCI0012_RD_PTP._upen_cnt_pkt_in_ro()
        allRegisters["upen_cnt_ptppkt_rx_r2c"] = _AF6CCI0012_RD_PTP._upen_cnt_ptppkt_rx_r2c()
        allRegisters["upen_cnt_ptppkt_rx_ro"] = _AF6CCI0012_RD_PTP._upen_cnt_ptppkt_rx_ro()
        allRegisters["upen_cnt_txpkt_out_r2c"] = _AF6CCI0012_RD_PTP._upen_cnt_txpkt_out_r2c()
        allRegisters["upen_cnt_txpkt_out_ro"] = _AF6CCI0012_RD_PTP._upen_cnt_txpkt_out_ro()
        allRegisters["upen_rx_sync_cnt40_r2c"] = _AF6CCI0012_RD_PTP._upen_rx_sync_cnt40_r2c()
        allRegisters["upen_rx_sync_cnt40_ro"] = _AF6CCI0012_RD_PTP._upen_rx_sync_cnt40_ro()
        allRegisters["upen_rx_folu_cnt40_r2c"] = _AF6CCI0012_RD_PTP._upen_rx_folu_cnt40_r2c()
        allRegisters["upen_rx_folu_cnt40_ro"] = _AF6CCI0012_RD_PTP._upen_rx_folu_cnt40_ro()
        allRegisters["upen_rx_dreq_cnt40_r2c"] = _AF6CCI0012_RD_PTP._upen_rx_dreq_cnt40_r2c()
        allRegisters["upen_rx_dreq_cnt40_ro"] = _AF6CCI0012_RD_PTP._upen_rx_dreq_cnt40_ro()
        allRegisters["upen_rx_dres_cnt40_r2c"] = _AF6CCI0012_RD_PTP._upen_rx_dres_cnt40_r2c()
        allRegisters["upen_rx_dres_cnt40_ro"] = _AF6CCI0012_RD_PTP._upen_rx_dres_cnt40_ro()
        allRegisters["upen_rx_extr_err_syncnt40_r2c"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_syncnt40_r2c()
        allRegisters["upen_rx_extr_err_syncnt40_ro"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_syncnt40_ro()
        allRegisters["upen_rx_extr_err_folucnt40_r2c"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_folucnt40_r2c()
        allRegisters["upen_rx_extr_err_folucnt40_ro"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_folucnt40_ro()
        allRegisters["upen_rx_extr_err_dreqcnt40_r2c"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_dreqcnt40_r2c()
        allRegisters["upen_rx_extr_err_dreqcnt40_ro"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_dreqcnt40_ro()
        allRegisters["upen_rx_extr_err_drescnt40_r2c"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_drescnt40_r2c()
        allRegisters["upen_rx_extr_err_drescnt40_ro"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_drescnt40_ro()
        allRegisters["upen_rx_chs_err_syncnt40_r2c"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_syncnt40_r2c()
        allRegisters["upen_rx_chs_err_syncnt40_ro"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_syncnt40_ro()
        allRegisters["upen_rx_chs_err_folucnt40_r2c"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_folucnt40_r2c()
        allRegisters["upen_rx_chs_err_folucnt40_ro"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_folucnt40_ro()
        allRegisters["upen_rx_chs_err_dreqcnt40_r2c"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_dreqcnt40_r2c()
        allRegisters["upen_rx_chs_err_dreqcnt40_ro"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_dreqcnt40_ro()
        allRegisters["upen_rx_chs_err_drescnt40_r2c"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_drescnt40_r2c()
        allRegisters["upen_rx_chs_err_drescnt40_ro"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_drescnt40_ro()
        allRegisters["upen_tx_sync_cnt40_r2c"] = _AF6CCI0012_RD_PTP._upen_tx_sync_cnt40_r2c()
        allRegisters["upen_tx_sync_cnt40_ro"] = _AF6CCI0012_RD_PTP._upen_tx_sync_cnt40_ro()
        allRegisters["upen_tx_folu_cnt40_r2c"] = _AF6CCI0012_RD_PTP._upen_tx_folu_cnt40_r2c()
        allRegisters["upen_tx_folu_cnt40_ro"] = _AF6CCI0012_RD_PTP._upen_tx_folu_cnt40_ro()
        allRegisters["upen_tx_dreq_cnt40_r2c"] = _AF6CCI0012_RD_PTP._upen_tx_dreq_cnt40_r2c()
        allRegisters["upen_tx_dreq_cnt40_ro"] = _AF6CCI0012_RD_PTP._upen_tx_dreq_cnt40_ro()
        allRegisters["upen_tx_dres_cnt40_r2c"] = _AF6CCI0012_RD_PTP._upen_tx_dres_cnt40_r2c()
        allRegisters["upen_tx_dres_cnt40_ro"] = _AF6CCI0012_RD_PTP._upen_tx_dres_cnt40_ro()
        allRegisters["upen_cpuque40"] = _AF6CCI0012_RD_PTP._upen_cpuque40()
        allRegisters["upen_int_en40"] = _AF6CCI0012_RD_PTP._upen_int_en40()
        allRegisters["upen_int40"] = _AF6CCI0012_RD_PTP._upen_int40()
        allRegisters["upen_sta40"] = _AF6CCI0012_RD_PTP._upen_sta40()
        allRegisters["upen_t1t3mode40g"] = _AF6CCI0012_RD_PTP._upen_t1t3mode40g()
        allRegisters["upen_int_sta"] = _AF6CCI0012_RD_PTP._upen_int_sta()
        allRegisters["upen_face_vid_glb"] = _AF6CCI0012_RD_PTP._upen_face_vid_glb()
        allRegisters["upen_face_vid_glb_en"] = _AF6CCI0012_RD_PTP._upen_face_vid_glb_en()
        allRegisters["upen_face_vid_perport"] = _AF6CCI0012_RD_PTP._upen_face_vid_perport()
        allRegisters["upen_bp_vid_glb"] = _AF6CCI0012_RD_PTP._upen_bp_vid_glb()
        allRegisters["upen_bp_vid_perport"] = _AF6CCI0012_RD_PTP._upen_bp_vid_perport()
        allRegisters["upen_ge_ptp_dis_timestamping"] = _AF6CCI0012_RD_PTP._upen_ge_ptp_dis_timestamping()
        allRegisters["upen_bp_ptp_dis_timestamping"] = _AF6CCI0012_RD_PTP._upen_bp_ptp_dis_timestamping()
        return allRegisters

    class _upen_hold1(AtRegister.AtRegister):
        def name(self):
            return "Hold Register 1"
    
        def description(self):
            return "This register hold value 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_hold1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_hold1"
            
            def description(self):
                return "hold value 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_hold1"] = _AF6CCI0012_RD_PTP._upen_hold1._cfg_hold1()
            return allFields

    class _upen_hold2(AtRegister.AtRegister):
        def name(self):
            return "Hold Register 2"
    
        def description(self):
            return "This register hold value 2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_hold2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_hold2"
            
            def description(self):
                return "hold value 2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_hold2"] = _AF6CCI0012_RD_PTP._upen_hold2._cfg_hold2()
            return allFields

    class _upen_hold3(AtRegister.AtRegister):
        def name(self):
            return "Hold Register 3"
    
        def description(self):
            return "This register hold value"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_hold3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_hold3"
            
            def description(self):
                return "hold value 3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_hold3"] = _AF6CCI0012_RD_PTP._upen_hold3._cfg_hold3()
            return allFields

    class _upen_ptp_en(AtRegister.AtRegister):
        def name(self):
            return "PTP enable"
    
        def description(self):
            return "used to enable ptp for 16 port ingress"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ptp_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ptp_en"
            
            def description(self):
                return "'1' : enable, '0': disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ptp_en"] = _AF6CCI0012_RD_PTP._upen_ptp_en._cfg_ptp_en()
            return allFields

    class _upen_ptp_stmode(AtRegister.AtRegister):
        def name(self):
            return "PTP enable"
    
        def description(self):
            return "used to indicate 1-step/2-step mode for each port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ptp_stmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ptp_stmode"
            
            def description(self):
                return "'1' : 2-step, '0': 1-step"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ptp_stmode"] = _AF6CCI0012_RD_PTP._upen_ptp_stmode._cfg_ptp_stmode()
            return allFields

    class _upen_ptp_ms(AtRegister.AtRegister):
        def name(self):
            return "PTP enable"
    
        def description(self):
            return "used to indicate master/slaver mode for each port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000005
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ptp_ms(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ptp_ms"
            
            def description(self):
                return "'1' : master, '0': slaver"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ptp_ms"] = _AF6CCI0012_RD_PTP._upen_ptp_ms._cfg_ptp_ms()
            return allFields

    class _upen_ptp_dev(AtRegister.AtRegister):
        def name(self):
            return "PTP Device"
    
        def description(self):
            return "This register is used to config PTP device"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000006
            
        def endAddress(self):
            return 0xffffffff

        class _device_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "device_mode"
            
            def description(self):
                return "'00': BC mode, '01': reserved '10': TC Separate mode, '11': TC General mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["device_mode"] = _AF6CCI0012_RD_PTP._upen_ptp_dev._device_mode()
            return allFields

    class _upen_ms_srcpid(AtRegister.AtRegister):
        def name(self):
            return "Config Master SPID"
    
        def description(self):
            return "Used to checking sourcePortIdentity of Sync, Follow-up and Delay Response packet"
            
        def width(self):
            return 96
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000007
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ms_srcpid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 79
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ms_srcpid"
            
            def description(self):
                return "sourcePortIdentity value of Master"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ms_srcpid"] = _AF6CCI0012_RD_PTP._upen_ms_srcpid._cfg_ms_srcpid()
            return allFields

    class _upen_sl_reqpid(AtRegister.AtRegister):
        def name(self):
            return "Config Slaver ReqPID"
    
        def description(self):
            return "Used to checking requestingPortIdentity of Delay Response packet"
            
        def width(self):
            return 96
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000008
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_sl_reqpid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 79
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_sl_reqpid"
            
            def description(self):
                return "RequestingPortIdentity value of Slaver"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_sl_reqpid"] = _AF6CCI0012_RD_PTP._upen_sl_reqpid._cfg_sl_reqpid()
            return allFields

    class _upen_chsv4_bypass(AtRegister.AtRegister):
        def name(self):
            return "Config chsv4 bypass"
    
        def description(self):
            return "Used to indicate bypass ipv4 header checksum error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000009
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_chsv4_bypass(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_chsv4_bypass"
            
            def description(self):
                return "'1': enable, '0': disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_chsv4_bypass"] = _AF6CCI0012_RD_PTP._upen_chsv4_bypass._cfg_chsv4_bypass()
            return allFields

    class _upen_ptp_rdy(AtRegister.AtRegister):
        def name(self):
            return "PTP Ready"
    
        def description(self):
            return "This register is used to indicate config ptp done"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000015
            
        def endAddress(self):
            return 0xffffffff

        class _ptp_rdy(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ptp_rdy"
            
            def description(self):
                return "'1' : ready"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ptp_rdy"] = _AF6CCI0012_RD_PTP._upen_ptp_rdy._ptp_rdy()
            return allFields

    class _upen_tx_ptp_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter TX FACE PTP"
    
        def description(self):
            return "Total tx face packet"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_0400 +  $pid"
            
        def startAddress(self):
            return 0x00000400
            
        def endAddress(self):
            return 0xffffffff

        class _tx_face_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tx_face_cnt"
            
            def description(self):
                return "total tx face packet"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tx_face_cnt"] = _AF6CCI0012_RD_PTP._upen_tx_ptp_cnt_r2c._tx_face_cnt()
            return allFields

    class _upen_tx_ptp_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter TX FACE PTP"
    
        def description(self):
            return "Total tx face packet"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_0410 +  $pid"
            
        def startAddress(self):
            return 0x00000410
            
        def endAddress(self):
            return 0xffffffff

        class _tx_face_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tx_face_cnt"
            
            def description(self):
                return "total tx face packet"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tx_face_cnt"] = _AF6CCI0012_RD_PTP._upen_tx_ptp_cnt_ro._tx_face_cnt()
            return allFields

    class _upen_rx_ptp_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter PTP Packet"
    
        def description(self):
            return "Total rx face packet"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_0460 +  $pid"
            
        def startAddress(self):
            return 0x00000460
            
        def endAddress(self):
            return 0xffffffff

        class _rx_ptp_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_ptp_cnt"
            
            def description(self):
                return "Total rx ptp packet"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_ptp_cnt"] = _AF6CCI0012_RD_PTP._upen_rx_ptp_cnt_r2c._rx_ptp_cnt()
            return allFields

    class _upen_rx_ptp_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter PTP Packet"
    
        def description(self):
            return "Total rx face packet"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_0470 +  $pid"
            
        def startAddress(self):
            return 0x00000470
            
        def endAddress(self):
            return 0xffffffff

        class _rx_ptp_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_ptp_cnt"
            
            def description(self):
                return "Total rx ptp packet"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_ptp_cnt"] = _AF6CCI0012_RD_PTP._upen_rx_ptp_cnt_ro._rx_ptp_cnt()
            return allFields

    class _upen_rx_sync_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter Sync Received"
    
        def description(self):
            return "Number of Sync message received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_480+ $pid"
            
        def startAddress(self):
            return 0x00000480
            
        def endAddress(self):
            return 0xffffffff

        class _syncrx_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "syncrx_cnt"
            
            def description(self):
                return "number sync packet at Rx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["syncrx_cnt"] = _AF6CCI0012_RD_PTP._upen_rx_sync_cnt_r2c._syncrx_cnt()
            return allFields

    class _upen_rx_sync_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter Sync Received"
    
        def description(self):
            return "Number of Sync message received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_4C0+ $pid"
            
        def startAddress(self):
            return 0x000004c0
            
        def endAddress(self):
            return 0xffffffff

        class _syncrx_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "syncrx_cnt"
            
            def description(self):
                return "number sync packet at Rx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["syncrx_cnt"] = _AF6CCI0012_RD_PTP._upen_rx_sync_cnt_ro._syncrx_cnt()
            return allFields

    class _upen_rx_folu_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter Folu Received"
    
        def description(self):
            return "Number of folu message received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_490 + $pid"
            
        def startAddress(self):
            return 0x00000490
            
        def endAddress(self):
            return 0xffffffff

        class _folurx_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "folurx_cnt"
            
            def description(self):
                return "number folu packet at Rx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["folurx_cnt"] = _AF6CCI0012_RD_PTP._upen_rx_folu_cnt_r2c._folurx_cnt()
            return allFields

    class _upen_rx_folu_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter Folu Received"
    
        def description(self):
            return "Number of folu message received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_4D0 + $pid"
            
        def startAddress(self):
            return 0x000004d0
            
        def endAddress(self):
            return 0xffffffff

        class _folurx_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "folurx_cnt"
            
            def description(self):
                return "number folu packet at Rx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["folurx_cnt"] = _AF6CCI0012_RD_PTP._upen_rx_folu_cnt_ro._folurx_cnt()
            return allFields

    class _upen_rx_dreq_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter dreq Received"
    
        def description(self):
            return "Number of dreq message received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_4A0+$pid"
            
        def startAddress(self):
            return 0x000004a0
            
        def endAddress(self):
            return 0xffffffff

        class _dreqrx_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dreqrx_cnt"
            
            def description(self):
                return "number dreq packet at Rx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dreqrx_cnt"] = _AF6CCI0012_RD_PTP._upen_rx_dreq_cnt_r2c._dreqrx_cnt()
            return allFields

    class _upen_rx_dreq_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter dreq Received"
    
        def description(self):
            return "Number of dreq message received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_4E0+$pid"
            
        def startAddress(self):
            return 0x000004e0
            
        def endAddress(self):
            return 0xffffffff

        class _dreqrx_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dreqrx_cnt"
            
            def description(self):
                return "number dreq packet at Rx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dreqrx_cnt"] = _AF6CCI0012_RD_PTP._upen_rx_dreq_cnt_ro._dreqrx_cnt()
            return allFields

    class _upen_rx_dres_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter dres Received"
    
        def description(self):
            return "Number of dres message received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_4B0 + $pid"
            
        def startAddress(self):
            return 0x000004b0
            
        def endAddress(self):
            return 0xffffffff

        class _dresrx_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dresrx_cnt"
            
            def description(self):
                return "number dres packet at Rx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dresrx_cnt"] = _AF6CCI0012_RD_PTP._upen_rx_dres_cnt_r2c._dresrx_cnt()
            return allFields

    class _upen_rx_dres_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter dres Received"
    
        def description(self):
            return "Number of dres message received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_4F0 + $pid"
            
        def startAddress(self):
            return 0x000004f0
            
        def endAddress(self):
            return 0xffffffff

        class _dresrx_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dresrx_cnt"
            
            def description(self):
                return "number dres packet at Rx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dresrx_cnt"] = _AF6CCI0012_RD_PTP._upen_rx_dres_cnt_ro._dresrx_cnt()
            return allFields

    class _upen_rx_extr_err_syncnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Sync Extract Err"
    
        def description(self):
            return "Number of extr err sync mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_500 + $pid"
            
        def startAddress(self):
            return 0x00000500
            
        def endAddress(self):
            return 0xffffffff

        class _rx_extrerr_synccnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_extrerr_synccnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_extrerr_synccnt"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_syncnt_r2c._rx_extrerr_synccnt()
            return allFields

    class _upen_rx_extr_err_syncnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Sync Extract Err"
    
        def description(self):
            return "Number of extr err sync mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_0540 + $pid"
            
        def startAddress(self):
            return 0x00000540
            
        def endAddress(self):
            return 0xffffffff

        class _rx_extrerr_synccnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_extrerr_synccnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_extrerr_synccnt"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_syncnt_ro._rx_extrerr_synccnt()
            return allFields

    class _upen_rx_extr_err_folucnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Folu Extract Err"
    
        def description(self):
            return "Number of extr err folu mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_0510 + $pid"
            
        def startAddress(self):
            return 0x00000510
            
        def endAddress(self):
            return 0xffffffff

        class _rx_extrerr_foluccnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_extrerr_foluccnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_extrerr_foluccnt"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_folucnt_r2c._rx_extrerr_foluccnt()
            return allFields

    class _upen_rx_extr_err_folucnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Folu Extract Err"
    
        def description(self):
            return "Number of extr err folu mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_0550 + $pid"
            
        def startAddress(self):
            return 0x00000550
            
        def endAddress(self):
            return 0xffffffff

        class _rx_extrerr_foluccnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_extrerr_foluccnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_extrerr_foluccnt"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_folucnt_ro._rx_extrerr_foluccnt()
            return allFields

    class _upen_rx_extr_err_dreqcnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Dreq Extract Err"
    
        def description(self):
            return "Number of extr err dreq mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_0520 + $pid"
            
        def startAddress(self):
            return 0x00000520
            
        def endAddress(self):
            return 0xffffffff

        class _rx_extrerr_dreqcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_extrerr_dreqcnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_extrerr_dreqcnt"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_dreqcnt_r2c._rx_extrerr_dreqcnt()
            return allFields

    class _upen_rx_extr_err_dreqcnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Dreq Extract Err"
    
        def description(self):
            return "Number of extr err dreq mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_0560 + $pid"
            
        def startAddress(self):
            return 0x00000560
            
        def endAddress(self):
            return 0xffffffff

        class _rx_extrerr_dreqcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_extrerr_dreqcnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_extrerr_dreqcnt"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_dreqcnt_ro._rx_extrerr_dreqcnt()
            return allFields

    class _upen_rx_extr_err_drescnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Dres Extract Err"
    
        def description(self):
            return "Number of extr err dres mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_0530 + $pid"
            
        def startAddress(self):
            return 0x00000530
            
        def endAddress(self):
            return 0xffffffff

        class _rx_extrerr_drescnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_extrerr_drescnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_extrerr_drescnt"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_drescnt_r2c._rx_extrerr_drescnt()
            return allFields

    class _upen_rx_extr_err_drescnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Dres Extract Err"
    
        def description(self):
            return "Number of extr err dres mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_0570 + $pid"
            
        def startAddress(self):
            return 0x00000570
            
        def endAddress(self):
            return 0xffffffff

        class _rx_extrerr_drescnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_extrerr_drescnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_extrerr_drescnt"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_drescnt_ro._rx_extrerr_drescnt()
            return allFields

    class _upen_rx_chs_err_syncnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Sync Chsum Err"
    
        def description(self):
            return "Number of Chs err sync mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_0580 + $pid"
            
        def startAddress(self):
            return 0x00000580
            
        def endAddress(self):
            return 0xffffffff

        class _rx_cherr_synccnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_cherr_synccnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_cherr_synccnt"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_syncnt_r2c._rx_cherr_synccnt()
            return allFields

    class _upen_rx_chs_err_syncnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Sync Chsum Err"
    
        def description(self):
            return "Number of Chs err sync mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_05C0 + $pid"
            
        def startAddress(self):
            return 0x000005c0
            
        def endAddress(self):
            return 0xffffffff

        class _rx_cherr_synccnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_cherr_synccnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_cherr_synccnt"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_syncnt_ro._rx_cherr_synccnt()
            return allFields

    class _upen_rx_chs_err_folucnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Folu Chsum Err"
    
        def description(self):
            return "Number of Chs err folu mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_0590 + $pid"
            
        def startAddress(self):
            return 0x00000590
            
        def endAddress(self):
            return 0xffffffff

        class _rx_cherr_folucnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_cherr_folucnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_cherr_folucnt"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_folucnt_r2c._rx_cherr_folucnt()
            return allFields

    class _upen_rx_chs_err_folucnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Folu Chsum Err"
    
        def description(self):
            return "Number of Chs err folu mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_05D0 + $pid"
            
        def startAddress(self):
            return 0x000005d0
            
        def endAddress(self):
            return 0xffffffff

        class _rx_cherr_folucnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_cherr_folucnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_cherr_folucnt"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_folucnt_ro._rx_cherr_folucnt()
            return allFields

    class _upen_rx_chs_err_dreqcnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Dreq Chsum Err"
    
        def description(self):
            return "Number of Chs err dreq mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_05A0 + $pid"
            
        def startAddress(self):
            return 0x000005a0
            
        def endAddress(self):
            return 0xffffffff

        class _rx_cherr_dreqcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_cherr_dreqcnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_cherr_dreqcnt"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_dreqcnt_r2c._rx_cherr_dreqcnt()
            return allFields

    class _upen_rx_chs_err_dreqcnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Dreq Chsum Err"
    
        def description(self):
            return "Number of Chs err dreq mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_5E0 + $pid"
            
        def startAddress(self):
            return 0x000005e0
            
        def endAddress(self):
            return 0xffffffff

        class _rx_cherr_dreqcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_cherr_dreqcnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_cherr_dreqcnt"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_dreqcnt_ro._rx_cherr_dreqcnt()
            return allFields

    class _upen_rx_chs_err_drescnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Dres Chsum Err"
    
        def description(self):
            return "Number of Chs err dres mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_05B0 + $pid"
            
        def startAddress(self):
            return 0x000005b0
            
        def endAddress(self):
            return 0xffffffff

        class _rx_cherr_drescnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_cherr_drescnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_cherr_drescnt"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_drescnt_r2c._rx_cherr_drescnt()
            return allFields

    class _upen_rx_chs_err_drescnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Dres Chsum Err"
    
        def description(self):
            return "Number of Chs err dres mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_05F0 + $pid"
            
        def startAddress(self):
            return 0x000005f0
            
        def endAddress(self):
            return 0xffffffff

        class _rx_cherr_drescnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_cherr_drescnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_cherr_drescnt"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_drescnt_ro._rx_cherr_drescnt()
            return allFields

    class _upen_tx_sync_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter Sync Transmit"
    
        def description(self):
            return "Number of Sync message transmit"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_0600 + $pid"
            
        def startAddress(self):
            return 0x00000600
            
        def endAddress(self):
            return 0xffffffff

        class _synctx_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "synctx_cnt"
            
            def description(self):
                return "number sync packet at Tx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["synctx_cnt"] = _AF6CCI0012_RD_PTP._upen_tx_sync_cnt_r2c._synctx_cnt()
            return allFields

    class _upen_tx_sync_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter Sync Transmit"
    
        def description(self):
            return "Number of Sync message transmit"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_0640 + $pid"
            
        def startAddress(self):
            return 0x00000640
            
        def endAddress(self):
            return 0xffffffff

        class _synctx_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "synctx_cnt"
            
            def description(self):
                return "number sync packet at Tx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["synctx_cnt"] = _AF6CCI0012_RD_PTP._upen_tx_sync_cnt_ro._synctx_cnt()
            return allFields

    class _upen_tx_folu_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter Folu Transmit"
    
        def description(self):
            return "Number of folu message transmit"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_0610 + $pid"
            
        def startAddress(self):
            return 0x00000610
            
        def endAddress(self):
            return 0xffffffff

        class _folutx_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "folutx_cnt"
            
            def description(self):
                return "number folu packet at Tx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["folutx_cnt"] = _AF6CCI0012_RD_PTP._upen_tx_folu_cnt_r2c._folutx_cnt()
            return allFields

    class _upen_tx_folu_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter Folu Transmit"
    
        def description(self):
            return "Number of folu message transmit"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_0650 + $pid"
            
        def startAddress(self):
            return 0x00000650
            
        def endAddress(self):
            return 0xffffffff

        class _folutx_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "folutx_cnt"
            
            def description(self):
                return "number folu packet at Tx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["folutx_cnt"] = _AF6CCI0012_RD_PTP._upen_tx_folu_cnt_ro._folutx_cnt()
            return allFields

    class _upen_tx_dreq_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter dreq transmit"
    
        def description(self):
            return "Number of dreq message transmit"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_0620 + $pid"
            
        def startAddress(self):
            return 0x00000620
            
        def endAddress(self):
            return 0xffffffff

        class _dreqtx_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dreqtx_cnt"
            
            def description(self):
                return "number dreq packet at Tx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dreqtx_cnt"] = _AF6CCI0012_RD_PTP._upen_tx_dreq_cnt_r2c._dreqtx_cnt()
            return allFields

    class _upen_tx_dreq_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter dreq transmit"
    
        def description(self):
            return "Number of dreq message transmit"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_0660 + $pid"
            
        def startAddress(self):
            return 0x00000660
            
        def endAddress(self):
            return 0xffffffff

        class _dreqtx_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dreqtx_cnt"
            
            def description(self):
                return "number dreq packet at Tx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dreqtx_cnt"] = _AF6CCI0012_RD_PTP._upen_tx_dreq_cnt_ro._dreqtx_cnt()
            return allFields

    class _upen_tx_dres_cnt_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter dres transmit"
    
        def description(self):
            return "Number of dres message transmit"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_0630 + $pid"
            
        def startAddress(self):
            return 0x00000630
            
        def endAddress(self):
            return 0xffffffff

        class _drestx_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "drestx_cnt"
            
            def description(self):
                return "number dres packet at Tx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["drestx_cnt"] = _AF6CCI0012_RD_PTP._upen_tx_dres_cnt_r2c._drestx_cnt()
            return allFields

    class _upen_tx_dres_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter dres transmit"
    
        def description(self):
            return "Number of dres message transmit"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_0670 + $pid"
            
        def startAddress(self):
            return 0x00000670
            
        def endAddress(self):
            return 0xffffffff

        class _drestx_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "drestx_cnt"
            
            def description(self):
                return "number dres packet at Tx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["drestx_cnt"] = _AF6CCI0012_RD_PTP._upen_tx_dres_cnt_ro._drestx_cnt()
            return allFields

    class _upen_cla_stk(AtRegister.AtRegister):
        def name(self):
            return "classify sticky"
    
        def description(self):
            return "sticky of classify"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000330
            
        def endAddress(self):
            return 0xffffffff

        class _cla_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cla_stk"
            
            def description(self):
                return "sticky ge_classify"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cla_stk"] = _AF6CCI0012_RD_PTP._upen_cla_stk._cla_stk()
            return allFields

    class _upen_chk_stk(AtRegister.AtRegister):
        def name(self):
            return "PTP check sticky"
    
        def description(self):
            return "sticky of ptp check"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000331
            
        def endAddress(self):
            return 0xffffffff

        class _chk_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "chk_stk"
            
            def description(self):
                return "sticky ptp check"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["chk_stk"] = _AF6CCI0012_RD_PTP._upen_chk_stk._chk_stk()
            return allFields

    class _upen_getinf_stk(AtRegister.AtRegister):
        def name(self):
            return "ptp getinfo sticky"
    
        def description(self):
            return "sticky of ptp get info"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000332
            
        def endAddress(self):
            return 0xffffffff

        class _getinf_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "getinf_stk"
            
            def description(self):
                return "sticky ptp get info"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["getinf_stk"] = _AF6CCI0012_RD_PTP._upen_getinf_stk._getinf_stk()
            return allFields

    class _upen_trans_stk(AtRegister.AtRegister):
        def name(self):
            return "trans sticky"
    
        def description(self):
            return "sticky of ptp trans"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000333
            
        def endAddress(self):
            return 0xffffffff

        class _trans_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "trans_stk"
            
            def description(self):
                return "sticky ptp trans"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["trans_stk"] = _AF6CCI0012_RD_PTP._upen_trans_stk._trans_stk()
            return allFields

    class _upen_cpuque_stk(AtRegister.AtRegister):
        def name(self):
            return "cpu queue sticky"
    
        def description(self):
            return "sticky of cpu que"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000334
            
        def endAddress(self):
            return 0xffffffff

        class _cpuque_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cpuque_stk"
            
            def description(self):
                return "sticky of cpu queue"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cpuque_stk"] = _AF6CCI0012_RD_PTP._upen_cpuque_stk._cpuque_stk()
            return allFields

    class _upen_t1t3mode(AtRegister.AtRegister):
        def name(self):
            return "Config t1t3mode"
    
        def description(self):
            return "Used to config t1t3mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000017
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_t1t3mode_enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_t1t3mode_enable"
            
            def description(self):
                return "1: enable this mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_t1t3mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_t1t3mode"
            
            def description(self):
                return "0: send timestamp to cpu queue 1: send back ptp packet mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_t1t3mode_enable"] = _AF6CCI0012_RD_PTP._upen_t1t3mode._cfg_t1t3mode_enable()
            allFields["cfg_t1t3mode"] = _AF6CCI0012_RD_PTP._upen_t1t3mode._cfg_t1t3mode()
            return allFields

    class _upen_cpu_flush(AtRegister.AtRegister):
        def name(self):
            return "Config cpu flush"
    
        def description(self):
            return "Used to config cpu flush"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000018
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_cpu_flush(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_cpu_flush"
            
            def description(self):
                return "cpu flush"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_cpu_flush"] = _AF6CCI0012_RD_PTP._upen_cpu_flush._cfg_cpu_flush()
            return allFields

    class _upen_cpuque(AtRegister.AtRegister):
        def name(self):
            return "Read CPU Queue"
    
        def description(self):
            return "Used to read CPU queue"
            
        def width(self):
            return 128
        
        def type(self):
            return "status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000216
            
        def endAddress(self):
            return 0xffffffff

        class _typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 101
                
            def startBit(self):
                return 100
        
            def name(self):
                return "typ"
            
            def description(self):
                return "typ of ptp packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _seqid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 99
                
            def startBit(self):
                return 84
        
            def name(self):
                return "seqid"
            
            def description(self):
                return "sequence of packet from sequenceId field"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _egr_pid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 83
                
            def startBit(self):
                return 80
        
            def name(self):
                return "egr_pid"
            
            def description(self):
                return "egress port id"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _egr_tmr_tod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 79
                
            def startBit(self):
                return 0
        
            def name(self):
                return "egr_tmr_tod"
            
            def description(self):
                return "egress timer tod"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["typ"] = _AF6CCI0012_RD_PTP._upen_cpuque._typ()
            allFields["seqid"] = _AF6CCI0012_RD_PTP._upen_cpuque._seqid()
            allFields["egr_pid"] = _AF6CCI0012_RD_PTP._upen_cpuque._egr_pid()
            allFields["egr_tmr_tod"] = _AF6CCI0012_RD_PTP._upen_cpuque._egr_tmr_tod()
            return allFields

    class _upen_int_en(AtRegister.AtRegister):
        def name(self):
            return "Enb CPU Interupt"
    
        def description(self):
            return "Used to read CPU queue"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000200
            
        def endAddress(self):
            return 0xffffffff

        class _int_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "int_en"
            
            def description(self):
                return "set \"1\" ,interupt enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["int_en"] = _AF6CCI0012_RD_PTP._upen_int_en._int_en()
            return allFields

    class _upen_int(AtRegister.AtRegister):
        def name(self):
            return "Interupt CPU Queue"
    
        def description(self):
            return "Used to read CPU queue"
            
        def width(self):
            return 128
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000201
            
        def endAddress(self):
            return 0xffffffff

        class _int_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "int_en"
            
            def description(self):
                return "set\"1\" queue info ready ,write 1 to clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["int_en"] = _AF6CCI0012_RD_PTP._upen_int._int_en()
            return allFields

    class _upen_sta(AtRegister.AtRegister):
        def name(self):
            return "Interupt CPU Queue"
    
        def description(self):
            return "Used to read CPU queue"
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000202
            
        def endAddress(self):
            return 0xffffffff

        class _upen_sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 0
        
            def name(self):
                return "upen_sta"
            
            def description(self):
                return "current state of CPU queue, bit[17] \"1\" queue FULL, bit[16] \"1\" NOT empty, ready for cpu read, bit[15:0] queue lengh"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["upen_sta"] = _AF6CCI0012_RD_PTP._upen_sta._upen_sta()
            return allFields

    class _upen_umac_glb(AtRegister.AtRegister):
        def name(self):
            return "config unicast MAC address global"
    
        def description(self):
            return "config unicast MAC address global for all port"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004003
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_umac_glb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_umac_glb"
            
            def description(self):
                return "uni MAC global"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_umac_glb"] = _AF6CCI0012_RD_PTP._upen_umac_glb._cfg_umac_glb()
            return allFields

    class _upen_mmac1_glb(AtRegister.AtRegister):
        def name(self):
            return "config multicast MAC address global 1"
    
        def description(self):
            return "config multicast MAC address global 1 for all port"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004004
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_mmac1_glb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_mmac1_glb"
            
            def description(self):
                return "mul MAC global 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_mmac1_glb"] = _AF6CCI0012_RD_PTP._upen_mmac1_glb._cfg_mmac1_glb()
            return allFields

    class _upen_mmac2_glb(AtRegister.AtRegister):
        def name(self):
            return "config multicast MAC address global 2"
    
        def description(self):
            return "config multicast MAC address global 2 for all port"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004005
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_mmac2_glb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_mmac2_glb"
            
            def description(self):
                return "mul MAC global 2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_mmac2_glb"] = _AF6CCI0012_RD_PTP._upen_mmac2_glb._cfg_mmac2_glb()
            return allFields

    class _upen_anymac_en(AtRegister.AtRegister):
        def name(self):
            return "config any MAC enable"
    
        def description(self):
            return "is used to enable any mac for each port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004006
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_anymac_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_anymac_en"
            
            def description(self):
                return "'1': enable , '0': disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_anymac_en"] = _AF6CCI0012_RD_PTP._upen_anymac_en._cfg_anymac_en()
            return allFields

    class _upen_macglb_en(AtRegister.AtRegister):
        def name(self):
            return "config MAC global matching enable"
    
        def description(self):
            return "is used to indicate MAC global matching enable"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004007
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_anymac_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_anymac_en"
            
            def description(self):
                return "[0] : en/dis unicast mac global matching [1] : en/dis multicast mac global 1 matching [2] : en/dis multicast mac global 2 matching"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_anymac_en"] = _AF6CCI0012_RD_PTP._upen_macglb_en._cfg_anymac_en()
            return allFields

    class _upen_v4mip1_glb(AtRegister.AtRegister):
        def name(self):
            return "Config mul ipv4 global 1"
    
        def description(self):
            return "config multicast ipv4 address global 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000400a
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_v4mip1_glb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_v4mip1_glb"
            
            def description(self):
                return "used to check multicast ip"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_v4mip1_glb"] = _AF6CCI0012_RD_PTP._upen_v4mip1_glb._cfg_v4mip1_glb()
            return allFields

    class _upen_v4mip2_glb(AtRegister.AtRegister):
        def name(self):
            return "Config mul ipv4 global 2"
    
        def description(self):
            return "config multicast ipv4 address global 2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000400b
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_v4mip2_glb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_v4mip2_glb"
            
            def description(self):
                return "used to check multicast ip"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_v4mip2_glb"] = _AF6CCI0012_RD_PTP._upen_v4mip2_glb._cfg_v4mip2_glb()
            return allFields

    class _upen_v6mip1_glb(AtRegister.AtRegister):
        def name(self):
            return "Config mul ipv6 global 1"
    
        def description(self):
            return "config multicast ipv6 address global 1"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000400c
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_v6mip1_glb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_v6mip1_glb"
            
            def description(self):
                return "used to check multicast ip"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_v6mip1_glb"] = _AF6CCI0012_RD_PTP._upen_v6mip1_glb._cfg_v6mip1_glb()
            return allFields

    class _upen_v6mip2_glb(AtRegister.AtRegister):
        def name(self):
            return "Config mul ipv6 global 2"
    
        def description(self):
            return "config multicast ipv6 address global 2"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000400d
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_v6mip2_glb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_v6mip2_glb"
            
            def description(self):
                return "used to check multicast ip"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_v6mip2_glb"] = _AF6CCI0012_RD_PTP._upen_v6mip2_glb._cfg_v6mip2_glb()
            return allFields

    class _upen_anyip_en(AtRegister.AtRegister):
        def name(self):
            return "config any IP enable"
    
        def description(self):
            return "is used to enable any ip for each port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000400e
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_anyip_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_anyip_en"
            
            def description(self):
                return "'1': enable , '0': disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_anyip_en"] = _AF6CCI0012_RD_PTP._upen_anyip_en._cfg_anyip_en()
            return allFields

    class _upen_cla_rdy(AtRegister.AtRegister):
        def name(self):
            return "CLA Ready"
    
        def description(self):
            return "This register is used to indicate config ptp done"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004011
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_cla_rdy(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_cla_rdy"
            
            def description(self):
                return "'1' : ready"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_cla_rdy"] = _AF6CCI0012_RD_PTP._upen_cla_rdy._cfg_cla_rdy()
            return allFields

    class _upen_force_ptplayer(AtRegister.AtRegister):
        def name(self):
            return "PTP layer force value"
    
        def description(self):
            return "This register is used to config ptp layer force value"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004016
            
        def endAddress(self):
            return 0xffffffff

        class _ptp_layer(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 1
        
            def name(self):
                return "ptp_layer"
            
            def description(self):
                return "3'b000: PTP L2 3'b001: IPv4 3'b010: IPv6 3'b011: IPv4 MPLS unicast 3'b100: IPv6 MPLS unicast"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ptp_ind(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ptp_ind"
            
            def description(self):
                return "ptp indicate: 0: disable 1: enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ptp_layer"] = _AF6CCI0012_RD_PTP._upen_force_ptplayer._ptp_layer()
            allFields["ptp_ind"] = _AF6CCI0012_RD_PTP._upen_force_ptplayer._ptp_ind()
            return allFields

    class _upen_mru_val(AtRegister.AtRegister):
        def name(self):
            return "MRU Packet Size"
    
        def description(self):
            return "This register is used to config MRU Value"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004017
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_mru_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "cfg_mru_en"
            
            def description(self):
                return "MRU enable 1: enable 0: disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mru_val(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_mru_val"
            
            def description(self):
                return "MRU value(Byte)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_mru_en"] = _AF6CCI0012_RD_PTP._upen_mru_val._cfg_mru_en()
            allFields["cfg_mru_val"] = _AF6CCI0012_RD_PTP._upen_mru_val._cfg_mru_val()
            return allFields

    class _upen_mac(AtRegister.AtRegister):
        def name(self):
            return "cfg uni/mul mac"
    
        def description(self):
            return "config uni/mul MAC for each port transtype = 0 : unicast transtype = 1 : multicast HDL_PATH     : inscorepi.insmac.membuf.ram.ram[$transtype*16 + $pid]"
            
        def width(self):
            return 64
        
        def type(self):
            return "config"
            
        def fomular(self):
            return "0x00_5100 + $transtype*16 + $pid"
            
        def startAddress(self):
            return 0x00005100
            
        def endAddress(self):
            return 0x0000511f

        class _macena(AtRegister.AtRegisterField):
            def stopBit(self):
                return 48
                
            def startBit(self):
                return 48
        
            def name(self):
                return "macena"
            
            def description(self):
                return "mac matching enable 0: disable 1: enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mac(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_mac"
            
            def description(self):
                return "unicast/multicast mac for each port"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["macena"] = _AF6CCI0012_RD_PTP._upen_mac._macena()
            allFields["cfg_mac"] = _AF6CCI0012_RD_PTP._upen_mac._cfg_mac()
            return allFields

    class _upen_geip1(AtRegister.AtRegister):
        def name(self):
            return "Config GE unicast/multicast IPv4/IPv6 address 1"
    
        def description(self):
            return "config GE uni/mul IP 1 for each port transtype = 0 : unicast transtype = 1 : multicast HDL_PATH     : inscorepi.insgeip1.membuf.ram.ram[$transtype*16 + $pid]"
            
        def width(self):
            return 128
        
        def type(self):
            return "config"
            
        def fomular(self):
            return "0x00_5120 + $transtype*16 + $pid"
            
        def startAddress(self):
            return 0x00005120
            
        def endAddress(self):
            return 0x0000513f

        class _cfg_geip1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_geip1"
            
            def description(self):
                return "uni/mul ip 1 for each port ipv4:"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_geip1"] = _AF6CCI0012_RD_PTP._upen_geip1._cfg_geip1()
            return allFields

    class _upen_geip2(AtRegister.AtRegister):
        def name(self):
            return "Config GE unicast/multicast IPv4/IPv6 address 2"
    
        def description(self):
            return "config GE uni/mul IP 2 for each port transtype = 0 : unicast transtype = 1 : multicast HDL_PATH     : inscorepi.insgeip2.membuf.ram.ram[$transtype*16 + $pid]"
            
        def width(self):
            return 128
        
        def type(self):
            return "config"
            
        def fomular(self):
            return "0x00_5140 + $transtype*16 + $pid"
            
        def startAddress(self):
            return 0x00005140
            
        def endAddress(self):
            return 0x0000515f

        class _cfg_geip2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_geip2"
            
            def description(self):
                return "uni/mul ip 2 for each port ipv4:"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_geip2"] = _AF6CCI0012_RD_PTP._upen_geip2._cfg_geip2()
            return allFields

    class _upen_geip3(AtRegister.AtRegister):
        def name(self):
            return "Config GE unicast/multicast IPv4/IPv6 address 3"
    
        def description(self):
            return "config GE uni/mul IP 3 for each port transtype = 0 : unicast transtype = 1 : multicast HDL_PATH     : inscorepi.insgeip3.membuf.ram.ram[$transtype*16 + $pid]"
            
        def width(self):
            return 128
        
        def type(self):
            return "config"
            
        def fomular(self):
            return "0x00_5160 + $transtype*16 + $pid"
            
        def startAddress(self):
            return 0x00005160
            
        def endAddress(self):
            return 0x0000517f

        class _cfg_geip3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_geip3"
            
            def description(self):
                return "uni/mul ip 3 for each port ipv4:"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_geip3"] = _AF6CCI0012_RD_PTP._upen_geip3._cfg_geip3()
            return allFields

    class _upen_geip4(AtRegister.AtRegister):
        def name(self):
            return "Config GE unicast/multicast IPv4/IPv6 address 4"
    
        def description(self):
            return "config GE uni/mul IP 4 for each port transtype = 0 : unicast transtype = 1 : multicast HDL_PATH     : inscorepi.insgeip4.membuf.ram.ram[$transtype*16 + $pid]"
            
        def width(self):
            return 128
        
        def type(self):
            return "config"
            
        def fomular(self):
            return "0x00_5180 + $transtype*16 + $pid"
            
        def startAddress(self):
            return 0x00005180
            
        def endAddress(self):
            return 0x0000519f

        class _cfg_geip4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_geip4"
            
            def description(self):
                return "uni/mul ip 4 for each port ipv4:"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_geip4"] = _AF6CCI0012_RD_PTP._upen_geip4._cfg_geip4()
            return allFields

    class _upen_ptl(AtRegister.AtRegister):
        def name(self):
            return "cfg ptp protocol"
    
        def description(self):
            return "config protocol for each port at BC mode in PTP service"
            
        def width(self):
            return 32 HDL_PATH     : inscorepi.insptl.membuf.ram.ram[$pid]
        
        def type(self):
            return "config"
            
        def fomular(self):
            return "0x00_51E0 + $pid"
            
        def startAddress(self):
            return 0x000051e0
            
        def endAddress(self):
            return 0x000051ef

        class _cfg_ptl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ptl"
            
            def description(self):
                return "protocol value: '000': L2 '001': ipv4 '010': ipv6 '011': ipv4 vpn '100': ipv6 vpn"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ptl"] = _AF6CCI0012_RD_PTP._upen_ptl._cfg_ptl()
            return allFields

    class _upen_geipen(AtRegister.AtRegister):
        def name(self):
            return "Config GE port ipv4/ipv6 matching enable"
    
        def description(self):
            return "config IPv4/IPv6 matching for each GE port HDL_PATH     : inscorepi.insptl.membuf.ram.ram[$pid]"
            
        def width(self):
            return 32
        
        def type(self):
            return "config"
            
        def fomular(self):
            return "0x00_51F0 + $pid"
            
        def startAddress(self):
            return 0x000051f0
            
        def endAddress(self):
            return 0x000051ff

        class _cfg_geipen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_geipen"
            
            def description(self):
                return "config ip matching for each GE port [6]: ena/dis mul ip global 2 matching [5]: ena/dis mul ip global 1 matching [4]: ena/dis mul ip 1 matching [3]: ena/dis uni ip 4 matching [2]: ena/dis uni ip 3 matching [1]: ena/dis uni ip 2 matching [0]: ena/dis uni ip 1 matching"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_geipen"] = _AF6CCI0012_RD_PTP._upen_geipen._cfg_geipen()
            return allFields

    class _upen_eth2cla_stk(AtRegister.AtRegister):
        def name(self):
            return "eth2cla sticky"
    
        def description(self):
            return "sticky of eth2cla interface"
            
        def width(self):
            return 64
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004100
            
        def endAddress(self):
            return 0xffffffff

        class _ethn_ptch(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 40
        
            def name(self):
                return "ethn_ptch"
            
            def description(self):
                return "ETH #n ptch sticky"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _eth0_ptch(AtRegister.AtRegisterField):
            def stopBit(self):
                return 36
                
            def startBit(self):
                return 36
        
            def name(self):
                return "eth0_ptch"
            
            def description(self):
                return "ETH_0 PTCH : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ethn_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 4
        
            def name(self):
                return "ethn_stk"
            
            def description(self):
                return "ETH_n valid, sop, eop, err sticky"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _eth0_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "eth0_err"
            
            def description(self):
                return "ETH_0 ERR  : 0: clear  1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _eth0_eop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "eth0_eop"
            
            def description(self):
                return "ETH_0 EOP  : 0: clear  1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _eth0_sop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "eth0_sop"
            
            def description(self):
                return "ETH_0 SOP  : 0: clear  1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _eth0_vld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "eth0_vld"
            
            def description(self):
                return "ETH_0 VALID: 0: clear  1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ethn_ptch"] = _AF6CCI0012_RD_PTP._upen_eth2cla_stk._ethn_ptch()
            allFields["eth0_ptch"] = _AF6CCI0012_RD_PTP._upen_eth2cla_stk._eth0_ptch()
            allFields["ethn_stk"] = _AF6CCI0012_RD_PTP._upen_eth2cla_stk._ethn_stk()
            allFields["eth0_err"] = _AF6CCI0012_RD_PTP._upen_eth2cla_stk._eth0_err()
            allFields["eth0_eop"] = _AF6CCI0012_RD_PTP._upen_eth2cla_stk._eth0_eop()
            allFields["eth0_sop"] = _AF6CCI0012_RD_PTP._upen_eth2cla_stk._eth0_sop()
            allFields["eth0_vld"] = _AF6CCI0012_RD_PTP._upen_eth2cla_stk._eth0_vld()
            return allFields

    class _upen_cla2epa_stk(AtRegister.AtRegister):
        def name(self):
            return "cla2epa sticky"
    
        def description(self):
            return "sticky of cla2epa interface"
            
        def width(self):
            return 96
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004101
            
        def endAddress(self):
            return 0xffffffff

        class _cla2epa_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 71
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cla2epa_stk"
            
            def description(self):
                return "sticky cla to epa"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cla2epa_stk"] = _AF6CCI0012_RD_PTP._upen_cla2epa_stk._cla2epa_stk()
            return allFields

    class _upen_cla2ptp_stk(AtRegister.AtRegister):
        def name(self):
            return "cla2ptp sticky"
    
        def description(self):
            return "sticky of cla2ptp interface"
            
        def width(self):
            return 96
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004102
            
        def endAddress(self):
            return 0xffffffff

        class _cla2ptp_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 71
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cla2ptp_stk"
            
            def description(self):
                return "sticky cla to ptp"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cla2ptp_stk"] = _AF6CCI0012_RD_PTP._upen_cla2ptp_stk._cla2ptp_stk()
            return allFields

    class _upen_parser_stk0(AtRegister.AtRegister):
        def name(self):
            return "parser sticky 0"
    
        def description(self):
            return "sticky 0  of parser"
            
        def width(self):
            return 96
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004104
            
        def endAddress(self):
            return 0xffffffff

        class _parser_stk0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 71
                
            def startBit(self):
                return 6
        
            def name(self):
                return "parser_stk0"
            
            def description(self):
                return "sticky parser"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _par0_free(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "par0_free"
            
            def description(self):
                return "PARSER_0 FREE BLK : 1: clear , 0: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _par0_eoseg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "par0_eoseg"
            
            def description(self):
                return "PARSER_0 EOSEG    : 1: clear , 0: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _par0_blkemp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "par0_blkemp"
            
            def description(self):
                return "PARSER_0 BLK EMPTY: 1: clear , 0: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _par0_errmru(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "par0_errmru"
            
            def description(self):
                return "PARSER_0 MRU ERROR: 1: clear , 0: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _par0_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "par0_err"
            
            def description(self):
                return "PARSER_0 PKT SHORT: 1: clear , 0: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _par0_done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "par0_done"
            
            def description(self):
                return "PARSER_0 DONE:      1: clear , 0: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["parser_stk0"] = _AF6CCI0012_RD_PTP._upen_parser_stk0._parser_stk0()
            allFields["par0_free"] = _AF6CCI0012_RD_PTP._upen_parser_stk0._par0_free()
            allFields["par0_eoseg"] = _AF6CCI0012_RD_PTP._upen_parser_stk0._par0_eoseg()
            allFields["par0_blkemp"] = _AF6CCI0012_RD_PTP._upen_parser_stk0._par0_blkemp()
            allFields["par0_errmru"] = _AF6CCI0012_RD_PTP._upen_parser_stk0._par0_errmru()
            allFields["par0_err"] = _AF6CCI0012_RD_PTP._upen_parser_stk0._par0_err()
            allFields["par0_done"] = _AF6CCI0012_RD_PTP._upen_parser_stk0._par0_done()
            return allFields

    class _upen_parser_stk1(AtRegister.AtRegister):
        def name(self):
            return "parser sticky 1"
    
        def description(self):
            return "sticky 1  of parser"
            
        def width(self):
            return 128
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004105
            
        def endAddress(self):
            return 0xffffffff

        class _parser_stk1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 107
                
            def startBit(self):
                return 12
        
            def name(self):
                return "parser_stk1"
            
            def description(self):
                return "sticky parser"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _par0_udpprt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "par0_udpprt"
            
            def description(self):
                return "PARSER_0 UDP PRT  : 0: clear, 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _par0_ipda(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "par0_ipda"
            
            def description(self):
                return "PARSER_0 IP DA    : 0: clear, 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _par0_ipsa(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "par0_ipsa"
            
            def description(self):
                return "PARSER_0 IP SA    : 0: clear, 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _par0_udpptl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "par0_udpptl"
            
            def description(self):
                return "PARSER_0 UDP PTL  : 0: clear, 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _par0_ipver(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "par0_ipver"
            
            def description(self):
                return "PARSER_0 IPVER  : 0: clear, 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _par0_mac(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "par0_mac"
            
            def description(self):
                return "PARSER_0 MAC    : 0: clear, 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _par0_mefecid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "par0_mefecid"
            
            def description(self):
                return "PARSER_0 MEFECID: 0: clear, 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _par0_pwlabel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "par0_pwlabel"
            
            def description(self):
                return "PARSER_0 PWLABEL: 0: clear, 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _par0_ethtyp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "par0_ethtyp"
            
            def description(self):
                return "PARSER_0 ETHTYPE: 0: clear, 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _par0_sndvlan(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "par0_sndvlan"
            
            def description(self):
                return "PARSER_0 SNDVLAN: 0: clear, 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _par0_fstvlan(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "par0_fstvlan"
            
            def description(self):
                return "PARSER_0 FSTVLAN: 0: clear, 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _par0_ptch(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "par0_ptch"
            
            def description(self):
                return "PARSER_0 PTCH   : 0: clear, 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["parser_stk1"] = _AF6CCI0012_RD_PTP._upen_parser_stk1._parser_stk1()
            allFields["par0_udpprt"] = _AF6CCI0012_RD_PTP._upen_parser_stk1._par0_udpprt()
            allFields["par0_ipda"] = _AF6CCI0012_RD_PTP._upen_parser_stk1._par0_ipda()
            allFields["par0_ipsa"] = _AF6CCI0012_RD_PTP._upen_parser_stk1._par0_ipsa()
            allFields["par0_udpptl"] = _AF6CCI0012_RD_PTP._upen_parser_stk1._par0_udpptl()
            allFields["par0_ipver"] = _AF6CCI0012_RD_PTP._upen_parser_stk1._par0_ipver()
            allFields["par0_mac"] = _AF6CCI0012_RD_PTP._upen_parser_stk1._par0_mac()
            allFields["par0_mefecid"] = _AF6CCI0012_RD_PTP._upen_parser_stk1._par0_mefecid()
            allFields["par0_pwlabel"] = _AF6CCI0012_RD_PTP._upen_parser_stk1._par0_pwlabel()
            allFields["par0_ethtyp"] = _AF6CCI0012_RD_PTP._upen_parser_stk1._par0_ethtyp()
            allFields["par0_sndvlan"] = _AF6CCI0012_RD_PTP._upen_parser_stk1._par0_sndvlan()
            allFields["par0_fstvlan"] = _AF6CCI0012_RD_PTP._upen_parser_stk1._par0_fstvlan()
            allFields["par0_ptch"] = _AF6CCI0012_RD_PTP._upen_parser_stk1._par0_ptch()
            return allFields

    class _upen_lkup_stk(AtRegister.AtRegister):
        def name(self):
            return "lookup sticky"
    
        def description(self):
            return "sticky of lookup"
            
        def width(self):
            return 96
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004106
            
        def endAddress(self):
            return 0xffffffff

        class _porttab_outen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 86
                
            def startBit(self):
                return 86
        
            def name(self):
                return "porttab_outen"
            
            def description(self):
                return "PORT    TAB OUT_EN: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ptchtab_outen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 85
                
            def startBit(self):
                return 85
        
            def name(self):
                return "ptchtab_outen"
            
            def description(self):
                return "PTCH    TAB OUT_EN: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _fvlntab_outen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 84
                
            def startBit(self):
                return 84
        
            def name(self):
                return "fvlntab_outen"
            
            def description(self):
                return "FSTVLAN TAB OUT_EN: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _svlntab_outen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 83
                
            def startBit(self):
                return 83
        
            def name(self):
                return "svlntab_outen"
            
            def description(self):
                return "SNDVLAN TAB OUT_EN: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _pwhash_outen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 82
                
            def startBit(self):
                return 82
        
            def name(self):
                return "pwhash_outen"
            
            def description(self):
                return "PWHASH  TAB OUT_EN: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _rulechk_outen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 80
                
            def startBit(self):
                return 80
        
            def name(self):
                return "rulechk_outen"
            
            def description(self):
                return "RULECHK TAB OUT_EN: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _porttab_win(AtRegister.AtRegisterField):
            def stopBit(self):
                return 78
                
            def startBit(self):
                return 78
        
            def name(self):
                return "porttab_win"
            
            def description(self):
                return "PORT    TABLE WIN: 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ptchtab_win(AtRegister.AtRegisterField):
            def stopBit(self):
                return 77
                
            def startBit(self):
                return 77
        
            def name(self):
                return "ptchtab_win"
            
            def description(self):
                return "PTCH    TABLE WIN: 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _fvlntab_win(AtRegister.AtRegisterField):
            def stopBit(self):
                return 76
                
            def startBit(self):
                return 76
        
            def name(self):
                return "fvlntab_win"
            
            def description(self):
                return "FSTVLAN TABLE WIN: 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _svlntab_win(AtRegister.AtRegisterField):
            def stopBit(self):
                return 75
                
            def startBit(self):
                return 75
        
            def name(self):
                return "svlntab_win"
            
            def description(self):
                return "SNDVLAN TABLE WIN: 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _pwhash_win(AtRegister.AtRegisterField):
            def stopBit(self):
                return 74
                
            def startBit(self):
                return 74
        
            def name(self):
                return "pwhash_win"
            
            def description(self):
                return "PWHASH  TABLE WIN: 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _etypcam_win(AtRegister.AtRegisterField):
            def stopBit(self):
                return 73
                
            def startBit(self):
                return 73
        
            def name(self):
                return "etypcam_win"
            
            def description(self):
                return "ETHTYPE TABLE WIN: 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _rulechk_win(AtRegister.AtRegisterField):
            def stopBit(self):
                return 72
                
            def startBit(self):
                return 72
        
            def name(self):
                return "rulechk_win"
            
            def description(self):
                return "RULECHK TABLE WIN: 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _lkup0_stk1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 71
                
            def startBit(self):
                return 8
        
            def name(self):
                return "lkup0_stk1"
            
            def description(self):
                return "LOOKUP_0 sticky 1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _lkup0_ptpind(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "lkup0_ptpind"
            
            def description(self):
                return "LOOKUP_0 PTP IND   : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _lkup0_udpind(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "lkup0_udpind"
            
            def description(self):
                return "LOOKUP_0 UDP IND   : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _lkup0_eoseg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "lkup0_eoseg"
            
            def description(self):
                return "LOOKUP_0 EOSEG     : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _lkup0_blkemp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "lkup0_blkemp"
            
            def description(self):
                return "LOOKUP_0 BLK EMPTY : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _lkup0_errmru(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "lkup0_errmru"
            
            def description(self):
                return "LOOKUP_0 MRU ERR: 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _lkup0_fail(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "lkup0_fail"
            
            def description(self):
                return "LOOKUP_0 FAIL   : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _lkup0_stk0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "lkup0_stk0"
            
            def description(self):
                return "LOOKUP_0 sticky 0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _lkup0_done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lkup0_done"
            
            def description(self):
                return "LOOKUP_0 DONE   : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["porttab_outen"] = _AF6CCI0012_RD_PTP._upen_lkup_stk._porttab_outen()
            allFields["ptchtab_outen"] = _AF6CCI0012_RD_PTP._upen_lkup_stk._ptchtab_outen()
            allFields["fvlntab_outen"] = _AF6CCI0012_RD_PTP._upen_lkup_stk._fvlntab_outen()
            allFields["svlntab_outen"] = _AF6CCI0012_RD_PTP._upen_lkup_stk._svlntab_outen()
            allFields["pwhash_outen"] = _AF6CCI0012_RD_PTP._upen_lkup_stk._pwhash_outen()
            allFields["rulechk_outen"] = _AF6CCI0012_RD_PTP._upen_lkup_stk._rulechk_outen()
            allFields["porttab_win"] = _AF6CCI0012_RD_PTP._upen_lkup_stk._porttab_win()
            allFields["ptchtab_win"] = _AF6CCI0012_RD_PTP._upen_lkup_stk._ptchtab_win()
            allFields["fvlntab_win"] = _AF6CCI0012_RD_PTP._upen_lkup_stk._fvlntab_win()
            allFields["svlntab_win"] = _AF6CCI0012_RD_PTP._upen_lkup_stk._svlntab_win()
            allFields["pwhash_win"] = _AF6CCI0012_RD_PTP._upen_lkup_stk._pwhash_win()
            allFields["etypcam_win"] = _AF6CCI0012_RD_PTP._upen_lkup_stk._etypcam_win()
            allFields["rulechk_win"] = _AF6CCI0012_RD_PTP._upen_lkup_stk._rulechk_win()
            allFields["lkup0_stk1"] = _AF6CCI0012_RD_PTP._upen_lkup_stk._lkup0_stk1()
            allFields["lkup0_ptpind"] = _AF6CCI0012_RD_PTP._upen_lkup_stk._lkup0_ptpind()
            allFields["lkup0_udpind"] = _AF6CCI0012_RD_PTP._upen_lkup_stk._lkup0_udpind()
            allFields["lkup0_eoseg"] = _AF6CCI0012_RD_PTP._upen_lkup_stk._lkup0_eoseg()
            allFields["lkup0_blkemp"] = _AF6CCI0012_RD_PTP._upen_lkup_stk._lkup0_blkemp()
            allFields["lkup0_errmru"] = _AF6CCI0012_RD_PTP._upen_lkup_stk._lkup0_errmru()
            allFields["lkup0_fail"] = _AF6CCI0012_RD_PTP._upen_lkup_stk._lkup0_fail()
            allFields["lkup0_stk0"] = _AF6CCI0012_RD_PTP._upen_lkup_stk._lkup0_stk0()
            allFields["lkup0_done"] = _AF6CCI0012_RD_PTP._upen_lkup_stk._lkup0_done()
            return allFields

    class _upen_datashidt_stk(AtRegister.AtRegister):
        def name(self):
            return "datashift sticky"
    
        def description(self):
            return "sticky of datashift"
            
        def width(self):
            return 64
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004107
            
        def endAddress(self):
            return 0xffffffff

        class _datsh0_ssercpu(AtRegister.AtRegisterField):
            def stopBit(self):
                return 43
                
            def startBit(self):
                return 43
        
            def name(self):
                return "datsh0_ssercpu"
            
            def description(self):
                return "DATSHF_0 SSER CPU : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_sseroam(AtRegister.AtRegisterField):
            def stopBit(self):
                return 42
                
            def startBit(self):
                return 42
        
            def name(self):
                return "datsh0_sseroam"
            
            def description(self):
                return "DATSHF_0 SSER OAM : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_sserdata(AtRegister.AtRegisterField):
            def stopBit(self):
                return 41
                
            def startBit(self):
                return 41
        
            def name(self):
                return "datsh0_sserdata"
            
            def description(self):
                return "DATSHF_0 SSER DATA: 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_sserpw(AtRegister.AtRegisterField):
            def stopBit(self):
                return 40
                
            def startBit(self):
                return 40
        
            def name(self):
                return "datsh0_sserpw"
            
            def description(self):
                return "DATSHF_0 SSER PW  : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_serptp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 37
                
            def startBit(self):
                return 37
        
            def name(self):
                return "datsh0_serptp"
            
            def description(self):
                return "DATSHF_0 SER PTP   : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_serepass(AtRegister.AtRegisterField):
            def stopBit(self):
                return 36
                
            def startBit(self):
                return 36
        
            def name(self):
                return "datsh0_serepass"
            
            def description(self):
                return "DATSHF_0 SER EPASS : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_serdcc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 35
        
            def name(self):
                return "datsh0_serdcc"
            
            def description(self):
                return "DATSHF_0 SER DCC   : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_serimsg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 34
                
            def startBit(self):
                return 34
        
            def name(self):
                return "datsh0_serimsg"
            
            def description(self):
                return "DATSHF_0 SER IMSG  : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_sercem(AtRegister.AtRegisterField):
            def stopBit(self):
                return 33
                
            def startBit(self):
                return 33
        
            def name(self):
                return "datsh0_sercem"
            
            def description(self):
                return "DATSHF_0 SER CEM   : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_serdrop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 32
                
            def startBit(self):
                return 32
        
            def name(self):
                return "datsh0_serdrop"
            
            def description(self):
                return "DATSHF_0 SER DROP  : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_mef(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "datsh0_mef"
            
            def description(self):
                return "DATSHF_0 MEF      : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_oam(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "datsh0_oam"
            
            def description(self):
                return "DATSHF_0 OAM      : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_mmpls(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "datsh0_mmpls"
            
            def description(self):
                return "DATSHF_0 MPLS MUL : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_umpls(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "datsh0_umpls"
            
            def description(self):
                return "DATSHF_0 MPLS UNI : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_ipv6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "datsh0_ipv6"
            
            def description(self):
                return "DATSHF_0 IPv6     : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_ipv4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "datsh0_ipv4"
            
            def description(self):
                return "DATSHF_0 IPv4     : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_4label(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "datsh0_4label"
            
            def description(self):
                return "DATSHF_0 4LABEL : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_3label(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "datsh0_3label"
            
            def description(self):
                return "DATSHF_0 3LABEL : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_2label(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "datsh0_2label"
            
            def description(self):
                return "DATSHF_0 2LABEL : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_1label(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "datsh0_1label"
            
            def description(self):
                return "DATSHF_0 1LABEL : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_0label(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "datsh0_0label"
            
            def description(self):
                return "DATSHF_0 0LABEL : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_2vlan(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "datsh0_2vlan"
            
            def description(self):
                return "DATSHF_0 2VLAN : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_1vlan(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "datsh0_1vlan"
            
            def description(self):
                return "DATSHF_0 1VLAN : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_0vlan(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "datsh0_0vlan"
            
            def description(self):
                return "DATSHF_0 0VLAN : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_ptpind(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "datsh0_ptpind"
            
            def description(self):
                return "DATSHF_0 PTP  IND : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_udpind(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "datsh0_udpind"
            
            def description(self):
                return "DATSHF_0 UDP  IND : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_lkwin(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "datsh0_lkwin"
            
            def description(self):
                return "DATSHF_0 LKUP WIN : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_err4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "datsh0_err4"
            
            def description(self):
                return "DATSHF_0 MRU ERROR: 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_err3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "datsh0_err3"
            
            def description(self):
                return "DATSHF_0 BLK EMPTY: 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_err2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "datsh0_err2"
            
            def description(self):
                return "DATSHF_0 LKUP FAIL: 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_err0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "datsh0_err0"
            
            def description(self):
                return "DATSHF_0 ETH ERROR: 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_eop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "datsh0_eop"
            
            def description(self):
                return "DATSHF_0 EOP      : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_sop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "datsh0_sop"
            
            def description(self):
                return "DATSHF_0 SOP      : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _datsh0_vld(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "datsh0_vld"
            
            def description(self):
                return "DATSHF_0 VALID    : 0: clear 1: set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["datsh0_ssercpu"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_ssercpu()
            allFields["datsh0_sseroam"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_sseroam()
            allFields["datsh0_sserdata"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_sserdata()
            allFields["datsh0_sserpw"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_sserpw()
            allFields["datsh0_serptp"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_serptp()
            allFields["datsh0_serepass"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_serepass()
            allFields["datsh0_serdcc"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_serdcc()
            allFields["datsh0_serimsg"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_serimsg()
            allFields["datsh0_sercem"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_sercem()
            allFields["datsh0_serdrop"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_serdrop()
            allFields["datsh0_mef"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_mef()
            allFields["datsh0_oam"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_oam()
            allFields["datsh0_mmpls"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_mmpls()
            allFields["datsh0_umpls"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_umpls()
            allFields["datsh0_ipv6"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_ipv6()
            allFields["datsh0_ipv4"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_ipv4()
            allFields["datsh0_4label"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_4label()
            allFields["datsh0_3label"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_3label()
            allFields["datsh0_2label"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_2label()
            allFields["datsh0_1label"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_1label()
            allFields["datsh0_0label"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_0label()
            allFields["datsh0_2vlan"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_2vlan()
            allFields["datsh0_1vlan"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_1vlan()
            allFields["datsh0_0vlan"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_0vlan()
            allFields["datsh0_ptpind"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_ptpind()
            allFields["datsh0_udpind"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_udpind()
            allFields["datsh0_lkwin"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_lkwin()
            allFields["datsh0_err4"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_err4()
            allFields["datsh0_err3"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_err3()
            allFields["datsh0_err2"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_err2()
            allFields["datsh0_err0"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_err0()
            allFields["datsh0_eop"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_eop()
            allFields["datsh0_sop"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_sop()
            allFields["datsh0_vld"] = _AF6CCI0012_RD_PTP._upen_datashidt_stk._datsh0_vld()
            return allFields

    class _upen_allc_stk(AtRegister.AtRegister):
        def name(self):
            return "allc sticky"
    
        def description(self):
            return "sticky of egress header"
            
        def width(self):
            return 64
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000410a
            
        def endAddress(self):
            return 0xffffffff

        class _ffge11_full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "ffge11_full"
            
            def description(self):
                return "FIFO GE11 FULL: 0: clear 1:set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ffge10_full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "ffge10_full"
            
            def description(self):
                return "FIFO GE10 FULL: 0: clear 1:set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ffge9_full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "ffge9_full"
            
            def description(self):
                return "FIFO GE9  FULL: 0: clear 1:set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ffge8_full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ffge8_full"
            
            def description(self):
                return "FIFO GE8  FULL: 0: clear 1:set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ffge7_full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "ffge7_full"
            
            def description(self):
                return "FIFO GE7  FULL: 0: clear 1:set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ffge6_full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "ffge6_full"
            
            def description(self):
                return "FIFO GE6  FULL: 0: clear 1:set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ffge5_full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "ffge5_full"
            
            def description(self):
                return "FIFO GE5  FULL: 0: clear 1:set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ffge4_full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "ffge4_full"
            
            def description(self):
                return "FIFO GE4  FULL: 0: clear 1:set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ffxfi_full(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "ffxfi_full"
            
            def description(self):
                return "FIFO XFI FULL: 0: clear 1:set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _blkreq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "blkreq"
            
            def description(self):
                return "BLK REQUEST: 0: clear 1:set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _buffre(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "buffre"
            
            def description(self):
                return "BUFFER FREE: 0: clear 1:set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _parfre(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "parfre"
            
            def description(self):
                return "PARSER FREE: 0: clear 1:set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _blkemp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "blkemp"
            
            def description(self):
                return "BLK EMPTY  : 0: clear 1:set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _samefree(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "samefree"
            
            def description(self):
                return "SAME FREE  : 0: clear 1:set"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ffge11_full"] = _AF6CCI0012_RD_PTP._upen_allc_stk._ffge11_full()
            allFields["ffge10_full"] = _AF6CCI0012_RD_PTP._upen_allc_stk._ffge10_full()
            allFields["ffge9_full"] = _AF6CCI0012_RD_PTP._upen_allc_stk._ffge9_full()
            allFields["ffge8_full"] = _AF6CCI0012_RD_PTP._upen_allc_stk._ffge8_full()
            allFields["ffge7_full"] = _AF6CCI0012_RD_PTP._upen_allc_stk._ffge7_full()
            allFields["ffge6_full"] = _AF6CCI0012_RD_PTP._upen_allc_stk._ffge6_full()
            allFields["ffge5_full"] = _AF6CCI0012_RD_PTP._upen_allc_stk._ffge5_full()
            allFields["ffge4_full"] = _AF6CCI0012_RD_PTP._upen_allc_stk._ffge4_full()
            allFields["ffxfi_full"] = _AF6CCI0012_RD_PTP._upen_allc_stk._ffxfi_full()
            allFields["blkreq"] = _AF6CCI0012_RD_PTP._upen_allc_stk._blkreq()
            allFields["buffre"] = _AF6CCI0012_RD_PTP._upen_allc_stk._buffre()
            allFields["parfre"] = _AF6CCI0012_RD_PTP._upen_allc_stk._parfre()
            allFields["blkemp"] = _AF6CCI0012_RD_PTP._upen_allc_stk._blkemp()
            allFields["samefree"] = _AF6CCI0012_RD_PTP._upen_allc_stk._samefree()
            return allFields

    class _upen_allc_blkfre_cnt(AtRegister.AtRegister):
        def name(self):
            return "allc_blkfre_cnt"
    
        def description(self):
            return "allc block free counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004200
            
        def endAddress(self):
            return 0xffffffff

        class _allc_blkfre_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "allc_blkfre_cnt"
            
            def description(self):
                return "block free counter"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["allc_blkfre_cnt"] = _AF6CCI0012_RD_PTP._upen_allc_blkfre_cnt._allc_blkfre_cnt()
            return allFields

    class _upen_allc_blkiss_cnt(AtRegister.AtRegister):
        def name(self):
            return "allc_blkiss_cnt"
    
        def description(self):
            return "allc block issue counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004201
            
        def endAddress(self):
            return 0xffffffff

        class _allc_blkiss_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "allc_blkiss_cnt"
            
            def description(self):
                return "block issue counter"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["allc_blkiss_cnt"] = _AF6CCI0012_RD_PTP._upen_allc_blkiss_cnt._allc_blkiss_cnt()
            return allFields

    class _upen_todinit_cfg(AtRegister.AtRegister):
        def name(self):
            return "todinit_cfg"
    
        def description(self):
            return "init value tod"
            
        def width(self):
            return 64
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002000
            
        def endAddress(self):
            return 0xffffffff

        class _upen_todsecinit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "upen_todsecinit"
            
            def description(self):
                return "cfg init sec"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["upen_todsecinit"] = _AF6CCI0012_RD_PTP._upen_todinit_cfg._upen_todsecinit()
            return allFields

    class _upen_tod_cur(AtRegister.AtRegister):
        def name(self):
            return "tod_current"
    
        def description(self):
            return "current timer"
            
        def width(self):
            return 64
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002001
            
        def endAddress(self):
            return 0xffffffff

        class _cur_todsec(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cur_todsec"
            
            def description(self):
                return "current second"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cur_todsec"] = _AF6CCI0012_RD_PTP._upen_tod_cur._cur_todsec()
            return allFields

    class _upen_tod_mode(AtRegister.AtRegister):
        def name(self):
            return "todmode_cfg"
    
        def description(self):
            return "operaation ToD"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002002
            
        def endAddress(self):
            return 0xffffffff

        class _compensate_route_1pps(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 3
        
            def name(self):
                return "compensate_route_1pps"
            
            def description(self):
                return "The ability to offset their internal time counter with respect the 1PPS signal they receive. This is to compensate for the propagation delay of the 1PPS signal in our system."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _upen_todmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "upen_todmode"
            
            def description(self):
                return "select output tod \"0\" free-run, \"1\" external pps"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["compensate_route_1pps"] = _AF6CCI0012_RD_PTP._upen_tod_mode._compensate_route_1pps()
            allFields["upen_todmode"] = _AF6CCI0012_RD_PTP._upen_tod_mode._upen_todmode()
            return allFields

    class _upen_bp_latency_mode(AtRegister.AtRegister):
        def name(self):
            return "bp_latency_cfg"
    
        def description(self):
            return "This register configure latency adjust for tx/rx backplan serdes"
            
        def width(self):
            return 22
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x2020 + $BackplanID"
            
        def startAddress(self):
            return 0x00002020
            
        def endAddress(self):
            return 0x00002021

        class _tx_latency_adj_bp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 11
        
            def name(self):
                return "tx_latency_adj_bp"
            
            def description(self):
                return "latency adjust from tx backplan MAC to serdes pin in nanosecond"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _rx_latency_adj_bp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_latency_adj_bp"
            
            def description(self):
                return "latency adjust from rx backplan serdes pin to MAC in nanosecond"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tx_latency_adj_bp"] = _AF6CCI0012_RD_PTP._upen_bp_latency_mode._tx_latency_adj_bp()
            allFields["rx_latency_adj_bp"] = _AF6CCI0012_RD_PTP._upen_bp_latency_mode._rx_latency_adj_bp()
            return allFields

    class _upen_face_latency_mode(AtRegister.AtRegister):
        def name(self):
            return "face_latency_cfg"
    
        def description(self):
            return "This register configure latency adjust for tx/rx face plate serdes"
            
        def width(self):
            return 22
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x2040 + $FacePlateID"
            
        def startAddress(self):
            return 0x00002040
            
        def endAddress(self):
            return 0x0000204f

        class _tx_latency_adj_face(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 11
        
            def name(self):
                return "tx_latency_adj_face"
            
            def description(self):
                return "latency adjust from tx face plate MAC to serdes pin in nanosecond calibrated value of TENGE is 0x82, calibrated value of GE is 0x198, calibrated value of FX is 0x490"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _rx_latency_adj_face(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_latency_adj_face"
            
            def description(self):
                return "latency adjust from rx face plate serdes pin to MAC in nanosecond calibrated value of TENGE is 0x82, calibrated value of GE is 0x138, calibrated value of FX is 0x581"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tx_latency_adj_face"] = _AF6CCI0012_RD_PTP._upen_face_latency_mode._tx_latency_adj_face()
            allFields["rx_latency_adj_face"] = _AF6CCI0012_RD_PTP._upen_face_latency_mode._rx_latency_adj_face()
            return allFields

    class _upen_hold_40g1(AtRegister.AtRegister):
        def name(self):
            return "Hold Register 40G 1"
    
        def description(self):
            return "This register hold value 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000b020
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_hold_40g1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_hold_40g1"
            
            def description(self):
                return "hold 40g value 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_hold_40g1"] = _AF6CCI0012_RD_PTP._upen_hold_40g1._cfg_hold_40g1()
            return allFields

    class _upen_hold_40g2(AtRegister.AtRegister):
        def name(self):
            return "Hold Register 40G 2"
    
        def description(self):
            return "This register hold value 2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000b021
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_hold_40g2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_hold_40g2"
            
            def description(self):
                return "hold 40g value 2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_hold_40g2"] = _AF6CCI0012_RD_PTP._upen_hold_40g2._cfg_hold_40g2()
            return allFields

    class _upen_hold_40g3(AtRegister.AtRegister):
        def name(self):
            return "Hold Register 40G 3"
    
        def description(self):
            return "This register hold value 3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000b022
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_hold_40g3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_hold_40g3"
            
            def description(self):
                return "hold 40g value 3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_hold_40g3"] = _AF6CCI0012_RD_PTP._upen_hold_40g3._cfg_hold_40g3()
            return allFields

    class _upen_ptp40_stmode(AtRegister.AtRegister):
        def name(self):
            return "PTP enable"
    
        def description(self):
            return "used to indicate 1-step/2-step mode for each port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008004
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ptp_stmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ptp_stmode"
            
            def description(self):
                return "'1' : 2-step, '0': 1-step"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ptp_stmode"] = _AF6CCI0012_RD_PTP._upen_ptp40_stmode._cfg_ptp_stmode()
            return allFields

    class _upen_ptp40_ms(AtRegister.AtRegister):
        def name(self):
            return "PTP enable"
    
        def description(self):
            return "used to indicate master/slaver mode for each port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008005
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ptp_ms(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ptp_ms"
            
            def description(self):
                return "'1' : master, '0': slaver"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ptp_ms"] = _AF6CCI0012_RD_PTP._upen_ptp40_ms._cfg_ptp_ms()
            return allFields

    class _upen_ptp40_dev(AtRegister.AtRegister):
        def name(self):
            return "PTP Device"
    
        def description(self):
            return "This register is used to config PTP device"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008006
            
        def endAddress(self):
            return 0xffffffff

        class _device_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "device_mode"
            
            def description(self):
                return "'00': BC mode, '01': reserved '10': TC Separate mode, '11': TC General mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["device_mode"] = _AF6CCI0012_RD_PTP._upen_ptp40_dev._device_mode()
            return allFields

    class _upen_cfg_ptp_bypass(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "enable for any MAC/IPv4/IPv6"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000b000
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ptp_bypass(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ptp_bypass"
            
            def description(self):
                return "bit[0] l2_sa bit[1] l2_da bit[2] ip4_sa bit[3] ip4_da bit[4] ip6_sa bit[5] ip6_da"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ptp_bypass"] = _AF6CCI0012_RD_PTP._upen_cfg_ptp_bypass._cfg_ptp_bypass()
            return allFields

    class _upen_cfg_mac_sa(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp mac sa"
            
        def width(self):
            return 128
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000b001
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_mac_sa(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_mac_sa"
            
            def description(self):
                return "MAC SA"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_mac_sa"] = _AF6CCI0012_RD_PTP._upen_cfg_mac_sa._cfg_mac_sa()
            return allFields

    class _upen_cfg_mac_da(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp mac da"
            
        def width(self):
            return 128
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000b002
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_mac_da(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_mac_da"
            
            def description(self):
                return "MAC DA"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_mac_da"] = _AF6CCI0012_RD_PTP._upen_cfg_mac_da._cfg_mac_da()
            return allFields

    class _upen_cfg_ip4_sa1(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ipv4 sa1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000b003
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ipv4_sa1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ipv4_sa1"
            
            def description(self):
                return "IPv4 SA1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ipv4_sa1"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_sa1._cfg_ipv4_sa1()
            return allFields

    class _upen_cfg_ip4_sa2(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ipv4 sa2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000b004
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ipv4_sa2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ipv4_sa2"
            
            def description(self):
                return "IPv4 SA2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ipv4_sa2"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_sa2._cfg_ipv4_sa2()
            return allFields

    class _upen_cfg_ip4_sa3(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ipv4 sa3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000b005
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ipv4_sa3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ipv4_sa3"
            
            def description(self):
                return "IPv4 SA3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ipv4_sa3"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_sa3._cfg_ipv4_sa3()
            return allFields

    class _upen_cfg_ip4_sa4(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ipv4 sa4"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000b006
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ipv4_sa4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ipv4_sa4"
            
            def description(self):
                return "IPv4 SA4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ipv4_sa4"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_sa4._cfg_ipv4_sa4()
            return allFields

    class _upen_cfg_ip4_da1(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ip4 da1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000b007
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ip4_da1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ip4_da1"
            
            def description(self):
                return "MAC DA1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ip4_da1"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_da1._cfg_ip4_da1()
            return allFields

    class _upen_cfg_ip4_da2(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ip4 da2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000b008
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ip4_da2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ip4_da2"
            
            def description(self):
                return "MAC DA2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ip4_da2"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_da2._cfg_ip4_da2()
            return allFields

    class _upen_cfg_ip4_da3(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ip4 da3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000b009
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ip4_da3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ip4_da3"
            
            def description(self):
                return "MAC DA3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ip4_da3"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_da3._cfg_ip4_da3()
            return allFields

    class _upen_cfg_ip4_da4(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ip4 da4"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000b00a
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ip4_da4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ip4_da4"
            
            def description(self):
                return "MAC DA4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ip4_da4"] = _AF6CCI0012_RD_PTP._upen_cfg_ip4_da4._cfg_ip4_da4()
            return allFields

    class _upen_cfg_ip6_sa1(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ipv6 sa1"
            
        def width(self):
            return 128
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000b00b
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ipv6_sa1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ipv6_sa1"
            
            def description(self):
                return "IPv6 SA1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ipv6_sa1"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_sa1._cfg_ipv6_sa1()
            return allFields

    class _upen_cfg_ip6_sa2(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ipv6 sa2"
            
        def width(self):
            return 128
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000b00c
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ipv6_sa2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ipv6_sa2"
            
            def description(self):
                return "IPv6 SA2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ipv6_sa2"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_sa2._cfg_ipv6_sa2()
            return allFields

    class _upen_cfg_ip6_sa3(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ipv6 sa3"
            
        def width(self):
            return 128
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000b00d
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ipv6_sa3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ipv6_sa3"
            
            def description(self):
                return "IPv6 SA3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ipv6_sa3"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_sa3._cfg_ipv6_sa3()
            return allFields

    class _upen_cfg_ip6_sa4(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ipv6 sa4"
            
        def width(self):
            return 128
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000b00e
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ipv4_sa4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ipv4_sa4"
            
            def description(self):
                return "IPv4 SA4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ipv4_sa4"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_sa4._cfg_ipv4_sa4()
            return allFields

    class _upen_cfg_ip6_da1(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ipv6 da1"
            
        def width(self):
            return 128
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000b00f
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ipv6_da1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ipv6_da1"
            
            def description(self):
                return "IPv6 DA1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ipv6_da1"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_da1._cfg_ipv6_da1()
            return allFields

    class _upen_cfg_ip6_da2(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ipv6 da2"
            
        def width(self):
            return 128
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000b010
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ipv6_da2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ipv6_da2"
            
            def description(self):
                return "IPv6 DA2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ipv6_da2"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_da2._cfg_ipv6_da2()
            return allFields

    class _upen_cfg_ip6_da3(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ipv6 da3"
            
        def width(self):
            return 128
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000b011
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ipv6_da3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ipv6_da3"
            
            def description(self):
                return "IPv6 DA3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ipv6_da3"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_da3._cfg_ipv6_da3()
            return allFields

    class _upen_cfg_ip6_da4(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "cfg ptp ipv6 da4"
            
        def width(self):
            return 128
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000b012
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_ipv6_da4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_ipv6_da4"
            
            def description(self):
                return "IPv6 DA4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_ipv6_da4"] = _AF6CCI0012_RD_PTP._upen_cfg_ip6_da4._cfg_ipv6_da4()
            return allFields

    class _upen_cnt_pkt_in_r2c(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "counter iport"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x00_8460 +  $ipid"
            
        def startAddress(self):
            return 0x00008460
            
        def endAddress(self):
            return 0xffffffff

        class _cnt_input_port(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_input_port"
            
            def description(self):
                return "Total RX counter input"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_input_port"] = _AF6CCI0012_RD_PTP._upen_cnt_pkt_in_r2c._cnt_input_port()
            return allFields

    class _upen_cnt_pkt_in_ro(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "counter iport"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x008470 +  $ipid"
            
        def startAddress(self):
            return 0x00008470
            
        def endAddress(self):
            return 0xffffffff

        class _cnt_input_port(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_input_port"
            
            def description(self):
                return "Total RX counter input"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_input_port"] = _AF6CCI0012_RD_PTP._upen_cnt_pkt_in_ro._cnt_input_port()
            return allFields

    class _upen_cnt_ptppkt_rx_r2c(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "Total Rx counter ptp "
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008420
            
        def endAddress(self):
            return 0xffffffff

        class _rxcnt_ptp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxcnt_ptp"
            
            def description(self):
                return "Total counter RX 40G port"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxcnt_ptp"] = _AF6CCI0012_RD_PTP._upen_cnt_ptppkt_rx_r2c._rxcnt_ptp()
            return allFields

    class _upen_cnt_ptppkt_rx_ro(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "Total Rx counter ptp "
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008430
            
        def endAddress(self):
            return 0xffffffff

        class _rxcnt_ptp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxcnt_ptp"
            
            def description(self):
                return "Total counter RX 40G port"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxcnt_ptp"] = _AF6CCI0012_RD_PTP._upen_cnt_ptppkt_rx_ro._rxcnt_ptp()
            return allFields

    class _upen_cnt_txpkt_out_r2c(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "Total Tx counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008400
            
        def endAddress(self):
            return 0xffffffff

        class _tx40cnt_ptp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tx40cnt_ptp"
            
            def description(self):
                return "tx counter 40G output"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tx40cnt_ptp"] = _AF6CCI0012_RD_PTP._upen_cnt_txpkt_out_r2c._tx40cnt_ptp()
            return allFields

    class _upen_cnt_txpkt_out_ro(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "Total Tx counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008410
            
        def endAddress(self):
            return 0xffffffff

        class _tx40cnt_ptp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tx40cnt_ptp"
            
            def description(self):
                return "tx counter 40G output"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tx40cnt_ptp"] = _AF6CCI0012_RD_PTP._upen_cnt_txpkt_out_ro._tx40cnt_ptp()
            return allFields

    class _upen_rx_sync_cnt40_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter Sync Received 40G PORT"
    
        def description(self):
            return "Number of Sync message received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_8480+ $pid"
            
        def startAddress(self):
            return 0x00008480
            
        def endAddress(self):
            return 0xffffffff

        class _syncrx_cnt40(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "syncrx_cnt40"
            
            def description(self):
                return "number sync packet at Rx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["syncrx_cnt40"] = _AF6CCI0012_RD_PTP._upen_rx_sync_cnt40_r2c._syncrx_cnt40()
            return allFields

    class _upen_rx_sync_cnt40_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter Sync Received 40G PORT"
    
        def description(self):
            return "Number of Sync message received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_84C0+ $pid"
            
        def startAddress(self):
            return 0x000084c0
            
        def endAddress(self):
            return 0xffffffff

        class _syncrx_cnt40(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "syncrx_cnt40"
            
            def description(self):
                return "number sync packet at Rx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["syncrx_cnt40"] = _AF6CCI0012_RD_PTP._upen_rx_sync_cnt40_ro._syncrx_cnt40()
            return allFields

    class _upen_rx_folu_cnt40_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter Folu Received 40G PORT"
    
        def description(self):
            return "Number of folu message received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_8490 + $pid"
            
        def startAddress(self):
            return 0x00008490
            
        def endAddress(self):
            return 0xffffffff

        class _folurx_cnt40(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "folurx_cnt40"
            
            def description(self):
                return "number folu packet at Rx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["folurx_cnt40"] = _AF6CCI0012_RD_PTP._upen_rx_folu_cnt40_r2c._folurx_cnt40()
            return allFields

    class _upen_rx_folu_cnt40_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter Folu Received 40G PORT"
    
        def description(self):
            return "Number of folu message received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_84D0 + $pid"
            
        def startAddress(self):
            return 0x000084d0
            
        def endAddress(self):
            return 0xffffffff

        class _folurx_cnt40(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "folurx_cnt40"
            
            def description(self):
                return "number folu packet at Rx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["folurx_cnt40"] = _AF6CCI0012_RD_PTP._upen_rx_folu_cnt40_ro._folurx_cnt40()
            return allFields

    class _upen_rx_dreq_cnt40_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter dreq Received 40G PORT"
    
        def description(self):
            return "Number of dreq message received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_84A0+$pid"
            
        def startAddress(self):
            return 0x000084a0
            
        def endAddress(self):
            return 0xffffffff

        class _dreqrx_cnt40(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dreqrx_cnt40"
            
            def description(self):
                return "number dreq packet at Rx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dreqrx_cnt40"] = _AF6CCI0012_RD_PTP._upen_rx_dreq_cnt40_r2c._dreqrx_cnt40()
            return allFields

    class _upen_rx_dreq_cnt40_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter dreq Received 40G PORT"
    
        def description(self):
            return "Number of dreq message received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_84E0+$pid"
            
        def startAddress(self):
            return 0x000084e0
            
        def endAddress(self):
            return 0xffffffff

        class _dreqrx_cnt40(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dreqrx_cnt40"
            
            def description(self):
                return "number dreq packet at Rx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dreqrx_cnt40"] = _AF6CCI0012_RD_PTP._upen_rx_dreq_cnt40_ro._dreqrx_cnt40()
            return allFields

    class _upen_rx_dres_cnt40_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter dres Received 40G PORT"
    
        def description(self):
            return "Number of dres message received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_84B0 + $pid"
            
        def startAddress(self):
            return 0x000084b0
            
        def endAddress(self):
            return 0xffffffff

        class _dresrx_cnt40(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dresrx_cnt40"
            
            def description(self):
                return "number dres packet at Rx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dresrx_cnt40"] = _AF6CCI0012_RD_PTP._upen_rx_dres_cnt40_r2c._dresrx_cnt40()
            return allFields

    class _upen_rx_dres_cnt40_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter dres Received 40G PORT"
    
        def description(self):
            return "Number of dres message received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_84F0 + $pid"
            
        def startAddress(self):
            return 0x000084f0
            
        def endAddress(self):
            return 0xffffffff

        class _dresrx_cnt40(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dresrx_cnt40"
            
            def description(self):
                return "number dres packet at Rx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dresrx_cnt40"] = _AF6CCI0012_RD_PTP._upen_rx_dres_cnt40_ro._dresrx_cnt40()
            return allFields

    class _upen_rx_extr_err_syncnt40_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Sync Extract Err 40G PORT"
    
        def description(self):
            return "Number of extr err sync mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_8500 + $pid"
            
        def startAddress(self):
            return 0x00008500
            
        def endAddress(self):
            return 0xffffffff

        class _rx_extrerr_synccnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_extrerr_synccnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_extrerr_synccnt"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_syncnt40_r2c._rx_extrerr_synccnt()
            return allFields

    class _upen_rx_extr_err_syncnt40_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Sync Extract Err 40G PORT"
    
        def description(self):
            return "Number of extr err sync mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_8540 + $pid"
            
        def startAddress(self):
            return 0x00008540
            
        def endAddress(self):
            return 0xffffffff

        class _rx_extrerr_synccnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_extrerr_synccnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_extrerr_synccnt"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_syncnt40_ro._rx_extrerr_synccnt()
            return allFields

    class _upen_rx_extr_err_folucnt40_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Folu Extract Err 40G PORT"
    
        def description(self):
            return "Number of extr err folu mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_8510 + $pid"
            
        def startAddress(self):
            return 0x00008510
            
        def endAddress(self):
            return 0xffffffff

        class _rx_extrerr_foluccnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_extrerr_foluccnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_extrerr_foluccnt"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_folucnt40_r2c._rx_extrerr_foluccnt()
            return allFields

    class _upen_rx_extr_err_folucnt40_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Folu Extract Err 40G PORT"
    
        def description(self):
            return "Number of extr err folu mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_8550 + $pid"
            
        def startAddress(self):
            return 0x00008550
            
        def endAddress(self):
            return 0xffffffff

        class _rx_extrerr_foluccnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_extrerr_foluccnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_extrerr_foluccnt"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_folucnt40_ro._rx_extrerr_foluccnt()
            return allFields

    class _upen_rx_extr_err_dreqcnt40_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Dreq Extract Err 40G PORT"
    
        def description(self):
            return "Number of extr err dreq mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_8520 + $pid"
            
        def startAddress(self):
            return 0x00008520
            
        def endAddress(self):
            return 0xffffffff

        class _rx_extrerr_dreqcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_extrerr_dreqcnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_extrerr_dreqcnt"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_dreqcnt40_r2c._rx_extrerr_dreqcnt()
            return allFields

    class _upen_rx_extr_err_dreqcnt40_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Dreq Extract Err 40G PORT"
    
        def description(self):
            return "Number of extr err dreq mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_8560 + $pid"
            
        def startAddress(self):
            return 0x00008560
            
        def endAddress(self):
            return 0xffffffff

        class _rx_extrerr_dreqcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_extrerr_dreqcnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_extrerr_dreqcnt"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_dreqcnt40_ro._rx_extrerr_dreqcnt()
            return allFields

    class _upen_rx_extr_err_drescnt40_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Dres Extract Err 40G PORT"
    
        def description(self):
            return "Number of extr err dres mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_8530 + $pid"
            
        def startAddress(self):
            return 0x00008530
            
        def endAddress(self):
            return 0xffffffff

        class _rx_extrerr_drescnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_extrerr_drescnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_extrerr_drescnt"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_drescnt40_r2c._rx_extrerr_drescnt()
            return allFields

    class _upen_rx_extr_err_drescnt40_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Dres Extract Err 40G PORT"
    
        def description(self):
            return "Number of extr err dres mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_8570 + $pid"
            
        def startAddress(self):
            return 0x00008570
            
        def endAddress(self):
            return 0xffffffff

        class _rx_extrerr_drescnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_extrerr_drescnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_extrerr_drescnt"] = _AF6CCI0012_RD_PTP._upen_rx_extr_err_drescnt40_ro._rx_extrerr_drescnt()
            return allFields

    class _upen_rx_chs_err_syncnt40_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Sync Chsum Err 40G PORT"
    
        def description(self):
            return "Number of Chs err sync mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_8580 + $pid"
            
        def startAddress(self):
            return 0x00008580
            
        def endAddress(self):
            return 0xffffffff

        class _rx_cherr_synccnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_cherr_synccnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_cherr_synccnt"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_syncnt40_r2c._rx_cherr_synccnt()
            return allFields

    class _upen_rx_chs_err_syncnt40_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Sync Chsum Err 40G PORT"
    
        def description(self):
            return "Number of Chs err sync mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_85C0 + $pid"
            
        def startAddress(self):
            return 0x000085c0
            
        def endAddress(self):
            return 0xffffffff

        class _rx_cherr_synccnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_cherr_synccnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_cherr_synccnt"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_syncnt40_ro._rx_cherr_synccnt()
            return allFields

    class _upen_rx_chs_err_folucnt40_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Folu Chsum Err 40G PORT"
    
        def description(self):
            return "Number of Chs err folu mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_8590 + $pid"
            
        def startAddress(self):
            return 0x00008590
            
        def endAddress(self):
            return 0xffffffff

        class _rx_cherr_folucnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_cherr_folucnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_cherr_folucnt"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_folucnt40_r2c._rx_cherr_folucnt()
            return allFields

    class _upen_rx_chs_err_folucnt40_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Folu Chsum Err 40G PORT"
    
        def description(self):
            return "Number of Chs err folu mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_85D0 + $pid"
            
        def startAddress(self):
            return 0x000085d0
            
        def endAddress(self):
            return 0xffffffff

        class _rx_cherr_folucnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_cherr_folucnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_cherr_folucnt"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_folucnt40_ro._rx_cherr_folucnt()
            return allFields

    class _upen_rx_chs_err_dreqcnt40_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Dreq Chsum Err 40G PORT"
    
        def description(self):
            return "Number of Chs err dreq mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_85A0 + $pid"
            
        def startAddress(self):
            return 0x000085a0
            
        def endAddress(self):
            return 0xffffffff

        class _rx_cherr_dreqcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_cherr_dreqcnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_cherr_dreqcnt"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_dreqcnt40_r2c._rx_cherr_dreqcnt()
            return allFields

    class _upen_rx_chs_err_dreqcnt40_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Dreq Chsum Err 40G PORT"
    
        def description(self):
            return "Number of Chs err dreq mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_85E0 + $pid"
            
        def startAddress(self):
            return 0x000085e0
            
        def endAddress(self):
            return 0xffffffff

        class _rx_cherr_dreqcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_cherr_dreqcnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_cherr_dreqcnt"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_dreqcnt40_ro._rx_cherr_dreqcnt()
            return allFields

    class _upen_rx_chs_err_drescnt40_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Dres Chsum Err 40G PORT"
    
        def description(self):
            return "Number of Chs err dres mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_85B0 + $pid"
            
        def startAddress(self):
            return 0x000085b0
            
        def endAddress(self):
            return 0xffffffff

        class _rx_cherr_drescnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_cherr_drescnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_cherr_drescnt"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_drescnt40_r2c._rx_cherr_drescnt()
            return allFields

    class _upen_rx_chs_err_drescnt40_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter Rx Dres Chsum Err 40G PORT"
    
        def description(self):
            return "Number of Chs err dres mess"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter "
            
        def fomular(self):
            return "0x00_85F0 + $pid"
            
        def startAddress(self):
            return 0x000085f0
            
        def endAddress(self):
            return 0xffffffff

        class _rx_cherr_drescnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_cherr_drescnt"
            
            def description(self):
                return ""
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_cherr_drescnt"] = _AF6CCI0012_RD_PTP._upen_rx_chs_err_drescnt40_ro._rx_cherr_drescnt()
            return allFields

    class _upen_tx_sync_cnt40_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter Sync Transmit 40G PORT"
    
        def description(self):
            return "Number of Sync message transmit"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_8600 + $pid"
            
        def startAddress(self):
            return 0x00008600
            
        def endAddress(self):
            return 0xffffffff

        class _synctx_cnt40(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "synctx_cnt40"
            
            def description(self):
                return "number sync packet at Tx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["synctx_cnt40"] = _AF6CCI0012_RD_PTP._upen_tx_sync_cnt40_r2c._synctx_cnt40()
            return allFields

    class _upen_tx_sync_cnt40_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter Sync Transmit 40G PORT"
    
        def description(self):
            return "Number of Sync message transmit"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_8640 + $pid"
            
        def startAddress(self):
            return 0x00008640
            
        def endAddress(self):
            return 0xffffffff

        class _synctx_cnt40(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "synctx_cnt40"
            
            def description(self):
                return "number sync packet at Tx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["synctx_cnt40"] = _AF6CCI0012_RD_PTP._upen_tx_sync_cnt40_ro._synctx_cnt40()
            return allFields

    class _upen_tx_folu_cnt40_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter Folu Transmit 40G PORT"
    
        def description(self):
            return "Number of folu message transmit"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_8610 + $pid"
            
        def startAddress(self):
            return 0x00008610
            
        def endAddress(self):
            return 0xffffffff

        class _folutx_cnt40(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "folutx_cnt40"
            
            def description(self):
                return "number folu packet at Tx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["folutx_cnt40"] = _AF6CCI0012_RD_PTP._upen_tx_folu_cnt40_r2c._folutx_cnt40()
            return allFields

    class _upen_tx_folu_cnt40_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter Folu Transmit 40G PORT"
    
        def description(self):
            return "Number of folu message transmit"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_8650 + $pid"
            
        def startAddress(self):
            return 0x00008650
            
        def endAddress(self):
            return 0xffffffff

        class _folutx_cnt40(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "folutx_cnt40"
            
            def description(self):
                return "number folu packet at Tx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["folutx_cnt40"] = _AF6CCI0012_RD_PTP._upen_tx_folu_cnt40_ro._folutx_cnt40()
            return allFields

    class _upen_tx_dreq_cnt40_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter dreq transmit"
    
        def description(self):
            return "Number of dreq message transmit"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_8620 + $pid"
            
        def startAddress(self):
            return 0x00008620
            
        def endAddress(self):
            return 0xffffffff

        class _dreqtx_cnt40(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dreqtx_cnt40"
            
            def description(self):
                return "number dreq packet at Tx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dreqtx_cnt40"] = _AF6CCI0012_RD_PTP._upen_tx_dreq_cnt40_r2c._dreqtx_cnt40()
            return allFields

    class _upen_tx_dreq_cnt40_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter dreq transmit"
    
        def description(self):
            return "Number of dreq message transmit"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_8660 + $pid"
            
        def startAddress(self):
            return 0x00008660
            
        def endAddress(self):
            return 0xffffffff

        class _dreqtx_cnt40(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dreqtx_cnt40"
            
            def description(self):
                return "number dreq packet at Tx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dreqtx_cnt40"] = _AF6CCI0012_RD_PTP._upen_tx_dreq_cnt40_ro._dreqtx_cnt40()
            return allFields

    class _upen_tx_dres_cnt40_r2c(AtRegister.AtRegister):
        def name(self):
            return "Counter dres transmit"
    
        def description(self):
            return "Number of dres message transmit"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_8630 + $pid"
            
        def startAddress(self):
            return 0x00008630
            
        def endAddress(self):
            return 0xffffffff

        class _drestx_cnt40(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "drestx_cnt40"
            
            def description(self):
                return "number dres packet at Tx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["drestx_cnt40"] = _AF6CCI0012_RD_PTP._upen_tx_dres_cnt40_r2c._drestx_cnt40()
            return allFields

    class _upen_tx_dres_cnt40_ro(AtRegister.AtRegister):
        def name(self):
            return "Counter dres transmit"
    
        def description(self):
            return "Number of dres message transmit"
            
        def width(self):
            return 32
        
        def type(self):
            return "Counter"
            
        def fomular(self):
            return "0x00_8670 + $pid"
            
        def startAddress(self):
            return 0x00008670
            
        def endAddress(self):
            return 0xffffffff

        class _drestx_cnt40(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "drestx_cnt40"
            
            def description(self):
                return "number dres packet at Tx"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["drestx_cnt40"] = _AF6CCI0012_RD_PTP._upen_tx_dres_cnt40_ro._drestx_cnt40()
            return allFields

    class _upen_cpuque40(AtRegister.AtRegister):
        def name(self):
            return "Read CPU Queue"
    
        def description(self):
            return "Used to read CPU queue"
            
        def width(self):
            return 128
        
        def type(self):
            return "status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008216
            
        def endAddress(self):
            return 0xffffffff

        class _typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 101
                
            def startBit(self):
                return 100
        
            def name(self):
                return "typ"
            
            def description(self):
                return "typ of ptp packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _seqid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 99
                
            def startBit(self):
                return 84
        
            def name(self):
                return "seqid"
            
            def description(self):
                return "sequence of packet from sequenceId field"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _egr_pid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 83
                
            def startBit(self):
                return 80
        
            def name(self):
                return "egr_pid"
            
            def description(self):
                return "egress port id"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _egr_tmr_tod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 79
                
            def startBit(self):
                return 0
        
            def name(self):
                return "egr_tmr_tod"
            
            def description(self):
                return "egress timer tod"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["typ"] = _AF6CCI0012_RD_PTP._upen_cpuque40._typ()
            allFields["seqid"] = _AF6CCI0012_RD_PTP._upen_cpuque40._seqid()
            allFields["egr_pid"] = _AF6CCI0012_RD_PTP._upen_cpuque40._egr_pid()
            allFields["egr_tmr_tod"] = _AF6CCI0012_RD_PTP._upen_cpuque40._egr_tmr_tod()
            return allFields

    class _upen_int_en40(AtRegister.AtRegister):
        def name(self):
            return "Enb CPU Interupt"
    
        def description(self):
            return "Used to read CPU queue"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008200
            
        def endAddress(self):
            return 0xffffffff

        class _int_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "int_en"
            
            def description(self):
                return "set \"1\" ,interupt enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["int_en"] = _AF6CCI0012_RD_PTP._upen_int_en40._int_en()
            return allFields

    class _upen_int40(AtRegister.AtRegister):
        def name(self):
            return "Interupt CPU Queue"
    
        def description(self):
            return "Used to read CPU queue"
            
        def width(self):
            return 128
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008201
            
        def endAddress(self):
            return 0xffffffff

        class _int_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "int_en"
            
            def description(self):
                return "set\"1\" queue info ready ,write 1 to clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["int_en"] = _AF6CCI0012_RD_PTP._upen_int40._int_en()
            return allFields

    class _upen_sta40(AtRegister.AtRegister):
        def name(self):
            return "Interupt CPU Queue"
    
        def description(self):
            return "Used to read CPU queue"
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008202
            
        def endAddress(self):
            return 0xffffffff

        class _upen_sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 0
        
            def name(self):
                return "upen_sta"
            
            def description(self):
                return "current state of CPU queue, bit[17] \"1\" queue FULL, bit[16] \"1\" NOT empty, ready for cpu read, bit[15:0] queue lengh"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["upen_sta"] = _AF6CCI0012_RD_PTP._upen_sta40._upen_sta()
            return allFields

    class _upen_t1t3mode40g(AtRegister.AtRegister):
        def name(self):
            return "Config t1t3mode"
    
        def description(self):
            return "Used to config t1t3mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008017
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_t1t3mode40g_enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_t1t3mode40g_enable"
            
            def description(self):
                return "1: enable this mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_t1t3mode40g(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_t1t3mode40g"
            
            def description(self):
                return "0: send timestamp to cpu queue 1: send back ptp packet mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_t1t3mode40g_enable"] = _AF6CCI0012_RD_PTP._upen_t1t3mode40g._cfg_t1t3mode40g_enable()
            allFields["cfg_t1t3mode40g"] = _AF6CCI0012_RD_PTP._upen_t1t3mode40g._cfg_t1t3mode40g()
            return allFields

    class _upen_int_sta(AtRegister.AtRegister):
        def name(self):
            return "PTP interupt status"
    
        def description(self):
            return "Used to detect interupt event"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000080fe
            
        def endAddress(self):
            return 0xffffffff

        class _interupt_of_40G_BP_port(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "interupt_of_40G_BP_port"
            
            def description(self):
                return "1: active, Read CPU Queue interupt 40G BP"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _interupt_of_Face___port(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "interupt_of_Face___port"
            
            def description(self):
                return "1: active, Read CPU Queue interupt FACE"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["interupt_of_40G_BP_port"] = _AF6CCI0012_RD_PTP._upen_int_sta._interupt_of_40G_BP_port()
            allFields["interupt_of_Face___port"] = _AF6CCI0012_RD_PTP._upen_int_sta._interupt_of_Face___port()
            return allFields

    class _upen_face_vid_glb(AtRegister.AtRegister):
        def name(self):
            return "config vlan id global"
    
        def description(self):
            return "config vlan id global  for all port FACE"
            
        def width(self):
            return 12
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00_4021 + $num"
            
        def startAddress(self):
            return 0x00004021
            
        def endAddress(self):
            return 0x00004024

        class _cfg_face_vidglb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_face_vidglb"
            
            def description(self):
                return "global vlan id for FACE"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_face_vidglb"] = _AF6CCI0012_RD_PTP._upen_face_vid_glb._cfg_face_vidglb()
            return allFields

    class _upen_face_vid_glb_en(AtRegister.AtRegister):
        def name(self):
            return "config vlan id global enable"
    
        def description(self):
            return "config vlan id global enable matching for all port FACE"
            
        def width(self):
            return 20
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00_4020"
            
        def startAddress(self):
            return 0x00004020
            
        def endAddress(self):
            return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 1
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "for sw use"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_face_vidglb_ena(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_face_vidglb_ena"
            
            def description(self):
                return "\"1\" enable checking global vlanid"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["reserve"] = _AF6CCI0012_RD_PTP._upen_face_vid_glb_en._reserve()
            allFields["cfg_face_vidglb_ena"] = _AF6CCI0012_RD_PTP._upen_face_vid_glb_en._cfg_face_vidglb_ena()
            return allFields

    class _upen_face_vid_perport(AtRegister.AtRegister):
        def name(self):
            return "config vlan id per port"
    
        def description(self):
            return "config vlan id per port FACE"
            
        def width(self):
            return 26
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00_51A0 + $pid"
            
        def startAddress(self):
            return 0x000051a0
            
        def endAddress(self):
            return 0x000051af

        class _ena_vlanid2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "ena_vlanid2"
            
            def description(self):
                return "set \"1\" enable checking vlan id2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ena_vlanid1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "ena_vlanid1"
            
            def description(self):
                return "set \"1\" enable checking vlan id1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _vlanid2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 12
        
            def name(self):
                return "vlanid2"
            
            def description(self):
                return "perport vlan id1 for FACE"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _vlanid1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "vlanid1"
            
            def description(self):
                return "perport vlan id2 for FACE"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ena_vlanid2"] = _AF6CCI0012_RD_PTP._upen_face_vid_perport._ena_vlanid2()
            allFields["ena_vlanid1"] = _AF6CCI0012_RD_PTP._upen_face_vid_perport._ena_vlanid1()
            allFields["vlanid2"] = _AF6CCI0012_RD_PTP._upen_face_vid_perport._vlanid2()
            allFields["vlanid1"] = _AF6CCI0012_RD_PTP._upen_face_vid_perport._vlanid1()
            return allFields

    class _upen_bp_vid_glb(AtRegister.AtRegister):
        def name(self):
            return "config vlan40 id global"
    
        def description(self):
            return "config vlan id global  for all port 40G BACKPLAN"
            
        def width(self):
            return 13
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00_B040 + $num"
            
        def startAddress(self):
            return 0x0000b040
            
        def endAddress(self):
            return 0x0000b043

        class _cfg_bp_vidglb_enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "cfg_bp_vidglb_enable"
            
            def description(self):
                return "set \"1\" ( in case $num = 0, $num!= 0 dont care) enable  checking  bp global vlanid"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_bp_vidglb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_bp_vidglb"
            
            def description(self):
                return "global vlan id for 40G port"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_bp_vidglb_enable"] = _AF6CCI0012_RD_PTP._upen_bp_vid_glb._cfg_bp_vidglb_enable()
            allFields["cfg_bp_vidglb"] = _AF6CCI0012_RD_PTP._upen_bp_vid_glb._cfg_bp_vidglb()
            return allFields

    class _upen_bp_vid_perport(AtRegister.AtRegister):
        def name(self):
            return "config vlan40 id perport"
    
        def description(self):
            return "config vlan id per port 40G BACKPLAN"
            
        def width(self):
            return 26
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00_A000 + $opid"
            
        def startAddress(self):
            return 0x0000a000
            
        def endAddress(self):
            return 0x0000a00f

        class _ena_bpvlanid2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "ena_bpvlanid2"
            
            def description(self):
                return "set \"1\" enable checking vlan id2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ena_bpvlanid1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "ena_bpvlanid1"
            
            def description(self):
                return "set \"1\" enable checking vlan id1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_bp_vid2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 12
        
            def name(self):
                return "cfg_bp_vid2"
            
            def description(self):
                return "global vlan id2 for 40G port"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_bp_vid1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_bp_vid1"
            
            def description(self):
                return "global vlan id1 for 40G port"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ena_bpvlanid2"] = _AF6CCI0012_RD_PTP._upen_bp_vid_perport._ena_bpvlanid2()
            allFields["ena_bpvlanid1"] = _AF6CCI0012_RD_PTP._upen_bp_vid_perport._ena_bpvlanid1()
            allFields["cfg_bp_vid2"] = _AF6CCI0012_RD_PTP._upen_bp_vid_perport._cfg_bp_vid2()
            allFields["cfg_bp_vid1"] = _AF6CCI0012_RD_PTP._upen_bp_vid_perport._cfg_bp_vid1()
            return allFields

    class _upen_ge_ptp_dis_timestamping(AtRegister.AtRegister):
        def name(self):
            return "PTP Face Dis Timestaping"
    
        def description(self):
            return "used to disable ptp for TC update CF face 2 BP"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000020
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_timstap_dis_face_2_BP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_timstap_dis_face_2_BP"
            
            def description(self):
                return "'1' : disable update CF, '0': enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_timstap_dis_face_2_BP"] = _AF6CCI0012_RD_PTP._upen_ge_ptp_dis_timestamping._cfg_timstap_dis_face_2_BP()
            return allFields

    class _upen_bp_ptp_dis_timestamping(AtRegister.AtRegister):
        def name(self):
            return "PTP BP Dis Timestamping"
    
        def description(self):
            return "used to disable ptp for TC update CF face 2 BP"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008020
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_timstap_dis_BP_2_face(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_timstap_dis_BP_2_face"
            
            def description(self):
                return "'1' : disable update CF, '0': enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_timstap_dis_BP_2_face"] = _AF6CCI0012_RD_PTP._upen_bp_ptp_dis_timestamping._cfg_timstap_dis_BP_2_face()
            return allFields

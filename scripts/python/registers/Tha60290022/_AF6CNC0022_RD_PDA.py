import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0022_RD_PDA(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["ramjitbufcfg"] = _AF6CNC0022_RD_PDA._ramjitbufcfg()
        allRegisters["ramjitbufsta"] = _AF6CNC0022_RD_PDA._ramjitbufsta()
        allRegisters["ramreorcfg"] = _AF6CNC0022_RD_PDA._ramreorcfg()
        allRegisters["ramtdmmodecfg"] = _AF6CNC0022_RD_PDA._ramtdmmodecfg()
        allRegisters["ramtdmlkupcfg"] = _AF6CNC0022_RD_PDA._ramtdmlkupcfg()
        allRegisters["pda_hold_status"] = _AF6CNC0022_RD_PDA._pda_hold_status()
        allRegisters["ramlotdmprbsmon"] = _AF6CNC0022_RD_PDA._ramlotdmprbsmon()
        allRegisters["pda_config_ecc_crc_parity_control"] = _AF6CNC0022_RD_PDA._pda_config_ecc_crc_parity_control()
        allRegisters["pda_config_ecc_crc_parity_disable_control"] = _AF6CNC0022_RD_PDA._pda_config_ecc_crc_parity_disable_control()
        allRegisters["pda_config_ecc_crc_parity_sticky"] = _AF6CNC0022_RD_PDA._pda_config_ecc_crc_parity_sticky()
        allRegisters["rdha3_0_control"] = _AF6CNC0022_RD_PDA._rdha3_0_control()
        allRegisters["rdha7_4_control"] = _AF6CNC0022_RD_PDA._rdha7_4_control()
        allRegisters["rdha11_8_control"] = _AF6CNC0022_RD_PDA._rdha11_8_control()
        allRegisters["rdha15_12_control"] = _AF6CNC0022_RD_PDA._rdha15_12_control()
        allRegisters["rdha19_16_control"] = _AF6CNC0022_RD_PDA._rdha19_16_control()
        allRegisters["rdha23_20_control"] = _AF6CNC0022_RD_PDA._rdha23_20_control()
        allRegisters["rdha24data_control"] = _AF6CNC0022_RD_PDA._rdha24data_control()
        allRegisters["rdha_hold63_32"] = _AF6CNC0022_RD_PDA._rdha_hold63_32()
        allRegisters["rdindr_hold95_64"] = _AF6CNC0022_RD_PDA._rdindr_hold95_64()
        allRegisters["rdindr_hold127_96"] = _AF6CNC0022_RD_PDA._rdindr_hold127_96()
        allRegisters["ramlotdmsmallds0control"] = _AF6CNC0022_RD_PDA._ramlotdmsmallds0control()
        allRegisters["ramtdm192lkupcfg"] = _AF6CNC0022_RD_PDA._ramtdm192lkupcfg()
        allRegisters["ramtdmmode192cfg"] = _AF6CNC0022_RD_PDA._ramtdmmode192cfg()
        allRegisters["ramjitbufcentercfg"] = _AF6CNC0022_RD_PDA._ramjitbufcentercfg()
        allRegisters["ramjitbufcentersta"] = _AF6CNC0022_RD_PDA._ramjitbufcentersta()
        allRegisters["bert_gen_config"] = _AF6CNC0022_RD_PDA._bert_gen_config()
        allRegisters["bert_gen_force"] = _AF6CNC0022_RD_PDA._bert_gen_force()
        allRegisters["bert_gen_force_sing"] = _AF6CNC0022_RD_PDA._bert_gen_force_sing()
        allRegisters["goodbit_pen_tdm_gen_ro"] = _AF6CNC0022_RD_PDA._goodbit_pen_tdm_gen_ro()
        allRegisters["goodbit_pen_tdm_gen_r2c"] = _AF6CNC0022_RD_PDA._goodbit_pen_tdm_gen_r2c()
        allRegisters["bert_monpsn_config"] = _AF6CNC0022_RD_PDA._bert_monpsn_config()
        allRegisters["goodbit_pen_psn_mon_r2c"] = _AF6CNC0022_RD_PDA._goodbit_pen_psn_mon_r2c()
        allRegisters["goodbit_pen_psn_mon_ro"] = _AF6CNC0022_RD_PDA._goodbit_pen_psn_mon_ro()
        allRegisters["errorbit_pen_psn_mon_r2c"] = _AF6CNC0022_RD_PDA._errorbit_pen_psn_mon_r2c()
        allRegisters["errorbit_pen_psn_mon_ro"] = _AF6CNC0022_RD_PDA._errorbit_pen_psn_mon_ro()
        allRegisters["lostbit_pen_psn_mon_r2c"] = _AF6CNC0022_RD_PDA._lostbit_pen_psn_mon_r2c()
        allRegisters["lostbit_pen_psn_mon_ro"] = _AF6CNC0022_RD_PDA._lostbit_pen_psn_mon_ro()
        allRegisters["lostbit_pen_psn_mon_ro"] = _AF6CNC0022_RD_PDA._lostbit_pen_psn_mon_ro()
        allRegisters["lostbit_pen_psn_mon_ro"] = _AF6CNC0022_RD_PDA._lostbit_pen_psn_mon_ro()
        return allRegisters

    class _ramjitbufcfg(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA Jitter Buffer Control"
    
        def description(self):
            return "This register configures jitter buffer parameters per pseudo-wire HDL_PATH: rtljitbuf.ramjitbufcfg.ram.ram[$PWID]"
            
        def width(self):
            return 58
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00010000 +  PWID"
            
        def startAddress(self):
            return 0x00010000
            
        def endAddress(self):
            return 0xffffffff

        class _PwLowDs0Mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 57
                
            def startBit(self):
                return 57
        
            def name(self):
                return "PwLowDs0Mode"
            
            def description(self):
                return "Pseudo-wire CES Low DS0 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwCEPMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 56
                
            def startBit(self):
                return 56
        
            def name(self):
                return "PwCEPMode"
            
            def description(self):
                return "Pseudo-wire CEP mode indication"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwHoLoOc48Id(AtRegister.AtRegisterField):
            def stopBit(self):
                return 55
                
            def startBit(self):
                return 53
        
            def name(self):
                return "PwHoLoOc48Id"
            
            def description(self):
                return "Indicate 8x Hi order OC48 or 8x Low order OC48 slice"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwTdmLineId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 52
                
            def startBit(self):
                return 42
        
            def name(self):
                return "PwTdmLineId"
            
            def description(self):
                return "Pseudo-wire(PW) corresponding TDM line ID. If the PW belong to Low order path, this is the OC48 TDM line ID that is using in Lo CDR,PDH and MAP. If the PW belong to Hi order CEP path, this is the OC48 master STS ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwEparEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 41
                
            def startBit(self):
                return 41
        
            def name(self):
                return "PwEparEn"
            
            def description(self):
                return "Pseudo-wire EPAR timing mode enable 1: Enable EPAR timing mode 0: Disable EPAR timing mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwHiLoPathInd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 40
                
            def startBit(self):
                return 40
        
            def name(self):
                return "PwHiLoPathInd"
            
            def description(self):
                return "Pseudo-wire belong to Hi-order or Lo-order path 1: Pseudo-wire belong to Hi-order path 0: Pseudo-wire belong to Lo-order path"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwPayloadLen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 39
                
            def startBit(self):
                return 26
        
            def name(self):
                return "PwPayloadLen"
            
            def description(self):
                return "TDM Payload of pseudo-wire in byte unit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PdvSizeInPkUnit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PdvSizeInPkUnit"
            
            def description(self):
                return "Pdv size in packet unit. This parameter is to prevent packet delay variation(PDV) from PSN. PdvSizePk is packet delay variation measured in packet unit. The formula is as below: PdvSizeInPkUnit = (PwSpeedinKbps * PdvInUsUnit)/(8000*PwPayloadLen) + ((PwSpeedInKbps * PdvInUsus)%(8000*PwPayloadLen) !=0)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _JitBufSizeInPkUnit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 0
        
            def name(self):
                return "JitBufSizeInPkUnit"
            
            def description(self):
                return "Jitter buffer size in packet unit. This parameter is mostly double of PdvSizeInPkUnit. The formula is as below: JitBufSizeInPkUnit = (PwSpeedinKbps * JitBufInUsUnit)/(8000*PwPayloadLen) + ((PwSpeedInKbps * JitBufInUsus)%(8000*PwPayloadLen) !=0)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwLowDs0Mode"] = _AF6CNC0022_RD_PDA._ramjitbufcfg._PwLowDs0Mode()
            allFields["PwCEPMode"] = _AF6CNC0022_RD_PDA._ramjitbufcfg._PwCEPMode()
            allFields["PwHoLoOc48Id"] = _AF6CNC0022_RD_PDA._ramjitbufcfg._PwHoLoOc48Id()
            allFields["PwTdmLineId"] = _AF6CNC0022_RD_PDA._ramjitbufcfg._PwTdmLineId()
            allFields["PwEparEn"] = _AF6CNC0022_RD_PDA._ramjitbufcfg._PwEparEn()
            allFields["PwHiLoPathInd"] = _AF6CNC0022_RD_PDA._ramjitbufcfg._PwHiLoPathInd()
            allFields["PwPayloadLen"] = _AF6CNC0022_RD_PDA._ramjitbufcfg._PwPayloadLen()
            allFields["PdvSizeInPkUnit"] = _AF6CNC0022_RD_PDA._ramjitbufcfg._PdvSizeInPkUnit()
            allFields["JitBufSizeInPkUnit"] = _AF6CNC0022_RD_PDA._ramjitbufcfg._JitBufSizeInPkUnit()
            return allFields

    class _ramjitbufsta(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA Jitter Buffer Status"
    
        def description(self):
            return "This register shows jitter buffer status per pseudo-wire"
            
        def width(self):
            return 55
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00014000 +  PWID"
            
        def startAddress(self):
            return 0x00014000
            
        def endAddress(self):
            return 0xffffffff

        class _JitBufHwComSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 54
                
            def startBit(self):
                return 20
        
            def name(self):
                return "JitBufHwComSta"
            
            def description(self):
                return "Harware debug only status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _JitBufNumPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 4
        
            def name(self):
                return "JitBufNumPk"
            
            def description(self):
                return "Current number of packet in jitter buffer"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _JitBufFull(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "JitBufFull"
            
            def description(self):
                return "Jitter Buffer full status"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _JitBufState(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "JitBufState"
            
            def description(self):
                return "Jitter buffer state machine status 0: START 1: FILL 2: READY 3: READ 4: LOST 5: NEAR_EMPTY Others: NOT VALID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["JitBufHwComSta"] = _AF6CNC0022_RD_PDA._ramjitbufsta._JitBufHwComSta()
            allFields["JitBufNumPk"] = _AF6CNC0022_RD_PDA._ramjitbufsta._JitBufNumPk()
            allFields["JitBufFull"] = _AF6CNC0022_RD_PDA._ramjitbufsta._JitBufFull()
            allFields["JitBufState"] = _AF6CNC0022_RD_PDA._ramjitbufsta._JitBufState()
            return allFields

    class _ramreorcfg(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA Reorder Control"
    
        def description(self):
            return "This register configures reorder parameters per pseudo-wire"
            
        def width(self):
            return 26
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00020000 +  PWID"
            
        def startAddress(self):
            return 0x00020000
            
        def endAddress(self):
            return 0xffffffff

        class _PwSetLofsInPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PwSetLofsInPk"
            
            def description(self):
                return "Number of consecutive lost packet to declare lost of packet state"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwSetLopsInMsec(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PwSetLopsInMsec"
            
            def description(self):
                return "Number of empty time to declare lost of packet synchronization"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwReorEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PwReorEn"
            
            def description(self):
                return "Set 1 to enable reorder"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwReorTimeout(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PwReorTimeout"
            
            def description(self):
                return "Reorder timeout in 512us unit to detect lost packets. The formula is as below: ReorTimeout = ((Min(31,PdvSizeInPk) * PwPayloadLen * 16)/PwSpeedInKbps"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwSetLofsInPk"] = _AF6CNC0022_RD_PDA._ramreorcfg._PwSetLofsInPk()
            allFields["PwSetLopsInMsec"] = _AF6CNC0022_RD_PDA._ramreorcfg._PwSetLopsInMsec()
            allFields["PwReorEn"] = _AF6CNC0022_RD_PDA._ramreorcfg._PwReorEn()
            allFields["PwReorTimeout"] = _AF6CNC0022_RD_PDA._ramreorcfg._PwReorTimeout()
            return allFields

    class _ramtdmmodecfg(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA TDM mode Control"
    
        def description(self):
            return "This register configure TDM mode for interworking between Pseudowire and Lo TDM"
            
        def width(self):
            return 29
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00030000 + $Oc48ID + $TdmOc48Pwid*8"
            
        def startAddress(self):
            return 0x00030000
            
        def endAddress(self):
            return 0xffffffff

        class _PDARDIOff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "PDARDIOff"
            
            def description(self):
                return "Set 1 to disable sending RDI from CES PW to TDM in case Mbit/Rbit of CESoP PW"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 23
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "reserved"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _PDALbitRepMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "PDALbitRepMode"
            
            def description(self):
                return "Mode to replace Lbit packet 0: Replace by AIS (default) 1: Replace by a configuration idle code Replace by a configuration idle code"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAIdleCode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 14
        
            def name(self):
                return "PDAIdleCode"
            
            def description(self):
                return "Idle pattern to replace data in case of Lost/Lbit packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAAisOff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PDAAisOff"
            
            def description(self):
                return "Set 1 to disable sending AIS from CES PW to TDM in case Lbit or Lost packet replace AIS"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDARepMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PDARepMode"
            
            def description(self):
                return "Mode to replace lost packet 0: Replace by replaying previous good packet (often for voice or video application) 1: Replace by AIS (default) 2: Replace by a configuration idle code Replace by a configuration idle code"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAStsId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PDAStsId"
            
            def description(self):
                return "Used in DS3/E3 SAToP  and TU3/VC3/Vc4/VC4-4C/VC4-16C CEP basic, per slice48 corresponding master STSID of of PW circuit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDASigType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PDASigType"
            
            def description(self):
                return "TDM Payload De-Assembler signal type 0: E3,DS3,TU3,VC3 1: VC4 2: VC4-4C 3: VC4-8C,Vc4-16C 4: VC11,VC12,E1,T1,NxDS0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDAMode"
            
            def description(self):
                return "TDM Payload De-Assembler modes 0: DS1/E1 SAToP 1: CESoP without CAS 2: CEP 3: reserved"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDARDIOff"] = _AF6CNC0022_RD_PDA._ramtdmmodecfg._PDARDIOff()
            allFields["Reserved"] = _AF6CNC0022_RD_PDA._ramtdmmodecfg._Reserved()
            allFields["PDALbitRepMode"] = _AF6CNC0022_RD_PDA._ramtdmmodecfg._PDALbitRepMode()
            allFields["PDAIdleCode"] = _AF6CNC0022_RD_PDA._ramtdmmodecfg._PDAIdleCode()
            allFields["PDAAisOff"] = _AF6CNC0022_RD_PDA._ramtdmmodecfg._PDAAisOff()
            allFields["PDARepMode"] = _AF6CNC0022_RD_PDA._ramtdmmodecfg._PDARepMode()
            allFields["PDAStsId"] = _AF6CNC0022_RD_PDA._ramtdmmodecfg._PDAStsId()
            allFields["PDASigType"] = _AF6CNC0022_RD_PDA._ramtdmmodecfg._PDASigType()
            allFields["PDAMode"] = _AF6CNC0022_RD_PDA._ramtdmmodecfg._PDAMode()
            return allFields

    class _ramtdmlkupcfg(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA TDM Look Up Control"
    
        def description(self):
            return "This register configure lookup from TDM PWID to global 10752 PWID"
            
        def width(self):
            return 15
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00040000 + $Oc48ID + $TdmOc48Pwid*8"
            
        def startAddress(self):
            return 0x00040000
            
        def endAddress(self):
            return 0xffffffff

        class _PwID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PwID"
            
            def description(self):
                return "Flat 10752 CES PWID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwLkEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PwLkEnable"
            
            def description(self):
                return "Enable Lookup"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwID"] = _AF6CNC0022_RD_PDA._ramtdmlkupcfg._PwID()
            allFields["PwLkEnable"] = _AF6CNC0022_RD_PDA._ramtdmlkupcfg._PwLkEnable()
            return allFields

    class _pda_hold_status(AtRegister.AtRegister):
        def name(self):
            return "PDA Hold Register Status"
    
        def description(self):
            return "This register using for hold remain that more than 128bits"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000000 +  HID"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x00000002

        class _PdaHoldStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PdaHoldStatus"
            
            def description(self):
                return "Hold 32bits"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PdaHoldStatus"] = _AF6CNC0022_RD_PDA._pda_hold_status._PdaHoldStatus()
            return allFields

    class _ramlotdmprbsmon(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA per OC48 Low Order TDM PRBS J1 V5 Monitor Status"
    
        def description(self):
            return "This register show the PRBS monitor status after PDA"
            
        def width(self):
            return 18
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00061000 + Oc48ID*8192 + TdmOc48PwID"
            
        def startAddress(self):
            return 0x00061000
            
        def endAddress(self):
            return 0x0006f53f

        class _PDAPrbsB3V5Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PDAPrbsB3V5Err"
            
            def description(self):
                return "PDA PRBS or B3 or V5 monitor error"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAPrbsSyncSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PDAPrbsSyncSta"
            
            def description(self):
                return "PDA PRBS sync status"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAPrbsValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDAPrbsValue"
            
            def description(self):
                return "PDA PRBS monitor value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDAPrbsB3V5Err"] = _AF6CNC0022_RD_PDA._ramlotdmprbsmon._PDAPrbsB3V5Err()
            allFields["PDAPrbsSyncSta"] = _AF6CNC0022_RD_PDA._ramlotdmprbsmon._PDAPrbsSyncSta()
            allFields["PDAPrbsValue"] = _AF6CNC0022_RD_PDA._ramlotdmprbsmon._PDAPrbsValue()
            return allFields

    class _pda_config_ecc_crc_parity_control(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA ECC CRC Parity Control"
    
        def description(self):
            return "This register configures PDA ECC CRC and Parity."
            
        def width(self):
            return 7
        
        def type(self):
            return "Config|Enable"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000008
            
        def endAddress(self):
            return 0xffffffff

        class _PDAForceEccCor(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PDAForceEccCor"
            
            def description(self):
                return "PDA force link list Ecc error correctable   1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAForceEccErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PDAForceEccErr"
            
            def description(self):
                return "PDA force link list Ecc error noncorrectable   1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAForceCrcErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PDAForceCrcErr"
            
            def description(self):
                return "PDA force data CRC error  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAForceLoTdmParErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PDAForceLoTdmParErr"
            
            def description(self):
                return "Pseudowire PDA Low Order TDM mode Control force parity error  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAForceTdmLkParErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PDAForceTdmLkParErr"
            
            def description(self):
                return "Pseudowire PDA Lo and Ho TDM Look Up Control force parity error  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAForceJitBufParErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PDAForceJitBufParErr"
            
            def description(self):
                return "Pseudowire PDA Jitter Buffer Control force parity error  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAForceReorderParErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDAForceReorderParErr"
            
            def description(self):
                return "Pseudowire PDA Reorder Control force parity error  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDAForceEccCor"] = _AF6CNC0022_RD_PDA._pda_config_ecc_crc_parity_control._PDAForceEccCor()
            allFields["PDAForceEccErr"] = _AF6CNC0022_RD_PDA._pda_config_ecc_crc_parity_control._PDAForceEccErr()
            allFields["PDAForceCrcErr"] = _AF6CNC0022_RD_PDA._pda_config_ecc_crc_parity_control._PDAForceCrcErr()
            allFields["PDAForceLoTdmParErr"] = _AF6CNC0022_RD_PDA._pda_config_ecc_crc_parity_control._PDAForceLoTdmParErr()
            allFields["PDAForceTdmLkParErr"] = _AF6CNC0022_RD_PDA._pda_config_ecc_crc_parity_control._PDAForceTdmLkParErr()
            allFields["PDAForceJitBufParErr"] = _AF6CNC0022_RD_PDA._pda_config_ecc_crc_parity_control._PDAForceJitBufParErr()
            allFields["PDAForceReorderParErr"] = _AF6CNC0022_RD_PDA._pda_config_ecc_crc_parity_control._PDAForceReorderParErr()
            return allFields

    class _pda_config_ecc_crc_parity_disable_control(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA ECC CRC Parity Disable Control"
    
        def description(self):
            return "This register configures PDA ECC CRC and Parity Disable."
            
        def width(self):
            return 7
        
        def type(self):
            return "Config|Enable"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000009
            
        def endAddress(self):
            return 0xffffffff

        class _PDADisableEccCor(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PDADisableEccCor"
            
            def description(self):
                return "PDA disable link list Ecc error correctable   1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDADisableEccErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PDADisableEccErr"
            
            def description(self):
                return "PDA disable link list Ecc error noncorrectable   1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDADisableCrcErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PDADisableCrcErr"
            
            def description(self):
                return "PDA disable data CRC error  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDADisableLoTdmParErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PDADisableLoTdmParErr"
            
            def description(self):
                return "Pseudowire PDA Low Order TDM mode Control disable parity error  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDADisableTdmLkParErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PDADisableTdmLkParErr"
            
            def description(self):
                return "Pseudowire PDA Lo and Ho TDM Look Up Control disable parity error  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDADisableJitBufParErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PDADisableJitBufParErr"
            
            def description(self):
                return "Pseudowire PDA Jitter Buffer Control disable parity error  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDADisableReorderParErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDADisableReorderParErr"
            
            def description(self):
                return "Pseudowire PDA Reorder Control disable parity error  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDADisableEccCor"] = _AF6CNC0022_RD_PDA._pda_config_ecc_crc_parity_disable_control._PDADisableEccCor()
            allFields["PDADisableEccErr"] = _AF6CNC0022_RD_PDA._pda_config_ecc_crc_parity_disable_control._PDADisableEccErr()
            allFields["PDADisableCrcErr"] = _AF6CNC0022_RD_PDA._pda_config_ecc_crc_parity_disable_control._PDADisableCrcErr()
            allFields["PDADisableLoTdmParErr"] = _AF6CNC0022_RD_PDA._pda_config_ecc_crc_parity_disable_control._PDADisableLoTdmParErr()
            allFields["PDADisableTdmLkParErr"] = _AF6CNC0022_RD_PDA._pda_config_ecc_crc_parity_disable_control._PDADisableTdmLkParErr()
            allFields["PDADisableJitBufParErr"] = _AF6CNC0022_RD_PDA._pda_config_ecc_crc_parity_disable_control._PDADisableJitBufParErr()
            allFields["PDADisableReorderParErr"] = _AF6CNC0022_RD_PDA._pda_config_ecc_crc_parity_disable_control._PDADisableReorderParErr()
            return allFields

    class _pda_config_ecc_crc_parity_sticky(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA ECC CRC Parity Sticky"
    
        def description(self):
            return "This register configures PDA ECC CRC and Parity."
            
        def width(self):
            return 7
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000a
            
        def endAddress(self):
            return 0xffffffff

        class _PDAStickyEccCor(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PDAStickyEccCor"
            
            def description(self):
                return "PDA sticky link list Ecc error correctable   1: Set  0: Clear"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAStickyEccErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PDAStickyEccErr"
            
            def description(self):
                return "PDA sticky link list Ecc error noncorrectable   1: Set  0: Clear"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAStickyCrcErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PDAStickyCrcErr"
            
            def description(self):
                return "PDA sticky data CRC error  1: Set  0: Clear"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAStickyLoTdmParErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PDAStickyLoTdmParErr"
            
            def description(self):
                return "Pseudowire PDA Low Order TDM mode Control sticky parity error  1: Set  0: Clear"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAStickyTdmLkParErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PDAStickyTdmLkParErr"
            
            def description(self):
                return "Pseudowire PDA Lo and Ho TDM Look Up Control sticky parity error  1: Set  0: Clear"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAStickyJitBufParErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PDAStickyJitBufParErr"
            
            def description(self):
                return "Pseudowire PDA Jitter Buffer Control sticky parity error  1: Set  0: Clear"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAStickyReorderParErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDAStickyReorderParErr"
            
            def description(self):
                return "Pseudowire PDA Reorder Control sticky parity error  1: Set  0: Clear"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDAStickyEccCor"] = _AF6CNC0022_RD_PDA._pda_config_ecc_crc_parity_sticky._PDAStickyEccCor()
            allFields["PDAStickyEccErr"] = _AF6CNC0022_RD_PDA._pda_config_ecc_crc_parity_sticky._PDAStickyEccErr()
            allFields["PDAStickyCrcErr"] = _AF6CNC0022_RD_PDA._pda_config_ecc_crc_parity_sticky._PDAStickyCrcErr()
            allFields["PDAStickyLoTdmParErr"] = _AF6CNC0022_RD_PDA._pda_config_ecc_crc_parity_sticky._PDAStickyLoTdmParErr()
            allFields["PDAStickyTdmLkParErr"] = _AF6CNC0022_RD_PDA._pda_config_ecc_crc_parity_sticky._PDAStickyTdmLkParErr()
            allFields["PDAStickyJitBufParErr"] = _AF6CNC0022_RD_PDA._pda_config_ecc_crc_parity_sticky._PDAStickyJitBufParErr()
            allFields["PDAStickyReorderParErr"] = _AF6CNC0022_RD_PDA._pda_config_ecc_crc_parity_sticky._PDAStickyReorderParErr()
            return allFields

    class _rdha3_0_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit3_0 Control"
    
        def description(self):
            return "This register is used to send HA read address bit3_0 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x01000 + HaAddr3_0"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr3_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr3_0"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr3_0"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr3_0"] = _AF6CNC0022_RD_PDA._rdha3_0_control._ReadAddr3_0()
            return allFields

    class _rdha7_4_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit7_4 Control"
    
        def description(self):
            return "This register is used to send HA read address bit7_4 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x01010 + HaAddr7_4"
            
        def startAddress(self):
            return 0x00001010
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr7_4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr7_4"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr7_4"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr7_4"] = _AF6CNC0022_RD_PDA._rdha7_4_control._ReadAddr7_4()
            return allFields

    class _rdha11_8_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit11_8 Control"
    
        def description(self):
            return "This register is used to send HA read address bit11_8 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x01020 + HaAddr11_8"
            
        def startAddress(self):
            return 0x00001020
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr11_8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr11_8"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr11_8"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr11_8"] = _AF6CNC0022_RD_PDA._rdha11_8_control._ReadAddr11_8()
            return allFields

    class _rdha15_12_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit15_12 Control"
    
        def description(self):
            return "This register is used to send HA read address bit15_12 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x01030 + HaAddr15_12"
            
        def startAddress(self):
            return 0x00001030
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr15_12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr15_12"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr15_12"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr15_12"] = _AF6CNC0022_RD_PDA._rdha15_12_control._ReadAddr15_12()
            return allFields

    class _rdha19_16_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit19_16 Control"
    
        def description(self):
            return "This register is used to send HA read address bit19_16 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x01040 + HaAddr19_16"
            
        def startAddress(self):
            return 0x00001040
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr19_16(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr19_16"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr19_16"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr19_16"] = _AF6CNC0022_RD_PDA._rdha19_16_control._ReadAddr19_16()
            return allFields

    class _rdha23_20_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit23_20 Control"
    
        def description(self):
            return "This register is used to send HA read address bit23_20 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x01050 + HaAddr23_20"
            
        def startAddress(self):
            return 0x00001050
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr23_20(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr23_20"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr23_20"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr23_20"] = _AF6CNC0022_RD_PDA._rdha23_20_control._ReadAddr23_20()
            return allFields

    class _rdha24data_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit24 and Data Control"
    
        def description(self):
            return "This register is used to send HA read address bit24 to HA engine to read data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x01060 + HaAddr24"
            
        def startAddress(self):
            return 0x00001060
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData31_0"
            
            def description(self):
                return "HA read data bit31_0"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData31_0"] = _AF6CNC0022_RD_PDA._rdha24data_control._ReadHaData31_0()
            return allFields

    class _rdha_hold63_32(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data63_32"
    
        def description(self):
            return "This register is used to read HA dword2 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001070
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData63_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData63_32"
            
            def description(self):
                return "HA read data bit63_32"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData63_32"] = _AF6CNC0022_RD_PDA._rdha_hold63_32._ReadHaData63_32()
            return allFields

    class _rdindr_hold95_64(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data95_64"
    
        def description(self):
            return "This register is used to read HA dword3 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001071
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData95_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData95_64"
            
            def description(self):
                return "HA read data bit95_64"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData95_64"] = _AF6CNC0022_RD_PDA._rdindr_hold95_64._ReadHaData95_64()
            return allFields

    class _rdindr_hold127_96(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data127_96"
    
        def description(self):
            return "This register is used to read HA dword4 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001072
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData127_96(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData127_96"
            
            def description(self):
                return "HA read data bit127_96"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData127_96"] = _AF6CNC0022_RD_PDA._rdindr_hold127_96._ReadHaData127_96()
            return allFields

    class _ramlotdmsmallds0control(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA per OC48 Low Order TDM CESoP Small DS0 Control"
    
        def description(self):
            return "This register show the PRBS monitor status after PDA"
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00060000 + Oc48ID*8192 + TdmOc48PwID"
            
        def startAddress(self):
            return 0x00060000
            
        def endAddress(self):
            return 0x0006e53f

        class _PDASmallDs0En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDASmallDs0En"
            
            def description(self):
                return "PDA CESoP small DS0 enable 1: CESoP Pseodo-wire with NxDS0 <= 3 0: Other Pseodo-wire modes"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDASmallDs0En"] = _AF6CNC0022_RD_PDA._ramlotdmsmallds0control._PDASmallDs0En()
            return allFields

    class _ramtdm192lkupcfg(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA TDM OC192 Look Up Control"
    
        def description(self):
            return "This register configure lookup from TDM 192 ID to global 10752 PWID"
            
        def width(self):
            return 15
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00043FFB + $Oc192ID*4"
            
        def startAddress(self):
            return 0x00043ffb
            
        def endAddress(self):
            return 0x00043fff

        class _PwID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PwID"
            
            def description(self):
                return "Flat 10752 CES PWID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwLkEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PwLkEnable"
            
            def description(self):
                return "Enable Lookup"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwID"] = _AF6CNC0022_RD_PDA._ramtdm192lkupcfg._PwID()
            allFields["PwLkEnable"] = _AF6CNC0022_RD_PDA._ramtdm192lkupcfg._PwLkEnable()
            return allFields

    class _ramtdmmode192cfg(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA TDM OC192 Mode Control"
    
        def description(self):
            return "This register configure TDM mode enable for interworking between Pseudowire and OCN OC192 line"
            
        def width(self):
            return 28
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00033FFB + $Oc192ID*4"
            
        def startAddress(self):
            return 0x00033ffb
            
        def endAddress(self):
            return 0x00033fff

        class _PDAIdleCode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 14
        
            def name(self):
                return "PDAIdleCode"
            
            def description(self):
                return "Idle pattern to replace data in case of Lost/Lbit packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAAisRdiOff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PDAAisRdiOff"
            
            def description(self):
                return "Set 1 to disable sending AIS/Unequip/RDI from CES PW to TDM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDARepMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PDARepMode"
            
            def description(self):
                return "Mode to replace lost and Lbit packet 0: Replace by replaying previous good packet (often for voice or video application) 1: Replace by AIS (default) 2: Replace by a configuration idle code Replace by a configuration idle code"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAStsId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PDAStsId"
            
            def description(self):
                return "Unused for 192C CEP case"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDASigType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PDASigType"
            
            def description(self):
                return "Unused for 192C CEP case"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDAMode"
            
            def description(self):
                return "TDM Payload De-Assembler modes 0: DS1/E1 SAToP 1: CESoP without CAS 2: CEP 3: reserved"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDAIdleCode"] = _AF6CNC0022_RD_PDA._ramtdmmode192cfg._PDAIdleCode()
            allFields["PDAAisRdiOff"] = _AF6CNC0022_RD_PDA._ramtdmmode192cfg._PDAAisRdiOff()
            allFields["PDARepMode"] = _AF6CNC0022_RD_PDA._ramtdmmode192cfg._PDARepMode()
            allFields["PDAStsId"] = _AF6CNC0022_RD_PDA._ramtdmmode192cfg._PDAStsId()
            allFields["PDASigType"] = _AF6CNC0022_RD_PDA._ramtdmmode192cfg._PDASigType()
            allFields["PDAMode"] = _AF6CNC0022_RD_PDA._ramtdmmode192cfg._PDAMode()
            return allFields

    class _ramjitbufcentercfg(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA Jitter Buffer Center Control"
    
        def description(self):
            return "This register configures jitter buffer centering enable"
            
        def width(self):
            return 15
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _CenterPwid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 1
        
            def name(self):
                return "CenterPwid"
            
            def description(self):
                return "Pseudowire need to be centered"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CenterJbReq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CenterJbReq"
            
            def description(self):
                return "Set 1 to request center jitter buffer, clear 0 when finish centering"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CenterPwid"] = _AF6CNC0022_RD_PDA._ramjitbufcentercfg._CenterPwid()
            allFields["CenterJbReq"] = _AF6CNC0022_RD_PDA._ramjitbufcentercfg._CenterJbReq()
            return allFields

    class _ramjitbufcentersta(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA Jitter Buffer Center Status"
    
        def description(self):
            return "This register show jitter buffer centering status"
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0xffffffff

        class _CenterJbSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CenterJbSta"
            
            def description(self):
                return "Sw poll value 0 then start centering and poll again until 0 to finish center jitter buffer"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CenterJbSta"] = _AF6CNC0022_RD_PDA._ramjitbufcentersta._CenterJbSta()
            return allFields

    class _bert_gen_config(AtRegister.AtRegister):
        def name(self):
            return "BERT GEN CONTROL"
    
        def description(self):
            return "This register config mode for bert"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x7_0000 + $bid * 0x1000"
            
        def startAddress(self):
            return 0x00070000
            
        def endAddress(self):
            return 0x00071000

        class _BertInv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "BertInv"
            
            def description(self):
                return "Set 1 to select invert mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _BertEna(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "BertEna"
            
            def description(self):
                return "Set 1 to enable BERT"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _BertMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "BertMode"
            
            def description(self):
                return "\"0\" prbs15, \"1\" prbs23, \"2\" prbs31,\"3\" prbs20, \"4\" prbs20r, \"5\" all1, \"6\" all0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["BertInv"] = _AF6CNC0022_RD_PDA._bert_gen_config._BertInv()
            allFields["BertEna"] = _AF6CNC0022_RD_PDA._bert_gen_config._BertEna()
            allFields["BertMode"] = _AF6CNC0022_RD_PDA._bert_gen_config._BertMode()
            return allFields

    class _bert_gen_force(AtRegister.AtRegister):
        def name(self):
            return "BERT GEN CONTROL"
    
        def description(self):
            return "This register config rate for force error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x7_0001 + $bid * 0x1000"
            
        def startAddress(self):
            return 0x00070001
            
        def endAddress(self):
            return 0x00071001

        class _ber_rate(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ber_rate"
            
            def description(self):
                return "TxBerMd [31:0] == BER_level_val  : Bit Error Rate inserted to Pattern Generator [31:0] == 32'd0          :disable BER_level_val	BER_level 1000:        	BER 10e-3 10_000:      	BER 10e-4 100_000:     	BER 10e-5 1_000_000:   	BER 10e-6 10_000_000:  	BER 10e-7"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ber_rate"] = _AF6CNC0022_RD_PDA._bert_gen_force._ber_rate()
            return allFields

    class _bert_gen_force_sing(AtRegister.AtRegister):
        def name(self):
            return "BERT GEN CONTROL"
    
        def description(self):
            return "This register config rate for force error"
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x7_0004 + $bid * 0x1000"
            
        def startAddress(self):
            return 0x00070004
            
        def endAddress(self):
            return 0x00071004

        class _singe_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "singe_err"
            
            def description(self):
                return "sw write \"1\" to force, hw auto clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["singe_err"] = _AF6CNC0022_RD_PDA._bert_gen_force_sing._singe_err()
            return allFields

    class _goodbit_pen_tdm_gen_ro(AtRegister.AtRegister):
        def name(self):
            return "good counter tdm gen ro"
    
        def description(self):
            return "Counter"
            
        def width(self):
            return 34
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x7_0002 + $bid * 0x1000"
            
        def startAddress(self):
            return 0x00070002
            
        def endAddress(self):
            return 0x00071002

        class _bertgenro(AtRegister.AtRegisterField):
            def stopBit(self):
                return 33
                
            def startBit(self):
                return 0
        
            def name(self):
                return "bertgenro"
            
            def description(self):
                return "gen goodbit mode read only"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["bertgenro"] = _AF6CNC0022_RD_PDA._goodbit_pen_tdm_gen_ro._bertgenro()
            return allFields

    class _goodbit_pen_tdm_gen_r2c(AtRegister.AtRegister):
        def name(self):
            return "good counter tdm gen r2c"
    
        def description(self):
            return "Counter"
            
        def width(self):
            return 34
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x7_0003 + $bid * 0x1000"
            
        def startAddress(self):
            return 0x00070003
            
        def endAddress(self):
            return 0x00071003

        class _bertgenr2c(AtRegister.AtRegisterField):
            def stopBit(self):
                return 33
                
            def startBit(self):
                return 0
        
            def name(self):
                return "bertgenr2c"
            
            def description(self):
                return "gen goodbit mode read 2 clear"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["bertgenr2c"] = _AF6CNC0022_RD_PDA._goodbit_pen_tdm_gen_r2c._bertgenr2c()
            return allFields

    class _bert_monpsn_config(AtRegister.AtRegister):
        def name(self):
            return "BERT GEN CONTROL"
    
        def description(self):
            return "This register config mode for bert"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x7_2000 + $bid * 0x1000"
            
        def startAddress(self):
            return 0x00072000
            
        def endAddress(self):
            return 0x00073000

        class _Bert_Inv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Bert_Inv"
            
            def description(self):
                return "Set 1 to select invert mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Thr_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 7
        
            def name(self):
                return "Thr_Err"
            
            def description(self):
                return "Thrhold declare lost syn sta"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Thr_Syn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Thr_Syn"
            
            def description(self):
                return "Thrhold declare syn sta"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _BertEna(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "BertEna"
            
            def description(self):
                return "Set 1 to enable BERT"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _BertMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "BertMode"
            
            def description(self):
                return "\"0\" prbs15, \"1\" prbs23, \"2\" prbs31,\"3\" prbs20, \"4\" prbs20r, \"5\" all1, \"6\" all0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Bert_Inv"] = _AF6CNC0022_RD_PDA._bert_monpsn_config._Bert_Inv()
            allFields["Thr_Err"] = _AF6CNC0022_RD_PDA._bert_monpsn_config._Thr_Err()
            allFields["Thr_Syn"] = _AF6CNC0022_RD_PDA._bert_monpsn_config._Thr_Syn()
            allFields["BertEna"] = _AF6CNC0022_RD_PDA._bert_monpsn_config._BertEna()
            allFields["BertMode"] = _AF6CNC0022_RD_PDA._bert_monpsn_config._BertMode()
            return allFields

    class _goodbit_pen_psn_mon_r2c(AtRegister.AtRegister):
        def name(self):
            return "good counter psn  r2c"
    
        def description(self):
            return "Counter"
            
        def width(self):
            return 34
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x7_2001 + $bid * 0x1000"
            
        def startAddress(self):
            return 0x00072001
            
        def endAddress(self):
            return 0x00073001

        class _goodmonr2c(AtRegister.AtRegisterField):
            def stopBit(self):
                return 33
                
            def startBit(self):
                return 0
        
            def name(self):
                return "goodmonr2c"
            
            def description(self):
                return "mon goodbit mode read 2clear"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["goodmonr2c"] = _AF6CNC0022_RD_PDA._goodbit_pen_psn_mon_r2c._goodmonr2c()
            return allFields

    class _goodbit_pen_psn_mon_ro(AtRegister.AtRegister):
        def name(self):
            return "good counter psn  ro"
    
        def description(self):
            return "Counter"
            
        def width(self):
            return 34
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x7_2002 + $bid * 0x1000"
            
        def startAddress(self):
            return 0x00072002
            
        def endAddress(self):
            return 0x00073002

        class _goodmonro(AtRegister.AtRegisterField):
            def stopBit(self):
                return 33
                
            def startBit(self):
                return 0
        
            def name(self):
                return "goodmonro"
            
            def description(self):
                return "mon goodbit mode read only"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["goodmonro"] = _AF6CNC0022_RD_PDA._goodbit_pen_psn_mon_ro._goodmonro()
            return allFields

    class _errorbit_pen_psn_mon_r2c(AtRegister.AtRegister):
        def name(self):
            return "error counter psn  r2c"
    
        def description(self):
            return "Counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x7_2003 + $bid * 0x1000"
            
        def startAddress(self):
            return 0x00072003
            
        def endAddress(self):
            return 0x00073003

        class _errormonr2c(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "errormonr2c"
            
            def description(self):
                return "mon errorbit mode read 2clear"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["errormonr2c"] = _AF6CNC0022_RD_PDA._errorbit_pen_psn_mon_r2c._errormonr2c()
            return allFields

    class _errorbit_pen_psn_mon_ro(AtRegister.AtRegister):
        def name(self):
            return "error counter psn  ro"
    
        def description(self):
            return "Counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x7_2004 + $bid * 0x1000"
            
        def startAddress(self):
            return 0x00072004
            
        def endAddress(self):
            return 0x00073004

        class _errormonro(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "errormonro"
            
            def description(self):
                return "mon errorbit mode read only"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["errormonro"] = _AF6CNC0022_RD_PDA._errorbit_pen_psn_mon_ro._errormonro()
            return allFields

    class _lostbit_pen_psn_mon_r2c(AtRegister.AtRegister):
        def name(self):
            return "lost counter psn  r2c"
    
        def description(self):
            return "Counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x7_2005 + $bid * 0x1000"
            
        def startAddress(self):
            return 0x00072005
            
        def endAddress(self):
            return 0x00073005

        class _lostmonr2c(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lostmonr2c"
            
            def description(self):
                return "mon lostbit mode read 2clear"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lostmonr2c"] = _AF6CNC0022_RD_PDA._lostbit_pen_psn_mon_r2c._lostmonr2c()
            return allFields

    class _lostbit_pen_psn_mon_ro(AtRegister.AtRegister):
        def name(self):
            return "lost counter psn  ro"
    
        def description(self):
            return "Counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x7_2006 + $bid * 0x1000"
            
        def startAddress(self):
            return 0x00072006
            
        def endAddress(self):
            return 0x00073006

        class _lostmonro(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lostmonro"
            
            def description(self):
                return "mon lostbit mode read only"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lostmonro"] = _AF6CNC0022_RD_PDA._lostbit_pen_psn_mon_ro._lostmonro()
            return allFields

    class _lostbit_pen_psn_mon_ro(AtRegister.AtRegister):
        def name(self):
            return "lost counter psn  ro"
    
        def description(self):
            return "Status"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x7_2007 + $bid * 0x1000"
            
        def startAddress(self):
            return 0x00072007
            
        def endAddress(self):
            return 0x00073007

        class _staprbs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "staprbs"
            
            def description(self):
                return "\"3\" SYNC , other LOST"
            
            def type(self):
                return "R0"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["staprbs"] = _AF6CNC0022_RD_PDA._lostbit_pen_psn_mon_ro._staprbs()
            return allFields

    class _lostbit_pen_psn_mon_ro(AtRegister.AtRegister):
        def name(self):
            return "lost counter psn  ro"
    
        def description(self):
            return "Sticky"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x7_2008 + $bid * 0x1000"
            
        def startAddress(self):
            return 0x00072008
            
        def endAddress(self):
            return 0x00073008

        class _stickyprbs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stickyprbs"
            
            def description(self):
                return "\"1\" LOSTSYN"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["stickyprbs"] = _AF6CNC0022_RD_PDA._lostbit_pen_psn_mon_ro._stickyprbs()
            return allFields

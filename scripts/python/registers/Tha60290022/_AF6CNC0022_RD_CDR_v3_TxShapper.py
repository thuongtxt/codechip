import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0022_RD_CDR_v3_TxShapper(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["dcr_tx_eng_timing_ctrl"] = _AF6CNC0022_RD_CDR_v3_TxShapper._dcr_tx_eng_timing_ctrl()
        return allRegisters

    class _dcr_tx_eng_timing_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DCR Tx Engine Timing control"
    
        def description(self):
            return "This register is used to configure timing mode for per STS"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0010400 + OC48*4096 + STS*32 + TUG*4 + VT"
            
        def startAddress(self):
            return 0x00010400
            
        def endAddress(self):
            return 0xffffffff

        class _PktLen_bit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 19
        
            def name(self):
                return "PktLen_bit"
            
            def description(self):
                return "The payload packet length BIT, PktLen[2:0]"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 16
        
            def name(self):
                return "LineType"
            
            def description(self):
                return "Line type mode 0: DS1 1: E1 2: DS3 3: E3 4: VT15 5: VT2 6: STS1/TU3 7: Reserve"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PktLen_byte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PktLen_byte"
            
            def description(self):
                return "The payload packet length Byte, PktLen[17:3]"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PktLen_bit"] = _AF6CNC0022_RD_CDR_v3_TxShapper._dcr_tx_eng_timing_ctrl._PktLen_bit()
            allFields["LineType"] = _AF6CNC0022_RD_CDR_v3_TxShapper._dcr_tx_eng_timing_ctrl._LineType()
            allFields["PktLen_byte"] = _AF6CNC0022_RD_CDR_v3_TxShapper._dcr_tx_eng_timing_ctrl._PktLen_byte()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0022_RD_DCCK_GMII(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["dcckgmiicfg"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiicfg()
        allRegisters["dcckgmiidiagcfg"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiidiagcfg()
        allRegisters["dcckgmiistk"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiistk()
        allRegisters["dcckgmiitxpktcnt"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiitxpktcnt()
        allRegisters["dcckgmiitxbytecnt"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiitxbytecnt()
        allRegisters["dcckgmiitxfcserrcnt"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiitxfcserrcnt()
        allRegisters["dcckgmiirxpktcnt"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiirxpktcnt()
        allRegisters["dcckgmiirxbytecnt"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiirxbytecnt()
        allRegisters["dcckgmiirxfcserrcnt"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiirxfcserrcnt()
        allRegisters["ramdumtypecfg"] = _AF6CNC0022_RD_DCCK_GMII._ramdumtypecfg()
        allRegisters["ramdumpdata"] = _AF6CNC0022_RD_DCCK_GMII._ramdumpdata()
        return allRegisters

    class _dcckgmiicfg(AtRegister.AtRegister):
        def name(self):
            return "DCCK GMII Control"
    
        def description(self):
            return "This register configures parameters for DCCK GMII"
            
        def width(self):
            return 11
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _DcckGmiiLoopOut(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DcckGmiiLoopOut"
            
            def description(self):
                return "DCCK GMII Remote Loop Out"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DcckGmiiLoopIn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DcckGmiiLoopIn"
            
            def description(self):
                return "DCCK GMII Local Loop In"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DcckGmiiPreNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DcckGmiiPreNum"
            
            def description(self):
                return "DCCK GMII Preamble Number of Byte"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DcckGmiiIpgNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DcckGmiiIpgNum"
            
            def description(self):
                return "DCCK GMII Interpacket GAP Number of Byte"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DcckGmiiLoopOut"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiicfg._DcckGmiiLoopOut()
            allFields["DcckGmiiLoopIn"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiicfg._DcckGmiiLoopIn()
            allFields["DcckGmiiPreNum"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiicfg._DcckGmiiPreNum()
            allFields["DcckGmiiIpgNum"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiicfg._DcckGmiiIpgNum()
            return allFields

    class _dcckgmiidiagcfg(AtRegister.AtRegister):
        def name(self):
            return "DCCK GMII Diagnostic Enable Control"
    
        def description(self):
            return "This register configures parameters for DCCK GMII"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0xffffffff

        class _DcckGmiiDiagMaxLen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 18
        
            def name(self):
                return "DcckGmiiDiagMaxLen"
            
            def description(self):
                return "DCCK GMII Diagnostic Max length count from zero"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DcckGmiiDiagMinLen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DcckGmiiDiagMinLen"
            
            def description(self):
                return "DCCK GMII Diagnostic Min length count from zero"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DcckGmiiForceFcsErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DcckGmiiForceFcsErr"
            
            def description(self):
                return "DCCK GMII Force FCS error"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DcckGmiiDiagForceDatErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DcckGmiiDiagForceDatErr"
            
            def description(self):
                return "DCCK GMII Diagnostic Force Data error"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DcckGmiiDiagGenPkEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DcckGmiiDiagGenPkEn"
            
            def description(self):
                return "DCCK GMII Diagnostic Generate packet enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DcckGmiiDiagEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DcckGmiiDiagEn"
            
            def description(self):
                return "DCCK GMII Diagnostic enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DcckGmiiDiagMaxLen"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiidiagcfg._DcckGmiiDiagMaxLen()
            allFields["DcckGmiiDiagMinLen"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiidiagcfg._DcckGmiiDiagMinLen()
            allFields["DcckGmiiForceFcsErr"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiidiagcfg._DcckGmiiForceFcsErr()
            allFields["DcckGmiiDiagForceDatErr"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiidiagcfg._DcckGmiiDiagForceDatErr()
            allFields["DcckGmiiDiagGenPkEn"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiidiagcfg._DcckGmiiDiagGenPkEn()
            allFields["DcckGmiiDiagEn"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiidiagcfg._DcckGmiiDiagEn()
            return allFields

    class _dcckgmiistk(AtRegister.AtRegister):
        def name(self):
            return "DCCK GMII Datapath and Diagnostic sticky"
    
        def description(self):
            return "This register show alarms of DCCK GMII"
            
        def width(self):
            return 12
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000012
            
        def endAddress(self):
            return 0xffffffff

        class _DcckGmiiTxFiFoConvErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "DcckGmiiTxFiFoConvErr"
            
            def description(self):
                return "DCCK GMII Tx FIFO Convert Error"
            
            def type(self):
                return "RWC"
            
            def resetValue(self):
                return 0xffffffff

        class _DcckGmiiTxFcsErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DcckGmiiTxFcsErr"
            
            def description(self):
                return "DCCK GMII Tx FCS Error"
            
            def type(self):
                return "RWC"
            
            def resetValue(self):
                return 0xffffffff

        class _DcckGmiiTxLostSop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DcckGmiiTxLostSop"
            
            def description(self):
                return "DCCK GMII Tx Lost SOP"
            
            def type(self):
                return "RWC"
            
            def resetValue(self):
                return 0xffffffff

        class _DcckGmiiRxPreErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DcckGmiiRxPreErr"
            
            def description(self):
                return "DCCK GMII Rx Preamble Error"
            
            def type(self):
                return "RWC"
            
            def resetValue(self):
                return 0xffffffff

        class _DcckGmiiRxSfdErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "DcckGmiiRxSfdErr"
            
            def description(self):
                return "DCCK GMII Rx SFD Error"
            
            def type(self):
                return "RWC"
            
            def resetValue(self):
                return 0xffffffff

        class _DcckGmiiRxDvErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DcckGmiiRxDvErr"
            
            def description(self):
                return "DCCK GMII Rx DV Error"
            
            def type(self):
                return "RWC"
            
            def resetValue(self):
                return 0xffffffff

        class _DcckGmiiRxFcsErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DcckGmiiRxFcsErr"
            
            def description(self):
                return "DCCK GMII Rx FCS Error"
            
            def type(self):
                return "RWC"
            
            def resetValue(self):
                return 0xffffffff

        class _DcckGmiiRxErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DcckGmiiRxErr"
            
            def description(self):
                return "DCCK GMII Rx Error"
            
            def type(self):
                return "RWC"
            
            def resetValue(self):
                return 0xffffffff

        class _DcckGmiiRxFiFoConvErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DcckGmiiRxFiFoConvErr"
            
            def description(self):
                return "DCCK GMII Rx FIFO Convert Error"
            
            def type(self):
                return "RWC"
            
            def resetValue(self):
                return 0xffffffff

        class _DcckGmiiRxMinLenErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DcckGmiiRxMinLenErr"
            
            def description(self):
                return "DCCK GMII Rx Min Len Error"
            
            def type(self):
                return "RWC"
            
            def resetValue(self):
                return 0xffffffff

        class _DcckGmiiRxMaxLenErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DcckGmiiRxMaxLenErr"
            
            def description(self):
                return "DCCK GMII Rx Max Len Error"
            
            def type(self):
                return "RWC"
            
            def resetValue(self):
                return 0xffffffff

        class _DcckGmiiDiagRxDatErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DcckGmiiDiagRxDatErr"
            
            def description(self):
                return "DCCK GMII Diagnostic Rx Data Error"
            
            def type(self):
                return "RWC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DcckGmiiTxFiFoConvErr"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiistk._DcckGmiiTxFiFoConvErr()
            allFields["DcckGmiiTxFcsErr"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiistk._DcckGmiiTxFcsErr()
            allFields["DcckGmiiTxLostSop"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiistk._DcckGmiiTxLostSop()
            allFields["DcckGmiiRxPreErr"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiistk._DcckGmiiRxPreErr()
            allFields["DcckGmiiRxSfdErr"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiistk._DcckGmiiRxSfdErr()
            allFields["DcckGmiiRxDvErr"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiistk._DcckGmiiRxDvErr()
            allFields["DcckGmiiRxFcsErr"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiistk._DcckGmiiRxFcsErr()
            allFields["DcckGmiiRxErr"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiistk._DcckGmiiRxErr()
            allFields["DcckGmiiRxFiFoConvErr"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiistk._DcckGmiiRxFiFoConvErr()
            allFields["DcckGmiiRxMinLenErr"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiistk._DcckGmiiRxMinLenErr()
            allFields["DcckGmiiRxMaxLenErr"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiistk._DcckGmiiRxMaxLenErr()
            allFields["DcckGmiiDiagRxDatErr"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiistk._DcckGmiiDiagRxDatErr()
            return allFields

    class _dcckgmiitxpktcnt(AtRegister.AtRegister):
        def name(self):
            return "DCCK GMII Tx Packet Counter"
    
        def description(self):
            return "This register show GMII Tx Packet Counter"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _DcckGmiiTxPktCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DcckGmiiTxPktCnt"
            
            def description(self):
                return "DCCK GMII Tx Packet Counter"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DcckGmiiTxPktCnt"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiitxpktcnt._DcckGmiiTxPktCnt()
            return allFields

    class _dcckgmiitxbytecnt(AtRegister.AtRegister):
        def name(self):
            return "DCCK GMII Tx Packet Counter"
    
        def description(self):
            return "This register show GMII Tx Byte Counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _DcckGmiiTxByteCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DcckGmiiTxByteCnt"
            
            def description(self):
                return "DCCK GMII Tx Byte Counter"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DcckGmiiTxByteCnt"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiitxbytecnt._DcckGmiiTxByteCnt()
            return allFields

    class _dcckgmiitxfcserrcnt(AtRegister.AtRegister):
        def name(self):
            return "DCCK GMII Tx Packet FCS Error Counter"
    
        def description(self):
            return "This register show GMII Tx Packet FCS Error Counter"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _DcckGmiiTxPktFcsErrCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DcckGmiiTxPktFcsErrCnt"
            
            def description(self):
                return "DCCK GMII Tx Packet FCS Error Counter"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DcckGmiiTxPktFcsErrCnt"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiitxfcserrcnt._DcckGmiiTxPktFcsErrCnt()
            return allFields

    class _dcckgmiirxpktcnt(AtRegister.AtRegister):
        def name(self):
            return "DCCK GMII Rx Packet Counter"
    
        def description(self):
            return "This register show GMII Rx Packet Counter"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _DcckGmiiRxPktCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DcckGmiiRxPktCnt"
            
            def description(self):
                return "DCCK GMII Rx Packet Counter"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DcckGmiiRxPktCnt"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiirxpktcnt._DcckGmiiRxPktCnt()
            return allFields

    class _dcckgmiirxbytecnt(AtRegister.AtRegister):
        def name(self):
            return "DCCK GMII Rx Packet Counter"
    
        def description(self):
            return "This register show GMII Rx Byte Counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0xffffffff

        class _DcckGmiiRxByteCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DcckGmiiRxByteCnt"
            
            def description(self):
                return "DCCK GMII Rx Byte Counter"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DcckGmiiRxByteCnt"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiirxbytecnt._DcckGmiiRxByteCnt()
            return allFields

    class _dcckgmiirxfcserrcnt(AtRegister.AtRegister):
        def name(self):
            return "DCCK GMII Rx Packet FCS Error Counter"
    
        def description(self):
            return "This register show GMII Rx Packet FCS Error Counter"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000005
            
        def endAddress(self):
            return 0xffffffff

        class _DcckGmiiRxPktFcsErrCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DcckGmiiRxPktFcsErrCnt"
            
            def description(self):
                return "DCCK GMII Rx Packet FCS Error Counter"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DcckGmiiRxPktFcsErrCnt"] = _AF6CNC0022_RD_DCCK_GMII._dcckgmiirxfcserrcnt._DcckGmiiRxPktFcsErrCnt()
            return allFields

    class _ramdumtypecfg(AtRegister.AtRegister):
        def name(self):
            return "Dcck Gmii dump direction control"
    
        def description(self):
            return "This register configures direction to dump packet"
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000c00
            
        def endAddress(self):
            return 0xffffffff

        class _DirectionDump(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DirectionDump"
            
            def description(self):
                return "Tx or Rx MAC direction 1: Dump Tx DCCK GMII 0: Dump Rx DCCK GMII"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DirectionDump"] = _AF6CNC0022_RD_DCCK_GMII._ramdumtypecfg._DirectionDump()
            return allFields

    class _ramdumpdata(AtRegister.AtRegister):
        def name(self):
            return "Dcck Gmii dump data status"
    
        def description(self):
            return "This register shows data value to be dump, first entry always contain SOP"
            
        def width(self):
            return 19
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000800 +  entry"
            
        def startAddress(self):
            return 0x00000800
            
        def endAddress(self):
            return 0xffffffff

        class _DumpSop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "DumpSop"
            
            def description(self):
                return "Start of packet indication"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _DumpEoP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "DumpEoP"
            
            def description(self):
                return "End of packet indication"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _DumpNob(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "DumpNob"
            
            def description(self):
                return "Number of byte indication 0 means 1 byte"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DumpData(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DumpData"
            
            def description(self):
                return "Data value Bit15_8 is MSB"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DumpSop"] = _AF6CNC0022_RD_DCCK_GMII._ramdumpdata._DumpSop()
            allFields["DumpEoP"] = _AF6CNC0022_RD_DCCK_GMII._ramdumpdata._DumpEoP()
            allFields["DumpNob"] = _AF6CNC0022_RD_DCCK_GMII._ramdumpdata._DumpNob()
            allFields["DumpData"] = _AF6CNC0022_RD_DCCK_GMII._ramdumpdata._DumpData()
            return allFields

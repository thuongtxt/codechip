import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0011_RD_BERT_GEN(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["sel_ts_gen"] = _AF6CCI0011_RD_BERT_GEN._sel_ts_gen()
        allRegisters["sel_mon_tstdm"] = _AF6CCI0011_RD_BERT_GEN._sel_mon_tstdm()
        allRegisters["sel_mon_tspw"] = _AF6CCI0011_RD_BERT_GEN._sel_mon_tspw()
        allRegisters["sel_ho_bert_gen"] = _AF6CCI0011_RD_BERT_GEN._sel_ho_bert_gen()
        allRegisters["sel_bert_montdm"] = _AF6CCI0011_RD_BERT_GEN._sel_bert_montdm()
        allRegisters["sel_bert_monpw"] = _AF6CCI0011_RD_BERT_GEN._sel_bert_monpw()
        allRegisters["ctrl_pen_gen"] = _AF6CCI0011_RD_BERT_GEN._ctrl_pen_gen()
        allRegisters["ctrl_pen_montdm"] = _AF6CCI0011_RD_BERT_GEN._ctrl_pen_montdm()
        allRegisters["ctrl_pen_montdm"] = _AF6CCI0011_RD_BERT_GEN._ctrl_pen_montdm()
        allRegisters["ctrl_ber_pen"] = _AF6CCI0011_RD_BERT_GEN._ctrl_ber_pen()
        allRegisters["goodbit_ber_pen"] = _AF6CCI0011_RD_BERT_GEN._goodbit_ber_pen()
        allRegisters["sel_ho_bert_tdm_mon"] = _AF6CCI0011_RD_BERT_GEN._sel_ho_bert_tdm_mon()
        allRegisters["err_tdm_mon"] = _AF6CCI0011_RD_BERT_GEN._err_tdm_mon()
        allRegisters["lossbit_pen_tdm_mon"] = _AF6CCI0011_RD_BERT_GEN._lossbit_pen_tdm_mon()
        allRegisters["lossbit_pen_tdm_mon"] = _AF6CCI0011_RD_BERT_GEN._lossbit_pen_tdm_mon()
        allRegisters["goodbit_pen_pw_mon"] = _AF6CCI0011_RD_BERT_GEN._goodbit_pen_pw_mon()
        allRegisters["errbit_pen_pw_mon"] = _AF6CCI0011_RD_BERT_GEN._errbit_pen_pw_mon()
        allRegisters["lossbit_pen_pw_mon"] = _AF6CCI0011_RD_BERT_GEN._lossbit_pen_pw_mon()
        return allRegisters

    class _sel_ts_gen(AtRegister.AtRegister):
        def name(self):
            return "Sel Timeslot Gen"
    
        def description(self):
            return "The registers select 1id in line to gen bert data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8340 + engid + 2048*slice"
            
        def startAddress(self):
            return 0x00008340
            
        def endAddress(self):
            return 0x0000bb5f

        class _gen_tsen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "gen_tsen"
            
            def description(self):
                return "bit map indicase 32 timeslot,\"1\" : enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["gen_tsen"] = _AF6CCI0011_RD_BERT_GEN._sel_ts_gen._gen_tsen()
            return allFields

    class _sel_mon_tstdm(AtRegister.AtRegister):
        def name(self):
            return "Sel Timeslot Mon tdm"
    
        def description(self):
            return "The registers select 1id in line to gen bert data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8500 + engid + 2048*slice"
            
        def startAddress(self):
            return 0x00008500
            
        def endAddress(self):
            return 0x0000bd00

        class _tdm_tsen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tdm_tsen"
            
            def description(self):
                return "bit map indicase 32 timeslot,\"1\" : enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tdm_tsen"] = _AF6CCI0011_RD_BERT_GEN._sel_mon_tstdm._tdm_tsen()
            return allFields

    class _sel_mon_tspw(AtRegister.AtRegister):
        def name(self):
            return "Sel Timeslot Mon PW"
    
        def description(self):
            return "The registers select 1id in line to gen bert data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8500 + engid + 2048*slice"
            
        def startAddress(self):
            return 0x00008500
            
        def endAddress(self):
            return 0x0000bd00

        class _pw_tsen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pw_tsen"
            
            def description(self):
                return "bit map indicase 32 timeslot,\"1\" : enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pw_tsen"] = _AF6CCI0011_RD_BERT_GEN._sel_mon_tspw._pw_tsen()
            return allFields

    class _sel_ho_bert_gen(AtRegister.AtRegister):
        def name(self):
            return "Sel Ho Bert Gen"
    
        def description(self):
            return "The registers select id in line to gen bert data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000200
            
        def endAddress(self):
            return 0x0000021f

        class _gen_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "gen_en"
            
            def description(self):
                return "set \"1\" to enable bert gen"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _lineid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 11
        
            def name(self):
                return "lineid"
            
            def description(self):
                return "line id OC48"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _id(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "id"
            
            def description(self):
                return "TDM ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["gen_en"] = _AF6CCI0011_RD_BERT_GEN._sel_ho_bert_gen._gen_en()
            allFields["lineid"] = _AF6CCI0011_RD_BERT_GEN._sel_ho_bert_gen._lineid()
            allFields["id"] = _AF6CCI0011_RD_BERT_GEN._sel_ho_bert_gen._id()
            return allFields

    class _sel_bert_montdm(AtRegister.AtRegister):
        def name(self):
            return "Sel TDM Bert mon"
    
        def description(self):
            return "The registers select 1id in line to gen bert data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x0000001f

        class _tdmmon_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "tdmmon_en"
            
            def description(self):
                return "set \"1\" to enable bert gen"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _tdm_lineid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 11
        
            def name(self):
                return "tdm_lineid"
            
            def description(self):
                return "line id OC48"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _id(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "id"
            
            def description(self):
                return "tdm ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tdmmon_en"] = _AF6CCI0011_RD_BERT_GEN._sel_bert_montdm._tdmmon_en()
            allFields["tdm_lineid"] = _AF6CCI0011_RD_BERT_GEN._sel_bert_montdm._tdm_lineid()
            allFields["id"] = _AF6CCI0011_RD_BERT_GEN._sel_bert_montdm._id()
            return allFields

    class _sel_bert_monpw(AtRegister.AtRegister):
        def name(self):
            return "Sel PW Bert Gen"
    
        def description(self):
            return "The registers select 1id in line to gen bert data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000600
            
        def endAddress(self):
            return 0x0000061f

        class _pwmon_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "pwmon_en"
            
            def description(self):
                return "set \"1\" to enable bert gen"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _pw_lineid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 11
        
            def name(self):
                return "pw_lineid"
            
            def description(self):
                return "line id OC48"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _pw_lkid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pw_lkid"
            
            def description(self):
                return "pwlk id"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pwmon_en"] = _AF6CCI0011_RD_BERT_GEN._sel_bert_monpw._pwmon_en()
            allFields["pw_lineid"] = _AF6CCI0011_RD_BERT_GEN._sel_bert_monpw._pw_lineid()
            allFields["pw_lkid"] = _AF6CCI0011_RD_BERT_GEN._sel_bert_monpw._pw_lkid()
            return allFields

    class _ctrl_pen_gen(AtRegister.AtRegister):
        def name(self):
            return "Sel Mode Bert Gen"
    
        def description(self):
            return "The registers select mode bert gen"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_300 + 2048*slice + id"
            
        def startAddress(self):
            return 0x00008300
            
        def endAddress(self):
            return 0x0000bb1f

        class _swapmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "swapmode"
            
            def description(self):
                return "swap data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _invmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "invmode"
            
            def description(self):
                return "invert data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _patt_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "patt_mode"
            
            def description(self):
                return "sel pattern gen # 0x01 : all0 # 0x02 : prbs15 # 0x10 : prbs23 # 0x20 : prbs31 # 0x80 : all1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["swapmode"] = _AF6CCI0011_RD_BERT_GEN._ctrl_pen_gen._swapmode()
            allFields["invmode"] = _AF6CCI0011_RD_BERT_GEN._ctrl_pen_gen._invmode()
            allFields["patt_mode"] = _AF6CCI0011_RD_BERT_GEN._ctrl_pen_gen._patt_mode()
            return allFields

    class _ctrl_pen_montdm(AtRegister.AtRegister):
        def name(self):
            return "Sel Mode Bert Mon TDM"
    
        def description(self):
            return "The registers select mode bert gen"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_520 + 2048*slice + id"
            
        def startAddress(self):
            return 0x00008520
            
        def endAddress(self):
            return 0x0000bd3f

        class _swapmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "swapmode"
            
            def description(self):
                return "swap data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _invmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "invmode"
            
            def description(self):
                return "invert data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _patt_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "patt_mode"
            
            def description(self):
                return "sel pattern gen # 0x02 : all0 # 0x04 : prbs15 # 0x20 : prbs23 # 0x40 : prbs31 # 0x80 : all1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["swapmode"] = _AF6CCI0011_RD_BERT_GEN._ctrl_pen_montdm._swapmode()
            allFields["invmode"] = _AF6CCI0011_RD_BERT_GEN._ctrl_pen_montdm._invmode()
            allFields["patt_mode"] = _AF6CCI0011_RD_BERT_GEN._ctrl_pen_montdm._patt_mode()
            return allFields

    class _ctrl_pen_montdm(AtRegister.AtRegister):
        def name(self):
            return "Sel Mode Bert Mon TDM"
    
        def description(self):
            return "The registers select mode bert gen"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_720 + 2048*slice"
            
        def startAddress(self):
            return 0x00008720
            
        def endAddress(self):
            return 0x0000bf3f

        class _swapmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "swapmode"
            
            def description(self):
                return "swap data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _invmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "invmode"
            
            def description(self):
                return "invert data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _patt_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "patt_mode"
            
            def description(self):
                return "sel pattern gen # 0x02 : all0 # 0x04 : prbs15 # 0x20 : prbs23 # 0x40 : prbs31 # 0x80 : all1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["swapmode"] = _AF6CCI0011_RD_BERT_GEN._ctrl_pen_montdm._swapmode()
            allFields["invmode"] = _AF6CCI0011_RD_BERT_GEN._ctrl_pen_montdm._invmode()
            allFields["patt_mode"] = _AF6CCI0011_RD_BERT_GEN._ctrl_pen_montdm._patt_mode()
            return allFields

    class _ctrl_ber_pen(AtRegister.AtRegister):
        def name(self):
            return "Inser Error"
    
        def description(self):
            return "The registers select rate inser error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_320 + 2048*slice + id"
            
        def startAddress(self):
            return 0x00008320
            
        def endAddress(self):
            return 0x0000bb3f

        class _ber_rate(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ber_rate"
            
            def description(self):
                return "TxBerMd [31:0] == BER_level_val  : Bit Error Rate inserted to Pattern Generator [31:0] == 32'd0          :disable  Incase N DS0 mode : n        = num timeslot use, BER_level_val recalculate by CFG_DS1,CFG_E1 CFG_DS1  = ( 8n  x  BER_level_val) / 193 CFG_E1   = ( 8n  x  BER_level_val) / 256 Other : BER_level_val	BER_level 1000:        	BER 10e-3 10_000:      	BER 10e-4 100_000:     	BER 10e-5 1_000_000:   	BER 10e-6 10_000_000:  	BER 10e-7"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ber_rate"] = _AF6CCI0011_RD_BERT_GEN._ctrl_ber_pen._ber_rate()
            return allFields

    class _goodbit_ber_pen(AtRegister.AtRegister):
        def name(self):
            return "Counter num of bit gen"
    
        def description(self):
            return "The registers counter bit genertaie in tdm side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_380 + 2048*slice + id"
            
        def startAddress(self):
            return 0x00008380
            
        def endAddress(self):
            return 0x0000bb80

        class _goodbit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "goodbit"
            
            def description(self):
                return "counter goodbit"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["goodbit"] = _AF6CCI0011_RD_BERT_GEN._goodbit_ber_pen._goodbit()
            return allFields

    class _sel_ho_bert_tdm_mon(AtRegister.AtRegister):
        def name(self):
            return "Sel Ho Bert TDM Mon"
    
        def description(self):
            return "The registers select 1id in line to gen bert data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_580 + 2048*slice + id"
            
        def startAddress(self):
            return 0x00008580
            
        def endAddress(self):
            return 0x0000bd9f

        class _mongood(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mongood"
            
            def description(self):
                return "moniter goofbit"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mongood"] = _AF6CCI0011_RD_BERT_GEN._sel_ho_bert_tdm_mon._mongood()
            return allFields

    class _err_tdm_mon(AtRegister.AtRegister):
        def name(self):
            return "TDM err sync"
    
        def description(self):
            return "The registers indicate bert mon err in tdm side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_540 + 2048*slice + id"
            
        def startAddress(self):
            return 0x00008540
            
        def endAddress(self):
            return 0x0000bd5f

        class _cnt_errbit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_errbit"
            
            def description(self):
                return "counter err bit"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_errbit"] = _AF6CCI0011_RD_BERT_GEN._err_tdm_mon._cnt_errbit()
            return allFields

    class _lossbit_pen_tdm_mon(AtRegister.AtRegister):
        def name(self):
            return "Counter loss bit in TDM mon"
    
        def description(self):
            return "The registers count lossbit in  mon tdm side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_400 + 2048*slice"
            
        def startAddress(self):
            return 0x00008400
            
        def endAddress(self):
            return 0x0000bc00

        class _stk_losssyn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stk_losssyn"
            
            def description(self):
                return "sticky loss sync"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["stk_losssyn"] = _AF6CCI0011_RD_BERT_GEN._lossbit_pen_tdm_mon._stk_losssyn()
            return allFields

    class _lossbit_pen_tdm_mon(AtRegister.AtRegister):
        def name(self):
            return "Counter loss bit in TDM mon"
    
        def description(self):
            return "The registers count lossbit in  mon tdm side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_5C0 + 2048*slice + id"
            
        def startAddress(self):
            return 0x000085c0
            
        def endAddress(self):
            return 0x0000bddf

        class _cnt_lossbit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_lossbit"
            
            def description(self):
                return "counter lossbit"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_lossbit"] = _AF6CCI0011_RD_BERT_GEN._lossbit_pen_tdm_mon._cnt_lossbit()
            return allFields

    class _goodbit_pen_pw_mon(AtRegister.AtRegister):
        def name(self):
            return "counter good bit PW mon"
    
        def description(self):
            return "The registers count goodbit in  mon pw side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_780 + 2048*slice + id"
            
        def startAddress(self):
            return 0x00008780
            
        def endAddress(self):
            return 0x0000bf9f

        class _cnt_pwgoodbit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_pwgoodbit"
            
            def description(self):
                return "counter goodbit"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_pwgoodbit"] = _AF6CCI0011_RD_BERT_GEN._goodbit_pen_pw_mon._cnt_pwgoodbit()
            return allFields

    class _errbit_pen_pw_mon(AtRegister.AtRegister):
        def name(self):
            return "counter error bit in PW mon"
    
        def description(self):
            return "The registers count goodbit in  mon tdm side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_740 + 2048*slice + id"
            
        def startAddress(self):
            return 0x00008740
            
        def endAddress(self):
            return 0x0000bf5f

        class _cnt_pwerrbit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_pwerrbit"
            
            def description(self):
                return "counter err bit"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_pwerrbit"] = _AF6CCI0011_RD_BERT_GEN._errbit_pen_pw_mon._cnt_pwerrbit()
            return allFields

    class _lossbit_pen_pw_mon(AtRegister.AtRegister):
        def name(self):
            return "Counter loss bit in PW mon"
    
        def description(self):
            return "The registers count lossbit in  mon tdm side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_7C0 + 2048*slice + id"
            
        def startAddress(self):
            return 0x000087c0
            
        def endAddress(self):
            return 0x0000bfdf

        class _cnt_pwlossbit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_pwlossbit"
            
            def description(self):
                return "counter loss bit"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_pwlossbit"] = _AF6CCI0011_RD_BERT_GEN._lossbit_pen_pw_mon._cnt_pwlossbit()
            return allFields

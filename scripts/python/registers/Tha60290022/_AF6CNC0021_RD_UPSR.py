import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0021_RD_UPSR(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upsralrmmasksfsdramctl"] = _AF6CNC0021_RD_UPSR._upsralrmmasksfsdramctl()
        allRegisters["upsrholdwtrtimerramctl"] = _AF6CNC0021_RD_UPSR._upsrholdwtrtimerramctl()
        allRegisters["upsrsmramctl1"] = _AF6CNC0021_RD_UPSR._upsrsmramctl1()
        allRegisters["upsrsmramctl2"] = _AF6CNC0021_RD_UPSR._upsrsmramctl2()
        allRegisters["upsrsmintenb"] = _AF6CNC0021_RD_UPSR._upsrsmintenb()
        allRegisters["upsrsmintint"] = _AF6CNC0021_RD_UPSR._upsrsmintint()
        allRegisters["upsrsmcursta"] = _AF6CNC0021_RD_UPSR._upsrsmcursta()
        allRegisters["upsrsmselorint"] = _AF6CNC0021_RD_UPSR._upsrsmselorint()
        allRegisters["upsrsmgrporint"] = _AF6CNC0021_RD_UPSR._upsrsmgrporint()
        allRegisters["upsrsmgrporintenb"] = _AF6CNC0021_RD_UPSR._upsrsmgrporintenb()
        return allRegisters

    class _upsralrmmasksfsdramctl(AtRegister.AtRegister):
        def name(self):
            return "UPSR SF/SD Alarm Mask Registers"
    
        def description(self):
            return "These configuration registers are used to assign these alarms {plm,tim,ber_sd,ber_sf,uneq,lop,ais} into SF/SD events for 8 lines OCn_Path. If each alarm is not assigned to SD and SF, this means that this alarm is disabled."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x01000 + 64*LineId + StsId"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0x0000172f

        class _UpsrPlmMaskSf(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "UpsrPlmMaskSf"
            
            def description(self):
                return "Assign PLM defect alarm to SF event, set 1 to choose. 1: Enable. 0: Disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UpsrTimMaskSf(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "UpsrTimMaskSf"
            
            def description(self):
                return "Assign TIM defect alarm to SF event, set 1 to choose. 1: Enable. 0: Disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UpsrBerSDMaskSf(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "UpsrBerSDMaskSf"
            
            def description(self):
                return "Assign Ber_SD to SF event, set 1 to choose. 1: Enable. 0: Disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UpsrBerSFMaskSf(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "UpsrBerSFMaskSf"
            
            def description(self):
                return "Assign Ber_SF to SF event, set 1 to choose. 1: Enable. 0: Disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UpsrUneqMaskSf(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "UpsrUneqMaskSf"
            
            def description(self):
                return "Assign UNEQ defect to SF event, set 1 to choose. 1: Enable. 0: Disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UpsrLopMaskSf(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "UpsrLopMaskSf"
            
            def description(self):
                return "Assign LOP defect to SF event, set 1 to choose. 1: Enable. 0: Disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UpsrAisMaskSf(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "UpsrAisMaskSf"
            
            def description(self):
                return "Assign AIS defect to SF event, set 1 to choose. 1: Enable. 0: Disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UpsrPlmMaskSd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "UpsrPlmMaskSd"
            
            def description(self):
                return "Assign PLM defect alarm to SD event, set 1 to choose. 1: Enable. 0: Disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UpsrTimMaskSd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "UpsrTimMaskSd"
            
            def description(self):
                return "Assign TIM defect alarm to SD event, set 1 to choose. 1: Enable. 0: Disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UpsrBerSDMaskSd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "UpsrBerSDMaskSd"
            
            def description(self):
                return "Assign Ber_SD to SD event, set 1 to choose. 1: Enable. 0: Disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UpsrBerSFMaskSd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "UpsrBerSFMaskSd"
            
            def description(self):
                return "Assign Ber_SF to SD event, set 1 to choose. 1: Enable. 0: Disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UpsrUneqMaskSd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "UpsrUneqMaskSd"
            
            def description(self):
                return "Assign UNEQ defect to SD event, set 1 to choose. 1: Enable. 0: Disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UpsrLopMaskSd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "UpsrLopMaskSd"
            
            def description(self):
                return "Assign LOP defect to SD event, set 1 to choose. 1: Enable. 0: Disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UpsrAisMaskSd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "UpsrAisMaskSd"
            
            def description(self):
                return "Assign AIS defect to SD event, set 1 to choose. 1: Enable. 0: Disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["UpsrPlmMaskSf"] = _AF6CNC0021_RD_UPSR._upsralrmmasksfsdramctl._UpsrPlmMaskSf()
            allFields["UpsrTimMaskSf"] = _AF6CNC0021_RD_UPSR._upsralrmmasksfsdramctl._UpsrTimMaskSf()
            allFields["UpsrBerSDMaskSf"] = _AF6CNC0021_RD_UPSR._upsralrmmasksfsdramctl._UpsrBerSDMaskSf()
            allFields["UpsrBerSFMaskSf"] = _AF6CNC0021_RD_UPSR._upsralrmmasksfsdramctl._UpsrBerSFMaskSf()
            allFields["UpsrUneqMaskSf"] = _AF6CNC0021_RD_UPSR._upsralrmmasksfsdramctl._UpsrUneqMaskSf()
            allFields["UpsrLopMaskSf"] = _AF6CNC0021_RD_UPSR._upsralrmmasksfsdramctl._UpsrLopMaskSf()
            allFields["UpsrAisMaskSf"] = _AF6CNC0021_RD_UPSR._upsralrmmasksfsdramctl._UpsrAisMaskSf()
            allFields["UpsrPlmMaskSd"] = _AF6CNC0021_RD_UPSR._upsralrmmasksfsdramctl._UpsrPlmMaskSd()
            allFields["UpsrTimMaskSd"] = _AF6CNC0021_RD_UPSR._upsralrmmasksfsdramctl._UpsrTimMaskSd()
            allFields["UpsrBerSDMaskSd"] = _AF6CNC0021_RD_UPSR._upsralrmmasksfsdramctl._UpsrBerSDMaskSd()
            allFields["UpsrBerSFMaskSd"] = _AF6CNC0021_RD_UPSR._upsralrmmasksfsdramctl._UpsrBerSFMaskSd()
            allFields["UpsrUneqMaskSd"] = _AF6CNC0021_RD_UPSR._upsralrmmasksfsdramctl._UpsrUneqMaskSd()
            allFields["UpsrLopMaskSd"] = _AF6CNC0021_RD_UPSR._upsralrmmasksfsdramctl._UpsrLopMaskSd()
            allFields["UpsrAisMaskSd"] = _AF6CNC0021_RD_UPSR._upsralrmmasksfsdramctl._UpsrAisMaskSd()
            return allFields

    class _upsrholdwtrtimerramctl(AtRegister.AtRegister):
        def name(self):
            return "UPSR SF/SD Hold_Off/Hold_On timer Registers"
    
        def description(self):
            return "These configuration registers are used to configure hold_off/hold_On for 8 lines OCn_Path (LineId:0-7) and 8 lines MATE (LineId:8-15). Hold_off Timer 			= UpsrHoldOffTmUnit * UpsrHoldOffTmMax Hold_on  Timer 			= UpsrHoldOnnTmUnit * UpsrHoldOnnTmMax"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x02000 + 64*LineId + StsId"
            
        def startAddress(self):
            return 0x00002000
            
        def endAddress(self):
            return 0x00002f2f

        class _UpsrHoldOnnTmUnit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 20
        
            def name(self):
                return "UpsrHoldOnnTmUnit"
            
            def description(self):
                return "Hold_on Timer Unit. (Scale down 5 times for simulation) 7,6: Unused. 5: Timer Unit is 100 ms. 4: Timer Unit is 50  ms. 3: Timer Unit is 10  ms. 2: Timer Unit is 5   ms. 1: Timer Unit is 1 	 ms. 0: Timer Unit is 0.5 ms."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UpsrHoldOffTmUnit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 16
        
            def name(self):
                return "UpsrHoldOffTmUnit"
            
            def description(self):
                return "Hold_off Timer Unit. (Scale down 5 times for simulation) 7,6: Unused. 5: Timer Unit is 100 ms. 4: Timer Unit is 50  ms. 3: Timer Unit is 10  ms. 2: Timer Unit is 5   ms. 1: Timer Unit is 1 	 ms. 0: Timer Unit is 0.5 ms."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UpsrHoldOnnTmMax(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "UpsrHoldOnnTmMax"
            
            def description(self):
                return "Hold_on Unit_Timer Maximum Value."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UpsrHoldOffTmMax(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "UpsrHoldOffTmMax"
            
            def description(self):
                return "Hold_off Unit_Timer Maximum Value."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["UpsrHoldOnnTmUnit"] = _AF6CNC0021_RD_UPSR._upsrholdwtrtimerramctl._UpsrHoldOnnTmUnit()
            allFields["UpsrHoldOffTmUnit"] = _AF6CNC0021_RD_UPSR._upsrholdwtrtimerramctl._UpsrHoldOffTmUnit()
            allFields["UpsrHoldOnnTmMax"] = _AF6CNC0021_RD_UPSR._upsrholdwtrtimerramctl._UpsrHoldOnnTmMax()
            allFields["UpsrHoldOffTmMax"] = _AF6CNC0021_RD_UPSR._upsrholdwtrtimerramctl._UpsrHoldOffTmMax()
            return allFields

    class _upsrsmramctl1(AtRegister.AtRegister):
        def name(self):
            return "UPSR State Machine Engine Registers 1"
    
        def description(self):
            return "These configuration registers are used to configure for UPSR state machine engine, They are configured per Path Selector Wait_to_Restore Timer 	= UpsrWtrTmUnit 	* UpsrWtrTmMax"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x03000 + SelectorId"
            
        def startAddress(self):
            return 0x00003000
            
        def endAddress(self):
            return 0x0000317f

        class _UpsrSmWtrTmUnit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "UpsrSmWtrTmUnit"
            
            def description(self):
                return "Wait_to_Restore Timer Unit.(Scale down (10ms/5, or 5ms/5 for simulation) 1: Timer Unit is 1 minute. 0: Timer Unit is 0.5 minute."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UpsrSmWtrTmMax(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 22
        
            def name(self):
                return "UpsrSmWtrTmMax"
            
            def description(self):
                return "Wait_to_Restore Unit_Timer Maximum Value."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UpsrSmEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "UpsrSmEnable"
            
            def description(self):
                return "UPSR enable per selector. When any selector is disable, all off status is reset to Reset value (0x0). and no interrupt is sent. 1: Enable. 0: Disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UpsrSmRevertMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "UpsrSmRevertMode"
            
            def description(self):
                return "Configure Path Selector is revertive or nonrevertive mode. Default is nonrevertive mode 1: Revertive Mode. 0: Nonrevertive Mode."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UpsrSmWokPathId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 10
        
            def name(self):
                return "UpsrSmWokPathId"
            
            def description(self):
                return "Working Path Sonet_ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UpsrSmProPathId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "UpsrSmProPathId"
            
            def description(self):
                return "Protection Path Sonet_ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["UpsrSmWtrTmUnit"] = _AF6CNC0021_RD_UPSR._upsrsmramctl1._UpsrSmWtrTmUnit()
            allFields["UpsrSmWtrTmMax"] = _AF6CNC0021_RD_UPSR._upsrsmramctl1._UpsrSmWtrTmMax()
            allFields["UpsrSmEnable"] = _AF6CNC0021_RD_UPSR._upsrsmramctl1._UpsrSmEnable()
            allFields["UpsrSmRevertMode"] = _AF6CNC0021_RD_UPSR._upsrsmramctl1._UpsrSmRevertMode()
            allFields["UpsrSmWokPathId"] = _AF6CNC0021_RD_UPSR._upsrsmramctl1._UpsrSmWokPathId()
            allFields["UpsrSmProPathId"] = _AF6CNC0021_RD_UPSR._upsrsmramctl1._UpsrSmProPathId()
            return allFields

    class _upsrsmramctl2(AtRegister.AtRegister):
        def name(self):
            return "UPSR State Machine Engine Registers 2"
    
        def description(self):
            return "These configuration registers are used to configure for UPSR state machine engine, They are configured per Path Selector Wait_to_Restore Timer 	= UpsrWtrTmUnit 	* UpsrWtrTmMax"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x03800 + SelectorId"
            
        def startAddress(self):
            return 0x00003800
            
        def endAddress(self):
            return 0x0000397f

        class _UpsrSmManualCmd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "UpsrSmManualCmd"
            
            def description(self):
                return "Manually Protection Switching Commands. 7,6: Unused. 5: Lockout of Protection. 4: Forced Switch to Working. 3: Forced Switch to Protection. 2: Manual Switch to Working. 1: Manual Switch to Protection. 0: Clear."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["UpsrSmManualCmd"] = _AF6CNC0021_RD_UPSR._upsrsmramctl2._UpsrSmManualCmd()
            return allFields

    class _upsrsmintenb(AtRegister.AtRegister):
        def name(self):
            return "UPSR per Alarm Interrupt Enable Control"
    
        def description(self):
            return "These are the per Alarm interrupt enable. Each register is used to store bits to enable interrupts when the related alarms/events in UPSR engine happen."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x04000 + SelectorId"
            
        def startAddress(self):
            return 0x00004000
            
        def endAddress(self):
            return 0x000041ff

        class _UpsrSmIntEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 0
        
            def name(self):
                return "UpsrSmIntEnb"
            
            def description(self):
                return "Set 1 to enable alarm/event in UPSR engine to generate an interrupt when having any local state or active path is changed. [12] : SDSF to NRQ Condition: Current state changed from SDSF to NRQ. [11] : WTR to NRQ Condition:  Current state changed from WTR to NRQ. [10] : WTR Condition: Current state changed from NRQ to WTR OR from SDSF to WTR. [9]  : Manual Switch Command. [8]  : SD Condition. [7]  : SF Condition. [6]  : Forced Switch Command. [5]  : Lockout of Protection Command. [4]  : Clear command when current state at External Command Switching. [3:1]: Unused. [0]  : Active path change from working to protection or protection to working."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["UpsrSmIntEnb"] = _AF6CNC0021_RD_UPSR._upsrsmintenb._UpsrSmIntEnb()
            return allFields

    class _upsrsmintint(AtRegister.AtRegister):
        def name(self):
            return "UPSR per Alarm Interrupt Status"
    
        def description(self):
            return "These are the per Alarm interrupt status. Each register is used to store sticky bits for each alarms/events in UPSR engine."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x04200 + SelectorId"
            
        def startAddress(self):
            return 0x00004200
            
        def endAddress(self):
            return 0x000043ff

        class _UpsrSmIntInt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 0
        
            def name(self):
                return "UpsrSmIntInt"
            
            def description(self):
                return "Set 1 while alarm/event detected in the related UPSR selector, and it is generated an interrupt if it is enabled. [12] : SDSF to NRQ Condition:Current state changed from SDSF to NRQ. [11] : WTR to NRQ Condition:Current state changed from WTR to NRQ. [10] : WTR Condition: Current state changed from NRQ to WTR OR from SDSF to WTR. [9]  : Manual Switch Command. [8]  : SD Condition. [7]  : SF Condition. [6]  : Forced Switch Command. [5]  : Lockout of Protection Command. [4]  : Clear command when current state at External Command Switching. [3:1]: Unused. [0]  : Active path change from working to protection or protection to working."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["UpsrSmIntInt"] = _AF6CNC0021_RD_UPSR._upsrsmintint._UpsrSmIntInt()
            return allFields

    class _upsrsmcursta(AtRegister.AtRegister):
        def name(self):
            return "UPSR per Alarm Current Status"
    
        def description(self):
            return "These are the per Alarm Current status. Each register include Local state and actived path indication."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x04400 + SelectorId"
            
        def startAddress(self):
            return 0x00004400
            
        def endAddress(self):
            return 0x000045ff

        class _UpsrSmStaCurState(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 1
        
            def name(self):
                return "UpsrSmStaCurState"
            
            def description(self):
                return "This field indicate Local State of Path Seletor. 7: Unused. 6: Lockout of Protection. 5: Forced Switch. 4: SF. 3: SD. 2: Manual Switch. 1: Wait_to_Restore. 0: No Request."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _UpsrSmStaActPath(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "UpsrSmStaActPath"
            
            def description(self):
                return "This field indicate Active Path is Working Path or Protection Path. 1: Protection Path. 0: Working Path."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["UpsrSmStaCurState"] = _AF6CNC0021_RD_UPSR._upsrsmcursta._UpsrSmStaCurState()
            allFields["UpsrSmStaActPath"] = _AF6CNC0021_RD_UPSR._upsrsmcursta._UpsrSmStaActPath()
            return allFields

    class _upsrsmselorint(AtRegister.AtRegister):
        def name(self):
            return "UPSR per Alarm per Selector Interrupt_OR Status"
    
        def description(self):
            return "There are 12 groups(0-11) of selector, These group consists of 32 bits for 32 selectors. Group #0 for SelectorId from 0 to 31, respectively. Each bit is used to store Interrupt_OR status of the related Selector."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x04600 + GroupSelectorId"
            
        def startAddress(self):
            return 0x00004600
            
        def endAddress(self):
            return 0x0000460f

        class _UpsrSmStaIntSelOr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "UpsrSmStaIntSelOr"
            
            def description(self):
                return "Set to 1 to indicate that there is any interrupt status bit in the \"UPSR per Alarm Interrupt Status\" registers of the related Selector to be //										set and they are enabled to raise interrupt. Bit 0 for Selector #0, respectively."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["UpsrSmStaIntSelOr"] = _AF6CNC0021_RD_UPSR._upsrsmselorint._UpsrSmStaIntSelOr()
            return allFields

    class _upsrsmgrporint(AtRegister.AtRegister):
        def name(self):
            return "UPSR per Alarm Selector Group Interrupt_OR Status"
    
        def description(self):
            return "The register consists of 12 bits for 12 Selector Groups. Each bit is used to store Interrupt_OR status of the related Selector Group."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000047ff
            
        def endAddress(self):
            return 0x000047ff

        class _UpsrSmStaIntGrpOr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "UpsrSmStaIntGrpOr"
            
            def description(self):
                return "Set to 1 to indicate that there is any interrupt status bit in the \"UPSR per Alarm per Selector Interrupt_OR Status\" register of the related //										Selector Group to be set and they are enabled to raise interrupt Bit 0 for Selector Group #0, respectively."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["UpsrSmStaIntGrpOr"] = _AF6CNC0021_RD_UPSR._upsrsmgrporint._UpsrSmStaIntGrpOr()
            return allFields

    class _upsrsmgrporintenb(AtRegister.AtRegister):
        def name(self):
            return "UPSR per Alarm Selector Group Interrupt_OR Enable Control"
    
        def description(self):
            return "The register consists of 12 bits for 12 Selector Groups to enable interrupts when alarms in related Selector Group happen."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000047fe
            
        def endAddress(self):
            return 0x000047fe

        class _UpsrSmStaIntGrpOrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "UpsrSmStaIntGrpOrEn"
            
            def description(self):
                return "Set to 1 to enable the related Selector Group to generate interrupt. Bit 0 for Selector Group #0, respectively."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["UpsrSmStaIntGrpOrEn"] = _AF6CNC0021_RD_UPSR._upsrsmgrporintenb._UpsrSmStaIntGrpOrEn()
            return allFields

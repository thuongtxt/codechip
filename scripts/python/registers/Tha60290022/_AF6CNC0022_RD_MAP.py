import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0022_RD_MAP(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["map_cpu_hold_ctrl"] = _AF6CNC0022_RD_MAP._map_cpu_hold_ctrl()
        allRegisters["demap_channel_ctrl"] = _AF6CNC0022_RD_MAP._demap_channel_ctrl()
        allRegisters["demapho_channel_ctrl"] = _AF6CNC0022_RD_MAP._demapho_channel_ctrl()
        allRegisters["map_line_ctrl"] = _AF6CNC0022_RD_MAP._map_line_ctrl()
        allRegisters["map_channel_ctrl"] = _AF6CNC0022_RD_MAP._map_channel_ctrl()
        allRegisters["map_idle_code"] = _AF6CNC0022_RD_MAP._map_idle_code()
        allRegisters["RAM_Map_Parity_Force_Control"] = _AF6CNC0022_RD_MAP._RAM_Map_Parity_Force_Control()
        allRegisters["RAM_Map_Parity_Disable_Control"] = _AF6CNC0022_RD_MAP._RAM_Map_Parity_Disable_Control()
        allRegisters["RAM_Map_Parity_Error_Sticky"] = _AF6CNC0022_RD_MAP._RAM_Map_Parity_Error_Sticky()
        allRegisters["RAM_DeMap_Parity_Force_Control"] = _AF6CNC0022_RD_MAP._RAM_DeMap_Parity_Force_Control()
        allRegisters["RAM_DeMap_Parity_Disable_Control"] = _AF6CNC0022_RD_MAP._RAM_DeMap_Parity_Disable_Control()
        allRegisters["RAM_DeMap_Parity_Error_Sticky"] = _AF6CNC0022_RD_MAP._RAM_DeMap_Parity_Error_Sticky()
        return allRegisters

    class _map_cpu_hold_ctrl(AtRegister.AtRegister):
        def name(self):
            return "MAP CPU  Reg Hold Control"
    
        def description(self):
            return "The register provides hold register for three word 32-bits MSB when CPU access to engine."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00F000 + HoldId"
            
        def startAddress(self):
            return 0x0000f000
            
        def endAddress(self):
            return 0x0000f002

        class _HoldReg0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HoldReg0"
            
            def description(self):
                return "Hold 32 bits"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HoldReg0"] = _AF6CNC0022_RD_MAP._map_cpu_hold_ctrl._HoldReg0()
            return allFields

    class _demap_channel_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Demap Channel Control"
    
        def description(self):
            return "The registers are used by the hardware to convert channel identification number to STS/VT/PDH identification number. All STS/VT/PDHs of a concatenation are assigned the same channel identification number."
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000000 + 168*stsid + 24*vtgid + 6*vtid + slotid"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x00001fff

        class _DemapFirstTs3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 63
        
            def name(self):
                return "DemapFirstTs3"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapChannelType3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 62
                
            def startBit(self):
                return 60
        
            def name(self):
                return "DemapChannelType3"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapPwEn3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 59
                
            def startBit(self):
                return 59
        
            def name(self):
                return "DemapPwEn3"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapPWIdField3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 58
                
            def startBit(self):
                return 48
        
            def name(self):
                return "DemapPWIdField3"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapFirstTs2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 47
        
            def name(self):
                return "DemapFirstTs2"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapChannelType2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 46
                
            def startBit(self):
                return 44
        
            def name(self):
                return "DemapChannelType2"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapPwEn2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 43
                
            def startBit(self):
                return 43
        
            def name(self):
                return "DemapPwEn2"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapPWIdField2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 42
                
            def startBit(self):
                return 32
        
            def name(self):
                return "DemapPWIdField2"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapFirstTs1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "DemapFirstTs1"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapChannelType1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 28
        
            def name(self):
                return "DemapChannelType1"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapPwEn1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "DemapPwEn1"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapPWIdField1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 16
        
            def name(self):
                return "DemapPWIdField1"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapFirstTs0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "DemapFirstTs0"
            
            def description(self):
                return "First timeslot indication. Use for CESoP mode only"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapChannelType0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DemapChannelType0"
            
            def description(self):
                return "Specify which type of mapping for this. Specify which type of mapping for this. 0: SAToP encapsulation from DS1/E1, DS3/E3 1: CESoP encapsulation from DS1/E1 2: ATM over DS1/ E1, SONET/SDH (unused) 3: IMA over DS1/E1, SONET/SDH(unused) 4: HDLC/PPP/LAPS over DS1/E1, SONET/SDH(unused) 5: MLPPP over DS1/E1, SONET/SDH(unused) 6: CEP encapsulation from SONET/SDH 7: VCAT/LCAS/PLCP"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapPwEn0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "DemapPwEn0"
            
            def description(self):
                return "PW/Channels Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapPWIdField0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DemapPWIdField0"
            
            def description(self):
                return "PW ID field that uses for Encapsulation"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DemapFirstTs3"] = _AF6CNC0022_RD_MAP._demap_channel_ctrl._DemapFirstTs3()
            allFields["DemapChannelType3"] = _AF6CNC0022_RD_MAP._demap_channel_ctrl._DemapChannelType3()
            allFields["DemapPwEn3"] = _AF6CNC0022_RD_MAP._demap_channel_ctrl._DemapPwEn3()
            allFields["DemapPWIdField3"] = _AF6CNC0022_RD_MAP._demap_channel_ctrl._DemapPWIdField3()
            allFields["DemapFirstTs2"] = _AF6CNC0022_RD_MAP._demap_channel_ctrl._DemapFirstTs2()
            allFields["DemapChannelType2"] = _AF6CNC0022_RD_MAP._demap_channel_ctrl._DemapChannelType2()
            allFields["DemapPwEn2"] = _AF6CNC0022_RD_MAP._demap_channel_ctrl._DemapPwEn2()
            allFields["DemapPWIdField2"] = _AF6CNC0022_RD_MAP._demap_channel_ctrl._DemapPWIdField2()
            allFields["DemapFirstTs1"] = _AF6CNC0022_RD_MAP._demap_channel_ctrl._DemapFirstTs1()
            allFields["DemapChannelType1"] = _AF6CNC0022_RD_MAP._demap_channel_ctrl._DemapChannelType1()
            allFields["DemapPwEn1"] = _AF6CNC0022_RD_MAP._demap_channel_ctrl._DemapPwEn1()
            allFields["DemapPWIdField1"] = _AF6CNC0022_RD_MAP._demap_channel_ctrl._DemapPWIdField1()
            allFields["DemapFirstTs0"] = _AF6CNC0022_RD_MAP._demap_channel_ctrl._DemapFirstTs0()
            allFields["DemapChannelType0"] = _AF6CNC0022_RD_MAP._demap_channel_ctrl._DemapChannelType0()
            allFields["DemapPwEn0"] = _AF6CNC0022_RD_MAP._demap_channel_ctrl._DemapPwEn0()
            allFields["DemapPWIdField0"] = _AF6CNC0022_RD_MAP._demap_channel_ctrl._DemapPWIdField0()
            return allFields

    class _demapho_channel_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Demap HO Channel Control"
    
        def description(self):
            return "The registers are used by the hardware to configure PW channel"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8300 + stsid"
            
        def startAddress(self):
            return 0x00008300
            
        def endAddress(self):
            return 0xffffffff

        class _DemapPWIdField(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DemapPWIdField"
            
            def description(self):
                return "PW ID field that uses for Encapsulation"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapChEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DemapChEn"
            
            def description(self):
                return "0:Disable,  1:Enable as HO path"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapChannelType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DemapChannelType"
            
            def description(self):
                return "Specify which type of mapping for this. Specify which type of mapping for this. 0: Unused 1: Unused 2: Unused 3: Unused 4: HDLC/PPP/LAPS over DS1/E1, SONET/SDH(unused) 5: MLPPP over DS1/E1, SONET/SDH(unused) 6: CEP encapsulation from SONET/SDH 7: VCAT/LCAS/PLCP"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Demapsr_vc3n3c(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Demapsr_vc3n3c"
            
            def description(self):
                return "0:slave of VC3_N3c  1:master of VC3-N3c"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Demapsrctype(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Demapsrctype"
            
            def description(self):
                return "0:VC3 1:VC3-3c"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapPwEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DemapPwEn"
            
            def description(self):
                return "PW Channels Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DemapPWIdField"] = _AF6CNC0022_RD_MAP._demapho_channel_ctrl._DemapPWIdField()
            allFields["DemapChEn"] = _AF6CNC0022_RD_MAP._demapho_channel_ctrl._DemapChEn()
            allFields["DemapChannelType"] = _AF6CNC0022_RD_MAP._demapho_channel_ctrl._DemapChannelType()
            allFields["Demapsr_vc3n3c"] = _AF6CNC0022_RD_MAP._demapho_channel_ctrl._Demapsr_vc3n3c()
            allFields["Demapsrctype"] = _AF6CNC0022_RD_MAP._demapho_channel_ctrl._Demapsrctype()
            allFields["DemapPwEn"] = _AF6CNC0022_RD_MAP._demapho_channel_ctrl._DemapPwEn()
            return allFields

    class _map_line_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Map Line Control"
    
        def description(self):
            return "The registers provide the per line configurations for STS/VT/DS1/E1 line."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x010000 + 32*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x00010000
            
        def endAddress(self):
            return 0x000107ff

        class _IDLEForce(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "IDLEForce"
            
            def description(self):
                return "Force IDLE Signal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IDLECode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 21
        
            def name(self):
                return "IDLECode"
            
            def description(self):
                return "IDLECode pattern"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapDs1LcEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 18
        
            def name(self):
                return "MapDs1LcEn"
            
            def description(self):
                return "DS1E1 Loop code insert enable 0: Disable 1: CSU Loop up 2: CSU Loop down 3: FA1 Loop up 4: FA1 Loop down 5: FA2 Loop up 6: FA2 Loop down"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapFrameType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 16
        
            def name(self):
                return "MapFrameType"
            
            def description(self):
                return "depend on MapSigType[3:0], the MapFrameType[1:0] bit field can have difference meaning. 0: DS1 SF/E1 BF/E3 G.751(unused) /DS3 framing(unused)/VT1.5,VT2,VT6 normal/STS no POH, no Stuff 1: DS1 ESF/E1 CRC/E3 G.832(unused)/DS1,E1,VT1.5,VT2 fractional 2: CEP DS3/E3/VC3 fractional 3:CEP basic mode or VT with POH/STS with POH, Stuff/DS1,E1,DS3,E3 unframe mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapSigType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "MapSigType"
            
            def description(self):
                return "0: DS1 1: E1 2: DS3 (unused) 3: E3 (unused) 4: VT 1.5 5: VT 2 6: unused 7: unused 8: VC3 9: VC4 10: STS12c/VC4_4c 11: STS48c/VC4_16c 12: TU3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapTimeSrcMaster(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "MapTimeSrcMaster"
            
            def description(self):
                return "This bit is used to indicate the master timing or the master VC3 in VC4/VC4-Xc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapTimeSrcId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MapTimeSrcId"
            
            def description(self):
                return "The reference line ID used for timing reference or the master VC3 ID in VC4/VC4-Xc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IDLEForce"] = _AF6CNC0022_RD_MAP._map_line_ctrl._IDLEForce()
            allFields["IDLECode"] = _AF6CNC0022_RD_MAP._map_line_ctrl._IDLECode()
            allFields["MapDs1LcEn"] = _AF6CNC0022_RD_MAP._map_line_ctrl._MapDs1LcEn()
            allFields["MapFrameType"] = _AF6CNC0022_RD_MAP._map_line_ctrl._MapFrameType()
            allFields["MapSigType"] = _AF6CNC0022_RD_MAP._map_line_ctrl._MapSigType()
            allFields["MapTimeSrcMaster"] = _AF6CNC0022_RD_MAP._map_line_ctrl._MapTimeSrcMaster()
            allFields["MapTimeSrcId"] = _AF6CNC0022_RD_MAP._map_line_ctrl._MapTimeSrcId()
            return allFields

    class _map_channel_ctrl(AtRegister.AtRegister):
        def name(self):
            return "map Channel Control"
    
        def description(self):
            return "The registers are used by the hardware to convert channel identification number to STS/VT/PDH identification number. All STS/VT/PDHs of a concatenation are assigned the same channel identification number."
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x018000 + 168*stsid + 24*vtgid + 6*vtid + slotid"
            
        def startAddress(self):
            return 0x00018000
            
        def endAddress(self):
            return 0x00019fff

        class _mapFirstTs3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 63
        
            def name(self):
                return "mapFirstTs3"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _mapChannelType3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 62
                
            def startBit(self):
                return 60
        
            def name(self):
                return "mapChannelType3"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _mapPwEn3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 59
                
            def startBit(self):
                return 59
        
            def name(self):
                return "mapPwEn3"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _mapPWIdField3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 58
                
            def startBit(self):
                return 48
        
            def name(self):
                return "mapPWIdField3"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _mapFirstTs2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 47
        
            def name(self):
                return "mapFirstTs2"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _mapChannelType2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 46
                
            def startBit(self):
                return 44
        
            def name(self):
                return "mapChannelType2"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _mapPwEn2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 43
                
            def startBit(self):
                return 43
        
            def name(self):
                return "mapPwEn2"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _mapPWIdField2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 42
                
            def startBit(self):
                return 32
        
            def name(self):
                return "mapPWIdField2"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _mapFirstTs1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "mapFirstTs1"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _mapChannelType1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 28
        
            def name(self):
                return "mapChannelType1"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _mapPwEn1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "mapPwEn1"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _mapPWIdField1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 16
        
            def name(self):
                return "mapPWIdField1"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _mapFirstTs0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "mapFirstTs0"
            
            def description(self):
                return "First timeslot indication. Use for CESoP mode only"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _mapChannelType0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 12
        
            def name(self):
                return "mapChannelType0"
            
            def description(self):
                return "Specify which type of mapping for this. Specify which type of mapping for this. 0: SAToP encapsulation from DS1/E1, DS3/E3 1: CESoP encapsulation from DS1/E1 2: ATM over DS1/ E1, SONET/SDH (unused) 3: IMA over DS1/E1, SONET/SDH(unused) 4: HDLC/PPP/LAPS over DS1/E1, SONET/SDH(unused) 5: MLPPP over DS1/E1, SONET/SDH(unused) 6: CEP encapsulation from SONET/SDH 7: VCAT/LCAS/PLCP"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _mapPwEn0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "mapPwEn0"
            
            def description(self):
                return "PW/Channels Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _mapPWIdField0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mapPWIdField0"
            
            def description(self):
                return "PW ID field that uses for Encapsulation"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mapFirstTs3"] = _AF6CNC0022_RD_MAP._map_channel_ctrl._mapFirstTs3()
            allFields["mapChannelType3"] = _AF6CNC0022_RD_MAP._map_channel_ctrl._mapChannelType3()
            allFields["mapPwEn3"] = _AF6CNC0022_RD_MAP._map_channel_ctrl._mapPwEn3()
            allFields["mapPWIdField3"] = _AF6CNC0022_RD_MAP._map_channel_ctrl._mapPWIdField3()
            allFields["mapFirstTs2"] = _AF6CNC0022_RD_MAP._map_channel_ctrl._mapFirstTs2()
            allFields["mapChannelType2"] = _AF6CNC0022_RD_MAP._map_channel_ctrl._mapChannelType2()
            allFields["mapPwEn2"] = _AF6CNC0022_RD_MAP._map_channel_ctrl._mapPwEn2()
            allFields["mapPWIdField2"] = _AF6CNC0022_RD_MAP._map_channel_ctrl._mapPWIdField2()
            allFields["mapFirstTs1"] = _AF6CNC0022_RD_MAP._map_channel_ctrl._mapFirstTs1()
            allFields["mapChannelType1"] = _AF6CNC0022_RD_MAP._map_channel_ctrl._mapChannelType1()
            allFields["mapPwEn1"] = _AF6CNC0022_RD_MAP._map_channel_ctrl._mapPwEn1()
            allFields["mapPWIdField1"] = _AF6CNC0022_RD_MAP._map_channel_ctrl._mapPWIdField1()
            allFields["mapFirstTs0"] = _AF6CNC0022_RD_MAP._map_channel_ctrl._mapFirstTs0()
            allFields["mapChannelType0"] = _AF6CNC0022_RD_MAP._map_channel_ctrl._mapChannelType0()
            allFields["mapPwEn0"] = _AF6CNC0022_RD_MAP._map_channel_ctrl._mapPwEn0()
            allFields["mapPWIdField0"] = _AF6CNC0022_RD_MAP._map_channel_ctrl._mapPWIdField0()
            return allFields

    class _map_idle_code(AtRegister.AtRegister):
        def name(self):
            return "Map IDLE Code"
    
        def description(self):
            return "The registers provide the idle pattern"
            
        def width(self):
            return 8
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00010800
            
        def endAddress(self):
            return 0xffffffff

        class _IdleCode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IdleCode"
            
            def description(self):
                return "Idle code"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IdleCode"] = _AF6CNC0022_RD_MAP._map_idle_code._IdleCode()
            return allFields

    class _RAM_Map_Parity_Force_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM Map Parity Force Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00010808
            
        def endAddress(self):
            return 0xffffffff

        class _MAPLineCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "MAPLineCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"MAP Thalassa Map Line Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPChlCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MAPChlCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"MAP Thalassa Map Channel Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MAPLineCtrl_ParErrFrc"] = _AF6CNC0022_RD_MAP._RAM_Map_Parity_Force_Control._MAPLineCtrl_ParErrFrc()
            allFields["MAPChlCtrl_ParErrFrc"] = _AF6CNC0022_RD_MAP._RAM_Map_Parity_Force_Control._MAPChlCtrl_ParErrFrc()
            return allFields

    class _RAM_Map_Parity_Disable_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM Map Parity Disable Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00010809
            
        def endAddress(self):
            return 0xffffffff

        class _MAPLineCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "MAPLineCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"MAP Thalassa Map Line Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPChlCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MAPChlCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"MAP Thalassa Map Channel Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MAPLineCtrl_ParErrDis"] = _AF6CNC0022_RD_MAP._RAM_Map_Parity_Disable_Control._MAPLineCtrl_ParErrDis()
            allFields["MAPChlCtrl_ParErrDis"] = _AF6CNC0022_RD_MAP._RAM_Map_Parity_Disable_Control._MAPChlCtrl_ParErrDis()
            return allFields

    class _RAM_Map_Parity_Error_Sticky(AtRegister.AtRegister):
        def name(self):
            return "RAM Map parity Error Sticky"
    
        def description(self):
            return "This register configures disable parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0001080a
            
        def endAddress(self):
            return 0xffffffff

        class _MAPLineCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "MAPLineCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"MAP Thalassa Map Line Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPChlCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MAPChlCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"MAP Thalassa Map Channel Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MAPLineCtrl_ParErrStk"] = _AF6CNC0022_RD_MAP._RAM_Map_Parity_Error_Sticky._MAPLineCtrl_ParErrStk()
            allFields["MAPChlCtrl_ParErrStk"] = _AF6CNC0022_RD_MAP._RAM_Map_Parity_Error_Sticky._MAPChlCtrl_ParErrStk()
            return allFields

    class _RAM_DeMap_Parity_Force_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM DeMap Parity Force Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008218
            
        def endAddress(self):
            return 0xffffffff

        class _DeMAPChlCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DeMAPChlCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"DeMAP Thalassa DeMap Channel Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DeMAPChlCtrl_ParErrFrc"] = _AF6CNC0022_RD_MAP._RAM_DeMap_Parity_Force_Control._DeMAPChlCtrl_ParErrFrc()
            return allFields

    class _RAM_DeMap_Parity_Disable_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM DeMap Parity Disable Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008219
            
        def endAddress(self):
            return 0xffffffff

        class _DeMAPChlCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DeMAPChlCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"DeMAP Thalassa DeMap Channel Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DeMAPChlCtrl_ParErrDis"] = _AF6CNC0022_RD_MAP._RAM_DeMap_Parity_Disable_Control._DeMAPChlCtrl_ParErrDis()
            return allFields

    class _RAM_DeMap_Parity_Error_Sticky(AtRegister.AtRegister):
        def name(self):
            return "RAM DeMap parity Error Sticky"
    
        def description(self):
            return "This register configures disable parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000821a
            
        def endAddress(self):
            return 0xffffffff

        class _DeMAPChlCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DeMAPChlCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"DeMAP Thalassa DeMap Channel Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DeMAPChlCtrl_ParErrStk"] = _AF6CNC0022_RD_MAP._RAM_DeMap_Parity_Error_Sticky._DeMAPChlCtrl_ParErrStk()
            return allFields

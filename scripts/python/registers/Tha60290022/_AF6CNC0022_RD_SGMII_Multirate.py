import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0022_RD_SGMII_Multirate(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["version_pen"] = _AF6CNC0022_RD_SGMII_Multirate._version_pen()
        allRegisters["an_mod_pen0"] = _AF6CNC0022_RD_SGMII_Multirate._an_mod_pen0()
        allRegisters["an_rst_pen0"] = _AF6CNC0022_RD_SGMII_Multirate._an_rst_pen0()
        allRegisters["an_enb_pen0"] = _AF6CNC0022_RD_SGMII_Multirate._an_enb_pen0()
        allRegisters["an_spd_pen0"] = _AF6CNC0022_RD_SGMII_Multirate._an_spd_pen0()
        allRegisters["an_sta_pen00"] = _AF6CNC0022_RD_SGMII_Multirate._an_sta_pen00()
        allRegisters["an_sta_pen01"] = _AF6CNC0022_RD_SGMII_Multirate._an_sta_pen01()
        allRegisters["an_rxab_pen00"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen00()
        allRegisters["an_rxab_pen01"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen01()
        allRegisters["an_rxab_pen02"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen02()
        allRegisters["an_rxab_pen03"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen03()
        allRegisters["an_rxab_pen04"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen04()
        allRegisters["an_rxab_pen05"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen05()
        allRegisters["an_rxab_pen06"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen06()
        allRegisters["an_rxab_pen07"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen07()
        allRegisters["an_rxab_pen00"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen00()
        allRegisters["an_rxab_pen01"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen01()
        allRegisters["an_rxab_pen02"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen02()
        allRegisters["an_rxab_pen03"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen03()
        allRegisters["an_rxab_pen04"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen04()
        allRegisters["an_rxab_pen05"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen05()
        allRegisters["an_rxab_pen06"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen06()
        allRegisters["an_rxab_pen07"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen07()
        allRegisters["TX_IPG_Config_Part0"] = _AF6CNC0022_RD_SGMII_Multirate._TX_IPG_Config_Part0()
        allRegisters["TX_IPG_Config_Part1"] = _AF6CNC0022_RD_SGMII_Multirate._TX_IPG_Config_Part1()
        allRegisters["Ge_Link_State_Current_Status"] = _AF6CNC0022_RD_SGMII_Multirate._Ge_Link_State_Current_Status()
        allRegisters["Ge_Link_State_Sticky"] = _AF6CNC0022_RD_SGMII_Multirate._Ge_Link_State_Sticky()
        allRegisters["Ge_Link_State_Interrupt_enb"] = _AF6CNC0022_RD_SGMII_Multirate._Ge_Link_State_Interrupt_enb()
        allRegisters["los_sync_stk_pen"] = _AF6CNC0022_RD_SGMII_Multirate._los_sync_stk_pen()
        allRegisters["los_sync_pen"] = _AF6CNC0022_RD_SGMII_Multirate._los_sync_pen()
        allRegisters["los_sync_int_enb_pen"] = _AF6CNC0022_RD_SGMII_Multirate._los_sync_int_enb_pen()
        allRegisters["an_sta_stk_pen"] = _AF6CNC0022_RD_SGMII_Multirate._an_sta_stk_pen()
        allRegisters["an_int_enb_pen"] = _AF6CNC0022_RD_SGMII_Multirate._an_int_enb_pen()
        allRegisters["an_rfi_stk_pen"] = _AF6CNC0022_RD_SGMII_Multirate._an_rfi_stk_pen()
        allRegisters["an_rfi_int_enb_pen"] = _AF6CNC0022_RD_SGMII_Multirate._an_rfi_int_enb_pen()
        allRegisters["rxexcer_sta_pen"] = _AF6CNC0022_RD_SGMII_Multirate._rxexcer_sta_pen()
        allRegisters["rxexcer_stk_pen"] = _AF6CNC0022_RD_SGMII_Multirate._rxexcer_stk_pen()
        allRegisters["rxexcer_enb_pen"] = _AF6CNC0022_RD_SGMII_Multirate._rxexcer_enb_pen()
        allRegisters["rxexcer_thres_pen"] = _AF6CNC0022_RD_SGMII_Multirate._rxexcer_thres_pen()
        allRegisters["GE_Interrupt_OR"] = _AF6CNC0022_RD_SGMII_Multirate._GE_Interrupt_OR()
        allRegisters["GE_Loss_Of_Sync_AND_MASK"] = _AF6CNC0022_RD_SGMII_Multirate._GE_Loss_Of_Sync_AND_MASK()
        allRegisters["GE_Auto_neg_State_Change_AND_MASK"] = _AF6CNC0022_RD_SGMII_Multirate._GE_Auto_neg_State_Change_AND_MASK()
        allRegisters["GE_Remote_Fault_AND_MASK"] = _AF6CNC0022_RD_SGMII_Multirate._GE_Remote_Fault_AND_MASK()
        allRegisters["GE_Excessive_Error_Ratio_AND_MASK"] = _AF6CNC0022_RD_SGMII_Multirate._GE_Excessive_Error_Ratio_AND_MASK()
        allRegisters["Ge_Link_State_AND_MASK"] = _AF6CNC0022_RD_SGMII_Multirate._Ge_Link_State_AND_MASK()
        allRegisters["GE_1000basex_Force_K30_7"] = _AF6CNC0022_RD_SGMII_Multirate._GE_1000basex_Force_K30_7()
        allRegisters["fx_txenb"] = _AF6CNC0022_RD_SGMII_Multirate._fx_txenb()
        allRegisters["fx_patstken"] = _AF6CNC0022_RD_SGMII_Multirate._fx_patstken()
        allRegisters["fx_stk_fefd_pen"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen()
        allRegisters["fx_alm_fefd_pen"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen()
        allRegisters["fx_fefd_inten"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten()
        return allRegisters

    class _version_pen(AtRegister.AtRegister):
        def name(self):
            return "Pedit Block Version"
    
        def description(self):
            return "Pedit Block Version"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000800
            
        def endAddress(self):
            return 0xffffffff

        class _day(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "day"
            
            def description(self):
                return "day"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _month(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "month"
            
            def description(self):
                return "month"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _year(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "year"
            
            def description(self):
                return "year"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _number(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "number"
            
            def description(self):
                return "number"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["day"] = _AF6CNC0022_RD_SGMII_Multirate._version_pen._day()
            allFields["month"] = _AF6CNC0022_RD_SGMII_Multirate._version_pen._month()
            allFields["year"] = _AF6CNC0022_RD_SGMII_Multirate._version_pen._year()
            allFields["number"] = _AF6CNC0022_RD_SGMII_Multirate._version_pen._number()
            return allFields

    class _an_mod_pen0(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg-group0 mode"
    
        def description(self):
            return "Select Auto-neg mode for 1000Basex or SGMII"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000801
            
        def endAddress(self):
            return 0xffffffff

        class _tx_enb0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "tx_enb0"
            
            def description(self):
                return "Enable TX side bit per port, bit[16] port 0, bit[31] port 15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _an_mod0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "an_mod0"
            
            def description(self):
                return "Auto-neg mode, bit per port, bit[0] port 0, bit[15] port 15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tx_enb0"] = _AF6CNC0022_RD_SGMII_Multirate._an_mod_pen0._tx_enb0()
            allFields["an_mod0"] = _AF6CNC0022_RD_SGMII_Multirate._an_mod_pen0._an_mod0()
            return allFields

    class _an_rst_pen0(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg-group0 reset"
    
        def description(self):
            return "Restart Auto-neg process of group0 from port 0 to port 15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000802
            
        def endAddress(self):
            return 0xffffffff

        class _an_rst0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "an_rst0"
            
            def description(self):
                return "Auto-neg reset, sw write 1 then write 0 to restart auto-neg bit per port, bit[0] port 0, bit[15] port 15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["an_rst0"] = _AF6CNC0022_RD_SGMII_Multirate._an_rst_pen0._an_rst0()
            return allFields

    class _an_enb_pen0(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg-group0 enable"
    
        def description(self):
            return "enable/disbale Auto-neg of group 0 from port 0 to port 15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000803
            
        def endAddress(self):
            return 0xffffffff

        class _an_enb0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "an_enb0"
            
            def description(self):
                return "Auto-neg enb , bit per port, bit[0] port 0, bit[15] port 15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["an_enb0"] = _AF6CNC0022_RD_SGMII_Multirate._an_enb_pen0._an_enb0()
            return allFields

    class _an_spd_pen0(AtRegister.AtRegister):
        def name(self):
            return "Config_speed-group0"
    
        def description(self):
            return "configure speed when disable Auto-neg of group 0 from port 0 to 15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000804
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_spd15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 30
        
            def name(self):
                return "cfg_spd15"
            
            def description(self):
                return "Speed port 15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 28
        
            def name(self):
                return "cfg_spd14"
            
            def description(self):
                return "Speed port 14"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 26
        
            def name(self):
                return "cfg_spd13"
            
            def description(self):
                return "Speed port 13"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 24
        
            def name(self):
                return "cfg_spd12"
            
            def description(self):
                return "Speed port 12"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 22
        
            def name(self):
                return "cfg_spd11"
            
            def description(self):
                return "Speed port 11"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 20
        
            def name(self):
                return "cfg_spd10"
            
            def description(self):
                return "Speed port 10"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd09(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 18
        
            def name(self):
                return "cfg_spd09"
            
            def description(self):
                return "Speed port 9"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd08(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 16
        
            def name(self):
                return "cfg_spd08"
            
            def description(self):
                return "Speed port 8"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 14
        
            def name(self):
                return "cfg_spd07"
            
            def description(self):
                return "Speed port 7"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "cfg_spd06"
            
            def description(self):
                return "Speed port 6"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "cfg_spd05"
            
            def description(self):
                return "Speed port 5"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "cfg_spd04"
            
            def description(self):
                return "Speed port 4"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "cfg_spd03"
            
            def description(self):
                return "Speed port 3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "cfg_spd02"
            
            def description(self):
                return "Speed port 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfg_spd01"
            
            def description(self):
                return "Speed port 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_spd00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_spd00"
            
            def description(self):
                return "Speed port 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_spd15"] = _AF6CNC0022_RD_SGMII_Multirate._an_spd_pen0._cfg_spd15()
            allFields["cfg_spd14"] = _AF6CNC0022_RD_SGMII_Multirate._an_spd_pen0._cfg_spd14()
            allFields["cfg_spd13"] = _AF6CNC0022_RD_SGMII_Multirate._an_spd_pen0._cfg_spd13()
            allFields["cfg_spd12"] = _AF6CNC0022_RD_SGMII_Multirate._an_spd_pen0._cfg_spd12()
            allFields["cfg_spd11"] = _AF6CNC0022_RD_SGMII_Multirate._an_spd_pen0._cfg_spd11()
            allFields["cfg_spd10"] = _AF6CNC0022_RD_SGMII_Multirate._an_spd_pen0._cfg_spd10()
            allFields["cfg_spd09"] = _AF6CNC0022_RD_SGMII_Multirate._an_spd_pen0._cfg_spd09()
            allFields["cfg_spd08"] = _AF6CNC0022_RD_SGMII_Multirate._an_spd_pen0._cfg_spd08()
            allFields["cfg_spd07"] = _AF6CNC0022_RD_SGMII_Multirate._an_spd_pen0._cfg_spd07()
            allFields["cfg_spd06"] = _AF6CNC0022_RD_SGMII_Multirate._an_spd_pen0._cfg_spd06()
            allFields["cfg_spd05"] = _AF6CNC0022_RD_SGMII_Multirate._an_spd_pen0._cfg_spd05()
            allFields["cfg_spd04"] = _AF6CNC0022_RD_SGMII_Multirate._an_spd_pen0._cfg_spd04()
            allFields["cfg_spd03"] = _AF6CNC0022_RD_SGMII_Multirate._an_spd_pen0._cfg_spd03()
            allFields["cfg_spd02"] = _AF6CNC0022_RD_SGMII_Multirate._an_spd_pen0._cfg_spd02()
            allFields["cfg_spd01"] = _AF6CNC0022_RD_SGMII_Multirate._an_spd_pen0._cfg_spd01()
            allFields["cfg_spd00"] = _AF6CNC0022_RD_SGMII_Multirate._an_spd_pen0._cfg_spd00()
            return allFields

    class _an_sta_pen00(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg-group00 status"
    
        def description(self):
            return "status of Auto-neg of group0 from port 0 to port 7"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000805
            
        def endAddress(self):
            return 0xffffffff

        class _an_sta07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 28
        
            def name(self):
                return "an_sta07"
            
            def description(self):
                return "Speed port 7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 24
        
            def name(self):
                return "an_sta06"
            
            def description(self):
                return "Speed port 6"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 20
        
            def name(self):
                return "an_sta05"
            
            def description(self):
                return "Speed port 5"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "an_sta04"
            
            def description(self):
                return "Speed port 4"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "an_sta03"
            
            def description(self):
                return "Speed port 3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "an_sta02"
            
            def description(self):
                return "Speed port 2"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "an_sta01"
            
            def description(self):
                return "Speed port 1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "an_sta00"
            
            def description(self):
                return "Speed port 0"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["an_sta07"] = _AF6CNC0022_RD_SGMII_Multirate._an_sta_pen00._an_sta07()
            allFields["an_sta06"] = _AF6CNC0022_RD_SGMII_Multirate._an_sta_pen00._an_sta06()
            allFields["an_sta05"] = _AF6CNC0022_RD_SGMII_Multirate._an_sta_pen00._an_sta05()
            allFields["an_sta04"] = _AF6CNC0022_RD_SGMII_Multirate._an_sta_pen00._an_sta04()
            allFields["an_sta03"] = _AF6CNC0022_RD_SGMII_Multirate._an_sta_pen00._an_sta03()
            allFields["an_sta02"] = _AF6CNC0022_RD_SGMII_Multirate._an_sta_pen00._an_sta02()
            allFields["an_sta01"] = _AF6CNC0022_RD_SGMII_Multirate._an_sta_pen00._an_sta01()
            allFields["an_sta00"] = _AF6CNC0022_RD_SGMII_Multirate._an_sta_pen00._an_sta00()
            return allFields

    class _an_sta_pen01(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg-group01 status"
    
        def description(self):
            return "status of Auto-neg of group0 from port 8 to port 15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000806
            
        def endAddress(self):
            return 0xffffffff

        class _an_sta15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 28
        
            def name(self):
                return "an_sta15"
            
            def description(self):
                return "Speed port 15"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 24
        
            def name(self):
                return "an_sta14"
            
            def description(self):
                return "Speed port 14"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 20
        
            def name(self):
                return "an_sta13"
            
            def description(self):
                return "Speed port 13"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "an_sta12"
            
            def description(self):
                return "Speed port 12"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "an_sta11"
            
            def description(self):
                return "Speed port 11"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "an_sta10"
            
            def description(self):
                return "Speed port 10"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta09(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "an_sta09"
            
            def description(self):
                return "Speed port 09"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_sta09(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "an_sta09"
            
            def description(self):
                return "Speed port 09"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["an_sta15"] = _AF6CNC0022_RD_SGMII_Multirate._an_sta_pen01._an_sta15()
            allFields["an_sta14"] = _AF6CNC0022_RD_SGMII_Multirate._an_sta_pen01._an_sta14()
            allFields["an_sta13"] = _AF6CNC0022_RD_SGMII_Multirate._an_sta_pen01._an_sta13()
            allFields["an_sta12"] = _AF6CNC0022_RD_SGMII_Multirate._an_sta_pen01._an_sta12()
            allFields["an_sta11"] = _AF6CNC0022_RD_SGMII_Multirate._an_sta_pen01._an_sta11()
            allFields["an_sta10"] = _AF6CNC0022_RD_SGMII_Multirate._an_sta_pen01._an_sta10()
            allFields["an_sta09"] = _AF6CNC0022_RD_SGMII_Multirate._an_sta_pen01._an_sta09()
            allFields["an_sta09"] = _AF6CNC0022_RD_SGMII_Multirate._an_sta_pen01._an_sta09()
            return allFields

    class _an_rxab_pen00(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX00-RX01 SGMII ability"
    
        def description(self):
            return "RX ability of RX-Port00-01 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a00
            
        def endAddress(self):
            return 0xffffffff

        class _link01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "link01"
            
            def description(self):
                return "Link status of port1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "duplex01"
            
            def description(self):
                return "duplex mode of port1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 26
        
            def name(self):
                return "speed01"
            
            def description(self):
                return "speed of SGMII of port1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _link00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "link00"
            
            def description(self):
                return "Link status of port0"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "duplex00"
            
            def description(self):
                return "duplex mode of port0"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "speed00"
            
            def description(self):
                return "speed of SGMII of port0"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["link01"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen00._link01()
            allFields["duplex01"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen00._duplex01()
            allFields["speed01"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen00._speed01()
            allFields["link00"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen00._link00()
            allFields["duplex00"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen00._duplex00()
            allFields["speed00"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen00._speed00()
            return allFields

    class _an_rxab_pen01(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX02-RX03 SGMII ability"
    
        def description(self):
            return "RX ability of RX-Port02-03 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a01
            
        def endAddress(self):
            return 0xffffffff

        class _link03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "link03"
            
            def description(self):
                return "Link status of port3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "duplex03"
            
            def description(self):
                return "duplex mode of port3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 26
        
            def name(self):
                return "speed03"
            
            def description(self):
                return "speed of SGMII of port3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _link02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "link02"
            
            def description(self):
                return "Link status of port2"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "duplex02"
            
            def description(self):
                return "duplex mode of port2"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "speed02"
            
            def description(self):
                return "speed of SGMII of port2"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["link03"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen01._link03()
            allFields["duplex03"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen01._duplex03()
            allFields["speed03"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen01._speed03()
            allFields["link02"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen01._link02()
            allFields["duplex02"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen01._duplex02()
            allFields["speed02"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen01._speed02()
            return allFields

    class _an_rxab_pen02(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX04-RX05 SGMII ability"
    
        def description(self):
            return "RX ability of RX-Port04-05 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a02
            
        def endAddress(self):
            return 0xffffffff

        class _link05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "link05"
            
            def description(self):
                return "Link status of port5"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "duplex05"
            
            def description(self):
                return "duplex mode of port5"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 26
        
            def name(self):
                return "speed05"
            
            def description(self):
                return "speed of SGMII of port5"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _link04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "link04"
            
            def description(self):
                return "Link status of port4"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "duplex04"
            
            def description(self):
                return "duplex mode of port4"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "speed04"
            
            def description(self):
                return "speed of SGMII of port4"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["link05"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen02._link05()
            allFields["duplex05"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen02._duplex05()
            allFields["speed05"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen02._speed05()
            allFields["link04"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen02._link04()
            allFields["duplex04"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen02._duplex04()
            allFields["speed04"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen02._speed04()
            return allFields

    class _an_rxab_pen03(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX06-RX07 SGMII ability"
    
        def description(self):
            return "RX ability of RX-Port06-07 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a03
            
        def endAddress(self):
            return 0xffffffff

        class _link07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "link07"
            
            def description(self):
                return "Link status of port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "duplex07"
            
            def description(self):
                return "duplex mode of port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 26
        
            def name(self):
                return "speed07"
            
            def description(self):
                return "speed of SGMII of port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _link06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "link06"
            
            def description(self):
                return "Link status of port6"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "duplex06"
            
            def description(self):
                return "duplex mode of port6"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "speed06"
            
            def description(self):
                return "speed of SGMII of port6"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["link07"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen03._link07()
            allFields["duplex07"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen03._duplex07()
            allFields["speed07"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen03._speed07()
            allFields["link06"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen03._link06()
            allFields["duplex06"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen03._duplex06()
            allFields["speed06"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen03._speed06()
            return allFields

    class _an_rxab_pen04(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX08-RX09 SGMII ability"
    
        def description(self):
            return "RX ability of RX-Port08-09 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a04
            
        def endAddress(self):
            return 0xffffffff

        class _link09(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "link09"
            
            def description(self):
                return "Link status of port9"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex09(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "duplex09"
            
            def description(self):
                return "duplex mode of port9"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed09(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 26
        
            def name(self):
                return "speed09"
            
            def description(self):
                return "speed of SGMII of port9"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _link08(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "link08"
            
            def description(self):
                return "Link status of port8"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex08(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "duplex08"
            
            def description(self):
                return "duplex mode of port8"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed08(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "speed08"
            
            def description(self):
                return "speed of SGMII of port8"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["link09"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen04._link09()
            allFields["duplex09"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen04._duplex09()
            allFields["speed09"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen04._speed09()
            allFields["link08"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen04._link08()
            allFields["duplex08"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen04._duplex08()
            allFields["speed08"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen04._speed08()
            return allFields

    class _an_rxab_pen05(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX10-RX11 SGMII ability"
    
        def description(self):
            return "RX ability of RX-Port10-11 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a05
            
        def endAddress(self):
            return 0xffffffff

        class _link11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "link11"
            
            def description(self):
                return "Link status of port11"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "duplex11"
            
            def description(self):
                return "duplex mode of port11"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 26
        
            def name(self):
                return "speed11"
            
            def description(self):
                return "speed of SGMII of port11"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _link10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "link10"
            
            def description(self):
                return "Link status of port10"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "duplex10"
            
            def description(self):
                return "duplex mode of port10"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "speed10"
            
            def description(self):
                return "speed of SGMII of port10"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["link11"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen05._link11()
            allFields["duplex11"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen05._duplex11()
            allFields["speed11"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen05._speed11()
            allFields["link10"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen05._link10()
            allFields["duplex10"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen05._duplex10()
            allFields["speed10"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen05._speed10()
            return allFields

    class _an_rxab_pen06(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX12-RX13 SGMII ability"
    
        def description(self):
            return "RX ability of RX-Port12-13 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a06
            
        def endAddress(self):
            return 0xffffffff

        class _link13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "link13"
            
            def description(self):
                return "Link status of port13"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "duplex13"
            
            def description(self):
                return "duplex mode of port13"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 26
        
            def name(self):
                return "speed13"
            
            def description(self):
                return "speed of SGMII of port13"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _link12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "link12"
            
            def description(self):
                return "Link status of port12"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "duplex12"
            
            def description(self):
                return "duplex mode of port12"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "speed12"
            
            def description(self):
                return "speed of SGMII of port12"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["link13"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen06._link13()
            allFields["duplex13"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen06._duplex13()
            allFields["speed13"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen06._speed13()
            allFields["link12"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen06._link12()
            allFields["duplex12"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen06._duplex12()
            allFields["speed12"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen06._speed12()
            return allFields

    class _an_rxab_pen07(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX14-RX15 SGMII ability"
    
        def description(self):
            return "RX ability of RX-Port14-15 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a07
            
        def endAddress(self):
            return 0xffffffff

        class _link15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "link15"
            
            def description(self):
                return "Link status of port15"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "duplex15"
            
            def description(self):
                return "duplex mode of port15"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 26
        
            def name(self):
                return "speed15"
            
            def description(self):
                return "speed of SGMII of port15"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _link14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "link14"
            
            def description(self):
                return "Link status of port14"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _duplex14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "duplex14"
            
            def description(self):
                return "duplex mode of port14"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _speed14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "speed14"
            
            def description(self):
                return "speed of SGMII of port14"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["link15"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen07._link15()
            allFields["duplex15"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen07._duplex15()
            allFields["speed15"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen07._speed15()
            allFields["link14"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen07._link14()
            allFields["duplex14"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen07._duplex14()
            allFields["speed14"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen07._speed14()
            return allFields

    class _an_rxab_pen00(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX00-RX01 1000Basex ability"
    
        def description(self):
            return "RX ability of RX-Port00-01 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a00
            
        def endAddress(self):
            return 0xffffffff

        class _NP01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "NP01"
            
            def description(self):
                return "Next page of port1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Ack01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "Ack01"
            
            def description(self):
                return "Acknowledge of port1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ReFault01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 28
        
            def name(self):
                return "ReFault01"
            
            def description(self):
                return "Remote Fault of port1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Hduplex01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "Hduplex01"
            
            def description(self):
                return "Half duplex of port1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Fduplex01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Fduplex01"
            
            def description(self):
                return "Full duplex of port1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _NP00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "NP00"
            
            def description(self):
                return "Next page of port0"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Ack00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Ack00"
            
            def description(self):
                return "Acknowledge of port0"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ReFault00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "ReFault00"
            
            def description(self):
                return "Remote Fault of port0"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Hduplex00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Hduplex00"
            
            def description(self):
                return "Half duplex of port0"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Fduplex00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Fduplex00"
            
            def description(self):
                return "Full duplex of port0"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["NP01"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen00._NP01()
            allFields["Ack01"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen00._Ack01()
            allFields["ReFault01"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen00._ReFault01()
            allFields["Hduplex01"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen00._Hduplex01()
            allFields["Fduplex01"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen00._Fduplex01()
            allFields["NP00"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen00._NP00()
            allFields["Ack00"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen00._Ack00()
            allFields["ReFault00"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen00._ReFault00()
            allFields["Hduplex00"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen00._Hduplex00()
            allFields["Fduplex00"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen00._Fduplex00()
            return allFields

    class _an_rxab_pen01(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX02-RX03 1000Basex ability"
    
        def description(self):
            return "RX ability of RX-Port02-03 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a01
            
        def endAddress(self):
            return 0xffffffff

        class _NP03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "NP03"
            
            def description(self):
                return "Next page  of port3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Ack03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "Ack03"
            
            def description(self):
                return "Acknowledge of port3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ReFault03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 28
        
            def name(self):
                return "ReFault03"
            
            def description(self):
                return "Remote Fault of port3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Hduplex03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "Hduplex03"
            
            def description(self):
                return "Half duplex of port3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Fduplex03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Fduplex03"
            
            def description(self):
                return "Full duplex of port3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _NP02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "NP02"
            
            def description(self):
                return "Next page of port2"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Ack02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Ack02"
            
            def description(self):
                return "Acknowledge of port2"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ReFault02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "ReFault02"
            
            def description(self):
                return "Remote Fault of port2"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Hduplex02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Hduplex02"
            
            def description(self):
                return "Half duplex of port2"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Fduplex02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Fduplex02"
            
            def description(self):
                return "Full duplex of port2"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["NP03"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen01._NP03()
            allFields["Ack03"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen01._Ack03()
            allFields["ReFault03"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen01._ReFault03()
            allFields["Hduplex03"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen01._Hduplex03()
            allFields["Fduplex03"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen01._Fduplex03()
            allFields["NP02"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen01._NP02()
            allFields["Ack02"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen01._Ack02()
            allFields["ReFault02"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen01._ReFault02()
            allFields["Hduplex02"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen01._Hduplex02()
            allFields["Fduplex02"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen01._Fduplex02()
            return allFields

    class _an_rxab_pen02(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX04-RX05 1000Basex ability"
    
        def description(self):
            return "RX ability of RX-Port04-05 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a02
            
        def endAddress(self):
            return 0xffffffff

        class _NP05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "NP05"
            
            def description(self):
                return "Next page  of port5"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Ack05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "Ack05"
            
            def description(self):
                return "Acknowledge of port5"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ReFault05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 28
        
            def name(self):
                return "ReFault05"
            
            def description(self):
                return "Remote Fault of port5"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Hduplex05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "Hduplex05"
            
            def description(self):
                return "Half duplex of port5"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Fduplex05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Fduplex05"
            
            def description(self):
                return "Full duplex of port5"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _NP04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "NP04"
            
            def description(self):
                return "Next page of port4"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Ack04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Ack04"
            
            def description(self):
                return "Acknowledge of port4"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ReFault04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "ReFault04"
            
            def description(self):
                return "Remote Fault of port4"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Hduplex04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Hduplex04"
            
            def description(self):
                return "Half duplex of port4"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Fduplex04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Fduplex04"
            
            def description(self):
                return "Full duplex of port4"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["NP05"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen02._NP05()
            allFields["Ack05"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen02._Ack05()
            allFields["ReFault05"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen02._ReFault05()
            allFields["Hduplex05"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen02._Hduplex05()
            allFields["Fduplex05"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen02._Fduplex05()
            allFields["NP04"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen02._NP04()
            allFields["Ack04"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen02._Ack04()
            allFields["ReFault04"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen02._ReFault04()
            allFields["Hduplex04"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen02._Hduplex04()
            allFields["Fduplex04"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen02._Fduplex04()
            return allFields

    class _an_rxab_pen03(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX06-RX07 1000Basex ability"
    
        def description(self):
            return "RX ability of RX-Port06-07 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a03
            
        def endAddress(self):
            return 0xffffffff

        class _NP07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "NP07"
            
            def description(self):
                return "Next page  of port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Ack07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "Ack07"
            
            def description(self):
                return "Acknowledge of port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ReFault07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 28
        
            def name(self):
                return "ReFault07"
            
            def description(self):
                return "Remote Fault of port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Hduplex07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "Hduplex07"
            
            def description(self):
                return "Half duplex of port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Fduplex07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Fduplex07"
            
            def description(self):
                return "Full duplex of port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _NP06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "NP06"
            
            def description(self):
                return "Next page of port6"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Ack06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Ack06"
            
            def description(self):
                return "Acknowledge of port6"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ReFault06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "ReFault06"
            
            def description(self):
                return "Remote Fault of port6"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Hduplex06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Hduplex06"
            
            def description(self):
                return "Half duplex of port6"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Fduplex06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Fduplex06"
            
            def description(self):
                return "Full duplex of port6"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["NP07"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen03._NP07()
            allFields["Ack07"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen03._Ack07()
            allFields["ReFault07"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen03._ReFault07()
            allFields["Hduplex07"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen03._Hduplex07()
            allFields["Fduplex07"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen03._Fduplex07()
            allFields["NP06"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen03._NP06()
            allFields["Ack06"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen03._Ack06()
            allFields["ReFault06"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen03._ReFault06()
            allFields["Hduplex06"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen03._Hduplex06()
            allFields["Fduplex06"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen03._Fduplex06()
            return allFields

    class _an_rxab_pen04(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX08-RX09 1000Basex ability"
    
        def description(self):
            return "RX ability of RX-Port08-09 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a04
            
        def endAddress(self):
            return 0xffffffff

        class _NP09(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "NP09"
            
            def description(self):
                return "Next page  of port9"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Ack09(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "Ack09"
            
            def description(self):
                return "Acknowledge of port9"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ReFault09(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 28
        
            def name(self):
                return "ReFault09"
            
            def description(self):
                return "Remote Fault of port9"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Hduplex09(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "Hduplex09"
            
            def description(self):
                return "Half duplex of port9"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Fduplex09(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Fduplex09"
            
            def description(self):
                return "Full duplex of port9"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _NP08(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "NP08"
            
            def description(self):
                return "Next page of port8"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Ack08(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Ack08"
            
            def description(self):
                return "Acknowledge of port8"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ReFault08(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "ReFault08"
            
            def description(self):
                return "Remote Fault of port8"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Hduplex08(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Hduplex08"
            
            def description(self):
                return "Half duplex of port8"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Fduplex08(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Fduplex08"
            
            def description(self):
                return "Full duplex of port8"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["NP09"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen04._NP09()
            allFields["Ack09"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen04._Ack09()
            allFields["ReFault09"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen04._ReFault09()
            allFields["Hduplex09"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen04._Hduplex09()
            allFields["Fduplex09"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen04._Fduplex09()
            allFields["NP08"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen04._NP08()
            allFields["Ack08"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen04._Ack08()
            allFields["ReFault08"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen04._ReFault08()
            allFields["Hduplex08"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen04._Hduplex08()
            allFields["Fduplex08"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen04._Fduplex08()
            return allFields

    class _an_rxab_pen05(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX10-RX11 1000Basex ability"
    
        def description(self):
            return "RX ability of RX-Port10-11 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a05
            
        def endAddress(self):
            return 0xffffffff

        class _NP11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "NP11"
            
            def description(self):
                return "Next page  of port11"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Ack11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "Ack11"
            
            def description(self):
                return "Acknowledge of port11"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ReFault11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 28
        
            def name(self):
                return "ReFault11"
            
            def description(self):
                return "Remote Fault of port11"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Hduplex11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "Hduplex11"
            
            def description(self):
                return "Half duplex of port11"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Fduplex11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Fduplex11"
            
            def description(self):
                return "Full duplex of port11"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _NP10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "NP10"
            
            def description(self):
                return "Next page of port10"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Ack10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Ack10"
            
            def description(self):
                return "Acknowledge of port10"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ReFault10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "ReFault10"
            
            def description(self):
                return "Remote Fault of port10"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Hduplex10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Hduplex10"
            
            def description(self):
                return "Half duplex of port10"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Fduplex10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Fduplex10"
            
            def description(self):
                return "Full duplex of port10"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["NP11"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen05._NP11()
            allFields["Ack11"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen05._Ack11()
            allFields["ReFault11"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen05._ReFault11()
            allFields["Hduplex11"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen05._Hduplex11()
            allFields["Fduplex11"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen05._Fduplex11()
            allFields["NP10"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen05._NP10()
            allFields["Ack10"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen05._Ack10()
            allFields["ReFault10"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen05._ReFault10()
            allFields["Hduplex10"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen05._Hduplex10()
            allFields["Fduplex10"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen05._Fduplex10()
            return allFields

    class _an_rxab_pen06(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX12-RX13 1000Basex ability"
    
        def description(self):
            return "RX ability of RX-Port12-13 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a06
            
        def endAddress(self):
            return 0xffffffff

        class _NP13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "NP13"
            
            def description(self):
                return "Next page  of port13"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Ack13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "Ack13"
            
            def description(self):
                return "Acknowledge of port13"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ReFault13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 28
        
            def name(self):
                return "ReFault13"
            
            def description(self):
                return "Remote Fault of port13"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Hduplex13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "Hduplex13"
            
            def description(self):
                return "Half duplex of port13"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Fduplex13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Fduplex13"
            
            def description(self):
                return "Full duplex of port13"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _NP12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "NP12"
            
            def description(self):
                return "Next page of port12"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Ack12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Ack12"
            
            def description(self):
                return "Acknowledge of port12"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ReFault12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "ReFault12"
            
            def description(self):
                return "Remote Fault of port12"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Hduplex12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Hduplex12"
            
            def description(self):
                return "Half duplex of port12"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Fduplex12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Fduplex12"
            
            def description(self):
                return "Full duplex of port12"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["NP13"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen06._NP13()
            allFields["Ack13"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen06._Ack13()
            allFields["ReFault13"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen06._ReFault13()
            allFields["Hduplex13"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen06._Hduplex13()
            allFields["Fduplex13"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen06._Fduplex13()
            allFields["NP12"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen06._NP12()
            allFields["Ack12"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen06._Ack12()
            allFields["ReFault12"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen06._ReFault12()
            allFields["Hduplex12"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen06._Hduplex12()
            allFields["Fduplex12"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen06._Fduplex12()
            return allFields

    class _an_rxab_pen07(AtRegister.AtRegister):
        def name(self):
            return "Auto-neg RX14-RX15 1000Basex ability"
    
        def description(self):
            return "RX ability of RX-Port12-13 after Auto-neg succeed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a07
            
        def endAddress(self):
            return 0xffffffff

        class _NP15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "NP15"
            
            def description(self):
                return "Next page  of port15"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Ack15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "Ack15"
            
            def description(self):
                return "Acknowledge of port15"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ReFault15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 28
        
            def name(self):
                return "ReFault15"
            
            def description(self):
                return "Remote Fault of port15"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Hduplex15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "Hduplex15"
            
            def description(self):
                return "Half duplex of port15"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Fduplex15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Fduplex15"
            
            def description(self):
                return "Full duplex of port15"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _NP14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "NP14"
            
            def description(self):
                return "Next page of port14"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Ack14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Ack14"
            
            def description(self):
                return "Acknowledge of port14"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ReFault14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "ReFault14"
            
            def description(self):
                return "Remote Fault of port14"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Hduplex14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Hduplex14"
            
            def description(self):
                return "Half duplex of port14"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Fduplex14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Fduplex14"
            
            def description(self):
                return "Full duplex of port14"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["NP15"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen07._NP15()
            allFields["Ack15"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen07._Ack15()
            allFields["ReFault15"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen07._ReFault15()
            allFields["Hduplex15"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen07._Hduplex15()
            allFields["Fduplex15"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen07._Fduplex15()
            allFields["NP14"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen07._NP14()
            allFields["Ack14"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen07._Ack14()
            allFields["ReFault14"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen07._ReFault14()
            allFields["Hduplex14"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen07._Hduplex14()
            allFields["Fduplex14"] = _AF6CNC0022_RD_SGMII_Multirate._an_rxab_pen07._Fduplex14()
            return allFields

    class _TX_IPG_Config_Part0(AtRegister.AtRegister):
        def name(self):
            return "TX IPG Config Part0"
    
        def description(self):
            return "TX IPG Config Part0, port#00 to port#07"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000810
            
        def endAddress(self):
            return 0xffffffff

        class _txipg_port07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 28
        
            def name(self):
                return "txipg_port07"
            
            def description(self):
                return "TX IPG port07, IPG = txipg_port07 * 2, default value is 6, it means IPG is 12bytes"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _txipg_port06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 24
        
            def name(self):
                return "txipg_port06"
            
            def description(self):
                return "TX IPG port06, IPG = txipg_port06 * 2, default value is 6, it means IPG is 12bytes"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _txipg_port05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 20
        
            def name(self):
                return "txipg_port05"
            
            def description(self):
                return "TX IPG port05, IPG = txipg_port05 * 2, default value is 6, it means IPG is 12bytes"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _txipg_port04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "txipg_port04"
            
            def description(self):
                return "TX IPG port04, IPG = txipg_port04 * 2, default value is 6, it means IPG is 12bytes"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _txipg_port03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "txipg_port03"
            
            def description(self):
                return "TX IPG port03, IPG = txipg_port03 * 2, default value is 6, it means IPG is 12bytes"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _txipg_port02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "txipg_port02"
            
            def description(self):
                return "TX IPG port02, IPG = txipg_port02 * 2, default value is 6, it means IPG is 12bytes"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _txipg_port01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "txipg_port01"
            
            def description(self):
                return "TX IPG port01, IPG = txipg_port01 * 2, default value is 6, it means IPG is 12bytes"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _txipg_port00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txipg_port00"
            
            def description(self):
                return "TX IPG port00, IPG = txipg_port00 * 2, default value is 6, it means IPG is 12bytes"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txipg_port07"] = _AF6CNC0022_RD_SGMII_Multirate._TX_IPG_Config_Part0._txipg_port07()
            allFields["txipg_port06"] = _AF6CNC0022_RD_SGMII_Multirate._TX_IPG_Config_Part0._txipg_port06()
            allFields["txipg_port05"] = _AF6CNC0022_RD_SGMII_Multirate._TX_IPG_Config_Part0._txipg_port05()
            allFields["txipg_port04"] = _AF6CNC0022_RD_SGMII_Multirate._TX_IPG_Config_Part0._txipg_port04()
            allFields["txipg_port03"] = _AF6CNC0022_RD_SGMII_Multirate._TX_IPG_Config_Part0._txipg_port03()
            allFields["txipg_port02"] = _AF6CNC0022_RD_SGMII_Multirate._TX_IPG_Config_Part0._txipg_port02()
            allFields["txipg_port01"] = _AF6CNC0022_RD_SGMII_Multirate._TX_IPG_Config_Part0._txipg_port01()
            allFields["txipg_port00"] = _AF6CNC0022_RD_SGMII_Multirate._TX_IPG_Config_Part0._txipg_port00()
            return allFields

    class _TX_IPG_Config_Part1(AtRegister.AtRegister):
        def name(self):
            return "TX IPG Config Part1"
    
        def description(self):
            return "TX IPG Config Part1, port#08 to port#15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000811
            
        def endAddress(self):
            return 0xffffffff

        class _txipg_port15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 28
        
            def name(self):
                return "txipg_port15"
            
            def description(self):
                return "TX IPG port15, IPG = txipg_port15 * 2, default value is 6, it means IPG is 12bytes"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _txipg_port14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 24
        
            def name(self):
                return "txipg_port14"
            
            def description(self):
                return "TX IPG port14, IPG = txipg_port14 * 2, default value is 6, it means IPG is 12bytes"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _txipg_port13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 20
        
            def name(self):
                return "txipg_port13"
            
            def description(self):
                return "TX IPG port13, IPG = txipg_port13 * 2, default value is 6, it means IPG is 12bytes"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _txipg_port12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "txipg_port12"
            
            def description(self):
                return "TX IPG port12, IPG = txipg_port12 * 2, default value is 6, it means IPG is 12bytes"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _txipg_port11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "txipg_port11"
            
            def description(self):
                return "TX IPG port11, IPG = txipg_port11 * 2, default value is 6, it means IPG is 12bytes"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _txipg_port10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "txipg_port10"
            
            def description(self):
                return "TX IPG port10, IPG = txipg_port10 * 2, default value is 6, it means IPG is 12bytes"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _txipg_port09(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "txipg_port09"
            
            def description(self):
                return "TX IPG port09, IPG = txipg_port09 * 2, default value is 6, it means IPG is 12bytes"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _txipg_port08(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txipg_port08"
            
            def description(self):
                return "TX IPG port08, IPG = txipg_port08 * 2, default value is 6, it means IPG is 12bytes"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txipg_port15"] = _AF6CNC0022_RD_SGMII_Multirate._TX_IPG_Config_Part1._txipg_port15()
            allFields["txipg_port14"] = _AF6CNC0022_RD_SGMII_Multirate._TX_IPG_Config_Part1._txipg_port14()
            allFields["txipg_port13"] = _AF6CNC0022_RD_SGMII_Multirate._TX_IPG_Config_Part1._txipg_port13()
            allFields["txipg_port12"] = _AF6CNC0022_RD_SGMII_Multirate._TX_IPG_Config_Part1._txipg_port12()
            allFields["txipg_port11"] = _AF6CNC0022_RD_SGMII_Multirate._TX_IPG_Config_Part1._txipg_port11()
            allFields["txipg_port10"] = _AF6CNC0022_RD_SGMII_Multirate._TX_IPG_Config_Part1._txipg_port10()
            allFields["txipg_port09"] = _AF6CNC0022_RD_SGMII_Multirate._TX_IPG_Config_Part1._txipg_port09()
            allFields["txipg_port08"] = _AF6CNC0022_RD_SGMII_Multirate._TX_IPG_Config_Part1._txipg_port08()
            return allFields

    class _Ge_Link_State_Current_Status(AtRegister.AtRegister):
        def name(self):
            return "Ge Link State Current Status"
    
        def description(self):
            return "Link status of group 0 from port 0  to port 15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000808
            
        def endAddress(self):
            return 0xffffffff

        class _lnk_sta_cur(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lnk_sta_cur"
            
            def description(self):
                return "Link status, bit per port, bit[0] port  0, bit[7] port 7 ... , bit[15] port 15"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lnk_sta_cur"] = _AF6CNC0022_RD_SGMII_Multirate._Ge_Link_State_Current_Status._lnk_sta_cur()
            return allFields

    class _Ge_Link_State_Sticky(AtRegister.AtRegister):
        def name(self):
            return "Ge Link State Sticky"
    
        def description(self):
            return "Link status of group 0 from port 0  to port 15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000809
            
        def endAddress(self):
            return 0xffffffff

        class _lnk_sta_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lnk_sta_stk"
            
            def description(self):
                return "Link status, bit per port, bit[0] port  0, bit[7] port 7 ... , bit[15] port 15"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lnk_sta_stk"] = _AF6CNC0022_RD_SGMII_Multirate._Ge_Link_State_Sticky._lnk_sta_stk()
            return allFields

    class _Ge_Link_State_Interrupt_enb(AtRegister.AtRegister):
        def name(self):
            return "Ge Link State Interrupt Enb"
    
        def description(self):
            return "Link status of group 0 from port 0  to port 15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000815
            
        def endAddress(self):
            return 0xffffffff

        class _lnk_sta_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lnk_sta_enb"
            
            def description(self):
                return "Link status, bit per port, bit[0] port  0, bit[7] port 7 ... , bit[15] port 15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lnk_sta_enb"] = _AF6CNC0022_RD_SGMII_Multirate._Ge_Link_State_Interrupt_enb._lnk_sta_enb()
            return allFields

    class _los_sync_stk_pen(AtRegister.AtRegister):
        def name(self):
            return "GE Loss Of Synchronization Sticky"
    
        def description(self):
            return "Sticky state change Loss of synchronization"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000080b
            
        def endAddress(self):
            return 0xffffffff

        class _GeLossOfSync_Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GeLossOfSync_Stk"
            
            def description(self):
                return "Sticky state change of Loss Of Synchronization, bit per port, bit0 is port0, bit7 is port7 ,..., bit15 is port15"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GeLossOfSync_Stk"] = _AF6CNC0022_RD_SGMII_Multirate._los_sync_stk_pen._GeLossOfSync_Stk()
            return allFields

    class _los_sync_pen(AtRegister.AtRegister):
        def name(self):
            return "GE Loss Of Synchronization Status"
    
        def description(self):
            return "Current status of Loss of synchronization"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000080a
            
        def endAddress(self):
            return 0xffffffff

        class _GeLossOfSync_Cur(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GeLossOfSync_Cur"
            
            def description(self):
                return "Current status of Loss Of Synchronization, bit per port, bit0 is port0, bit7 is port7 ,..., bit15 is port15"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GeLossOfSync_Cur"] = _AF6CNC0022_RD_SGMII_Multirate._los_sync_pen._GeLossOfSync_Cur()
            return allFields

    class _los_sync_int_enb_pen(AtRegister.AtRegister):
        def name(self):
            return "GE Loss Of Synchronization Interrupt Enb"
    
        def description(self):
            return "Interrupt enable of Loss of synchronization"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000818
            
        def endAddress(self):
            return 0xffffffff

        class _GeLossOfSync_Enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GeLossOfSync_Enb"
            
            def description(self):
                return "Interrupt enable of Loss of synchronization, bit per port, bit0 is port0, bit7 is port7 ,..., bit15 is port15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GeLossOfSync_Enb"] = _AF6CNC0022_RD_SGMII_Multirate._los_sync_int_enb_pen._GeLossOfSync_Enb()
            return allFields

    class _an_sta_stk_pen(AtRegister.AtRegister):
        def name(self):
            return "GE Auto-neg Sticky"
    
        def description(self):
            return "Sticky state change Auto-neg"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000807
            
        def endAddress(self):
            return 0xffffffff

        class _GeAuto_neg_Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GeAuto_neg_Stk"
            
            def description(self):
                return "Sticky state change of Auto-neg , bit per port, bit0 is port0, bit7 is port7 ,..., bit15 is port15"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GeAuto_neg_Stk"] = _AF6CNC0022_RD_SGMII_Multirate._an_sta_stk_pen._GeAuto_neg_Stk()
            return allFields

    class _an_int_enb_pen(AtRegister.AtRegister):
        def name(self):
            return "GE Auto-neg Interrupt Enb"
    
        def description(self):
            return "Interrupt enable of Loss of synchronization"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000817
            
        def endAddress(self):
            return 0xffffffff

        class _GeAuto_neg_Enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GeAuto_neg_Enb"
            
            def description(self):
                return "Interrupt enable of Auto-neg, bit per port, bit0 is port0, bit7 is port7 ,... , bit15 is port15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GeAuto_neg_Enb"] = _AF6CNC0022_RD_SGMII_Multirate._an_int_enb_pen._GeAuto_neg_Enb()
            return allFields

    class _an_rfi_stk_pen(AtRegister.AtRegister):
        def name(self):
            return "GE Remote Fault Sticky"
    
        def description(self):
            return "Sticky state change Remote Fault"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000819
            
        def endAddress(self):
            return 0xffffffff

        class _GeRemote_Fault_Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GeRemote_Fault_Stk"
            
            def description(self):
                return "Sticky state change of Remote Fault , bit per port, bit0 is port0, bit7 is port7 ,..., bit15 is port15"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GeRemote_Fault_Stk"] = _AF6CNC0022_RD_SGMII_Multirate._an_rfi_stk_pen._GeRemote_Fault_Stk()
            return allFields

    class _an_rfi_int_enb_pen(AtRegister.AtRegister):
        def name(self):
            return "GE Remote Fault Interrupt Enb"
    
        def description(self):
            return "Interrupt enable of Remote Fault"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000081a
            
        def endAddress(self):
            return 0xffffffff

        class _GeRemote_Fault_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GeRemote_Fault_enb"
            
            def description(self):
                return "Interrupt enable of Remote Fault, bit per port, bit0 is port0, bit7 is port7 , bit15 is port15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GeRemote_Fault_enb"] = _AF6CNC0022_RD_SGMII_Multirate._an_rfi_int_enb_pen._GeRemote_Fault_enb()
            return allFields

    class _rxexcer_sta_pen(AtRegister.AtRegister):
        def name(self):
            return "GE Excessive Error Ratio Status"
    
        def description(self):
            return "Interrupt Excessive Error Ratio Status"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000081c
            
        def endAddress(self):
            return 0xffffffff

        class _rxexcer_sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxexcer_sta"
            
            def description(self):
                return "Interrupt status of Excessive Error Ratio, bit per port, bit0 is port0, bit7 is port7"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxexcer_sta"] = _AF6CNC0022_RD_SGMII_Multirate._rxexcer_sta_pen._rxexcer_sta()
            return allFields

    class _rxexcer_stk_pen(AtRegister.AtRegister):
        def name(self):
            return "GE Excessive Error Ratio Sticky"
    
        def description(self):
            return "Interrupt Excessive Error Ratio Sticky"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000081d
            
        def endAddress(self):
            return 0xffffffff

        class _rxexcer_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxexcer_stk"
            
            def description(self):
                return "Interrupt sticky of Excessive Error Ratio, bit per port, bit0 is port0, bit7 is port7"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxexcer_stk"] = _AF6CNC0022_RD_SGMII_Multirate._rxexcer_stk_pen._rxexcer_stk()
            return allFields

    class _rxexcer_enb_pen(AtRegister.AtRegister):
        def name(self):
            return "GE Excessive Error Ratio Enble"
    
        def description(self):
            return "Interrupt Excessive Error Ratio Enable"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000081e
            
        def endAddress(self):
            return 0xffffffff

        class _rxexcer_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxexcer_enb"
            
            def description(self):
                return "Interrupt Enable of Excessive Error Ratio, bit per port, bit0 is port0, bit7 is port7"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxexcer_enb"] = _AF6CNC0022_RD_SGMII_Multirate._rxexcer_enb_pen._rxexcer_enb()
            return allFields

    class _rxexcer_thres_pen(AtRegister.AtRegister):
        def name(self):
            return "GE Excessive Error Ratio Threshold"
    
        def description(self):
            return "Interrupt Excessive Error Ratio Threshold"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000835
            
        def endAddress(self):
            return 0xffffffff

        class _rxexcer_thres_up(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "rxexcer_thres_up"
            
            def description(self):
                return "Upper threshold of Excessive Error Ratio, max value is 32767 Excessive error alarm will raise when value error counter is greater than upper threshold"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _rxexcer_thres_low(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxexcer_thres_low"
            
            def description(self):
                return "Lower threshold of Excessive Error Ratio, max value is 32767 Excessive error alarm will clear when value error counter is lower than upper threshold"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxexcer_thres_up"] = _AF6CNC0022_RD_SGMII_Multirate._rxexcer_thres_pen._rxexcer_thres_up()
            allFields["rxexcer_thres_low"] = _AF6CNC0022_RD_SGMII_Multirate._rxexcer_thres_pen._rxexcer_thres_low()
            return allFields

    class _GE_Interrupt_OR(AtRegister.AtRegister):
        def name(self):
            return "GE Interrupt OR"
    
        def description(self):
            return "Interrupt OR of all port per interrupt type"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000830
            
        def endAddress(self):
            return 0xffffffff

        class _link_sta_int_or(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "link_sta_int_or"
            
            def description(self):
                return "Interrupt OR of Ge Link Sate"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _fx100base_int_or(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "fx100base_int_or"
            
            def description(self):
                return "Interrupt OR of 100Base Fx"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _rxexcer_int_or(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "rxexcer_int_or"
            
            def description(self):
                return "Interrupt OR of Excessive Error Ratio"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _an_rfi_int_or(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "an_rfi_int_or"
            
            def description(self):
                return "Interrupt OR of Remote fault"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ant_int_or(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "ant_int_or"
            
            def description(self):
                return "Interrupt OR of Auto-neg state change"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _los_sync_int_or(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "los_sync_int_or"
            
            def description(self):
                return "Interrupt OR of Loss of sync"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["link_sta_int_or"] = _AF6CNC0022_RD_SGMII_Multirate._GE_Interrupt_OR._link_sta_int_or()
            allFields["fx100base_int_or"] = _AF6CNC0022_RD_SGMII_Multirate._GE_Interrupt_OR._fx100base_int_or()
            allFields["rxexcer_int_or"] = _AF6CNC0022_RD_SGMII_Multirate._GE_Interrupt_OR._rxexcer_int_or()
            allFields["an_rfi_int_or"] = _AF6CNC0022_RD_SGMII_Multirate._GE_Interrupt_OR._an_rfi_int_or()
            allFields["ant_int_or"] = _AF6CNC0022_RD_SGMII_Multirate._GE_Interrupt_OR._ant_int_or()
            allFields["los_sync_int_or"] = _AF6CNC0022_RD_SGMII_Multirate._GE_Interrupt_OR._los_sync_int_or()
            return allFields

    class _GE_Loss_Of_Sync_AND_MASK(AtRegister.AtRegister):
        def name(self):
            return "GE Loss Of Sync AND MASK"
    
        def description(self):
            return "Interrupt Loss of sync current status AND MASK"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000831
            
        def endAddress(self):
            return 0xffffffff

        class _GeLossOfSync_and_mask(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GeLossOfSync_and_mask"
            
            def description(self):
                return "Interrupt Loss of sync, bit per port, bit0 is port0, bit7 is port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GeLossOfSync_and_mask"] = _AF6CNC0022_RD_SGMII_Multirate._GE_Loss_Of_Sync_AND_MASK._GeLossOfSync_and_mask()
            return allFields

    class _GE_Auto_neg_State_Change_AND_MASK(AtRegister.AtRegister):
        def name(self):
            return "GE Auto_neg State Change AND MASK"
    
        def description(self):
            return "Interrupt Auto-neg state change current status AND MASK"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000832
            
        def endAddress(self):
            return 0xffffffff

        class _GeAuto_neg_and_mask(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GeAuto_neg_and_mask"
            
            def description(self):
                return "Interrupt Auto-neg state change, bit per port, bit0 is port0, bit7 is port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GeAuto_neg_and_mask"] = _AF6CNC0022_RD_SGMII_Multirate._GE_Auto_neg_State_Change_AND_MASK._GeAuto_neg_and_mask()
            return allFields

    class _GE_Remote_Fault_AND_MASK(AtRegister.AtRegister):
        def name(self):
            return "GE Remote Fault AND MASK"
    
        def description(self):
            return "Interrupt Remote Fault current status AND MASK"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000833
            
        def endAddress(self):
            return 0xffffffff

        class _GeRemote_Fault_and_mask(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GeRemote_Fault_and_mask"
            
            def description(self):
                return "Interrupt Enable of Remote Fault, bit per port, bit0 is port0, bit7 is port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GeRemote_Fault_and_mask"] = _AF6CNC0022_RD_SGMII_Multirate._GE_Remote_Fault_AND_MASK._GeRemote_Fault_and_mask()
            return allFields

    class _GE_Excessive_Error_Ratio_AND_MASK(AtRegister.AtRegister):
        def name(self):
            return "GE Excessive Error Ratio AND MASK"
    
        def description(self):
            return "Interrupt Excessive Error Ratio current status AND MASK"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000834
            
        def endAddress(self):
            return 0xffffffff

        class _rxexcer_and_mask(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxexcer_and_mask"
            
            def description(self):
                return "Interrupt Enable of Excessive Error Ratio, bit per port, bit0 is port0, bit7 is port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxexcer_and_mask"] = _AF6CNC0022_RD_SGMII_Multirate._GE_Excessive_Error_Ratio_AND_MASK._rxexcer_and_mask()
            return allFields

    class _Ge_Link_State_AND_MASK(AtRegister.AtRegister):
        def name(self):
            return "Ge Link State AND MASK"
    
        def description(self):
            return "Interrupt Excessive Error Ratio current status AND MASK"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000083a
            
        def endAddress(self):
            return 0xffffffff

        class _linksta_and_mask(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "linksta_and_mask"
            
            def description(self):
                return "Ge Link State AND MASK, bit per port, bit0 is port0, bit7 is port7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["linksta_and_mask"] = _AF6CNC0022_RD_SGMII_Multirate._Ge_Link_State_AND_MASK._linksta_and_mask()
            return allFields

    class _GE_1000basex_Force_K30_7(AtRegister.AtRegister):
        def name(self):
            return "GE 1000basex Force K30_7"
    
        def description(self):
            return "Forcing countinuous K30.7 for 1000basex mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000822
            
        def endAddress(self):
            return 0xffffffff

        class _tx_fk30_7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tx_fk30_7"
            
            def description(self):
                return "Force K30.7 code, bit per port, bit0 is port0, bit7 is port7"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tx_fk30_7"] = _AF6CNC0022_RD_SGMII_Multirate._GE_1000basex_Force_K30_7._tx_fk30_7()
            return allFields

    class _fx_txenb(AtRegister.AtRegister):
        def name(self):
            return "ENABLE TX 100base FX"
    
        def description(self):
            return "Interrupt enable of Remote Fault"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a08
            
        def endAddress(self):
            return 0xffffffff

        class _fx_entx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "fx_entx"
            
            def description(self):
                return "(1) is enable, (0) is disable,  bit0 is port0, bit15 is port15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["fx_entx"] = _AF6CNC0022_RD_SGMII_Multirate._fx_txenb._fx_entx()
            return allFields

    class _fx_patstken(AtRegister.AtRegister):
        def name(self):
            return "Sticky Rx PATTERN CODE DETECT"
    
        def description(self):
            return "Interrupt enable of Remote Fault"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a09
            
        def endAddress(self):
            return 0xffffffff

        class _fx_codeerrstk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "fx_codeerrstk"
            
            def description(self):
                return "sticky code error detect per port, bit[16] is port 0, bit[31] is port 15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_patstk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "fx_patstk"
            
            def description(self):
                return "sticky pattern detect per port bit[0] is port 0, bit[15] is port 15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["fx_codeerrstk"] = _AF6CNC0022_RD_SGMII_Multirate._fx_patstken._fx_codeerrstk()
            allFields["fx_patstk"] = _AF6CNC0022_RD_SGMII_Multirate._fx_patstken._fx_patstk()
            return allFields

    class _fx_stk_fefd_pen(AtRegister.AtRegister):
        def name(self):
            return "Sticky Rx CODE ERROR DETECT"
    
        def description(self):
            return "Interrupt Alarm of Remote Fault and Local fault"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a16
            
        def endAddress(self):
            return 0xffffffff

        class _fx_stk_rf15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "fx_stk_rf15"
            
            def description(self):
                return "sticky far end detect port 15 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_lf15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "fx_stk_lf15"
            
            def description(self):
                return "sticky local fault port 15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_rf14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "fx_stk_rf14"
            
            def description(self):
                return "sticky far end detect port 14 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_lf14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "fx_stk_lf14"
            
            def description(self):
                return "sticky local fault port 14"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_rf13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "fx_stk_rf13"
            
            def description(self):
                return "sticky far end detect port 13 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_lf13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "fx_stk_lf13"
            
            def description(self):
                return "sticky local fault port 13"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_rf12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "fx_stk_rf12"
            
            def description(self):
                return "sticky far end detect port 12 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_lf12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "fx_stk_lf12"
            
            def description(self):
                return "sticky local fault port 12"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_rf11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "fx_stk_rf11"
            
            def description(self):
                return "sticky far end detect port 11 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_lf11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "fx_stk_lf11"
            
            def description(self):
                return "sticky local fault port 11"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_rf10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "fx_stk_rf10"
            
            def description(self):
                return "sticky far end detect port 10 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_lf10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "fx_stk_lf10"
            
            def description(self):
                return "sticky local fault port 10"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_rf9(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "fx_stk_rf9"
            
            def description(self):
                return "sticky far end detect port 9 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_lf9(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "fx_stk_lf9"
            
            def description(self):
                return "sticky local fault port 9"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_rf8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "fx_stk_rf8"
            
            def description(self):
                return "sticky far end detect port 8 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_lf8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "fx_stk_lf8"
            
            def description(self):
                return "sticky local fault port 8"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_rf7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "fx_stk_rf7"
            
            def description(self):
                return "sticky far end detect port 7 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_lf7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "fx_stk_lf7"
            
            def description(self):
                return "sticky local fault port 7"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_rf7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "fx_stk_rf7"
            
            def description(self):
                return "sticky far end detect port 6 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_lf6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "fx_stk_lf6"
            
            def description(self):
                return "sticky local fault port 6"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_rf5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "fx_stk_rf5"
            
            def description(self):
                return "sticky far end detect port 5 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_lf5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "fx_stk_lf5"
            
            def description(self):
                return "sticky local fault port 5"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_rf4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "fx_stk_rf4"
            
            def description(self):
                return "sticky far end detect port 4 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_lf4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "fx_stk_lf4"
            
            def description(self):
                return "sticky local fault port 4"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_rf3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "fx_stk_rf3"
            
            def description(self):
                return "sticky far end detect port 3 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_lf3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "fx_stk_lf3"
            
            def description(self):
                return "sticky local fault port 3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_rf2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "fx_stk_rf2"
            
            def description(self):
                return "sticky far end detect port 2 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_lf2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "fx_stk_lf2"
            
            def description(self):
                return "sticky local fault port 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_rf1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "fx_stk_rf1"
            
            def description(self):
                return "sticky far end detect port 1 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_lf1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "fx_stk_lf1"
            
            def description(self):
                return "sticky local fault port 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_rf0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "fx_stk_rf0"
            
            def description(self):
                return "sticky far end detect port 0 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_stk_lf0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "fx_stk_lf0"
            
            def description(self):
                return "sticky local fault port 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["fx_stk_rf15"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_rf15()
            allFields["fx_stk_lf15"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_lf15()
            allFields["fx_stk_rf14"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_rf14()
            allFields["fx_stk_lf14"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_lf14()
            allFields["fx_stk_rf13"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_rf13()
            allFields["fx_stk_lf13"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_lf13()
            allFields["fx_stk_rf12"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_rf12()
            allFields["fx_stk_lf12"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_lf12()
            allFields["fx_stk_rf11"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_rf11()
            allFields["fx_stk_lf11"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_lf11()
            allFields["fx_stk_rf10"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_rf10()
            allFields["fx_stk_lf10"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_lf10()
            allFields["fx_stk_rf9"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_rf9()
            allFields["fx_stk_lf9"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_lf9()
            allFields["fx_stk_rf8"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_rf8()
            allFields["fx_stk_lf8"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_lf8()
            allFields["fx_stk_rf7"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_rf7()
            allFields["fx_stk_lf7"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_lf7()
            allFields["fx_stk_rf7"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_rf7()
            allFields["fx_stk_lf6"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_lf6()
            allFields["fx_stk_rf5"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_rf5()
            allFields["fx_stk_lf5"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_lf5()
            allFields["fx_stk_rf4"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_rf4()
            allFields["fx_stk_lf4"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_lf4()
            allFields["fx_stk_rf3"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_rf3()
            allFields["fx_stk_lf3"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_lf3()
            allFields["fx_stk_rf2"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_rf2()
            allFields["fx_stk_lf2"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_lf2()
            allFields["fx_stk_rf1"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_rf1()
            allFields["fx_stk_lf1"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_lf1()
            allFields["fx_stk_rf0"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_rf0()
            allFields["fx_stk_lf0"] = _AF6CNC0022_RD_SGMII_Multirate._fx_stk_fefd_pen._fx_stk_lf0()
            return allFields

    class _fx_alm_fefd_pen(AtRegister.AtRegister):
        def name(self):
            return "CURRENT STATUS Rx CODE ERROR DETECT"
    
        def description(self):
            return "Interrupt current status of Remote Fault"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a17
            
        def endAddress(self):
            return 0xffffffff

        class _fx_alm_rf15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "fx_alm_rf15"
            
            def description(self):
                return "current status far end detect port 15 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_lf15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "fx_alm_lf15"
            
            def description(self):
                return "current status local fault port 15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_rf14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "fx_alm_rf14"
            
            def description(self):
                return "current status far end detect port 14 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_lf14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "fx_alm_lf14"
            
            def description(self):
                return "current status local fault port 14"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_rf13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "fx_alm_rf13"
            
            def description(self):
                return "current status far end detect port 13 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_lf13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "fx_alm_lf13"
            
            def description(self):
                return "current status local fault port 13"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_rf12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "fx_alm_rf12"
            
            def description(self):
                return "current status far end detect port 12 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_lf12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "fx_alm_lf12"
            
            def description(self):
                return "current status local fault port 12"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_rf11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "fx_alm_rf11"
            
            def description(self):
                return "current status far end detect port 11 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_lf11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "fx_alm_lf11"
            
            def description(self):
                return "current status local fault port 11"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_rf10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "fx_alm_rf10"
            
            def description(self):
                return "current status far end detect port 10 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_lf10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "fx_alm_lf10"
            
            def description(self):
                return "current status local fault port 10"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_rf9(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "fx_alm_rf9"
            
            def description(self):
                return "current status far end detect port 9 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_lf9(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "fx_alm_lf9"
            
            def description(self):
                return "current status local fault port 9"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_rf8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "fx_alm_rf8"
            
            def description(self):
                return "current status far end detect port 8 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_lf8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "fx_alm_lf8"
            
            def description(self):
                return "current status local fault port 8"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_rf7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "fx_alm_rf7"
            
            def description(self):
                return "current status far end detect port 7 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_lf7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "fx_alm_lf7"
            
            def description(self):
                return "current status local fault port 7"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_rf7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "fx_alm_rf7"
            
            def description(self):
                return "current status far end detect port 6 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_lf6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "fx_alm_lf6"
            
            def description(self):
                return "current status local fault port 6"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_rf5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "fx_alm_rf5"
            
            def description(self):
                return "current status far end detect port 5 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_lf5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "fx_alm_lf5"
            
            def description(self):
                return "current status local fault port 5"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_rf4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "fx_alm_rf4"
            
            def description(self):
                return "current status far end detect port 4 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_lf4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "fx_alm_lf4"
            
            def description(self):
                return "current status local fault port 4"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_rf3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "fx_alm_rf3"
            
            def description(self):
                return "current status far end detect port 3 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_lf3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "fx_alm_lf3"
            
            def description(self):
                return "current status local fault port 3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_rf2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "fx_alm_rf2"
            
            def description(self):
                return "current status far end detect port 2 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_lf2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "fx_alm_lf2"
            
            def description(self):
                return "current status local fault port 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_rf1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "fx_alm_rf1"
            
            def description(self):
                return "current status far end detect port 1 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_lf1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "fx_alm_lf1"
            
            def description(self):
                return "current status local fault port 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_rf0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "fx_alm_rf0"
            
            def description(self):
                return "current status far end detect port 0 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_alm_lf0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "fx_alm_lf0"
            
            def description(self):
                return "current status local fault port 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["fx_alm_rf15"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_rf15()
            allFields["fx_alm_lf15"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_lf15()
            allFields["fx_alm_rf14"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_rf14()
            allFields["fx_alm_lf14"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_lf14()
            allFields["fx_alm_rf13"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_rf13()
            allFields["fx_alm_lf13"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_lf13()
            allFields["fx_alm_rf12"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_rf12()
            allFields["fx_alm_lf12"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_lf12()
            allFields["fx_alm_rf11"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_rf11()
            allFields["fx_alm_lf11"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_lf11()
            allFields["fx_alm_rf10"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_rf10()
            allFields["fx_alm_lf10"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_lf10()
            allFields["fx_alm_rf9"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_rf9()
            allFields["fx_alm_lf9"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_lf9()
            allFields["fx_alm_rf8"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_rf8()
            allFields["fx_alm_lf8"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_lf8()
            allFields["fx_alm_rf7"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_rf7()
            allFields["fx_alm_lf7"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_lf7()
            allFields["fx_alm_rf7"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_rf7()
            allFields["fx_alm_lf6"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_lf6()
            allFields["fx_alm_rf5"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_rf5()
            allFields["fx_alm_lf5"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_lf5()
            allFields["fx_alm_rf4"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_rf4()
            allFields["fx_alm_lf4"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_lf4()
            allFields["fx_alm_rf3"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_rf3()
            allFields["fx_alm_lf3"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_lf3()
            allFields["fx_alm_rf2"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_rf2()
            allFields["fx_alm_lf2"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_lf2()
            allFields["fx_alm_rf1"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_rf1()
            allFields["fx_alm_lf1"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_lf1()
            allFields["fx_alm_rf0"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_rf0()
            allFields["fx_alm_lf0"] = _AF6CNC0022_RD_SGMII_Multirate._fx_alm_fefd_pen._fx_alm_lf0()
            return allFields

    class _fx_fefd_inten(AtRegister.AtRegister):
        def name(self):
            return "ENABLE FEFD INTTERUPT"
    
        def description(self):
            return "Interrupt enable of Remote Fault"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000a18
            
        def endAddress(self):
            return 0xffffffff

        class _fx_int_msk_rf15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "fx_int_msk_rf15"
            
            def description(self):
                return "enable interrupt far end detect port 15 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_lf15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "fx_int_msk_lf15"
            
            def description(self):
                return "enable interrupt local fault port 15"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_rf14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "fx_int_msk_rf14"
            
            def description(self):
                return "enable interrupt far end detect port 14 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_lf14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "fx_int_msk_lf14"
            
            def description(self):
                return "enable interrupt local fault port 14"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_rf13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "fx_int_msk_rf13"
            
            def description(self):
                return "enable interrupt far end detect port 13 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_lf13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "fx_int_msk_lf13"
            
            def description(self):
                return "enable interrupt local fault port 13"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_rf12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "fx_int_msk_rf12"
            
            def description(self):
                return "enable interrupt far end detect port 12 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_lf12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "fx_int_msk_lf12"
            
            def description(self):
                return "enable interrupt local fault port 12"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_rf11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "fx_int_msk_rf11"
            
            def description(self):
                return "enable interrupt far end detect port 11 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_lf11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "fx_int_msk_lf11"
            
            def description(self):
                return "enable interrupt local fault port 11"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_rf10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "fx_int_msk_rf10"
            
            def description(self):
                return "enable interrupt far end detect port 10 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_lf10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "fx_int_msk_lf10"
            
            def description(self):
                return "enable interrupt local fault port 10"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_rf9(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "fx_int_msk_rf9"
            
            def description(self):
                return "enable interrupt far end detect port 9 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_lf9(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "fx_int_msk_lf9"
            
            def description(self):
                return "enable interrupt local fault port 9"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_rf8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "fx_int_msk_rf8"
            
            def description(self):
                return "enable interrupt far end detect port 8 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_lf8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "fx_int_msk_lf8"
            
            def description(self):
                return "enable interrupt local fault port 8"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_rf7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "fx_int_msk_rf7"
            
            def description(self):
                return "enable interrupt far end detect port 7 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_lf7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "fx_int_msk_lf7"
            
            def description(self):
                return "enable interrupt local fault port 7"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_rf7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "fx_int_msk_rf7"
            
            def description(self):
                return "enable interrupt far end detect port 6 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_lf6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "fx_int_msk_lf6"
            
            def description(self):
                return "enable interrupt local fault port 6"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_rf5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "fx_int_msk_rf5"
            
            def description(self):
                return "enable interrupt far end detect port 5 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_lf5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "fx_int_msk_lf5"
            
            def description(self):
                return "enable interrupt local fault port 5"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_rf4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "fx_int_msk_rf4"
            
            def description(self):
                return "enable interrupt far end detect port 4 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_lf4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "fx_int_msk_lf4"
            
            def description(self):
                return "enable interrupt local fault port 4"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_rf3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "fx_int_msk_rf3"
            
            def description(self):
                return "enable interrupt far end detect port 3 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_lf3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "fx_int_msk_lf3"
            
            def description(self):
                return "enable interrupt local fault port 3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_rf2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "fx_int_msk_rf2"
            
            def description(self):
                return "enable interrupt far end detect port 2 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_lf2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "fx_int_msk_lf2"
            
            def description(self):
                return "enable interrupt local fault port 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_rf1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "fx_int_msk_rf1"
            
            def description(self):
                return "enable interrupt far end detect port 1 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_lf1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "fx_int_msk_lf1"
            
            def description(self):
                return "enable interrupt local fault port 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_rf0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "fx_int_msk_rf0"
            
            def description(self):
                return "enable interrupt far end detect port 0 ( remote fault)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fx_int_msk_lf0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "fx_int_msk_lf0"
            
            def description(self):
                return "enable interrupt local fault port 0"
            
            def type(self):
                return ""
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["fx_int_msk_rf15"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_rf15()
            allFields["fx_int_msk_lf15"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_lf15()
            allFields["fx_int_msk_rf14"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_rf14()
            allFields["fx_int_msk_lf14"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_lf14()
            allFields["fx_int_msk_rf13"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_rf13()
            allFields["fx_int_msk_lf13"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_lf13()
            allFields["fx_int_msk_rf12"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_rf12()
            allFields["fx_int_msk_lf12"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_lf12()
            allFields["fx_int_msk_rf11"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_rf11()
            allFields["fx_int_msk_lf11"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_lf11()
            allFields["fx_int_msk_rf10"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_rf10()
            allFields["fx_int_msk_lf10"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_lf10()
            allFields["fx_int_msk_rf9"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_rf9()
            allFields["fx_int_msk_lf9"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_lf9()
            allFields["fx_int_msk_rf8"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_rf8()
            allFields["fx_int_msk_lf8"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_lf8()
            allFields["fx_int_msk_rf7"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_rf7()
            allFields["fx_int_msk_lf7"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_lf7()
            allFields["fx_int_msk_rf7"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_rf7()
            allFields["fx_int_msk_lf6"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_lf6()
            allFields["fx_int_msk_rf5"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_rf5()
            allFields["fx_int_msk_lf5"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_lf5()
            allFields["fx_int_msk_rf4"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_rf4()
            allFields["fx_int_msk_lf4"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_lf4()
            allFields["fx_int_msk_rf3"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_rf3()
            allFields["fx_int_msk_lf3"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_lf3()
            allFields["fx_int_msk_rf2"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_rf2()
            allFields["fx_int_msk_lf2"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_lf2()
            allFields["fx_int_msk_rf1"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_rf1()
            allFields["fx_int_msk_lf1"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_lf1()
            allFields["fx_int_msk_rf0"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_rf0()
            allFields["fx_int_msk_lf0"] = _AF6CNC0022_RD_SGMII_Multirate._fx_fefd_inten._fx_int_msk_lf0()
            return allFields

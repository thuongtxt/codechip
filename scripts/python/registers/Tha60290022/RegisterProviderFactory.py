import python.arrive.atsdk.AtRegister as AtRegister

class RegisterProviderFactory(AtRegister.AtRegisterProviderFactory):
    def _allRegisterProviders(self):
        allProviders = {}

        from _AF6CCI0022_RD_PLA_DEBUG import _AF6CCI0022_RD_PLA_DEBUG
        allProviders["_AF6CCI0022_RD_PLA_DEBUG"] = _AF6CCI0022_RD_PLA_DEBUG()

        from _AF6CNC0022_RD_CDR_v3_TxShapper import _AF6CNC0022_RD_CDR_v3_TxShapper
        allProviders["_AF6CNC0022_RD_CDR_v3_TxShapper"] = _AF6CNC0022_RD_CDR_v3_TxShapper()

        from _AF6CCI0011_RD_PDH_PRM import _AF6CCI0011_RD_PDH_PRM
        allProviders["_AF6CCI0011_RD_PDH_PRM"] = _AF6CCI0011_RD_PDH_PRM()

        from _AF6CNC0022_RD_GLBPMC import _AF6CNC0022_RD_GLBPMC
        allProviders["_AF6CNC0022_RD_GLBPMC"] = _AF6CNC0022_RD_GLBPMC()

        from _AF6CNC0022_RD_CDR_TxShapper import _AF6CNC0022_RD_CDR_TxShapper
        allProviders["_AF6CNC0022_RD_CDR_TxShapper"] = _AF6CNC0022_RD_CDR_TxShapper()

        from _AF6CNC0022_RD_PDA import _AF6CNC0022_RD_PDA
        allProviders["_AF6CNC0022_RD_PDA"] = _AF6CNC0022_RD_PDA()

        from _AF6CNC0022_RD_CDR_HO import _AF6CNC0022_RD_CDR_HO
        allProviders["_AF6CNC0022_RD_CDR_HO"] = _AF6CNC0022_RD_CDR_HO()

        from _AF6CNC0022_RD_PDH_PRM import _AF6CNC0022_RD_PDH_PRM
        allProviders["_AF6CNC0022_RD_PDH_PRM"] = _AF6CNC0022_RD_PDH_PRM()

        from _AF6CNC0022_RD_GLBPMC_ver2 import _AF6CNC0022_RD_GLBPMC_ver2
        allProviders["_AF6CNC0022_RD_GLBPMC_ver2"] = _AF6CNC0022_RD_GLBPMC_ver2()

        from _AF6CNC0022_RD_PDH_MDL import _AF6CNC0022_RD_PDH_MDL
        allProviders["_AF6CNC0022_RD_PDH_MDL"] = _AF6CNC0022_RD_PDH_MDL()

        from _AF6CNC0022_RD_CDR_v3_TimGen import _AF6CNC0022_RD_CDR_v3_TimGen
        allProviders["_AF6CNC0022_RD_CDR_v3_TimGen"] = _AF6CNC0022_RD_CDR_v3_TimGen()

        from _AF6CNC0022_RD_POH_BER import _AF6CNC0022_RD_POH_BER
        allProviders["_AF6CNC0022_RD_POH_BER"] = _AF6CNC0022_RD_POH_BER()

        from _AF6CNC0022_RD_SGMII_Multirate import _AF6CNC0022_RD_SGMII_Multirate
        allProviders["_AF6CNC0022_RD_SGMII_Multirate"] = _AF6CNC0022_RD_SGMII_Multirate()

        from _AF6CCI0011_RD_BERT_GEN import _AF6CCI0011_RD_BERT_GEN
        allProviders["_AF6CCI0011_RD_BERT_GEN"] = _AF6CCI0011_RD_BERT_GEN()

        from _AF6CNC0022_RD_CDR_v3_ACR import _AF6CNC0022_RD_CDR_v3_ACR
        allProviders["_AF6CNC0022_RD_CDR_v3_ACR"] = _AF6CNC0022_RD_CDR_v3_ACR()

        from _AF6CNC0022_RD_CDR_v3 import _AF6CNC0022_RD_CDR_v3
        allProviders["_AF6CNC0022_RD_CDR_v3"] = _AF6CNC0022_RD_CDR_v3()

        from _AF6CNC0022_RD_MAP import _AF6CNC0022_RD_MAP
        allProviders["_AF6CNC0022_RD_MAP"] = _AF6CNC0022_RD_MAP()

        from _AF6CNC0022_RD_CDR import _AF6CNC0022_RD_CDR
        allProviders["_AF6CNC0022_RD_CDR"] = _AF6CNC0022_RD_CDR()

        from _AF6CNC0022_RD_MAP_HO import _AF6CNC0022_RD_MAP_HO
        allProviders["_AF6CNC0022_RD_MAP_HO"] = _AF6CNC0022_RD_MAP_HO()

        from _AF6CNC0022_RD_PLA_DEBUG import _AF6CNC0022_RD_PLA_DEBUG
        allProviders["_AF6CNC0022_RD_PLA_DEBUG"] = _AF6CNC0022_RD_PLA_DEBUG()

        from _AF6NC0022_RD_INTR import _AF6NC0022_RD_INTR
        allProviders["_AF6NC0022_RD_INTR"] = _AF6NC0022_RD_INTR()

        from _AF6CNC0011_RD_ETH import _AF6CNC0011_RD_ETH
        allProviders["_AF6CNC0011_RD_ETH"] = _AF6CNC0011_RD_ETH()

        from _AF6CNC0022_RD_PM import _AF6CNC0022_RD_PM
        allProviders["_AF6CNC0022_RD_PM"] = _AF6CNC0022_RD_PM()

        from _AF6CNC0022_RD_BERT_OPT import _AF6CNC0022_RD_BERT_OPT
        allProviders["_AF6CNC0022_RD_BERT_OPT"] = _AF6CNC0022_RD_BERT_OPT()

        from _AF6CNC0022_RD_INTALM import _AF6CNC0022_RD_INTALM
        allProviders["_AF6CNC0022_RD_INTALM"] = _AF6CNC0022_RD_INTALM()

        from _AF6CNC0022_RD_BERT_GEN import _AF6CNC0022_RD_BERT_GEN
        allProviders["_AF6CNC0022_RD_BERT_GEN"] = _AF6CNC0022_RD_BERT_GEN()

        from _AF6CNC0021_RD_UPSR import _AF6CNC0021_RD_UPSR
        allProviders["_AF6CNC0021_RD_UPSR"] = _AF6CNC0021_RD_UPSR()

        from _AF6CNC0022_RD_CDR_new import _AF6CNC0022_RD_CDR_new
        allProviders["_AF6CNC0022_RD_CDR_new"] = _AF6CNC0022_RD_CDR_new()

        from _AF6CNC0022_RD_GLBPMC_1PAGE import _AF6CNC0022_RD_GLBPMC_1PAGE
        allProviders["_AF6CNC0022_RD_GLBPMC_1PAGE"] = _AF6CNC0022_RD_GLBPMC_1PAGE()

        from _AF6CNC0022_RD_DCCK_GMII import _AF6CNC0022_RD_DCCK_GMII
        allProviders["_AF6CNC0022_RD_DCCK_GMII"] = _AF6CNC0022_RD_DCCK_GMII()

        from _AF6CNC0021_RD_MACDUMP import _AF6CNC0021_RD_MACDUMP
        allProviders["_AF6CNC0021_RD_MACDUMP"] = _AF6CNC0021_RD_MACDUMP()

        from _AF6CNC0022_RD_TimGen_v3 import _AF6CNC0022_RD_TimGen_v3
        allProviders["_AF6CNC0022_RD_TimGen_v3"] = _AF6CNC0022_RD_TimGen_v3()

        from _AF6CNC0022_RD_PLA import _AF6CNC0022_RD_PLA
        allProviders["_AF6CNC0022_RD_PLA"] = _AF6CNC0022_RD_PLA()

        from _AF6CCI0011_RD_PDH_MDL import _AF6CCI0011_RD_PDH_MDL
        allProviders["_AF6CCI0011_RD_PDH_MDL"] = _AF6CCI0011_RD_PDH_MDL()

        from _AF6CCI0011_RD_POH_BER import _AF6CCI0011_RD_POH_BER
        allProviders["_AF6CCI0011_RD_POH_BER"] = _AF6CCI0011_RD_POH_BER()

        from _AF6CNC0022_RD_PDH import _AF6CNC0022_RD_PDH
        allProviders["_AF6CNC0022_RD_PDH"] = _AF6CNC0022_RD_PDH()

        from _AF6CNC0021_RD_ETHPASS import _AF6CNC0021_RD_ETHPASS
        allProviders["_AF6CNC0021_RD_ETHPASS"] = _AF6CNC0021_RD_ETHPASS()

        from _AF6CNC0022_RD_PDH_v3 import _AF6CNC0022_RD_PDH_v3
        allProviders["_AF6CNC0022_RD_PDH_v3"] = _AF6CNC0022_RD_PDH_v3()

        from _AF6CNC0022_RD_PWE import _AF6CNC0022_RD_PWE
        allProviders["_AF6CNC0022_RD_PWE"] = _AF6CNC0022_RD_PWE()

        from _AF6CNC0022_RD_GLB import _AF6CNC0022_RD_GLB
        allProviders["_AF6CNC0022_RD_GLB"] = _AF6CNC0022_RD_GLB()

        from _AF6CCI0011_RD_OCN import _AF6CCI0011_RD_OCN
        allProviders["_AF6CCI0011_RD_OCN"] = _AF6CCI0011_RD_OCN()


        return allProviders

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0021_RD_MACDUMP(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["ramdumtypecfg"] = _AF6CNC0021_RD_MACDUMP._ramdumtypecfg()
        allRegisters["ramdumpdata"] = _AF6CNC0021_RD_MACDUMP._ramdumpdata()
        allRegisters["dump_hold_status"] = _AF6CNC0021_RD_MACDUMP._dump_hold_status()
        return allRegisters

    class _ramdumtypecfg(AtRegister.AtRegister):
        def name(self):
            return "Mac 40G dump direction control"
    
        def description(self):
            return "This register configures direction to dump packet #HDL_PATH: rtljitbuf.ramjitbufcfg.ram.ram[$PWID]"
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0000804 #The address format for these registers is 0x00010000 + PWID +  PWID"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _DirectionDump(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DirectionDump"
            
            def description(self):
                return "Tx or Rx MAC direction 1: Dump Tx MAC40G 0: Dump Rx MAC40G"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DirectionDump"] = _AF6CNC0021_RD_MACDUMP._ramdumtypecfg._DirectionDump()
            return allFields

    class _ramdumpdata(AtRegister.AtRegister):
        def name(self):
            return "Mac 40G dump data status"
    
        def description(self):
            return "This register shows data value to be dump, first entry always contain SOP"
            
        def width(self):
            return 69
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000000 +  entry"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _DumpSop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 68
                
            def startBit(self):
                return 68
        
            def name(self):
                return "DumpSop"
            
            def description(self):
                return "Start of packet indication"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _DumpEoP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 67
                
            def startBit(self):
                return 67
        
            def name(self):
                return "DumpEoP"
            
            def description(self):
                return "End of packet indication"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _DumpNob(AtRegister.AtRegisterField):
            def stopBit(self):
                return 66
                
            def startBit(self):
                return 64
        
            def name(self):
                return "DumpNob"
            
            def description(self):
                return "Number of byte indication 0 means 1 byte"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DumpData(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DumpData"
            
            def description(self):
                return "Data value Bit63_56 is MSB"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DumpSop"] = _AF6CNC0021_RD_MACDUMP._ramdumpdata._DumpSop()
            allFields["DumpEoP"] = _AF6CNC0021_RD_MACDUMP._ramdumpdata._DumpEoP()
            allFields["DumpNob"] = _AF6CNC0021_RD_MACDUMP._ramdumpdata._DumpNob()
            allFields["DumpData"] = _AF6CNC0021_RD_MACDUMP._ramdumpdata._DumpData()
            return allFields

    class _dump_hold_status(AtRegister.AtRegister):
        def name(self):
            return "Mac 40G dump Hold Register Status"
    
        def description(self):
            return "This register using for hold remain that more than 32bits"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000800 +  HID"
            
        def startAddress(self):
            return 0x00000800
            
        def endAddress(self):
            return 0x00000001

        class _DumpHoldStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DumpHoldStatus"
            
            def description(self):
                return "Hold 32bits"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DumpHoldStatus"] = _AF6CNC0021_RD_MACDUMP._dump_hold_status._DumpHoldStatus()
            return allFields

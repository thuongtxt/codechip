import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0021_RD_ETHPASS(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_cfgvlan1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan1()
        allRegisters["upen_cfgvlan2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan2()
        allRegisters["upen_cfgvlan3"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan3()
        allRegisters["upen_cfgvlan4"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan4()
        allRegisters["upen_cfgvlan5"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan5()
        allRegisters["upen_cfgvlan6"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan6()
        allRegisters["upen_cfgvlan7"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan7()
        allRegisters["upen_cfgvlan8"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan8()
        allRegisters["upen_cfg_pwvlan"] = _AF6CNC0021_RD_ETHPASS._upen_cfg_pwvlan()
        allRegisters["upen_enbtpid"] = _AF6CNC0021_RD_ETHPASS._upen_enbtpid()
        allRegisters["upen_cfgrmtpid"] = _AF6CNC0021_RD_ETHPASS._upen_cfgrmtpid()
        allRegisters["upen_cfgrmtpid_pw"] = _AF6CNC0021_RD_ETHPASS._upen_cfgrmtpid_pw()
        allRegisters["upen_cfginsvlan"] = _AF6CNC0021_RD_ETHPASS._upen_cfginsvlan()
        allRegisters["upen_enbinstpid"] = _AF6CNC0021_RD_ETHPASS._upen_enbinstpid()
        allRegisters["upen_cfginstpid"] = _AF6CNC0021_RD_ETHPASS._upen_cfginstpid()
        allRegisters["upen_enbins"] = _AF6CNC0021_RD_ETHPASS._upen_enbins()
        allRegisters["upen_cfgthreshold"] = _AF6CNC0021_RD_ETHPASS._upen_cfgthreshold()
        allRegisters["upen_enbflwctrl"] = _AF6CNC0021_RD_ETHPASS._upen_enbflwctrl()
        allRegisters["upen_stathreshold"] = _AF6CNC0021_RD_ETHPASS._upen_stathreshold()
        allRegisters["upen_stabufwater"] = _AF6CNC0021_RD_ETHPASS._upen_stabufwater()
        allRegisters["epa_hold_status"] = _AF6CNC0021_RD_ETHPASS._epa_hold_status()
        allRegisters["upen_txepagrp1cnt"] = _AF6CNC0021_RD_ETHPASS._upen_txepagrp1cnt()
        allRegisters["upen_txepagrp2cnt"] = _AF6CNC0021_RD_ETHPASS._upen_txepagrp2cnt()
        allRegisters["upen_txepagrp3cnt"] = _AF6CNC0021_RD_ETHPASS._upen_txepagrp3cnt()
        allRegisters["upen_rxepagrp1cnt"] = _AF6CNC0021_RD_ETHPASS._upen_rxepagrp1cnt()
        allRegisters["upen_rxepagrp2cnt"] = _AF6CNC0021_RD_ETHPASS._upen_rxepagrp2cnt()
        allRegisters["upen_rxepagrp3cnt"] = _AF6CNC0021_RD_ETHPASS._upen_rxepagrp3cnt()
        allRegisters["upen_rxepagrp4cnt"] = _AF6CNC0021_RD_ETHPASS._upen_rxepagrp4cnt()
        allRegisters["upen_cfgpwvlan1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgpwvlan1()
        allRegisters["upen_cfgpwvlan2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgpwvlan2()
        allRegisters["upen_cfgpwvlaninsen"] = _AF6CNC0021_RD_ETHPASS._upen_cfgpwvlaninsen()
        allRegisters["ramrxepacfg0"] = _AF6CNC0021_RD_ETHPASS._ramrxepacfg0()
        allRegisters["ramrxepacfg1"] = _AF6CNC0021_RD_ETHPASS._ramrxepacfg1()
        return allRegisters

    class _upen_cfgvlan1(AtRegister.AtRegister):
        def name(self):
            return "CONFIG VLAN REMOVED port 1-2"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _prior_p2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 29
        
            def name(self):
                return "prior_p2"
            
            def description(self):
                return "priority"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfi_p2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "cfi_p2"
            
            def description(self):
                return "CFI bit"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _vlanID_p2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 16
        
            def name(self):
                return "vlanID_p2"
            
            def description(self):
                return "VLan ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _prior_p1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 13
        
            def name(self):
                return "prior_p1"
            
            def description(self):
                return "priority"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfi_p1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "cfi_p1"
            
            def description(self):
                return "CFI bit"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _vlanID_p1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "vlanID_p1"
            
            def description(self):
                return "VLan ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prior_p2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan1._prior_p2()
            allFields["cfi_p2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan1._cfi_p2()
            allFields["vlanID_p2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan1._vlanID_p2()
            allFields["prior_p1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan1._prior_p1()
            allFields["cfi_p1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan1._cfi_p1()
            allFields["vlanID_p1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan1._vlanID_p1()
            return allFields

    class _upen_cfgvlan2(AtRegister.AtRegister):
        def name(self):
            return "CONFIG VLAN REMOVED port 3-4"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _prior_p2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 29
        
            def name(self):
                return "prior_p2"
            
            def description(self):
                return "priority"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfi_p2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "cfi_p2"
            
            def description(self):
                return "CFI bit"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _vlanID_p2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 16
        
            def name(self):
                return "vlanID_p2"
            
            def description(self):
                return "VLan ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _prior_p1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 13
        
            def name(self):
                return "prior_p1"
            
            def description(self):
                return "priority"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfi_p1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "cfi_p1"
            
            def description(self):
                return "CFI bit"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _vlanID_p1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "vlanID_p1"
            
            def description(self):
                return "VLan ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prior_p2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan2._prior_p2()
            allFields["cfi_p2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan2._cfi_p2()
            allFields["vlanID_p2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan2._vlanID_p2()
            allFields["prior_p1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan2._prior_p1()
            allFields["cfi_p1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan2._cfi_p1()
            allFields["vlanID_p1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan2._vlanID_p1()
            return allFields

    class _upen_cfgvlan3(AtRegister.AtRegister):
        def name(self):
            return "CONFIG VLAN REMOVED port 5-6"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _prior_p2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 29
        
            def name(self):
                return "prior_p2"
            
            def description(self):
                return "priority"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfi_p2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "cfi_p2"
            
            def description(self):
                return "CFI bit"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _vlanID_p2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 16
        
            def name(self):
                return "vlanID_p2"
            
            def description(self):
                return "VLan ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _prior_p1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 13
        
            def name(self):
                return "prior_p1"
            
            def description(self):
                return "priority"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfi_p1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "cfi_p1"
            
            def description(self):
                return "CFI bit"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _vlanID_p1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "vlanID_p1"
            
            def description(self):
                return "VLan ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prior_p2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan3._prior_p2()
            allFields["cfi_p2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan3._cfi_p2()
            allFields["vlanID_p2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan3._vlanID_p2()
            allFields["prior_p1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan3._prior_p1()
            allFields["cfi_p1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan3._cfi_p1()
            allFields["vlanID_p1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan3._vlanID_p1()
            return allFields

    class _upen_cfgvlan4(AtRegister.AtRegister):
        def name(self):
            return "CONFIG VLAN REMOVED port 7-8"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _prior_p2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 29
        
            def name(self):
                return "prior_p2"
            
            def description(self):
                return "priority"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfi_p2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "cfi_p2"
            
            def description(self):
                return "CFI bit"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _vlanID_p2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 16
        
            def name(self):
                return "vlanID_p2"
            
            def description(self):
                return "VLan ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _prior_p1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 13
        
            def name(self):
                return "prior_p1"
            
            def description(self):
                return "priority"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfi_p1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "cfi_p1"
            
            def description(self):
                return "CFI bit"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _vlanID_p1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "vlanID_p1"
            
            def description(self):
                return "VLan ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prior_p2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan4._prior_p2()
            allFields["cfi_p2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan4._cfi_p2()
            allFields["vlanID_p2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan4._vlanID_p2()
            allFields["prior_p1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan4._prior_p1()
            allFields["cfi_p1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan4._cfi_p1()
            allFields["vlanID_p1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan4._vlanID_p1()
            return allFields

    class _upen_cfgvlan5(AtRegister.AtRegister):
        def name(self):
            return "CONFIG VLAN REMOVED  port 9-10"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0xffffffff

        class _prior_p2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 29
        
            def name(self):
                return "prior_p2"
            
            def description(self):
                return "priority"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfi_p2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "cfi_p2"
            
            def description(self):
                return "CFI bit"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _vlanID_p2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 16
        
            def name(self):
                return "vlanID_p2"
            
            def description(self):
                return "VLan ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _prior_p1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 13
        
            def name(self):
                return "prior_p1"
            
            def description(self):
                return "priority"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfi_p1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "cfi_p1"
            
            def description(self):
                return "CFI bit"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _vlanID_p1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "vlanID_p1"
            
            def description(self):
                return "VLan ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prior_p2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan5._prior_p2()
            allFields["cfi_p2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan5._cfi_p2()
            allFields["vlanID_p2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan5._vlanID_p2()
            allFields["prior_p1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan5._prior_p1()
            allFields["cfi_p1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan5._cfi_p1()
            allFields["vlanID_p1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan5._vlanID_p1()
            return allFields

    class _upen_cfgvlan6(AtRegister.AtRegister):
        def name(self):
            return "CONFIG VLAN REMOVED  port 11-12"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000005
            
        def endAddress(self):
            return 0xffffffff

        class _prior_p2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 29
        
            def name(self):
                return "prior_p2"
            
            def description(self):
                return "priority"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfi_p2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "cfi_p2"
            
            def description(self):
                return "CFI bit"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _vlanID_p2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 16
        
            def name(self):
                return "vlanID_p2"
            
            def description(self):
                return "VLan ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _prior_p1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 13
        
            def name(self):
                return "prior_p1"
            
            def description(self):
                return "priority"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfi_p1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "cfi_p1"
            
            def description(self):
                return "CFI bit"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _vlanID_p1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "vlanID_p1"
            
            def description(self):
                return "VLan ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prior_p2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan6._prior_p2()
            allFields["cfi_p2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan6._cfi_p2()
            allFields["vlanID_p2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan6._vlanID_p2()
            allFields["prior_p1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan6._prior_p1()
            allFields["cfi_p1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan6._cfi_p1()
            allFields["vlanID_p1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan6._vlanID_p1()
            return allFields

    class _upen_cfgvlan7(AtRegister.AtRegister):
        def name(self):
            return "CONFIG VLAN REMOVED port 13-14"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000006
            
        def endAddress(self):
            return 0xffffffff

        class _prior_p2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 29
        
            def name(self):
                return "prior_p2"
            
            def description(self):
                return "priority"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfi_p2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "cfi_p2"
            
            def description(self):
                return "CFI bit"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _vlanID_p2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 16
        
            def name(self):
                return "vlanID_p2"
            
            def description(self):
                return "VLan ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _prior_p1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 13
        
            def name(self):
                return "prior_p1"
            
            def description(self):
                return "priority"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfi_p1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "cfi_p1"
            
            def description(self):
                return "CFI bit"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _vlanID_p1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "vlanID_p1"
            
            def description(self):
                return "VLan ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prior_p2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan7._prior_p2()
            allFields["cfi_p2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan7._cfi_p2()
            allFields["vlanID_p2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan7._vlanID_p2()
            allFields["prior_p1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan7._prior_p1()
            allFields["cfi_p1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan7._cfi_p1()
            allFields["vlanID_p1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan7._vlanID_p1()
            return allFields

    class _upen_cfgvlan8(AtRegister.AtRegister):
        def name(self):
            return "CONFIG VLAN REMOVED port 15-16"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000007
            
        def endAddress(self):
            return 0xffffffff

        class _prior_p2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 29
        
            def name(self):
                return "prior_p2"
            
            def description(self):
                return "priority"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfi_p2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "cfi_p2"
            
            def description(self):
                return "CFI bit"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _vlanID_p2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 16
        
            def name(self):
                return "vlanID_p2"
            
            def description(self):
                return "VLan ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _prior_p1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 13
        
            def name(self):
                return "prior_p1"
            
            def description(self):
                return "priority"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfi_p1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "cfi_p1"
            
            def description(self):
                return "CFI bit"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _vlanID_p1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "vlanID_p1"
            
            def description(self):
                return "VLan ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prior_p2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan8._prior_p2()
            allFields["cfi_p2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan8._cfi_p2()
            allFields["vlanID_p2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan8._vlanID_p2()
            allFields["prior_p1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan8._prior_p1()
            allFields["cfi_p1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan8._cfi_p1()
            allFields["vlanID_p1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgvlan8._vlanID_p1()
            return allFields

    class _upen_cfg_pwvlan(AtRegister.AtRegister):
        def name(self):
            return "CONFIG VLAN REMOVED PW"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000008
            
        def endAddress(self):
            return 0xffffffff

        class _prior_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 29
        
            def name(self):
                return "prior_2"
            
            def description(self):
                return "priority"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfi_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "cfi_2"
            
            def description(self):
                return "CFI bit"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _vlanID_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 16
        
            def name(self):
                return "vlanID_2"
            
            def description(self):
                return "VLan ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _prior_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 13
        
            def name(self):
                return "prior_1"
            
            def description(self):
                return "priority"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfi_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "cfi_1"
            
            def description(self):
                return "CFI bit"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _vlanID_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "vlanID_1"
            
            def description(self):
                return "VLan ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prior_2"] = _AF6CNC0021_RD_ETHPASS._upen_cfg_pwvlan._prior_2()
            allFields["cfi_2"] = _AF6CNC0021_RD_ETHPASS._upen_cfg_pwvlan._cfi_2()
            allFields["vlanID_2"] = _AF6CNC0021_RD_ETHPASS._upen_cfg_pwvlan._vlanID_2()
            allFields["prior_1"] = _AF6CNC0021_RD_ETHPASS._upen_cfg_pwvlan._prior_1()
            allFields["cfi_1"] = _AF6CNC0021_RD_ETHPASS._upen_cfg_pwvlan._cfi_1()
            allFields["vlanID_1"] = _AF6CNC0021_RD_ETHPASS._upen_cfg_pwvlan._vlanID_1()
            return allFields

    class _upen_enbtpid(AtRegister.AtRegister):
        def name(self):
            return "CONFIG ENABLE REMOVE VLAN TPID"
    
        def description(self):
            return ""
            
        def width(self):
            return 18
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _out_enbtpid_pw(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "out_enbtpid_pw"
            
            def description(self):
                return "enable compare PW VLAN,"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _out_cfg_tpid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "out_cfg_tpid"
            
            def description(self):
                return "bit corresponding port to config ( bit 0 is port 0),"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["out_enbtpid_pw"] = _AF6CNC0021_RD_ETHPASS._upen_enbtpid._out_enbtpid_pw()
            allFields["reserve"] = _AF6CNC0021_RD_ETHPASS._upen_enbtpid._reserve()
            allFields["out_cfg_tpid"] = _AF6CNC0021_RD_ETHPASS._upen_enbtpid._out_cfg_tpid()
            return allFields

    class _upen_cfgrmtpid(AtRegister.AtRegister):
        def name(self):
            return "CONFIG ENABLE REMOVE VLAN TPID"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0xffffffff

        class _out_cfgtpid2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "out_cfgtpid2"
            
            def description(self):
                return "value TPID 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _out_cfgtpid1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "out_cfgtpid1"
            
            def description(self):
                return "value TPID 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["out_cfgtpid2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgrmtpid._out_cfgtpid2()
            allFields["out_cfgtpid1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgrmtpid._out_cfgtpid1()
            return allFields

    class _upen_cfgrmtpid_pw(AtRegister.AtRegister):
        def name(self):
            return "CONFIG ENABLE REMOVE VLAN TPID"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000012
            
        def endAddress(self):
            return 0xffffffff

        class _out_cfgtpid_pw2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "out_cfgtpid_pw2"
            
            def description(self):
                return "value TPID PW 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _out_cfgtpid_pw1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "out_cfgtpid_pw1"
            
            def description(self):
                return "value TPID PW 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["out_cfgtpid_pw2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgrmtpid_pw._out_cfgtpid_pw2()
            allFields["out_cfgtpid_pw1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgrmtpid_pw._out_cfgtpid_pw1()
            return allFields

    class _upen_cfginsvlan(AtRegister.AtRegister):
        def name(self):
            return "CONFIG VLAN INSERTED"
    
        def description(self):
            return ""
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x100+ $PID"
            
        def startAddress(self):
            return 0x00000100
            
        def endAddress(self):
            return 0x00000107

        class _prior(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 13
        
            def name(self):
                return "prior"
            
            def description(self):
                return "priority"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfi(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "cfi"
            
            def description(self):
                return "CFI bit"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _vlanID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "vlanID"
            
            def description(self):
                return "VLan ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prior"] = _AF6CNC0021_RD_ETHPASS._upen_cfginsvlan._prior()
            allFields["cfi"] = _AF6CNC0021_RD_ETHPASS._upen_cfginsvlan._cfi()
            allFields["vlanID"] = _AF6CNC0021_RD_ETHPASS._upen_cfginsvlan._vlanID()
            return allFields

    class _upen_enbinstpid(AtRegister.AtRegister):
        def name(self):
            return "CONFIG ENABLE INSERT VLAN TPID"
    
        def description(self):
            return ""
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000110
            
        def endAddress(self):
            return 0xffffffff

        class _out_enbtpid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "out_enbtpid"
            
            def description(self):
                return "bit corresponding port to config ( bit 0 is port 0),(0)  use config TPID1, (1) use config TPID 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["out_enbtpid"] = _AF6CNC0021_RD_ETHPASS._upen_enbinstpid._out_enbtpid()
            return allFields

    class _upen_cfginstpid(AtRegister.AtRegister):
        def name(self):
            return "CONFIG ENABLE INSERT VLAN TPID"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000111
            
        def endAddress(self):
            return 0xffffffff

        class _out_cfgtpid2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "out_cfgtpid2"
            
            def description(self):
                return "valud TPID 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _out_cfgtpid1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "out_cfgtpid1"
            
            def description(self):
                return "value TPID 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["out_cfgtpid2"] = _AF6CNC0021_RD_ETHPASS._upen_cfginstpid._out_cfgtpid2()
            allFields["out_cfgtpid1"] = _AF6CNC0021_RD_ETHPASS._upen_cfginstpid._out_cfgtpid1()
            return allFields

    class _upen_enbins(AtRegister.AtRegister):
        def name(self):
            return "CONFIG ENABLE INSERT VLAN TPID"
    
        def description(self):
            return ""
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000112
            
        def endAddress(self):
            return 0xffffffff

        class _out_enbins(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "out_enbins"
            
            def description(self):
                return "bit corresponding port to config ( bit 0 is port 0), (0) is disable, (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["out_enbins"] = _AF6CNC0021_RD_ETHPASS._upen_enbins._out_enbins()
            return allFields

    class _upen_cfgthreshold(AtRegister.AtRegister):
        def name(self):
            return "CONFIG THRESHOLD"
    
        def description(self):
            return "Theshold in 1-kbyte unit to report STARVING/HUNGRY/SATISFIED via OOF bus of MAC40G to FACE ETH buffer"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x200+ $PID"
            
        def startAddress(self):
            return 0x00000200
            
        def endAddress(self):
            return 0x0000020f

        class _hungry_thres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "hungry_thres"
            
            def description(self):
                return "report HUNGRY if below this threshold and over starving_thres, report SATISFIED of over this threshold, default 3/4 of max buffer"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _starving_thres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "starving_thres"
            
            def description(self):
                return "report STARVING if below this threshold, default 1/4 of max buffer"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["hungry_thres"] = _AF6CNC0021_RD_ETHPASS._upen_cfgthreshold._hungry_thres()
            allFields["starving_thres"] = _AF6CNC0021_RD_ETHPASS._upen_cfgthreshold._starving_thres()
            return allFields

    class _upen_enbflwctrl(AtRegister.AtRegister):
        def name(self):
            return "CONFIG ENABLE FLOW CONTROL"
    
        def description(self):
            return ""
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000210
            
        def endAddress(self):
            return 0xffffffff

        class _out_enbflw(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "out_enbflw"
            
            def description(self):
                return "(0) is disable, (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["out_enbflw"] = _AF6CNC0021_RD_ETHPASS._upen_enbflwctrl._out_enbflw()
            return allFields

    class _upen_stathreshold(AtRegister.AtRegister):
        def name(self):
            return "CONFIG THRESHOLD"
    
        def description(self):
            return "Currest status buffer in 1-kbyte unit to report STARVING/HUNGRY/SATISFIED via OOF bus of MAC40G to FACE ETH buffer"
            
        def width(self):
            return 8
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x830+ $PID"
            
        def startAddress(self):
            return 0x00000830
            
        def endAddress(self):
            return 0x0000083f

        class _BufferLength(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "BufferLength"
            
            def description(self):
                return "Current buffer length in 1-byte unit"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["BufferLength"] = _AF6CNC0021_RD_ETHPASS._upen_stathreshold._BufferLength()
            return allFields

    class _upen_stabufwater(AtRegister.AtRegister):
        def name(self):
            return "STATUS MAX MIN BUFFER"
    
        def description(self):
            return "Watermakr max min status buffer in 1-kbyte unit to report STARVING/HUNGRY/SATISFIED via OOF"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x840+ $PID"
            
        def startAddress(self):
            return 0x00000840
            
        def endAddress(self):
            return 0x0000084f

        class _MaxBufferLength(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "MaxBufferLength"
            
            def description(self):
                return "Max buffer length in 1-byte unit"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _MinBufferLength(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MinBufferLength"
            
            def description(self):
                return "Min buffer length in 1-byte unit"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MaxBufferLength"] = _AF6CNC0021_RD_ETHPASS._upen_stabufwater._MaxBufferLength()
            allFields["MinBufferLength"] = _AF6CNC0021_RD_ETHPASS._upen_stabufwater._MinBufferLength()
            return allFields

    class _epa_hold_status(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Pass Through Hold Register Status"
    
        def description(self):
            return "This register using for hold remain that more than 128bits"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x80 +  HID"
            
        def startAddress(self):
            return 0x00000080
            
        def endAddress(self):
            return 0xffffffff

        class _EpaHoldStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "EpaHoldStatus"
            
            def description(self):
                return "Hold 32bits"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["EpaHoldStatus"] = _AF6CNC0021_RD_ETHPASS._epa_hold_status._EpaHoldStatus()
            return allFields

    class _upen_txepagrp1cnt(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Pass Through Group1 Counter"
    
        def description(self):
            return "The register count information as below. Depending on cntoffset it 's the events specify."
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xA00 + 16*$cntoffset + $ethpid"
            
        def startAddress(self):
            return 0x00000a00
            
        def endAddress(self):
            return 0x00000a2f

        class _txethgrp1cnt1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 32
        
            def name(self):
                return "txethgrp1cnt1"
            
            def description(self):
                return "cntoffset = 0 : txpkt255byte cntoffset = 1 : txpkt1023byte cntoffset = 2 : txpktjumbo"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        class _txethgrp1cnt0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txethgrp1cnt0"
            
            def description(self):
                return "cntoffset = 0 : txpkt127byte cntoffset = 1 : txpkt511byte cntoffset = 2 : txpkt1518byte"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txethgrp1cnt1"] = _AF6CNC0021_RD_ETHPASS._upen_txepagrp1cnt._txethgrp1cnt1()
            allFields["txethgrp1cnt0"] = _AF6CNC0021_RD_ETHPASS._upen_txepagrp1cnt._txethgrp1cnt0()
            return allFields

    class _upen_txepagrp2cnt(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Pass Through Group2 Counter"
    
        def description(self):
            return "The register count information as below. Depending on cntoffset it 's the events specify."
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xA80 + 16*$cntoffset + $ethpid"
            
        def startAddress(self):
            return 0x00000a80
            
        def endAddress(self):
            return 0x00000aaf

        class _txethgrp2cnt1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 32
        
            def name(self):
                return "txethgrp2cnt1"
            
            def description(self):
                return "cntoffset = 0 : txpktgood cntoffset = 1 : unused cntoffset = 2 : txoversize"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        class _txethgrp2cnt0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txethgrp2cnt0"
            
            def description(self):
                return "cntoffset = 0 : txbytecnt cntoffset = 1 : txpkttotaldis cntoffset = 2 : txundersize"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txethgrp2cnt1"] = _AF6CNC0021_RD_ETHPASS._upen_txepagrp2cnt._txethgrp2cnt1()
            allFields["txethgrp2cnt0"] = _AF6CNC0021_RD_ETHPASS._upen_txepagrp2cnt._txethgrp2cnt0()
            return allFields

    class _upen_txepagrp3cnt(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Pass Through Group3 Counter"
    
        def description(self):
            return "The register count information as below. Depending on cntoffset it 's the events specify."
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xB00 + 16*$cntoffset + $ethpid"
            
        def startAddress(self):
            return 0x00000b00
            
        def endAddress(self):
            return 0x00000b2f

        class _txethgrp1cnt1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 32
        
            def name(self):
                return "txethgrp1cnt1"
            
            def description(self):
                return "cntoffset = 0 : txpktoverrun cntoffset = 1 : txpktmulticast cntoffset = 2 : unused"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        class _txethgrp1cnt0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txethgrp1cnt0"
            
            def description(self):
                return "cntoffset = 0 : txpktpausefrm cntoffset = 1 : txpktbroadcast cntoffset = 2 : txpkt64byte"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txethgrp1cnt1"] = _AF6CNC0021_RD_ETHPASS._upen_txepagrp3cnt._txethgrp1cnt1()
            allFields["txethgrp1cnt0"] = _AF6CNC0021_RD_ETHPASS._upen_txepagrp3cnt._txethgrp1cnt0()
            return allFields

    class _upen_rxepagrp1cnt(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Pass Through Group1 Counter"
    
        def description(self):
            return "The register count information as below. Depending on cntoffset it 's the events specify."
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xE00 + 16*$cntoffset + $ethpid"
            
        def startAddress(self):
            return 0x00000e00
            
        def endAddress(self):
            return 0x00000e2f

        class _rxethgrp1cnt1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 32
        
            def name(self):
                return "rxethgrp1cnt1"
            
            def description(self):
                return "cntoffset = 0 : rxpkt255byte cntoffset = 1 : rxpkt1023byte cntoffset = 2 : rxpktjumbo"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        class _rxethgrp1cnt0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxethgrp1cnt0"
            
            def description(self):
                return "cntoffset = 0 : rxpkt127byte cntoffset = 1 : rxpkt511byte cntoffset = 2 : rxpkt1518byte"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxethgrp1cnt1"] = _AF6CNC0021_RD_ETHPASS._upen_rxepagrp1cnt._rxethgrp1cnt1()
            allFields["rxethgrp1cnt0"] = _AF6CNC0021_RD_ETHPASS._upen_rxepagrp1cnt._rxethgrp1cnt0()
            return allFields

    class _upen_rxepagrp2cnt(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Pass Through Group2 Counter"
    
        def description(self):
            return "The register count information as below. Depending on cntoffset it 's the events specify."
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xE80 + 16*$cntoffset + $ethpid"
            
        def startAddress(self):
            return 0x00000e80
            
        def endAddress(self):
            return 0x00000eaf

        class _rxethgrp2cnt1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 32
        
            def name(self):
                return "rxethgrp2cnt1"
            
            def description(self):
                return "cntoffset = 0 : rxpkttotal cntoffset = 1 : unused cntoffset = 2 : rxpktdaloop"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        class _rxethgrp2cnt0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxethgrp2cnt0"
            
            def description(self):
                return "cntoffset = 0 : rxbytecnt cntoffset = 1 : rxpktfcserr cntoffset = 2 : rxpkttotaldis"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxethgrp2cnt1"] = _AF6CNC0021_RD_ETHPASS._upen_rxepagrp2cnt._rxethgrp2cnt1()
            allFields["rxethgrp2cnt0"] = _AF6CNC0021_RD_ETHPASS._upen_rxepagrp2cnt._rxethgrp2cnt0()
            return allFields

    class _upen_rxepagrp3cnt(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Pass Through Group3 Counter"
    
        def description(self):
            return "The register count information as below. Depending on cntoffset it 's the events specify."
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xF00 + 16*$cntoffset + $ethpid"
            
        def startAddress(self):
            return 0x00000f00
            
        def endAddress(self):
            return 0x00000f2f

        class _rxethgrp2cnt1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 32
        
            def name(self):
                return "rxethgrp2cnt1"
            
            def description(self):
                return "cntoffset = 0 : rxpktjabber cntoffset = 1 : rxpktpausefrm cntoffset = 2 : rxpktoversize"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        class _rxethgrp2cnt0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxethgrp2cnt0"
            
            def description(self):
                return "cntoffset = 0 : rxpktpcserr cntoffset = 1 : rxpktfragment cntoffset = 2 : rxpktundersize"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxethgrp2cnt1"] = _AF6CNC0021_RD_ETHPASS._upen_rxepagrp3cnt._rxethgrp2cnt1()
            allFields["rxethgrp2cnt0"] = _AF6CNC0021_RD_ETHPASS._upen_rxepagrp3cnt._rxethgrp2cnt0()
            return allFields

    class _upen_rxepagrp4cnt(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Pass Through Group4 Counter"
    
        def description(self):
            return "The register count information as below. Depending on cntoffset it 's the events specify."
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xF80 + 16*$cntoffset + $ethpid"
            
        def startAddress(self):
            return 0x00000f80
            
        def endAddress(self):
            return 0x00000fbf

        class _rxethgrp2cnt1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 32
        
            def name(self):
                return "rxethgrp2cnt1"
            
            def description(self):
                return "cntoffset = 0 : rxpktbroadcast cntoffset = 1 : rxpkt64byte cntoffset = 2 : unused"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        class _rxethgrp2cnt0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxethgrp2cnt0"
            
            def description(self):
                return "cntoffset = 0 : rxpktoverrun cntoffset = 1 : rxpktmulticast cntoffset = 2 : unused"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxethgrp2cnt1"] = _AF6CNC0021_RD_ETHPASS._upen_rxepagrp4cnt._rxethgrp2cnt1()
            allFields["rxethgrp2cnt0"] = _AF6CNC0021_RD_ETHPASS._upen_rxepagrp4cnt._rxethgrp2cnt0()
            return allFields

    class _upen_cfgpwvlan1(AtRegister.AtRegister):
        def name(self):
            return "CONFIG VLAN1 INSERTED PW"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000000a0
            
        def endAddress(self):
            return 0xffffffff

        class _PwSubVlan1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PwSubVlan1"
            
            def description(self):
                return "Pw subport VLAN1 value"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwSubVlan1"] = _AF6CNC0021_RD_ETHPASS._upen_cfgpwvlan1._PwSubVlan1()
            return allFields

    class _upen_cfgpwvlan2(AtRegister.AtRegister):
        def name(self):
            return "CONFIG VLAN2 INSERTED PW"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000000a1
            
        def endAddress(self):
            return 0xffffffff

        class _PwSubVlan2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PwSubVlan2"
            
            def description(self):
                return "Pw subport VLAN2 value"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwSubVlan2"] = _AF6CNC0021_RD_ETHPASS._upen_cfgpwvlan2._PwSubVlan2()
            return allFields

    class _upen_cfgpwvlaninsen(AtRegister.AtRegister):
        def name(self):
            return "CONFIG VLAN INSERTED PW ENABLE"
    
        def description(self):
            return ""
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000000a2
            
        def endAddress(self):
            return 0xffffffff

        class _PwVlanSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PwVlanSel"
            
            def description(self):
                return "Select VLAN to insert 0: select VLAN1 1: select VLAN2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _PwVlanInsEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PwVlanInsEn"
            
            def description(self):
                return "Enable insert VLAN 0: disable insert 1: enable insert"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwVlanSel"] = _AF6CNC0021_RD_ETHPASS._upen_cfgpwvlaninsen._PwVlanSel()
            allFields["PwVlanInsEn"] = _AF6CNC0021_RD_ETHPASS._upen_cfgpwvlaninsen._PwVlanInsEn()
            return allFields

    class _ramrxepacfg0(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Pass Through MAC Address Control"
    
        def description(self):
            return "This register configures parameters per ethernet pass through port"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xC30 +  ethpid"
            
        def startAddress(self):
            return 0x00000c30
            
        def endAddress(self):
            return 0xffffffff

        class _EthPassMacAdd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "EthPassMacAdd"
            
            def description(self):
                return "Ethernet pass through MAC address used for Loop DA counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["EthPassMacAdd"] = _AF6CNC0021_RD_ETHPASS._ramrxepacfg0._EthPassMacAdd()
            return allFields

    class _ramrxepacfg1(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Pass Through Port Enable and MTU Control"
    
        def description(self):
            return "This register configures parameters per ethernet pass through port"
            
        def width(self):
            return 21
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xC40 +  ethpid"
            
        def startAddress(self):
            return 0x00000c40
            
        def endAddress(self):
            return 0xffffffff

        class _EthPassPortEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "EthPassPortEn"
            
            def description(self):
                return "Ethernet pass port enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EthPassPcsErrDropEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "EthPassPcsErrDropEn"
            
            def description(self):
                return "Ethernet PCS error drop enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EthPassFcsErrDropEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "EthPassFcsErrDropEn"
            
            def description(self):
                return "Ethernet FCS error drop enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EthPassUndersizeDropEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "EthPassUndersizeDropEn"
            
            def description(self):
                return "Ethernet Undersize drop enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EthPassOversizeDropEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "EthPassOversizeDropEn"
            
            def description(self):
                return "Ethernet Oversize drop enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EthPassPausefrmDropEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "EthPassPausefrmDropEn"
            
            def description(self):
                return "Ethernet Pausefrm drop enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EthPassDaloopDropEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "EthPassDaloopDropEn"
            
            def description(self):
                return "Ethernet Daloop drop enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EthPassMtuSize(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "EthPassMtuSize"
            
            def description(self):
                return "Ethernet pass through MTU size"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["EthPassPortEn"] = _AF6CNC0021_RD_ETHPASS._ramrxepacfg1._EthPassPortEn()
            allFields["EthPassPcsErrDropEn"] = _AF6CNC0021_RD_ETHPASS._ramrxepacfg1._EthPassPcsErrDropEn()
            allFields["EthPassFcsErrDropEn"] = _AF6CNC0021_RD_ETHPASS._ramrxepacfg1._EthPassFcsErrDropEn()
            allFields["EthPassUndersizeDropEn"] = _AF6CNC0021_RD_ETHPASS._ramrxepacfg1._EthPassUndersizeDropEn()
            allFields["EthPassOversizeDropEn"] = _AF6CNC0021_RD_ETHPASS._ramrxepacfg1._EthPassOversizeDropEn()
            allFields["EthPassPausefrmDropEn"] = _AF6CNC0021_RD_ETHPASS._ramrxepacfg1._EthPassPausefrmDropEn()
            allFields["EthPassDaloopDropEn"] = _AF6CNC0021_RD_ETHPASS._ramrxepacfg1._EthPassDaloopDropEn()
            allFields["EthPassMtuSize"] = _AF6CNC0021_RD_ETHPASS._ramrxepacfg1._EthPassMtuSize()
            return allFields

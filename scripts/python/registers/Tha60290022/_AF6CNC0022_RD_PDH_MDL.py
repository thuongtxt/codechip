import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0022_RD_PDH_MDL(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_mdl_buffer1"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_buffer1()
        allRegisters["upen_mdl_buffer1_2"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_buffer1_2()
        allRegisters["upen_mdl_tp1"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_tp1()
        allRegisters["upen_mdl_tp2"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_tp2()
        allRegisters["upen_sta_idle_alren"] = _AF6CNC0022_RD_PDH_MDL._upen_sta_idle_alren()
        allRegisters["upen_sta_tp_alren"] = _AF6CNC0022_RD_PDH_MDL._upen_sta_tp_alren()
        allRegisters["upen_cfg_mdl"] = _AF6CNC0022_RD_PDH_MDL._upen_cfg_mdl()
        allRegisters["upen_txmdl_cntr2c_valididle1"] = _AF6CNC0022_RD_PDH_MDL._upen_txmdl_cntr2c_valididle1()
        allRegisters["upen_txmdl_cntr2c_validpath"] = _AF6CNC0022_RD_PDH_MDL._upen_txmdl_cntr2c_validpath()
        allRegisters["upen_txmdl_cntr2c_validtest"] = _AF6CNC0022_RD_PDH_MDL._upen_txmdl_cntr2c_validtest()
        allRegisters["upen_txmdl_cntr2c_byteidle"] = _AF6CNC0022_RD_PDH_MDL._upen_txmdl_cntr2c_byteidle()
        allRegisters["upen_txmdl_cntr2c_bytepath"] = _AF6CNC0022_RD_PDH_MDL._upen_txmdl_cntr2c_bytepath()
        allRegisters["upen_txmdl_cntr2c_bytetest"] = _AF6CNC0022_RD_PDH_MDL._upen_txmdl_cntr2c_bytetest()
        allRegisters["upen_rxmdl_typebuff"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_typebuff()
        allRegisters["upen_rxmdl_cfgtype"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_cfgtype()
        allRegisters["upen_destuff_ctrl0"] = _AF6CNC0022_RD_PDH_MDL._upen_destuff_ctrl0()
        allRegisters["upen_mdl_stk_cfg1"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_stk_cfg1()
        allRegisters["upen_mdl_stk_cfg2"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_stk_cfg2()
        allRegisters["upen_mdl_stk_cfg3"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_stk_cfg3()
        allRegisters["upen_mdl_stk_cfg4"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_stk_cfg4()
        allRegisters["upen_mdl_stk_cfg5"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_stk_cfg5()
        allRegisters["upen_mdl_stk_cfg6"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_stk_cfg6()
        allRegisters["upen_mdl_stk_cfg7"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_stk_cfg7()
        allRegisters["upen_mdl_stk_cfg8"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_stk_cfg8()
        allRegisters["upen_mdl_stk_cfg9"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_stk_cfg9()
        allRegisters["upen_mdl_stk_cfg10"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_stk_cfg10()
        allRegisters["upen_mdl_stk_cfg11"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_stk_cfg11()
        allRegisters["upen_mdl_stk_cfg12"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_stk_cfg12()
        allRegisters["upen_rxmdl_cntr2c_byteidle"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_cntr2c_byteidle()
        allRegisters["upen_rxmdl_cntr2c_bytepath"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_cntr2c_bytepath()
        allRegisters["upen_rxmdl_cntr2c_bytetest"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_cntr2c_bytetest()
        allRegisters["upen_rxmdl_cntr2c_goodidle"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_cntr2c_goodidle()
        allRegisters["upen_rxmdl_cntr2c_goodpath"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_cntr2c_goodpath()
        allRegisters["upen_rxmdl_cntr2c_goodtest"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_cntr2c_goodtest()
        allRegisters["upen_rxmdl_cntr2c_dropidle"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_cntr2c_dropidle()
        allRegisters["upen_rxmdl_cntr2c_droppath"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_cntr2c_droppath()
        allRegisters["upen_rxmdl_cntr2c_droptest"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_cntr2c_droptest()
        allRegisters["upen_int_ndlsta"] = _AF6CNC0022_RD_PDH_MDL._upen_int_ndlsta()
        allRegisters["mdl_cfgen_int1"] = _AF6CNC0022_RD_PDH_MDL._mdl_cfgen_int1()
        allRegisters["mdl_int1_sta"] = _AF6CNC0022_RD_PDH_MDL._mdl_int1_sta()
        allRegisters["mdl_int1_crrsta"] = _AF6CNC0022_RD_PDH_MDL._mdl_int1_crrsta()
        allRegisters["mdl_int1sta"] = _AF6CNC0022_RD_PDH_MDL._mdl_int1sta()
        allRegisters["mdl_sta_int1"] = _AF6CNC0022_RD_PDH_MDL._mdl_sta_int1()
        allRegisters["mdl_en_int1"] = _AF6CNC0022_RD_PDH_MDL._mdl_en_int1()
        allRegisters["mdl_cfgen_int2"] = _AF6CNC0022_RD_PDH_MDL._mdl_cfgen_int2()
        allRegisters["mdl_int2_sta"] = _AF6CNC0022_RD_PDH_MDL._mdl_int2_sta()
        allRegisters["mdl_int2_crrsta"] = _AF6CNC0022_RD_PDH_MDL._mdl_int2_crrsta()
        allRegisters["mdl_int2sta"] = _AF6CNC0022_RD_PDH_MDL._mdl_int2sta()
        allRegisters["mdl_sta_int2"] = _AF6CNC0022_RD_PDH_MDL._mdl_sta_int2()
        allRegisters["mdl_en_int2"] = _AF6CNC0022_RD_PDH_MDL._mdl_en_int2()
        return allRegisters

    class _upen_mdl_buffer1(AtRegister.AtRegister):
        def name(self):
            return "Buff Message MDL BUFFER1"
    
        def description(self):
            return "config message MDL BUFFER Channel ID 0-31"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0000+ $SLICEID*0x1000 + $DWORDID*32 + $DE3ID1"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x0000025f

        class _idle_byte13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "idle_byte13"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ilde_byte12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ilde_byte12"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_byte11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "idle_byte11"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_byte10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "idle_byte10"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["idle_byte13"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_buffer1._idle_byte13()
            allFields["ilde_byte12"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_buffer1._ilde_byte12()
            allFields["idle_byte11"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_buffer1._idle_byte11()
            allFields["idle_byte10"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_buffer1._idle_byte10()
            return allFields

    class _upen_mdl_buffer1_2(AtRegister.AtRegister):
        def name(self):
            return "Buff Message MDL BUFFER1_2"
    
        def description(self):
            return "config message MDL BUFFER Channel ID 32-47"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0260+ $SLICEID*0x1000 + $DWORDID*16 + $DE3ID2"
            
        def startAddress(self):
            return 0x00000260
            
        def endAddress(self):
            return 0x0000038f

        class _idle_byte23(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "idle_byte23"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_byte22(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "idle_byte22"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_byte21(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "idle_byte21"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_byte20(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "idle_byte20"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["idle_byte23"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_buffer1_2._idle_byte23()
            allFields["idle_byte22"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_buffer1_2._idle_byte22()
            allFields["idle_byte21"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_buffer1_2._idle_byte21()
            allFields["idle_byte20"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_buffer1_2._idle_byte20()
            return allFields

    class _upen_mdl_tp1(AtRegister.AtRegister):
        def name(self):
            return "Config Buff Message MDL BUFFER2"
    
        def description(self):
            return "config message MDL BUFFER2 Channel ID 0-15"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0400+$SLICEID*0x1000 + $DE3ID1 + $DWORDID*16"
            
        def startAddress(self):
            return 0x00000400
            
        def endAddress(self):
            return 0x0000065f

        class _tp_byte13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "tp_byte13"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "tp_byte12"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "tp_byte11"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tp_byte10"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tp_byte13"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_tp1._tp_byte13()
            allFields["tp_byte12"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_tp1._tp_byte12()
            allFields["tp_byte11"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_tp1._tp_byte11()
            allFields["tp_byte10"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_tp1._tp_byte10()
            return allFields

    class _upen_mdl_tp2(AtRegister.AtRegister):
        def name(self):
            return "Config Buff Message MDL BUFFER2_2"
    
        def description(self):
            return "config message MDL BUFFER2 Channel ID 16-23"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0660+$SLICEID*0x1000 + $DWORDID*8+ $DE3ID2"
            
        def startAddress(self):
            return 0x00000660
            
        def endAddress(self):
            return 0x0000078f

        class _tp_byte23(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "tp_byte23"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte22(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "tp_byte22"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte21(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "tp_byte21"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte20(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tp_byte20"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tp_byte23"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_tp2._tp_byte23()
            allFields["tp_byte22"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_tp2._tp_byte22()
            allFields["tp_byte21"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_tp2._tp_byte21()
            allFields["tp_byte20"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_tp2._tp_byte20()
            return allFields

    class _upen_sta_idle_alren(AtRegister.AtRegister):
        def name(self):
            return "SET TO HAVE PACKET MDL BUFFER 1"
    
        def description(self):
            return "SET/CLEAR to ALRM STATUS MESSAGE in BUFFER 1"
            
        def width(self):
            return 1
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0840+$SLICEID*0x1000 +$DE3ID"
            
        def startAddress(self):
            return 0x00000840
            
        def endAddress(self):
            return 0x0000086f

        class _idle_cfgen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "idle_cfgen"
            
            def description(self):
                return "(0) : engine clear for indication to have sent, (1) CPU set for indication to have new message which must send for buffer 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["idle_cfgen"] = _AF6CNC0022_RD_PDH_MDL._upen_sta_idle_alren._idle_cfgen()
            return allFields

    class _upen_sta_tp_alren(AtRegister.AtRegister):
        def name(self):
            return "SET TO HAVE PACKET MDL BUFFER 2"
    
        def description(self):
            return "SET/CLEAR to ALRM STATUS MESSAGE in BUFFER 2"
            
        def width(self):
            return 1
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0880+$SLICEID*0x1000 +$DE3ID"
            
        def startAddress(self):
            return 0x00000880
            
        def endAddress(self):
            return 0x000008af

        class _tp_cfgen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tp_cfgen"
            
            def description(self):
                return "(0) : engine clear for indication to have sent, (1) CPU set for indication to have new message which must send for buffer 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tp_cfgen"] = _AF6CNC0022_RD_PDH_MDL._upen_sta_tp_alren._tp_cfgen()
            return allFields

    class _upen_cfg_mdl(AtRegister.AtRegister):
        def name(self):
            return "CONFIG MDL Tx"
    
        def description(self):
            return "Config Tx MDL"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x08C0+$SLICEID*0x1000 +$DE3ID"
            
        def startAddress(self):
            return 0x000008c0
            
        def endAddress(self):
            return 0x000008ef

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_seq_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "cfg_seq_tx"
            
            def description(self):
                return "config enable Tx continous, (0) is disable, (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_entx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "cfg_entx"
            
            def description(self):
                return "config enable Tx, (0) is disable, (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_fcs_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfg_fcs_tx"
            
            def description(self):
                return "config mode transmit FCS, (1) is T.403-MSB, (0) is T.107-LSB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdlstd_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_mdlstd_tx"
            
            def description(self):
                return "config standard Tx, (0) is ANSI, (1) is AT&T"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_cr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_mdl_cr"
            
            def description(self):
                return "config bit command/respond MDL message, bit 0 is channel 0 of DS3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["reserve"] = _AF6CNC0022_RD_PDH_MDL._upen_cfg_mdl._reserve()
            allFields["cfg_seq_tx"] = _AF6CNC0022_RD_PDH_MDL._upen_cfg_mdl._cfg_seq_tx()
            allFields["cfg_entx"] = _AF6CNC0022_RD_PDH_MDL._upen_cfg_mdl._cfg_entx()
            allFields["cfg_fcs_tx"] = _AF6CNC0022_RD_PDH_MDL._upen_cfg_mdl._cfg_fcs_tx()
            allFields["cfg_mdlstd_tx"] = _AF6CNC0022_RD_PDH_MDL._upen_cfg_mdl._cfg_mdlstd_tx()
            allFields["cfg_mdl_cr"] = _AF6CNC0022_RD_PDH_MDL._upen_cfg_mdl._cfg_mdl_cr()
            return allFields

    class _upen_txmdl_cntr2c_valididle1(AtRegister.AtRegister):
        def name(self):
            return "COUNTER VALID MESSAGE TX MDL IDLE"
    
        def description(self):
            return "counter valid read to clear IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0C00+ $SLICEID*0x1000 + $DE3ID + $UPRO * 256"
            
        def startAddress(self):
            return 0x00000c00
            
        def endAddress(self):
            return 0x00000d2f

        class _cntr2c_valid_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_valid_idle_mdl"
            
            def description(self):
                return "value counter valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_valid_idle_mdl"] = _AF6CNC0022_RD_PDH_MDL._upen_txmdl_cntr2c_valididle1._cntr2c_valid_idle_mdl()
            return allFields

    class _upen_txmdl_cntr2c_validpath(AtRegister.AtRegister):
        def name(self):
            return "COUNTER VALID MESSAGE TX MDL PATH"
    
        def description(self):
            return "counter valid read to clear PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0C40+ $SLICEID*0x1000 + $DE3ID + $UPRO * 256"
            
        def startAddress(self):
            return 0x00000c40
            
        def endAddress(self):
            return 0x00000d6f

        class _cntr2c_valid_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_valid_path_mdl"
            
            def description(self):
                return "value counter valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_valid_path_mdl"] = _AF6CNC0022_RD_PDH_MDL._upen_txmdl_cntr2c_validpath._cntr2c_valid_path_mdl()
            return allFields

    class _upen_txmdl_cntr2c_validtest(AtRegister.AtRegister):
        def name(self):
            return "COUNTER VALID MESSAGE TX MDL TEST"
    
        def description(self):
            return "counter valid read to clear TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0C80+ $SLICEID*0x1000 + $DE3ID + $UPRO * 256"
            
        def startAddress(self):
            return 0x00000c80
            
        def endAddress(self):
            return 0x00000daf

        class _cntr2c_valid_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_valid_test_mdl"
            
            def description(self):
                return "value counter valid"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_valid_test_mdl"] = _AF6CNC0022_RD_PDH_MDL._upen_txmdl_cntr2c_validtest._cntr2c_valid_test_mdl()
            return allFields

    class _upen_txmdl_cntr2c_byteidle(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE TX MDL IDLE"
    
        def description(self):
            return "counter byte read to clear IDLE message MDL"
            
        def width(self):
            return 18
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0E00+ $SLICEID*0x1000 + $DE3ID + $UPRO * 256"
            
        def startAddress(self):
            return 0x00000e00
            
        def endAddress(self):
            return 0x00000f2f

        class _cntr2c_byte_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_byte_idle_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_byte_idle_mdl"] = _AF6CNC0022_RD_PDH_MDL._upen_txmdl_cntr2c_byteidle._cntr2c_byte_idle_mdl()
            return allFields

    class _upen_txmdl_cntr2c_bytepath(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE TX MDL PATH"
    
        def description(self):
            return "counter byte read to clear PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0E40+ $SLICEID*0x1000 + $DE3ID + $UPRO * 256"
            
        def startAddress(self):
            return 0x00000e40
            
        def endAddress(self):
            return 0x00000f6f

        class _cntr2c_byte_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_byte_path_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_byte_path_mdl"] = _AF6CNC0022_RD_PDH_MDL._upen_txmdl_cntr2c_bytepath._cntr2c_byte_path_mdl()
            return allFields

    class _upen_txmdl_cntr2c_bytetest(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE TX MDL TEST R2C"
    
        def description(self):
            return "counter byte read to clear TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0E80+ $SLICEID*0x1000 + $DE3ID + $UPRO * 256"
            
        def startAddress(self):
            return 0x00000e80
            
        def endAddress(self):
            return 0x00000faf

        class _cntr2c_byte_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_byte_test_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_byte_test_mdl"] = _AF6CNC0022_RD_PDH_MDL._upen_txmdl_cntr2c_bytetest._cntr2c_byte_test_mdl()
            return allFields

    class _upen_rxmdl_typebuff(AtRegister.AtRegister):
        def name(self):
            return "Buff Message MDL with configuration type"
    
        def description(self):
            return "buffer message MDL which is configured type"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x44000 + $STSID* 256 + $SLICEID*32 + $RXDWORDID"
            
        def startAddress(self):
            return 0x00044000
            
        def endAddress(self):
            return 0x00047fff

        class _mdl_tbyte3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "mdl_tbyte3"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _mdl_tbyte2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "mdl_tbyte2"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _mdl_tbyte1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "mdl_tbyte1"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _mdl_tbyte0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_tbyte0"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_tbyte3"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_typebuff._mdl_tbyte3()
            allFields["mdl_tbyte2"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_typebuff._mdl_tbyte2()
            allFields["mdl_tbyte1"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_typebuff._mdl_tbyte1()
            allFields["mdl_tbyte0"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_typebuff._mdl_tbyte0()
            return allFields

    class _upen_rxmdl_cfgtype(AtRegister.AtRegister):
        def name(self):
            return "Config Buff Message MDL TYPE"
    
        def description(self):
            return "config type message MDL"
            
        def width(self):
            return 10
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4A800+ $STSID*8 + $SLICEID"
            
        def startAddress(self):
            return 0x0004a800
            
        def endAddress(self):
            return 0x0004a97f

        class _cfg_fcs_rx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "cfg_fcs_rx"
            
            def description(self):
                return "config mode receive FCS, (1) is T.403-MSB, (0) is T.107-LSB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_cr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "cfg_mdl_cr"
            
            def description(self):
                return "config C/R bit expected"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_std(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "cfg_mdl_std"
            
            def description(self):
                return "(0) is ANSI, (1) is AT&T"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_mask_test(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfg_mdl_mask_test"
            
            def description(self):
                return "config enable mask to moitor test massage (1): enable, (0): disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_mask_path(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_mdl_mask_path"
            
            def description(self):
                return "config enable mask to moitor path massage (1): enable, (0): disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_mask_idle(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_mdl_mask_idle"
            
            def description(self):
                return "config enable mask to moitor idle massage (1): enable, (0): disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_fcs_rx"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_cfgtype._cfg_fcs_rx()
            allFields["cfg_mdl_cr"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_cfgtype._cfg_mdl_cr()
            allFields["cfg_mdl_std"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_cfgtype._cfg_mdl_std()
            allFields["cfg_mdl_mask_test"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_cfgtype._cfg_mdl_mask_test()
            allFields["cfg_mdl_mask_path"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_cfgtype._cfg_mdl_mask_path()
            allFields["cfg_mdl_mask_idle"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_cfgtype._cfg_mdl_mask_idle()
            return allFields

    class _upen_destuff_ctrl0(AtRegister.AtRegister):
        def name(self):
            return "CONFIG CONTROL DESTUFF 0"
    
        def description(self):
            return "config control DeStuff global"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0004aa00
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_crmon(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "cfg_crmon"
            
            def description(self):
                return "(0) : disable, (1) : enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "reserve1"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fcsmon(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "fcsmon"
            
            def description(self):
                return "(0) : disable monitor, (1) : enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 1
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _headermon_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "headermon_en"
            
            def description(self):
                return "(1) : enable monitor, (0) disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_crmon"] = _AF6CNC0022_RD_PDH_MDL._upen_destuff_ctrl0._cfg_crmon()
            allFields["reserve1"] = _AF6CNC0022_RD_PDH_MDL._upen_destuff_ctrl0._reserve1()
            allFields["fcsmon"] = _AF6CNC0022_RD_PDH_MDL._upen_destuff_ctrl0._fcsmon()
            allFields["reserve"] = _AF6CNC0022_RD_PDH_MDL._upen_destuff_ctrl0._reserve()
            allFields["headermon_en"] = _AF6CNC0022_RD_PDH_MDL._upen_destuff_ctrl0._headermon_en()
            return allFields

    class _upen_mdl_stk_cfg1(AtRegister.AtRegister):
        def name(self):
            return "STICKY TO RECEIVE PACKET MDL BUFF CONFIG1"
    
        def description(self):
            return "engine set to alarm message event, and CPU clear to be received new messase"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0004aa10
            
        def endAddress(self):
            return 0xffffffff

        class _mdl_cfg_alr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_cfg_alr"
            
            def description(self):
                return "sticky for new message channel 0-31"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfg_alr"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_stk_cfg1._mdl_cfg_alr()
            return allFields

    class _upen_mdl_stk_cfg2(AtRegister.AtRegister):
        def name(self):
            return "STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 2"
    
        def description(self):
            return "engine set to alarm message event, and CPU clear to be received new messase"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0004aa11
            
        def endAddress(self):
            return 0xffffffff

        class _mdl_cfg_alr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_cfg_alr"
            
            def description(self):
                return "sticky for new message channel 32 -63"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfg_alr"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_stk_cfg2._mdl_cfg_alr()
            return allFields

    class _upen_mdl_stk_cfg3(AtRegister.AtRegister):
        def name(self):
            return "STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 3"
    
        def description(self):
            return "engine set to alarm message event, and CPU clear to be received new messase"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0004aa12
            
        def endAddress(self):
            return 0xffffffff

        class _mdl_cfg_alr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_cfg_alr"
            
            def description(self):
                return "sticky for new message channel 64 -95"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfg_alr"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_stk_cfg3._mdl_cfg_alr()
            return allFields

    class _upen_mdl_stk_cfg4(AtRegister.AtRegister):
        def name(self):
            return "STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 4"
    
        def description(self):
            return "engine set to alarm message event, and CPU clear to be received new messase"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0004aa13
            
        def endAddress(self):
            return 0xffffffff

        class _mdl_cfg_alr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_cfg_alr"
            
            def description(self):
                return "sticky for new message channel 96 -127"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfg_alr"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_stk_cfg4._mdl_cfg_alr()
            return allFields

    class _upen_mdl_stk_cfg5(AtRegister.AtRegister):
        def name(self):
            return "STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 5"
    
        def description(self):
            return "engine set to alarm message event, and CPU clear to be received new messase"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0004aa14
            
        def endAddress(self):
            return 0xffffffff

        class _mdl_cfg_alr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_cfg_alr"
            
            def description(self):
                return "sticky for new message channel 128 -159"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfg_alr"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_stk_cfg5._mdl_cfg_alr()
            return allFields

    class _upen_mdl_stk_cfg6(AtRegister.AtRegister):
        def name(self):
            return "STICKY TO RECEIVE PACKET MDL BUFF CONFIG 6"
    
        def description(self):
            return "engine set to alarm message event, and CPU clear to be received new messase"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0004aa15
            
        def endAddress(self):
            return 0xffffffff

        class _mdl_cfg_alr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_cfg_alr"
            
            def description(self):
                return "sticky for new message channel 160 -191"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfg_alr"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_stk_cfg6._mdl_cfg_alr()
            return allFields

    class _upen_mdl_stk_cfg7(AtRegister.AtRegister):
        def name(self):
            return "STICKY TO RECEIVE PACKET MDL BUFF CONFIG7"
    
        def description(self):
            return "engine set to alarm message event, and CPU clear to be received new messase"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0004aa16
            
        def endAddress(self):
            return 0xffffffff

        class _mdl_cfg_alr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_cfg_alr"
            
            def description(self):
                return "sticky for new message channel 192-223"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfg_alr"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_stk_cfg7._mdl_cfg_alr()
            return allFields

    class _upen_mdl_stk_cfg8(AtRegister.AtRegister):
        def name(self):
            return "STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 8"
    
        def description(self):
            return "engine set to alarm message event, and CPU clear to be received new messase"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0004aa17
            
        def endAddress(self):
            return 0xffffffff

        class _mdl_cfg_alr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_cfg_alr"
            
            def description(self):
                return "sticky for new message channel 224 -255"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfg_alr"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_stk_cfg8._mdl_cfg_alr()
            return allFields

    class _upen_mdl_stk_cfg9(AtRegister.AtRegister):
        def name(self):
            return "STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 9"
    
        def description(self):
            return "engine set to alarm message event, and CPU clear to be received new messase"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0004aa18
            
        def endAddress(self):
            return 0xffffffff

        class _mdl_cfg_alr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_cfg_alr"
            
            def description(self):
                return "sticky for new message channel 256 -287"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfg_alr"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_stk_cfg9._mdl_cfg_alr()
            return allFields

    class _upen_mdl_stk_cfg10(AtRegister.AtRegister):
        def name(self):
            return "STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 10"
    
        def description(self):
            return "engine set to alarm message event, and CPU clear to be received new messase"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0004aa19
            
        def endAddress(self):
            return 0xffffffff

        class _mdl_cfg_alr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_cfg_alr"
            
            def description(self):
                return "sticky for new message channel 288 -319"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfg_alr"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_stk_cfg10._mdl_cfg_alr()
            return allFields

    class _upen_mdl_stk_cfg11(AtRegister.AtRegister):
        def name(self):
            return "STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 11"
    
        def description(self):
            return "engine set to alarm message event, and CPU clear to be received new messase"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0004aa1a
            
        def endAddress(self):
            return 0xffffffff

        class _mdl_cfg_alr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_cfg_alr"
            
            def description(self):
                return "sticky for new message channel 320 -351"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfg_alr"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_stk_cfg11._mdl_cfg_alr()
            return allFields

    class _upen_mdl_stk_cfg12(AtRegister.AtRegister):
        def name(self):
            return "STICKY TO RECEIVE PACKET MDL BUFF CONFIG 12"
    
        def description(self):
            return "engine set to alarm message event, and CPU clear to be received new messase"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0004aa1b
            
        def endAddress(self):
            return 0xffffffff

        class _mdl_cfg_alr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_cfg_alr"
            
            def description(self):
                return "sticky for new message channel 352 -383"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfg_alr"] = _AF6CNC0022_RD_PDH_MDL._upen_mdl_stk_cfg12._mdl_cfg_alr()
            return allFields

    class _upen_rxmdl_cntr2c_byteidle(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE RX MDL IDLE"
    
        def description(self):
            return "counter byte read to clear IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x4D000+ $STSID*8 + $SLICEID + $UPRO * 2048"
            
        def startAddress(self):
            return 0x0004d000
            
        def endAddress(self):
            return 0x0004d97f

        class _cntr2c_byte_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_byte_idle_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_byte_idle_mdl"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_cntr2c_byteidle._cntr2c_byte_idle_mdl()
            return allFields

    class _upen_rxmdl_cntr2c_bytepath(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE RX MDL PATH"
    
        def description(self):
            return "counter byte read to clear PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x4D200+ $STSID*8 + $SLICEID + $UPRO * 2048"
            
        def startAddress(self):
            return 0x0004d200
            
        def endAddress(self):
            return 0x0004db7f

        class _cntr2c_byte_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_byte_path_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_byte_path_mdl"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_cntr2c_bytepath._cntr2c_byte_path_mdl()
            return allFields

    class _upen_rxmdl_cntr2c_bytetest(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE RX MDL TEST"
    
        def description(self):
            return "counter byte read to clear TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x4D400+ $STSID*8 + $SLICEID + $UPRO * 2048"
            
        def startAddress(self):
            return 0x0004d400
            
        def endAddress(self):
            return 0x0004df7f

        class _cntr2c_byte_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_byte_test_mdl"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_byte_test_mdl"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_cntr2c_bytetest._cntr2c_byte_test_mdl()
            return allFields

    class _upen_rxmdl_cntr2c_goodidle(AtRegister.AtRegister):
        def name(self):
            return "COUNTER GOOD MESSAGE RX MDL IDLE"
    
        def description(self):
            return "counter good read to clear IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x4E000+ $STSID*8 + $SLICEID + $UPRO * 4096"
            
        def startAddress(self):
            return 0x0004e000
            
        def endAddress(self):
            return 0x0004f17f

        class _cntr2c_good_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_good_idle_mdl"
            
            def description(self):
                return "value counter good"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_good_idle_mdl"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_cntr2c_goodidle._cntr2c_good_idle_mdl()
            return allFields

    class _upen_rxmdl_cntr2c_goodpath(AtRegister.AtRegister):
        def name(self):
            return "COUNTER GOOD MESSAGE RX MDL PATH"
    
        def description(self):
            return "counter good read to clear PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x4E200+ $STSID*8 + $SLICEID + $UPRO * 4096"
            
        def startAddress(self):
            return 0x0004e200
            
        def endAddress(self):
            return 0x0004f37f

        class _cntr2c_good_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_good_path_mdl"
            
            def description(self):
                return "value counter good"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_good_path_mdl"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_cntr2c_goodpath._cntr2c_good_path_mdl()
            return allFields

    class _upen_rxmdl_cntr2c_goodtest(AtRegister.AtRegister):
        def name(self):
            return "COUNTER GOOD MESSAGE RX MDL TEST"
    
        def description(self):
            return "counter good read to clear TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x4E400+ $STSID*8 + $SLICEID + $UPRO * 4096"
            
        def startAddress(self):
            return 0x0004e400
            
        def endAddress(self):
            return 0x0004f57f

        class _cntr2c_good_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_good_test_mdl"
            
            def description(self):
                return "value counter good"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_good_test_mdl"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_cntr2c_goodtest._cntr2c_good_test_mdl()
            return allFields

    class _upen_rxmdl_cntr2c_dropidle(AtRegister.AtRegister):
        def name(self):
            return "COUNTER DROP MESSAGE RX MDL IDLE"
    
        def description(self):
            return "counter drop read to clear IDLE message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x4E600+ $STSID*8 + $SLICEID + $UPRO * 4096"
            
        def startAddress(self):
            return 0x0004e600
            
        def endAddress(self):
            return 0x0004f77f

        class _cntr2c_drop_idle_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_drop_idle_mdl"
            
            def description(self):
                return "value counter drop"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_drop_idle_mdl"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_cntr2c_dropidle._cntr2c_drop_idle_mdl()
            return allFields

    class _upen_rxmdl_cntr2c_droppath(AtRegister.AtRegister):
        def name(self):
            return "COUNTER DROP MESSAGE RX MDL PATH"
    
        def description(self):
            return "counter drop read to clear PATH message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x4E800+ $STSID*8 + $SLICEID + $UPRO * 4096"
            
        def startAddress(self):
            return 0x0004e800
            
        def endAddress(self):
            return 0x0004f97f

        class _cntr2c_drop_path_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_drop_path_mdl"
            
            def description(self):
                return "value counter drop"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_drop_path_mdl"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_cntr2c_droppath._cntr2c_drop_path_mdl()
            return allFields

    class _upen_rxmdl_cntr2c_droptest(AtRegister.AtRegister):
        def name(self):
            return "COUNTER DROP MESSAGE RX MDL TEST R2C"
    
        def description(self):
            return "counter drop read to clear TEST message MDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x4EA00+ $STSID*8 + $SLICEID + $UPRO * 4096"
            
        def startAddress(self):
            return 0x0004ea00
            
        def endAddress(self):
            return 0x0004fbff

        class _cntr2c_drop_test_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cntr2c_drop_test_mdl"
            
            def description(self):
                return "value counter drop"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cntr2c_drop_test_mdl"] = _AF6CNC0022_RD_PDH_MDL._upen_rxmdl_cntr2c_droptest._cntr2c_drop_test_mdl()
            return allFields

    class _upen_int_ndlsta(AtRegister.AtRegister):
        def name(self):
            return "RX MDL INT STA"
    
        def description(self):
            return "config control DeStuff global"
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0004aa06
            
        def endAddress(self):
            return 0xffffffff

        class _mdllb_intsta2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "mdllb_intsta2"
            
            def description(self):
                return "interrupt sta MDL 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _mdllb_intsta1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdllb_intsta1"
            
            def description(self):
                return "interrupt sta MDL 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdllb_intsta2"] = _AF6CNC0022_RD_PDH_MDL._upen_int_ndlsta._mdllb_intsta2()
            allFields["mdllb_intsta1"] = _AF6CNC0022_RD_PDH_MDL._upen_int_ndlsta._mdllb_intsta1()
            return allFields

    class _mdl_cfgen_int1(AtRegister.AtRegister):
        def name(self):
            return "MDL per Channel Interrupt1 Enable Control"
    
        def description(self):
            return "This is the per Channel interrupt enable"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4A000 +  $STSID1 + $SLICEID * 32"
            
        def startAddress(self):
            return 0x0004a000
            
        def endAddress(self):
            return 0x0004a0ff

        class _mdl_cfgen_int1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_cfgen_int1"
            
            def description(self):
                return "Set 1 to enable change event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfgen_int1"] = _AF6CNC0022_RD_PDH_MDL._mdl_cfgen_int1._mdl_cfgen_int1()
            return allFields

    class _mdl_int1_sta(AtRegister.AtRegister):
        def name(self):
            return "MDL Interrupt1 per Channel Interrupt Status"
    
        def description(self):
            return "This is the per Channel interrupt status."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x4A200 +  $STSID1 + $SLICEID * 32"
            
        def startAddress(self):
            return 0x0004a200
            
        def endAddress(self):
            return 0x0004a2ff

        class _mdlint1_sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdlint1_sta"
            
            def description(self):
                return "Set 1 if there is a change event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdlint1_sta"] = _AF6CNC0022_RD_PDH_MDL._mdl_int1_sta._mdlint1_sta()
            return allFields

    class _mdl_int1_crrsta(AtRegister.AtRegister):
        def name(self):
            return "MDL Interrupt per Channel Current Status"
    
        def description(self):
            return "This is the per Channel Current status."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x4A400 +  $STSID1 + $SLICEID * 32"
            
        def startAddress(self):
            return 0x0004a400
            
        def endAddress(self):
            return 0x0004a4ff

        class _mdlint1_crrsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdlint1_crrsta"
            
            def description(self):
                return "Current status of event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdlint1_crrsta"] = _AF6CNC0022_RD_PDH_MDL._mdl_int1_crrsta._mdlint1_crrsta()
            return allFields

    class _mdl_int1sta(AtRegister.AtRegister):
        def name(self):
            return "MDL Interrupt1 per Channel Interrupt OR Status"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x4A600 +  $GID"
            
        def startAddress(self):
            return 0x0004a600
            
        def endAddress(self):
            return 0x0004a607

        class _mdlint1sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdlint1sta"
            
            def description(self):
                return "Set to 1 if any interrupt status bit of corresponding channel is set and its interrupt is enabled."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdlint1sta"] = _AF6CNC0022_RD_PDH_MDL._mdl_int1sta._mdlint1sta()
            return allFields

    class _mdl_sta_int1(AtRegister.AtRegister):
        def name(self):
            return "MDL Interrupt1 OR Status"
    
        def description(self):
            return "The register consists of 2 bits. Each bit is used to store Interrupt OR status."
            
        def width(self):
            return 8
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0004a6ff
            
        def endAddress(self):
            return 0xffffffff

        class _mdlsta_int1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdlsta_int1"
            
            def description(self):
                return "Set to 1 if any interrupt status bit is set and its interrupt is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdlsta_int1"] = _AF6CNC0022_RD_PDH_MDL._mdl_sta_int1._mdlsta_int1()
            return allFields

    class _mdl_en_int1(AtRegister.AtRegister):
        def name(self):
            return "MDL Interrupt1 Enable Control"
    
        def description(self):
            return "The register consists of 2 interrupt enable bits ."
            
        def width(self):
            return 8
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0004a6fe
            
        def endAddress(self):
            return 0xffffffff

        class _mdlen_int1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdlen_int1"
            
            def description(self):
                return "Set to 1 to enable to generate interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdlen_int1"] = _AF6CNC0022_RD_PDH_MDL._mdl_en_int1._mdlen_int1()
            return allFields

    class _mdl_cfgen_int2(AtRegister.AtRegister):
        def name(self):
            return "MDL per Channel Interrupt2 Enable Control"
    
        def description(self):
            return "This is the per Channel interrupt enable"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4A100 +  $STSID2 + $SLICEID * 32"
            
        def startAddress(self):
            return 0x0004a100
            
        def endAddress(self):
            return 0x0004a1ff

        class _mdl_cfgen_int2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_cfgen_int2"
            
            def description(self):
                return "Set 1 to enable change event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfgen_int2"] = _AF6CNC0022_RD_PDH_MDL._mdl_cfgen_int2._mdl_cfgen_int2()
            return allFields

    class _mdl_int2_sta(AtRegister.AtRegister):
        def name(self):
            return "MDL Interrupt2 per Channel Interrupt Status"
    
        def description(self):
            return "This is the per Channel interrupt status."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x4A300 +  $STSID2 + $SLICEID * 32"
            
        def startAddress(self):
            return 0x0004a300
            
        def endAddress(self):
            return 0x0004a3ff

        class _mdlint2_sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdlint2_sta"
            
            def description(self):
                return "Set 1 if there is a change event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdlint2_sta"] = _AF6CNC0022_RD_PDH_MDL._mdl_int2_sta._mdlint2_sta()
            return allFields

    class _mdl_int2_crrsta(AtRegister.AtRegister):
        def name(self):
            return "MDL Interrupt per Channel Current Status"
    
        def description(self):
            return "This is the per Channel Current status."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x4A500 +  $STSID2 + $SLICEID * 32"
            
        def startAddress(self):
            return 0x0004a500
            
        def endAddress(self):
            return 0x0004a5ff

        class _mdlint2_crrsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdlint2_crrsta"
            
            def description(self):
                return "Current status of event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdlint2_crrsta"] = _AF6CNC0022_RD_PDH_MDL._mdl_int2_crrsta._mdlint2_crrsta()
            return allFields

    class _mdl_int2sta(AtRegister.AtRegister):
        def name(self):
            return "MDL Interrupt2 per Channel Interrupt OR Status"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x4A700 +  $GID"
            
        def startAddress(self):
            return 0x0004a700
            
        def endAddress(self):
            return 0x0004a707

        class _mdlint2sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdlint2sta"
            
            def description(self):
                return "Set to 1 if any interrupt status bit of corresponding channel is set and its interrupt is enabled."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdlint2sta"] = _AF6CNC0022_RD_PDH_MDL._mdl_int2sta._mdlint2sta()
            return allFields

    class _mdl_sta_int2(AtRegister.AtRegister):
        def name(self):
            return "MDL Interrupt2 OR Status"
    
        def description(self):
            return "The register consists of 2 bits. Each bit is used to store Interrupt OR status."
            
        def width(self):
            return 8
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0004a7ff
            
        def endAddress(self):
            return 0xffffffff

        class _mdlsta_int2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdlsta_int2"
            
            def description(self):
                return "Set to 1 if any interrupt status bit is set and its interrupt is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdlsta_int2"] = _AF6CNC0022_RD_PDH_MDL._mdl_sta_int2._mdlsta_int2()
            return allFields

    class _mdl_en_int2(AtRegister.AtRegister):
        def name(self):
            return "MDL Interrupt2 Enable Control"
    
        def description(self):
            return "The register consists of 2 interrupt enable bits ."
            
        def width(self):
            return 8
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0004a7fe
            
        def endAddress(self):
            return 0xffffffff

        class _mdlen_int2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdlen_int2"
            
            def description(self):
                return "Set to 1 to enable to generate interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdlen_int2"] = _AF6CNC0022_RD_PDH_MDL._mdl_en_int2._mdlen_int2()
            return allFields

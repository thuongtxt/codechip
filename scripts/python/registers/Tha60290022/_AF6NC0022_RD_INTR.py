import python.arrive.atsdk.AtRegister as AtRegister

class _AF6NC0022_RD_INTR(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["global_interrupt_status"] = _AF6NC0022_RD_INTR._global_interrupt_status()
        allRegisters["global_interrupt_mask_enable"] = _AF6NC0022_RD_INTR._global_interrupt_mask_enable()
        allRegisters["global_pdh_interrupt_status"] = _AF6NC0022_RD_INTR._global_pdh_interrupt_status()
        allRegisters["global_interrupt_pin_disable"] = _AF6NC0022_RD_INTR._global_interrupt_pin_disable()
        allRegisters["global_interrupt_mask_restore"] = _AF6NC0022_RD_INTR._global_interrupt_mask_restore()
        allRegisters["global_parity_interrupt_mask_enable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable()
        allRegisters["global_parity_interrupt_status"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status()
        allRegisters["global_locdr_interrupt_status"] = _AF6NC0022_RD_INTR._global_locdr_interrupt_status()
        allRegisters["global_pmc_2page_interrupt_status"] = _AF6NC0022_RD_INTR._global_pmc_2page_interrupt_status()
        return allRegisters

    class _global_interrupt_status(AtRegister.AtRegister):
        def name(self):
            return "Global Interrupt Status"
    
        def description(self):
            return "This register indicate global interrupt status."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _ExtPinIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "ExtPinIntStatus"
            
            def description(self):
                return "External Pin Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _ExtOrIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "ExtOrIntStatus"
            
            def description(self):
                return "External OR Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _ReserveIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 20
        
            def name(self):
                return "ReserveIntStatus"
            
            def description(self):
                return "Reserve Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _DccKIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "DccKIntStatus"
            
            def description(self):
                return "DCCK Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Mac40gIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 17
        
            def name(self):
                return "Mac40gIntStatus"
            
            def description(self):
                return "Two Mac40G Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _UpsrIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "UpsrIntStatus"
            
            def description(self):
                return "UPSR Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _TenGeIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "TenGeIntStatus"
            
            def description(self):
                return "Chip SEM Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _GeIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "GeIntStatus"
            
            def description(self):
                return "Chip SEM Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _GlobalPmc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "GlobalPmc"
            
            def description(self):
                return "Global PMC 2 Page Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _SemIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "SemIntStatus"
            
            def description(self):
                return "Chip SEM Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _PtpIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PtpIntStatus"
            
            def description(self):
                return "PTP Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _LoCdrIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "LoCdrIntStatus"
            
            def description(self):
                return "Low Order CDR Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _MdlIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "MdlIntStatus"
            
            def description(self):
                return "MDL Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _PrmIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PrmIntStatus"
            
            def description(self):
                return "PRM Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _PmIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PmIntStatus"
            
            def description(self):
                return "PM Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _FmIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FmIntStatus"
            
            def description(self):
                return "FM Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _TFI5IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "TFI5IntStatus"
            
            def description(self):
                return "TFI5 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _STSIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "STSIntStatus"
            
            def description(self):
                return "STS Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _VTIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "VTIntStatus"
            
            def description(self):
                return "VT Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DE3IntStatus"
            
            def description(self):
                return "DS3/E3 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DE1IntStatus"
            
            def description(self):
                return "DS1/E1 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _PWEIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PWEIntStatus"
            
            def description(self):
                return "PWE Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ExtPinIntStatus"] = _AF6NC0022_RD_INTR._global_interrupt_status._ExtPinIntStatus()
            allFields["ExtOrIntStatus"] = _AF6NC0022_RD_INTR._global_interrupt_status._ExtOrIntStatus()
            allFields["ReserveIntStatus"] = _AF6NC0022_RD_INTR._global_interrupt_status._ReserveIntStatus()
            allFields["DccKIntStatus"] = _AF6NC0022_RD_INTR._global_interrupt_status._DccKIntStatus()
            allFields["Mac40gIntStatus"] = _AF6NC0022_RD_INTR._global_interrupt_status._Mac40gIntStatus()
            allFields["UpsrIntStatus"] = _AF6NC0022_RD_INTR._global_interrupt_status._UpsrIntStatus()
            allFields["TenGeIntStatus"] = _AF6NC0022_RD_INTR._global_interrupt_status._TenGeIntStatus()
            allFields["GeIntStatus"] = _AF6NC0022_RD_INTR._global_interrupt_status._GeIntStatus()
            allFields["GlobalPmc"] = _AF6NC0022_RD_INTR._global_interrupt_status._GlobalPmc()
            allFields["SemIntStatus"] = _AF6NC0022_RD_INTR._global_interrupt_status._SemIntStatus()
            allFields["PtpIntStatus"] = _AF6NC0022_RD_INTR._global_interrupt_status._PtpIntStatus()
            allFields["LoCdrIntStatus"] = _AF6NC0022_RD_INTR._global_interrupt_status._LoCdrIntStatus()
            allFields["MdlIntStatus"] = _AF6NC0022_RD_INTR._global_interrupt_status._MdlIntStatus()
            allFields["PrmIntStatus"] = _AF6NC0022_RD_INTR._global_interrupt_status._PrmIntStatus()
            allFields["PmIntStatus"] = _AF6NC0022_RD_INTR._global_interrupt_status._PmIntStatus()
            allFields["FmIntStatus"] = _AF6NC0022_RD_INTR._global_interrupt_status._FmIntStatus()
            allFields["TFI5IntStatus"] = _AF6NC0022_RD_INTR._global_interrupt_status._TFI5IntStatus()
            allFields["STSIntStatus"] = _AF6NC0022_RD_INTR._global_interrupt_status._STSIntStatus()
            allFields["VTIntStatus"] = _AF6NC0022_RD_INTR._global_interrupt_status._VTIntStatus()
            allFields["DE3IntStatus"] = _AF6NC0022_RD_INTR._global_interrupt_status._DE3IntStatus()
            allFields["DE1IntStatus"] = _AF6NC0022_RD_INTR._global_interrupt_status._DE1IntStatus()
            allFields["PWEIntStatus"] = _AF6NC0022_RD_INTR._global_interrupt_status._PWEIntStatus()
            return allFields

    class _global_interrupt_mask_enable(AtRegister.AtRegister):
        def name(self):
            return "Global Interrupt Mask Enable"
    
        def description(self):
            return "This register configures global interrupt mask enable."
            
        def width(self):
            return 20
        
        def type(self):
            return "Config|Enable"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _DccKIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "DccKIntEnable"
            
            def description(self):
                return "DccK Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Mac40gIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 17
        
            def name(self):
                return "Mac40gIntEnable"
            
            def description(self):
                return "Two MAC40G Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _UpsrIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "UpsrIntEnable"
            
            def description(self):
                return "UPSR Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _TenGeIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "TenGeIntEnable"
            
            def description(self):
                return "Ten Ge Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _GeIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "GeIntEnable"
            
            def description(self):
                return "Ge Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _GlbpmcIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "GlbpmcIntEnable"
            
            def description(self):
                return "GLB PMC Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _SemIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "SemIntEnable"
            
            def description(self):
                return "Chip SEM Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _PtpIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PtpIntEnable"
            
            def description(self):
                return "PTP  Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CdrIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "CdrIntEnable"
            
            def description(self):
                return "CDR Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MdlIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "MdlIntEnable"
            
            def description(self):
                return "MDL Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PrmIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PrmIntEnable"
            
            def description(self):
                return "PRM Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PmIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PmIntEnable"
            
            def description(self):
                return "PM Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FmIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FmIntEnable"
            
            def description(self):
                return "FM Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TFI5IntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "TFI5IntEnable"
            
            def description(self):
                return "TFI5 Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STSIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "STSIntEnable"
            
            def description(self):
                return "STS Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VTIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "VTIntEnable"
            
            def description(self):
                return "VT Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3IntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DE3IntEnable"
            
            def description(self):
                return "DS3/E3 Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1IntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DE1IntEnable"
            
            def description(self):
                return "DS1/E1 Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWEIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PWEIntEnable"
            
            def description(self):
                return "PWE Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DccKIntEnable"] = _AF6NC0022_RD_INTR._global_interrupt_mask_enable._DccKIntEnable()
            allFields["Mac40gIntEnable"] = _AF6NC0022_RD_INTR._global_interrupt_mask_enable._Mac40gIntEnable()
            allFields["UpsrIntEnable"] = _AF6NC0022_RD_INTR._global_interrupt_mask_enable._UpsrIntEnable()
            allFields["TenGeIntEnable"] = _AF6NC0022_RD_INTR._global_interrupt_mask_enable._TenGeIntEnable()
            allFields["GeIntEnable"] = _AF6NC0022_RD_INTR._global_interrupt_mask_enable._GeIntEnable()
            allFields["GlbpmcIntEnable"] = _AF6NC0022_RD_INTR._global_interrupt_mask_enable._GlbpmcIntEnable()
            allFields["SemIntEnable"] = _AF6NC0022_RD_INTR._global_interrupt_mask_enable._SemIntEnable()
            allFields["PtpIntEnable"] = _AF6NC0022_RD_INTR._global_interrupt_mask_enable._PtpIntEnable()
            allFields["CdrIntEnable"] = _AF6NC0022_RD_INTR._global_interrupt_mask_enable._CdrIntEnable()
            allFields["MdlIntEnable"] = _AF6NC0022_RD_INTR._global_interrupt_mask_enable._MdlIntEnable()
            allFields["PrmIntEnable"] = _AF6NC0022_RD_INTR._global_interrupt_mask_enable._PrmIntEnable()
            allFields["PmIntEnable"] = _AF6NC0022_RD_INTR._global_interrupt_mask_enable._PmIntEnable()
            allFields["FmIntEnable"] = _AF6NC0022_RD_INTR._global_interrupt_mask_enable._FmIntEnable()
            allFields["TFI5IntEnable"] = _AF6NC0022_RD_INTR._global_interrupt_mask_enable._TFI5IntEnable()
            allFields["STSIntEnable"] = _AF6NC0022_RD_INTR._global_interrupt_mask_enable._STSIntEnable()
            allFields["VTIntEnable"] = _AF6NC0022_RD_INTR._global_interrupt_mask_enable._VTIntEnable()
            allFields["DE3IntEnable"] = _AF6NC0022_RD_INTR._global_interrupt_mask_enable._DE3IntEnable()
            allFields["DE1IntEnable"] = _AF6NC0022_RD_INTR._global_interrupt_mask_enable._DE1IntEnable()
            allFields["PWEIntEnable"] = _AF6NC0022_RD_INTR._global_interrupt_mask_enable._PWEIntEnable()
            return allFields

    class _global_pdh_interrupt_status(AtRegister.AtRegister):
        def name(self):
            return "Global PDH Interrupt Status"
    
        def description(self):
            return "This register indicate global pdh interrupt status."
            
        def width(self):
            return 16
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0xffffffff

        class _DE3Slice8IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "DE3Slice8IntStatus"
            
            def description(self):
                return "DE3 Slice8 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3Slice7IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "DE3Slice7IntStatus"
            
            def description(self):
                return "DE3 Slice7 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3Slice6IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "DE3Slice6IntStatus"
            
            def description(self):
                return "DE3 Slice6 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3Slice5IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DE3Slice5IntStatus"
            
            def description(self):
                return "DE3 Slice5 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3Slice4IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "DE3Slice4IntStatus"
            
            def description(self):
                return "DE3 Slice4 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3Slice3IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DE3Slice3IntStatus"
            
            def description(self):
                return "DE3 Slice3 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3Slice2IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DE3Slice2IntStatus"
            
            def description(self):
                return "DE3 Slice2 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3Slice1IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DE3Slice1IntStatus"
            
            def description(self):
                return "DE3 Slice1 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1Slice8IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "DE1Slice8IntStatus"
            
            def description(self):
                return "DE1 Slice8 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1Slice7IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DE1Slice7IntStatus"
            
            def description(self):
                return "DE1 Slice7 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1Slice6IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DE1Slice6IntStatus"
            
            def description(self):
                return "DE1 Slice6 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1Slice5IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DE1Slice5IntStatus"
            
            def description(self):
                return "DE1 Slice5 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1Slice4IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DE1Slice4IntStatus"
            
            def description(self):
                return "DE1 Slice4 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1Slice3IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DE1Slice3IntStatus"
            
            def description(self):
                return "DE1 Slice3 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1Slice2IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DE1Slice2IntStatus"
            
            def description(self):
                return "DE1 Slice2 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1Slice1IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE1Slice1IntStatus"
            
            def description(self):
                return "DE1 Slice1 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE3Slice8IntStatus"] = _AF6NC0022_RD_INTR._global_pdh_interrupt_status._DE3Slice8IntStatus()
            allFields["DE3Slice7IntStatus"] = _AF6NC0022_RD_INTR._global_pdh_interrupt_status._DE3Slice7IntStatus()
            allFields["DE3Slice6IntStatus"] = _AF6NC0022_RD_INTR._global_pdh_interrupt_status._DE3Slice6IntStatus()
            allFields["DE3Slice5IntStatus"] = _AF6NC0022_RD_INTR._global_pdh_interrupt_status._DE3Slice5IntStatus()
            allFields["DE3Slice4IntStatus"] = _AF6NC0022_RD_INTR._global_pdh_interrupt_status._DE3Slice4IntStatus()
            allFields["DE3Slice3IntStatus"] = _AF6NC0022_RD_INTR._global_pdh_interrupt_status._DE3Slice3IntStatus()
            allFields["DE3Slice2IntStatus"] = _AF6NC0022_RD_INTR._global_pdh_interrupt_status._DE3Slice2IntStatus()
            allFields["DE3Slice1IntStatus"] = _AF6NC0022_RD_INTR._global_pdh_interrupt_status._DE3Slice1IntStatus()
            allFields["DE1Slice8IntStatus"] = _AF6NC0022_RD_INTR._global_pdh_interrupt_status._DE1Slice8IntStatus()
            allFields["DE1Slice7IntStatus"] = _AF6NC0022_RD_INTR._global_pdh_interrupt_status._DE1Slice7IntStatus()
            allFields["DE1Slice6IntStatus"] = _AF6NC0022_RD_INTR._global_pdh_interrupt_status._DE1Slice6IntStatus()
            allFields["DE1Slice5IntStatus"] = _AF6NC0022_RD_INTR._global_pdh_interrupt_status._DE1Slice5IntStatus()
            allFields["DE1Slice4IntStatus"] = _AF6NC0022_RD_INTR._global_pdh_interrupt_status._DE1Slice4IntStatus()
            allFields["DE1Slice3IntStatus"] = _AF6NC0022_RD_INTR._global_pdh_interrupt_status._DE1Slice3IntStatus()
            allFields["DE1Slice2IntStatus"] = _AF6NC0022_RD_INTR._global_pdh_interrupt_status._DE1Slice2IntStatus()
            allFields["DE1Slice1IntStatus"] = _AF6NC0022_RD_INTR._global_pdh_interrupt_status._DE1Slice1IntStatus()
            return allFields

    class _global_interrupt_pin_disable(AtRegister.AtRegister):
        def name(self):
            return "Global Interrupt Pin Disable"
    
        def description(self):
            return "This register configures global interrupt pin disable"
            
        def width(self):
            return 1
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000005
            
        def endAddress(self):
            return 0xffffffff

        class _GlbPinIntDisable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GlbPinIntDisable"
            
            def description(self):
                return "Global Interrupt Pin Disable  1: Disable  0: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GlbPinIntDisable"] = _AF6NC0022_RD_INTR._global_interrupt_pin_disable._GlbPinIntDisable()
            return allFields

    class _global_interrupt_mask_restore(AtRegister.AtRegister):
        def name(self):
            return "Global Interrupt Mask Restore"
    
        def description(self):
            return "This register configures global interrupt mask enable."
            
        def width(self):
            return 13
        
        def type(self):
            return "Config|Enable"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000006
            
        def endAddress(self):
            return 0xffffffff

        class _UpsrIntRestore(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "UpsrIntRestore"
            
            def description(self):
                return "UPSR Interrupt Restore  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ReserveIntRestore(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "ReserveIntRestore"
            
            def description(self):
                return "HoCDR Interrupt Restore  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CdrIntRestore(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "CdrIntRestore"
            
            def description(self):
                return "CDR Interrupt Restore  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MdlIntRestore(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "MdlIntRestore"
            
            def description(self):
                return "MDL Interrupt Restore  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PrmIntRestore(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PrmIntRestore"
            
            def description(self):
                return "PRM Interrupt Restore  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PmIntRestore(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PmIntRestore"
            
            def description(self):
                return "PM Interrupt Restore  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FmIntRestore(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FmIntRestore"
            
            def description(self):
                return "FM Interrupt Restore  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TFI5IntRestore(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "TFI5IntRestore"
            
            def description(self):
                return "TFI5 Interrupt Restore  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STSIntRestore(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "STSIntRestore"
            
            def description(self):
                return "STS Interrupt Restore  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VTIntRestore(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "VTIntRestore"
            
            def description(self):
                return "VT Interrupt Restore  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3IntRestore(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DE3IntRestore"
            
            def description(self):
                return "DS3/E3 Interrupt Restore  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1IntRestore(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DE1IntRestore"
            
            def description(self):
                return "DS1/E1 Interrupt Restore  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWEIntRestore(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PWEIntRestore"
            
            def description(self):
                return "PWE Interrupt Restore  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["UpsrIntRestore"] = _AF6NC0022_RD_INTR._global_interrupt_mask_restore._UpsrIntRestore()
            allFields["ReserveIntRestore"] = _AF6NC0022_RD_INTR._global_interrupt_mask_restore._ReserveIntRestore()
            allFields["CdrIntRestore"] = _AF6NC0022_RD_INTR._global_interrupt_mask_restore._CdrIntRestore()
            allFields["MdlIntRestore"] = _AF6NC0022_RD_INTR._global_interrupt_mask_restore._MdlIntRestore()
            allFields["PrmIntRestore"] = _AF6NC0022_RD_INTR._global_interrupt_mask_restore._PrmIntRestore()
            allFields["PmIntRestore"] = _AF6NC0022_RD_INTR._global_interrupt_mask_restore._PmIntRestore()
            allFields["FmIntRestore"] = _AF6NC0022_RD_INTR._global_interrupt_mask_restore._FmIntRestore()
            allFields["TFI5IntRestore"] = _AF6NC0022_RD_INTR._global_interrupt_mask_restore._TFI5IntRestore()
            allFields["STSIntRestore"] = _AF6NC0022_RD_INTR._global_interrupt_mask_restore._STSIntRestore()
            allFields["VTIntRestore"] = _AF6NC0022_RD_INTR._global_interrupt_mask_restore._VTIntRestore()
            allFields["DE3IntRestore"] = _AF6NC0022_RD_INTR._global_interrupt_mask_restore._DE3IntRestore()
            allFields["DE1IntRestore"] = _AF6NC0022_RD_INTR._global_interrupt_mask_restore._DE1IntRestore()
            allFields["PWEIntRestore"] = _AF6NC0022_RD_INTR._global_interrupt_mask_restore._PWEIntRestore()
            return allFields

    class _global_parity_interrupt_mask_enable(AtRegister.AtRegister):
        def name(self):
            return "Global  Configuration RAM Parity Interrupt Mask Enable"
    
        def description(self):
            return "This register configures parity global interrupt mask enable."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Enable"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000007
            
        def endAddress(self):
            return 0xffffffff

        class _PrmParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "PrmParIntEnable"
            
            def description(self):
                return "PRM Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PmcParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "PmcParIntEnable"
            
            def description(self):
                return "PMC Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ClaParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "ClaParIntEnable"
            
            def description(self):
                return "CLA Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PweParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "PweParIntEnable"
            
            def description(self):
                return "PWE ETH Port0 Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PdaParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "PdaParIntEnable"
            
            def description(self):
                return "PDA Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "PlaParIntEnable"
            
            def description(self):
                return "PLA Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr8ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "Cdr8ParIntEnable"
            
            def description(self):
                return "CDR Slice6 Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Map8ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "Map8ParIntEnable"
            
            def description(self):
                return "MAP Slice6 Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Pdh8ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "Pdh8ParIntEnable"
            
            def description(self):
                return "PDH Slice6 Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr7ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "Cdr7ParIntEnable"
            
            def description(self):
                return "CDR Slice6 Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Map7ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Map7ParIntEnable"
            
            def description(self):
                return "MAP Slice6 Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Pdh7ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Pdh7ParIntEnable"
            
            def description(self):
                return "PDH Slice6 Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr6ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "Cdr6ParIntEnable"
            
            def description(self):
                return "CDR Slice6 Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Map6ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "Map6ParIntEnable"
            
            def description(self):
                return "MAP Slice6 Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Pdh6ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "Pdh6ParIntEnable"
            
            def description(self):
                return "PDH Slice6 Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr5ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Cdr5ParIntEnable"
            
            def description(self):
                return "CDR Slice5 Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Map5ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "Map5ParIntEnable"
            
            def description(self):
                return "MAP Slice5 Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Pdh5ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Pdh5ParIntEnable"
            
            def description(self):
                return "PDH Slice5 Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr4ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "Cdr4ParIntEnable"
            
            def description(self):
                return "CDR Slice4 Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Map4ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Map4ParIntEnable"
            
            def description(self):
                return "MAP Slice4 Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Pdh4ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "Pdh4ParIntEnable"
            
            def description(self):
                return "PDH Slice4 Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr3ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Cdr3ParIntEnable"
            
            def description(self):
                return "CDR Slice3 Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Map3ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "Map3ParIntEnable"
            
            def description(self):
                return "MAP Slice3 Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Pdh3ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Pdh3ParIntEnable"
            
            def description(self):
                return "PDH Slice3 Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr2ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "Cdr2ParIntEnable"
            
            def description(self):
                return "CDR Slice2 Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Map2ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Map2ParIntEnable"
            
            def description(self):
                return "MAP Slice2 Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Pdh2ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Pdh2ParIntEnable"
            
            def description(self):
                return "PDH Slice2 Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr1ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Cdr1ParIntEnable"
            
            def description(self):
                return "CDR Slice1 Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Map1ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Map1ParIntEnable"
            
            def description(self):
                return "MAP Slice1 Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Pdh1ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Pdh1ParIntEnable"
            
            def description(self):
                return "PDH Slice1 Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PohParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PohParIntEnable"
            
            def description(self):
                return "POH Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OcnParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OcnParIntEnable"
            
            def description(self):
                return "OCN Parity Interrupt Enable  1: Set  0: Clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PrmParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._PrmParIntEnable()
            allFields["PmcParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._PmcParIntEnable()
            allFields["ClaParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._ClaParIntEnable()
            allFields["PweParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._PweParIntEnable()
            allFields["PdaParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._PdaParIntEnable()
            allFields["PlaParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._PlaParIntEnable()
            allFields["Cdr8ParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._Cdr8ParIntEnable()
            allFields["Map8ParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._Map8ParIntEnable()
            allFields["Pdh8ParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._Pdh8ParIntEnable()
            allFields["Cdr7ParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._Cdr7ParIntEnable()
            allFields["Map7ParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._Map7ParIntEnable()
            allFields["Pdh7ParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._Pdh7ParIntEnable()
            allFields["Cdr6ParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._Cdr6ParIntEnable()
            allFields["Map6ParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._Map6ParIntEnable()
            allFields["Pdh6ParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._Pdh6ParIntEnable()
            allFields["Cdr5ParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._Cdr5ParIntEnable()
            allFields["Map5ParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._Map5ParIntEnable()
            allFields["Pdh5ParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._Pdh5ParIntEnable()
            allFields["Cdr4ParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._Cdr4ParIntEnable()
            allFields["Map4ParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._Map4ParIntEnable()
            allFields["Pdh4ParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._Pdh4ParIntEnable()
            allFields["Cdr3ParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._Cdr3ParIntEnable()
            allFields["Map3ParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._Map3ParIntEnable()
            allFields["Pdh3ParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._Pdh3ParIntEnable()
            allFields["Cdr2ParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._Cdr2ParIntEnable()
            allFields["Map2ParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._Map2ParIntEnable()
            allFields["Pdh2ParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._Pdh2ParIntEnable()
            allFields["Cdr1ParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._Cdr1ParIntEnable()
            allFields["Map1ParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._Map1ParIntEnable()
            allFields["Pdh1ParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._Pdh1ParIntEnable()
            allFields["PohParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._PohParIntEnable()
            allFields["OcnParIntEnable"] = _AF6NC0022_RD_INTR._global_parity_interrupt_mask_enable._OcnParIntEnable()
            return allFields

    class _global_parity_interrupt_status(AtRegister.AtRegister):
        def name(self):
            return "Global  Configuration RAM Parity Interrupt Status"
    
        def description(self):
            return "This register show global parity interrupt status."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000008
            
        def endAddress(self):
            return 0xffffffff

        class _PrmParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "PrmParIntStatus"
            
            def description(self):
                return "PRM Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _PmcParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "PmcParIntStatus"
            
            def description(self):
                return "PMC Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _ClaParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "ClaParIntStatus"
            
            def description(self):
                return "CLA Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _PweParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "PweParIntStatus"
            
            def description(self):
                return "PWE ETH Port0 Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _PdaParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "PdaParIntStatus"
            
            def description(self):
                return "PDA Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "PlaParIntStatus"
            
            def description(self):
                return "PLA Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr8ParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "Cdr8ParIntStatus"
            
            def description(self):
                return "CDR Slice6 Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Map8ParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "Map8ParIntStatus"
            
            def description(self):
                return "MAP Slice6 Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Pdh8ParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "Pdh8ParIntStatus"
            
            def description(self):
                return "PDH Slice6 Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr7ParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "Cdr7ParIntStatus"
            
            def description(self):
                return "CDR Slice6 Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Map7ParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Map7ParIntStatus"
            
            def description(self):
                return "MAP Slice6 Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Pdh7ParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Pdh7ParIntStatus"
            
            def description(self):
                return "PDH Slice6 Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr6ParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "Cdr6ParIntStatus"
            
            def description(self):
                return "CDR Slice6 Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Map6ParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "Map6ParIntStatus"
            
            def description(self):
                return "MAP Slice6 Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Pdh6ParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "Pdh6ParIntStatus"
            
            def description(self):
                return "PDH Slice6 Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr5ParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Cdr5ParIntStatus"
            
            def description(self):
                return "CDR Slice5 Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Map5ParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "Map5ParIntStatus"
            
            def description(self):
                return "MAP Slice5 Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Pdh5ParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "Pdh5ParIntStatus"
            
            def description(self):
                return "PDH Slice5 Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr4ParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "Cdr4ParIntStatus"
            
            def description(self):
                return "CDR Slice4 Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Map4ParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Map4ParIntStatus"
            
            def description(self):
                return "MAP Slice4 Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Pdh4ParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "Pdh4ParIntStatus"
            
            def description(self):
                return "PDH Slice4 Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr3ParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Cdr3ParIntStatus"
            
            def description(self):
                return "CDR Slice3 Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Map3ParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "Map3ParIntStatus"
            
            def description(self):
                return "MAP Slice3 Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Pdh3ParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Pdh3ParIntStatus"
            
            def description(self):
                return "PDH Slice3 Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr2ParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "Cdr2ParIntStatus"
            
            def description(self):
                return "CDR Slice2 Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Map2ParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Map2ParIntStatus"
            
            def description(self):
                return "MAP Slice2 Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Pdh2ParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Pdh2ParIntStatus"
            
            def description(self):
                return "PDH Slice2 Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr1ParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Cdr1ParIntStatus"
            
            def description(self):
                return "CDR Slice1 Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Map1ParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Map1ParIntStatus"
            
            def description(self):
                return "MAP Slice1 Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Pdh1ParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Pdh1ParIntStatus"
            
            def description(self):
                return "PDH Slice1 Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _PohParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PohParIntStatus"
            
            def description(self):
                return "POH Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _OcnParIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OcnParIntStatus"
            
            def description(self):
                return "OCN Parity Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PrmParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._PrmParIntStatus()
            allFields["PmcParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._PmcParIntStatus()
            allFields["ClaParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._ClaParIntStatus()
            allFields["PweParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._PweParIntStatus()
            allFields["PdaParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._PdaParIntStatus()
            allFields["PlaParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._PlaParIntStatus()
            allFields["Cdr8ParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._Cdr8ParIntStatus()
            allFields["Map8ParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._Map8ParIntStatus()
            allFields["Pdh8ParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._Pdh8ParIntStatus()
            allFields["Cdr7ParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._Cdr7ParIntStatus()
            allFields["Map7ParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._Map7ParIntStatus()
            allFields["Pdh7ParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._Pdh7ParIntStatus()
            allFields["Cdr6ParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._Cdr6ParIntStatus()
            allFields["Map6ParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._Map6ParIntStatus()
            allFields["Pdh6ParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._Pdh6ParIntStatus()
            allFields["Cdr5ParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._Cdr5ParIntStatus()
            allFields["Map5ParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._Map5ParIntStatus()
            allFields["Pdh5ParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._Pdh5ParIntStatus()
            allFields["Cdr4ParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._Cdr4ParIntStatus()
            allFields["Map4ParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._Map4ParIntStatus()
            allFields["Pdh4ParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._Pdh4ParIntStatus()
            allFields["Cdr3ParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._Cdr3ParIntStatus()
            allFields["Map3ParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._Map3ParIntStatus()
            allFields["Pdh3ParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._Pdh3ParIntStatus()
            allFields["Cdr2ParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._Cdr2ParIntStatus()
            allFields["Map2ParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._Map2ParIntStatus()
            allFields["Pdh2ParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._Pdh2ParIntStatus()
            allFields["Cdr1ParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._Cdr1ParIntStatus()
            allFields["Map1ParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._Map1ParIntStatus()
            allFields["Pdh1ParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._Pdh1ParIntStatus()
            allFields["PohParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._PohParIntStatus()
            allFields["OcnParIntStatus"] = _AF6NC0022_RD_INTR._global_parity_interrupt_status._OcnParIntStatus()
            return allFields

    class _global_locdr_interrupt_status(AtRegister.AtRegister):
        def name(self):
            return "Global LoCDR Interrupt Status"
    
        def description(self):
            return "This register indicate global low order CDR interrupt status."
            
        def width(self):
            return 8
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000009
            
        def endAddress(self):
            return 0xffffffff

        class _LoCDRSlice8IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "LoCDRSlice8IntStatus"
            
            def description(self):
                return "LoCDR Slice8 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _LoCDRSlice7IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "LoCDRSlice7IntStatus"
            
            def description(self):
                return "LoCDR Slice7 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _LoCDRSlice6IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "LoCDRSlice6IntStatus"
            
            def description(self):
                return "LoCDR Slice6 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _LoCDRSlice5IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "LoCDRSlice5IntStatus"
            
            def description(self):
                return "LoCDR Slice5 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _LoCDRSlice4IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "LoCDRSlice4IntStatus"
            
            def description(self):
                return "LoCDR Slice4 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _LoCDRSlice3IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "LoCDRSlice3IntStatus"
            
            def description(self):
                return "LoCDR Slice3 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _LoCDRSlice2IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "LoCDRSlice2IntStatus"
            
            def description(self):
                return "LoCDR Slice2 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _LoCDRSlice1IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LoCDRSlice1IntStatus"
            
            def description(self):
                return "LoCDR Slice1 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LoCDRSlice8IntStatus"] = _AF6NC0022_RD_INTR._global_locdr_interrupt_status._LoCDRSlice8IntStatus()
            allFields["LoCDRSlice7IntStatus"] = _AF6NC0022_RD_INTR._global_locdr_interrupt_status._LoCDRSlice7IntStatus()
            allFields["LoCDRSlice6IntStatus"] = _AF6NC0022_RD_INTR._global_locdr_interrupt_status._LoCDRSlice6IntStatus()
            allFields["LoCDRSlice5IntStatus"] = _AF6NC0022_RD_INTR._global_locdr_interrupt_status._LoCDRSlice5IntStatus()
            allFields["LoCDRSlice4IntStatus"] = _AF6NC0022_RD_INTR._global_locdr_interrupt_status._LoCDRSlice4IntStatus()
            allFields["LoCDRSlice3IntStatus"] = _AF6NC0022_RD_INTR._global_locdr_interrupt_status._LoCDRSlice3IntStatus()
            allFields["LoCDRSlice2IntStatus"] = _AF6NC0022_RD_INTR._global_locdr_interrupt_status._LoCDRSlice2IntStatus()
            allFields["LoCDRSlice1IntStatus"] = _AF6NC0022_RD_INTR._global_locdr_interrupt_status._LoCDRSlice1IntStatus()
            return allFields

    class _global_pmc_2page_interrupt_status(AtRegister.AtRegister):
        def name(self):
            return "Global PMC 2page Interrupt Status"
    
        def description(self):
            return "This register indicate global low order CDR interrupt status."
            
        def width(self):
            return 7
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000a
            
        def endAddress(self):
            return 0xffffffff

        class _PmrIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PmrIntStatus"
            
            def description(self):
                return "Pmr     Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _PohpmvtIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PohpmvtIntStatus"
            
            def description(self):
                return "Pohpmvt Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _PohpmstsIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PohpmstsIntStatus"
            
            def description(self):
                return "Pohpmsts Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _PohIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PohIntStatus"
            
            def description(self):
                return "Poh    Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Pdhde1IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Pdhde1IntStatus"
            
            def description(self):
                return "Pdhde1 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Pdhde3IntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Pdhde3IntStatus"
            
            def description(self):
                return "Pdhde3 Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _PwcntIntStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PwcntIntStatus"
            
            def description(self):
                return "Pwcnt  Interrupt Status  1: Set  0: Clear"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PmrIntStatus"] = _AF6NC0022_RD_INTR._global_pmc_2page_interrupt_status._PmrIntStatus()
            allFields["PohpmvtIntStatus"] = _AF6NC0022_RD_INTR._global_pmc_2page_interrupt_status._PohpmvtIntStatus()
            allFields["PohpmstsIntStatus"] = _AF6NC0022_RD_INTR._global_pmc_2page_interrupt_status._PohpmstsIntStatus()
            allFields["PohIntStatus"] = _AF6NC0022_RD_INTR._global_pmc_2page_interrupt_status._PohIntStatus()
            allFields["Pdhde1IntStatus"] = _AF6NC0022_RD_INTR._global_pmc_2page_interrupt_status._Pdhde1IntStatus()
            allFields["Pdhde3IntStatus"] = _AF6NC0022_RD_INTR._global_pmc_2page_interrupt_status._Pdhde3IntStatus()
            allFields["PwcntIntStatus"] = _AF6NC0022_RD_INTR._global_pmc_2page_interrupt_status._PwcntIntStatus()
            return allFields

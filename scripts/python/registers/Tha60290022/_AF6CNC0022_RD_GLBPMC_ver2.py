import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0022_RD_GLBPMC_ver2(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_txpwcnt_info1_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_txpwcnt_info1_rw()
        allRegisters["upen_txpwcnt_info1_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_txpwcnt_info1_rw()
        allRegisters["upen_txpwcnt_info0"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_txpwcnt_info0()
        allRegisters["upen_rxpwcnt_info0_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_rxpwcnt_info0_rw()
        allRegisters["upen_rxpwcnt_info0_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_rxpwcnt_info0_rw()
        allRegisters["upen_rxpwcnt_info1_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_rxpwcnt_info1_rw()
        allRegisters["upen_rxpwcnt_info1_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_rxpwcnt_info1_rw()
        allRegisters["upen_rxpwcnt_info2_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_rxpwcnt_info2_rw()
        allRegisters["upen_rxpwcnt_info2_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_rxpwcnt_info2_rw()
        allRegisters["upen_poh_pmr_cnt0_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_poh_pmr_cnt0_rw()
        allRegisters["upen_poh_pmr_cnt0_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_poh_pmr_cnt0_rw()
        allRegisters["upen_sts_pohpm_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_sts_pohpm_rw()
        allRegisters["upen_sts_pohpm_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_sts_pohpm_rw()
        allRegisters["upen_vt_pohpm_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_vt_pohpm_rw()
        allRegisters["upen_vt_pohpm_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_vt_pohpm_rw()
        allRegisters["upen_poh_vt_cnt0_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_poh_vt_cnt0_rw()
        allRegisters["upen_poh_vt_cnt0_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_poh_vt_cnt0_rw()
        allRegisters["upen_poh_vt_cnt1_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_poh_vt_cnt1_rw()
        allRegisters["upen_poh_vt_cnt1_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_poh_vt_cnt1_rw()
        allRegisters["upen_poh_sts_cnt0_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_poh_sts_cnt0_rw()
        allRegisters["upen_poh_sts_cnt0_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_poh_sts_cnt0_rw()
        allRegisters["upen_poh_sts_cnt1_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_poh_sts_cnt1_rw()
        allRegisters["upen_poh_sts_cnt1_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_poh_sts_cnt1_rw()
        allRegisters["upen_pdh_de1cntval30_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_pdh_de1cntval30_rw()
        allRegisters["upen_pdh_de1cntval30_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_pdh_de1cntval30_rw()
        allRegisters["upen_pdh_de3cnt0_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_pdh_de3cnt0_rw()
        allRegisters["upen_pdh_de3cnt0_rw"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_pdh_de3cnt0_rw()
        return allRegisters

    class _upen_txpwcnt_info1_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_0000 + 10752*$offset + $pwid"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x00007dff

        class _txpwcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txpwcnt"
            
            def description(self):
                return "in case of txpwcnt0 side: + offset = 0 : txrbit + offset = 1 : txnbit + offset = 2 : txpkt in case of txpwcnt1 side: + offset = 0 : txpbit + offset = 1 : txlbit + offset = 2 : unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txpwcnt"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_txpwcnt_info1_rw._txpwcnt()
            return allFields

    class _upen_txpwcnt_info1_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_8000 + 10752*$offset + $pwid"
            
        def startAddress(self):
            return 0x00008000
            
        def endAddress(self):
            return 0x0000fdff

        class _txpwcnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txpwcnt"
            
            def description(self):
                return "in case of txpwcnt0 side: + offset = 0 : txrbit + offset = 1 : txnbit + offset = 2 : txpkt in case of txpwcnt1 side: + offset = 0 : txpbit + offset = 1 : txlbit + offset = 2 : unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txpwcnt"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_txpwcnt_info1_rw._txpwcnt()
            return allFields

    class _upen_txpwcnt_info0(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire TX Byte Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_0000 + $pwid"
            
        def startAddress(self):
            return 0x00080000
            
        def endAddress(self):
            return 0x000829ff

        class _pwcntbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pwcntbyte"
            
            def description(self):
                return "txpwcntbyte"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pwcntbyte"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_txpwcnt_info0._pwcntbyte()
            return allFields

    class _upen_rxpwcnt_info0_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info0"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1_0000 + $pwid"
            
        def startAddress(self):
            return 0x00010000
            
        def endAddress(self):
            return 0x000129ff

        class _rxpwcnt_info0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info0"
            
            def description(self):
                return "in case of rxpwcnt_info0_cnt0 side: + rxcntbyte in case of rxpwcnt_info0_cnt1 side: + rxpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info0"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_rxpwcnt_info0_rw._rxpwcnt_info0()
            return allFields

    class _upen_rxpwcnt_info0_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info0"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1_4000 + $pwid"
            
        def startAddress(self):
            return 0x00014000
            
        def endAddress(self):
            return 0x000169ff

        class _rxpwcnt_info0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info0"
            
            def description(self):
                return "in case of rxpwcnt_info0_cnt0 side: + rxcntbyte in case of rxpwcnt_info0_cnt1 side: + rxpkt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info0"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_rxpwcnt_info0_rw._rxpwcnt_info0()
            return allFields

    class _upen_rxpwcnt_info1_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x2_0000 + 10752*$offset + $pwid"
            
        def startAddress(self):
            return 0x00020000
            
        def endAddress(self):
            return 0x00027dff

        class _rxpwcnt_info0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info0"
            
            def description(self):
                return "in case of rxpwcnt_info0_cnt0 side: + offset = 0 : rxrbit + offset = 1 : rxnbit + offset = 2 : rxstray in case of rxpwcnt_info0_cnt1 side: + offset = 0 : rxpbit + offset = 1 : rxlbit + offset = 2 : rxmalform"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info0"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_rxpwcnt_info1_rw._rxpwcnt_info0()
            return allFields

    class _upen_rxpwcnt_info1_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x2_8000 + 10752*$offset + $pwid"
            
        def startAddress(self):
            return 0x00028000
            
        def endAddress(self):
            return 0x0002fdff

        class _rxpwcnt_info0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info0"
            
            def description(self):
                return "in case of rxpwcnt_info0_cnt0 side: + offset = 0 : rxrbit + offset = 1 : rxnbit + offset = 2 : rxstray in case of rxpwcnt_info0_cnt1 side: + offset = 0 : rxpbit + offset = 1 : rxlbit + offset = 2 : rxmalform"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info0"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_rxpwcnt_info1_rw._rxpwcnt_info0()
            return allFields

    class _upen_rxpwcnt_info2_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info2"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_0000 + 10752*$offset + $pwid"
            
        def startAddress(self):
            return 0x00030000
            
        def endAddress(self):
            return 0x00037dff

        class _rxpwcnt_info1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info1"
            
            def description(self):
                return "in case of rxpwcnt_info1_cnt0 side: + offset = 0 : rxunderrun + offset = 1 : rxlops + offset = 2 : rxearly in case of rxpwcnt_info1_cnt1 side: + offset = 0 : rxoverrun + offset = 1 : rxlost + offset = 2 : rxlate"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info1"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_rxpwcnt_info2_rw._rxpwcnt_info1()
            return allFields

    class _upen_rxpwcnt_info2_rw(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receiver Counter Info2"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_8000 + 10752*$offset + $pwid"
            
        def startAddress(self):
            return 0x00038000
            
        def endAddress(self):
            return 0x0003fdff

        class _rxpwcnt_info1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt_info1"
            
            def description(self):
                return "in case of rxpwcnt_info1_cnt0 side: + offset = 0 : rxunderrun + offset = 1 : rxlops + offset = 2 : rxearly in case of rxpwcnt_info1_cnt1 side: + offset = 0 : rxoverrun + offset = 1 : rxlost + offset = 2 : rxlate"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt_info1"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_rxpwcnt_info2_rw._rxpwcnt_info1()
            return allFields

    class _upen_poh_pmr_cnt0_rw(AtRegister.AtRegister):
        def name(self):
            return "PMR Error Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_1800 + 16*$offset + $lineid"
            
        def startAddress(self):
            return 0x00091800
            
        def endAddress(self):
            return 0x0009182f

        class _pmr_err_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pmr_err_cnt"
            
            def description(self):
                return "in case of pmr_err_cnt0 side: + offset = 0 : rei_l + offset = 1 : b2 + offset = 2 : b1 in case of pmr_err_cnt1 side: + offset = 0 : rei_l_block_err + offset = 1 : b2_block_err + offset = 2 : b1_block_err"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pmr_err_cnt"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_poh_pmr_cnt0_rw._pmr_err_cnt()
            return allFields

    class _upen_poh_pmr_cnt0_rw(AtRegister.AtRegister):
        def name(self):
            return "PMR Error Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_1840 + 16*$offset + $lineid"
            
        def startAddress(self):
            return 0x00091840
            
        def endAddress(self):
            return 0x0009186f

        class _pmr_err_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pmr_err_cnt"
            
            def description(self):
                return "in case of pmr_err_cnt0 side: + offset = 0 : rei_l + offset = 1 : b2 + offset = 2 : b1 in case of pmr_err_cnt1 side: + offset = 0 : rei_l_block_err + offset = 1 : b2_block_err + offset = 2 : b1_block_err"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pmr_err_cnt"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_poh_pmr_cnt0_rw._pmr_err_cnt()
            return allFields

    class _upen_sts_pohpm_rw(AtRegister.AtRegister):
        def name(self):
            return "POH Path STS Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_2000 + 8*$stsid + $slcid"
            
        def startAddress(self):
            return 0x00092000
            
        def endAddress(self):
            return 0x000921ff

        class _pohpath_vt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pohpath_vt_cnt"
            
            def description(self):
                return "in case of pohpath_cnt0 side: + sts_rei in case of pohpath_cnt1 side: + sts_bip(B3)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pohpath_vt_cnt"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_sts_pohpm_rw._pohpath_vt_cnt()
            return allFields

    class _upen_sts_pohpm_rw(AtRegister.AtRegister):
        def name(self):
            return "POH Path STS Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_2200 + 8*$stsid + $slcid"
            
        def startAddress(self):
            return 0x00092200
            
        def endAddress(self):
            return 0x000923ff

        class _pohpath_vt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pohpath_vt_cnt"
            
            def description(self):
                return "in case of pohpath_cnt0 side: + sts_rei in case of pohpath_cnt1 side: + sts_bip(B3)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pohpath_vt_cnt"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_sts_pohpm_rw._pohpath_vt_cnt()
            return allFields

    class _upen_vt_pohpm_rw(AtRegister.AtRegister):
        def name(self):
            return "POH Path VT Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x7_0000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid"
            
        def startAddress(self):
            return 0x00070000
            
        def endAddress(self):
            return 0x00073fff

        class _pohpath_vt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pohpath_vt_cnt"
            
            def description(self):
                return "in case of pohpath_cnt0 side: + vt_rei in case of pohpath_cnt1 side: + vt_bip"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pohpath_vt_cnt"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_vt_pohpm_rw._pohpath_vt_cnt()
            return allFields

    class _upen_vt_pohpm_rw(AtRegister.AtRegister):
        def name(self):
            return "POH Path VT Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x7_4000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid"
            
        def startAddress(self):
            return 0x00074000
            
        def endAddress(self):
            return 0x00077fff

        class _pohpath_vt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pohpath_vt_cnt"
            
            def description(self):
                return "in case of pohpath_cnt0 side: + vt_rei in case of pohpath_cnt1 side: + vt_bip"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pohpath_vt_cnt"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_vt_pohpm_rw._pohpath_vt_cnt()
            return allFields

    class _upen_poh_vt_cnt0_rw(AtRegister.AtRegister):
        def name(self):
            return "POH vt pointer Counter 0"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x5_0000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid"
            
        def startAddress(self):
            return 0x00050000
            
        def endAddress(self):
            return 0x00053fff

        class _poh_vt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_vt_cnt"
            
            def description(self):
                return "in case of poh_vt_cnt0 side: + pohtx_vt_dec in case of poh_vt_cnt1 side: + pohtx_vt_inc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_vt_cnt"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_poh_vt_cnt0_rw._poh_vt_cnt()
            return allFields

    class _upen_poh_vt_cnt0_rw(AtRegister.AtRegister):
        def name(self):
            return "POH vt pointer Counter 0"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x5_4000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid"
            
        def startAddress(self):
            return 0x00054000
            
        def endAddress(self):
            return 0x00057fff

        class _poh_vt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_vt_cnt"
            
            def description(self):
                return "in case of poh_vt_cnt0 side: + pohtx_vt_dec in case of poh_vt_cnt1 side: + pohtx_vt_inc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_vt_cnt"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_poh_vt_cnt0_rw._poh_vt_cnt()
            return allFields

    class _upen_poh_vt_cnt1_rw(AtRegister.AtRegister):
        def name(self):
            return "POH vt pointer Counter 1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x6_0000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid"
            
        def startAddress(self):
            return 0x00060000
            
        def endAddress(self):
            return 0x00063fff

        class _poh_vt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_vt_cnt"
            
            def description(self):
                return "in case of poh_vt_cnt0 side: + pohrx_vt_dec in case of poh_vt_cnt1 side: + pohrx_vt_inc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_vt_cnt"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_poh_vt_cnt1_rw._poh_vt_cnt()
            return allFields

    class _upen_poh_vt_cnt1_rw(AtRegister.AtRegister):
        def name(self):
            return "POH vt pointer Counter 1"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x6_4000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid"
            
        def startAddress(self):
            return 0x00064000
            
        def endAddress(self):
            return 0x00067fff

        class _poh_vt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_vt_cnt"
            
            def description(self):
                return "in case of poh_vt_cnt0 side: + pohrx_vt_dec in case of poh_vt_cnt1 side: + pohrx_vt_inc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_vt_cnt"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_poh_vt_cnt1_rw._poh_vt_cnt()
            return allFields

    class _upen_poh_sts_cnt0_rw(AtRegister.AtRegister):
        def name(self):
            return "POH sts pointer Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_0800 + 8*$stsid + $slcid"
            
        def startAddress(self):
            return 0x00090800
            
        def endAddress(self):
            return 0x000909ff

        class _poh_sts_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_sts_cnt"
            
            def description(self):
                return "in case of poh_vt_cnt0 side: + pohtx_sts_dec in case of poh_vt_cnt1 side: + pohtx_sts_inc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_sts_cnt"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_poh_sts_cnt0_rw._poh_sts_cnt()
            return allFields

    class _upen_poh_sts_cnt0_rw(AtRegister.AtRegister):
        def name(self):
            return "POH sts pointer Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_0A00 + 8*$stsid + $slcid"
            
        def startAddress(self):
            return 0x00090a00
            
        def endAddress(self):
            return 0x00090bff

        class _poh_sts_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_sts_cnt"
            
            def description(self):
                return "in case of poh_vt_cnt0 side: + pohtx_sts_dec in case of poh_vt_cnt1 side: + pohtx_sts_inc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_sts_cnt"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_poh_sts_cnt0_rw._poh_sts_cnt()
            return allFields

    class _upen_poh_sts_cnt1_rw(AtRegister.AtRegister):
        def name(self):
            return "POH sts pointer Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_1000 + 8*$stsid + $slcid"
            
        def startAddress(self):
            return 0x00091000
            
        def endAddress(self):
            return 0x000911ff

        class _poh_sts_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_sts_cnt"
            
            def description(self):
                return "in case of poh_vt_cnt0 side: + pohrx_sts_dec in case of poh_vt_cnt1 side: + pohrx_sts_inc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_sts_cnt"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_poh_sts_cnt1_rw._poh_sts_cnt()
            return allFields

    class _upen_poh_sts_cnt1_rw(AtRegister.AtRegister):
        def name(self):
            return "POH sts pointer Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_1200 + 8*$stsid + $slcid"
            
        def startAddress(self):
            return 0x00091200
            
        def endAddress(self):
            return 0x000913ff

        class _poh_sts_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_sts_cnt"
            
            def description(self):
                return "in case of poh_vt_cnt0 side: + pohrx_sts_dec in case of poh_vt_cnt1 side: + pohrx_sts_inc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_sts_cnt"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_poh_sts_cnt1_rw._poh_sts_cnt()
            return allFields

    class _upen_pdh_de1cntval30_rw(AtRegister.AtRegister):
        def name(self):
            return "PDH ds1 cntval Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4_0000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid"
            
        def startAddress(self):
            return 0x00040000
            
        def endAddress(self):
            return 0x00043fff

        class _ds1_cntval_bus1_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ds1_cntval_bus1_cnt"
            
            def description(self):
                return "(rei/fe/crc)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ds1_cntval_bus1_cnt"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_pdh_de1cntval30_rw._ds1_cntval_bus1_cnt()
            return allFields

    class _upen_pdh_de1cntval30_rw(AtRegister.AtRegister):
        def name(self):
            return "PDH ds1 cntval Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4_4000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid"
            
        def startAddress(self):
            return 0x00044000
            
        def endAddress(self):
            return 0x00047fff

        class _ds1_cntval_bus1_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ds1_cntval_bus1_cnt"
            
            def description(self):
                return "(rei/fe/crc)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ds1_cntval_bus1_cnt"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_pdh_de1cntval30_rw._ds1_cntval_bus1_cnt()
            return allFields

    class _upen_pdh_de3cnt0_rw(AtRegister.AtRegister):
        def name(self):
            return "PDH ds3 cntval Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_0000 + 8*$stsid + $slcid"
            
        def startAddress(self):
            return 0x00090000
            
        def endAddress(self):
            return 0x000901ff

        class _ds3_cntval_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ds3_cntval_cnt"
            
            def description(self):
                return "(cb/pb/rei/fe)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ds3_cntval_cnt"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_pdh_de3cnt0_rw._ds3_cntval_cnt()
            return allFields

    class _upen_pdh_de3cnt0_rw(AtRegister.AtRegister):
        def name(self):
            return "PDH ds3 cntval Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9_0200 + 8*$stsid + $slcid"
            
        def startAddress(self):
            return 0x00090200
            
        def endAddress(self):
            return 0x000903ff

        class _ds3_cntval_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ds3_cntval_cnt"
            
            def description(self):
                return "(cb/pb/rei/fe)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ds3_cntval_cnt"] = _AF6CNC0022_RD_GLBPMC_ver2._upen_pdh_de3cnt0_rw._ds3_cntval_cnt()
            return allFields

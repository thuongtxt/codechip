import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0022_RD_DCCK(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_dcc_ver"] = _AF6CNC0022_RD_DCCK._upen_dcc_ver()
        allRegisters["dcck_cpu_hold"] = _AF6CNC0022_RD_DCCK._dcck_cpu_hold()
        allRegisters["upen_glbint_stt"] = _AF6CNC0022_RD_DCCK._upen_glbint_stt()
        allRegisters["upen_glb_intenb"] = _AF6CNC0022_RD_DCCK._upen_glb_intenb()
        allRegisters["af6ces10grtldcc_rx__upen_glb_sta_cur_glb"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcc_rx__upen_glb_sta_cur_glb()
        allRegisters["upen_int_rxdcc1_stt"] = _AF6CNC0022_RD_DCCK._upen_int_rxdcc1_stt()
        allRegisters["upen_rxdcc_intenb_r1"] = _AF6CNC0022_RD_DCCK._upen_rxdcc_intenb_r1()
        allRegisters["af6ces10grtldcck_corepi__upen_rxdcc_or_stt"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__upen_rxdcc_or_stt()
        allRegisters["af6ces10grtldcck_corepi__upen_rxdcc_or_enb"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__upen_rxdcc_or_enb()
        allRegisters["af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en()
        allRegisters["af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta()
        allRegisters["af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta()
        allRegisters["af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_or_sta"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_or_sta()
        allRegisters["af6ces10grtldcck_corepi__rtl_hdlc_int_pgrp_or_sta"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int_pgrp_or_sta()
        allRegisters["af6ces10grtldcck_corepi__rtl_hdlc_int_pgrp_or_en"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int_pgrp_or_en()
        allRegisters["upen_int_rxk12_stt"] = _AF6CNC0022_RD_DCCK._upen_int_rxk12_stt()
        allRegisters["upen_rxk12_intenb"] = _AF6CNC0022_RD_DCCK._upen_rxk12_intenb()
        allRegisters["upen_k12rx_cur_err"] = _AF6CNC0022_RD_DCCK._upen_k12rx_cur_err()
        allRegisters["af6ces10grtldcck_corepi__upen_k12rx_or_stt"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__upen_k12rx_or_stt()
        allRegisters["af6ces10grtldcck_corepi__upen_k12rx_or_enb"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__upen_k12rx_or_enb()
        allRegisters["upen_loopen"] = _AF6CNC0022_RD_DCCK._upen_loopen()
        allRegisters["upen_cfg_dump_lid"] = _AF6CNC0022_RD_DCCK._upen_cfg_dump_lid()
        allRegisters["upen_cfg_dcc_en"] = _AF6CNC0022_RD_DCCK._upen_cfg_dcc_en()
        allRegisters["upen_dcctx_fcsrem"] = _AF6CNC0022_RD_DCCK._upen_dcctx_fcsrem()
        allRegisters["upen_dcchdr"] = _AF6CNC0022_RD_DCCK._upen_dcchdr()
        allRegisters["upen_dcctx_enacid"] = _AF6CNC0022_RD_DCCK._upen_dcctx_enacid()
        allRegisters["upen_dccrxhdr"] = _AF6CNC0022_RD_DCCK._upen_dccrxhdr()
        allRegisters["upen_dccrxmacenb"] = _AF6CNC0022_RD_DCCK._upen_dccrxmacenb()
        allRegisters["upen_dccrxcvlenb"] = _AF6CNC0022_RD_DCCK._upen_dccrxcvlenb()
        allRegisters["upen_dccdec"] = _AF6CNC0022_RD_DCCK._upen_dccdec()
        allRegisters["upen_k12pro"] = _AF6CNC0022_RD_DCCK._upen_k12pro()
        allRegisters["upen_cfgenacid"] = _AF6CNC0022_RD_DCCK._upen_cfgenacid()
        allRegisters["upen_cfgtim"] = _AF6CNC0022_RD_DCCK._upen_cfgtim()
        allRegisters["upen_cfgcid"] = _AF6CNC0022_RD_DCCK._upen_cfgcid()
        allRegisters["upen_upen_cfgovwcid"] = _AF6CNC0022_RD_DCCK._upen_upen_cfgovwcid()
        allRegisters["upen_cfg_hdrmda"] = _AF6CNC0022_RD_DCCK._upen_cfg_hdrmda()
        allRegisters["upen_cfg_hdrmsa"] = _AF6CNC0022_RD_DCCK._upen_cfg_hdrmsa()
        allRegisters["upen_cfg_hdrvtl"] = _AF6CNC0022_RD_DCCK._upen_cfg_hdrvtl()
        allRegisters["upen_cid2pid"] = _AF6CNC0022_RD_DCCK._upen_cid2pid()
        allRegisters["upen_cfg_gencid"] = _AF6CNC0022_RD_DCCK._upen_cfg_gencid()
        allRegisters["upen_k12rx_macda"] = _AF6CNC0022_RD_DCCK._upen_k12rx_macda()
        allRegisters["upen_k12rx_macsa"] = _AF6CNC0022_RD_DCCK._upen_k12rx_macsa()
        allRegisters["upen_k12rx_evt"] = _AF6CNC0022_RD_DCCK._upen_k12rx_evt()
        allRegisters["upen_k12rx_chcfg"] = _AF6CNC0022_RD_DCCK._upen_k12rx_chcfg()
        allRegisters["upen_k12rx_chmap"] = _AF6CNC0022_RD_DCCK._upen_k12rx_chmap()
        allRegisters["upen_k12rx_alarm"] = _AF6CNC0022_RD_DCCK._upen_k12rx_alarm()
        allRegisters["upen_k12rx_watdog"] = _AF6CNC0022_RD_DCCK._upen_k12rx_watdog()
        allRegisters["upen_stkerr_pktlen"] = _AF6CNC0022_RD_DCCK._upen_stkerr_pktlen()
        allRegisters["upen_stkerr_crcbuf"] = _AF6CNC0022_RD_DCCK._upen_stkerr_crcbuf()
        allRegisters["upen_stkerr_rx_eth2ocn"] = _AF6CNC0022_RD_DCCK._upen_stkerr_rx_eth2ocn()
        allRegisters["upen_curerr_undsz"] = _AF6CNC0022_RD_DCCK._upen_curerr_undsz()
        allRegisters["upen_curerr_ovrsz"] = _AF6CNC0022_RD_DCCK._upen_curerr_ovrsz()
        allRegisters["upen_curerr_crcbuf"] = _AF6CNC0022_RD_DCCK._upen_curerr_crcbuf()
        allRegisters["upen_curerr_bufept"] = _AF6CNC0022_RD_DCCK._upen_curerr_bufept()
        allRegisters["upen_curerr_glb"] = _AF6CNC0022_RD_DCCK._upen_curerr_glb()
        allRegisters["upen_cur_rxdcc0"] = _AF6CNC0022_RD_DCCK._upen_cur_rxdcc0()
        allRegisters["upen_cur_rxdcc2"] = _AF6CNC0022_RD_DCCK._upen_cur_rxdcc2()
        allRegisters["upen_k12rx_trig_encap"] = _AF6CNC0022_RD_DCCK._upen_k12rx_trig_encap()
        allRegisters["upen_k12rx_cap_reg0"] = _AF6CNC0022_RD_DCCK._upen_k12rx_cap_reg0()
        allRegisters["upen_k12rx_cap_reg1"] = _AF6CNC0022_RD_DCCK._upen_k12rx_cap_reg1()
        allRegisters["upen_k12rx_cap_reg2"] = _AF6CNC0022_RD_DCCK._upen_k12rx_cap_reg2()
        allRegisters["upen_k12_glbcnt_rxeop"] = _AF6CNC0022_RD_DCCK._upen_k12_glbcnt_rxeop()
        allRegisters["upen_k12_glbcnt_sfail"] = _AF6CNC0022_RD_DCCK._upen_k12_glbcnt_sfail()
        allRegisters["upen_k12_glbcnt_sok"] = _AF6CNC0022_RD_DCCK._upen_k12_glbcnt_sok()
        allRegisters["upen_k12_glbcnt_sok_byte"] = _AF6CNC0022_RD_DCCK._upen_k12_glbcnt_sok_byte()
        allRegisters["upen_k12_glbcnt_serr"] = _AF6CNC0022_RD_DCCK._upen_k12_glbcnt_serr()
        allRegisters["upen_k12_sokpkt_pcid"] = _AF6CNC0022_RD_DCCK._upen_k12_sokpkt_pcid()
        allRegisters["upen_byt2k12_pcid"] = _AF6CNC0022_RD_DCCK._upen_byt2k12_pcid()
        allRegisters["upen_glbsop2k12"] = _AF6CNC0022_RD_DCCK._upen_glbsop2k12()
        allRegisters["upen_glbbyt2k12"] = _AF6CNC0022_RD_DCCK._upen_glbbyt2k12()
        allRegisters["upen_k12sop2sgm_pcid"] = _AF6CNC0022_RD_DCCK._upen_k12sop2sgm_pcid()
        allRegisters["upen_k12eop2sgm_pcid"] = _AF6CNC0022_RD_DCCK._upen_k12eop2sgm_pcid()
        allRegisters["upen_k12byt2sgm_pcid"] = _AF6CNC0022_RD_DCCK._upen_k12byt2sgm_pcid()
        allRegisters["upen_dcc_glbcnt_sgmrxeop"] = _AF6CNC0022_RD_DCCK._upen_dcc_glbcnt_sgmrxeop()
        allRegisters["upen_dcc_glbcnt_sgmrxerr"] = _AF6CNC0022_RD_DCCK._upen_dcc_glbcnt_sgmrxerr()
        allRegisters["upen_dcc_glbcnt_sgmrxbyt"] = _AF6CNC0022_RD_DCCK._upen_dcc_glbcnt_sgmrxbyt()
        allRegisters["upen_dcc_glbcnt_sgmrxfail"] = _AF6CNC0022_RD_DCCK._upen_dcc_glbcnt_sgmrxfail()
        allRegisters["upen_dcc_glbcnt_sgmrxpass"] = _AF6CNC0022_RD_DCCK._upen_dcc_glbcnt_sgmrxpass()
        allRegisters["upen_dcc_glbcnt_sgmrxdisc"] = _AF6CNC0022_RD_DCCK._upen_dcc_glbcnt_sgmrxdisc()
        allRegisters["upen_dcc_glbcnt_sgmtxeop"] = _AF6CNC0022_RD_DCCK._upen_dcc_glbcnt_sgmtxeop()
        allRegisters["upen_dcc_glbcnt_sgmtxerr"] = _AF6CNC0022_RD_DCCK._upen_dcc_glbcnt_sgmtxerr()
        allRegisters["upen_dcc_glbcnt_sgmtxbyt"] = _AF6CNC0022_RD_DCCK._upen_dcc_glbcnt_sgmtxbyt()
        allRegisters["upen_dcc_cnt"] = _AF6CNC0022_RD_DCCK._upen_dcc_cnt()
        allRegisters["upen_hdlc_locfg"] = _AF6CNC0022_RD_DCCK._upen_hdlc_locfg()
        allRegisters["af6cci0012_lodec_core__icfg_fcslsb"] = _AF6CNC0022_RD_DCCK._af6cci0012_lodec_core__icfg_fcslsb()
        allRegisters["upen_hdlc_enc_data_lsb_first"] = _AF6CNC0022_RD_DCCK._upen_hdlc_enc_data_lsb_first()
        allRegisters["upen_hdlc_enc_ctrl_reg1"] = _AF6CNC0022_RD_DCCK._upen_hdlc_enc_ctrl_reg1()
        allRegisters["upen_hdlc_enc_ctrl_reg2"] = _AF6CNC0022_RD_DCCK._upen_hdlc_enc_ctrl_reg2()
        allRegisters["upen_hdlc_enc_ctrl_reg3"] = _AF6CNC0022_RD_DCCK._upen_hdlc_enc_ctrl_reg3()
        allRegisters["upen_hdlc_enc_ctrl_reg4"] = _AF6CNC0022_RD_DCCK._upen_hdlc_enc_ctrl_reg4()
        allRegisters["upen_trig_encap"] = _AF6CNC0022_RD_DCCK._upen_trig_encap()
        allRegisters["upen_pktcap"] = _AF6CNC0022_RD_DCCK._upen_pktcap()
        allRegisters["upen_k12acc"] = _AF6CNC0022_RD_DCCK._upen_k12acc()
        allRegisters["upen_genmon_reqint"] = _AF6CNC0022_RD_DCCK._upen_genmon_reqint()
        allRegisters["upen_dcc_cfg_testgen_hdr"] = _AF6CNC0022_RD_DCCK._upen_dcc_cfg_testgen_hdr()
        allRegisters["upen_dcc_cfg_testgen_mod"] = _AF6CNC0022_RD_DCCK._upen_dcc_cfg_testgen_mod()
        allRegisters["upen_dcc_cfg_testgen_enacid"] = _AF6CNC0022_RD_DCCK._upen_dcc_cfg_testgen_enacid()
        allRegisters["upen_dcc_cfg_testgen_glb_gen_mode"] = _AF6CNC0022_RD_DCCK._upen_dcc_cfg_testgen_glb_gen_mode()
        allRegisters["upen_dcc_cfg_testgen_glb_length"] = _AF6CNC0022_RD_DCCK._upen_dcc_cfg_testgen_glb_length()
        allRegisters["upen_dcc_cfg_testgen_glb_gen_interval"] = _AF6CNC0022_RD_DCCK._upen_dcc_cfg_testgen_glb_gen_interval()
        allRegisters["upen_mon_godpkt"] = _AF6CNC0022_RD_DCCK._upen_mon_godpkt()
        allRegisters["upen_mon_errpkt"] = _AF6CNC0022_RD_DCCK._upen_mon_errpkt()
        allRegisters["upen_mon_errvcg"] = _AF6CNC0022_RD_DCCK._upen_mon_errvcg()
        allRegisters["upen_mon_errseq"] = _AF6CNC0022_RD_DCCK._upen_mon_errseq()
        allRegisters["upen_mon_errfcs"] = _AF6CNC0022_RD_DCCK._upen_mon_errfcs()
        allRegisters["upen_mon_abrpkt"] = _AF6CNC0022_RD_DCCK._upen_mon_abrpkt()
        allRegisters["af6ces10grtl_dccpro__upen_txbuf_stk_err"] = _AF6CNC0022_RD_DCCK._af6ces10grtl_dccpro__upen_txbuf_stk_err()
        allRegisters["af6ces10grtl_dccpro__upen_rxbuf_stk_err1"] = _AF6CNC0022_RD_DCCK._af6ces10grtl_dccpro__upen_rxbuf_stk_err1()
        allRegisters["af6ces10grtl_dccpro__upen_cfg_maxlen"] = _AF6CNC0022_RD_DCCK._af6ces10grtl_dccpro__upen_cfg_maxlen()
        allRegisters["af6ces10grtldcc_rx__upen_dispkt_pcid"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcc_rx__upen_dispkt_pcid()
        allRegisters["af6ces10grtldcc_rx__upen_en_benchmark"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcc_rx__upen_en_benchmark()
        allRegisters["af6ces10grtldcc_rx__upen_cfg_numtik"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcc_rx__upen_cfg_numtik()
        allRegisters["af6ces10grtldcc_rx__upen_bytpsec_cnt"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcc_rx__upen_bytpsec_cnt()
        return allRegisters

    class _upen_dcc_ver(AtRegister.AtRegister):
        def name(self):
            return "DCC Version Reg"
    
        def description(self):
            return "The register provides DCC's code version"
            
        def width(self):
            return 32
        
        def type(self):
            return ""
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _DCC_VER(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DCC_VER"
            
            def description(self):
                return "DAY_MON_YEAR_HOUR [31:24]: DAY [23:16]: MONTH [15:08]: YEAR [07:00]: HOUR"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DCC_VER"] = _AF6CNC0022_RD_DCCK._upen_dcc_ver._DCC_VER()
            return allFields

    class _dcck_cpu_hold(AtRegister.AtRegister):
        def name(self):
            return "DCCK CPU Reg Hold Control"
    
        def description(self):
            return "The register provides hold register for two word 32-bits MSB when CPU access to engine."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0000A + $HoldId"
            
        def startAddress(self):
            return 0x0000000a
            
        def endAddress(self):
            return 0x0000000b

        class _HoldReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HoldReg"
            
            def description(self):
                return "Hold 32 bits"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HoldReg"] = _AF6CNC0022_RD_DCCK._dcck_cpu_hold._HoldReg()
            return allFields

    class _upen_glbint_stt(AtRegister.AtRegister):
        def name(self):
            return "Global Interrupt Status"
    
        def description(self):
            return "The register provides global interrupt status"
            
        def width(self):
            return 3
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002001
            
        def endAddress(self):
            return 0xffffffff

        class _kbyte_interrupt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "kbyte_interrupt"
            
            def description(self):
                return "interrupt from KByte	event"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_interrupt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "dcc_interrupt"
            
            def description(self):
                return "interrupt from DCC event"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _hdlc_interrupt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "hdlc_interrupt"
            
            def description(self):
                return "interrupt from HDLC event"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["kbyte_interrupt"] = _AF6CNC0022_RD_DCCK._upen_glbint_stt._kbyte_interrupt()
            allFields["dcc_interrupt"] = _AF6CNC0022_RD_DCCK._upen_glbint_stt._dcc_interrupt()
            allFields["hdlc_interrupt"] = _AF6CNC0022_RD_DCCK._upen_glbint_stt._hdlc_interrupt()
            return allFields

    class _upen_glb_intenb(AtRegister.AtRegister):
        def name(self):
            return "Global Interrupt Enable"
    
        def description(self):
            return "The register provides configuration to enable global interrupt"
            
        def width(self):
            return 3
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _enb_kbyte_interrupt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "enb_kbyte_interrupt"
            
            def description(self):
                return "Enable interrupt for KByte events"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_dcc_interrupt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "enb_dcc_interrupt"
            
            def description(self):
                return "Enable interrupt for DCC event"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_hdlc_interrupt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "enb_hdlc_interrupt"
            
            def description(self):
                return "Enable interrupt for HDLC event"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["enb_kbyte_interrupt"] = _AF6CNC0022_RD_DCCK._upen_glb_intenb._enb_kbyte_interrupt()
            allFields["enb_dcc_interrupt"] = _AF6CNC0022_RD_DCCK._upen_glb_intenb._enb_dcc_interrupt()
            allFields["enb_hdlc_interrupt"] = _AF6CNC0022_RD_DCCK._upen_glb_intenb._enb_hdlc_interrupt()
            return allFields

    class _af6ces10grtldcc_rx__upen_glb_sta_cur_glb(AtRegister.AtRegister):
        def name(self):
            return "DCC ETH2OCN Direction Interupt Status of Packet Classification Error"
    
        def description(self):
            return "The register provides interrupt status of DCC event ETH2OCN direction"
            
        def width(self):
            return 5
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001080
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_eth2ocn_cur_sta_type_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "dcc_eth2ocn_cur_sta_type_mismat"
            
            def description(self):
                return "Current status of event \"Received ETHERNET TYPE value of DCC frame different from global provisioned TYPE\""
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_eth2ocn_cur_sta_ovrsize_len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "dcc_eth2ocn_cur_sta_ovrsize_len"
            
            def description(self):
                return "Current status of event \"Received DCC packet's length from SGMII port over maximum allowed length (1318 bytes)\""
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_eth2ocn_cur_sta_crc_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "dcc_eth2ocn_cur_sta_crc_error"
            
            def description(self):
                return "Current status of event \"Received packet from SGMII port has FCS error\""
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_eth2ocn_cur_sta_cvlid_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "dcc_eth2ocn_cur_sta_cvlid_mismat"
            
            def description(self):
                return "Current status of event \"Received 12b CVLAN ID value of DCC frame different from global provisioned CVID\""
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_eth2ocn_cur_sta_macda_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_eth2ocn_cur_sta_macda_mismat"
            
            def description(self):
                return "Current status of event \"Received 43b MAC DA value of DCC frame different from global provisioned DA\""
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_eth2ocn_cur_sta_type_mismat"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcc_rx__upen_glb_sta_cur_glb._dcc_eth2ocn_cur_sta_type_mismat()
            allFields["dcc_eth2ocn_cur_sta_ovrsize_len"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcc_rx__upen_glb_sta_cur_glb._dcc_eth2ocn_cur_sta_ovrsize_len()
            allFields["dcc_eth2ocn_cur_sta_crc_error"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcc_rx__upen_glb_sta_cur_glb._dcc_eth2ocn_cur_sta_crc_error()
            allFields["dcc_eth2ocn_cur_sta_cvlid_mismat"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcc_rx__upen_glb_sta_cur_glb._dcc_eth2ocn_cur_sta_cvlid_mismat()
            allFields["dcc_eth2ocn_cur_sta_macda_mismat"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcc_rx__upen_glb_sta_cur_glb._dcc_eth2ocn_cur_sta_macda_mismat()
            return allFields

    class _upen_int_rxdcc1_stt(AtRegister.AtRegister):
        def name(self):
            return "DCC ETH2OCN Direction Interupt Status of Packet Classification Error"
    
        def description(self):
            return "The register provides interrupt status of DCC event ETH2OCN direction"
            
        def width(self):
            return 5
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001040
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_eth2ocn_type_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "dcc_eth2ocn_type_mismat"
            
            def description(self):
                return "Received ETHERNET TYPE value of DCC frame different from global provisioned TYPE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_eth2ocn_ovrsize_len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "dcc_eth2ocn_ovrsize_len"
            
            def description(self):
                return "Received DCC packet's length from SGMII port over maximum allowed length	(1318 bytes)"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_eth2ocn_crc_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "dcc_eth2ocn_crc_error"
            
            def description(self):
                return "Received packet from SGMII port has FCS error"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_eth2ocn_cvlid_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "dcc_eth2ocn_cvlid_mismat"
            
            def description(self):
                return "Received 12b CVLAN ID value of DCC frame different from global provisioned CVID"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_eth2ocn_macda_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_eth2ocn_macda_mismat"
            
            def description(self):
                return "Received 43b MAC DA value of DCC frame different from global provisioned DA"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_eth2ocn_type_mismat"] = _AF6CNC0022_RD_DCCK._upen_int_rxdcc1_stt._dcc_eth2ocn_type_mismat()
            allFields["dcc_eth2ocn_ovrsize_len"] = _AF6CNC0022_RD_DCCK._upen_int_rxdcc1_stt._dcc_eth2ocn_ovrsize_len()
            allFields["dcc_eth2ocn_crc_error"] = _AF6CNC0022_RD_DCCK._upen_int_rxdcc1_stt._dcc_eth2ocn_crc_error()
            allFields["dcc_eth2ocn_cvlid_mismat"] = _AF6CNC0022_RD_DCCK._upen_int_rxdcc1_stt._dcc_eth2ocn_cvlid_mismat()
            allFields["dcc_eth2ocn_macda_mismat"] = _AF6CNC0022_RD_DCCK._upen_int_rxdcc1_stt._dcc_eth2ocn_macda_mismat()
            return allFields

    class _upen_rxdcc_intenb_r1(AtRegister.AtRegister):
        def name(self):
            return "DCC ETH2OCN Direction Enable Interrupt of "Packet Classification Error""
    
        def description(self):
            return "The register provides configuration to enable interrupt of DCC events ETH2OCN direction"
            
        def width(self):
            return 5
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0xffffffff

        class _enb_inb_dcc_eth2ocn_type_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "enb_inb_dcc_eth2ocn_type_mismat"
            
            def description(self):
                return "Enable Interrupt of \"Received ETHERNET TYPE value of DCC frame different from global provisioned TYPE\" (reg 0x02005)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_int_dcc_eth2ocn_ovrsize_len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "enb_int_dcc_eth2ocn_ovrsize_len"
            
            def description(self):
                return "Enable Interrupt of \"Received DCC packet's length from SGMII port over maximum allowed length (1318 bytes)\"(reg 0x02005)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_int_dcc_eth2ocn_crc_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "enb_int_dcc_eth2ocn_crc_error"
            
            def description(self):
                return "Enable Interrupt of \"Received packet from SGMII port has FCS error\" (reg 0x02005)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_int_dcc_eth2ocn_cvlid_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "enb_int_dcc_eth2ocn_cvlid_mismat"
            
            def description(self):
                return "Enable Interrupt of \"Received 12b CVLAN ID value of DCC frame different from global provisioned CVID \" (reg 0x02005)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_int_dcc_eth2ocn_macda_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "enb_int_dcc_eth2ocn_macda_mismat"
            
            def description(self):
                return "Enable Interrupt of \"Received 43b MAC DA value of DCC frame different from global provisioned DA\" (reg 0x02005)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["enb_inb_dcc_eth2ocn_type_mismat"] = _AF6CNC0022_RD_DCCK._upen_rxdcc_intenb_r1._enb_inb_dcc_eth2ocn_type_mismat()
            allFields["enb_int_dcc_eth2ocn_ovrsize_len"] = _AF6CNC0022_RD_DCCK._upen_rxdcc_intenb_r1._enb_int_dcc_eth2ocn_ovrsize_len()
            allFields["enb_int_dcc_eth2ocn_crc_error"] = _AF6CNC0022_RD_DCCK._upen_rxdcc_intenb_r1._enb_int_dcc_eth2ocn_crc_error()
            allFields["enb_int_dcc_eth2ocn_cvlid_mismat"] = _AF6CNC0022_RD_DCCK._upen_rxdcc_intenb_r1._enb_int_dcc_eth2ocn_cvlid_mismat()
            allFields["enb_int_dcc_eth2ocn_macda_mismat"] = _AF6CNC0022_RD_DCCK._upen_rxdcc_intenb_r1._enb_int_dcc_eth2ocn_macda_mismat()
            return allFields

    class _af6ces10grtldcck_corepi__upen_rxdcc_or_stt(AtRegister.AtRegister):
        def name(self):
            return "ETH2OCN DCC "Packet Clasification Error" Global OR Status"
    
        def description(self):
            return "This is status OR interrupt of Global."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000010ff
            
        def endAddress(self):
            return 0xffffffff

        class _Global_Intr_Or(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Global_Intr_Or"
            
            def description(self):
                return "Set to 1 if any interrupt bit of corresponding Group is set and its interrupt is enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Global_Intr_Or"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__upen_rxdcc_or_stt._Global_Intr_Or()
            return allFields

    class _af6ces10grtldcck_corepi__upen_rxdcc_or_enb(AtRegister.AtRegister):
        def name(self):
            return "ETH2OCN DCC "Packet Clasification Error" Global Interrupt OR Enable"
    
        def description(self):
            return "This is OR interrupt Enable per HDLC Channel Group. Each bit represent enable for that Group"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000010fe
            
        def endAddress(self):
            return 0xffffffff

        class _Global_Intr_Or_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Global_Intr_Or_En"
            
            def description(self):
                return "Set to 1 to enable Group interrupt. Bit[0] is for Group 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Global_Intr_Or_En"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__upen_rxdcc_or_enb._Global_Intr_Or_En()
            return allFields

    class _af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en(AtRegister.AtRegister):
        def name(self):
            return "DCC HDLC per Channel Interrupt Enable Control"
    
        def description(self):
            return "This is the per Channel interrupt enable of HDLC DCC"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00003000 +  ChannelID"
            
        def startAddress(self):
            return 0x00003000
            
        def endAddress(self):
            return 0x0000301f

        class _enb_int_ocn2eth_dcc_erro_und(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "enb_int_ocn2eth_dcc_erro_und"
            
            def description(self):
                return "Set 1 to enable Interrupt of \"OCN2ETH packet is undersize \""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_int_ocn2eth_dcc_erro_ovr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "enb_int_ocn2eth_dcc_erro_ovr"
            
            def description(self):
                return "Set 1 to enable Interrupt of \"OCN2ETH packet is oversize  \""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_int_ocn2eth_dcc_erro_fcs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "enb_int_ocn2eth_dcc_erro_fcs"
            
            def description(self):
                return "Set 1 to enable Interrupt of \"OCN2ETH packet has FCS error\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_int_ocn2eth_dcc_buff_ful(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "enb_int_ocn2eth_dcc_buff_ful"
            
            def description(self):
                return "Set 1 to enable Interrupt of \"OCN2ETH buffer full indication\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_int_eth2ocn_dcc_buff_ful(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "enb_int_eth2ocn_dcc_buff_ful"
            
            def description(self):
                return "Set 1 to enable Interrupt of \"ETH2OCN buffer full indication\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_int_eth2ocn_dcc_chan_dis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "enb_int_eth2ocn_dcc_chan_dis"
            
            def description(self):
                return "Set 1 to enable Interrupt of \"DCC Local Channel Identifier mapping Is Disable\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["enb_int_ocn2eth_dcc_erro_und"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en._enb_int_ocn2eth_dcc_erro_und()
            allFields["enb_int_ocn2eth_dcc_erro_ovr"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en._enb_int_ocn2eth_dcc_erro_ovr()
            allFields["enb_int_ocn2eth_dcc_erro_fcs"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en._enb_int_ocn2eth_dcc_erro_fcs()
            allFields["enb_int_ocn2eth_dcc_buff_ful"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en._enb_int_ocn2eth_dcc_buff_ful()
            allFields["enb_int_eth2ocn_dcc_buff_ful"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en._enb_int_eth2ocn_dcc_buff_ful()
            allFields["enb_int_eth2ocn_dcc_chan_dis"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en._enb_int_eth2ocn_dcc_chan_dis()
            return allFields

    class _af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta(AtRegister.AtRegister):
        def name(self):
            return "DCC HDLC per Channel Interrupt Status"
    
        def description(self):
            return "This is the per Channel interrupt status of HDLC DCC"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00003040 + ChannelID"
            
        def startAddress(self):
            return 0x00003040
            
        def endAddress(self):
            return 0x0000305f

        class _sta_int_ocn2eth_dcc_erro_und(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "sta_int_ocn2eth_dcc_erro_und"
            
            def description(self):
                return "Set 1 if there is a change event \"OCN2ETH packet is undersize \""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _sta_int_ocn2eth_dcc_erro_ovr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "sta_int_ocn2eth_dcc_erro_ovr"
            
            def description(self):
                return "Set 1 if there is a change event \"OCN2ETH packet is oversize  \""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _sta_int_ocn2eth_dcc_erro_fcs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "sta_int_ocn2eth_dcc_erro_fcs"
            
            def description(self):
                return "Set 1 if there is a change event \"OCN2ETH packet has FCS error\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _sta_int_ocn2eth_dcc_buff_ful(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "sta_int_ocn2eth_dcc_buff_ful"
            
            def description(self):
                return "Set 1 if there is a change event \"OCN2ETH buffer full indication\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _sta_int_eth2ocn_dcc_buff_ful(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "sta_int_eth2ocn_dcc_buff_ful"
            
            def description(self):
                return "Set 1 if there is a change event \"ETH2OCN buffer full indication\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _sta_int_eth2ocn_dcc_chan_dis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "sta_int_eth2ocn_dcc_chan_dis"
            
            def description(self):
                return "Set 1 if there is a change event \"DCC Local Channel Identifier mapping Is Disable\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["sta_int_ocn2eth_dcc_erro_und"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta._sta_int_ocn2eth_dcc_erro_und()
            allFields["sta_int_ocn2eth_dcc_erro_ovr"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta._sta_int_ocn2eth_dcc_erro_ovr()
            allFields["sta_int_ocn2eth_dcc_erro_fcs"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta._sta_int_ocn2eth_dcc_erro_fcs()
            allFields["sta_int_ocn2eth_dcc_buff_ful"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta._sta_int_ocn2eth_dcc_buff_ful()
            allFields["sta_int_eth2ocn_dcc_buff_ful"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta._sta_int_eth2ocn_dcc_buff_ful()
            allFields["sta_int_eth2ocn_dcc_chan_dis"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta._sta_int_eth2ocn_dcc_chan_dis()
            return allFields

    class _af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta(AtRegister.AtRegister):
        def name(self):
            return "DCC HDLC per Channel Currnet Status"
    
        def description(self):
            return "This is the per Channel current status of HDLC DCC"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00003080 +  ChannelID"
            
        def startAddress(self):
            return 0x00003080
            
        def endAddress(self):
            return 0x0000309f

        class _sta_cur_ocn2eth_dcc_erro_und(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "sta_cur_ocn2eth_dcc_erro_und"
            
            def description(self):
                return "Current Status of event \"OCN2ETH packet is undersize \""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sta_cur_ocn2eth_dcc_erro_ovr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "sta_cur_ocn2eth_dcc_erro_ovr"
            
            def description(self):
                return "Current Status of event \"OCN2ETH packet is oversize  \""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sta_cur_ocn2eth_dcc_erro_fcs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "sta_cur_ocn2eth_dcc_erro_fcs"
            
            def description(self):
                return "Current Status of event \"OCN2ETH packet has FCS error\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sta_cur_ocn2eth_dcc_buff_ful(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "sta_cur_ocn2eth_dcc_buff_ful"
            
            def description(self):
                return "Current Status of event \"OCN2ETH buffer full indication\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sta_cur_eth2ocn_dcc_buff_ful(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "sta_cur_eth2ocn_dcc_buff_ful"
            
            def description(self):
                return "Current Status of event \"ETH2OCN buffer full indication\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sta_cur_eth2ocn_dcc_chan_dis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "sta_cur_eth2ocn_dcc_chan_dis"
            
            def description(self):
                return "Current Status of event \"DCC Local Channel Identifier mapping Is Disable\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["sta_cur_ocn2eth_dcc_erro_und"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta._sta_cur_ocn2eth_dcc_erro_und()
            allFields["sta_cur_ocn2eth_dcc_erro_ovr"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta._sta_cur_ocn2eth_dcc_erro_ovr()
            allFields["sta_cur_ocn2eth_dcc_erro_fcs"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta._sta_cur_ocn2eth_dcc_erro_fcs()
            allFields["sta_cur_ocn2eth_dcc_buff_ful"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta._sta_cur_ocn2eth_dcc_buff_ful()
            allFields["sta_cur_eth2ocn_dcc_buff_ful"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta._sta_cur_eth2ocn_dcc_buff_ful()
            allFields["sta_cur_eth2ocn_dcc_chan_dis"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta._sta_cur_eth2ocn_dcc_chan_dis()
            return allFields

    class _af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_or_sta(AtRegister.AtRegister):
        def name(self):
            return "DCC HDLC per Channel Interrupt OR Status"
    
        def description(self):
            return "This is status OR interrupt of per HDLC channel. Each bit represent OR status of a HDLC channel"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000030C0 +  GrpID*32"
            
        def startAddress(self):
            return 0x000030c0
            
        def endAddress(self):
            return 0xffffffff

        class _HDLC_Chid_Intr_Or(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HDLC_Chid_Intr_Or"
            
            def description(self):
                return "Set to 1 if any interrupt bit of corresponding channelID is set and its interrupt is enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HDLC_Chid_Intr_Or"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_or_sta._HDLC_Chid_Intr_Or()
            return allFields

    class _af6ces10grtldcck_corepi__rtl_hdlc_int_pgrp_or_sta(AtRegister.AtRegister):
        def name(self):
            return "DCC HDLC per Group Interrupt OR Status"
    
        def description(self):
            return "This is status OR interrupt of per HDLC group. Each bit represent OR status of a HDLC group. Group 0 include channelID 0->31"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000030ff
            
        def endAddress(self):
            return 0xffffffff

        class _HDLC_Group_Intr_Or(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HDLC_Group_Intr_Or"
            
            def description(self):
                return "Set to 1 if any interrupt bit of corresponding Group is set and its interrupt is enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HDLC_Group_Intr_Or"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int_pgrp_or_sta._HDLC_Group_Intr_Or()
            return allFields

    class _af6ces10grtldcck_corepi__rtl_hdlc_int_pgrp_or_en(AtRegister.AtRegister):
        def name(self):
            return "DCC HDLC per Group Interrupt OR Enable"
    
        def description(self):
            return "This is OR interrupt Enable per HDLC Channel Group. Each bit represent enable for that Group"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000030fe
            
        def endAddress(self):
            return 0xffffffff

        class _HDLC_Group_Intr_Or_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HDLC_Group_Intr_Or_En"
            
            def description(self):
                return "Set to 1 to enable Group interrupt. Bit[0] is for Group 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HDLC_Group_Intr_Or_En"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__rtl_hdlc_int_pgrp_or_en._HDLC_Group_Intr_Or_En()
            return allFields

    class _upen_int_rxk12_stt(AtRegister.AtRegister):
        def name(self):
            return "KByte Interupt Status"
    
        def description(self):
            return "The register provides interrupt status of Kbyte event"
            
        def width(self):
            return 24
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002440
            
        def endAddress(self):
            return 0xffffffff

        class _aps_per_channel_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 8
        
            def name(self):
                return "aps_per_channel_mismat"
            
            def description(self):
                return "Received CHANNEL ID value of APS frame different from configured value. This is a per channel status. Bit 23 -> 08 represent for channel 15 -> 0 respectively"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _aps_watdog_alarm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "aps_watdog_alarm"
            
            def description(self):
                return "No packets received in the period defined in watchdog timer register"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _aps_glb_channel_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "aps_glb_channel_mismat"
            
            def description(self):
                return "There is one or more mismatch between received CHANNELID value of APS frame and configuration value of that channelID. This bit is set whenever a mismatch happen in one or more of 16 channels"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _aps_lencount_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "aps_lencount_mismat"
            
            def description(self):
                return "Received PACKET BYTE COUNTER value of APS frame different from configuration"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _aps_lenfield_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "aps_lenfield_mismat"
            
            def description(self):
                return "Received LENGTH FIELD value of APS frame different from configuration"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _aps_ver_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "aps_ver_mismat"
            
            def description(self):
                return "Received VERSION value of APS frame different from configuration"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _aps_apstp_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "aps_apstp_mismat"
            
            def description(self):
                return "Received TYPE value of APS frame different from configuration"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _aps_ethtp_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "aps_ethtp_mismat"
            
            def description(self):
                return "Received ETHERNET TYPE value of APS frame different from configuration"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _aps_macda_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "aps_macda_mismat"
            
            def description(self):
                return "Received DA value of APS frame different from configed DA"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["aps_per_channel_mismat"] = _AF6CNC0022_RD_DCCK._upen_int_rxk12_stt._aps_per_channel_mismat()
            allFields["aps_watdog_alarm"] = _AF6CNC0022_RD_DCCK._upen_int_rxk12_stt._aps_watdog_alarm()
            allFields["aps_glb_channel_mismat"] = _AF6CNC0022_RD_DCCK._upen_int_rxk12_stt._aps_glb_channel_mismat()
            allFields["aps_lencount_mismat"] = _AF6CNC0022_RD_DCCK._upen_int_rxk12_stt._aps_lencount_mismat()
            allFields["aps_lenfield_mismat"] = _AF6CNC0022_RD_DCCK._upen_int_rxk12_stt._aps_lenfield_mismat()
            allFields["aps_ver_mismat"] = _AF6CNC0022_RD_DCCK._upen_int_rxk12_stt._aps_ver_mismat()
            allFields["aps_apstp_mismat"] = _AF6CNC0022_RD_DCCK._upen_int_rxk12_stt._aps_apstp_mismat()
            allFields["aps_ethtp_mismat"] = _AF6CNC0022_RD_DCCK._upen_int_rxk12_stt._aps_ethtp_mismat()
            allFields["aps_macda_mismat"] = _AF6CNC0022_RD_DCCK._upen_int_rxk12_stt._aps_macda_mismat()
            return allFields

    class _upen_rxk12_intenb(AtRegister.AtRegister):
        def name(self):
            return "KByte Interupt Enable"
    
        def description(self):
            return "The register provides configuration to enable interrupt of Kbyte events"
            
        def width(self):
            return 24
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002400
            
        def endAddress(self):
            return 0xffffffff

        class _enb_int_aps_per_channel_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 8
        
            def name(self):
                return "enb_int_aps_per_channel_mismat"
            
            def description(self):
                return "Enable Interrupt of \"Received CHANNEL ID value of APS frame different from configuration per channel \". Bit 23->08 represent for mismatch of channelID 15->0 respectively"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_int_aps_watdog_alarm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "enb_int_aps_watdog_alarm"
            
            def description(self):
                return "Enable Interrupt of \"No packets received in the period defined in watchdog timer register\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_int_aps_glb_channel_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "enb_int_aps_glb_channel_mismat"
            
            def description(self):
                return "Enable Interrupt of \"Received CHANNEL ID value of APS frame different from configuration \". This bit is set when there is one or more mismatching channelID happen"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_int_aps_lencount_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "enb_int_aps_lencount_mismat"
            
            def description(self):
                return "Enable Interrupt of \"Received PACKET BYTE COUNTER value of APS frame different from configuration \""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_int_aps_lenfield_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "enb_int_aps_lenfield_mismat"
            
            def description(self):
                return "Enable Interrupt of \"Received LENGTH FIELD value of APS frame different from configuration \""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_int_aps_ver_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "enb_int_aps_ver_mismat"
            
            def description(self):
                return "Enable Interrupt of \"Received VERSION value of APS frame different from configuration \""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_int_aps_apstp_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "enb_int_aps_apstp_mismat"
            
            def description(self):
                return "Enable Interrupt of \"Received TYPE value of APS frame different from configuration \""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_int_aps_ethtp_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "enb_int_aps_ethtp_mismat"
            
            def description(self):
                return "Enable Interrupt of \"Received ETHERNET TYPE value of APS frame different from configuration \""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_int_aps_macda_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "enb_int_aps_macda_mismat"
            
            def description(self):
                return "Enable Interrupt of \"Received DA value of APS frame different from configed DA \""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["enb_int_aps_per_channel_mismat"] = _AF6CNC0022_RD_DCCK._upen_rxk12_intenb._enb_int_aps_per_channel_mismat()
            allFields["enb_int_aps_watdog_alarm"] = _AF6CNC0022_RD_DCCK._upen_rxk12_intenb._enb_int_aps_watdog_alarm()
            allFields["enb_int_aps_glb_channel_mismat"] = _AF6CNC0022_RD_DCCK._upen_rxk12_intenb._enb_int_aps_glb_channel_mismat()
            allFields["enb_int_aps_lencount_mismat"] = _AF6CNC0022_RD_DCCK._upen_rxk12_intenb._enb_int_aps_lencount_mismat()
            allFields["enb_int_aps_lenfield_mismat"] = _AF6CNC0022_RD_DCCK._upen_rxk12_intenb._enb_int_aps_lenfield_mismat()
            allFields["enb_int_aps_ver_mismat"] = _AF6CNC0022_RD_DCCK._upen_rxk12_intenb._enb_int_aps_ver_mismat()
            allFields["enb_int_aps_apstp_mismat"] = _AF6CNC0022_RD_DCCK._upen_rxk12_intenb._enb_int_aps_apstp_mismat()
            allFields["enb_int_aps_ethtp_mismat"] = _AF6CNC0022_RD_DCCK._upen_rxk12_intenb._enb_int_aps_ethtp_mismat()
            allFields["enb_int_aps_macda_mismat"] = _AF6CNC0022_RD_DCCK._upen_rxk12_intenb._enb_int_aps_macda_mismat()
            return allFields

    class _upen_k12rx_cur_err(AtRegister.AtRegister):
        def name(self):
            return "APS_RX_Alarm_Status"
    
        def description(self):
            return "The register provides current status of APS alarm"
            
        def width(self):
            return 24
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x02480"
            
        def startAddress(self):
            return 0x00002480
            
        def endAddress(self):
            return 0xffffffff

        class _per_channel_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 8
        
            def name(self):
                return "per_channel_mismat"
            
            def description(self):
                return "Received CHANNEL ID value of APS frame different from configured value. This is a per channel status. Bit 23 -> 08 represent for channel 15 -> 0 respectively"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _Frame_miss(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "Frame_miss"
            
            def description(self):
                return "Current status of APS FRAME missed alarm. When watchdog timer is enable. This bit is set if no frame is received in pre-defined interval"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _channel_miss(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "channel_miss"
            
            def description(self):
                return "Current status of global CHANNEL mismatch alarm. Received CHANNELID value different from configed"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _real_len_miss(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "real_len_miss"
            
            def description(self):
                return "Current status of ACTUAL LENGH mismatch alarm. acket LENGTH value different 0x60"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _len_miss(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "len_miss"
            
            def description(self):
                return "Current status of LENGH mismatch alarm. Received LENGTH value different 0x60"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _ver_miss(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "ver_miss"
            
            def description(self):
                return "Current status of VERSION  mismatch alarm. Received VERSION value different from configed VER"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _type_miss(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "type_miss"
            
            def description(self):
                return "Current status of TYPE mismatch alarm. Received TYPE value different from configed TYPE"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _ethtype_miss(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "ethtype_miss"
            
            def description(self):
                return "Current status of ETHERNET TYPE mismatch alarm. Received ETHTYPE value different from configed value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _mac_da_miss(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mac_da_miss"
            
            def description(self):
                return "DA mismatch. Received DA value different from configed DA"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["per_channel_mismat"] = _AF6CNC0022_RD_DCCK._upen_k12rx_cur_err._per_channel_mismat()
            allFields["Frame_miss"] = _AF6CNC0022_RD_DCCK._upen_k12rx_cur_err._Frame_miss()
            allFields["channel_miss"] = _AF6CNC0022_RD_DCCK._upen_k12rx_cur_err._channel_miss()
            allFields["real_len_miss"] = _AF6CNC0022_RD_DCCK._upen_k12rx_cur_err._real_len_miss()
            allFields["len_miss"] = _AF6CNC0022_RD_DCCK._upen_k12rx_cur_err._len_miss()
            allFields["ver_miss"] = _AF6CNC0022_RD_DCCK._upen_k12rx_cur_err._ver_miss()
            allFields["type_miss"] = _AF6CNC0022_RD_DCCK._upen_k12rx_cur_err._type_miss()
            allFields["ethtype_miss"] = _AF6CNC0022_RD_DCCK._upen_k12rx_cur_err._ethtype_miss()
            allFields["mac_da_miss"] = _AF6CNC0022_RD_DCCK._upen_k12rx_cur_err._mac_da_miss()
            return allFields

    class _af6ces10grtldcck_corepi__upen_k12rx_or_stt(AtRegister.AtRegister):
        def name(self):
            return "KByte Interupt OR Status"
    
        def description(self):
            return "This is status OR interrupt of Global."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000024ff
            
        def endAddress(self):
            return 0xffffffff

        class _KByte_Intr_Or_Stt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "KByte_Intr_Or_Stt"
            
            def description(self):
                return "Set to 1 if any interrupt bit of corresponding Group is set and its interrupt is enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["KByte_Intr_Or_Stt"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__upen_k12rx_or_stt._KByte_Intr_Or_Stt()
            return allFields

    class _af6ces10grtldcck_corepi__upen_k12rx_or_enb(AtRegister.AtRegister):
        def name(self):
            return "KByte Interupt OR Enable"
    
        def description(self):
            return "This is status OR interrupt of Global."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000024fe
            
        def endAddress(self):
            return 0xffffffff

        class _KByte_Intr_Or_Enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "KByte_Intr_Or_Enb"
            
            def description(self):
                return "Set to 1 to enable Group interrupt. Bit[0] is for Group 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["KByte_Intr_Or_Enb"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcck_corepi__upen_k12rx_or_enb._KByte_Intr_Or_Enb()
            return allFields

    class _upen_loopen(AtRegister.AtRegister):
        def name(self):
            return "Loopback Enable Configuration"
    
        def description(self):
            return "The register provides loopback enable configuration"
            
        def width(self):
            return 9
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _sel_cap_DCC_ENC_DEC(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "sel_cap_DCC_ENC_DEC"
            
            def description(self):
                return "Select capture DCC ENC or DEC data ."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sgmii_cap_tx_DCC_KByte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "sgmii_cap_tx_DCC_KByte"
            
            def description(self):
                return "Select capture TX of DCC or Kbyte data     ."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sgmii_cap_select(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "sgmii_cap_select"
            
            def description(self):
                return "Select capture TX or RX SGMII data             ."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_buffer_loop_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "dcc_buffer_loop_en"
            
            def description(self):
                return "Enable loopback of RX-BUFFER to TX-BUFFER      ."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _genmon_loop_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "genmon_loop_en"
            
            def description(self):
                return "Enable DCC GEN to RX-DCC                       ."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ksdh_loop_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "ksdh_loop_en"
            
            def description(self):
                return "Enable loopback of Kbyte information (TDM side)          ."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ksgm_loop_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "ksgm_loop_en"
            
            def description(self):
                return "Enable SGMII loopback of Kbyte Port."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _hdlc_loop_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "hdlc_loop_en"
            
            def description(self):
                return "Enable loopback from HDLC Encap to HDLC DEcap of DCC byte."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _dsgm_loop_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dsgm_loop_en"
            
            def description(self):
                return "Enable TX-SGMII to RX-SGMII loopback of DCC Port."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["sel_cap_DCC_ENC_DEC"] = _AF6CNC0022_RD_DCCK._upen_loopen._sel_cap_DCC_ENC_DEC()
            allFields["sgmii_cap_tx_DCC_KByte"] = _AF6CNC0022_RD_DCCK._upen_loopen._sgmii_cap_tx_DCC_KByte()
            allFields["sgmii_cap_select"] = _AF6CNC0022_RD_DCCK._upen_loopen._sgmii_cap_select()
            allFields["dcc_buffer_loop_en"] = _AF6CNC0022_RD_DCCK._upen_loopen._dcc_buffer_loop_en()
            allFields["genmon_loop_en"] = _AF6CNC0022_RD_DCCK._upen_loopen._genmon_loop_en()
            allFields["ksdh_loop_en"] = _AF6CNC0022_RD_DCCK._upen_loopen._ksdh_loop_en()
            allFields["ksgm_loop_en"] = _AF6CNC0022_RD_DCCK._upen_loopen._ksgm_loop_en()
            allFields["hdlc_loop_en"] = _AF6CNC0022_RD_DCCK._upen_loopen._hdlc_loop_en()
            allFields["dsgm_loop_en"] = _AF6CNC0022_RD_DCCK._upen_loopen._dsgm_loop_en()
            return allFields

    class _upen_cfg_dump_lid(AtRegister.AtRegister):
        def name(self):
            return "Configure Data Captured Channel ID"
    
        def description(self):
            return "The register is used to configure channel ID to capture packet of ENC/DEC"
            
        def width(self):
            return 8
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000030
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_dump_lid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_dump_lid"
            
            def description(self):
                return "Captured ENC/DEC Channel ID value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_dump_lid"] = _AF6CNC0022_RD_DCCK._upen_cfg_dump_lid._cfg_dump_lid()
            return allFields

    class _upen_cfg_dcc_en(AtRegister.AtRegister):
        def name(self):
            return "Enable DCC Engine"
    
        def description(self):
            return "The register is used to enable DCC Engine to receive/transmit data from/to OCN"
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000001f
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_dcc_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_dcc_en"
            
            def description(self):
                return "Enable DCC Engine to receive/transmit data from/to OCN."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_dcc_en"] = _AF6CNC0022_RD_DCCK._upen_cfg_dcc_en._cfg_dcc_en()
            return allFields

    class _upen_dcctx_fcsrem(AtRegister.AtRegister):
        def name(self):
            return "Dcc_Fcs_Rem_Mode"
    
        def description(self):
            return "The register provides configuration to remove FCS32 or FCS16"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00010003
            
        def endAddress(self):
            return 0xffffffff

        class _FCS_Remove_Mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FCS_Remove_Mode"
            
            def description(self):
                return "Remove 4 bytes FCS32 or 2byte FCS16 This configuration depend on the FCS checking mode at HDLC DEC (which is configed at reg address : 0x04000 - 0x04037) Bit 0 -> 31 : channel 0 -> 31"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FCS_Remove_Mode"] = _AF6CNC0022_RD_DCCK._upen_dcctx_fcsrem._FCS_Remove_Mode()
            return allFields

    class _upen_dcchdr(AtRegister.AtRegister):
        def name(self):
            return "DDC_TX_Header_Per_Channel"
    
        def description(self):
            return "The register provides data for configuration of 22bytes Header of each channel ID, in which, only 16byte is configurable the configuration postion of header byte is as follow; DA(6byte) + SA(6byte) + {PCP,DEI,VID}(2byte) + VERSION(1byte) + TYPE(1byte) hdrpos = 0 is position of DA[47:40] and hdrpos = 15 is position of TYPE field"
            
        def width(self):
            return 8
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x11200 + $channelid*16 + $hdrpos"
            
        def startAddress(self):
            return 0x00011200
            
        def endAddress(self):
            return 0xffffffff

        class _HEADER_POS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HEADER_POS"
            
            def description(self):
                return "8b value of header per Channel ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HEADER_POS"] = _AF6CNC0022_RD_DCCK._upen_dcchdr._HEADER_POS()
            return allFields

    class _upen_dcctx_enacid(AtRegister.AtRegister):
        def name(self):
            return "DDC_Channel_Enable"
    
        def description(self):
            return "The register provides configuration for enable transmission DDC packet per channel"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00011000
            
        def endAddress(self):
            return 0xffffffff

        class _Channel_Enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Channel_Enable"
            
            def description(self):
                return "Enable transmitting of DDC packet per channel. Bit[31:0] represent for channel 31->0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Channel_Enable"] = _AF6CNC0022_RD_DCCK._upen_dcctx_enacid._Channel_Enable()
            return allFields

    class _upen_dccrxhdr(AtRegister.AtRegister):
        def name(self):
            return "DDC_RX_Global_ProvisionedHeader_Configuration"
    
        def description(self):
            return "The register provides data for configuration of 22bytes Header of each channel ID"
            
        def width(self):
            return 71
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12001"
            
        def startAddress(self):
            return 0x00012001
            
        def endAddress(self):
            return 0xffffffff

        class _ETH_TYP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 70
                
            def startBit(self):
                return 55
        
            def name(self):
                return "ETH_TYP"
            
            def description(self):
                return "16b value of Provisioned ETHERTYPE of DCC"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CVID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 54
                
            def startBit(self):
                return 43
        
            def name(self):
                return "CVID"
            
            def description(self):
                return "12b value of Provisioned C-VLAN ID. This value is used to compared with received CVLAN ID value."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAC_DA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 42
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MAC_DA"
            
            def description(self):
                return "43b MSB of Provisioned MAC DA value. This value is used to compared with received MAC_DA[47:05] value. If a match is confirmed, MAC_DA[04:00] is used to represent channelID value before mapping."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ETH_TYP"] = _AF6CNC0022_RD_DCCK._upen_dccrxhdr._ETH_TYP()
            allFields["CVID"] = _AF6CNC0022_RD_DCCK._upen_dccrxhdr._CVID()
            allFields["MAC_DA"] = _AF6CNC0022_RD_DCCK._upen_dccrxhdr._MAC_DA()
            return allFields

    class _upen_dccrxmacenb(AtRegister.AtRegister):
        def name(self):
            return "DDC_RX_MAC_Check_Enable_Configuration"
    
        def description(self):
            return "The register provides configuration that enable base MAC DA check"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12002"
            
        def startAddress(self):
            return 0x00012002
            
        def endAddress(self):
            return 0xffffffff

        class _MAC_check_enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MAC_check_enable"
            
            def description(self):
                return "Enable checking of received MAC DA compare to globally provisioned Base MAC.On mismatch, frame is discarded."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MAC_check_enable"] = _AF6CNC0022_RD_DCCK._upen_dccrxmacenb._MAC_check_enable()
            return allFields

    class _upen_dccrxcvlenb(AtRegister.AtRegister):
        def name(self):
            return "DDC_RX_CVLAN_Check_Enable_Configuration"
    
        def description(self):
            return "The register provides configuration that enable base CVLAN Check"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12003"
            
        def startAddress(self):
            return 0x00012003
            
        def endAddress(self):
            return 0xffffffff

        class _CVL_check_enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CVL_check_enable"
            
            def description(self):
                return "Enable checking of received CVLAN ID compare to globally provisioned CVLAN ID.On mismatch, frame is discarded."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CVL_check_enable"] = _AF6CNC0022_RD_DCCK._upen_dccrxcvlenb._CVL_check_enable()
            return allFields

    class _upen_dccdec(AtRegister.AtRegister):
        def name(self):
            return "DDC_RX_Channel_Mapping"
    
        def description(self):
            return "The register provides channel mapping from received MAC DA to internal channelID"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12200 + $channelid"
            
        def startAddress(self):
            return 0x00012200
            
        def endAddress(self):
            return 0x0001221f

        class _Channel_enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Channel_enable"
            
            def description(self):
                return "Enable Local Channel Identifier. Rx packet is discarded if enable is not set"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Mapping_ChannelID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Mapping_ChannelID"
            
            def description(self):
                return "Local ChannelID that is mapped from received bit[4:0] of MAC DA"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Channel_enable"] = _AF6CNC0022_RD_DCCK._upen_dccdec._Channel_enable()
            allFields["Mapping_ChannelID"] = _AF6CNC0022_RD_DCCK._upen_dccdec._Mapping_ChannelID()
            return allFields

    class _upen_k12pro(AtRegister.AtRegister):
        def name(self):
            return "Provisioned_APS_KByte_Value_Per_Channel"
    
        def description(self):
            return "The register provides configuration for Kbyte value to overwrite"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x21200 + $channelid"
            
        def startAddress(self):
            return 0x00021200
            
        def endAddress(self):
            return 0x0002120f

        class _CHANNELID_APS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CHANNELID_APS"
            
            def description(self):
                return "32b of ChannelID APS value to overwrite This Kbyte value is used to overwrite Kbyte value get from RX-OCN when enable [31:24]: K1 byte value [23:16]: K2 byte value [15:08]: D1(EK1&EK2) (extend K) byte value [07:00]: set to 0x0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CHANNELID_APS"] = _AF6CNC0022_RD_DCCK._upen_k12pro._CHANNELID_APS()
            return allFields

    class _upen_cfgenacid(AtRegister.AtRegister):
        def name(self):
            return "Enable Transmit Validated KByte Change Per ChannelID Configuration"
    
        def description(self):
            return "The register configures channelID for provisioned APS packet"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x21001"
            
        def startAddress(self):
            return 0x00021001
            
        def endAddress(self):
            return 0xffffffff

        class _ChannelID_Enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ChannelID_Enable"
            
            def description(self):
                return "Enable provisioned ChannelID. Bit[15:0] represents channelID 15->0 This channelID bitmap is used to allow transmission of newly validated K byte change."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ChannelID_Enable"] = _AF6CNC0022_RD_DCCK._upen_cfgenacid._ChannelID_Enable()
            return allFields

    class _upen_cfgtim(AtRegister.AtRegister):
        def name(self):
            return "Timer_TX_Provisioned_APS_Packet"
    
        def description(self):
            return "The register configures timer for transmiting provisioned APS packets of configured channelID"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x21002"
            
        def startAddress(self):
            return 0x00021002
            
        def endAddress(self):
            return 0xffffffff

        class _Timer_Ena(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "Timer_Ena"
            
            def description(self):
                return "Enable Timer for TX provisoned APS packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Timer_Val(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Timer_Val"
            
            def description(self):
                return "Timer value for TX provisoned APS packet. This value is the number of clock counter represent the time interval For ex: to set an interval 256us with the clock 155Mz The clock counter is: 256x10^3/(10^3/155) = 256*155 = 39680 Timer range from 125us to 8ms. This timer is used only when bit 31 (Timer Ena) is set Each time the timer reach configurated value, an APS packet will be transmit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Timer_Ena"] = _AF6CNC0022_RD_DCCK._upen_cfgtim._Timer_Ena()
            allFields["Timer_Val"] = _AF6CNC0022_RD_DCCK._upen_cfgtim._Timer_Val()
            return allFields

    class _upen_cfgcid(AtRegister.AtRegister):
        def name(self):
            return "Trigger_TX_Provisioned_APS_Packet"
    
        def description(self):
            return "The register configures channelID for provisioned APS packet"
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x21003"
            
        def startAddress(self):
            return 0x00021003
            
        def endAddress(self):
            return 0xffffffff

        class _SW_Trig(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SW_Trig"
            
            def description(self):
                return "SW trigger transmission of Provisioned APS packet Write 0, then write 1 to trigger"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SW_Trig"] = _AF6CNC0022_RD_DCCK._upen_cfgcid._SW_Trig()
            return allFields

    class _upen_upen_cfgovwcid(AtRegister.AtRegister):
        def name(self):
            return "Enable KByte Overwrite Per ChannelID Configuration"
    
        def description(self):
            return "The register configures channelID for provisioned APS packet"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x21004"
            
        def startAddress(self):
            return 0x00021004
            
        def endAddress(self):
            return 0xffffffff

        class _ChannelID_Enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ChannelID_Enable"
            
            def description(self):
                return "Enable overwrite Kbyte value of channel Bit[15:0] represents channelID 15->0 This channelID bitmap is used to allow overwrite of channel's Kbyte. When this bit is set. Kbyte will get value from configurated Provisioned Kbyte instead of Kbyte received from RX-OCN"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ChannelID_Enable"] = _AF6CNC0022_RD_DCCK._upen_upen_cfgovwcid._ChannelID_Enable()
            return allFields

    class _upen_cfg_hdrmda(AtRegister.AtRegister):
        def name(self):
            return "APS_TX_Header_Mac_DA"
    
        def description(self):
            return "The register provides configuration of TX APS Packet"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00021006
            
        def endAddress(self):
            return 0xffffffff

        class _MAC_DA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MAC_DA"
            
            def description(self):
                return "MAC DA value of TX APS Packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MAC_DA"] = _AF6CNC0022_RD_DCCK._upen_cfg_hdrmda._MAC_DA()
            return allFields

    class _upen_cfg_hdrmsa(AtRegister.AtRegister):
        def name(self):
            return "APS_TX_Header_Mac_SA"
    
        def description(self):
            return "The register provides configuration of TX APS Packet"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00021007
            
        def endAddress(self):
            return 0xffffffff

        class _MAC_SA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MAC_SA"
            
            def description(self):
                return "MAC SA value of TX APS Packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MAC_SA"] = _AF6CNC0022_RD_DCCK._upen_cfg_hdrmsa._MAC_SA()
            return allFields

    class _upen_cfg_hdrvtl(AtRegister.AtRegister):
        def name(self):
            return "APS_TX_Header_VTL"
    
        def description(self):
            return "The register provides configuration of 18bytes Header of each channel ID receive from RX-OCN"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00021008
            
        def endAddress(self):
            return 0xffffffff

        class _VLAN(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 32
        
            def name(self):
                return "VLAN"
            
            def description(self):
                return "16b"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ETHTYPE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ETHTYPE"
            
            def description(self):
                return "16b Ethernet Type field"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _APSTYPE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "APSTYPE"
            
            def description(self):
                return "8b Type field"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VERSION(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VERSION"
            
            def description(self):
                return "8b Version Field"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VLAN"] = _AF6CNC0022_RD_DCCK._upen_cfg_hdrvtl._VLAN()
            allFields["ETHTYPE"] = _AF6CNC0022_RD_DCCK._upen_cfg_hdrvtl._ETHTYPE()
            allFields["APSTYPE"] = _AF6CNC0022_RD_DCCK._upen_cfg_hdrvtl._APSTYPE()
            allFields["VERSION"] = _AF6CNC0022_RD_DCCK._upen_cfg_hdrvtl._VERSION()
            return allFields

    class _upen_cid2pid(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "The register provides channel mapping from portID to ChannelID configuraton"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x21100 + $portID"
            
        def startAddress(self):
            return 0x00021100
            
        def endAddress(self):
            return 0x0002110f

        class _CHANNELID_VAL(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CHANNELID_VAL"
            
            def description(self):
                return "16b value of ChannelID of PortID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CHANNELID_VAL"] = _AF6CNC0022_RD_DCCK._upen_cid2pid._CHANNELID_VAL()
            return allFields

    class _upen_cfg_gencid(AtRegister.AtRegister):
        def name(self):
            return ""
    
        def description(self):
            return "The register provides the initialization Kbyte value of each channelID To init the content of each channelID, write bit31 = 0 first, then write bit[31] = 1 and the channelId as well of Kbyte value of that channelID. For example: to Init data FF for channel 0. Step1: wr 0x21009 0 Step2: wr 0x21009 800000FF"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00021009
            
        def endAddress(self):
            return 0xffffffff

        class _Init_Enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "Init_Enable"
            
            def description(self):
                return "Enable ChannelID Data Initializtion"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Init_ChannelId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 24
        
            def name(self):
                return "Init_ChannelId"
            
            def description(self):
                return "ChannelID value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Init_KbyteVal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Init_KbyteVal"
            
            def description(self):
                return "Default 24b value of Kbyte"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Init_Enable"] = _AF6CNC0022_RD_DCCK._upen_cfg_gencid._Init_Enable()
            allFields["Init_ChannelId"] = _AF6CNC0022_RD_DCCK._upen_cfg_gencid._Init_ChannelId()
            allFields["Init_KbyteVal"] = _AF6CNC0022_RD_DCCK._upen_cfg_gencid._Init_KbyteVal()
            return allFields

    class _upen_k12rx_macda(AtRegister.AtRegister):
        def name(self):
            return "APS_RX_Header_MAC_DA_Configuration"
    
        def description(self):
            return "The register provides MAC DA value of provisioned APS port"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x22001"
            
        def startAddress(self):
            return 0x00022001
            
        def endAddress(self):
            return 0xffffffff

        class _MAC_DA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MAC_DA"
            
            def description(self):
                return "Provisioned MAC DA value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MAC_DA"] = _AF6CNC0022_RD_DCCK._upen_k12rx_macda._MAC_DA()
            return allFields

    class _upen_k12rx_macsa(AtRegister.AtRegister):
        def name(self):
            return "APS_RX_Header_MAC_SA_Configuration"
    
        def description(self):
            return "The register provides MAC SA value of provisioned APS port"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x22002"
            
        def startAddress(self):
            return 0x00022002
            
        def endAddress(self):
            return 0xffffffff

        class _MAC_SA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MAC_SA"
            
            def description(self):
                return "Provisioned MAC SA value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MAC_SA"] = _AF6CNC0022_RD_DCCK._upen_k12rx_macsa._MAC_SA()
            return allFields

    class _upen_k12rx_evt(AtRegister.AtRegister):
        def name(self):
            return "APS_RX_Header_EVT_Configuration"
    
        def description(self):
            return "The register provides EVT(Ethernet Type, Version, APS Type) value of provisioned APS port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x22003"
            
        def startAddress(self):
            return 0x00022003
            
        def endAddress(self):
            return 0xffffffff

        class _ETHTYP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ETHTYP"
            
            def description(self):
                return "Provisioned Ethernet Type"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VER(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "VER"
            
            def description(self):
                return "Provisioned Version Number"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TYPE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TYPE"
            
            def description(self):
                return "Provisioned APS Type"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ETHTYP"] = _AF6CNC0022_RD_DCCK._upen_k12rx_evt._ETHTYP()
            allFields["VER"] = _AF6CNC0022_RD_DCCK._upen_k12rx_evt._VER()
            allFields["TYPE"] = _AF6CNC0022_RD_DCCK._upen_k12rx_evt._TYPE()
            return allFields

    class _upen_k12rx_chcfg(AtRegister.AtRegister):
        def name(self):
            return "APS_RX_ChannelID_Configuration"
    
        def description(self):
            return "The register provides ChannelID configuration"
            
        def width(self):
            return 20
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x22100 + $PortID"
            
        def startAddress(self):
            return 0x00022100
            
        def endAddress(self):
            return 0x0002210f

        class _ChannelID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ChannelID"
            
            def description(self):
                return "12b represent for Channel ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ChannelID"] = _AF6CNC0022_RD_DCCK._upen_k12rx_chcfg._ChannelID()
            return allFields

    class _upen_k12rx_chmap(AtRegister.AtRegister):
        def name(self):
            return "APS_RX_ChannelID_Mapping_Configuration"
    
        def description(self):
            return "The register provides ChannelID configuration"
            
        def width(self):
            return 20
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x22400 + $ChannelID"
            
        def startAddress(self):
            return 0x00022400
            
        def endAddress(self):
            return 0x0002240f

        class _PortID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PortID"
            
            def description(self):
                return "PortID value map from received Channel ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PortID"] = _AF6CNC0022_RD_DCCK._upen_k12rx_chmap._PortID()
            return allFields

    class _upen_k12rx_alarm(AtRegister.AtRegister):
        def name(self):
            return "APS_RX_Alarm_Sticky"
    
        def description(self):
            return "The register provides Receive APS Frame Alarm sticky"
            
        def width(self):
            return 24
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x22010"
            
        def startAddress(self):
            return 0x00022010
            
        def endAddress(self):
            return 0xffffffff

        class _per_channel_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 8
        
            def name(self):
                return "per_channel_mismat"
            
            def description(self):
                return "Received CHANNEL ID value of APS frame different from configured value. This is a per channel status. Bit 23 -> 08 represent for channel 15 -> 0 respectively"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Frame_miss(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "Frame_miss"
            
            def description(self):
                return "APS FRAME missed When watchdog timer is enable. This bit is set if no frame is received in pre-defined interval"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _channel_miss(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "channel_miss"
            
            def description(self):
                return "CHANNEL mismatch. Received CHANNELID value different from configed"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _real_len_miss(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "real_len_miss"
            
            def description(self):
                return "ACTUAL LENGH mismatch. acket LENGTH value different 0x60"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _len_miss(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "len_miss"
            
            def description(self):
                return "LENGH mismatch. Received LENGTH value different 0x60"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ver_miss(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "ver_miss"
            
            def description(self):
                return "VERSION  mismatch. Received VERSION value different from configed VER"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _type_miss(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "type_miss"
            
            def description(self):
                return "TYPE mismatch. Received TYPE value different from configed TYPE"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ethtype_miss(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "ethtype_miss"
            
            def description(self):
                return "ETHERNET TYPE mismatch. Received ETHTYPE value different from configed E"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _mac_da_miss(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mac_da_miss"
            
            def description(self):
                return "DA mismatch. Received DA value different from configed DA"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["per_channel_mismat"] = _AF6CNC0022_RD_DCCK._upen_k12rx_alarm._per_channel_mismat()
            allFields["Frame_miss"] = _AF6CNC0022_RD_DCCK._upen_k12rx_alarm._Frame_miss()
            allFields["channel_miss"] = _AF6CNC0022_RD_DCCK._upen_k12rx_alarm._channel_miss()
            allFields["real_len_miss"] = _AF6CNC0022_RD_DCCK._upen_k12rx_alarm._real_len_miss()
            allFields["len_miss"] = _AF6CNC0022_RD_DCCK._upen_k12rx_alarm._len_miss()
            allFields["ver_miss"] = _AF6CNC0022_RD_DCCK._upen_k12rx_alarm._ver_miss()
            allFields["type_miss"] = _AF6CNC0022_RD_DCCK._upen_k12rx_alarm._type_miss()
            allFields["ethtype_miss"] = _AF6CNC0022_RD_DCCK._upen_k12rx_alarm._ethtype_miss()
            allFields["mac_da_miss"] = _AF6CNC0022_RD_DCCK._upen_k12rx_alarm._mac_da_miss()
            return allFields

    class _upen_k12rx_watdog(AtRegister.AtRegister):
        def name(self):
            return "APS_RX_WatchDog_Timer"
    
        def description(self):
            return "The register provides WatchDog Timer configuration"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x22008"
            
        def startAddress(self):
            return 0x00022008
            
        def endAddress(self):
            return 0xffffffff

        class _Enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "Enable"
            
            def description(self):
                return "Enable Watchdog Timer"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Timer_Val(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Timer_Val"
            
            def description(self):
                return "Watch Dog Timer value. This value is the number of clock counter represent the expected time interval For ex: to set an interval 256us with the clock 155Mz The clock counter is: 256x10^3/(10^3/155) = 256*155 = 39680 This value will be write to this field. Timer range from 256us to 16ms. If in this timer window, no frames received, alarm will be set"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Enable"] = _AF6CNC0022_RD_DCCK._upen_k12rx_watdog._Enable()
            allFields["Timer_Val"] = _AF6CNC0022_RD_DCCK._upen_k12rx_watdog._Timer_Val()
            return allFields

    class _upen_stkerr_pktlen(AtRegister.AtRegister):
        def name(self):
            return "DCC_OCN2ETH_Pkt_Length_Alarm_Sticky"
    
        def description(self):
            return "The register provides Alarm Related to HDLC Length Error (OCN2ETH Direction)"
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x11004"
            
        def startAddress(self):
            return 0x00011004
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_overize_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 32
        
            def name(self):
                return "dcc_overize_err"
            
            def description(self):
                return "HDLC Length Oversize Error. Alarm per channel Received HDLC Frame from OCN has frame length over maximum allowed length (1536bytes) Bit[63] -> Bit[32]: channel ID 31 ->0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_undsize_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_undsize_err"
            
            def description(self):
                return "HDLC Length Undersize Error. Alarm per channel Received HDLC Frame from OCN has frame length below minimum allowed length (2bytes) Bit[31] -> Bit[0]: channel ID 31 ->0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_overize_err"] = _AF6CNC0022_RD_DCCK._upen_stkerr_pktlen._dcc_overize_err()
            allFields["dcc_undsize_err"] = _AF6CNC0022_RD_DCCK._upen_stkerr_pktlen._dcc_undsize_err()
            return allFields

    class _upen_stkerr_crcbuf(AtRegister.AtRegister):
        def name(self):
            return "DCC_OCN2ETH_Pkt_Error_And_Buffer_Full_Alarm_Sticky"
    
        def description(self):
            return "The register provides Alarm Related to HDLC CRC Error and Buffer Full (OCN2ETH Direction)"
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x11005"
            
        def startAddress(self):
            return 0x00011005
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_crc_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 32
        
            def name(self):
                return "dcc_crc_err"
            
            def description(self):
                return "HDLC Packet has CRC error (OCN to ETH direction) . Alarm per channel Bit[63] -> Bit[32]: channel ID 31 ->0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_buffull_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_buffull_err"
            
            def description(self):
                return "HDLC Packet buffer full (OCN to ETH direction) . Alarm per channel Buffer for HDLC Frame has been full. Some frames will be dropped Bit[31] -> Bit[0]: channel ID 31 ->0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_crc_err"] = _AF6CNC0022_RD_DCCK._upen_stkerr_crcbuf._dcc_crc_err()
            allFields["dcc_buffull_err"] = _AF6CNC0022_RD_DCCK._upen_stkerr_crcbuf._dcc_buffull_err()
            return allFields

    class _upen_stkerr_rx_eth2ocn(AtRegister.AtRegister):
        def name(self):
            return "DCC_ETH2OCN_Alarm_Sticky"
    
        def description(self):
            return "The register provides Alarms of ETH2OCN Direction"
            
        def width(self):
            return 69
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12008"
            
        def startAddress(self):
            return 0x00012008
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_eth2ocn_buffull_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 68
                
            def startBit(self):
                return 37
        
            def name(self):
                return "dcc_eth2ocn_buffull_err"
            
            def description(self):
                return "Packet buffer full (ETH to OCN direction) . Alarm per channel Buffer of Ethernet Frame has been full. Some frames will be dropped Bit[68] -> Bit[37]: channel ID 31 ->0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_rxeth_maxlenerr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 36
                
            def startBit(self):
                return 36
        
            def name(self):
                return "dcc_rxeth_maxlenerr"
            
            def description(self):
                return "Received DCC packet from SGMII port has violated maximum packet's length error"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_rxeth_crcerr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 35
        
            def name(self):
                return "dcc_rxeth_crcerr"
            
            def description(self):
                return "Received DCC packet from SGMII port has CRC error"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_channel_disable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 34
                
            def startBit(self):
                return 3
        
            def name(self):
                return "dcc_channel_disable"
            
            def description(self):
                return "DCC Local Channel Identifier mapping is disable Bit[34] -> Bit[03] indicate channel 31-> 00."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_cvlid_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "dcc_cvlid_mismat"
            
            def description(self):
                return "Received 12b CVLAN ID value of DCC frame different from global provisioned CVID"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_macda_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "dcc_macda_mismat"
            
            def description(self):
                return "Received 43b MAC DA value of DCC frame different from global provisioned DA"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _dcc_ethtp_mismat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_ethtp_mismat"
            
            def description(self):
                return "Received Ethernet Type of DCC frame different from global provisioned value"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_eth2ocn_buffull_err"] = _AF6CNC0022_RD_DCCK._upen_stkerr_rx_eth2ocn._dcc_eth2ocn_buffull_err()
            allFields["dcc_rxeth_maxlenerr"] = _AF6CNC0022_RD_DCCK._upen_stkerr_rx_eth2ocn._dcc_rxeth_maxlenerr()
            allFields["dcc_rxeth_crcerr"] = _AF6CNC0022_RD_DCCK._upen_stkerr_rx_eth2ocn._dcc_rxeth_crcerr()
            allFields["dcc_channel_disable"] = _AF6CNC0022_RD_DCCK._upen_stkerr_rx_eth2ocn._dcc_channel_disable()
            allFields["dcc_cvlid_mismat"] = _AF6CNC0022_RD_DCCK._upen_stkerr_rx_eth2ocn._dcc_cvlid_mismat()
            allFields["dcc_macda_mismat"] = _AF6CNC0022_RD_DCCK._upen_stkerr_rx_eth2ocn._dcc_macda_mismat()
            allFields["dcc_ethtp_mismat"] = _AF6CNC0022_RD_DCCK._upen_stkerr_rx_eth2ocn._dcc_ethtp_mismat()
            return allFields

    class _upen_curerr_undsz(AtRegister.AtRegister):
        def name(self):
            return "DCC_OCN2ETH_Min_Pkt_Length_Current Status"
    
        def description(self):
            return "The register provides Alarm Related to HDLC Length Error (OCN2ETH Direction)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x11008"
            
        def startAddress(self):
            return 0x00011008
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_ocn2eth_undsz_err_cursta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_ocn2eth_undsz_err_cursta"
            
            def description(self):
                return "HDLC Length Undersize Error. Status per channel Received HDLC Frame from OCN has frame length below minimum allowed length (2bytes) Bit[31] -> Bit[0]: channel ID 31 ->0"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_ocn2eth_undsz_err_cursta"] = _AF6CNC0022_RD_DCCK._upen_curerr_undsz._dcc_ocn2eth_undsz_err_cursta()
            return allFields

    class _upen_curerr_ovrsz(AtRegister.AtRegister):
        def name(self):
            return "DCC_OCN2ETH_Max_Pkt_Length_Current_Status"
    
        def description(self):
            return "The register provides Alarm Status Related to HDLC Length Error (OCN2ETH Direction)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x11009"
            
        def startAddress(self):
            return 0x00011009
            
        def endAddress(self):
            return 0xffffffff

        class _cc_ocn2eth_ovrsz_err_cursta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cc_ocn2eth_ovrsz_err_cursta"
            
            def description(self):
                return "HDLC Length Oversize Error. Status per channel Received HDLC Frame from OCN has frame length over maximum allowed length (1536bytes) Bit[31] -> Bit[0]: channel ID 31 ->0"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cc_ocn2eth_ovrsz_err_cursta"] = _AF6CNC0022_RD_DCCK._upen_curerr_ovrsz._cc_ocn2eth_ovrsz_err_cursta()
            return allFields

    class _upen_curerr_crcbuf(AtRegister.AtRegister):
        def name(self):
            return "DCC_OCN2ETH_Pkt_CRC_Error_Alarm_Status"
    
        def description(self):
            return "The register provides Alarm Status Related to HDLC CRC Error (OCN2ETH Direction)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1100A"
            
        def startAddress(self):
            return 0x0001100a
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_ocn2eth_crc_err_cursta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_ocn2eth_crc_err_cursta"
            
            def description(self):
                return "HDLC Packet has CRC error (OCN to ETH direction) . Alarm per channel Bit[31] -> Bit[0]: channel ID 31 ->0"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_ocn2eth_crc_err_cursta"] = _AF6CNC0022_RD_DCCK._upen_curerr_crcbuf._dcc_ocn2eth_crc_err_cursta()
            return allFields

    class _upen_curerr_bufept(AtRegister.AtRegister):
        def name(self):
            return "DCC_OCN2ETH_Buffer_Full_Alarm_Current_Status"
    
        def description(self):
            return "The register provides Alarm Related to HDLC Buffer Full (OCN2ETH Direction)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1100B"
            
        def startAddress(self):
            return 0x0001100b
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_ocn2eth_bufful_err_cursta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_ocn2eth_bufful_err_cursta"
            
            def description(self):
                return "HDLC Packet buffer full (OCN to ETH direction) . Alarm per channel Buffer for HDLC Frame has been full. Some frames will be dropped Bit[31] -> Bit[0]: channel ID 31 ->0"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_ocn2eth_bufful_err_cursta"] = _AF6CNC0022_RD_DCCK._upen_curerr_bufept._dcc_ocn2eth_bufful_err_cursta()
            return allFields

    class _upen_curerr_glb(AtRegister.AtRegister):
        def name(self):
            return "DCC OCN2ETH Current Status of Global Alarm"
    
        def description(self):
            return "The register provides current status of global alarm of DCC, OCN2ETH direction"
            
        def width(self):
            return 4
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0001100e
            
        def endAddress(self):
            return 0xffffffff

        class _mux_dcc_ocn2eth_cursta_ful_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "mux_dcc_ocn2eth_cursta_ful_err"
            
            def description(self):
                return "Current Status of  \"OCN2ETH: One or more channels has buffer full error\""
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _mux_dcc_ocn2eth_cursta_crc_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "mux_dcc_ocn2eth_cursta_crc_err"
            
            def description(self):
                return "Current Status of  \"OCN2ETH: One or more channels has CRC error packet \""
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _mux_dcc_ocn2eth_cursta_undsz_len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "mux_dcc_ocn2eth_cursta_undsz_len"
            
            def description(self):
                return "Current Status of  \"OCN2ETH: One or more channels has minimum packet length violation\""
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _mux_dcc_ocn2eth_cursta_ovrsz_len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mux_dcc_ocn2eth_cursta_ovrsz_len"
            
            def description(self):
                return "Current Status of  \"OCN2ETH: One or more channels has maximum packet length violation \""
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mux_dcc_ocn2eth_cursta_ful_err"] = _AF6CNC0022_RD_DCCK._upen_curerr_glb._mux_dcc_ocn2eth_cursta_ful_err()
            allFields["mux_dcc_ocn2eth_cursta_crc_err"] = _AF6CNC0022_RD_DCCK._upen_curerr_glb._mux_dcc_ocn2eth_cursta_crc_err()
            allFields["mux_dcc_ocn2eth_cursta_undsz_len"] = _AF6CNC0022_RD_DCCK._upen_curerr_glb._mux_dcc_ocn2eth_cursta_undsz_len()
            allFields["mux_dcc_ocn2eth_cursta_ovrsz_len"] = _AF6CNC0022_RD_DCCK._upen_curerr_glb._mux_dcc_ocn2eth_cursta_ovrsz_len()
            return allFields

    class _upen_cur_rxdcc0(AtRegister.AtRegister):
        def name(self):
            return "DCC ETH2OCN Direction Alarm " Local Channel Mapping Is Disable" Current Status of Channel 31 to 0"
    
        def description(self):
            return "The register provides configuration to enable interrupt of DCC events ETH2OCN direction"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00012004
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_eth2ocn_chandis_cursta_31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_eth2ocn_chandis_cursta_31_0"
            
            def description(self):
                return "Current Status of Alarm \"DCC Local Channel Identifier mapping is disable\" of channel 0 to 31 Bit[00] -> Bit[31] indicate channel 0 -> 31."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_eth2ocn_chandis_cursta_31_0"] = _AF6CNC0022_RD_DCCK._upen_cur_rxdcc0._dcc_eth2ocn_chandis_cursta_31_0()
            return allFields

    class _upen_cur_rxdcc2(AtRegister.AtRegister):
        def name(self):
            return "DCC ETH2OCN Direction Curren Status of Buffer Full Channel 31 to 0"
    
        def description(self):
            return "The register provides current alarm status of DCC event ETH2OCN direction"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00012006
            
        def endAddress(self):
            return 0xffffffff

        class _dcc_eth2ocn_bufful_cursta_31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_eth2ocn_bufful_cursta_31_0"
            
            def description(self):
                return "Current status of \"DCC packet buffer for ETH2OCN direction was fulled, some packets will be lost.\" channel 0 to 31 Bit[00] -> Bit[31] indicate status of channel 0 -> 31."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_eth2ocn_bufful_cursta_31_0"] = _AF6CNC0022_RD_DCCK._upen_cur_rxdcc2._dcc_eth2ocn_bufful_cursta_31_0()
            return allFields

    class _upen_k12rx_trig_encap(AtRegister.AtRegister):
        def name(self):
            return "APS_RX_Trig_En_Cap"
    
        def description(self):
            return "The register provides WatchDog Timer configuration"
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x22006"
            
        def startAddress(self):
            return 0x00022006
            
        def endAddress(self):
            return 0xffffffff

        class _Enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Enable"
            
            def description(self):
                return "Trigger Enable Capturing Ethernet Header - Write '0' first, then write '1' to enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Enable"] = _AF6CNC0022_RD_DCCK._upen_k12rx_trig_encap._Enable()
            return allFields

    class _upen_k12rx_cap_reg0(AtRegister.AtRegister):
        def name(self):
            return "APS_RX_Packet_Header_Cap_Reg0"
    
        def description(self):
            return "The register provides captured header data"
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x22200 + 3*$pktnum"
            
        def startAddress(self):
            return 0x00022200
            
        def endAddress(self):
            return 0x0002220c

        class _MAC_DA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 16
        
            def name(self):
                return "MAC_DA"
            
            def description(self):
                return "Captured MAC DA value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _MAC_SA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MAC_SA"
            
            def description(self):
                return "16b MSB of Captured MAC SA value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MAC_DA"] = _AF6CNC0022_RD_DCCK._upen_k12rx_cap_reg0._MAC_DA()
            allFields["MAC_SA"] = _AF6CNC0022_RD_DCCK._upen_k12rx_cap_reg0._MAC_SA()
            return allFields

    class _upen_k12rx_cap_reg1(AtRegister.AtRegister):
        def name(self):
            return "APS_RX_Packet_Header_Cap_Reg1"
    
        def description(self):
            return "The register provides captured header data"
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x22201 + 3*$pktnum"
            
        def startAddress(self):
            return 0x00022201
            
        def endAddress(self):
            return 0x0002220d

        class _MAC_SA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 32
        
            def name(self):
                return "MAC_SA"
            
            def description(self):
                return "32b MSB of Captured MAC SA value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _VLAN(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VLAN"
            
            def description(self):
                return "Captured VLAN value:"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MAC_SA"] = _AF6CNC0022_RD_DCCK._upen_k12rx_cap_reg1._MAC_SA()
            allFields["VLAN"] = _AF6CNC0022_RD_DCCK._upen_k12rx_cap_reg1._VLAN()
            return allFields

    class _upen_k12rx_cap_reg2(AtRegister.AtRegister):
        def name(self):
            return "APS_RX_Packet_Header_Cap_Reg2"
    
        def description(self):
            return "The register provides captured header data"
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x22202 + 3*$pktnum"
            
        def startAddress(self):
            return 0x00022202
            
        def endAddress(self):
            return 0x0002220e

        class _ETH_TYP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 48
        
            def name(self):
                return "ETH_TYP"
            
            def description(self):
                return "16b value of Ethernet Type"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _VER(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 40
        
            def name(self):
                return "VER"
            
            def description(self):
                return "8b Version"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _TYP(AtRegister.AtRegisterField):
            def stopBit(self):
                return 39
                
            def startBit(self):
                return 32
        
            def name(self):
                return "TYP"
            
            def description(self):
                return "8b APS Type"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _LENGTH(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "LENGTH"
            
            def description(self):
                return "16b packet length"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ETH_TYP"] = _AF6CNC0022_RD_DCCK._upen_k12rx_cap_reg2._ETH_TYP()
            allFields["VER"] = _AF6CNC0022_RD_DCCK._upen_k12rx_cap_reg2._VER()
            allFields["TYP"] = _AF6CNC0022_RD_DCCK._upen_k12rx_cap_reg2._TYP()
            allFields["LENGTH"] = _AF6CNC0022_RD_DCCK._upen_k12rx_cap_reg2._LENGTH()
            return allFields

    class _upen_k12_glbcnt_rxeop(AtRegister.AtRegister):
        def name(self):
            return "Counter_Rx_Unknow_From_SGMII_Port0"
    
        def description(self):
            return "Counter of number of Unknow Packet (not APS packet) receive from SGMII port 0"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x22021"
            
        def startAddress(self):
            return 0x00022021
            
        def endAddress(self):
            return 0xffffffff

        class _glb_sgm2ocn_aps_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "glb_sgm2ocn_aps_counter"
            
            def description(self):
                return "Counter of receive packet from SGMII port 0 has ETH_TYPE of KByte"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["glb_sgm2ocn_aps_counter"] = _AF6CNC0022_RD_DCCK._upen_k12_glbcnt_rxeop._glb_sgm2ocn_aps_counter()
            return allFields

    class _upen_k12_glbcnt_sfail(AtRegister.AtRegister):
        def name(self):
            return "Counter_Rx_Unknow_From_SGMII_Port0"
    
        def description(self):
            return "Counter of number of Unknow Packet (not APS packet) receive from SGMII port 0"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x22024"
            
        def startAddress(self):
            return 0x00022024
            
        def endAddress(self):
            return 0xffffffff

        class _glb_sgm2ocn_unk_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "glb_sgm2ocn_unk_counter"
            
            def description(self):
                return "Counter of unknow receive packet from SGMII port 0 (not KByte nor DCC packet)"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["glb_sgm2ocn_unk_counter"] = _AF6CNC0022_RD_DCCK._upen_k12_glbcnt_sfail._glb_sgm2ocn_unk_counter()
            return allFields

    class _upen_k12_glbcnt_sok(AtRegister.AtRegister):
        def name(self):
            return "Counter_Total_Rx_APS_Passed_From_SGMII_Port0"
    
        def description(self):
            return "Counter of total number of detected APS Packet receive from SGMII port 0"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x22025"
            
        def startAddress(self):
            return 0x00022025
            
        def endAddress(self):
            return 0xffffffff

        class _glb_sgm2ocn_sok_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "glb_sgm2ocn_sok_counter"
            
            def description(self):
                return "Counter of detected APS packet and passed classification from SGMII port 0"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["glb_sgm2ocn_sok_counter"] = _AF6CNC0022_RD_DCCK._upen_k12_glbcnt_sok._glb_sgm2ocn_sok_counter()
            return allFields

    class _upen_k12_glbcnt_sok_byte(AtRegister.AtRegister):
        def name(self):
            return "Counter_Total_Rx_APS_Byte_Passed_From_SGMII_Port0"
    
        def description(self):
            return "Counter of total number of detected APS Packet receive from SGMII port 0"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x22023"
            
        def startAddress(self):
            return 0x00022023
            
        def endAddress(self):
            return 0xffffffff

        class _glb_sgm2ocn_sok_byte_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "glb_sgm2ocn_sok_byte_counter"
            
            def description(self):
                return "Counter bytes of detected Kbyte packet passed classification from SGMII port 0"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["glb_sgm2ocn_sok_byte_counter"] = _AF6CNC0022_RD_DCCK._upen_k12_glbcnt_sok_byte._glb_sgm2ocn_sok_byte_counter()
            return allFields

    class _upen_k12_glbcnt_serr(AtRegister.AtRegister):
        def name(self):
            return "Counter_Rx_Discarded_APS_From_SGMII_Port0"
    
        def description(self):
            return "Counter of number of Unknow Packet (not APS packet) receive from SGMII port 0"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x22026"
            
        def startAddress(self):
            return 0x00022026
            
        def endAddress(self):
            return 0xffffffff

        class _glb_sgm2ocn_serr_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "glb_sgm2ocn_serr_counter"
            
            def description(self):
                return "Counter of discarded APS packets received from SGMII port 0 Ethernet Type of packet is detect as Kbyte packet but the packet has fail other conditions to extract Kbyte value in packet"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["glb_sgm2ocn_serr_counter"] = _AF6CNC0022_RD_DCCK._upen_k12_glbcnt_serr._glb_sgm2ocn_serr_counter()
            return allFields

    class _upen_k12_sokpkt_pcid(AtRegister.AtRegister):
        def name(self):
            return "Counter_Rx_APS_From_SGMII_Port0_Per_Chan"
    
        def description(self):
            return "Counter of number of detected APS Packet per channel receive from SGMII port 0 per channelID"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x22300 + $channelID"
            
        def startAddress(self):
            return 0x00022300
            
        def endAddress(self):
            return 0x0002230f

        class _pcid_sgm2ocn_sok_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pcid_sgm2ocn_sok_counter"
            
            def description(self):
                return "Counter of APS packets receive from SGMII port 0 per channelID"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pcid_sgm2ocn_sok_counter"] = _AF6CNC0022_RD_DCCK._upen_k12_sokpkt_pcid._pcid_sgm2ocn_sok_counter()
            return allFields

    class _upen_byt2k12_pcid(AtRegister.AtRegister):
        def name(self):
            return "Counter_Rx_APS_BYTE_From_RxOcn_Per_Chan"
    
        def description(self):
            return "Counter number of BYTE of APS packet receive from RX-OCN Per Channel"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x21330 + $channelID"
            
        def startAddress(self):
            return 0x00021330
            
        def endAddress(self):
            return 0x0002133f

        class _byte_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "byte_counter"
            
            def description(self):
                return "Counter number of BYTE of APS packets receive from RX-OCN"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["byte_counter"] = _AF6CNC0022_RD_DCCK._upen_byt2k12_pcid._byte_counter()
            return allFields

    class _upen_glbsop2k12(AtRegister.AtRegister):
        def name(self):
            return "Counter_Total_Rx_APS_Packet_From_RxOcn"
    
        def description(self):
            return "Counter Total number of RX APS packet receive from RX-OCN"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00021360
            
        def endAddress(self):
            return 0xffffffff

        class _rx_aps_pkt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_aps_pkt_cnt"
            
            def description(self):
                return "Counter number of APS packets receive from RX-OCN"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_aps_pkt_cnt"] = _AF6CNC0022_RD_DCCK._upen_glbsop2k12._rx_aps_pkt_cnt()
            return allFields

    class _upen_glbbyt2k12(AtRegister.AtRegister):
        def name(self):
            return "Counter_Rx_APS_BYTE_From_RxOcn_Per_Chan"
    
        def description(self):
            return "Counter number of Total BYTE of APS packet receive from RX-OCN"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00021361
            
        def endAddress(self):
            return 0xffffffff

        class _rx_aps_byte_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rx_aps_byte_counter"
            
            def description(self):
                return "Counter number of BYTE of APS packets receive from RX-OCN"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rx_aps_byte_counter"] = _AF6CNC0022_RD_DCCK._upen_glbbyt2k12._rx_aps_byte_counter()
            return allFields

    class _upen_k12sop2sgm_pcid(AtRegister.AtRegister):
        def name(self):
            return "Counter_Tx_APS_SOP_To_SGMII"
    
        def description(self):
            return "Counter number of SOP of TX APS packet to SGMII"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00021350
            
        def endAddress(self):
            return 0xffffffff

        class _sop_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "sop_counter"
            
            def description(self):
                return "Counter number of SOP of TX APS packets to SGMII"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["sop_counter"] = _AF6CNC0022_RD_DCCK._upen_k12sop2sgm_pcid._sop_counter()
            return allFields

    class _upen_k12eop2sgm_pcid(AtRegister.AtRegister):
        def name(self):
            return "Counter_Tx_APS_EOP_To_SGMII"
    
        def description(self):
            return "Counter number of EOP of TX APS packet to SGMII"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00021351
            
        def endAddress(self):
            return 0xffffffff

        class _eop_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "eop_counter"
            
            def description(self):
                return "Counter number of EOP of TX APS packets to SGMII"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["eop_counter"] = _AF6CNC0022_RD_DCCK._upen_k12eop2sgm_pcid._eop_counter()
            return allFields

    class _upen_k12byt2sgm_pcid(AtRegister.AtRegister):
        def name(self):
            return "Counter_Tx_APS_BYTE_To_SGMII"
    
        def description(self):
            return "Counter number of BYTE of TX APS packet to SGMII"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00021352
            
        def endAddress(self):
            return 0xffffffff

        class _byte_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "byte_counter"
            
            def description(self):
                return "Counter number of BYTE of TX APS packets to SGMII"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["byte_counter"] = _AF6CNC0022_RD_DCCK._upen_k12byt2sgm_pcid._byte_counter()
            return allFields

    class _upen_dcc_glbcnt_sgmrxeop(AtRegister.AtRegister):
        def name(self):
            return "Counter_Rx_Eop_From_SGMII_Port"
    
        def description(self):
            return "Counter of number of EOP receive from SGMII port 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12021"
            
        def startAddress(self):
            return 0x00012021
            
        def endAddress(self):
            return 0xffffffff

        class _eop_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "eop_counter"
            
            def description(self):
                return "Counter of EOP receive from SGMII port 1"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["eop_counter"] = _AF6CNC0022_RD_DCCK._upen_dcc_glbcnt_sgmrxeop._eop_counter()
            return allFields

    class _upen_dcc_glbcnt_sgmrxerr(AtRegister.AtRegister):
        def name(self):
            return "Counter_Rx_Err_From_SGMII_Port"
    
        def description(self):
            return "Counter of number of ERR receive from SGMII port 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12022"
            
        def startAddress(self):
            return 0x00012022
            
        def endAddress(self):
            return 0xffffffff

        class _err_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "err_counter"
            
            def description(self):
                return "Counter of ERR receive from SGMII port 1"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["err_counter"] = _AF6CNC0022_RD_DCCK._upen_dcc_glbcnt_sgmrxerr._err_counter()
            return allFields

    class _upen_dcc_glbcnt_sgmrxbyt(AtRegister.AtRegister):
        def name(self):
            return "Counter_Rx_Byte_From_SGMII_Port"
    
        def description(self):
            return "Counter of number of BYTE receive from SGMII port 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12023"
            
        def startAddress(self):
            return 0x00012023
            
        def endAddress(self):
            return 0xffffffff

        class _byte_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "byte_counter"
            
            def description(self):
                return "Counter of BYTE receive from SGMII port 1"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["byte_counter"] = _AF6CNC0022_RD_DCCK._upen_dcc_glbcnt_sgmrxbyt._byte_counter()
            return allFields

    class _upen_dcc_glbcnt_sgmrxfail(AtRegister.AtRegister):
        def name(self):
            return "Counter_Rx_Unknow_From_SGMII_Port"
    
        def description(self):
            return "Counter of number of Unknow Packet (not DCC packet) receive from SGMII port 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12024"
            
        def startAddress(self):
            return 0x00012024
            
        def endAddress(self):
            return 0xffffffff

        class _unk_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "unk_counter"
            
            def description(self):
                return "Counter of unknow receive packet from SGMII port 1"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["unk_counter"] = _AF6CNC0022_RD_DCCK._upen_dcc_glbcnt_sgmrxfail._unk_counter()
            return allFields

    class _upen_dcc_glbcnt_sgmrxpass(AtRegister.AtRegister):
        def name(self):
            return "Counter_Rx_Passed_From_SGMII_Port"
    
        def description(self):
            return "Counter of number of Unknow Packet (not DCC packet) receive from SGMII port 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12031"
            
        def startAddress(self):
            return 0x00012031
            
        def endAddress(self):
            return 0xffffffff

        class _pass_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pass_counter"
            
            def description(self):
                return "Counter of passed received DCC packet from SGMII port 1 .Packet is detected as DCC packet and classify into channel successfully"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pass_counter"] = _AF6CNC0022_RD_DCCK._upen_dcc_glbcnt_sgmrxpass._pass_counter()
            return allFields

    class _upen_dcc_glbcnt_sgmrxdisc(AtRegister.AtRegister):
        def name(self):
            return "Counter_Rx_Discarded_From_SGMII_Port"
    
        def description(self):
            return "Counter of number of Unknow Packet (not DCC packet) receive from SGMII port 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12033"
            
        def startAddress(self):
            return 0x00012033
            
        def endAddress(self):
            return 0xffffffff

        class _disc_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "disc_counter"
            
            def description(self):
                return "Counter of discarded received DCC packet from SGMII port 1 .Packet is detected as DCC packet but fail other conditions to classify into channel"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["disc_counter"] = _AF6CNC0022_RD_DCCK._upen_dcc_glbcnt_sgmrxdisc._disc_counter()
            return allFields

    class _upen_dcc_glbcnt_sgmtxeop(AtRegister.AtRegister):
        def name(self):
            return "Counter_Tx_Eop_To_SGMII_Port1"
    
        def description(self):
            return "Counter of number of EOP transmit to SGMII port 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x11501"
            
        def startAddress(self):
            return 0x00011501
            
        def endAddress(self):
            return 0xffffffff

        class _sgm_txglb_eop_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "sgm_txglb_eop_counter"
            
            def description(self):
                return "Counter of EOP transmit to SGMII port 1"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["sgm_txglb_eop_counter"] = _AF6CNC0022_RD_DCCK._upen_dcc_glbcnt_sgmtxeop._sgm_txglb_eop_counter()
            return allFields

    class _upen_dcc_glbcnt_sgmtxerr(AtRegister.AtRegister):
        def name(self):
            return "Counter_Tx_Err_To_SGMII_Port1"
    
        def description(self):
            return "Counter of number of ERR transmit to SGMII port 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x11502"
            
        def startAddress(self):
            return 0x00011502
            
        def endAddress(self):
            return 0xffffffff

        class _sgm_txglb_err_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "sgm_txglb_err_counter"
            
            def description(self):
                return "Counter of ERR  transmit to SGMII port 1"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["sgm_txglb_err_counter"] = _AF6CNC0022_RD_DCCK._upen_dcc_glbcnt_sgmtxerr._sgm_txglb_err_counter()
            return allFields

    class _upen_dcc_glbcnt_sgmtxbyt(AtRegister.AtRegister):
        def name(self):
            return "Counter_Tx_Byte_To_SGMII_Port1"
    
        def description(self):
            return "Counter of number of BYTE transmit to SGMII port 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x11503"
            
        def startAddress(self):
            return 0x00011503
            
        def endAddress(self):
            return 0xffffffff

        class _sgm_txglb_byte_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "sgm_txglb_byte_counter"
            
            def description(self):
                return "Counter of TX BYTE to SGMII port 1"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["sgm_txglb_byte_counter"] = _AF6CNC0022_RD_DCCK._upen_dcc_glbcnt_sgmtxbyt._sgm_txglb_byte_counter()
            return allFields

    class _upen_dcc_cnt(AtRegister.AtRegister):
        def name(self):
            return "Counter_Packet_And_Byte_Per_Channel"
    
        def description(self):
            return "Counter of DCC at different point represent by cnt_type value: {1} : counter RX bytes from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction {2} : counter RX good packets from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction {3} : counter RX error packets from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction {4} : counter RX lost packets from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction {5} : counter TX bytes from DCC TX BUFFER to SGMII port (BUF2SGM), TDM2PSN direction {6} : counter TX packets from DCC TX BUFFER to SGMII port (BUF2SGM), TDM2PSN direction {7} : counter TX error packet from DCC TX BUFFER to SGMII port (BUF2SGM), TDM2PSN direction {8} : counter RX bytes from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction {9} : counter RX good packets from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction {10}: counter RX error packets from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction {11}: counter RX lost packets from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction {12}: counter TX bytes from DCC RX BUFFER to HDLC ENCAP (BUF2ENC), PSN2TDM direction {13}: counter TX good packets from DCC RX BUFFER to HDLC ENCAP (BUF2ENC), PSN2TDM direction {14}: counter TX error packets from DCC RX BUFFER to HDLC ENCAP (BUF2ENC), PSN2TDM direction"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x13000 + $cnt_type*32 + $channelID"
            
        def startAddress(self):
            return 0x00013000
            
        def endAddress(self):
            return 0x00013fff

        class _dcc_counter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_counter"
            
            def description(self):
                return "Counter of DCC packet and byte"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_counter"] = _AF6CNC0022_RD_DCCK._upen_dcc_cnt._dcc_counter()
            return allFields

    class _upen_hdlc_locfg(AtRegister.AtRegister):
        def name(self):
            return "CONFIG HDLC LO DEC"
    
        def description(self):
            return "config HDLC ID 0-31 HDL_PATH: iaf6cci0012_lodec_core.ihdlc_cfg.imem113x.ram.ram[$CID]"
            
        def width(self):
            return 5
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x04000+ $CID"
            
        def startAddress(self):
            return 0x00004000
            
        def endAddress(self):
            return 0x0000401f

        class _cfg_scren(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "cfg_scren"
            
            def description(self):
                return "config to enable scramble, (1) is enable, (0) is disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_bitstuff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfg_bitstuff"
            
            def description(self):
                return "config to select bit stuff or byte sutff, (1) is bit stuff, (0) is byte stuff"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_fcsmsb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_fcsmsb"
            
            def description(self):
                return "config to calculate FCS from MSB or LSB, (1) is MSB, (0) is LSB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_fcsmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_fcsmode"
            
            def description(self):
                return "config to calculate FCS 32 or FCS 16, (1) is FCS 32, (0) is FCS 16"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_scren"] = _AF6CNC0022_RD_DCCK._upen_hdlc_locfg._cfg_scren()
            allFields["reserve"] = _AF6CNC0022_RD_DCCK._upen_hdlc_locfg._reserve()
            allFields["cfg_bitstuff"] = _AF6CNC0022_RD_DCCK._upen_hdlc_locfg._cfg_bitstuff()
            allFields["cfg_fcsmsb"] = _AF6CNC0022_RD_DCCK._upen_hdlc_locfg._cfg_fcsmsb()
            allFields["cfg_fcsmode"] = _AF6CNC0022_RD_DCCK._upen_hdlc_locfg._cfg_fcsmode()
            return allFields

    class _af6cci0012_lodec_core__icfg_fcslsb(AtRegister.AtRegister):
        def name(self):
            return "CONFIG HDLC BIT ORDER LO DEC"
    
        def description(self):
            return "config Bit Order"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004407
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_lsbfirst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_lsbfirst"
            
            def description(self):
                return "config to receive LSB first. Bit[31]->[0] equivalent to channel 31 -> 0 (1) is LSB first, (0) is default MSB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_lsbfirst"] = _AF6CNC0022_RD_DCCK._af6cci0012_lodec_core__icfg_fcslsb._cfg_lsbfirst()
            return allFields

    class _upen_hdlc_enc_data_lsb_first(AtRegister.AtRegister):
        def name(self):
            return "HDLC Encode Data_LSB_First"
    
        def description(self):
            return "config HDLC Data LSB first"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00030008
            
        def endAddress(self):
            return 0xffffffff

        class _encap_lsbfirst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "encap_lsbfirst"
            
            def description(self):
                return "config to transmit LSB first per channel Bit[31] -> [0]: channel 31 -> 0 (1) is LSB first, (0) is default MSB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["encap_lsbfirst"] = _AF6CNC0022_RD_DCCK._upen_hdlc_enc_data_lsb_first._encap_lsbfirst()
            return allFields

    class _upen_hdlc_enc_ctrl_reg1(AtRegister.AtRegister):
        def name(self):
            return "HDLC Encode Control Reg 1"
    
        def description(self):
            return "config HDLC Encode Control Register 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x38000+ $CID"
            
        def startAddress(self):
            return 0x00038000
            
        def endAddress(self):
            return 0x0003801f

        class _encap_screnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "encap_screnb"
            
            def description(self):
                return "Enable Scamble (1) Enable      , (0) disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encap_idlemod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "encap_idlemod"
            
            def description(self):
                return "This bit is only used in Bit Stuffing mode Used to configured IDLE Mode When it is active, the ENC engine will insert '1' pattern when the ENC is idle. Otherwise the ENC will insert FLAG '7E' pattern (1) Enable            , (0) Disabe"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encap_sabimod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "encap_sabimod"
            
            def description(self):
                return "Sabi/Protocol Field Mode (1) Field has 2 bytes , (0) field has 1 byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encap_sabiins(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "encap_sabiins"
            
            def description(self):
                return "Sabi/Protocol Field Insert Enable (1) Enable Insert     , (0) Disable Insert"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encap_ctrlins(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "encap_ctrlins"
            
            def description(self):
                return "Address/Control Field Insert Enable (1) Enable Insert     , (0) Disable Insert"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encap_fcsmod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "encap_fcsmod"
            
            def description(self):
                return "FCS Select Mode (1) 32b FCS           , (0) 16b FCS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encap_fcsins(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "encap_fcsins"
            
            def description(self):
                return "FCS Insert Enable (1) Enable Insert     , (0) Disable Insert"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encap_flgmod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "encap_flgmod"
            
            def description(self):
                return "Flag Mode (1) Minimum 2 Flag    , (0) Minimum 1 Flag"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encap_stfmod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "encap_stfmod"
            
            def description(self):
                return "Stuffing Mode (1) Bit Stuffing      , (0) Byte Stuffing"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["encap_screnb"] = _AF6CNC0022_RD_DCCK._upen_hdlc_enc_ctrl_reg1._encap_screnb()
            allFields["Reserved"] = _AF6CNC0022_RD_DCCK._upen_hdlc_enc_ctrl_reg1._Reserved()
            allFields["encap_idlemod"] = _AF6CNC0022_RD_DCCK._upen_hdlc_enc_ctrl_reg1._encap_idlemod()
            allFields["encap_sabimod"] = _AF6CNC0022_RD_DCCK._upen_hdlc_enc_ctrl_reg1._encap_sabimod()
            allFields["encap_sabiins"] = _AF6CNC0022_RD_DCCK._upen_hdlc_enc_ctrl_reg1._encap_sabiins()
            allFields["encap_ctrlins"] = _AF6CNC0022_RD_DCCK._upen_hdlc_enc_ctrl_reg1._encap_ctrlins()
            allFields["encap_fcsmod"] = _AF6CNC0022_RD_DCCK._upen_hdlc_enc_ctrl_reg1._encap_fcsmod()
            allFields["encap_fcsins"] = _AF6CNC0022_RD_DCCK._upen_hdlc_enc_ctrl_reg1._encap_fcsins()
            allFields["encap_flgmod"] = _AF6CNC0022_RD_DCCK._upen_hdlc_enc_ctrl_reg1._encap_flgmod()
            allFields["encap_stfmod"] = _AF6CNC0022_RD_DCCK._upen_hdlc_enc_ctrl_reg1._encap_stfmod()
            return allFields

    class _upen_hdlc_enc_ctrl_reg2(AtRegister.AtRegister):
        def name(self):
            return "HDLC Encode Control Reg 2"
    
        def description(self):
            return "config HDLC Encode Control Register 2 Byte Stuff Mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x39000+ $CID"
            
        def startAddress(self):
            return 0x00039000
            
        def endAddress(self):
            return 0x0003901f

        class _encap_addrval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "encap_addrval"
            
            def description(self):
                return "Address Field"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encap_ctlrval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "encap_ctlrval"
            
            def description(self):
                return "Control Field"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encap_sapival0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "encap_sapival0"
            
            def description(self):
                return "SAPI/PROTOCOL Field 1st byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _encap_sapival1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "encap_sapival1"
            
            def description(self):
                return "SAPI/PROTOCOL Field 2nd byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["encap_addrval"] = _AF6CNC0022_RD_DCCK._upen_hdlc_enc_ctrl_reg2._encap_addrval()
            allFields["encap_ctlrval"] = _AF6CNC0022_RD_DCCK._upen_hdlc_enc_ctrl_reg2._encap_ctlrval()
            allFields["encap_sapival0"] = _AF6CNC0022_RD_DCCK._upen_hdlc_enc_ctrl_reg2._encap_sapival0()
            allFields["encap_sapival1"] = _AF6CNC0022_RD_DCCK._upen_hdlc_enc_ctrl_reg2._encap_sapival1()
            return allFields

    class _upen_hdlc_enc_ctrl_reg3(AtRegister.AtRegister):
        def name(self):
            return "HDLC Encode Control Reg 3"
    
        def description(self):
            return "config HDLC Encode Control Register 3 config FCS calculation MSB bit first"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00030006
            
        def endAddress(self):
            return 0xffffffff

        class _encap_fcsmsb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "encap_fcsmsb"
            
            def description(self):
                return "config to calculate FCS from MSB or LSB, (1) is MSB, (0) is LSB Bit 31->0: channel 31->0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["encap_fcsmsb"] = _AF6CNC0022_RD_DCCK._upen_hdlc_enc_ctrl_reg3._encap_fcsmsb()
            return allFields

    class _upen_hdlc_enc_ctrl_reg4(AtRegister.AtRegister):
        def name(self):
            return "HDLC Encode Control Reg 4"
    
        def description(self):
            return "config HDLC Encode Control Register 3 config swap final FCS value"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00030007
            
        def endAddress(self):
            return 0xffffffff

        class _encap_swapfcs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "encap_swapfcs"
            
            def description(self):
                return "config to swap FCS bit. Per channel (1) enable swap , (0): disable Bit 31->0: channel 31->0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["encap_swapfcs"] = _AF6CNC0022_RD_DCCK._upen_hdlc_enc_ctrl_reg4._encap_swapfcs()
            return allFields

    class _upen_trig_encap(AtRegister.AtRegister):
        def name(self):
            return "Enable Packet Capture"
    
        def description(self):
            return "config enable capture ethernet packet."
            
        def width(self):
            return 4
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000a000
            
        def endAddress(self):
            return 0xffffffff

        class _ram_trig_fls(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "ram_trig_fls"
            
            def description(self):
                return "Trigger flush packets captured RAM Write '0' first, then write '1' to trigger flush RAM process"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _enb_cap_dec(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "enb_cap_dec"
            
            def description(self):
                return "Enable capture packets from HDLC Encap/Decapsulation. Write '0' first, then write '1' to trigger capture process"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ram_trig_fls"] = _AF6CNC0022_RD_DCCK._upen_trig_encap._ram_trig_fls()
            allFields["enb_cap_dec"] = _AF6CNC0022_RD_DCCK._upen_trig_encap._enb_cap_dec()
            return allFields

    class _upen_pktcap(AtRegister.AtRegister):
        def name(self):
            return "Captured Packet Data"
    
        def description(self):
            return "Captured packet data"
            
        def width(self):
            return 70
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x08000 + $loc"
            
        def startAddress(self):
            return 0x00008000
            
        def endAddress(self):
            return 0x000080ff

        class _cap_sop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 69
                
            def startBit(self):
                return 69
        
            def name(self):
                return "cap_sop"
            
            def description(self):
                return "Start of packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _cap_eop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 68
                
            def startBit(self):
                return 68
        
            def name(self):
                return "cap_eop"
            
            def description(self):
                return "End   of packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _cap_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 67
                
            def startBit(self):
                return 67
        
            def name(self):
                return "cap_err"
            
            def description(self):
                return "Error of packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _cap_nob(AtRegister.AtRegisterField):
            def stopBit(self):
                return 66
                
            def startBit(self):
                return 64
        
            def name(self):
                return "cap_nob"
            
            def description(self):
                return "Number of valid bytes in 8-bytes data captured"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _cap_dat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cap_dat"
            
            def description(self):
                return "packet data captured"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cap_sop"] = _AF6CNC0022_RD_DCCK._upen_pktcap._cap_sop()
            allFields["cap_eop"] = _AF6CNC0022_RD_DCCK._upen_pktcap._cap_eop()
            allFields["cap_err"] = _AF6CNC0022_RD_DCCK._upen_pktcap._cap_err()
            allFields["cap_nob"] = _AF6CNC0022_RD_DCCK._upen_pktcap._cap_nob()
            allFields["cap_dat"] = _AF6CNC0022_RD_DCCK._upen_pktcap._cap_dat()
            return allFields

    class _upen_k12acc(AtRegister.AtRegister):
        def name(self):
            return "APS_Acc_Received_Kbyte_Value_Per_Channel"
    
        def description(self):
            return "The register provides received Kbyte of each channel ID"
            
        def width(self):
            return 24
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x22800 + $channelid"
            
        def startAddress(self):
            return 0x00022800
            
        def endAddress(self):
            return 0x000228ff

        class _Kbyte_Value(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Kbyte_Value"
            
            def description(self):
                return "Received Kbyte value of each channel [23:16]: K1 Byte [15:08]: K2 Byte [07:00]: Extend Kbyte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Kbyte_Value"] = _AF6CNC0022_RD_DCCK._upen_k12acc._Kbyte_Value()
            return allFields

    class _upen_genmon_reqint(AtRegister.AtRegister):
        def name(self):
            return "DDC_Test_Sdh_Req_Interval_Configuration"
    
        def description(self):
            return "The register is used to configure time interval to generate fake SDH request signal"
            
        def width(self):
            return 29
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _Timer_Enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "Timer_Enable"
            
            def description(self):
                return "Enable generation of fake request SDH signal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Timer_Value(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Timer_Value"
            
            def description(self):
                return "Counter of number of clk 155MHz between fake request SDH signal. This counter is used make a delay interval between 2 consecutive SDH requests generated by DCC GENERATOR. For example: to create a delay of 125us between SDH request signals, with a clock 155Mz, the value of counter to be configed to this field is (125*10^3)ns/(10^3/155)ns = 125*155 = 19375 = 0x4BAF"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Timer_Enable"] = _AF6CNC0022_RD_DCCK._upen_genmon_reqint._Timer_Enable()
            allFields["Timer_Value"] = _AF6CNC0022_RD_DCCK._upen_genmon_reqint._Timer_Value()
            return allFields

    class _upen_dcc_cfg_testgen_hdr(AtRegister.AtRegister):
        def name(self):
            return "DDC_Config_Test_Gen_Header_Per_Channel"
    
        def description(self):
            return "The register provides data for configuration of VID and MAC DA Header of each channel ID"
            
        def width(self):
            return 20
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0C100 + $channelid"
            
        def startAddress(self):
            return 0x0000c100
            
        def endAddress(self):
            return 0x0000c120

        class _HDR_VID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 8
        
            def name(self):
                return "HDR_VID"
            
            def description(self):
                return "12b of VID value in header of Generated Packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAC_DA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MAC_DA"
            
            def description(self):
                return "Bit [7:0] of MAC DA value [47:0] of generated Packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HDR_VID"] = _AF6CNC0022_RD_DCCK._upen_dcc_cfg_testgen_hdr._HDR_VID()
            allFields["MAC_DA"] = _AF6CNC0022_RD_DCCK._upen_dcc_cfg_testgen_hdr._MAC_DA()
            return allFields

    class _upen_dcc_cfg_testgen_mod(AtRegister.AtRegister):
        def name(self):
            return "DDC_Config_Test_Gen_Mode_Per_Channel"
    
        def description(self):
            return "The register provides data for configuration of generation mode of each channel ID"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0C200 + $channelid"
            
        def startAddress(self):
            return 0x0000c200
            
        def endAddress(self):
            return 0x0000c220

        class _gen_payload_mod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 30
        
            def name(self):
                return "gen_payload_mod"
            
            def description(self):
                return "Modes of generated packet payload:"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _gen_len_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 28
        
            def name(self):
                return "gen_len_mode"
            
            def description(self):
                return "Modes of generated packet length :"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _gen_fix_pattern(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 20
        
            def name(self):
                return "gen_fix_pattern"
            
            def description(self):
                return "8b fix pattern payload if mode \"fix payload\" is used"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _gen_number_of_packet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "gen_number_of_packet"
            
            def description(self):
                return "Number of packets the GEN generates for each channelID in gen burst mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["gen_payload_mod"] = _AF6CNC0022_RD_DCCK._upen_dcc_cfg_testgen_mod._gen_payload_mod()
            allFields["gen_len_mode"] = _AF6CNC0022_RD_DCCK._upen_dcc_cfg_testgen_mod._gen_len_mode()
            allFields["gen_fix_pattern"] = _AF6CNC0022_RD_DCCK._upen_dcc_cfg_testgen_mod._gen_fix_pattern()
            allFields["gen_number_of_packet"] = _AF6CNC0022_RD_DCCK._upen_dcc_cfg_testgen_mod._gen_number_of_packet()
            return allFields

    class _upen_dcc_cfg_testgen_enacid(AtRegister.AtRegister):
        def name(self):
            return "DDC_Config_Test_Gen_Global_Gen_Enable_Channel"
    
        def description(self):
            return "The register provides configuration to enable Channel to generate DCC packets"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000c000
            
        def endAddress(self):
            return 0xffffffff

        class _Channel_enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Channel_enable"
            
            def description(self):
                return "Enable Channel to generate DCC packets. Bit[31:0] <-> Channel 31->0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Channel_enable"] = _AF6CNC0022_RD_DCCK._upen_dcc_cfg_testgen_enacid._Channel_enable()
            return allFields

    class _upen_dcc_cfg_testgen_glb_gen_mode(AtRegister.AtRegister):
        def name(self):
            return "DDC_Config_Test_Gen_Global_Gen_Mode"
    
        def description(self):
            return "The register provides global configuration of GEN mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000c001
            
        def endAddress(self):
            return 0xffffffff

        class _gen_force_err_len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "gen_force_err_len"
            
            def description(self):
                return "Create wrong packet's length fiedl generated packets"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _gen_force_err_fcs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "gen_force_err_fcs"
            
            def description(self):
                return "Create wrong FCS field in generated packets"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _gen_force_err_dat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "gen_force_err_dat"
            
            def description(self):
                return "Create wrong data value in generated packets"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _gen_force_err_seq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "gen_force_err_seq"
            
            def description(self):
                return "Create wrong Sequence field in generated packets"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _gen_force_err_vcg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "gen_force_err_vcg"
            
            def description(self):
                return "Create wrong VCG field in generated packets"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _gen_burst_mod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "gen_burst_mod"
            
            def description(self):
                return "Gen each Channel a number of packet as configured in \"Gen mode per channel\" resgister"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _gen_conti_mod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "gen_conti_mod"
            
            def description(self):
                return "Gen packet forever without stopping"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["gen_force_err_len"] = _AF6CNC0022_RD_DCCK._upen_dcc_cfg_testgen_glb_gen_mode._gen_force_err_len()
            allFields["gen_force_err_fcs"] = _AF6CNC0022_RD_DCCK._upen_dcc_cfg_testgen_glb_gen_mode._gen_force_err_fcs()
            allFields["gen_force_err_dat"] = _AF6CNC0022_RD_DCCK._upen_dcc_cfg_testgen_glb_gen_mode._gen_force_err_dat()
            allFields["gen_force_err_seq"] = _AF6CNC0022_RD_DCCK._upen_dcc_cfg_testgen_glb_gen_mode._gen_force_err_seq()
            allFields["gen_force_err_vcg"] = _AF6CNC0022_RD_DCCK._upen_dcc_cfg_testgen_glb_gen_mode._gen_force_err_vcg()
            allFields["gen_burst_mod"] = _AF6CNC0022_RD_DCCK._upen_dcc_cfg_testgen_glb_gen_mode._gen_burst_mod()
            allFields["gen_conti_mod"] = _AF6CNC0022_RD_DCCK._upen_dcc_cfg_testgen_glb_gen_mode._gen_conti_mod()
            return allFields

    class _upen_dcc_cfg_testgen_glb_length(AtRegister.AtRegister):
        def name(self):
            return "DDC_Config_Test_Gen_Global_Packet_Length"
    
        def description(self):
            return "The register provides configuration min length max length of generated Packets"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000c002
            
        def endAddress(self):
            return 0xffffffff

        class _gen_max_length(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "gen_max_length"
            
            def description(self):
                return "Maximum length of generated packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _gen_min_length(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "gen_min_length"
            
            def description(self):
                return "Minimum length of generated packet, also used for fix length packets in case fix length mode is used"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["gen_max_length"] = _AF6CNC0022_RD_DCCK._upen_dcc_cfg_testgen_glb_length._gen_max_length()
            allFields["gen_min_length"] = _AF6CNC0022_RD_DCCK._upen_dcc_cfg_testgen_glb_length._gen_min_length()
            return allFields

    class _upen_dcc_cfg_testgen_glb_gen_interval(AtRegister.AtRegister):
        def name(self):
            return "DDC_Config_Test_Gen_Global_Gen_Interval"
    
        def description(self):
            return "The register provides configuration for packet generating interval"
            
        def width(self):
            return 29
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000c003
            
        def endAddress(self):
            return 0xffffffff

        class _Gen_Intv_Enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "Gen_Intv_Enable"
            
            def description(self):
                return "Enable using this interval value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Gen_Intv_Value(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Gen_Intv_Value"
            
            def description(self):
                return "Counter of number of clk 155MHz between packet generation. This counter is used make a delay interval between 2 consecutive packet generation. For example: to create a delay of 125us between 2 packet generating, with a clock 155Mz, the value of counter to be configed to this field is (125*10^3)ns/(10^3/155)ns = 125*155 = 19375 = 0x4BAF"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Gen_Intv_Enable"] = _AF6CNC0022_RD_DCCK._upen_dcc_cfg_testgen_glb_gen_interval._Gen_Intv_Enable()
            allFields["Gen_Intv_Value"] = _AF6CNC0022_RD_DCCK._upen_dcc_cfg_testgen_glb_gen_interval._Gen_Intv_Value()
            return allFields

    class _upen_mon_godpkt(AtRegister.AtRegister):
        def name(self):
            return "DDC_Test_Mon_Good_Packet_Counter"
    
        def description(self):
            return "Counter of receive good packet"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0E100 + $channelid"
            
        def startAddress(self):
            return 0x0000e100
            
        def endAddress(self):
            return 0x0000e120

        class _dcc_test_mon_good_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_test_mon_good_cnt"
            
            def description(self):
                return "Counter of Receive Good Packet from TEST GEN"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_test_mon_good_cnt"] = _AF6CNC0022_RD_DCCK._upen_mon_godpkt._dcc_test_mon_good_cnt()
            return allFields

    class _upen_mon_errpkt(AtRegister.AtRegister):
        def name(self):
            return "DDC_Test_Mon_Error_Data_Packet_Counter"
    
        def description(self):
            return "Counter of received packets has error data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0E200 + $channelid"
            
        def startAddress(self):
            return 0x0000e200
            
        def endAddress(self):
            return 0x0000e220

        class _dcc_test_mon_errdat_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_test_mon_errdat_cnt"
            
            def description(self):
                return "Counter of Receive Error Data Packet from TEST GEN"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_test_mon_errdat_cnt"] = _AF6CNC0022_RD_DCCK._upen_mon_errpkt._dcc_test_mon_errdat_cnt()
            return allFields

    class _upen_mon_errvcg(AtRegister.AtRegister):
        def name(self):
            return "DDC_Test_Mon_Error_VCG_Packet_Counter"
    
        def description(self):
            return "Counter of received packet has wrong VCG value"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0E300 + $channelid"
            
        def startAddress(self):
            return 0x0000e300
            
        def endAddress(self):
            return 0x0000e320

        class _dcc_test_mon_errvcg_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_test_mon_errvcg_cnt"
            
            def description(self):
                return "Counter of Receive Error VCG Packet from TEST GEN"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_test_mon_errvcg_cnt"] = _AF6CNC0022_RD_DCCK._upen_mon_errvcg._dcc_test_mon_errvcg_cnt()
            return allFields

    class _upen_mon_errseq(AtRegister.AtRegister):
        def name(self):
            return "DDC_Test_Mon_Error_SEQ_Packet_Counter"
    
        def description(self):
            return "Counter of received packet has wrong Sequence value"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0E400 + $channelid"
            
        def startAddress(self):
            return 0x0000e400
            
        def endAddress(self):
            return 0x0000e420

        class _dcc_test_mon_errseq_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_test_mon_errseq_cnt"
            
            def description(self):
                return "Counter of Receive Error Sequence Packet from TEST GEN"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_test_mon_errseq_cnt"] = _AF6CNC0022_RD_DCCK._upen_mon_errseq._dcc_test_mon_errseq_cnt()
            return allFields

    class _upen_mon_errfcs(AtRegister.AtRegister):
        def name(self):
            return "DDC_Test_Mon_Error_FCS_Packet_Counter"
    
        def description(self):
            return "Counter of received packet has wrong FCS value"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0E500 + $channelid"
            
        def startAddress(self):
            return 0x0000e500
            
        def endAddress(self):
            return 0x0000e520

        class _dcc_test_mon_errfcs_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_test_mon_errfcs_cnt"
            
            def description(self):
                return "Counter of Receive Error FCS Packet from TEST GEN"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_test_mon_errfcs_cnt"] = _AF6CNC0022_RD_DCCK._upen_mon_errfcs._dcc_test_mon_errfcs_cnt()
            return allFields

    class _upen_mon_abrpkt(AtRegister.AtRegister):
        def name(self):
            return "DDC_Test_Mon_Abort_VCG_Packet_Counter"
    
        def description(self):
            return "Counter of received packet has been abbort due to wrong length information"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0E600 + $channelid"
            
        def startAddress(self):
            return 0x0000e600
            
        def endAddress(self):
            return 0x0000e620

        class _dcc_test_mon_abrpkt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dcc_test_mon_abrpkt_cnt"
            
            def description(self):
                return "Counter of Abbort Packet from TEST GEN"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dcc_test_mon_abrpkt_cnt"] = _AF6CNC0022_RD_DCCK._upen_mon_abrpkt._dcc_test_mon_abrpkt_cnt()
            return allFields

    class _af6ces10grtl_dccpro__upen_txbuf_stk_err(AtRegister.AtRegister):
        def name(self):
            return "DCC_OCN2ETH_Debug_Stk_Reg1"
    
        def description(self):
            return "The register provides Alarm Related to HDLC Length Error (OCN2ETH Direction)"
            
        def width(self):
            return 96
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x10008"
            
        def startAddress(self):
            return 0x00010008
            
        def endAddress(self):
            return 0xffffffff

        class _ffq_ful(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "ffq_ful"
            
            def description(self):
                return "Fifo contain packet information is full, some packets will be lossed"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ffq_ful"] = _AF6CNC0022_RD_DCCK._af6ces10grtl_dccpro__upen_txbuf_stk_err._ffq_ful()
            return allFields

    class _af6ces10grtl_dccpro__upen_rxbuf_stk_err1(AtRegister.AtRegister):
        def name(self):
            return "DCC_ETH2OCN_Debug_Stk_Reg1"
    
        def description(self):
            return "The register provides Alarm Related to HDLC Length Error (OCN2ETH Direction)"
            
        def width(self):
            return 96
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x10009"
            
        def startAddress(self):
            return 0x00010009
            
        def endAddress(self):
            return 0xffffffff

        class _rambuf_ful(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "rambuf_ful"
            
            def description(self):
                return "Buffer contain data per channelID is full"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ffinf_ful(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "ffinf_ful"
            
            def description(self):
                return "Fifo contain packet information is full, some packets will be lossed"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rambuf_ful"] = _AF6CNC0022_RD_DCCK._af6ces10grtl_dccpro__upen_rxbuf_stk_err1._rambuf_ful()
            allFields["ffinf_ful"] = _AF6CNC0022_RD_DCCK._af6ces10grtl_dccpro__upen_rxbuf_stk_err1._ffinf_ful()
            return allFields

    class _af6ces10grtl_dccpro__upen_cfg_maxlen(AtRegister.AtRegister):
        def name(self):
            return "DCC_Config_Maximum_Allowed_Length"
    
        def description(self):
            return "The register provides configuration for maximum allowed Frame"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x10004"
            
        def startAddress(self):
            return 0x00010004
            
        def endAddress(self):
            return 0xffffffff

        class _OCN2ETH_maxlen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 12
        
            def name(self):
                return "OCN2ETH_maxlen"
            
            def description(self):
                return "Maximum allowed length OCN2ETH direction"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ETH2OCN_maxlen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ETH2OCN_maxlen"
            
            def description(self):
                return "Maximum allowed length ETH2OCN direction"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["OCN2ETH_maxlen"] = _AF6CNC0022_RD_DCCK._af6ces10grtl_dccpro__upen_cfg_maxlen._OCN2ETH_maxlen()
            allFields["ETH2OCN_maxlen"] = _AF6CNC0022_RD_DCCK._af6ces10grtl_dccpro__upen_cfg_maxlen._ETH2OCN_maxlen()
            return allFields

    class _af6ces10grtldcc_rx__upen_dispkt_pcid(AtRegister.AtRegister):
        def name(self):
            return "Counter_Rx_Drop_Packet_Per_Channel"
    
        def description(self):
            return "Counter of number of discard packet per channel due too full"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12100 +  ChannelID"
            
        def startAddress(self):
            return 0x00012100
            
        def endAddress(self):
            return 0x0001211f

        class _eth2ocn_disc_pkt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "eth2ocn_disc_pkt_cnt"
            
            def description(self):
                return "Counter of discarded packet due too buffer full"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["eth2ocn_disc_pkt_cnt"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcc_rx__upen_dispkt_pcid._eth2ocn_disc_pkt_cnt()
            return allFields

    class _af6ces10grtldcc_rx__upen_en_benchmark(AtRegister.AtRegister):
        def name(self):
            return "Enable_Count_Number_Of_Bytes_Per_Interval"
    
        def description(self):
            return "Counter of number of discard packet per channel due too full"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12800"
            
        def startAddress(self):
            return 0x00012800
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_trigclr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "cfg_trigclr"
            
            def description(self):
                return "Trigger to Restart Start Address of \"Number of Bytes (NOB) per Interval\" Dump RAM to 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_en_benlid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "cfg_en_benlid"
            
            def description(self):
                return "Enable to Dump NOB per channel ID. When this bit is set, only data bytes of configed channel is counted"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_en_benmar(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "cfg_en_benmar"
            
            def description(self):
                return "Enable to Dump NOB."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_benlid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_benlid"
            
            def description(self):
                return "Channel ID to dump NOB. Use with bit [9] above"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_trigclr"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcc_rx__upen_en_benchmark._cfg_trigclr()
            allFields["cfg_en_benlid"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcc_rx__upen_en_benchmark._cfg_en_benlid()
            allFields["cfg_en_benmar"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcc_rx__upen_en_benchmark._cfg_en_benmar()
            allFields["cfg_benlid"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcc_rx__upen_en_benchmark._cfg_benlid()
            return allFields

    class _af6ces10grtldcc_rx__upen_cfg_numtik(AtRegister.AtRegister):
        def name(self):
            return "Cfg_Interval_To_Dump_NOB"
    
        def description(self):
            return "Configure the Interval to Dump NOB Counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x12801"
            
        def startAddress(self):
            return 0x00012801
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_numtik(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_numtik"
            
            def description(self):
                return "Number of clks 125Mhz represent the interval window that the NOB will be dumped over. For example, if we want to count number of byte in an interval of 1second window, then this reg will be configed 32'h7735944 (32'd125*10^6)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_numtik"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcc_rx__upen_cfg_numtik._cfg_numtik()
            return allFields

    class _af6ces10grtldcc_rx__upen_bytpsec_cnt(AtRegister.AtRegister):
        def name(self):
            return "NOB_Counter_In_Configed_Interval"
    
        def description(self):
            return "Counter of number of discard packet per channel due too full"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x12A00 + dump_adr"
            
        def startAddress(self):
            return 0x00012a00
            
        def endAddress(self):
            return 0x00012bff

        class _Numb_of_Byte_Per_Interval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Numb_of_Byte_Per_Interval"
            
            def description(self):
                return "Number of Bytes Counted over the configed interval"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Numb_of_Byte_Per_Interval"] = _AF6CNC0022_RD_DCCK._af6ces10grtldcc_rx__upen_bytpsec_cnt._Numb_of_Byte_Per_Interval()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0022_RD_PDH_v3(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["glb"] = _AF6CNC0022_RD_PDH_v3._glb()
        allRegisters["stsvt_demap_ctrl"] = _AF6CNC0022_RD_PDH_v3._stsvt_demap_ctrl()
        allRegisters["stsvt_demap_hw_stat"] = _AF6CNC0022_RD_PDH_v3._stsvt_demap_hw_stat()
        allRegisters["rx_stsvc_payload_ctrl"] = _AF6CNC0022_RD_PDH_v3._rx_stsvc_payload_ctrl()
        allRegisters["rxm23e23_ctrl"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_ctrl()
        allRegisters["rxm23e23_hw_stat"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_hw_stat()
        allRegisters["rx_de3_oh_ctrl"] = _AF6CNC0022_RD_PDH_v3._rx_de3_oh_ctrl()
        allRegisters["upen_invert_ssm"] = _AF6CNC0022_RD_PDH_v3._upen_invert_ssm()
        allRegisters["upen_sta_ssm"] = _AF6CNC0022_RD_PDH_v3._upen_sta_ssm()
        allRegisters["rxde3_oh_thrsh_cfg"] = _AF6CNC0022_RD_PDH_v3._rxde3_oh_thrsh_cfg()
        allRegisters["rxm23e23_rei_thrsh_cfg"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_rei_thrsh_cfg()
        allRegisters["rxm23e23_fbe_thrsh_cfg"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_fbe_thrsh_cfg()
        allRegisters["rxm23e23_parity_thrsh_cfg"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_parity_thrsh_cfg()
        allRegisters["rxm23e23_cbit_parity_thrsh_cfg"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_cbit_parity_thrsh_cfg()
        allRegisters["rxm23e23_rei_cnt"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_rei_cnt()
        allRegisters["rxm23e23_fbe_cnt"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_fbe_cnt()
        allRegisters["rxm23e23_parity_cnt"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_parity_cnt()
        allRegisters["rxm23e23_cbit_parity_cnt"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_cbit_parity_cnt()
        allRegisters["rx_de3_feac_buffer"] = _AF6CNC0022_RD_PDH_v3._rx_de3_feac_buffer()
        allRegisters["rxm23e23_per_chn_intr_cfg"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_cfg()
        allRegisters["rxm23e23_per_chn_intr_stat"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_stat()
        allRegisters["rxm23e23_per_chn_curr_stat"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_curr_stat()
        allRegisters["RxM23E23_Framer_per_chn_intr_or_stat"] = _AF6CNC0022_RD_PDH_v3._RxM23E23_Framer_per_chn_intr_or_stat()
        allRegisters["RxM23E23_Framer_per_grp_intr_or_stat"] = _AF6CNC0022_RD_PDH_v3._RxM23E23_Framer_per_grp_intr_or_stat()
        allRegisters["RxM23E23_Framer_per_Group_intr_en_ctrl"] = _AF6CNC0022_RD_PDH_v3._RxM23E23_Framer_per_Group_intr_en_ctrl()
        allRegisters["rxde3_oh_dlk_swap_bit_cfg"] = _AF6CNC0022_RD_PDH_v3._rxde3_oh_dlk_swap_bit_cfg()
        allRegisters["rxm12e12_ctrl"] = _AF6CNC0022_RD_PDH_v3._rxm12e12_ctrl()
        allRegisters["rxm12e12_stsvc_payload_ctrl"] = _AF6CNC0022_RD_PDH_v3._rxm12e12_stsvc_payload_ctrl()
        allRegisters["rxm12e12_hw_stat"] = _AF6CNC0022_RD_PDH_v3._rxm12e12_hw_stat()
        allRegisters["RxM12E12_per_chn_intr_cfg"] = _AF6CNC0022_RD_PDH_v3._RxM12E12_per_chn_intr_cfg()
        allRegisters["RxM12E12_per_chn_intr_stat"] = _AF6CNC0022_RD_PDH_v3._RxM12E12_per_chn_intr_stat()
        allRegisters["RxM12E12_per_chn_curr_stat"] = _AF6CNC0022_RD_PDH_v3._RxM12E12_per_chn_curr_stat()
        allRegisters["RxM12E12_Framer_per_chn_intr_or_stat"] = _AF6CNC0022_RD_PDH_v3._RxM12E12_Framer_per_chn_intr_or_stat()
        allRegisters["RxM12E12_Framer_per_grp_intr_or_stat"] = _AF6CNC0022_RD_PDH_v3._RxM12E12_Framer_per_grp_intr_or_stat()
        allRegisters["RxM12E12_Framer_per_Group_intr_en_ctrl"] = _AF6CNC0022_RD_PDH_v3._RxM12E12_Framer_per_Group_intr_en_ctrl()
        allRegisters["dej1_fbe_thrsh_cfg"] = _AF6CNC0022_RD_PDH_v3._dej1_fbe_thrsh_cfg()
        allRegisters["dej1_rx_framer_ctrl"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_ctrl()
        allRegisters["dej1_rx_framer_hw_stat"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_hw_stat()
        allRegisters["dej1_rx_framer_fbe_cnt"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_fbe_cnt()
        allRegisters["dej1_rx_framer_crcerr_cnt"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_crcerr_cnt()
        allRegisters["dej1_rx_framer_rei_cnt"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_rei_cnt()
        allRegisters["dej1_rx_framer_per_chn_intr_en_ctrl"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_en_ctrl()
        allRegisters["dej1_rx_framer_per_chn_intr_stat"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_stat()
        allRegisters["dej1_rx_framer_per_chn_curr_stat"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_curr_stat()
        allRegisters["dej1_rx_framer_per_chn_intr_or_stat"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_or_stat()
        allRegisters["dej1_rx_framer_per_stsvc_intr_or_stat"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_stsvc_intr_or_stat()
        allRegisters["dej1_rx_framer_per_stsvc_intr_en_ctrl"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_stsvc_intr_en_ctrl()
        allRegisters["dej1_rx_framer_per_grp_intr_or_stat"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_grp_intr_or_stat()
        allRegisters["stsvt_map_ctrl"] = _AF6CNC0022_RD_PDH_v3._stsvt_map_ctrl()
        allRegisters["stsvt_map_hw_stat"] = _AF6CNC0022_RD_PDH_v3._stsvt_map_hw_stat()
        allRegisters["dej1_tx_framer_ctrl"] = _AF6CNC0022_RD_PDH_v3._dej1_tx_framer_ctrl()
        allRegisters["dej1_tx_framer_stat"] = _AF6CNC0022_RD_PDH_v3._dej1_tx_framer_stat()
        allRegisters["dej1_tx_framer_sign_insertion_ctrl"] = _AF6CNC0022_RD_PDH_v3._dej1_tx_framer_sign_insertion_ctrl()
        allRegisters["dej1_errins_en_cfg"] = _AF6CNC0022_RD_PDH_v3._dej1_errins_en_cfg()
        allRegisters["dej1_errins_thr_cfg"] = _AF6CNC0022_RD_PDH_v3._dej1_errins_thr_cfg()
        allRegisters["dej1_tx_framer_dlk_ins_bom_ctrl"] = _AF6CNC0022_RD_PDH_v3._dej1_tx_framer_dlk_ins_bom_ctrl()
        allRegisters["txm23e23_ctrl"] = _AF6CNC0022_RD_PDH_v3._txm23e23_ctrl()
        allRegisters["txm23e23_ctrl2"] = _AF6CNC0022_RD_PDH_v3._txm23e23_ctrl2()
        allRegisters["txm23e23_hw_stat"] = _AF6CNC0022_RD_PDH_v3._txm23e23_hw_stat()
        allRegisters["txm12e12_ctrl"] = _AF6CNC0022_RD_PDH_v3._txm12e12_ctrl()
        allRegisters["txm12e12_hw_stat"] = _AF6CNC0022_RD_PDH_v3._txm12e12_hw_stat()
        allRegisters["upen_lim_err"] = _AF6CNC0022_RD_PDH_v3._upen_lim_err()
        allRegisters["upen_lim_mat"] = _AF6CNC0022_RD_PDH_v3._upen_lim_mat()
        allRegisters["upen_th_fdl_mat"] = _AF6CNC0022_RD_PDH_v3._upen_th_fdl_mat()
        allRegisters["upen_th_err"] = _AF6CNC0022_RD_PDH_v3._upen_th_err()
        allRegisters["upen_info"] = _AF6CNC0022_RD_PDH_v3._upen_info()
        allRegisters["upen_fdlpat"] = _AF6CNC0022_RD_PDH_v3._upen_fdlpat()
        allRegisters["upen_fdl_info"] = _AF6CNC0022_RD_PDH_v3._upen_fdl_info()
        allRegisters["upen_th_stt_int"] = _AF6CNC0022_RD_PDH_v3._upen_th_stt_int()
        allRegisters["RAM_Parity_Force_Control"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Force_Control()
        allRegisters["RAM_Parity_Disable_Control"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Disable_Control()
        allRegisters["RAM_Parity_Error_Sticky"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Error_Sticky()
        return allRegisters

    class _glb(AtRegister.AtRegister):
        def name(self):
            return "PDH Global Registers"
    
        def description(self):
            return "This is the global configuration register for the PDH"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x00000000

        class _PDHE13IdConv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PDHE13IdConv"
            
            def description(self):
                return "Set 1 to enable ID conversion of the E1 in an E3 as following ID in an E3 (bit 4:0) ID at Data Map (bit 4:0) 5'd0                  5'd0 5'd1                  5'd1 5'd2                  5'd2 5'd3                  5'd4 5'd4                  5'd5 5'd5                  5'd6 5'd6                  5'd8 5'd7                  5'd9 5'd8                  5'd10 5'd9                  5'd12 5'd10                 5'd13 5'd11                 5'd14 5'd12                 5'd16 5'd13                 5'd17 5'd14                 5'd18 5'd15                 5'd20"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHFullSonetSDH(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PDHFullSonetSDH"
            
            def description(self):
                return "Set 1 to enable full SONET/SDH mode (no traffic on PDH interface)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHFullLineLoop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PDHFullLineLoop"
            
            def description(self):
                return "Set 1 to enable full Line Loop Back at Data map side"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHFullPayLoop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PDHFullPayLoop"
            
            def description(self):
                return "Set 1 to enable full Payload Loop back at Data map side"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHFullPDHBus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PDHFullPDHBus"
            
            def description(self):
                return "Set 1 to enable Full traffic on PDH Bus"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHParallelPDHMd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PDHParallelPDHMd"
            
            def description(self):
                return "Set 1 to enable Parallel PDH Bus mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSaturation(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PDHSaturation"
            
            def description(self):
                return "Set 1 to enable Saturation mode of all PDH counters. Set 0 for Roll-Over mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHReservedBit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PDHReservedBit"
            
            def description(self):
                return "Transmit Reserved Bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHStufftBit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PDHStufftBit"
            
            def description(self):
                return "Transmit Stuffing Bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHNoIDConv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHNoIDConv"
            
            def description(self):
                return "Set 1 to enable no ID convert at PDH Block"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHE13IdConv"] = _AF6CNC0022_RD_PDH_v3._glb._PDHE13IdConv()
            allFields["PDHFullSonetSDH"] = _AF6CNC0022_RD_PDH_v3._glb._PDHFullSonetSDH()
            allFields["PDHFullLineLoop"] = _AF6CNC0022_RD_PDH_v3._glb._PDHFullLineLoop()
            allFields["PDHFullPayLoop"] = _AF6CNC0022_RD_PDH_v3._glb._PDHFullPayLoop()
            allFields["PDHFullPDHBus"] = _AF6CNC0022_RD_PDH_v3._glb._PDHFullPDHBus()
            allFields["PDHParallelPDHMd"] = _AF6CNC0022_RD_PDH_v3._glb._PDHParallelPDHMd()
            allFields["PDHSaturation"] = _AF6CNC0022_RD_PDH_v3._glb._PDHSaturation()
            allFields["PDHReservedBit"] = _AF6CNC0022_RD_PDH_v3._glb._PDHReservedBit()
            allFields["PDHStufftBit"] = _AF6CNC0022_RD_PDH_v3._glb._PDHStufftBit()
            allFields["PDHNoIDConv"] = _AF6CNC0022_RD_PDH_v3._glb._PDHNoIDConv()
            return allFields

    class _stsvt_demap_ctrl(AtRegister.AtRegister):
        def name(self):
            return "STS/VT Demap Control"
    
        def description(self):
            return "The STS/VT Demap Control is use to configure for per channel STS/VT demap operation."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00024000 +  32*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x00024000
            
        def endAddress(self):
            return 0x000247ff

        class _Pdhoversdhmd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "Pdhoversdhmd"
            
            def description(self):
                return "Set 1 to PDH LIU over OCN"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsVtDemapFrcAis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "StsVtDemapFrcAis"
            
            def description(self):
                return "This bit is set to 1 to force the AIS to downstream data. 1: force AIS. 0: not force AIS."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsVtDemapAutoAis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "StsVtDemapAutoAis"
            
            def description(self):
                return "This bit is set to 1 to automatically generate AIS to downstream data whenever the high-order STS/VT is in AIS. 1: auto AIS generation. 0: not auto AIS generation"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsVtDemapCepMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "StsVtDemapCepMode"
            
            def description(self):
                return "STS/VC CEP fractional mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsVtDemapFrmtype(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "StsVtDemapFrmtype"
            
            def description(self):
                return "STS/VT Frmtype configure"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsVtDemapSigtype(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "StsVtDemapSigtype"
            
            def description(self):
                return "STS/VT Sigtype configure Sigtype[3:0] Frmtype[1:0] CepMode Operation Mode 0 x x VT/TU to DS1 Demap 1 x x VT/TU to E1 Demap 2 x 0 STS1/VC3 to DS3 Demap 2 x 1 TUG3/TU3 to DS3 Demap 3 x 0 STS1/VC3 to E3 Demap 3 x 1 TUG3/TU3 to E3 Demap 4 0 x Packet over VT1.5/TU11 4 1 x Reserved 4 2 x Reserved 4 3 x VT1.5/TU11 Basic CEP 5 0 x Packet over VT2/TU12 5 1 x Reserved 5 2 x Reserved 5 3 x VT2/TU12 Basic CEP 6 x x Reserved 7 x x Reserved 8 0 x Packet over STS1/VC3 8 1 0 STS1/VC3 Fractional CEP carrying VT2/TU12 8 1 1 STS1/VC3 Fractional CEP carrying VT15/TU11 8 2 0 STS1/VC3 Fractional CEP carrying E3 8 2 1 STS1/VC3 Fractional CEP carrying DS3 8 3 x STS1/VC3 Basic CEP 9 0 x Packet over STS3c/VC4 9 1 0 STS3c/VC4 Fractional CEP carrying VT2/TU12 9 1 1 STS3c/VC4 Fractional CEP carrying VT15/TU11 9 2 0 STS3c/VC4 Fractional CEP carrying E3 9 2 1 STS3c/VC4 Fractional CEP carrying DS3 9 3 x STS3c/VC4 Basic CEP 10 3 x STS12c/VC4_4c Basic CEP 11 x x Reserved 12 x x Packet over TU3 13 x x Reserved 14 x x Reserved 15 x x Disable STS/VC/VT/TU/DS1/E1/DS3/E3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Pdhoversdhmd"] = _AF6CNC0022_RD_PDH_v3._stsvt_demap_ctrl._Pdhoversdhmd()
            allFields["StsVtDemapFrcAis"] = _AF6CNC0022_RD_PDH_v3._stsvt_demap_ctrl._StsVtDemapFrcAis()
            allFields["StsVtDemapAutoAis"] = _AF6CNC0022_RD_PDH_v3._stsvt_demap_ctrl._StsVtDemapAutoAis()
            allFields["StsVtDemapCepMode"] = _AF6CNC0022_RD_PDH_v3._stsvt_demap_ctrl._StsVtDemapCepMode()
            allFields["StsVtDemapFrmtype"] = _AF6CNC0022_RD_PDH_v3._stsvt_demap_ctrl._StsVtDemapFrmtype()
            allFields["StsVtDemapSigtype"] = _AF6CNC0022_RD_PDH_v3._stsvt_demap_ctrl._StsVtDemapSigtype()
            return allFields

    class _stsvt_demap_hw_stat(AtRegister.AtRegister):
        def name(self):
            return "STS/VT Demap HW Status"
    
        def description(self):
            return "for HW debug only"
            
        def width(self):
            return 27
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00024800
            
        def endAddress(self):
            return 0x00024fff

        class _STSVTDemapStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 0
        
            def name(self):
                return "STSVTDemapStatus"
            
            def description(self):
                return "for HW debug only"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["STSVTDemapStatus"] = _AF6CNC0022_RD_PDH_v3._stsvt_demap_hw_stat._STSVTDemapStatus()
            return allFields

    class _rx_stsvc_payload_ctrl(AtRegister.AtRegister):
        def name(self):
            return "PDH Rx Per STS/VC Payload Control"
    
        def description(self):
            return "The STS configuration registers are used to configure kinds of STS-1s and the kinds of VT/TUs in each STS-1."
            
        def width(self):
            return 14
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00030080 +  stsid"
            
        def startAddress(self):
            return 0x00030080
            
        def endAddress(self):
            return 0x000300bf

        class _PDHVtRxMapTug26Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PDHVtRxMapTug26Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-6. 0: TU11/VT1.5 type 1: TU12/VT2 type 2: reserved 3: VC/STS/DS3/E3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVtRxMapTug25Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PDHVtRxMapTug25Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-5"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVtRxMapTug24Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PDHVtRxMapTug24Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVtRxMapTug23Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PDHVtRxMapTug23Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVtRxMapTug22Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PDHVtRxMapTug22Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVtRxMapTug21Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PDHVtRxMapTug21Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVtRxMapTug20Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHVtRxMapTug20Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHVtRxMapTug26Type"] = _AF6CNC0022_RD_PDH_v3._rx_stsvc_payload_ctrl._PDHVtRxMapTug26Type()
            allFields["PDHVtRxMapTug25Type"] = _AF6CNC0022_RD_PDH_v3._rx_stsvc_payload_ctrl._PDHVtRxMapTug25Type()
            allFields["PDHVtRxMapTug24Type"] = _AF6CNC0022_RD_PDH_v3._rx_stsvc_payload_ctrl._PDHVtRxMapTug24Type()
            allFields["PDHVtRxMapTug23Type"] = _AF6CNC0022_RD_PDH_v3._rx_stsvc_payload_ctrl._PDHVtRxMapTug23Type()
            allFields["PDHVtRxMapTug22Type"] = _AF6CNC0022_RD_PDH_v3._rx_stsvc_payload_ctrl._PDHVtRxMapTug22Type()
            allFields["PDHVtRxMapTug21Type"] = _AF6CNC0022_RD_PDH_v3._rx_stsvc_payload_ctrl._PDHVtRxMapTug21Type()
            allFields["PDHVtRxMapTug20Type"] = _AF6CNC0022_RD_PDH_v3._rx_stsvc_payload_ctrl._PDHVtRxMapTug20Type()
            return allFields

    class _rxm23e23_ctrl(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Control"
    
        def description(self):
            return "The RxM23E23 Control is use to configure for per channel Rx DS3/E3 operation."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00040000 +  de3id"
            
        def startAddress(self):
            return 0x00040000
            
        def endAddress(self):
            return 0x0004003f

        class _DE3AISAll1sFrwDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "DE3AISAll1sFrwDis"
            
            def description(self):
                return "Forward DE3 AIS all-1s to generate L-bit to PSN Disable, set 1 to disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3LOFFrwDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "DS3LOFFrwDis"
            
            def description(self):
                return "Forward DS3 LOF to generate L-bit to PSN in case of MonOnly Disable, set 1 to disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3AISSigFrwEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "DS3AISSigFrwEnb"
            
            def description(self):
                return "Forward DS3 AIS signal to generate L-bit to PSN in case of MonOnly Enable, set 1 to enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RXDS3ForceAISLbit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "RXDS3ForceAISLbit"
            
            def description(self):
                return "Force DS3 AIS, L-bit to PSN, data all one to PSN for Payload/Line Remote Loopback"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE3MonOnly(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "RxDE3MonOnly"
            
            def description(self):
                return "Rx Framer Monitor only"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3E3ChEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "RxDS3E3ChEnb"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3E3LosThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 14
        
            def name(self):
                return "RxDS3E3LosThres"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3E3AisThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 11
        
            def name(self):
                return "RxDS3E3AisThres"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3E3LofThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "RxDS3E3LofThres"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3E3PayAllOne(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "RxDS3E3PayAllOne"
            
            def description(self):
                return "This bit is set to 1 to force the DS3/E3 framer payload AIS downstream"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3E3LineAllOne(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "RxDS3E3LineAllOne"
            
            def description(self):
                return "This bit is set to 1 to force the DS3/E3 framer line AIS downstream"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3E3LineLoop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RxDS3E3LineLoop"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3E3FrcLof(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxDS3E3FrcLof"
            
            def description(self):
                return "This bit is set to 1 to force the Rx DS3/E3 framer into LOF status. 1: force LOF. 0: not force LOF."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3E3Md(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDS3E3Md"
            
            def description(self):
                return "Rx DS3/E3 framing mode 0000: DS3 Unframed 0001: DS3 C-bit parity carrying 28 DS1s or 21 E1s 0010: DS3 M23 carrying 28 DS1s or 21 E1s 0011: DS3 C-bit map 0100: E3 Unframed 0101: E3 G832 0110: E3 G.751 carrying 16 E1s 0111: E3 G.751 Map 1000: Bypass RxM13E13 (DS1/E1 mode)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE3AISAll1sFrwDis"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_ctrl._DE3AISAll1sFrwDis()
            allFields["DS3LOFFrwDis"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_ctrl._DS3LOFFrwDis()
            allFields["DS3AISSigFrwEnb"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_ctrl._DS3AISSigFrwEnb()
            allFields["RXDS3ForceAISLbit"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_ctrl._RXDS3ForceAISLbit()
            allFields["RxDE3MonOnly"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_ctrl._RxDE3MonOnly()
            allFields["RxDS3E3ChEnb"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_ctrl._RxDS3E3ChEnb()
            allFields["RxDS3E3LosThres"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_ctrl._RxDS3E3LosThres()
            allFields["RxDS3E3AisThres"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_ctrl._RxDS3E3AisThres()
            allFields["RxDS3E3LofThres"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_ctrl._RxDS3E3LofThres()
            allFields["RxDS3E3PayAllOne"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_ctrl._RxDS3E3PayAllOne()
            allFields["RxDS3E3LineAllOne"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_ctrl._RxDS3E3LineAllOne()
            allFields["RxDS3E3LineLoop"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_ctrl._RxDS3E3LineLoop()
            allFields["RxDS3E3FrcLof"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_ctrl._RxDS3E3FrcLof()
            allFields["RxDS3E3Md"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_ctrl._RxDS3E3Md()
            return allFields

    class _rxm23e23_hw_stat(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 HW Status"
    
        def description(self):
            return "for HW debug only"
            
        def width(self):
            return 32
        
        def type(self):
            return "Stat"
            
        def fomular(self):
            return "0x00040080 +  de3id"
            
        def startAddress(self):
            return 0x00040080
            
        def endAddress(self):
            return 0x000400bf

        class _RxDS3_CrrAIC(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "RxDS3_CrrAIC"
            
            def description(self):
                return "Current AIC Bit value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3_CrrAISDownStr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RxDS3_CrrAISDownStr"
            
            def description(self):
                return "AIS Down Stream due to HI_Level AIS of DS3E3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3_CrrLosAllZero(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxDS3_CrrLosAllZero"
            
            def description(self):
                return "LOS All zero of DS3E3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3_CrrAISAllOne(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "RxDS3_CrrAISAllOne"
            
            def description(self):
                return "AIS All one of DS3E3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3_CrrAISSignal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RxDS3_CrrAISSignal"
            
            def description(self):
                return "AIS signal of DS3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS3E3State(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDS3E3State"
            
            def description(self):
                return "0:LOF, 1:Searching, 2: Verify, 3:InFrame"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDS3_CrrAIC"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_hw_stat._RxDS3_CrrAIC()
            allFields["RxDS3_CrrAISDownStr"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_hw_stat._RxDS3_CrrAISDownStr()
            allFields["RxDS3_CrrLosAllZero"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_hw_stat._RxDS3_CrrLosAllZero()
            allFields["RxDS3_CrrAISAllOne"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_hw_stat._RxDS3_CrrAISAllOne()
            allFields["RxDS3_CrrAISSignal"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_hw_stat._RxDS3_CrrAISSignal()
            allFields["RxDS3E3State"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_hw_stat._RxDS3E3State()
            return allFields

    class _rx_de3_oh_ctrl(AtRegister.AtRegister):
        def name(self):
            return "RxDS3E3 OH Pro Control"
    
        def description(self):
            return ""
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x040840 +  de3id"
            
        def startAddress(self):
            return 0x00040840
            
        def endAddress(self):
            return 0x0004087f

        class _DLEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DLEn"
            
            def description(self):
                return "DLEn 1: Data Link send to HDLC 0: Data Link not send to HDLC"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Ds3E3DLSL(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Ds3E3DLSL"
            
            def description(self):
                return "00: DL 01: UDL 10: FEAC 11: NA"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Ds3FEACEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Ds3FEACEn"
            
            def description(self):
                return "Enable detection FEAC state change"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FBEFbitCntEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FBEFbitCntEn"
            
            def description(self):
                return "Enable FBE counts F-Bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FBEMbitCntEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FBEMbitCntEn"
            
            def description(self):
                return "Enable FBE counts M-Bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DLEn"] = _AF6CNC0022_RD_PDH_v3._rx_de3_oh_ctrl._DLEn()
            allFields["Ds3E3DLSL"] = _AF6CNC0022_RD_PDH_v3._rx_de3_oh_ctrl._Ds3E3DLSL()
            allFields["Ds3FEACEn"] = _AF6CNC0022_RD_PDH_v3._rx_de3_oh_ctrl._Ds3FEACEn()
            allFields["FBEFbitCntEn"] = _AF6CNC0022_RD_PDH_v3._rx_de3_oh_ctrl._FBEFbitCntEn()
            allFields["FBEMbitCntEn"] = _AF6CNC0022_RD_PDH_v3._rx_de3_oh_ctrl._FBEMbitCntEn()
            return allFields

    class _upen_invert_ssm(AtRegister.AtRegister):
        def name(self):
            return "Rx DS3E3 SSM Invert"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0004040b
            
        def endAddress(self):
            return 0xffffffff

        class _SSMMessInv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SSMMessInv"
            
            def description(self):
                return "SSM message Invert - 1: invert 11111111_11_0xxxxxx0(bit0-5) - 0: non_invert 0xxxxxx0_11_11111111(bit5-0)"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SSMMessInv"] = _AF6CNC0022_RD_PDH_v3._upen_invert_ssm._SSMMessInv()
            return allFields

    class _upen_sta_ssm(AtRegister.AtRegister):
        def name(self):
            return "Rx DS3E3 SSM status"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x040EC0 +  de3id"
            
        def startAddress(self):
            return 0x00040ec0
            
        def endAddress(self):
            return 0xffffffff

        class _SSMMessSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "SSMMessSta"
            
            def description(self):
                return "SSM Status - 0: Seaching Status - 1: Checking Status - 2: Inframe Status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _SSMMessCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "SSMMessCnt"
            
            def description(self):
                return "SSM message matching counter"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _SSMMessVal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SSMMessVal"
            
            def description(self):
                return "SSM message - [3:0]: SSM E3 G832 Message - [5:0]: SSM DS3 C-bit Message"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SSMMessSta"] = _AF6CNC0022_RD_PDH_v3._upen_sta_ssm._SSMMessSta()
            allFields["SSMMessCnt"] = _AF6CNC0022_RD_PDH_v3._upen_sta_ssm._SSMMessCnt()
            allFields["SSMMessVal"] = _AF6CNC0022_RD_PDH_v3._upen_sta_ssm._SSMMessVal()
            return allFields

    class _rxde3_oh_thrsh_cfg(AtRegister.AtRegister):
        def name(self):
            return "RxDS3E3 OH Pro Threshold"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00040801
            
        def endAddress(self):
            return 0xffffffff

        class _IdleThresDS3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 21
        
            def name(self):
                return "IdleThresDS3"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _AisThresDS3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 18
        
            def name(self):
                return "AisThresDS3"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TimingMakerE3G832(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 15
        
            def name(self):
                return "TimingMakerE3G832"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _NewPayTypeE3G832(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 12
        
            def name(self):
                return "NewPayTypeE3G832"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _AICThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 9
        
            def name(self):
                return "AICThres"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RdiE3G832Thres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 6
        
            def name(self):
                return "RdiE3G832Thres"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RdiE3G751Thres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 3
        
            def name(self):
                return "RdiE3G751Thres"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RdiDS3Thres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RdiDS3Thres"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IdleThresDS3"] = _AF6CNC0022_RD_PDH_v3._rxde3_oh_thrsh_cfg._IdleThresDS3()
            allFields["AisThresDS3"] = _AF6CNC0022_RD_PDH_v3._rxde3_oh_thrsh_cfg._AisThresDS3()
            allFields["TimingMakerE3G832"] = _AF6CNC0022_RD_PDH_v3._rxde3_oh_thrsh_cfg._TimingMakerE3G832()
            allFields["NewPayTypeE3G832"] = _AF6CNC0022_RD_PDH_v3._rxde3_oh_thrsh_cfg._NewPayTypeE3G832()
            allFields["AICThres"] = _AF6CNC0022_RD_PDH_v3._rxde3_oh_thrsh_cfg._AICThres()
            allFields["RdiE3G832Thres"] = _AF6CNC0022_RD_PDH_v3._rxde3_oh_thrsh_cfg._RdiE3G832Thres()
            allFields["RdiE3G751Thres"] = _AF6CNC0022_RD_PDH_v3._rxde3_oh_thrsh_cfg._RdiE3G751Thres()
            allFields["RdiDS3Thres"] = _AF6CNC0022_RD_PDH_v3._rxde3_oh_thrsh_cfg._RdiDS3Thres()
            return allFields

    class _rxm23e23_rei_thrsh_cfg(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer REI Threshold Control"
    
        def description(self):
            return "This is the REI threshold configuration register for the PDH DS3/E3 Rx framer"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00040803
            
        def endAddress(self):
            return 0xffffffff

        class _RxM23E23ReiThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxM23E23ReiThres"
            
            def description(self):
                return "Threshold to generate interrupt if the REI counter exceed this threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxM23E23ReiThres"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_rei_thrsh_cfg._RxM23E23ReiThres()
            return allFields

    class _rxm23e23_fbe_thrsh_cfg(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer FBE Threshold Control"
    
        def description(self):
            return "This is the REI threshold configuration register for the PDH DS3/E3 Rx framer"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00040804
            
        def endAddress(self):
            return 0xffffffff

        class _RxM23E23FbeThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxM23E23FbeThres"
            
            def description(self):
                return "Threshold to generate interrupt if the FBE counter exceed this threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxM23E23FbeThres"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_fbe_thrsh_cfg._RxM23E23FbeThres()
            return allFields

    class _rxm23e23_parity_thrsh_cfg(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer Parity Threshold Control"
    
        def description(self):
            return "This is the REI threshold configuration register for the PDH DS3/E3 Rx framer"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00040805
            
        def endAddress(self):
            return 0xffffffff

        class _RxM23E23ParThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxM23E23ParThres"
            
            def description(self):
                return "Threshold to generate interrupt if the PAR counter exceed this threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxM23E23ParThres"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_parity_thrsh_cfg._RxM23E23ParThres()
            return allFields

    class _rxm23e23_cbit_parity_thrsh_cfg(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer Cbit Parity Threshold Control"
    
        def description(self):
            return "This is the REI threshold configuration register for the PDH DS3/E3 Rx framer"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00040806
            
        def endAddress(self):
            return 0xffffffff

        class _RxM23E23CParThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxM23E23CParThres"
            
            def description(self):
                return "Threshold to generate interrupt if the Cbit Parity counter exceed this threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxM23E23CParThres"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_cbit_parity_thrsh_cfg._RxM23E23CParThres()
            return allFields

    class _rxm23e23_rei_cnt(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer REI Counter"
    
        def description(self):
            return "This is the per channel REI counter for DS3/E3 receive framer"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00040880 +  64*Rd2Clr + de3id"
            
        def startAddress(self):
            return 0x00040880
            
        def endAddress(self):
            return 0x000408bf

        class _RxM23E23ReiCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxM23E23ReiCnt"
            
            def description(self):
                return "DS3/E3 REI error accumulator"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxM23E23ReiCnt"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_rei_cnt._RxM23E23ReiCnt()
            return allFields

    class _rxm23e23_fbe_cnt(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer FBE Counter"
    
        def description(self):
            return "This is the per channel FBE counter for DS3/E3 receive framer"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00040900 +  64*Rd2Clr + de3id"
            
        def startAddress(self):
            return 0x00040900
            
        def endAddress(self):
            return 0x0004093f

        class _count(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "count"
            
            def description(self):
                return "frame FBE counter"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["count"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_fbe_cnt._count()
            return allFields

    class _rxm23e23_parity_cnt(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer Parity Counter"
    
        def description(self):
            return "This is the per channel Parity counter for DS3/E3 receive framer"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00040980 +  64*Rd2Clr + de3id"
            
        def startAddress(self):
            return 0x00040980
            
        def endAddress(self):
            return 0x000409bf

        class _RxM23E23ParCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxM23E23ParCnt"
            
            def description(self):
                return "DS3/E3 Parity error accumulator"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxM23E23ParCnt"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_parity_cnt._RxM23E23ParCnt()
            return allFields

    class _rxm23e23_cbit_parity_cnt(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer Cbit Parity Counter"
    
        def description(self):
            return "This is the per channel Cbit Parity counter for DS3/E3 receive framer"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00040A00 +  64*Rd2Clr + de3id"
            
        def startAddress(self):
            return 0x00040a00
            
        def endAddress(self):
            return 0x00040a3f

        class _RxM23E23CParCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxM23E23CParCnt"
            
            def description(self):
                return "DS3/E3 Cbit Parity error accumulator"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxM23E23CParCnt"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_cbit_parity_cnt._RxM23E23CParCnt()
            return allFields

    class _rx_de3_feac_buffer(AtRegister.AtRegister):
        def name(self):
            return "RxDS3E3 FEAC Buffer"
    
        def description(self):
            return ""
            
        def width(self):
            return 9
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x040B40 +  de3id"
            
        def startAddress(self):
            return 0x00040b40
            
        def endAddress(self):
            return 0x00040b7f

        class _FEAC_State(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FEAC_State"
            
            def description(self):
                return "00: idle codeword 01: active codeword 10: de-active codeword 11: alarm/status codeword"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FEAC_Codeword(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FEAC_Codeword"
            
            def description(self):
                return "6 x-bits cw in format 111111110xxxxxx0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FEAC_State"] = _AF6CNC0022_RD_PDH_v3._rx_de3_feac_buffer._FEAC_State()
            allFields["FEAC_Codeword"] = _AF6CNC0022_RD_PDH_v3._rx_de3_feac_buffer._FEAC_Codeword()
            return allFields

    class _rxm23e23_per_chn_intr_cfg(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer per Channel Interrupt Enable Control"
    
        def description(self):
            return "This is the per Channel interrupt enable of DS1/E1/J1 Rx Framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00040D00 +  de3id"
            
        def startAddress(self):
            return 0x00040d00
            
        def endAddress(self):
            return 0x00040d3f

        class _DE3BERTCA_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "DE3BERTCA_En"
            
            def description(self):
                return "Set 1 to enable change DE3 BER TCA event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3BERSD_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "DE3BERSD_En"
            
            def description(self):
                return "Set 1 to enable change DE3 BER SD event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3BERSF_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "DE3BERSF_En"
            
            def description(self):
                return "Set 1 to enable change DE3 BER SF event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3LCVCnt_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DE3LCVCnt_En"
            
            def description(self):
                return "Set 1 to enable change DE3 LCV counter event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3CP_E3Tr_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "DS3CP_E3Tr_En"
            
            def description(self):
                return "Set 1 to enable change DS3 CP counter or E3G831 Trace event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3ParCnt_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DE3ParCnt_En"
            
            def description(self):
                return "Set 1 to enable change DE3 Parity counter event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3FBECnt_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DE3FBECnt_En"
            
            def description(self):
                return "Set 1 to enable change DE3 FBE counter event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3REICnt_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DE3REICnt_En"
            
            def description(self):
                return "Set 1 to enable change DE3 REI counter event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3FEAC_E3SSM_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "DS3FEAC_E3SSM_En"
            
            def description(self):
                return "Set 1 to enable change DS3 FEAC or E3G832 SSM event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3AIC_E3TM_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DS3AIC_E3TM_En"
            
            def description(self):
                return "Set 1 to enable change DS3 AIC or E3G832 TM event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3IDLESig_E3PT_En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DS3IDLESig_E3PT_En"
            
            def description(self):
                return "Set 1 to enable change DS3 IDLE signal or E3G832 PayloadType event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3AisAllOneIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DE3AisAllOneIntrEn"
            
            def description(self):
                return "Set 1 to enable change AIS All One event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3LosAllZeroIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DE3LosAllZeroIntrEn"
            
            def description(self):
                return "Set 1 to enable change LOS All Zero event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3AISSigIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DS3AISSigIntrEn"
            
            def description(self):
                return "Set 1 to enable change AIS Signal event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3RDIIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DE3RDIIntrEn"
            
            def description(self):
                return "Set 1 to enable change RDI event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3LofIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE3LofIntrEn"
            
            def description(self):
                return "Set 1 to enable change LOF event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE3BERTCA_En"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_cfg._DE3BERTCA_En()
            allFields["DE3BERSD_En"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_cfg._DE3BERSD_En()
            allFields["DE3BERSF_En"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_cfg._DE3BERSF_En()
            allFields["DE3LCVCnt_En"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_cfg._DE3LCVCnt_En()
            allFields["DS3CP_E3Tr_En"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_cfg._DS3CP_E3Tr_En()
            allFields["DE3ParCnt_En"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_cfg._DE3ParCnt_En()
            allFields["DE3FBECnt_En"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_cfg._DE3FBECnt_En()
            allFields["DE3REICnt_En"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_cfg._DE3REICnt_En()
            allFields["DS3FEAC_E3SSM_En"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_cfg._DS3FEAC_E3SSM_En()
            allFields["DS3AIC_E3TM_En"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_cfg._DS3AIC_E3TM_En()
            allFields["DS3IDLESig_E3PT_En"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_cfg._DS3IDLESig_E3PT_En()
            allFields["DE3AisAllOneIntrEn"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_cfg._DE3AisAllOneIntrEn()
            allFields["DE3LosAllZeroIntrEn"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_cfg._DE3LosAllZeroIntrEn()
            allFields["DS3AISSigIntrEn"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_cfg._DS3AISSigIntrEn()
            allFields["DE3RDIIntrEn"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_cfg._DE3RDIIntrEn()
            allFields["DE3LofIntrEn"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_cfg._DE3LofIntrEn()
            return allFields

    class _rxm23e23_per_chn_intr_stat(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer per Channel Interrupt Change Status"
    
        def description(self):
            return "This is the per Channel interrupt tus of DS3/E3 Rx Framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x00040D40 +  de3id"
            
        def startAddress(self):
            return 0x00040d40
            
        def endAddress(self):
            return 0x00040d7f

        class _DE3BERTCA_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "DE3BERTCA_Intr"
            
            def description(self):
                return "Set 1 if there is change DE3 BER TCA event to generate an interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3BERSD_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "DE3BERSD_Intr"
            
            def description(self):
                return "Set 1 if there is change DE3 BER SD event to generate an interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3BERSF_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "DE3BERSF_Intr"
            
            def description(self):
                return "Set 1 if there is change DE3 BER SF event to generate an interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3LCVCnt_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DE3LCVCnt_Intr"
            
            def description(self):
                return "Set 1 if there is change DE3 LCV counter event to generate an interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3CP_E3Tr_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "DS3CP_E3Tr_Intr"
            
            def description(self):
                return "Set 1 if there is change DS3 CP counter or E3G831 Trace event to generate an interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3ParCnt_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DE3ParCnt_Intr"
            
            def description(self):
                return "Set 1 if there is change DE3 Parity counter event to generate an interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3FBECnt_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DE3FBECnt_Intr"
            
            def description(self):
                return "Set 1 if there is change DE3 FBE counter event to generate an interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3REICnt_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DE3REICnt_Intr"
            
            def description(self):
                return "Set 1 to enable change DE3 REI counter event to generate an interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3FEAC_E3SSM_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "DS3FEAC_E3SSM_Intr"
            
            def description(self):
                return "Set 1 if there is a change DS3 FEAC or E3G832 SSM event to generate an interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3AIC_E3TM_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DS3AIC_E3TM_Intr"
            
            def description(self):
                return "Set 1 if there is a change DS3 AIC or E3G832 TM event to generate an interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3IDLESig_E3PT_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DS3IDLESig_E3PT_Intr"
            
            def description(self):
                return "Set 1 if there is a change DS3 IDLE signal or E3G832 PayloadType event to generate an interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3AisAllOne_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DE3AisAllOne_Intr"
            
            def description(self):
                return "Set 1 if there is a change AIS All One event to generate an interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3LosAllZero_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DE3LosAllZero_Intr"
            
            def description(self):
                return "Set 1 if there is a change LOS All Zero event to generate an interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3AISSig_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DS3AISSig_Intr"
            
            def description(self):
                return "Set 1 if there is a change AIS Signal event to generate an interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3RDI_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DE3RDI_Intr"
            
            def description(self):
                return "Set 1 if there is a  change RDI event to generate an interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3Lof_Intr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE3Lof_Intr"
            
            def description(self):
                return "Set 1 if there is a  change LOF event to generate an interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE3BERTCA_Intr"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_stat._DE3BERTCA_Intr()
            allFields["DE3BERSD_Intr"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_stat._DE3BERSD_Intr()
            allFields["DE3BERSF_Intr"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_stat._DE3BERSF_Intr()
            allFields["DE3LCVCnt_Intr"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_stat._DE3LCVCnt_Intr()
            allFields["DS3CP_E3Tr_Intr"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_stat._DS3CP_E3Tr_Intr()
            allFields["DE3ParCnt_Intr"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_stat._DE3ParCnt_Intr()
            allFields["DE3FBECnt_Intr"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_stat._DE3FBECnt_Intr()
            allFields["DE3REICnt_Intr"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_stat._DE3REICnt_Intr()
            allFields["DS3FEAC_E3SSM_Intr"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_stat._DS3FEAC_E3SSM_Intr()
            allFields["DS3AIC_E3TM_Intr"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_stat._DS3AIC_E3TM_Intr()
            allFields["DS3IDLESig_E3PT_Intr"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_stat._DS3IDLESig_E3PT_Intr()
            allFields["DE3AisAllOne_Intr"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_stat._DE3AisAllOne_Intr()
            allFields["DE3LosAllZero_Intr"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_stat._DE3LosAllZero_Intr()
            allFields["DS3AISSig_Intr"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_stat._DS3AISSig_Intr()
            allFields["DE3RDI_Intr"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_stat._DE3RDI_Intr()
            allFields["DE3Lof_Intr"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_intr_stat._DE3Lof_Intr()
            return allFields

    class _rxm23e23_per_chn_curr_stat(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer per Channel Current Status"
    
        def description(self):
            return "This is the per Channel Current tus of DS3/E3 Rx Framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00040D80 +  de3id"
            
        def startAddress(self):
            return 0x00040d80
            
        def endAddress(self):
            return 0x00040dbf

        class _DE3BERTCA_CurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "DE3BERTCA_CurrSta"
            
            def description(self):
                return "Current status DE3 BER TCA event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3BERSD_CurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "DE3BERSD_CurrSta"
            
            def description(self):
                return "Current status DE3 BER SD event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3BERSF_CurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "DE3BERSF_CurrSta"
            
            def description(self):
                return "Current status DE3 BER SF event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3LCVCnt_CurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DE3LCVCnt_CurrSta"
            
            def description(self):
                return "Current status DE3 LCV counter event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3CP_E3Tr_CurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "DS3CP_E3Tr_CurrSta"
            
            def description(self):
                return "Current status DS3 CP counter or E3G831 Trace event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3ParCnt_CurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DE3ParCnt_CurrSta"
            
            def description(self):
                return "Current status DE3 Parity counter event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3FBECnt_CurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DE3FBECnt_CurrSta"
            
            def description(self):
                return "Current status DE3 FBE counter event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3REICnt_CurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DE3REICnt_CurrSta"
            
            def description(self):
                return "Current status DE3 REI counter event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3FEAC_E3SSM_CurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "DS3FEAC_E3SSM_CurrSta"
            
            def description(self):
                return "Current status of DS3 FEAC or E3G832 SSM event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3AIC_E3TM_CurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DS3AIC_E3TM_CurrSta"
            
            def description(self):
                return "Current status of DS3 AIC or E3G832 TM event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3IDLESig_E3PT_CurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DS3IDLESig_E3PT_CurrSta"
            
            def description(self):
                return "Current status of DS3 IDLE signal or E3G832 PayloadType event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3AisAllOneCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DE3AisAllOneCurrSta"
            
            def description(self):
                return "Current status of AIS All One event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3LosAllZeroCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DE3LosAllZeroCurrSta"
            
            def description(self):
                return "Current status of LOS All Zero event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3AISSigCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DS3AISSigCurrSta"
            
            def description(self):
                return "Current status of AIS Signal event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3RDICurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DE3RDICurrSta"
            
            def description(self):
                return "Current status of RDI event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3LofCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE3LofCurrSta"
            
            def description(self):
                return "Current status of LOF event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE3BERTCA_CurrSta"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_curr_stat._DE3BERTCA_CurrSta()
            allFields["DE3BERSD_CurrSta"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_curr_stat._DE3BERSD_CurrSta()
            allFields["DE3BERSF_CurrSta"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_curr_stat._DE3BERSF_CurrSta()
            allFields["DE3LCVCnt_CurrSta"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_curr_stat._DE3LCVCnt_CurrSta()
            allFields["DS3CP_E3Tr_CurrSta"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_curr_stat._DS3CP_E3Tr_CurrSta()
            allFields["DE3ParCnt_CurrSta"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_curr_stat._DE3ParCnt_CurrSta()
            allFields["DE3FBECnt_CurrSta"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_curr_stat._DE3FBECnt_CurrSta()
            allFields["DE3REICnt_CurrSta"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_curr_stat._DE3REICnt_CurrSta()
            allFields["DS3FEAC_E3SSM_CurrSta"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_curr_stat._DS3FEAC_E3SSM_CurrSta()
            allFields["DS3AIC_E3TM_CurrSta"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_curr_stat._DS3AIC_E3TM_CurrSta()
            allFields["DS3IDLESig_E3PT_CurrSta"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_curr_stat._DS3IDLESig_E3PT_CurrSta()
            allFields["DE3AisAllOneCurrSta"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_curr_stat._DE3AisAllOneCurrSta()
            allFields["DE3LosAllZeroCurrSta"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_curr_stat._DE3LosAllZeroCurrSta()
            allFields["DS3AISSigCurrSta"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_curr_stat._DS3AISSigCurrSta()
            allFields["DE3RDICurrSta"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_curr_stat._DE3RDICurrSta()
            allFields["DE3LofCurrSta"] = _AF6CNC0022_RD_PDH_v3._rxm23e23_per_chn_curr_stat._DE3LofCurrSta()
            return allFields

    class _RxM23E23_Framer_per_chn_intr_or_stat(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer per Channel Interrupt OR Status"
    
        def description(self):
            return "The register consists of 2 bits for 2 Group of the related STS/VC in the RxM23E23. Each bit is used to store Interrupt OR status of the related Group DS3/E3."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x00040DC0 +  GrpID"
            
        def startAddress(self):
            return 0x00040dc0
            
        def endAddress(self):
            return 0x00040dc1

        class _DE3GrpIntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE3GrpIntrOrSta"
            
            def description(self):
                return "Set to 1 if any interrupt status bit of corresponding DS3/E3 is set and its interrupt is enabled."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE3GrpIntrOrSta"] = _AF6CNC0022_RD_PDH_v3._RxM23E23_Framer_per_chn_intr_or_stat._DE3GrpIntrOrSta()
            return allFields

    class _RxM23E23_Framer_per_grp_intr_or_stat(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer per Group Interrupt OR Status"
    
        def description(self):
            return "The register consists of 2 bits for 2 Group of the RxM23E23 Framer. Each bit is used to store Interrupt OR status of the related Group."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00040dff
            
        def endAddress(self):
            return 0xffffffff

        class _DE3GrpIntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE3GrpIntrOrSta"
            
            def description(self):
                return "Set to 1 if any interrupt status bit of corresponding Group is set and its interrupt is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE3GrpIntrOrSta"] = _AF6CNC0022_RD_PDH_v3._RxM23E23_Framer_per_grp_intr_or_stat._DE3GrpIntrOrSta()
            return allFields

    class _RxM23E23_Framer_per_Group_intr_en_ctrl(AtRegister.AtRegister):
        def name(self):
            return "RxM23E23 Framer per Group Interrupt Enable Control"
    
        def description(self):
            return "The register consists of 2 interrupt enable bits for 2 Group in the Rx DS3/E3 Framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00040dfe
            
        def endAddress(self):
            return 0xffffffff

        class _DE3GrpIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE3GrpIntrEn"
            
            def description(self):
                return "Set to 1 to enable the related Group to generate interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE3GrpIntrEn"] = _AF6CNC0022_RD_PDH_v3._RxM23E23_Framer_per_Group_intr_en_ctrl._DE3GrpIntrEn()
            return allFields

    class _rxde3_oh_dlk_swap_bit_cfg(AtRegister.AtRegister):
        def name(self):
            return "RxDS3E3 OH Pro DLK Swap Bit Enable"
    
        def description(self):
            return ""
            
        def width(self):
            return 3
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0004080b
            
        def endAddress(self):
            return 0xffffffff

        class _DlkSwapBitDs3En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DlkSwapBitDs3En"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DlkSwapBitE3g751En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DlkSwapBitE3g751En"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DlkSwapBitE3g823En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DlkSwapBitE3g823En"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DlkSwapBitDs3En"] = _AF6CNC0022_RD_PDH_v3._rxde3_oh_dlk_swap_bit_cfg._DlkSwapBitDs3En()
            allFields["DlkSwapBitE3g751En"] = _AF6CNC0022_RD_PDH_v3._rxde3_oh_dlk_swap_bit_cfg._DlkSwapBitE3g751En()
            allFields["DlkSwapBitE3g823En"] = _AF6CNC0022_RD_PDH_v3._rxde3_oh_dlk_swap_bit_cfg._DlkSwapBitE3g823En()
            return allFields

    class _rxm12e12_ctrl(AtRegister.AtRegister):
        def name(self):
            return "RxM12E12 Control"
    
        def description(self):
            return "The RxM12E12 Control is use to configure for per channel Rx DS2/E2 operation."
            
        def width(self):
            return 3
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00041000 +  8*de3id + de2id"
            
        def startAddress(self):
            return 0x00041000
            
        def endAddress(self):
            return 0x000411ff

        class _RxDS2E2FrcLof(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RxDS2E2FrcLof"
            
            def description(self):
                return "This bit is set to 1 to force the Rx DS3/E3 framer into LOF tus. 1: force LOF. 0: not force LOF."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS2E2Md(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDS2E2Md"
            
            def description(self):
                return "Rx DS2/E2 framing mode 00: DS2 T1.107 carrying 4 DS1s 01: DS2 G.747 carrying 3 E1s 10: E2 G.742 carrying 4 E1s 11: Reserved"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDS2E2FrcLof"] = _AF6CNC0022_RD_PDH_v3._rxm12e12_ctrl._RxDS2E2FrcLof()
            allFields["RxDS2E2Md"] = _AF6CNC0022_RD_PDH_v3._rxm12e12_ctrl._RxDS2E2Md()
            return allFields

    class _rxm12e12_stsvc_payload_ctrl(AtRegister.AtRegister):
        def name(self):
            return "PDH RxM12E12 Per STS/VC Payload Control"
    
        def description(self):
            return "The STS configuration registers are used to configure kinds of STS-1s and the kinds of VT/TUs in each STS-1."
            
        def width(self):
            return 14
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00041600 +  stsid"
            
        def startAddress(self):
            return 0x00041600
            
        def endAddress(self):
            return 0x0004163f

        class _PDHVtRxMapTug26Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PDHVtRxMapTug26Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-6. 0: TU11/VT1.5 type 1: TU12/VT2 type 2: reserved 3: reserved"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVtRxMapTug25Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PDHVtRxMapTug25Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-5"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVtRxMapTug24Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PDHVtRxMapTug24Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVtRxMapTug23Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PDHVtRxMapTug23Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVtRxMapTug22Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PDHVtRxMapTug22Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVtRxMapTug21Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PDHVtRxMapTug21Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHVtRxMapTug20Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHVtRxMapTug20Type"
            
            def description(self):
                return "Define types of VT/TUs in TUG-2-0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHVtRxMapTug26Type"] = _AF6CNC0022_RD_PDH_v3._rxm12e12_stsvc_payload_ctrl._PDHVtRxMapTug26Type()
            allFields["PDHVtRxMapTug25Type"] = _AF6CNC0022_RD_PDH_v3._rxm12e12_stsvc_payload_ctrl._PDHVtRxMapTug25Type()
            allFields["PDHVtRxMapTug24Type"] = _AF6CNC0022_RD_PDH_v3._rxm12e12_stsvc_payload_ctrl._PDHVtRxMapTug24Type()
            allFields["PDHVtRxMapTug23Type"] = _AF6CNC0022_RD_PDH_v3._rxm12e12_stsvc_payload_ctrl._PDHVtRxMapTug23Type()
            allFields["PDHVtRxMapTug22Type"] = _AF6CNC0022_RD_PDH_v3._rxm12e12_stsvc_payload_ctrl._PDHVtRxMapTug22Type()
            allFields["PDHVtRxMapTug21Type"] = _AF6CNC0022_RD_PDH_v3._rxm12e12_stsvc_payload_ctrl._PDHVtRxMapTug21Type()
            allFields["PDHVtRxMapTug20Type"] = _AF6CNC0022_RD_PDH_v3._rxm12e12_stsvc_payload_ctrl._PDHVtRxMapTug20Type()
            return allFields

    class _rxm12e12_hw_stat(AtRegister.AtRegister):
        def name(self):
            return "RxM12E12 HW Status"
    
        def description(self):
            return "for HW debug only"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00041400 +  8*de3id + de2id"
            
        def startAddress(self):
            return 0x00041400
            
        def endAddress(self):
            return 0x000415ff

        class _RxM12E12Status(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxM12E12Status"
            
            def description(self):
                return "for HW debug only"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxM12E12Status"] = _AF6CNC0022_RD_PDH_v3._rxm12e12_hw_stat._RxM12E12Status()
            return allFields

    class _RxM12E12_per_chn_intr_cfg(AtRegister.AtRegister):
        def name(self):
            return "RxM12E12 Framer per Channel Interrupt Enable Control"
    
        def description(self):
            return "This is the per Channel interrupt enable of DS2/E2 Rx Framer."
            
        def width(self):
            return 7
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00041700 +  de3id"
            
        def startAddress(self):
            return 0x00041700
            
        def endAddress(self):
            return 0x0004173f

        class _DE2LOFIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE2LOFIntrEn"
            
            def description(self):
                return "Each bit correspond with each channel DS2/E2 in DS3/E3 0: No Interrupt 1: Have Interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE2LOFIntrEn"] = _AF6CNC0022_RD_PDH_v3._RxM12E12_per_chn_intr_cfg._DE2LOFIntrEn()
            return allFields

    class _RxM12E12_per_chn_intr_stat(AtRegister.AtRegister):
        def name(self):
            return "RxM12E12 Framer per Channel Interrupt Change Status"
    
        def description(self):
            return "This is the per Channel interrupt status of DS2/E2 Rx Framer."
            
        def width(self):
            return 7
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x00041740 +  de3id"
            
        def startAddress(self):
            return 0x00041740
            
        def endAddress(self):
            return 0x0004177f

        class _DE2LOFIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE2LOFIntr"
            
            def description(self):
                return "Each bit correspond with each channel DS2/E2 in DS3/E3 0: No Interrupt 1: Have Interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE2LOFIntr"] = _AF6CNC0022_RD_PDH_v3._RxM12E12_per_chn_intr_stat._DE2LOFIntr()
            return allFields

    class _RxM12E12_per_chn_curr_stat(AtRegister.AtRegister):
        def name(self):
            return "RxM12E12 Framer per Channel Current Status"
    
        def description(self):
            return "This is the per Channel Current status of DS2/E2 Rx Framer."
            
        def width(self):
            return 7
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00041780 +  de3id"
            
        def startAddress(self):
            return 0x00041780
            
        def endAddress(self):
            return 0x000417bf

        class _DE2LOFCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE2LOFCurrSta"
            
            def description(self):
                return "Each bit correspond with each channel DS2/E2 in DS3/E3 0: No Interrupt 1: Have Interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE2LOFCurrSta"] = _AF6CNC0022_RD_PDH_v3._RxM12E12_per_chn_curr_stat._DE2LOFCurrSta()
            return allFields

    class _RxM12E12_Framer_per_chn_intr_or_stat(AtRegister.AtRegister):
        def name(self):
            return "RxM12E12 Framer per Channel Interrupt OR Status"
    
        def description(self):
            return "The register consists of 32 bits for 2 Group of the related STS/VC in the RxM12E12. Each bit is used to store Interrupt OR tus of the related Group DS2/E2."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x000417C0 +  GrpID"
            
        def startAddress(self):
            return 0x000417c0
            
        def endAddress(self):
            return 0x000417c1

        class _DE2GrpIntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE2GrpIntrOrSta"
            
            def description(self):
                return "Set to 1 if any interrupt status bit of corresponding Group DS3/E3 is set and its interrupt is enabled."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE2GrpIntrOrSta"] = _AF6CNC0022_RD_PDH_v3._RxM12E12_Framer_per_chn_intr_or_stat._DE2GrpIntrOrSta()
            return allFields

    class _RxM12E12_Framer_per_grp_intr_or_stat(AtRegister.AtRegister):
        def name(self):
            return "RxM12E12 Framer per Group Interrupt OR Status"
    
        def description(self):
            return "The register consists of 2 bits for 2 Group of the RxM12E12 Framer. Each bit is used to store Interrupt OR status of the related Group."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000417ff
            
        def endAddress(self):
            return 0xffffffff

        class _DE2GrpIntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE2GrpIntrOrSta"
            
            def description(self):
                return "Set to 1 if any interrupt status bit of corresponding Group is set and its interrupt is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE2GrpIntrOrSta"] = _AF6CNC0022_RD_PDH_v3._RxM12E12_Framer_per_grp_intr_or_stat._DE2GrpIntrOrSta()
            return allFields

    class _RxM12E12_Framer_per_Group_intr_en_ctrl(AtRegister.AtRegister):
        def name(self):
            return "RxM12E12 Framer per Group Interrupt Enable Control"
    
        def description(self):
            return "The register consists of 2 interrupt enable bits for 2 Group in the Rx DS2/E2 Framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000417fe
            
        def endAddress(self):
            return 0xffffffff

        class _DE2GrpIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE2GrpIntrEn"
            
            def description(self):
                return "Set to 1 to enable the related Group to generate interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE2GrpIntrEn"] = _AF6CNC0022_RD_PDH_v3._RxM12E12_Framer_per_Group_intr_en_ctrl._DE2GrpIntrEn()
            return allFields

    class _dej1_fbe_thrsh_cfg(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer FBE Threshold Control"
    
        def description(self):
            return "This is the FBE threshold configuration register for the PDH DS1/E1/J1 Rx framer"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00050003
            
        def endAddress(self):
            return 0x00050003

        class _RxDE1FbeThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1FbeThres"
            
            def description(self):
                return "Threshold to generate interrupt if the FBE counter exceed this threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1FbeThres"] = _AF6CNC0022_RD_PDH_v3._dej1_fbe_thrsh_cfg._RxDE1FbeThres()
            return allFields

    class _dej1_rx_framer_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer Control"
    
        def description(self):
            return "DS1/E1/J1 Rx framer control is used to configure for Frame mode (DS1, E1, J1) at the DS1/E1 framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00054000 +  32*de3id + 4*de2id + de1id"
            
        def startAddress(self):
            return 0x00054000
            
        def endAddress(self):
            return 0x000547ff

        class _RxDE1LOFFrwDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "RxDE1LOFFrwDis"
            
            def description(self):
                return "Forward DE1 LOF to generate L-bit to PSN in case of MonOnly, set 1 to disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1FrcAISLBit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "RxDE1FrcAISLBit"
            
            def description(self):
                return "Force AIS alarm here,L-bit, data all one to PSN when Line/Payload Remote Loopback"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1FrcAISLineDwn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "RxDE1FrcAISLineDwn"
            
            def description(self):
                return "Set 1 to force AIS Line DownStream"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1FrcAISPldDwn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "RxDE1FrcAISPldDwn"
            
            def description(self):
                return "Set 1 to force AIS payload DownStream"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1FrcLos(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "RxDE1FrcLos"
            
            def description(self):
                return "Set 1 to force LOS"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1LomfDstrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "RxDE1LomfDstrEn"
            
            def description(self):
                return "Set 1 to generate AIS forwarding dowstream in case E1CRC LOMF defect is present. This bit set 1 also to backwarding RDI upstream if E1 path is terminated in case E1CRC LOMF defect is present"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1LosCntThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 23
        
            def name(self):
                return "RxDE1LosCntThres"
            
            def description(self):
                return "Threshold of LOS monitoring block. This function always monitors the number of bit 1 of the incoming signal in each of two consecutive frame periods (256*2 bits in E1, 193*3 bits in DS1). If it is less than the desire number, LOS will be set then."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1MonOnly(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "RxDE1MonOnly"
            
            def description(self):
                return "Set 1 to enable the Monitor Only mode at the RXDS1/E1 framer"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1LocalLineLoop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "RxDE1LocalLineLoop"
            
            def description(self):
                return "Set 1 to enable Local Line loop back"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDe1LocalPayLoop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "RxDe1LocalPayLoop"
            
            def description(self):
                return "Set 1 to enable Local Payload loop back. Sharing for VT Loopback in case of CEP path"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1E1SaCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 17
        
            def name(self):
                return "RxDE1E1SaCfg"
            
            def description(self):
                return "Incase E1 Sa used for FDL configuration 0x0: No Sa bit used 0x3: Sa4 is used for SSM 0x4: Sa5 is used for SSM 0x5: Sa6 is used for SSM 0x6: Sa7 is used for SSM 0x7: Sa8 is used for SSM Incase DS1 bit[19:18] dont care bit [17]   set \" 1\" to enable Rx DLK"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1LofThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 13
        
            def name(self):
                return "RxDE1LofThres"
            
            def description(self):
                return "Threshold for FAS error to declare LOF in DS1/E1/J1 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1AisCntThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 10
        
            def name(self):
                return "RxDE1AisCntThres"
            
            def description(self):
                return "Threshold of AIS monitoring block. This function always monitors the number of bit 0 of the incoming signal in each of two consecutive frame periods (256*2 bits in E1, 193*3 bits in DS1). If it is less than the desire number, AIS will be set then."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1RaiCntThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 7
        
            def name(self):
                return "RxDE1RaiCntThres"
            
            def description(self):
                return "Threshold of RAI monitoring block"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1AisMaskEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "RxDE1AisMaskEn"
            
            def description(self):
                return "Forward DE1 AIS to generate L-bit to PSN, set 1 to enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RxDE1En"
            
            def description(self):
                return "Set 1 to enable the DS1/E1/J1 framer."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1FrcLof(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxDE1FrcLof"
            
            def description(self):
                return "Set 1 to force Re-frame"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDs1Md(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDs1Md"
            
            def description(self):
                return "Receive DS1/J1 framing mode 0000: DS1/J1 Unframe 0001: DS1 SF (D4) 0010: DS1 ESF 0011: DS1 DDS 0100: DS1 SLC 0101: J1 SF 0110: J1 ESF 1000: E1 Unframe 1001: E1 Basic Frame 1010: E1 CRC4 Frame"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1LOFFrwDis"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_ctrl._RxDE1LOFFrwDis()
            allFields["RxDE1FrcAISLBit"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_ctrl._RxDE1FrcAISLBit()
            allFields["RxDE1FrcAISLineDwn"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_ctrl._RxDE1FrcAISLineDwn()
            allFields["RxDE1FrcAISPldDwn"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_ctrl._RxDE1FrcAISPldDwn()
            allFields["RxDE1FrcLos"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_ctrl._RxDE1FrcLos()
            allFields["RxDE1LomfDstrEn"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_ctrl._RxDE1LomfDstrEn()
            allFields["RxDE1LosCntThres"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_ctrl._RxDE1LosCntThres()
            allFields["RxDE1MonOnly"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_ctrl._RxDE1MonOnly()
            allFields["RxDE1LocalLineLoop"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_ctrl._RxDE1LocalLineLoop()
            allFields["RxDe1LocalPayLoop"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_ctrl._RxDe1LocalPayLoop()
            allFields["RxDE1E1SaCfg"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_ctrl._RxDE1E1SaCfg()
            allFields["RxDE1LofThres"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_ctrl._RxDE1LofThres()
            allFields["RxDE1AisCntThres"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_ctrl._RxDE1AisCntThres()
            allFields["RxDE1RaiCntThres"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_ctrl._RxDE1RaiCntThres()
            allFields["RxDE1AisMaskEn"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_ctrl._RxDE1AisMaskEn()
            allFields["RxDE1En"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_ctrl._RxDE1En()
            allFields["RxDE1FrcLof"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_ctrl._RxDE1FrcLof()
            allFields["RxDs1Md"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_ctrl._RxDs1Md()
            return allFields

    class _dej1_rx_framer_hw_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer HW Status"
    
        def description(self):
            return "These registers are used for Hardware tus only"
            
        def width(self):
            return 32
        
        def type(self):
            return "Stat"
            
        def fomular(self):
            return "0x00054800 +  32*de3id + 4*de2id + de1id"
            
        def startAddress(self):
            return 0x00054800
            
        def endAddress(self):
            return 0x00054fff

        class _RxDS1E1_CrrE1SSM(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 7
        
            def name(self):
                return "RxDS1E1_CrrE1SSM"
            
            def description(self):
                return "Current E1 SSM value"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS1E1_CrrAISDownStr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "RxDS1E1_CrrAISDownStr"
            
            def description(self):
                return "AIS Down Stream due to HI_Level AIS of DS1E1"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS1E1_CrrLosAllZero(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RxDS1E1_CrrLosAllZero"
            
            def description(self):
                return "LOS All zero of DS1E1"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDS1E1_CrrAISAllOne(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxDS1E1_CrrAISAllOne"
            
            def description(self):
                return "AIS All one of DS1E1"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1Sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1Sta"
            
            def description(self):
                return "RxDE1Sta 000: Search 001: Shift 010: Wait 011: Verify 100: Fs search 101: Inframe"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDS1E1_CrrE1SSM"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_hw_stat._RxDS1E1_CrrE1SSM()
            allFields["RxDS1E1_CrrAISDownStr"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_hw_stat._RxDS1E1_CrrAISDownStr()
            allFields["RxDS1E1_CrrLosAllZero"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_hw_stat._RxDS1E1_CrrLosAllZero()
            allFields["RxDS1E1_CrrAISAllOne"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_hw_stat._RxDS1E1_CrrAISAllOne()
            allFields["RxDE1Sta"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_hw_stat._RxDE1Sta()
            return allFields

    class _dej1_rx_framer_fbe_cnt(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer FBE Counter"
    
        def description(self):
            return "This is the per channel REI counter for DS1/E1/J1 receive framer"
            
        def width(self):
            return 8
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0005C000 +  2048*Rd2Clr + 32*de3id + 4*de2id + de1id"
            
        def startAddress(self):
            return 0x0005c000
            
        def endAddress(self):
            return 0x0005cfff

        class _DE1FbeCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE1FbeCnt"
            
            def description(self):
                return "DS1/E1/J1 F-bit error accumulator"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE1FbeCnt"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_fbe_cnt._DE1FbeCnt()
            return allFields

    class _dej1_rx_framer_crcerr_cnt(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer CRC Error Counter"
    
        def description(self):
            return "This is the per channel CRC error counter for DS1/E1/J1 receive framer"
            
        def width(self):
            return 8
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0005D000 +  2048*Rd2Clr + 32*de3id + 4*de2id + de1id"
            
        def startAddress(self):
            return 0x0005d000
            
        def endAddress(self):
            return 0x0005dfff

        class _DE1CrcErrCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE1CrcErrCnt"
            
            def description(self):
                return "DS1/E1/J1 CRC Error Accumulator"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE1CrcErrCnt"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_crcerr_cnt._DE1CrcErrCnt()
            return allFields

    class _dej1_rx_framer_rei_cnt(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer REI Counter"
    
        def description(self):
            return "This is the per channel REI counter for DS1/E1/J1 receive framer"
            
        def width(self):
            return 8
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0005E000 +  2048*Rd2Clr + 32*de3id + 4*de2id + de1id"
            
        def startAddress(self):
            return 0x0005e000
            
        def endAddress(self):
            return 0x0005efff

        class _DE1ReiCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE1ReiCnt"
            
            def description(self):
                return "DS1/E1/J1 REI error accumulator"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE1ReiCnt"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_rei_cnt._DE1ReiCnt()
            return allFields

    class _dej1_rx_framer_per_chn_intr_en_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer per Channel Interrupt Enable Control"
    
        def description(self):
            return "This is the per Channel interrupt enable of DS1/E1/J1 Rx Framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00056000 +  StsID*32 + Tug2ID*4 + VtnID"
            
        def startAddress(self):
            return 0x00056000
            
        def endAddress(self):
            return 0x000567ff

        class _DE1BerSdIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "DE1BerSdIntrEn"
            
            def description(self):
                return "Set 1 to enable the BER-SD interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1BerSfIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DE1BerSfIntrEn"
            
            def description(self):
                return "Set 1 to enable the BER-SF interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1oblcIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "DE1oblcIntrEn"
            
            def description(self):
                return "Set 1 to enable the new Outband LoopCode detected to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1iblcIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DE1iblcIntrEn"
            
            def description(self):
                return "Set 1 to enable the new Inband LoopCode detected to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1SigRAIIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DE1SigRAIIntrEn"
            
            def description(self):
                return "Set 1 to enable Signalling RAI event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1SigLOFIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DE1SigLOFIntrEn"
            
            def description(self):
                return "Set 1 to enable Signalling LOF event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1FbeIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "DE1FbeIntrEn"
            
            def description(self):
                return "Set 1 to enable change FBE te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1CrcIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DE1CrcIntrEn"
            
            def description(self):
                return "Set 1 to enable change CRC te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1ReiIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DE1ReiIntrEn"
            
            def description(self):
                return "Set 1 to enable change REI te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LomfIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DE1LomfIntrEn"
            
            def description(self):
                return "Set 1 to enable change LOMF te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LosIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DE1LosIntrEn"
            
            def description(self):
                return "Set 1 to enable change LOS te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1AisIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DE1AisIntrEn"
            
            def description(self):
                return "Set 1 to enable change AIS te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1RaiIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DE1RaiIntrEn"
            
            def description(self):
                return "Set 1 to enable change RAI te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LofIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE1LofIntrEn"
            
            def description(self):
                return "Set 1 to enable change LOF te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE1BerSdIntrEn"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_en_ctrl._DE1BerSdIntrEn()
            allFields["DE1BerSfIntrEn"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_en_ctrl._DE1BerSfIntrEn()
            allFields["DE1oblcIntrEn"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_en_ctrl._DE1oblcIntrEn()
            allFields["DE1iblcIntrEn"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_en_ctrl._DE1iblcIntrEn()
            allFields["DE1SigRAIIntrEn"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_en_ctrl._DE1SigRAIIntrEn()
            allFields["DE1SigLOFIntrEn"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_en_ctrl._DE1SigLOFIntrEn()
            allFields["DE1FbeIntrEn"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_en_ctrl._DE1FbeIntrEn()
            allFields["DE1CrcIntrEn"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_en_ctrl._DE1CrcIntrEn()
            allFields["DE1ReiIntrEn"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_en_ctrl._DE1ReiIntrEn()
            allFields["DE1LomfIntrEn"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_en_ctrl._DE1LomfIntrEn()
            allFields["DE1LosIntrEn"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_en_ctrl._DE1LosIntrEn()
            allFields["DE1AisIntrEn"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_en_ctrl._DE1AisIntrEn()
            allFields["DE1RaiIntrEn"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_en_ctrl._DE1RaiIntrEn()
            allFields["DE1LofIntrEn"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_en_ctrl._DE1LofIntrEn()
            return allFields

    class _dej1_rx_framer_per_chn_intr_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer per Channel Interrupt Status"
    
        def description(self):
            return "This is the per Channel interrupt tus of DS1/E1/J1 Rx Framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00056800 +  StsID*32 + Tug2ID*4 + VtnID"
            
        def startAddress(self):
            return 0x00056800
            
        def endAddress(self):
            return 0x00056fff

        class _DE1BerSdIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "DE1BerSdIntr"
            
            def description(self):
                return "Set 1 if there is a change of BER-SD."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1BerSfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DE1BerSfIntr"
            
            def description(self):
                return "Set 1 if there is a change of BER-SF."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1oblcIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "DE1oblcIntr"
            
            def description(self):
                return "Set 1 if there is a change the new Outband LoopCode detected to generate an interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1iblcIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DE1iblcIntr"
            
            def description(self):
                return "Set 1 if there is a change the new Inband LoopCode detected to generate an interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1SigRAIIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DE1SigRAIIntr"
            
            def description(self):
                return "Set 1 if there is a change Signalling RAI event to generate an interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1SigLOFIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DE1SigLOFIntr"
            
            def description(self):
                return "Set 1 if there is a change Signalling LOF event to generate an interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1FbeIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "DE1FbeIntr"
            
            def description(self):
                return "Set 1 if there is a change in FBE exceed threshold te event."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1CrcIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DE1CrcIntr"
            
            def description(self):
                return "Set 1 if there is a change in CRC exceed threshold te event."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1ReiIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DE1ReiIntr"
            
            def description(self):
                return "Set 1 if there is a change in REI exceed threshold te event."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LomfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DE1LomfIntr"
            
            def description(self):
                return "Set 1 if there is a change in LOMF te event."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LosIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DE1LosIntr"
            
            def description(self):
                return "Set 1 if there is a change in LOS te event."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1AisIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DE1AisIntr"
            
            def description(self):
                return "Set 1 if there is a change in AIS te event."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1RaiIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DE1RaiIntr"
            
            def description(self):
                return "Set 1 if there is a change in RAI te event."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LofIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE1LofIntr"
            
            def description(self):
                return "Set 1 if there is a change in LOF te event."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE1BerSdIntr"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_stat._DE1BerSdIntr()
            allFields["DE1BerSfIntr"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_stat._DE1BerSfIntr()
            allFields["DE1oblcIntr"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_stat._DE1oblcIntr()
            allFields["DE1iblcIntr"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_stat._DE1iblcIntr()
            allFields["DE1SigRAIIntr"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_stat._DE1SigRAIIntr()
            allFields["DE1SigLOFIntr"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_stat._DE1SigLOFIntr()
            allFields["DE1FbeIntr"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_stat._DE1FbeIntr()
            allFields["DE1CrcIntr"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_stat._DE1CrcIntr()
            allFields["DE1ReiIntr"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_stat._DE1ReiIntr()
            allFields["DE1LomfIntr"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_stat._DE1LomfIntr()
            allFields["DE1LosIntr"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_stat._DE1LosIntr()
            allFields["DE1AisIntr"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_stat._DE1AisIntr()
            allFields["DE1RaiIntr"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_stat._DE1RaiIntr()
            allFields["DE1LofIntr"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_stat._DE1LofIntr()
            return allFields

    class _dej1_rx_framer_per_chn_curr_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer per Channel Current Status"
    
        def description(self):
            return "This is the per Channel Current tus of DS1/E1/J1 Rx Framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00057000 +  StsID*32 + Tug2ID*4 + VtnID"
            
        def startAddress(self):
            return 0x00057000
            
        def endAddress(self):
            return 0x000577ff

        class _DE1BerSdIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "DE1BerSdIntr"
            
            def description(self):
                return "Set 1 if there is a change of BER-SD."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1BerSfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DE1BerSfIntr"
            
            def description(self):
                return "Set 1 if there is a change of BER-SF."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1oblcIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "DE1oblcIntrSta"
            
            def description(self):
                return "Current status the new Outband LoopCode detected to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1iblcIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DE1iblcIntrSta"
            
            def description(self):
                return "Current status the new Inband LoopCode detected to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1SigRAIIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DE1SigRAIIntrSta"
            
            def description(self):
                return "Current status Signalling RAI event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1SigLOFIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DE1SigLOFIntrSta"
            
            def description(self):
                return "Current status Signalling LOF event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1FbeCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "DE1FbeCurrSta"
            
            def description(self):
                return "Current tus of FBE exceed threshold event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1CrcCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DE1CrcCurrSta"
            
            def description(self):
                return "Current tus of CRC exceed threshold event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1ReiCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DE1ReiCurrSta"
            
            def description(self):
                return "Current tus of REI exceed threshold event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LomfCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DE1LomfCurrSta"
            
            def description(self):
                return "Current tus of LOMF event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LosCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DE1LosCurrSta"
            
            def description(self):
                return "Current tus of LOS event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1AisCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DE1AisCurrSta"
            
            def description(self):
                return "Current tus of AIS event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1RaiCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DE1RaiCurrSta"
            
            def description(self):
                return "Current tus of RAI event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LofCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE1LofCurrSta"
            
            def description(self):
                return "Current tus of LOF event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE1BerSdIntr"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_curr_stat._DE1BerSdIntr()
            allFields["DE1BerSfIntr"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_curr_stat._DE1BerSfIntr()
            allFields["DE1oblcIntrSta"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_curr_stat._DE1oblcIntrSta()
            allFields["DE1iblcIntrSta"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_curr_stat._DE1iblcIntrSta()
            allFields["DE1SigRAIIntrSta"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_curr_stat._DE1SigRAIIntrSta()
            allFields["DE1SigLOFIntrSta"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_curr_stat._DE1SigLOFIntrSta()
            allFields["DE1FbeCurrSta"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_curr_stat._DE1FbeCurrSta()
            allFields["DE1CrcCurrSta"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_curr_stat._DE1CrcCurrSta()
            allFields["DE1ReiCurrSta"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_curr_stat._DE1ReiCurrSta()
            allFields["DE1LomfCurrSta"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_curr_stat._DE1LomfCurrSta()
            allFields["DE1LosCurrSta"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_curr_stat._DE1LosCurrSta()
            allFields["DE1AisCurrSta"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_curr_stat._DE1AisCurrSta()
            allFields["DE1RaiCurrSta"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_curr_stat._DE1RaiCurrSta()
            allFields["DE1LofCurrSta"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_curr_stat._DE1LofCurrSta()
            return allFields

    class _dej1_rx_framer_per_chn_intr_or_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer per Channel Interrupt OR Status"
    
        def description(self):
            return "The register consists of 28 bits for 28 VT/TUs of the related STS/VC in the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR tus of the related DS1/E1/J1."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x00057800 +  GrpID*32 + STSID"
            
        def startAddress(self):
            return 0x00057800
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE1VtIntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1VtIntrOrSta"
            
            def description(self):
                return "Set to 1 if any interrupt tus bit of corresponding DS1/E1/J1 is set and its interrupt is enabled."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1VtIntrOrSta"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_chn_intr_or_stat._RxDE1VtIntrOrSta()
            return allFields

    class _dej1_rx_framer_per_stsvc_intr_or_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer per STS/VC Interrupt OR Status"
    
        def description(self):
            return "The register consists of 12 bits for 12 STS/VCs of the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR tus of the related STS/VC."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x00057BFE +  GrpID"
            
        def startAddress(self):
            return 0x00057bfe
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE1StsIntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1StsIntrOrSta"
            
            def description(self):
                return "Set to 1 if any interrupt tus bit of corresponding STS/VC is set and its interrupt is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1StsIntrOrSta"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_stsvc_intr_or_stat._RxDE1StsIntrOrSta()
            return allFields

    class _dej1_rx_framer_per_stsvc_intr_en_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer per STS/VC Interrupt Enable Control"
    
        def description(self):
            return "The register consists of 12 interrupt enable bits for 12 STS/VCs in the Rx DS1/E1/J1 Framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00057BFC +  GrpID"
            
        def startAddress(self):
            return 0x00057bfc
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE1StsIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1StsIntrEn"
            
            def description(self):
                return "Set to 1 to enable the related STS/VC to generate interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1StsIntrEn"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_stsvc_intr_en_ctrl._RxDE1StsIntrEn()
            return allFields

    class _dej1_rx_framer_per_grp_intr_or_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer per Group Interrupt OR Status"
    
        def description(self):
            return "The register consists of 2 bits for 2 Group of the Rx DS1/E1/J1 Framer"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00050002
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE1GrpIntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1GrpIntrOrSta"
            
            def description(self):
                return "Set to 1 if any interrupt tus bit of corresponding Group is set and its interrupt is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1GrpIntrOrSta"] = _AF6CNC0022_RD_PDH_v3._dej1_rx_framer_per_grp_intr_or_stat._RxDE1GrpIntrOrSta()
            return allFields

    class _stsvt_map_ctrl(AtRegister.AtRegister):
        def name(self):
            return "STS/VT Map Control"
    
        def description(self):
            return "The STS/VT Map Control is use to configure for per channel STS/VT Map operation."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00076000 +  32*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x00076000
            
        def endAddress(self):
            return 0x000767ff

        class _Pdhoversdhmd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Pdhoversdhmd"
            
            def description(self):
                return "Set 1 to PDH LIU over OCN"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsVtMapAismask(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "StsVtMapAismask"
            
            def description(self):
                return "Set 1 to mask AIS from PDH to OCN"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsVtMapJitIDCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 15
        
            def name(self):
                return "StsVtMapJitIDCfg"
            
            def description(self):
                return "STS/VT Mapping Jitter ID configure, default value is 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsVtMapStuffThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 10
        
            def name(self):
                return "StsVtMapStuffThres"
            
            def description(self):
                return "STS/VT Mapping Stuff Threshold Configure, default value is 12"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsVtMapJitterCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 6
        
            def name(self):
                return "StsVtMapJitterCfg"
            
            def description(self):
                return "STS/VT Mapping Jitter configure - Vtmap: 4'b0000: looptiming mode 4'b1000: fine_stuff_mode, default - Spe Map: 4'b0000: full_stuff_mode 4'b1000: fine_stuff_mode, default"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsVtMapCenterFifo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "StsVtMapCenterFifo"
            
            def description(self):
                return "This bit is set to 1 to force center the mapping FIFO."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsVtMapBypass(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "StsVtMapBypass"
            
            def description(self):
                return "This bit is set to 1 to bypass the STS/VT map"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsVtMapMd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "StsVtMapMd"
            
            def description(self):
                return "STS/VT Mapping mode StsVtMapMd	StsVtMapBypass	Operation Mode 0	          0	E1 to VT2 Map 0	          1	E1 to Bus 1	          0	DS1 to VT1.5 Map 1	          1	DS1 to Bus 2	          0	E3 to VC3 Map 2	          1	E3 to TU3 Map 3	          0	DS3 to VC3 Map 3	          1	DS3 to TU3 Map 4	          x	VT15/VT2 Basic CEP 5	          x	Packet over STS1/VC3 6	          0	STS1/VC3 Fractional CEP carrying VT2/TU12 6	          1	STS1/VC3 Fractional CEP carrying VT15/TU11 7	          0	STS1/VC3 Fractional CEP carrying E3 7	          1	STS1/VC3 Fractional CEP carrying DS3 8	          x	STS1/VC3 Basic CEP 9	          x	Packet over STS3c/VC4 10	        0	STS3c/VC4 Fractional CEP carrying VT2/TU12 10	        1	STS3c/VC4 Fractional CEP carrying VT15/TU11 11	        0	STS3c/VC4 Fractional CEP carrying E3 11	        1	STS3c/VC4 Fractional CEP carrying DS3 12	        x	STS3c/VC4/STS12c/VC4_4c Basic CEP 13	        x	Packet over TU3 14	        0	E3 to Bus 14	        1	DS3 to Bus 15	        x	Disable STS/VC/VT/TU/DS1/E1/DS3/E3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Pdhoversdhmd"] = _AF6CNC0022_RD_PDH_v3._stsvt_map_ctrl._Pdhoversdhmd()
            allFields["StsVtMapAismask"] = _AF6CNC0022_RD_PDH_v3._stsvt_map_ctrl._StsVtMapAismask()
            allFields["StsVtMapJitIDCfg"] = _AF6CNC0022_RD_PDH_v3._stsvt_map_ctrl._StsVtMapJitIDCfg()
            allFields["StsVtMapStuffThres"] = _AF6CNC0022_RD_PDH_v3._stsvt_map_ctrl._StsVtMapStuffThres()
            allFields["StsVtMapJitterCfg"] = _AF6CNC0022_RD_PDH_v3._stsvt_map_ctrl._StsVtMapJitterCfg()
            allFields["StsVtMapCenterFifo"] = _AF6CNC0022_RD_PDH_v3._stsvt_map_ctrl._StsVtMapCenterFifo()
            allFields["StsVtMapBypass"] = _AF6CNC0022_RD_PDH_v3._stsvt_map_ctrl._StsVtMapBypass()
            allFields["StsVtMapMd"] = _AF6CNC0022_RD_PDH_v3._stsvt_map_ctrl._StsVtMapMd()
            return allFields

    class _stsvt_map_hw_stat(AtRegister.AtRegister):
        def name(self):
            return "STS/VT Map HW Status"
    
        def description(self):
            return "for HW debug only"
            
        def width(self):
            return 48
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00074000 +  32*stsid + 4*vtgid + vtid"
            
        def startAddress(self):
            return 0x00074000
            
        def endAddress(self):
            return 0x000747ff

        class _STSVTStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 0
        
            def name(self):
                return "STSVTStatus"
            
            def description(self):
                return "for HW debug only"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["STSVTStatus"] = _AF6CNC0022_RD_PDH_v3._stsvt_map_hw_stat._STSVTStatus()
            return allFields

    class _dej1_tx_framer_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer Control"
    
        def description(self):
            return "DS1/E1/J1 Rx framer control is used to configure for Frame mode (DS1, E1, J1) at the DS1/E1 framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00094000 +  32*de3id + 4*de2id + de1id"
            
        def startAddress(self):
            return 0x00094000
            
        def endAddress(self):
            return 0x000947ff

        class _TxDE1ForceAllOne(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "TxDE1ForceAllOne"
            
            def description(self):
                return "Force all one to TDM side when Line Local Loopback only side"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1FbitBypass(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "TxDE1FbitBypass"
            
            def description(self):
                return "Set 1 to bypass Fbit insertion in Tx DS1/E1 Framer"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1LineAisIns(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "TxDE1LineAisIns"
            
            def description(self):
                return "Set 1 to enable Line AIS Insert"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1PayAisIns(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "TxDE1PayAisIns"
            
            def description(self):
                return "Set 1 to enable Payload AIS Insert"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1RmtLineloop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "TxDE1RmtLineloop"
            
            def description(self):
                return "Set 1 to enable remote Line Loop back"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1RmtPayloop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "TxDE1RmtPayloop"
            
            def description(self):
                return "Set 1 to enable remote Payload Loop back. Sharing for VT Loopback in case of CEP path"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1AutoAis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "TxDE1AutoAis"
            
            def description(self):
                return "Set 1 to enable AIS indication from data map block to automatically transmit all 1s at DS1/E1/J1 Tx framer"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1DLCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 9
        
            def name(self):
                return "TxDE1DLCfg"
            
            def description(self):
                return "Incase E1 mode : use for SSM in Sa, bit[15:13] is Sa position 0=disable, 3=Sa4, 4=Sa5, 5=Sa6, 6=Sa7,7=Sa8. Bit[12:9] is SSM value. Incase DS1 mode : bit[12] Set 1 to Enable FDL transmit , dont care other bits"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1AutoYel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TxDE1AutoYel"
            
            def description(self):
                return "Auto Yellow generation enable 1: Yellow alarm detected from Rx Framer will be automatically transmitted in Tx Framer 0: No automatically Yellow alarm transmitted"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1FrcYel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "TxDE1FrcYel"
            
            def description(self):
                return "SW force to Tx Yellow alarm"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1AutoCrcErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "TxDE1AutoCrcErr"
            
            def description(self):
                return "Auto CRC error enable 1: CRC Error detected from Rx Framer will be automatically transmitted in REI bit of Tx Framer 0: No automatically CRC Error alarm transmitted"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1FrcCrcErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "TxDE1FrcCrcErr"
            
            def description(self):
                return "SW force to Tx CRC Error alarm (REI bit)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "TxDE1En"
            
            def description(self):
                return "Set 1 to enable the DS1/E1/J1 framer."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1Md(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1Md"
            
            def description(self):
                return "Receive DS1/J1 framing mode 0000: DS1/J1 Unframe 0001: DS1 SF (D4) 0010: DS1 ESF 0011: DS1 DDS 0100: DS1 SLC 0101: J1 SF 0110: J1 ESF 1000: E1 Unframe 1001: E1 Basic Frame 1010: E1 CRC4 Frame"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1ForceAllOne"] = _AF6CNC0022_RD_PDH_v3._dej1_tx_framer_ctrl._TxDE1ForceAllOne()
            allFields["TxDE1FbitBypass"] = _AF6CNC0022_RD_PDH_v3._dej1_tx_framer_ctrl._TxDE1FbitBypass()
            allFields["TxDE1LineAisIns"] = _AF6CNC0022_RD_PDH_v3._dej1_tx_framer_ctrl._TxDE1LineAisIns()
            allFields["TxDE1PayAisIns"] = _AF6CNC0022_RD_PDH_v3._dej1_tx_framer_ctrl._TxDE1PayAisIns()
            allFields["TxDE1RmtLineloop"] = _AF6CNC0022_RD_PDH_v3._dej1_tx_framer_ctrl._TxDE1RmtLineloop()
            allFields["TxDE1RmtPayloop"] = _AF6CNC0022_RD_PDH_v3._dej1_tx_framer_ctrl._TxDE1RmtPayloop()
            allFields["TxDE1AutoAis"] = _AF6CNC0022_RD_PDH_v3._dej1_tx_framer_ctrl._TxDE1AutoAis()
            allFields["TxDE1DLCfg"] = _AF6CNC0022_RD_PDH_v3._dej1_tx_framer_ctrl._TxDE1DLCfg()
            allFields["TxDE1AutoYel"] = _AF6CNC0022_RD_PDH_v3._dej1_tx_framer_ctrl._TxDE1AutoYel()
            allFields["TxDE1FrcYel"] = _AF6CNC0022_RD_PDH_v3._dej1_tx_framer_ctrl._TxDE1FrcYel()
            allFields["TxDE1AutoCrcErr"] = _AF6CNC0022_RD_PDH_v3._dej1_tx_framer_ctrl._TxDE1AutoCrcErr()
            allFields["TxDE1FrcCrcErr"] = _AF6CNC0022_RD_PDH_v3._dej1_tx_framer_ctrl._TxDE1FrcCrcErr()
            allFields["TxDE1En"] = _AF6CNC0022_RD_PDH_v3._dej1_tx_framer_ctrl._TxDE1En()
            allFields["RxDE1Md"] = _AF6CNC0022_RD_PDH_v3._dej1_tx_framer_ctrl._RxDE1Md()
            return allFields

    class _dej1_tx_framer_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer Status"
    
        def description(self):
            return "These registers are used for Hardware tus only"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00095000 +  32*de3id + 4*de2id + de1id"
            
        def startAddress(self):
            return 0x00095000
            
        def endAddress(self):
            return 0x000957ff

        class _TxDE1Sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE1Sta"
            
            def description(self):
                return "HW status only"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1Sta"] = _AF6CNC0022_RD_PDH_v3._dej1_tx_framer_stat._TxDE1Sta()
            return allFields

    class _dej1_tx_framer_sign_insertion_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer Signaling Insertion Control"
    
        def description(self):
            return "DS1/E1/J1 Rx framer signaling insertion control is used to configure for signaling operation modes at the DS1/E1 framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00094800 +  32*de3id + 4*de2id + de1id"
            
        def startAddress(self):
            return 0x00094800
            
        def endAddress(self):
            return 0x00094fff

        class _TxDE1SigMfrmEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "TxDE1SigMfrmEn"
            
            def description(self):
                return "Set 1 to enable the Tx DS1/E1/J1 framer to sync the signaling multiframe to the incoming data flow. No applicable for DS1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1SigBypass(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE1SigBypass"
            
            def description(self):
                return "30 signaling bypass bit for 32 DS0 in an E1 frame. In DS1 mode, only 24 LSB bits is used. Set 1 to bypass Signaling insertion in Tx DS1/E1 Framer"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1SigMfrmEn"] = _AF6CNC0022_RD_PDH_v3._dej1_tx_framer_sign_insertion_ctrl._TxDE1SigMfrmEn()
            allFields["TxDE1SigBypass"] = _AF6CNC0022_RD_PDH_v3._dej1_tx_framer_sign_insertion_ctrl._TxDE1SigBypass()
            return allFields

    class _dej1_errins_en_cfg(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Payload Error Insert Enable Configuration"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00090002
            
        def endAddress(self):
            return 0xffffffff

        class _TxDE1errorins_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "TxDE1errorins_en"
            
            def description(self):
                return "Force payload error, inclued CRC"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1errorfbit_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "TxDE1errorfbit_en"
            
            def description(self):
                return "Force Fbit error"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1ErrInsID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE1ErrInsID"
            
            def description(self):
                return "Force ID channel"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1errorins_en"] = _AF6CNC0022_RD_PDH_v3._dej1_errins_en_cfg._TxDE1errorins_en()
            allFields["TxDE1errorfbit_en"] = _AF6CNC0022_RD_PDH_v3._dej1_errins_en_cfg._TxDE1errorfbit_en()
            allFields["TxDE1ErrInsID"] = _AF6CNC0022_RD_PDH_v3._dej1_errins_en_cfg._TxDE1ErrInsID()
            return allFields

    class _dej1_errins_thr_cfg(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Payload Error Insert threshold Configuration"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00090003
            
        def endAddress(self):
            return 0xffffffff

        class _TxDE1PayErrInsThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE1PayErrInsThr"
            
            def description(self):
                return "Error Threshold in byte unit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1PayErrInsThr"] = _AF6CNC0022_RD_PDH_v3._dej1_errins_thr_cfg._TxDE1PayErrInsThr()
            return allFields

    class _dej1_tx_framer_dlk_ins_bom_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer DLK Insertion BOM Control"
    
        def description(self):
            return "This memory is used to control transmitting Bit-Oriented Message (BOM). This memory is accessed indirectly via indirect Access registers"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x097000 +  de3id*32 + de2id*4 + de1id"
            
        def startAddress(self):
            return 0x00097000
            
        def endAddress(self):
            return 0x000977ff

        class _DlkBOMEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DlkBOMEn"
            
            def description(self):
                return "enable transmitting BOM. By setting this field to 1, DLK engine will rt to insert 6-bit into the BOM pattern 111111110xxxxxx0 with the MSB is transmitted first. When transmitting finishes, engine automatically clears this bit to inform to CPU that the BOM message has been already transmitted."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DlkBOMRptTime(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DlkBOMRptTime"
            
            def description(self):
                return "indicate the number of time that the BOM is transmitted repeatedly."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DlkBOMMsg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DlkBOMMsg"
            
            def description(self):
                return "6-bit BOM message in pattern BOM 111111110xxxxxx0 in which the MSB is transmitted first."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DlkBOMEn"] = _AF6CNC0022_RD_PDH_v3._dej1_tx_framer_dlk_ins_bom_ctrl._DlkBOMEn()
            allFields["DlkBOMRptTime"] = _AF6CNC0022_RD_PDH_v3._dej1_tx_framer_dlk_ins_bom_ctrl._DlkBOMRptTime()
            allFields["DlkBOMMsg"] = _AF6CNC0022_RD_PDH_v3._dej1_tx_framer_dlk_ins_bom_ctrl._DlkBOMMsg()
            return allFields

    class _txm23e23_ctrl(AtRegister.AtRegister):
        def name(self):
            return "TxM23E23 Control"
    
        def description(self):
            return "The TxM23E23 Control is use to configure for per channel Tx DS3/E3 operation."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00080000 +  de3id"
            
        def startAddress(self):
            return 0x00080000
            
        def endAddress(self):
            return 0x0008003f

        class _TxDS3UnfrmAISMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "TxDS3UnfrmAISMode"
            
            def description(self):
                return "Set 1 to send out AIS signal pattern in DS3Unframe, default set 0 to send out AIS all 1's pattern in DS3Unframe"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3FrcAllone(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "TxDS3E3FrcAllone"
            
            def description(self):
                return "Force All One to remote only for Line Local Loopback(control bit5 of RX M23 0x40000)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3ChEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "TxDS3E3ChEnb"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3DLKMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 26
        
            def name(self):
                return "TxDS3E3DLKMode"
            
            def description(self):
                return "0:DS3DL/E3G751NA/E3G832GC; 1:DS3UDL/E3G832NR; 2: DS3FEAC; 3:DS3NA"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3DLKEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "TxDS3E3DLKEnb"
            
            def description(self):
                return "Set 1 to enable DLK DS3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3FeacThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 21
        
            def name(self):
                return "TxDS3E3FeacThres"
            
            def description(self):
                return "Number of word repeat, 0xF for send continuos"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3LineAllOne(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "TxDS3E3LineAllOne"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3PayLoop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "TxDS3E3PayLoop"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3LineLoop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "TxDS3E3LineLoop"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3FrcYel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "TxDS3E3FrcYel"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3AutoYel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "TxDS3E3AutoYel"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3LoopMd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 14
        
            def name(self):
                return "TxDS3E3LoopMd"
            
            def description(self):
                return "DS3 C-bit mode: [15:14]: FE Control: 00: idle code; 01: activate cw; 10: de-activate cw; 11: alrm/status E3G832 mode: [15:14]: DL mode Normal: Loop Mode configuration"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3LoopEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 7
        
            def name(self):
                return "TxDS3E3LoopEn"
            
            def description(self):
                return "DS3 C-bit mode: [12:7]: FEAC Control Word E3G832 mode: [13:11]: Payload type [10:7]:  SSM Config Normal: [13:7]: Loop en configuration per channel"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3Ohbypass(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "TxDS3E3Ohbypass"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3IdleSet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "TxDS3E3IdleSet"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3AisSet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "TxDS3E3AisSet"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDS3E3Md(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDS3E3Md"
            
            def description(self):
                return "Tx DS3/E3 framing mode 0000: DS3 Unframed 0001: DS3 C-bit parity carrying 28 DS1s or 21 E1s 0010: DS3 M23 carrying 28 DS1s or 21 E1s 0011: DS3 C-bit map 0100: E3 Unframed 0101: E3 G832 0110: E3 G.751 carrying 16 E1s 0111: E3 G751 Map 1000: Bypass RxM13E13 (DS1/E1 mode)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDS3UnfrmAISMode"] = _AF6CNC0022_RD_PDH_v3._txm23e23_ctrl._TxDS3UnfrmAISMode()
            allFields["TxDS3E3FrcAllone"] = _AF6CNC0022_RD_PDH_v3._txm23e23_ctrl._TxDS3E3FrcAllone()
            allFields["TxDS3E3ChEnb"] = _AF6CNC0022_RD_PDH_v3._txm23e23_ctrl._TxDS3E3ChEnb()
            allFields["TxDS3E3DLKMode"] = _AF6CNC0022_RD_PDH_v3._txm23e23_ctrl._TxDS3E3DLKMode()
            allFields["TxDS3E3DLKEnb"] = _AF6CNC0022_RD_PDH_v3._txm23e23_ctrl._TxDS3E3DLKEnb()
            allFields["TxDS3E3FeacThres"] = _AF6CNC0022_RD_PDH_v3._txm23e23_ctrl._TxDS3E3FeacThres()
            allFields["TxDS3E3LineAllOne"] = _AF6CNC0022_RD_PDH_v3._txm23e23_ctrl._TxDS3E3LineAllOne()
            allFields["TxDS3E3PayLoop"] = _AF6CNC0022_RD_PDH_v3._txm23e23_ctrl._TxDS3E3PayLoop()
            allFields["TxDS3E3LineLoop"] = _AF6CNC0022_RD_PDH_v3._txm23e23_ctrl._TxDS3E3LineLoop()
            allFields["TxDS3E3FrcYel"] = _AF6CNC0022_RD_PDH_v3._txm23e23_ctrl._TxDS3E3FrcYel()
            allFields["TxDS3E3AutoYel"] = _AF6CNC0022_RD_PDH_v3._txm23e23_ctrl._TxDS3E3AutoYel()
            allFields["TxDS3E3LoopMd"] = _AF6CNC0022_RD_PDH_v3._txm23e23_ctrl._TxDS3E3LoopMd()
            allFields["TxDS3E3LoopEn"] = _AF6CNC0022_RD_PDH_v3._txm23e23_ctrl._TxDS3E3LoopEn()
            allFields["TxDS3E3Ohbypass"] = _AF6CNC0022_RD_PDH_v3._txm23e23_ctrl._TxDS3E3Ohbypass()
            allFields["TxDS3E3IdleSet"] = _AF6CNC0022_RD_PDH_v3._txm23e23_ctrl._TxDS3E3IdleSet()
            allFields["TxDS3E3AisSet"] = _AF6CNC0022_RD_PDH_v3._txm23e23_ctrl._TxDS3E3AisSet()
            allFields["TxDS3E3Md"] = _AF6CNC0022_RD_PDH_v3._txm23e23_ctrl._TxDS3E3Md()
            return allFields

    class _txm23e23_ctrl2(AtRegister.AtRegister):
        def name(self):
            return "TxM23E23 Control2"
    
        def description(self):
            return "The TxM23E23 Control is use to configure for per channel Tx DS3/E3 operation."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00080040 +  de3id"
            
        def startAddress(self):
            return 0x00080040
            
        def endAddress(self):
            return 0x0008007f

        class _TxDS3E3AutoFEBE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TxDS3E3AutoFEBE"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DS3Cbit_SSMEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "DS3Cbit_SSMEn"
            
            def description(self):
                return "DS3 Cbit SSM message to transmit Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SSMInv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "SSMInv"
            
            def description(self):
                return "Set 1 to invert SSM - 1:invert mode , SSM format 11111111_11_0xxxxxx0 - 1:non_invert mode , SSM format 0xxxxxx0_11_11111111"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SSMMess(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SSMMess"
            
            def description(self):
                return "SSM Message - [3:0]: G832 SSM Message - [5:0]: G832 SSM Message"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDS3E3AutoFEBE"] = _AF6CNC0022_RD_PDH_v3._txm23e23_ctrl2._TxDS3E3AutoFEBE()
            allFields["DS3Cbit_SSMEn"] = _AF6CNC0022_RD_PDH_v3._txm23e23_ctrl2._DS3Cbit_SSMEn()
            allFields["SSMInv"] = _AF6CNC0022_RD_PDH_v3._txm23e23_ctrl2._SSMInv()
            allFields["SSMMess"] = _AF6CNC0022_RD_PDH_v3._txm23e23_ctrl2._SSMMess()
            return allFields

    class _txm23e23_hw_stat(AtRegister.AtRegister):
        def name(self):
            return "TxM23E23 HW Status"
    
        def description(self):
            return "for HW debug only"
            
        def width(self):
            return 48
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00080080 +  de3id"
            
        def startAddress(self):
            return 0x00080080
            
        def endAddress(self):
            return 0x000800bf

        class _TxM23E23Status(AtRegister.AtRegisterField):
            def stopBit(self):
                return 42
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxM23E23Status"
            
            def description(self):
                return "for HW debug only"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxM23E23Status"] = _AF6CNC0022_RD_PDH_v3._txm23e23_hw_stat._TxM23E23Status()
            return allFields

    class _txm12e12_ctrl(AtRegister.AtRegister):
        def name(self):
            return "TxM12E12 Control"
    
        def description(self):
            return "The TxM12E12 Control is use to configure for per channel Tx DS2/E2 operation."
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00081000 +  8*de3id + de2id"
            
        def startAddress(self):
            return 0x00081000
            
        def endAddress(self):
            return 0x000811ff

        class _TxDS2E2Md(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDS2E2Md"
            
            def description(self):
                return "Tx DS2/E2 framing mode 00: DS2 T1.107 carrying 4 DS1s 01: DS2 G.747 carrying 3 E1s 10: E2 G.742 carrying 4 E1s 11: Reserved"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDS2E2Md"] = _AF6CNC0022_RD_PDH_v3._txm12e12_ctrl._TxDS2E2Md()
            return allFields

    class _txm12e12_hw_stat(AtRegister.AtRegister):
        def name(self):
            return "TxM12E12 HW Status"
    
        def description(self):
            return "for HW debug only"
            
        def width(self):
            return 96
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00081200 +  8*de3id + de2id"
            
        def startAddress(self):
            return 0x00081200
            
        def endAddress(self):
            return 0x000813ff

        class _TxM12E12Status(AtRegister.AtRegisterField):
            def stopBit(self):
                return 80
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxM12E12Status"
            
            def description(self):
                return "for HW debug only"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxM12E12Status"] = _AF6CNC0022_RD_PDH_v3._txm12e12_hw_stat._TxM12E12Status()
            return allFields

    class _upen_lim_err(AtRegister.AtRegister):
        def name(self):
            return "Config Limit error"
    
        def description(self):
            return "Check pattern error in state matching pattern"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0002
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_lim_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_lim_err"
            
            def description(self):
                return "if cnt err more than limit error,search failed"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_lim_err"] = _AF6CNC0022_RD_PDH_v3._upen_lim_err._cfg_lim_err()
            return allFields

    class _upen_lim_mat(AtRegister.AtRegister):
        def name(self):
            return "Config limit match"
    
        def description(self):
            return "Used to check parttern match"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0003
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_lim_mat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_lim_mat"
            
            def description(self):
                return "if cnt match more than limit mat, serch ok"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_lim_mat"] = _AF6CNC0022_RD_PDH_v3._upen_lim_mat._cfg_lim_mat()
            return allFields

    class _upen_th_fdl_mat(AtRegister.AtRegister):
        def name(self):
            return "Config thresh fdl parttern"
    
        def description(self):
            return "Check fdl message codes match"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0004
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_th_fdl_mat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_th_fdl_mat"
            
            def description(self):
                return "if message codes repeat more than thresh, search ok,"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_th_fdl_mat"] = _AF6CNC0022_RD_PDH_v3._upen_th_fdl_mat._cfg_th_fdl_mat()
            return allFields

    class _upen_th_err(AtRegister.AtRegister):
        def name(self):
            return "Config thersh error"
    
        def description(self):
            return "Max error for one pattern"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0005
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_th_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_th_err"
            
            def description(self):
                return "if pattern error more than thresh , change to check another pattern"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_th_err"] = _AF6CNC0022_RD_PDH_v3._upen_th_err._cfg_th_err()
            return allFields

    class _upen_info(AtRegister.AtRegister):
        def name(self):
            return "Current Inband Code"
    
        def description(self):
            return "Used to report current status of chid"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0xE_2100 + id"
            
        def startAddress(self):
            return 0x000e2100
            
        def endAddress(self):
            return 0x000e27ff

        class _updo_sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_sta"
            
            def description(self):
                return "bit [2:0] == 0x0  CSU UP bit [2:0] == 0x1  CSU DOWN bit [2:0] == 0x2  FAC1 UP bit [2:0] == 0x3  FAC1 DOWN bit [2:0] == 0x4  FAC2 UP bit [2:0] == 0x5  FAC2 DOWN bit [2:0] == 0x7  Change from LoopCode to nomal"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_sta"] = _AF6CNC0022_RD_PDH_v3._upen_info._updo_sta()
            return allFields

    class _upen_fdlpat(AtRegister.AtRegister):
        def name(self):
            return "Config User programmble Pattern User Codes"
    
        def description(self):
            return "check fdl message codes"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e08f8
            
        def endAddress(self):
            return 0xffffffff

        class _updo_fdl_mess(AtRegister.AtRegisterField):
            def stopBit(self):
                return 55
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_fdl_mess"
            
            def description(self):
                return "mess codes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_fdl_mess"] = _AF6CNC0022_RD_PDH_v3._upen_fdlpat._updo_fdl_mess()
            return allFields

    class _upen_fdl_info(AtRegister.AtRegister):
        def name(self):
            return "Currenr Message FDL Detected"
    
        def description(self):
            return "Info fdl message detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0xE_1800 + $fdl_len"
            
        def startAddress(self):
            return 0x000e1800
            
        def endAddress(self):
            return 0x000e1fff

        class _mess_info_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mess_info_l"
            
            def description(self):
                return "message info bit[15:8] :	8bit BOM pattern format 0xxxxxx0 bit[4]    : set 1 indicate BOM message  bit[3:0]== 0x0 11111111_01111110 bit[3:0]== 0x1 line loop up bit[3:0]== 0x2 line loop down bit[3:0]== 0x3 payload loop up bit[3:0]== 0x4 payload loop down bit[3:0]== 0x5 smartjack act bit[3:0]== 0x6 smartjact deact bit[3:0]== 0x7 idle"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mess_info_l"] = _AF6CNC0022_RD_PDH_v3._upen_fdl_info._mess_info_l()
            return allFields

    class _upen_th_stt_int(AtRegister.AtRegister):
        def name(self):
            return "Config Theshold Pattern Report"
    
        def description(self):
            return "Maximum Pattern Report"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0006
            
        def endAddress(self):
            return 0xffffffff

        class _updo_th_stt_int(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_th_stt_int"
            
            def description(self):
                return "if number of pattern detected more than threshold, interrupt enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_th_stt_int"] = _AF6CNC0022_RD_PDH_v3._upen_th_stt_int._updo_th_stt_int()
            return allFields

    class _RAM_Parity_Force_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM Parity Force Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _PDHVTAsyncMapCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PDHVTAsyncMapCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH VT Async Map Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSTSVTMapCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PDHSTSVTMapCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH STS/VT Map Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM23E23Trace_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PDHTxM23E23Trace_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH TxM23E23 E3g832 Trace Byte\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM23E23Ctrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PDHTxM23E23Ctrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH TxM23E23 Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM12E12Ctrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PDHTxM12E12Ctrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH TxM12E12 Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxDE1SigCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PDHTxDE1SigCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH DS1/E1/J1 Tx Framer Signalling Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxDE1FrmCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PDHTxDE1FrmCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH DS1/E1/J1 Tx Framer Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxDE1FrmCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PDHRxDE1FrmCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH DS1/E1/J1 Rx Framer Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxM12E12Ctrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PDHRxM12E12Ctrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH RxM12E12 Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxDS3E3OHCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PDHRxDS3E3OHCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH RxDS3E3 OH Pro Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxM23E23Ctrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PDHRxM23E23Ctrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH RxM23E23 Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSTSVTDeMapCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PDHSTSVTDeMapCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH STS/VT Demap Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHMuxCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHMuxCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"PDH DS1/E1/J1 Rx Framer Mux Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHVTAsyncMapCtrl_ParErrFrc"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Force_Control._PDHVTAsyncMapCtrl_ParErrFrc()
            allFields["PDHSTSVTMapCtrl_ParErrFrc"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Force_Control._PDHSTSVTMapCtrl_ParErrFrc()
            allFields["PDHTxM23E23Trace_ParErrFrc"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Force_Control._PDHTxM23E23Trace_ParErrFrc()
            allFields["PDHTxM23E23Ctrl_ParErrFrc"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Force_Control._PDHTxM23E23Ctrl_ParErrFrc()
            allFields["PDHTxM12E12Ctrl_ParErrFrc"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Force_Control._PDHTxM12E12Ctrl_ParErrFrc()
            allFields["PDHTxDE1SigCtrl_ParErrFrc"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Force_Control._PDHTxDE1SigCtrl_ParErrFrc()
            allFields["PDHTxDE1FrmCtrl_ParErrFrc"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Force_Control._PDHTxDE1FrmCtrl_ParErrFrc()
            allFields["PDHRxDE1FrmCtrl_ParErrFrc"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Force_Control._PDHRxDE1FrmCtrl_ParErrFrc()
            allFields["PDHRxM12E12Ctrl_ParErrFrc"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Force_Control._PDHRxM12E12Ctrl_ParErrFrc()
            allFields["PDHRxDS3E3OHCtrl_ParErrFrc"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Force_Control._PDHRxDS3E3OHCtrl_ParErrFrc()
            allFields["PDHRxM23E23Ctrl_ParErrFrc"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Force_Control._PDHRxM23E23Ctrl_ParErrFrc()
            allFields["PDHSTSVTDeMapCtrl_ParErrFrc"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Force_Control._PDHSTSVTDeMapCtrl_ParErrFrc()
            allFields["PDHMuxCtrl_ParErrFrc"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Force_Control._PDHMuxCtrl_ParErrFrc()
            return allFields

    class _RAM_Parity_Disable_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM Parity Disable Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0xffffffff

        class _PDHVTAsyncMapCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PDHVTAsyncMapCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH VT Async Map Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSTSVTMapCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PDHSTSVTMapCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH STS/VT Map Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM23E23Trace_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PDHTxM23E23Trace_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH TxM23E23 E3g832 Trace Byte\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM23E23Ctrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PDHTxM23E23Ctrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH TxM23E23 Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM12E12Ctrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PDHTxM12E12Ctrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH TxM12E12 Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxDE1SigCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PDHTxDE1SigCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH DS1/E1/J1 Tx Framer Signalling Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxDE1FrmCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PDHTxDE1FrmCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH DS1/E1/J1 Tx Framer Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxDE1FrmCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PDHRxDE1FrmCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH DS1/E1/J1 Rx Framer Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxM12E12Ctrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PDHRxM12E12Ctrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH RxM12E12 Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxDS3E3OHCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PDHRxDS3E3OHCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH RxDS3E3 OH Pro Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxM23E23Ctrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PDHRxM23E23Ctrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH RxM23E23 Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSTSVTDeMapCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PDHSTSVTDeMapCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH STS/VT Demap Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHMuxCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHMuxCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"PDH DS1/E1/J1 Rx Framer Mux Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHVTAsyncMapCtrl_ParErrDis"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Disable_Control._PDHVTAsyncMapCtrl_ParErrDis()
            allFields["PDHSTSVTMapCtrl_ParErrDis"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Disable_Control._PDHSTSVTMapCtrl_ParErrDis()
            allFields["PDHTxM23E23Trace_ParErrDis"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Disable_Control._PDHTxM23E23Trace_ParErrDis()
            allFields["PDHTxM23E23Ctrl_ParErrDis"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Disable_Control._PDHTxM23E23Ctrl_ParErrDis()
            allFields["PDHTxM12E12Ctrl_ParErrDis"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Disable_Control._PDHTxM12E12Ctrl_ParErrDis()
            allFields["PDHTxDE1SigCtrl_ParErrDis"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Disable_Control._PDHTxDE1SigCtrl_ParErrDis()
            allFields["PDHTxDE1FrmCtrl_ParErrDis"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Disable_Control._PDHTxDE1FrmCtrl_ParErrDis()
            allFields["PDHRxDE1FrmCtrl_ParErrDis"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Disable_Control._PDHRxDE1FrmCtrl_ParErrDis()
            allFields["PDHRxM12E12Ctrl_ParErrDis"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Disable_Control._PDHRxM12E12Ctrl_ParErrDis()
            allFields["PDHRxDS3E3OHCtrl_ParErrDis"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Disable_Control._PDHRxDS3E3OHCtrl_ParErrDis()
            allFields["PDHRxM23E23Ctrl_ParErrDis"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Disable_Control._PDHRxM23E23Ctrl_ParErrDis()
            allFields["PDHSTSVTDeMapCtrl_ParErrDis"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Disable_Control._PDHSTSVTDeMapCtrl_ParErrDis()
            allFields["PDHMuxCtrl_ParErrDis"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Disable_Control._PDHMuxCtrl_ParErrDis()
            return allFields

    class _RAM_Parity_Error_Sticky(AtRegister.AtRegister):
        def name(self):
            return "RAM parity Error Sticky"
    
        def description(self):
            return "This register configures disable parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000012
            
        def endAddress(self):
            return 0xffffffff

        class _PDHVTAsyncMapCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PDHVTAsyncMapCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH VT Async Map Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSTSVTMapCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PDHSTSVTMapCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH STS/VT Map Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM23E23Trace_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PDHTxM23E23Trace_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH TxM23E23 E3g832 Trace Byte\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM23E23Ctrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PDHTxM23E23Ctrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH TxM23E23 Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxM12E12Ctrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PDHTxM12E12Ctrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH TxM12E12 Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxDE1SigCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PDHTxDE1SigCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH DS1/E1/J1 Tx Framer Signalling Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHTxDE1FrmCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PDHTxDE1FrmCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH DS1/E1/J1 Tx Framer Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxDE1FrmCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PDHRxDE1FrmCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH DS1/E1/J1 Rx Framer Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxM12E12Ctrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PDHRxM12E12Ctrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH RxM12E12 Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxDS3E3OHCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PDHRxDS3E3OHCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH RxDS3E3 OH Pro Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHRxM23E23Ctrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PDHRxM23E23Ctrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH RxM23E23 Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSTSVTDeMapCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PDHSTSVTDeMapCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH STS/VT Demap Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHMuxCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHMuxCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"PDH DS1/E1/J1 Rx Framer Mux Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHVTAsyncMapCtrl_ParErrStk"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Error_Sticky._PDHVTAsyncMapCtrl_ParErrStk()
            allFields["PDHSTSVTMapCtrl_ParErrStk"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Error_Sticky._PDHSTSVTMapCtrl_ParErrStk()
            allFields["PDHTxM23E23Trace_ParErrStk"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Error_Sticky._PDHTxM23E23Trace_ParErrStk()
            allFields["PDHTxM23E23Ctrl_ParErrStk"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Error_Sticky._PDHTxM23E23Ctrl_ParErrStk()
            allFields["PDHTxM12E12Ctrl_ParErrStk"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Error_Sticky._PDHTxM12E12Ctrl_ParErrStk()
            allFields["PDHTxDE1SigCtrl_ParErrStk"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Error_Sticky._PDHTxDE1SigCtrl_ParErrStk()
            allFields["PDHTxDE1FrmCtrl_ParErrStk"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Error_Sticky._PDHTxDE1FrmCtrl_ParErrStk()
            allFields["PDHRxDE1FrmCtrl_ParErrStk"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Error_Sticky._PDHRxDE1FrmCtrl_ParErrStk()
            allFields["PDHRxM12E12Ctrl_ParErrStk"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Error_Sticky._PDHRxM12E12Ctrl_ParErrStk()
            allFields["PDHRxDS3E3OHCtrl_ParErrStk"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Error_Sticky._PDHRxDS3E3OHCtrl_ParErrStk()
            allFields["PDHRxM23E23Ctrl_ParErrStk"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Error_Sticky._PDHRxM23E23Ctrl_ParErrStk()
            allFields["PDHSTSVTDeMapCtrl_ParErrStk"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Error_Sticky._PDHSTSVTDeMapCtrl_ParErrStk()
            allFields["PDHMuxCtrl_ParErrStk"] = _AF6CNC0022_RD_PDH_v3._RAM_Parity_Error_Sticky._PDHMuxCtrl_ParErrStk()
            return allFields

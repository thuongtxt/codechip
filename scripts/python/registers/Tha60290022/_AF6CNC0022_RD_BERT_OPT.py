import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0022_RD_BERT_OPT(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["sel_ts_gen"] = _AF6CNC0022_RD_BERT_OPT._sel_ts_gen()
        allRegisters["sel_mon_tstdm"] = _AF6CNC0022_RD_BERT_OPT._sel_mon_tstdm()
        allRegisters["sel_ho_bert_gen"] = _AF6CNC0022_RD_BERT_OPT._sel_ho_bert_gen()
        allRegisters["sel_bert_montdm"] = _AF6CNC0022_RD_BERT_OPT._sel_bert_montdm()
        allRegisters["ctrl_pen_gen"] = _AF6CNC0022_RD_BERT_OPT._ctrl_pen_gen()
        allRegisters["txfix_gen"] = _AF6CNC0022_RD_BERT_OPT._txfix_gen()
        allRegisters["ctrl_pen_montdm"] = _AF6CNC0022_RD_BERT_OPT._ctrl_pen_montdm()
        allRegisters["rxfix_gen"] = _AF6CNC0022_RD_BERT_OPT._rxfix_gen()
        allRegisters["ctrl_ber_pen"] = _AF6CNC0022_RD_BERT_OPT._ctrl_ber_pen()
        allRegisters["goodbit_pen_tdm_gen"] = _AF6CNC0022_RD_BERT_OPT._goodbit_pen_tdm_gen()
        allRegisters["goodbit_pen_tdm_mon"] = _AF6CNC0022_RD_BERT_OPT._goodbit_pen_tdm_mon()
        allRegisters["err_tdm_mon"] = _AF6CNC0022_RD_BERT_OPT._err_tdm_mon()
        allRegisters["lossyn_pen_tdm_mon"] = _AF6CNC0022_RD_BERT_OPT._lossyn_pen_tdm_mon()
        allRegisters["lossbit_pen_tdm_mon"] = _AF6CNC0022_RD_BERT_OPT._lossbit_pen_tdm_mon()
        allRegisters["singe_ber_pen"] = _AF6CNC0022_RD_BERT_OPT._singe_ber_pen()
        allRegisters["stt_tdm_mon"] = _AF6CNC0022_RD_BERT_OPT._stt_tdm_mon()
        return allRegisters

    class _sel_ts_gen(AtRegister.AtRegister):
        def name(self):
            return "Sel Timeslot Gen"
    
        def description(self):
            return "The registers select 1id in line to gen bert data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4102 + 512*engid"
            
        def startAddress(self):
            return 0x00004102
            
        def endAddress(self):
            return 0x00007702

        class _gen_tsen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "gen_tsen"
            
            def description(self):
                return "bit map indicase 32 timeslot,\"1\" : enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["gen_tsen"] = _AF6CNC0022_RD_BERT_OPT._sel_ts_gen._gen_tsen()
            return allFields

    class _sel_mon_tstdm(AtRegister.AtRegister):
        def name(self):
            return "Sel Timeslot Mon tdm"
    
        def description(self):
            return "The registers select 1id in line to gen bert data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8102 + 512*engid"
            
        def startAddress(self):
            return 0x00008102
            
        def endAddress(self):
            return 0x0000b102

        class _tdm_tsen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tdm_tsen"
            
            def description(self):
                return "bit map indicase 32 timeslot,\"1\" : enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tdm_tsen"] = _AF6CNC0022_RD_BERT_OPT._sel_mon_tstdm._tdm_tsen()
            return allFields

    class _sel_ho_bert_gen(AtRegister.AtRegister):
        def name(self):
            return "Sel Ho Bert Gen"
    
        def description(self):
            return "The registers select id in line to gen bert data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_200 + engid"
            
        def startAddress(self):
            return 0x00000200
            
        def endAddress(self):
            return 0x0000021b

        class _gen_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "gen_en"
            
            def description(self):
                return "set \"1\" to enable bert gen"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _lineid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 11
        
            def name(self):
                return "lineid"
            
            def description(self):
                return "line id OC48"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _id(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "id"
            
            def description(self):
                return "TDM_ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["gen_en"] = _AF6CNC0022_RD_BERT_OPT._sel_ho_bert_gen._gen_en()
            allFields["lineid"] = _AF6CNC0022_RD_BERT_OPT._sel_ho_bert_gen._lineid()
            allFields["id"] = _AF6CNC0022_RD_BERT_OPT._sel_ho_bert_gen._id()
            return allFields

    class _sel_bert_montdm(AtRegister.AtRegister):
        def name(self):
            return "Sel TDM Bert mon"
    
        def description(self):
            return "The registers select 1id in line to gen bert data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_000 + engid"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x0000001b

        class _tdmmon_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "tdmmon_en"
            
            def description(self):
                return "set \"1\" to enable bert gen"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _mon_side(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "mon_side"
            
            def description(self):
                return "\"0\" tdm side, \"1\" psn side moniter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _tdm_lineid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 11
        
            def name(self):
                return "tdm_lineid"
            
            def description(self):
                return "line id OC48"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _id(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "id"
            
            def description(self):
                return "TDM_ID for TDM Side or MAP_PWID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tdmmon_en"] = _AF6CNC0022_RD_BERT_OPT._sel_bert_montdm._tdmmon_en()
            allFields["mon_side"] = _AF6CNC0022_RD_BERT_OPT._sel_bert_montdm._mon_side()
            allFields["tdm_lineid"] = _AF6CNC0022_RD_BERT_OPT._sel_bert_montdm._tdm_lineid()
            allFields["id"] = _AF6CNC0022_RD_BERT_OPT._sel_bert_montdm._id()
            return allFields

    class _ctrl_pen_gen(AtRegister.AtRegister):
        def name(self):
            return "Sel Mode Bert Gen"
    
        def description(self):
            return "The registers select mode bert gen"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4100 + 512*engid"
            
        def startAddress(self):
            return 0x00004100
            
        def endAddress(self):
            return 0x00007700

        class _swapmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "swapmode"
            
            def description(self):
                return "swap data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _invmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "invmode"
            
            def description(self):
                return "invert data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _patt_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "patt_mode"
            
            def description(self):
                return "sel pattern gen # 0x01 : all0 # 0x02 : prbs15 # 0x03 : prbs23 # 0x05 : prbs31 # 0x7  : all1 # 0x8  : SEQ(SIM)/prbs20(SYN) # 0x4  : prbs20r # 0x06 : fixpattern 4byte"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["swapmode"] = _AF6CNC0022_RD_BERT_OPT._ctrl_pen_gen._swapmode()
            allFields["invmode"] = _AF6CNC0022_RD_BERT_OPT._ctrl_pen_gen._invmode()
            allFields["patt_mode"] = _AF6CNC0022_RD_BERT_OPT._ctrl_pen_gen._patt_mode()
            return allFields

    class _txfix_gen(AtRegister.AtRegister):
        def name(self):
            return "Sel Value of fix pattern"
    
        def description(self):
            return "The registers select mode bert gen"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4104 + 512*engid"
            
        def startAddress(self):
            return 0x00004104
            
        def endAddress(self):
            return 0x0000b704

        class _txpatten_val(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txpatten_val"
            
            def description(self):
                return "value of fix paatern"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txpatten_val"] = _AF6CNC0022_RD_BERT_OPT._txfix_gen._txpatten_val()
            return allFields

    class _ctrl_pen_montdm(AtRegister.AtRegister):
        def name(self):
            return "Sel Mode Bert Mon TDM"
    
        def description(self):
            return "The registers select mode bert gen"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8100 + 512*engid"
            
        def startAddress(self):
            return 0x00008100
            
        def endAddress(self):
            return 0x0000b700

        class _swapmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "swapmode"
            
            def description(self):
                return "swap data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _invmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "invmode"
            
            def description(self):
                return "invert data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _patt_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "patt_mode"
            
            def description(self):
                return "sel pattern gen # 0x01 : fixpattern # 0x02 : all0 # 0x04 : prbs15 # 0x03 : prbs23 # 0x05 : prbs31 # 0x00 : all1 / SEQ(SIM) # 0x06 : prbs20(SYN) # 0x08  :prbs20r"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["swapmode"] = _AF6CNC0022_RD_BERT_OPT._ctrl_pen_montdm._swapmode()
            allFields["invmode"] = _AF6CNC0022_RD_BERT_OPT._ctrl_pen_montdm._invmode()
            allFields["patt_mode"] = _AF6CNC0022_RD_BERT_OPT._ctrl_pen_montdm._patt_mode()
            return allFields

    class _rxfix_gen(AtRegister.AtRegister):
        def name(self):
            return "Sel Value of fix pattern"
    
        def description(self):
            return "The registers select mode bert gen"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8104 + 512*engid"
            
        def startAddress(self):
            return 0x00008104
            
        def endAddress(self):
            return 0x00008704

        class _rxpatten_val(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpatten_val"
            
            def description(self):
                return "value of fix paatern"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpatten_val"] = _AF6CNC0022_RD_BERT_OPT._rxfix_gen._rxpatten_val()
            return allFields

    class _ctrl_ber_pen(AtRegister.AtRegister):
        def name(self):
            return "Inser Error"
    
        def description(self):
            return "The registers select rate inser error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4101 + 512*engid"
            
        def startAddress(self):
            return 0x00004101
            
        def endAddress(self):
            return 0x0000b701

        class _ber_rate(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ber_rate"
            
            def description(self):
                return "TxBerMd [31:0] == BER_level_val  : Bit Error Rate inserted to Pattern Generator [31:0] == 32'd0          :disable  Incase N DS0 mode : n        = num timeslot use, BER_level_val recalculate by CFG_DS1,CFG_E1 CFG_DS1  = ( 8n  x  BER_level_val) / 193 CFG_E1   = ( 8n  x  BER_level_val) / 256 Other : BER_level_val	BER_level 1000:        	BER 10e-3 10_000:      	BER 10e-4 100_000:     	BER 10e-5 1_000_000:   	BER 10e-6 10_000_000:  	BER 10e-7"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ber_rate"] = _AF6CNC0022_RD_BERT_OPT._ctrl_ber_pen._ber_rate()
            return allFields

    class _goodbit_pen_tdm_gen(AtRegister.AtRegister):
        def name(self):
            return "good counter tdm gen"
    
        def description(self):
            return "The registers select 1id in line to gen bert data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0600 + id"
            
        def startAddress(self):
            return 0x00000600
            
        def endAddress(self):
            return 0x0000063f

        class _bertgen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "bertgen"
            
            def description(self):
                return "gen goodbit"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["bertgen"] = _AF6CNC0022_RD_BERT_OPT._goodbit_pen_tdm_gen._bertgen()
            return allFields

    class _goodbit_pen_tdm_mon(AtRegister.AtRegister):
        def name(self):
            return "good counter tdm"
    
        def description(self):
            return "The registers select 1id in line to gen bert data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8600 + id"
            
        def startAddress(self):
            return 0x00008600
            
        def endAddress(self):
            return 0x0000863f

        class _mongood(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mongood"
            
            def description(self):
                return "moniter goodbit"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mongood"] = _AF6CNC0022_RD_BERT_OPT._goodbit_pen_tdm_mon._mongood()
            return allFields

    class _err_tdm_mon(AtRegister.AtRegister):
        def name(self):
            return "TDM err sync"
    
        def description(self):
            return "The registers indicate bert mon err in tdm side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8640 + id"
            
        def startAddress(self):
            return 0x00008640
            
        def endAddress(self):
            return 0x0000867f

        class _cnt_errbit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_errbit"
            
            def description(self):
                return "counter err bit"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_errbit"] = _AF6CNC0022_RD_BERT_OPT._err_tdm_mon._cnt_errbit()
            return allFields

    class _lossyn_pen_tdm_mon(AtRegister.AtRegister):
        def name(self):
            return "Counter loss bit in TDM mon"
    
        def description(self):
            return "The registers count lossbit in  mon tdm side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8105 + engid*512"
            
        def startAddress(self):
            return 0x00008105
            
        def endAddress(self):
            return 0x0000b705

        class _stk_losssyn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stk_losssyn"
            
            def description(self):
                return "sticky loss sync"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["stk_losssyn"] = _AF6CNC0022_RD_BERT_OPT._lossyn_pen_tdm_mon._stk_losssyn()
            return allFields

    class _lossbit_pen_tdm_mon(AtRegister.AtRegister):
        def name(self):
            return "Counter loss bit in TDM mon"
    
        def description(self):
            return "The registers count lossbit in  mon tdm side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8680 + id"
            
        def startAddress(self):
            return 0x00008680
            
        def endAddress(self):
            return 0x000086bf

        class _cnt_lossbit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_lossbit"
            
            def description(self):
                return "counter lossbit"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_lossbit"] = _AF6CNC0022_RD_BERT_OPT._lossbit_pen_tdm_mon._cnt_lossbit()
            return allFields

    class _singe_ber_pen(AtRegister.AtRegister):
        def name(self):
            return "Single Inser Error"
    
        def description(self):
            return "The registers select rate inser error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4105 + 512*engid"
            
        def startAddress(self):
            return 0x00004105
            
        def endAddress(self):
            return 0x00007105

        class _ena(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ena"
            
            def description(self):
                return "sw write \"1\" to force, hw auto clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ena"] = _AF6CNC0022_RD_BERT_OPT._singe_ber_pen._ena()
            return allFields

    class _stt_tdm_mon(AtRegister.AtRegister):
        def name(self):
            return "tdm mon stt"
    
        def description(self):
            return "The registers indicate bert mon state in pw side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8103 + 512*engid"
            
        def startAddress(self):
            return 0x00008103
            
        def endAddress(self):
            return 0x0000b703

        class _tdmstate(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tdmstate"
            
            def description(self):
                return "Status [1:0]: Prbs status LOPSTA = 2'd0; SRCSTA = 2'd1; VERSTA = 2'd2; INFSTA = 2'd3;"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tdmstate"] = _AF6CNC0022_RD_BERT_OPT._stt_tdm_mon._tdmstate()
            return allFields

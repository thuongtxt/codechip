import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0022_RD_CLA(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["cla_glb_psn"] = _AF6CNC0022_RD_CLA._cla_glb_psn()
        allRegisters["cla_hbce_hash_table"] = _AF6CNC0022_RD_CLA._cla_hbce_hash_table()
        allRegisters["cla_per_grp_enb"] = _AF6CNC0022_RD_CLA._cla_per_grp_enb()
        allRegisters["cla_per_eth_enb"] = _AF6CNC0022_RD_CLA._cla_per_eth_enb()
        allRegisters["Eth_cnt0_64_ro"] = _AF6CNC0022_RD_CLA._Eth_cnt0_64_ro()
        allRegisters["Eth_cnt0_64_rc"] = _AF6CNC0022_RD_CLA._Eth_cnt0_64_rc()
        allRegisters["Eth_cnt65_127_ro"] = _AF6CNC0022_RD_CLA._Eth_cnt65_127_ro()
        allRegisters["Eth_cnt65_127_rc"] = _AF6CNC0022_RD_CLA._Eth_cnt65_127_rc()
        allRegisters["Eth_cnt128_255_ro"] = _AF6CNC0022_RD_CLA._Eth_cnt128_255_ro()
        allRegisters["Eth_cnt128_255_rc"] = _AF6CNC0022_RD_CLA._Eth_cnt128_255_rc()
        allRegisters["Eth_cnt256_511_ro"] = _AF6CNC0022_RD_CLA._Eth_cnt256_511_ro()
        allRegisters["Eth_cnt256_511_rc"] = _AF6CNC0022_RD_CLA._Eth_cnt256_511_rc()
        allRegisters["Eth_cnt512_1024_ro"] = _AF6CNC0022_RD_CLA._Eth_cnt512_1024_ro()
        allRegisters["Eth_cnt512_1024_rc"] = _AF6CNC0022_RD_CLA._Eth_cnt512_1024_rc()
        allRegisters["Eth_cnt1025_1528_ro"] = _AF6CNC0022_RD_CLA._Eth_cnt1025_1528_ro()
        allRegisters["Eth_cnt1025_1528_rc"] = _AF6CNC0022_RD_CLA._Eth_cnt1025_1528_rc()
        allRegisters["Eth_cnt1529_2047_ro"] = _AF6CNC0022_RD_CLA._Eth_cnt1529_2047_ro()
        allRegisters["Eth_cnt1529_2047_rc"] = _AF6CNC0022_RD_CLA._Eth_cnt1529_2047_rc()
        allRegisters["Eth_cnt_jumbo_ro"] = _AF6CNC0022_RD_CLA._Eth_cnt_jumbo_ro()
        allRegisters["Eth_cnt_jumbo_rc"] = _AF6CNC0022_RD_CLA._Eth_cnt_jumbo_rc()
        allRegisters["Eth_cnt_Unicast_ro"] = _AF6CNC0022_RD_CLA._Eth_cnt_Unicast_ro()
        allRegisters["Eth_cnt_Unicast_rc"] = _AF6CNC0022_RD_CLA._Eth_cnt_Unicast_rc()
        allRegisters["rx_port_pkt_cnt_ro"] = _AF6CNC0022_RD_CLA._rx_port_pkt_cnt_ro()
        allRegisters["rx_port_pkt_cnt_rc"] = _AF6CNC0022_RD_CLA._rx_port_pkt_cnt_rc()
        allRegisters["rx_port_bcast_pkt_cnt_ro"] = _AF6CNC0022_RD_CLA._rx_port_bcast_pkt_cnt_ro()
        allRegisters["rx_port_bcast_pkt_cnt_rc"] = _AF6CNC0022_RD_CLA._rx_port_bcast_pkt_cnt_rc()
        allRegisters["rx_port_mcast_pkt_cnt_ro"] = _AF6CNC0022_RD_CLA._rx_port_mcast_pkt_cnt_ro()
        allRegisters["rx_port_mcast_pkt_cnt_rc"] = _AF6CNC0022_RD_CLA._rx_port_mcast_pkt_cnt_rc()
        allRegisters["rx_port_under_size_pkt_cnt_ro"] = _AF6CNC0022_RD_CLA._rx_port_under_size_pkt_cnt_ro()
        allRegisters["rx_port_under_size_pkt_cnt_rc"] = _AF6CNC0022_RD_CLA._rx_port_under_size_pkt_cnt_rc()
        allRegisters["rx_port_over_size_pkt_cnt_ro"] = _AF6CNC0022_RD_CLA._rx_port_over_size_pkt_cnt_ro()
        allRegisters["rx_port_over_size_pkt_cnt_rc"] = _AF6CNC0022_RD_CLA._rx_port_over_size_pkt_cnt_rc()
        allRegisters["Eth_cnt_phy_err_ro"] = _AF6CNC0022_RD_CLA._Eth_cnt_phy_err_ro()
        allRegisters["Eth_cnt_phy_err_rc"] = _AF6CNC0022_RD_CLA._Eth_cnt_phy_err_rc()
        allRegisters["Eth_cnt_byte_ro"] = _AF6CNC0022_RD_CLA._Eth_cnt_byte_ro()
        allRegisters["Eth_cnt_byte_rc"] = _AF6CNC0022_RD_CLA._Eth_cnt_byte_rc()
        allRegisters["cla_hbce_lkup_info_extra"] = _AF6CNC0022_RD_CLA._cla_hbce_lkup_info_extra()
        allRegisters["cla_2_cdr_cfg"] = _AF6CNC0022_RD_CLA._cla_2_cdr_cfg()
        allRegisters["cla_hold_status"] = _AF6CNC0022_RD_CLA._cla_hold_status()
        allRegisters["rdha3_0_control"] = _AF6CNC0022_RD_CLA._rdha3_0_control()
        allRegisters["rdha7_4_control"] = _AF6CNC0022_RD_CLA._rdha7_4_control()
        allRegisters["rdha11_8_control"] = _AF6CNC0022_RD_CLA._rdha11_8_control()
        allRegisters["rdha15_12_control"] = _AF6CNC0022_RD_CLA._rdha15_12_control()
        allRegisters["rdha19_16_control"] = _AF6CNC0022_RD_CLA._rdha19_16_control()
        allRegisters["rdha23_20_control"] = _AF6CNC0022_RD_CLA._rdha23_20_control()
        allRegisters["rdha24data_control"] = _AF6CNC0022_RD_CLA._rdha24data_control()
        allRegisters["rdha_hold63_32"] = _AF6CNC0022_RD_CLA._rdha_hold63_32()
        allRegisters["rdindr_hold95_64"] = _AF6CNC0022_RD_CLA._rdindr_hold95_64()
        allRegisters["rdindr_hold127_96"] = _AF6CNC0022_RD_CLA._rdindr_hold127_96()
        allRegisters["cla_Parity_control"] = _AF6CNC0022_RD_CLA._cla_Parity_control()
        allRegisters["cla_Parity_Disable_control"] = _AF6CNC0022_RD_CLA._cla_Parity_Disable_control()
        allRegisters["cla_Parity_stk_err"] = _AF6CNC0022_RD_CLA._cla_Parity_stk_err()
        allRegisters["Eth_cnt_psn_err1_ro"] = _AF6CNC0022_RD_CLA._Eth_cnt_psn_err1_ro()
        allRegisters["Eth_cnt_psn_err1_rc"] = _AF6CNC0022_RD_CLA._Eth_cnt_psn_err1_rc()
        allRegisters["Eth_cnt_psn_err2_ro"] = _AF6CNC0022_RD_CLA._Eth_cnt_psn_err2_ro()
        allRegisters["Eth_cnt_psn_err2_rc"] = _AF6CNC0022_RD_CLA._Eth_cnt_psn_err2_rc()
        return allRegisters

    class _cla_glb_psn(AtRegister.AtRegister):
        def name(self):
            return "Classify Global PSN Control"
    
        def description(self):
            return "This register configures identification per Ethernet port"
            
        def width(self):
            return 80
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00000000 +  eth_port"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x00000003

        class _RxEthChkSumEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 79
                
            def startBit(self):
                return 79
        
            def name(self):
                return "RxEthChkSumEn"
            
            def description(self):
                return "Enable checksum checking for IPv4/6"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthTypeDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 78
                
            def startBit(self):
                return 78
        
            def name(self):
                return "RxEthTypeDis"
            
            def description(self):
                return "Disable check ETH type"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnDAExp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 77
                
            def startBit(self):
                return 30
        
            def name(self):
                return "RxPsnDAExp"
            
            def description(self):
                return "Mac Address expected at Rx Ethernet port"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxSendLbit2CdrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "RxSendLbit2CdrEn"
            
            def description(self):
                return "Enable to send Lbit to CDR engine 1: Enable to send Lbit 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnMplsOutLabelCheckEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "RxPsnMplsOutLabelCheckEn"
            
            def description(self):
                return "Enable to check MPLS outer 1: Enable checking 0: Disable checking"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnCpuBfdCtlEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "RxPsnCpuBfdCtlEn"
            
            def description(self):
                return "Enable VCCV BFD control packet sending to CPU for processing 1: Enable sending 0: Discard"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxMacCheckDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "RxMacCheckDis"
            
            def description(self):
                return "Disable to check MAC address at Ethernet port receive direction 1: Disable checking 0: Enable checking"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnCpuIcmpEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "RxPsnCpuIcmpEn"
            
            def description(self):
                return "Enable ICMP control packet sending to CPU for processing 1: Enable sending 0: Discard"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnCpuArpEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "RxPsnCpuArpEn"
            
            def description(self):
                return "Enable ARP control packet sending to CPU for processing 1: Enable sending 0: Discard"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PweLoopClaEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "PweLoopClaEn"
            
            def description(self):
                return "Enable Loop back traffic from PW Encapsulation to Classification 1: Enable Loop back mode 0: Normal, not loop back"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnIpUdpMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "RxPsnIpUdpMode"
            
            def description(self):
                return "This bit is applicable for Ipv4/Ipv6 packet from Ethernet side 1: Classify engine uses RxPsnIpUdpSel to decide which UDP port (Source or Destination) is used to identify pseudowire packet 0: Classify engine will automatically search for value 0x85E in source or destination UDP port. The remaining UDP port is used to identify pseudowire packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnIpUdpSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "RxPsnIpUdpSel"
            
            def description(self):
                return "This bit is applicable for Ipv4/Ipv6 using to select Source or Destination to identify pseudowire packet from Ethernet side. It is not use when RxPsnIpUdpMode is zero 1: Classify engine selects source UDP port to identify pseudowire packet 0: Classify engine selects destination UDP port to identify pseudowire packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnIpTtlChkEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "RxPsnIpTtlChkEn"
            
            def description(self):
                return "Enable check TTL field in MPLS/Ipv4 or Hop Limit field in Ipv6 1: Enable checking 0: Disable checking"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnMplsOutLabel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPsnMplsOutLabel"
            
            def description(self):
                return "Received 2-label MPLS packet from PSN side will be discarded when it's outer label is different than RxPsnMplsOutLabel"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthChkSumEn"] = _AF6CNC0022_RD_CLA._cla_glb_psn._RxEthChkSumEn()
            allFields["RxEthTypeDis"] = _AF6CNC0022_RD_CLA._cla_glb_psn._RxEthTypeDis()
            allFields["RxPsnDAExp"] = _AF6CNC0022_RD_CLA._cla_glb_psn._RxPsnDAExp()
            allFields["RxSendLbit2CdrEn"] = _AF6CNC0022_RD_CLA._cla_glb_psn._RxSendLbit2CdrEn()
            allFields["RxPsnMplsOutLabelCheckEn"] = _AF6CNC0022_RD_CLA._cla_glb_psn._RxPsnMplsOutLabelCheckEn()
            allFields["RxPsnCpuBfdCtlEn"] = _AF6CNC0022_RD_CLA._cla_glb_psn._RxPsnCpuBfdCtlEn()
            allFields["RxMacCheckDis"] = _AF6CNC0022_RD_CLA._cla_glb_psn._RxMacCheckDis()
            allFields["RxPsnCpuIcmpEn"] = _AF6CNC0022_RD_CLA._cla_glb_psn._RxPsnCpuIcmpEn()
            allFields["RxPsnCpuArpEn"] = _AF6CNC0022_RD_CLA._cla_glb_psn._RxPsnCpuArpEn()
            allFields["PweLoopClaEn"] = _AF6CNC0022_RD_CLA._cla_glb_psn._PweLoopClaEn()
            allFields["RxPsnIpUdpMode"] = _AF6CNC0022_RD_CLA._cla_glb_psn._RxPsnIpUdpMode()
            allFields["RxPsnIpUdpSel"] = _AF6CNC0022_RD_CLA._cla_glb_psn._RxPsnIpUdpSel()
            allFields["RxPsnIpTtlChkEn"] = _AF6CNC0022_RD_CLA._cla_glb_psn._RxPsnIpTtlChkEn()
            allFields["RxPsnMplsOutLabel"] = _AF6CNC0022_RD_CLA._cla_glb_psn._RxPsnMplsOutLabel()
            return allFields

    class _cla_hbce_hash_table(AtRegister.AtRegister):
        def name(self):
            return "Classify HBCE Hashing Table Control"
    
        def description(self):
            return "HBCE module uses 14 bits for tab-index. Tab-index is generated by hashing function applied to the original id. %% # HDL_PATH     :rtlclapro.iaf6ces96rtlcla_hcbe.mem_p0hshtabinf.memram.ram.ram[$HashID] # Hashing function applies an XOR function to all bits of tab-index. %% # The indexes to the tab-index are generated by hashing function and therefore collisions may occur. %% # There are maximum fours (4) entries for every hash to identify flow traffic whether match or not. %% # If the collisions are more than fours (4), they are handled by pointer to link another memory. %% # The formula of hash pattern is {label ID(20bits), PSN mode (2bits), eth_port(2bits)}, call HashPattern (24bits)%% # The HashID formula has two case depend on CLAHbceCodingSelectedMode%% # CLAHbceCodingSelectedMode = 1: HashID = HashPattern[14:0] XOR {6'd0,HashPattern[23:15]}, CLAHbceStoreID[8:0] = HashPattern[23:15]%% # CLAHbceCodingSelectedMode = 0: HashID = HashPattern[14:0], CLAHbceStoreID = HashPattern[23:15] # #CLAHbceCodingSelectedMode = 1: HashID = HashPattern[13:0] XOR {4'd0,HashPattern[23:14]}, CLAHbceStoreID = HashPattern[23:14]%% # #CLAHbceCodingSelectedMode = 0: HashID = HashPattern[13:0], CLAHbceStoreID = HashPattern[23:14]"
            
        def width(self):
            return 13
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00020000 + HashID"
            
        def startAddress(self):
            return 0x00020000
            
        def endAddress(self):
            return 0x00023fff

        class _RxEthPwOnflyVC34(AtRegister.AtRegisterField):
            def stopBit(self):
                return 60
                
            def startBit(self):
                return 60
        
            def name(self):
                return "RxEthPwOnflyVC34"
            
            def description(self):
                return "CEP EBM on-fly fractional, length changed"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthPwLen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 59
                
            def startBit(self):
                return 46
        
            def name(self):
                return "RxEthPwLen"
            
            def description(self):
                return "length of packet to check malform"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthSupEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 45
                
            def startBit(self):
                return 45
        
            def name(self):
                return "RxEthSupEn"
            
            def description(self):
                return "Suppress Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthCepMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 44
                
            def startBit(self):
                return 44
        
            def name(self):
                return "RxEthCepMode"
            
            def description(self):
                return "CEP mode working 0,1: CEP basic"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthRtpSsrcValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 43
                
            def startBit(self):
                return 12
        
            def name(self):
                return "RxEthRtpSsrcValue"
            
            def description(self):
                return "This value is used to compare with SSRC value in RTP header of received TDM PW packets"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthRtpPtValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RxEthRtpPtValue"
            
            def description(self):
                return "This value is used to compare with PT value in RTP header of received TDM PW packets"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthRtpEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxEthRtpEn"
            
            def description(self):
                return "Enable RTP 1: Enable RTP 0: Disable RTP"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthRtpSsrcChkEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "RxEthRtpSsrcChkEn"
            
            def description(self):
                return "Enable checking SSRC field of RTP header in received TDM PW packet 1: Enable checking 0: Disable checking"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthRtpPtChkEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RxEthRtpPtChkEn"
            
            def description(self):
                return "Enable checking PT field of RTP header in received TDM PW packet 1: Enable checking 0: Disable checking"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPwMEFoMPLSEcidValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 70
                
            def startBit(self):
                return 51
        
            def name(self):
                return "RxPwMEFoMPLSEcidValue"
            
            def description(self):
                return "ECID value used to compare with ECID in Ethernet MEF packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPwMEFoMPLSDaValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 50
                
            def startBit(self):
                return 3
        
            def name(self):
                return "RxPwMEFoMPLSDaValue"
            
            def description(self):
                return "DA value used to compare with DA in Ethernet MEF packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0011_RD_OCN(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["glbrfm_reg"] = _AF6CCI0011_RD_OCN._glbrfm_reg()
        allRegisters["glbclkmon_reg"] = _AF6CCI0011_RD_OCN._glbclkmon_reg()
        allRegisters["glbdetlos_pen"] = _AF6CCI0011_RD_OCN._glbdetlos_pen()
        allRegisters["glbtfm_reg"] = _AF6CCI0011_RD_OCN._glbtfm_reg()
        allRegisters["glbspi_reg"] = _AF6CCI0011_RD_OCN._glbspi_reg()
        allRegisters["glbvpi_reg"] = _AF6CCI0011_RD_OCN._glbvpi_reg()
        allRegisters["glbtpg_reg"] = _AF6CCI0011_RD_OCN._glbtpg_reg()
        allRegisters["spiramctl"] = _AF6CCI0011_RD_OCN._spiramctl()
        allRegisters["spgramctl"] = _AF6CCI0011_RD_OCN._spgramctl()
        allRegisters["rxsxcramctl"] = _AF6CCI0011_RD_OCN._rxsxcramctl()
        allRegisters["txsxcramctl"] = _AF6CCI0011_RD_OCN._txsxcramctl()
        allRegisters["demramctl"] = _AF6CCI0011_RD_OCN._demramctl()
        allRegisters["vpiramctl"] = _AF6CCI0011_RD_OCN._vpiramctl()
        allRegisters["pgdemramctl"] = _AF6CCI0011_RD_OCN._pgdemramctl()
        allRegisters["vpgramctl"] = _AF6CCI0011_RD_OCN._vpgramctl()
        allRegisters["rxfrmsta"] = _AF6CCI0011_RD_OCN._rxfrmsta()
        allRegisters["rxfrmstk"] = _AF6CCI0011_RD_OCN._rxfrmstk()
        allRegisters["rxfrmb1cntro"] = _AF6CCI0011_RD_OCN._rxfrmb1cntro()
        allRegisters["rxfrmb1cntr2c"] = _AF6CCI0011_RD_OCN._rxfrmb1cntr2c()
        allRegisters["upstschstkram"] = _AF6CCI0011_RD_OCN._upstschstkram()
        allRegisters["upstschstaram"] = _AF6CCI0011_RD_OCN._upstschstaram()
        allRegisters["upvtchstkram"] = _AF6CCI0011_RD_OCN._upvtchstkram()
        allRegisters["upvtchstaram"] = _AF6CCI0011_RD_OCN._upvtchstaram()
        allRegisters["adjcntperstsram"] = _AF6CCI0011_RD_OCN._adjcntperstsram()
        allRegisters["adjcntperstkram"] = _AF6CCI0011_RD_OCN._adjcntperstkram()
        allRegisters["stspgstkram"] = _AF6CCI0011_RD_OCN._stspgstkram()
        allRegisters["adjcntpgperstsram"] = _AF6CCI0011_RD_OCN._adjcntpgperstsram()
        allRegisters["vtpgstkram"] = _AF6CCI0011_RD_OCN._vtpgstkram()
        allRegisters["adjcntpgpervtram"] = _AF6CCI0011_RD_OCN._adjcntpgpervtram()
        allRegisters["rxbarsxcramctl"] = _AF6CCI0011_RD_OCN._rxbarsxcramctl()
        allRegisters["txbarsxcramctl"] = _AF6CCI0011_RD_OCN._txbarsxcramctl()
        allRegisters["rxhomapramctl"] = _AF6CCI0011_RD_OCN._rxhomapramctl()
        allRegisters["parfrccfg1"] = _AF6CCI0011_RD_OCN._parfrccfg1()
        allRegisters["pardiscfg1"] = _AF6CCI0011_RD_OCN._pardiscfg1()
        allRegisters["parerrstk1"] = _AF6CCI0011_RD_OCN._parerrstk1()
        allRegisters["parfrccfg2"] = _AF6CCI0011_RD_OCN._parfrccfg2()
        allRegisters["pardiscfg2"] = _AF6CCI0011_RD_OCN._pardiscfg2()
        allRegisters["parerrstk2"] = _AF6CCI0011_RD_OCN._parerrstk2()
        allRegisters["parfrccfg3"] = _AF6CCI0011_RD_OCN._parfrccfg3()
        allRegisters["pardiscfg3"] = _AF6CCI0011_RD_OCN._pardiscfg3()
        allRegisters["parerrstk3"] = _AF6CCI0011_RD_OCN._parerrstk3()
        return allRegisters

    class _glbrfm_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Rx Framer Control"
    
        def description(self):
            return "This is the global configuration register for the Rx Framer"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _RxLineSyncSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxLineSyncSel"
            
            def description(self):
                return "Select line id for synchronization 8 rx lines. Bit[0] for line 0. Note that must only 1 bit is set"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLosAisEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "RxFrmLosAisEn"
            
            def description(self):
                return "Enable/disable forwarding P_AIS when LOS detected at Rx Framer. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmOofAisEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "RxFrmOofAisEn"
            
            def description(self):
                return "Enable/disable forwarding P_AIS when OOF detected at Rx Framer. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmB1BlockCntMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "RxFrmB1BlockCntMod"
            
            def description(self):
                return "B1 Counter Mode. 1: Block mode 0: Bit-wise mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmBadFrmThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "RxFrmBadFrmThresh"
            
            def description(self):
                return "Threshold for A1A2 missing counter, that is used to change state from FRAMED to HUNT."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmB1GoodThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxFrmB1GoodThresh"
            
            def description(self):
                return "Threshold for B1 good counter, that is used to change state from CHECK to FRAMED."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmDescrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RxFrmDescrEn"
            
            def description(self):
                return "Enable/disable de-scrambling of the Rx coming data stream. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmB1ChkFrmEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "RxFrmB1ChkFrmEn"
            
            def description(self):
                return "Enable/disable B1 check option is added to the required framing algorithm. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmEssiModeEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFrmEssiModeEn"
            
            def description(self):
                return "TFI-5 mode. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxLineSyncSel"] = _AF6CCI0011_RD_OCN._glbrfm_reg._RxLineSyncSel()
            allFields["RxFrmLosAisEn"] = _AF6CCI0011_RD_OCN._glbrfm_reg._RxFrmLosAisEn()
            allFields["RxFrmOofAisEn"] = _AF6CCI0011_RD_OCN._glbrfm_reg._RxFrmOofAisEn()
            allFields["RxFrmB1BlockCntMod"] = _AF6CCI0011_RD_OCN._glbrfm_reg._RxFrmB1BlockCntMod()
            allFields["RxFrmBadFrmThresh"] = _AF6CCI0011_RD_OCN._glbrfm_reg._RxFrmBadFrmThresh()
            allFields["RxFrmB1GoodThresh"] = _AF6CCI0011_RD_OCN._glbrfm_reg._RxFrmB1GoodThresh()
            allFields["RxFrmDescrEn"] = _AF6CCI0011_RD_OCN._glbrfm_reg._RxFrmDescrEn()
            allFields["RxFrmB1ChkFrmEn"] = _AF6CCI0011_RD_OCN._glbrfm_reg._RxFrmB1ChkFrmEn()
            allFields["RxFrmEssiModeEn"] = _AF6CCI0011_RD_OCN._glbrfm_reg._RxFrmEssiModeEn()
            return allFields

    class _glbclkmon_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Rx Framer LOS Detecting Control 1"
    
        def description(self):
            return "This is the global configuration register for the Rx Framer LOS detecting 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000006
            
        def endAddress(self):
            return 0xffffffff

        class _RxFrmLosDetMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "RxFrmLosDetMod"
            
            def description(self):
                return "Detected LOS mode. 1: the LOS declare when all zero or all one detected 0: the LOS declare when all zero detected"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLosDetDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "RxFrmLosDetDis"
            
            def description(self):
                return "Disable detect LOS. 1: Disable 0: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmClkMonDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RxFrmClkMonDis"
            
            def description(self):
                return "Disable to generate LOS to Rx Framer if detecting error on Rx line clock. 1: Disable 0: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmClkMonThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFrmClkMonThr"
            
            def description(self):
                return "Threshold to generate LOS to Rx Framer if detecting error on Rx line clock.(Threshold = PPM * 155.52/8). Default 0x3cc ~ 50ppm"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFrmLosDetMod"] = _AF6CCI0011_RD_OCN._glbclkmon_reg._RxFrmLosDetMod()
            allFields["RxFrmLosDetDis"] = _AF6CCI0011_RD_OCN._glbclkmon_reg._RxFrmLosDetDis()
            allFields["RxFrmClkMonDis"] = _AF6CCI0011_RD_OCN._glbclkmon_reg._RxFrmClkMonDis()
            allFields["RxFrmClkMonThr"] = _AF6CCI0011_RD_OCN._glbclkmon_reg._RxFrmClkMonThr()
            return allFields

    class _glbdetlos_pen(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Rx Framer LOS Detecting Control 2"
    
        def description(self):
            return "This is the global configuration register for the Rx Framer LOS detecting 2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000007
            
        def endAddress(self):
            return 0xffffffff

        class _RxFrmLosClr2Thr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "RxFrmLosClr2Thr"
            
            def description(self):
                return "Configure the period of time during which there is no period of consecutive data without transition, used for reset no transition counter. The recommended value is 0x30 (~2.5ms)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLosSetThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 12
        
            def name(self):
                return "RxFrmLosSetThr"
            
            def description(self):
                return "Configure the value that define the period of time in which there is no transition detected from incoming data from SERDES. The recommended value is 0x3cc (~50ms)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxFrmLosClr1Thr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFrmLosClr1Thr"
            
            def description(self):
                return "Configure the period of time during which there is no period of consecutive data without transition, used for counting at INFRM to change state to OOF The recommended value is 0x3cc (~50ms)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFrmLosClr2Thr"] = _AF6CCI0011_RD_OCN._glbdetlos_pen._RxFrmLosClr2Thr()
            allFields["RxFrmLosSetThr"] = _AF6CCI0011_RD_OCN._glbdetlos_pen._RxFrmLosSetThr()
            allFields["RxFrmLosClr1Thr"] = _AF6CCI0011_RD_OCN._glbdetlos_pen._RxFrmLosClr1Thr()
            return allFields

    class _glbtfm_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Tx Framer Control"
    
        def description(self):
            return "This is the global configuration register for the Tx Framer"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _TxLineOofFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "TxLineOofFrc"
            
            def description(self):
                return "Enable/disable force OOF for 8 tx lines. Bit[0] for line 0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxLineB1ErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "TxLineB1ErrFrc"
            
            def description(self):
                return "Enable/disable force B1 error for 8 tx lines. Bit[0] for line 0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxLineSyncSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 4
        
            def name(self):
                return "TxLineSyncSel"
            
            def description(self):
                return "Select line id for synchronization 8 tx lines. Bit[0] for line 0. Note that must only 1 bit is set"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxFrmScrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxFrmScrEn"
            
            def description(self):
                return "Enable/disable scrambling of the Tx data stream. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxLineOofFrc"] = _AF6CCI0011_RD_OCN._glbtfm_reg._TxLineOofFrc()
            allFields["TxLineB1ErrFrc"] = _AF6CCI0011_RD_OCN._glbtfm_reg._TxLineB1ErrFrc()
            allFields["TxLineSyncSel"] = _AF6CCI0011_RD_OCN._glbtfm_reg._TxLineSyncSel()
            allFields["TxFrmScrEn"] = _AF6CCI0011_RD_OCN._glbtfm_reg._TxFrmScrEn()
            return allFields

    class _glbspi_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global STS Pointer Interpreter Control"
    
        def description(self):
            return "This is the global configuration register for the STS Pointer Interpreter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _RxPgFlowThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 24
        
            def name(self):
                return "RxPgFlowThresh"
            
            def description(self):
                return "Overflow/underflow threshold to resynchronize read/write pointer."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPgAdjThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 20
        
            def name(self):
                return "RxPgAdjThresh"
            
            def description(self):
                return "Adjustment threshold to make a pointer increment/decrement."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiAisAisPEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "StsPiAisAisPEn"
            
            def description(self):
                return "Enable/Disable forwarding P_AIS when AIS state is detected at STS Pointer interpreter. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiLopAisPEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "StsPiLopAisPEn"
            
            def description(self):
                return "Enable/Disable forwarding P_AIS when LOP state is detected at STS Pointer interpreter. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiMajorMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "StsPiMajorMode"
            
            def description(self):
                return "Majority mode for detecting increment/decrement at STS pointer Interpreter. It is used for rule n of 5. 1: n = 3 0: n = 5"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiNorPtrThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "StsPiNorPtrThresh"
            
            def description(self):
                return "Threshold of number of normal pointers between two contiguous frames within pointer adjustments."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiNdfPtrThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "StsPiNdfPtrThresh"
            
            def description(self):
                return "Threshold of number of contiguous NDF pointers for entering LOP state at FSM."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiBadPtrThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "StsPiBadPtrThresh"
            
            def description(self):
                return "Threshold of number of contiguous invalid pointers for entering LOP state at FSM."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiPohAisType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "StsPiPohAisType"
            
            def description(self):
                return "Enable/disable STS POH defect types to downstream AIS in case of terminating the related STS such as the STS carries VT/TU. [0]: Enable for TIM defect [1]: Enable for Unequiped defect [2]: Enable for VC-AIS [3]: Enable for PLM defect"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPgFlowThresh"] = _AF6CCI0011_RD_OCN._glbspi_reg._RxPgFlowThresh()
            allFields["RxPgAdjThresh"] = _AF6CCI0011_RD_OCN._glbspi_reg._RxPgAdjThresh()
            allFields["StsPiAisAisPEn"] = _AF6CCI0011_RD_OCN._glbspi_reg._StsPiAisAisPEn()
            allFields["StsPiLopAisPEn"] = _AF6CCI0011_RD_OCN._glbspi_reg._StsPiLopAisPEn()
            allFields["StsPiMajorMode"] = _AF6CCI0011_RD_OCN._glbspi_reg._StsPiMajorMode()
            allFields["StsPiNorPtrThresh"] = _AF6CCI0011_RD_OCN._glbspi_reg._StsPiNorPtrThresh()
            allFields["StsPiNdfPtrThresh"] = _AF6CCI0011_RD_OCN._glbspi_reg._StsPiNdfPtrThresh()
            allFields["StsPiBadPtrThresh"] = _AF6CCI0011_RD_OCN._glbspi_reg._StsPiBadPtrThresh()
            allFields["StsPiPohAisType"] = _AF6CCI0011_RD_OCN._glbspi_reg._StsPiPohAisType()
            return allFields

    class _glbvpi_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global VTTU Pointer Interpreter Control"
    
        def description(self):
            return "This is the global configuration register for the VTTU Pointer Interpreter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _VtPiLomAisPEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "VtPiLomAisPEn"
            
            def description(self):
                return "Enable/Disable forwarding AIS when LOM is detected. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiLomInvlCntMod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "VtPiLomInvlCntMod"
            
            def description(self):
                return "H4 monitoring mode. 1: Expected H4 is current frame in the validated sequence plus one. 0: Expected H4 is the last received value plus one."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiLomGoodThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 24
        
            def name(self):
                return "VtPiLomGoodThresh"
            
            def description(self):
                return "Threshold of number of contiguous frames with validated sequence	of multi framers in LOM state for condition to entering IM state."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiLomInvlThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 20
        
            def name(self):
                return "VtPiLomInvlThresh"
            
            def description(self):
                return "Threshold of number of contiguous frames with invalidated sequence of multi framers in IM state  for condition to entering LOM state."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiAisAisPEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "VtPiAisAisPEn"
            
            def description(self):
                return "Enable/Disable forwarding AIS when AIS state is detected at VTTU Pointer interpreter. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiLopAisPEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "VtPiLopAisPEn"
            
            def description(self):
                return "Enable/Disable forwarding AIS when LOP state is detected at VTTU Pointer interpreter. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiMajorMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "VtPiMajorMode"
            
            def description(self):
                return "Majority mode detecting increment/decrement in VTTU pointer Interpreter. It is used for rule n of 5. 1: n = 3 0: n = 5"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiNorPtrThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "VtPiNorPtrThresh"
            
            def description(self):
                return "Threshold of number of normal pointers between two contiguous frames within pointer adjustments."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiNdfPtrThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "VtPiNdfPtrThresh"
            
            def description(self):
                return "Threshold of number of contiguous NDF pointers for entering LOP state at FSM."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiBadPtrThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "VtPiBadPtrThresh"
            
            def description(self):
                return "Threshold of number of contiguous invalid pointers for entering LOP state at FSM."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiPohAisType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VtPiPohAisType"
            
            def description(self):
                return "Enable/disable VTTU POH defect types to downstream AIS in case of terminating the related VTTU. [0]: Enable for TIM defect [1]: Enable for Un-equipment defect [2]: Enable for VC-AIS [3]: Enable for PLM defect"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VtPiLomAisPEn"] = _AF6CCI0011_RD_OCN._glbvpi_reg._VtPiLomAisPEn()
            allFields["VtPiLomInvlCntMod"] = _AF6CCI0011_RD_OCN._glbvpi_reg._VtPiLomInvlCntMod()
            allFields["VtPiLomGoodThresh"] = _AF6CCI0011_RD_OCN._glbvpi_reg._VtPiLomGoodThresh()
            allFields["VtPiLomInvlThresh"] = _AF6CCI0011_RD_OCN._glbvpi_reg._VtPiLomInvlThresh()
            allFields["VtPiAisAisPEn"] = _AF6CCI0011_RD_OCN._glbvpi_reg._VtPiAisAisPEn()
            allFields["VtPiLopAisPEn"] = _AF6CCI0011_RD_OCN._glbvpi_reg._VtPiLopAisPEn()
            allFields["VtPiMajorMode"] = _AF6CCI0011_RD_OCN._glbvpi_reg._VtPiMajorMode()
            allFields["VtPiNorPtrThresh"] = _AF6CCI0011_RD_OCN._glbvpi_reg._VtPiNorPtrThresh()
            allFields["VtPiNdfPtrThresh"] = _AF6CCI0011_RD_OCN._glbvpi_reg._VtPiNdfPtrThresh()
            allFields["VtPiBadPtrThresh"] = _AF6CCI0011_RD_OCN._glbvpi_reg._VtPiBadPtrThresh()
            allFields["VtPiPohAisType"] = _AF6CCI0011_RD_OCN._glbvpi_reg._VtPiPohAisType()
            return allFields

    class _glbtpg_reg(AtRegister.AtRegister):
        def name(self):
            return "OCN Global Pointer Generator Control"
    
        def description(self):
            return "This is the global configuration register for the Tx Pointer Generator"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0xffffffff

        class _TxPgNorPtrThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TxPgNorPtrThresh"
            
            def description(self):
                return "Threshold of number of normal pointers between two contiguous frames to make a condition of pointer adjustments."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxPgFlowThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "TxPgFlowThresh"
            
            def description(self):
                return "Overflow/underflow threshold to resynchronize read/write pointer of TxFiFo."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxPgAdjThresh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPgAdjThresh"
            
            def description(self):
                return "Adjustment threshold to make a condition of pointer increment/decrement."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPgNorPtrThresh"] = _AF6CCI0011_RD_OCN._glbtpg_reg._TxPgNorPtrThresh()
            allFields["TxPgFlowThresh"] = _AF6CCI0011_RD_OCN._glbtpg_reg._TxPgFlowThresh()
            allFields["TxPgAdjThresh"] = _AF6CCI0011_RD_OCN._glbtpg_reg._TxPgAdjThresh()
            return allFields

    class _spiramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN STS Pointer Interpreter Per Channel Control"
    
        def description(self):
            return "Each register is used to configure for STS pointer interpreter engines of the STS-1/VC-3. Backdoor		: irxpp_stspp_inst.irxpp_stspp[0].rxpp_stspiramctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x22000 + 512*LineId + StsId"
            
        def startAddress(self):
            return 0x00022000
            
        def endAddress(self):
            return 0x00022e2f

        class _StsPiChkLom(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "StsPiChkLom"
            
            def description(self):
                return "Enable/disable LOM checking. This field will be set to 1 when payload type of VC3/VC4 includes any VC11/VC12 1: Enable. 0: Disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiSSDetPatt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "StsPiSSDetPatt"
            
            def description(self):
                return "Configure pattern SS bits that is used to compare with the extracted SS bits from receive direction."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiAisFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "StsPiAisFrc"
            
            def description(self):
                return "Forcing SFM to AIS state. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiSSDetEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "StsPiSSDetEn"
            
            def description(self):
                return "Enable/disable checking SS bits in STSPI state machine. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiAdjRule(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "StsPiAdjRule"
            
            def description(self):
                return "Configure the rule for detecting adjustment condition. 1: The n of 5 rule is selected. This mode is applied for SDH mode 0: The 8 of 10 rule is selected. This mode is applied for SONET mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiStsSlvInd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "StsPiStsSlvInd"
            
            def description(self):
                return "This is used to configure STS is slaver or master. 1: Slaver."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiStsMstId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "StsPiStsMstId"
            
            def description(self):
                return "This is the ID of the master STS-1 in the concatenation that contains this STS-1."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StsPiChkLom"] = _AF6CCI0011_RD_OCN._spiramctl._StsPiChkLom()
            allFields["StsPiSSDetPatt"] = _AF6CCI0011_RD_OCN._spiramctl._StsPiSSDetPatt()
            allFields["StsPiAisFrc"] = _AF6CCI0011_RD_OCN._spiramctl._StsPiAisFrc()
            allFields["StsPiSSDetEn"] = _AF6CCI0011_RD_OCN._spiramctl._StsPiSSDetEn()
            allFields["StsPiAdjRule"] = _AF6CCI0011_RD_OCN._spiramctl._StsPiAdjRule()
            allFields["StsPiStsSlvInd"] = _AF6CCI0011_RD_OCN._spiramctl._StsPiStsSlvInd()
            allFields["StsPiStsMstId"] = _AF6CCI0011_RD_OCN._spiramctl._StsPiStsMstId()
            return allFields

    class _spgramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN STS Pointer Generator Per Channel Control"
    
        def description(self):
            return "Each register is used to configure for STS pointer Generator engines. Backdoor		: itxpp_stspp_inst.itxpp_stspp[0].txpg_stspgctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x23000 + 512*LineId + StsId"
            
        def startAddress(self):
            return 0x00023000
            
        def endAddress(self):
            return 0x00023e2f

        class _StsPgStsSlvInd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "StsPgStsSlvInd"
            
            def description(self):
                return "This is used to configure STS is slaver or master. 1: Slaver."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPgStsMstId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 8
        
            def name(self):
                return "StsPgStsMstId"
            
            def description(self):
                return "This is the ID of the master STS-1 in the concatenation that contains this STS-1."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPgB3BipErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "StsPgB3BipErrFrc"
            
            def description(self):
                return "Forcing B3 Bip error. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPgLopFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "StsPgLopFrc"
            
            def description(self):
                return "Forcing LOP. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPgUeqFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "StsPgUeqFrc"
            
            def description(self):
                return "Forcing SFM to UEQ state. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPgAisFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "StsPgAisFrc"
            
            def description(self):
                return "Forcing SFM to AIS state. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPgSSInsPatt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "StsPgSSInsPatt"
            
            def description(self):
                return "Configure pattern SS bits that is used to insert to pointer value."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPgSSInsEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "StsPgSSInsEn"
            
            def description(self):
                return "Enable/disable SS bits insertion. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPgPohIns(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "StsPgPohIns"
            
            def description(self):
                return "Enable/disable POH Insertion. High to enable insertion of POH."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StsPgStsSlvInd"] = _AF6CCI0011_RD_OCN._spgramctl._StsPgStsSlvInd()
            allFields["StsPgStsMstId"] = _AF6CCI0011_RD_OCN._spgramctl._StsPgStsMstId()
            allFields["StsPgB3BipErrFrc"] = _AF6CCI0011_RD_OCN._spgramctl._StsPgB3BipErrFrc()
            allFields["StsPgLopFrc"] = _AF6CCI0011_RD_OCN._spgramctl._StsPgLopFrc()
            allFields["StsPgUeqFrc"] = _AF6CCI0011_RD_OCN._spgramctl._StsPgUeqFrc()
            allFields["StsPgAisFrc"] = _AF6CCI0011_RD_OCN._spgramctl._StsPgAisFrc()
            allFields["StsPgSSInsPatt"] = _AF6CCI0011_RD_OCN._spgramctl._StsPgSSInsPatt()
            allFields["StsPgSSInsEn"] = _AF6CCI0011_RD_OCN._spgramctl._StsPgSSInsEn()
            allFields["StsPgPohIns"] = _AF6CCI0011_RD_OCN._spgramctl._StsPgPohIns()
            return allFields

    class _rxsxcramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx SXC Control"
    
        def description(self):
            return "Each register is used for each outgoing STS (to PDH) of any line (16 lines - 6 Lo and 8 Ho) can be randomly configured to //connect  to any ingoing STS of any ingoing line (from TFI-5 line - 8 lines). Backdoor		: isdhsxc_inst.isxc_rxrd[0].rxsxcramctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x24000 + 256*LineId + StsId"
            
        def startAddress(self):
            return 0x00024000
            
        def endAddress(self):
            return 0x00024d2f

        class _RxSxcLineId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "RxSxcLineId"
            
            def description(self):
                return "Contains the ingoing LineID (0-7) or used to Configure loop back to PDH (If value LineID is 15). Disconnect (output is all one) if value LineID is other value (recommend is 14)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxSxcStsId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxSxcStsId"
            
            def description(self):
                return "Contains the ingoing STSID (0-47)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxSxcLineId"] = _AF6CCI0011_RD_OCN._rxsxcramctl._RxSxcLineId()
            allFields["RxSxcStsId"] = _AF6CCI0011_RD_OCN._rxsxcramctl._RxSxcStsId()
            return allFields

    class _txsxcramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN Tx SXC Control"
    
        def description(self):
            return "Each register is used for each outgoing STS (to TFI-5 Line) of any line (8 lines) can be randomly configured to connect to any ingoing STS of any ingoing line (from PDH) (16 lines - 6 Lo and 8 Ho). Backdoor		: isdhsxc_inst.isxc_txrd[0].txxcramctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x25000 + 256*LineId + StsId"
            
        def startAddress(self):
            return 0x00025000
            
        def endAddress(self):
            return 0x0002572f

        class _TxSxcLineId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TxSxcLineId"
            
            def description(self):
                return "Contains the ingoing LineID (0-13: 0-5 for Lo) or used to Configure loop back to TFI-5 (If value LineID is 15). Disconnect (output is all one) if value LineID is other value (recommend is 14)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxSxcStsId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxSxcStsId"
            
            def description(self):
                return "Contains the ingoing STSID (0-47)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxSxcLineId"] = _AF6CCI0011_RD_OCN._txsxcramctl._TxSxcLineId()
            allFields["TxSxcStsId"] = _AF6CCI0011_RD_OCN._txsxcramctl._TxSxcStsId()
            return allFields

    class _demramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN RXPP Per STS payload Control"
    
        def description(self):
            return "Each register is used to configure VT payload mode per STS. Backdoor		: irxpp_vtpp_inst.irxpp_vtpp[0].rxpp_demramctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x40000 + 16384*LineId + StsId"
            
        def startAddress(self):
            return 0x00040000
            
        def endAddress(self):
            return 0x0005402f

        class _PiDemStsTerm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PiDemStsTerm"
            
            def description(self):
                return "Enable to terminate the related STS/VC. It means that STS POH defects related to the STS/VC to generate AIS to downstream. Must be set to 1. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PiDemSpeType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 14
        
            def name(self):
                return "PiDemSpeType"
            
            def description(self):
                return "Configure types of SPE. 0: Disable processing pointers of all VT/TUs in this SPE. 1: VC-3 type or STS SPE containing VT/TU exception TU-3 2: TUG-3 type containing VT/TU exception TU-3 3: TUG-3 type containing TU-3."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PiDemTug26Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PiDemTug26Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #6. 0: TU11 1: TU12"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PiDemTug25Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PiDemTug25Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #5."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PiDemTug24Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PiDemTug24Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #4."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PiDemTug23Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PiDemTug23Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #3."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PiDemTug22Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PiDemTug22Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #2."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PiDemTug21Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PiDemTug21Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #1."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PiDemTug20Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PiDemTug20Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PiDemStsTerm"] = _AF6CCI0011_RD_OCN._demramctl._PiDemStsTerm()
            allFields["PiDemSpeType"] = _AF6CCI0011_RD_OCN._demramctl._PiDemSpeType()
            allFields["PiDemTug26Type"] = _AF6CCI0011_RD_OCN._demramctl._PiDemTug26Type()
            allFields["PiDemTug25Type"] = _AF6CCI0011_RD_OCN._demramctl._PiDemTug25Type()
            allFields["PiDemTug24Type"] = _AF6CCI0011_RD_OCN._demramctl._PiDemTug24Type()
            allFields["PiDemTug23Type"] = _AF6CCI0011_RD_OCN._demramctl._PiDemTug23Type()
            allFields["PiDemTug22Type"] = _AF6CCI0011_RD_OCN._demramctl._PiDemTug22Type()
            allFields["PiDemTug21Type"] = _AF6CCI0011_RD_OCN._demramctl._PiDemTug21Type()
            allFields["PiDemTug20Type"] = _AF6CCI0011_RD_OCN._demramctl._PiDemTug20Type()
            return allFields

    class _vpiramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN VTTU Pointer Interpreter Per Channel Control"
    
        def description(self):
            return "Each register is used to configure for VTTU pointer interpreter engines. Backdoor		: irxpp_vtpp_inst.irxpp_vtpp[0].rxpp_vpiramctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x40800 + 16384*LineId + 32*StsId + 4*VtgId + VtId"
            
        def startAddress(self):
            return 0x00040800
            
        def endAddress(self):
            return 0x00054fff

        class _VtPiLoTerm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "VtPiLoTerm"
            
            def description(self):
                return "Enable to terminate the related VTTU. It means that VTTU POH defects related to the VTTU to generate AIS to downstream."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiAisFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "VtPiAisFrc"
            
            def description(self):
                return "Forcing SFM to AIS state. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiSSDetPatt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "VtPiSSDetPatt"
            
            def description(self):
                return "Configure pattern SS bits that is used to compare with the extracted SS bits from receive direction."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiSSDetEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "VtPiSSDetEn"
            
            def description(self):
                return "Enable/disable checking SS bits in PI State Machine. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiAdjRule(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VtPiAdjRule"
            
            def description(self):
                return "Configure the rule for detecting adjustment condition. 1: The n of 5 rule is selected. This mode is applied for SDH mode 0: The 8 of 10 rule is selected. This mode is applied for SONET mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VtPiLoTerm"] = _AF6CCI0011_RD_OCN._vpiramctl._VtPiLoTerm()
            allFields["VtPiAisFrc"] = _AF6CCI0011_RD_OCN._vpiramctl._VtPiAisFrc()
            allFields["VtPiSSDetPatt"] = _AF6CCI0011_RD_OCN._vpiramctl._VtPiSSDetPatt()
            allFields["VtPiSSDetEn"] = _AF6CCI0011_RD_OCN._vpiramctl._VtPiSSDetEn()
            allFields["VtPiAdjRule"] = _AF6CCI0011_RD_OCN._vpiramctl._VtPiAdjRule()
            return allFields

    class _pgdemramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN TXPP Per STS Multiplexing Control"
    
        def description(self):
            return "Each register is used to configure VT payload mode per STS at Tx pointer generator. Backdoor		: itxpp_vtpp_inst.itxpp_vtpp[0].txpg_pgdmctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x60000 + 16384*LineId + StsId"
            
        def startAddress(self):
            return 0x00060000
            
        def endAddress(self):
            return 0x0007402f

        class _PgDemSpeType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 14
        
            def name(self):
                return "PgDemSpeType"
            
            def description(self):
                return "Configure types of SPE. 0: Disable processing pointers of all VT/TUs in this SPE. 1: VC-3 type or STS SPE containing VT/TU exception TU-3 2: TUG-3 type containing VT/TU exception TU-3 3: TUG-3 type containing TU-3."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PgDemTug26Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PgDemTug26Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #6. 0: TU11 1: TU12"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PgDemTug25Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PgDemTug25Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #5."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PgDemTug24Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PgDemTug24Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #4."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PgDemTug23Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PgDemTug23Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #3."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PgDemTug22Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PgDemTug22Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #2."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PgDemTug21Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PgDemTug21Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #1."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PgDemTug20Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PgDemTug20Type"
            
            def description(self):
                return "Configure types of VT/TUs in TUG-2 #0."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PgDemSpeType"] = _AF6CCI0011_RD_OCN._pgdemramctl._PgDemSpeType()
            allFields["PgDemTug26Type"] = _AF6CCI0011_RD_OCN._pgdemramctl._PgDemTug26Type()
            allFields["PgDemTug25Type"] = _AF6CCI0011_RD_OCN._pgdemramctl._PgDemTug25Type()
            allFields["PgDemTug24Type"] = _AF6CCI0011_RD_OCN._pgdemramctl._PgDemTug24Type()
            allFields["PgDemTug23Type"] = _AF6CCI0011_RD_OCN._pgdemramctl._PgDemTug23Type()
            allFields["PgDemTug22Type"] = _AF6CCI0011_RD_OCN._pgdemramctl._PgDemTug22Type()
            allFields["PgDemTug21Type"] = _AF6CCI0011_RD_OCN._pgdemramctl._PgDemTug21Type()
            allFields["PgDemTug20Type"] = _AF6CCI0011_RD_OCN._pgdemramctl._PgDemTug20Type()
            return allFields

    class _vpgramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN VTTU Pointer Generator Per Channel Control"
    
        def description(self):
            return "Each register is used to configure for VTTU pointer Generator engines. Backdoor		: itxpp_vtpp_inst.itxpp_vtpp[0].txpg_pgctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x60800 + 16384*LineId + 32*StsId + 4*VtgId + VtId"
            
        def startAddress(self):
            return 0x00060800
            
        def endAddress(self):
            return 0x00074fff

        class _VtPgBipErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "VtPgBipErrFrc"
            
            def description(self):
                return "Forcing Bip error. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPgLopFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "VtPgLopFrc"
            
            def description(self):
                return "Forcing LOP. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPgUeqFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "VtPgUeqFrc"
            
            def description(self):
                return "Forcing SFM to UEQ state. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPgAisFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "VtPgAisFrc"
            
            def description(self):
                return "Forcing SFM to AIS state. 1: Force 0: Not force"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPgSSInsPatt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "VtPgSSInsPatt"
            
            def description(self):
                return "Configure pattern SS bits that is used to insert to Pointer value."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPgSSInsEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "VtPgSSInsEn"
            
            def description(self):
                return "Enable/disable SS bits insertion. 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPgPohIns(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VtPgPohIns"
            
            def description(self):
                return "Enable/ disable POH Insertion. High to enable insertion of POH."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VtPgBipErrFrc"] = _AF6CCI0011_RD_OCN._vpgramctl._VtPgBipErrFrc()
            allFields["VtPgLopFrc"] = _AF6CCI0011_RD_OCN._vpgramctl._VtPgLopFrc()
            allFields["VtPgUeqFrc"] = _AF6CCI0011_RD_OCN._vpgramctl._VtPgUeqFrc()
            allFields["VtPgAisFrc"] = _AF6CCI0011_RD_OCN._vpgramctl._VtPgAisFrc()
            allFields["VtPgSSInsPatt"] = _AF6CCI0011_RD_OCN._vpgramctl._VtPgSSInsPatt()
            allFields["VtPgSSInsEn"] = _AF6CCI0011_RD_OCN._vpgramctl._VtPgSSInsEn()
            allFields["VtPgPohIns"] = _AF6CCI0011_RD_OCN._vpgramctl._VtPgPohIns()
            return allFields

    class _rxfrmsta(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Framer Status"
    
        def description(self):
            return "Rx Framer status"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x20000 + 512*LineId"
            
        def startAddress(self):
            return 0x00020000
            
        def endAddress(self):
            return 0x00020e00

        class _StaFFCnvFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "StaFFCnvFul"
            
            def description(self):
                return "Status fifo convert clock domain is full"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _StaLos(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "StaLos"
            
            def description(self):
                return "Loss of Signal status"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _StaOof(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "StaOof"
            
            def description(self):
                return "Out of Frame that is detected at RxFramer"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StaFFCnvFul"] = _AF6CCI0011_RD_OCN._rxfrmsta._StaFFCnvFul()
            allFields["StaLos"] = _AF6CCI0011_RD_OCN._rxfrmsta._StaLos()
            allFields["StaOof"] = _AF6CCI0011_RD_OCN._rxfrmsta._StaOof()
            return allFields

    class _rxfrmstk(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Framer Sticky"
    
        def description(self):
            return "Rx Framer sticky"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x20001 + 512*LineId"
            
        def startAddress(self):
            return 0x00020001
            
        def endAddress(self):
            return 0x00020e01

        class _StkFFCnvFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "StkFFCnvFul"
            
            def description(self):
                return "Sticky fifo convert clock domain is full"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _StkLos(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "StkLos"
            
            def description(self):
                return "Loss of Signal  sticky change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _StkOof(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "StkOof"
            
            def description(self):
                return "Out of Frame sticky  change"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StkFFCnvFul"] = _AF6CCI0011_RD_OCN._rxfrmstk._StkFFCnvFul()
            allFields["StkLos"] = _AF6CCI0011_RD_OCN._rxfrmstk._StkLos()
            allFields["StkOof"] = _AF6CCI0011_RD_OCN._rxfrmstk._StkOof()
            return allFields

    class _rxfrmb1cntro(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Framer B1 error counter read only"
    
        def description(self):
            return "Rx Framer B1 error counter read only"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x20002 + 512*LineId"
            
        def startAddress(self):
            return 0x00020002
            
        def endAddress(self):
            return 0x00020e02

        class _RxFrmB1ErrRo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFrmB1ErrRo"
            
            def description(self):
                return "Number of B1 error - read only"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFrmB1ErrRo"] = _AF6CCI0011_RD_OCN._rxfrmb1cntro._RxFrmB1ErrRo()
            return allFields

    class _rxfrmb1cntr2c(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Framer B1 error counter read to clear"
    
        def description(self):
            return "Rx Framer B1 error counter read to clear"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x20003 + 512*LineId"
            
        def startAddress(self):
            return 0x00020003
            
        def endAddress(self):
            return 0x00020e03

        class _RxFrmB1ErrR2C(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFrmB1ErrR2C"
            
            def description(self):
                return "Number of B1 error - read to clear"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFrmB1ErrR2C"] = _AF6CCI0011_RD_OCN._rxfrmb1cntr2c._RxFrmB1ErrR2C()
            return allFields

    class _upstschstkram(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx STS/VC per Alarm Interrupt Status"
    
        def description(self):
            return "This is the per Alarm interrupt status of STS/VC pointer interpreter.  %% Each register is used to store 6 sticky bits for 6 alarms in the STS/VC."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x22140 + 512*LineId + StsId"
            
        def startAddress(self):
            return 0x00022140
            
        def endAddress(self):
            return 0x00022f6f

        class _StsPiStsConcDetIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "StsPiStsConcDetIntr"
            
            def description(self):
                return "Set to 1 while an Concatenation Detection event is detected at STS/VC pointer interpreter. This event doesn't raise interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiStsNewDetIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "StsPiStsNewDetIntr"
            
            def description(self):
                return "Set to 1 while an New Pointer Detection event is detected at STS/VC pointer interpreter. This event doesn't raise interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiStsNdfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "StsPiStsNdfIntr"
            
            def description(self):
                return "Set to 1 while an NDF event is detected at STS/VC pointer interpreter. This event doesn't raise interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiCepUneqStatePChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "StsPiCepUneqStatePChgIntr"
            
            def description(self):
                return "Set to 1  while there is change in CEP Un-equip Path in the related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status register of the related STS/VC to know the STS/VC whether in CEP Un-equip Path state or not."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiStsAISStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "StsPiStsAISStateChgIntr"
            
            def description(self):
                return "Set to 1 while there is change in AIS state in the related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status register of the related STS/VC to know the STS/VC whether in AIS state or not."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiStsLopStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "StsPiStsLopStateChgIntr"
            
            def description(self):
                return "Set 1 to while there is change in LOP state in the related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status register of the related STS/VC to know the STS/VC whether in LOP state or not."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StsPiStsConcDetIntr"] = _AF6CCI0011_RD_OCN._upstschstkram._StsPiStsConcDetIntr()
            allFields["StsPiStsNewDetIntr"] = _AF6CCI0011_RD_OCN._upstschstkram._StsPiStsNewDetIntr()
            allFields["StsPiStsNdfIntr"] = _AF6CCI0011_RD_OCN._upstschstkram._StsPiStsNdfIntr()
            allFields["StsPiCepUneqStatePChgIntr"] = _AF6CCI0011_RD_OCN._upstschstkram._StsPiCepUneqStatePChgIntr()
            allFields["StsPiStsAISStateChgIntr"] = _AF6CCI0011_RD_OCN._upstschstkram._StsPiStsAISStateChgIntr()
            allFields["StsPiStsLopStateChgIntr"] = _AF6CCI0011_RD_OCN._upstschstkram._StsPiStsLopStateChgIntr()
            return allFields

    class _upstschstaram(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx STS/VC per Alarm Current Status"
    
        def description(self):
            return "This is the per Alarm current status of STS/VC pointer interpreter.  %% Each register is used to store 3 bits to store current status of 3 alarms in the STS/VC."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x22180 + 512*LineId + StsId"
            
        def startAddress(self):
            return 0x00022180
            
        def endAddress(self):
            return 0x00022faf

        class _StsPiStsCepUneqPCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "StsPiStsCepUneqPCurStatus"
            
            def description(self):
                return "CEP Un-eqip Path current status in the related STS/VC. When it changes for 0 to 1 or vice versa, the  StsPiStsCepUeqPStateChgIntr bit in the OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiStsAisCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "StsPiStsAisCurStatus"
            
            def description(self):
                return "AIS current status in the related STS/VC. When it changes for 0 to 1 or vice versa, the  StsPiStsAISStateChgIntr bit in the OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPiStsLopCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "StsPiStsLopCurStatus"
            
            def description(self):
                return "LOP current status in the related STS/VC. When it changes for 0 to 1 or vice versa, the  StsPiStsLopStateChgIntr bit in the OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StsPiStsCepUneqPCurStatus"] = _AF6CCI0011_RD_OCN._upstschstaram._StsPiStsCepUneqPCurStatus()
            allFields["StsPiStsAisCurStatus"] = _AF6CCI0011_RD_OCN._upstschstaram._StsPiStsAisCurStatus()
            allFields["StsPiStsLopCurStatus"] = _AF6CCI0011_RD_OCN._upstschstaram._StsPiStsLopCurStatus()
            return allFields

    class _upvtchstkram(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx VT/TU per Alarm Interrupt Status"
    
        def description(self):
            return "This is the per Alarm interrupt status of VT/TU pointer interpreter . Each register is used to store 5 sticky bits for 5 alarms in the VT/TU."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x42800 + 16384*LineId + 32*StsId + 4*VtgId + VtId"
            
        def startAddress(self):
            return 0x00042800
            
        def endAddress(self):
            return 0x00056fff

        class _VtPiStsNewDetIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "VtPiStsNewDetIntr"
            
            def description(self):
                return "Set to 1 while an New Pointer Detection event is detected at VT/TU pointer interpreter. This event doesn't raise interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiStsNdfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "VtPiStsNdfIntr"
            
            def description(self):
                return "Set to 1 while an NDF event is detected at VT/TU pointer interpreter. This event doesn't raise interrupt."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiStsCepUneqVStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "VtPiStsCepUneqVStateChgIntr"
            
            def description(self):
                return "Set 1 to while there is change in Unequip state in the related VT/TU to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status register of the related VT/TU to know the VT/TU whether in Uneqip state or not."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiStsAISStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "VtPiStsAISStateChgIntr"
            
            def description(self):
                return "Set 1 to while there is change in AIS state in the related VT/TU to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status register of the related VT/TU to know the VT/TU whether in LOP state or not."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiStsLopStateChgIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VtPiStsLopStateChgIntr"
            
            def description(self):
                return "Set 1 to while there is change in LOP state in the related VT/TU to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status register of the related VT/TU to know the VT/TU whether in AIS state or not."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VtPiStsNewDetIntr"] = _AF6CCI0011_RD_OCN._upvtchstkram._VtPiStsNewDetIntr()
            allFields["VtPiStsNdfIntr"] = _AF6CCI0011_RD_OCN._upvtchstkram._VtPiStsNdfIntr()
            allFields["VtPiStsCepUneqVStateChgIntr"] = _AF6CCI0011_RD_OCN._upvtchstkram._VtPiStsCepUneqVStateChgIntr()
            allFields["VtPiStsAISStateChgIntr"] = _AF6CCI0011_RD_OCN._upvtchstkram._VtPiStsAISStateChgIntr()
            allFields["VtPiStsLopStateChgIntr"] = _AF6CCI0011_RD_OCN._upvtchstkram._VtPiStsLopStateChgIntr()
            return allFields

    class _upvtchstaram(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx VT/TU per Alarm Current Status"
    
        def description(self):
            return "This is the per Alarm current status of VT/TU pointer interpreter. Each register is used to store 3 bits to store current status of 3 alarms in the VT/TU."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x43000 + 16384*LineId + 32*StsId + 4*VtgId + VtId"
            
        def startAddress(self):
            return 0x00043000
            
        def endAddress(self):
            return 0x00057fff

        class _VtPiStsCepUneqCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "VtPiStsCepUneqCurStatus"
            
            def description(self):
                return "Unequip current status in the related VT/TU. When it changes for 0 to 1 or vice versa, the VtPiStsCepUneqVStateChgIntr bit in the OCN Rx VT/TU per Alarm Interrupt Status register of the related VT/TU is set."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiStsAisCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "VtPiStsAisCurStatus"
            
            def description(self):
                return "AIS current status in the related VT/TU. When it changes for 0 to 1 or vice versa, the VtPiStsAISStateChgIntr bit in the OCN Rx VT/TU per Alarm Interrupt Status register of the related VT/TU is set."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPiStsLopCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VtPiStsLopCurStatus"
            
            def description(self):
                return "LOP current status in the related VT/TU. When it changes for 0 to 1 or vice versa, the VtPiStsLopStateChgIntr bit in the OCN Rx VT/TU per Alarm Interrupt Status register of the related VT/TU is set."
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VtPiStsCepUneqCurStatus"] = _AF6CCI0011_RD_OCN._upvtchstaram._VtPiStsCepUneqCurStatus()
            allFields["VtPiStsAisCurStatus"] = _AF6CCI0011_RD_OCN._upvtchstaram._VtPiStsAisCurStatus()
            allFields["VtPiStsLopCurStatus"] = _AF6CCI0011_RD_OCN._upvtchstaram._VtPiStsLopCurStatus()
            return allFields

    class _adjcntperstsram(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx PP STS/VC Pointer interpreter pointer adjustment per channel counter"
    
        def description(self):
            return "Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS/VC pointer interpreter. These counters are in saturation mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x22080 + 512*LineId + 64*AdjMode + StsId"
            
        def startAddress(self):
            return 0x00022080
            
        def endAddress(self):
            return 0x00022eaf

        class _RxPpStsPiPtAdjCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPpStsPiPtAdjCnt"
            
            def description(self):
                return "The pointer Increment counter or decrease counter. Bit [6] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF)."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPpStsPiPtAdjCnt"] = _AF6CCI0011_RD_OCN._adjcntperstsram._RxPpStsPiPtAdjCnt()
            return allFields

    class _adjcntperstkram(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx PP VT/TU Pointer interpreter pointer adjustment per channel counter"
    
        def description(self):
            return "Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in VT/TU pointer interpreter. These counters are in saturation mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x41000 + 16384*LineId + 2048*AdjMode + 32*StsId + 4*VtgId + VtId"
            
        def startAddress(self):
            return 0x00041000
            
        def endAddress(self):
            return 0x00055fff

        class _RxPpStsPiPtAdjCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPpStsPiPtAdjCnt"
            
            def description(self):
                return "The pointer Increment counter or decrease counter. Bit [11] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF)."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPpStsPiPtAdjCnt"] = _AF6CCI0011_RD_OCN._adjcntperstkram._RxPpStsPiPtAdjCnt()
            return allFields

    class _stspgstkram(AtRegister.AtRegister):
        def name(self):
            return "OCN TxPg STS per Alarm Interrupt Status"
    
        def description(self):
            return "This is the per Alarm interrupt status of STS pointer generator . Each register is used to store 3 sticky bits for 3 alarms"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x23140 + 512*LineId + StsId"
            
        def startAddress(self):
            return 0x00023140
            
        def endAddress(self):
            return 0x00023f6f

        class _StsPgAisIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "StsPgAisIntr"
            
            def description(self):
                return "Set to 1 while an AIS status event (at h2pos) is detected at Tx STS Pointer Generator."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPgFiFoOvfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "StsPgFiFoOvfIntr"
            
            def description(self):
                return "Set to 1 while an FIFO Overflowed event is detected at Tx STS Pointer Generator."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _StsPgNdfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "StsPgNdfIntr"
            
            def description(self):
                return "Set to 1 while an NDF status event (at h2pos) is detected at Tx STS Pointer Generator."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StsPgAisIntr"] = _AF6CCI0011_RD_OCN._stspgstkram._StsPgAisIntr()
            allFields["StsPgFiFoOvfIntr"] = _AF6CCI0011_RD_OCN._stspgstkram._StsPgFiFoOvfIntr()
            allFields["StsPgNdfIntr"] = _AF6CCI0011_RD_OCN._stspgstkram._StsPgNdfIntr()
            return allFields

    class _adjcntpgperstsram(AtRegister.AtRegister):
        def name(self):
            return "OCN TxPg STS pointer adjustment per channel counter"
    
        def description(self):
            return "Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS pointer generator. These counters are in saturation mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x23080 + 512*LineId + 64*AdjMode + StsId"
            
        def startAddress(self):
            return 0x00023080
            
        def endAddress(self):
            return 0x00023eaf

        class _StsPgPtAdjCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "StsPgPtAdjCnt"
            
            def description(self):
                return "The pointer Increment counter or decrease counter. Bit [11] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF)."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StsPgPtAdjCnt"] = _AF6CCI0011_RD_OCN._adjcntpgperstsram._StsPgPtAdjCnt()
            return allFields

    class _vtpgstkram(AtRegister.AtRegister):
        def name(self):
            return "OCN TxPg VTTU per Alarm Interrupt Status"
    
        def description(self):
            return "This is the per Alarm interrupt status of STS/VT/TU pointer generator . Each register is used to store 3 sticky bits for 3 alarms"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x62800 + 16384*LineId + 32*StsId + 4*VtgId + VtId"
            
        def startAddress(self):
            return 0x00062800
            
        def endAddress(self):
            return 0x00076fff

        class _VtPgAisIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "VtPgAisIntr"
            
            def description(self):
                return "Set to 1 while an AIS status event (at h2pos) is detected at Tx VTTU Pointer Generator."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPgFiFoOvfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "VtPgFiFoOvfIntr"
            
            def description(self):
                return "Set to 1 while an FIFO Overflowed event is detected at Tx VTTU Pointer Generator."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VtPgNdfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "VtPgNdfIntr"
            
            def description(self):
                return "Set to 1 while an NDF status event (at h2pos) is detected at Tx VTTU Pointer Generator."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VtPgAisIntr"] = _AF6CCI0011_RD_OCN._vtpgstkram._VtPgAisIntr()
            allFields["VtPgFiFoOvfIntr"] = _AF6CCI0011_RD_OCN._vtpgstkram._VtPgFiFoOvfIntr()
            allFields["VtPgNdfIntr"] = _AF6CCI0011_RD_OCN._vtpgstkram._VtPgNdfIntr()
            return allFields

    class _adjcntpgpervtram(AtRegister.AtRegister):
        def name(self):
            return "OCN TxPg VTTU pointer adjustment per channel counter"
    
        def description(self):
            return "Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in VTTU pointer generator. These counters are in saturation mode"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x61000 + 16384*LineId + 2048*AdjMode + 32*StsId + 4*VtgId + VtId"
            
        def startAddress(self):
            return 0x00061000
            
        def endAddress(self):
            return 0x00075fff

        class _VtpgPtAdjCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VtpgPtAdjCnt"
            
            def description(self):
                return "The pointer Increment counter or decrease counter. Bit [11] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF)."
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VtpgPtAdjCnt"] = _AF6CCI0011_RD_OCN._adjcntpgpervtram._VtpgPtAdjCnt()
            return allFields

    class _rxbarsxcramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx Bridge and Roll SXC Control"
    
        def description(self):
            return "Each register is used for each outgoing STS (to Rx SPI) of any line (8 lines) can be randomly configured to //connect to any ingoing STS of any ingoing line (from TFI-5 line - 8 lines). Backdoor		: irxfrm_inst.isdhbar_sxc.isdhbar_sxcrd[0].rxsxcramctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x26000 + 256*LineId + StsId"
            
        def startAddress(self):
            return 0x00026000
            
        def endAddress(self):
            return 0x0002672f

        class _RxBarSxcLineId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "RxBarSxcLineId"
            
            def description(self):
                return "Contains the ingoing LineID (0-7). Disconnect (output is all one) if value LineID is other value (recommend is 14)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxBarSxcStsId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxBarSxcStsId"
            
            def description(self):
                return "Contains the ingoing STSID (0-47)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxBarSxcLineId"] = _AF6CCI0011_RD_OCN._rxbarsxcramctl._RxBarSxcLineId()
            allFields["RxBarSxcStsId"] = _AF6CCI0011_RD_OCN._rxbarsxcramctl._RxBarSxcStsId()
            return allFields

    class _txbarsxcramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN Tx Bridge and Roll SXC Control"
    
        def description(self):
            return "Each register is used for each outgoing STS (to TFI-5 Line) of any line (8 lines) can be randomly configured to connect to any ingoing STS of any ingoing line (from Tx SPG) . Backdoor		: itxpp_stspp_inst.isdhtxbar_sxc.isdhbar_sxcrd[0].rxsxcramctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x27000 + 256*LineId + StsId"
            
        def startAddress(self):
            return 0x00027000
            
        def endAddress(self):
            return 0x0002772f

        class _TxBarSxcLineId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TxBarSxcLineId"
            
            def description(self):
                return "Contains the ingoing LineID (0-7). Disconnect (output is all one) if value LineID is other value (recommend is 14)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxSxcStsId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxSxcStsId"
            
            def description(self):
                return "Contains the ingoing STSID (0-47)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxBarSxcLineId"] = _AF6CCI0011_RD_OCN._txbarsxcramctl._TxBarSxcLineId()
            allFields["TxSxcStsId"] = _AF6CCI0011_RD_OCN._txbarsxcramctl._TxSxcStsId()
            return allFields

    class _rxhomapramctl(AtRegister.AtRegister):
        def name(self):
            return "OCN Rx High Order Map concatenate configuration"
    
        def description(self):
            return "Each register is used to configure concatenation for each STS to High Order Map path. Backdoor		: irxpp_outho_inst.irxpp_outputho [0].ohoramctl.array"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x28000 + 512*LineId + StsId"
            
        def startAddress(self):
            return 0x00028000
            
        def endAddress(self):
            return 0x0002872f

        class _HoMapStsSlvInd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "HoMapStsSlvInd"
            
            def description(self):
                return "This is used to configure STS is slaver or master. 1: Slaver."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _HoMapStsMstId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HoMapStsMstId"
            
            def description(self):
                return "This is the ID of the master STS-1 in the concatenation that contains this STS-1."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HoMapStsSlvInd"] = _AF6CCI0011_RD_OCN._rxhomapramctl._HoMapStsSlvInd()
            allFields["HoMapStsMstId"] = _AF6CCI0011_RD_OCN._rxhomapramctl._HoMapStsMstId()
            return allFields

    class _parfrccfg1(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Force Config 1 - RxBar+TxBar+SPI+SPG"
    
        def description(self):
            return "OCN Parity Force Config 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _SpgParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "SpgParErrFrc"
            
            def description(self):
                return "Force Parity for ram config \"OCN STS Pointer Generator Per Channel Control\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SpiParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "SpiParErrFrc"
            
            def description(self):
                return "Force Parity for ram config \"OCN STS Pointer Interpreter Per Channel Control\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxBarParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TxBarParErrFrc"
            
            def description(self):
                return "Force Parity for ram config \"OCN Tx Bridge and Roll SXC Control\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxBarParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxBarParErrFrc"
            
            def description(self):
                return "Force Parity for ram config \"OCN Rx Bridge and Roll SXC Control\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SpgParErrFrc"] = _AF6CCI0011_RD_OCN._parfrccfg1._SpgParErrFrc()
            allFields["SpiParErrFrc"] = _AF6CCI0011_RD_OCN._parfrccfg1._SpiParErrFrc()
            allFields["TxBarParErrFrc"] = _AF6CCI0011_RD_OCN._parfrccfg1._TxBarParErrFrc()
            allFields["RxBarParErrFrc"] = _AF6CCI0011_RD_OCN._parfrccfg1._RxBarParErrFrc()
            return allFields

    class _pardiscfg1(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Disable Config 1 - RxBar+TxBar+SPI+SPG"
    
        def description(self):
            return "OCN Parity Disable Config 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0xffffffff

        class _SpgParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "SpgParErrFrc"
            
            def description(self):
                return "Disable Parity for ram config \"OCN STS Pointer Generator Per Channel Control\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SpiParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "SpiParErrFrc"
            
            def description(self):
                return "Disable Parity for ram config \"OCN STS Pointer Interpreter Per Channel Control\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxBarParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TxBarParErrFrc"
            
            def description(self):
                return "Disable Parity for ram config \"OCN Tx Bridge and Roll SXC Control\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxBarParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxBarParErrFrc"
            
            def description(self):
                return "Disable Parity for ram config \"OCN Rx Bridge and Roll SXC Control\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SpgParErrFrc"] = _AF6CCI0011_RD_OCN._pardiscfg1._SpgParErrFrc()
            allFields["SpiParErrFrc"] = _AF6CCI0011_RD_OCN._pardiscfg1._SpiParErrFrc()
            allFields["TxBarParErrFrc"] = _AF6CCI0011_RD_OCN._pardiscfg1._TxBarParErrFrc()
            allFields["RxBarParErrFrc"] = _AF6CCI0011_RD_OCN._pardiscfg1._RxBarParErrFrc()
            return allFields

    class _parerrstk1(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Error Sticky 1 - RxBar+TxBar+SPI+SPG"
    
        def description(self):
            return "OCN Parity Error Sticky 1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000012
            
        def endAddress(self):
            return 0xffffffff

        class _SpgParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "SpgParErrFrc"
            
            def description(self):
                return "Error Sticky for ram config \"OCN STS Pointer Generator Per Channel Control\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _SpiParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "SpiParErrFrc"
            
            def description(self):
                return "Error Sticky for ram config \"OCN STS Pointer Interpreter Per Channel Control\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _TxBarParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TxBarParErrFrc"
            
            def description(self):
                return "Error Sticky for ram config \"OCN Tx Bridge and Roll SXC Control\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _RxBarParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxBarParErrFrc"
            
            def description(self):
                return "Error Sticky for ram config \"OCN Rx Bridge and Roll SXC Control\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SpgParErrFrc"] = _AF6CCI0011_RD_OCN._parerrstk1._SpgParErrFrc()
            allFields["SpiParErrFrc"] = _AF6CCI0011_RD_OCN._parerrstk1._SpiParErrFrc()
            allFields["TxBarParErrFrc"] = _AF6CCI0011_RD_OCN._parerrstk1._TxBarParErrFrc()
            allFields["RxBarParErrFrc"] = _AF6CCI0011_RD_OCN._parerrstk1._RxBarParErrFrc()
            return allFields

    class _parfrccfg2(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Force Config 2 - RxSXC+TxSXC+OHO"
    
        def description(self):
            return "OCN Parity Force Config 2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000014
            
        def endAddress(self):
            return 0xffffffff

        class _RxHoParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 22
        
            def name(self):
                return "RxHoParErrFrc"
            
            def description(self):
                return "Force Parity for ram config \"OCN Rx High Order Map concatenate configuration\".Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxSxcParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 14
        
            def name(self):
                return "TxSxcParErrFrc"
            
            def description(self):
                return "Force Parity for ram config \"OCN Tx SXC Control\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxSxcParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxSxcParErrFrc"
            
            def description(self):
                return "Force Parity for ram config \"OCN Rx SXC Control\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxHoParErrFrc"] = _AF6CCI0011_RD_OCN._parfrccfg2._RxHoParErrFrc()
            allFields["TxSxcParErrFrc"] = _AF6CCI0011_RD_OCN._parfrccfg2._TxSxcParErrFrc()
            allFields["RxSxcParErrFrc"] = _AF6CCI0011_RD_OCN._parfrccfg2._RxSxcParErrFrc()
            return allFields

    class _pardiscfg2(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Disable Config 2 - RxSXC+TxSXC+OHO"
    
        def description(self):
            return "OCN Parity Disable Config 2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000015
            
        def endAddress(self):
            return 0xffffffff

        class _RxHoParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 22
        
            def name(self):
                return "RxHoParErrFrc"
            
            def description(self):
                return "Disable Parity for ram config \"OCN Rx High Order Map concatenate configuration\".Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxSxcParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 14
        
            def name(self):
                return "TxSxcParErrFrc"
            
            def description(self):
                return "Disable Parity for ram config \"OCN Tx SXC Control\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxSxcParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxSxcParErrFrc"
            
            def description(self):
                return "Disable Parity for ram config \"OCN Rx SXC Control\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxHoParErrFrc"] = _AF6CCI0011_RD_OCN._pardiscfg2._RxHoParErrFrc()
            allFields["TxSxcParErrFrc"] = _AF6CCI0011_RD_OCN._pardiscfg2._TxSxcParErrFrc()
            allFields["RxSxcParErrFrc"] = _AF6CCI0011_RD_OCN._pardiscfg2._RxSxcParErrFrc()
            return allFields

    class _parerrstk2(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Error Sticky 2 - RxSXC+TxSXC+OHO"
    
        def description(self):
            return "OCN Parity Error Sticky 2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000016
            
        def endAddress(self):
            return 0xffffffff

        class _RxHoParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 22
        
            def name(self):
                return "RxHoParErrFrc"
            
            def description(self):
                return "Error Sticky for ram config \"OCN Rx High Order Map concatenate configuration\".Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _TxSxcParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 14
        
            def name(self):
                return "TxSxcParErrFrc"
            
            def description(self):
                return "Error Sticky for ram config \"OCN Tx SXC Control\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _RxSxcParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxSxcParErrFrc"
            
            def description(self):
                return "Error Sticky for ram config \"OCN Rx SXC Control\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxHoParErrFrc"] = _AF6CCI0011_RD_OCN._parerrstk2._RxHoParErrFrc()
            allFields["TxSxcParErrFrc"] = _AF6CCI0011_RD_OCN._parerrstk2._TxSxcParErrFrc()
            allFields["RxSxcParErrFrc"] = _AF6CCI0011_RD_OCN._parerrstk2._RxSxcParErrFrc()
            return allFields

    class _parfrccfg3(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Force Config 3 - VPI+VPG"
    
        def description(self):
            return "OCN Parity Force Config 3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000018
            
        def endAddress(self):
            return 0xffffffff

        class _VpgCtlParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 18
        
            def name(self):
                return "VpgCtlParErrFrc"
            
            def description(self):
                return "Force Parity for ram config \"OCN VTTU Pointer Generator Per Channel Control\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VpgDemParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 12
        
            def name(self):
                return "VpgDemParErrFrc"
            
            def description(self):
                return "Force Parity for ram config \"OCN TXPP Per STS Multiplexing Control\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VpiCtlParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 6
        
            def name(self):
                return "VpiCtlParErrFrc"
            
            def description(self):
                return "Force Parity for ram config \"OCN VTTU Pointer Interpreter Per Channel Control\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VpiDemParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VpiDemParErrFrc"
            
            def description(self):
                return "Force Parity for ram config \"OCN RXPP Per STS payload Control\". Each bit correspond with each LineId. Set 1 to force."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VpgCtlParErrFrc"] = _AF6CCI0011_RD_OCN._parfrccfg3._VpgCtlParErrFrc()
            allFields["VpgDemParErrFrc"] = _AF6CCI0011_RD_OCN._parfrccfg3._VpgDemParErrFrc()
            allFields["VpiCtlParErrFrc"] = _AF6CCI0011_RD_OCN._parfrccfg3._VpiCtlParErrFrc()
            allFields["VpiDemParErrFrc"] = _AF6CCI0011_RD_OCN._parfrccfg3._VpiDemParErrFrc()
            return allFields

    class _pardiscfg3(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Disable Config 3 - VPI+VPG"
    
        def description(self):
            return "OCN Parity Disable Config 3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000019
            
        def endAddress(self):
            return 0xffffffff

        class _VpgCtlParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 18
        
            def name(self):
                return "VpgCtlParErrFrc"
            
            def description(self):
                return "Disable Parity for ram config \"OCN VTTU Pointer Generator Per Channel Control\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VpgDemParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 12
        
            def name(self):
                return "VpgDemParErrFrc"
            
            def description(self):
                return "Disable Parity for ram config \"OCN TXPP Per STS Multiplexing Control\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VpiCtlParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 6
        
            def name(self):
                return "VpiCtlParErrFrc"
            
            def description(self):
                return "Disable Parity for ram config \"OCN VTTU Pointer Interpreter Per Channel Control\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VpiDemParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VpiDemParErrFrc"
            
            def description(self):
                return "Disable Parity for ram config \"OCN RXPP Per STS payload Control\". Each bit correspond with each LineId. Set 1 to disable."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VpgCtlParErrFrc"] = _AF6CCI0011_RD_OCN._pardiscfg3._VpgCtlParErrFrc()
            allFields["VpgDemParErrFrc"] = _AF6CCI0011_RD_OCN._pardiscfg3._VpgDemParErrFrc()
            allFields["VpiCtlParErrFrc"] = _AF6CCI0011_RD_OCN._pardiscfg3._VpiCtlParErrFrc()
            allFields["VpiDemParErrFrc"] = _AF6CCI0011_RD_OCN._pardiscfg3._VpiDemParErrFrc()
            return allFields

    class _parerrstk3(AtRegister.AtRegister):
        def name(self):
            return "OCN Parity Error Sticky 3 - VPI+VPG"
    
        def description(self):
            return "OCN Parity Error Sticky 3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000001a
            
        def endAddress(self):
            return 0xffffffff

        class _VpgCtlParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 18
        
            def name(self):
                return "VpgCtlParErrFrc"
            
            def description(self):
                return "Error Sticky for ram config \"OCN VTTU Pointer Generator Per Channel Control\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VpgDemParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 12
        
            def name(self):
                return "VpgDemParErrFrc"
            
            def description(self):
                return "Error Sticky for ram config \"OCN TXPP Per STS Multiplexing Control\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VpiCtlParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 6
        
            def name(self):
                return "VpiCtlParErrFrc"
            
            def description(self):
                return "Error Sticky for ram config \"OCN VTTU Pointer Interpreter Per Channel Control\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _VpiDemParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VpiDemParErrFrc"
            
            def description(self):
                return "Error Sticky for ram config \"OCN RXPP Per STS payload Control\". Each bit correspond with each LineId."
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VpgCtlParErrFrc"] = _AF6CCI0011_RD_OCN._parerrstk3._VpgCtlParErrFrc()
            allFields["VpgDemParErrFrc"] = _AF6CCI0011_RD_OCN._parerrstk3._VpgDemParErrFrc()
            allFields["VpiCtlParErrFrc"] = _AF6CCI0011_RD_OCN._parerrstk3._VpiCtlParErrFrc()
            allFields["VpiDemParErrFrc"] = _AF6CCI0011_RD_OCN._parerrstk3._VpiDemParErrFrc()
            return allFields

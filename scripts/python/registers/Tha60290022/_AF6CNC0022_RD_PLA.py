import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0022_RD_PLA(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["pla_pld_ctrl"] = _AF6CNC0022_RD_PLA._pla_pld_ctrl()
        allRegisters["pla_add_rmv_pw_ctrl"] = _AF6CNC0022_RD_PLA._pla_add_rmv_pw_ctrl()
        allRegisters["pla_pw_hdr_ctrl"] = _AF6CNC0022_RD_PLA._pla_pw_hdr_ctrl()
        allRegisters["pla_pw_psn_ctrl"] = _AF6CNC0022_RD_PLA._pla_pw_psn_ctrl()
        allRegisters["pla_pw_pro_ctrl"] = _AF6CNC0022_RD_PLA._pla_pw_pro_ctrl()
        allRegisters["pla_out_upsr_ctrl"] = _AF6CNC0022_RD_PLA._pla_out_upsr_ctrl()
        allRegisters["pla_out_hspw_ctrl"] = _AF6CNC0022_RD_PLA._pla_out_hspw_ctrl()
        allRegisters["pla_out_psnpro_ctrl"] = _AF6CNC0022_RD_PLA._pla_out_psnpro_ctrl()
        allRegisters["pla_out_psnbuf_ctrl"] = _AF6CNC0022_RD_PLA._pla_out_psnbuf_ctrl()
        allRegisters["pla_hold_reg_ctrl"] = _AF6CNC0022_RD_PLA._pla_hold_reg_ctrl()
        allRegisters["rdha3_0_control"] = _AF6CNC0022_RD_PLA._rdha3_0_control()
        allRegisters["rdha7_4_control"] = _AF6CNC0022_RD_PLA._rdha7_4_control()
        allRegisters["rdha11_8_control"] = _AF6CNC0022_RD_PLA._rdha11_8_control()
        allRegisters["rdha15_12_control"] = _AF6CNC0022_RD_PLA._rdha15_12_control()
        allRegisters["rdha19_16_control"] = _AF6CNC0022_RD_PLA._rdha19_16_control()
        allRegisters["rdha23_20_control"] = _AF6CNC0022_RD_PLA._rdha23_20_control()
        allRegisters["rdha24data_control"] = _AF6CNC0022_RD_PLA._rdha24data_control()
        allRegisters["rdha_hold63_32"] = _AF6CNC0022_RD_PLA._rdha_hold63_32()
        allRegisters["rdindr_hold95_64"] = _AF6CNC0022_RD_PLA._rdindr_hold95_64()
        allRegisters["rdindr_hold127_96"] = _AF6CNC0022_RD_PLA._rdindr_hold127_96()
        allRegisters["pla_out_psnpro_ha_ctrl"] = _AF6CNC0022_RD_PLA._pla_out_psnpro_ha_ctrl()
        allRegisters["pla_force_par_err_control"] = _AF6CNC0022_RD_PLA._pla_force_par_err_control()
        allRegisters["pla_dis_par_control"] = _AF6CNC0022_RD_PLA._pla_dis_par_control()
        allRegisters["pla_par_err_stk"] = _AF6CNC0022_RD_PLA._pla_par_err_stk()
        allRegisters["pla_force_crc_err_control"] = _AF6CNC0022_RD_PLA._pla_force_crc_err_control()
        allRegisters["pla_crc_sticky"] = _AF6CNC0022_RD_PLA._pla_crc_sticky()
        allRegisters["PLA_crc_counter"] = _AF6CNC0022_RD_PLA._PLA_crc_counter()
        allRegisters["pla_192c_pld_ctrl"] = _AF6CNC0022_RD_PLA._pla_192c_pld_ctrl()
        allRegisters["pla_oc192c_add_rmv_pw_ctrl"] = _AF6CNC0022_RD_PLA._pla_oc192c_add_rmv_pw_ctrl()
        allRegisters["bert_montdm_config"] = _AF6CNC0022_RD_PLA._bert_montdm_config()
        allRegisters["goodbit_pen_tdm_mon_r2c"] = _AF6CNC0022_RD_PLA._goodbit_pen_tdm_mon_r2c()
        allRegisters["goodbit_pen_tdm_mon_ro"] = _AF6CNC0022_RD_PLA._goodbit_pen_tdm_mon_ro()
        allRegisters["errorbit_pen_tdm_mon_r2c"] = _AF6CNC0022_RD_PLA._errorbit_pen_tdm_mon_r2c()
        allRegisters["errorbit_pen_tdm_mon_ro"] = _AF6CNC0022_RD_PLA._errorbit_pen_tdm_mon_ro()
        allRegisters["lostbit_pen_tdm_mon_r2c"] = _AF6CNC0022_RD_PLA._lostbit_pen_tdm_mon_r2c()
        allRegisters["lostbit_pen_tdm_mon_ro"] = _AF6CNC0022_RD_PLA._lostbit_pen_tdm_mon_ro()
        allRegisters["lostbit_pen_tdm_mon_ro"] = _AF6CNC0022_RD_PLA._lostbit_pen_tdm_mon_ro()
        allRegisters["lostbit_pen_tdm_mon_ro"] = _AF6CNC0022_RD_PLA._lostbit_pen_tdm_mon_ro()
        return allRegisters

    class _pla_pld_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Low-Order Payload Control"
    
        def description(self):
            return "This register is used to configure payload in each Pseudowire channels per slice HDL_PATH  : ipwcore.ipwslcore.isl48core[$Oc96Slice*2 + $Oc48Slice].irtlpla.cfgpwctrl.membuf.ram.ram[$Pwid]"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_1000 + $Oc96Slice*32768 + $Oc48Slice*8192 + $Pwid"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0x0001b7ff

        class _PlaLkPWIDCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 18
        
            def name(self):
                return "PlaLkPWIDCtrl"
            
            def description(self):
                return "Lookup to a output psedowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoPldTSSrcCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PlaLoPldTSSrcCtrl"
            
            def description(self):
                return "Timestamp Source 0: PRC global 1: TS from CDR engine"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoPldTypeCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 15
        
            def name(self):
                return "PlaLoPldTypeCtrl"
            
            def description(self):
                return "Payload Type 0: satop 1: ces without cas 2: cep"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoPldSizeCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaLoPldSizeCtrl"
            
            def description(self):
                return "Payload Size satop/cep mode: bit[13:0] payload size (ex: value as 0x100 is payload size 256 bytes) ces mode: bit[5:0] number of DS0 timeslot bit[14:6] number of NxDS0 frame"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaLkPWIDCtrl"] = _AF6CNC0022_RD_PLA._pla_pld_ctrl._PlaLkPWIDCtrl()
            allFields["PlaLoPldTSSrcCtrl"] = _AF6CNC0022_RD_PLA._pla_pld_ctrl._PlaLoPldTSSrcCtrl()
            allFields["PlaLoPldTypeCtrl"] = _AF6CNC0022_RD_PLA._pla_pld_ctrl._PlaLoPldTypeCtrl()
            allFields["PlaLoPldSizeCtrl"] = _AF6CNC0022_RD_PLA._pla_pld_ctrl._PlaLoPldSizeCtrl()
            return allFields

    class _pla_add_rmv_pw_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Add/Remove Pseudowire Protocol Control"
    
        def description(self):
            return "This register is used to add/remove pseudowire to prevent burst packets to other packet processing engines in each OC-48 slice"
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_1800 + $Oc96Slice*32768 + $Oc48Slice*8192 + $Pwid"
            
        def startAddress(self):
            return 0x00001800
            
        def endAddress(self):
            return 0x0001bfff

        class _PlaLoStaAddRmvCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaLoStaAddRmvCtrl"
            
            def description(self):
                return "protocol state to add/remove pw Step1: Add intitial or pw idle, the protocol state value is \"zero\" Step2: For adding the pw, CPU will Write \"1\" value for the protocol state to start adding pw. The protocol state value is \"1\". Step3: CPU enables pw at demap to finish the adding pw process. HW will automatically change the protocol state value to \"2\" to run the pw, and keep this state. Step4: For removing the pw, CPU will write \"3\" value for the protocol state to start removing pw. The protocol state value is \"3\". Step5: Poll the protocol state until return value \"0\" value, after that CPU disables pw at demap to finish the removing pw process"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaLoStaAddRmvCtrl"] = _AF6CNC0022_RD_PLA._pla_add_rmv_pw_ctrl._PlaLoStaAddRmvCtrl()
            return allFields

    class _pla_pw_hdr_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Pseudowire Header Control"
    
        def description(self):
            return "This register is used to config Control Word for the psedowire channels HDL_PATH  : ipwcore.ipwcfg.membuf.ram.ram[$pwid]"
            
        def width(self):
            return 10
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x2_0000 + $pwid"
            
        def startAddress(self):
            return 0x00020000
            
        def endAddress(self):
            return 0x000229ff

        class _PlaPwHiRateCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PlaPwHiRateCtrl"
            
            def description(self):
                return "set 1 for high rate path (12c/48c/192c), other is set 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPwSuprCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PlaPwSuprCtrl"
            
            def description(self):
                return "Suppresion Enable 1: enable 0: disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPwRbitDisCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PlaPwRbitDisCtrl"
            
            def description(self):
                return "R bit disable 1: disable R bit in the Control Word (assign zero in the packet) 0: normal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPwMbitDisCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PlaPwMbitDisCtrl"
            
            def description(self):
                return "M or NP bits disable 1: disable M or NP bits in the Control Word (assign zero in the packet) 0: normal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPwLbitDisCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PlaPwLbitDisCtrl"
            
            def description(self):
                return "L bit disable 1: disable L bit in the Control Word (assign zero in the packet) 0: normal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPwRbitCPUCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PlaPwRbitCPUCtrl"
            
            def description(self):
                return "R bit value from CPU (low priority than PlaPwRbitDisCtrl)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPwMbitCPUCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PlaPwMbitCPUCtrl"
            
            def description(self):
                return "M or NP bits value from CPU (low priority than PlaLoPwMbitDisCtrl)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPwLbitCPUCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PlaPwLbitCPUCtrl"
            
            def description(self):
                return "L bit value from CPU (low priority than PlaLoPwLbitDisCtrl)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPwEnCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaPwEnCtrl"
            
            def description(self):
                return "Pseudowire enable 1: enable 0: disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaPwHiRateCtrl"] = _AF6CNC0022_RD_PLA._pla_pw_hdr_ctrl._PlaPwHiRateCtrl()
            allFields["PlaPwSuprCtrl"] = _AF6CNC0022_RD_PLA._pla_pw_hdr_ctrl._PlaPwSuprCtrl()
            allFields["PlaPwRbitDisCtrl"] = _AF6CNC0022_RD_PLA._pla_pw_hdr_ctrl._PlaPwRbitDisCtrl()
            allFields["PlaPwMbitDisCtrl"] = _AF6CNC0022_RD_PLA._pla_pw_hdr_ctrl._PlaPwMbitDisCtrl()
            allFields["PlaPwLbitDisCtrl"] = _AF6CNC0022_RD_PLA._pla_pw_hdr_ctrl._PlaPwLbitDisCtrl()
            allFields["PlaPwRbitCPUCtrl"] = _AF6CNC0022_RD_PLA._pla_pw_hdr_ctrl._PlaPwRbitCPUCtrl()
            allFields["PlaPwMbitCPUCtrl"] = _AF6CNC0022_RD_PLA._pla_pw_hdr_ctrl._PlaPwMbitCPUCtrl()
            allFields["PlaPwLbitCPUCtrl"] = _AF6CNC0022_RD_PLA._pla_pw_hdr_ctrl._PlaPwLbitCPUCtrl()
            allFields["PlaPwEnCtrl"] = _AF6CNC0022_RD_PLA._pla_pw_hdr_ctrl._PlaPwEnCtrl()
            return allFields

    class _pla_pw_psn_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Pseudowire PSN Control"
    
        def description(self):
            return "This register is used to config psedowire PSN and APS at output HDL_PATH  : irdport.iflowcfg.membuf.ram.ram[$pwid]"
            
        def width(self):
            return 7
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4_8000 + $pwid"
            
        def startAddress(self):
            return 0x00048000
            
        def endAddress(self):
            return 0x0004a9ff

        class _PlaOutPSNLenCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaOutPSNLenCtrl"
            
            def description(self):
                return "PSN length"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaOutPSNLenCtrl"] = _AF6CNC0022_RD_PLA._pla_pw_psn_ctrl._PlaOutPSNLenCtrl()
            return allFields

    class _pla_pw_pro_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Psedowire Protection Control"
    
        def description(self):
            return "This register is used to config UPSR/HSPW for protect psedowire HDL_PATH  : irdport.iapslkcfg.membuf.ram.ram[$pwid]"
            
        def width(self):
            return 30
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x5_0000 + $pwid"
            
        def startAddress(self):
            return 0x00050000
            
        def endAddress(self):
            return 0x000529ff

        class _PlaOutUpsrGrpCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PlaOutUpsrGrpCtr"
            
            def description(self):
                return "UPRS group"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutHspwGrpCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PlaOutHspwGrpCtr"
            
            def description(self):
                return "HSPW group"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaFlowUPSRUsedCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PlaFlowUPSRUsedCtrl"
            
            def description(self):
                return "Flow UPSR is used or not 0:not used, all configurations for UPSR will be not effected 1:used"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaFlowHSPWUsedCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaFlowHSPWUsedCtrl"
            
            def description(self):
                return "Flow HSPW is used or not 0:not used, all configurations for HSPW will be not effected 1:used"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaOutUpsrGrpCtr"] = _AF6CNC0022_RD_PLA._pla_pw_pro_ctrl._PlaOutUpsrGrpCtr()
            allFields["PlaOutHspwGrpCtr"] = _AF6CNC0022_RD_PLA._pla_pw_pro_ctrl._PlaOutHspwGrpCtr()
            allFields["PlaFlowUPSRUsedCtrl"] = _AF6CNC0022_RD_PLA._pla_pw_pro_ctrl._PlaFlowUPSRUsedCtrl()
            allFields["PlaFlowHSPWUsedCtrl"] = _AF6CNC0022_RD_PLA._pla_pw_pro_ctrl._PlaFlowHSPWUsedCtrl()
            return allFields

    class _pla_out_upsr_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Output UPSR Control"
    
        def description(self):
            return "This register is used to config UPSR group HDL_PATH  : irdport.iupsrcfg.membuf.ram.ram[$upsrgrp]"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x5_8000 + $upsrgrp"
            
        def startAddress(self):
            return 0x00058000
            
        def endAddress(self):
            return 0x0005a9ff

        class _PlaOutUpsrEnCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaOutUpsrEnCtr"
            
            def description(self):
                return "Total is 10752 upsrID, divide into 672 group address for configuration, 16-bit is correlative with 16 upsrID each group. Bit#0 is for upsrID#0, bit#1 is for upsrID#1... 1: enable 0: disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaOutUpsrEnCtr"] = _AF6CNC0022_RD_PLA._pla_out_upsr_ctrl._PlaOutUpsrEnCtr()
            return allFields

    class _pla_out_hspw_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Output HSPW Control"
    
        def description(self):
            return "This register is used to config HSPW group HDL_PATH  : irdport.ihspwcfg.membuf.ram.ram[$hspwgrp]"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x6_0000 + $hspwgrp"
            
        def startAddress(self):
            return 0x00060000
            
        def endAddress(self):
            return 0x0006029f

        class _PlaOutHSPWCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaOutHSPWCtr"
            
            def description(self):
                return "Total is 10752 hspwID, divide into 672 group address for configuration, 16-bit is correlative with 16 hspwID each group. Bit#0 is for hspwID#0, bit#1 is for hspwID#1..."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaOutHSPWCtr"] = _AF6CNC0022_RD_PLA._pla_out_hspw_ctrl._PlaOutHSPWCtr()
            return allFields

    class _pla_out_psnpro_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Output PSN Process Control"
    
        def description(self):
            return "This register is used to control PSN configuration"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4_0000"
            
        def startAddress(self):
            return 0x00040000
            
        def endAddress(self):
            return 0xffffffff

        class _PlaOutPsnProReqCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "PlaOutPsnProReqCtr"
            
            def description(self):
                return "Request to process PSN 1: request to write/read PSN header buffer. The CPU need to prepare a complete PSN header into PSN buffer before request write OR read out a complete PSN header in PSN buffer after request read done 0: HW will automatically set to 0 when a request done"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutPsnProRnWCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "PlaOutPsnProRnWCtr"
            
            def description(self):
                return "read or write PSN 1: read PSN 0: write PSN"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutPsnProLenCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PlaOutPsnProLenCtr"
            
            def description(self):
                return "Length of a complete PSN need to read/write"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutPsnProPageCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "PlaOutPsnProPageCtr"
            
            def description(self):
                return "there is 2 pages PSN location per PWID, depend on the configuration of \"PlaOutHspwPsnCtr\", the engine will choice which the page to encapsulate into the packet."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutPsnProFlowCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaOutPsnProFlowCtr"
            
            def description(self):
                return "Flow channel"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaOutPsnProReqCtr"] = _AF6CNC0022_RD_PLA._pla_out_psnpro_ctrl._PlaOutPsnProReqCtr()
            allFields["PlaOutPsnProRnWCtr"] = _AF6CNC0022_RD_PLA._pla_out_psnpro_ctrl._PlaOutPsnProRnWCtr()
            allFields["PlaOutPsnProLenCtr"] = _AF6CNC0022_RD_PLA._pla_out_psnpro_ctrl._PlaOutPsnProLenCtr()
            allFields["PlaOutPsnProPageCtr"] = _AF6CNC0022_RD_PLA._pla_out_psnpro_ctrl._PlaOutPsnProPageCtr()
            allFields["PlaOutPsnProFlowCtr"] = _AF6CNC0022_RD_PLA._pla_out_psnpro_ctrl._PlaOutPsnProFlowCtr()
            return allFields

    class _pla_out_psnbuf_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Output PSN Buffer Control"
    
        def description(self):
            return "This register is used to store PSN data which is before CPU request write or after CPU request read %% The header format from entry#0 to entry#4 is as follow: %% {DA(6-byte),SA(6-byte),VLAN1(4-byte,optional),VLAN2(4-byte, optional),EthType(2-byte),PSN Header(4 to 96 bytes)} %% Depends on specific PSN selected for each pseudowire, the EthType and PSN Header field may have the following values and formats: %%  PSN header is MEF-8: %% EthType:  0x88D8 %% 4-byte PSN Header with format: {ECID[19:0], 0x102} where ECID is pseodowire identification for 	remote 	receive side. %% PSN header is MPLS:  %% EthType:  0x8847 %% 4/8/12-byte PSN Header with format: {OuterLabel2[31:0](optional), OuterLabel1[31:0](optional),  InnerLabel[31:0]}   %% Where InnerLabel[31:12] is pseodowire identification for remote receive side. %% Label format: {Idenfifier[19:0],Exp[2:0],StackBit,TTL[7:0]} where StackBit =  for InnerLabel %% PSN header is UDP/Ipv4:  %% EthType:  0x0800 %% 28-byte PSN Header with format {Ipv4 Header(20 bytes), UDP Header(8 byte)} as below: %% {IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0], IP_Header_Sum[31:16]} %% {IP_Iden[15:0], IP_Flag[2:0], IP_Frag_Offset[12:0]}  %% {IP_TTL[7:0], IP_Protocol[7:0], IP_Header_Sum[15:0]} %% {IP_SrcAdr[31:0]} %% {IP_DesAdr[31:0]} %% {UDP_SrcPort[15:0], UDP_DesPort[15:0]} %% {UDP_Sum[31:0] (optional)} %% Case: %% IP_Protocol[7:0]: 0x11 to signify UDP  %% IP_Protocol[7:0]: 0x11 to signify UDP  %% IP_Header_Sum[31:0]:  CPU calculate these fields as a temporarily for HW before plus rest fields %% {IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0]} +%% IP_Iden[15:0] + {IP_Flag[2:0], IP_Frag_Offset[12:0]}  +%% {IP_TTL[7:0], IP_Protocol[7:0]} + %% IP_SrcAdr[31:16] + IP_SrcAdr[15:0] +%% IP_DesAdr[31:16] + IP_DesAdr[15:0]%% UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused%% UDP_DesPort : used as remote pseudowire identification or 0x85E if unused%% UDP_Sum[31:0] (optional): CPU calculate these fields as a temporarily for HW before plus rest fields%% IP_SrcAdr[127:112] + 	IP_SrcAdr[111:96] + 	%% IP_SrcAdr[95:80]  + IP_SrcAdr[79:64] + 	%% IP_SrcAdr[63:48]  + IP_SrcAdr[47:32] + 	%% IP_SrcAdr[31:16]  + IP_SrcAdr[15:0] + 	%% IP_DesAdr[127:112] + 	IP_DesAdr[111:96] + 	%% IP_DesAdr[95:80]  + IP_DesAdr[79:64] + 	%% IP_DesAdr[63:48]  + IP_DesAdr[47:32] + 	%% IP_DesAdr[31:16]  + IP_DesAdr[15:0] +%% {8-bit zeros, IP_Protocol[7:0]} +%% UDP_SrcPort[15:0] +  UDP_DesPort[15:0]%% PSN header is UDP/Ipv6:  %% EthType:  0x86DD%% 48-byte PSN Header with format {Ipv6 Header(40 bytes), UDP Header(8 byte)} as below:%% {IP_Ver[3:0], IP_Traffic_Class[7:0], IP_Flow_Label[19:0]}%% {16-bit zeros, IP_Next_Header[7:0], IP_Hop_Limit[7:0]} %% {IP_SrcAdr[127:96]}%% {IP_SrcAdr[95:64]}%% {IP_SrcAdr[63:32]}%% {IP_SrcAdr[31:0]}%% {IP_DesAdr[127:96]}%% {IP_DesAdr[95:64]}%% {IP_DesAdr[63:32]}%% {IP_DesAdr[31:0]}%% {UDP_SrcPort[15:0], UDP_DesPort[15:0]}%% {UDP_Sum[31:0]}%% Case:%% IP_Next_Header[7:0]: 0x11 to signify UDP %% UDP_Sum[31:0]:  CPU calculate these fields as a temporarily for HW before plus rest fields%% IP_SrcAdr[127:112] + 	IP_SrcAdr[111:96] + 	%% IP_SrcAdr[95:80]  + IP_SrcAdr[79:64] + 	%% IP_SrcAdr[63:48]  + IP_SrcAdr[47:32] + 	%% IP_SrcAdr[31:16]  + IP_SrcAdr[15:0] + 	%% IP_DesAdr[127:112] + 	IP_DesAdr[111:96] + 	%% IP_DesAdr[95:80]  + IP_DesAdr[79:64] + 	%% IP_DesAdr[63:48]  + IP_DesAdr[47:32] + 	%% IP_DesAdr[31:16]  + IP_DesAdr[15:0] +%% {8-bit zeros, IP_Next_Header[7:0]} +%% UDP_SrcPort[15:0] +  UDP_DesPort[15:0]%% UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused%% UDP_DesPort : used as remote pseudowire identification or 0x85E if unused%% User can select either source or destination port for pseodowire identification. See IP/UDP standards for more description about other field. %% PSN header is MPLS over Ipv4:%% IPv4 header must have IP_Protocol[7:0] value 0x89 to signify MPLS%% After IPv4 header is MPLS labels where inner label is used for PW identification%% PSN header is MPLS over Ipv6:%% IPv6 header must have IP_Next_Header[7:0] value 0x89 to signify MPLS%% After IPv6 header is MPLS labels where inner label is used for PW identification%% PSN header (all modes) with RTP enable: RTP used for TDM PW (define in RFC3550), is in the first 8-byte of this buffer (bit[127:64] of segid == 0), following is PSN header (start from bit[63:0] of segid = 0)%% Case:%% RTPSSRC[31:0] : This is the SSRC value of RTP header%% RtpPtValue[6:0]: This is the PT value of RTP header, define in http://www.iana.org/assignments/rtp-parameters/rtp-parameters.xml"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4_0010 + $segid"
            
        def startAddress(self):
            return 0x00040010
            
        def endAddress(self):
            return 0x00040015

        class _PlaOutPsnProReqCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaOutPsnProReqCtr"
            
            def description(self):
                return "PSN buffer"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaOutPsnProReqCtr"] = _AF6CNC0022_RD_PLA._pla_out_psnbuf_ctrl._PlaOutPsnProReqCtr()
            return allFields

    class _pla_hold_reg_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Hold Register Control"
    
        def description(self):
            return "This register is used to control hold register."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x7_0000 + $holdreg"
            
        def startAddress(self):
            return 0x00070000
            
        def endAddress(self):
            return 0x00070002

        class _PlaHoldRegCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaHoldRegCtr"
            
            def description(self):
                return "hold register value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaHoldRegCtr"] = _AF6CNC0022_RD_PLA._pla_hold_reg_ctrl._PlaHoldRegCtr()
            return allFields

    class _rdha3_0_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit3_0 Control"
    
        def description(self):
            return "This register is used to send HA read address bit3_0 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x70100 + HaAddr3_0"
            
        def startAddress(self):
            return 0x00070100
            
        def endAddress(self):
            return 0x0007010f

        class _ReadAddr3_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr3_0"
            
            def description(self):
                return "Read value will be 0x8C100 plus HaAddr3_0"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr3_0"] = _AF6CNC0022_RD_PLA._rdha3_0_control._ReadAddr3_0()
            return allFields

    class _rdha7_4_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit7_4 Control"
    
        def description(self):
            return "This register is used to send HA read address bit7_4 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x70110 + HaAddr7_4"
            
        def startAddress(self):
            return 0x00070110
            
        def endAddress(self):
            return 0x0007011f

        class _ReadAddr7_4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr7_4"
            
            def description(self):
                return "Read value will be 0x8C100 plus HaAddr7_4"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr7_4"] = _AF6CNC0022_RD_PLA._rdha7_4_control._ReadAddr7_4()
            return allFields

    class _rdha11_8_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit11_8 Control"
    
        def description(self):
            return "This register is used to send HA read address bit11_8 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x70120 + HaAddr11_8"
            
        def startAddress(self):
            return 0x00070120
            
        def endAddress(self):
            return 0x0007012f

        class _ReadAddr11_8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr11_8"
            
            def description(self):
                return "Read value will be 0x8C100 plus HaAddr11_8"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr11_8"] = _AF6CNC0022_RD_PLA._rdha11_8_control._ReadAddr11_8()
            return allFields

    class _rdha15_12_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit15_12 Control"
    
        def description(self):
            return "This register is used to send HA read address bit15_12 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x070130 + HaAddr15_12"
            
        def startAddress(self):
            return 0x00070130
            
        def endAddress(self):
            return 0x0007013f

        class _ReadAddr15_12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr15_12"
            
            def description(self):
                return "Read value will be 0x8C100 plus HaAddr15_12"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr15_12"] = _AF6CNC0022_RD_PLA._rdha15_12_control._ReadAddr15_12()
            return allFields

    class _rdha19_16_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit19_16 Control"
    
        def description(self):
            return "This register is used to send HA read address bit19_16 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x70140 + HaAddr19_16"
            
        def startAddress(self):
            return 0x00070140
            
        def endAddress(self):
            return 0x0007014f

        class _ReadAddr19_16(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr19_16"
            
            def description(self):
                return "Read value will be 0x8C100 plus HaAddr19_16"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr19_16"] = _AF6CNC0022_RD_PLA._rdha19_16_control._ReadAddr19_16()
            return allFields

    class _rdha23_20_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit23_20 Control"
    
        def description(self):
            return "This register is used to send HA read address bit23_20 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x70150 + HaAddr23_20"
            
        def startAddress(self):
            return 0x00070150
            
        def endAddress(self):
            return 0x0007015f

        class _ReadAddr23_20(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr23_20"
            
            def description(self):
                return "Read value will be 0x8C100 plus HaAddr23_20"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr23_20"] = _AF6CNC0022_RD_PLA._rdha23_20_control._ReadAddr23_20()
            return allFields

    class _rdha24data_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit24 and Data Control"
    
        def description(self):
            return "This register is used to send HA read address bit24 to HA engine to read data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x70160 + HaAddr24"
            
        def startAddress(self):
            return 0x00070160
            
        def endAddress(self):
            return 0x00070161

        class _ReadHaData31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData31_0"
            
            def description(self):
                return "HA read data bit31_0"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData31_0"] = _AF6CNC0022_RD_PLA._rdha24data_control._ReadHaData31_0()
            return allFields

    class _rdha_hold63_32(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data63_32"
    
        def description(self):
            return "This register is used to read HA dword2 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00070170
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData63_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData63_32"
            
            def description(self):
                return "HA read data bit63_32"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData63_32"] = _AF6CNC0022_RD_PLA._rdha_hold63_32._ReadHaData63_32()
            return allFields

    class _rdindr_hold95_64(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data95_64"
    
        def description(self):
            return "This register is used to read HA dword3 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00070171
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData95_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData95_64"
            
            def description(self):
                return "HA read data bit95_64"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData95_64"] = _AF6CNC0022_RD_PLA._rdindr_hold95_64._ReadHaData95_64()
            return allFields

    class _rdindr_hold127_96(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data127_96"
    
        def description(self):
            return "This register is used to read HA dword4 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00070172
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData127_96(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData127_96"
            
            def description(self):
                return "HA read data bit127_96"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData127_96"] = _AF6CNC0022_RD_PLA._rdindr_hold127_96._ReadHaData127_96()
            return allFields

    class _pla_out_psnpro_ha_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Output PSN Process Control"
    
        def description(self):
            return "This register is used to control PSN configuration"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00040001
            
        def endAddress(self):
            return 0xffffffff

        class _PlaOutPsnProReqCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "PlaOutPsnProReqCtr"
            
            def description(self):
                return "Request to process PSN 1: request to write/read PSN header buffer. The CPU need to prepare a complete PSN header into PSN buffer before request write OR read out a complete PSN header in PSN buffer after request read done 0: HW will automatically set to 0 when a request done"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutPsnProLenCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PlaOutPsnProLenCtr"
            
            def description(self):
                return "Length of a complete PSN need to read/write"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutPsnProPageCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PlaOutPsnProPageCtr"
            
            def description(self):
                return "there is 2 pages PSN location per PWID, depend on the configuration of \"PlaOutHspwPsnCtr\", the engine will choice which the page to encapsulate into the packet."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaOutPsnProPWIDCtr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaOutPsnProPWIDCtr"
            
            def description(self):
                return "Pseudowire channel"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaOutPsnProReqCtr"] = _AF6CNC0022_RD_PLA._pla_out_psnpro_ha_ctrl._PlaOutPsnProReqCtr()
            allFields["PlaOutPsnProLenCtr"] = _AF6CNC0022_RD_PLA._pla_out_psnpro_ha_ctrl._PlaOutPsnProLenCtr()
            allFields["PlaOutPsnProPageCtr"] = _AF6CNC0022_RD_PLA._pla_out_psnpro_ha_ctrl._PlaOutPsnProPageCtr()
            allFields["PlaOutPsnProPWIDCtr"] = _AF6CNC0022_RD_PLA._pla_out_psnpro_ha_ctrl._PlaOutPsnProPWIDCtr()
            return allFields

    class _pla_force_par_err_control(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Force Parity Error Control"
    
        def description(self):
            return "This register is used to force parity error"
            
        def width(self):
            return 21
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00042040
            
        def endAddress(self):
            return 0xffffffff

        class _ForceParErrOutUPSRCtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "ForceParErrOutUPSRCtrReg"
            
            def description(self):
                return "Force Parity Error Ouput UPSR Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrPWProCtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "ForceParErrPWProCtrReg"
            
            def description(self):
                return "Force Parity Error PW Protection Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrPWPSNCtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "ForceParErrPWPSNCtrReg"
            
            def description(self):
                return "Force Parity Error PW PSNr Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrPWHdrCtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ForceParErrPWHdrCtrReg"
            
            def description(self):
                return "Force Parity Error PW Header Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLkPWCtrRegOC96_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "ForceParErrLkPWCtrRegOC96_3"
            
            def description(self):
                return "Force Parity Error Lookup PW  Control Register OC96#3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrPldCtrRegOC96_3_OC48_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "ForceParErrPldCtrRegOC96_3_OC48_1"
            
            def description(self):
                return "Force Parity Error Payload Control Register OC48#1 of OC96#3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrPldCtrRegOC96_3_OC48_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "ForceParErrPldCtrRegOC96_3_OC48_0"
            
            def description(self):
                return "Force Parity Error Payload Control Register OC48#0 of OC96#3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLkPWCtrRegOC96_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "ForceParErrLkPWCtrRegOC96_2"
            
            def description(self):
                return "Force Parity Error Lookup PW  Control Register OC96#2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrPldCtrRegOC96_2_OC48_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "ForceParErrPldCtrRegOC96_2_OC48_1"
            
            def description(self):
                return "Force Parity Error Payload Control Register OC48#1 of OC96#2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrPldCtrRegOC96_2_OC48_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "ForceParErrPldCtrRegOC96_2_OC48_0"
            
            def description(self):
                return "Force Parity Error Payload Control Register OC48#0 of OC96#2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLkPWCtrRegOC96_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "ForceParErrLkPWCtrRegOC96_1"
            
            def description(self):
                return "Force Parity Error Lookup PW  Control Register OC96#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrPldCtrRegOC96_1_OC48_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "ForceParErrPldCtrRegOC96_1_OC48_1"
            
            def description(self):
                return "Force Parity Error Payload Control Register OC48#1 of OC96#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrPldCtrRegOC96_1_OC48_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "ForceParErrPldCtrRegOC96_1_OC48_0"
            
            def description(self):
                return "Force Parity Error Payload Control Register OC48#0 of OC96#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrLkPWCtrRegOC96_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "ForceParErrLkPWCtrRegOC96_0"
            
            def description(self):
                return "Force Parity Error Lookup PW  Control Register OC96#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrPldCtrRegOC96_0_OC48_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "ForceParErrPldCtrRegOC96_0_OC48_1"
            
            def description(self):
                return "Force Parity Error Payload Control Register OC48#1 of OC96#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ForceParErrPldCtrRegOC96_0_OC48_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ForceParErrPldCtrRegOC96_0_OC48_0"
            
            def description(self):
                return "Force Parity Error Payload Control Register OC48#0 of OC96#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ForceParErrOutUPSRCtrReg"] = _AF6CNC0022_RD_PLA._pla_force_par_err_control._ForceParErrOutUPSRCtrReg()
            allFields["ForceParErrPWProCtrReg"] = _AF6CNC0022_RD_PLA._pla_force_par_err_control._ForceParErrPWProCtrReg()
            allFields["ForceParErrPWPSNCtrReg"] = _AF6CNC0022_RD_PLA._pla_force_par_err_control._ForceParErrPWPSNCtrReg()
            allFields["ForceParErrPWHdrCtrReg"] = _AF6CNC0022_RD_PLA._pla_force_par_err_control._ForceParErrPWHdrCtrReg()
            allFields["ForceParErrLkPWCtrRegOC96_3"] = _AF6CNC0022_RD_PLA._pla_force_par_err_control._ForceParErrLkPWCtrRegOC96_3()
            allFields["ForceParErrPldCtrRegOC96_3_OC48_1"] = _AF6CNC0022_RD_PLA._pla_force_par_err_control._ForceParErrPldCtrRegOC96_3_OC48_1()
            allFields["ForceParErrPldCtrRegOC96_3_OC48_0"] = _AF6CNC0022_RD_PLA._pla_force_par_err_control._ForceParErrPldCtrRegOC96_3_OC48_0()
            allFields["ForceParErrLkPWCtrRegOC96_2"] = _AF6CNC0022_RD_PLA._pla_force_par_err_control._ForceParErrLkPWCtrRegOC96_2()
            allFields["ForceParErrPldCtrRegOC96_2_OC48_1"] = _AF6CNC0022_RD_PLA._pla_force_par_err_control._ForceParErrPldCtrRegOC96_2_OC48_1()
            allFields["ForceParErrPldCtrRegOC96_2_OC48_0"] = _AF6CNC0022_RD_PLA._pla_force_par_err_control._ForceParErrPldCtrRegOC96_2_OC48_0()
            allFields["ForceParErrLkPWCtrRegOC96_1"] = _AF6CNC0022_RD_PLA._pla_force_par_err_control._ForceParErrLkPWCtrRegOC96_1()
            allFields["ForceParErrPldCtrRegOC96_1_OC48_1"] = _AF6CNC0022_RD_PLA._pla_force_par_err_control._ForceParErrPldCtrRegOC96_1_OC48_1()
            allFields["ForceParErrPldCtrRegOC96_1_OC48_0"] = _AF6CNC0022_RD_PLA._pla_force_par_err_control._ForceParErrPldCtrRegOC96_1_OC48_0()
            allFields["ForceParErrLkPWCtrRegOC96_0"] = _AF6CNC0022_RD_PLA._pla_force_par_err_control._ForceParErrLkPWCtrRegOC96_0()
            allFields["ForceParErrPldCtrRegOC96_0_OC48_1"] = _AF6CNC0022_RD_PLA._pla_force_par_err_control._ForceParErrPldCtrRegOC96_0_OC48_1()
            allFields["ForceParErrPldCtrRegOC96_0_OC48_0"] = _AF6CNC0022_RD_PLA._pla_force_par_err_control._ForceParErrPldCtrRegOC96_0_OC48_0()
            return allFields

    class _pla_dis_par_control(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Disable Parity Control"
    
        def description(self):
            return "This register is used to disable parity check"
            
        def width(self):
            return 21
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00042041
            
        def endAddress(self):
            return 0xffffffff

        class _DisParErrOutUPSRCtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "DisParErrOutUPSRCtrReg"
            
            def description(self):
                return "Dis Parity Error Ouput UPSR Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrPWProCtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "DisParErrPWProCtrReg"
            
            def description(self):
                return "Dis Parity Error PW Protection Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrPWPSNCtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "DisParErrPWPSNCtrReg"
            
            def description(self):
                return "Dis Parity Error PW PSNr Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrPWHdrCtrReg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "DisParErrPWHdrCtrReg"
            
            def description(self):
                return "Dis Parity Error PW Header Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrLkPWCtrRegOC96_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "DisParErrLkPWCtrRegOC96_3"
            
            def description(self):
                return "Dis Parity Error Lookup PW  Control Register OC96#3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrPldCtrRegOC96_3_OC48_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "DisParErrPldCtrRegOC96_3_OC48_1"
            
            def description(self):
                return "Dis Parity Error Payload Control Register OC48#1 of OC96#3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrPldCtrRegOC96_3_OC48_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DisParErrPldCtrRegOC96_3_OC48_0"
            
            def description(self):
                return "Dis Parity Error Payload Control Register OC48#0 of OC96#3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrLkPWCtrRegOC96_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DisParErrLkPWCtrRegOC96_2"
            
            def description(self):
                return "Dis Parity Error Lookup PW  Control Register OC96#2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrPldCtrRegOC96_2_OC48_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DisParErrPldCtrRegOC96_2_OC48_1"
            
            def description(self):
                return "Dis Parity Error Payload Control Register OC48#1 of OC96#2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrPldCtrRegOC96_2_OC48_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DisParErrPldCtrRegOC96_2_OC48_0"
            
            def description(self):
                return "Dis Parity Error Payload Control Register OC48#0 of OC96#2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrLkPWCtrRegOC96_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DisParErrLkPWCtrRegOC96_1"
            
            def description(self):
                return "Dis Parity Error Lookup PW  Control Register OC96#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrPldCtrRegOC96_1_OC48_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DisParErrPldCtrRegOC96_1_OC48_1"
            
            def description(self):
                return "Dis Parity Error Payload Control Register OC48#1 of OC96#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrPldCtrRegOC96_1_OC48_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DisParErrPldCtrRegOC96_1_OC48_0"
            
            def description(self):
                return "Dis Parity Error Payload Control Register OC48#0 of OC96#1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrLkPWCtrRegOC96_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DisParErrLkPWCtrRegOC96_0"
            
            def description(self):
                return "Dis Parity Error Lookup PW  Control Register OC96#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrPldCtrRegOC96_0_OC48_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DisParErrPldCtrRegOC96_0_OC48_1"
            
            def description(self):
                return "Dis Parity Error Payload Control Register OC48#1 of OC96#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DisParErrPldCtrRegOC96_0_OC48_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DisParErrPldCtrRegOC96_0_OC48_0"
            
            def description(self):
                return "Dis Parity Error Payload Control Register OC48#0 of OC96#0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DisParErrOutUPSRCtrReg"] = _AF6CNC0022_RD_PLA._pla_dis_par_control._DisParErrOutUPSRCtrReg()
            allFields["DisParErrPWProCtrReg"] = _AF6CNC0022_RD_PLA._pla_dis_par_control._DisParErrPWProCtrReg()
            allFields["DisParErrPWPSNCtrReg"] = _AF6CNC0022_RD_PLA._pla_dis_par_control._DisParErrPWPSNCtrReg()
            allFields["DisParErrPWHdrCtrReg"] = _AF6CNC0022_RD_PLA._pla_dis_par_control._DisParErrPWHdrCtrReg()
            allFields["DisParErrLkPWCtrRegOC96_3"] = _AF6CNC0022_RD_PLA._pla_dis_par_control._DisParErrLkPWCtrRegOC96_3()
            allFields["DisParErrPldCtrRegOC96_3_OC48_1"] = _AF6CNC0022_RD_PLA._pla_dis_par_control._DisParErrPldCtrRegOC96_3_OC48_1()
            allFields["DisParErrPldCtrRegOC96_3_OC48_0"] = _AF6CNC0022_RD_PLA._pla_dis_par_control._DisParErrPldCtrRegOC96_3_OC48_0()
            allFields["DisParErrLkPWCtrRegOC96_2"] = _AF6CNC0022_RD_PLA._pla_dis_par_control._DisParErrLkPWCtrRegOC96_2()
            allFields["DisParErrPldCtrRegOC96_2_OC48_1"] = _AF6CNC0022_RD_PLA._pla_dis_par_control._DisParErrPldCtrRegOC96_2_OC48_1()
            allFields["DisParErrPldCtrRegOC96_2_OC48_0"] = _AF6CNC0022_RD_PLA._pla_dis_par_control._DisParErrPldCtrRegOC96_2_OC48_0()
            allFields["DisParErrLkPWCtrRegOC96_1"] = _AF6CNC0022_RD_PLA._pla_dis_par_control._DisParErrLkPWCtrRegOC96_1()
            allFields["DisParErrPldCtrRegOC96_1_OC48_1"] = _AF6CNC0022_RD_PLA._pla_dis_par_control._DisParErrPldCtrRegOC96_1_OC48_1()
            allFields["DisParErrPldCtrRegOC96_1_OC48_0"] = _AF6CNC0022_RD_PLA._pla_dis_par_control._DisParErrPldCtrRegOC96_1_OC48_0()
            allFields["DisParErrLkPWCtrRegOC96_0"] = _AF6CNC0022_RD_PLA._pla_dis_par_control._DisParErrLkPWCtrRegOC96_0()
            allFields["DisParErrPldCtrRegOC96_0_OC48_1"] = _AF6CNC0022_RD_PLA._pla_dis_par_control._DisParErrPldCtrRegOC96_0_OC48_1()
            allFields["DisParErrPldCtrRegOC96_0_OC48_0"] = _AF6CNC0022_RD_PLA._pla_dis_par_control._DisParErrPldCtrRegOC96_0_OC48_0()
            return allFields

    class _pla_par_err_stk(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Parity Error Sticky"
    
        def description(self):
            return "This register is used to sticky parity check"
            
        def width(self):
            return 21
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00042042
            
        def endAddress(self):
            return 0xffffffff

        class _ParErrOutUPSRCtrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "ParErrOutUPSRCtrStk"
            
            def description(self):
                return "Dis Parity Error Ouput UPSR Control Register"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrPWProCtrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "ParErrPWProCtrStk"
            
            def description(self):
                return "Dis Parity Error PW Protection Control Register"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrPWPSNCtrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "ParErrPWPSNCtrStk"
            
            def description(self):
                return "Dis Parity Error PW PSNr Control Register"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrPWHdrCtrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ParErrPWHdrCtrStk"
            
            def description(self):
                return "Dis Parity Error PW Header Control Register"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrLkPWCtrRegOC96_3Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "ParErrLkPWCtrRegOC96_3Stk"
            
            def description(self):
                return "Dis Parity Error Lookup PW  Control Register OC96#3"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrPldCtrRegOC96_3_OC48_1Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "ParErrPldCtrRegOC96_3_OC48_1Stk"
            
            def description(self):
                return "Dis Parity Error Payload Control Register OC48#1 of OC96#3"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrPldCtrRegOC96_3_OC48_0Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "ParErrPldCtrRegOC96_3_OC48_0Stk"
            
            def description(self):
                return "Dis Parity Error Payload Control Register OC48#0 of OC96#3"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrLkPWCtrRegOC96_2Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "ParErrLkPWCtrRegOC96_2Stk"
            
            def description(self):
                return "Dis Parity Error Lookup PW  Control Register OC96#2"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrPldCtrRegOC96_2_OC48_1Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "ParErrPldCtrRegOC96_2_OC48_1Stk"
            
            def description(self):
                return "Dis Parity Error Payload Control Register OC48#1 of OC96#2"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrPldCtrRegOC96_2_OC48_0Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "ParErrPldCtrRegOC96_2_OC48_0Stk"
            
            def description(self):
                return "Dis Parity Error Payload Control Register OC48#0 of OC96#2"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrLkPWCtrRegOC96_1Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "ParErrLkPWCtrRegOC96_1Stk"
            
            def description(self):
                return "Dis Parity Error Lookup PW  Control Register OC96#1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrPldCtrRegOC96_1_OC48_1Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "ParErrPldCtrRegOC96_1_OC48_1Stk"
            
            def description(self):
                return "Dis Parity Error Payload Control Register OC48#1 of OC96#1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrPldCtrRegOC96_1_OC48_0Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "ParErrPldCtrRegOC96_1_OC48_0Stk"
            
            def description(self):
                return "Dis Parity Error Payload Control Register OC48#0 of OC96#1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrLkPWCtrRegOC96_0Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "ParErrLkPWCtrRegOC96_0Stk"
            
            def description(self):
                return "Dis Parity Error Lookup PW  Control Register OC96#0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrPldCtrRegOC96_0_OC48_1Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "ParErrPldCtrRegOC96_0_OC48_1Stk"
            
            def description(self):
                return "Dis Parity Error Payload Control Register OC48#1 of OC96#0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _ParErrPldCtrRegOC96_0_OC48_0Stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ParErrPldCtrRegOC96_0_OC48_0Stk"
            
            def description(self):
                return "Dis Parity Error Payload Control Register OC48#0 of OC96#0"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ParErrOutUPSRCtrStk"] = _AF6CNC0022_RD_PLA._pla_par_err_stk._ParErrOutUPSRCtrStk()
            allFields["ParErrPWProCtrStk"] = _AF6CNC0022_RD_PLA._pla_par_err_stk._ParErrPWProCtrStk()
            allFields["ParErrPWPSNCtrStk"] = _AF6CNC0022_RD_PLA._pla_par_err_stk._ParErrPWPSNCtrStk()
            allFields["ParErrPWHdrCtrStk"] = _AF6CNC0022_RD_PLA._pla_par_err_stk._ParErrPWHdrCtrStk()
            allFields["ParErrLkPWCtrRegOC96_3Stk"] = _AF6CNC0022_RD_PLA._pla_par_err_stk._ParErrLkPWCtrRegOC96_3Stk()
            allFields["ParErrPldCtrRegOC96_3_OC48_1Stk"] = _AF6CNC0022_RD_PLA._pla_par_err_stk._ParErrPldCtrRegOC96_3_OC48_1Stk()
            allFields["ParErrPldCtrRegOC96_3_OC48_0Stk"] = _AF6CNC0022_RD_PLA._pla_par_err_stk._ParErrPldCtrRegOC96_3_OC48_0Stk()
            allFields["ParErrLkPWCtrRegOC96_2Stk"] = _AF6CNC0022_RD_PLA._pla_par_err_stk._ParErrLkPWCtrRegOC96_2Stk()
            allFields["ParErrPldCtrRegOC96_2_OC48_1Stk"] = _AF6CNC0022_RD_PLA._pla_par_err_stk._ParErrPldCtrRegOC96_2_OC48_1Stk()
            allFields["ParErrPldCtrRegOC96_2_OC48_0Stk"] = _AF6CNC0022_RD_PLA._pla_par_err_stk._ParErrPldCtrRegOC96_2_OC48_0Stk()
            allFields["ParErrLkPWCtrRegOC96_1Stk"] = _AF6CNC0022_RD_PLA._pla_par_err_stk._ParErrLkPWCtrRegOC96_1Stk()
            allFields["ParErrPldCtrRegOC96_1_OC48_1Stk"] = _AF6CNC0022_RD_PLA._pla_par_err_stk._ParErrPldCtrRegOC96_1_OC48_1Stk()
            allFields["ParErrPldCtrRegOC96_1_OC48_0Stk"] = _AF6CNC0022_RD_PLA._pla_par_err_stk._ParErrPldCtrRegOC96_1_OC48_0Stk()
            allFields["ParErrLkPWCtrRegOC96_0Stk"] = _AF6CNC0022_RD_PLA._pla_par_err_stk._ParErrLkPWCtrRegOC96_0Stk()
            allFields["ParErrPldCtrRegOC96_0_OC48_1Stk"] = _AF6CNC0022_RD_PLA._pla_par_err_stk._ParErrPldCtrRegOC96_0_OC48_1Stk()
            allFields["ParErrPldCtrRegOC96_0_OC48_0Stk"] = _AF6CNC0022_RD_PLA._pla_par_err_stk._ParErrPldCtrRegOC96_0_OC48_0Stk()
            return allFields

    class _pla_force_crc_err_control(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Force CRC Error Control"
    
        def description(self):
            return "This register is used to force CRC error"
            
        def width(self):
            return 29
        
        def type(self):
            return "Config|Enable"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00042030
            
        def endAddress(self):
            return 0xffffffff

        class _PLACrcErrForeverCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "PLACrcErrForeverCfg"
            
            def description(self):
                return "Force crc error mode  1: forever when field PLACrcErrNumberCfg differ zero 0: burst"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PLACrcErrNumberCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PLACrcErrNumberCfg"
            
            def description(self):
                return "number of CRC error inserted to PLA DDR"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PLACrcErrForeverCfg"] = _AF6CNC0022_RD_PLA._pla_force_crc_err_control._PLACrcErrForeverCfg()
            allFields["PLACrcErrNumberCfg"] = _AF6CNC0022_RD_PLA._pla_force_crc_err_control._PLACrcErrNumberCfg()
            return allFields

    class _pla_crc_sticky(AtRegister.AtRegister):
        def name(self):
            return "PLA CRC Error Sticky"
    
        def description(self):
            return "This register report sticky of PLA CRC error."
            
        def width(self):
            return 1
        
        def type(self):
            return "Config|Enable"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00042031
            
        def endAddress(self):
            return 0xffffffff

        class _PLACrcError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PLACrcError"
            
            def description(self):
                return "PLA CRC error"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PLACrcError"] = _AF6CNC0022_RD_PLA._pla_crc_sticky._PLACrcError()
            return allFields

    class _PLA_crc_counter(AtRegister.AtRegister):
        def name(self):
            return "PLA CRC Error Counter"
    
        def description(self):
            return "This register counts PLA CRC error."
            
        def width(self):
            return 28
        
        def type(self):
            return "Config|Enable"
            
        def fomular(self):
            return "0x42032 + R2C"
            
        def startAddress(self):
            return 0x00042032
            
        def endAddress(self):
            return 0xffffffff

        class _PLACrcErrorCounter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PLACrcErrorCounter"
            
            def description(self):
                return "PLA Crc error counter"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PLACrcErrorCounter"] = _AF6CNC0022_RD_PLA._PLA_crc_counter._PLACrcErrorCounter()
            return allFields

    class _pla_192c_pld_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Low-Order Payload Control"
    
        def description(self):
            return "This register is used to configure payload in each Pseudowire channels per slice"
            
        def width(self):
            return 30
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x2_8000 + $Oc192Slice*16384"
            
        def startAddress(self):
            return 0x00028000
            
        def endAddress(self):
            return 0x0002c000

        class _Pla192PWIDCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Pla192PWIDCtrl"
            
            def description(self):
                return "lookup pwid"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Pla192PldCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Pla192PldCtrl"
            
            def description(self):
                return "Payload Size"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Pla192PWIDCtrl"] = _AF6CNC0022_RD_PLA._pla_192c_pld_ctrl._Pla192PWIDCtrl()
            allFields["Pla192PldCtrl"] = _AF6CNC0022_RD_PLA._pla_192c_pld_ctrl._Pla192PldCtrl()
            return allFields

    class _pla_oc192c_add_rmv_pw_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler OC192c Add/Remove Pseudowire Protocol Control"
    
        def description(self):
            return "This register is used to add/remove pseudowire to prevent burst packets to other packet processing engines in each OC-192 slice"
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x2_8001 + $Oc192Slice*16384"
            
        def startAddress(self):
            return 0x00028001
            
        def endAddress(self):
            return 0x0002c001

        class _PlaLoStaAddRmvCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaLoStaAddRmvCtrl"
            
            def description(self):
                return "protocol state to add/remove pw Step1: Add intitial or pw idle, the protocol state value is \"zero\" Step2: For adding the pw, CPU will Write \"1\" value for the protocol state to start adding pw. The protocol state value is \"1\". Step3: CPU enables pw at demap to finish the adding pw process. HW will automatically change the protocol state value to \"2\" to run the pw, and keep this state. Step4: For removing the pw, CPU will write \"3\" value for the protocol state to start removing pw. The protocol state value is \"3\". Step5: Poll the protocol state until return value \"0\" value, after that CPU disables pw at demap to finish the removing pw process"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaLoStaAddRmvCtrl"] = _AF6CNC0022_RD_PLA._pla_oc192c_add_rmv_pw_ctrl._PlaLoStaAddRmvCtrl()
            return allFields

    class _bert_montdm_config(AtRegister.AtRegister):
        def name(self):
            return "BERT GEN CONTROL"
    
        def description(self):
            return "This register config mode for bert"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4_4000 + $bid * 0x1000"
            
        def startAddress(self):
            return 0x00044000
            
        def endAddress(self):
            return 0x00045000

        class _BERT_inv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "BERT_inv"
            
            def description(self):
                return "Set 1 to enbale invert mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Thr_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 7
        
            def name(self):
                return "Thr_Err"
            
            def description(self):
                return "Thrhold declare lost syn sta"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Thr_Syn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Thr_Syn"
            
            def description(self):
                return "Thrhold declare syn sta"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _BertEna(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "BertEna"
            
            def description(self):
                return "Set 1 to enable BERT"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _BertMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "BertMode"
            
            def description(self):
                return "\"0\" prbs15, \"1\" prbs23, \"2\" prbs31,\"3\" prbs20, \"4\" prbs20r, \"5\" all1,\"6\" all0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["BERT_inv"] = _AF6CNC0022_RD_PLA._bert_montdm_config._BERT_inv()
            allFields["Thr_Err"] = _AF6CNC0022_RD_PLA._bert_montdm_config._Thr_Err()
            allFields["Thr_Syn"] = _AF6CNC0022_RD_PLA._bert_montdm_config._Thr_Syn()
            allFields["BertEna"] = _AF6CNC0022_RD_PLA._bert_montdm_config._BertEna()
            allFields["BertMode"] = _AF6CNC0022_RD_PLA._bert_montdm_config._BertMode()
            return allFields

    class _goodbit_pen_tdm_mon_r2c(AtRegister.AtRegister):
        def name(self):
            return "good counter tdm  r2c"
    
        def description(self):
            return "Counter"
            
        def width(self):
            return 34
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4_4001 + $bid * 0x1000"
            
        def startAddress(self):
            return 0x00044001
            
        def endAddress(self):
            return 0x00045001

        class _goodmonr2c(AtRegister.AtRegisterField):
            def stopBit(self):
                return 33
                
            def startBit(self):
                return 0
        
            def name(self):
                return "goodmonr2c"
            
            def description(self):
                return "mon goodbit mode read 2clear"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["goodmonr2c"] = _AF6CNC0022_RD_PLA._goodbit_pen_tdm_mon_r2c._goodmonr2c()
            return allFields

    class _goodbit_pen_tdm_mon_ro(AtRegister.AtRegister):
        def name(self):
            return "good counter tdm  ro"
    
        def description(self):
            return "Counter"
            
        def width(self):
            return 34
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4_4002 + $bid * 0x1000"
            
        def startAddress(self):
            return 0x00044002
            
        def endAddress(self):
            return 0x00045002

        class _goodmonro(AtRegister.AtRegisterField):
            def stopBit(self):
                return 33
                
            def startBit(self):
                return 0
        
            def name(self):
                return "goodmonro"
            
            def description(self):
                return "mon goodbit mode read only"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["goodmonro"] = _AF6CNC0022_RD_PLA._goodbit_pen_tdm_mon_ro._goodmonro()
            return allFields

    class _errorbit_pen_tdm_mon_r2c(AtRegister.AtRegister):
        def name(self):
            return "error counter tdm  r2c"
    
        def description(self):
            return "Counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4_4003 + $bid * 0x1000"
            
        def startAddress(self):
            return 0x00044003
            
        def endAddress(self):
            return 0x00045003

        class _errormonr2c(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "errormonr2c"
            
            def description(self):
                return "mon errorbit mode read 2clear"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["errormonr2c"] = _AF6CNC0022_RD_PLA._errorbit_pen_tdm_mon_r2c._errormonr2c()
            return allFields

    class _errorbit_pen_tdm_mon_ro(AtRegister.AtRegister):
        def name(self):
            return "error counter tdm  ro"
    
        def description(self):
            return "Counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4_4004 + $bid * 0x1000"
            
        def startAddress(self):
            return 0x00044004
            
        def endAddress(self):
            return 0x00045004

        class _errormonro(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "errormonro"
            
            def description(self):
                return "mon errorbit mode read only"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["errormonro"] = _AF6CNC0022_RD_PLA._errorbit_pen_tdm_mon_ro._errormonro()
            return allFields

    class _lostbit_pen_tdm_mon_r2c(AtRegister.AtRegister):
        def name(self):
            return "lost counter tdm  r2c"
    
        def description(self):
            return "Counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4_4005 + $bid * 0x1000"
            
        def startAddress(self):
            return 0x00044005
            
        def endAddress(self):
            return 0x00045005

        class _lostmonr2c(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lostmonr2c"
            
            def description(self):
                return "mon lostbit mode read 2clear"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lostmonr2c"] = _AF6CNC0022_RD_PLA._lostbit_pen_tdm_mon_r2c._lostmonr2c()
            return allFields

    class _lostbit_pen_tdm_mon_ro(AtRegister.AtRegister):
        def name(self):
            return "lost counter tdm  ro"
    
        def description(self):
            return "Counter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4_4006 + $bid * 0x1000"
            
        def startAddress(self):
            return 0x00044006
            
        def endAddress(self):
            return 0x00045006

        class _lostmonro(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lostmonro"
            
            def description(self):
                return "mon lostbit mode read only"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lostmonro"] = _AF6CNC0022_RD_PLA._lostbit_pen_tdm_mon_ro._lostmonro()
            return allFields

    class _lostbit_pen_tdm_mon_ro(AtRegister.AtRegister):
        def name(self):
            return "lost counter tdm  ro"
    
        def description(self):
            return "Status"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x4_4007 + $bid * 0x1000"
            
        def startAddress(self):
            return 0x00044007
            
        def endAddress(self):
            return 0x00045007

        class _staprbs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "staprbs"
            
            def description(self):
                return "\"3\" SYNC , other LOST"
            
            def type(self):
                return "R0"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["staprbs"] = _AF6CNC0022_RD_PLA._lostbit_pen_tdm_mon_ro._staprbs()
            return allFields

    class _lostbit_pen_tdm_mon_ro(AtRegister.AtRegister):
        def name(self):
            return "lost counter tdm  ro"
    
        def description(self):
            return "Sticky"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x4_4008 + $bid * 0x1000"
            
        def startAddress(self):
            return 0x00044008
            
        def endAddress(self):
            return 0x00045008

        class _stickyprbs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stickyprbs"
            
            def description(self):
                return "\"1\" LOSTSYN"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["stickyprbs"] = _AF6CNC0022_RD_PLA._lostbit_pen_tdm_mon_ro._stickyprbs()
            return allFields

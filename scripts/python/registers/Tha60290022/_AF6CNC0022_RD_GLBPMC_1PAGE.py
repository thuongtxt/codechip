import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0022_RD_GLBPMC_1PAGE(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_txpwcnt0"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_txpwcnt0()
        allRegisters["upen_txpwcnt1"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_txpwcnt1()
        allRegisters["upen_rxpwcnt0"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_rxpwcnt0()
        allRegisters["upen_rxpwcnt1"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_rxpwcnt1()
        allRegisters["upen_rxpwcnt2"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_rxpwcnt2()
        allRegisters["upen_rxpwcnt3"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_rxpwcnt3()
        allRegisters["ramcnthotdmlkupcfg"] = _AF6CNC0022_RD_GLBPMC_1PAGE._ramcnthotdmlkupcfg()
        allRegisters["upen_txpwcntbyte"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_txpwcntbyte()
        allRegisters["upen_rxpwcntbyte"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_rxpwcntbyte()
        allRegisters["upen_poh_pmr_cnt"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_poh_pmr_cnt()
        allRegisters["upen_sts_pohpm"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_sts_pohpm()
        allRegisters["upen_vt_pohpm"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_vt_pohpm()
        allRegisters["upen_poh_vt_cnt"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_poh_vt_cnt()
        allRegisters["upen_poh_sts_cnt0"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_poh_sts_cnt0()
        allRegisters["upen_poh_sts_cnt1"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_poh_sts_cnt1()
        allRegisters["upen_pdh_de3cnt"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_pdh_de3cnt()
        allRegisters["upen_pdh_de1cntval"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_pdh_de1cntval()
        return allRegisters

    class _upen_txpwcnt0(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Group0 Counter"
    
        def description(self):
            return "The register count Tx PW group0 of rate up to VC4"
            
        def width(self):
            return 28
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x2_0000 + $type*32768 + $rdmode*16384 + $pwid"
            
        def startAddress(self):
            return 0x00020000
            
        def endAddress(self):
            return 0x0002ffff

        class _txpwcnt0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txpwcnt0"
            
            def description(self):
                return "transmit PW group0 counter + type = 0: tx PW byte counter + type = 1: tx PW packet counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txpwcnt0"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_txpwcnt0._txpwcnt0()
            return allFields

    class _upen_txpwcnt1(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Group1 Counter"
    
        def description(self):
            return "The register count Tx PW group1 of rate up to VC4"
            
        def width(self):
            return 18
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_0000 + $type*32768 + $rdmode*16384 + $pwid"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x0001ffff

        class _txpwcnt1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txpwcnt1"
            
            def description(self):
                return "transmit PW group1 counter + type = 0: tx PW Rbit counter + type = 1: tx PW Pbit counter + type = 2: tx PW Nbit/Mbit counter + type = 3: tx PW Lbit counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txpwcnt1"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_txpwcnt1._txpwcnt1()
            return allFields

    class _upen_rxpwcnt0(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Group0 Counter"
    
        def description(self):
            return "The register count Rx PW group0 of rate up to VC4"
            
        def width(self):
            return 28
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3_0000 + $type*32768 + $rdmode*16384 + $pwid"
            
        def startAddress(self):
            return 0x00030000
            
        def endAddress(self):
            return 0x0003ffff

        class _rxpwcnt0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt0"
            
            def description(self):
                return "receive PW group0 counter + type = 0: rx PW byte counter + type = 1: rx PW packet counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt0"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_rxpwcnt0._rxpwcnt0()
            return allFields

    class _upen_rxpwcnt1(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Group1 Counter"
    
        def description(self):
            return "The register count Rx PW group1 of rate up to VC4"
            
        def width(self):
            return 18
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4_0000 + $type*32768 + $rdmode*16384 + $pwid"
            
        def startAddress(self):
            return 0x00040000
            
        def endAddress(self):
            return 0x0005ffff

        class _rxpwcnt1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt1"
            
            def description(self):
                return "receive PW group1 counter + type = 0: rx PW Rbit counter + type = 1: rx PW Pbit counter + type = 2: rx PW Nbit/Mbit counter + type = 3: rx PW Lbit counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt1"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_rxpwcnt1._rxpwcnt1()
            return allFields

    class _upen_rxpwcnt2(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Group2 Counter"
    
        def description(self):
            return "The register count Rx PW group2 of rate up to VC4"
            
        def width(self):
            return 18
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x6_0000 + $type*32768 + $rdmode*16384 + $pwid"
            
        def startAddress(self):
            return 0x00060000
            
        def endAddress(self):
            return 0x0007ffff

        class _rxpwcnt2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt2"
            
            def description(self):
                return "receive PW group2 counter + type = 0: rx PW Stray counter + type = 1: rx PW Malform counter + type = 2: rx PW Underrun counter + type = 3: rx PW Overrun counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt2"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_rxpwcnt2._rxpwcnt2()
            return allFields

    class _upen_rxpwcnt3(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Group3 Counter"
    
        def description(self):
            return "The register count Rx PW group2 of rate up to VC4"
            
        def width(self):
            return 18
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_0000 + $type*32768 + $rdmode*16384 + $pwid"
            
        def startAddress(self):
            return 0x00080000
            
        def endAddress(self):
            return 0x0009ffff

        class _rxpwcnt3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxpwcnt3"
            
            def description(self):
                return "receive PW group2 counter + type = 0: rx PW LOPS counter + type = 1: rx PW ReorderLost counter + type = 2: rx PW ReorderOk counter + type = 3: rx PW ReorderDrop counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxpwcnt3"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_rxpwcnt3._rxpwcnt3()
            return allFields

    class _ramcnthotdmlkupcfg(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Count High Order Look Up Control"
    
        def description(self):
            return "This register configure lookup from PWID to a pool high speed ID allocated by SW"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xF8_000 + $pwid"
            
        def startAddress(self):
            return 0x000f8000
            
        def endAddress(self):
            return 0x000fa9ff

        class _PwCntHighRateEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PwCntHighRateEn"
            
            def description(self):
                return "Set 1 to indicate the rate of PW is higher than VC4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _HighRateSwPwID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HighRateSwPwID"
            
            def description(self):
                return "Allocated high rate PW ID by solfware"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwCntHighRateEn"] = _AF6CNC0022_RD_GLBPMC_1PAGE._ramcnthotdmlkupcfg._PwCntHighRateEn()
            allFields["HighRateSwPwID"] = _AF6CNC0022_RD_GLBPMC_1PAGE._ramcnthotdmlkupcfg._HighRateSwPwID()
            return allFields

    class _upen_txpwcntbyte(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire High Rate Transmit Byte Counter"
    
        def description(self):
            return "The register count Tx PW byte of rate over VC4"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xF_5000 + $type*64 + $rdmode*32 + $hipwid"
            
        def startAddress(self):
            return 0x000f5000
            
        def endAddress(self):
            return 0x000f5fff

        class _hitxpwbytecnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 32
                
            def startBit(self):
                return 0
        
            def name(self):
                return "hitxpwbytecnt"
            
            def description(self):
                return "high rate transmit PW byte counter + type = 0 : high rate tx PW byte counter + type = 1 : high rate tx PW packet counter + type = 2 : high rate tx PW Rbit counter + type = 3 : high rate tx PW Lbit counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["hitxpwbytecnt"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_txpwcntbyte._hitxpwbytecnt()
            return allFields

    class _upen_rxpwcntbyte(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire High Rate Receive Byte Counter"
    
        def description(self):
            return "The register count Rx PW byte of rate over VC4"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xF_6000 + $type*64 + $rdmode*32 + $hipwid"
            
        def startAddress(self):
            return 0x000f6000
            
        def endAddress(self):
            return 0x000f6fff

        class _hirxpwbytecnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 32
                
            def startBit(self):
                return 0
        
            def name(self):
                return "hirxpwbytecnt"
            
            def description(self):
                return "high rate receive PW byte counter + type = 0 : high rate rx PW byte counter + type = 1 : high rate rx PW packet counter + type = 2 : high rate rx PW Rbit counter + type = 3 : high rate rx PW Lbit counter + type = 4 : high rate rx PW Stray counter + type = 5 : high rate rx PW Malform counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["hirxpwbytecnt"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_rxpwcntbyte._hirxpwbytecnt()
            return allFields

    class _upen_poh_pmr_cnt(AtRegister.AtRegister):
        def name(self):
            return "PMR Error Counter"
    
        def description(self):
            return "The register count information as below. Depending on type it 's the events specify."
            
        def width(self):
            return 20
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xF_3000 + 32*$type + $rdmode*16 + $lineid"
            
        def startAddress(self):
            return 0x000f3000
            
        def endAddress(self):
            return 0x000f3fff

        class _pmr_err_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pmr_err_cnt"
            
            def description(self):
                return "+ type = 0 : rei_l_block_err + type = 1 : rei_l + type = 2 : b2_block_err + type = 3 : b2 + type = 4 : b1_block_err + type = 5 : b1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pmr_err_cnt"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_poh_pmr_cnt._pmr_err_cnt()
            return allFields

    class _upen_sts_pohpm(AtRegister.AtRegister):
        def name(self):
            return "POH Path STS Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 18
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xF_4000 + 1024*$type + $rdmode*512 + 8*$stsid + $slcid"
            
        def startAddress(self):
            return 0x000f4000
            
        def endAddress(self):
            return 0x000f4fff

        class _pohpath_sts_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pohpath_sts_cnt"
            
            def description(self):
                return "pohpath_sts_cnt type = 0:  sts_rei type = 1:  sts_b3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pohpath_sts_cnt"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_sts_pohpm._pohpath_sts_cnt()
            return allFields

    class _upen_vt_pohpm(AtRegister.AtRegister):
        def name(self):
            return "POH Path VT Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 12
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xE_0000 + 32768*$type + $rdmode*16384 + 256*$stsid + 32*$vtgid + 8*vtid + slcid"
            
        def startAddress(self):
            return 0x000e0000
            
        def endAddress(self):
            return 0x000effff

        class _pohpath_vt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pohpath_vt_cnt"
            
            def description(self):
                return "pohpath_vt_cnt type = 0:  vt_rei type = 1:  vt_bip"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pohpath_vt_cnt"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_vt_pohpm._pohpath_vt_cnt()
            return allFields

    class _upen_poh_vt_cnt(AtRegister.AtRegister):
        def name(self):
            return "POH vt pointer Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 12
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xC_0000 + $type*32768 + $rdmode*16384 + 256*$stsid + 32*$vtgid + 8*vtid + slcid"
            
        def startAddress(self):
            return 0x000c0000
            
        def endAddress(self):
            return 0x000dffff

        class _poh_vt_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_vt_cnt"
            
            def description(self):
                return "poh vt pointer + type = 0 :  pohtx_vt_dec + type = 1 :  pohtx_vt_inc + type = 2 :  pohrx_vt_dec + type = 3 :  pohrx_vt_inc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_vt_cnt"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_poh_vt_cnt._poh_vt_cnt()
            return allFields

    class _upen_poh_sts_cnt0(AtRegister.AtRegister):
        def name(self):
            return "POH sts pointer Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xF_000 + 1024*$type + $rdmode*512 + 8*$stsid + $slcid"
            
        def startAddress(self):
            return 0x0000f000
            
        def endAddress(self):
            return 0x000f0fff

        class _poh_sts_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_sts_cnt"
            
            def description(self):
                return "poh sts pointer + type = 0 :  pohtx_sts_dec + type = 1 :  pohtx_sts_inc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_sts_cnt"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_poh_sts_cnt0._poh_sts_cnt()
            return allFields

    class _upen_poh_sts_cnt1(AtRegister.AtRegister):
        def name(self):
            return "POH sts pointer Counter"
    
        def description(self):
            return "The register count information as below. Depending on offset it 's the events specify."
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xF_1000 + 1024*$type + $rdmode*512 + 8*$stsid + $slcid"
            
        def startAddress(self):
            return 0x000f1000
            
        def endAddress(self):
            return 0x000f1fff

        class _poh_sts_cnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "poh_sts_cnt"
            
            def description(self):
                return "poh sts pointer + type = 0 :  pohrx_sts_dec + type = 1 :  pohrx_sts_inc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["poh_sts_cnt"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_poh_sts_cnt1._poh_sts_cnt()
            return allFields

    class _upen_pdh_de3cnt(AtRegister.AtRegister):
        def name(self):
            return "PDH ds3 cntval Counter"
    
        def description(self):
            return "The register count DS3"
            
        def width(self):
            return 18
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xF_2000 + $type*1024 + $rdmode*512 + 8*$stsid + $slcid"
            
        def startAddress(self):
            return 0x000f2000
            
        def endAddress(self):
            return 0x000f2fff

        class _ds3_cnt_val(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ds3_cnt_val"
            
            def description(self):
                return "ds3_cnt_val + type = 0 : FE + type = 1 : REI + type = 2 : PB + type = 3 : CB"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ds3_cnt_val"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_pdh_de3cnt._ds3_cnt_val()
            return allFields

    class _upen_pdh_de1cntval(AtRegister.AtRegister):
        def name(self):
            return "PDH ds1 cntval Counter"
    
        def description(self):
            return "The register count DS1"
            
        def width(self):
            return 14
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0xA_0000 + $type*32768 + $rdmode*16384 + 256*$stsid + 32*$vtgid + 8*vtid + slcid"
            
        def startAddress(self):
            return 0x000a0000
            
        def endAddress(self):
            return 0x000affff

        class _ds1_cnt_val(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ds1_cnt_val"
            
            def description(self):
                return "ds1_cnt_val + type = 0 : FE + type = 1 : CRC + type = 2 : REI"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ds1_cnt_val"] = _AF6CNC0022_RD_GLBPMC_1PAGE._upen_pdh_de1cntval._ds1_cnt_val()
            return allFields

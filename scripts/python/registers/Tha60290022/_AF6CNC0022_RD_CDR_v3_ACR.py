import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0022_RD_CDR_v3_ACR(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["cdr_acr_cpu_hold_ctrl"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_acr_cpu_hold_ctrl()
        allRegisters["cdr_acr_eng_timing_ctrl"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_acr_eng_timing_ctrl()
        allRegisters["cdr_adj_state_stat"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_adj_state_stat()
        allRegisters["cdr_adj_holdover_value_stat"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_adj_holdover_value_stat()
        allRegisters["dcr_tx_eng_active_ctrl"] = _AF6CNC0022_RD_CDR_v3_ACR._dcr_tx_eng_active_ctrl()
        allRegisters["dcr_prc_src_sel_cfg"] = _AF6CNC0022_RD_CDR_v3_ACR._dcr_prc_src_sel_cfg()
        allRegisters["dcr_prc_freq_cfg"] = _AF6CNC0022_RD_CDR_v3_ACR._dcr_prc_freq_cfg()
        allRegisters["dcr_rtp_freq_cfg"] = _AF6CNC0022_RD_CDR_v3_ACR._dcr_rtp_freq_cfg()
        allRegisters["RAM_ACR_Parity_Force_Control"] = _AF6CNC0022_RD_CDR_v3_ACR._RAM_ACR_Parity_Force_Control()
        allRegisters["RAM_ACR_Parity_Disable_Control"] = _AF6CNC0022_RD_CDR_v3_ACR._RAM_ACR_Parity_Disable_Control()
        allRegisters["RAM_ACR_Parity_Error_Sticky"] = _AF6CNC0022_RD_CDR_v3_ACR._RAM_ACR_Parity_Error_Sticky()
        allRegisters["rdha3_0_control"] = _AF6CNC0022_RD_CDR_v3_ACR._rdha3_0_control()
        allRegisters["rdha7_4_control"] = _AF6CNC0022_RD_CDR_v3_ACR._rdha7_4_control()
        allRegisters["rdha11_8_control"] = _AF6CNC0022_RD_CDR_v3_ACR._rdha11_8_control()
        allRegisters["rdha15_12_control"] = _AF6CNC0022_RD_CDR_v3_ACR._rdha15_12_control()
        allRegisters["rdha19_16_control"] = _AF6CNC0022_RD_CDR_v3_ACR._rdha19_16_control()
        allRegisters["rdha23_20_control"] = _AF6CNC0022_RD_CDR_v3_ACR._rdha23_20_control()
        allRegisters["rdha24data_control"] = _AF6CNC0022_RD_CDR_v3_ACR._rdha24data_control()
        allRegisters["rdha_hold63_32"] = _AF6CNC0022_RD_CDR_v3_ACR._rdha_hold63_32()
        allRegisters["rdindr_hold95_64"] = _AF6CNC0022_RD_CDR_v3_ACR._rdindr_hold95_64()
        allRegisters["rdindr_hold127_96"] = _AF6CNC0022_RD_CDR_v3_ACR._rdindr_hold127_96()
        allRegisters["cdr_per_chn_intr_en_ctrl"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_per_chn_intr_en_ctrl()
        allRegisters["cdr_per_chn_intr_stat"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_per_chn_intr_stat()
        allRegisters["cdr_per_chn_curr_stat"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_per_chn_curr_stat()
        allRegisters["cdr_per_chn_intr_or_stat"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_per_chn_intr_or_stat()
        allRegisters["cdr_per_stsvc_intr_or_stat"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_per_stsvc_intr_or_stat()
        allRegisters["cdr_per_stsvc_intr_en_ctrl"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_per_stsvc_intr_en_ctrl()
        allRegisters["CDR_per_grp_intr_or_stat"] = _AF6CNC0022_RD_CDR_v3_ACR._CDR_per_grp_intr_or_stat()
        return allRegisters

    class _cdr_acr_cpu_hold_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR ACR CPU  Reg Hold Control"
    
        def description(self):
            return "The register provides hold register for three word 32-bits MSB when CPU access to engine."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x070000 + HoldId"
            
        def startAddress(self):
            return 0x00070000
            
        def endAddress(self):
            return 0x00070002

        class _HoldReg0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HoldReg0"
            
            def description(self):
                return "Hold 32 bits"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HoldReg0"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_acr_cpu_hold_ctrl._HoldReg0()
            return allFields

    class _cdr_acr_eng_timing_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR ACR Engine Timing control"
    
        def description(self):
            return "This register is used to configure timing mode for per STS"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0000800 + OC48*32768 + STS*32 + TUG*4 + VT"
            
        def startAddress(self):
            return 0x00000800
            
        def endAddress(self):
            return 0x00000fff

        class _VC4_4c(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "VC4_4c"
            
            def description(self):
                return "VC4_4c/TU3 mode 0: STS1/VC4/DS3 1: TU3/VC4-4c/OC48 STS192c (VC4_64c = 4xOC48 => payload / 4, for support full payload size of STS192c STS192c (VC4_64c), STS48c (VC4_16c), STS24c (VC4_8c), STS12c (VC4_4c), thi phai config theo VC4_4c rate (bit26 =1) STS192c (VC4_64c = 16xVC4_4c => payload / 16, STS48c (VC4_16c = 4xVC4_4c => payload / 4), STS24c (VC4_8c = 2xVC4_4c => payload / 2), STS12c (VC4_4c, payload real) Sample: VC4_8c (STS24c) with paylaod zie = 783byte =  783x8 = 6264 bit => payload config to CDR = 6264/2 = 3132bit = 0xC3C"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PktLen_ind(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "PktLen_ind"
            
            def description(self):
                return "The payload packet  length for jumbo frame"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _HoldValMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "HoldValMode"
            
            def description(self):
                return "Hold value mode of NCO, default value 0 0: Hardware calculated and auto update 1: Software calculated and update, hardware is disabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SeqMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "SeqMode"
            
            def description(self):
                return "Sequence mode mode, default value 0 0: Wrap zero 1: Skip zero"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 20
        
            def name(self):
                return "LineType"
            
            def description(self):
                return "Line type mode 0: E1 1: DS1 2: VT2 3: VT15 4: E3 5: DS3/OC48 6: STS1/TU3 7: VC4/VC4-4c"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PktLen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PktLen"
            
            def description(self):
                return "The payload packet  length parameter to create a packet. SAToP mode: The number payload of bit. CESoPSN mode: The number of bit which converted to full DS1/E1 rate mode. In CESoPSN mode, the payload is assembled by NxDS0 with M frame, the value configured to this register is Mx256 bits for E1 mode, Mx193 bits for DS1 mode."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRTimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRTimeMode"
            
            def description(self):
                return "CDR time mode 0: System mode 1: Loop timing mode, transparency service Rx clock to service Tx clock 2: LIU timing mode, using service Rx clock for CDR source to generate service Tx clock 3: Prc timing mode, using Prc clock for CDR source to generate service Tx clock 4: Ext#1 timing mode, using Ext#1 clock for CDR source to generate service Tx clock 5: Ext#2 timing mode, using Ext#2 clock for CDR source to generate service Tx clock 6: Tx SONET timing mode, using Tx Line OCN clock for CDR source to generate service Tx clock 7: Free timing mode, using system clock for CDR source to generate service Tx clock 8: ACR timing mode 9: Reserve 10: DCR timing mode 11: Reserve 12: ACR Fast lock mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VC4_4c"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_acr_eng_timing_ctrl._VC4_4c()
            allFields["PktLen_ind"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_acr_eng_timing_ctrl._PktLen_ind()
            allFields["HoldValMode"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_acr_eng_timing_ctrl._HoldValMode()
            allFields["SeqMode"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_acr_eng_timing_ctrl._SeqMode()
            allFields["LineType"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_acr_eng_timing_ctrl._LineType()
            allFields["PktLen"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_acr_eng_timing_ctrl._PktLen()
            allFields["CDRTimeMode"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_acr_eng_timing_ctrl._CDRTimeMode()
            return allFields

    class _cdr_adj_state_stat(AtRegister.AtRegister):
        def name(self):
            return "CDR Adjust State status"
    
        def description(self):
            return "This register is used to store status or configure  some parameter of per CDR engine"
            
        def width(self):
            return 8
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0001000 + OC48*32768 + STS*32 + TUG*4 + VT"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0x000017ff

        class _Adjstate(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Adjstate"
            
            def description(self):
                return "Adjust state 0: Load state 1: Wait state 2: Init state 3: Learn State 4: ReLearn State 5: Lock State 6: ReInit State 7: Holdover State"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Adjstate"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_adj_state_stat._Adjstate()
            return allFields

    class _cdr_adj_holdover_value_stat(AtRegister.AtRegister):
        def name(self):
            return "CDR Adjust Holdover value status"
    
        def description(self):
            return "This register is used to store status or configure  some parameter of per CDR engine"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0001800 + OC48*32768 + STS*32 + TUG*4 + VT"
            
        def startAddress(self):
            return 0x00001800
            
        def endAddress(self):
            return 0x00001fff

        class _HoldVal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HoldVal"
            
            def description(self):
                return "NCO Holdover value parameter E1RATE   = 32'd3817748707;   Resolution =    0.262 parameter DS1RATE  = 32'd3837632815;   Resolution =    0.260 parameter VT2RATE  = 32'd4175662648;   Resolution =    0.239 parameter VT15RATE = 32'd4135894433;   Resolution =    0.241 parameter E3RATE   = 32'd2912117977;   Resolution =    0.343 parameter DS3RATE  = 32'd3790634015;   Resolution =    0.263 parameter TU3RATE  = 32'd4148547956;   Resolution =    0.239 parameter STS1RATE = 32'd4246160849;   Resolution =    0.239"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HoldVal"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_adj_holdover_value_stat._HoldVal()
            return allFields

    class _dcr_tx_eng_active_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DCR TX Engine Active Control"
    
        def description(self):
            return "This register is used to activate the DCR TX Engine. Active high."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00040000
            
        def endAddress(self):
            return 0xffffffff

        class _data(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "data"
            
            def description(self):
                return "DCR TX Engine Active Control"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["data"] = _AF6CNC0022_RD_CDR_v3_ACR._dcr_tx_eng_active_ctrl._data()
            return allFields

    class _dcr_prc_src_sel_cfg(AtRegister.AtRegister):
        def name(self):
            return "DCR PRC Source Select Configuration"
    
        def description(self):
            return "This register is used to configure to select PRC source for PRC timer. The PRC clock selected must be less than 19.44 Mhz."
            
        def width(self):
            return 8
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00040001
            
        def endAddress(self):
            return 0xffffffff

        class _DcrPrcSourceSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DcrPrcSourceSel"
            
            def description(self):
                return "PRC source selection. 0: PRC Reference Clock 1: System Clock 19Mhz 2: External Reference Clock 1. 3: External Reference Clock 2. 4: Ocn Line Clock Port 1 5: Ocn Line Clock Port 2 6: Ocn Line Clock Port 3 7: Ocn Line Clock Port 4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DcrPrcSourceSel"] = _AF6CNC0022_RD_CDR_v3_ACR._dcr_prc_src_sel_cfg._DcrPrcSourceSel()
            return allFields

    class _dcr_prc_freq_cfg(AtRegister.AtRegister):
        def name(self):
            return "DCR PRC Frequency Configuration"
    
        def description(self):
            return "This register is used to configure the frequency of PRC clock."
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0004000b
            
        def endAddress(self):
            return 0xffffffff

        class _DCRPrcFrequency(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DCRPrcFrequency"
            
            def description(self):
                return "Frequency of PRC clock. This is used to configure the frequency of PRC clock in Khz. Unit is Khz. Exp: The PRC clock is 19.44Mhz. This register will be configured to 19440."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DCRPrcFrequency"] = _AF6CNC0022_RD_CDR_v3_ACR._dcr_prc_freq_cfg._DCRPrcFrequency()
            return allFields

    class _dcr_rtp_freq_cfg(AtRegister.AtRegister):
        def name(self):
            return "DCR RTP Frequency Configuration"
    
        def description(self):
            return "This register is used to configure the frequency of PRC clock."
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0004000c
            
        def endAddress(self):
            return 0xffffffff

        class _DCRRTPFreq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DCRRTPFreq"
            
            def description(self):
                return "Frequency of RTP clock. This is used to configure the frequency of RTP clock in Khz. Unit is Khz. Exp: The RTP clock is 19.44Mhz. This register will be configured to 19440."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DCRRTPFreq"] = _AF6CNC0022_RD_CDR_v3_ACR._dcr_rtp_freq_cfg._DCRRTPFreq()
            return allFields

    class _RAM_ACR_Parity_Force_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM ACR Parity Force Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000c
            
        def endAddress(self):
            return 0xffffffff

        class _CDRACRDDR_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "CDRACRDDR_ParErrFrc"
            
            def description(self):
                return "Force parity For DDR of CDR"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRACRTimingCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"CDR ACR Engine Timing control\", each bit for per OC48"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRACRDDR_ParErrFrc"] = _AF6CNC0022_RD_CDR_v3_ACR._RAM_ACR_Parity_Force_Control._CDRACRDDR_ParErrFrc()
            allFields["CDRACRTimingCtrl_ParErrFrc"] = _AF6CNC0022_RD_CDR_v3_ACR._RAM_ACR_Parity_Force_Control._CDRACRTimingCtrl_ParErrFrc()
            return allFields

    class _RAM_ACR_Parity_Disable_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM ACR Parity Disable Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000d
            
        def endAddress(self):
            return 0xffffffff

        class _CDRACRDDR_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "CDRACRDDR_ParErrDis"
            
            def description(self):
                return "Disable parity For DDR of CDR"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRACRTimingCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"CDR ACR Engine Timing control\" , each bit for per OC48"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRACRDDR_ParErrDis"] = _AF6CNC0022_RD_CDR_v3_ACR._RAM_ACR_Parity_Disable_Control._CDRACRDDR_ParErrDis()
            allFields["CDRACRTimingCtrl_ParErrDis"] = _AF6CNC0022_RD_CDR_v3_ACR._RAM_ACR_Parity_Disable_Control._CDRACRTimingCtrl_ParErrDis()
            return allFields

    class _RAM_ACR_Parity_Error_Sticky(AtRegister.AtRegister):
        def name(self):
            return "RAM ACR parity Error Sticky"
    
        def description(self):
            return "This register configures disable parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000e
            
        def endAddress(self):
            return 0xffffffff

        class _CDRACRDDR_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "CDRACRDDR_ParErrStk"
            
            def description(self):
                return "Error parity For DDR of CDR"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRACRTimingCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"CDR ACR Engine Timing control\"  , each bit for per OC48"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRACRDDR_ParErrStk"] = _AF6CNC0022_RD_CDR_v3_ACR._RAM_ACR_Parity_Error_Sticky._CDRACRDDR_ParErrStk()
            allFields["CDRACRTimingCtrl_ParErrStk"] = _AF6CNC0022_RD_CDR_v3_ACR._RAM_ACR_Parity_Error_Sticky._CDRACRTimingCtrl_ParErrStk()
            return allFields

    class _rdha3_0_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit3_0 Control"
    
        def description(self):
            return "This register is used to send HA read address bit3_0 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x70200 + HaAddr3_0"
            
        def startAddress(self):
            return 0x00070200
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr3_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr3_0"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr3_0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr3_0"] = _AF6CNC0022_RD_CDR_v3_ACR._rdha3_0_control._ReadAddr3_0()
            return allFields

    class _rdha7_4_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit7_4 Control"
    
        def description(self):
            return "This register is used to send HA read address bit7_4 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x70210 + HaAddr7_4"
            
        def startAddress(self):
            return 0x00070210
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr7_4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr7_4"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr7_4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr7_4"] = _AF6CNC0022_RD_CDR_v3_ACR._rdha7_4_control._ReadAddr7_4()
            return allFields

    class _rdha11_8_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit11_8 Control"
    
        def description(self):
            return "This register is used to send HA read address bit11_8 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x70220 + HaAddr11_8"
            
        def startAddress(self):
            return 0x00070220
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr11_8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr11_8"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr11_8"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr11_8"] = _AF6CNC0022_RD_CDR_v3_ACR._rdha11_8_control._ReadAddr11_8()
            return allFields

    class _rdha15_12_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit15_12 Control"
    
        def description(self):
            return "This register is used to send HA read address bit15_12 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x70230 + HaAddr15_12"
            
        def startAddress(self):
            return 0x00070230
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr15_12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr15_12"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr15_12"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr15_12"] = _AF6CNC0022_RD_CDR_v3_ACR._rdha15_12_control._ReadAddr15_12()
            return allFields

    class _rdha19_16_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit19_16 Control"
    
        def description(self):
            return "This register is used to send HA read address bit19_16 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x70240 + HaAddr19_16"
            
        def startAddress(self):
            return 0x00070240
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr19_16(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr19_16"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr19_16"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr19_16"] = _AF6CNC0022_RD_CDR_v3_ACR._rdha19_16_control._ReadAddr19_16()
            return allFields

    class _rdha23_20_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit23_20 Control"
    
        def description(self):
            return "This register is used to send HA read address bit23_20 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x70250 + HaAddr23_20"
            
        def startAddress(self):
            return 0x00070250
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr23_20(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr23_20"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr23_20"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr23_20"] = _AF6CNC0022_RD_CDR_v3_ACR._rdha23_20_control._ReadAddr23_20()
            return allFields

    class _rdha24data_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit24 and Data Control"
    
        def description(self):
            return "This register is used to send HA read address bit24 to HA engine to read data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x70260 + HaAddr24"
            
        def startAddress(self):
            return 0x00070260
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData31_0"
            
            def description(self):
                return "HA read data bit31_0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData31_0"] = _AF6CNC0022_RD_CDR_v3_ACR._rdha24data_control._ReadHaData31_0()
            return allFields

    class _rdha_hold63_32(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data63_32"
    
        def description(self):
            return "This register is used to read HA dword2 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00070270
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData63_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData63_32"
            
            def description(self):
                return "HA read data bit63_32"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData63_32"] = _AF6CNC0022_RD_CDR_v3_ACR._rdha_hold63_32._ReadHaData63_32()
            return allFields

    class _rdindr_hold95_64(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data95_64"
    
        def description(self):
            return "This register is used to read HA dword3 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00070271
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData95_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData95_64"
            
            def description(self):
                return "HA read data bit95_64"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData95_64"] = _AF6CNC0022_RD_CDR_v3_ACR._rdindr_hold95_64._ReadHaData95_64()
            return allFields

    class _rdindr_hold127_96(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data127_96"
    
        def description(self):
            return "This register is used to read HA dword4 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00070272
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData127_96(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData127_96"
            
            def description(self):
                return "HA read data bit127_96"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData127_96"] = _AF6CNC0022_RD_CDR_v3_ACR._rdindr_hold127_96._ReadHaData127_96()
            return allFields

    class _cdr_per_chn_intr_en_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR per Channel Interrupt Enable Control"
    
        def description(self):
            return "This is the per Channel interrupt enable of CDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0006000 +  StsID*32 + VtnID"
            
        def startAddress(self):
            return 0x00006000
            
        def endAddress(self):
            return 0x000067ff

        class _CDRUnlokcedIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRUnlokcedIntrEn"
            
            def description(self):
                return "Set 1 to enable change UnLocked te event to generate an interrupt. Each bit for per OC48"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRUnlokcedIntrEn"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_per_chn_intr_en_ctrl._CDRUnlokcedIntrEn()
            return allFields

    class _cdr_per_chn_intr_stat(AtRegister.AtRegister):
        def name(self):
            return "CDR per Channel Interrupt Status"
    
        def description(self):
            return "This is the per Channel interrupt tus of CDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0006800 +  StsID*32 + VtnID"
            
        def startAddress(self):
            return 0x00006800
            
        def endAddress(self):
            return 0x00006fff

        class _CDRUnLockedIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRUnLockedIntr"
            
            def description(self):
                return "Set 1 if there is a change in UnLocked the event. Each bit for per OC48"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRUnLockedIntr"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_per_chn_intr_stat._CDRUnLockedIntr()
            return allFields

    class _cdr_per_chn_curr_stat(AtRegister.AtRegister):
        def name(self):
            return "CDR per Channel Current Status"
    
        def description(self):
            return "This is the per Channel Current tus of CDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0007000 +  StsID*32 + VtnID"
            
        def startAddress(self):
            return 0x00007000
            
        def endAddress(self):
            return 0x000077ff

        class _CDRUnLockedCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRUnLockedCurrSta"
            
            def description(self):
                return "Current tus of UnLocked event. Each bit for per OC48"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRUnLockedCurrSta"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_per_chn_curr_stat._CDRUnLockedCurrSta()
            return allFields

    class _cdr_per_chn_intr_or_stat(AtRegister.AtRegister):
        def name(self):
            return "CDR per Channel Interrupt OR Status"
    
        def description(self):
            return "The register consists of 32 bits for 32 VT/TUs of the related STS/VC in the CDR. Each bit is used to store Interrupt OR tus of the related DS1/E1."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x0007800 +  GrpID*1024 + STSID"
            
        def startAddress(self):
            return 0x00007800
            
        def endAddress(self):
            return 0x0000782f

        class _CDRVtIntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRVtIntrOrSta"
            
            def description(self):
                return "Set to 1 if any interrupt status bit of corresponding DS1/E1 is set and its interrupt is enabled."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRVtIntrOrSta"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_per_chn_intr_or_stat._CDRVtIntrOrSta()
            return allFields

    class _cdr_per_stsvc_intr_or_stat(AtRegister.AtRegister):
        def name(self):
            return "CDR per STS/VC Interrupt OR Status"
    
        def description(self):
            return "The register consists of 24 bits for 24 STS/VCs of the CDR. Each bit is used to store Interrupt OR tus of the related STS/VC."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x0007BFF +  GrpID*1024"
            
        def startAddress(self):
            return 0x00007bff
            
        def endAddress(self):
            return 0xffffffff

        class _CDRStsIntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRStsIntrOrSta"
            
            def description(self):
                return "Set to 1 if any interrupt status bit of corresponding STS/VC is set and its interrupt is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRStsIntrOrSta"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_per_stsvc_intr_or_stat._CDRStsIntrOrSta()
            return allFields

    class _cdr_per_stsvc_intr_en_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR per STS/VC Interrupt Enable Control"
    
        def description(self):
            return "The register consists of 24 interrupt enable bits for 24 STS/VCs in the Rx DS1/E1/J1 Framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0007BFE +  GrpID*1024"
            
        def startAddress(self):
            return 0x00007bfe
            
        def endAddress(self):
            return 0xffffffff

        class _CDRStsIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRStsIntrEn"
            
            def description(self):
                return "Set to 1 to enable the related STS/VC to generate interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRStsIntrEn"] = _AF6CNC0022_RD_CDR_v3_ACR._cdr_per_stsvc_intr_en_ctrl._CDRStsIntrEn()
            return allFields

    class _CDR_per_grp_intr_or_stat(AtRegister.AtRegister):
        def name(self):
            return "CDR per Group Interrupt OR Status"
    
        def description(self):
            return "The register consists of 2 bits for 2 Group of the CDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE1GrpIntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1GrpIntrOrSta"
            
            def description(self):
                return "Set to 1 if any interrupt tus bit of corresponding Group is set and its interrupt is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1GrpIntrOrSta"] = _AF6CNC0022_RD_CDR_v3_ACR._CDR_per_grp_intr_or_stat._RxDE1GrpIntrOrSta()
            return allFields

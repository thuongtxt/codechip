import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0022_RD_MAP_HO(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["demap_channel_ctrl"] = _AF6CNC0022_RD_MAP_HO._demap_channel_ctrl()
        allRegisters["map_line_ctrl"] = _AF6CNC0022_RD_MAP_HO._map_line_ctrl()
        allRegisters["map_loopback_ctrl"] = _AF6CNC0022_RD_MAP_HO._map_loopback_ctrl()
        allRegisters["sel_ho_bert_gen"] = _AF6CNC0022_RD_MAP_HO._sel_ho_bert_gen()
        allRegisters["ctrl_pen_gen"] = _AF6CNC0022_RD_MAP_HO._ctrl_pen_gen()
        allRegisters["ctrl_ber_pen"] = _AF6CNC0022_RD_MAP_HO._ctrl_ber_pen()
        allRegisters["goodbit_ber_pen"] = _AF6CNC0022_RD_MAP_HO._goodbit_ber_pen()
        allRegisters["sel_ho_bert_tdm_mon"] = _AF6CNC0022_RD_MAP_HO._sel_ho_bert_tdm_mon()
        allRegisters["sel_ho_bert_pw_mon"] = _AF6CNC0022_RD_MAP_HO._sel_ho_bert_pw_mon()
        allRegisters["ctrl_pen_tdm_mon"] = _AF6CNC0022_RD_MAP_HO._ctrl_pen_tdm_mon()
        allRegisters["ctrl_pen_pw_mon"] = _AF6CNC0022_RD_MAP_HO._ctrl_pen_pw_mon()
        allRegisters["loss_tdm_mon"] = _AF6CNC0022_RD_MAP_HO._loss_tdm_mon()
        allRegisters["loss_pw_mon"] = _AF6CNC0022_RD_MAP_HO._loss_pw_mon()
        allRegisters["goodbit_pen_tdm_mon"] = _AF6CNC0022_RD_MAP_HO._goodbit_pen_tdm_mon()
        allRegisters["err_pen_tdm_mon"] = _AF6CNC0022_RD_MAP_HO._err_pen_tdm_mon()
        allRegisters["lossbit_pen_tdm_mon"] = _AF6CNC0022_RD_MAP_HO._lossbit_pen_tdm_mon()
        allRegisters["goodbit_pen_pw_mon"] = _AF6CNC0022_RD_MAP_HO._goodbit_pen_pw_mon()
        allRegisters["errbit_pen_pw_mon"] = _AF6CNC0022_RD_MAP_HO._errbit_pen_pw_mon()
        allRegisters["lossbit_pen_pw_mon"] = _AF6CNC0022_RD_MAP_HO._lossbit_pen_pw_mon()
        allRegisters["RAM_Map_Parity_Force_Control"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Force_Control()
        allRegisters["RAM_Map_Parity_Disable_Control"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Disable_Control()
        allRegisters["RAM_Map_Parity_Error_Sticky"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Error_Sticky()
        allRegisters["RAM_DeMap_Parity_Force_Control"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Force_Control()
        allRegisters["RAM_DeMap_Parity_Disable_Control"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Disable_Control()
        allRegisters["RAM_DeMap_Parity_Error_Sticky"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Error_Sticky()
        return allRegisters

    class _demap_channel_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Demap Channel Control"
    
        def description(self):
            return "The registers are used by the hardware to configure PW channel"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0000 + 256*slice + stsid"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x000007ff

        class _DemapChType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DemapChType"
            
            def description(self):
                return "0:CEP,  1:HDLC/PPP/ML, 2:VCAT/LCAS"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Demapsr_vc3n3c(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Demapsr_vc3n3c"
            
            def description(self):
                return "0:slave of VC3_N3c  1:master of VC3-N3c"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Demapsrctype(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Demapsrctype"
            
            def description(self):
                return "0:VC3 1:VC3-3c"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapChEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DemapChEn"
            
            def description(self):
                return "PW/Channels Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DemapChType"] = _AF6CNC0022_RD_MAP_HO._demap_channel_ctrl._DemapChType()
            allFields["Demapsr_vc3n3c"] = _AF6CNC0022_RD_MAP_HO._demap_channel_ctrl._Demapsr_vc3n3c()
            allFields["Demapsrctype"] = _AF6CNC0022_RD_MAP_HO._demap_channel_ctrl._Demapsrctype()
            allFields["DemapChEn"] = _AF6CNC0022_RD_MAP_HO._demap_channel_ctrl._DemapChEn()
            return allFields

    class _map_line_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Map Line Control"
    
        def description(self):
            return "The registers provide the per line configurations for STS"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4000 + 256*slice + stsid"
            
        def startAddress(self):
            return 0x00004000
            
        def endAddress(self):
            return 0x000047ff

        class _MapChType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "MapChType"
            
            def description(self):
                return "0:CEP,  1:HDLC/PPP/ML, 2:VCAT/LCAS"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Mapsrc_vc3n3c(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "Mapsrc_vc3n3c"
            
            def description(self):
                return "0:slave of VC3_N3c  1:master of VC3-N3c"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Mapsrc_type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Mapsrc_type"
            
            def description(self):
                return "0:VC3 1:VC3-3c"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapChEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "MapChEn"
            
            def description(self):
                return "PW/Channels Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapTimeSrcMaster(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "MapTimeSrcMaster"
            
            def description(self):
                return "This bit is used to indicate the master timing or the master VC3 in VC4/VC4-Xc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapTimeSrcId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MapTimeSrcId"
            
            def description(self):
                return "The reference line ID used for timing reference or the master VC3 ID in VC4/VC4-Xc"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MapChType"] = _AF6CNC0022_RD_MAP_HO._map_line_ctrl._MapChType()
            allFields["Mapsrc_vc3n3c"] = _AF6CNC0022_RD_MAP_HO._map_line_ctrl._Mapsrc_vc3n3c()
            allFields["Mapsrc_type"] = _AF6CNC0022_RD_MAP_HO._map_line_ctrl._Mapsrc_type()
            allFields["MapChEn"] = _AF6CNC0022_RD_MAP_HO._map_line_ctrl._MapChEn()
            allFields["MapTimeSrcMaster"] = _AF6CNC0022_RD_MAP_HO._map_line_ctrl._MapTimeSrcMaster()
            allFields["MapTimeSrcId"] = _AF6CNC0022_RD_MAP_HO._map_line_ctrl._MapTimeSrcId()
            return allFields

    class _map_loopback_ctrl(AtRegister.AtRegister):
        def name(self):
            return "map_loopback_ctrl"
    
        def description(self):
            return "The registers select 1id in line to gen bert data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4_0FF + 256*slice"
            
        def startAddress(self):
            return 0x000040ff
            
        def endAddress(self):
            return 0xffffffff

        class _Map_loopout(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Map_loopout"
            
            def description(self):
                return "Map loop out full Slice 48"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _IDLECode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IDLECode"
            
            def description(self):
                return "IDLE Code"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Map_loopout"] = _AF6CNC0022_RD_MAP_HO._map_loopback_ctrl._Map_loopout()
            allFields["IDLECode"] = _AF6CNC0022_RD_MAP_HO._map_loopback_ctrl._IDLECode()
            return allFields

    class _sel_ho_bert_gen(AtRegister.AtRegister):
        def name(self):
            return "Sel Ho Bert Gen"
    
        def description(self):
            return "The registers select 1id in line to gen bert data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_200 + 2048*engid"
            
        def startAddress(self):
            return 0x00008200
            
        def endAddress(self):
            return 0x0000ba00

        class _gen_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "gen_en"
            
            def description(self):
                return "set \"1\" to enable bert gen"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _line_id(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 6
        
            def name(self):
                return "line_id"
            
            def description(self):
                return "line id OC48"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _stsid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stsid"
            
            def description(self):
                return "STS ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["gen_en"] = _AF6CNC0022_RD_MAP_HO._sel_ho_bert_gen._gen_en()
            allFields["line_id"] = _AF6CNC0022_RD_MAP_HO._sel_ho_bert_gen._line_id()
            allFields["stsid"] = _AF6CNC0022_RD_MAP_HO._sel_ho_bert_gen._stsid()
            return allFields

    class _ctrl_pen_gen(AtRegister.AtRegister):
        def name(self):
            return "Sel Mode Bert Gen"
    
        def description(self):
            return "The registers select mode bert gen"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_300 + 2048*slice"
            
        def startAddress(self):
            return 0x00008300
            
        def endAddress(self):
            return 0x0000bb00

        class _swapmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "swapmode"
            
            def description(self):
                return "swap data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _invmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "invmode"
            
            def description(self):
                return "invert data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _patt_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "patt_mode"
            
            def description(self):
                return "sel pattern gen # 0x01 : all0 # 0x02 : prbs15 # 0x04 : prbs20r # 0x08 : prbs20 # 0x10 : prbs23 # 0x20 : prbs31 # other: all1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["swapmode"] = _AF6CNC0022_RD_MAP_HO._ctrl_pen_gen._swapmode()
            allFields["invmode"] = _AF6CNC0022_RD_MAP_HO._ctrl_pen_gen._invmode()
            allFields["patt_mode"] = _AF6CNC0022_RD_MAP_HO._ctrl_pen_gen._patt_mode()
            return allFields

    class _ctrl_ber_pen(AtRegister.AtRegister):
        def name(self):
            return "Inser Error"
    
        def description(self):
            return "The registers select rate inser error"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_320 + 2048*slice"
            
        def startAddress(self):
            return 0x00008320
            
        def endAddress(self):
            return 0x0000bb20

        class _ber_rate(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ber_rate"
            
            def description(self):
                return "TxBerMd [31:0] == BER_level_val  : Bit Error Rate inserted to Pattern Generator [31:0] == 32'd0          :disable  BER_level_val	BER_level 1000:        	BER 10e-3 10_000:      	BER 10e-4 100_000:     	BER 10e-5 1_000_000:   	BER 10e-6 10_000_000:  	BER 10e-7 100_000_000: 	BER 10e-8 1_000_000_000 : BER 10e-9"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ber_rate"] = _AF6CNC0022_RD_MAP_HO._ctrl_ber_pen._ber_rate()
            return allFields

    class _goodbit_ber_pen(AtRegister.AtRegister):
        def name(self):
            return "Counter num of bit gen"
    
        def description(self):
            return "The registers counter bit genertaie in tdm side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_380 + 2048*slice"
            
        def startAddress(self):
            return 0x00008380
            
        def endAddress(self):
            return 0x0000bb80

        class _goodbit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "goodbit"
            
            def description(self):
                return "counter goodbit"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["goodbit"] = _AF6CNC0022_RD_MAP_HO._goodbit_ber_pen._goodbit()
            return allFields

    class _sel_ho_bert_tdm_mon(AtRegister.AtRegister):
        def name(self):
            return "Sel Ho Bert TDM Mon"
    
        def description(self):
            return "The registers select 1id in line to gen bert data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_400 + 2048*slice"
            
        def startAddress(self):
            return 0x00008400
            
        def endAddress(self):
            return 0x0000bc00

        class _montdmen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "montdmen"
            
            def description(self):
                return "set \"1\" to enable bert tdm mon"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _tdmslide(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 10
        
            def name(self):
                return "tdmslide"
            
            def description(self):
                return "slide id"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _masterID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "masterID"
            
            def description(self):
                return "chanel ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["montdmen"] = _AF6CNC0022_RD_MAP_HO._sel_ho_bert_tdm_mon._montdmen()
            allFields["tdmslide"] = _AF6CNC0022_RD_MAP_HO._sel_ho_bert_tdm_mon._tdmslide()
            allFields["masterID"] = _AF6CNC0022_RD_MAP_HO._sel_ho_bert_tdm_mon._masterID()
            return allFields

    class _sel_ho_bert_pw_mon(AtRegister.AtRegister):
        def name(self):
            return "Sel Ho Bert PW Mon"
    
        def description(self):
            return "The registers select 1id in line to gen bert data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_600 + 2048*slice"
            
        def startAddress(self):
            return 0x00008600
            
        def endAddress(self):
            return 0x0000be00

        class _monpwen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "monpwen"
            
            def description(self):
                return "set \"1\" to enable bert pw mon"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _monpwslide(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 10
        
            def name(self):
                return "monpwslide"
            
            def description(self):
                return "pw slide id"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _masterID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "masterID"
            
            def description(self):
                return "chanel ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["monpwen"] = _AF6CNC0022_RD_MAP_HO._sel_ho_bert_pw_mon._monpwen()
            allFields["monpwslide"] = _AF6CNC0022_RD_MAP_HO._sel_ho_bert_pw_mon._monpwslide()
            allFields["masterID"] = _AF6CNC0022_RD_MAP_HO._sel_ho_bert_pw_mon._masterID()
            return allFields

    class _ctrl_pen_tdm_mon(AtRegister.AtRegister):
        def name(self):
            return "Sel Mode Bert TDM mon"
    
        def description(self):
            return "The registers select mode bert mon in tdm side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_510 + 2048*slice"
            
        def startAddress(self):
            return 0x00008510
            
        def endAddress(self):
            return 0x0000bd10

        class _thrhold_pattern_sync(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "thrhold_pattern_sync"
            
            def description(self):
                return "minimum number of byte sync to go sync state"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _thrhold_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "thrhold_error"
            
            def description(self):
                return "maximum err in state sync, if more than thrhold go loss sync"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _reservee(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "reservee"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _swapmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "swapmode"
            
            def description(self):
                return "swap data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _invmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "invmode"
            
            def description(self):
                return "invert data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _patt_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "patt_mode"
            
            def description(self):
                return "sel pattern gen # 0x01 : all1 # 0x02 : all0 # 0x04 : prbs15 # 0x08 : prbs20r # 0x10 : prbs20 # 0x20 : prss23 # 0x40 : prbs31"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["thrhold_pattern_sync"] = _AF6CNC0022_RD_MAP_HO._ctrl_pen_tdm_mon._thrhold_pattern_sync()
            allFields["thrhold_error"] = _AF6CNC0022_RD_MAP_HO._ctrl_pen_tdm_mon._thrhold_error()
            allFields["reservee"] = _AF6CNC0022_RD_MAP_HO._ctrl_pen_tdm_mon._reservee()
            allFields["swapmode"] = _AF6CNC0022_RD_MAP_HO._ctrl_pen_tdm_mon._swapmode()
            allFields["invmode"] = _AF6CNC0022_RD_MAP_HO._ctrl_pen_tdm_mon._invmode()
            allFields["patt_mode"] = _AF6CNC0022_RD_MAP_HO._ctrl_pen_tdm_mon._patt_mode()
            return allFields

    class _ctrl_pen_pw_mon(AtRegister.AtRegister):
        def name(self):
            return "Sel Mode Bert PW mon"
    
        def description(self):
            return "The registers select mode bert mon in pw side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_710 + 2048*slice"
            
        def startAddress(self):
            return 0x00008710
            
        def endAddress(self):
            return 0x0000bf10

        class _thrhold_pattern_sync(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "thrhold_pattern_sync"
            
            def description(self):
                return "minimum number of byte sync to go sync state"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _thrhold_error(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "thrhold_error"
            
            def description(self):
                return "maximum err in state sync, if more than thrhold go loss sync"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _pw_reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 10
        
            def name(self):
                return "pw_reserve"
            
            def description(self):
                return "pw reserve"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _swapmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "swapmode"
            
            def description(self):
                return "swap data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _invmode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "invmode"
            
            def description(self):
                return "invert data"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _patt_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "patt_mode"
            
            def description(self):
                return "sel pattern gen # 0x01 : all1 # 0x02 : all0 # 0x04 : prbs15 # 0x08 : prbs20r # 0x10 : prbs20 # 0x20 : prss23 # 0x40 : prbs31"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["thrhold_pattern_sync"] = _AF6CNC0022_RD_MAP_HO._ctrl_pen_pw_mon._thrhold_pattern_sync()
            allFields["thrhold_error"] = _AF6CNC0022_RD_MAP_HO._ctrl_pen_pw_mon._thrhold_error()
            allFields["pw_reserve"] = _AF6CNC0022_RD_MAP_HO._ctrl_pen_pw_mon._pw_reserve()
            allFields["swapmode"] = _AF6CNC0022_RD_MAP_HO._ctrl_pen_pw_mon._swapmode()
            allFields["invmode"] = _AF6CNC0022_RD_MAP_HO._ctrl_pen_pw_mon._invmode()
            allFields["patt_mode"] = _AF6CNC0022_RD_MAP_HO._ctrl_pen_pw_mon._patt_mode()
            return allFields

    class _loss_tdm_mon(AtRegister.AtRegister):
        def name(self):
            return "TDM loss sync"
    
        def description(self):
            return "The registers indicate bert mon loss sync in tdm side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_502 + 2048*slice"
            
        def startAddress(self):
            return 0x00008502
            
        def endAddress(self):
            return 0x0000bd02

        class _sticky_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "sticky_err"
            
            def description(self):
                return "\"1\" indicate loss sync"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["sticky_err"] = _AF6CNC0022_RD_MAP_HO._loss_tdm_mon._sticky_err()
            return allFields

    class _loss_pw_mon(AtRegister.AtRegister):
        def name(self):
            return "PW loss sync"
    
        def description(self):
            return "The registers indicate bert mon loss sync in pw side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_702 + 2048*slice"
            
        def startAddress(self):
            return 0x00008702
            
        def endAddress(self):
            return 0x0000bf02

        class _sticky_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "sticky_err"
            
            def description(self):
                return "\"1\" indicate loss sync"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["sticky_err"] = _AF6CNC0022_RD_MAP_HO._loss_pw_mon._sticky_err()
            return allFields

    class _goodbit_pen_tdm_mon(AtRegister.AtRegister):
        def name(self):
            return "counter good bit TDM mon"
    
        def description(self):
            return "The registers count goodbit in  mon tdm side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_580 + 2048*slice"
            
        def startAddress(self):
            return 0x00008580
            
        def endAddress(self):
            return 0x0000bd80

        class _cnt_goodbit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_goodbit"
            
            def description(self):
                return "counter goodbit"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_goodbit"] = _AF6CNC0022_RD_MAP_HO._goodbit_pen_tdm_mon._cnt_goodbit()
            return allFields

    class _err_pen_tdm_mon(AtRegister.AtRegister):
        def name(self):
            return "counter error bit in TDM mon"
    
        def description(self):
            return "The registers count goodbit in  mon tdm side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_560 + 2048*slice"
            
        def startAddress(self):
            return 0x00008560
            
        def endAddress(self):
            return 0x0000bd60

        class _cnt_errbit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_errbit"
            
            def description(self):
                return "counter err bit"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_errbit"] = _AF6CNC0022_RD_MAP_HO._err_pen_tdm_mon._cnt_errbit()
            return allFields

    class _lossbit_pen_tdm_mon(AtRegister.AtRegister):
        def name(self):
            return "Counter loss bit in TDM mon"
    
        def description(self):
            return "The registers count goodbit in  mon tdm side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_5A0 + 2048*slice"
            
        def startAddress(self):
            return 0x000085a0
            
        def endAddress(self):
            return 0x0000bda0

        class _cnt_errbit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_errbit"
            
            def description(self):
                return "counter err bit"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_errbit"] = _AF6CNC0022_RD_MAP_HO._lossbit_pen_tdm_mon._cnt_errbit()
            return allFields

    class _goodbit_pen_pw_mon(AtRegister.AtRegister):
        def name(self):
            return "counter good bit TDM mon"
    
        def description(self):
            return "The registers count goodbit in  mon tdm side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_780 + 2048*slice"
            
        def startAddress(self):
            return 0x00008780
            
        def endAddress(self):
            return 0x0000bf80

        class _cnt_pwgoodbit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_pwgoodbit"
            
            def description(self):
                return "counter goodbit"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_pwgoodbit"] = _AF6CNC0022_RD_MAP_HO._goodbit_pen_pw_mon._cnt_pwgoodbit()
            return allFields

    class _errbit_pen_pw_mon(AtRegister.AtRegister):
        def name(self):
            return "counter error bit in TDM mon"
    
        def description(self):
            return "The registers count goodbit in  mon tdm side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_760 + 2048*slice"
            
        def startAddress(self):
            return 0x00008760
            
        def endAddress(self):
            return 0x0000bf60

        class _cnt_pwerrbit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_pwerrbit"
            
            def description(self):
                return "counter err bit"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_pwerrbit"] = _AF6CNC0022_RD_MAP_HO._errbit_pen_pw_mon._cnt_pwerrbit()
            return allFields

    class _lossbit_pen_pw_mon(AtRegister.AtRegister):
        def name(self):
            return "Counter loss bit in TDM mon"
    
        def description(self):
            return "The registers count goodbit in  mon tdm side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x8_7A0 + 2048*slice"
            
        def startAddress(self):
            return 0x000087a0
            
        def endAddress(self):
            return 0x0000bfa0

        class _cnt_pwlossbi(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_pwlossbi"
            
            def description(self):
                return "counter loss bit"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_pwlossbi"] = _AF6CNC0022_RD_MAP_HO._lossbit_pen_pw_mon._cnt_pwlossbi()
            return allFields

    class _RAM_Map_Parity_Force_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM Map Parity Force Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004808
            
        def endAddress(self):
            return 0xffffffff

        class _MAPSlc7Ctrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "MAPSlc7Ctrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Thalassa Map Line Control Slice7\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc6Ctrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "MAPSlc6Ctrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Thalassa Map Line Control Slice6\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc5Ctrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "MAPSlc5Ctrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Thalassa Map Line Control Slice5\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc4Ctrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "MAPSlc4Ctrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Thalassa Map Line Control Slice4\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc3Ctrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "MAPSlc3Ctrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Thalassa Map Line Control Slice3\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc2Ctrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "MAPSlc2Ctrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Thalassa Map Line Control Slice2\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc1Ctrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "MAPSlc1Ctrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Thalassa Map Line Control Slice1\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc0Ctrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MAPSlc0Ctrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Thalassa Map Line Control Slice0\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MAPSlc7Ctrl_ParErrFrc"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Force_Control._MAPSlc7Ctrl_ParErrFrc()
            allFields["MAPSlc6Ctrl_ParErrFrc"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Force_Control._MAPSlc6Ctrl_ParErrFrc()
            allFields["MAPSlc5Ctrl_ParErrFrc"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Force_Control._MAPSlc5Ctrl_ParErrFrc()
            allFields["MAPSlc4Ctrl_ParErrFrc"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Force_Control._MAPSlc4Ctrl_ParErrFrc()
            allFields["MAPSlc3Ctrl_ParErrFrc"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Force_Control._MAPSlc3Ctrl_ParErrFrc()
            allFields["MAPSlc2Ctrl_ParErrFrc"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Force_Control._MAPSlc2Ctrl_ParErrFrc()
            allFields["MAPSlc1Ctrl_ParErrFrc"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Force_Control._MAPSlc1Ctrl_ParErrFrc()
            allFields["MAPSlc0Ctrl_ParErrFrc"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Force_Control._MAPSlc0Ctrl_ParErrFrc()
            return allFields

    class _RAM_Map_Parity_Disable_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM Map Parity Disable Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004809
            
        def endAddress(self):
            return 0xffffffff

        class _MAPSlc7Ctrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "MAPSlc7Ctrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Thalassa Map Line Control Slice7\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc6Ctrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "MAPSlc6Ctrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Thalassa Map Line Control Slice6\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc5Ctrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "MAPSlc5Ctrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Thalassa Map Line Control Slice5\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc4Ctrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "MAPSlc4Ctrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Thalassa Map Line Control Slice4\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc3Ctrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "MAPSlc3Ctrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Thalassa Map Line Control Slice3\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc2Ctrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "MAPSlc2Ctrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Thalassa Map Line Control Slice2\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc1Ctrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "MAPSlc1Ctrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Thalassa Map Line Control Slice1\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc0Ctrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MAPSlc0Ctrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Thalassa Map Line Control Slice0\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MAPSlc7Ctrl_ParErrDis"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Disable_Control._MAPSlc7Ctrl_ParErrDis()
            allFields["MAPSlc6Ctrl_ParErrDis"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Disable_Control._MAPSlc6Ctrl_ParErrDis()
            allFields["MAPSlc5Ctrl_ParErrDis"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Disable_Control._MAPSlc5Ctrl_ParErrDis()
            allFields["MAPSlc4Ctrl_ParErrDis"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Disable_Control._MAPSlc4Ctrl_ParErrDis()
            allFields["MAPSlc3Ctrl_ParErrDis"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Disable_Control._MAPSlc3Ctrl_ParErrDis()
            allFields["MAPSlc2Ctrl_ParErrDis"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Disable_Control._MAPSlc2Ctrl_ParErrDis()
            allFields["MAPSlc1Ctrl_ParErrDis"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Disable_Control._MAPSlc1Ctrl_ParErrDis()
            allFields["MAPSlc0Ctrl_ParErrDis"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Disable_Control._MAPSlc0Ctrl_ParErrDis()
            return allFields

    class _RAM_Map_Parity_Error_Sticky(AtRegister.AtRegister):
        def name(self):
            return "RAM Map parity Error Sticky"
    
        def description(self):
            return "This register configures disable parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000480a
            
        def endAddress(self):
            return 0xffffffff

        class _MAPSlc7Ctrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "MAPSlc7Ctrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"Thalassa Map Line Control Slice7\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc6Ctrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "MAPSlc6Ctrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"Thalassa Map Line Control Slice6\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc5Ctrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "MAPSlc5Ctrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"Thalassa Map Line Control Slice5\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc4Ctrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "MAPSlc4Ctrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"Thalassa Map Line Control Slice4\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc3Ctrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "MAPSlc3Ctrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"Thalassa Map Line Control Slice3\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc2Ctrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "MAPSlc2Ctrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"Thalassa Map Line Control Slice2\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc1Ctrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "MAPSlc1Ctrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"Thalassa Map Line Control Slice1\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc0Ctrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MAPSlc0Ctrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"Thalassa Map Line Control Slice0\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MAPSlc7Ctrl_ParErrStk"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Error_Sticky._MAPSlc7Ctrl_ParErrStk()
            allFields["MAPSlc6Ctrl_ParErrStk"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Error_Sticky._MAPSlc6Ctrl_ParErrStk()
            allFields["MAPSlc5Ctrl_ParErrStk"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Error_Sticky._MAPSlc5Ctrl_ParErrStk()
            allFields["MAPSlc4Ctrl_ParErrStk"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Error_Sticky._MAPSlc4Ctrl_ParErrStk()
            allFields["MAPSlc3Ctrl_ParErrStk"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Error_Sticky._MAPSlc3Ctrl_ParErrStk()
            allFields["MAPSlc2Ctrl_ParErrStk"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Error_Sticky._MAPSlc2Ctrl_ParErrStk()
            allFields["MAPSlc1Ctrl_ParErrStk"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Error_Sticky._MAPSlc1Ctrl_ParErrStk()
            allFields["MAPSlc0Ctrl_ParErrStk"] = _AF6CNC0022_RD_MAP_HO._RAM_Map_Parity_Error_Sticky._MAPSlc0Ctrl_ParErrStk()
            return allFields

    class _RAM_DeMap_Parity_Force_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM DeMap Parity Force Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000808
            
        def endAddress(self):
            return 0xffffffff

        class _MAPSlc7Ctrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "MAPSlc7Ctrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Thalassa Map Line Control Slice7\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc6Ctrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "MAPSlc6Ctrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Thalassa Map Line Control Slice6\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc5Ctrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "MAPSlc5Ctrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Thalassa Map Line Control Slice5\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc4Ctrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "MAPSlc4Ctrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Thalassa Map Line Control Slice4\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc3Ctrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "MAPSlc3Ctrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Thalassa Map Line Control Slice3\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc2Ctrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "MAPSlc2Ctrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Thalassa Map Line Control Slice2\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc1Ctrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "MAPSlc1Ctrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Thalassa Map Line Control Slice1\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc0Ctrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MAPSlc0Ctrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"Thalassa Map Line Control Slice0\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MAPSlc7Ctrl_ParErrFrc"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Force_Control._MAPSlc7Ctrl_ParErrFrc()
            allFields["MAPSlc6Ctrl_ParErrFrc"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Force_Control._MAPSlc6Ctrl_ParErrFrc()
            allFields["MAPSlc5Ctrl_ParErrFrc"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Force_Control._MAPSlc5Ctrl_ParErrFrc()
            allFields["MAPSlc4Ctrl_ParErrFrc"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Force_Control._MAPSlc4Ctrl_ParErrFrc()
            allFields["MAPSlc3Ctrl_ParErrFrc"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Force_Control._MAPSlc3Ctrl_ParErrFrc()
            allFields["MAPSlc2Ctrl_ParErrFrc"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Force_Control._MAPSlc2Ctrl_ParErrFrc()
            allFields["MAPSlc1Ctrl_ParErrFrc"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Force_Control._MAPSlc1Ctrl_ParErrFrc()
            allFields["MAPSlc0Ctrl_ParErrFrc"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Force_Control._MAPSlc0Ctrl_ParErrFrc()
            return allFields

    class _RAM_DeMap_Parity_Disable_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM DeMap Parity Disable Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000809
            
        def endAddress(self):
            return 0xffffffff

        class _MAPSlc7Ctrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "MAPSlc7Ctrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Thalassa Map Line Control Slice7\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc6Ctrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "MAPSlc6Ctrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Thalassa Map Line Control Slice6\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc5Ctrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "MAPSlc5Ctrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Thalassa Map Line Control Slice5\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc4Ctrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "MAPSlc4Ctrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Thalassa Map Line Control Slice4\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc3Ctrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "MAPSlc3Ctrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Thalassa Map Line Control Slice3\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc2Ctrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "MAPSlc2Ctrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Thalassa Map Line Control Slice2\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc1Ctrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "MAPSlc1Ctrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Thalassa Map Line Control Slice1\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc0Ctrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MAPSlc0Ctrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"Thalassa Map Line Control Slice0\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MAPSlc7Ctrl_ParErrDis"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Disable_Control._MAPSlc7Ctrl_ParErrDis()
            allFields["MAPSlc6Ctrl_ParErrDis"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Disable_Control._MAPSlc6Ctrl_ParErrDis()
            allFields["MAPSlc5Ctrl_ParErrDis"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Disable_Control._MAPSlc5Ctrl_ParErrDis()
            allFields["MAPSlc4Ctrl_ParErrDis"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Disable_Control._MAPSlc4Ctrl_ParErrDis()
            allFields["MAPSlc3Ctrl_ParErrDis"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Disable_Control._MAPSlc3Ctrl_ParErrDis()
            allFields["MAPSlc2Ctrl_ParErrDis"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Disable_Control._MAPSlc2Ctrl_ParErrDis()
            allFields["MAPSlc1Ctrl_ParErrDis"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Disable_Control._MAPSlc1Ctrl_ParErrDis()
            allFields["MAPSlc0Ctrl_ParErrDis"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Disable_Control._MAPSlc0Ctrl_ParErrDis()
            return allFields

    class _RAM_DeMap_Parity_Error_Sticky(AtRegister.AtRegister):
        def name(self):
            return "RAM DeMap parity Error Sticky"
    
        def description(self):
            return "This register configures disable parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000080a
            
        def endAddress(self):
            return 0xffffffff

        class _MAPSlc7Ctrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "MAPSlc7Ctrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"Thalassa Map Line Control Slice7\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc6Ctrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "MAPSlc6Ctrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"Thalassa Map Line Control Slice6\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc5Ctrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "MAPSlc5Ctrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"Thalassa Map Line Control Slice5\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc4Ctrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "MAPSlc4Ctrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"Thalassa Map Line Control Slice4\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc3Ctrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "MAPSlc3Ctrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"Thalassa Map Line Control Slice3\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc2Ctrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "MAPSlc2Ctrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"Thalassa Map Line Control Slice2\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc1Ctrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "MAPSlc1Ctrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"Thalassa Map Line Control Slice1\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSlc0Ctrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MAPSlc0Ctrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"Thalassa Map Line Control Slice0\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MAPSlc7Ctrl_ParErrStk"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Error_Sticky._MAPSlc7Ctrl_ParErrStk()
            allFields["MAPSlc6Ctrl_ParErrStk"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Error_Sticky._MAPSlc6Ctrl_ParErrStk()
            allFields["MAPSlc5Ctrl_ParErrStk"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Error_Sticky._MAPSlc5Ctrl_ParErrStk()
            allFields["MAPSlc4Ctrl_ParErrStk"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Error_Sticky._MAPSlc4Ctrl_ParErrStk()
            allFields["MAPSlc3Ctrl_ParErrStk"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Error_Sticky._MAPSlc3Ctrl_ParErrStk()
            allFields["MAPSlc2Ctrl_ParErrStk"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Error_Sticky._MAPSlc2Ctrl_ParErrStk()
            allFields["MAPSlc1Ctrl_ParErrStk"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Error_Sticky._MAPSlc1Ctrl_ParErrStk()
            allFields["MAPSlc0Ctrl_ParErrStk"] = _AF6CNC0022_RD_MAP_HO._RAM_DeMap_Parity_Error_Sticky._MAPSlc0Ctrl_ParErrStk()
            return allFields

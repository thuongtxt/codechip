import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0022_RD_PLA_DEBUG(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["pla_out_clk311_ctrl"] = _AF6CNC0022_RD_PLA_DEBUG._pla_out_clk311_ctrl()
        allRegisters["pla_oc48_clk311_stk"] = _AF6CNC0022_RD_PLA_DEBUG._pla_oc48_clk311_stk()
        allRegisters["pla_buf_stk1"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1()
        allRegisters["pla_buf_stk2"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2()
        allRegisters["pla_buf_stk3"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3()
        allRegisters["pla_buf_blk_num_sta"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_blk_num_sta()
        allRegisters["pla_buf_blk_cache_sta"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_blk_cache_sta()
        return allRegisters

    class _pla_out_clk311_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler clk311 Control"
    
        def description(self):
            return "This register is used to control pwid for debug"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_0000 + $Oc96Slice*32768 + $Oc48Slice*8192"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x0001a000

        class _PWID_for_debug(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PWID_for_debug"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PWID_for_debug"] = _AF6CNC0022_RD_PLA_DEBUG._pla_out_clk311_ctrl._PWID_for_debug()
            return allFields

    class _pla_oc48_clk311_stk(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler OC48 clk311 Sticky"
    
        def description(self):
            return "This register is used to used to sticky some alarms for debug per OC-48 @clk311.02 domain"
            
        def width(self):
            return 14
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0_0001 + $Oc96Slice*32768 + $Oc48Slice*8192"
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0x0001a001

        class _Pla311OC48_ConvErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "Pla311OC48_ConvErr"
            
            def description(self):
                return "OC48 conver clock error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _Pla311OC48_InPosPW(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "Pla311OC48_InPosPW"
            
            def description(self):
                return "OC48 input CEP Pos based PW"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _Pla311OC48_InNegPW(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Pla311OC48_InNegPW"
            
            def description(self):
                return "OC48 input CEP Neg based PW"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _Pla311OC48_InValodPW(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "Pla311OC48_InValodPW"
            
            def description(self):
                return "OC48 input Valid based PW"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _Pla311OC48_InValid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Pla311OC48_InValid"
            
            def description(self):
                return "OC48 input valid"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _Pla311OC48_InAisPW(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Pla311OC48_InAisPW"
            
            def description(self):
                return "OC48 input AIS based PW"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _Pla311OC48_InAIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Pla311OC48_InAIS"
            
            def description(self):
                return "OC48 input AIS"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _Pla311OC48_InJ1PosPW(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Pla311OC48_InJ1PosPW"
            
            def description(self):
                return "OC48 input CEP J1 Posiion based PW"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _Pla311OC48_InPos(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Pla311OC48_InPos"
            
            def description(self):
                return "OC48 input CEP Pos Pointer"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _Pla311OC48_InNeg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Pla311OC48_InNeg"
            
            def description(self):
                return "OC48 input CEP Neg Pointer"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _Pla311OC48_InJ1Pos(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Pla311OC48_InJ1Pos"
            
            def description(self):
                return "OC48 input CEP J1 Position"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Pla311OC48_ConvErr"] = _AF6CNC0022_RD_PLA_DEBUG._pla_oc48_clk311_stk._Pla311OC48_ConvErr()
            allFields["Pla311OC48_InPosPW"] = _AF6CNC0022_RD_PLA_DEBUG._pla_oc48_clk311_stk._Pla311OC48_InPosPW()
            allFields["Pla311OC48_InNegPW"] = _AF6CNC0022_RD_PLA_DEBUG._pla_oc48_clk311_stk._Pla311OC48_InNegPW()
            allFields["Pla311OC48_InValodPW"] = _AF6CNC0022_RD_PLA_DEBUG._pla_oc48_clk311_stk._Pla311OC48_InValodPW()
            allFields["Pla311OC48_InValid"] = _AF6CNC0022_RD_PLA_DEBUG._pla_oc48_clk311_stk._Pla311OC48_InValid()
            allFields["Pla311OC48_InAisPW"] = _AF6CNC0022_RD_PLA_DEBUG._pla_oc48_clk311_stk._Pla311OC48_InAisPW()
            allFields["Pla311OC48_InAIS"] = _AF6CNC0022_RD_PLA_DEBUG._pla_oc48_clk311_stk._Pla311OC48_InAIS()
            allFields["Pla311OC48_InJ1PosPW"] = _AF6CNC0022_RD_PLA_DEBUG._pla_oc48_clk311_stk._Pla311OC48_InJ1PosPW()
            allFields["Pla311OC48_InPos"] = _AF6CNC0022_RD_PLA_DEBUG._pla_oc48_clk311_stk._Pla311OC48_InPos()
            allFields["Pla311OC48_InNeg"] = _AF6CNC0022_RD_PLA_DEBUG._pla_oc48_clk311_stk._Pla311OC48_InNeg()
            allFields["Pla311OC48_InJ1Pos"] = _AF6CNC0022_RD_PLA_DEBUG._pla_oc48_clk311_stk._Pla311OC48_InJ1Pos()
            return allFields

    class _pla_buf_stk1(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Buffer Sticky 1"
    
        def description(self):
            return "This register is used to used to sticky some alarms for debug#1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00042000
            
        def endAddress(self):
            return 0xffffffff

        class _PlaGetAllBlkErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "PlaGetAllBlkErr"
            
            def description(self):
                return "Get All Block Type Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaGetHiBlkErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "PlaGetHiBlkErr"
            
            def description(self):
                return "Get Hi-Block Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaGetLoBlkErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "PlaGetLoBlkErr"
            
            def description(self):
                return "Get Lo-Block Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaAllCaEmpt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "PlaAllCaEmpt"
            
            def description(self):
                return "All Cache Empty Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaGetCa512Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "PlaGetCa512Err"
            
            def description(self):
                return "Get cache-512 Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaGetCa256Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "PlaGetCa256Err"
            
            def description(self):
                return "Get cache-256 Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaGetCa128Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "PlaGetCa128Err"
            
            def description(self):
                return "Get cache-128 Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrHiBlkNearFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "PlaWrHiBlkNearFul"
            
            def description(self):
                return "Monitor high block near full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrLoBlkNearFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "PlaWrLoBlkNearFul"
            
            def description(self):
                return "Monitor low block near full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrCaNearFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "PlaWrCaNearFul"
            
            def description(self):
                return "Monitor total cache near full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrBlkErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "PlaWrBlkErr"
            
            def description(self):
                return "block fail due to near empty cache/block"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrDDRFifoFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "PlaWrDDRFifoFul"
            
            def description(self):
                return "Write DDR Fifo Full (due to 1st or 2nd buffer)"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrDDRLenErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "PlaWrDDRLenErr"
            
            def description(self):
                return "Write DDR Check Length Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaHiBlkEmpt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PlaHiBlkEmpt"
            
            def description(self):
                return "High Block Empty Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoBlkEmpt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PlaLoBlkEmpt"
            
            def description(self):
                return "Low Block Empty Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrDDRReorRdFifoFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "PlaWrDDRReorRdFifoFul"
            
            def description(self):
                return "Write DDR Reorder Mux Read Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrDDRReorWrFifoFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "PlaWrDDRReorWrFifoFul"
            
            def description(self):
                return "Write DDR Reorder Mux Write Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaHiBlkSame(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PlaHiBlkSame"
            
            def description(self):
                return "High Block Same Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoBlkSame(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PlaLoBlkSame"
            
            def description(self):
                return "Low Block Same Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaCa512Empt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PlaCa512Empt"
            
            def description(self):
                return "Cache 512 Empty Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaCa256Empt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PlaCa256Empt"
            
            def description(self):
                return "Cache 256 Empty Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaCa128Empt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PlaCa128Empt"
            
            def description(self):
                return "Cache 128 Empty Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaCa512Same(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PlaCa512Same"
            
            def description(self):
                return "Cache 512 Same Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaCa256Same(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PlaCa256Same"
            
            def description(self):
                return "Cache 256 Same Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaCa128Same(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PlaCa128Same"
            
            def description(self):
                return "Cache 128 Same Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaMaxLengthErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PlaMaxLengthErr"
            
            def description(self):
                return "Max Length Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaReqWrDdrNearFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PlaReqWrDdrNearFul"
            
            def description(self):
                return "Request write DDR near full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaFreeBlockSet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PlaFreeBlockSet"
            
            def description(self):
                return "Free Block Set"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaFreeCacheSet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaFreeCacheSet"
            
            def description(self):
                return "Free Cache Set"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaGetAllBlkErr"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaGetAllBlkErr()
            allFields["PlaGetHiBlkErr"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaGetHiBlkErr()
            allFields["PlaGetLoBlkErr"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaGetLoBlkErr()
            allFields["PlaAllCaEmpt"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaAllCaEmpt()
            allFields["PlaGetCa512Err"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaGetCa512Err()
            allFields["PlaGetCa256Err"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaGetCa256Err()
            allFields["PlaGetCa128Err"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaGetCa128Err()
            allFields["PlaWrHiBlkNearFul"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaWrHiBlkNearFul()
            allFields["PlaWrLoBlkNearFul"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaWrLoBlkNearFul()
            allFields["PlaWrCaNearFul"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaWrCaNearFul()
            allFields["PlaWrBlkErr"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaWrBlkErr()
            allFields["PlaWrDDRFifoFul"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaWrDDRFifoFul()
            allFields["PlaWrDDRLenErr"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaWrDDRLenErr()
            allFields["PlaHiBlkEmpt"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaHiBlkEmpt()
            allFields["PlaLoBlkEmpt"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaLoBlkEmpt()
            allFields["PlaWrDDRReorRdFifoFul"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaWrDDRReorRdFifoFul()
            allFields["PlaWrDDRReorWrFifoFul"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaWrDDRReorWrFifoFul()
            allFields["PlaHiBlkSame"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaHiBlkSame()
            allFields["PlaLoBlkSame"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaLoBlkSame()
            allFields["PlaCa512Empt"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaCa512Empt()
            allFields["PlaCa256Empt"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaCa256Empt()
            allFields["PlaCa128Empt"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaCa128Empt()
            allFields["PlaCa512Same"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaCa512Same()
            allFields["PlaCa256Same"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaCa256Same()
            allFields["PlaCa128Same"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaCa128Same()
            allFields["PlaMaxLengthErr"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaMaxLengthErr()
            allFields["PlaReqWrDdrNearFul"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaReqWrDdrNearFul()
            allFields["PlaFreeBlockSet"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaFreeBlockSet()
            allFields["PlaFreeCacheSet"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk1._PlaFreeCacheSet()
            return allFields

    class _pla_buf_stk2(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Buffer Sticky 2"
    
        def description(self):
            return "This register is used to used to sticky some alarms for debug#2"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00042001
            
        def endAddress(self):
            return 0xffffffff

        class _PlaWrDDRReorderErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "PlaWrDDRReorderErr"
            
            def description(self):
                return "Write DDR Reorder Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrDDRBuf1stFifoFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "PlaWrDDRBuf1stFifoFul"
            
            def description(self):
                return "Write DDR Buffer 1st Fifo Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrDDRBuf2ndFifoFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "PlaWrDDRBuf2ndFifoFul"
            
            def description(self):
                return "Write DDR Buffer 2nd Fifo Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrDDRAckFifoFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "PlaWrDDRAckFifoFul"
            
            def description(self):
                return "Write DDR ACK Fifo Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPkBufFFFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "PlaPkBufFFFul"
            
            def description(self):
                return "Packet Buffer Fifo Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPkBufFFRdy(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "PlaPkBufFFRdy"
            
            def description(self):
                return "Packet Buffer Fifo Ready"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPkBufFull(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "PlaPkBufFull"
            
            def description(self):
                return "Packet Buffer Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPkBufNotEmpt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "PlaPkBufNotEmpt"
            
            def description(self):
                return "Packet Buffer Not Empty"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaRdPortPktDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "PlaRdPortPktDis"
            
            def description(self):
                return "Read Port Side Packet Disabled"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaRdPortPktFFErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "PlaRdPortPktFFErr"
            
            def description(self):
                return "Read Port Side Packet Fifo Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaRdPortDatFFErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PlaRdPortDatFFErr"
            
            def description(self):
                return "Read Port Side Data Fifo Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaRdPortInfFFErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PlaRdPortInfFFErr"
            
            def description(self):
                return "Read Port Side Info Fifo Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaRdBufDatPkVldFFErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "PlaRdBufDatPkVldFFErr"
            
            def description(self):
                return "Read Buf Side Pkt Vld Fifo Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaRdBufDatReqVldFFErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "PlaRdBufDatReqVldFFErr"
            
            def description(self):
                return "Read Buf Side Data Request Vld Fifo Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaRdBufPSNAckFFErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PlaRdBufPSNAckFFErr"
            
            def description(self):
                return "Read Buf Side PSN Ack Fifo Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaRdBufPSNVldFFErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PlaRdBufPSNVldFFErr"
            
            def description(self):
                return "Read Buf Side PSN Vld Fifo Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaMuxPWSl96_3_SL48_1_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PlaMuxPWSl96_3_SL48_1_Err"
            
            def description(self):
                return "Fifo Mux PW SLice48#1 of Slice96#3 Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaMuxPWSl96_3_SL48_0_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PlaMuxPWSl96_3_SL48_0_Err"
            
            def description(self):
                return "Fifo Mux PW Slice48#0 of Slice96#3 Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaMuxPWSl96_2_SL48_1_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PlaMuxPWSl96_2_SL48_1_Err"
            
            def description(self):
                return "Fifo Mux PW Slice48#1 of Slice96#2 Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaMuxPWSl96_2_SL48_0_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PlaMuxPWSl96_2_SL48_0_Err"
            
            def description(self):
                return "Fifo Mux PW Slice48#0 of Slice96#2 Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaMuxPWSl96_1_SL48_1_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PlaMuxPWSl96_1_SL48_1_Err"
            
            def description(self):
                return "Fifo Mux PW SLice48#1 of Slice96#1 Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaMuxPWSl96_1_SL48_0_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PlaMuxPWSl96_1_SL48_0_Err"
            
            def description(self):
                return "Fifo Mux PW Slice48#0 of Slice96#1 Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaMuxPWSl96_0_SL48_1_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PlaMuxPWSl96_0_SL48_1_Err"
            
            def description(self):
                return "Fifo Mux PW Slice48#1 of Slice96#0 Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaMuxPWSl96_0_SL48_0_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PlaMuxPWSl96_0_SL48_0_Err"
            
            def description(self):
                return "Fifo Mux PW Slice48#0 of Slice96#0 Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaMuxPWSl96_3_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PlaMuxPWSl96_3_Err"
            
            def description(self):
                return "Fifo Mux PW Slice96#3 Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaMuxPWSl96_2_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PlaMuxPWSl96_2_Err"
            
            def description(self):
                return "Fifo Mux PW Slice96#2 Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaMuxPWSl96_1_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PlaMuxPWSl96_1_Err"
            
            def description(self):
                return "Fifo Mux PW Slice96#1 Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaMuxPWSl96_0_Err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaMuxPWSl96_0_Err"
            
            def description(self):
                return "Fifo Mux PW Slice96#0 Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaWrDDRReorderErr"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaWrDDRReorderErr()
            allFields["PlaWrDDRBuf1stFifoFul"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaWrDDRBuf1stFifoFul()
            allFields["PlaWrDDRBuf2ndFifoFul"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaWrDDRBuf2ndFifoFul()
            allFields["PlaWrDDRAckFifoFul"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaWrDDRAckFifoFul()
            allFields["PlaPkBufFFFul"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaPkBufFFFul()
            allFields["PlaPkBufFFRdy"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaPkBufFFRdy()
            allFields["PlaPkBufFull"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaPkBufFull()
            allFields["PlaPkBufNotEmpt"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaPkBufNotEmpt()
            allFields["PlaRdPortPktDis"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaRdPortPktDis()
            allFields["PlaRdPortPktFFErr"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaRdPortPktFFErr()
            allFields["PlaRdPortDatFFErr"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaRdPortDatFFErr()
            allFields["PlaRdPortInfFFErr"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaRdPortInfFFErr()
            allFields["PlaRdBufDatPkVldFFErr"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaRdBufDatPkVldFFErr()
            allFields["PlaRdBufDatReqVldFFErr"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaRdBufDatReqVldFFErr()
            allFields["PlaRdBufPSNAckFFErr"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaRdBufPSNAckFFErr()
            allFields["PlaRdBufPSNVldFFErr"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaRdBufPSNVldFFErr()
            allFields["PlaMuxPWSl96_3_SL48_1_Err"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaMuxPWSl96_3_SL48_1_Err()
            allFields["PlaMuxPWSl96_3_SL48_0_Err"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaMuxPWSl96_3_SL48_0_Err()
            allFields["PlaMuxPWSl96_2_SL48_1_Err"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaMuxPWSl96_2_SL48_1_Err()
            allFields["PlaMuxPWSl96_2_SL48_0_Err"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaMuxPWSl96_2_SL48_0_Err()
            allFields["PlaMuxPWSl96_1_SL48_1_Err"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaMuxPWSl96_1_SL48_1_Err()
            allFields["PlaMuxPWSl96_1_SL48_0_Err"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaMuxPWSl96_1_SL48_0_Err()
            allFields["PlaMuxPWSl96_0_SL48_1_Err"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaMuxPWSl96_0_SL48_1_Err()
            allFields["PlaMuxPWSl96_0_SL48_0_Err"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaMuxPWSl96_0_SL48_0_Err()
            allFields["PlaMuxPWSl96_3_Err"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaMuxPWSl96_3_Err()
            allFields["PlaMuxPWSl96_2_Err"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaMuxPWSl96_2_Err()
            allFields["PlaMuxPWSl96_1_Err"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaMuxPWSl96_1_Err()
            allFields["PlaMuxPWSl96_0_Err"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk2._PlaMuxPWSl96_0_Err()
            return allFields

    class _pla_buf_stk3(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Buffer Sticky 3"
    
        def description(self):
            return "This register is used to used to sticky some alarms for debug#3"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00042002
            
        def endAddress(self):
            return 0xffffffff

        class _PlaWrDDRBak7FifoFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "PlaWrDDRBak7FifoFul"
            
            def description(self):
                return "Write DDR Bak#7 Fifo Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrDDRBak6FifoFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "PlaWrDDRBak6FifoFul"
            
            def description(self):
                return "Write DDR Bak#6 Fifo Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrDDRBak5FifoFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "PlaWrDDRBak5FifoFul"
            
            def description(self):
                return "Write DDR Bak#5 Fifo Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrDDRBak4FifoFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "PlaWrDDRBak4FifoFul"
            
            def description(self):
                return "Write DDR Bak#4 Fifo Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrDDRBak3FifoFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "PlaWrDDRBak3FifoFul"
            
            def description(self):
                return "Write DDR Bak#3 Fifo Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrDDRBak2FifoFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "PlaWrDDRBak2FifoFul"
            
            def description(self):
                return "Write DDR Bak#2 Fifo Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrDDRBak1FifoFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "PlaWrDDRBak1FifoFul"
            
            def description(self):
                return "Write DDR Bak#1 Fifo Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrDDRBak0FifoFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "PlaWrDDRBak0FifoFul"
            
            def description(self):
                return "Write DDR Bak#0 Fifo Full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPktInfoFifoNearFul(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "PlaPktInfoFifoNearFul"
            
            def description(self):
                return "Packet Info Fifo near full"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaQDRPSNReqSet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "PlaQDRPSNReqSet"
            
            def description(self):
                return "QDR PSN Request set"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaQDRPSNAckSet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "PlaQDRPSNAckSet"
            
            def description(self):
                return "QDR PSN Ack set"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaQDRPSNVldSet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "PlaQDRPSNVldSet"
            
            def description(self):
                return "QDR PSN valid set"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaFreBlkReFiFoErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "PlaFreBlkReFiFoErr"
            
            def description(self):
                return "Free Block Return Fifo Error"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaDDRRdReqSet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "PlaDDRRdReqSet"
            
            def description(self):
                return "DDR Read Request set"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaDDRRdAckSet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PlaDDRRdAckSet"
            
            def description(self):
                return "DDR Read Ack set"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaDDRRdVldSet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PlaDDRRdVldSet"
            
            def description(self):
                return "DDR Read valid set"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaBlkMdOutEOPSet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "PlaBlkMdOutEOPSet"
            
            def description(self):
                return "Output block module EOP set"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaDDRWrReqSet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "PlaDDRWrReqSet"
            
            def description(self):
                return "DDR Write Request set"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaDDRWrAckSet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PlaDDRWrAckSet"
            
            def description(self):
                return "DDR Write Ack set"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaDDRWrVldSet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PlaDDRWrVldSet"
            
            def description(self):
                return "DDR Write valid set"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaBlkMdInVldSet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PlaBlkMdInVldSet"
            
            def description(self):
                return "Input block module valid set"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaBlkMdInEOPSet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PlaBlkMdInEOPSet"
            
            def description(self):
                return "Input block module EOP set"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPWEGetPSNErr1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PlaPWEGetPSNErr1"
            
            def description(self):
                return "PWE to get PSN Error due to empty"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPWEGetPSNErr0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PlaPWEGetPSNErr0"
            
            def description(self):
                return "PWE to get PSN Error due to PSN len"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPWEGetPSN(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PlaPWEGetPSN"
            
            def description(self):
                return "PWE to get PSN from PLA"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaReadyDat4PWE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PlaReadyDat4PWE"
            
            def description(self):
                return "Ready data for PWE"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPWEGetDat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PlaPWEGetDat"
            
            def description(self):
                return "PWE to get data from PLA"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaReady2RdPkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PlaReady2RdPkt"
            
            def description(self):
                return "Read Port ready to read packet Info"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaRdpkInfVldSet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PlaRdpkInfVldSet"
            
            def description(self):
                return "Read Packet Info Valid Set"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaWrpkInfVldSet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PlaWrpkInfVldSet"
            
            def description(self):
                return "Write Packet Info Valid Set"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaBlkMdOutVldSet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PlaBlkMdOutVldSet"
            
            def description(self):
                return "Output block module valid set"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPWCoreOutVldSet(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaPWCoreOutVldSet"
            
            def description(self):
                return "Output pwcore valid set"
            
            def type(self):
                return "W2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaWrDDRBak7FifoFul"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaWrDDRBak7FifoFul()
            allFields["PlaWrDDRBak6FifoFul"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaWrDDRBak6FifoFul()
            allFields["PlaWrDDRBak5FifoFul"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaWrDDRBak5FifoFul()
            allFields["PlaWrDDRBak4FifoFul"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaWrDDRBak4FifoFul()
            allFields["PlaWrDDRBak3FifoFul"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaWrDDRBak3FifoFul()
            allFields["PlaWrDDRBak2FifoFul"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaWrDDRBak2FifoFul()
            allFields["PlaWrDDRBak1FifoFul"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaWrDDRBak1FifoFul()
            allFields["PlaWrDDRBak0FifoFul"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaWrDDRBak0FifoFul()
            allFields["PlaPktInfoFifoNearFul"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaPktInfoFifoNearFul()
            allFields["PlaQDRPSNReqSet"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaQDRPSNReqSet()
            allFields["PlaQDRPSNAckSet"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaQDRPSNAckSet()
            allFields["PlaQDRPSNVldSet"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaQDRPSNVldSet()
            allFields["PlaFreBlkReFiFoErr"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaFreBlkReFiFoErr()
            allFields["PlaDDRRdReqSet"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaDDRRdReqSet()
            allFields["PlaDDRRdAckSet"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaDDRRdAckSet()
            allFields["PlaDDRRdVldSet"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaDDRRdVldSet()
            allFields["PlaBlkMdOutEOPSet"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaBlkMdOutEOPSet()
            allFields["PlaDDRWrReqSet"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaDDRWrReqSet()
            allFields["PlaDDRWrAckSet"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaDDRWrAckSet()
            allFields["PlaDDRWrVldSet"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaDDRWrVldSet()
            allFields["PlaBlkMdInVldSet"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaBlkMdInVldSet()
            allFields["PlaBlkMdInEOPSet"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaBlkMdInEOPSet()
            allFields["PlaPWEGetPSNErr1"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaPWEGetPSNErr1()
            allFields["PlaPWEGetPSNErr0"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaPWEGetPSNErr0()
            allFields["PlaPWEGetPSN"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaPWEGetPSN()
            allFields["PlaReadyDat4PWE"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaReadyDat4PWE()
            allFields["PlaPWEGetDat"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaPWEGetDat()
            allFields["PlaReady2RdPkt"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaReady2RdPkt()
            allFields["PlaRdpkInfVldSet"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaRdpkInfVldSet()
            allFields["PlaWrpkInfVldSet"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaWrpkInfVldSet()
            allFields["PlaBlkMdOutVldSet"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaBlkMdOutVldSet()
            allFields["PlaPWCoreOutVldSet"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_stk3._PlaPWCoreOutVldSet()
            return allFields

    class _pla_buf_blk_num_sta(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Buffer Block Number Status"
    
        def description(self):
            return "This register is used to store current blocks number"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00042010
            
        def endAddress(self):
            return 0xffffffff

        class _HiBlkNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 20
        
            def name(self):
                return "HiBlkNum"
            
            def description(self):
                return "Current High Block Number (init value is 0x6000)"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _LoBlkNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LoBlkNum"
            
            def description(self):
                return "Current Low Block Number (init value is 0x10000)"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HiBlkNum"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_blk_num_sta._HiBlkNum()
            allFields["LoBlkNum"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_blk_num_sta._LoBlkNum()
            return allFields

    class _pla_buf_blk_cache_sta(AtRegister.AtRegister):
        def name(self):
            return "Payload Assembler Buffer Cache Number Status"
    
        def description(self):
            return "This register is used to store current caches number"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00042011
            
        def endAddress(self):
            return 0xffffffff

        class _Ca512Num(AtRegister.AtRegisterField):
            def stopBit(self):
                return 43
                
            def startBit(self):
                return 32
        
            def name(self):
                return "Ca512Num"
            
            def description(self):
                return "Current Cache-512 Number (init value is 0x800)"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Ca256Num(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Ca256Num"
            
            def description(self):
                return "Current Cache-256 Number (init value is 0x2000)"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Ca128Num(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Ca128Num"
            
            def description(self):
                return "Current Cache-128 Number (init value is 0x2A00)"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Ca512Num"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_blk_cache_sta._Ca512Num()
            allFields["Ca256Num"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_blk_cache_sta._Ca256Num()
            allFields["Ca128Num"] = _AF6CNC0022_RD_PLA_DEBUG._pla_buf_blk_cache_sta._Ca128Num()
            return allFields

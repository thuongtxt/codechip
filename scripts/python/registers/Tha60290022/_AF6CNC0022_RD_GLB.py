import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0022_RD_GLB(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["ProductID"] = _AF6CNC0022_RD_GLB._ProductID()
        allRegisters["YYMMDD_VerID"] = _AF6CNC0022_RD_GLB._YYMMDD_VerID()
        allRegisters["InternalID"] = _AF6CNC0022_RD_GLB._InternalID()
        allRegisters["Active"] = _AF6CNC0022_RD_GLB._Active()
        allRegisters["DebugPwControl"] = _AF6CNC0022_RD_GLB._DebugPwControl()
        allRegisters["DebugPwSticky"] = _AF6CNC0022_RD_GLB._DebugPwSticky()
        allRegisters["DebugPwPlaSpeed"] = _AF6CNC0022_RD_GLB._DebugPwPlaSpeed()
        allRegisters["DebugPwPdaHoSpeed"] = _AF6CNC0022_RD_GLB._DebugPwPdaHoSpeed()
        allRegisters["DebugPwPweSpeed"] = _AF6CNC0022_RD_GLB._DebugPwPweSpeed()
        allRegisters["DebugPwClaLoSpeed"] = _AF6CNC0022_RD_GLB._DebugPwClaLoSpeed()
        allRegisters["ChipTempSticky"] = _AF6CNC0022_RD_GLB._ChipTempSticky()
        allRegisters["DebugMacLoopControl"] = _AF6CNC0022_RD_GLB._DebugMacLoopControl()
        allRegisters["GlbEth1GClkSquelControl"] = _AF6CNC0022_RD_GLB._GlbEth1GClkSquelControl()
        allRegisters["GlbEth10GClkSquelControl"] = _AF6CNC0022_RD_GLB._GlbEth10GClkSquelControl()
        return allRegisters

    class _ProductID(AtRegister.AtRegister):
        def name(self):
            return "Device Product ID"
    
        def description(self):
            return "This register indicates Product ID."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _ProductID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ProductID"
            
            def description(self):
                return "ProductId"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ProductID"] = _AF6CNC0022_RD_GLB._ProductID._ProductID()
            return allFields

    class _YYMMDD_VerID(AtRegister.AtRegister):
        def name(self):
            return "Device Year Month Day Version ID"
    
        def description(self):
            return "This register indicates Year Month Day and main version ID."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _Year(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "Year"
            
            def description(self):
                return "Year"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Month(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Month"
            
            def description(self):
                return "Month"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Day(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Day"
            
            def description(self):
                return "Day"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Version(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Version"
            
            def description(self):
                return "Version"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Year"] = _AF6CNC0022_RD_GLB._YYMMDD_VerID._Year()
            allFields["Month"] = _AF6CNC0022_RD_GLB._YYMMDD_VerID._Month()
            allFields["Day"] = _AF6CNC0022_RD_GLB._YYMMDD_VerID._Day()
            allFields["Version"] = _AF6CNC0022_RD_GLB._YYMMDD_VerID._Version()
            return allFields

    class _InternalID(AtRegister.AtRegister):
        def name(self):
            return "Device Internal ID"
    
        def description(self):
            return "This register indicates internal ID."
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _InternalID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "InternalID"
            
            def description(self):
                return "InternalID"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["InternalID"] = _AF6CNC0022_RD_GLB._InternalID._InternalID()
            return allFields

    class _Active(AtRegister.AtRegister):
        def name(self):
            return "GLB Sub-Core Active"
    
        def description(self):
            return "This register indicates the active ports."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _SubCoreEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SubCoreEn"
            
            def description(self):
                return "Enable per sub-core"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SubCoreEn"] = _AF6CNC0022_RD_GLB._Active._SubCoreEn()
            return allFields

    class _DebugPwControl(AtRegister.AtRegister):
        def name(self):
            return "GLB Debug PW Control"
    
        def description(self):
            return "This register is used to configure debug PW parameters"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _Oc192CepEna(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "Oc192CepEna"
            
            def description(self):
                return "Set 1 to enable debug OC192c CEP PW"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LoTdmPwID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 20
        
            def name(self):
                return "LoTdmPwID"
            
            def description(self):
                return "Lo TDM PW ID or HO Master STS ID corresponding to global PW ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _GlobalPwID12_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 7
        
            def name(self):
                return "GlobalPwID12_0"
            
            def description(self):
                return "Global PW ID bit12_0 need to debug"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OC48ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 4
        
            def name(self):
                return "OC48ID"
            
            def description(self):
                return "OC48 ID, OC192 ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LoOC24Slice(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "LoOC24Slice"
            
            def description(self):
                return "Low Order OC24 slice if the PW belong to low order path"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _GlobalPwID13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "GlobalPwID13"
            
            def description(self):
                return "Global PW ID bit13 need to debug"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _HoPwType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HoPwType"
            
            def description(self):
                return "High Order PW type if the PW belong to high order path 0: STS1/VC3 CEP 1: STS3/VC4 CEP 2: STS12/VC4-4C CEP 3: STS48/VC4-16C CEP 4: VC11/VC12 CEP 5: TU3 CEP 6: STS24/VC4-8C CEP 7: SAToP/CESoP"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Oc192CepEna"] = _AF6CNC0022_RD_GLB._DebugPwControl._Oc192CepEna()
            allFields["LoTdmPwID"] = _AF6CNC0022_RD_GLB._DebugPwControl._LoTdmPwID()
            allFields["GlobalPwID12_0"] = _AF6CNC0022_RD_GLB._DebugPwControl._GlobalPwID12_0()
            allFields["OC48ID"] = _AF6CNC0022_RD_GLB._DebugPwControl._OC48ID()
            allFields["LoOC24Slice"] = _AF6CNC0022_RD_GLB._DebugPwControl._LoOC24Slice()
            allFields["GlobalPwID13"] = _AF6CNC0022_RD_GLB._DebugPwControl._GlobalPwID13()
            allFields["HoPwType"] = _AF6CNC0022_RD_GLB._DebugPwControl._HoPwType()
            return allFields

    class _DebugPwSticky(AtRegister.AtRegister):
        def name(self):
            return "GLB Debug PW Control"
    
        def description(self):
            return "This register is used to debug PW modules"
            
        def width(self):
            return 29
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000024
            
        def endAddress(self):
            return 0xffffffff

        class _ClaPrbsErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "ClaPrbsErr"
            
            def description(self):
                return "CLA output PRBS error"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        class _PwePrbsErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "PwePrbsErr"
            
            def description(self):
                return "PWE input PRBS error"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        class _PdaPrbsErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PdaPrbsErr"
            
            def description(self):
                return "High Order PDA output PRBS error"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPrbsErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PlaPrbsErr"
            
            def description(self):
                return "High Order PLA input PRBS error"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        class _ClaPrbsSyn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "ClaPrbsSyn"
            
            def description(self):
                return "CLA output PRBS sync"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        class _PwePrbsSyn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PwePrbsSyn"
            
            def description(self):
                return "PWE input PRBS sync"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        class _PdaPrbsSyn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PdaPrbsSyn"
            
            def description(self):
                return "High Order PDA output PRBS sync"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaPrbsSyn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaPrbsSyn"
            
            def description(self):
                return "High Order PLA input PRBS sync"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ClaPrbsErr"] = _AF6CNC0022_RD_GLB._DebugPwSticky._ClaPrbsErr()
            allFields["PwePrbsErr"] = _AF6CNC0022_RD_GLB._DebugPwSticky._PwePrbsErr()
            allFields["PdaPrbsErr"] = _AF6CNC0022_RD_GLB._DebugPwSticky._PdaPrbsErr()
            allFields["PlaPrbsErr"] = _AF6CNC0022_RD_GLB._DebugPwSticky._PlaPrbsErr()
            allFields["ClaPrbsSyn"] = _AF6CNC0022_RD_GLB._DebugPwSticky._ClaPrbsSyn()
            allFields["PwePrbsSyn"] = _AF6CNC0022_RD_GLB._DebugPwSticky._PwePrbsSyn()
            allFields["PdaPrbsSyn"] = _AF6CNC0022_RD_GLB._DebugPwSticky._PdaPrbsSyn()
            allFields["PlaPrbsSyn"] = _AF6CNC0022_RD_GLB._DebugPwSticky._PlaPrbsSyn()
            return allFields

    class _DebugPwPlaSpeed(AtRegister.AtRegister):
        def name(self):
            return "GLB Debug PW PLA HO Speed"
    
        def description(self):
            return "This register is used to show PLA HO PW speed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000030
            
        def endAddress(self):
            return 0xffffffff

        class _PlaPwSpeed(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaPwSpeed"
            
            def description(self):
                return "PLA input PW speed"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaPwSpeed"] = _AF6CNC0022_RD_GLB._DebugPwPlaSpeed._PlaPwSpeed()
            return allFields

    class _DebugPwPdaHoSpeed(AtRegister.AtRegister):
        def name(self):
            return "GLB Debug PW PDA HO Speed"
    
        def description(self):
            return "This register is used to show PDA HO PW speed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000031
            
        def endAddress(self):
            return 0xffffffff

        class _PdaPwSpeed(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PdaPwSpeed"
            
            def description(self):
                return "PDA output PW speed"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PdaPwSpeed"] = _AF6CNC0022_RD_GLB._DebugPwPdaHoSpeed._PdaPwSpeed()
            return allFields

    class _DebugPwPweSpeed(AtRegister.AtRegister):
        def name(self):
            return "GLB Debug PW PWE Speed"
    
        def description(self):
            return "This register is used to show PWE PW speed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000034
            
        def endAddress(self):
            return 0xffffffff

        class _PwePwSpeed(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PwePwSpeed"
            
            def description(self):
                return "PWE input PW speed"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwePwSpeed"] = _AF6CNC0022_RD_GLB._DebugPwPweSpeed._PwePwSpeed()
            return allFields

    class _DebugPwClaLoSpeed(AtRegister.AtRegister):
        def name(self):
            return "GLB Debug PW CLA Speed"
    
        def description(self):
            return "This register is used to show PWE PW speed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000035
            
        def endAddress(self):
            return 0xffffffff

        class _ClaPwSpeed(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ClaPwSpeed"
            
            def description(self):
                return "CLA output PW speed"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ClaPwSpeed"] = _AF6CNC0022_RD_GLB._DebugPwClaLoSpeed._ClaPwSpeed()
            return allFields

    class _ChipTempSticky(AtRegister.AtRegister):
        def name(self):
            return "Chip Temperature Sticky"
    
        def description(self):
            return "This register is used to sticky temperature"
            
        def width(self):
            return 3
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000020
            
        def endAddress(self):
            return 0xffffffff

        class _ChipSlr2TempStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "ChipSlr2TempStk"
            
            def description(self):
                return "Chip SLR2 temperature"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        class _ChipSlr1TempStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "ChipSlr1TempStk"
            
            def description(self):
                return "Chip SLR1 temperature"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        class _ChipSlr0TempStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ChipSlr0TempStk"
            
            def description(self):
                return "Chip SLR0 temperature"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ChipSlr2TempStk"] = _AF6CNC0022_RD_GLB._ChipTempSticky._ChipSlr2TempStk()
            allFields["ChipSlr1TempStk"] = _AF6CNC0022_RD_GLB._ChipTempSticky._ChipSlr1TempStk()
            allFields["ChipSlr0TempStk"] = _AF6CNC0022_RD_GLB._ChipTempSticky._ChipSlr0TempStk()
            return allFields

    class _DebugMacLoopControl(AtRegister.AtRegister):
        def name(self):
            return "GLB Debug Mac Loopback Control"
    
        def description(self):
            return "This register is used to configure debug loopback MAC parameters"
            
        def width(self):
            return 2
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000013
            
        def endAddress(self):
            return 0xffffffff

        class _MacMroLoopOut(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "MacMroLoopOut"
            
            def description(self):
                return "Set 1 for Mac 10G/1G/100Fx Loop Out"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Mac40gLoopIn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Mac40gLoopIn"
            
            def description(self):
                return "Set 1 for Mac40gLoopIn"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MacMroLoopOut"] = _AF6CNC0022_RD_GLB._DebugMacLoopControl._MacMroLoopOut()
            allFields["Mac40gLoopIn"] = _AF6CNC0022_RD_GLB._DebugMacLoopControl._Mac40gLoopIn()
            return allFields

    class _GlbEth1GClkSquelControl(AtRegister.AtRegister):
        def name(self):
            return "GLB Eth1G Clock Squelching Control"
    
        def description(self):
            return "This register is used to configure debug PW parameters"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0xffffffff

        class _EthGeLinkDownSchelEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "EthGeLinkDownSchelEn"
            
            def description(self):
                return "Each Bit represent for each ETH port, value each bit is as below 1: Disable 8Khz refout when GE link down occur 0: Enable 8Khz refout when GE link down occur"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EthGeExcessErrRateSchelEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "EthGeExcessErrRateSchelEn"
            
            def description(self):
                return "Each Bit represent for each ETH port, value each bit is as below 1: Disable 8Khz refout when GE Excessive Error Rate occur 0: Enable 8Khz refout when GE Excessive Error Rate occur"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["EthGeLinkDownSchelEn"] = _AF6CNC0022_RD_GLB._GlbEth1GClkSquelControl._EthGeLinkDownSchelEn()
            allFields["EthGeExcessErrRateSchelEn"] = _AF6CNC0022_RD_GLB._GlbEth1GClkSquelControl._EthGeExcessErrRateSchelEn()
            return allFields

    class _GlbEth10GClkSquelControl(AtRegister.AtRegister):
        def name(self):
            return "GLB Eth10G Clock Squelching Control"
    
        def description(self):
            return "This register is used to configure debug PW parameters"
            
        def width(self):
            return 8
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000012
            
        def endAddress(self):
            return 0xffffffff

        class _TenGeLocalFaultSchelEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "TenGeLocalFaultSchelEn"
            
            def description(self):
                return "Each Bit represent for each TenGe port0 and port8, value each bit is as below 1: Disable 8Khz refout when TenGe Local Fault occur 0: Enable 8Khz refout when TenGe Local Fault occur"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TenGeRemoteFaultSchelEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "TenGeRemoteFaultSchelEn"
            
            def description(self):
                return "Each Bit represent for each TenGe port0 and port8, value each bit is as below 1: Disable 8Khz refout when TenGe Remote Fault occur 0: Enable 8Khz refout when TenGe Remote Fault occur"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TenGeLossDataSyncSchelEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "TenGeLossDataSyncSchelEn"
            
            def description(self):
                return "Each Bit represent for each TenGe port0 and port8, value each bit is as below 1: Disable 8Khz refout when TenGe Loss of Data Sync occur 0: Enable 8Khz refout when TenGe Loss of Data Sync occur"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TenGeExcessErrRateSchelEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TenGeExcessErrRateSchelEn"
            
            def description(self):
                return "Each Bit represent for each TenGe port0 and port8, value each bit is as below 1: Disable 8Khz refout when TenGe Excessive Error Rate occur 0: Enable 8Khz refout when TenGe Excessive Error Rate occur"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TenGeLocalFaultSchelEn"] = _AF6CNC0022_RD_GLB._GlbEth10GClkSquelControl._TenGeLocalFaultSchelEn()
            allFields["TenGeRemoteFaultSchelEn"] = _AF6CNC0022_RD_GLB._GlbEth10GClkSquelControl._TenGeRemoteFaultSchelEn()
            allFields["TenGeLossDataSyncSchelEn"] = _AF6CNC0022_RD_GLB._GlbEth10GClkSquelControl._TenGeLossDataSyncSchelEn()
            allFields["TenGeExcessErrRateSchelEn"] = _AF6CNC0022_RD_GLB._GlbEth10GClkSquelControl._TenGeExcessErrRateSchelEn()
            return allFields

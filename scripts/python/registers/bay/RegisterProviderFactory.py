import python.arrive.atsdk.AtRegister as AtRegister

class RegisterProviderFactory(AtRegister.AtRegisterProviderFactory):
    def _allRegisterProviders(self):
        allProviders = {}

        from _AF_CRYPTRD import _AF_CRYPTRD
        allProviders["_AF_CRYPTRD"] = _AF_CRYPTRD()


        return allProviders

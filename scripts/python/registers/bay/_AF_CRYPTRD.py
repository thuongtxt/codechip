import python.arrive.atsdk.AtRegister as AtRegister

class _AF_CRYPTRD(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_maxpsize"] = _AF_CRYPTRD._upen_maxpsize()
        allRegisters["upen_sysfree"] = _AF_CRYPTRD._upen_sysfree()
        allRegisters["upen_donepkt"] = _AF_CRYPTRD._upen_donepkt()
        allRegisters["upen_flowcryptcfg"] = _AF_CRYPTRD._upen_flowcryptcfg()
        allRegisters["upen_flowkeywr0"] = _AF_CRYPTRD._upen_flowkeywr0()
        allRegisters["upen_flowkeywr1"] = _AF_CRYPTRD._upen_flowkeywr1()
        allRegisters["upen_flowkeywr2"] = _AF_CRYPTRD._upen_flowkeywr2()
        allRegisters["upen_flowkeywr3"] = _AF_CRYPTRD._upen_flowkeywr3()
        allRegisters["upen_cryptsalt"] = _AF_CRYPTRD._upen_cryptsalt()
        allRegisters["upen_msbseq"] = _AF_CRYPTRD._upen_msbseq()
        allRegisters["upen_pkttriger"] = _AF_CRYPTRD._upen_pkttriger()
        allRegisters["upen_InStream"] = _AF_CRYPTRD._upen_InStream()
        allRegisters["upen_InStream"] = _AF_CRYPTRD._upen_InStream()
        return allRegisters

    class _upen_maxpsize(AtRegister.AtRegister):
        def name(self):
            return "Sys_MaxPktSize"
    
        def description(self):
            return "Maximum IP packet size to be written"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x100"
            
        def startAddress(self):
            return 0x00000100
            
        def endAddress(self):
            return 0xffffffff

        class _Max_PKT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Max_PKT"
            
            def description(self):
                return "Maximum IP packet size to be written in per 2Kbyte unit: 0x00 : 0 Kbyte 0x01 : 2 Kbyte 0x02 : 4 Kbyte"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Max_PKT"] = _AF_CRYPTRD._upen_maxpsize._Max_PKT()
            return allFields

    class _upen_sysfree(AtRegister.AtRegister):
        def name(self):
            return "Sys_FreePkt"
    
        def description(self):
            return "Current Free Space in Engine in Pkt unit"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x120"
            
        def startAddress(self):
            return 0x00000120
            
        def endAddress(self):
            return 0xffffffff

        class _FreeSize(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FreeSize"
            
            def description(self):
                return "Current Free Space in Engine in Pkt unit"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FreeSize"] = _AF_CRYPTRD._upen_sysfree._FreeSize()
            return allFields

    class _upen_donepkt(AtRegister.AtRegister):
        def name(self):
            return "Sys_DonePkt"
    
        def description(self):
            return "Current Done Pkt number"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x140"
            
        def startAddress(self):
            return 0x00000140
            
        def endAddress(self):
            return 0xffffffff

        class _DoneCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DoneCnt"
            
            def description(self):
                return "Done pkt count"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DoneCnt"] = _AF_CRYPTRD._upen_donepkt._DoneCnt()
            return allFields

    class _upen_flowcryptcfg(AtRegister.AtRegister):
        def name(self):
            return "Flow_CryptConfiguration"
    
        def description(self):
            return "Configure per flow cryto information"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x2000"
            
        def startAddress(self):
            return 0x00002000
            
        def endAddress(self):
            return 0xffffffff

        class _CfgTagLen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "CfgTagLen"
            
            def description(self):
                return "Configure ICV length mode: 0x00 : 8 byte 0x01 : 12 byte 0x02 : 16 byte Other: Reserved"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CfgSeqMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "CfgSeqMode"
            
            def description(self):
                return "Choosing between Extended sequence mode or normal sequence mode: 0x00 : Normal 0x01 : Extended sequence mode (ESN) Other: Reserved"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CfgCryptMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "CfgCryptMode"
            
            def description(self):
                return "Cryto mode : 0x00 : Encr AES CBC 0x01 : Decr AES CBC 0x02 : Encr AES GCM 0x03 : Decr AES GCM Other: Reserved"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CfgKeyMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CfgKeyMode"
            
            def description(self):
                return "Key mode of crypto Engine: 0x00 : 128 bit AES 0x01 : 192 bit AES 0x02 : 256 bit AES Other: Reserved"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CfgTagLen"] = _AF_CRYPTRD._upen_flowcryptcfg._CfgTagLen()
            allFields["CfgSeqMode"] = _AF_CRYPTRD._upen_flowcryptcfg._CfgSeqMode()
            allFields["CfgCryptMode"] = _AF_CRYPTRD._upen_flowcryptcfg._CfgCryptMode()
            allFields["CfgKeyMode"] = _AF_CRYPTRD._upen_flowcryptcfg._CfgKeyMode()
            return allFields

    class _upen_flowkeywr0(AtRegister.AtRegister):
        def name(self):
            return "Flow_CryptKeyWord0"
    
        def description(self):
            return "Configure first 32 bit LSB of Crypto Key Value"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3000"
            
        def startAddress(self):
            return 0x00003000
            
        def endAddress(self):
            return 0xffffffff

        class _CryptKeyWR0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CryptKeyWR0"
            
            def description(self):
                return "first 32 bit LSB of 256 bit Key"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CryptKeyWR0"] = _AF_CRYPTRD._upen_flowkeywr0._CryptKeyWR0()
            return allFields

    class _upen_flowkeywr1(AtRegister.AtRegister):
        def name(self):
            return "Flow_CryptKeyWord1"
    
        def description(self):
            return "Configure second 32 bit LSB of Crypto Key Value"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3400"
            
        def startAddress(self):
            return 0x00003400
            
        def endAddress(self):
            return 0xffffffff

        class _CryptKeyWR1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CryptKeyWR1"
            
            def description(self):
                return "second 32 bit LSB of 256 bit Key"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CryptKeyWR1"] = _AF_CRYPTRD._upen_flowkeywr1._CryptKeyWR1()
            return allFields

    class _upen_flowkeywr2(AtRegister.AtRegister):
        def name(self):
            return "Flow_CryptKeyWord2"
    
        def description(self):
            return "Configure third 32 bit LSB of Crypto Key Value"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3800"
            
        def startAddress(self):
            return 0x00003800
            
        def endAddress(self):
            return 0xffffffff

        class _CryptKeyWR2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CryptKeyWR2"
            
            def description(self):
                return "third 32 bit LSB of 256 bit Key"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CryptKeyWR2"] = _AF_CRYPTRD._upen_flowkeywr2._CryptKeyWR2()
            return allFields

    class _upen_flowkeywr3(AtRegister.AtRegister):
        def name(self):
            return "Flow_CryptKeyWord3"
    
        def description(self):
            return "Configure 32 bit MSB of Crypto Key Value"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x3c00"
            
        def startAddress(self):
            return 0x00003c00
            
        def endAddress(self):
            return 0xffffffff

        class _CryptKeyWR3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CryptKeyWR3"
            
            def description(self):
                return "32 bit MSB of 256 bit Key"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CryptKeyWR3"] = _AF_CRYPTRD._upen_flowkeywr3._CryptKeyWR3()
            return allFields

    class _upen_cryptsalt(AtRegister.AtRegister):
        def name(self):
            return "Flow_CryptSalt"
    
        def description(self):
            return "Configure Salt value"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4000"
            
        def startAddress(self):
            return 0x00004000
            
        def endAddress(self):
            return 0xffffffff

        class _CryptSalt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CryptSalt"
            
            def description(self):
                return "Configure Salt Value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CryptSalt"] = _AF_CRYPTRD._upen_cryptsalt._CryptSalt()
            return allFields

    class _upen_msbseq(AtRegister.AtRegister):
        def name(self):
            return "Flow_MSBSeq"
    
        def description(self):
            return "Configure Salt value"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x4400"
            
        def startAddress(self):
            return 0x00004400
            
        def endAddress(self):
            return 0xffffffff

        class _MSBSeq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MSBSeq"
            
            def description(self):
                return "Configure 32 bit MSB of sequence in Extended sequence mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MSBSeq"] = _AF_CRYPTRD._upen_msbseq._MSBSeq()
            return allFields

    class _upen_pkttriger(AtRegister.AtRegister):
        def name(self):
            return "Pkt_Trigger"
    
        def description(self):
            return "Configure Salt value"
            
        def width(self):
            return 256
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1000_0000"
            
        def startAddress(self):
            return 0x10000000
            
        def endAddress(self):
            return 0xffffffff

        class _Reserved(AtRegister.AtRegisterField):
            def stopBit(self):
                return 255
                
            def startBit(self):
                return 48
        
            def name(self):
                return "Reserved"
            
            def description(self):
                return "Reserved"
            
            def type(self):
                return "WO"
            
            def resetValue(self):
                return 0xffffffff

        class _FlowID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 32
        
            def name(self):
                return "FlowID"
            
            def description(self):
                return "Flow ID of packet"
            
            def type(self):
                return "WO"
            
            def resetValue(self):
                return 0xffffffff

        class _PayloadOfs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PayloadOfs"
            
            def description(self):
                return "offset from start of packet to payload to be encrypted/descrypted"
            
            def type(self):
                return "WO"
            
            def resetValue(self):
                return 0xffffffff

        class _PKtLen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PKtLen"
            
            def description(self):
                return "Packet Length"
            
            def type(self):
                return "WO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Reserved"] = _AF_CRYPTRD._upen_pkttriger._Reserved()
            allFields["FlowID"] = _AF_CRYPTRD._upen_pkttriger._FlowID()
            allFields["PayloadOfs"] = _AF_CRYPTRD._upen_pkttriger._PayloadOfs()
            allFields["PKtLen"] = _AF_CRYPTRD._upen_pkttriger._PKtLen()
            return allFields

    class _upen_InStream(AtRegister.AtRegister):
        def name(self):
            return "Dat_InStream"
    
        def description(self):
            return "Configure Salt value"
            
        def width(self):
            return 256
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1100_0000"
            
        def startAddress(self):
            return 0x11000000
            
        def endAddress(self):
            return 0xffffffff

        class _InData(AtRegister.AtRegisterField):
            def stopBit(self):
                return 255
                
            def startBit(self):
                return 0
        
            def name(self):
                return "InData"
            
            def description(self):
                return "Write Data to engine"
            
            def type(self):
                return "WO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["InData"] = _AF_CRYPTRD._upen_InStream._InData()
            return allFields

    class _upen_InStream(AtRegister.AtRegister):
        def name(self):
            return "Dat_InStream"
    
        def description(self):
            return "Configure Salt value"
            
        def width(self):
            return 256
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1200_0000"
            
        def startAddress(self):
            return 0x12000000
            
        def endAddress(self):
            return 0xffffffff

        class _OutData(AtRegister.AtRegisterField):
            def stopBit(self):
                return 255
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OutData"
            
            def description(self):
                return "Read Data from Engine"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["OutData"] = _AF_CRYPTRD._upen_InStream._OutData()
            return allFields

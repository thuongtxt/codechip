import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0021_RD_PLA(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["pdhpw_pl_assemble_size_ctrl"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_size_ctrl()
        allRegisters["pdhpw_add_prot_prevent_burst"] = _AF6CCI0021_RD_PLA._pdhpw_add_prot_prevent_burst()
        allRegisters["pdhpw_pl_assemble_eng_stat"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_eng_stat()
        allRegisters["pdhpw_pl_assemble_sign_inv_lkup_ctrl"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_inv_lkup_ctrl()
        allRegisters["pdhpw_pl_assemble_sign_buf0_stat"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat()
        return allRegisters

    class _pdhpw_pl_assemble_size_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Thalassa PDHPW Payload Assemble Payload Size Control"
    
        def description(self):
            return "These registers are used to configure payload size in each PW channel"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x002000 + pwid"
            
        def startAddress(self):
            return 0x00002000
            
        def endAddress(self):
            return 0x000020ff

        class _PDHPWPlaAsmPldSupress(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "PDHPWPlaAsmPldSupress"
            
            def description(self):
                return "1: Enable suppress payload when AIS or LOS 0: Disable suppress payload  when AIS or LOS"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmPldTypeCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 15
        
            def name(self):
                return "PDHPWPlaAsmPldTypeCtrl"
            
            def description(self):
                return "Payload type in the PW channel 0: SAToP 4: CES with CAS 5: CES without CAS Others: reserved"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmPldSizeCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHPWPlaAsmPldSizeCtrl"
            
            def description(self):
                return "Payload size in the PW channel SAToP mode: [11:0] payload size in a packet AAL1 mode [5:0] number of DS0 [11:6] number of PDU CES mode [5:0] number of DS0 [14:6] number of NxDS0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHPWPlaAsmPldSupress"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_size_ctrl._PDHPWPlaAsmPldSupress()
            allFields["PDHPWPlaAsmPldTypeCtrl"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_size_ctrl._PDHPWPlaAsmPldTypeCtrl()
            allFields["PDHPWPlaAsmPldSizeCtrl"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_size_ctrl._PDHPWPlaAsmPldSizeCtrl()
            return allFields

    class _pdhpw_add_prot_prevent_burst(AtRegister.AtRegister):
        def name(self):
            return "Thalassa PDHPW Adding Protocol To Prevent Burst"
    
        def description(self):
            return "This register is to add a pseudo-wire that does not cause bottle-neck at transmit PW cause ACR performance bad at receive side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _AddPwId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 4
        
            def name(self):
                return "AddPwId"
            
            def description(self):
                return "PWID to be added"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _AddPwState(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "AddPwState"
            
            def description(self):
                return "State machine to add a PDH PW Step1: Write pseudo-wire ID to be added to AddPwId and set AddPwState to 0x1 to prepare add PW Step2: Do all configuration to add a pseudo-wire such as MAP,DEMAP,PLA,PDA,CLA,PSN.... Step3: Write AddPwState value 0x2 to start add PW protocol Step4: Poll AddPwState until return value of AddPwState equal to 0x0 the finish"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["AddPwId"] = _AF6CCI0021_RD_PLA._pdhpw_add_prot_prevent_burst._AddPwId()
            allFields["AddPwState"] = _AF6CCI0021_RD_PLA._pdhpw_add_prot_prevent_burst._AddPwState()
            return allFields

    class _pdhpw_pl_assemble_eng_stat(AtRegister.AtRegister):
        def name(self):
            return "Thalassa PDHPW Payload Assemble Engine Status"
    
        def description(self):
            return "These registers are used to save status in the engine."
            
        def width(self):
            return 96
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x002400 + pwid"
            
        def startAddress(self):
            return 0x00002400
            
        def endAddress(self):
            return 0x000024ff

        class _PDHPWPlaAsmAAL1PtrSetSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 78
                
            def startBit(self):
                return 78
        
            def name(self):
                return "PDHPWPlaAsmAAL1PtrSetSta"
            
            def description(self):
                return "indicate that the pointer is already set in that AAL1 cycle 1: enable 0: disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigNumStartSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 77
                
            def startBit(self):
                return 76
        
            def name(self):
                return "PDHPWPlaAsmSigNumStartSta"
            
            def description(self):
                return "indicate number of signaling bytes to append into the last payload bytes to become a 4-bytes data valid."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmGenSigCntSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 75
                
            def startBit(self):
                return 71
        
            def name(self):
                return "PDHPWPlaAsmGenSigCntSta"
            
            def description(self):
                return "indicate number of the remaining signaling bytes which need to be append into PW packet."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmPDUNumCntSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 70
                
            def startBit(self):
                return 65
        
            def name(self):
                return "PDHPWPlaAsmPDUNumCntSta"
            
            def description(self):
                return "indicate the current number of the PDU counter in AAL1 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmPDUCylCntSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 64
                
            def startBit(self):
                return 62
        
            def name(self):
                return "PDHPWPlaAsmPDUCylCntSta"
            
            def description(self):
                return "indicate the current number of the cycle PDU counter in AAL1 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmPDUByteCntSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 61
                
            def startBit(self):
                return 58
        
            def name(self):
                return "PDHPWPlaAsmPDUByteCntSta"
            
            def description(self):
                return "indicate the current number of bytes of a PDU in AAL1 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSatopCESByteCntSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 57
                
            def startBit(self):
                return 43
        
            def name(self):
                return "PDHPWPlaAsmSatopCESByteCntSta"
            
            def description(self):
                return "indicate number of bytes in Satop and CES mode With Satop mode: number of bytes of the payload in PW packet With CES mode: [57:49] indicate the current number of NxDS0 counter in CES mode [48:43] indicate the current number of DS0 byte counter in CES mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmDataNumSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 42
                
            def startBit(self):
                return 40
        
            def name(self):
                return "PDHPWPlaAsmDataNumSta"
            
            def description(self):
                return "indicate the current number of data bytes in the data buffer"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmDataBufSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 39
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHPWPlaAsmDataBufSta"
            
            def description(self):
                return "as data buffer to save data with the maximum 5 bytes"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHPWPlaAsmAAL1PtrSetSta"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_eng_stat._PDHPWPlaAsmAAL1PtrSetSta()
            allFields["PDHPWPlaAsmSigNumStartSta"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_eng_stat._PDHPWPlaAsmSigNumStartSta()
            allFields["PDHPWPlaAsmGenSigCntSta"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_eng_stat._PDHPWPlaAsmGenSigCntSta()
            allFields["PDHPWPlaAsmPDUNumCntSta"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_eng_stat._PDHPWPlaAsmPDUNumCntSta()
            allFields["PDHPWPlaAsmPDUCylCntSta"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_eng_stat._PDHPWPlaAsmPDUCylCntSta()
            allFields["PDHPWPlaAsmPDUByteCntSta"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_eng_stat._PDHPWPlaAsmPDUByteCntSta()
            allFields["PDHPWPlaAsmSatopCESByteCntSta"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_eng_stat._PDHPWPlaAsmSatopCESByteCntSta()
            allFields["PDHPWPlaAsmDataNumSta"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_eng_stat._PDHPWPlaAsmDataNumSta()
            allFields["PDHPWPlaAsmDataBufSta"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_eng_stat._PDHPWPlaAsmDataBufSta()
            return allFields

    class _pdhpw_pl_assemble_sign_inv_lkup_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Thalassa PDHPW Payload Assemble Signaling Inverse Lookup Control"
    
        def description(self):
            return "These registers are used to configure which timeslots are mapped into a Pseudowire channels. So this Pseudowire channel can read signaling byte in the signaling buffer to append into Pseudowire packet."
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x002800 + pwid"
            
        def startAddress(self):
            return 0x00002800
            
        def endAddress(self):
            return 0x000028ff

        class _PDHPWPlaAsmSigInvLkIdCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 32
        
            def name(self):
                return "PDHPWPlaAsmSigInvLkIdCtrl"
            
            def description(self):
                return "indicate PDH line identification which the timeslots of the Pseudowire channel belong to."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigInvLkBitMapCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHPWPlaAsmSigInvLkBitMapCtrl"
            
            def description(self):
                return "a 32-bit bit map is used to indicate 32 timeslots of the PDH line channel which belong to the Pseudowire channel. The 32th bit is represent the 32th timeslot, the 31th bit is represent the 31th timeslot, and so so. If a bit is set 1, it mean that the corresponding timeslot is belong to the Pseudowire channel."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHPWPlaAsmSigInvLkIdCtrl"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_inv_lkup_ctrl._PDHPWPlaAsmSigInvLkIdCtrl()
            allFields["PDHPWPlaAsmSigInvLkBitMapCtrl"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_inv_lkup_ctrl._PDHPWPlaAsmSigInvLkBitMapCtrl()
            return allFields

    class _pdhpw_pl_assemble_sign_buf0_stat(AtRegister.AtRegister):
        def name(self):
            return "Thalassa PDHPW Payload Assemble Signaling Buffer 0 Status"
    
        def description(self):
            return "These registers are used to contain signaling data. When A Pseudowire channel carry this timeslot with signaling, it can read out the signaling data to append into the Pseudowire packet."
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00C000 + lid"
            
        def startAddress(self):
            return 0x0000c000
            
        def endAddress(self):
            return 0x0000c02f

        class _PDHPWPlaAsmSigBuf0Ctrl_31(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 124
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_31"
            
            def description(self):
                return "signaling data of the 31th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_30(AtRegister.AtRegisterField):
            def stopBit(self):
                return 123
                
            def startBit(self):
                return 120
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_30"
            
            def description(self):
                return "signaling data of the 30th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_29(AtRegister.AtRegisterField):
            def stopBit(self):
                return 119
                
            def startBit(self):
                return 116
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_29"
            
            def description(self):
                return "signaling data of the 29th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_28(AtRegister.AtRegisterField):
            def stopBit(self):
                return 115
                
            def startBit(self):
                return 112
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_28"
            
            def description(self):
                return "signaling data of the 28th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_27(AtRegister.AtRegisterField):
            def stopBit(self):
                return 111
                
            def startBit(self):
                return 108
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_27"
            
            def description(self):
                return "signaling data of the 27th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_26(AtRegister.AtRegisterField):
            def stopBit(self):
                return 107
                
            def startBit(self):
                return 104
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_26"
            
            def description(self):
                return "signaling data of the 26th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_25(AtRegister.AtRegisterField):
            def stopBit(self):
                return 103
                
            def startBit(self):
                return 100
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_25"
            
            def description(self):
                return "signaling data of the 25th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_24(AtRegister.AtRegisterField):
            def stopBit(self):
                return 99
                
            def startBit(self):
                return 96
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_24"
            
            def description(self):
                return "signaling data of the 24th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_23(AtRegister.AtRegisterField):
            def stopBit(self):
                return 95
                
            def startBit(self):
                return 92
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_23"
            
            def description(self):
                return "signaling data of the 23th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_22(AtRegister.AtRegisterField):
            def stopBit(self):
                return 91
                
            def startBit(self):
                return 88
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_22"
            
            def description(self):
                return "signaling data of the 22th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_21(AtRegister.AtRegisterField):
            def stopBit(self):
                return 87
                
            def startBit(self):
                return 84
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_21"
            
            def description(self):
                return "signaling data of the 21th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_20(AtRegister.AtRegisterField):
            def stopBit(self):
                return 83
                
            def startBit(self):
                return 80
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_20"
            
            def description(self):
                return "signaling data of the 20th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_19(AtRegister.AtRegisterField):
            def stopBit(self):
                return 79
                
            def startBit(self):
                return 76
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_19"
            
            def description(self):
                return "signaling data of the 19th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_18(AtRegister.AtRegisterField):
            def stopBit(self):
                return 75
                
            def startBit(self):
                return 72
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_18"
            
            def description(self):
                return "signaling data of the 18th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_17(AtRegister.AtRegisterField):
            def stopBit(self):
                return 71
                
            def startBit(self):
                return 68
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_17"
            
            def description(self):
                return "signaling data of the 17th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_16(AtRegister.AtRegisterField):
            def stopBit(self):
                return 67
                
            def startBit(self):
                return 64
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_16"
            
            def description(self):
                return "signaling data of the 16th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 60
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_15"
            
            def description(self):
                return "signaling data of the 15th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 59
                
            def startBit(self):
                return 56
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_14"
            
            def description(self):
                return "signaling data of the 14th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 55
                
            def startBit(self):
                return 52
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_13"
            
            def description(self):
                return "signaling data of the 13th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 51
                
            def startBit(self):
                return 48
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_12"
            
            def description(self):
                return "signaling data of the 12th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 44
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_11"
            
            def description(self):
                return "signaling data of the 11th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 43
                
            def startBit(self):
                return 40
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_10"
            
            def description(self):
                return "signaling data of the 10th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_09(AtRegister.AtRegisterField):
            def stopBit(self):
                return 39
                
            def startBit(self):
                return 36
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_09"
            
            def description(self):
                return "signaling data of the 9th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_08(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 32
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_08"
            
            def description(self):
                return "signaling data of the 8th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 28
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_07"
            
            def description(self):
                return "signaling data of the 7th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 24
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_06"
            
            def description(self):
                return "signaling data of the 6th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 20
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_05"
            
            def description(self):
                return "signaling data of the 5th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_04"
            
            def description(self):
                return "signaling data of the 4th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_03"
            
            def description(self):
                return "signaling data of the 3rd timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_02"
            
            def description(self):
                return "signaling data of the 2nd timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_01"
            
            def description(self):
                return "signaling data of the 1st timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHPWPlaAsmSigBuf0Ctrl_00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHPWPlaAsmSigBuf0Ctrl_00"
            
            def description(self):
                return "signaling data of the 0th timeslot"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHPWPlaAsmSigBuf0Ctrl_31"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_31()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_30"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_30()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_29"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_29()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_28"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_28()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_27"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_27()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_26"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_26()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_25"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_25()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_24"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_24()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_23"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_23()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_22"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_22()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_21"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_21()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_20"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_20()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_19"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_19()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_18"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_18()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_17"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_17()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_16"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_16()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_15"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_15()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_14"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_14()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_13"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_13()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_12"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_12()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_11"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_11()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_10"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_10()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_09"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_09()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_08"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_08()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_07"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_07()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_06"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_06()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_05"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_05()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_04"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_04()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_03"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_03()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_02"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_02()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_01"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_01()
            allFields["PDHPWPlaAsmSigBuf0Ctrl_00"] = _AF6CCI0021_RD_PLA._pdhpw_pl_assemble_sign_buf0_stat._PDHPWPlaAsmSigBuf0Ctrl_00()
            return allFields

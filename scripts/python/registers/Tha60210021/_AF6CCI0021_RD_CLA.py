import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0021_RD_CLA(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["Classify_Global_PSN_Control"] = _AF6CCI0021_RD_CLA._Classify_Global_PSN_Control()
        allRegisters["Classify_Pseudowire_Type_Control"] = _AF6CCI0021_RD_CLA._Classify_Pseudowire_Type_Control()
        allRegisters["Classify_HBCE_Global_Control"] = _AF6CCI0021_RD_CLA._Classify_HBCE_Global_Control()
        allRegisters["Classify_HBCE_Hashing_Table_Control"] = _AF6CCI0021_RD_CLA._Classify_HBCE_Hashing_Table_Control()
        allRegisters["Classify_HBCE_Looking_Up_Information_Control"] = _AF6CCI0021_RD_CLA._Classify_HBCE_Looking_Up_Information_Control()
        allRegisters["Classify_VLAN_TAG_Lookup_Control"] = _AF6CCI0021_RD_CLA._Classify_VLAN_TAG_Lookup_Control()
        allRegisters["cla_per_grp_enb"] = _AF6CCI0021_RD_CLA._cla_per_grp_enb()
        allRegisters["Classify_Pseudowire_length_checking_Control"] = _AF6CCI0021_RD_CLA._Classify_Pseudowire_length_checking_Control()
        return allRegisters

    class _Classify_Global_PSN_Control(AtRegister.AtRegister):
        def name(self):
            return "Classify Global PSN Control"
    
        def description(self):
            return "This register controls operation modes of PSN interface."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _RxPsnMplsOutLabel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPsnMplsOutLabel"
            
            def description(self):
                return "Received 2-label MPLS packet from PSN side will be discarded when it's outer label is different than RxPsnMplsOutLabel"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnIpTtlChkEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "RxPsnIpTtlChkEn"
            
            def description(self):
                return "1: Enable check TTL field in Ipv4 header and MPLS label or Ipv6 Hop Limit field in Ipv6 header. When enabled, the packet with TTL or Hop Limit value equal to zero will be discarded and the PSN header error counter will count up. 0: Disable check TTL field or Ipv6 Hop Limit field."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnIpUdpSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "RxPsnIpUdpSel"
            
            def description(self):
                return "This bit is applicable for Ipv4 packet from Ethernet side. 0: Classify engine selects destination UDP port to identify pseudowire packet 1: Classify engine selects source UDP port to identify pseudowire packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnIpUdpMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "RxPsnIpUdpMode"
            
            def description(self):
                return "This bit is applicable for Ipv4/Ipv6 packet from Ethernet side. 0: Classify engine will automatically search for value 0x85E in source or destination UDP port. The remaining UDP port is used to identify pseudowire packet. 1: Classify engine uses RxPsnIpUdpSel to decide which UDP port (source of destination) is used to identify pseudowire packet."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PweLoopClaEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "PweLoopClaEn"
            
            def description(self):
                return "1: Enable loop back from Pseudowire Encapsulation to Classify 0: Normal operation, packets after  Pseudowire Encapsulation will be sent out to Ethernet interface"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnCpuArpEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "RxPsnCpuArpEn"
            
            def description(self):
                return "1: ARP packet from Ethernet is sent to CPU for processing 0: ARP packet from Ethernet is processed by AF6FHW0013 instead of sending to CPU"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnCpuIcmpEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "RxPsnCpuIcmpEn"
            
            def description(self):
                return "1: ICMP packet from Ethernet is sent to CPU for processing 0: ICMP packet from Ethernet is process by AF6FHW0013 instead of sending to CPU"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxMacCheckDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "RxMacCheckDis"
            
            def description(self):
                return "1: Disable checking MAC address at receive direction. 0: Default, enable checking MAC address at receive direction."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnCpuBfdCtlEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "RxPsnCpuBfdCtlEn"
            
            def description(self):
                return "1: VCCV BFD control packet from Ethernet is sent to CPU for processing 0:  VCCV BFD control packet from Ethernet is discarded"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnMplsOutLabelCheckEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "RxPsnMplsOutLabelCheckEn"
            
            def description(self):
                return "1: Enable checking MPLS outer label 0:  Disable checking MPLS outer label (default)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxSendLbit2CdrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "RxSendLbit2CdrEn"
            
            def description(self):
                return "1: Enable (default) 0:  Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxPsnVlanTagModeEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "RxPsnVlanTagModeEn"
            
            def description(self):
                return "1: Enable (default) VLAN TAG mode which contain PWID 0:  Disable VLAN TAG mode, normal PSN operation"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPsnMplsOutLabel"] = _AF6CCI0021_RD_CLA._Classify_Global_PSN_Control._RxPsnMplsOutLabel()
            allFields["RxPsnIpTtlChkEn"] = _AF6CCI0021_RD_CLA._Classify_Global_PSN_Control._RxPsnIpTtlChkEn()
            allFields["RxPsnIpUdpSel"] = _AF6CCI0021_RD_CLA._Classify_Global_PSN_Control._RxPsnIpUdpSel()
            allFields["RxPsnIpUdpMode"] = _AF6CCI0021_RD_CLA._Classify_Global_PSN_Control._RxPsnIpUdpMode()
            allFields["PweLoopClaEn"] = _AF6CCI0021_RD_CLA._Classify_Global_PSN_Control._PweLoopClaEn()
            allFields["RxPsnCpuArpEn"] = _AF6CCI0021_RD_CLA._Classify_Global_PSN_Control._RxPsnCpuArpEn()
            allFields["RxPsnCpuIcmpEn"] = _AF6CCI0021_RD_CLA._Classify_Global_PSN_Control._RxPsnCpuIcmpEn()
            allFields["RxMacCheckDis"] = _AF6CCI0021_RD_CLA._Classify_Global_PSN_Control._RxMacCheckDis()
            allFields["RxPsnCpuBfdCtlEn"] = _AF6CCI0021_RD_CLA._Classify_Global_PSN_Control._RxPsnCpuBfdCtlEn()
            allFields["RxPsnMplsOutLabelCheckEn"] = _AF6CCI0021_RD_CLA._Classify_Global_PSN_Control._RxPsnMplsOutLabelCheckEn()
            allFields["RxSendLbit2CdrEn"] = _AF6CCI0021_RD_CLA._Classify_Global_PSN_Control._RxSendLbit2CdrEn()
            allFields["RxPsnVlanTagModeEn"] = _AF6CCI0021_RD_CLA._Classify_Global_PSN_Control._RxPsnVlanTagModeEn()
            return allFields

    class _Classify_Pseudowire_Type_Control(AtRegister.AtRegister):
        def name(self):
            return "Classify Pseudowire Type Control"
    
        def description(self):
            return "This register configures AF6FHW0013 pseudowire types identification."
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x009C00 + PwId"
            
        def startAddress(self):
            return 0x00009c00
            
        def endAddress(self):
            return 0x00009cff

        class _RxEthPwEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 44
                
            def startBit(self):
                return 44
        
            def name(self):
                return "RxEthPwEn"
            
            def description(self):
                return "set 1 to enable PW, otherwise, disable PW"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthRtpSsrcValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 43
                
            def startBit(self):
                return 12
        
            def name(self):
                return "RxEthRtpSsrcValue"
            
            def description(self):
                return "Used for TDM PW, set 0 for ATM PW. This value is used to compare with SSRC value in RTP header of received TDM PW packets"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthRtpPtValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RxEthRtpPtValue"
            
            def description(self):
                return "Used for TDM PW, set 0 for ATM PW. This value is used to compare with PT value in RTP header of received TDM PW packets"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthRtpEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxEthRtpEn"
            
            def description(self):
                return "Used for TDM PW, set 0 for ATM PW 1: Enable RTP field in received TDM PW packet 0: Disable RTP field in received TDM PW packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthRtpSsrcChkEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "RxEthRtpSsrcChkEn"
            
            def description(self):
                return "Used for TDM PW, set 0 for ATM PW 1: Enable checking  SSRC field of RTP header in received TDM PW packet 0: Disable checking  SSRC field of RTP header in received TDM PW packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthRtpPtChkEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RxEthRtpPtChkEn"
            
            def description(self):
                return "Used for TDM PW, set 0 for ATM PW 1: Enable checking  PT field of RTP header in received TDM PW packet 0: Disable checking  PT field of RTP header in received TDM PW packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthPwType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthPwType"
            
            def description(self):
                return "0: PDH pseudowire without CAS 1: PDH pseudowire with CAS others: reserved"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthPwEn"] = _AF6CCI0021_RD_CLA._Classify_Pseudowire_Type_Control._RxEthPwEn()
            allFields["RxEthRtpSsrcValue"] = _AF6CCI0021_RD_CLA._Classify_Pseudowire_Type_Control._RxEthRtpSsrcValue()
            allFields["RxEthRtpPtValue"] = _AF6CCI0021_RD_CLA._Classify_Pseudowire_Type_Control._RxEthRtpPtValue()
            allFields["RxEthRtpEn"] = _AF6CCI0021_RD_CLA._Classify_Pseudowire_Type_Control._RxEthRtpEn()
            allFields["RxEthRtpSsrcChkEn"] = _AF6CCI0021_RD_CLA._Classify_Pseudowire_Type_Control._RxEthRtpSsrcChkEn()
            allFields["RxEthRtpPtChkEn"] = _AF6CCI0021_RD_CLA._Classify_Pseudowire_Type_Control._RxEthRtpPtChkEn()
            allFields["RxEthPwType"] = _AF6CCI0021_RD_CLA._Classify_Pseudowire_Type_Control._RxEthPwType()
            return allFields

    class _Classify_HBCE_Global_Control(AtRegister.AtRegister):
        def name(self):
            return "Classify HBCE Global Control"
    
        def description(self):
            return "This register is used to configure global signals for HBCE module."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008000
            
        def endAddress(self):
            return 0xffffffff

        class _CLAHbceCodingSelectedMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAHbceCodingSelectedMode"
            
            def description(self):
                return "selects mode to code origin id 4/5/6/7: code level 4/5/6/7 3: code level 3 (xor 4 subgroups origin id together) 2: code level 2 1: code level 1 0: none xor origin id"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceHashingTableSelectedPage(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "CLAHbceHashingTableSelectedPage"
            
            def description(self):
                return "selects hashing table page to access looking up information buffer 1: page 1 0: page 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceTimeoutValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 4
        
            def name(self):
                return "CLAHbceTimeoutValue"
            
            def description(self):
                return "time out value for HBCE module."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAHbceCodingSelectedMode"] = _AF6CCI0021_RD_CLA._Classify_HBCE_Global_Control._CLAHbceCodingSelectedMode()
            allFields["CLAHbceHashingTableSelectedPage"] = _AF6CCI0021_RD_CLA._Classify_HBCE_Global_Control._CLAHbceHashingTableSelectedPage()
            allFields["CLAHbceTimeoutValue"] = _AF6CCI0021_RD_CLA._Classify_HBCE_Global_Control._CLAHbceTimeoutValue()
            return allFields

    class _Classify_HBCE_Hashing_Table_Control(AtRegister.AtRegister):
        def name(self):
            return "Classify HBCE Hashing Table Control"
    
        def description(self):
            return "HBCE module uses 10 bits for tabindex. tabindex is generated by hasding function applied to the origin id. The collisions due to hashing are handled by pointer to variable size blocks. Handling variable  size blocks requires a dynamic memory management scheme. The number of entries in each variable size block is defined by the number of rules that collide within a specific entry of the tabindex. Hashing function applies an XOR function to all bits of tabindex. The indexes to the tabindex are generated by hashing function and therefore collisions may occur. In order to resolve these collisions efficiently, HBCE define a complex data structure associated with each entry of the orgin id. Hashing table has two fields, flow number (flownum) (number of labelid mapped to this particular table entry) and memory start pointer (memsptr) (hold a pointer to the variable size block and the number of  rules that collide). In case, a table entry might be empty  which means that it is not mapped to any flow address rule, flownum = 0. Moreover, a table entry may be mapped to many flow address rules. In this case, where collisions occur, hashing table have to store a pointer to  the variable size block and the number of rules that collide. The number of colliding rules also indicates the size of the block. The formating of hashing table page0 in each case is shown as below."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x008800 + PageID*0x400 + HashID"
            
        def startAddress(self):
            return 0x00008800
            
        def endAddress(self):
            return 0x00008fff

        class _CLAHbceFlowNumber(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 11
        
            def name(self):
                return "CLAHbceFlowNumber"
            
            def description(self):
                return "flow number, 0: empty format 1: normal format others: collision format"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceMemoryStartPointer(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAHbceMemoryStartPointer"
            
            def description(self):
                return "memory start pointer Hash ID: PATERN HASH Label ID (20bit)  +  Lookup mode (2bit) + port (1bit) With Lookup Mode: 0/1/2 ~ PSN MEF/UDP/MPLS + NOXOR       :  Hashid  = Patern Hash [9:0], StoreID = Patern Hash [22:10] + ONEXOR     :  Hashid  = Patern Hash [9:0] XOR Patern Hash [19:10], StoreID = Patern Hash [22:10] + TWOXOR    :   Hashid  = Patern Hash [9:0] XOR Patern Hash [19:10] XOR"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAHbceFlowNumber"] = _AF6CCI0021_RD_CLA._Classify_HBCE_Hashing_Table_Control._CLAHbceFlowNumber()
            allFields["CLAHbceMemoryStartPointer"] = _AF6CCI0021_RD_CLA._Classify_HBCE_Hashing_Table_Control._CLAHbceMemoryStartPointer()
            return allFields

    class _Classify_HBCE_Looking_Up_Information_Control(AtRegister.AtRegister):
        def name(self):
            return "Classify HBCE Looking Up Information Control"
    
        def description(self):
            return "In HBCE module, all operations access this memory in order to examine whether an exact match exists or not. The Memory Pool implements dynamic memory management scheme and supports variable size blocks. It supports requests for allocation and deallocation of variable size blocks when inserting and deleting lookup address occur. Linking between multiple blocks is implemented by writing the address of the  next  block  in  the  previous block. In general, HBCE implementation is based on sequential accesses to memory pool and to the dynamically allocated collision nodes. The formating of memory pool word is shown as below."
            
        def width(self):
            return 34
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x009000 + CellID"
            
        def startAddress(self):
            return 0x00009000
            
        def endAddress(self):
            return 0x000097ff

        class _CLAHbceStoreID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAHbceStoreID"
            
            def description(self):
                return "To identify a certain lookup address rule within a particular table entry HBCE also need to save some additional information so as to be able to distinguish those that collide. But, HBCE don't need to save all bits and it can take advantage of the fact that the XOR function can be inversed, is defined as storeid."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceFlowEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "CLAHbceFlowEnb"
            
            def description(self):
                return "set active for this flow 0: is disabled, that mean this packet will be discarded 1: is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceFlowID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 14
        
            def name(self):
                return "CLAHbceFlowID"
            
            def description(self):
                return "a number identifying the output port of HBCE module"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceMemoryStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "CLAHbceMemoryStatus"
            
            def description(self):
                return "in normal format case, value of this bit is 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceGrpIDFlow(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "CLAHbceGrpIDFlow"
            
            def description(self):
                return "this configure a group ID that FlowID following"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceGrpWorking(AtRegister.AtRegisterField):
            def stopBit(self):
                return 32
                
            def startBit(self):
                return 32
        
            def name(self):
                return "CLAHbceGrpWorking"
            
            def description(self):
                return "this configure group working or protection"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHbceFlowDirect(AtRegister.AtRegisterField):
            def stopBit(self):
                return 33
                
            def startBit(self):
                return 33
        
            def name(self):
                return "CLAHbceFlowDirect"
            
            def description(self):
                return "this configure FlowID working in normal mode (not in any group)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAHbceStoreID"] = _AF6CCI0021_RD_CLA._Classify_HBCE_Looking_Up_Information_Control._CLAHbceStoreID()
            allFields["CLAHbceFlowEnb"] = _AF6CCI0021_RD_CLA._Classify_HBCE_Looking_Up_Information_Control._CLAHbceFlowEnb()
            allFields["CLAHbceFlowID"] = _AF6CCI0021_RD_CLA._Classify_HBCE_Looking_Up_Information_Control._CLAHbceFlowID()
            allFields["CLAHbceMemoryStatus"] = _AF6CCI0021_RD_CLA._Classify_HBCE_Looking_Up_Information_Control._CLAHbceMemoryStatus()
            allFields["CLAHbceGrpIDFlow"] = _AF6CCI0021_RD_CLA._Classify_HBCE_Looking_Up_Information_Control._CLAHbceGrpIDFlow()
            allFields["CLAHbceGrpWorking"] = _AF6CCI0021_RD_CLA._Classify_HBCE_Looking_Up_Information_Control._CLAHbceGrpWorking()
            allFields["CLAHbceFlowDirect"] = _AF6CCI0021_RD_CLA._Classify_HBCE_Looking_Up_Information_Control._CLAHbceFlowDirect()
            return allFields

    class _Classify_VLAN_TAG_Lookup_Control(AtRegister.AtRegister):
        def name(self):
            return "Classify VLAN TAG Lookup Control"
    
        def description(self):
            return "This register configures channel lookup to PWID"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00C000 + stmID*256 + channelID"
            
        def startAddress(self):
            return 0x0000c000
            
        def endAddress(self):
            return 0x0000c3ff

        class _RxEthPwLookupEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "RxEthPwLookupEn"
            
            def description(self):
                return "Enable look up to Pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthPwId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthPwId"
            
            def description(self):
                return "Pseudowire identification"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthPwLookupEn"] = _AF6CCI0021_RD_CLA._Classify_VLAN_TAG_Lookup_Control._RxEthPwLookupEn()
            allFields["RxEthPwId"] = _AF6CCI0021_RD_CLA._Classify_VLAN_TAG_Lookup_Control._RxEthPwId()
            return allFields

    class _cla_per_grp_enb(AtRegister.AtRegister):
        def name(self):
            return "Classify Per Group Enable Control"
    
        def description(self):
            return "This register configures Group that Flowid (or Pseudowire ID) is enable or not"
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0000A000 + Working_ID*0x100 + Grp_ID"
            
        def startAddress(self):
            return 0x0000a000
            
        def endAddress(self):
            return 0x0000a1ff

        class _CLAGrpPWEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAGrpPWEn"
            
            def description(self):
                return "This indicate the FlowID (or Pseudowire ID) is enable or not 1: FlowID enable 0: FlowID disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAGrpPWEn"] = _AF6CCI0021_RD_CLA._cla_per_grp_enb._CLAGrpPWEn()
            return allFields

    class _Classify_Pseudowire_length_checking_Control(AtRegister.AtRegister):
        def name(self):
            return "Classify Pseudowire length checking Control"
    
        def description(self):
            return "This register configures AF6FHW0013 pseudowire types identification."
            
        def width(self):
            return 14
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x009800 + PwId"
            
        def startAddress(self):
            return 0x00009800
            
        def endAddress(self):
            return 0x000098ff

        class _RxEthLengthValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthLengthValue"
            
            def description(self):
                return "Used for checking packet length for malform"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthLengthValue"] = _AF6CCI0021_RD_CLA._Classify_Pseudowire_length_checking_Control._RxEthLengthValue()
            return allFields

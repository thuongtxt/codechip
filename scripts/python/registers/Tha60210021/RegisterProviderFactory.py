import python.arrive.atsdk.AtRegister as AtRegister

class RegisterProviderFactory(AtRegister.AtRegisterProviderFactory):
    def _allRegisterProviders(self):
        allProviders = {}

        from _AF6CCI0021_RD_CDR import _AF6CCI0021_RD_CDR
        allProviders["_AF6CCI0021_RD_CDR"] = _AF6CCI0021_RD_CDR()

        from _AF6CCI0021_RD_CLA import _AF6CCI0021_RD_CLA
        allProviders["_AF6CCI0021_RD_CLA"] = _AF6CCI0021_RD_CLA()

        from _AF6CCI0021_RD_ETH import _AF6CCI0021_RD_ETH
        allProviders["_AF6CCI0021_RD_ETH"] = _AF6CCI0021_RD_ETH()

        from _AF6CCI0021_RD_GLB import _AF6CCI0021_RD_GLB
        allProviders["_AF6CCI0021_RD_GLB"] = _AF6CCI0021_RD_GLB()

        from _AF6CCI0021_RD_MAP import _AF6CCI0021_RD_MAP
        allProviders["_AF6CCI0021_RD_MAP"] = _AF6CCI0021_RD_MAP()

        from _AF6CCI0021_RD_PDA import _AF6CCI0021_RD_PDA
        allProviders["_AF6CCI0021_RD_PDA"] = _AF6CCI0021_RD_PDA()

        from _AF6CCI0021_RD_PDH import _AF6CCI0021_RD_PDH
        allProviders["_AF6CCI0021_RD_PDH"] = _AF6CCI0021_RD_PDH()

        from _AF6CCI0021_RD_PDH_LPC import _AF6CCI0021_RD_PDH_LPC
        allProviders["_AF6CCI0021_RD_PDH_LPC"] = _AF6CCI0021_RD_PDH_LPC()

        from _AF6CCI0021_RD_PLA import _AF6CCI0021_RD_PLA
        allProviders["_AF6CCI0021_RD_PLA"] = _AF6CCI0021_RD_PLA()

        from _AF6CCI0021_RD_PM import _AF6CCI0021_RD_PM
        allProviders["_AF6CCI0021_RD_PM"] = _AF6CCI0021_RD_PM()

        from _AF6CCI0021_RD_PMC import _AF6CCI0021_RD_PMC
        allProviders["_AF6CCI0021_RD_PMC"] = _AF6CCI0021_RD_PMC()

        from _AF6CCI0021_RD_PWE import _AF6CCI0021_RD_PWE
        allProviders["_AF6CCI0021_RD_PWE"] = _AF6CCI0021_RD_PWE()

        from _AF6CCI0021_RD_SSM import _AF6CCI0021_RD_SSM
        allProviders["_AF6CCI0021_RD_SSM"] = _AF6CCI0021_RD_SSM()

        from _AF6CCI0021_RD_XGE import _AF6CCI0021_RD_XGE
        allProviders["_AF6CCI0021_RD_XGE"] = _AF6CCI0021_RD_XGE()


        return allProviders

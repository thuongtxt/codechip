import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0021_RD_SSM(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["ssmbypass_pen"] = _AF6CCI0021_RD_SSM._ssmbypass_pen()
        allRegisters["ssmsta_pen"] = _AF6CCI0021_RD_SSM._ssmsta_pen()
        allRegisters["ssmsrc_pen"] = _AF6CCI0021_RD_SSM._ssmsrc_pen()
        allRegisters["txctrl_pen"] = _AF6CCI0021_RD_SSM._txctrl_pen()
        allRegisters["rxctrl_pen"] = _AF6CCI0021_RD_SSM._rxctrl_pen()
        allRegisters["stk0_upen"] = _AF6CCI0021_RD_SSM._stk0_upen()
        allRegisters["stk1_upen"] = _AF6CCI0021_RD_SSM._stk1_upen()
        allRegisters["cnt0_pen"] = _AF6CCI0021_RD_SSM._cnt0_pen()
        allRegisters["status_pen"] = _AF6CCI0021_RD_SSM._status_pen()
        allRegisters["rxfrm_crc_err_cnt"] = _AF6CCI0021_RD_SSM._rxfrm_crc_err_cnt()
        allRegisters["rxfrm_rei_cnt"] = _AF6CCI0021_RD_SSM._rxfrm_rei_cnt()
        allRegisters["rxfrm_fbe_cnt"] = _AF6CCI0021_RD_SSM._rxfrm_fbe_cnt()
        return allRegisters

    class _ssmbypass_pen(AtRegister.AtRegister):
        def name(self):
            return "SSM Engine Enable"
    
        def description(self):
            return "These registers are used to enable SSM engine"
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f4000
            
        def endAddress(self):
            return 0xffffffff

        class _SSMEngEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SSMEngEn"
            
            def description(self):
                return "SSM engine Enable - 1: SSM Engine Enable - 0: SSM Engine Bypass"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SSMEngEn"] = _AF6CCI0021_RD_SSM._ssmbypass_pen._SSMEngEn()
            return allFields

    class _ssmsta_pen(AtRegister.AtRegister):
        def name(self):
            return "SSM Engine Status"
    
        def description(self):
            return "These registers are used to enable SSM engine"
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f0200
            
        def endAddress(self):
            return 0xffffffff

        class _SSMEngSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SSMEngSta"
            
            def description(self):
                return "SSM engine Status - 1: SSM Engine Ready for sending - 0: SSM Engine Busy"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SSMEngSta"] = _AF6CCI0021_RD_SSM._ssmsta_pen._SSMEngSta()
            return allFields

    class _ssmsrc_pen(AtRegister.AtRegister):
        def name(self):
            return "Tx SSM Loopback"
    
        def description(self):
            return "These registers are used to select source SSM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000315
            
        def endAddress(self):
            return 0xffffffff

        class _Src0raifrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Src0raifrc"
            
            def description(self):
                return "Source 0 force AIS"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Src0aisfrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Src0aisfrc"
            
            def description(self):
                return "Source 0 force AIS"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Src1TiFrTXPdh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Src1TiFrTXPdh"
            
            def description(self):
                return "Source 1 timing from TX PDH"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Src1TiRXLiu(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "Src1TiRXLiu"
            
            def description(self):
                return "Source 1 timing from RX LIU"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Src0Mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Src0Mode"
            
            def description(self):
                return "Source 0 Mode E1/T1 in case Loop RX LIU mode 1: T1 0: E1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Src0FrRXLiu(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Src0FrRXLiu"
            
            def description(self):
                return "Source 0 Data from RX LIU"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Src0raifrc"] = _AF6CCI0021_RD_SSM._ssmsrc_pen._Src0raifrc()
            allFields["Src0aisfrc"] = _AF6CCI0021_RD_SSM._ssmsrc_pen._Src0aisfrc()
            allFields["Src1TiFrTXPdh"] = _AF6CCI0021_RD_SSM._ssmsrc_pen._Src1TiFrTXPdh()
            allFields["Src1TiRXLiu"] = _AF6CCI0021_RD_SSM._ssmsrc_pen._Src1TiRXLiu()
            allFields["Src0Mode"] = _AF6CCI0021_RD_SSM._ssmsrc_pen._Src0Mode()
            allFields["Src0FrRXLiu"] = _AF6CCI0021_RD_SSM._ssmsrc_pen._Src0FrRXLiu()
            return allFields

    class _txctrl_pen(AtRegister.AtRegister):
        def name(self):
            return "Tx SSM Framer Control"
    
        def description(self):
            return "This is used to configure the SSM transmitter"
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000F0000 + LiuID"
            
        def startAddress(self):
            return 0x000f0000
            
        def endAddress(self):
            return 0x000f002f

        class _Raifardis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 38
                
            def startBit(self):
                return 38
        
            def name(self):
                return "Raifardis"
            
            def description(self):
                return "RAI far-end disable 1: Disable forwading RAI 0: Enable forwading RAI"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Raineadis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 37
                
            def startBit(self):
                return 37
        
            def name(self):
                return "Raineadis"
            
            def description(self):
                return "RAI near-end disable 1: Disable forwading RAI 0: Enable forwading RAI"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SiBypass(AtRegister.AtRegisterField):
            def stopBit(self):
                return 36
                
            def startBit(self):
                return 36
        
            def name(self):
                return "SiBypass"
            
            def description(self):
                return "Si CRC4 Bypass Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SaBypass(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 35
        
            def name(self):
                return "SaBypass"
            
            def description(self):
                return "Sa4-Sa8 Bypass Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _E2Bypass(AtRegister.AtRegisterField):
            def stopBit(self):
                return 34
                
            def startBit(self):
                return 34
        
            def name(self):
                return "E2Bypass"
            
            def description(self):
                return "E2 of CRC4 Bypass Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _E1Bypass(AtRegister.AtRegisterField):
            def stopBit(self):
                return 33
                
            def startBit(self):
                return 33
        
            def name(self):
                return "E1Bypass"
            
            def description(self):
                return "E1 of CRC4 Bypass Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _E2ErrIns(AtRegister.AtRegisterField):
            def stopBit(self):
                return 32
                
            def startBit(self):
                return 32
        
            def name(self):
                return "E2ErrIns"
            
            def description(self):
                return "E2 Error Insert Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _E1ErrIns(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "E1ErrIns"
            
            def description(self):
                return "E1 Error Insert Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SiValCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "SiValCfg"
            
            def description(self):
                return "Si Value default"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SaValCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 25
        
            def name(self):
                return "SaValCfg"
            
            def description(self):
                return "Sa4-Sa8 Value default"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SSMEna(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "SSMEna"
            
            def description(self):
                return "SSM enable 1: Enable SSM 0: Disable SSM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SSMCount(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "SSMCount"
            
            def description(self):
                return "SSM Message Counter. These bits are used to store the amount of repetitions the Transmit SSM message will be sent - 0 : the transmit BOC will be set continuously - other: the amount of repetitions the Transmit SSM message will be sent before an all ones"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SSMMess(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 10
        
            def name(self):
                return "SSMMess"
            
            def description(self):
                return "SSM Message. These bits are used to store the SSM message to be transmitted out the National bits or Si International bit or FDL - E1: [13:10] for SSM Message - T1: [15:10] for BOC code"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SSMSasel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 5
        
            def name(self):
                return "SSMSasel"
            
            def description(self):
                return "Only one of the five Sa bits can be chosen for SSM transmission at a time. Don't care in T1 case - Bit9: for Sa8 - Bit8: for Sa7 - Bit7: for Sa6 - Bit6: for Sa5 - Bit5: for Sa4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SSMSiSaSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "SSMSiSaSel"
            
            def description(self):
                return "Select Si or Sa to transmit SSM. Don't care in T1 case - 0: Sa National Bits (Only one of the five Sa bits can be chosen for SSM transmission at a time) - 1: Si International Bits"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1Md(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE1Md"
            
            def description(self):
                return "Transmit DS1/E1 framing mode - 0000: DS1 Unframe (For bypass TX Framer of PDH) - 0001: DS1 SF (D4) - 0010: DS1 ESF - 1000: E1 Unframe (For bypass TX Framer of PDH) - 1001: E1 Basic Frame - 1010: E1 CRC4 Frame - Other: Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Raifardis"] = _AF6CCI0021_RD_SSM._txctrl_pen._Raifardis()
            allFields["Raineadis"] = _AF6CCI0021_RD_SSM._txctrl_pen._Raineadis()
            allFields["SiBypass"] = _AF6CCI0021_RD_SSM._txctrl_pen._SiBypass()
            allFields["SaBypass"] = _AF6CCI0021_RD_SSM._txctrl_pen._SaBypass()
            allFields["E2Bypass"] = _AF6CCI0021_RD_SSM._txctrl_pen._E2Bypass()
            allFields["E1Bypass"] = _AF6CCI0021_RD_SSM._txctrl_pen._E1Bypass()
            allFields["E2ErrIns"] = _AF6CCI0021_RD_SSM._txctrl_pen._E2ErrIns()
            allFields["E1ErrIns"] = _AF6CCI0021_RD_SSM._txctrl_pen._E1ErrIns()
            allFields["SiValCfg"] = _AF6CCI0021_RD_SSM._txctrl_pen._SiValCfg()
            allFields["SaValCfg"] = _AF6CCI0021_RD_SSM._txctrl_pen._SaValCfg()
            allFields["SSMEna"] = _AF6CCI0021_RD_SSM._txctrl_pen._SSMEna()
            allFields["SSMCount"] = _AF6CCI0021_RD_SSM._txctrl_pen._SSMCount()
            allFields["SSMMess"] = _AF6CCI0021_RD_SSM._txctrl_pen._SSMMess()
            allFields["SSMSasel"] = _AF6CCI0021_RD_SSM._txctrl_pen._SSMSasel()
            allFields["SSMSiSaSel"] = _AF6CCI0021_RD_SSM._txctrl_pen._SSMSiSaSel()
            allFields["TxDE1Md"] = _AF6CCI0021_RD_SSM._txctrl_pen._TxDE1Md()
            return allFields

    class _rxctrl_pen(AtRegister.AtRegister):
        def name(self):
            return "Rx SSM Framer Control"
    
        def description(self):
            return "This is used to configure the SSM transmitter"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000FC000 + LiuID"
            
        def startAddress(self):
            return 0x000fc000
            
        def endAddress(self):
            return 0x000fc02f

        class _RxDE1CRCcheck(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "RxDE1CRCcheck"
            
            def description(self):
                return "Set 1 to check CRC6"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1LofThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 6
        
            def name(self):
                return "RxDE1LofThres"
            
            def description(self):
                return "Threshold for declare LOF"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1FLofmod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RxDE1FLofmod"
            
            def description(self):
                return "Select mode Slide Window or Continous to declare LOF - 1: Slide Window detection (window 8, threshold RxDE1LofThres) - 0: Continous detection"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1FrcLof(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxDE1FrcLof"
            
            def description(self):
                return "Set 1 to force Re-frame (default 0)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1Md(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1Md"
            
            def description(self):
                return "Receive DS1/E1 framing mode - 0000: DS1 Unframe (For bypass TX Framer of PDH) - 0001: DS1 SF (D4) - 0010: DS1 ESF - 1000: E1 Unframe (For bypass TX Framer of PDH) - 1001: E1 Basic Frame - 1010: E1 CRC4 Frame - Other: Unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1CRCcheck"] = _AF6CCI0021_RD_SSM._rxctrl_pen._RxDE1CRCcheck()
            allFields["RxDE1LofThres"] = _AF6CCI0021_RD_SSM._rxctrl_pen._RxDE1LofThres()
            allFields["RxDE1FLofmod"] = _AF6CCI0021_RD_SSM._rxctrl_pen._RxDE1FLofmod()
            allFields["RxDE1FrcLof"] = _AF6CCI0021_RD_SSM._rxctrl_pen._RxDE1FrcLof()
            allFields["RxDE1Md"] = _AF6CCI0021_RD_SSM._rxctrl_pen._RxDE1Md()
            return allFields

    class _stk0_upen(AtRegister.AtRegister):
        def name(self):
            return "SSM Slip Buffer Data Sticky"
    
        def description(self):
            return "These registers are used to"
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f4001
            
        def endAddress(self):
            return 0xffffffff

        class _SSMSlipDat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SSMSlipDat"
            
            def description(self):
                return "SSM Slip Buffer for Data - 1: SSM Data Slip Enable - 0: SSM Data Slip Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SSMSlipDat"] = _AF6CCI0021_RD_SSM._stk0_upen._SSMSlipDat()
            return allFields

    class _stk1_upen(AtRegister.AtRegister):
        def name(self):
            return "SSM Slip Buffer Frame Sticky"
    
        def description(self):
            return "These registers are used to"
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000f4002
            
        def endAddress(self):
            return 0xffffffff

        class _SSMSlipFrm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SSMSlipFrm"
            
            def description(self):
                return "SSM Slip Buffer for Frame - 1: SSM Frame Slip Enable - 0: SSM Frame Slip Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SSMSlipFrm"] = _AF6CCI0021_RD_SSM._stk1_upen._SSMSlipFrm()
            return allFields

    class _cnt0_pen(AtRegister.AtRegister):
        def name(self):
            return "SSM Slip Buffer Frame Sticky"
    
        def description(self):
            return "This is the per channel CRC error counter for DS1/E1/J1 receive framer"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000F4100 + 64*Rd2Clr + LiuID"
            
        def startAddress(self):
            return 0x000f4100
            
        def endAddress(self):
            return 0x000f412f

        class _SlipCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SlipCnt"
            
            def description(self):
                return "Slip Buffer Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SlipCnt"] = _AF6CCI0021_RD_SSM._cnt0_pen._SlipCnt()
            return allFields

    class _status_pen(AtRegister.AtRegister):
        def name(self):
            return "Rx SSM Framer HW Status"
    
        def description(self):
            return "These registers are used for Hardware status only"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000FC400 + liuid"
            
        def startAddress(self):
            return 0x000fc400
            
        def endAddress(self):
            return 0x000fc02f

        class _RxDE1Sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1Sta"
            
            def description(self):
                return "RX Framer Status - 5: In Frame - 0: LOF - Other: Searching State"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1Sta"] = _AF6CCI0021_RD_SSM._status_pen._RxDE1Sta()
            return allFields

    class _rxfrm_crc_err_cnt(AtRegister.AtRegister):
        def name(self):
            return "Rx SSM Framer CRC Error Counter"
    
        def description(self):
            return "This is the per channel CRC error counter for DS1/E1/J1 receive framer"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000FE800 + 64*Rd2Clr + LiuID"
            
        def startAddress(self):
            return 0x000fe800
            
        def endAddress(self):
            return 0x000fe87f

        class _DE1CrcErrCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE1CrcErrCnt"
            
            def description(self):
                return "CRC Error Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE1CrcErrCnt"] = _AF6CCI0021_RD_SSM._rxfrm_crc_err_cnt._DE1CrcErrCnt()
            return allFields

    class _rxfrm_rei_cnt(AtRegister.AtRegister):
        def name(self):
            return "Rx SSM Framer REI Counter"
    
        def description(self):
            return "This is the per channel REI counter for DS1/E1/J1 receive framer"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000FF000 + 64*Rd2Clr + LiuID"
            
        def startAddress(self):
            return 0x000ff000
            
        def endAddress(self):
            return 0x000ff07f

        class _DE1REIErrCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE1REIErrCnt"
            
            def description(self):
                return "REI Error Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE1REIErrCnt"] = _AF6CCI0021_RD_SSM._rxfrm_rei_cnt._DE1REIErrCnt()
            return allFields

    class _rxfrm_fbe_cnt(AtRegister.AtRegister):
        def name(self):
            return "Rx SSM Framer FBE Counter"
    
        def description(self):
            return "This is the per channel REI counter for DS1/E1/J1 receive framer"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000FE000 + 64*Rd2Clr + LiuID"
            
        def startAddress(self):
            return 0x000fe000
            
        def endAddress(self):
            return 0x000fe07f

        class _DE1FBEErrCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE1FBEErrCnt"
            
            def description(self):
                return "CRC Error Counter"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE1FBEErrCnt"] = _AF6CCI0021_RD_SSM._rxfrm_fbe_cnt._DE1FBEErrCnt()
            return allFields

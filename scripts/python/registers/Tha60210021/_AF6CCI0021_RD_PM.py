import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0021_RD_PM(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["FMPM_Block_Version"] = _AF6CCI0021_RD_PM._FMPM_Block_Version()
        allRegisters["FMPM_Read_DDR_Control"] = _AF6CCI0021_RD_PM._FMPM_Read_DDR_Control()
        allRegisters["FMPM_Change_Page_DDR"] = _AF6CCI0021_RD_PM._FMPM_Change_Page_DDR()
        allRegisters["FMPM_Pariry_Force"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Force()
        allRegisters["FMPM_Pariry_Disable"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Disable()
        allRegisters["FMPM_Pariry_Sticky"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Sticky()
        allRegisters["FMPM_Force_Reset_Core"] = _AF6CCI0021_RD_PM._FMPM_Force_Reset_Core()
        allRegisters["FMPM_Config_Number_Clock_Of_125us"] = _AF6CCI0021_RD_PM._FMPM_Config_Number_Clock_Of_125us()
        allRegisters["FMPM_Syn_Get_time"] = _AF6CCI0021_RD_PM._FMPM_Syn_Get_time()
        allRegisters["FMPM_Time_Value"] = _AF6CCI0021_RD_PM._FMPM_Time_Value()
        allRegisters["FMPM_Hold00_Data_Of_Read_DDR"] = _AF6CCI0021_RD_PM._FMPM_Hold00_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold01_Data_Of_Read_DDR"] = _AF6CCI0021_RD_PM._FMPM_Hold01_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold02_Data_Of_Read_DDR"] = _AF6CCI0021_RD_PM._FMPM_Hold02_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold03_Data_Of_Read_DDR"] = _AF6CCI0021_RD_PM._FMPM_Hold03_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold04_Data_Of_Read_DDR"] = _AF6CCI0021_RD_PM._FMPM_Hold04_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold05_Data_Of_Read_DDR"] = _AF6CCI0021_RD_PM._FMPM_Hold05_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold06_Data_Of_Read_DDR"] = _AF6CCI0021_RD_PM._FMPM_Hold06_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold07_Data_Of_Read_DDR"] = _AF6CCI0021_RD_PM._FMPM_Hold07_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold08_Data_Of_Read_DDR"] = _AF6CCI0021_RD_PM._FMPM_Hold08_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold09_Data_Of_Read_DDR"] = _AF6CCI0021_RD_PM._FMPM_Hold09_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold10_Data_Of_Read_DDR"] = _AF6CCI0021_RD_PM._FMPM_Hold10_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold11_Data_Of_Read_DDR"] = _AF6CCI0021_RD_PM._FMPM_Hold11_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold12_Data_Of_Read_DDR"] = _AF6CCI0021_RD_PM._FMPM_Hold12_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold13_Data_Of_Read_DDR"] = _AF6CCI0021_RD_PM._FMPM_Hold13_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold14_Data_Of_Read_DDR"] = _AF6CCI0021_RD_PM._FMPM_Hold14_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold15_Data_Of_Read_DDR"] = _AF6CCI0021_RD_PM._FMPM_Hold15_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold16_Data_Of_Read_DDR"] = _AF6CCI0021_RD_PM._FMPM_Hold16_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold17_Data_Of_Read_DDR"] = _AF6CCI0021_RD_PM._FMPM_Hold17_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold18_Data_Of_Read_DDR"] = _AF6CCI0021_RD_PM._FMPM_Hold18_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold19_Data_Of_Read_DDR"] = _AF6CCI0021_RD_PM._FMPM_Hold19_Data_Of_Read_DDR()
        allRegisters["FMPM_Hold20_Data_Of_Read_DDR"] = _AF6CCI0021_RD_PM._FMPM_Hold20_Data_Of_Read_DDR()
        allRegisters["FMPM_TCA_Threshold"] = _AF6CCI0021_RD_PM._FMPM_TCA_Threshold()
        allRegisters["FMPM_K_Threshold_of_DS1"] = _AF6CCI0021_RD_PM._FMPM_K_Threshold_of_DS1()
        allRegisters["FMPM_K_Threshold_of_PW"] = _AF6CCI0021_RD_PM._FMPM_K_Threshold_of_PW()
        allRegisters["FMPM_Threshold_Type_of_DS1"] = _AF6CCI0021_RD_PM._FMPM_Threshold_Type_of_DS1()
        allRegisters["FMPM_Threshold_Type_of_PW"] = _AF6CCI0021_RD_PM._FMPM_Threshold_Type_of_PW()
        allRegisters["FMPM_FM_Interrupt"] = _AF6CCI0021_RD_PM._FMPM_FM_Interrupt()
        allRegisters["FMPM_FM_Interrupt_Mask"] = _AF6CCI0021_RD_PM._FMPM_FM_Interrupt_Mask()
        allRegisters["FMPM_FM_DS1E1_Group_Interrupt_OR"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Group_Interrupt_OR()
        allRegisters["FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1()
        allRegisters["FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1()
        allRegisters["FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1()
        allRegisters["FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1()
        allRegisters["FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1()
        allRegisters["FMPM_FM_PW_Group_Interrupt_OR"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Group_Interrupt_OR()
        allRegisters["FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW()
        allRegisters["FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW()
        allRegisters["FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW()
        allRegisters["FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW()
        allRegisters["FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW()
        allRegisters["FMPM_PW_Def_Interrupt"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt()
        allRegisters["FMPM_PW_Def_Interrupt_Mask"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Mask()
        allRegisters["FMPM_PW_Def_Level1_Group_Interrupt_OR"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Level1_Group_Interrupt_OR()
        allRegisters["FMPM_PW_Def_Framer_Interrupt_OR_AND_MASK_Per_PW"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Framer_Interrupt_OR_AND_MASK_Per_PW()
        allRegisters["FMPM_PW_Def_Framer_Interrupt_MASK_Per_PW"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Framer_Interrupt_MASK_Per_PW()
        allRegisters["FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW()
        allRegisters["FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW()
        allRegisters["FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW()
        return allRegisters

    class _FMPM_Block_Version(AtRegister.AtRegister):
        def name(self):
            return "FMPM Block Version"
    
        def description(self):
            return "PM Block Version"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _day(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "day"
            
            def description(self):
                return "day, hexdecimal format"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _month(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "month"
            
            def description(self):
                return "month, hexdecimal format"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _year(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "year"
            
            def description(self):
                return "year, hexdecimal format"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _project(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "project"
            
            def description(self):
                return "Project ID, hexdecimal format"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _number(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "number"
            
            def description(self):
                return "number of synthesis FPGA each day, hexdecimal format"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["day"] = _AF6CCI0021_RD_PM._FMPM_Block_Version._day()
            allFields["month"] = _AF6CCI0021_RD_PM._FMPM_Block_Version._month()
            allFields["year"] = _AF6CCI0021_RD_PM._FMPM_Block_Version._year()
            allFields["project"] = _AF6CCI0021_RD_PM._FMPM_Block_Version._project()
            allFields["number"] = _AF6CCI0021_RD_PM._FMPM_Block_Version._number()
            return allFields

    class _FMPM_Read_DDR_Control(AtRegister.AtRegister):
        def name(self):
            return "FMPM Read DDR Control"
    
        def description(self):
            return "Read Parameters or Statistic counters of DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _Done(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "Done"
            
            def description(self):
                return "Access DDR Is Done"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _Start(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "Start"
            
            def description(self):
                return "Trigger 0->1 to start access DDR"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Page(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "Page"
            
            def description(self):
                return "Page access"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _R2C(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "R2C"
            
            def description(self):
                return "Read mode"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Type(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Type"
            
            def description(self):
                return "Counter Type"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ADR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ADR"
            
            def description(self):
                return "Address of Counter"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Done"] = _AF6CCI0021_RD_PM._FMPM_Read_DDR_Control._Done()
            allFields["Start"] = _AF6CCI0021_RD_PM._FMPM_Read_DDR_Control._Start()
            allFields["Page"] = _AF6CCI0021_RD_PM._FMPM_Read_DDR_Control._Page()
            allFields["R2C"] = _AF6CCI0021_RD_PM._FMPM_Read_DDR_Control._R2C()
            allFields["Type"] = _AF6CCI0021_RD_PM._FMPM_Read_DDR_Control._Type()
            allFields["ADR"] = _AF6CCI0021_RD_PM._FMPM_Read_DDR_Control._ADR()
            return allFields

    class _FMPM_Change_Page_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Change Page DDR"
    
        def description(self):
            return "Change Page for DDR when SW want to get Current Period Parameters"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _CHG_Page_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "CHG_Page_enb"
            
            def description(self):
                return "Enable change page"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _HW_Page(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "HW_Page"
            
            def description(self):
                return "Get Current Page of DDR"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _CHG_DONE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "CHG_DONE"
            
            def description(self):
                return "Change page is done"
            
            def type(self):
                return "R/W/C"
            
            def resetValue(self):
                return 0xffffffff

        class _CHG_START(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CHG_START"
            
            def description(self):
                return "Trigger 0->1 to request change page"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CHG_Page_enb"] = _AF6CCI0021_RD_PM._FMPM_Change_Page_DDR._CHG_Page_enb()
            allFields["HW_Page"] = _AF6CCI0021_RD_PM._FMPM_Change_Page_DDR._HW_Page()
            allFields["CHG_DONE"] = _AF6CCI0021_RD_PM._FMPM_Change_Page_DDR._CHG_DONE()
            allFields["CHG_START"] = _AF6CCI0021_RD_PM._FMPM_Change_Page_DDR._CHG_START()
            return allFields

    class _FMPM_Pariry_Force(AtRegister.AtRegister):
        def name(self):
            return "FMPM Parity Force"
    
        def description(self):
            return "Force parity for RAM configuration"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000060
            
        def endAddress(self):
            return 0xffffffff

        class _Par_For_pw_def_msk_chn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Par_For_pw_def_msk_chn"
            
            def description(self):
                return "FMPM PW Def Framer Interrupt MASK Per PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_pw_def_msk_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Par_For_pw_def_msk_typ"
            
            def description(self):
                return "FMPM PW Def Interrupt MASK Per Type Per PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_ds1_msk_chn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "Par_For_ds1_msk_chn"
            
            def description(self):
                return "FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_ds1_msk_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Par_For_ds1_msk_typ"
            
            def description(self):
                return "FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_pw_msk_chn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "Par_For_pw_msk_chn"
            
            def description(self):
                return "FMPM FM PW Framer Interrupt MASK Per PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_pw_msk_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Par_For_pw_msk_typ"
            
            def description(self):
                return "FMPM FM PW Interrupt MASK Per Type Per PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_ds1_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Par_For_config_ds1_typ"
            
            def description(self):
                return "FMPM Threshold Type of DS1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_pw_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Par_For_config_pw_typ"
            
            def description(self):
                return "FMPM Threshold Type of PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_ds1_k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Par_For_config_ds1_k"
            
            def description(self):
                return "FMPM K Threshold of DS1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_pw_k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Par_For_config_pw_k"
            
            def description(self):
                return "FMPM K Threshold of PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Par_For_pw_def_msk_chn"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Force._Par_For_pw_def_msk_chn()
            allFields["Par_For_pw_def_msk_typ"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Force._Par_For_pw_def_msk_typ()
            allFields["Par_For_ds1_msk_chn"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Force._Par_For_ds1_msk_chn()
            allFields["Par_For_ds1_msk_typ"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Force._Par_For_ds1_msk_typ()
            allFields["Par_For_pw_msk_chn"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Force._Par_For_pw_msk_chn()
            allFields["Par_For_pw_msk_typ"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Force._Par_For_pw_msk_typ()
            allFields["Par_For_config_ds1_typ"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Force._Par_For_config_ds1_typ()
            allFields["Par_For_config_pw_typ"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Force._Par_For_config_pw_typ()
            allFields["Par_For_config_ds1_k"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Force._Par_For_config_ds1_k()
            allFields["Par_For_config_pw_k"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Force._Par_For_config_pw_k()
            return allFields

    class _FMPM_Pariry_Disable(AtRegister.AtRegister):
        def name(self):
            return "FMPM Parity Disable"
    
        def description(self):
            return "Force parity for RAM configuration"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000061
            
        def endAddress(self):
            return 0xffffffff

        class _Par_For_pw_def_msk_chn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Par_For_pw_def_msk_chn"
            
            def description(self):
                return "FMPM PW Def Framer Interrupt MASK Per PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_pw_def_msk_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Par_For_pw_def_msk_typ"
            
            def description(self):
                return "FMPM PW Def Interrupt MASK Per Type Per PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_ds1_msk_chn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "Par_For_ds1_msk_chn"
            
            def description(self):
                return "FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_ds1_msk_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Par_For_ds1_msk_typ"
            
            def description(self):
                return "FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_pw_msk_chn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "Par_For_pw_msk_chn"
            
            def description(self):
                return "FMPM FM PW Framer Interrupt MASK Per PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_pw_msk_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Par_For_pw_msk_typ"
            
            def description(self):
                return "FMPM FM PW Interrupt MASK Per Type Per PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_ds1_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Par_For_config_ds1_typ"
            
            def description(self):
                return "FMPM Threshold Type of DS1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_pw_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Par_For_config_pw_typ"
            
            def description(self):
                return "FMPM Threshold Type of PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_ds1_k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Par_For_config_ds1_k"
            
            def description(self):
                return "FMPM K Threshold of DS1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_For_config_pw_k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Par_For_config_pw_k"
            
            def description(self):
                return "FMPM K Threshold of PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Par_For_pw_def_msk_chn"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Disable._Par_For_pw_def_msk_chn()
            allFields["Par_For_pw_def_msk_typ"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Disable._Par_For_pw_def_msk_typ()
            allFields["Par_For_ds1_msk_chn"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Disable._Par_For_ds1_msk_chn()
            allFields["Par_For_ds1_msk_typ"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Disable._Par_For_ds1_msk_typ()
            allFields["Par_For_pw_msk_chn"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Disable._Par_For_pw_msk_chn()
            allFields["Par_For_pw_msk_typ"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Disable._Par_For_pw_msk_typ()
            allFields["Par_For_config_ds1_typ"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Disable._Par_For_config_ds1_typ()
            allFields["Par_For_config_pw_typ"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Disable._Par_For_config_pw_typ()
            allFields["Par_For_config_ds1_k"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Disable._Par_For_config_ds1_k()
            allFields["Par_For_config_pw_k"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Disable._Par_For_config_pw_k()
            return allFields

    class _FMPM_Pariry_Sticky(AtRegister.AtRegister):
        def name(self):
            return "FMPM Parity Sticky"
    
        def description(self):
            return "Change Page for DDR when SW want to get Current Period Parameters"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000062
            
        def endAddress(self):
            return 0xffffffff

        class _Par_Stk_pw_def_msk_chn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "Par_Stk_pw_def_msk_chn"
            
            def description(self):
                return "FMPM PW Def Framer Interrupt MASK Per PW"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_pw_def_msk_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Par_Stk_pw_def_msk_typ"
            
            def description(self):
                return "FMPM PW Def Interrupt MASK Per Type Per PW"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_ds1_msk_chn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "Par_Stk_ds1_msk_chn"
            
            def description(self):
                return "FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_ds1_msk_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "Par_Stk_ds1_msk_typ"
            
            def description(self):
                return "FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_pw_msk_chn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "Par_Stk_pw_msk_chn"
            
            def description(self):
                return "FMPM FM PW Framer Interrupt MASK Per PW"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_pw_msk_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "Par_Stk_pw_msk_typ"
            
            def description(self):
                return "FMPM FM PW Interrupt MASK Per Type Per PW"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_config_ds1_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "Par_Stk_config_ds1_typ"
            
            def description(self):
                return "FMPM Threshold Type of DS1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_config_pw_typ(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "Par_Stk_config_pw_typ"
            
            def description(self):
                return "FMPM Threshold Type of PW"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_config_ds1_k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "Par_Stk_config_ds1_k"
            
            def description(self):
                return "FMPM K Threshold of DS1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _Par_Stk_config_pw_k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Par_Stk_config_pw_k"
            
            def description(self):
                return "FMPM K Threshold of PW"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Par_Stk_pw_def_msk_chn"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_pw_def_msk_chn()
            allFields["Par_Stk_pw_def_msk_typ"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_pw_def_msk_typ()
            allFields["Par_Stk_ds1_msk_chn"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_ds1_msk_chn()
            allFields["Par_Stk_ds1_msk_typ"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_ds1_msk_typ()
            allFields["Par_Stk_pw_msk_chn"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_pw_msk_chn()
            allFields["Par_Stk_pw_msk_typ"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_pw_msk_typ()
            allFields["Par_Stk_config_ds1_typ"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_config_ds1_typ()
            allFields["Par_Stk_config_pw_typ"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_config_pw_typ()
            allFields["Par_Stk_config_ds1_k"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_config_ds1_k()
            allFields["Par_Stk_config_pw_k"] = _AF6CCI0021_RD_PM._FMPM_Pariry_Sticky._Par_Stk_config_pw_k()
            return allFields

    class _FMPM_Force_Reset_Core(AtRegister.AtRegister):
        def name(self):
            return "FMPM Force Reset Core"
    
        def description(self):
            return "Force Reset PM Core"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000005
            
        def endAddress(self):
            return 0xffffffff

        class _param_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "param_enb"
            
            def description(self):
                return "Enable Parameter counters"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lopwcnt_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "lopwcnt_enb"
            
            def description(self):
                return "Enable PW counters"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _drst_enb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "drst_enb"
            
            def description(self):
                return "Enable PM block, SW has to do reset PM block when this bit is transition from 0/1 to 1/0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _drdy_chg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "drdy_chg"
            
            def description(self):
                return "Ready signal has changed 0/1"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _drst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "drst"
            
            def description(self):
                return "PM Core reset done"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _frst(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "frst"
            
            def description(self):
                return "Trigger 0->1 to reset PM Core"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["param_enb"] = _AF6CCI0021_RD_PM._FMPM_Force_Reset_Core._param_enb()
            allFields["lopwcnt_enb"] = _AF6CCI0021_RD_PM._FMPM_Force_Reset_Core._lopwcnt_enb()
            allFields["drst_enb"] = _AF6CCI0021_RD_PM._FMPM_Force_Reset_Core._drst_enb()
            allFields["drdy_chg"] = _AF6CCI0021_RD_PM._FMPM_Force_Reset_Core._drdy_chg()
            allFields["drst"] = _AF6CCI0021_RD_PM._FMPM_Force_Reset_Core._drst()
            allFields["frst"] = _AF6CCI0021_RD_PM._FMPM_Force_Reset_Core._frst()
            return allFields

    class _FMPM_Config_Number_Clock_Of_125us(AtRegister.AtRegister):
        def name(self):
            return "FMPM Config Number Clock Of 125us"
    
        def description(self):
            return "Configure number clock of 125us. This register is used by HW only."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000007
            
        def endAddress(self):
            return 0xffffffff

        class _config125us(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "config125us"
            
            def description(self):
                return "Number of clocks for one 125us unit. N(mhz) is operation clock of PM block then the configuration value is (N*125-2)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["config125us"] = _AF6CCI0021_RD_PM._FMPM_Config_Number_Clock_Of_125us._config125us()
            return allFields

    class _FMPM_Syn_Get_time(AtRegister.AtRegister):
        def name(self):
            return "FMPM Syn Get Time"
    
        def description(self):
            return "SW request HW synchronize timing."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000008
            
        def endAddress(self):
            return 0xffffffff

        class _syntime(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "syntime"
            
            def description(self):
                return "Trigger 0->1 to synchronize timing"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _gettime(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "gettime"
            
            def description(self):
                return "Trigger 0->1 to get time value"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _numclk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "numclk"
            
            def description(self):
                return "number of clocks for one 125us unit"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["syntime"] = _AF6CCI0021_RD_PM._FMPM_Syn_Get_time._syntime()
            allFields["gettime"] = _AF6CCI0021_RD_PM._FMPM_Syn_Get_time._gettime()
            allFields["numclk"] = _AF6CCI0021_RD_PM._FMPM_Syn_Get_time._numclk()
            return allFields

    class _FMPM_Time_Value(AtRegister.AtRegister):
        def name(self):
            return "FMPM Time Value"
    
        def description(self):
            return "Time value after SW request to get time"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000009
            
        def endAddress(self):
            return 0xffffffff

        class _cnt1s(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 16
        
            def name(self):
                return "cnt1s"
            
            def description(self):
                return "counter 1s"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _cnt1ms(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 4
        
            def name(self):
                return "cnt1ms"
            
            def description(self):
                return "counter 1ms"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _cnt125us(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt125us"
            
            def description(self):
                return "counter 125us"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt1s"] = _AF6CCI0021_RD_PM._FMPM_Time_Value._cnt1s()
            allFields["cnt1ms"] = _AF6CCI0021_RD_PM._FMPM_Time_Value._cnt1ms()
            allFields["cnt125us"] = _AF6CCI0021_RD_PM._FMPM_Time_Value._cnt125us()
            return allFields

    class _FMPM_Hold00_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold00 Data Of Read DDR"
    
        def description(self):
            return "Hold00 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000020
            
        def endAddress(self):
            return 0xffffffff

        class _Hold00(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold00"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold00"] = _AF6CCI0021_RD_PM._FMPM_Hold00_Data_Of_Read_DDR._Hold00()
            return allFields

    class _FMPM_Hold01_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold01 Data Of Read DDR"
    
        def description(self):
            return "Hold01 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000021
            
        def endAddress(self):
            return 0xffffffff

        class _Hold01(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold01"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold01"] = _AF6CCI0021_RD_PM._FMPM_Hold01_Data_Of_Read_DDR._Hold01()
            return allFields

    class _FMPM_Hold02_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold02 Data Of Read DDR"
    
        def description(self):
            return "Hold02 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000022
            
        def endAddress(self):
            return 0xffffffff

        class _Hold02(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold02"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold02"] = _AF6CCI0021_RD_PM._FMPM_Hold02_Data_Of_Read_DDR._Hold02()
            return allFields

    class _FMPM_Hold03_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold03 Data Of Read DDR"
    
        def description(self):
            return "Hold03 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000023
            
        def endAddress(self):
            return 0xffffffff

        class _Hold03(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold03"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold03"] = _AF6CCI0021_RD_PM._FMPM_Hold03_Data_Of_Read_DDR._Hold03()
            return allFields

    class _FMPM_Hold04_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold04 Data Of Read DDR"
    
        def description(self):
            return "Hold04 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000024
            
        def endAddress(self):
            return 0xffffffff

        class _Hold04(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold04"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold04"] = _AF6CCI0021_RD_PM._FMPM_Hold04_Data_Of_Read_DDR._Hold04()
            return allFields

    class _FMPM_Hold05_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold05 Data Of Read DDR"
    
        def description(self):
            return "Hold05 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000025
            
        def endAddress(self):
            return 0xffffffff

        class _Hold05(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold05"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold05"] = _AF6CCI0021_RD_PM._FMPM_Hold05_Data_Of_Read_DDR._Hold05()
            return allFields

    class _FMPM_Hold06_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold06 Data Of Read DDR"
    
        def description(self):
            return "Hold06 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000026
            
        def endAddress(self):
            return 0xffffffff

        class _Hold06(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold06"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold06"] = _AF6CCI0021_RD_PM._FMPM_Hold06_Data_Of_Read_DDR._Hold06()
            return allFields

    class _FMPM_Hold07_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold07 Data Of Read DDR"
    
        def description(self):
            return "Hold07 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000027
            
        def endAddress(self):
            return 0xffffffff

        class _Hold07(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold07"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold07"] = _AF6CCI0021_RD_PM._FMPM_Hold07_Data_Of_Read_DDR._Hold07()
            return allFields

    class _FMPM_Hold08_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold08 Data Of Read DDR"
    
        def description(self):
            return "Hold08 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000028
            
        def endAddress(self):
            return 0xffffffff

        class _Hold08(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold08"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold08"] = _AF6CCI0021_RD_PM._FMPM_Hold08_Data_Of_Read_DDR._Hold08()
            return allFields

    class _FMPM_Hold09_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold09 Data Of Read DDR"
    
        def description(self):
            return "Hold09 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000029
            
        def endAddress(self):
            return 0xffffffff

        class _Hold09(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold09"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold09"] = _AF6CCI0021_RD_PM._FMPM_Hold09_Data_Of_Read_DDR._Hold09()
            return allFields

    class _FMPM_Hold10_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold10 Data Of Read DDR"
    
        def description(self):
            return "Hold10 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000002a
            
        def endAddress(self):
            return 0xffffffff

        class _Hold10(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold10"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold10"] = _AF6CCI0021_RD_PM._FMPM_Hold10_Data_Of_Read_DDR._Hold10()
            return allFields

    class _FMPM_Hold11_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold11 Data Of Read DDR"
    
        def description(self):
            return "Hold11 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000002b
            
        def endAddress(self):
            return 0xffffffff

        class _Hold11(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold11"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold11"] = _AF6CCI0021_RD_PM._FMPM_Hold11_Data_Of_Read_DDR._Hold11()
            return allFields

    class _FMPM_Hold12_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold12 Data Of Read DDR"
    
        def description(self):
            return "Hold12 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000002c
            
        def endAddress(self):
            return 0xffffffff

        class _Hold12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold12"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold12"] = _AF6CCI0021_RD_PM._FMPM_Hold12_Data_Of_Read_DDR._Hold12()
            return allFields

    class _FMPM_Hold13_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold13 Data Of Read DDR"
    
        def description(self):
            return "Hold13 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000002d
            
        def endAddress(self):
            return 0xffffffff

        class _Hold13(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold13"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold13"] = _AF6CCI0021_RD_PM._FMPM_Hold13_Data_Of_Read_DDR._Hold13()
            return allFields

    class _FMPM_Hold14_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold14 Data Of Read DDR"
    
        def description(self):
            return "Hold14 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000002e
            
        def endAddress(self):
            return 0xffffffff

        class _Hold14(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold14"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold14"] = _AF6CCI0021_RD_PM._FMPM_Hold14_Data_Of_Read_DDR._Hold14()
            return allFields

    class _FMPM_Hold15_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold15 Data Of Read DDR"
    
        def description(self):
            return "Hold15 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000002f
            
        def endAddress(self):
            return 0xffffffff

        class _Hold15(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold15"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold15"] = _AF6CCI0021_RD_PM._FMPM_Hold15_Data_Of_Read_DDR._Hold15()
            return allFields

    class _FMPM_Hold16_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold16 Data Of Read DDR"
    
        def description(self):
            return "Hold16 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000030
            
        def endAddress(self):
            return 0xffffffff

        class _Hold16(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold16"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold16"] = _AF6CCI0021_RD_PM._FMPM_Hold16_Data_Of_Read_DDR._Hold16()
            return allFields

    class _FMPM_Hold17_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold17 Data Of Read DDR"
    
        def description(self):
            return "Hold17 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000031
            
        def endAddress(self):
            return 0xffffffff

        class _Hold17(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold17"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold17"] = _AF6CCI0021_RD_PM._FMPM_Hold17_Data_Of_Read_DDR._Hold17()
            return allFields

    class _FMPM_Hold18_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold18 Data Of Read DDR"
    
        def description(self):
            return "Hold18 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000032
            
        def endAddress(self):
            return 0xffffffff

        class _Hold18(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold18"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold18"] = _AF6CCI0021_RD_PM._FMPM_Hold18_Data_Of_Read_DDR._Hold18()
            return allFields

    class _FMPM_Hold19_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold19 Data Of Read DDR"
    
        def description(self):
            return "Hold19 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000033
            
        def endAddress(self):
            return 0xffffffff

        class _Hold19(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold19"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold19"] = _AF6CCI0021_RD_PM._FMPM_Hold19_Data_Of_Read_DDR._Hold19()
            return allFields

    class _FMPM_Hold20_Data_Of_Read_DDR(AtRegister.AtRegister):
        def name(self):
            return "FMPM Hold20 Data Of Read DDR"
    
        def description(self):
            return "Hold20 Data Of Read DDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000034
            
        def endAddress(self):
            return 0xffffffff

        class _Hold20(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Hold20"
            
            def description(self):
                return "Missing bit is unused"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Hold20"] = _AF6CCI0021_RD_PM._FMPM_Hold20_Data_Of_Read_DDR._Hold20()
            return allFields

    class _FMPM_TCA_Threshold(AtRegister.AtRegister):
        def name(self):
            return "FMPM TCA Threshold"
    
        def description(self):
            return "TCA Threshold"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00050+$id"
            
        def startAddress(self):
            return 0x00000050
            
        def endAddress(self):
            return 0x00000057

        class _tca(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tca"
            
            def description(self):
                return "TCA threshold"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tca"] = _AF6CCI0021_RD_PM._FMPM_TCA_Threshold._tca()
            return allFields

    class _FMPM_K_Threshold_of_DS1(AtRegister.AtRegister):
        def name(self):
            return "FMPM K Threshold of DS1"
    
        def description(self):
            return "K threshold of DS1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000B0+$id"
            
        def startAddress(self):
            return 0x000000b0
            
        def endAddress(self):
            return 0x000000b7

        class _line_kval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "line_kval"
            
            def description(self):
                return "K threshold of Line DS1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _path_kval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "path_kval"
            
            def description(self):
                return "K threshold of Path DS1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["line_kval"] = _AF6CCI0021_RD_PM._FMPM_K_Threshold_of_DS1._line_kval()
            allFields["path_kval"] = _AF6CCI0021_RD_PM._FMPM_K_Threshold_of_DS1._path_kval()
            return allFields

    class _FMPM_K_Threshold_of_PW(AtRegister.AtRegister):
        def name(self):
            return "FMPM K Threshold of PW"
    
        def description(self):
            return "K threshold of PW"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000C0+$id"
            
        def startAddress(self):
            return 0x000000c0
            
        def endAddress(self):
            return 0x000000c7

        class _kval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "kval"
            
            def description(self):
                return "K threshold of PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["kval"] = _AF6CCI0021_RD_PM._FMPM_K_Threshold_of_PW._kval()
            return allFields

    class _FMPM_Threshold_Type_of_DS1(AtRegister.AtRegister):
        def name(self):
            return "FMPM Threshold Type of DS1"
    
        def description(self):
            return "Threshold Type of DS1"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x08000+$H*0x20+$I"
            
        def startAddress(self):
            return 0x00008000
            
        def endAddress(self):
            return 0x0000bfff

        class _K_TYPE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "K_TYPE"
            
            def description(self):
                return "Use to read K threshold table"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TCA_TYPE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TCA_TYPE"
            
            def description(self):
                return "Use to read TCA threshold table"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["K_TYPE"] = _AF6CCI0021_RD_PM._FMPM_Threshold_Type_of_DS1._K_TYPE()
            allFields["TCA_TYPE"] = _AF6CCI0021_RD_PM._FMPM_Threshold_Type_of_DS1._TCA_TYPE()
            return allFields

    class _FMPM_Threshold_Type_of_PW(AtRegister.AtRegister):
        def name(self):
            return "FMPM Threshold Type of PW"
    
        def description(self):
            return "Threshold Type of PW"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0C000+$id"
            
        def startAddress(self):
            return 0x0000c000
            
        def endAddress(self):
            return 0x0000dfff

        class _K_TYPE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "K_TYPE"
            
            def description(self):
                return "Use to read K threshold table"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TCA_TYPE(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TCA_TYPE"
            
            def description(self):
                return "Use to read TCA threshold table"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["K_TYPE"] = _AF6CCI0021_RD_PM._FMPM_Threshold_Type_of_PW._K_TYPE()
            allFields["TCA_TYPE"] = _AF6CCI0021_RD_PM._FMPM_Threshold_Type_of_PW._TCA_TYPE()
            return allFields

    class _FMPM_FM_Interrupt(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM Interrupt"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00014000
            
        def endAddress(self):
            return 0xffffffff

        class _FMPM_FM_Intr_PW(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_Intr_PW"
            
            def description(self):
                return "PW Interrupt"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_Intr_DS1E1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_Intr_DS1E1"
            
            def description(self):
                return "DS1/E1 Interrupt"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_Intr_PW"] = _AF6CCI0021_RD_PM._FMPM_FM_Interrupt._FMPM_FM_Intr_PW()
            allFields["FMPM_FM_Intr_DS1E1"] = _AF6CCI0021_RD_PM._FMPM_FM_Interrupt._FMPM_FM_Intr_DS1E1()
            return allFields

    class _FMPM_FM_Interrupt_Mask(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM Interrupt Mask"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00014001
            
        def endAddress(self):
            return 0xffffffff

        class _FMPM_FM_Intr_PW(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_Intr_PW"
            
            def description(self):
                return "PW Interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_Intr_DS1E1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_Intr_DS1E1"
            
            def description(self):
                return "DS1/E1 Interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_Intr_PW"] = _AF6CCI0021_RD_PM._FMPM_FM_Interrupt_Mask._FMPM_FM_Intr_PW()
            allFields["FMPM_FM_Intr_DS1E1"] = _AF6CCI0021_RD_PM._FMPM_FM_Interrupt_Mask._FMPM_FM_Intr_DS1E1()
            return allFields

    class _FMPM_FM_DS1E1_Group_Interrupt_OR(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM DS1E1 Group Interrupt OR"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00018003
            
        def endAddress(self):
            return 0xffffffff

        class _FMPM_FM_DS1E1_GRP_OR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_DS1E1_GRP_OR"
            
            def description(self):
                return "DS1/E1 Group Interrupt OR"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_DS1E1_GRP_OR"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Group_Interrupt_OR._FMPM_FM_DS1E1_GRP_OR()
            return allFields

    class _FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM DS1E1 Framer Interrupt OR AND MASK Per DS1E1"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1B000+$H"
            
        def startAddress(self):
            return 0x0001b000
            
        def endAddress(self):
            return 0x0001b1ff

        class _FMPM_FM_DS1E1_OAMSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_DS1E1_OAMSK"
            
            def description(self):
                return "DS1/E1 Framer Interrupt"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_DS1E1_OAMSK"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1._FMPM_FM_DS1E1_OAMSK()
            return allFields

    class _FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM DS1E1 Framer Interrupt MASK Per DS1E1"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1B200+$H"
            
        def startAddress(self):
            return 0x0001b200
            
        def endAddress(self):
            return 0x0001b3ff

        class _FMPM_FM_DS1E1_MSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_DS1E1_MSK"
            
            def description(self):
                return "DS1/E1 Framer Interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_DS1E1_MSK"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1._FMPM_FM_DS1E1_MSK()
            return allFields

    class _FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM DS1E1 Interrupt Sticky Per Type Per DS1E1"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x30000+$H*0x20+$I"
            
        def startAddress(self):
            return 0x00030000
            
        def endAddress(self):
            return 0x00033fff

        class _FMPM_FM_DS1E1_STK_RAI_CI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_DS1E1_STK_RAI_CI"
            
            def description(self):
                return "RAI_CI"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_STK_RAI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_DS1E1_STK_RAI"
            
            def description(self):
                return "RAI"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_STK_LOF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_DS1E1_STK_LOF"
            
            def description(self):
                return "LOF"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_STK_AIS_CI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_DS1E1_STK_AIS_CI"
            
            def description(self):
                return "AIS_CI"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_STK_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_DS1E1_STK_AIS"
            
            def description(self):
                return "AIS"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_STK_LOS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_DS1E1_STK_LOS"
            
            def description(self):
                return "LOS"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_DS1E1_STK_RAI_CI"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_STK_RAI_CI()
            allFields["FMPM_FM_DS1E1_STK_RAI"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_STK_RAI()
            allFields["FMPM_FM_DS1E1_STK_LOF"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_STK_LOF()
            allFields["FMPM_FM_DS1E1_STK_AIS_CI"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_STK_AIS_CI()
            allFields["FMPM_FM_DS1E1_STK_AIS"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_STK_AIS()
            allFields["FMPM_FM_DS1E1_STK_LOS"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_STK_LOS()
            return allFields

    class _FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x34000+$H*0x20+$I"
            
        def startAddress(self):
            return 0x00034000
            
        def endAddress(self):
            return 0x00037fff

        class _FMPM_FM_DS1E1_MSK_RAI_CI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_DS1E1_MSK_RAI_CI"
            
            def description(self):
                return "RAI_CI"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_MSK_RAI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_DS1E1_MSK_RAI"
            
            def description(self):
                return "RAI"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_MSK_LOF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_DS1E1_MSK_LOF"
            
            def description(self):
                return "LOF"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_MSK_AIS_CI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_DS1E1_MSK_AIS_CI"
            
            def description(self):
                return "AIS_CI"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_MSK_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_DS1E1_MSK_AIS"
            
            def description(self):
                return "AIS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_MSK_LOS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_DS1E1_MSK_LOS"
            
            def description(self):
                return "LOS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_DS1E1_MSK_RAI_CI"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_MSK_RAI_CI()
            allFields["FMPM_FM_DS1E1_MSK_RAI"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_MSK_RAI()
            allFields["FMPM_FM_DS1E1_MSK_LOF"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_MSK_LOF()
            allFields["FMPM_FM_DS1E1_MSK_AIS_CI"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_MSK_AIS_CI()
            allFields["FMPM_FM_DS1E1_MSK_AIS"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_MSK_AIS()
            allFields["FMPM_FM_DS1E1_MSK_LOS"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_MSK_LOS()
            return allFields

    class _FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM DS1E1 Interrupt Current Status Per Type Per DS1E1"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x38000+$H*0x20+$I"
            
        def startAddress(self):
            return 0x00038000
            
        def endAddress(self):
            return 0x0003bfff

        class _FMPM_FM_DS1E1_AMSK_RAI_CI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "FMPM_FM_DS1E1_AMSK_RAI_CI"
            
            def description(self):
                return "RAI_CI"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_AMSK_RAI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "FMPM_FM_DS1E1_AMSK_RAI"
            
            def description(self):
                return "RAI"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_AMSK_LOF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "FMPM_FM_DS1E1_AMSK_LOF"
            
            def description(self):
                return "LOF"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_AMSK_AIS_CI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "FMPM_FM_DS1E1_AMSK_AIS_CI"
            
            def description(self):
                return "AIS_CI"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_AMSK_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "FMPM_FM_DS1E1_AMSK_AIS"
            
            def description(self):
                return "AIS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_AMSK_LOS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FMPM_FM_DS1E1_AMSK_LOS"
            
            def description(self):
                return "LOS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_CUR_RAI_CI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_DS1E1_CUR_RAI_CI"
            
            def description(self):
                return "RAI_CI"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_CUR_RAI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_DS1E1_CUR_RAI"
            
            def description(self):
                return "RAI"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_CUR_LOF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_DS1E1_CUR_LOF"
            
            def description(self):
                return "LOF"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_CUR_AIS_CI(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_DS1E1_CUR_AIS_CI"
            
            def description(self):
                return "AIS_CI"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_CUR_AIS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_DS1E1_CUR_AIS"
            
            def description(self):
                return "AIS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_DS1E1_CUR_LOS(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_DS1E1_CUR_LOS"
            
            def description(self):
                return "LOS"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_DS1E1_AMSK_RAI_CI"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_AMSK_RAI_CI()
            allFields["FMPM_FM_DS1E1_AMSK_RAI"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_AMSK_RAI()
            allFields["FMPM_FM_DS1E1_AMSK_LOF"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_AMSK_LOF()
            allFields["FMPM_FM_DS1E1_AMSK_AIS_CI"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_AMSK_AIS_CI()
            allFields["FMPM_FM_DS1E1_AMSK_AIS"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_AMSK_AIS()
            allFields["FMPM_FM_DS1E1_AMSK_LOS"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_AMSK_LOS()
            allFields["FMPM_FM_DS1E1_CUR_RAI_CI"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_CUR_RAI_CI()
            allFields["FMPM_FM_DS1E1_CUR_RAI"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_CUR_RAI()
            allFields["FMPM_FM_DS1E1_CUR_LOF"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_CUR_LOF()
            allFields["FMPM_FM_DS1E1_CUR_AIS_CI"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_CUR_AIS_CI()
            allFields["FMPM_FM_DS1E1_CUR_AIS"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_CUR_AIS()
            allFields["FMPM_FM_DS1E1_CUR_LOS"] = _AF6CCI0021_RD_PM._FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1._FMPM_FM_DS1E1_CUR_LOS()
            return allFields

    class _FMPM_FM_PW_Group_Interrupt_OR(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM PW Group Interrupt OR"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00018004
            
        def endAddress(self):
            return 0xffffffff

        class _FMPM_FM_PW_GRP_OR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_PW_GRP_OR"
            
            def description(self):
                return "PW Group Interrupt OR, bit per group"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_PW_GRP_OR"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Group_Interrupt_OR._FMPM_FM_PW_GRP_OR()
            return allFields

    class _FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM PW Framer Interrupt OR AND MASK Per PW"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1B400+$J"
            
        def startAddress(self):
            return 0x0001b400
            
        def endAddress(self):
            return 0x0001b4ff

        class _FMPM_FM_PW_OAMSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_PW_OAMSK"
            
            def description(self):
                return "Interrupt OR AND MASK per PW, bit per PW"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_PW_OAMSK"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW._FMPM_FM_PW_OAMSK()
            return allFields

    class _FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM PW Framer Interrupt MASK Per PW"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1B500+$J"
            
        def startAddress(self):
            return 0x0001b500
            
        def endAddress(self):
            return 0x0001b5ff

        class _FMPM_FM_PW_MSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_PW_MSK"
            
            def description(self):
                return "Interrupt OR AND MASK per PW, bit per PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_PW_MSK"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW._FMPM_FM_PW_MSK()
            return allFields

    class _FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM PW Interrupt Sticky Per Type Per PW"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1C000+$J*0x20+$K"
            
        def startAddress(self):
            return 0x0001c000
            
        def endAddress(self):
            return 0x0001dfff

        class _FMPM_FM_PW_STK_RBIT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_FM_PW_STK_RBIT"
            
            def description(self):
                return "RBIT"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_STK_STRAY(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_FM_PW_STK_STRAY"
            
            def description(self):
                return "STRAY"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_STK_MALF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_PW_STK_MALF"
            
            def description(self):
                return "MALF"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_STK_BufUder(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_PW_STK_BufUder"
            
            def description(self):
                return "BufUder"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_STK_BufOver(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_PW_STK_BufOver"
            
            def description(self):
                return "BufOver"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_STK_LBIT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_PW_STK_LBIT"
            
            def description(self):
                return "LBIT"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_STK_LOPSYN(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_PW_STK_LOPSYN"
            
            def description(self):
                return "LOPSYN"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_STK_LOPSTA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_PW_STK_LOPSTA"
            
            def description(self):
                return "LOPSTA"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_PW_STK_RBIT"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW._FMPM_FM_PW_STK_RBIT()
            allFields["FMPM_FM_PW_STK_STRAY"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW._FMPM_FM_PW_STK_STRAY()
            allFields["FMPM_FM_PW_STK_MALF"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW._FMPM_FM_PW_STK_MALF()
            allFields["FMPM_FM_PW_STK_BufUder"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW._FMPM_FM_PW_STK_BufUder()
            allFields["FMPM_FM_PW_STK_BufOver"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW._FMPM_FM_PW_STK_BufOver()
            allFields["FMPM_FM_PW_STK_LBIT"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW._FMPM_FM_PW_STK_LBIT()
            allFields["FMPM_FM_PW_STK_LOPSYN"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW._FMPM_FM_PW_STK_LOPSYN()
            allFields["FMPM_FM_PW_STK_LOPSTA"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW._FMPM_FM_PW_STK_LOPSTA()
            return allFields

    class _FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM PW Interrupt MASK Per Type Per PW"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1E000+$J*0x20+$K"
            
        def startAddress(self):
            return 0x0001e000
            
        def endAddress(self):
            return 0x0001ffff

        class _FMPM_FM_PW_MSK_RBIT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_FM_PW_MSK_RBIT"
            
            def description(self):
                return "RBIT"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_MSK_STRAY(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_FM_PW_MSK_STRAY"
            
            def description(self):
                return "STRAY"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_MSK_MALF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_PW_MSK_MALF"
            
            def description(self):
                return "MALF"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_MSK_BufUder(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_PW_MSK_BufUder"
            
            def description(self):
                return "BufUder"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_MSK_BufOver(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_PW_MSK_BufOver"
            
            def description(self):
                return "BufOver"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_MSK_LBIT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_PW_MSK_LBIT"
            
            def description(self):
                return "LBIT"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_MSK_LOPSYN(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_PW_MSK_LOPSYN"
            
            def description(self):
                return "LOPSYN"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_MSK_LOPSTA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_PW_MSK_LOPSTA"
            
            def description(self):
                return "LOPSTA"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_PW_MSK_RBIT"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW._FMPM_FM_PW_MSK_RBIT()
            allFields["FMPM_FM_PW_MSK_STRAY"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW._FMPM_FM_PW_MSK_STRAY()
            allFields["FMPM_FM_PW_MSK_MALF"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW._FMPM_FM_PW_MSK_MALF()
            allFields["FMPM_FM_PW_MSK_BufUder"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW._FMPM_FM_PW_MSK_BufUder()
            allFields["FMPM_FM_PW_MSK_BufOver"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW._FMPM_FM_PW_MSK_BufOver()
            allFields["FMPM_FM_PW_MSK_LBIT"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW._FMPM_FM_PW_MSK_LBIT()
            allFields["FMPM_FM_PW_MSK_LOPSYN"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW._FMPM_FM_PW_MSK_LOPSYN()
            allFields["FMPM_FM_PW_MSK_LOPSTA"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW._FMPM_FM_PW_MSK_LOPSTA()
            return allFields

    class _FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW(AtRegister.AtRegister):
        def name(self):
            return "FMPM FM PW Interrupt Current Status Per Type Per PW"
    
        def description(self):
            return "FMPM FM Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x20000+$J*0x20+$K"
            
        def startAddress(self):
            return 0x00020000
            
        def endAddress(self):
            return 0x00021fff

        class _FMPM_FM_PW_AMSK_RBIT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "FMPM_FM_PW_AMSK_RBIT"
            
            def description(self):
                return "RBIT"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_AMSK_STRAY(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "FMPM_FM_PW_AMSK_STRAY"
            
            def description(self):
                return "STRAY"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_AMSK_MALF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "FMPM_FM_PW_AMSK_MALF"
            
            def description(self):
                return "MALF"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_AMSK_BufUder(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "FMPM_FM_PW_AMSK_BufUder"
            
            def description(self):
                return "BufUder"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_AMSK_BufOver(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "FMPM_FM_PW_AMSK_BufOver"
            
            def description(self):
                return "BufOver"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_AMSK_LBIT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "FMPM_FM_PW_AMSK_LBIT"
            
            def description(self):
                return "LBIT"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_AMSK_LOPSYN(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "FMPM_FM_PW_AMSK_LOPSYN"
            
            def description(self):
                return "LOPSYN"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_AMSK_LOPSTA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FMPM_FM_PW_AMSK_LOPSTA"
            
            def description(self):
                return "LOPSTA"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_CUR_RBIT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_FM_PW_CUR_RBIT"
            
            def description(self):
                return "RBIT"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_CUR_STRAY(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_FM_PW_CUR_STRAY"
            
            def description(self):
                return "STRAY"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_CUR_MALF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_PW_CUR_MALF"
            
            def description(self):
                return "MALF"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_CUR_BufUder(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_PW_CUR_BufUder"
            
            def description(self):
                return "BufUder"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_CUR_BufOver(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_PW_CUR_BufOver"
            
            def description(self):
                return "BufOver"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_CUR_LBIT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_PW_CUR_LBIT"
            
            def description(self):
                return "LBIT"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_CUR_LOPSYN(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_PW_CUR_LOPSYN"
            
            def description(self):
                return "LOPSYN"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_CUR_LOPSTA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_PW_CUR_LOPSTA"
            
            def description(self):
                return "LOPSTA"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_PW_AMSK_RBIT"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_AMSK_RBIT()
            allFields["FMPM_FM_PW_AMSK_STRAY"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_AMSK_STRAY()
            allFields["FMPM_FM_PW_AMSK_MALF"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_AMSK_MALF()
            allFields["FMPM_FM_PW_AMSK_BufUder"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_AMSK_BufUder()
            allFields["FMPM_FM_PW_AMSK_BufOver"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_AMSK_BufOver()
            allFields["FMPM_FM_PW_AMSK_LBIT"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_AMSK_LBIT()
            allFields["FMPM_FM_PW_AMSK_LOPSYN"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_AMSK_LOPSYN()
            allFields["FMPM_FM_PW_AMSK_LOPSTA"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_AMSK_LOPSTA()
            allFields["FMPM_FM_PW_CUR_RBIT"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_CUR_RBIT()
            allFields["FMPM_FM_PW_CUR_STRAY"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_CUR_STRAY()
            allFields["FMPM_FM_PW_CUR_MALF"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_CUR_MALF()
            allFields["FMPM_FM_PW_CUR_BufUder"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_CUR_BufUder()
            allFields["FMPM_FM_PW_CUR_BufOver"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_CUR_BufOver()
            allFields["FMPM_FM_PW_CUR_LBIT"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_CUR_LBIT()
            allFields["FMPM_FM_PW_CUR_LOPSYN"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_CUR_LOPSYN()
            allFields["FMPM_FM_PW_CUR_LOPSTA"] = _AF6CCI0021_RD_PM._FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_CUR_LOPSTA()
            return allFields

    class _FMPM_PW_Def_Interrupt(AtRegister.AtRegister):
        def name(self):
            return "FMPM PW Def Interrupt"
    
        def description(self):
            return "FMPM PW defect Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00094000
            
        def endAddress(self):
            return 0xffffffff

        class _FMPM_PW_Def_Intr_PW(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_PW_Def_Intr_PW"
            
            def description(self):
                return "PW defect interrupt"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_PW_Def_Intr_PW"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt._FMPM_PW_Def_Intr_PW()
            return allFields

    class _FMPM_PW_Def_Interrupt_Mask(AtRegister.AtRegister):
        def name(self):
            return "FMPM PW Def Interrupt Mask"
    
        def description(self):
            return "FMPM PW defect Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00094001
            
        def endAddress(self):
            return 0xffffffff

        class _FMPM_PW_Def_Intr_MSK_PW(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_PW_Def_Intr_MSK_PW"
            
            def description(self):
                return "PW defect interrupt"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_PW_Def_Intr_MSK_PW"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Mask._FMPM_PW_Def_Intr_MSK_PW()
            return allFields

    class _FMPM_PW_Def_Level1_Group_Interrupt_OR(AtRegister.AtRegister):
        def name(self):
            return "FMPM PW Def Level1 Group Interrupt OR"
    
        def description(self):
            return "FMPM PW defect Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00098004
            
        def endAddress(self):
            return 0xffffffff

        class _FMPM_PW_Def_GRP_OR(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_PW_Def_GRP_OR"
            
            def description(self):
                return "PW Group Interrupt OR, bit per group"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_PW_Def_GRP_OR"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Level1_Group_Interrupt_OR._FMPM_PW_Def_GRP_OR()
            return allFields

    class _FMPM_PW_Def_Framer_Interrupt_OR_AND_MASK_Per_PW(AtRegister.AtRegister):
        def name(self):
            return "FMPM PW Def Framer Interrupt OR AND MASK Per PW"
    
        def description(self):
            return "FMPM PW defect Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x9B400+$J"
            
        def startAddress(self):
            return 0x0009b400
            
        def endAddress(self):
            return 0x0009b4ff

        class _FMPM_PW_Def_OAMSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_PW_Def_OAMSK"
            
            def description(self):
                return "Interrupt OR AND MASK per PW, bit per PW"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_PW_Def_OAMSK"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Framer_Interrupt_OR_AND_MASK_Per_PW._FMPM_PW_Def_OAMSK()
            return allFields

    class _FMPM_PW_Def_Framer_Interrupt_MASK_Per_PW(AtRegister.AtRegister):
        def name(self):
            return "FMPM PW Def Framer Interrupt MASK Per PW"
    
        def description(self):
            return "FMPM PW defect Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9B500+$J"
            
        def startAddress(self):
            return 0x0009b500
            
        def endAddress(self):
            return 0x0009b5ff

        class _FMPM_PW_Def_MSK(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_PW_Def_MSK"
            
            def description(self):
                return "Interrupt OR AND MASK per PW, bit per PW"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_PW_Def_MSK"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Framer_Interrupt_MASK_Per_PW._FMPM_PW_Def_MSK()
            return allFields

    class _FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW(AtRegister.AtRegister):
        def name(self):
            return "FMPM PW Def Interrupt Sticky Per Type Per PW"
    
        def description(self):
            return "FMPM PW defect Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x9C000+$J*0x20+$K"
            
        def startAddress(self):
            return 0x0009c000
            
        def endAddress(self):
            return 0x0009dfff

        class _FMPM_PW_Def_STK_RBIT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_PW_Def_STK_RBIT"
            
            def description(self):
                return "RBIT"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_PW_Def_STK_STRAY(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_PW_Def_STK_STRAY"
            
            def description(self):
                return "STRAY"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_PW_Def_STK_MALF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_PW_Def_STK_MALF"
            
            def description(self):
                return "MALF"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_PW_Def_STK_BufUder(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_PW_Def_STK_BufUder"
            
            def description(self):
                return "BufUder"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_PW_Def_STK_BufOver(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_PW_Def_STK_BufOver"
            
            def description(self):
                return "BufOver"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_PW_Def_STK_LBIT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_PW_Def_STK_LBIT"
            
            def description(self):
                return "LBIT"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_PW_Def_STK_LOPSYN(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_PW_Def_STK_LOPSYN"
            
            def description(self):
                return "LOPSYN"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_PW_Def_STK_LOPSTA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_PW_Def_STK_LOPSTA"
            
            def description(self):
                return "LOPSTA"
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_PW_Def_STK_RBIT"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW._FMPM_PW_Def_STK_RBIT()
            allFields["FMPM_PW_Def_STK_STRAY"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW._FMPM_PW_Def_STK_STRAY()
            allFields["FMPM_PW_Def_STK_MALF"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW._FMPM_PW_Def_STK_MALF()
            allFields["FMPM_PW_Def_STK_BufUder"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW._FMPM_PW_Def_STK_BufUder()
            allFields["FMPM_PW_Def_STK_BufOver"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW._FMPM_PW_Def_STK_BufOver()
            allFields["FMPM_PW_Def_STK_LBIT"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW._FMPM_PW_Def_STK_LBIT()
            allFields["FMPM_PW_Def_STK_LOPSYN"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW._FMPM_PW_Def_STK_LOPSYN()
            allFields["FMPM_PW_Def_STK_LOPSTA"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Sticky_Per_Type_Per_PW._FMPM_PW_Def_STK_LOPSTA()
            return allFields

    class _FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW(AtRegister.AtRegister):
        def name(self):
            return "FMPM PW Def Interrupt MASK Per Type Per PW"
    
        def description(self):
            return "FMPM PW defect Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x9E000+$J*0x20+$K"
            
        def startAddress(self):
            return 0x0009e000
            
        def endAddress(self):
            return 0x0009ffff

        class _FMPM_PW_Def_MSK_RBIT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_PW_Def_MSK_RBIT"
            
            def description(self):
                return "RBIT"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_PW_Def_MSK_STRAY(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_PW_Def_MSK_STRAY"
            
            def description(self):
                return "STRAY"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_PW_Def_MSK_MALF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_PW_Def_MSK_MALF"
            
            def description(self):
                return "MALF"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_PW_Def_MSK_BufUder(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_PW_Def_MSK_BufUder"
            
            def description(self):
                return "BufUder"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_PW_Def_MSK_BufOver(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_PW_Def_MSK_BufOver"
            
            def description(self):
                return "BufOver"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_PW_Def_MSK_LBIT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_PW_Def_MSK_LBIT"
            
            def description(self):
                return "LBIT"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_PW_Def_MSK_LOPSYN(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_PW_Def_MSK_LOPSYN"
            
            def description(self):
                return "LOPSYN"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_PW_Def_MSK_LOPSTA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_PW_Def_MSK_LOPSTA"
            
            def description(self):
                return "LOPSTA"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_PW_Def_MSK_RBIT"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW._FMPM_PW_Def_MSK_RBIT()
            allFields["FMPM_PW_Def_MSK_STRAY"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW._FMPM_PW_Def_MSK_STRAY()
            allFields["FMPM_PW_Def_MSK_MALF"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW._FMPM_PW_Def_MSK_MALF()
            allFields["FMPM_PW_Def_MSK_BufUder"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW._FMPM_PW_Def_MSK_BufUder()
            allFields["FMPM_PW_Def_MSK_BufOver"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW._FMPM_PW_Def_MSK_BufOver()
            allFields["FMPM_PW_Def_MSK_LBIT"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW._FMPM_PW_Def_MSK_LBIT()
            allFields["FMPM_PW_Def_MSK_LOPSYN"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW._FMPM_PW_Def_MSK_LOPSYN()
            allFields["FMPM_PW_Def_MSK_LOPSTA"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_MASK_Per_Type_Per_PW._FMPM_PW_Def_MSK_LOPSTA()
            return allFields

    class _FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW(AtRegister.AtRegister):
        def name(self):
            return "FMPM PW Def Interrupt Current Status Per Type Per PW"
    
        def description(self):
            return "FMPM PW defect Interrupt"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0xA0000+$J*0x20+$K"
            
        def startAddress(self):
            return 0x000a0000
            
        def endAddress(self):
            return 0x000a1fff

        class _FMPM_FM_PW_AMSK_RBIT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "FMPM_FM_PW_AMSK_RBIT"
            
            def description(self):
                return "RBIT"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_AMSK_STRAY(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "FMPM_FM_PW_AMSK_STRAY"
            
            def description(self):
                return "STRAY"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_AMSK_MALF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "FMPM_FM_PW_AMSK_MALF"
            
            def description(self):
                return "MALF"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_AMSK_BufUder(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "FMPM_FM_PW_AMSK_BufUder"
            
            def description(self):
                return "BufUder"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_AMSK_BufOver(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "FMPM_FM_PW_AMSK_BufOver"
            
            def description(self):
                return "BufOver"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_AMSK_LBIT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "FMPM_FM_PW_AMSK_LBIT"
            
            def description(self):
                return "LBIT"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_AMSK_LOPSYN(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "FMPM_FM_PW_AMSK_LOPSYN"
            
            def description(self):
                return "LOPSYN"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_AMSK_LOPSTA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "FMPM_FM_PW_AMSK_LOPSTA"
            
            def description(self):
                return "LOPSTA"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_CUR_RBIT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "FMPM_FM_PW_CUR_RBIT"
            
            def description(self):
                return "RBIT"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_CUR_STRAY(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "FMPM_FM_PW_CUR_STRAY"
            
            def description(self):
                return "STRAY"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_CUR_MALF(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FMPM_FM_PW_CUR_MALF"
            
            def description(self):
                return "MALF"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_CUR_BufUder(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "FMPM_FM_PW_CUR_BufUder"
            
            def description(self):
                return "BufUder"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_CUR_BufOver(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FMPM_FM_PW_CUR_BufOver"
            
            def description(self):
                return "BufOver"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_CUR_LBIT(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "FMPM_FM_PW_CUR_LBIT"
            
            def description(self):
                return "LBIT"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_CUR_LOPSYN(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "FMPM_FM_PW_CUR_LOPSYN"
            
            def description(self):
                return "LOPSYN"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _FMPM_FM_PW_CUR_LOPSTA(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FMPM_FM_PW_CUR_LOPSTA"
            
            def description(self):
                return "LOPSTA"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["FMPM_FM_PW_AMSK_RBIT"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_AMSK_RBIT()
            allFields["FMPM_FM_PW_AMSK_STRAY"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_AMSK_STRAY()
            allFields["FMPM_FM_PW_AMSK_MALF"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_AMSK_MALF()
            allFields["FMPM_FM_PW_AMSK_BufUder"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_AMSK_BufUder()
            allFields["FMPM_FM_PW_AMSK_BufOver"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_AMSK_BufOver()
            allFields["FMPM_FM_PW_AMSK_LBIT"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_AMSK_LBIT()
            allFields["FMPM_FM_PW_AMSK_LOPSYN"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_AMSK_LOPSYN()
            allFields["FMPM_FM_PW_AMSK_LOPSTA"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_AMSK_LOPSTA()
            allFields["FMPM_FM_PW_CUR_RBIT"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_CUR_RBIT()
            allFields["FMPM_FM_PW_CUR_STRAY"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_CUR_STRAY()
            allFields["FMPM_FM_PW_CUR_MALF"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_CUR_MALF()
            allFields["FMPM_FM_PW_CUR_BufUder"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_CUR_BufUder()
            allFields["FMPM_FM_PW_CUR_BufOver"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_CUR_BufOver()
            allFields["FMPM_FM_PW_CUR_LBIT"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_CUR_LBIT()
            allFields["FMPM_FM_PW_CUR_LOPSYN"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_CUR_LOPSYN()
            allFields["FMPM_FM_PW_CUR_LOPSTA"] = _AF6CCI0021_RD_PM._FMPM_PW_Def_Interrupt_Current_Status_Per_Type_Per_PW._FMPM_FM_PW_CUR_LOPSTA()
            return allFields

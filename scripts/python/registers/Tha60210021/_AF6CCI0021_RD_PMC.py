import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0021_RD_PMC(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["Ethernet_Receive_FCS_Error_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_FCS_Error_Packet_Counter_ro()
        allRegisters["Ethernet_Receive_FCS_Error_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_FCS_Error_Packet_Counter_rc()
        allRegisters["Ethernet_Receive_Header_Error_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_Header_Error_Packet_Counter_ro()
        allRegisters["Ethernet_Receive_Header_Error_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_Header_Error_Packet_Counter_rc()
        allRegisters["Ethernet_Receive_PSN_Header_Error_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_PSN_Header_Error_Packet_Counter_ro()
        allRegisters["Ethernet_Receive_PSN_Header_Error_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_PSN_Header_Error_Packet_Counter_rc()
        allRegisters["Ethernet_Receive_UDP_Port_Error_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_UDP_Port_Error_Packet_Counter_ro()
        allRegisters["Ethernet_Receive_UDP_Port_Error_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_UDP_Port_Error_Packet_Counter_rc()
        allRegisters["Ethernet_Receive_OAM_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_OAM_Packet_Counter_ro()
        allRegisters["Ethernet_Receive_OAM_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_OAM_Packet_Counter_rc()
        allRegisters["Ethernet_Transmit_OAM_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_OAM_Packet_Counter_ro()
        allRegisters["Ethernet_Transmit_OAM_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_OAM_Packet_Counter_rc()
        allRegisters["Ethernet_Receive_Total_Byte_Counter_ro"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_Total_Byte_Counter_ro()
        allRegisters["Ethernet_Receive_Total_Byte_Counter_rc"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_Total_Byte_Counter_rc()
        allRegisters["Ethernet_Receive_Total_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_Total_Packet_Counter_ro()
        allRegisters["Ethernet_Receive_Total_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_Total_Packet_Counter_rc()
        allRegisters["Ethernet_Receive_Classified_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_Classified_Packet_Counter_ro()
        allRegisters["Ethernet_Receive_Classified_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_Classified_Packet_Counter_rc()
        allRegisters["Ethernet_Transmit_Byte_Counter_ro"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_Byte_Counter_ro()
        allRegisters["Ethernet_Transmit_Byte_Counter_rc"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_Byte_Counter_rc()
        allRegisters["Ethernet_Transmit_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_Packet_Counter_ro()
        allRegisters["Ethernet_Transmit_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_Packet_Counter_rc()
        allRegisters["Pseudowire_Transmit_Good_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Pseudowire_Transmit_Good_Packet_Counter_ro()
        allRegisters["Pseudowire_Transmit_Good_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Pseudowire_Transmit_Good_Packet_Counter_rc()
        allRegisters["Pseudowire_Receive_Payload_Octet_Counter_ro"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Payload_Octet_Counter_ro()
        allRegisters["Pseudowire_Receive_Payload_Octet_Counter_rc"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Payload_Octet_Counter_rc()
        allRegisters["Pseudowire_Receive_Good_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Good_Packet_Counter_ro()
        allRegisters["Pseudowire_Receive_Good_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Good_Packet_Counter_rc()
        allRegisters["Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro()
        allRegisters["Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc()
        allRegisters["Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_ro()
        allRegisters["Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_rc()
        allRegisters["Pseudowire_Receive_LOFS_Transition_Counter_ro"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_LOFS_Transition_Counter_ro()
        allRegisters["Pseudowire_Receive_LOFS_Transition_Counter_rc"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_LOFS_Transition_Counter_rc()
        allRegisters["Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_ro"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_ro()
        allRegisters["Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_rc"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_rc()
        allRegisters["Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro()
        allRegisters["Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc()
        allRegisters["Pseudowire_Receive_Malform_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Malform_Packet_Counter_ro()
        allRegisters["Pseudowire_Receive_Malform_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Malform_Packet_Counter_rc()
        allRegisters["Pseudowire_Receive_Stray_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Stray_Packet_Counter_ro()
        allRegisters["Pseudowire_Receive_Stray_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Stray_Packet_Counter_rc()
        allRegisters["Pseudowire_Transmit_L_Bit_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Pseudowire_Transmit_L_Bit_Packet_Counter_ro()
        allRegisters["Pseudowire_Transmit_L_Bit_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Pseudowire_Transmit_L_Bit_Packet_Counter_rc()
        allRegisters["Pseudowire_Receive_L_Bit_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_L_Bit_Packet_Counter_ro()
        allRegisters["Pseudowire_Receive_L_Bit_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_L_Bit_Packet_Counter_rc()
        allRegisters["Pseudowire_Transmit_R_Bit_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Pseudowire_Transmit_R_Bit_Packet_Counter_ro()
        allRegisters["Pseudowire_Transmit_R_Bit_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Pseudowire_Transmit_R_Bit_Packet_Counter_rc()
        allRegisters["Pseudowire_Receive_R_Bit_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_R_Bit_Packet_Counter_ro()
        allRegisters["Pseudowire_Receive_R_Bit_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_R_Bit_Packet_Counter_rc()
        allRegisters["Pseudowire_Transmit_Payload_Octet_Counter_ro"] = _AF6CCI0021_RD_PMC._Pseudowire_Transmit_Payload_Octet_Counter_ro()
        allRegisters["Pseudowire_Transmit_Payload_Octet_Counter_rc"] = _AF6CCI0021_RD_PMC._Pseudowire_Transmit_Payload_Octet_Counter_rc()
        allRegisters["Pseudowire_Transmit_Mbit_Counter_ro"] = _AF6CCI0021_RD_PMC._Pseudowire_Transmit_Mbit_Counter_ro()
        allRegisters["Pseudowire_Transmit_Mbit_Counter_rc"] = _AF6CCI0021_RD_PMC._Pseudowire_Transmit_Mbit_Counter_rc()
        allRegisters["Pseudowire_Receive_Mbit_Counter_ro"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Mbit_Counter_ro()
        allRegisters["Pseudowire_Receive_Mbit_Counter_rc"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Mbit_Counter_rc()
        allRegisters["Pseudowire_Receive_Duplicate_Counter_ro"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Duplicate_Counter_ro()
        allRegisters["Pseudowire_Receive_Duplicate_Counter_rc"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Duplicate_Counter_rc()
        allRegisters["Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_ro"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_ro()
        allRegisters["Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_rc"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_rc()
        allRegisters["Ethernet_and_Pseudowire_Counter_Sticky"] = _AF6CCI0021_RD_PMC._Ethernet_and_Pseudowire_Counter_Sticky()
        allRegisters["Receive_Ethernet_Header_Error_Sticky"] = _AF6CCI0021_RD_PMC._Receive_Ethernet_Header_Error_Sticky()
        allRegisters["Counter_Per_Alarm_Interrupt_Enable_Control"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Interrupt_Enable_Control()
        allRegisters["Counter_Per_Alarm_Interrupt_Status"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Interrupt_Status()
        allRegisters["Counter_Per_Alarm_Current_Status"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Current_Status()
        allRegisters["Counter_Interrupt_OR_Status"] = _AF6CCI0021_RD_PMC._Counter_Interrupt_OR_Status()
        allRegisters["counter_per_group_intr_or_stat"] = _AF6CCI0021_RD_PMC._counter_per_group_intr_or_stat()
        allRegisters["counter_per_group_intr_en_ctrl"] = _AF6CCI0021_RD_PMC._counter_per_group_intr_en_ctrl()
        allRegisters["Ethernet_Receive_1to64Byte_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_1to64Byte_Packet_Counter_ro()
        allRegisters["Ethernet_Receive_1to64Byte_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_1to64Byte_Packet_Counter_rc()
        allRegisters["Ethernet_Transmit_1to64Byte_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_1to64Byte_Packet_Counter_ro()
        allRegisters["Ethernet_Transmit_1to64Byte_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_1to64Byte_Packet_Counter_rc()
        allRegisters["Ethernet_Receive_65to128Byte_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_65to128Byte_Packet_Counter_ro()
        allRegisters["Ethernet_Receive_65to128Byte_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_65to128Byte_Packet_Counter_rc()
        allRegisters["Ethernet_Transmit_65to128Byte_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_65to128Byte_Packet_Counter_ro()
        allRegisters["Ethernet_Transmit_65to128Byte_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_65to128Byte_Packet_Counter_rc()
        allRegisters["Ethernet_Receive_129to256Byte_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_129to256Byte_Packet_Counter_ro()
        allRegisters["Ethernet_Receive_129to256Byte_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_129to256Byte_Packet_Counter_rc()
        allRegisters["Ethernet_Transmit_129to256Byte_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_129to256Byte_Packet_Counter_ro()
        allRegisters["Ethernet_Transmit_129to256Byte_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_129to256Byte_Packet_Counter_rc()
        allRegisters["Ethernet_Receive_257to512Byte_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_257to512Byte_Packet_Counter_ro()
        allRegisters["Ethernet_Receive_257to512Byte_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_257to512Byte_Packet_Counter_rc()
        allRegisters["Ethernet_Transmit_257to512Byte_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_257to512Byte_Packet_Counter_ro()
        allRegisters["Ethernet_Transmit_257to512Byte_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_257to512Byte_Packet_Counter_rc()
        allRegisters["Ethernet_Receive_513to1024Byte_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_513to1024Byte_Packet_Counter_ro()
        allRegisters["Ethernet_Receive_513to1024Byte_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_513to1024Byte_Packet_Counter_rc()
        allRegisters["Ethernet_Transmit_513to1024Byte_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_513to1024Byte_Packet_Counter_ro()
        allRegisters["Ethernet_Transmit_513to1024Byte_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_513to1024Byte_Packet_Counter_rc()
        allRegisters["Ethernet_Receive_1025to1528Byte_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_1025to1528Byte_Packet_Counter_ro()
        allRegisters["Ethernet_Receive_1025to1528Byte_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_1025to1528Byte_Packet_Counter_rc()
        allRegisters["Ethernet_Transmit_1025to1528Byte_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_1025to1528Byte_Packet_Counter_ro()
        allRegisters["Ethernet_Transmit_1025to1528Byte_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_1025to1528Byte_Packet_Counter_rc()
        allRegisters["Ethernet_Receive_Jumbo_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_Jumbo_Packet_Counter_ro()
        allRegisters["Ethernet_Receive_Jumbo_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_Jumbo_Packet_Counter_rc()
        allRegisters["Ethernet_Transmit_Jumbo_Packet_Counter_ro"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_Jumbo_Packet_Counter_ro()
        allRegisters["Ethernet_Transmit_Jumbo_Packet_Counter_rc"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_Jumbo_Packet_Counter_rc()
        return allRegisters

    class _Ethernet_Receive_FCS_Error_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive FCS Error Packet Counter"
    
        def description(self):
            return "Count number of FCS error packets detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _RxFcsErrorPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFcsErrorPk"
            
            def description(self):
                return "This counter count the number of FCS error Ethernet PHY packets received."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFcsErrorPk"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_FCS_Error_Packet_Counter_ro._RxFcsErrorPk()
            return allFields

    class _Ethernet_Receive_FCS_Error_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive FCS Error Packet Counter"
    
        def description(self):
            return "Count number of FCS error packets detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _RxFcsErrorPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxFcsErrorPk"
            
            def description(self):
                return "This counter count the number of FCS error Ethernet PHY packets received."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxFcsErrorPk"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_FCS_Error_Packet_Counter_rc._RxFcsErrorPk()
            return allFields

    class _Ethernet_Receive_Header_Error_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive Header Error Packet Counter"
    
        def description(self):
            return "Count number of Ethernet MAC header error packets detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthErrorPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthErrorPk"
            
            def description(self):
                return "This counter count the number of packets with MAC Header Error. This counter just count up when FCS is already checked good. MAC Header error packet can be on of following: DA does not match AF6FHW0013 MAC address (broadcast MAC address is an exception) Ethernet Type fields do not match 0x8100 (C-VLAN), 0x88A8 (S-VLAN), 0x0800 (Ipv4), 0x86DD(Ipv6), 0x9947(MPLS), 0x88D8(MEF-8) and some of specific values for OAM. Packet with total length (from DA to FCS) > 2000 bytes (oversize packet) Packet with total length (from DA to FCS) < 64 bytes (undersize packet)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthErrorPk"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_Header_Error_Packet_Counter_ro._RxEthErrorPk()
            return allFields

    class _Ethernet_Receive_Header_Error_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive Header Error Packet Counter"
    
        def description(self):
            return "Count number of Ethernet MAC header error packets detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthErrorPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthErrorPk"
            
            def description(self):
                return "This counter count the number of packets with MAC Header Error. This counter just count up when FCS is already checked good. MAC Header error packet can be on of following: DA does not match AF6FHW0013 MAC address (broadcast MAC address is an exception) Ethernet Type fields do not match 0x8100 (C-VLAN), 0x88A8 (S-VLAN), 0x0800 (Ipv4), 0x86DD(Ipv6), 0x9947(MPLS), 0x88D8(MEF-8) and some of specific values for OAM. Packet with total length (from DA to FCS) > 2000 bytes (oversize packet) Packet with total length (from DA to FCS) < 64 bytes (undersize packet)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthErrorPk"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_Header_Error_Packet_Counter_rc._RxEthErrorPk()
            return allFields

    class _Ethernet_Receive_PSN_Header_Error_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive PSN Header Error Packet Counter"
    
        def description(self):
            return "Count number of PSN header error packets detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0xffffffff

        class _RxPsnErrorPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPsnErrorPk"
            
            def description(self):
                return "This counter count the number of packets with PSN Header error. This counter just count up when FCS and MAC Header are already checked good. PSN Header Error packet can be on of following: Ipv4 header error: Checksum error, IP destination address does not match AF6FHW0013 IP address. Ipv6 header error: IP destination address does not match AF6FHW0013 IP address. MEF-8 header error: ECID field will be Pseudowire ID. Counter will count up if the pseudowire ID differs from RxEthPwid value defined in Ethernet Receive Pseudowire Identification Control register. MPLS header error: Bit 31 to 12 of outer label (if exist, also called tunnel label) differs from RxEthMplsOutLabel value defined in Global Ethernet Control register will be Pseudowire ID. Counter will count up if the pseudowire ID differs from RxEthPwid value defined in Ethernet Receive Pseudowire Identification Control register."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPsnErrorPk"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_PSN_Header_Error_Packet_Counter_ro._RxPsnErrorPk()
            return allFields

    class _Ethernet_Receive_PSN_Header_Error_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive PSN Header Error Packet Counter"
    
        def description(self):
            return "Count number of PSN header error packets detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000005
            
        def endAddress(self):
            return 0xffffffff

        class _RxPsnErrorPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPsnErrorPk"
            
            def description(self):
                return "This counter count the number of packets with PSN Header error. This counter just count up when FCS and MAC Header are already checked good. PSN Header Error packet can be on of following: Ipv4 header error: Checksum error, IP destination address does not match AF6FHW0013 IP address. Ipv6 header error: IP destination address does not match AF6FHW0013 IP address. MEF-8 header error: ECID field will be Pseudowire ID. Counter will count up if the pseudowire ID differs from RxEthPwid value defined in Ethernet Receive Pseudowire Identification Control register. MPLS header error: Bit 31 to 12 of outer label (if exist, also called tunnel label) differs from RxEthMplsOutLabel value defined in Global Ethernet Control register will be Pseudowire ID. Counter will count up if the pseudowire ID differs from RxEthPwid value defined in Ethernet Receive Pseudowire Identification Control register."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPsnErrorPk"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_PSN_Header_Error_Packet_Counter_rc._RxPsnErrorPk()
            return allFields

    class _Ethernet_Receive_UDP_Port_Error_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive UDP Port Error Packet Counter"
    
        def description(self):
            return "Count number of UDP port error packets detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000006
            
        def endAddress(self):
            return 0xffffffff

        class _RxUdpErrorPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxUdpErrorPk"
            
            def description(self):
                return "This counter count the number of packets with UDP port error. This counter just count up when FCS, MAC Header and PSN Header (exactly Ipv4 and Ipv6 PSN header) are already checked good. If RxEthUdpMode defined in Global Ethernet Control register is not set, AF6FHW0013 will automatically search for UDP port number with value 0x085E, the remaining UDP port number will be Pseudowire ID. The counter will count up if the  Pseudowire ID differ from RxEthPwid value defined in Ethernet Receive Pseudowire Identification Control register. If  RxEthUdpMode is set, then depending on the  RxEthUdpSel (also  defined in Global Ethernet Control register), UDP source port number or UDP destination port number is used for Pseudowire ID. The counter will count up if the  Pseudowire ID differ from RxEthPwid value defined in Ethernet Receive Pseudowire Identification Control register."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxUdpErrorPk"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_UDP_Port_Error_Packet_Counter_ro._RxUdpErrorPk()
            return allFields

    class _Ethernet_Receive_UDP_Port_Error_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive UDP Port Error Packet Counter"
    
        def description(self):
            return "Count number of UDP port error packets detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000007
            
        def endAddress(self):
            return 0xffffffff

        class _RxUdpErrorPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxUdpErrorPk"
            
            def description(self):
                return "This counter count the number of packets with UDP port error. This counter just count up when FCS, MAC Header and PSN Header (exactly Ipv4 and Ipv6 PSN header) are already checked good. If RxEthUdpMode defined in Global Ethernet Control register is not set, AF6FHW0013 will automatically search for UDP port number with value 0x085E, the remaining UDP port number will be Pseudowire ID. The counter will count up if the  Pseudowire ID differ from RxEthPwid value defined in Ethernet Receive Pseudowire Identification Control register. If  RxEthUdpMode is set, then depending on the  RxEthUdpSel (also  defined in Global Ethernet Control register), UDP source port number or UDP destination port number is used for Pseudowire ID. The counter will count up if the  Pseudowire ID differ from RxEthPwid value defined in Ethernet Receive Pseudowire Identification Control register."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxUdpErrorPk"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_UDP_Port_Error_Packet_Counter_rc._RxUdpErrorPk()
            return allFields

    class _Ethernet_Receive_OAM_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive OAM Packet Counter"
    
        def description(self):
            return "Count number of classified OAM packets (ARP, ICMP,...) received from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000e
            
        def endAddress(self):
            return 0xffffffff

        class _RxOamPkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxOamPkt"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxOamPkt"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_OAM_Packet_Counter_ro._RxOamPkt()
            return allFields

    class _Ethernet_Receive_OAM_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive OAM Packet Counter"
    
        def description(self):
            return "Count number of classified OAM packets (ARP, ICMP,...) received from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000f
            
        def endAddress(self):
            return 0xffffffff

        class _RxOamPkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxOamPkt"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxOamPkt"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_OAM_Packet_Counter_rc._RxOamPkt()
            return allFields

    class _Ethernet_Transmit_OAM_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit OAM Packet Counter"
    
        def description(self):
            return "Count number of transmitted OAM packets (ARP, ICMP,...) to Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _TxOamPkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxOamPkt"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxOamPkt"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_OAM_Packet_Counter_ro._TxOamPkt()
            return allFields

    class _Ethernet_Transmit_OAM_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit OAM Packet Counter"
    
        def description(self):
            return "Count number of transmitted OAM packets (ARP, ICMP,...) to Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0xffffffff

        class _TxOamPkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxOamPkt"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxOamPkt"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_OAM_Packet_Counter_rc._TxOamPkt()
            return allFields

    class _Ethernet_Receive_Total_Byte_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive Total Byte Counter"
    
        def description(self):
            return "Count total number of bytes received from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000012
            
        def endAddress(self):
            return 0xffffffff

        class _RxByte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxByte"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxByte"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_Total_Byte_Counter_ro._RxByte()
            return allFields

    class _Ethernet_Receive_Total_Byte_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive Total Byte Counter"
    
        def description(self):
            return "Count total number of bytes received from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000013
            
        def endAddress(self):
            return 0xffffffff

        class _RxByte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxByte"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxByte"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_Total_Byte_Counter_rc._RxByte()
            return allFields

    class _Ethernet_Receive_Total_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive Total Packet Counter"
    
        def description(self):
            return "Count total number of packets received from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000014
            
        def endAddress(self):
            return 0xffffffff

        class _RxPacket(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPacket"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPacket"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_Total_Packet_Counter_ro._RxPacket()
            return allFields

    class _Ethernet_Receive_Total_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive Total Packet Counter"
    
        def description(self):
            return "Count total number of packets received from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000015
            
        def endAddress(self):
            return 0xffffffff

        class _RxPacket(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPacket"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPacket"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_Total_Packet_Counter_rc._RxPacket()
            return allFields

    class _Ethernet_Receive_Classified_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive Classified Packet Counter"
    
        def description(self):
            return "Count number of classified packets (PW packets plus OAM packets) from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000016
            
        def endAddress(self):
            return 0xffffffff

        class _RxClassifyPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxClassifyPk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxClassifyPk"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_Classified_Packet_Counter_ro._RxClassifyPk()
            return allFields

    class _Ethernet_Receive_Classified_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive Classified Packet Counter"
    
        def description(self):
            return "Count number of classified packets (PW packets plus OAM packets) from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000017
            
        def endAddress(self):
            return 0xffffffff

        class _RxClassifyPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxClassifyPk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxClassifyPk"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_Classified_Packet_Counter_rc._RxClassifyPk()
            return allFields

    class _Ethernet_Transmit_Byte_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit Byte Counter"
    
        def description(self):
            return "Count total number of  bytes transmitted to Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000018
            
        def endAddress(self):
            return 0xffffffff

        class _TxByte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxByte"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxByte"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_Byte_Counter_ro._TxByte()
            return allFields

    class _Ethernet_Transmit_Byte_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit Byte Counter"
    
        def description(self):
            return "Count total number of  bytes transmitted to Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000019
            
        def endAddress(self):
            return 0xffffffff

        class _TxByte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxByte"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxByte"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_Byte_Counter_rc._TxByte()
            return allFields

    class _Ethernet_Transmit_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit Packet Counter"
    
        def description(self):
            return "Count total number of  packets transmitted to Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000001a
            
        def endAddress(self):
            return 0xffffffff

        class _TxPacket(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPacket"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPacket"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_Packet_Counter_ro._TxPacket()
            return allFields

    class _Ethernet_Transmit_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit Packet Counter"
    
        def description(self):
            return "Count total number of  packets transmitted to Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000001b
            
        def endAddress(self):
            return 0xffffffff

        class _TxPacket(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPacket"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPacket"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_Packet_Counter_rc._TxPacket()
            return allFields

    class _Pseudowire_Transmit_Good_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Good Packet Counter"
    
        def description(self):
            return "Count number of CESoETH packets transmitted to Ethernet side."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x011800 + PwId"
            
        def startAddress(self):
            return 0x00011800
            
        def endAddress(self):
            return 0x000118ff

        class _TxPwGoodPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPwGoodPk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPwGoodPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Transmit_Good_Packet_Counter_ro._TxPwGoodPk()
            return allFields

    class _Pseudowire_Transmit_Good_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Good Packet Counter"
    
        def description(self):
            return "Count number of CESoETH packets transmitted to Ethernet side."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x011000 + PwId"
            
        def startAddress(self):
            return 0x00011000
            
        def endAddress(self):
            return 0x000110ff

        class _TxPwGoodPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPwGoodPk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPwGoodPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Transmit_Good_Packet_Counter_rc._TxPwGoodPk()
            return allFields

    class _Pseudowire_Receive_Payload_Octet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Payload Octet Counter"
    
        def description(self):
            return "Count number of pseudowire payload octet received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x012800 + PwId"
            
        def startAddress(self):
            return 0x00012800
            
        def endAddress(self):
            return 0x000128ff

        class _RxPwPayOct(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwPayOct"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwPayOct"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Payload_Octet_Counter_ro._RxPwPayOct()
            return allFields

    class _Pseudowire_Receive_Payload_Octet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Payload Octet Counter"
    
        def description(self):
            return "Count number of pseudowire payload octet received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x012000 + PwId"
            
        def startAddress(self):
            return 0x00012000
            
        def endAddress(self):
            return 0x000120ff

        class _RxPwPayOct(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwPayOct"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwPayOct"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Payload_Octet_Counter_rc._RxPwPayOct()
            return allFields

    class _Pseudowire_Receive_Good_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Good Packet Counter"
    
        def description(self):
            return "Count number of CESoETH packets received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x013800 + PwId"
            
        def startAddress(self):
            return 0x00013800
            
        def endAddress(self):
            return 0x000138ff

        class _RxPwGoodPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwGoodPk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwGoodPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Good_Packet_Counter_ro._RxPwGoodPk()
            return allFields

    class _Pseudowire_Receive_Good_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Good Packet Counter"
    
        def description(self):
            return "Count number of CESoETH packets received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x013000 + PwId"
            
        def startAddress(self):
            return 0x00013000
            
        def endAddress(self):
            return 0x000130ff

        class _RxPwGoodPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwGoodPk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwGoodPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Good_Packet_Counter_rc._RxPwGoodPk()
            return allFields

    class _Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Reorder Drop Packet Counter"
    
        def description(self):
            return "Count number of dropped packet (too late or too soon) by reorder function"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x014800 + PwId"
            
        def startAddress(self):
            return 0x00014800
            
        def endAddress(self):
            return 0x000148ff

        class _RxPwReorDropPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwReorDropPk"
            
            def description(self):
                return "Reorder Dropped Packet Counter count up when a packet comes too soon or too late compared with expected sequence number"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwReorDropPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro._RxPwReorDropPk()
            return allFields

    class _Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Reorder Drop Packet Counter"
    
        def description(self):
            return "Count number of dropped packet (too late or too soon) by reorder function"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x014000 + PwId"
            
        def startAddress(self):
            return 0x00014000
            
        def endAddress(self):
            return 0x000140ff

        class _RxPwReorDropPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwReorDropPk"
            
            def description(self):
                return "Reorder Dropped Packet Counter count up when a packet comes too soon or too late compared with expected sequence number"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwReorDropPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc._RxPwReorDropPk()
            return allFields

    class _Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Reorder Out Of Sequence  Packet Counter"
    
        def description(self):
            return "Count number of frames received that are out-of-sequence, but successfully re-ordered"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x015800 + PwId"
            
        def startAddress(self):
            return 0x00015800
            
        def endAddress(self):
            return 0x000158ff

        class _RxPwReorOSeqPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwReorOSeqPk"
            
            def description(self):
                return "This counter count the number of packets that are out-of-sequence but successfully re-ordered."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwReorOSeqPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_ro._RxPwReorOSeqPk()
            return allFields

    class _Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Reorder Out Of Sequence  Packet Counter"
    
        def description(self):
            return "Count number of frames received that are out-of-sequence, but successfully re-ordered"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x015000 + PwId"
            
        def startAddress(self):
            return 0x00015000
            
        def endAddress(self):
            return 0x000150ff

        class _RxPwReorOSeqPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwReorOSeqPk"
            
            def description(self):
                return "This counter count the number of packets that are out-of-sequence but successfully re-ordered."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwReorOSeqPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Reorder_Out_Of_Sequence__Packet_Counter_rc._RxPwReorOSeqPk()
            return allFields

    class _Pseudowire_Receive_LOFS_Transition_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive LOFS Transition Counter"
    
        def description(self):
            return "Count number of transitions from the normal to the loss of frames state (LOFS)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x016800 + PwId"
            
        def startAddress(self):
            return 0x00016800
            
        def endAddress(self):
            return 0x000168ff

        class _RxPwLofsEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwLofsEvent"
            
            def description(self):
                return "This counter count the number of transitions from normal sate to loss of frame state"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwLofsEvent"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_LOFS_Transition_Counter_ro._RxPwLofsEvent()
            return allFields

    class _Pseudowire_Receive_LOFS_Transition_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive LOFS Transition Counter"
    
        def description(self):
            return "Count number of transitions from the normal to the loss of frames state (LOFS)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x016000 + PwId"
            
        def startAddress(self):
            return 0x00016000
            
        def endAddress(self):
            return 0x000160ff

        class _RxPwLofsEvent(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwLofsEvent"
            
            def description(self):
                return "This counter count the number of transitions from normal sate to loss of frame state"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwLofsEvent"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_LOFS_Transition_Counter_rc._RxPwLofsEvent()
            return allFields

    class _Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Jitter Buffer Overrun Event Counter"
    
        def description(self):
            return "Count number of jitter buffer overrun events"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x017800 + PwId"
            
        def startAddress(self):
            return 0x00017800
            
        def endAddress(self):
            return 0x000178ff

        class _RxOverrunPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxOverrunPk"
            
            def description(self):
                return "Counter value."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxOverrunPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_ro._RxOverrunPk()
            return allFields

    class _Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Jitter Buffer Overrun Event Counter"
    
        def description(self):
            return "Count number of jitter buffer overrun events"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x017000 + PwId"
            
        def startAddress(self):
            return 0x00017000
            
        def endAddress(self):
            return 0x000170ff

        class _RxOverrunPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxOverrunPk"
            
            def description(self):
                return "Counter value."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxOverrunPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Jitter_Buffer_Overrun_Event_Counter_rc._RxOverrunPk()
            return allFields

    class _Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Reorder Lost Packet Counter"
    
        def description(self):
            return "Count number of lost packets detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x018800 + PwId"
            
        def startAddress(self):
            return 0x00018800
            
        def endAddress(self):
            return 0x000188ff

        class _RxPwReorLostPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwReorLostPk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwReorLostPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro._RxPwReorLostPk()
            return allFields

    class _Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Reorder Lost Packet Counter"
    
        def description(self):
            return "Count number of lost packets detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x018000 + PwId"
            
        def startAddress(self):
            return 0x00018000
            
        def endAddress(self):
            return 0x000180ff

        class _RxPwReorLostPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwReorLostPk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwReorLostPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc._RxPwReorLostPk()
            return allFields

    class _Pseudowire_Receive_Malform_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Malform Packet Counter"
    
        def description(self):
            return "Count number of malformed packets received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x019800 + PwId"
            
        def startAddress(self):
            return 0x00019800
            
        def endAddress(self):
            return 0x000198ff

        class _RxPwMalformPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwMalformPk"
            
            def description(self):
                return "This counter count the number of pseudowire good packets that have the length differ than PwPayloadLen defined in Pseudowire Payload Control register or PT value in RTP header mismatch"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwMalformPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Malform_Packet_Counter_ro._RxPwMalformPk()
            return allFields

    class _Pseudowire_Receive_Malform_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Malform Packet Counter"
    
        def description(self):
            return "Count number of malformed packets received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x019000 + PwId"
            
        def startAddress(self):
            return 0x00019000
            
        def endAddress(self):
            return 0x000190ff

        class _RxPwMalformPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwMalformPk"
            
            def description(self):
                return "This counter count the number of pseudowire good packets that have the length differ than PwPayloadLen defined in Pseudowire Payload Control register or PT value in RTP header mismatch"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwMalformPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Malform_Packet_Counter_rc._RxPwMalformPk()
            return allFields

    class _Pseudowire_Receive_Stray_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Stray Packet Counter"
    
        def description(self):
            return "Count number of stray packets received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x010800 + PwId"
            
        def startAddress(self):
            return 0x00010800
            
        def endAddress(self):
            return 0x000108ff

        class _RxPwStrayPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwStrayPk"
            
            def description(self):
                return "This counter count the number of pseudowire good packets that have the RTP SSRC field mismatch"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwStrayPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Stray_Packet_Counter_ro._RxPwStrayPk()
            return allFields

    class _Pseudowire_Receive_Stray_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Stray Packet Counter"
    
        def description(self):
            return "Count number of stray packets received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x010000 + PwId"
            
        def startAddress(self):
            return 0x00010000
            
        def endAddress(self):
            return 0x000100ff

        class _RxPwStrayPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwStrayPk"
            
            def description(self):
                return "This counter count the number of pseudowire good packets that have the RTP SSRC field mismatch"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwStrayPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Stray_Packet_Counter_rc._RxPwStrayPk()
            return allFields

    class _Pseudowire_Transmit_L_Bit_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit L Bit Packet Counter"
    
        def description(self):
            return "Count number of good pseudowire packets transmitted with L bit set"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x01A800 + PwId"
            
        def startAddress(self):
            return 0x0001a800
            
        def endAddress(self):
            return 0x0001a8ff

        class _TxPwLbitPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPwLbitPk"
            
            def description(self):
                return "This counter counts the number of packets with L bit set at the PSN transmit side."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPwLbitPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Transmit_L_Bit_Packet_Counter_ro._TxPwLbitPk()
            return allFields

    class _Pseudowire_Transmit_L_Bit_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit L Bit Packet Counter"
    
        def description(self):
            return "Count number of good pseudowire packets transmitted with L bit set"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x01A000 + PwId"
            
        def startAddress(self):
            return 0x0001a000
            
        def endAddress(self):
            return 0x0001a0ff

        class _TxPwLbitPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPwLbitPk"
            
            def description(self):
                return "This counter counts the number of packets with L bit set at the PSN transmit side."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPwLbitPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Transmit_L_Bit_Packet_Counter_rc._TxPwLbitPk()
            return allFields

    class _Pseudowire_Receive_L_Bit_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive L Bit Packet Counter"
    
        def description(self):
            return "Count number of good pseudowire packets received with L bit set"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x01B800 + PwId"
            
        def startAddress(self):
            return 0x0001b800
            
        def endAddress(self):
            return 0x0001b8ff

        class _RxPwMalformPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwMalformPk"
            
            def description(self):
                return "This counter count the number of good packets received with L bit set"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwMalformPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_L_Bit_Packet_Counter_ro._RxPwMalformPk()
            return allFields

    class _Pseudowire_Receive_L_Bit_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive L Bit Packet Counter"
    
        def description(self):
            return "Count number of good pseudowire packets received with L bit set"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x01B000 + PwId"
            
        def startAddress(self):
            return 0x0001b000
            
        def endAddress(self):
            return 0x0001b0ff

        class _RxPwMalformPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwMalformPk"
            
            def description(self):
                return "This counter count the number of good packets received with L bit set"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwMalformPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_L_Bit_Packet_Counter_rc._RxPwMalformPk()
            return allFields

    class _Pseudowire_Transmit_R_Bit_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit R Bit Packet Counter"
    
        def description(self):
            return "Count number of good pseudowire packets transmitted with R bit set"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x01C800 + PwId"
            
        def startAddress(self):
            return 0x0001c800
            
        def endAddress(self):
            return 0x0001c8ff

        class _TxPwLbitPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPwLbitPk"
            
            def description(self):
                return "This counter count the number of packets with R bit set at the PSN transmit side."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPwLbitPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Transmit_R_Bit_Packet_Counter_ro._TxPwLbitPk()
            return allFields

    class _Pseudowire_Transmit_R_Bit_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit R Bit Packet Counter"
    
        def description(self):
            return "Count number of good pseudowire packets transmitted with R bit set"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x01C000 + PwId"
            
        def startAddress(self):
            return 0x0001c000
            
        def endAddress(self):
            return 0x0001c0ff

        class _TxPwLbitPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPwLbitPk"
            
            def description(self):
                return "This counter count the number of packets with R bit set at the PSN transmit side."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPwLbitPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Transmit_R_Bit_Packet_Counter_rc._TxPwLbitPk()
            return allFields

    class _Pseudowire_Receive_R_Bit_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive R Bit Packet Counter"
    
        def description(self):
            return "Count number of good pseudowire packets received with R bit set"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x01D800 + PwId"
            
        def startAddress(self):
            return 0x0001d800
            
        def endAddress(self):
            return 0x0001d8ff

        class _RxPwMalformPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwMalformPk"
            
            def description(self):
                return "This counter count the number of good packets received with R bit set"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwMalformPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_R_Bit_Packet_Counter_ro._RxPwMalformPk()
            return allFields

    class _Pseudowire_Receive_R_Bit_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive R Bit Packet Counter"
    
        def description(self):
            return "Count number of good pseudowire packets received with R bit set"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x01D000 + PwId"
            
        def startAddress(self):
            return 0x0001d000
            
        def endAddress(self):
            return 0x0001d0ff

        class _RxPwMalformPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwMalformPk"
            
            def description(self):
                return "This counter count the number of good packets received with R bit set"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwMalformPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_R_Bit_Packet_Counter_rc._RxPwMalformPk()
            return allFields

    class _Pseudowire_Transmit_Payload_Octet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Payload Octet Counter"
    
        def description(self):
            return "Count number of pseudowire payload octet transmitted"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x01E800 + PwId"
            
        def startAddress(self):
            return 0x0001e800
            
        def endAddress(self):
            return 0x0001e8ff

        class _TxPwPayOct(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPwPayOct"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPwPayOct"] = _AF6CCI0021_RD_PMC._Pseudowire_Transmit_Payload_Octet_Counter_ro._TxPwPayOct()
            return allFields

    class _Pseudowire_Transmit_Payload_Octet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Payload Octet Counter"
    
        def description(self):
            return "Count number of pseudowire payload octet transmitted"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x01E000 + PwId"
            
        def startAddress(self):
            return 0x0001e000
            
        def endAddress(self):
            return 0x0001e0ff

        class _TxPwPayOct(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPwPayOct"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPwPayOct"] = _AF6CCI0021_RD_PMC._Pseudowire_Transmit_Payload_Octet_Counter_rc._TxPwPayOct()
            return allFields

    class _Pseudowire_Transmit_Mbit_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Mbit Counter"
    
        def description(self):
            return "Count number of pseudowire Mbit packet transmitted"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x020800 + PwId"
            
        def startAddress(self):
            return 0x00020800
            
        def endAddress(self):
            return 0x000208ff

        class _TxPwMbitPkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPwMbitPkt"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPwMbitPkt"] = _AF6CCI0021_RD_PMC._Pseudowire_Transmit_Mbit_Counter_ro._TxPwMbitPkt()
            return allFields

    class _Pseudowire_Transmit_Mbit_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Mbit Counter"
    
        def description(self):
            return "Count number of pseudowire Mbit packet transmitted"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x020000 + PwId"
            
        def startAddress(self):
            return 0x00020000
            
        def endAddress(self):
            return 0x000200ff

        class _TxPwMbitPkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPwMbitPkt"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPwMbitPkt"] = _AF6CCI0021_RD_PMC._Pseudowire_Transmit_Mbit_Counter_rc._TxPwMbitPkt()
            return allFields

    class _Pseudowire_Receive_Mbit_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Mbit Counter"
    
        def description(self):
            return "Count number of pseudowire Mbit packet transmitted"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x021800 + PwId"
            
        def startAddress(self):
            return 0x00021800
            
        def endAddress(self):
            return 0x000218ff

        class _RxPwMbitPkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwMbitPkt"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwMbitPkt"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Mbit_Counter_ro._RxPwMbitPkt()
            return allFields

    class _Pseudowire_Receive_Mbit_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Mbit Counter"
    
        def description(self):
            return "Count number of pseudowire Mbit packet transmitted"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x021000 + PwId"
            
        def startAddress(self):
            return 0x00021000
            
        def endAddress(self):
            return 0x000210ff

        class _RxPwMbitPkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwMbitPkt"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwMbitPkt"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Mbit_Counter_rc._RxPwMbitPkt()
            return allFields

    class _Pseudowire_Receive_Duplicate_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Duplicate Counter"
    
        def description(self):
            return "Count number of pseudowire Duplicate packet received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x022800 + PwId"
            
        def startAddress(self):
            return 0x00022800
            
        def endAddress(self):
            return 0x000228ff

        class _RxPwDuplicatePkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwDuplicatePkt"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwDuplicatePkt"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Duplicate_Counter_ro._RxPwDuplicatePkt()
            return allFields

    class _Pseudowire_Receive_Duplicate_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Duplicate Counter"
    
        def description(self):
            return "Count number of pseudowire Duplicate packet received"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x022000 + PwId"
            
        def startAddress(self):
            return 0x00022000
            
        def endAddress(self):
            return 0x000220ff

        class _RxPwDuplicatePkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxPwDuplicatePkt"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxPwDuplicatePkt"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Duplicate_Counter_rc._RxPwDuplicatePkt()
            return allFields

    class _Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Jitter Buffer Underrun Event Counter"
    
        def description(self):
            return "Count number of jitter buffer transition events from normal to underrun status"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x01F800 + PwId"
            
        def startAddress(self):
            return 0x0001f800
            
        def endAddress(self):
            return 0x0001f8ff

        class _RxUnderrunPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxUnderrunPk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxUnderrunPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_ro._RxUnderrunPk()
            return allFields

    class _Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Receive Jitter Buffer Underrun Event Counter"
    
        def description(self):
            return "Count number of jitter buffer transition events from normal to underrun status"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x01F000 + PwId"
            
        def startAddress(self):
            return 0x0001f000
            
        def endAddress(self):
            return 0x0001f0ff

        class _RxUnderrunPk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxUnderrunPk"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxUnderrunPk"] = _AF6CCI0021_RD_PMC._Pseudowire_Receive_Jitter_Buffer_Underrun_Event_Counter_rc._RxUnderrunPk()
            return allFields

    class _Ethernet_and_Pseudowire_Counter_Sticky(AtRegister.AtRegister):
        def name(self):
            return "Ethernet and Pseudowire Counter Sticky"
    
        def description(self):
            return "This register sticky AF6FHW0013 error counter alarms. This register must be cleared by written to all one value after power up."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000008
            
        def endAddress(self):
            return 0xffffffff

        class _UnderrunEvt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "UnderrunEvt"
            
            def description(self):
                return "This bit set when Pseudowire Receive Jitter Buffer Underrun Event Counter counts up."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FcsErrEvt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "FcsErrEvt"
            
            def description(self):
                return "This bit set when Ethernet Receive FCS Error Packet Counter counts up."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EthErrEvt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "EthErrEvt"
            
            def description(self):
                return "This bit set when Ethernet Receive Header Error Packet Counter counts up."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PsnErrEvt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PsnErrEvt"
            
            def description(self):
                return "This bit set when Ethernet Receive PSN Header Error Packet Counter counts up."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UdpErrEvt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "UdpErrEvt"
            
            def description(self):
                return "This bit set when Ethernet Receive UDP Port Error Packet Counter counts up."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ReorDropEvt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "ReorDropEvt"
            
            def description(self):
                return "This bit set when Pseudowire Receive Reorder Drop Packet Counter counts up."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ReorOSeqEvt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "ReorOSeqEvt"
            
            def description(self):
                return "This bit set when Pseudowire Receive Reorder Out Of Sequence Packet Counter counts up."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LofsEvt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "LofsEvt"
            
            def description(self):
                return "This bit set when Pseudowire Receive LOFS Transition Counter counts up."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OverrunEvt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "OverrunEvt"
            
            def description(self):
                return "This bit set when Pseudowire Receive Jitter Buffer Overrun Packet Counter counts up."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ReorLostEvt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "ReorLostEvt"
            
            def description(self):
                return "This bit set when Pseudowire Receive Reorder Lost Packet Counter counts up."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MalformEvt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "MalformEvt"
            
            def description(self):
                return "This bit set when Pseudowire Receive Malform Packet Counter counts up."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxLbitEvt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "TxLbitEvt"
            
            def description(self):
                return "This bit set when Pseudowire Pseudowire Transmit L Bit Packet Counter counts up."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxLbitEvt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RxLbitEvt"
            
            def description(self):
                return "This bit set when Pseudowire Pseudowire Receive L Bit Packet Counter counts up."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxRbitEvt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "TxRbitEvt"
            
            def description(self):
                return "This bit set when Pseudowire Pseudowire Transmit R Bit Packet Counter counts up."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxRbitEvt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxRbitEvt"
            
            def description(self):
                return "This bit set when Pseudowire Pseudowire Receive R Bit Packet Counter counts up."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["UnderrunEvt"] = _AF6CCI0021_RD_PMC._Ethernet_and_Pseudowire_Counter_Sticky._UnderrunEvt()
            allFields["FcsErrEvt"] = _AF6CCI0021_RD_PMC._Ethernet_and_Pseudowire_Counter_Sticky._FcsErrEvt()
            allFields["EthErrEvt"] = _AF6CCI0021_RD_PMC._Ethernet_and_Pseudowire_Counter_Sticky._EthErrEvt()
            allFields["PsnErrEvt"] = _AF6CCI0021_RD_PMC._Ethernet_and_Pseudowire_Counter_Sticky._PsnErrEvt()
            allFields["UdpErrEvt"] = _AF6CCI0021_RD_PMC._Ethernet_and_Pseudowire_Counter_Sticky._UdpErrEvt()
            allFields["ReorDropEvt"] = _AF6CCI0021_RD_PMC._Ethernet_and_Pseudowire_Counter_Sticky._ReorDropEvt()
            allFields["ReorOSeqEvt"] = _AF6CCI0021_RD_PMC._Ethernet_and_Pseudowire_Counter_Sticky._ReorOSeqEvt()
            allFields["LofsEvt"] = _AF6CCI0021_RD_PMC._Ethernet_and_Pseudowire_Counter_Sticky._LofsEvt()
            allFields["OverrunEvt"] = _AF6CCI0021_RD_PMC._Ethernet_and_Pseudowire_Counter_Sticky._OverrunEvt()
            allFields["ReorLostEvt"] = _AF6CCI0021_RD_PMC._Ethernet_and_Pseudowire_Counter_Sticky._ReorLostEvt()
            allFields["MalformEvt"] = _AF6CCI0021_RD_PMC._Ethernet_and_Pseudowire_Counter_Sticky._MalformEvt()
            allFields["TxLbitEvt"] = _AF6CCI0021_RD_PMC._Ethernet_and_Pseudowire_Counter_Sticky._TxLbitEvt()
            allFields["RxLbitEvt"] = _AF6CCI0021_RD_PMC._Ethernet_and_Pseudowire_Counter_Sticky._RxLbitEvt()
            allFields["TxRbitEvt"] = _AF6CCI0021_RD_PMC._Ethernet_and_Pseudowire_Counter_Sticky._TxRbitEvt()
            allFields["RxRbitEvt"] = _AF6CCI0021_RD_PMC._Ethernet_and_Pseudowire_Counter_Sticky._RxRbitEvt()
            return allFields

    class _Receive_Ethernet_Header_Error_Sticky(AtRegister.AtRegister):
        def name(self):
            return "Receive Ethernet Header Error Sticky"
    
        def description(self):
            return "This register stores AF6FHW0013 Jitter Buffer empty alarms which will cause interrupt to CPU. This register must be cleared by written to all one value after power up."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000000a
            
        def endAddress(self):
            return 0xffffffff

        class _RxEthUdpLenErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "RxEthUdpLenErr"
            
            def description(self):
                return "Set 1 to indicate UDP length field error."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthUdpPortErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "RxEthUdpPortErr"
            
            def description(self):
                return "Set 1 to indicate UDP port differ from pseudowire identification descibed in Classify Pseudowire Identification Control register."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthMplsOutLabelErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "RxEthMplsOutLabelErr"
            
            def description(self):
                return "Set 1 to indicate MPLS outer label differ from RxPsnMplsOutLabel field descibed in Classify Global PSN Control register."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthMplsInLabelErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "RxEthMplsInLabelErr"
            
            def description(self):
                return "Set 1 to indicate MPLS inner label differ from pseudowire identification descibed in Classify Pseudowire Identification Control register."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthMefEcidLabelErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "RxEthMefEcidLabelErr"
            
            def description(self):
                return "Set 1 to indicate MEF8 ECID differ from pseudowire identification descibed in Classify Pseudowire Identification Control register."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthIpv6HdrErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RxEthIpv6HdrErr"
            
            def description(self):
                return "Set 1 to indicate Ipv6 header error"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthIpv4HdrErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxEthIpv4HdrErr"
            
            def description(self):
                return "Set 1 to indicate Ipv4 header error"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthOversize(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "RxEthOversize"
            
            def description(self):
                return "Set 1 to indicate an oversize packet received"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthUndersize(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RxEthUndersize"
            
            def description(self):
                return "Set 1 to indicate an undersize packet received"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthTypeErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "RxEthTypeErr"
            
            def description(self):
                return "Set 1 to indicate reception of packets with Ethernet Type field not supported by AF6FHW0013"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxEthFcsErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxEthFcsErr"
            
            def description(self):
                return "Set 1 to indicate reception of packets with FCS error or Ethernet PHY error"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxEthUdpLenErr"] = _AF6CCI0021_RD_PMC._Receive_Ethernet_Header_Error_Sticky._RxEthUdpLenErr()
            allFields["RxEthUdpPortErr"] = _AF6CCI0021_RD_PMC._Receive_Ethernet_Header_Error_Sticky._RxEthUdpPortErr()
            allFields["RxEthMplsOutLabelErr"] = _AF6CCI0021_RD_PMC._Receive_Ethernet_Header_Error_Sticky._RxEthMplsOutLabelErr()
            allFields["RxEthMplsInLabelErr"] = _AF6CCI0021_RD_PMC._Receive_Ethernet_Header_Error_Sticky._RxEthMplsInLabelErr()
            allFields["RxEthMefEcidLabelErr"] = _AF6CCI0021_RD_PMC._Receive_Ethernet_Header_Error_Sticky._RxEthMefEcidLabelErr()
            allFields["RxEthIpv6HdrErr"] = _AF6CCI0021_RD_PMC._Receive_Ethernet_Header_Error_Sticky._RxEthIpv6HdrErr()
            allFields["RxEthIpv4HdrErr"] = _AF6CCI0021_RD_PMC._Receive_Ethernet_Header_Error_Sticky._RxEthIpv4HdrErr()
            allFields["RxEthOversize"] = _AF6CCI0021_RD_PMC._Receive_Ethernet_Header_Error_Sticky._RxEthOversize()
            allFields["RxEthUndersize"] = _AF6CCI0021_RD_PMC._Receive_Ethernet_Header_Error_Sticky._RxEthUndersize()
            allFields["RxEthTypeErr"] = _AF6CCI0021_RD_PMC._Receive_Ethernet_Header_Error_Sticky._RxEthTypeErr()
            allFields["RxEthFcsErr"] = _AF6CCI0021_RD_PMC._Receive_Ethernet_Header_Error_Sticky._RxEthFcsErr()
            return allFields

    class _Counter_Per_Alarm_Interrupt_Enable_Control(AtRegister.AtRegister):
        def name(self):
            return "Counter Per Alarm Interrupt Enable Control"
    
        def description(self):
            return "This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x001000 +  GrpID*32 + BitID"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0x000010ff

        class _StrayStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "StrayStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable Stray packet event to generate an interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MalformStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "MalformStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable Malform packet event to generate an interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MbitStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "MbitStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable Mbit packet event to generate an interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RbitStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RbitStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable Rbit packet event to generate an interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LostPkStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "LostPkStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable lost of packet event to generate an interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UnderrunStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "UnderrunStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable change jitter buffer state event from normal to underrun and vice versa in the related pseudowire to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OverrunStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "OverrunStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable change jitter buffer state event from normal to overrun and vice versa in the related pseudowire to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LofsStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "LofsStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable change lost of frame state(LOFS) event from normal to LOFS and vice versa in the related pseudowire to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LbitStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LbitStateChgIntrEn"
            
            def description(self):
                return "Set 1 to enable Lbit packet event to generate an interrupt"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StrayStateChgIntrEn"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Interrupt_Enable_Control._StrayStateChgIntrEn()
            allFields["MalformStateChgIntrEn"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Interrupt_Enable_Control._MalformStateChgIntrEn()
            allFields["MbitStateChgIntrEn"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Interrupt_Enable_Control._MbitStateChgIntrEn()
            allFields["RbitStateChgIntrEn"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Interrupt_Enable_Control._RbitStateChgIntrEn()
            allFields["LostPkStateChgIntrEn"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Interrupt_Enable_Control._LostPkStateChgIntrEn()
            allFields["UnderrunStateChgIntrEn"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Interrupt_Enable_Control._UnderrunStateChgIntrEn()
            allFields["OverrunStateChgIntrEn"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Interrupt_Enable_Control._OverrunStateChgIntrEn()
            allFields["LofsStateChgIntrEn"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Interrupt_Enable_Control._LofsStateChgIntrEn()
            allFields["LbitStateChgIntrEn"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Interrupt_Enable_Control._LbitStateChgIntrEn()
            return allFields

    class _Counter_Per_Alarm_Interrupt_Status(AtRegister.AtRegister):
        def name(self):
            return "Counter Per Alarm Interrupt Status"
    
        def description(self):
            return "This is the per Alarm interrupt status of pseudowires. Each register is used to store 5 sticky bits for 5 alarms in pseudowires"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x001100 +  GrpID*32 + BitID"
            
        def startAddress(self):
            return 0x00001100
            
        def endAddress(self):
            return 0x000011ff

        class _StrayStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "StrayStateChgIntrEn"
            
            def description(self):
                return "Set 1 when a Stray packet event is detected in the pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MalformStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "MalformStateChgIntrEn"
            
            def description(self):
                return "Set 1 when a Malform packet event is detected in the pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MbitStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "MbitStateChgIntrEn"
            
            def description(self):
                return "Set 1 when a Mbit packet event is detected in the pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RbitStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RbitStateChgIntrEn"
            
            def description(self):
                return "Set 1 when a Rbit packet event is detected in the pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LostPkStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "LostPkStateChgIntrEn"
            
            def description(self):
                return "Set 1 when a lost of packet event is detected in the pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UnderrunStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "UnderrunStateChgIntrEn"
            
            def description(self):
                return "Set 1 when there is a change in jitter buffer underrun state  in the related pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OverrunStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "OverrunStateChgIntrEn"
            
            def description(self):
                return "Set 1 when there is a change in jitter buffer overrun state in the related pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LofsStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "LofsStateChgIntrEn"
            
            def description(self):
                return "Set 1 when there is a change in lost of frame state(LOFS) in the related pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LbitStateChgIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LbitStateChgIntrEn"
            
            def description(self):
                return "Set 1 when a Lbit packet event is detected in the pseudowire"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StrayStateChgIntrEn"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Interrupt_Status._StrayStateChgIntrEn()
            allFields["MalformStateChgIntrEn"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Interrupt_Status._MalformStateChgIntrEn()
            allFields["MbitStateChgIntrEn"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Interrupt_Status._MbitStateChgIntrEn()
            allFields["RbitStateChgIntrEn"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Interrupt_Status._RbitStateChgIntrEn()
            allFields["LostPkStateChgIntrEn"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Interrupt_Status._LostPkStateChgIntrEn()
            allFields["UnderrunStateChgIntrEn"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Interrupt_Status._UnderrunStateChgIntrEn()
            allFields["OverrunStateChgIntrEn"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Interrupt_Status._OverrunStateChgIntrEn()
            allFields["LofsStateChgIntrEn"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Interrupt_Status._LofsStateChgIntrEn()
            allFields["LbitStateChgIntrEn"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Interrupt_Status._LbitStateChgIntrEn()
            return allFields

    class _Counter_Per_Alarm_Current_Status(AtRegister.AtRegister):
        def name(self):
            return "Counter Per Alarm Current Status"
    
        def description(self):
            return "This is the per Alarm current status of pseudowires. Each register is used to store 4 current status of 4 alarms in pseudowires"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x001200 +  GrpID*32 + BitID"
            
        def startAddress(self):
            return 0x00001200
            
        def endAddress(self):
            return 0x000012ff

        class _StrayCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "StrayCurStatus"
            
            def description(self):
                return "Stray current status in the related pseudowire. When it changes from 0 to 1 or vice versa, the  MalformStateChgIntr bit in the Counter Per Alarm Interrupt Status register of the related pseudowire is set or clear."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MalformCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "MalformCurStatus"
            
            def description(self):
                return "Malform current status in the related pseudowire. When it changes from 0 to 1 or vice versa, the  MalformStateChgIntr bit in the Counter Per Alarm Interrupt Status register of the related pseudowire is set or clear."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MbitCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "MbitCurStatus"
            
            def description(self):
                return "Mbits current status in the related pseudowire. When it changes from 0 to 1 or vice versa, the  MbitStateChgIntr bit in the Counter Per Alarm Interrupt Status register of the related pseudowire is set or clear."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RbitCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RbitCurStatus"
            
            def description(self):
                return "Rbits current status in the related pseudowire. When it changes from 0 to 1 or vice versa, the  RbitStateChgIntr bit in the Counter Per Alarm Interrupt Status register of the related pseudowire is set or clear."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LostPkCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "LostPkCurStatus"
            
            def description(self):
                return "Lost of packet current status in the related pseudowire. When it changes from 0 to 1 or vice versa, the  LostStateChgIntr bit in the Counter Per Alarm Interrupt Status register of the related pseudowire is set or clear."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _UnderrunCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "UnderrunCurStatus"
            
            def description(self):
                return "Jitter buffer underrun current status in the related pseudowire. When it changes from 0 to 1 or vice versa, the  UnderrunStateChgIntr bit in the Counter Per Alarm Interrupt Status register of the related pseudowire is set or clear."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OverrunCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "OverrunCurStatus"
            
            def description(self):
                return "Jitter buffer overrun current status in the related pseudowire. When it changes from 0 to 1 or vice versa, the  OverrunStateChgIntr bit in the Counter Per Alarm Interrupt Status register of the related pseudowire is set or clear."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LofsStateCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "LofsStateCurStatus"
            
            def description(self):
                return "Lost of frame state current status in the related pseudowire. When it changes from 0 to 1 or vice versa, the  LofsStateChgIntr bit in the Counter Per Alarm Interrupt Status register of the related pseudowire is set or clear."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LbitStateCurStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LbitStateCurStatus"
            
            def description(self):
                return "Lbits current status in the related pseudowire. When it changes from 0 to 1 or vice versa, the  LbitStateChgIntr bit in the Counter Per Alarm Interrupt Status register of the related pseudowire is set or clear."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["StrayCurStatus"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Current_Status._StrayCurStatus()
            allFields["MalformCurStatus"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Current_Status._MalformCurStatus()
            allFields["MbitCurStatus"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Current_Status._MbitCurStatus()
            allFields["RbitCurStatus"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Current_Status._RbitCurStatus()
            allFields["LostPkCurStatus"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Current_Status._LostPkCurStatus()
            allFields["UnderrunCurStatus"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Current_Status._UnderrunCurStatus()
            allFields["OverrunCurStatus"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Current_Status._OverrunCurStatus()
            allFields["LofsStateCurStatus"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Current_Status._LofsStateCurStatus()
            allFields["LbitStateCurStatus"] = _AF6CCI0021_RD_PMC._Counter_Per_Alarm_Current_Status._LbitStateCurStatus()
            return allFields

    class _Counter_Interrupt_OR_Status(AtRegister.AtRegister):
        def name(self):
            return "Counter Interrupt OR Status"
    
        def description(self):
            return "The register consists of 32 bits for 32 pseudowires. Each bit is used to store Interrupt OR status of the related pseudowires."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x001300 +  GrpID"
            
        def startAddress(self):
            return 0x00001300
            
        def endAddress(self):
            return 0x00001307

        class _IntrORStatus(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "IntrORStatus"
            
            def description(self):
                return "Set to 1 to indicate that there is any interrupt status bit in the Counter per Alarm Interrupt Status register of the related pseudowires to be set and they are enabled to raise interrupt. Bit 0 of GrpID#0 for pseudowire 0, bit 31 of GrpID#0 for pseudowire 31, bit 0 of GrpID#1 for pseudowire 32, respectively."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["IntrORStatus"] = _AF6CCI0021_RD_PMC._Counter_Interrupt_OR_Status._IntrORStatus()
            return allFields

    class _counter_per_group_intr_or_stat(AtRegister.AtRegister):
        def name(self):
            return "Counter per Group Interrupt OR Status"
    
        def description(self):
            return "The register consists of 8 bits for 8 Group of the PW Counter. Each bit is used to store Interrupt OR status of the related Group."
            
        def width(self):
            return 8
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000013ff
            
        def endAddress(self):
            return 0xffffffff

        class _GroupIntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GroupIntrOrSta"
            
            def description(self):
                return "Set to 1 if any interrupt bit of corresponding Group is set and its interrupt is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GroupIntrOrSta"] = _AF6CCI0021_RD_PMC._counter_per_group_intr_or_stat._GroupIntrOrSta()
            return allFields

    class _counter_per_group_intr_en_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Counter per Group Interrupt Enable Control"
    
        def description(self):
            return "The register consists of 8 interrupt enable bits for 8 group in the PW counter."
            
        def width(self):
            return 8
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000013fe
            
        def endAddress(self):
            return 0xffffffff

        class _GroupIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "GroupIntrEn"
            
            def description(self):
                return "Set to 1 to enable the related Group to generate interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GroupIntrEn"] = _AF6CCI0021_RD_PMC._counter_per_group_intr_en_ctrl._GroupIntrEn()
            return allFields

    class _Ethernet_Receive_1to64Byte_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive 1to64Byte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from 1 to 64 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000020
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_1to64Byte_Packet_Counter_ro._PacketNum()
            return allFields

    class _Ethernet_Receive_1to64Byte_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive 1to64Byte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from 1 to 64 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000021
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_1to64Byte_Packet_Counter_rc._PacketNum()
            return allFields

    class _Ethernet_Transmit_1to64Byte_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit 1to64Byte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from 1 to 64 bytes to Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000030
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_1to64Byte_Packet_Counter_ro._PacketNum()
            return allFields

    class _Ethernet_Transmit_1to64Byte_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit 1to64Byte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from 1 to 64 bytes to Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000031
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_1to64Byte_Packet_Counter_rc._PacketNum()
            return allFields

    class _Ethernet_Receive_65to128Byte_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive 65to128Byte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from 65 to 128 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000022
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_65to128Byte_Packet_Counter_ro._PacketNum()
            return allFields

    class _Ethernet_Receive_65to128Byte_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive 65to128Byte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from 65 to 128 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000023
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_65to128Byte_Packet_Counter_rc._PacketNum()
            return allFields

    class _Ethernet_Transmit_65to128Byte_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit 65to128Byte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from 65 to 128 bytes to Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000032
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_65to128Byte_Packet_Counter_ro._PacketNum()
            return allFields

    class _Ethernet_Transmit_65to128Byte_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit 65to128Byte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from 65 to 128 bytes to Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000033
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_65to128Byte_Packet_Counter_rc._PacketNum()
            return allFields

    class _Ethernet_Receive_129to256Byte_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive 129to256Byte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from 129 to 256 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000024
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_129to256Byte_Packet_Counter_ro._PacketNum()
            return allFields

    class _Ethernet_Receive_129to256Byte_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive 129to256Byte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from 129 to 256 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000025
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_129to256Byte_Packet_Counter_rc._PacketNum()
            return allFields

    class _Ethernet_Transmit_129to256Byte_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit 129to256Byte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from 129 to 256 bytes to Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000034
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_129to256Byte_Packet_Counter_ro._PacketNum()
            return allFields

    class _Ethernet_Transmit_129to256Byte_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit 129to256Byte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from 129 to 256 bytes to Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000035
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_129to256Byte_Packet_Counter_rc._PacketNum()
            return allFields

    class _Ethernet_Receive_257to512Byte_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive 257to512Byte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from 257 to 512 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000026
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_257to512Byte_Packet_Counter_ro._PacketNum()
            return allFields

    class _Ethernet_Receive_257to512Byte_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive 257to512Byte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from 257 to 512 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000027
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_257to512Byte_Packet_Counter_rc._PacketNum()
            return allFields

    class _Ethernet_Transmit_257to512Byte_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit 257to512Byte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from 257 to 512 bytes to Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000036
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_257to512Byte_Packet_Counter_ro._PacketNum()
            return allFields

    class _Ethernet_Transmit_257to512Byte_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit 257to512Byte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from 257 to 512 bytes to Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000037
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_257to512Byte_Packet_Counter_rc._PacketNum()
            return allFields

    class _Ethernet_Receive_513to1024Byte_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive 513to1024Byte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from 513  to 1024 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000028
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_513to1024Byte_Packet_Counter_ro._PacketNum()
            return allFields

    class _Ethernet_Receive_513to1024Byte_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive 513to1024Byte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from 513  to 1024 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000029
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_513to1024Byte_Packet_Counter_rc._PacketNum()
            return allFields

    class _Ethernet_Transmit_513to1024Byte_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit 513to1024Byte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from 513  to 1024 bytes to Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000038
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_513to1024Byte_Packet_Counter_ro._PacketNum()
            return allFields

    class _Ethernet_Transmit_513to1024Byte_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit 513to1024Byte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from 513  to 1024 bytes to Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000039
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_513to1024Byte_Packet_Counter_rc._PacketNum()
            return allFields

    class _Ethernet_Receive_1025to1528Byte_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive 1025to1528Byte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from 1025   to 1528 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000002a
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_1025to1528Byte_Packet_Counter_ro._PacketNum()
            return allFields

    class _Ethernet_Receive_1025to1528Byte_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive 1025to1528Byte Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length from 1025   to 1528 bytes from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000002b
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_1025to1528Byte_Packet_Counter_rc._PacketNum()
            return allFields

    class _Ethernet_Transmit_1025to1528Byte_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit 1025to1528Byte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from 1025   to 1528 bytes to Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000003a
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_1025to1528Byte_Packet_Counter_ro._PacketNum()
            return allFields

    class _Ethernet_Transmit_1025to1528Byte_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit 1025to1528Byte Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length from 1025   to 1528 bytes to Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000003b
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_1025to1528Byte_Packet_Counter_rc._PacketNum()
            return allFields

    class _Ethernet_Receive_Jumbo_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive Jumbo Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length Jumbo from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000002c
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_Jumbo_Packet_Counter_ro._PacketNum()
            return allFields

    class _Ethernet_Receive_Jumbo_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Receive Jumbo Packet Counter"
    
        def description(self):
            return "Count total number of  received packets length Jumbo from Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000002d
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Receive_Jumbo_Packet_Counter_rc._PacketNum()
            return allFields

    class _Ethernet_Transmit_Jumbo_Packet_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit Jumbo Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length jumbo to Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000003c
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_Jumbo_Packet_Counter_ro._PacketNum()
            return allFields

    class _Ethernet_Transmit_Jumbo_Packet_Counter_rc(AtRegister.AtRegister):
        def name(self):
            return "Ethernet Transmit Jumbo Packet Counter"
    
        def description(self):
            return "Count total number of  transmitted packets length jumbo to Ethernet port"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000003d
            
        def endAddress(self):
            return 0xffffffff

        class _PacketNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PacketNum"
            
            def description(self):
                return "Counter value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PacketNum"] = _AF6CCI0021_RD_PMC._Ethernet_Transmit_Jumbo_Packet_Counter_rc._PacketNum()
            return allFields

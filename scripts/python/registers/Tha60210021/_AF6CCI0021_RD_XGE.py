import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0021_RD_XGE(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["ebufversion_pen"] = _AF6CCI0021_RD_XGE._ebufversion_pen()
        allRegisters["ebufglbctrl_pen"] = _AF6CCI0021_RD_XGE._ebufglbctrl_pen()
        allRegisters["ebufextblkint_pen"] = _AF6CCI0021_RD_XGE._ebufextblkint_pen()
        allRegisters["ebufextblkstat_pen"] = _AF6CCI0021_RD_XGE._ebufextblkstat_pen()
        allRegisters["ebufddrwrca_init_pen"] = _AF6CCI0021_RD_XGE._ebufddrwrca_init_pen()
        allRegisters["ebufthrctrl_pen"] = _AF6CCI0021_RD_XGE._ebufthrctrl_pen()
        allRegisters["claintval_pen"] = _AF6CCI0021_RD_XGE._claintval_pen()
        allRegisters["ebufshap_pen"] = _AF6CCI0021_RD_XGE._ebufshap_pen()
        allRegisters["XFI_FCS_errins_en_cfg"] = _AF6CCI0021_RD_XGE._XFI_FCS_errins_en_cfg()
        return allRegisters

    class _ebufversion_pen(AtRegister.AtRegister):
        def name(self):
            return "Ethenet Buffer Manager Block Version"
    
        def description(self):
            return "Ethenet Buffer Manager Block Version"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002001
            
        def endAddress(self):
            return 0xffffffff

        class _year(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "year"
            
            def description(self):
                return "Year"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _month(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "month"
            
            def description(self):
                return "Month"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _day(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "day"
            
            def description(self):
                return "Day"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _ver(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ver"
            
            def description(self):
                return "Version"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["year"] = _AF6CCI0021_RD_XGE._ebufversion_pen._year()
            allFields["month"] = _AF6CCI0021_RD_XGE._ebufversion_pen._month()
            allFields["day"] = _AF6CCI0021_RD_XGE._ebufversion_pen._day()
            allFields["ver"] = _AF6CCI0021_RD_XGE._ebufversion_pen._ver()
            return allFields

    class _ebufglbctrl_pen(AtRegister.AtRegister):
        def name(self):
            return "Ethenet Buffer Global Control"
    
        def description(self):
            return "Ethenet Buffer Global Control"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002002
            
        def endAddress(self):
            return 0xffffffff

        class _pause_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "pause_en"
            
            def description(self):
                return "Paugen Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _pause_set(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "pause_set"
            
            def description(self):
                return "Paugen Set"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ectrl_flsh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "ectrl_flsh"
            
            def description(self):
                return "External control flush"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _eblk_flsh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "eblk_flsh"
            
            def description(self):
                return "External free blk flush"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pause_en"] = _AF6CCI0021_RD_XGE._ebufglbctrl_pen._pause_en()
            allFields["pause_set"] = _AF6CCI0021_RD_XGE._ebufglbctrl_pen._pause_set()
            allFields["ectrl_flsh"] = _AF6CCI0021_RD_XGE._ebufglbctrl_pen._ectrl_flsh()
            allFields["eblk_flsh"] = _AF6CCI0021_RD_XGE._ebufglbctrl_pen._eblk_flsh()
            return allFields

    class _ebufextblkint_pen(AtRegister.AtRegister):
        def name(self):
            return "Ethener Buffer External Free-List Initiation"
    
        def description(self):
            return "Used to configure initiation of the external Free-List (16k blocks)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002003
            
        def endAddress(self):
            return 0xffffffff

        class _extfreelist_len(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 4
        
            def name(self):
                return "extfreelist_len"
            
            def description(self):
                return "Number of external free block"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _extfreelist_init(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "extfreelist_init"
            
            def description(self):
                return "Write 1 to init Free-List HW Return 0 when initiation done"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["extfreelist_len"] = _AF6CCI0021_RD_XGE._ebufextblkint_pen._extfreelist_len()
            allFields["extfreelist_init"] = _AF6CCI0021_RD_XGE._ebufextblkint_pen._extfreelist_init()
            return allFields

    class _ebufextblkstat_pen(AtRegister.AtRegister):
        def name(self):
            return "Ethener Buffer External Free-List Status"
    
        def description(self):
            return "Used to indicate status external Free-List (2k blocks)"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002004
            
        def endAddress(self):
            return 0xffffffff

        class _Unsed(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 72
        
            def name(self):
                return "Unsed"
            
            def description(self):
                return "Unsed"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _extfreelist_ba7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 71
                
            def startBit(self):
                return 63
        
            def name(self):
                return "extfreelist_ba7"
            
            def description(self):
                return "External free block on bank 7"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _extfreelist_ba6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 62
                
            def startBit(self):
                return 54
        
            def name(self):
                return "extfreelist_ba6"
            
            def description(self):
                return "External free block on bank 6"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _extfreelist_ba5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 53
                
            def startBit(self):
                return 45
        
            def name(self):
                return "extfreelist_ba5"
            
            def description(self):
                return "External free block on bank 5"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _extfreelist_ba4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 44
                
            def startBit(self):
                return 36
        
            def name(self):
                return "extfreelist_ba4"
            
            def description(self):
                return "External free block on bank 4"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _extfreelist_ba3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 27
        
            def name(self):
                return "extfreelist_ba3"
            
            def description(self):
                return "External free block on bank 3"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _extfreelist_ba2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 18
        
            def name(self):
                return "extfreelist_ba2"
            
            def description(self):
                return "External free block on bank 2"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _extfreelist_ba1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 9
        
            def name(self):
                return "extfreelist_ba1"
            
            def description(self):
                return "External free block on bank 1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _extfreelist_ba0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 0
        
            def name(self):
                return "extfreelist_ba0"
            
            def description(self):
                return "External free block on bank 0"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Unsed"] = _AF6CCI0021_RD_XGE._ebufextblkstat_pen._Unsed()
            allFields["extfreelist_ba7"] = _AF6CCI0021_RD_XGE._ebufextblkstat_pen._extfreelist_ba7()
            allFields["extfreelist_ba6"] = _AF6CCI0021_RD_XGE._ebufextblkstat_pen._extfreelist_ba6()
            allFields["extfreelist_ba5"] = _AF6CCI0021_RD_XGE._ebufextblkstat_pen._extfreelist_ba5()
            allFields["extfreelist_ba4"] = _AF6CCI0021_RD_XGE._ebufextblkstat_pen._extfreelist_ba4()
            allFields["extfreelist_ba3"] = _AF6CCI0021_RD_XGE._ebufextblkstat_pen._extfreelist_ba3()
            allFields["extfreelist_ba2"] = _AF6CCI0021_RD_XGE._ebufextblkstat_pen._extfreelist_ba2()
            allFields["extfreelist_ba1"] = _AF6CCI0021_RD_XGE._ebufextblkstat_pen._extfreelist_ba1()
            allFields["extfreelist_ba0"] = _AF6CCI0021_RD_XGE._ebufextblkstat_pen._extfreelist_ba0()
            return allFields

    class _ebufddrwrca_init_pen(AtRegister.AtRegister):
        def name(self):
            return "Ethenet Buffer DDR Write Data Cache Free-List Initiation"
    
        def description(self):
            return "Used to configure initiation of the data write cache Linked-List."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002005
            
        def endAddress(self):
            return 0xffffffff

        class _ddrwrca_llist_init(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ddrwrca_llist_init"
            
            def description(self):
                return "Write 1 to init Linked-List HW Return 0 when initiation done"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ddrwrca_llist_init"] = _AF6CCI0021_RD_XGE._ebufddrwrca_init_pen._ddrwrca_llist_init()
            return allFields

    class _ebufthrctrl_pen(AtRegister.AtRegister):
        def name(self):
            return "Ethenet Buffer Threshold Control"
    
        def description(self):
            return "Used to configure ethenet buffer thresholds."
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002006
            
        def endAddress(self):
            return 0xffffffff

        class _Unsued(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 33
        
            def name(self):
                return "Unsued"
            
            def description(self):
                return "Unsed"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _wrblk_thr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 32
                
            def startBit(self):
                return 24
        
            def name(self):
                return "wrblk_thr"
            
            def description(self):
                return "Write cache blk threshold"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _under_thr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 12
        
            def name(self):
                return "under_thr"
            
            def description(self):
                return "Flow Control under threshold"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _over_thr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "over_thr"
            
            def description(self):
                return "Flow Control over threshold"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Unsued"] = _AF6CCI0021_RD_XGE._ebufthrctrl_pen._Unsued()
            allFields["wrblk_thr"] = _AF6CCI0021_RD_XGE._ebufthrctrl_pen._wrblk_thr()
            allFields["under_thr"] = _AF6CCI0021_RD_XGE._ebufthrctrl_pen._under_thr()
            allFields["over_thr"] = _AF6CCI0021_RD_XGE._ebufthrctrl_pen._over_thr()
            return allFields

    class _claintval_pen(AtRegister.AtRegister):
        def name(self):
            return "Ethenet Buffer CLA Interval Control"
    
        def description(self):
            return "Used to configure Interval(number of clk) to shift data to CLA."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002008
            
        def endAddress(self):
            return 0xffffffff

        class _Unsued(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Unsued"
            
            def description(self):
                return "Unsed"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cla_intval(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cla_intval"
            
            def description(self):
                return "CLA valid interval"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Unsued"] = _AF6CCI0021_RD_XGE._claintval_pen._Unsued()
            allFields["cla_intval"] = _AF6CCI0021_RD_XGE._claintval_pen._cla_intval()
            return allFields

    class _ebufshap_pen(AtRegister.AtRegister):
        def name(self):
            return "Ethenet Buffer Output Shaping Control"
    
        def description(self):
            return "Used to configure output shaping control."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000200a
            
        def endAddress(self):
            return 0xffffffff

        class _shapena(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "shapena"
            
            def description(self):
                return "Shaping control Enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _shapcbs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 8
        
            def name(self):
                return "shapcbs"
            
            def description(self):
                return "Shaping control CBS"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _shapcir(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "shapcir"
            
            def description(self):
                return "Shaping control CIR token"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["shapena"] = _AF6CCI0021_RD_XGE._ebufshap_pen._shapena()
            allFields["shapcbs"] = _AF6CCI0021_RD_XGE._ebufshap_pen._shapcbs()
            allFields["shapcir"] = _AF6CCI0021_RD_XGE._ebufshap_pen._shapcir()
            return allFields

    class _XFI_FCS_errins_en_cfg(AtRegister.AtRegister):
        def name(self):
            return "XFI FCS Force Error Control"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001007
            
        def endAddress(self):
            return 0xffffffff

        class _ETHFCSErrMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 29
        
            def name(self):
                return "ETHFCSErrMode"
            
            def description(self):
                return "0: FCS (MAC); 1:Tx interface MAC2PCS; 2: Rx interface PCS2MAC"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ETHFCSErrRate(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "ETHFCSErrRate"
            
            def description(self):
                return "0: One-shot with number of errors; 1:Line rate"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ETHFCSErrThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ETHFCSErrThr"
            
            def description(self):
                return "Error Threshold in bit unit or the number of error in one-shot mode. Valid value from 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ETHFCSErrMode"] = _AF6CCI0021_RD_XGE._XFI_FCS_errins_en_cfg._ETHFCSErrMode()
            allFields["ETHFCSErrRate"] = _AF6CCI0021_RD_XGE._XFI_FCS_errins_en_cfg._ETHFCSErrRate()
            allFields["ETHFCSErrThr"] = _AF6CCI0021_RD_XGE._XFI_FCS_errins_en_cfg._ETHFCSErrThr()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0021_RD_PDA(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["pw_pda_jitbuf_ctrl"] = _AF6CCI0021_RD_PDA._pw_pda_jitbuf_ctrl()
        allRegisters["pw_pda_jitbuf_stat"] = _AF6CCI0021_RD_PDA._pw_pda_jitbuf_stat()
        allRegisters["pw_pda_reo_ctrl"] = _AF6CCI0021_RD_PDA._pw_pda_reo_ctrl()
        allRegisters["pw_pda_tdm_mode_ctrl"] = _AF6CCI0021_RD_PDA._pw_pda_tdm_mode_ctrl()
        allRegisters["pw_pda_idlecode_ctrl"] = _AF6CCI0021_RD_PDA._pw_pda_idlecode_ctrl()
        allRegisters["pw_pda_pw_prbs_gen_ctrl"] = _AF6CCI0021_RD_PDA._pw_pda_pw_prbs_gen_ctrl()
        allRegisters["pw_pda_pw_prbs_mon_ctrl"] = _AF6CCI0021_RD_PDA._pw_pda_pw_prbs_mon_ctrl()
        allRegisters["PDA_DDR_ECC_Cor_Error_Counter_ro"] = _AF6CCI0021_RD_PDA._PDA_DDR_ECC_Cor_Error_Counter_ro()
        allRegisters["PDA_DDR_ECC_Cor_Error_Counter_r2c"] = _AF6CCI0021_RD_PDA._PDA_DDR_ECC_Cor_Error_Counter_r2c()
        allRegisters["PDA_DDR_ECC_UnCor_Error_Counter_ro"] = _AF6CCI0021_RD_PDA._PDA_DDR_ECC_UnCor_Error_Counter_ro()
        allRegisters["PDA_DDR_ECC_UnCor_Error_Counter_r2c"] = _AF6CCI0021_RD_PDA._PDA_DDR_ECC_UnCor_Error_Counter_r2c()
        allRegisters["PDA_DDR_errins_en_cfg"] = _AF6CCI0021_RD_PDA._PDA_DDR_errins_en_cfg()
        return allRegisters

    class _pw_pda_jitbuf_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA Jitter Buffer Control"
    
        def description(self):
            return "This register sets jitter buffer parameters"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x003100 + PwId"
            
        def startAddress(self):
            return 0x00003100
            
        def endAddress(self):
            return 0x000031ff

        class _PwPayloadLen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 44
                
            def startBit(self):
                return 34
        
            def name(self):
                return "PwPayloadLen"
            
            def description(self):
                return "Payload length of receive pseudowire packet, set 0 for ATM PW"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwSetLofs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 33
                
            def startBit(self):
                return 25
        
            def name(self):
                return "PwSetLofs"
            
            def description(self):
                return "Number of consecutive lost packets to enter lost of frame state(LOFS), set 0 for ATM PW"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwClearLofs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "PwClearLofs"
            
            def description(self):
                return "Number of consecutive good packets to exit lost of frame state(LOFS), set 0 for ATM PW"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PdvSizePk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PdvSizePk"
            
            def description(self):
                return "Set 0 for ATM PW. This parameter is to prevent packet delay variation(PDV) from PSN. PdvSizePk is packet delay variation measured in packet unit. The formula is as follow: PdvSizePk = (Pw64kbpsSpeed * PDVus)/(125*PwPayloadLen) +((Pw64kbpsSpeed * PDVus)%(125*PwPayloadLen) !=0) Case: Pseudowire speed =   Pw64kbpsSpeed * 64kbps PDVus is packet delay variation in microsecond unit. This value should be multiple of 125us for more accurate"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _JitBufSizePk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "JitBufSizePk"
            
            def description(self):
                return "Set default value for ATM PW. Jitter buffer size measured in packet unit. The formula is as follow: In common: JitBufSizePk = 2 * PdvSizePk When user want to configure PDV and jitter buffer as independent parameters, the formular is: JitBufSizePk = (Pw64kbpsSpeed * JITBUFus)/(125*PwPayloadLen) + ((Pw64kbpsSpeed * JITBUFus)%(125*PwPayloadLen) !=0) Case: Pseudowire speed =   Pw64kbpsSpeed * 64kbps JITBUFus is jitter buffer size measured in microsecond unit. This value should be multiple of 125us for more accurate"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwPayloadLen"] = _AF6CCI0021_RD_PDA._pw_pda_jitbuf_ctrl._PwPayloadLen()
            allFields["PwSetLofs"] = _AF6CCI0021_RD_PDA._pw_pda_jitbuf_ctrl._PwSetLofs()
            allFields["PwClearLofs"] = _AF6CCI0021_RD_PDA._pw_pda_jitbuf_ctrl._PwClearLofs()
            allFields["PdvSizePk"] = _AF6CCI0021_RD_PDA._pw_pda_jitbuf_ctrl._PdvSizePk()
            allFields["JitBufSizePk"] = _AF6CCI0021_RD_PDA._pw_pda_jitbuf_ctrl._JitBufSizePk()
            return allFields

    class _pw_pda_jitbuf_stat(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA Jitter Buffer Status"
    
        def description(self):
            return "This register show jitter buffer status"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x003200 + PwId"
            
        def startAddress(self):
            return 0x00003200
            
        def endAddress(self):
            return 0x000032ff

        class _JitBufComSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 16
        
            def name(self):
                return "JitBufComSta"
            
            def description(self):
                return "Jitter buffer Common Status, set default 0 at initial"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _JitBufNumPkt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 4
        
            def name(self):
                return "JitBufNumPkt"
            
            def description(self):
                return "Jitter buffer number of current packets"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _JitBufFull(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "JitBufFull"
            
            def description(self):
                return "Jitter buffer full status, value 1 indicate full status, otherwise, normal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _JitBufState(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "JitBufState"
            
            def description(self):
                return "Jitter buffer state machine status 0: START 1: FILL 2: READY 3: READWAIT 4: READREQ 5: REPLAY 6: NEAR_EMPTY 7: NOT VALID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["JitBufComSta"] = _AF6CCI0021_RD_PDA._pw_pda_jitbuf_stat._JitBufComSta()
            allFields["JitBufNumPkt"] = _AF6CCI0021_RD_PDA._pw_pda_jitbuf_stat._JitBufNumPkt()
            allFields["JitBufFull"] = _AF6CCI0021_RD_PDA._pw_pda_jitbuf_stat._JitBufFull()
            allFields["JitBufState"] = _AF6CCI0021_RD_PDA._pw_pda_jitbuf_stat._JitBufState()
            return allFields

    class _pw_pda_reo_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA Reorder Control"
    
        def description(self):
            return "This register sets reorder parameters"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x004000 + PwId"
            
        def startAddress(self):
            return 0x00004000
            
        def endAddress(self):
            return 0x000040ff

        class _PwReorEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PwReorEn"
            
            def description(self):
                return "0: Enable reorder pseudowire packets from Ethernet side before sending to jitter buffer 1: Disable reorder function"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwReorTimeOut(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PwReorTimeOut"
            
            def description(self):
                return "Timer to detect lost of packet sequence. The formula to calculate PwReorTimeOut value from PdvSizePk (defined in Pseudowire PDA Jitter Buffer Control register) is as follow: PwReorTimeout = (Min(32,PdvSizePk) * PwPayloadLen)/ Pw64kbpsSpeed where  Pseudowire speed =   Pw64kbpsSpeed * 64kbps"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwReorEn"] = _AF6CCI0021_RD_PDA._pw_pda_reo_ctrl._PwReorEn()
            allFields["PwReorTimeOut"] = _AF6CCI0021_RD_PDA._pw_pda_reo_ctrl._PwReorTimeOut()
            return allFields

    class _pw_pda_tdm_mode_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA TDM Mode Control"
    
        def description(self):
            return "The register configures modes of TDM Payload De-Assembler function"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x005000 + PwId"
            
        def startAddress(self):
            return 0x00005000
            
        def endAddress(self):
            return 0x000050ff

        class _PDARDIOff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PDARDIOff"
            
            def description(self):
                return "Mode to send RDI signal to PDH in structured mode 0: Enable sending RDI signal to PDH when PDA receive Lbit = 0 and Mbit = 0 1: Disable sending RDI signal to PDH when PDA receive Lbit = 0 and Mbit = 10"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDARepMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 15
        
            def name(self):
                return "PDARepMode"
            
            def description(self):
                return "Mode to replace lost or L bit set packets 0: Replace by replaying previous good packet (often for voice or video application) 1: Replace by AIS 2: Replace by a configuration idle code"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDANxDS0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PDANxDS0"
            
            def description(self):
                return "number of DS0 time slot in Structured Mode (CESoPSN)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PDAMode"
            
            def description(self):
                return "TDM Payload De-Assembler modes 0: SAToP 2: CESoPSN without CAS 3: CESoPSN with CAS Others: Reserved"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAE1orT1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDAE1orT1"
            
            def description(self):
                return "0: T1/DS1 1: E1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDARDIOff"] = _AF6CCI0021_RD_PDA._pw_pda_tdm_mode_ctrl._PDARDIOff()
            allFields["PDARepMode"] = _AF6CCI0021_RD_PDA._pw_pda_tdm_mode_ctrl._PDARepMode()
            allFields["PDANxDS0"] = _AF6CCI0021_RD_PDA._pw_pda_tdm_mode_ctrl._PDANxDS0()
            allFields["PDAMode"] = _AF6CCI0021_RD_PDA._pw_pda_tdm_mode_ctrl._PDAMode()
            allFields["PDAE1orT1"] = _AF6CCI0021_RD_PDA._pw_pda_tdm_mode_ctrl._PDAE1orT1()
            return allFields

    class _pw_pda_idlecode_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA Idle Code Control"
    
        def description(self):
            return "The register configures idle code for TDM Payload De-Assembler replay function"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00005c00
            
        def endAddress(self):
            return 0xffffffff

        class _PDAIdleCode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDAIdleCode"
            
            def description(self):
                return "Idle code value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDAIdleCode"] = _AF6CCI0021_RD_PDA._pw_pda_idlecode_ctrl._PDAIdleCode()
            return allFields

    class _pw_pda_pw_prbs_gen_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA PW PRBS Generation Control"
    
        def description(self):
            return "The register configures modes of PW PRBS Generation function"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x006400 + PwId"
            
        def startAddress(self):
            return 0x00006400
            
        def endAddress(self):
            return 0x000064ff

        class _PrbsData(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PrbsData"
            
            def description(self):
                return "Prbs data random"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PrbsError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PrbsError"
            
            def description(self):
                return "Force PRBS Error"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PrbsMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PrbsMode"
            
            def description(self):
                return "Prbs modes 0: Prbs15 (x15 + x14 + 1) 1: Prbs31 (x31 + x28 + 1)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PrbsEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PrbsEnb"
            
            def description(self):
                return "0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PrbsData"] = _AF6CCI0021_RD_PDA._pw_pda_pw_prbs_gen_ctrl._PrbsData()
            allFields["PrbsError"] = _AF6CCI0021_RD_PDA._pw_pda_pw_prbs_gen_ctrl._PrbsError()
            allFields["PrbsMode"] = _AF6CCI0021_RD_PDA._pw_pda_pw_prbs_gen_ctrl._PrbsMode()
            allFields["PrbsEnb"] = _AF6CCI0021_RD_PDA._pw_pda_pw_prbs_gen_ctrl._PrbsEnb()
            return allFields

    class _pw_pda_pw_prbs_mon_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire PDA PW PRBS Monitor Control"
    
        def description(self):
            return "The register configures modes of PW PRBS Generation function"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x006800 + PwId"
            
        def startAddress(self):
            return 0x00006800
            
        def endAddress(self):
            return 0x000068ff

        class _PrbsError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PrbsError"
            
            def description(self):
                return "Force PRBS Error Sticky, Read to clear"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PrbsSync(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PrbsSync"
            
            def description(self):
                return "0: Prbs Loss Status 1: Prbs Sync Status"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PrbsData(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PrbsData"
            
            def description(self):
                return "Prbs data random"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PrbsMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PrbsMode"
            
            def description(self):
                return "Prbs modes 0: Prbs15 (x15 + x14 + 1) 1: Prbs31 (x31 + x28 + 1)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PrbsError"] = _AF6CCI0021_RD_PDA._pw_pda_pw_prbs_mon_ctrl._PrbsError()
            allFields["PrbsSync"] = _AF6CCI0021_RD_PDA._pw_pda_pw_prbs_mon_ctrl._PrbsSync()
            allFields["PrbsData"] = _AF6CCI0021_RD_PDA._pw_pda_pw_prbs_mon_ctrl._PrbsData()
            allFields["PrbsMode"] = _AF6CCI0021_RD_PDA._pw_pda_pw_prbs_mon_ctrl._PrbsMode()
            return allFields

    class _PDA_DDR_ECC_Cor_Error_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "PDA DDR ECC Correctable Error Counter"
    
        def description(self):
            return "Count number of DDR ECC Correctable error detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008009
            
        def endAddress(self):
            return 0xffffffff

        class _PDADDRECCCorError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDADDRECCCorError"
            
            def description(self):
                return "This counter count the number of DDR ECC Correctable error."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDADDRECCCorError"] = _AF6CCI0021_RD_PDA._PDA_DDR_ECC_Cor_Error_Counter_ro._PDADDRECCCorError()
            return allFields

    class _PDA_DDR_ECC_Cor_Error_Counter_r2c(AtRegister.AtRegister):
        def name(self):
            return "PDA DDR ECC Correctable Error Counter"
    
        def description(self):
            return "Count number of DDR ECC Correctable error detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00008008
            
        def endAddress(self):
            return 0xffffffff

        class _PDADDRECCCorError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDADDRECCCorError"
            
            def description(self):
                return "This counter count the number of DDR ECC Correctable error."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDADDRECCCorError"] = _AF6CCI0021_RD_PDA._PDA_DDR_ECC_Cor_Error_Counter_r2c._PDADDRECCCorError()
            return allFields

    class _PDA_DDR_ECC_UnCor_Error_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "PDA DDR ECC UnCorrectable Error Counter"
    
        def description(self):
            return "Count number of DDR ECC Correctable error detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000800b
            
        def endAddress(self):
            return 0xffffffff

        class _PDADDRECCUnCorError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDADDRECCUnCorError"
            
            def description(self):
                return "This counter count the number of DDR ECC Correctable error."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDADDRECCUnCorError"] = _AF6CCI0021_RD_PDA._PDA_DDR_ECC_UnCor_Error_Counter_ro._PDADDRECCUnCorError()
            return allFields

    class _PDA_DDR_ECC_UnCor_Error_Counter_r2c(AtRegister.AtRegister):
        def name(self):
            return "PDA DDR ECC UnCorrectable Error Counter"
    
        def description(self):
            return "Count number of DDR ECC Correctable error detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000800a
            
        def endAddress(self):
            return 0xffffffff

        class _PDADDRECCUnCorError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDADDRECCUnCorError"
            
            def description(self):
                return "This counter count the number of DDR ECC Correctable error."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDADDRECCUnCorError"] = _AF6CCI0021_RD_PDA._PDA_DDR_ECC_UnCor_Error_Counter_r2c._PDADDRECCUnCorError()
            return allFields

    class _PDA_DDR_errins_en_cfg(AtRegister.AtRegister):
        def name(self):
            return "PDA DDR Force Error Control"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00003601
            
        def endAddress(self):
            return 0xffffffff

        class _PDADDRErrMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "PDADDRErrMode"
            
            def description(self):
                return "0: Correctable (1bit); 1:UnCorrectable (2bit)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDADDRErrRate(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "PDADDRErrRate"
            
            def description(self):
                return "0: One-shot with number of errors; 1:Line rate"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDADDRErrThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDADDRErrThr"
            
            def description(self):
                return "Error Threshold in bit unit or the number of error in one-shot mode. Valid value from 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDADDRErrMode"] = _AF6CCI0021_RD_PDA._PDA_DDR_errins_en_cfg._PDADDRErrMode()
            allFields["PDADDRErrRate"] = _AF6CCI0021_RD_PDA._PDA_DDR_errins_en_cfg._PDADDRErrRate()
            allFields["PDADDRErrThr"] = _AF6CCI0021_RD_PDA._PDA_DDR_errins_en_cfg._PDADDRErrThr()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0021_RD_MAP(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["demap_channel_ctrl"] = _AF6CCI0021_RD_MAP._demap_channel_ctrl()
        allRegisters["map_channel_ctrl"] = _AF6CCI0021_RD_MAP._map_channel_ctrl()
        allRegisters["map_timing_reference_ctrl"] = _AF6CCI0021_RD_MAP._map_timing_reference_ctrl()
        allRegisters["Bert_Force_Single_Bit_Error"] = _AF6CCI0021_RD_MAP._Bert_Force_Single_Bit_Error()
        return allRegisters

    class _demap_channel_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Thalassa Demap Channel Control"
    
        def description(self):
            return "The registers provide the pseudo-wire configurations for PDH ? pseudo-wire side.."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000000 + liuid[5:0]*32 + tsid[4:0]"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x000007ff

        class _DemapChannelType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DemapChannelType"
            
            def description(self):
                return "Specify which type of mapping for this. 0: SAToP encapsulation from DS1/E1, DS3/E3 1: CESoP encapsulation from DS1/E1 2: CESoP pseudowire with CAS"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapFirstTs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DemapFirstTs"
            
            def description(self):
                return "First timeslot indication. Use for CESoP mode only."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapTsEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DemapTsEn"
            
            def description(self):
                return "Enable for DS0 timeslot."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DemapPwId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DemapPwId"
            
            def description(self):
                return "Pseudo-wire ID."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DemapChannelType"] = _AF6CCI0021_RD_MAP._demap_channel_ctrl._DemapChannelType()
            allFields["DemapFirstTs"] = _AF6CCI0021_RD_MAP._demap_channel_ctrl._DemapFirstTs()
            allFields["DemapTsEn"] = _AF6CCI0021_RD_MAP._demap_channel_ctrl._DemapTsEn()
            allFields["DemapPwId"] = _AF6CCI0021_RD_MAP._demap_channel_ctrl._DemapPwId()
            return allFields

    class _map_channel_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Thalassa Map Channel Control"
    
        def description(self):
            return "The registers provide the pseudo-wire configurations for pseudo-wire to PDH side.."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x001000 + liuid[5:0]*32 + tsid[4:0]"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0x000017ff

        class _MapChannelType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 16
        
            def name(self):
                return "MapChannelType"
            
            def description(self):
                return "Specify which type of mapping for this. 0: SAToP encapsulation from DS1/E1, DS3/E3 1: CESoP encapsulation from DS1/E1 2: CESoP pseudowire with CAS"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapFirstTs(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "MapFirstTs"
            
            def description(self):
                return "First timeslot indication. Use for CESoP mode only."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapTsEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "MapTsEn"
            
            def description(self):
                return "Enable for DS0 timeslot."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapChannelId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 8
        
            def name(self):
                return "MapChannelId"
            
            def description(self):
                return "Channel ID."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapPwId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MapPwId"
            
            def description(self):
                return "Pseudo-wire ID."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MapChannelType"] = _AF6CCI0021_RD_MAP._map_channel_ctrl._MapChannelType()
            allFields["MapFirstTs"] = _AF6CCI0021_RD_MAP._map_channel_ctrl._MapFirstTs()
            allFields["MapTsEn"] = _AF6CCI0021_RD_MAP._map_channel_ctrl._MapTsEn()
            allFields["MapChannelId"] = _AF6CCI0021_RD_MAP._map_channel_ctrl._MapChannelId()
            allFields["MapPwId"] = _AF6CCI0021_RD_MAP._map_channel_ctrl._MapPwId()
            return allFields

    class _map_timing_reference_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Thalassa Map Timing Reference Control"
    
        def description(self):
            return "The registers provide the timing reference source for physical DS1/E1 ports."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000800 + liuid[5:0]"
            
        def startAddress(self):
            return 0x00000800
            
        def endAddress(self):
            return 0x0000083f

        class _MapDs1LcEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 8
        
            def name(self):
                return "MapDs1LcEn"
            
            def description(self):
                return "DS1E1 Loop code insert enable 0: Disable 1: CSU Loop up 2: CSU Loop down 3: FA1 Loop up 4: FA1 Loop down 5: FA2 Loop up 6: FA2 Loop down"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapDs1FrmMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "MapDs1FrmMode"
            
            def description(self):
                return "DS1E1 Frame mode 0: Unfrm mode (default) 1: DS1 SF/ESF mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapDs1E1Mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "MapDs1E1Mode"
            
            def description(self):
                return "DS1E1 Line Type mode 0: E1 mode (default) 1: DS1 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MaptimeSrcId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "MaptimeSrcId"
            
            def description(self):
                return "The physical port ID uses for timing reference."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["MapDs1LcEn"] = _AF6CCI0021_RD_MAP._map_timing_reference_ctrl._MapDs1LcEn()
            allFields["MapDs1FrmMode"] = _AF6CCI0021_RD_MAP._map_timing_reference_ctrl._MapDs1FrmMode()
            allFields["MapDs1E1Mode"] = _AF6CCI0021_RD_MAP._map_timing_reference_ctrl._MapDs1E1Mode()
            allFields["MaptimeSrcId"] = _AF6CCI0021_RD_MAP._map_timing_reference_ctrl._MaptimeSrcId()
            return allFields

    class _Bert_Force_Single_Bit_Error(AtRegister.AtRegister):
        def name(self):
            return "Thalassa Bert Force Single Bit Error"
    
        def description(self):
            return "The registers provide the threshold  for force bit err."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x002070 + Ber_level"
            
        def startAddress(self):
            return 0x00002070
            
        def endAddress(self):
            return 0x00002079

        class _ber_threshold(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ber_threshold"
            
            def description(self):
                return "Threshold HW force one bit error."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ber_threshold"] = _AF6CCI0021_RD_MAP._Bert_Force_Single_Bit_Error._ber_threshold()
            return allFields

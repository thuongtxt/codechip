import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0021_RD_CDR(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["cdr_ti_extref_prc_ctrl"] = _AF6CCI0021_RD_CDR._cdr_ti_extref_prc_ctrl()
        allRegisters["cdr_glb_wrcnt_line_rate_ctrl"] = _AF6CCI0021_RD_CDR._cdr_glb_wrcnt_line_rate_ctrl()
        allRegisters["cdr_eng_ti_ctrl"] = _AF6CCI0021_RD_CDR._cdr_eng_ti_ctrl()
        allRegisters["cdr_adj_state_stat"] = _AF6CCI0021_RD_CDR._cdr_adj_state_stat()
        allRegisters["cdr_pw_lkup_ctrl"] = _AF6CCI0021_RD_CDR._cdr_pw_lkup_ctrl()
        allRegisters["cdr_improv_ctrl"] = _AF6CCI0021_RD_CDR._cdr_improv_ctrl()
        allRegisters["dcr_txeng_act_ctrl"] = _AF6CCI0021_RD_CDR._dcr_txeng_act_ctrl()
        allRegisters["dcr_prc_src_sel_cfg"] = _AF6CCI0021_RD_CDR._dcr_prc_src_sel_cfg()
        allRegisters["dcr_pdc_fre_cfg"] = _AF6CCI0021_RD_CDR._dcr_pdc_fre_cfg()
        allRegisters["dcr_rtp_fre_cfg"] = _AF6CCI0021_RD_CDR._dcr_rtp_fre_cfg()
        allRegisters["cdr_refout_8khz_ctrl"] = _AF6CCI0021_RD_CDR._cdr_refout_8khz_ctrl()
        allRegisters["cdr_per_chn_intr_en_ctrl"] = _AF6CCI0021_RD_CDR._cdr_per_chn_intr_en_ctrl()
        allRegisters["cdr_per_chn_intr_stat"] = _AF6CCI0021_RD_CDR._cdr_per_chn_intr_stat()
        allRegisters["cdr_per_chn_curr_stat"] = _AF6CCI0021_RD_CDR._cdr_per_chn_curr_stat()
        allRegisters["cdr_per_chn_intr_or_stat"] = _AF6CCI0021_RD_CDR._cdr_per_chn_intr_or_stat()
        allRegisters["cdr_per_stsvc_intr_or_stat"] = _AF6CCI0021_RD_CDR._cdr_per_stsvc_intr_or_stat()
        allRegisters["cdr_per_stsvc_intr_en_ctrl"] = _AF6CCI0021_RD_CDR._cdr_per_stsvc_intr_en_ctrl()
        allRegisters["cdr_acr_par_1"] = _AF6CCI0021_RD_CDR._cdr_acr_par_1()
        allRegisters["cdr_acr_par_2"] = _AF6CCI0021_RD_CDR._cdr_acr_par_2()
        return allRegisters

    class _cdr_ti_extref_prc_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR Timing External reference  and PRC control"
    
        def description(self):
            return "Used for TDM PW, This register is used to configure for the 2Khz coefficient of external reference signal, PRC clock in order to generate the sync 125us timing. The mean that, if the reference signal is 8Khz, the  coefficient must be configured 4 (I.e 8Khz  = 4x2Khz)"
            
        def width(self):
            return 96
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _PrcN2k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 79
                
            def startBit(self):
                return 64
        
            def name(self):
                return "PrcN2k"
            
            def description(self):
                return "The 2Khz coefficient of the PRC signal, default 2.048Mhz input clock"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EthN2k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 48
        
            def name(self):
                return "EthN2k"
            
            def description(self):
                return "The 2Khz coefficient of the Ethernet clock signal, default 125Mhz input clock"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Ext2N2k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 32
        
            def name(self):
                return "Ext2N2k"
            
            def description(self):
                return "The 2Khz coefficient of the second external reference signal, default 8khz input signal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Ext1N2k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Ext1N2k"
            
            def description(self):
                return "The 2Khz coefficient of the first external reference signal, default 8khz input signal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Ocn1N2k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Ocn1N2k"
            
            def description(self):
                return "The 2Khz coefficient of the SONET interface signal, default 8khz input signal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PrcN2k"] = _AF6CCI0021_RD_CDR._cdr_ti_extref_prc_ctrl._PrcN2k()
            allFields["EthN2k"] = _AF6CCI0021_RD_CDR._cdr_ti_extref_prc_ctrl._EthN2k()
            allFields["Ext2N2k"] = _AF6CCI0021_RD_CDR._cdr_ti_extref_prc_ctrl._Ext2N2k()
            allFields["Ext1N2k"] = _AF6CCI0021_RD_CDR._cdr_ti_extref_prc_ctrl._Ext1N2k()
            allFields["Ocn1N2k"] = _AF6CCI0021_RD_CDR._cdr_ti_extref_prc_ctrl._Ocn1N2k()
            return allFields

    class _cdr_glb_wrcnt_line_rate_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR global Write counter Line rate control"
    
        def description(self):
            return "Used for TDM PW, This register is used to configure global write counter line rate DS1/E1 with formula Fo = Fi * (S/M). Default lower than normal rate 60ppm"
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _DS1Rate(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 32
        
            def name(self):
                return "DS1Rate"
            
            def description(self):
                return "The DS1 line rate coefficient, default value 32'd2728819400"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _E1Rate(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "E1Rate"
            
            def description(self):
                return "The E1 line rate coefficient, default value  32'd3619627628"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DS1Rate"] = _AF6CCI0021_RD_CDR._cdr_glb_wrcnt_line_rate_ctrl._DS1Rate()
            allFields["E1Rate"] = _AF6CCI0021_RD_CDR._cdr_glb_wrcnt_line_rate_ctrl._E1Rate()
            return allFields

    class _cdr_eng_ti_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR Engine Timing control"
    
        def description(self):
            return "This register is used to configure timing mode for per STS"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000200 + CHID"
            
        def startAddress(self):
            return 0x00000200
            
        def endAddress(self):
            return 0x0000022f

        class _cfgrelearn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "cfgrelearn"
            
            def description(self):
                return "Set 1 to enable output current value mode in ReLearn State. Default is Hold value mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _cfglowp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "cfglowp"
            
            def description(self):
                return "Set 1 to select LOW performance mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdefdis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "cfgdefdis"
            
            def description(self):
                return "Set 1 to disable PLL go to Holdover state in Lbit packet"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgpripll(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "cfgpripll"
            
            def description(self):
                return "Set 1 to select output PLL in lock state is PRI. Default is SEC"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _HoldValMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "HoldValMode"
            
            def description(self):
                return "Hold value mode of NCO, default value 0 0: Hardware calculated and auto update 1: Software calculated and update, hardware is disabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SeqMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "SeqMode"
            
            def description(self):
                return "Sequence mode mode, default value 0 0: Wrap zero 1: Skip zero"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 20
        
            def name(self):
                return "LineType"
            
            def description(self):
                return "Line type mode 0: E1 1: DS1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PktLen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PktLen"
            
            def description(self):
                return "The payload packet  length parameter to create a packet - SAToP mode: The number payload of bit - CESoPSN mode: The number of bit which converted to full DS1/E1 rate mode. In CESoPSN mode, the payload is assembled by NxDS0 with M frame, the value configured to this register is Mx256 bits for E1 mode, Mx193 bits for DS1 mode."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRTimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRTimeMode"
            
            def description(self):
                return "CDR time mode 0: System mode 1: Loop timing mode, transparency service Rx clock to service Tx clock 2: LIU timing mode, using service Rx clock for CDR source to generate service Tx clock 3: Prc timing mode, using Prc clock for CDR source to generate service Tx clock 4: Ext#1 timing mode, using Ext#1 clock for CDR source to generate service Tx clock 5: Ext#2 timing mode, using Ext#2 clock for CDR source to generate service Tx clock 6: Ethernet timing mode, using Rx Ethernet clock for CDR source to generate service Tx clock 7: Free timing mode, using system clock for CDR source to generate service Tx clock 8: ACR timing mode 9: Reserve 10: DCR timing mode 11: External CDR timing mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfgrelearn"] = _AF6CCI0021_RD_CDR._cdr_eng_ti_ctrl._cfgrelearn()
            allFields["cfglowp"] = _AF6CCI0021_RD_CDR._cdr_eng_ti_ctrl._cfglowp()
            allFields["cfgdefdis"] = _AF6CCI0021_RD_CDR._cdr_eng_ti_ctrl._cfgdefdis()
            allFields["cfgpripll"] = _AF6CCI0021_RD_CDR._cdr_eng_ti_ctrl._cfgpripll()
            allFields["HoldValMode"] = _AF6CCI0021_RD_CDR._cdr_eng_ti_ctrl._HoldValMode()
            allFields["SeqMode"] = _AF6CCI0021_RD_CDR._cdr_eng_ti_ctrl._SeqMode()
            allFields["LineType"] = _AF6CCI0021_RD_CDR._cdr_eng_ti_ctrl._LineType()
            allFields["PktLen"] = _AF6CCI0021_RD_CDR._cdr_eng_ti_ctrl._PktLen()
            allFields["CDRTimeMode"] = _AF6CCI0021_RD_CDR._cdr_eng_ti_ctrl._CDRTimeMode()
            return allFields

    class _cdr_adj_state_stat(AtRegister.AtRegister):
        def name(self):
            return "CDR Adjust State status"
    
        def description(self):
            return "Used for TDM PW, This register is used to store status or configure  some parameter of per CDR engine"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000800 + CHID"
            
        def startAddress(self):
            return 0x00000800
            
        def endAddress(self):
            return 0x0000082f

        class _Adjstate(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Adjstate"
            
            def description(self):
                return "Adjust state 0: Load state 1: Wait state 2: Initialization state 3: Learn State 4: Rapid State 5: Lock State 6: Freeze State 7: Holdover State"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Adjstate"] = _AF6CCI0021_RD_CDR._cdr_adj_state_stat._Adjstate()
            return allFields

    class _cdr_pw_lkup_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR Pseudowire Look Up Control"
    
        def description(self):
            return "Used for TDM PW. This register is used to select PW for CDR function."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0A2000 + PWID"
            
        def startAddress(self):
            return 0x000a2000
            
        def endAddress(self):
            return 0x000a20ff

        class _PwCdrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PwCdrDis"
            
            def description(self):
                return "With SAToP pseudowires, must enable CDR function. With CESoP pseudowires sourced from the same E1/DS1 line, only one pseudowire is enabled CDR function 0: Enable CDR (default) 1: Disable CDR"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwCdrLineId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PwCdrLineId"
            
            def description(self):
                return "PDH line ID lookup from pseudowire Ids."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwCdrDis"] = _AF6CCI0021_RD_CDR._cdr_pw_lkup_ctrl._PwCdrDis()
            allFields["PwCdrLineId"] = _AF6CCI0021_RD_CDR._cdr_pw_lkup_ctrl._PwCdrLineId()
            return allFields

    class _cdr_improv_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR  Improvement Control"
    
        def description(self):
            return "Used for TDM PW. This register is used to improve  CDR function."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000a0000
            
        def endAddress(self):
            return 0xffffffff

        class _PwCdrSyncTime(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PwCdrSyncTime"
            
            def description(self):
                return "Timer for CDR to synchronize . Set zero value to disable CDR improvement function. Default value is 0x1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwCdrSyncTime"] = _AF6CCI0021_RD_CDR._cdr_improv_ctrl._PwCdrSyncTime()
            return allFields

    class _dcr_txeng_act_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DCR TX Engine Active Control"
    
        def description(self):
            return "This register is used to activate the DCR TX Engine. Active high."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00010000
            
        def endAddress(self):
            return 0xffffffff

        class _DcrTxAct(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DcrTxAct"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DcrTxAct"] = _AF6CCI0021_RD_CDR._dcr_txeng_act_ctrl._DcrTxAct()
            return allFields

    class _dcr_prc_src_sel_cfg(AtRegister.AtRegister):
        def name(self):
            return "DCR PRC Source Select Configuration"
    
        def description(self):
            return "This register is used to configure to select PRC source for PRC timer. The PRC clock selected must be less than 19.44 Mhz."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00010001
            
        def endAddress(self):
            return 0xffffffff

        class _DcrPrcSourceSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DcrPrcSourceSel"
            
            def description(self):
                return "PRC source selection. 0: PRC Reference Clock. 1: System Clock 19Mhz. 2: External Reference Clock 1. 3: External Reference Clock 2."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DcrPrcSourceSel"] = _AF6CCI0021_RD_CDR._dcr_prc_src_sel_cfg._DcrPrcSourceSel()
            return allFields

    class _dcr_pdc_fre_cfg(AtRegister.AtRegister):
        def name(self):
            return "DCR PRC Frequency Configuration"
    
        def description(self):
            return "This register is used to configure the frequency of PRC clock."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0001000b
            
        def endAddress(self):
            return 0xffffffff

        class _DCRPrcFrequency(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DCRPrcFrequency"
            
            def description(self):
                return "Frequency of PRC clock. This is used to configure the frequency of PRC clock in Khz. Unit is Khz. Exp: The PRC clock is 19.44Mhz. This register will be configured to 19440."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DCRPrcFrequency"] = _AF6CCI0021_RD_CDR._dcr_pdc_fre_cfg._DCRPrcFrequency()
            return allFields

    class _dcr_rtp_fre_cfg(AtRegister.AtRegister):
        def name(self):
            return "DCR RTP Frequency Configuration"
    
        def description(self):
            return "This register is used to configure the frequency of PRC clock."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0001000c
            
        def endAddress(self):
            return 0xffffffff

        class _DCRRTPFreq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DCRRTPFreq"
            
            def description(self):
                return "Frequency of RTP clock. This is used to configure the frequency of RTP clock in Khz. Unit is Khz. Exp: The RTP clock is 19.44Mhz. This register will be configured to 19440."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DCRRTPFreq"] = _AF6CCI0021_RD_CDR._dcr_rtp_fre_cfg._DCRRTPFreq()
            return allFields

    class _cdr_refout_8khz_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR REF OUT 8KHZ Control"
    
        def description(self):
            return "This register is used to get the status of RX NCO engine"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00020000
            
        def endAddress(self):
            return 0xffffffff

        class _Cdr1RefID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 20
        
            def name(self):
                return "Cdr1RefID"
            
            def description(self):
                return "CDR engine ID (in CDR mode) or Rx LIU ID (in loop mode) that used for 8Khz ref out"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr1RefDs1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "Cdr1RefDs1"
            
            def description(self):
                return "0: Line ID used for 8KHZ ref out is E1 mode (default) 1: Line ID used for 8KHZ ref out is DS1 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr1RefSelect(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Cdr1RefSelect"
            
            def description(self):
                return ": 0: System mode, used system clock to generate 8Khz ref out 1: Loop timing mode,  used rx LIU 2M/1.5M to generate 8Khz ref out 2: CDR timing mode,  used 2M/1.5M clock from CDR engine to generate 8KHZ out. CDR clock can be ACR/DCR/Loop/System/Ext1/..."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr0RefID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 4
        
            def name(self):
                return "Cdr0RefID"
            
            def description(self):
                return "CDR engine ID (in CDR mode) or Rx LIU ID (in loop mode) that used for 8Khz ref out"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr0RefDs1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "Cdr0RefDs1"
            
            def description(self):
                return ": 0: Line ID used for 8KHZ ref out is E1 mode (default) 1: Line ID used for 8KHZ ref out is DS1 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Cdr0RefSelect(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Cdr0RefSelect"
            
            def description(self):
                return "0: System mode, used system clock to generate 8Khz ref out 1: Loop timing mode,  used rx LIU 2M/1.5M to generate 8Khz ref out 2: CDR timing mode,  used 2M/1.5M clock from CDR engine to generate 8KHZ out. CDR clock can be ACR/DCR/Loop/System/Ext1/..."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Cdr1RefID"] = _AF6CCI0021_RD_CDR._cdr_refout_8khz_ctrl._Cdr1RefID()
            allFields["Cdr1RefDs1"] = _AF6CCI0021_RD_CDR._cdr_refout_8khz_ctrl._Cdr1RefDs1()
            allFields["Cdr1RefSelect"] = _AF6CCI0021_RD_CDR._cdr_refout_8khz_ctrl._Cdr1RefSelect()
            allFields["Cdr0RefID"] = _AF6CCI0021_RD_CDR._cdr_refout_8khz_ctrl._Cdr0RefID()
            allFields["Cdr0RefDs1"] = _AF6CCI0021_RD_CDR._cdr_refout_8khz_ctrl._Cdr0RefDs1()
            allFields["Cdr0RefSelect"] = _AF6CCI0021_RD_CDR._cdr_refout_8khz_ctrl._Cdr0RefSelect()
            return allFields

    class _cdr_per_chn_intr_en_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR per Channel Interrupt Enable Control"
    
        def description(self):
            return "This is the per Channel interrupt enable of CDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00002000 +  StsID*32 + VtnID"
            
        def startAddress(self):
            return 0x00002000
            
        def endAddress(self):
            return 0x0000203f

        class _CDRUnlokcedIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRUnlokcedIntrEn"
            
            def description(self):
                return "Set 1 to enable change UnLocked te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRUnlokcedIntrEn"] = _AF6CCI0021_RD_CDR._cdr_per_chn_intr_en_ctrl._CDRUnlokcedIntrEn()
            return allFields

    class _cdr_per_chn_intr_stat(AtRegister.AtRegister):
        def name(self):
            return "CDR per Channel Interrupt Status"
    
        def description(self):
            return "This is the per Channel interrupt tus of CDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00002040 +  StsID*32 + VtnID"
            
        def startAddress(self):
            return 0x00002040
            
        def endAddress(self):
            return 0x0000207f

        class _CDRUnLockedIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRUnLockedIntr"
            
            def description(self):
                return "Set 1 if there is a change in UnLocked the event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRUnLockedIntr"] = _AF6CCI0021_RD_CDR._cdr_per_chn_intr_stat._CDRUnLockedIntr()
            return allFields

    class _cdr_per_chn_curr_stat(AtRegister.AtRegister):
        def name(self):
            return "CDR per Channel Current Status"
    
        def description(self):
            return "This is the per Channel Current tus of CDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00002080 +  StsID*32 + VtnID"
            
        def startAddress(self):
            return 0x00002080
            
        def endAddress(self):
            return 0x000020bf

        class _CDRUnLockedCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRUnLockedCurrSta"
            
            def description(self):
                return "Current tus of UnLocked event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRUnLockedCurrSta"] = _AF6CCI0021_RD_CDR._cdr_per_chn_curr_stat._CDRUnLockedCurrSta()
            return allFields

    class _cdr_per_chn_intr_or_stat(AtRegister.AtRegister):
        def name(self):
            return "CDR per Channel Interrupt OR Status"
    
        def description(self):
            return "The register consists of 32 bits for 32 VT/TUs of the related STS/VC in the CDR. Each bit is used to store Interrupt OR tus of the related DS1/E1."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x000020C0 +  StsID"
            
        def startAddress(self):
            return 0x000020c0
            
        def endAddress(self):
            return 0x000020c1

        class _CDRVtIntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRVtIntrOrSta"
            
            def description(self):
                return "Set to 1 if any interrupt status bit of corresponding DS1/E1 is set and its interrupt is enabled."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRVtIntrOrSta"] = _AF6CCI0021_RD_CDR._cdr_per_chn_intr_or_stat._CDRVtIntrOrSta()
            return allFields

    class _cdr_per_stsvc_intr_or_stat(AtRegister.AtRegister):
        def name(self):
            return "CDR per STS/VC Interrupt OR Status"
    
        def description(self):
            return "The register consists of 2 bits for 2 STS/VCs of the CDR. Each bit is used to store Interrupt OR tus of the related STS/VC."
            
        def width(self):
            return 2
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000020ff
            
        def endAddress(self):
            return 0xffffffff

        class _CDRStsIntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRStsIntrOrSta"
            
            def description(self):
                return "Set to 1 if any interrupt status bit of corresponding STS/VC is set and its interrupt is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRStsIntrOrSta"] = _AF6CCI0021_RD_CDR._cdr_per_stsvc_intr_or_stat._CDRStsIntrOrSta()
            return allFields

    class _cdr_per_stsvc_intr_en_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR per STS/VC Interrupt Enable Control"
    
        def description(self):
            return "The register consists of 2 interrupt enable bits for 2 STS/VCs in the Rx DS1/E1/J1 Framer."
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000020fe
            
        def endAddress(self):
            return 0xffffffff

        class _CDRStsIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRStsIntrEn"
            
            def description(self):
                return "Set to 1 to enable the related STS/VC to generate interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRStsIntrEn"] = _AF6CCI0021_RD_CDR._cdr_per_stsvc_intr_en_ctrl._CDRStsIntrEn()
            return allFields

    class _cdr_acr_par_1(AtRegister.AtRegister):
        def name(self):
            return "CDR ACR parameter #1"
    
        def description(self):
            return "The register consists of global parameter for ACR/DCR engine"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _SysClkMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SysClkMode"
            
            def description(self):
                return "System Clock reference mode. 0: OCXO reference clock. 1: TCXO reference clock"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LearingMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "LearingMode"
            
            def description(self):
                return "Output of ACR/DCR frequency in Learning state. 0: Holdover. 1: Acquiring"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ExtraNCO_Enable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "ExtraNCO_Enable"
            
            def description(self):
                return "If this mode is enable, Extra NCO PLL is used to play out frequency after go to locked state (Acquired) 16 minutes. 0: Disable, Secondary NCO PLL is used to play out frequency in locked state 1: enable, Extra NCO PLL is used to play out frequency after go to locked state (Acquired) 16 minutes, before that SECNCO is used"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DCRTxRTPTSShaperDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DCRTxRTPTSShaperDis"
            
            def description(self):
                return "DCR RTP Timestamp Tx Shaper Disable. 0: Disable. 1: Enable. Direct serial clock DS1/E1 is used to sample Timestamp and latch for PWE send to PSN at SOP."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SysClkMode"] = _AF6CCI0021_RD_CDR._cdr_acr_par_1._SysClkMode()
            allFields["LearingMode"] = _AF6CCI0021_RD_CDR._cdr_acr_par_1._LearingMode()
            allFields["ExtraNCO_Enable"] = _AF6CCI0021_RD_CDR._cdr_acr_par_1._ExtraNCO_Enable()
            allFields["DCRTxRTPTSShaperDis"] = _AF6CCI0021_RD_CDR._cdr_acr_par_1._DCRTxRTPTSShaperDis()
            return allFields

    class _cdr_acr_par_2(AtRegister.AtRegister):
        def name(self):
            return "CDR ACR parameter #2"
    
        def description(self):
            return "The register consists of global parameter for ACR/DCR engine"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _BypassSECNCO(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "BypassSECNCO"
            
            def description(self):
                return "Bypass Secondary NCO."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FastLearningTime(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "FastLearningTime"
            
            def description(self):
                return "Fix Learning timer 64s."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _NoPacketTimeOut(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "NoPacketTimeOut"
            
            def description(self):
                return "Enable holdover when no packet input during 128ms (Or sequence field no change)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DCRRawFilterDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "DCRRawFilterDis"
            
            def description(self):
                return "Disable filter Raw DCR frequency after lookup over range (+-200ppm)."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["BypassSECNCO"] = _AF6CCI0021_RD_CDR._cdr_acr_par_2._BypassSECNCO()
            allFields["FastLearningTime"] = _AF6CCI0021_RD_CDR._cdr_acr_par_2._FastLearningTime()
            allFields["NoPacketTimeOut"] = _AF6CCI0021_RD_CDR._cdr_acr_par_2._NoPacketTimeOut()
            allFields["DCRRawFilterDis"] = _AF6CCI0021_RD_CDR._cdr_acr_par_2._DCRRawFilterDis()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0021_RD_GLB(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["chip_ver"] = _AF6CCI0021_RD_GLB._chip_ver()
        allRegisters["chip_act_ctrl"] = _AF6CCI0021_RD_GLB._chip_act_ctrl()
        allRegisters["inten_status"] = _AF6CCI0021_RD_GLB._inten_status()
        allRegisters["inten_ctrl"] = _AF6CCI0021_RD_GLB._inten_ctrl()
        allRegisters["inten_restore"] = _AF6CCI0021_RD_GLB._inten_restore()
        allRegisters["inten_PIN"] = _AF6CCI0021_RD_GLB._inten_PIN()
        allRegisters["inten_Eport"] = _AF6CCI0021_RD_GLB._inten_Eport()
        allRegisters["linkalm_Eport"] = _AF6CCI0021_RD_GLB._linkalm_Eport()
        allRegisters["linksta_Eport"] = _AF6CCI0021_RD_GLB._linksta_Eport()
        allRegisters["linkfrc_Eport"] = _AF6CCI0021_RD_GLB._linkfrc_Eport()
        allRegisters["wr_hold63_32"] = _AF6CCI0021_RD_GLB._wr_hold63_32()
        allRegisters["wr_hold95_64"] = _AF6CCI0021_RD_GLB._wr_hold95_64()
        allRegisters["wr_hold_127_96"] = _AF6CCI0021_RD_GLB._wr_hold_127_96()
        allRegisters["rd_hold63_32"] = _AF6CCI0021_RD_GLB._rd_hold63_32()
        allRegisters["rd_hold95_64"] = _AF6CCI0021_RD_GLB._rd_hold95_64()
        allRegisters["rd_hold_127_96"] = _AF6CCI0021_RD_GLB._rd_hold_127_96()
        allRegisters["mac_addr_b31_0"] = _AF6CCI0021_RD_GLB._mac_addr_b31_0()
        allRegisters["mac_addr_b47_32"] = _AF6CCI0021_RD_GLB._mac_addr_b47_32()
        allRegisters["ip_addr_b31_0"] = _AF6CCI0021_RD_GLB._ip_addr_b31_0()
        allRegisters["ipv6_addr_b63_32"] = _AF6CCI0021_RD_GLB._ipv6_addr_b63_32()
        allRegisters["ipv6_addr_b95_64"] = _AF6CCI0021_RD_GLB._ipv6_addr_b95_64()
        allRegisters["ipv6_addr_b127_96"] = _AF6CCI0021_RD_GLB._ipv6_addr_b127_96()
        allRegisters["RAM_Parity_Force_Control"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control()
        allRegisters["RAM_Parity_Disable_Control"] = _AF6CCI0021_RD_GLB._RAM_Parity_Disable_Control()
        allRegisters["RAM_Parity_Error_Sticky"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky()
        allRegisters["rdha3_0_control"] = _AF6CCI0021_RD_GLB._rdha3_0_control()
        allRegisters["rdha7_4_control"] = _AF6CCI0021_RD_GLB._rdha7_4_control()
        allRegisters["rdha11_8_control"] = _AF6CCI0021_RD_GLB._rdha11_8_control()
        allRegisters["rdha15_12_control"] = _AF6CCI0021_RD_GLB._rdha15_12_control()
        allRegisters["rdha19_16_control"] = _AF6CCI0021_RD_GLB._rdha19_16_control()
        allRegisters["rdha23_20_control"] = _AF6CCI0021_RD_GLB._rdha23_20_control()
        allRegisters["rdha24data_control"] = _AF6CCI0021_RD_GLB._rdha24data_control()
        allRegisters["rdha_hold63_32"] = _AF6CCI0021_RD_GLB._rdha_hold63_32()
        allRegisters["rdindr_hold95_64"] = _AF6CCI0021_RD_GLB._rdindr_hold95_64()
        allRegisters["rdindr_hold127_96"] = _AF6CCI0021_RD_GLB._rdindr_hold127_96()
        allRegisters["alm_temp"] = _AF6CCI0021_RD_GLB._alm_temp()
        return allRegisters

    class _chip_ver(AtRegister.AtRegister):
        def name(self):
            return "Chip Version"
    
        def description(self):
            return "This register is used to check CHIP version."
            
        def width(self):
            return 64
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _YMMDDHH(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 32
        
            def name(self):
                return "YMMDDHH"
            
            def description(self):
                return ""
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _roductID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "roductID"
            
            def description(self):
                return ""
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["YMMDDHH"] = _AF6CCI0021_RD_GLB._chip_ver._YMMDDHH()
            allFields["roductID"] = _AF6CCI0021_RD_GLB._chip_ver._roductID()
            return allFields

    class _chip_act_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Chip Active Control"
    
        def description(self):
            return "This register is used to configure global signal for CHIP."
            
        def width(self):
            return 10
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _PmcPact(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PmcPact"
            
            def description(self):
                return "PMC Active 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EthPact(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "EthPact"
            
            def description(self):
                return "Ethernet Interface Active 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _cdrPact(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "cdrPact"
            
            def description(self):
                return "CDR Active 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _claPact(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "claPact"
            
            def description(self):
                return "CLA Active 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PwePact(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "PwePact"
            
            def description(self):
                return "PWE Active 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PdaPact(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PdaPact"
            
            def description(self):
                return "PDA Active 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _plaPact(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "plaPact"
            
            def description(self):
                return "PLA Active 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapPact(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "MapPact"
            
            def description(self):
                return "MAP Active 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PdhPact(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PdhPact"
            
            def description(self):
                return "PDH Active 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OcnPact(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "OcnPact"
            
            def description(self):
                return "OCN Active 1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PmcPact"] = _AF6CCI0021_RD_GLB._chip_act_ctrl._PmcPact()
            allFields["EthPact"] = _AF6CCI0021_RD_GLB._chip_act_ctrl._EthPact()
            allFields["cdrPact"] = _AF6CCI0021_RD_GLB._chip_act_ctrl._cdrPact()
            allFields["claPact"] = _AF6CCI0021_RD_GLB._chip_act_ctrl._claPact()
            allFields["PwePact"] = _AF6CCI0021_RD_GLB._chip_act_ctrl._PwePact()
            allFields["PdaPact"] = _AF6CCI0021_RD_GLB._chip_act_ctrl._PdaPact()
            allFields["plaPact"] = _AF6CCI0021_RD_GLB._chip_act_ctrl._plaPact()
            allFields["MapPact"] = _AF6CCI0021_RD_GLB._chip_act_ctrl._MapPact()
            allFields["PdhPact"] = _AF6CCI0021_RD_GLB._chip_act_ctrl._PdhPact()
            allFields["OcnPact"] = _AF6CCI0021_RD_GLB._chip_act_ctrl._OcnPact()
            return allFields

    class _inten_status(AtRegister.AtRegister):
        def name(self):
            return "Chip Interrupt Status"
    
        def description(self):
            return "This register configure interrupt enable."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _CDRIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "CDRIntEnable"
            
            def description(self):
                return "CDR Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SysMonTempEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "SysMonTempEnable"
            
            def description(self):
                return "SysMon temperature Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "ParIntEnable"
            
            def description(self):
                return "RAM Parity Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PRMIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "PRMIntEnable"
            
            def description(self):
                return "PRM Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PMIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "PMIntEnable"
            
            def description(self):
                return "FM Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FMIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "FMIntEnable"
            
            def description(self):
                return "FM Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SEUIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "SEUIntEnable"
            
            def description(self):
                return "SEU Interrupt Alarm 1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ETHIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "ETHIntEnable"
            
            def description(self):
                return "ETH Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PWIntEnable"
            
            def description(self):
                return "PW Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1IntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DE1IntEnable"
            
            def description(self):
                return "PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRIntEnable"] = _AF6CCI0021_RD_GLB._inten_status._CDRIntEnable()
            allFields["SysMonTempEnable"] = _AF6CCI0021_RD_GLB._inten_status._SysMonTempEnable()
            allFields["ParIntEnable"] = _AF6CCI0021_RD_GLB._inten_status._ParIntEnable()
            allFields["PRMIntEnable"] = _AF6CCI0021_RD_GLB._inten_status._PRMIntEnable()
            allFields["PMIntEnable"] = _AF6CCI0021_RD_GLB._inten_status._PMIntEnable()
            allFields["FMIntEnable"] = _AF6CCI0021_RD_GLB._inten_status._FMIntEnable()
            allFields["SEUIntEnable"] = _AF6CCI0021_RD_GLB._inten_status._SEUIntEnable()
            allFields["ETHIntEnable"] = _AF6CCI0021_RD_GLB._inten_status._ETHIntEnable()
            allFields["PWIntEnable"] = _AF6CCI0021_RD_GLB._inten_status._PWIntEnable()
            allFields["DE1IntEnable"] = _AF6CCI0021_RD_GLB._inten_status._DE1IntEnable()
            return allFields

    class _inten_ctrl(AtRegister.AtRegister):
        def name(self):
            return "Chip Interrupt Enable Control"
    
        def description(self):
            return "This register configure interrupt enable."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _CDRIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "CDRIntEnable"
            
            def description(self):
                return "CDR Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SysMonTempEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "SysMonTempEnable"
            
            def description(self):
                return "SysMon temperature Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "ParIntEnable"
            
            def description(self):
                return "RAM Parity Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PRMIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "PRMIntEnable"
            
            def description(self):
                return "PRM Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PMIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "PMIntEnable"
            
            def description(self):
                return "FM Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FMIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "FMIntEnable"
            
            def description(self):
                return "FM Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SEUIntEnble(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "SEUIntEnble"
            
            def description(self):
                return "SEU Interrupt Alarm"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EthIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "EthIntEnable"
            
            def description(self):
                return "ETH Port Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PWIntEnable"
            
            def description(self):
                return "PW Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1IntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DE1IntEnable"
            
            def description(self):
                return "PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRIntEnable"] = _AF6CCI0021_RD_GLB._inten_ctrl._CDRIntEnable()
            allFields["SysMonTempEnable"] = _AF6CCI0021_RD_GLB._inten_ctrl._SysMonTempEnable()
            allFields["ParIntEnable"] = _AF6CCI0021_RD_GLB._inten_ctrl._ParIntEnable()
            allFields["PRMIntEnable"] = _AF6CCI0021_RD_GLB._inten_ctrl._PRMIntEnable()
            allFields["PMIntEnable"] = _AF6CCI0021_RD_GLB._inten_ctrl._PMIntEnable()
            allFields["FMIntEnable"] = _AF6CCI0021_RD_GLB._inten_ctrl._FMIntEnable()
            allFields["SEUIntEnble"] = _AF6CCI0021_RD_GLB._inten_ctrl._SEUIntEnble()
            allFields["EthIntEnable"] = _AF6CCI0021_RD_GLB._inten_ctrl._EthIntEnable()
            allFields["PWIntEnable"] = _AF6CCI0021_RD_GLB._inten_ctrl._PWIntEnable()
            allFields["DE1IntEnable"] = _AF6CCI0021_RD_GLB._inten_ctrl._DE1IntEnable()
            return allFields

    class _inten_restore(AtRegister.AtRegister):
        def name(self):
            return "Chip Interrupt Restore Control"
    
        def description(self):
            return "This register configure interrupt restore enable."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0xffffffff

        class _CDRIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "CDRIntEnable"
            
            def description(self):
                return "CDR Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SysMonTempEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "SysMonTempEnable"
            
            def description(self):
                return "SysMon temperature Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ParIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "ParIntEnable"
            
            def description(self):
                return "RAM Parity Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PRMIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "PRMIntEnable"
            
            def description(self):
                return "PRM Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PMIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "PMIntEnable"
            
            def description(self):
                return "FM Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FMIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "FMIntEnable"
            
            def description(self):
                return "FM Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _EthIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "EthIntEnable"
            
            def description(self):
                return "ETH Port Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWIntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PWIntEnable"
            
            def description(self):
                return "PW Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1IntEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DE1IntEnable"
            
            def description(self):
                return "PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRIntEnable"] = _AF6CCI0021_RD_GLB._inten_restore._CDRIntEnable()
            allFields["SysMonTempEnable"] = _AF6CCI0021_RD_GLB._inten_restore._SysMonTempEnable()
            allFields["ParIntEnable"] = _AF6CCI0021_RD_GLB._inten_restore._ParIntEnable()
            allFields["PRMIntEnable"] = _AF6CCI0021_RD_GLB._inten_restore._PRMIntEnable()
            allFields["PMIntEnable"] = _AF6CCI0021_RD_GLB._inten_restore._PMIntEnable()
            allFields["FMIntEnable"] = _AF6CCI0021_RD_GLB._inten_restore._FMIntEnable()
            allFields["EthIntEnable"] = _AF6CCI0021_RD_GLB._inten_restore._EthIntEnable()
            allFields["PWIntEnable"] = _AF6CCI0021_RD_GLB._inten_restore._PWIntEnable()
            allFields["DE1IntEnable"] = _AF6CCI0021_RD_GLB._inten_restore._DE1IntEnable()
            return allFields

    class _inten_PIN(AtRegister.AtRegister):
        def name(self):
            return "Chip Interrupt PIN Control"
    
        def description(self):
            return "This register configure interrupt PIN enable."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000005
            
        def endAddress(self):
            return 0xffffffff

        class _DefectPINEnable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DefectPINEnable"
            
            def description(self):
                return "PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DefectPINEnable"] = _AF6CCI0021_RD_GLB._inten_PIN._DefectPINEnable()
            return allFields

    class _inten_Eport(AtRegister.AtRegister):
        def name(self):
            return "EPort Interrupt Enable Control"
    
        def description(self):
            return "This register configure interrupt enable."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000000c0
            
        def endAddress(self):
            return 0xffffffff

        class _XFI_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "XFI_3"
            
            def description(self):
                return "XFI_3    Interrupt Enable  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "XFI_2"
            
            def description(self):
                return "XFI_2    Interrupt Enable  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "XFI_1"
            
            def description(self):
                return "XFI_1    Interrupt Enable  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "XFI_0"
            
            def description(self):
                return "XFI_0    Interrupt Enable  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "QSGMII_3"
            
            def description(self):
                return "QSGMII_3 Interrupt Enable  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "QSGMII_2"
            
            def description(self):
                return "QSGMII_2 Interrupt Enable  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "QSGMII_1"
            
            def description(self):
                return "QSGMII_1 Interrupt Enable  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "QSGMII_0"
            
            def description(self):
                return "QSGMII_0 Interrupt Enable  1: Enable  0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["XFI_3"] = _AF6CCI0021_RD_GLB._inten_Eport._XFI_3()
            allFields["XFI_2"] = _AF6CCI0021_RD_GLB._inten_Eport._XFI_2()
            allFields["XFI_1"] = _AF6CCI0021_RD_GLB._inten_Eport._XFI_1()
            allFields["XFI_0"] = _AF6CCI0021_RD_GLB._inten_Eport._XFI_0()
            allFields["QSGMII_3"] = _AF6CCI0021_RD_GLB._inten_Eport._QSGMII_3()
            allFields["QSGMII_2"] = _AF6CCI0021_RD_GLB._inten_Eport._QSGMII_2()
            allFields["QSGMII_1"] = _AF6CCI0021_RD_GLB._inten_Eport._QSGMII_1()
            allFields["QSGMII_0"] = _AF6CCI0021_RD_GLB._inten_Eport._QSGMII_0()
            return allFields

    class _linkalm_Eport(AtRegister.AtRegister):
        def name(self):
            return "EPort Link Event"
    
        def description(self):
            return "This register report state change of link status."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000000c4
            
        def endAddress(self):
            return 0xffffffff

        class _XFI_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "XFI_3"
            
            def description(self):
                return "XFI_3    Link State change Event  1: Changed  0: Normal"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "XFI_2"
            
            def description(self):
                return "XFI_2    Link State change Event  1: Changed  0: Normal"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "XFI_1"
            
            def description(self):
                return "XFI_1    Link State change Event  1: Changed  0: Normal"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "XFI_0"
            
            def description(self):
                return "XFI_0    Link State change Event  1: Changed  0: Normal"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "QSGMII_3"
            
            def description(self):
                return "QSGMII_3 Link State change Event  1: Changed  0: Normal"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "QSGMII_2"
            
            def description(self):
                return "QSGMII_2 Link State change Event  1: Changed  0: Normal"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "QSGMII_1"
            
            def description(self):
                return "QSGMII_1 Link State change Event  1: Changed  0: Normal"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "QSGMII_0"
            
            def description(self):
                return "QSGMII_0 Link State change Event  1: Changed  0: Normal"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["XFI_3"] = _AF6CCI0021_RD_GLB._linkalm_Eport._XFI_3()
            allFields["XFI_2"] = _AF6CCI0021_RD_GLB._linkalm_Eport._XFI_2()
            allFields["XFI_1"] = _AF6CCI0021_RD_GLB._linkalm_Eport._XFI_1()
            allFields["XFI_0"] = _AF6CCI0021_RD_GLB._linkalm_Eport._XFI_0()
            allFields["QSGMII_3"] = _AF6CCI0021_RD_GLB._linkalm_Eport._QSGMII_3()
            allFields["QSGMII_2"] = _AF6CCI0021_RD_GLB._linkalm_Eport._QSGMII_2()
            allFields["QSGMII_1"] = _AF6CCI0021_RD_GLB._linkalm_Eport._QSGMII_1()
            allFields["QSGMII_0"] = _AF6CCI0021_RD_GLB._linkalm_Eport._QSGMII_0()
            return allFields

    class _linksta_Eport(AtRegister.AtRegister):
        def name(self):
            return "EPort Link Status"
    
        def description(self):
            return "This register configure interrupt enable."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000000c8
            
        def endAddress(self):
            return 0xffffffff

        class _XFI_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "XFI_3"
            
            def description(self):
                return "XFI_3 Link Status  1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "XFI_2"
            
            def description(self):
                return "XFI_2 Link Status  1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "XFI_1"
            
            def description(self):
                return "XFI_1 Link Status  1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "XFI_0"
            
            def description(self):
                return "XFI_0 Link Status  1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "QSGMII_3"
            
            def description(self):
                return "QSGMII_3 Link Status 1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "QSGMII_2"
            
            def description(self):
                return "QSGMII_2 Link Status 1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "QSGMII_1"
            
            def description(self):
                return "QSGMII_1 Link Status 1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "QSGMII_0"
            
            def description(self):
                return "QSGMII_0 Link Status 1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["XFI_3"] = _AF6CCI0021_RD_GLB._linksta_Eport._XFI_3()
            allFields["XFI_2"] = _AF6CCI0021_RD_GLB._linksta_Eport._XFI_2()
            allFields["XFI_1"] = _AF6CCI0021_RD_GLB._linksta_Eport._XFI_1()
            allFields["XFI_0"] = _AF6CCI0021_RD_GLB._linksta_Eport._XFI_0()
            allFields["QSGMII_3"] = _AF6CCI0021_RD_GLB._linksta_Eport._QSGMII_3()
            allFields["QSGMII_2"] = _AF6CCI0021_RD_GLB._linksta_Eport._QSGMII_2()
            allFields["QSGMII_1"] = _AF6CCI0021_RD_GLB._linksta_Eport._QSGMII_1()
            allFields["QSGMII_0"] = _AF6CCI0021_RD_GLB._linksta_Eport._QSGMII_0()
            return allFields

    class _linkfrc_Eport(AtRegister.AtRegister):
        def name(self):
            return "EPort Link Force"
    
        def description(self):
            return "This register configure interrupt enable."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000000cc
            
        def endAddress(self):
            return 0xffffffff

        class _XFI_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "XFI_3"
            
            def description(self):
                return "XFI_3 Link Force  1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "XFI_2"
            
            def description(self):
                return "XFI_2 Link Force  1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "XFI_1"
            
            def description(self):
                return "XFI_1 Link Force  1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _XFI_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "XFI_0"
            
            def description(self):
                return "XFI_0 Link Force  1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "QSGMII_3"
            
            def description(self):
                return "QSGMII_3 Link Force 1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "QSGMII_2"
            
            def description(self):
                return "QSGMII_2 Link Force 1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "QSGMII_1"
            
            def description(self):
                return "QSGMII_1 Link Force 1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _QSGMII_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "QSGMII_0"
            
            def description(self):
                return "QSGMII_0 Link Force 1: Up  0: Down"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["XFI_3"] = _AF6CCI0021_RD_GLB._linkfrc_Eport._XFI_3()
            allFields["XFI_2"] = _AF6CCI0021_RD_GLB._linkfrc_Eport._XFI_2()
            allFields["XFI_1"] = _AF6CCI0021_RD_GLB._linkfrc_Eport._XFI_1()
            allFields["XFI_0"] = _AF6CCI0021_RD_GLB._linkfrc_Eport._XFI_0()
            allFields["QSGMII_3"] = _AF6CCI0021_RD_GLB._linkfrc_Eport._QSGMII_3()
            allFields["QSGMII_2"] = _AF6CCI0021_RD_GLB._linkfrc_Eport._QSGMII_2()
            allFields["QSGMII_1"] = _AF6CCI0021_RD_GLB._linkfrc_Eport._QSGMII_1()
            allFields["QSGMII_0"] = _AF6CCI0021_RD_GLB._linkfrc_Eport._QSGMII_0()
            return allFields

    class _wr_hold63_32(AtRegister.AtRegister):
        def name(self):
            return "Write Hold Data63_32"
    
        def description(self):
            return "This register is used to write dword2 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0xffffffff

        class _rHold63_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rHold63_32"
            
            def description(self):
                return "The write dword 2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rHold63_32"] = _AF6CCI0021_RD_GLB._wr_hold63_32._rHold63_32()
            return allFields

    class _wr_hold95_64(AtRegister.AtRegister):
        def name(self):
            return "Write Hold Data95_64"
    
        def description(self):
            return "This register is used to write dword3 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000012
            
        def endAddress(self):
            return 0xffffffff

        class _rHold95_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rHold95_64"
            
            def description(self):
                return "The write dword 3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rHold95_64"] = _AF6CCI0021_RD_GLB._wr_hold95_64._rHold95_64()
            return allFields

    class _wr_hold_127_96(AtRegister.AtRegister):
        def name(self):
            return "Write Hold Data127_96"
    
        def description(self):
            return "This register is used to write dword4 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000013
            
        def endAddress(self):
            return 0xffffffff

        class _rHold127_96(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rHold127_96"
            
            def description(self):
                return "The write dword 4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rHold127_96"] = _AF6CCI0021_RD_GLB._wr_hold_127_96._rHold127_96()
            return allFields

    class _rd_hold63_32(AtRegister.AtRegister):
        def name(self):
            return "Read Hold Data63_32"
    
        def description(self):
            return "This register is used to read dword2 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000021
            
        def endAddress(self):
            return 0xffffffff

        class _dHold63_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dHold63_32"
            
            def description(self):
                return "The read dword2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dHold63_32"] = _AF6CCI0021_RD_GLB._rd_hold63_32._dHold63_32()
            return allFields

    class _rd_hold95_64(AtRegister.AtRegister):
        def name(self):
            return "Read Hold Data95_64"
    
        def description(self):
            return "This register is used to read dword3 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000022
            
        def endAddress(self):
            return 0xffffffff

        class _dHold95_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dHold95_64"
            
            def description(self):
                return "The read dword3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dHold95_64"] = _AF6CCI0021_RD_GLB._rd_hold95_64._dHold95_64()
            return allFields

    class _rd_hold_127_96(AtRegister.AtRegister):
        def name(self):
            return "Read Hold Data127_96"
    
        def description(self):
            return "This register is used to read dword4 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000023
            
        def endAddress(self):
            return 0xffffffff

        class _dHold127_96(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dHold127_96"
            
            def description(self):
                return "The read dword4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dHold127_96"] = _AF6CCI0021_RD_GLB._rd_hold_127_96._dHold127_96()
            return allFields

    class _mac_addr_b31_0(AtRegister.AtRegister):
        def name(self):
            return "Mac Address Bit31_0 Control"
    
        def description(self):
            return "This register configures bit 31 to 0 of MAC address."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000100
            
        def endAddress(self):
            return 0xffffffff

        class _acAddress31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "acAddress31_0"
            
            def description(self):
                return "Bit 31 to 0 of MAC Address. Bit 31 is MSB bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["acAddress31_0"] = _AF6CCI0021_RD_GLB._mac_addr_b31_0._acAddress31_0()
            return allFields

    class _mac_addr_b47_32(AtRegister.AtRegister):
        def name(self):
            return "Mac Address Bit47_32 Control"
    
        def description(self):
            return "This register configures bit 47 to 32 of MAC address."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000101
            
        def endAddress(self):
            return 0xffffffff

        class _GeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "GeMode"
            
            def description(self):
                return "GE mode 0:XGMII; 1:GMII"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _acAddress47_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "acAddress47_32"
            
            def description(self):
                return "Bit 47 to 32 of MAC Address. Bit 47 is MSB bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["GeMode"] = _AF6CCI0021_RD_GLB._mac_addr_b47_32._GeMode()
            allFields["acAddress47_32"] = _AF6CCI0021_RD_GLB._mac_addr_b47_32._acAddress47_32()
            return allFields

    class _ip_addr_b31_0(AtRegister.AtRegister):
        def name(self):
            return "IP Address Bit31_0 Control"
    
        def description(self):
            return "This register configures IPv4 address or bit 31 to 0 Ipv6 address."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000102
            
        def endAddress(self):
            return 0xffffffff

        class _pAddress(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pAddress"
            
            def description(self):
                return "Ipv4/IPv6 Address . Bit 0 is LSB bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pAddress"] = _AF6CCI0021_RD_GLB._ip_addr_b31_0._pAddress()
            return allFields

    class _ipv6_addr_b63_32(AtRegister.AtRegister):
        def name(self):
            return "IPv6 Address Bit63_32 Control"
    
        def description(self):
            return "This register configures bit 63 to 32 of Ipv6 address."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000103
            
        def endAddress(self):
            return 0xffffffff

        class _pv6Address63_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pv6Address63_32"
            
            def description(self):
                return "Ipv6 Address bit 63 to 32. Bit 32 is LSB bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pv6Address63_32"] = _AF6CCI0021_RD_GLB._ipv6_addr_b63_32._pv6Address63_32()
            return allFields

    class _ipv6_addr_b95_64(AtRegister.AtRegister):
        def name(self):
            return "IPv6 Address Bit95_64 Control"
    
        def description(self):
            return "This register configures bit 95 to 64 of Ipv6 address."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000104
            
        def endAddress(self):
            return 0xffffffff

        class _pv6Address95_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pv6Address95_64"
            
            def description(self):
                return "Ipv6 Address bit 95 to 64. Bit 64 is LSB bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pv6Address95_64"] = _AF6CCI0021_RD_GLB._ipv6_addr_b95_64._pv6Address95_64()
            return allFields

    class _ipv6_addr_b127_96(AtRegister.AtRegister):
        def name(self):
            return "IPv6 Address Bit127_96 Control"
    
        def description(self):
            return "This register configures bit 127 to 96 of Ipv6 address."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000105
            
        def endAddress(self):
            return 0xffffffff

        class _pv6Address127_96(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pv6Address127_96"
            
            def description(self):
                return "Ipv6 Address bit 95 to 64. Bit 64 is LSB bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pv6Address127_96"] = _AF6CCI0021_RD_GLB._ipv6_addr_b127_96._pv6Address127_96()
            return allFields

    class _RAM_Parity_Force_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM Parity Force Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000107
            
        def endAddress(self):
            return 0xffffffff

        class _DDRECCNonCorErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "DDRECCNonCorErrFrc"
            
            def description(self):
                return "Force parity For DDR ECC NonCorrectable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DDRECCCorErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "DDRECCCorErrFrc"
            
            def description(self):
                return "Force parity For DDR ECC Correctable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DDRCRCErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "DDRCRCErrFrc"
            
            def description(self):
                return "Force parity For DDR CRC"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHBCECtrl5ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "CLAHBCECtrl5ParErrFrc"
            
            def description(self):
                return "Force parity For CLA Classify Classify Per Group Enable Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHBCECtrl4ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "CLAHBCECtrl4ParErrFrc"
            
            def description(self):
                return "Force parity For CLA Classify Pseudowire Type Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHBCECtrl3ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "CLAHBCECtrl3ParErrFrc"
            
            def description(self):
                return "Force parity For CLA Classify HBCE Looking Up Information Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHBCECtrl2ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "CLAHBCECtrl2ParErrFrc"
            
            def description(self):
                return "Force parity For CLA Classify HBCE Hashing Table page1 Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHBCECtrl1ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "CLAHBCECtrl1ParErrFrc"
            
            def description(self):
                return "Force parity For CLA Classify HBCE Hashing Table page0 Control  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAVLANCtrlParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "CLAVLANCtrlParErrFrc"
            
            def description(self):
                return "Force parity For CLA Classify VLAN TAG Lookup Control  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWEHSGrpExtParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PWEHSGrpExtParErrFrc"
            
            def description(self):
                return "Force parity For PWE Pseudowire Transmit HSPW Group Protection Control  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWEUPSRCtrlParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PWEUPSRCtrlParErrFrc"
            
            def description(self):
                return "Force parity For PWE Pseudowire Transmit UPSR Group Enable Control  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWEHSGrpCtrlParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "PWEHSGrpCtrlParErrFrc"
            
            def description(self):
                return "Force parity For PWE Pseudowire Transmit UPSR and HSPW mode Control   RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWEHSPWCtrlParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "PWEHSPWCtrlParErrFrc"
            
            def description(self):
                return "Force parity For PWE Pseudowire Transmit HSPW Label Control  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWESRCCtrlParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PWESRCCtrlParErrFrc"
            
            def description(self):
                return "Force parity For PWE Pseudowire Transmit Header RTP SSRC Value Control  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWEPSNModeCtrlParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PWEPSNModeCtrlParErrFrc"
            
            def description(self):
                return "Force parity For PWE Pseudowire Transmit Ethernet Header Length Control  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWEPSNHeaderCtrlParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PWEPSNHeaderCtrlParErrFrc"
            
            def description(self):
                return "Force parity For PWE Pseudowire Transmit Ethernet Header Value Control  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDADeasCtrlParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PDADeasCtrlParErrFrc"
            
            def description(self):
                return "Force parity For Pseudowire PDA TDM Mode Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAJitBufCtrlParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PDAJitBufCtrlParErrFrc"
            
            def description(self):
                return "Force parity For Pseudowire PDA Jitter Buffer Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAReOrCtrlParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PDAReOrCtrlParErrFrc"
            
            def description(self):
                return "Force parity For Pseudowire PDA Reorder Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PLACtrlParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PLACtrlParErrFrc"
            
            def description(self):
                return "Force parity For Thalassa PDHPW Payload Assemble Payload Size Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRCtrlParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "CDRCtrlParErrFrc"
            
            def description(self):
                return "Force parity For CDR Engine Timing control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSrcCtrlParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "MAPSrcCtrlParErrFrc"
            
            def description(self):
                return "Force parity For Thalassa Map Timing Reference Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPChlCtrlParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "MAPChlCtrlParErrFrc"
            
            def description(self):
                return "Force parity For  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DeMapCtrlParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DeMapCtrlParErrFrc"
            
            def description(self):
                return "Force parity For Thalassa Demap Channel Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHDE1TxSigCtrlParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PDHDE1TxSigCtrlParErrFrc"
            
            def description(self):
                return "Force parity For DS1/E1/J1 Tx Framer Signaling Insertion Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHDE1TxFrmCtrlParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PDHDE1TxFrmCtrlParErrFrc"
            
            def description(self):
                return "Force parity For DS1/E1/J1 Tx Framer Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHDE1RxFrmCtrlParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHDE1RxFrmCtrlParErrFrc"
            
            def description(self):
                return "Force parity For DS1/E1/J1 Rx Framer Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DDRECCNonCorErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._DDRECCNonCorErrFrc()
            allFields["DDRECCCorErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._DDRECCCorErrFrc()
            allFields["DDRCRCErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._DDRCRCErrFrc()
            allFields["CLAHBCECtrl5ParErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._CLAHBCECtrl5ParErrFrc()
            allFields["CLAHBCECtrl4ParErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._CLAHBCECtrl4ParErrFrc()
            allFields["CLAHBCECtrl3ParErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._CLAHBCECtrl3ParErrFrc()
            allFields["CLAHBCECtrl2ParErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._CLAHBCECtrl2ParErrFrc()
            allFields["CLAHBCECtrl1ParErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._CLAHBCECtrl1ParErrFrc()
            allFields["CLAVLANCtrlParErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._CLAVLANCtrlParErrFrc()
            allFields["PWEHSGrpExtParErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._PWEHSGrpExtParErrFrc()
            allFields["PWEUPSRCtrlParErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._PWEUPSRCtrlParErrFrc()
            allFields["PWEHSGrpCtrlParErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._PWEHSGrpCtrlParErrFrc()
            allFields["PWEHSPWCtrlParErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._PWEHSPWCtrlParErrFrc()
            allFields["PWESRCCtrlParErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._PWESRCCtrlParErrFrc()
            allFields["PWEPSNModeCtrlParErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._PWEPSNModeCtrlParErrFrc()
            allFields["PWEPSNHeaderCtrlParErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._PWEPSNHeaderCtrlParErrFrc()
            allFields["PDADeasCtrlParErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._PDADeasCtrlParErrFrc()
            allFields["PDAJitBufCtrlParErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._PDAJitBufCtrlParErrFrc()
            allFields["PDAReOrCtrlParErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._PDAReOrCtrlParErrFrc()
            allFields["PLACtrlParErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._PLACtrlParErrFrc()
            allFields["CDRCtrlParErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._CDRCtrlParErrFrc()
            allFields["MAPSrcCtrlParErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._MAPSrcCtrlParErrFrc()
            allFields["MAPChlCtrlParErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._MAPChlCtrlParErrFrc()
            allFields["DeMapCtrlParErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._DeMapCtrlParErrFrc()
            allFields["PDHDE1TxSigCtrlParErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._PDHDE1TxSigCtrlParErrFrc()
            allFields["PDHDE1TxFrmCtrlParErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._PDHDE1TxFrmCtrlParErrFrc()
            allFields["PDHDE1RxFrmCtrlParErrFrc"] = _AF6CCI0021_RD_GLB._RAM_Parity_Force_Control._PDHDE1RxFrmCtrlParErrFrc()
            return allFields

    class _RAM_Parity_Disable_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM parity Disable Control"
    
        def description(self):
            return "This register configures disable parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000108
            
        def endAddress(self):
            return 0xffffffff

        class _CLAHBCECtrl5ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "CLAHBCECtrl5ParDis"
            
            def description(self):
                return "Disable parity For CLA Classify Classify Per Group Enable Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHBCECtrl4ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "CLAHBCECtrl4ParDis"
            
            def description(self):
                return "Disable parity For CLA Classify Pseudowire Type Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHBCECtrl3ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "CLAHBCECtrl3ParDis"
            
            def description(self):
                return "Disable parity For CLA Classify HBCE Looking Up Information Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHBCECtrl2ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "CLAHBCECtrl2ParDis"
            
            def description(self):
                return "Disable parity For CLA Classify HBCE Hashing Table page1 Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHBCECtrl1ParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "CLAHBCECtrl1ParDis"
            
            def description(self):
                return "Disable parity For CLA Classify HBCE Hashing Table page0 Control  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAVLANCtrlParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "CLAVLANCtrlParDis"
            
            def description(self):
                return "Disable parity For CLA Classify VLAN TAG Lookup Control  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWEHSGrpExtParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PWEHSGrpExtParDis"
            
            def description(self):
                return "Disable parity For PWE Pseudowire Transmit HSPW Group Protection Control  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWEUPSRCtrlParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PWEUPSRCtrlParDis"
            
            def description(self):
                return "Disable parity For PWE Pseudowire Transmit UPSR Group Enable Control  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWEHSGrpCtrlParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "PWEHSGrpCtrlParDis"
            
            def description(self):
                return "Disable parity For PWE Pseudowire Transmit UPSR and HSPW mode Control   RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWEHSPWCtrlParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "PWEHSPWCtrlParDis"
            
            def description(self):
                return "Disable parity For PWE Pseudowire Transmit HSPW Label Control  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWESRCCtrlParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PWESRCCtrlParDis"
            
            def description(self):
                return "Disable parity For PWE Pseudowire Transmit Header RTP SSRC Value Control  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWEPSNModeCtrlParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PWEPSNModeCtrlParDis"
            
            def description(self):
                return "Disable parity For PWE Pseudowire Transmit Ethernet Header Length Control  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWEPSNHeaderCtrlParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PWEPSNHeaderCtrlParDis"
            
            def description(self):
                return "Disable parity For PWE Pseudowire Transmit Ethernet Header Value Control  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDADeasCtrlParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PDADeasCtrlParDis"
            
            def description(self):
                return "Disable parity For Pseudowire PDA TDM Mode Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAJitBufCtrlParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PDAJitBufCtrlParDis"
            
            def description(self):
                return "Disable parity For Pseudowire PDA Jitter Buffer Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAReOrCtrlParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PDAReOrCtrlParDis"
            
            def description(self):
                return "Disable parity For Pseudowire PDA Reorder Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PLACtrlParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PLACtrlParDis"
            
            def description(self):
                return "Disable parity For Thalassa PDHPW Payload Assemble Payload Size Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRCtrlParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "CDRCtrlParDis"
            
            def description(self):
                return "Disable parity For CDR Engine Timing control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSrcCtrlParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "MAPSrcCtrlParDis"
            
            def description(self):
                return "Disable parity For Thalassa Map Timing Reference Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPChlCtrlParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "MAPChlCtrlParDis"
            
            def description(self):
                return "Disable parity For  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DeMapCtrlParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DeMapCtrlParDis"
            
            def description(self):
                return "Disable parity For Thalassa Demap Channel Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHDE1TxSigCtrlParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PDHDE1TxSigCtrlParDis"
            
            def description(self):
                return "Disable parity For DS1/E1/J1 Tx Framer Signaling Insertion Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHDE1TxFrmCtrlParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PDHDE1TxFrmCtrlParDis"
            
            def description(self):
                return "Disable parity For DS1/E1/J1 Tx Framer Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHDE1RxFrmCtrlParDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHDE1RxFrmCtrlParDis"
            
            def description(self):
                return "Disable parity For DS1/E1/J1 Rx Framer Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAHBCECtrl5ParDis"] = _AF6CCI0021_RD_GLB._RAM_Parity_Disable_Control._CLAHBCECtrl5ParDis()
            allFields["CLAHBCECtrl4ParDis"] = _AF6CCI0021_RD_GLB._RAM_Parity_Disable_Control._CLAHBCECtrl4ParDis()
            allFields["CLAHBCECtrl3ParDis"] = _AF6CCI0021_RD_GLB._RAM_Parity_Disable_Control._CLAHBCECtrl3ParDis()
            allFields["CLAHBCECtrl2ParDis"] = _AF6CCI0021_RD_GLB._RAM_Parity_Disable_Control._CLAHBCECtrl2ParDis()
            allFields["CLAHBCECtrl1ParDis"] = _AF6CCI0021_RD_GLB._RAM_Parity_Disable_Control._CLAHBCECtrl1ParDis()
            allFields["CLAVLANCtrlParDis"] = _AF6CCI0021_RD_GLB._RAM_Parity_Disable_Control._CLAVLANCtrlParDis()
            allFields["PWEHSGrpExtParDis"] = _AF6CCI0021_RD_GLB._RAM_Parity_Disable_Control._PWEHSGrpExtParDis()
            allFields["PWEUPSRCtrlParDis"] = _AF6CCI0021_RD_GLB._RAM_Parity_Disable_Control._PWEUPSRCtrlParDis()
            allFields["PWEHSGrpCtrlParDis"] = _AF6CCI0021_RD_GLB._RAM_Parity_Disable_Control._PWEHSGrpCtrlParDis()
            allFields["PWEHSPWCtrlParDis"] = _AF6CCI0021_RD_GLB._RAM_Parity_Disable_Control._PWEHSPWCtrlParDis()
            allFields["PWESRCCtrlParDis"] = _AF6CCI0021_RD_GLB._RAM_Parity_Disable_Control._PWESRCCtrlParDis()
            allFields["PWEPSNModeCtrlParDis"] = _AF6CCI0021_RD_GLB._RAM_Parity_Disable_Control._PWEPSNModeCtrlParDis()
            allFields["PWEPSNHeaderCtrlParDis"] = _AF6CCI0021_RD_GLB._RAM_Parity_Disable_Control._PWEPSNHeaderCtrlParDis()
            allFields["PDADeasCtrlParDis"] = _AF6CCI0021_RD_GLB._RAM_Parity_Disable_Control._PDADeasCtrlParDis()
            allFields["PDAJitBufCtrlParDis"] = _AF6CCI0021_RD_GLB._RAM_Parity_Disable_Control._PDAJitBufCtrlParDis()
            allFields["PDAReOrCtrlParDis"] = _AF6CCI0021_RD_GLB._RAM_Parity_Disable_Control._PDAReOrCtrlParDis()
            allFields["PLACtrlParDis"] = _AF6CCI0021_RD_GLB._RAM_Parity_Disable_Control._PLACtrlParDis()
            allFields["CDRCtrlParDis"] = _AF6CCI0021_RD_GLB._RAM_Parity_Disable_Control._CDRCtrlParDis()
            allFields["MAPSrcCtrlParDis"] = _AF6CCI0021_RD_GLB._RAM_Parity_Disable_Control._MAPSrcCtrlParDis()
            allFields["MAPChlCtrlParDis"] = _AF6CCI0021_RD_GLB._RAM_Parity_Disable_Control._MAPChlCtrlParDis()
            allFields["DeMapCtrlParDis"] = _AF6CCI0021_RD_GLB._RAM_Parity_Disable_Control._DeMapCtrlParDis()
            allFields["PDHDE1TxSigCtrlParDis"] = _AF6CCI0021_RD_GLB._RAM_Parity_Disable_Control._PDHDE1TxSigCtrlParDis()
            allFields["PDHDE1TxFrmCtrlParDis"] = _AF6CCI0021_RD_GLB._RAM_Parity_Disable_Control._PDHDE1TxFrmCtrlParDis()
            allFields["PDHDE1RxFrmCtrlParDis"] = _AF6CCI0021_RD_GLB._RAM_Parity_Disable_Control._PDHDE1RxFrmCtrlParDis()
            return allFields

    class _RAM_Parity_Error_Sticky(AtRegister.AtRegister):
        def name(self):
            return "RAM parity Error Sticky"
    
        def description(self):
            return "This register configures disable parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000112
            
        def endAddress(self):
            return 0xffffffff

        class _DDRECCNonCorErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "DDRECCNonCorErrStk"
            
            def description(self):
                return "Disable parity For DDR ECC NonCorrectable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DDRECCCorErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "DDRECCCorErrStk"
            
            def description(self):
                return "Disable parity For DDR ECC Correctable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DDRCRCErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "DDRCRCErrStk"
            
            def description(self):
                return "Disable parity For DDR CRC"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHBCECtrl5ErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "CLAHBCECtrl5ErrStk"
            
            def description(self):
                return "Disable parity For CLA Classify Classify Per Group Enable Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHBCECtrl4ErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "CLAHBCECtrl4ErrStk"
            
            def description(self):
                return "Disable parity For CLA Classify Pseudowire Type Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHBCECtrl3ErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "CLAHBCECtrl3ErrStk"
            
            def description(self):
                return "Disable parity For CLA Classify HBCE Looking Up Information Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHBCECtrl2ErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "CLAHBCECtrl2ErrStk"
            
            def description(self):
                return "Disable parity For CLA Classify HBCE Hashing Table page1 Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAHBCECtrl1ErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "CLAHBCECtrl1ErrStk"
            
            def description(self):
                return "Disable parity For CLA Classify HBCE Hashing Table page0 Control  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CLAVLANCtrlErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "CLAVLANCtrlErrStk"
            
            def description(self):
                return "Disable parity For CLA Classify VLAN TAG Lookup Control  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWEHSGrpExtErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PWEHSGrpExtErrStk"
            
            def description(self):
                return "Disable parity For PWE Pseudowire Transmit HSPW Group Protection Control  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWEUPSRCtrlErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PWEUPSRCtrlErrStk"
            
            def description(self):
                return "Disable parity For PWE Pseudowire Transmit UPSR Group Enable Control  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWEHSGrpCtrlErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "PWEHSGrpCtrlErrStk"
            
            def description(self):
                return "Disable parity For PWE Pseudowire Transmit UPSR and HSPW mode Control   RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWEHSPWCtrlErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "PWEHSPWCtrlErrStk"
            
            def description(self):
                return "Disable parity For PWE Pseudowire Transmit HSPW Label Control  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWESRCCtrlErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "PWESRCCtrlErrStk"
            
            def description(self):
                return "Disable parity For PWE Pseudowire Transmit Header RTP SSRC Value Control  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWEPSNModeCtrlErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "PWEPSNModeCtrlErrStk"
            
            def description(self):
                return "Disable parity For PWE Pseudowire Transmit Ethernet Header Length Control  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWEPSNHeaderCtrlErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "PWEPSNHeaderCtrlErrStk"
            
            def description(self):
                return "Disable parity For PWE Pseudowire Transmit Ethernet Header Value Control  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDADeasCtrlErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "PDADeasCtrlErrStk"
            
            def description(self):
                return "Disable parity For Pseudowire PDA TDM Mode Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAJitBufCtrlErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "PDAJitBufCtrlErrStk"
            
            def description(self):
                return "Disable parity For Pseudowire PDA Jitter Buffer Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDAReOrCtrlErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "PDAReOrCtrlErrStk"
            
            def description(self):
                return "Disable parity For Pseudowire PDA Reorder Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PLACtrlErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PLACtrlErrStk"
            
            def description(self):
                return "Disable parity For Thalassa PDHPW Payload Assemble Payload Size Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRCtrlErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "CDRCtrlErrStk"
            
            def description(self):
                return "Disable parity For CDR Engine Timing control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPSrcCtrlErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "MAPSrcCtrlErrStk"
            
            def description(self):
                return "Disable parity For Thalassa Map Timing Reference Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MAPChlCtrlErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "MAPChlCtrlErrStk"
            
            def description(self):
                return "Disable parity For  RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DeMapCtrlErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DeMapCtrlErrStk"
            
            def description(self):
                return "Disable parity For Thalassa Demap Channel Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHDE1TxSigCtrlErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PDHDE1TxSigCtrlErrStk"
            
            def description(self):
                return "Disable parity For DS1/E1/J1 Tx Framer Signaling Insertion Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHDE1TxFrmCtrlErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PDHDE1TxFrmCtrlErrStk"
            
            def description(self):
                return "Disable parity For DS1/E1/J1 Tx Framer Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHDE1RxFrmCtrlErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHDE1RxFrmCtrlErrStk"
            
            def description(self):
                return "Disable parity For DS1/E1/J1 Rx Framer Control RAM"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DDRECCNonCorErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._DDRECCNonCorErrStk()
            allFields["DDRECCCorErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._DDRECCCorErrStk()
            allFields["DDRCRCErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._DDRCRCErrStk()
            allFields["CLAHBCECtrl5ErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._CLAHBCECtrl5ErrStk()
            allFields["CLAHBCECtrl4ErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._CLAHBCECtrl4ErrStk()
            allFields["CLAHBCECtrl3ErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._CLAHBCECtrl3ErrStk()
            allFields["CLAHBCECtrl2ErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._CLAHBCECtrl2ErrStk()
            allFields["CLAHBCECtrl1ErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._CLAHBCECtrl1ErrStk()
            allFields["CLAVLANCtrlErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._CLAVLANCtrlErrStk()
            allFields["PWEHSGrpExtErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._PWEHSGrpExtErrStk()
            allFields["PWEUPSRCtrlErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._PWEUPSRCtrlErrStk()
            allFields["PWEHSGrpCtrlErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._PWEHSGrpCtrlErrStk()
            allFields["PWEHSPWCtrlErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._PWEHSPWCtrlErrStk()
            allFields["PWESRCCtrlErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._PWESRCCtrlErrStk()
            allFields["PWEPSNModeCtrlErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._PWEPSNModeCtrlErrStk()
            allFields["PWEPSNHeaderCtrlErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._PWEPSNHeaderCtrlErrStk()
            allFields["PDADeasCtrlErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._PDADeasCtrlErrStk()
            allFields["PDAJitBufCtrlErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._PDAJitBufCtrlErrStk()
            allFields["PDAReOrCtrlErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._PDAReOrCtrlErrStk()
            allFields["PLACtrlErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._PLACtrlErrStk()
            allFields["CDRCtrlErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._CDRCtrlErrStk()
            allFields["MAPSrcCtrlErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._MAPSrcCtrlErrStk()
            allFields["MAPChlCtrlErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._MAPChlCtrlErrStk()
            allFields["DeMapCtrlErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._DeMapCtrlErrStk()
            allFields["PDHDE1TxSigCtrlErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._PDHDE1TxSigCtrlErrStk()
            allFields["PDHDE1TxFrmCtrlErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._PDHDE1TxFrmCtrlErrStk()
            allFields["PDHDE1RxFrmCtrlErrStk"] = _AF6CCI0021_RD_GLB._RAM_Parity_Error_Sticky._PDHDE1RxFrmCtrlErrStk()
            return allFields

    class _rdha3_0_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit3_0 Control"
    
        def description(self):
            return "This register is used to send HA read address bit3_0 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00200 + HaAddr3_0"
            
        def startAddress(self):
            return 0x00000200
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr3_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr3_0"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr3_0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr3_0"] = _AF6CCI0021_RD_GLB._rdha3_0_control._ReadAddr3_0()
            return allFields

    class _rdha7_4_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit7_4 Control"
    
        def description(self):
            return "This register is used to send HA read address bit7_4 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00210 + HaAddr7_4"
            
        def startAddress(self):
            return 0x00000210
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr7_4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr7_4"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr7_4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr7_4"] = _AF6CCI0021_RD_GLB._rdha7_4_control._ReadAddr7_4()
            return allFields

    class _rdha11_8_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit11_8 Control"
    
        def description(self):
            return "This register is used to send HA read address bit11_8 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00220 + HaAddr11_8"
            
        def startAddress(self):
            return 0x00000220
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr11_8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr11_8"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr11_8"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr11_8"] = _AF6CCI0021_RD_GLB._rdha11_8_control._ReadAddr11_8()
            return allFields

    class _rdha15_12_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit15_12 Control"
    
        def description(self):
            return "This register is used to send HA read address bit15_12 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00230 + HaAddr15_12"
            
        def startAddress(self):
            return 0x00000230
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr15_12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr15_12"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr15_12"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr15_12"] = _AF6CCI0021_RD_GLB._rdha15_12_control._ReadAddr15_12()
            return allFields

    class _rdha19_16_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit19_16 Control"
    
        def description(self):
            return "This register is used to send HA read address bit19_16 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00240 + HaAddr19_16"
            
        def startAddress(self):
            return 0x00000240
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr19_16(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr19_16"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr19_16"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr19_16"] = _AF6CCI0021_RD_GLB._rdha19_16_control._ReadAddr19_16()
            return allFields

    class _rdha23_20_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit23_20 Control"
    
        def description(self):
            return "This register is used to send HA read address bit23_20 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00250 + HaAddr23_20"
            
        def startAddress(self):
            return 0x00000250
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr23_20(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr23_20"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr23_20"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr23_20"] = _AF6CCI0021_RD_GLB._rdha23_20_control._ReadAddr23_20()
            return allFields

    class _rdha24data_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit24 and Data Control"
    
        def description(self):
            return "This register is used to send HA read address bit24 to HA engine to read data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00260 + HaAddr24"
            
        def startAddress(self):
            return 0x00000260
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData31_0"
            
            def description(self):
                return "HA read data bit31_0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData31_0"] = _AF6CCI0021_RD_GLB._rdha24data_control._ReadHaData31_0()
            return allFields

    class _rdha_hold63_32(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data63_32"
    
        def description(self):
            return "This register is used to read HA dword2 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000270
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData63_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData63_32"
            
            def description(self):
                return "HA read data bit63_32"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData63_32"] = _AF6CCI0021_RD_GLB._rdha_hold63_32._ReadHaData63_32()
            return allFields

    class _rdindr_hold95_64(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data95_64"
    
        def description(self):
            return "This register is used to read HA dword3 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000271
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData95_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData95_64"
            
            def description(self):
                return "HA read data bit95_64"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData95_64"] = _AF6CCI0021_RD_GLB._rdindr_hold95_64._ReadHaData95_64()
            return allFields

    class _rdindr_hold127_96(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data127_96"
    
        def description(self):
            return "This register is used to read HA dword4 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000272
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData127_96(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData127_96"
            
            def description(self):
                return "HA read data bit127_96"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData127_96"] = _AF6CCI0021_RD_GLB._rdindr_hold127_96._ReadHaData127_96()
            return allFields

    class _alm_temp(AtRegister.AtRegister):
        def name(self):
            return "Temperate Event"
    
        def description(self):
            return "This register report state change of Temperate status."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000000d0
            
        def endAddress(self):
            return 0xffffffff

        class _Temp_alarm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Temp_alarm"
            
            def description(self):
                return "Temperate change Event  1: Changed  0: Normal"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Temp_alarm"] = _AF6CCI0021_RD_GLB._alm_temp._Temp_alarm()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0021_RD_PDH(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["PDH_Global_Control"] = _AF6CCI0021_RD_PDH._PDH_Global_Control()
        allRegisters["PDH_LIU_Tx_SW_Center_FIFO_Configuration"] = _AF6CCI0021_RD_PDH._PDH_LIU_Tx_SW_Center_FIFO_Configuration()
        allRegisters["PDH_LIU_Looptime_Configuration"] = _AF6CCI0021_RD_PDH._PDH_LIU_Looptime_Configuration()
        allRegisters["PDH_LIU_Tx_Clock_Invert_Configuration"] = _AF6CCI0021_RD_PDH._PDH_LIU_Tx_Clock_Invert_Configuration()
        allRegisters["PDH_LIU_Tx_Fifo_Full_Sticky"] = _AF6CCI0021_RD_PDH._PDH_LIU_Tx_Fifo_Full_Sticky()
        allRegisters["PDH_LIU_Tx_Fifo_Empty_Sticky"] = _AF6CCI0021_RD_PDH._PDH_LIU_Tx_Fifo_Empty_Sticky()
        allRegisters["PDH_LIU_Rx_Fifo_Full_Sticky"] = _AF6CCI0021_RD_PDH._PDH_LIU_Rx_Fifo_Full_Sticky()
        allRegisters["dej1_rxfrm_fbe_thrsh"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_fbe_thrsh()
        allRegisters["dej1_rxfrm_crc_thrsh"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_crc_thrsh()
        allRegisters["dej1_rxfrm_rei_thrsh"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_rei_thrsh()
        allRegisters["dej1_rxfrm_ctrl"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_ctrl()
        allRegisters["liuloopin_pen"] = _AF6CCI0021_RD_PDH._liuloopin_pen()
        allRegisters["liuloopinallone_pen"] = _AF6CCI0021_RD_PDH._liuloopinallone_pen()
        allRegisters["dej1_rxfrm_hw_stat"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_hw_stat()
        allRegisters["dej1_rxfrm_crc_err_cnt"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_crc_err_cnt()
        allRegisters["dej1_rxfrm_rei_cnt"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_rei_cnt()
        allRegisters["dej1_rxfrm_fbe_cnt"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_fbe_cnt()
        allRegisters["dej1_rx_framer_per_chn_intr_en_ctrl"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl()
        allRegisters["dej1_rx_framer_per_chn_intr_stat"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_stat()
        allRegisters["dej1_rx_framer_per_chn_curr_stat"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_curr_stat()
        allRegisters["dej1_rx_framer_per_chn_intr_or_stat"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_or_stat()
        allRegisters["dej1_rx_framer_per_stsvc_intr_or_stat"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_stsvc_intr_or_stat()
        allRegisters["dej1_rx_framer_per_stsvc_intr_en_ctrl"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_stsvc_intr_en_ctrl()
        allRegisters["dej1_txfrm_ctrl"] = _AF6CCI0021_RD_PDH._dej1_txfrm_ctrl()
        allRegisters["dej1_txfrm_stat"] = _AF6CCI0021_RD_PDH._dej1_txfrm_stat()
        allRegisters["dej1_txfrm_sign_insert_ctrl"] = _AF6CCI0021_RD_PDH._dej1_txfrm_sign_insert_ctrl()
        allRegisters["dej1_txfrm_sign_id_conv_ctrl"] = _AF6CCI0021_RD_PDH._dej1_txfrm_sign_id_conv_ctrl()
        allRegisters["dej1_txfrm_sign_cpu_insert_en_ctrl"] = _AF6CCI0021_RD_PDH._dej1_txfrm_sign_cpu_insert_en_ctrl()
        allRegisters["dej1_txfrm_sign_cpu_de1_sign_wr_ctrl"] = _AF6CCI0021_RD_PDH._dej1_txfrm_sign_cpu_de1_sign_wr_ctrl()
        allRegisters["dej1_txfrm_sign_buf"] = _AF6CCI0021_RD_PDH._dej1_txfrm_sign_buf()
        allRegisters["indctrl_pen"] = _AF6CCI0021_RD_PDH._indctrl_pen()
        allRegisters["indwr_pen"] = _AF6CCI0021_RD_PDH._indwr_pen()
        allRegisters["indrd_pen"] = _AF6CCI0021_RD_PDH._indrd_pen()
        allRegisters["stkgen031_pen"] = _AF6CCI0021_RD_PDH._stkgen031_pen()
        allRegisters["stkgen3247_pen"] = _AF6CCI0021_RD_PDH._stkgen3247_pen()
        allRegisters["stkrxssmch1"] = _AF6CCI0021_RD_PDH._stkrxssmch1()
        allRegisters["stkrxssmch2"] = _AF6CCI0021_RD_PDH._stkrxssmch2()
        allRegisters["curstassme1"] = _AF6CCI0021_RD_PDH._curstassme1()
        allRegisters["upen_test1"] = _AF6CCI0021_RD_PDH._upen_test1()
        allRegisters["upen_test2"] = _AF6CCI0021_RD_PDH._upen_test2()
        allRegisters["upen_lim_err"] = _AF6CCI0021_RD_PDH._upen_lim_err()
        allRegisters["upen_lim_mat"] = _AF6CCI0021_RD_PDH._upen_lim_mat()
        allRegisters["upen_th_fdl_mat"] = _AF6CCI0021_RD_PDH._upen_th_fdl_mat()
        allRegisters["upen_th_err"] = _AF6CCI0021_RD_PDH._upen_th_err()
        allRegisters["upen_stk"] = _AF6CCI0021_RD_PDH._upen_stk()
        allRegisters["upen_pat"] = _AF6CCI0021_RD_PDH._upen_pat()
        allRegisters["upen_len"] = _AF6CCI0021_RD_PDH._upen_len()
        allRegisters["upen_info"] = _AF6CCI0021_RD_PDH._upen_info()
        allRegisters["upen_fdl_stk"] = _AF6CCI0021_RD_PDH._upen_fdl_stk()
        allRegisters["upen_fdlpat"] = _AF6CCI0021_RD_PDH._upen_fdlpat()
        allRegisters["upen_len_mess"] = _AF6CCI0021_RD_PDH._upen_len_mess()
        allRegisters["upen_fdl_info"] = _AF6CCI0021_RD_PDH._upen_fdl_info()
        allRegisters["upen_th_stt_int"] = _AF6CCI0021_RD_PDH._upen_th_stt_int()
        allRegisters["upen_stk_rs031"] = _AF6CCI0021_RD_PDH._upen_stk_rs031()
        allRegisters["upen_stk_rs3247"] = _AF6CCI0021_RD_PDH._upen_stk_rs3247()
        allRegisters["upen_stk_fl031"] = _AF6CCI0021_RD_PDH._upen_stk_fl031()
        allRegisters["upen_stk_fl3247"] = _AF6CCI0021_RD_PDH._upen_stk_fl3247()
        allRegisters["pcfg_glbenb"] = _AF6CCI0021_RD_PDH._pcfg_glbenb()
        allRegisters["imemrwptrsh1"] = _AF6CCI0021_RD_PDH._imemrwptrsh1()
        allRegisters["imemrwptrsh2"] = _AF6CCI0021_RD_PDH._imemrwptrsh2()
        allRegisters["imemrwpctrl1"] = _AF6CCI0021_RD_PDH._imemrwpctrl1()
        allRegisters["ramberratevtds"] = _AF6CCI0021_RD_PDH._ramberratevtds()
        allRegisters["imemrwptrsh1_ds1_bpv"] = _AF6CCI0021_RD_PDH._imemrwptrsh1_ds1_bpv()
        allRegisters["imemrwptrsh1_e1_bpv"] = _AF6CCI0021_RD_PDH._imemrwptrsh1_e1_bpv()
        allRegisters["isel_alarm1"] = _AF6CCI0021_RD_PDH._isel_alarm1()
        allRegisters["isel_alarm2"] = _AF6CCI0021_RD_PDH._isel_alarm2()
        allRegisters["dej1_errins_en_cfg"] = _AF6CCI0021_RD_PDH._dej1_errins_en_cfg()
        allRegisters["dej1_errins_thr_cfg"] = _AF6CCI0021_RD_PDH._dej1_errins_thr_cfg()
        allRegisters["dej1_rx_errins_en_cfg"] = _AF6CCI0021_RD_PDH._dej1_rx_errins_en_cfg()
        allRegisters["dej1_rx_errins_thr_cfg"] = _AF6CCI0021_RD_PDH._dej1_rx_errins_thr_cfg()
        return allRegisters

    class _PDH_Global_Control(AtRegister.AtRegister):
        def name(self):
            return "PDH Global Control"
    
        def description(self):
            return "This is the global configuration register for the PDH"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00000000"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x00000000

        class _PDHFullLineLoop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "PDHFullLineLoop"
            
            def description(self):
                return "Set 1 to enable full Line Loop Back at LIU side"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHFullPayLoop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "PDHFullPayLoop"
            
            def description(self):
                return "Set 1 to enable full Payload Loop back at Data map side"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHSaturation(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PDHSaturation"
            
            def description(self):
                return "Set 1 to enable Saturation mode of all PDH counters. Set 0 for Roll-Over mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHReservedBit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PDHReservedBit"
            
            def description(self):
                return "Transmit Reserved Bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PDHStufftBit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PDHStufftBit"
            
            def description(self):
                return "Transmit Stuffing Bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHFullLineLoop"] = _AF6CCI0021_RD_PDH._PDH_Global_Control._PDHFullLineLoop()
            allFields["PDHFullPayLoop"] = _AF6CCI0021_RD_PDH._PDH_Global_Control._PDHFullPayLoop()
            allFields["PDHSaturation"] = _AF6CCI0021_RD_PDH._PDH_Global_Control._PDHSaturation()
            allFields["PDHReservedBit"] = _AF6CCI0021_RD_PDH._PDH_Global_Control._PDHReservedBit()
            allFields["PDHStufftBit"] = _AF6CCI0021_RD_PDH._PDH_Global_Control._PDHStufftBit()
            return allFields

    class _PDH_LIU_Tx_SW_Center_FIFO_Configuration(AtRegister.AtRegister):
        def name(self):
            return "PDH LIU Tx SW Center FIFO Configuration"
    
        def description(self):
            return "This is the configuration center the FIFO of transmit side on the LIU interface"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000301
            
        def endAddress(self):
            return 0x00000301

        class _PDHLiuTxFifoCenterCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHLiuTxFifoCenterCfg"
            
            def description(self):
                return "Each bit set to 1 to center the Tx FIFO of corresponding LIU line, set 0 for normal operation"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHLiuTxFifoCenterCfg"] = _AF6CCI0021_RD_PDH._PDH_LIU_Tx_SW_Center_FIFO_Configuration._PDHLiuTxFifoCenterCfg()
            return allFields

    class _PDH_LIU_Looptime_Configuration(AtRegister.AtRegister):
        def name(self):
            return "PDH LIU Looptime Configuration"
    
        def description(self):
            return "This is the configuration for the looptime mode in the LIU interface"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000302
            
        def endAddress(self):
            return 0x00000302

        class _PDHLiuLooptimeCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHLiuLooptimeCfg"
            
            def description(self):
                return "Each bit set to 1 to enable the looptime mode of corresponding LIU line, set 0 for timing from CDR Block operation"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHLiuLooptimeCfg"] = _AF6CCI0021_RD_PDH._PDH_LIU_Looptime_Configuration._PDHLiuLooptimeCfg()
            return allFields

    class _PDH_LIU_Tx_Clock_Invert_Configuration(AtRegister.AtRegister):
        def name(self):
            return "PDH LIU Tx Clock Invert Configuration"
    
        def description(self):
            return "This is the configuration for the transmit clock edge in the LIU interface"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000304
            
        def endAddress(self):
            return 0x00000304

        class _PDHLiuTxClkInvCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHLiuTxClkInvCfg"
            
            def description(self):
                return "Each bit set to 1 for sending the LIU Tx data on positive clock edge of corresponding LIU line, clear to 0 for negative clock edge"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHLiuTxClkInvCfg"] = _AF6CCI0021_RD_PDH._PDH_LIU_Tx_Clock_Invert_Configuration._PDHLiuTxClkInvCfg()
            return allFields

    class _PDH_LIU_Tx_Fifo_Full_Sticky(AtRegister.AtRegister):
        def name(self):
            return "PDH LIU Tx Fifo Full Sticky"
    
        def description(self):
            return "This register reports the FIFO Full status of the LIU Tx"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000310
            
        def endAddress(self):
            return 0x00000310

        class _PDHLiuTxFiFoFull(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHLiuTxFiFoFull"
            
            def description(self):
                return "Each bit set to 1 to report the Tx FIFO full status of corresponding LIU line"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHLiuTxFiFoFull"] = _AF6CCI0021_RD_PDH._PDH_LIU_Tx_Fifo_Full_Sticky._PDHLiuTxFiFoFull()
            return allFields

    class _PDH_LIU_Tx_Fifo_Empty_Sticky(AtRegister.AtRegister):
        def name(self):
            return "PDH LIU Tx Fifo Empty Sticky"
    
        def description(self):
            return "This register reports the FIFO Empty status of the LIU Rx"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000311
            
        def endAddress(self):
            return 0x00000311

        class _PDHLiuTxFifoEmpty(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHLiuTxFifoEmpty"
            
            def description(self):
                return "Each bit set to 1 to report the Rx FIFO empty status of corresponding LIU line"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHLiuTxFifoEmpty"] = _AF6CCI0021_RD_PDH._PDH_LIU_Tx_Fifo_Empty_Sticky._PDHLiuTxFifoEmpty()
            return allFields

    class _PDH_LIU_Rx_Fifo_Full_Sticky(AtRegister.AtRegister):
        def name(self):
            return "PDH LIU Rx Fifo Full Sticky"
    
        def description(self):
            return "This register reports the FIFO Full status of the LIU Rx"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000312
            
        def endAddress(self):
            return 0x00000312

        class _PDHLiuRxFifoFull(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PDHLiuRxFifoFull"
            
            def description(self):
                return "Each bit set to 1 to report the Rx FIFO full status of corresponding LIU line"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PDHLiuRxFifoFull"] = _AF6CCI0021_RD_PDH._PDH_LIU_Rx_Fifo_Full_Sticky._PDHLiuRxFifoFull()
            return allFields

    class _dej1_rxfrm_fbe_thrsh(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer FBE Threshold Control"
    
        def description(self):
            return "This is the FBE threshold configuration register for the PDH DS1/E1/J1 Rx framer"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00050003"
            
        def startAddress(self):
            return 0x00050003
            
        def endAddress(self):
            return 0x00050003

        class _RxDE1FbeThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1FbeThres"
            
            def description(self):
                return "Threshold to generate interrupt if the FBE counter exceed this threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1FbeThres"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_fbe_thrsh._RxDE1FbeThres()
            return allFields

    class _dej1_rxfrm_crc_thrsh(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer CRC Threshold Control"
    
        def description(self):
            return "This is the CRC threshold configuration register for the PDH DS1/E1/J1 Rx framer"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00050004"
            
        def startAddress(self):
            return 0x00050004
            
        def endAddress(self):
            return 0x00050004

        class _RxDE1CrcThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1CrcThres"
            
            def description(self):
                return "Threshold to generate interrupt if the CRC counter exceed this threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1CrcThres"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_crc_thrsh._RxDE1CrcThres()
            return allFields

    class _dej1_rxfrm_rei_thrsh(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer REI Threshold Control"
    
        def description(self):
            return "This is the REI threshold configuration register for the PDH DS1/E1/J1 Rx framer"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00050005"
            
        def startAddress(self):
            return 0x00050005
            
        def endAddress(self):
            return 0x00050005

        class _RxDE1ReiThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1ReiThres"
            
            def description(self):
                return "Threshold to generate interrupt if the FBE counter exceed this threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1ReiThres"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_rei_thrsh._RxDE1ReiThres()
            return allFields

    class _dej1_rxfrm_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer Control"
    
        def description(self):
            return "DS1/E1/J1 Rx framer control is used to configure for Frame mode (DS1, E1, J1) at the DS1/E1 framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00054000 + liuid"
            
        def startAddress(self):
            return 0x00054000
            
        def endAddress(self):
            return 0x0005402f

        class _RxDE1FrcAISAlarm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "RxDE1FrcAISAlarm"
            
            def description(self):
                return "Set 1 to force AIS Alarm when Line/Payload Remote Loopback"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1FrcAISLineDwn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "RxDE1FrcAISLineDwn"
            
            def description(self):
                return "Set 1 to force AIS Line DownStream"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1FrcAISPldDwn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "RxDE1FrcAISPldDwn"
            
            def description(self):
                return "Set 1 to force AIS payload DownStream,L-Bit to PSN"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1FrcLos(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 27
        
            def name(self):
                return "RxDE1FrcLos"
            
            def description(self):
                return "Set 1 to force LOS"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1MFASChken(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "RxDE1MFASChken"
            
            def description(self):
                return "Set 1 to enable check MFAS to go INFRAME in case of E1CRC mode. Default is 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1LosCntThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 23
        
            def name(self):
                return "RxDE1LosCntThres"
            
            def description(self):
                return "Threshold of LOS monitoring block. This function always monitors 	the number of bit 1 of the incoming signal in each of two consecutive frame periods (256*2 bits 	in E1, 193*3 bits in DS1). If it is less than the desire number, LOS will be set then."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1MonOnly(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "RxDE1MonOnly"
            
            def description(self):
                return "Set 1 to enable the Monitor Only mode at the RXDS1/E1 framer"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1LocalLineLoop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "RxDE1LocalLineLoop"
            
            def description(self):
                return "Set 1 to enable Local Line loop back (loop at RX LIU)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDe1LocalPayLoop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "RxDe1LocalPayLoop"
            
            def description(self):
                return "Set 1 to enable Local Payload loop back"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1E1SaCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 17
        
            def name(self):
                return "RxDE1E1SaCfg"
            
            def description(self):
                return "Select position of rx Sa bit 011 : bit3 of frame 1,3,5,7 100 : bit4 of frame 1,3,5,7 101 : bit5 of frame 1,3,5,7 110 : bit6 of frame 1,3,5,7 111 : bit7 of frame 1,3,5,7"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1LofThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 13
        
            def name(self):
                return "RxDE1LofThres"
            
            def description(self):
                return "Threshold for FAS error to declare LOF in DS1/E1/J1 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1AisCntThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 10
        
            def name(self):
                return "RxDE1AisCntThres"
            
            def description(self):
                return "Threshold of AIS monitoring block. This function always monitors the 	number of bit 0 of the incoming signal in each of two consecutive frame periods (256*2 bits in 	E1, 193*3 bits in DS1). If it is less than the desire number, AIS will be set then."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1RaiCntThres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 7
        
            def name(self):
                return "RxDE1RaiCntThres"
            
            def description(self):
                return "Threshold of RAI monitoring block"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1AisMaskEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "RxDE1AisMaskEn"
            
            def description(self):
                return "Set 1 to enable output data of the DS1/E1/J1 framer to be set to all one (1) 	during LOF"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "RxDE1En"
            
            def description(self):
                return "Set 1 to enable the DS1/E1/J1 framer."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1FrcLof(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RxDE1FrcLof"
            
            def description(self):
                return "Set 1 to force Re-frame (default 0)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDs1Md(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDs1Md"
            
            def description(self):
                return "Receive DS1/J1 framing mode 0000: DS1/J1 Unframe 0001: DS1 SF (D4) 0010: DS1 ESF 0011: DS1 DDS 0100: DS1 SLC 0101: J1 SF 0110: J1 ESF 1000: E1 Unframe 1001: E1 Basic Frame 1010: E1 CRC4 Frame"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1FrcAISAlarm"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_ctrl._RxDE1FrcAISAlarm()
            allFields["RxDE1FrcAISLineDwn"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_ctrl._RxDE1FrcAISLineDwn()
            allFields["RxDE1FrcAISPldDwn"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_ctrl._RxDE1FrcAISPldDwn()
            allFields["RxDE1FrcLos"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_ctrl._RxDE1FrcLos()
            allFields["RxDE1MFASChken"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_ctrl._RxDE1MFASChken()
            allFields["RxDE1LosCntThres"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_ctrl._RxDE1LosCntThres()
            allFields["RxDE1MonOnly"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_ctrl._RxDE1MonOnly()
            allFields["RxDE1LocalLineLoop"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_ctrl._RxDE1LocalLineLoop()
            allFields["RxDe1LocalPayLoop"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_ctrl._RxDe1LocalPayLoop()
            allFields["RxDE1E1SaCfg"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_ctrl._RxDE1E1SaCfg()
            allFields["RxDE1LofThres"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_ctrl._RxDE1LofThres()
            allFields["RxDE1AisCntThres"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_ctrl._RxDE1AisCntThres()
            allFields["RxDE1RaiCntThres"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_ctrl._RxDE1RaiCntThres()
            allFields["RxDE1AisMaskEn"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_ctrl._RxDE1AisMaskEn()
            allFields["RxDE1En"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_ctrl._RxDE1En()
            allFields["RxDE1FrcLof"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_ctrl._RxDE1FrcLof()
            allFields["RxDs1Md"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_ctrl._RxDs1Md()
            return allFields

    class _liuloopin_pen(AtRegister.AtRegister):
        def name(self):
            return "Local Line Loopback"
    
        def description(self):
            return "48bit for 48 LIU. Set 1 to Line Local Loopback"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00000305"
            
        def startAddress(self):
            return 0x00000305
            
        def endAddress(self):
            return 0xffffffff

        class _LIUDE1LocalLineLoop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LIUDE1LocalLineLoop"
            
            def description(self):
                return "Set 1 to enable Local Line loop back"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LIUDE1LocalLineLoop"] = _AF6CCI0021_RD_PDH._liuloopin_pen._LIUDE1LocalLineLoop()
            return allFields

    class _liuloopinallone_pen(AtRegister.AtRegister):
        def name(self):
            return "Local Line Loopback"
    
        def description(self):
            return "48bit for 48 LIU. Set 1 to force all one to TDM side for Line Local Loopback"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00000307"
            
        def startAddress(self):
            return 0x00000307
            
        def endAddress(self):
            return 0xffffffff

        class _LIUDE1FrcAlloneLocalLineLoop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 0
        
            def name(self):
                return "LIUDE1FrcAlloneLocalLineLoop"
            
            def description(self):
                return "Set 1 to force all one"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LIUDE1FrcAlloneLocalLineLoop"] = _AF6CCI0021_RD_PDH._liuloopinallone_pen._LIUDE1FrcAlloneLocalLineLoop()
            return allFields

    class _dej1_rxfrm_hw_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer HW Status"
    
        def description(self):
            return "These bit[2:0] of registers are used for Hardware status only, bit[19:16] of registers are indicate SSM E1 value"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00054400 + liuid"
            
        def startAddress(self):
            return 0x00054400
            
        def endAddress(self):
            return 0x0005402f

        class _RXE1SSMSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RXE1SSMSta"
            
            def description(self):
                return "Current value of SSM message"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1Sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1Sta"
            
            def description(self):
                return "5 In Frame"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RXE1SSMSta"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_hw_stat._RXE1SSMSta()
            allFields["RxDE1Sta"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_hw_stat._RxDE1Sta()
            return allFields

    class _dej1_rxfrm_crc_err_cnt(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer CRC Error Counter"
    
        def description(self):
            return "This is the per channel CRC error counter for DS1/E1/J1 receive framer"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00056800 + 64*Rd2Clr + LiuID"
            
        def startAddress(self):
            return 0x00056800
            
        def endAddress(self):
            return 0x0005687f

        class _DE1CrcErrCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE1CrcErrCnt"
            
            def description(self):
                return "DS1/E1/J1 CRC Error Accumulator"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE1CrcErrCnt"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_crc_err_cnt._DE1CrcErrCnt()
            return allFields

    class _dej1_rxfrm_rei_cnt(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer REI Counter"
    
        def description(self):
            return "This is the per channel REI counter for DS1/E1/J1 receive framer"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00057000 + 64*Rd2Clr + LiuID"
            
        def startAddress(self):
            return 0x00057000
            
        def endAddress(self):
            return 0x0005707f

        class _DE1ReiCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE1ReiCnt"
            
            def description(self):
                return "DS1/E1/J1 REI error accumulator"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE1ReiCnt"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_rei_cnt._DE1ReiCnt()
            return allFields

    class _dej1_rxfrm_fbe_cnt(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer FBE Counter"
    
        def description(self):
            return "This is the per channel REI counter for DS1/E1/J1 receive framer"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00056000 + 64*Rd2Clr + LiuID"
            
        def startAddress(self):
            return 0x00056000
            
        def endAddress(self):
            return 0x0005607f

        class _DE1FbeCnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE1FbeCnt"
            
            def description(self):
                return "DS1/E1/J1 F-bit error accumulator"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE1FbeCnt"] = _AF6CCI0021_RD_PDH._dej1_rxfrm_fbe_cnt._DE1FbeCnt()
            return allFields

    class _dej1_rx_framer_per_chn_intr_en_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer per Channel Interrupt Enable Control"
    
        def description(self):
            return "This is the per Channel interrupt enable of DS1/E1/J1 Rx Framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00055000 +  StsID*32 + VtnID"
            
        def startAddress(self):
            return 0x00055000
            
        def endAddress(self):
            return 0x0005503f

        class _DE1BerTCAIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "DE1BerTCAIntrEn"
            
            def description(self):
                return "Set 1 to enable the BER-TCA interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1BerSdIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "DE1BerSdIntrEn"
            
            def description(self):
                return "Set 1 to enable the BER-SD interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1BerSfIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DE1BerSfIntrEn"
            
            def description(self):
                return "Set 1 to enable the BER-SF interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1oblcIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "DE1oblcIntrEn"
            
            def description(self):
                return "Set 1 to enable the new Outband LoopCode detected to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1iblcIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DE1iblcIntrEn"
            
            def description(self):
                return "Set 1 to enable the new Inband LoopCode detected to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1SigRAIIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DE1SigRAIIntrEn"
            
            def description(self):
                return "Set 1 to enable Signalling RAI event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1SigLOFIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DE1SigLOFIntrEn"
            
            def description(self):
                return "Set 1 to enable Signalling LOF event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1FbeIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "DE1FbeIntrEn"
            
            def description(self):
                return "Set 1 to enable change FBE te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1CrcIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DE1CrcIntrEn"
            
            def description(self):
                return "Set 1 to enable change CRC te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1ReiIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DE1ReiIntrEn"
            
            def description(self):
                return "Set 1 to enable change REI te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LomfIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DE1LomfIntrEn"
            
            def description(self):
                return "Set 1 to enable change LOMF te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LosIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DE1LosIntrEn"
            
            def description(self):
                return "Set 1 to enable change LOS te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1AisIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DE1AisIntrEn"
            
            def description(self):
                return "Set 1 to enable change AIS te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1RaiIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DE1RaiIntrEn"
            
            def description(self):
                return "Set 1 to enable change RAI te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LofIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE1LofIntrEn"
            
            def description(self):
                return "Set 1 to enable change LOF te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE1BerTCAIntrEn"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1BerTCAIntrEn()
            allFields["DE1BerSdIntrEn"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1BerSdIntrEn()
            allFields["DE1BerSfIntrEn"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1BerSfIntrEn()
            allFields["DE1oblcIntrEn"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1oblcIntrEn()
            allFields["DE1iblcIntrEn"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1iblcIntrEn()
            allFields["DE1SigRAIIntrEn"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1SigRAIIntrEn()
            allFields["DE1SigLOFIntrEn"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1SigLOFIntrEn()
            allFields["DE1FbeIntrEn"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1FbeIntrEn()
            allFields["DE1CrcIntrEn"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1CrcIntrEn()
            allFields["DE1ReiIntrEn"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1ReiIntrEn()
            allFields["DE1LomfIntrEn"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1LomfIntrEn()
            allFields["DE1LosIntrEn"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1LosIntrEn()
            allFields["DE1AisIntrEn"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1AisIntrEn()
            allFields["DE1RaiIntrEn"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1RaiIntrEn()
            allFields["DE1LofIntrEn"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_en_ctrl._DE1LofIntrEn()
            return allFields

    class _dej1_rx_framer_per_chn_intr_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer per Channel Interrupt Status"
    
        def description(self):
            return "This is the per Channel interrupt tus of DS1/E1/J1 Rx Framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00055040 +  StsID*32 + VtnID"
            
        def startAddress(self):
            return 0x00055040
            
        def endAddress(self):
            return 0x0005507f

        class _DE1BerTCAIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "DE1BerTCAIntr"
            
            def description(self):
                return "Set 1 if there is a change of BER-TCA."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1BerSdIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "DE1BerSdIntr"
            
            def description(self):
                return "Set 1 if there is a change of BER-SD."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1BerSfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DE1BerSfIntr"
            
            def description(self):
                return "Set 1 if there is a change of BER-SF."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1oblcIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "DE1oblcIntr"
            
            def description(self):
                return "Set 1 if there is a change the new Outband LoopCode detected to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1iblcIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DE1iblcIntr"
            
            def description(self):
                return "Set 1 if there is a change the new Inband LoopCode detected to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1SigRAIIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DE1SigRAIIntr"
            
            def description(self):
                return "Set 1 if there is a change Signalling RAI event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1SigLOFIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DE1SigLOFIntr"
            
            def description(self):
                return "Set 1 if there is a change Signalling LOF event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1FbeIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "DE1FbeIntr"
            
            def description(self):
                return "Set 1 if there is a change in FBE exceed threshold te event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1CrcIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DE1CrcIntr"
            
            def description(self):
                return "Set 1 if there is a change in CRC exceed threshold te event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1ReiIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DE1ReiIntr"
            
            def description(self):
                return "Set 1 if there is a change in REI exceed threshold te event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LomfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DE1LomfIntr"
            
            def description(self):
                return "Set 1 if there is a change in LOMF te event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LosIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DE1LosIntr"
            
            def description(self):
                return "Set 1 if there is a change in LOS te event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1AisIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DE1AisIntr"
            
            def description(self):
                return "Set 1 if there is a change in AIS te event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1RaiIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DE1RaiIntr"
            
            def description(self):
                return "Set 1 if there is a change in RAI te event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LofIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE1LofIntr"
            
            def description(self):
                return "Set 1 if there is a change in LOF te event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE1BerTCAIntr"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1BerTCAIntr()
            allFields["DE1BerSdIntr"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1BerSdIntr()
            allFields["DE1BerSfIntr"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1BerSfIntr()
            allFields["DE1oblcIntr"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1oblcIntr()
            allFields["DE1iblcIntr"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1iblcIntr()
            allFields["DE1SigRAIIntr"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1SigRAIIntr()
            allFields["DE1SigLOFIntr"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1SigLOFIntr()
            allFields["DE1FbeIntr"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1FbeIntr()
            allFields["DE1CrcIntr"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1CrcIntr()
            allFields["DE1ReiIntr"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1ReiIntr()
            allFields["DE1LomfIntr"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1LomfIntr()
            allFields["DE1LosIntr"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1LosIntr()
            allFields["DE1AisIntr"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1AisIntr()
            allFields["DE1RaiIntr"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1RaiIntr()
            allFields["DE1LofIntr"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_stat._DE1LofIntr()
            return allFields

    class _dej1_rx_framer_per_chn_curr_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer per Channel Current Status"
    
        def description(self):
            return "This is the per Channel Current tus of DS1/E1/J1 Rx Framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00055080 +  StsID*32 + VtnID"
            
        def startAddress(self):
            return 0x00055080
            
        def endAddress(self):
            return 0x000550bf

        class _DE1BerTCAIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "DE1BerTCAIntr"
            
            def description(self):
                return "Set 1 if there is a change of BER-TCA."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1BerSdIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "DE1BerSdIntr"
            
            def description(self):
                return "Set 1 if there is a change of BER-SD."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1BerSfIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "DE1BerSfIntr"
            
            def description(self):
                return "Set 1 if there is a change of BER-SF."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1oblcIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "DE1oblcIntrSta"
            
            def description(self):
                return "Current status the new Outband LoopCode detected to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1iblcIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "DE1iblcIntrSta"
            
            def description(self):
                return "Current status the new Inband LoopCode detected to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1SigRAIIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "DE1SigRAIIntrSta"
            
            def description(self):
                return "Current status Signalling RAI event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1SigLOFIntrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "DE1SigLOFIntrSta"
            
            def description(self):
                return "Current status Signalling LOF event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1FbeCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "DE1FbeCurrSta"
            
            def description(self):
                return "Current tus of FBE exceed threshold event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1CrcCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "DE1CrcCurrSta"
            
            def description(self):
                return "Current tus of CRC exceed threshold event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1ReiCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "DE1ReiCurrSta"
            
            def description(self):
                return "Current tus of REI exceed threshold event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LomfCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DE1LomfCurrSta"
            
            def description(self):
                return "Current tus of LOMF event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LosCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DE1LosCurrSta"
            
            def description(self):
                return "Current tus of LOS event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1AisCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "DE1AisCurrSta"
            
            def description(self):
                return "Current tus of AIS event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1RaiCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DE1RaiCurrSta"
            
            def description(self):
                return "Current tus of RAI event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1LofCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DE1LofCurrSta"
            
            def description(self):
                return "Current tus of LOF event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DE1BerTCAIntr"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1BerTCAIntr()
            allFields["DE1BerSdIntr"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1BerSdIntr()
            allFields["DE1BerSfIntr"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1BerSfIntr()
            allFields["DE1oblcIntrSta"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1oblcIntrSta()
            allFields["DE1iblcIntrSta"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1iblcIntrSta()
            allFields["DE1SigRAIIntrSta"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1SigRAIIntrSta()
            allFields["DE1SigLOFIntrSta"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1SigLOFIntrSta()
            allFields["DE1FbeCurrSta"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1FbeCurrSta()
            allFields["DE1CrcCurrSta"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1CrcCurrSta()
            allFields["DE1ReiCurrSta"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1ReiCurrSta()
            allFields["DE1LomfCurrSta"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1LomfCurrSta()
            allFields["DE1LosCurrSta"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1LosCurrSta()
            allFields["DE1AisCurrSta"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1AisCurrSta()
            allFields["DE1RaiCurrSta"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1RaiCurrSta()
            allFields["DE1LofCurrSta"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_curr_stat._DE1LofCurrSta()
            return allFields

    class _dej1_rx_framer_per_chn_intr_or_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer per Channel Interrupt OR Status"
    
        def description(self):
            return "The register consists of 32 bits for 32 VT/TUs of the related STS/VC in the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR tus of the related DS1/E1/J1."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x000550C0 +  StsID"
            
        def startAddress(self):
            return 0x000550c0
            
        def endAddress(self):
            return 0x000550c1

        class _RxDE1VtIntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1VtIntrOrSta"
            
            def description(self):
                return "Set to 1 if any interrupt status bit of corresponding DS1/E1/J1 is set and its interrupt is enabled."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1VtIntrOrSta"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_chn_intr_or_stat._RxDE1VtIntrOrSta()
            return allFields

    class _dej1_rx_framer_per_stsvc_intr_or_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer per STS/VC Interrupt OR Status"
    
        def description(self):
            return "The register consists of 2 bits for 2 STS/VCs of the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR tus of the related STS/VC."
            
        def width(self):
            return 2
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000550ff
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE1StsIntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1StsIntrOrSta"
            
            def description(self):
                return "Set to 1 if any interrupt status bit of corresponding STS/VC is set and its interrupt is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1StsIntrOrSta"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_stsvc_intr_or_stat._RxDE1StsIntrOrSta()
            return allFields

    class _dej1_rx_framer_per_stsvc_intr_en_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Framer per STS/VC Interrupt Enable Control"
    
        def description(self):
            return "The register consists of 2 interrupt enable bits for 2 STS/VCs in the Rx DS1/E1/J1 Framer."
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000550fe
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE1StsIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1StsIntrEn"
            
            def description(self):
                return "Set to 1 to enable the related STS/VC to generate interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1StsIntrEn"] = _AF6CCI0021_RD_PDH._dej1_rx_framer_per_stsvc_intr_en_ctrl._RxDE1StsIntrEn()
            return allFields

    class _dej1_txfrm_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer Control"
    
        def description(self):
            return "DS1/E1/J1 Rx framer control is used to configure for Frame mode (DS1, E1, J1) at the DS1/E1 framer."
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00094000 + LiuID"
            
        def startAddress(self):
            return 0x00094000
            
        def endAddress(self):
            return 0x0009402f

        class _unuse(AtRegister.AtRegisterField):
            def stopBit(self):
                return 127
                
            def startBit(self):
                return 32
        
            def name(self):
                return "unuse"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _thr_ssm2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 27
        
            def name(self):
                return "thr_ssm2"
            
            def description(self):
                return "Configured number of SSM message transmit consequent,if all bit thr_ssm2 set, transmit permanent"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sa_loc_conf2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 24
        
            def name(self):
                return "sa_loc_conf2"
            
            def description(self):
                return "Configured possition of SSM bit in E1,  (bit 3/4/5/6/7) of TS0 frame 1/3/5/7"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _mdl_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "mdl_en"
            
            def description(self):
                return "enable mdl ,set 1 to enable, clr 0 to disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1TxSaBit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "TxDE1TxSaBit"
            
            def description(self):
                return "Transmitted unused Sa bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1FbitBypass(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "TxDE1FbitBypass"
            
            def description(self):
                return "Set 1 to bypass Fbit insertion in Tx DS1/E1 Framer"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1LineAisIns(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "TxDE1LineAisIns"
            
            def description(self):
                return "Set 1 to enable Line AIS Insert"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1PayAisIns(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "TxDE1PayAisIns"
            
            def description(self):
                return "Set 1 to enable Payload AIS Insert"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1RmtLineloop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "TxDE1RmtLineloop"
            
            def description(self):
                return "Set 1 to enable remote Line Loop back"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1RmtPayloop(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "TxDE1RmtPayloop"
            
            def description(self):
                return "Set 1 to enable remote Payload Loop back"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1AutoAis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "TxDE1AutoAis"
            
            def description(self):
                return "Set 1 to enable AIS indication from data map block to automatically transmit all 	1s at DS1/E1/J1 Tx framer"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1DlkBypass(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "TxDE1DlkBypass"
            
            def description(self):
                return "Set 1 to bypass FDL insertion"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1DlkSwap(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "TxDE1DlkSwap"
            
            def description(self):
                return "Set 1 ti Swap FDL transmit bit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1DLCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 9
        
            def name(self):
                return "TxDE1DLCfg"
            
            def description(self):
                return "Configured for transmit E1 SSM message : bit[13] set: enable, bit[12:9] Value of SSM."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1AutoYel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "TxDE1AutoYel"
            
            def description(self):
                return "Auto Yellow generation enable 1: Yellow alarm detected from Rx Framer will be automatically transmitted in Tx Framer 0: No automatically Yellow alarm transmitted"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1FrcYel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "TxDE1FrcYel"
            
            def description(self):
                return "SW force to Tx Yellow alarm"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1AutoCrcErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "TxDE1AutoCrcErr"
            
            def description(self):
                return "Auto CRC error enable 1: CRC Error detected from Rx Framer will be automatically transmitted in REI bit of Tx Framer 0: No automatically CRC Error alarm transmitted"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1FrcCrcErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "TxDE1FrcCrcErr"
            
            def description(self):
                return "SW force to Tx CRC Error alarm (REI bit)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1En(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "TxDE1En"
            
            def description(self):
                return "Set 1 to enable the DS1/E1/J1 framer."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1Md(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1Md"
            
            def description(self):
                return "Receive DS1/J1 framing mode 0000: DS1/J1 Unframe 0001: DS1 SF (D4) 0010: DS1 ESF 0011: DS1 DDS 0100: DS1 SLC 0101: J1 SF 0110: J1 ESF 1000: E1 Unframe 1001: E1 Basic Frame 1010: E1 CRC4 Frame"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["unuse"] = _AF6CCI0021_RD_PDH._dej1_txfrm_ctrl._unuse()
            allFields["thr_ssm2"] = _AF6CCI0021_RD_PDH._dej1_txfrm_ctrl._thr_ssm2()
            allFields["sa_loc_conf2"] = _AF6CCI0021_RD_PDH._dej1_txfrm_ctrl._sa_loc_conf2()
            allFields["mdl_en"] = _AF6CCI0021_RD_PDH._dej1_txfrm_ctrl._mdl_en()
            allFields["TxDE1TxSaBit"] = _AF6CCI0021_RD_PDH._dej1_txfrm_ctrl._TxDE1TxSaBit()
            allFields["TxDE1FbitBypass"] = _AF6CCI0021_RD_PDH._dej1_txfrm_ctrl._TxDE1FbitBypass()
            allFields["TxDE1LineAisIns"] = _AF6CCI0021_RD_PDH._dej1_txfrm_ctrl._TxDE1LineAisIns()
            allFields["TxDE1PayAisIns"] = _AF6CCI0021_RD_PDH._dej1_txfrm_ctrl._TxDE1PayAisIns()
            allFields["TxDE1RmtLineloop"] = _AF6CCI0021_RD_PDH._dej1_txfrm_ctrl._TxDE1RmtLineloop()
            allFields["TxDE1RmtPayloop"] = _AF6CCI0021_RD_PDH._dej1_txfrm_ctrl._TxDE1RmtPayloop()
            allFields["TxDE1AutoAis"] = _AF6CCI0021_RD_PDH._dej1_txfrm_ctrl._TxDE1AutoAis()
            allFields["TxDE1DlkBypass"] = _AF6CCI0021_RD_PDH._dej1_txfrm_ctrl._TxDE1DlkBypass()
            allFields["TxDE1DlkSwap"] = _AF6CCI0021_RD_PDH._dej1_txfrm_ctrl._TxDE1DlkSwap()
            allFields["TxDE1DLCfg"] = _AF6CCI0021_RD_PDH._dej1_txfrm_ctrl._TxDE1DLCfg()
            allFields["TxDE1AutoYel"] = _AF6CCI0021_RD_PDH._dej1_txfrm_ctrl._TxDE1AutoYel()
            allFields["TxDE1FrcYel"] = _AF6CCI0021_RD_PDH._dej1_txfrm_ctrl._TxDE1FrcYel()
            allFields["TxDE1AutoCrcErr"] = _AF6CCI0021_RD_PDH._dej1_txfrm_ctrl._TxDE1AutoCrcErr()
            allFields["TxDE1FrcCrcErr"] = _AF6CCI0021_RD_PDH._dej1_txfrm_ctrl._TxDE1FrcCrcErr()
            allFields["TxDE1En"] = _AF6CCI0021_RD_PDH._dej1_txfrm_ctrl._TxDE1En()
            allFields["RxDE1Md"] = _AF6CCI0021_RD_PDH._dej1_txfrm_ctrl._RxDE1Md()
            return allFields

    class _dej1_txfrm_stat(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer Status"
    
        def description(self):
            return "These registers are used for Hardware status only"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00094400 + LiuID"
            
        def startAddress(self):
            return 0x00094400
            
        def endAddress(self):
            return 0x0009442f

        class _RxDE1Sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1Sta"
            
            def description(self):
                return ""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1Sta"] = _AF6CCI0021_RD_PDH._dej1_txfrm_stat._RxDE1Sta()
            return allFields

    class _dej1_txfrm_sign_insert_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer Signaling Insertion Control"
    
        def description(self):
            return "DS1/E1/J1 Rx framer signaling insertion control is used to configure for signaling operation modes at the DS1/E1 framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00094800 + LiuID"
            
        def startAddress(self):
            return 0x00094800
            
        def endAddress(self):
            return 0x0009482f

        class _TxDE1SigMfrmEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 30
        
            def name(self):
                return "TxDE1SigMfrmEn"
            
            def description(self):
                return "Set 1 to enable the Tx DS1/E1/J1 framer to sync the signaling multiframe to 	the incoming data flow. No applicable for DS1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1SigBypass(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE1SigBypass"
            
            def description(self):
                return "30 signaling bypass bit for 32 DS0 in an E1 frame. In DS1 mode, only 	24 LSB bits is used. Set 1 to bypass Signaling insertion in Tx DS1/E1 Framer"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1SigMfrmEn"] = _AF6CCI0021_RD_PDH._dej1_txfrm_sign_insert_ctrl._TxDE1SigMfrmEn()
            allFields["TxDE1SigBypass"] = _AF6CCI0021_RD_PDH._dej1_txfrm_sign_insert_ctrl._TxDE1SigBypass()
            return allFields

    class _dej1_txfrm_sign_id_conv_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer Signaling ID Conversion Control"
    
        def description(self):
            return "DS1/E1/J1 Rx framer signaling ID conversion control is used to convert the PW ID to DS1/E1/J1 Line ID at the DS1/E1 framer."
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000C2000 + pwid"
            
        def startAddress(self):
            return 0x000c2000
            
        def endAddress(self):
            return 0x000c207f

        class _TxDE1SigDS1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 41
                
            def startBit(self):
                return 41
        
            def name(self):
                return "TxDE1SigDS1"
            
            def description(self):
                return "Set 1 if the PW carry DS0 for the DS1/J1, set 0 for E1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1SigLineID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 40
                
            def startBit(self):
                return 32
        
            def name(self):
                return "TxDE1SigLineID"
            
            def description(self):
                return "Output DS1/E1/J1 Line ID of the conversion."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1SigEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE1SigEnb"
            
            def description(self):
                return "32 signaling enable bit for 32 DS0 in an E1 frame. In DS1 mode, only 24 LSB bits is used."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1SigDS1"] = _AF6CCI0021_RD_PDH._dej1_txfrm_sign_id_conv_ctrl._TxDE1SigDS1()
            allFields["TxDE1SigLineID"] = _AF6CCI0021_RD_PDH._dej1_txfrm_sign_id_conv_ctrl._TxDE1SigLineID()
            allFields["TxDE1SigEnb"] = _AF6CCI0021_RD_PDH._dej1_txfrm_sign_id_conv_ctrl._TxDE1SigEnb()
            return allFields

    class _dej1_txfrm_sign_cpu_insert_en_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer Signaling CPU Insert Enable Control"
    
        def description(self):
            return "This is the CPU signaling insert global enable bit"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000c8001
            
        def endAddress(self):
            return 0x000c8001

        class _TxDE1SigCPUMd(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE1SigCPUMd"
            
            def description(self):
                return "Set 1 to enable CPU mode (all signaling is from CPU)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1SigCPUMd"] = _AF6CCI0021_RD_PDH._dej1_txfrm_sign_cpu_insert_en_ctrl._TxDE1SigCPUMd()
            return allFields

    class _dej1_txfrm_sign_cpu_de1_sign_wr_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer Signaling CPU DS1/E1 signaling Write Control"
    
        def description(self):
            return "This is the CPU signaling write control"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000c8002
            
        def endAddress(self):
            return 0x000c8002

        class _TxDE1SigCPUWrCtrl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE1SigCPUWrCtrl"
            
            def description(self):
                return "Set 1 if signaling value written to the Signaling buffer in next CPU cylce 	is for DS1/J1, set 0 for E1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1SigCPUWrCtrl"] = _AF6CCI0021_RD_PDH._dej1_txfrm_sign_cpu_de1_sign_wr_ctrl._TxDE1SigCPUWrCtrl()
            return allFields

    class _dej1_txfrm_sign_buf(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Tx Framer Signaling Buffer"
    
        def description(self):
            return "DS1/E1/J1 Rx framer signaling ABCD bit store for each DS0 at the DS1/E1 framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000CC000 + 32*LiuID + tscnt"
            
        def startAddress(self):
            return 0x000cc000
            
        def endAddress(self):
            return 0x000cc7ff

        class _TxDE1SigABCD(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE1SigABCD"
            
            def description(self):
                return "4-bit ABCD signaling for each DS0 (Note: for DS1/J1, must write the 	0x000C8002 value 1 before writing this ABCD, for E1  must write the 0x000C8002 value 0 	before writing this ABCD)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1SigABCD"] = _AF6CCI0021_RD_PDH._dej1_txfrm_sign_buf._TxDE1SigABCD()
            return allFields

    class _indctrl_pen(AtRegister.AtRegister):
        def name(self):
            return "Register Control Indirect Access"
    
        def description(self):
            return "Use to enable one Liu id which want to config indirect for FDL insert"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00090008
            
        def endAddress(self):
            return 0xffffffff

        class _unuse(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "unuse"
            
            def description(self):
                return "unuse"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _pinden(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "pinden"
            
            def description(self):
                return "set 1 to enable indirect access"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _dont_care(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 7
        
            def name(self):
                return "dont_care"
            
            def description(self):
                return "unuse"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _indctrlpdo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "indctrlpdo"
            
            def description(self):
                return "bit [6] set 1 : read indirect , bit [6] clear 0 : write indicate, bit [5:0]     : Liu id access indirect"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["unuse"] = _AF6CCI0021_RD_PDH._indctrl_pen._unuse()
            allFields["pinden"] = _AF6CCI0021_RD_PDH._indctrl_pen._pinden()
            allFields["dont_care"] = _AF6CCI0021_RD_PDH._indctrl_pen._dont_care()
            allFields["indctrlpdo"] = _AF6CCI0021_RD_PDH._indctrl_pen._indctrlpdo()
            return allFields

    class _indwr_pen(AtRegister.AtRegister):
        def name(self):
            return "Info Cpu write Indirect Access"
    
        def description(self):
            return "Use to write FDL config"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00090009
            
        def endAddress(self):
            return 0xffffffff

        class _unuse(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 13
        
            def name(self):
                return "unuse"
            
            def description(self):
                return "unuse"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_gap(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 11
        
            def name(self):
                return "idle_gap"
            
            def description(self):
                return "number idle pattern beetwen 2 fdl message"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _thr_rep(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 7
        
            def name(self):
                return "thr_rep"
            
            def description(self):
                return "Times fdl message is repeated"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _force_bom(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "force_bom"
            
            def description(self):
                return "cpu create rising edge trigger to gen bom message"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _patt_bom2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "patt_bom2"
            
            def description(self):
                return "6bit x of pattern  bom message in format is 0xxxxxx0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["unuse"] = _AF6CCI0021_RD_PDH._indwr_pen._unuse()
            allFields["idle_gap"] = _AF6CCI0021_RD_PDH._indwr_pen._idle_gap()
            allFields["thr_rep"] = _AF6CCI0021_RD_PDH._indwr_pen._thr_rep()
            allFields["force_bom"] = _AF6CCI0021_RD_PDH._indwr_pen._force_bom()
            allFields["patt_bom2"] = _AF6CCI0021_RD_PDH._indwr_pen._patt_bom2()
            return allFields

    class _indrd_pen(AtRegister.AtRegister):
        def name(self):
            return "Info Cpu write Indirect Access"
    
        def description(self):
            return "Use to check config FDL"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0009000a
            
        def endAddress(self):
            return 0xffffffff

        class _unuse(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 13
        
            def name(self):
                return "unuse"
            
            def description(self):
                return "unuse"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _indrdpdo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 0
        
            def name(self):
                return "indrdpdo"
            
            def description(self):
                return "read to check config fdl indirect"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["unuse"] = _AF6CCI0021_RD_PDH._indrd_pen._unuse()
            allFields["indrdpdo"] = _AF6CCI0021_RD_PDH._indrd_pen._indrdpdo()
            return allFields

    class _stkgen031_pen(AtRegister.AtRegister):
        def name(self):
            return "Sticky Insert done liu 0 to 31"
    
        def description(self):
            return "Use to indicate insert done"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0009000b
            
        def endAddress(self):
            return 0xffffffff

        class _gendone0_31(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "gendone0_31"
            
            def description(self):
                return "sticky gen done for liu0 to liu 31"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["gendone0_31"] = _AF6CCI0021_RD_PDH._stkgen031_pen._gendone0_31()
            return allFields

    class _stkgen3247_pen(AtRegister.AtRegister):
        def name(self):
            return "Sticky Insert done liu 31 to 47"
    
        def description(self):
            return "Use to indicate insert done"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0009000c
            
        def endAddress(self):
            return 0xffffffff

        class _gendone32_47(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "gendone32_47"
            
            def description(self):
                return "sticky gen done for liu32 to liu 47"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["gendone32_47"] = _AF6CCI0021_RD_PDH._stkgen3247_pen._gendone32_47()
            return allFields

    class _stkrxssmch1(AtRegister.AtRegister):
        def name(self):
            return "Sticky Indicate change SSM messsage of ID0-ID31"
    
        def description(self):
            return "Use to indicate SSM message of ID0-ID31 change"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00050006
            
        def endAddress(self):
            return 0xffffffff

        class _stk_ssm0031_pen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stk_ssm0031_pen"
            
            def description(self):
                return "sticky change for liu0 to liu 31"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["stk_ssm0031_pen"] = _AF6CCI0021_RD_PDH._stkrxssmch1._stk_ssm0031_pen()
            return allFields

    class _stkrxssmch2(AtRegister.AtRegister):
        def name(self):
            return "Sticky Indicate change SSM messsage of ID32-ID47"
    
        def description(self):
            return "Use to indicate SSM message of ID32-ID47 change"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00050007
            
        def endAddress(self):
            return 0xffffffff

        class _stk_ssm3247_pen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stk_ssm3247_pen"
            
            def description(self):
                return "sticky change for liu32 to liu 47"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["stk_ssm3247_pen"] = _AF6CCI0021_RD_PDH._stkrxssmch2._stk_ssm3247_pen()
            return allFields

    class _curstassme1(AtRegister.AtRegister):
        def name(self):
            return "Current Status SSM message of E1"
    
        def description(self):
            return "These registers are used for Softwave decode SSM message"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0_54600 + LiuID"
            
        def startAddress(self):
            return 0x00054600
            
        def endAddress(self):
            return 0x0005462f

        class _unuse(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 4
        
            def name(self):
                return "unuse"
            
            def description(self):
                return "unuse"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _crr_ssm_sta2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "crr_ssm_sta2"
            
            def description(self):
                return "4bit message of SSM receive"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["unuse"] = _AF6CCI0021_RD_PDH._curstassme1._unuse()
            allFields["crr_ssm_sta2"] = _AF6CCI0021_RD_PDH._curstassme1._crr_ssm_sta2()
            return allFields

    class _upen_test1(AtRegister.AtRegister):
        def name(self):
            return "LC test reg1"
    
        def description(self):
            return "For testing only"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0000
            
        def endAddress(self):
            return 0xffffffff

        class _test_reg1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "test_reg1"
            
            def description(self):
                return "value of reg1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["test_reg1"] = _AF6CCI0021_RD_PDH._upen_test1._test_reg1()
            return allFields

    class _upen_test2(AtRegister.AtRegister):
        def name(self):
            return "LC test reg2"
    
        def description(self):
            return "For testing only"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0001
            
        def endAddress(self):
            return 0xffffffff

        class _test_reg2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "test_reg2"
            
            def description(self):
                return "value of reg2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["test_reg2"] = _AF6CCI0021_RD_PDH._upen_test2._test_reg2()
            return allFields

    class _upen_lim_err(AtRegister.AtRegister):
        def name(self):
            return "Config Limit error"
    
        def description(self):
            return "Check pattern error in state matching pattern"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0002
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_lim_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_lim_err"
            
            def description(self):
                return "if cnt err more than limit error,search failed"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_lim_err"] = _AF6CCI0021_RD_PDH._upen_lim_err._cfg_lim_err()
            return allFields

    class _upen_lim_mat(AtRegister.AtRegister):
        def name(self):
            return "Config limit match"
    
        def description(self):
            return "Used to check parttern match"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0003
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_lim_mat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_lim_mat"
            
            def description(self):
                return "if cnt match more than limit mat, serch ok"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_lim_mat"] = _AF6CCI0021_RD_PDH._upen_lim_mat._cfg_lim_mat()
            return allFields

    class _upen_th_fdl_mat(AtRegister.AtRegister):
        def name(self):
            return "Config thresh fdl parttern"
    
        def description(self):
            return "Check fdl message codes match"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0004
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_th_fdl_mat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_th_fdl_mat"
            
            def description(self):
                return "if message codes repeat more than thresh, search ok,"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_th_fdl_mat"] = _AF6CCI0021_RD_PDH._upen_th_fdl_mat._cfg_th_fdl_mat()
            return allFields

    class _upen_th_err(AtRegister.AtRegister):
        def name(self):
            return "Config thersh error"
    
        def description(self):
            return "Max error for one pattern"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0005
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_th_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_th_err"
            
            def description(self):
                return "if pattern error more than thresh , change to check another pattern"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_th_err"] = _AF6CCI0021_RD_PDH._upen_th_err._cfg_th_err()
            return allFields

    class _upen_stk(AtRegister.AtRegister):
        def name(self):
            return "Sticky pattern detected"
    
        def description(self):
            return "Indicate status of fifo inband partern empty or full"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e13ff
            
        def endAddress(self):
            return 0xffffffff

        class _updo_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_stk"
            
            def description(self):
                return "bit[0] = 1 :empty bit[1] = 1 :full, bit[2] = 1 :interrupt"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_stk"] = _AF6CCI0021_RD_PDH._upen_stk._updo_stk()
            return allFields

    class _upen_pat(AtRegister.AtRegister):
        def name(self):
            return "Config User programmble Pattern User Codes"
    
        def description(self):
            return "config patter search"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0xE_13F0 + $pat"
            
        def startAddress(self):
            return 0x000e13f0
            
        def endAddress(self):
            return 0xffffffff

        class _updo_pat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_pat"
            
            def description(self):
                return "patt codes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_pat"] = _AF6CCI0021_RD_PDH._upen_pat._updo_pat()
            return allFields

    class _upen_len(AtRegister.AtRegister):
        def name(self):
            return "Number of pattern"
    
        def description(self):
            return "Indicate number of pattern is detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e13fe
            
        def endAddress(self):
            return 0xffffffff

        class _info_len_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "info_len_l"
            
            def description(self):
                return "number patt detected"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["info_len_l"] = _AF6CCI0021_RD_PDH._upen_len._info_len_l()
            return allFields

    class _upen_info(AtRegister.AtRegister):
        def name(self):
            return "Cur Pattern Inband Detected"
    
        def description(self):
            return "Used to report current status of id0 to id47"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0xE_0C00 + lid"
            
        def startAddress(self):
            return 0x000e0c00
            
        def endAddress(self):
            return 0x000e0c2f

        class _updo_sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_sta"
            
            def description(self):
                return "bit [2:0] == 0x0  CSU UP bit [2:0] == 0x1  CSU DOWN bit [2:0] == 0x2  FAC1 UP bit [2:0] == 0x3  FAC1 DOWN bit [2:0] == 0x4  FAC2 UP bit [2:0] == 0x5  FAC2 DOWN bit [2:0] == 0x7  idle"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_sta"] = _AF6CCI0021_RD_PDH._upen_info._updo_sta()
            return allFields

    class _upen_fdl_stk(AtRegister.AtRegister):
        def name(self):
            return "Sticky FDL message detected"
    
        def description(self):
            return "Indicate status of fifo fdl message empty or full"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0bff
            
        def endAddress(self):
            return 0xffffffff

        class _updo_fdlstk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_fdlstk"
            
            def description(self):
                return "bit[0] = 1 :empty, bit[1] = 1 :full, bit[2] = 1 :interrupt"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_fdlstk"] = _AF6CCI0021_RD_PDH._upen_fdl_stk._updo_fdlstk()
            return allFields

    class _upen_fdlpat(AtRegister.AtRegister):
        def name(self):
            return "Config User programmble Pattern User Codes"
    
        def description(self):
            return "check fdl message codes"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0bfd
            
        def endAddress(self):
            return 0xffffffff

        class _updo_fdl_mess(AtRegister.AtRegisterField):
            def stopBit(self):
                return 55
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_fdl_mess"
            
            def description(self):
                return "mess codes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_fdl_mess"] = _AF6CCI0021_RD_PDH._upen_fdlpat._updo_fdl_mess()
            return allFields

    class _upen_len_mess(AtRegister.AtRegister):
        def name(self):
            return "Number of message"
    
        def description(self):
            return "Indicate number of message codes is detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0bfe
            
        def endAddress(self):
            return 0xffffffff

        class _info_len_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "info_len_l"
            
            def description(self):
                return "number patt detected"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["info_len_l"] = _AF6CCI0021_RD_PDH._upen_len_mess._info_len_l()
            return allFields

    class _upen_fdl_info(AtRegister.AtRegister):
        def name(self):
            return "Cur Message FDL Detected"
    
        def description(self):
            return "Info fdl message detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0xE_0400 + $fdl_id"
            
        def startAddress(self):
            return 0x000e0400
            
        def endAddress(self):
            return 0x000e042f

        class _mess_info_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mess_info_l"
            
            def description(self):
                return "message info bit[15:8] :	8bit BOM pattern format 0xxxxxx0 bit[4]    : set 1 indicate BOM message  bit[3:0]== 0x0 11111111_01111110 bit[3:0]== 0x1 line loop up bit[3:0]== 0x2 line loop down bit[3:0]== 0x3 payload loop up bit[3:0]== 0x4 payload loop down bit[3:0]== 0x5 smartjack act bit[3:0]== 0x6 smartjact deact bit[3:0]== 0x7 idle"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mess_info_l"] = _AF6CCI0021_RD_PDH._upen_fdl_info._mess_info_l()
            return allFields

    class _upen_th_stt_int(AtRegister.AtRegister):
        def name(self):
            return "Config Theshold Pattern Report"
    
        def description(self):
            return "Maximum Pattern Report"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0006
            
        def endAddress(self):
            return 0xffffffff

        class _updo_th_stt_int(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_th_stt_int"
            
            def description(self):
                return "if number of pattern detected more than threshold, interrupt enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_th_stt_int"] = _AF6CCI0021_RD_PDH._upen_th_stt_int._updo_th_stt_int()
            return allFields

    class _upen_stk_rs031(AtRegister.AtRegister):
        def name(self):
            return "Sticky Loopcode detected id0 to id31"
    
        def description(self):
            return "Indicate loopcode is detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e13fa
            
        def endAddress(self):
            return 0xffffffff

        class _updo_stk_rs031(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_stk_rs031"
            
            def description(self):
                return "bit map sticky of id0,...,id31"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_stk_rs031"] = _AF6CCI0021_RD_PDH._upen_stk_rs031._updo_stk_rs031()
            return allFields

    class _upen_stk_rs3247(AtRegister.AtRegister):
        def name(self):
            return "Sticky Loopcode detected id32 to id47"
    
        def description(self):
            return "Indicate loopcode is detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e13f9
            
        def endAddress(self):
            return 0xffffffff

        class _updo_stk_rs3247(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_stk_rs3247"
            
            def description(self):
                return "bit map sticky of id32,...,id47"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_stk_rs3247"] = _AF6CCI0021_RD_PDH._upen_stk_rs3247._updo_stk_rs3247()
            return allFields

    class _upen_stk_fl031(AtRegister.AtRegister):
        def name(self):
            return "Sticky Loopcode down id0 to id31"
    
        def description(self):
            return "Indicate loopcode is down"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e13fc
            
        def endAddress(self):
            return 0xffffffff

        class _updo_stk_fl031(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_stk_fl031"
            
            def description(self):
                return "bit map sticky of id0,...,id31"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_stk_fl031"] = _AF6CCI0021_RD_PDH._upen_stk_fl031._updo_stk_fl031()
            return allFields

    class _upen_stk_fl3247(AtRegister.AtRegister):
        def name(self):
            return "Sticky Loopcode down id32 to id47"
    
        def description(self):
            return "Indicate loopcode is down"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e13fb
            
        def endAddress(self):
            return 0xffffffff

        class _updo_stk_fl3247(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_stk_fl3247"
            
            def description(self):
                return "bit map sticky of id32,...,id47"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_stk_fl3247"] = _AF6CCI0021_RD_PDH._upen_stk_fl3247._updo_stk_fl3247()
            return allFields

    class _pcfg_glbenb(AtRegister.AtRegister):
        def name(self):
            return "POH BER Global Control"
    
        def description(self):
            return "This register is used to enable STS,VT,DSN globally."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00060000
            
        def endAddress(self):
            return 0xffffffff

        class _timerenb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "timerenb"
            
            def description(self):
                return "Enable timer"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _stsenb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "stsenb"
            
            def description(self):
                return "reserve1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _vtenb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "vtenb"
            
            def description(self):
                return "reserve2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _dsnsenb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dsnsenb"
            
            def description(self):
                return "Enable DE1 channel"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["timerenb"] = _AF6CCI0021_RD_PDH._pcfg_glbenb._timerenb()
            allFields["stsenb"] = _AF6CCI0021_RD_PDH._pcfg_glbenb._stsenb()
            allFields["vtenb"] = _AF6CCI0021_RD_PDH._pcfg_glbenb._vtenb()
            allFields["dsnsenb"] = _AF6CCI0021_RD_PDH._pcfg_glbenb._dsnsenb()
            return allFields

    class _imemrwptrsh1(AtRegister.AtRegister):
        def name(self):
            return "POH BER Threshold 1"
    
        def description(self):
            return "This register is used to configure threshold of BER level 3."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x06_2047 + $Rate[2:0]*8  + $Rate[6:3]*128"
            
        def startAddress(self):
            return 0x00062047
            
        def endAddress(self):
            return 0xffffffff

        class _setthres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 9
        
            def name(self):
                return "setthres"
            
            def description(self):
                return "SetThreshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _winthres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 0
        
            def name(self):
                return "winthres"
            
            def description(self):
                return "WindowThreshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["setthres"] = _AF6CCI0021_RD_PDH._imemrwptrsh1._setthres()
            allFields["winthres"] = _AF6CCI0021_RD_PDH._imemrwptrsh1._winthres()
            return allFields

    class _imemrwptrsh2(AtRegister.AtRegister):
        def name(self):
            return "POH BER Threshold 2"
    
        def description(self):
            return "This register is used to configure threshold of BER level 4 to level 8."
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x06_0400 + $Rate*8 + $Thresloc"
            
        def startAddress(self):
            return 0x00060400
            
        def endAddress(self):
            return 0xffffffff

        class _scwthres1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 33
                
            def startBit(self):
                return 17
        
            def name(self):
                return "scwthres1"
            
            def description(self):
                return "Set/Clear/Window Threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _scwthres2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 0
        
            def name(self):
                return "scwthres2"
            
            def description(self):
                return "Set/Clear/Window Threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["scwthres1"] = _AF6CCI0021_RD_PDH._imemrwptrsh2._scwthres1()
            allFields["scwthres2"] = _AF6CCI0021_RD_PDH._imemrwptrsh2._scwthres2()
            return allFields

    class _imemrwpctrl1(AtRegister.AtRegister):
        def name(self):
            return "POH BER Control VT/DSN"
    
        def description(self):
            return "This register is used to enable and set threshold SD SF ."
            
        def width(self):
            return 64
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x06_2030 + $GID"
            
        def startAddress(self):
            return 0x00062030
            
        def endAddress(self):
            return 0xffffffff

        class _bermode3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 47
        
            def name(self):
                return "bermode3"
            
            def description(self):
                return "select bermode chanel 3: 1-line, 0-path"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _bermode2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 46
                
            def startBit(self):
                return 46
        
            def name(self):
                return "bermode2"
            
            def description(self):
                return "select bermode chanel 2: 1-line, 0-path"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _bermode1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 45
                
            def startBit(self):
                return 45
        
            def name(self):
                return "bermode1"
            
            def description(self):
                return "select bermode chanel 1: 1-line, 0-path"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _bermode0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 44
                
            def startBit(self):
                return 44
        
            def name(self):
                return "bermode0"
            
            def description(self):
                return "select bermode chanel 0: 1-line, 0-path"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _tca3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 43
                
            def startBit(self):
                return 41
        
            def name(self):
                return "tca3"
            
            def description(self):
                return "TCA threshold channel 3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _tca2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 40
                
            def startBit(self):
                return 38
        
            def name(self):
                return "tca2"
            
            def description(self):
                return "TCA threshold channel 2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _tca1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 37
                
            def startBit(self):
                return 35
        
            def name(self):
                return "tca1"
            
            def description(self):
                return "TCA threshold channel 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _tca0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 34
                
            def startBit(self):
                return 32
        
            def name(self):
                return "tca0"
            
            def description(self):
                return "TCA threshold channel 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _etype4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "etype4"
            
            def description(self):
                return "0:DS1 1:E1 channel 3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sftrsh4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 28
        
            def name(self):
                return "sftrsh4"
            
            def description(self):
                return "SF threshold raise channel 3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sdtrsh4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 25
        
            def name(self):
                return "sdtrsh4"
            
            def description(self):
                return "SD threshold raise channel 3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ena4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "ena4"
            
            def description(self):
                return "Enable channel 3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _etype3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "etype3"
            
            def description(self):
                return "0:DS1 1:E1 channel 2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sftrsh3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 20
        
            def name(self):
                return "sftrsh3"
            
            def description(self):
                return "SF threshold raise channel 2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sdtrsh3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 17
        
            def name(self):
                return "sdtrsh3"
            
            def description(self):
                return "SD threshold raise channel 2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ena3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ena3"
            
            def description(self):
                return "Enable channel 2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _etype2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "etype2"
            
            def description(self):
                return "0:DS1 1:E1 channel 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sftrsh2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 12
        
            def name(self):
                return "sftrsh2"
            
            def description(self):
                return "SF threshold raise channel 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sdtrsh2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 9
        
            def name(self):
                return "sdtrsh2"
            
            def description(self):
                return "SD threshold raise channel 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ena2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "ena2"
            
            def description(self):
                return "Enable channel 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _etype1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "etype1"
            
            def description(self):
                return "0:DS1 1:E1 channel 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sftrsh1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 4
        
            def name(self):
                return "sftrsh1"
            
            def description(self):
                return "SF threshold raise channel 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _sdtrsh1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 1
        
            def name(self):
                return "sdtrsh1"
            
            def description(self):
                return "SD threshold raise channel 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ena1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ena1"
            
            def description(self):
                return "Enable channel 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["bermode3"] = _AF6CCI0021_RD_PDH._imemrwpctrl1._bermode3()
            allFields["bermode2"] = _AF6CCI0021_RD_PDH._imemrwpctrl1._bermode2()
            allFields["bermode1"] = _AF6CCI0021_RD_PDH._imemrwpctrl1._bermode1()
            allFields["bermode0"] = _AF6CCI0021_RD_PDH._imemrwpctrl1._bermode0()
            allFields["tca3"] = _AF6CCI0021_RD_PDH._imemrwpctrl1._tca3()
            allFields["tca2"] = _AF6CCI0021_RD_PDH._imemrwpctrl1._tca2()
            allFields["tca1"] = _AF6CCI0021_RD_PDH._imemrwpctrl1._tca1()
            allFields["tca0"] = _AF6CCI0021_RD_PDH._imemrwpctrl1._tca0()
            allFields["etype4"] = _AF6CCI0021_RD_PDH._imemrwpctrl1._etype4()
            allFields["sftrsh4"] = _AF6CCI0021_RD_PDH._imemrwpctrl1._sftrsh4()
            allFields["sdtrsh4"] = _AF6CCI0021_RD_PDH._imemrwpctrl1._sdtrsh4()
            allFields["ena4"] = _AF6CCI0021_RD_PDH._imemrwpctrl1._ena4()
            allFields["etype3"] = _AF6CCI0021_RD_PDH._imemrwpctrl1._etype3()
            allFields["sftrsh3"] = _AF6CCI0021_RD_PDH._imemrwpctrl1._sftrsh3()
            allFields["sdtrsh3"] = _AF6CCI0021_RD_PDH._imemrwpctrl1._sdtrsh3()
            allFields["ena3"] = _AF6CCI0021_RD_PDH._imemrwpctrl1._ena3()
            allFields["etype2"] = _AF6CCI0021_RD_PDH._imemrwpctrl1._etype2()
            allFields["sftrsh2"] = _AF6CCI0021_RD_PDH._imemrwpctrl1._sftrsh2()
            allFields["sdtrsh2"] = _AF6CCI0021_RD_PDH._imemrwpctrl1._sdtrsh2()
            allFields["ena2"] = _AF6CCI0021_RD_PDH._imemrwpctrl1._ena2()
            allFields["etype1"] = _AF6CCI0021_RD_PDH._imemrwpctrl1._etype1()
            allFields["sftrsh1"] = _AF6CCI0021_RD_PDH._imemrwpctrl1._sftrsh1()
            allFields["sdtrsh1"] = _AF6CCI0021_RD_PDH._imemrwpctrl1._sdtrsh1()
            allFields["ena1"] = _AF6CCI0021_RD_PDH._imemrwpctrl1._ena1()
            return allFields

    class _ramberratevtds(AtRegister.AtRegister):
        def name(self):
            return "POH BER Report VT/DSN"
    
        def description(self):
            return "This register is used to get current BER rate ."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x06_80A8 +  $LID"
            
        def startAddress(self):
            return 0x000680a8
            
        def endAddress(self):
            return 0xffffffff

        class _hwsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "hwsta"
            
            def description(self):
                return "Hardware status"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _rate(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rate"
            
            def description(self):
                return "BER rate"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["hwsta"] = _AF6CCI0021_RD_PDH._ramberratevtds._hwsta()
            allFields["rate"] = _AF6CCI0021_RD_PDH._ramberratevtds._rate()
            return allFields

    class _imemrwptrsh1_ds1_bpv(AtRegister.AtRegister):
        def name(self):
            return "POH BER3 BPV DS1 Threshold 1"
    
        def description(self):
            return "This register is used to configure threshold of BER level 3."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0006248f
            
        def endAddress(self):
            return 0xffffffff

        class _setthres_ds1_bpv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 9
        
            def name(self):
                return "setthres_ds1_bpv"
            
            def description(self):
                return "SetThreshold_bpv"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _winthres_ds1_bpv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 0
        
            def name(self):
                return "winthres_ds1_bpv"
            
            def description(self):
                return "WindowThreshold_bpv"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["setthres_ds1_bpv"] = _AF6CCI0021_RD_PDH._imemrwptrsh1_ds1_bpv._setthres_ds1_bpv()
            allFields["winthres_ds1_bpv"] = _AF6CCI0021_RD_PDH._imemrwptrsh1_ds1_bpv._winthres_ds1_bpv()
            return allFields

    class _imemrwptrsh1_e1_bpv(AtRegister.AtRegister):
        def name(self):
            return "POH BER3 BPV E1 Threshold 1"
    
        def description(self):
            return "This register is used to configure threshold of BER level 3."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0006249f
            
        def endAddress(self):
            return 0xffffffff

        class _setthres_e1_bpv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 9
        
            def name(self):
                return "setthres_e1_bpv"
            
            def description(self):
                return "SetThreshold_bpv"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _winthres_e1_bpv(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 0
        
            def name(self):
                return "winthres_e1_bpv"
            
            def description(self):
                return "WindowThreshold_bpv"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["setthres_e1_bpv"] = _AF6CCI0021_RD_PDH._imemrwptrsh1_e1_bpv._setthres_e1_bpv()
            allFields["winthres_e1_bpv"] = _AF6CCI0021_RD_PDH._imemrwptrsh1_e1_bpv._winthres_e1_bpv()
            return allFields

    class _isel_alarm1(AtRegister.AtRegister):
        def name(self):
            return "Interface Alarm PDH"
    
        def description(self):
            return "This register is used to select alram from pdh."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000fffe
            
        def endAddress(self):
            return 0xffffffff

        class _sel_event_alarm1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "sel_event_alarm1"
            
            def description(self):
                return "bitmap select alrm chanel 0-31, 1-bpv 0-crc,"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["sel_event_alarm1"] = _AF6CCI0021_RD_PDH._isel_alarm1._sel_event_alarm1()
            return allFields

    class _isel_alarm2(AtRegister.AtRegister):
        def name(self):
            return "Interface Alarm PDH"
    
        def description(self):
            return "This register is used to select alarm from pdh"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000ffff
            
        def endAddress(self):
            return 0xffffffff

        class _sel_event_alarm2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "sel_event_alarm2"
            
            def description(self):
                return "bitmap select alrm chanel 32-48, 1-bpv 0-crc,"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["sel_event_alarm2"] = _AF6CCI0021_RD_PDH._isel_alarm2._sel_event_alarm2()
            return allFields

    class _dej1_errins_en_cfg(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Payload Error Insert Enable Configuration"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00090002
            
        def endAddress(self):
            return 0xffffffff

        class _TxDE1ErrInsID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE1ErrInsID"
            
            def description(self):
                return "Force ID channel"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1ErrInsID"] = _AF6CCI0021_RD_PDH._dej1_errins_en_cfg._TxDE1ErrInsID()
            return allFields

    class _dej1_errins_thr_cfg(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Payload Error Insert threhold Configuration"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00090003
            
        def endAddress(self):
            return 0xffffffff

        class _TxDE1errormode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 29
        
            def name(self):
                return "TxDE1errormode"
            
            def description(self):
                return "Force error mode: 3:bit (included Fbit); 2:payload (exclude fbit); 1: fbit only: 0: disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1errorsingle_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "TxDE1errorsingle_en"
            
            def description(self):
                return "0: One-shot with number of errors; 1:Line rate"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxDE1PayErrInsThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxDE1PayErrInsThr"
            
            def description(self):
                return "Error Threshold in bit unit or the number of error in one-shot mode. Valid value from 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxDE1errormode"] = _AF6CCI0021_RD_PDH._dej1_errins_thr_cfg._TxDE1errormode()
            allFields["TxDE1errorsingle_en"] = _AF6CCI0021_RD_PDH._dej1_errins_thr_cfg._TxDE1errorsingle_en()
            allFields["TxDE1PayErrInsThr"] = _AF6CCI0021_RD_PDH._dej1_errins_thr_cfg._TxDE1PayErrInsThr()
            return allFields

    class _dej1_rx_errins_en_cfg(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Rx Error Insert Enable Configuration"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00050017
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE1ErrInsID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1ErrInsID"
            
            def description(self):
                return "Force ID channel"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1ErrInsID"] = _AF6CCI0021_RD_PDH._dej1_rx_errins_en_cfg._RxDE1ErrInsID()
            return allFields

    class _dej1_rx_errins_thr_cfg(AtRegister.AtRegister):
        def name(self):
            return "DS1/E1/J1 Payload Error Insert threhold Configuration"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00050018
            
        def endAddress(self):
            return 0xffffffff

        class _RxDE1errormode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 29
        
            def name(self):
                return "RxDE1errormode"
            
            def description(self):
                return "Force error mode: 3:bit (included Fbit); 2:unused; 1: unused: 0: disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1errorsingle_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "RxDE1errorsingle_en"
            
            def description(self):
                return "0: One-shot with number of errors; 1:Line rate"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RxDE1PayErrInsThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RxDE1PayErrInsThr"
            
            def description(self):
                return "Error Threshold in bit unit or the number of error in one-shot mode. Valid value from 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RxDE1errormode"] = _AF6CCI0021_RD_PDH._dej1_rx_errins_thr_cfg._RxDE1errormode()
            allFields["RxDE1errorsingle_en"] = _AF6CCI0021_RD_PDH._dej1_rx_errins_thr_cfg._RxDE1errorsingle_en()
            allFields["RxDE1PayErrInsThr"] = _AF6CCI0021_RD_PDH._dej1_rx_errins_thr_cfg._RxDE1PayErrInsThr()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0021_RD_PWE(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["Pseudowire_Transmit_Ethernet_Header_Value_Control"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_Ethernet_Header_Value_Control()
        allRegisters["Pseudowire_Transmit_Ethernet_Header_Length_Control"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_Ethernet_Header_Length_Control()
        allRegisters["Pseudowire_Transmit_Ethernet_PAD_Control"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_Ethernet_PAD_Control()
        allRegisters["Pseudowire_Transmit_Header_RTP_SSRC_Value_Control"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_Header_RTP_SSRC_Value_Control()
        allRegisters["Pseudowire_Transmit_Enable_Control"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_Enable_Control()
        allRegisters["Pseudowire_Transmit_R_bit_Control"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_R_bit_Control()
        allRegisters["Pseudowire_Transmit_HSPW_Label_Control"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_HSPW_Label_Control()
        allRegisters["Pseudowire_Transmit_UPSR_and_HSPW_Control"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_UPSR_and_HSPW_Control()
        allRegisters["Pseudowire_Transmit_UPSR_Group_Control"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_UPSR_Group_Control()
        allRegisters["Pseudowire_Transmit_HSPW_Protection_Control"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_HSPW_Protection_Control()
        allRegisters["Pwe_DDR_CRC_Error_Counter_ro"] = _AF6CCI0021_RD_PWE._Pwe_DDR_CRC_Error_Counter_ro()
        allRegisters["Pwe_DDR_CRC_Error_Counter_r2c"] = _AF6CCI0021_RD_PWE._Pwe_DDR_CRC_Error_Counter_r2c()
        allRegisters["Pwe_DDR_errins_en_cfg"] = _AF6CCI0021_RD_PWE._Pwe_DDR_errins_en_cfg()
        return allRegisters

    class _Pseudowire_Transmit_Ethernet_Header_Value_Control(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Ethernet Header Value Control"
    
        def description(self):
            return "This register configures value of transmit Ethernet pseudowire header. Each pseudowire has 16 entries (1 entry contains 32 bits) for header value configuration. The header format from entry#0 to entry#15 is as follow: $DA(6-byte),VLAN1(4-byte,optional),VLAN2(4-byte, optional),EthType(2-byte),PSN Header(4 to 48 bytes)}  %% Depends on specific PSN selected for each pseudowire, the EthType and PSN Header field may have the following values and formats %% PSN header is MEF-8:   %% EthType:  0x88D8 %% 4-byte PSN Header with format: {ECID[19:0], 0x102} where ECID is pseodowire identification for 	remote 	receive side. %% PSN header is MPLS:   %% EthType:  0x8847 %% 4/8-byte PSN Header with format: {OuterLabel[31:0](optional), InnerLabel[31:0]}  where 	InnerLabel[31:12] is pseodowire identification for remote receive side. %% Label format: {Idenfifier[19:0],Exp[2:0],StackBit,TTL[7:0]} where StackBit = 1 for InnerLabel %% PSN header is UDP/Ipv4:   %% EthType:  0x0800 %% 28-byte PSN Header with format {Ipv4 Header(20 bytes), UDP Header(8 byte)} as below: %% {IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0], IP_Header_Sum[31:16]} %% {IP_Iden[15:0], IP_Flag[2:0], IP_Frag_Offset[12:0]}  %% {IP_TTL[7:0], IP_Protocol[7:0], IP_Header_Sum[15:0]}  %% {IP_SrcAdr[31:0]} %% {IP_DesAdr[31:0]} %% {UDP_SrcPort[15:0], UDP_DesPort[15:0]} %% {32-bit zeros} %% Case: %% IP_Protocol %%  0x11 to signify UDP  %% IP_Header_Sum %%    %% {IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0]} + %% IP_Iden[15:0] + {IP_Flag[2:0], IP_Frag_Offset[12:0]}  + %% {IP_TTL[7:0], IP_Protocol[7:0]} +  %% IP_SrcAdr[31:16] + IP_SrcAdr[15:0] + %% IP_DesAdr[31:16] + IP_DesAdr[15:0] %% UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused %% UDP_DesPort : used as remote pseudowire identification or 0x85E if unused %% PSN header is UDP/Ipv6:   %% EthType:  0x86DD %% 48-byte PSN Header with format {Ipv6 Header(40 bytes), UDP Header(8 byte)} as below: %% {IP_Ver[3:0], IP_Traffic_Class[7:0], IP_Flow_Label[19:0]} %% {16-bit zeros, IP_Next_Header[7:0], IP_Hop_Limit[7:0]}  %% {IP_SrcAdr[127:96]} %% {IP_SrcAdr[95:64]} %% {IP_SrcAdr[63:32]} %% {IP_SrcAdr[31:0]} %% {IP_DesAdr[127:96]} %% {IP_DesAdr[95:64]} %% {IP_DesAdr[63:32]} %% {IP_DesAdr[31:0]} %% {UDP_SrcPort[15:0], UDP_DesPort[15:0]} %% {UDP_Sum[31:0]} %% Case: %% IP_Next_Header %%  0x11 to signify UDP  %% UDP_Sum %%    %% IP_SrcAdr[127:112] + 	IP_SrcAdr[111:96] + 	 %% IP_SrcAdr[95:80]  + IP_SrcAdr[79:64] + 	 %% IP_SrcAdr[63:48]  + IP_SrcAdr[47:32] + 	 %% IP_SrcAdr[31:16]  + IP_SrcAdr[15:0] + 	 %% IP_DesAdr[127:112] + 	IP_DesAdr[111:96] + 	 %% IP_DesAdr[95:80]  + IP_DesAdr[79:64] + 	 %% IP_DesAdr[63:48]  + IP_DesAdr[47:32] + 	 %% IP_DesAdr[31:16]  + IP_DesAdr[15:0] + %% {8-bit zeros, IP_Next_Header[7:0]} + %% UDP_SrcPort[15:0] +  UDP_DesPort[15:0] %% UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused %% UDP_DesPort : used as remote pseudowire identification or 0x85E if unused %% User can select either source or destination port for pseodowire identification. See IP/UDP standards for more description about other field.  %% PSN header is MPLS over Ipv4: %% IPV4 header must have IP_Protocol[7:0] value 0x89 to signify MPLS %% After IPV4 header is MPLS labels where inner label is used for PW identification %% PSN header is MPLS over Ipv6: %% IPV6 header must have IP_Next_Header[7:0] value 0x89 to signify MPLS %% After IPV6 header is MPLS labels where inner label is used for PW identification %% Format in case of VLAN TAG mode contains PWID %% DA[47:0],                   destination address %% TYPE_CVLAN,                 0x8100, ethernet type indicate first VLAN TAG %% 4'b0,1'b0,5'b01010,6'b0,    0x0280, TAG indicate TDM PW, bit[11] = 0 indicate this packet is not OAM,  %% bit[10:6] = 5'b01010        indicate PW %% TYPE_CVLAN,	               0x8100, ethernet type indicate second VLAN TAG  %% 4'b0,PWID[9:0],2'b0         TAG indicate PWID"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x000000 + PwId*16 + Entry"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x00000fff

        class _TxEthPwHeadValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthPwHeadValue"
            
            def description(self):
                return "Transmit Ethernet Pseudowire Header value in each entry"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthPwHeadValue"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_Ethernet_Header_Value_Control._TxEthPwHeadValue()
            return allFields

    class _Pseudowire_Transmit_Ethernet_Header_Length_Control(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Ethernet Header Length Control"
    
        def description(self):
            return "This register configures length in number of bytes of AF6F1 transmit Ethernet header."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x001000 + PwId"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0x000010ff

        class _TxEthPwRtpPtValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 16
        
            def name(self):
                return "TxEthPwRtpPtValue"
            
            def description(self):
                return "Used for TDM PW, this is the PT value of RTP header, define in http://www.iana.org/assignments/rtp-parameters/rtp-parameters.xml"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxEthPwRtpEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "TxEthPwRtpEn"
            
            def description(self):
                return "Used for TDM PW 1: Enable RTP field in PSN header (used for TDM PW with DCR timing) 0: Disable RTP field in PSN header (used for ATM PW or TDM PW without DCR timing)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxEthPwPsnType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 12
        
            def name(self):
                return "TxEthPwPsnType"
            
            def description(self):
                return "1: PW PSN header is UDP/IPv4 2: PW PSN header is UDP/IPv6 3: PW PSN header is MPLS and EXP bits of inner label is replaced by ATM cell priority bits (used for ATM PW) 4: PW MPLS no outer label (total 1 MPLS label) 5: PW MPLS one outer label(total 2 MPLS label) 6: PW MPLS two outer label (total 3 MPLS label) 7: PW MPLS one outer label over Ipv6 (total 2 MPLS label) Others: for other PW PSN header type (include MPLS with configuration EXP bits) Others: for other PW PSN header type"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxEthPwCwType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "TxEthPwCwType"
            
            def description(self):
                return "0: Control word 4-byte (used for PDH/ATM N to one pseudowire) 1: Control word 3-byte (used for ATM one to one VCC/VPC pseudowire)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxEthPwNumVlan(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 9
        
            def name(self):
                return "TxEthPwNumVlan"
            
            def description(self):
                return "Number of VLANs in Transmit Ethernet Pseudowire Header."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxEthPwNumMplsOutLb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 7
        
            def name(self):
                return "TxEthPwNumMplsOutLb"
            
            def description(self):
                return "Number of MPLS outer labels in Transmit Ethernet Pseudowire Header. This field is only applicable when the pseudowire selects MPLS as its PSN header, otherwise, this field must be set to zero value."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxEthPwHeadLen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthPwHeadLen"
            
            def description(self):
                return "Length in number of bytes of Transmit Ethernet Pseudowire Header. This length is counted from start of Ethernet packet (DA) to end of pseudowire header (the next of pseudowire header is control word and TDM payload). Be noted that this length field includes SA field for counting too (although the below Pseudowire Transmit Ethernet Header Value Control does not contains SA field because AF6FHW0013 already has a global MacAddress for the SA field). In case of VLAN TAG mode contains PWID (DA,SA,VLAN1,VLAN2), header length value equal to 0x14"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthPwRtpPtValue"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_Ethernet_Header_Length_Control._TxEthPwRtpPtValue()
            allFields["TxEthPwRtpEn"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_Ethernet_Header_Length_Control._TxEthPwRtpEn()
            allFields["TxEthPwPsnType"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_Ethernet_Header_Length_Control._TxEthPwPsnType()
            allFields["TxEthPwCwType"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_Ethernet_Header_Length_Control._TxEthPwCwType()
            allFields["TxEthPwNumVlan"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_Ethernet_Header_Length_Control._TxEthPwNumVlan()
            allFields["TxEthPwNumMplsOutLb"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_Ethernet_Header_Length_Control._TxEthPwNumMplsOutLb()
            allFields["TxEthPwHeadLen"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_Ethernet_Header_Length_Control._TxEthPwHeadLen()
            return allFields

    class _Pseudowire_Transmit_Ethernet_PAD_Control(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Ethernet PAD Control"
    
        def description(self):
            return "This register configures mode to insert number of PAD bytes of AF6FH1 transmit Ethernet"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002000
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthPadMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthPadMode"
            
            def description(self):
                return "Modes to insert PAD for pseudowire packet at Ethernet transmit direction 0: Insert PAD when total Ethernet packet length is less than 64 bytes. Refer to IEEE 802.3 1: Insert PAD when control word plus payload length is less than 64 bytes 2: Insert PAD when total Ethernet packet length minus VLANs and MPLS outer labels(if exist) is less than 64 bytes. The reason is that networking devices often insert or remove VLANs and MPLS outer labels when packet traverse them."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthPadMode"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_Ethernet_PAD_Control._TxEthPadMode()
            return allFields

    class _Pseudowire_Transmit_Header_RTP_SSRC_Value_Control(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Header RTP SSRC Value Control"
    
        def description(self):
            return "Used for TDM PW. This register configures RTP SSRC value."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x003000 + PwId"
            
        def startAddress(self):
            return 0x00003000
            
        def endAddress(self):
            return 0x000030ff

        class _TxEthPwRtpSsrcValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthPwRtpSsrcValue"
            
            def description(self):
                return "Used for TDM PW, this is the SSRC value of RTP header, define in RFC3550"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthPwRtpSsrcValue"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_Header_RTP_SSRC_Value_Control._TxEthPwRtpSsrcValue()
            return allFields

    class _Pseudowire_Transmit_Enable_Control(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Enable Control"
    
        def description(self):
            return "This register configures pseudowire enable for transmit to Ethernet direction"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x021100 + PWID"
            
        def startAddress(self):
            return 0x00021100
            
        def endAddress(self):
            return 0xffffffff

        class _TxPwCwMbitDisable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "TxPwCwMbitDisable"
            
            def description(self):
                return "0: normal 1: Disable Mbit in control word"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxPwCwLbitDisable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "TxPwCwLbitDisable"
            
            def description(self):
                return "0: normal 1: Disable Lbit in control word"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxPwCwMbitCpu(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "TxPwCwMbitCpu"
            
            def description(self):
                return "low priority than TxPwMLbitDisable 0: normal 1: CPU force Mbit in control word, the value of Mbit would be 2'b10"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxPwCwLbitCpu(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "TxPwCwLbitCpu"
            
            def description(self):
                return "low priority than TxPwLbitDisable 0: normal 1: CPU force Lbit in control word"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxPwCwType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "TxPwCwType"
            
            def description(self):
                return "0: Control word 4-byte (used for PDH/ATM N to one pseudowire) 1: Control word 3-byte (used for ATM one to one VCC/VPC pseudowire)"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxPwEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPwEn"
            
            def description(self):
                return "1: Enable 0: Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPwCwMbitDisable"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_Enable_Control._TxPwCwMbitDisable()
            allFields["TxPwCwLbitDisable"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_Enable_Control._TxPwCwLbitDisable()
            allFields["TxPwCwMbitCpu"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_Enable_Control._TxPwCwMbitCpu()
            allFields["TxPwCwLbitCpu"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_Enable_Control._TxPwCwLbitCpu()
            allFields["TxPwCwType"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_Enable_Control._TxPwCwType()
            allFields["TxPwEn"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_Enable_Control._TxPwEn()
            return allFields

    class _Pseudowire_Transmit_R_bit_Control(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit R bit Control"
    
        def description(self):
            return "This register configures pseudowire L/M bit control  for transmit to Ethernet direction"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x025000+PWID"
            
        def startAddress(self):
            return 0x00025000
            
        def endAddress(self):
            return 0xffffffff

        class _TxPwRbitCpu(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "TxPwRbitCpu"
            
            def description(self):
                return "low priority than TxPwRbitDisable 0: Normal 1: CPU force Rbit at transmit PW direction"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxPwRbitDisable(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxPwRbitDisable"
            
            def description(self):
                return "1: Disable sending Rbit in PW control word 0: Normal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxPwRbitCpu"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_R_bit_Control._TxPwRbitCpu()
            allFields["TxPwRbitDisable"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_R_bit_Control._TxPwRbitDisable()
            return allFields

    class _Pseudowire_Transmit_HSPW_Label_Control(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit HSPW Label Control"
    
        def description(self):
            return "Used for TDM PW. This register configures \"label ID\" when HSPW working at protection mode value."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x005000 + PwId"
            
        def startAddress(self):
            return 0x00005000
            
        def endAddress(self):
            return 0x000050ff

        class _TxEthPwHspwLabelValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthPwHspwLabelValue"
            
            def description(self):
                return "Used for TDM PW, this is the HSPW label"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthPwHspwLabelValue"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_HSPW_Label_Control._TxEthPwHspwLabelValue()
            return allFields

    class _Pseudowire_Transmit_UPSR_and_HSPW_Control(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit UPSR and HSPW Control"
    
        def description(self):
            return "Used for TDM PW. This register configures UPSR and HSPW value."
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x006000 + PwId"
            
        def startAddress(self):
            return 0x00006000
            
        def endAddress(self):
            return 0x000060ff

        class _TxEthPwUpsrUseValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "TxEthPwUpsrUseValue"
            
            def description(self):
                return "Used for TDM PW, this is the UPSR using or not 0: PW not join any UPSR Group 1: PW join a UPSR Group"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxEthPwHspwUseValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "TxEthPwHspwUseValue"
            
            def description(self):
                return "Used for TDM PW, this is the HSPW using or not 0: PW not join any HSPW Group 1: PW join a HSPW Group"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxEthPwUpsrGrpValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 6
        
            def name(self):
                return "TxEthPwUpsrGrpValue"
            
            def description(self):
                return "Used for TDM PW, this is the UPSR group"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxEthPwHspwGrpValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthPwHspwGrpValue"
            
            def description(self):
                return "Used for TDM PW, this is the HSPW group"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthPwUpsrUseValue"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_UPSR_and_HSPW_Control._TxEthPwUpsrUseValue()
            allFields["TxEthPwHspwUseValue"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_UPSR_and_HSPW_Control._TxEthPwHspwUseValue()
            allFields["TxEthPwUpsrGrpValue"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_UPSR_and_HSPW_Control._TxEthPwUpsrGrpValue()
            allFields["TxEthPwHspwGrpValue"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_UPSR_and_HSPW_Control._TxEthPwHspwGrpValue()
            return allFields

    class _Pseudowire_Transmit_UPSR_Group_Control(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit UPSR Group Control"
    
        def description(self):
            return "Used for TDM PW. This register configures UPSR Group enable or not"
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x007000 + TxEthPwUpsrGrpValue"
            
        def startAddress(self):
            return 0x00007000
            
        def endAddress(self):
            return 0x000070ff

        class _TxEthPwUpsrEnValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthPwUpsrEnValue"
            
            def description(self):
                return "Used for TDM PW, this is the UPSR Group enable or not 0: UPSR Group disable 1: UPSR Group enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthPwUpsrEnValue"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_UPSR_Group_Control._TxEthPwUpsrEnValue()
            return allFields

    class _Pseudowire_Transmit_HSPW_Protection_Control(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit HSPW Protection Control"
    
        def description(self):
            return "Used for TDM PW. This register configures HSPW Group Protection enable"
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x008000 + TxEthPwHspwGrpValue"
            
        def startAddress(self):
            return 0x00008000
            
        def endAddress(self):
            return 0x0000803f

        class _TxEthPwHspwEnValue(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthPwHspwEnValue"
            
            def description(self):
                return "Used for TDM PW, this is the HSPW enable 0: HSPW Group using normal label 1: HSPW Group using \"HSPW Label\" as lable for packet transmit"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthPwHspwEnValue"] = _AF6CCI0021_RD_PWE._Pseudowire_Transmit_HSPW_Protection_Control._TxEthPwHspwEnValue()
            return allFields

    class _Pwe_DDR_CRC_Error_Counter_ro(AtRegister.AtRegister):
        def name(self):
            return "PWE DDR CRC Error Counter"
    
        def description(self):
            return "Count number of CRC error detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00020009
            
        def endAddress(self):
            return 0xffffffff

        class _PweDDRCRCError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PweDDRCRCError"
            
            def description(self):
                return "This counter count the number of DDR CRC error."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PweDDRCRCError"] = _AF6CCI0021_RD_PWE._Pwe_DDR_CRC_Error_Counter_ro._PweDDRCRCError()
            return allFields

    class _Pwe_DDR_CRC_Error_Counter_r2c(AtRegister.AtRegister):
        def name(self):
            return "PWE DDR CRC Error Counter"
    
        def description(self):
            return "Count number of CRC error detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00020008
            
        def endAddress(self):
            return 0xffffffff

        class _PweDDRCRCError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PweDDRCRCError"
            
            def description(self):
                return "This counter count the number of DDR CRC error."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PweDDRCRCError"] = _AF6CCI0021_RD_PWE._Pwe_DDR_CRC_Error_Counter_r2c._PweDDRCRCError()
            return allFields

    class _Pwe_DDR_errins_en_cfg(AtRegister.AtRegister):
        def name(self):
            return "PWE DDR Force Error Control"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00021201
            
        def endAddress(self):
            return 0xffffffff

        class _PWEDDRErrRate(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "PWEDDRErrRate"
            
            def description(self):
                return "0: One-shot with number of errors; 1:Line rate"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PWEDDRErrThr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PWEDDRErrThr"
            
            def description(self):
                return "Error Threshold in bit unit or the number of error in one-shot mode. Valid value from 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PWEDDRErrRate"] = _AF6CCI0021_RD_PWE._Pwe_DDR_errins_en_cfg._PWEDDRErrRate()
            allFields["PWEDDRErrThr"] = _AF6CCI0021_RD_PWE._Pwe_DDR_errins_en_cfg._PWEDDRErrThr()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0021_RD_PDH_MDLPRM(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_store_txinfo"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_store_txinfo()
        allRegisters["upen_store_rx2pm"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_store_rx2pm()
        allRegisters["upen_store_txmess"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_store_txmess()
        allRegisters["upen_store_rxmess"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_store_rxmess()
        allRegisters["upen_cfg_ver"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_cfg_ver()
        allRegisters["upen_cfg_debug"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_cfg_debug()
        allRegisters["upen_cfgid_mon"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_cfgid_mon()
        allRegisters["upen_prm_txcfgcr"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_prm_txcfgcr()
        allRegisters["upen_prm_txcfglb"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_prm_txcfglb()
        allRegisters["upen_txcfg_ctrl0"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txcfg_ctrl0()
        allRegisters["upen_txcfg_ctrl1"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txcfg_ctrl1()
        allRegisters["upen_txprm_cnt_byte"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txprm_cnt_byte()
        allRegisters["upen_txprm_cnt_pkt"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txprm_cnt_pkt()
        allRegisters["upen_txprm_cnt_info"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txprm_cnt_info()
        allRegisters["upen_txcfg_par"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txcfg_par()
        allRegisters["upen_txprestuff"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txprestuff()
        allRegisters["upen_rxprm_cfgstd"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxprm_cfgstd()
        allRegisters["upen_rxdestuff_ctrl0"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxdestuff_ctrl0()
        allRegisters["upen_rxdestuff_ctrl1"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxdestuff_ctrl1()
        allRegisters["upen_rxgmess_stk1"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxgmess_stk1()
        allRegisters["upen_rxgmess_stk2"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxgmess_stk2()
        allRegisters["upen_rxdrmess_stk1"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxdrmess_stk1()
        allRegisters["upen_rxdrmess_stk2"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxdrmess_stk2()
        allRegisters["upen_rxmmess_stk1"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxmmess_stk1()
        allRegisters["upen_rxmmess_stk2"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxmmess_stk2()
        allRegisters["upen_rxprm_gmess"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxprm_gmess()
        allRegisters["upen_rxprm_drmess"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxprm_drmess()
        allRegisters["upen_rxprm_mmess"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxprm_mmess()
        allRegisters["upen_rxprm_cnt_byte"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxprm_cnt_byte()
        allRegisters["upen_rxcfg_par"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxcfg_par()
        allRegisters["upen_rxpoststuff"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxpoststuff()
        allRegisters["prm_cfg_lben_int"] = _AF6CCI0021_RD_PDH_MDLPRM._prm_cfg_lben_int()
        allRegisters["prm_lb_int_sta"] = _AF6CCI0021_RD_PDH_MDLPRM._prm_lb_int_sta()
        allRegisters["prm_lb_int_crrsta"] = _AF6CCI0021_RD_PDH_MDLPRM._prm_lb_int_crrsta()
        allRegisters["prm_lb_intsta"] = _AF6CCI0021_RD_PDH_MDLPRM._prm_lb_intsta()
        allRegisters["prm_lb_sta_int"] = _AF6CCI0021_RD_PDH_MDLPRM._prm_lb_sta_int()
        allRegisters["prm_lb_en_int"] = _AF6CCI0021_RD_PDH_MDLPRM._prm_lb_en_int()
        return allRegisters

    class _upen_store_txinfo(AtRegister.AtRegister):
        def name(self):
            return "BUFFER STORE TX INFO FROM PDH FOR DEBUG BOARD"
    
        def description(self):
            return "STORE TX INFO FROM PDH"
            
        def width(self):
            return 10
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00002000
            
        def endAddress(self):
            return 0x000027ff

        class _dta_txinfo_pdh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dta_txinfo_pdh"
            
            def description(self):
                return "info from PDH"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dta_txinfo_pdh"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_store_txinfo._dta_txinfo_pdh()
            return allFields

    class _upen_store_rx2pm(AtRegister.AtRegister):
        def name(self):
            return "BUFFER STORE RX INFO TO PM FOR DEBUG BOARD"
    
        def description(self):
            return "STORE RX INFO TO PM"
            
        def width(self):
            return 9
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00003000
            
        def endAddress(self):
            return 0x000037ff

        class _dta_rxinfo_pm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dta_rxinfo_pm"
            
            def description(self):
                return "info to PM  (G1,G2,G3,G4,G5,G6,SE,LV,SL)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dta_rxinfo_pm"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_store_rx2pm._dta_rxinfo_pm()
            return allFields

    class _upen_store_txmess(AtRegister.AtRegister):
        def name(self):
            return "BUFFER BYTE TX MESSAGE AFTER STUFF FOR DEBUG BOARD"
    
        def description(self):
            return "Buffer byte Tx After stuff"
            
        def width(self):
            return 8
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004000
            
        def endAddress(self):
            return 0x000047ff

        class _dta_txmess_aft(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dta_txmess_aft"
            
            def description(self):
                return "byte data mesage before stuff"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dta_txmess_aft"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_store_txmess._dta_txmess_aft()
            return allFields

    class _upen_store_rxmess(AtRegister.AtRegister):
        def name(self):
            return "BUFFER BYTE RX MESSAGE BEFORE DESTUFF FOR DEBUG BOARD"
    
        def description(self):
            return "Buffer byte Rx before destuff PRM"
            
        def width(self):
            return 8
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00005000
            
        def endAddress(self):
            return 0x000057ff

        class _dta_rxmess_bef(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dta_rxmess_bef"
            
            def description(self):
                return "byte data mesage before destuff"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dta_rxmess_bef"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_store_rxmess._dta_rxmess_bef()
            return allFields

    class _upen_cfg_ver(AtRegister.AtRegister):
        def name(self):
            return "Version"
    
        def description(self):
            return "config ID to monitor tx message"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00006000
            
        def endAddress(self):
            return 0xffffffff

        class _version(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "version"
            
            def description(self):
                return "32'hDDAA_3110"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["version"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_cfg_ver._version()
            return allFields

    class _upen_cfg_debug(AtRegister.AtRegister):
        def name(self):
            return "CONFIG ENABLE MONITOR FOR DEBUG BOARD"
    
        def description(self):
            return "config enable monitor data before stuff"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00006001
            
        def endAddress(self):
            return 0xffffffff

        class _out_cfg_bar(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "out_cfg_bar"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _out_info_force(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 6
        
            def name(self):
                return "out_info_force"
            
            def description(self):
                return "10 bits info force [15:13] : 3'd1 : to set bit G1 3'd2 :  to set bit G2 3'd3 :  to set bit G3 3'd4 : to set bit G4 3'd5 : to set bit G5 3'd6 :  to set bit G6 [12] : to set bit SE [11] : to set bit FE [10] : to set bit LV [9] : to set bit SL [8] : to set bit LB [7:6] to set 2bit NmNi (00-01-10-11)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 3
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _out_cfg_selinfo2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "out_cfg_selinfo2"
            
            def description(self):
                return "select all 10 bit INFO from PRM FORCE , (1) is enable, (0) is disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _out_cfg_selinfo1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "out_cfg_selinfo1"
            
            def description(self):
                return "select just 8 bit INFO from PRM FORCE but except 2 bit NmNi, (1) is enable, (0) is disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _out_cfg_dis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "out_cfg_dis"
            
            def description(self):
                return "disable monitor, reset address buffer monitor, (1) is disable, (0) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["out_cfg_bar"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_cfg_debug._out_cfg_bar()
            allFields["out_info_force"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_cfg_debug._out_info_force()
            allFields["reserve"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_cfg_debug._reserve()
            allFields["out_cfg_selinfo2"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_cfg_debug._out_cfg_selinfo2()
            allFields["out_cfg_selinfo1"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_cfg_debug._out_cfg_selinfo1()
            allFields["out_cfg_dis"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_cfg_debug._out_cfg_dis()
            return allFields

    class _upen_cfgid_mon(AtRegister.AtRegister):
        def name(self):
            return "CONFIG ID TO MONITOR FOR DEBUG BOARD"
    
        def description(self):
            return "config ID to monitor tx message"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00006002
            
        def endAddress(self):
            return 0xffffffff

        class _out_cfgid_mon(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "out_cfgid_mon"
            
            def description(self):
                return "ID which is configured to monitor"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["out_cfgid_mon"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_cfgid_mon._out_cfgid_mon()
            return allFields

    class _upen_prm_txcfgcr(AtRegister.AtRegister):
        def name(self):
            return "CONFIG PRM BIT C/R & STANDARD TX"
    
        def description(self):
            return "Config PRM bit C/R & Standard"
            
        def width(self):
            return 4
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0000+$PRMID"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x0000002f

        class _cfg_prmen_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "cfg_prmen_tx"
            
            def description(self):
                return "config enable Tx, (0) is disable (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_prm_txdisstuff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfg_prm_txdisstuff"
            
            def description(self):
                return "config enable stuff message, (1) is disable, (0) is enbale"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_prmstd_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_prmstd_tx"
            
            def description(self):
                return "config standard Tx, (0) is ANSI, (1) is AT&T"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_prm_cr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_prm_cr"
            
            def description(self):
                return "config bit command/respond PRM message"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_prmen_tx"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_prm_txcfgcr._cfg_prmen_tx()
            allFields["cfg_prm_txdisstuff"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_prm_txcfgcr._cfg_prm_txdisstuff()
            allFields["cfg_prmstd_tx"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_prm_txcfgcr._cfg_prmstd_tx()
            allFields["cfg_prm_cr"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_prm_txcfgcr._cfg_prm_cr()
            return allFields

    class _upen_prm_txcfglb(AtRegister.AtRegister):
        def name(self):
            return "CONFIG PRM BIT LB & STANDARD Tx"
    
        def description(self):
            return "Config PRM bit LB & Standard"
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0300+$PRMID"
            
        def startAddress(self):
            return 0x00000300
            
        def endAddress(self):
            return 0x-000032f

        class _cfg_prm_enlb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_prm_enlb"
            
            def description(self):
                return "config enable CPU config LB bit, (0) is disable, (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_prm_lb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_prm_lb"
            
            def description(self):
                return "config bit Loopback PRM message"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_prm_enlb"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_prm_txcfglb._cfg_prm_enlb()
            allFields["cfg_prm_lb"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_prm_txcfglb._cfg_prm_lb()
            return allFields

    class _upen_txcfg_ctrl0(AtRegister.AtRegister):
        def name(self):
            return "CONFIG CONTROL TX STUFF 0"
    
        def description(self):
            return "config control Stuff global"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000000c0
            
        def endAddress(self):
            return 0xffffffff

        class _out_txcfg_ctrl0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 8
        
            def name(self):
                return "out_txcfg_ctrl0"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fcs_err_ins(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "fcs_err_ins"
            
            def description(self):
                return "(0): disable,(1): enable insert error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idlemod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "idlemod"
            
            def description(self):
                return "(0): transmit flag,(1): transmit ABORT: 1 pattern"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _sapimod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "sapimod"
            
            def description(self):
                return "mode byte sapi, reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ctraddinscfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "ctraddinscfg"
            
            def description(self):
                return "byte ctradd insert, insert header 3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _sapinscfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "sapinscfg"
            
            def description(self):
                return "byte sapi insert, insert header 1,2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fcsmod32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "fcsmod32"
            
            def description(self):
                return "select FCS 32 or 16,  (0) : 16, (1) : 32"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fcsinscfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "fcsinscfg"
            
            def description(self):
                return "(1) enable insert FCS, (0) disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _flagmod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "flagmod"
            
            def description(self):
                return "(0): 2 flag; 1 start flag and 1 end flag, (1): 1 flag: shared flag between two package"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["out_txcfg_ctrl0"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txcfg_ctrl0._out_txcfg_ctrl0()
            allFields["fcs_err_ins"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txcfg_ctrl0._fcs_err_ins()
            allFields["idlemod"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txcfg_ctrl0._idlemod()
            allFields["sapimod"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txcfg_ctrl0._sapimod()
            allFields["ctraddinscfg"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txcfg_ctrl0._ctraddinscfg()
            allFields["sapinscfg"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txcfg_ctrl0._sapinscfg()
            allFields["fcsmod32"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txcfg_ctrl0._fcsmod32()
            allFields["fcsinscfg"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txcfg_ctrl0._fcsinscfg()
            allFields["flagmod"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txcfg_ctrl0._flagmod()
            return allFields

    class _upen_txcfg_ctrl1(AtRegister.AtRegister):
        def name(self):
            return "CONFIG CONTROL TX STUFF 1"
    
        def description(self):
            return "config control Stuff global, config header"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000000c1
            
        def endAddress(self):
            return 0xffffffff

        class _sapibyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "sapibyte"
            
            def description(self):
                return "first sapi byte, header 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _teibyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "teibyte"
            
            def description(self):
                return "tei byte, header 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ctrbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "ctrbyte"
            
            def description(self):
                return "control byte, header 3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["sapibyte"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txcfg_ctrl1._sapibyte()
            allFields["teibyte"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txcfg_ctrl1._teibyte()
            allFields["ctrbyte"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txcfg_ctrl1._ctrbyte()
            allFields["reserve"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txcfg_ctrl1._reserve()
            return allFields

    class _upen_txprm_cnt_byte(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE TX PRM"
    
        def description(self):
            return "counter byte message PRM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0100 +$UPRO * 64 + $PRMID"
            
        def startAddress(self):
            return 0x00000100
            
        def endAddress(self):
            return 0x0000016f

        class _cnt_byte_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_byte_prm"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_byte_prm"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txprm_cnt_byte._cnt_byte_prm()
            return allFields

    class _upen_txprm_cnt_pkt(AtRegister.AtRegister):
        def name(self):
            return "COUNTER PACKET MESSAGE TX PRM"
    
        def description(self):
            return "counter packet message PRM"
            
        def width(self):
            return 15
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0180 +$UPRO * 64 + $PRMID"
            
        def startAddress(self):
            return 0x00000180
            
        def endAddress(self):
            return 0x000001ef

        class _cnt_pkt_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_pkt_prm"
            
            def description(self):
                return "value counter packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_pkt_prm"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txprm_cnt_pkt._cnt_pkt_prm()
            return allFields

    class _upen_txprm_cnt_info(AtRegister.AtRegister):
        def name(self):
            return "COUNTER VALID INFO TX PRM"
    
        def description(self):
            return "counter valid info PRM"
            
        def width(self):
            return 15
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0200 +$UPRO * 64 + $PRMID"
            
        def startAddress(self):
            return 0x00000200
            
        def endAddress(self):
            return 0x0000026f

        class _cnt_vld_prm_info(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_vld_prm_info"
            
            def description(self):
                return "value counter valid info"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_vld_prm_info"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txprm_cnt_info._cnt_vld_prm_info()
            return allFields

    class _upen_txcfg_par(AtRegister.AtRegister):
        def name(self):
            return "CONFIG ENABLE MONITOR TX MESSAGE FOR DEBUG BOARD"
    
        def description(self):
            return "config enable monitor data before stuff"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000000c2
            
        def endAddress(self):
            return 0xffffffff

        class _out_cfg_bar(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 5
        
            def name(self):
                return "out_cfg_bar"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _out_cfg_swap(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "out_cfg_swap"
            
            def description(self):
                return "(1) enable swap, (0) disable swap, swap LSB-->MSB data before stuffing"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _out_cfg_bar1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "out_cfg_bar1"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["out_cfg_bar"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txcfg_par._out_cfg_bar()
            allFields["out_cfg_swap"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txcfg_par._out_cfg_swap()
            allFields["out_cfg_bar1"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txcfg_par._out_cfg_bar1()
            return allFields

    class _upen_txprestuff(AtRegister.AtRegister):
        def name(self):
            return "BUFFER BYTE TX MESSAGE BEFORE STUFF FOR DEBUG BOARD"
    
        def description(self):
            return "Buffer byte Tx before stuff"
            
        def width(self):
            return 8
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000800
            
        def endAddress(self):
            return 0x00000fff

        class _dta_txmess_mon(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dta_txmess_mon"
            
            def description(self):
                return "byte data mesage before stuff"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dta_txmess_mon"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_txprestuff._dta_txmess_mon()
            return allFields

    class _upen_rxprm_cfgstd(AtRegister.AtRegister):
        def name(self):
            return "Config Buff Message PRM RX STANDARD"
    
        def description(self):
            return "config standard message PRM"
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1000+$PRMID"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0x0000102f

        class _cfg_prm_cr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfg_prm_cr"
            
            def description(self):
                return "config C/R bit expected"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_prm_rxdisstuff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_prm_rxdisstuff"
            
            def description(self):
                return "enable destuff   (1) is disable, (0) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_std_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_std_prm"
            
            def description(self):
                return "config standard   (0) is ANSI, (1) is AT&T"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_prm_cr"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxprm_cfgstd._cfg_prm_cr()
            allFields["cfg_prm_rxdisstuff"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxprm_cfgstd._cfg_prm_rxdisstuff()
            allFields["cfg_std_prm"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxprm_cfgstd._cfg_std_prm()
            return allFields

    class _upen_rxdestuff_ctrl0(AtRegister.AtRegister):
        def name(self):
            return "CONFIG CONTROL RX DESTUFF 0"
    
        def description(self):
            return "config control DeStuff global"
            
        def width(self):
            return 5
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000010c0
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_crmon(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "cfg_crmon"
            
            def description(self):
                return "(0) : disable, (1) : enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fcsmod32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "fcsmod32"
            
            def description(self):
                return "select FCS 32 or 16, (0) : 16, (1) : 32"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fcsmon(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "fcsmon"
            
            def description(self):
                return "(0) : disable monitor, (1) : enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _sapimod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "sapimod"
            
            def description(self):
                return "mode byte sapi"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _headermon_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "headermon_en"
            
            def description(self):
                return "(1) : enable monitor, (0) disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_crmon"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxdestuff_ctrl0._cfg_crmon()
            allFields["fcsmod32"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxdestuff_ctrl0._fcsmod32()
            allFields["fcsmon"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxdestuff_ctrl0._fcsmon()
            allFields["sapimod"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxdestuff_ctrl0._sapimod()
            allFields["reserve"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxdestuff_ctrl0._reserve()
            allFields["headermon_en"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxdestuff_ctrl0._headermon_en()
            return allFields

    class _upen_rxdestuff_ctrl1(AtRegister.AtRegister):
        def name(self):
            return "CONFIG CONTROL RX DESTUFF 1"
    
        def description(self):
            return "config control DeStuff global"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000010c1
            
        def endAddress(self):
            return 0xffffffff

        class _sapibyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "sapibyte"
            
            def description(self):
                return "first sapi byte, header 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _teibyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "teibyte"
            
            def description(self):
                return "tei byte, header 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ctrbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "ctrbyte"
            
            def description(self):
                return "control byte, header 3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["sapibyte"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxdestuff_ctrl1._sapibyte()
            allFields["teibyte"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxdestuff_ctrl1._teibyte()
            allFields["ctrbyte"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxdestuff_ctrl1._ctrbyte()
            allFields["reserve"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxdestuff_ctrl1._reserve()
            return allFields

    class _upen_rxgmess_stk1(AtRegister.AtRegister):
        def name(self):
            return "STICKY RECEIVE GOOD MESSAGE RX PRM1"
    
        def description(self):
            return "sticky good message from ID 0-31"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000010c4
            
        def endAddress(self):
            return 0xffffffff

        class _prm_gmess_stk1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_gmess_stk1"
            
            def description(self):
                return "sticky good message, bit '0' is ID '0'"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_gmess_stk1"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxgmess_stk1._prm_gmess_stk1()
            return allFields

    class _upen_rxgmess_stk2(AtRegister.AtRegister):
        def name(self):
            return "STICKY RECEIVE GOOD MESSAGE RX PRM2"
    
        def description(self):
            return "sticky good message from ID 32-47"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000010c5
            
        def endAddress(self):
            return 0xffffffff

        class _prm_gmess_stk2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_gmess_stk2"
            
            def description(self):
                return "sticky good message, bit '0' is ID '32'"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_gmess_stk2"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxgmess_stk2._prm_gmess_stk2()
            return allFields

    class _upen_rxdrmess_stk1(AtRegister.AtRegister):
        def name(self):
            return "STICKY RECEIVE DROP MESSAGE RX PRM1"
    
        def description(self):
            return "sticky drop message from ID 0-31"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000010c6
            
        def endAddress(self):
            return 0xffffffff

        class _prm_drmess_stk1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_drmess_stk1"
            
            def description(self):
                return "sticky drop message, bit '0' is ID '0'"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_drmess_stk1"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxdrmess_stk1._prm_drmess_stk1()
            return allFields

    class _upen_rxdrmess_stk2(AtRegister.AtRegister):
        def name(self):
            return "STICKY RECEIVE DROP MESSAGE RX PRM2"
    
        def description(self):
            return "sticky good message from ID 32-47"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000010c7
            
        def endAddress(self):
            return 0xffffffff

        class _prm_drmess_stk2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_drmess_stk2"
            
            def description(self):
                return "sticky drop message, bit '0' is ID '32'"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_drmess_stk2"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxdrmess_stk2._prm_drmess_stk2()
            return allFields

    class _upen_rxmmess_stk1(AtRegister.AtRegister):
        def name(self):
            return "STICKY RECEIVE MISS MESSAGE RX PRM1"
    
        def description(self):
            return "sticky miss message from ID 0-31"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000010c8
            
        def endAddress(self):
            return 0xffffffff

        class _prm_mmess_stk1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_mmess_stk1"
            
            def description(self):
                return "sticky miss message, bit '0' is ID '0'"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_mmess_stk1"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxmmess_stk1._prm_mmess_stk1()
            return allFields

    class _upen_rxmmess_stk2(AtRegister.AtRegister):
        def name(self):
            return "STICKY RECEIVE MISS MESSAGE RX PRM2"
    
        def description(self):
            return "sticky miss message from ID 32-47"
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000010c9
            
        def endAddress(self):
            return 0xffffffff

        class _prm_mmess_stk2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_mmess_stk2"
            
            def description(self):
                return "sticky miss message, bit '0' is ID '32'"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_mmess_stk2"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxmmess_stk2._prm_mmess_stk2()
            return allFields

    class _upen_rxprm_gmess(AtRegister.AtRegister):
        def name(self):
            return "COUNTER GOOD MESSAGE RX PRM"
    
        def description(self):
            return "counter good message PRM"
            
        def width(self):
            return 15
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1100 +$UPRO * 64 + $PRMID"
            
        def startAddress(self):
            return 0x00001100
            
        def endAddress(self):
            return 0x0000116f

        class _cnt_gmess_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_gmess_prm"
            
            def description(self):
                return "value counter packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_gmess_prm"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxprm_gmess._cnt_gmess_prm()
            return allFields

    class _upen_rxprm_drmess(AtRegister.AtRegister):
        def name(self):
            return "COUNTER DROP MESSAGE RX PRM"
    
        def description(self):
            return "counter drop message PRM"
            
        def width(self):
            return 15
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1180 +$UPRO * 64 + $PRMID"
            
        def startAddress(self):
            return 0x00001180
            
        def endAddress(self):
            return 0x000011ef

        class _cnt_drmess_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_drmess_prm"
            
            def description(self):
                return "value counter packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_drmess_prm"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxprm_drmess._cnt_drmess_prm()
            return allFields

    class _upen_rxprm_mmess(AtRegister.AtRegister):
        def name(self):
            return "COUNTER MISS MESSAGE RX PRM"
    
        def description(self):
            return "counter miss message PRM"
            
        def width(self):
            return 15
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1200 +$UPRO * 64 + $PRMID"
            
        def startAddress(self):
            return 0x00001200
            
        def endAddress(self):
            return 0x0000126f

        class _cnt_mmess_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_mmess_prm"
            
            def description(self):
                return "value counter packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_mmess_prm"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxprm_mmess._cnt_mmess_prm()
            return allFields

    class _upen_rxprm_cnt_byte(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE RX PRM"
    
        def description(self):
            return "counter byte message PRM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1280 +$UPRO * 64 + $PRMID"
            
        def startAddress(self):
            return 0x00001280
            
        def endAddress(self):
            return 0x000012ef

        class _cnt_byte_rxprm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_byte_rxprm"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_byte_rxprm"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxprm_cnt_byte._cnt_byte_rxprm()
            return allFields

    class _upen_rxcfg_par(AtRegister.AtRegister):
        def name(self):
            return "CONFIG RX PRM SELECT INFO TO PM"
    
        def description(self):
            return "config select INFO T or T-3 in message PRM to send PM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000010c2
            
        def endAddress(self):
            return 0xffffffff

        class _out_rxcfg_bar(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 1
        
            def name(self):
                return "out_rxcfg_bar"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _out_rxcfg_sel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "out_rxcfg_sel"
            
            def description(self):
                return "select info T or T-3 to send PM"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["out_rxcfg_bar"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxcfg_par._out_rxcfg_bar()
            allFields["out_rxcfg_sel"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxcfg_par._out_rxcfg_sel()
            return allFields

    class _upen_rxpoststuff(AtRegister.AtRegister):
        def name(self):
            return "BUFFER BYTE RX MESSAGE AFTER DESTUFF FOR DEBUG BOARD"
    
        def description(self):
            return "Buffer byte Rx after poststuff PRM"
            
        def width(self):
            return 8
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00001800
            
        def endAddress(self):
            return 0x00001fff

        class _dta_rxmess_mon(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dta_rxmess_mon"
            
            def description(self):
                return "byte data mesage after destuff"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dta_rxmess_mon"] = _AF6CCI0021_RD_PDH_MDLPRM._upen_rxpoststuff._dta_rxmess_mon()
            return allFields

    class _prm_cfg_lben_int(AtRegister.AtRegister):
        def name(self):
            return "PRM LB per Channel Interrupt Enable Control"
    
        def description(self):
            return "This is the per Channel interrupt enable"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1400 +  $PRMID"
            
        def startAddress(self):
            return 0x00001400
            
        def endAddress(self):
            return 0x0000142f

        class _prm_cfglben_int(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_cfglben_int"
            
            def description(self):
                return "Set 1 to enable change event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_cfglben_int"] = _AF6CCI0021_RD_PDH_MDLPRM._prm_cfg_lben_int._prm_cfglben_int()
            return allFields

    class _prm_lb_int_sta(AtRegister.AtRegister):
        def name(self):
            return "PRM LB Interrupt per Channel Interrupt Status"
    
        def description(self):
            return "This is the per Channel interrupt status."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1440 +  $PRMID"
            
        def startAddress(self):
            return 0x00001440
            
        def endAddress(self):
            return 0x0000146f

        class _prm_lbint_sta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_lbint_sta"
            
            def description(self):
                return "Set 1 if there is a change event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_lbint_sta"] = _AF6CCI0021_RD_PDH_MDLPRM._prm_lb_int_sta._prm_lbint_sta()
            return allFields

    class _prm_lb_int_crrsta(AtRegister.AtRegister):
        def name(self):
            return "PRM LB Interrupt per Channel Current Status"
    
        def description(self):
            return "This is the per Channel Current status."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x1480 +  $PRMID"
            
        def startAddress(self):
            return 0x00001480
            
        def endAddress(self):
            return 0x000014af

        class _prm_lbint_crrsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_lbint_crrsta"
            
            def description(self):
                return "Current status of event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_lbint_crrsta"] = _AF6CCI0021_RD_PDH_MDLPRM._prm_lb_int_crrsta._prm_lbint_crrsta()
            return allFields

    class _prm_lb_intsta(AtRegister.AtRegister):
        def name(self):
            return "PRM LB Interrupt per Channel Interrupt OR Status"
    
        def description(self):
            return "The register consists of 32 bits for 32 VT/TUs of the related STS/VC in the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR tus of the related DS1/E1/J1."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x14C0 +  $GID"
            
        def startAddress(self):
            return 0x000014c0
            
        def endAddress(self):
            return 0x000014c1

        class _prm_lbintsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_lbintsta"
            
            def description(self):
                return "Set to 1 if any interrupt status bit of corresponding channel is set and its interrupt is enabled."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_lbintsta"] = _AF6CCI0021_RD_PDH_MDLPRM._prm_lb_intsta._prm_lbintsta()
            return allFields

    class _prm_lb_sta_int(AtRegister.AtRegister):
        def name(self):
            return "PRM LB Interrupt OR Status"
    
        def description(self):
            return "The register consists of 2 bits. Each bit is used to store Interrupt OR status."
            
        def width(self):
            return 2
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000014ff
            
        def endAddress(self):
            return 0xffffffff

        class _prm_lbsta_int(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_lbsta_int"
            
            def description(self):
                return "Set to 1 if any interrupt status bit is set and its interrupt is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_lbsta_int"] = _AF6CCI0021_RD_PDH_MDLPRM._prm_lb_sta_int._prm_lbsta_int()
            return allFields

    class _prm_lb_en_int(AtRegister.AtRegister):
        def name(self):
            return "PRM LB Interrupt Enable Control"
    
        def description(self):
            return "The register consists of 2 interrupt enable bits ."
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000014fe
            
        def endAddress(self):
            return 0xffffffff

        class _prm_lben_int(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_lben_int"
            
            def description(self):
                return "Set to 1 to enable to generate interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_lben_int"] = _AF6CCI0021_RD_PDH_MDLPRM._prm_lb_en_int._prm_lben_int()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0021_RD_PDH_LPC(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_test1"] = _AF6CCI0021_RD_PDH_LPC._upen_test1()
        allRegisters["upen_test2"] = _AF6CCI0021_RD_PDH_LPC._upen_test2()
        allRegisters["upen_lim_err"] = _AF6CCI0021_RD_PDH_LPC._upen_lim_err()
        allRegisters["upen_lim_mat"] = _AF6CCI0021_RD_PDH_LPC._upen_lim_mat()
        allRegisters["upen_th_fdl_mat"] = _AF6CCI0021_RD_PDH_LPC._upen_th_fdl_mat()
        allRegisters["upen_th_err"] = _AF6CCI0021_RD_PDH_LPC._upen_th_err()
        allRegisters["upen_stk"] = _AF6CCI0021_RD_PDH_LPC._upen_stk()
        allRegisters["upen_pat"] = _AF6CCI0021_RD_PDH_LPC._upen_pat()
        allRegisters["upen_len"] = _AF6CCI0021_RD_PDH_LPC._upen_len()
        allRegisters["upen_info"] = _AF6CCI0021_RD_PDH_LPC._upen_info()
        allRegisters["upen_fdl_stk"] = _AF6CCI0021_RD_PDH_LPC._upen_fdl_stk()
        allRegisters["upen_fdlpat"] = _AF6CCI0021_RD_PDH_LPC._upen_fdlpat()
        allRegisters["upen_len_mess"] = _AF6CCI0021_RD_PDH_LPC._upen_len_mess()
        allRegisters["upen_fdl_info"] = _AF6CCI0021_RD_PDH_LPC._upen_fdl_info()
        allRegisters["upen_th_stt_int"] = _AF6CCI0021_RD_PDH_LPC._upen_th_stt_int()
        return allRegisters

    class _upen_test1(AtRegister.AtRegister):
        def name(self):
            return "LC test reg1"
    
        def description(self):
            return "For testing only"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0000
            
        def endAddress(self):
            return 0xffffffff

        class _test_reg1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "test_reg1"
            
            def description(self):
                return "value of reg1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["test_reg1"] = _AF6CCI0021_RD_PDH_LPC._upen_test1._test_reg1()
            return allFields

    class _upen_test2(AtRegister.AtRegister):
        def name(self):
            return "LC test reg2"
    
        def description(self):
            return "For testing only"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0001
            
        def endAddress(self):
            return 0xffffffff

        class _test_reg2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "test_reg2"
            
            def description(self):
                return "value of reg2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["test_reg2"] = _AF6CCI0021_RD_PDH_LPC._upen_test2._test_reg2()
            return allFields

    class _upen_lim_err(AtRegister.AtRegister):
        def name(self):
            return "Config Limit error"
    
        def description(self):
            return "Check pattern error in state matching pattern"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0002
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_lim_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_lim_err"
            
            def description(self):
                return "if cnt err more than limit error,search failed"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_lim_err"] = _AF6CCI0021_RD_PDH_LPC._upen_lim_err._cfg_lim_err()
            return allFields

    class _upen_lim_mat(AtRegister.AtRegister):
        def name(self):
            return "Config limit match"
    
        def description(self):
            return "Used to check parttern match"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0003
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_lim_mat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_lim_mat"
            
            def description(self):
                return "if cnt match more than limit mat, serch ok"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_lim_mat"] = _AF6CCI0021_RD_PDH_LPC._upen_lim_mat._cfg_lim_mat()
            return allFields

    class _upen_th_fdl_mat(AtRegister.AtRegister):
        def name(self):
            return "Config thresh fdl parttern"
    
        def description(self):
            return "Check fdl message codes match"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0004
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_th_fdl_mat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_th_fdl_mat"
            
            def description(self):
                return "if message codes repeat more than thresh, search ok,"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_th_fdl_mat"] = _AF6CCI0021_RD_PDH_LPC._upen_th_fdl_mat._cfg_th_fdl_mat()
            return allFields

    class _upen_th_err(AtRegister.AtRegister):
        def name(self):
            return "Config thersh error"
    
        def description(self):
            return "Max error for one pattern"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0005
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_th_err(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_th_err"
            
            def description(self):
                return "if pattern error more than thresh , change to check another pattern"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_th_err"] = _AF6CCI0021_RD_PDH_LPC._upen_th_err._cfg_th_err()
            return allFields

    class _upen_stk(AtRegister.AtRegister):
        def name(self):
            return "Sticky pattern detected"
    
        def description(self):
            return "Indicate status of fifo inband partern empty or full"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e13ff
            
        def endAddress(self):
            return 0xffffffff

        class _updo_stk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_stk"
            
            def description(self):
                return "bit[0] = 1 :empty bit[1] = 1 :full, bit[2] = 1 :interrupt"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_stk"] = _AF6CCI0021_RD_PDH_LPC._upen_stk._updo_stk()
            return allFields

    class _upen_pat(AtRegister.AtRegister):
        def name(self):
            return "Config User programmble Pattern User Codes"
    
        def description(self):
            return "config patter search"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "0xE_13F0 + $pat"
            
        def startAddress(self):
            return 0x000e13f0
            
        def endAddress(self):
            return 0xffffffff

        class _updo_pat(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_pat"
            
            def description(self):
                return "patt codes"
            
            def type(self):
                return "R_0"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_pat"] = _AF6CCI0021_RD_PDH_LPC._upen_pat._updo_pat()
            return allFields

    class _upen_len(AtRegister.AtRegister):
        def name(self):
            return "Number of pattern"
    
        def description(self):
            return "Indicate number of pattern is detected"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e13fe
            
        def endAddress(self):
            return 0xffffffff

        class _info_len_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "info_len_l"
            
            def description(self):
                return "number patt detected"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["info_len_l"] = _AF6CCI0021_RD_PDH_LPC._upen_len._info_len_l()
            return allFields

    class _upen_info(AtRegister.AtRegister):
        def name(self):
            return "Pattern Inband Detected"
    
        def description(self):
            return "Used to report pattern detected to cpu"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "$len 0-1023"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _pat_info_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "pat_info_l"
            
            def description(self):
                return "pattern info bit [14:4] chanel id, bit [3:0] == 0x0  CSU UP bit [3:0] == 0x1  CSU DOWN bit [3:0] == 0x2  FAC1 UP bit [3:0] == 0x3  FAC1 DOWN bit [3:0] == 0x4  FAC2 UP bit [3:0] == 0x5  FAC2 DOWN"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["pat_info_l"] = _AF6CCI0021_RD_PDH_LPC._upen_info._pat_info_l()
            return allFields

    class _upen_fdl_stk(AtRegister.AtRegister):
        def name(self):
            return "Sticky FDL message detected"
    
        def description(self):
            return "Indicate status of fifo fdl message empty or full"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0bff
            
        def endAddress(self):
            return 0xffffffff

        class _updo_fdlstk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_fdlstk"
            
            def description(self):
                return "bit[0] = 1 :empty, bit[1] = 1 :full, bit[2] = 1 :interrupt"
            
            def type(self):
                return "R2C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_fdlstk"] = _AF6CCI0021_RD_PDH_LPC._upen_fdl_stk._updo_fdlstk()
            return allFields

    class _upen_fdlpat(AtRegister.AtRegister):
        def name(self):
            return "Config User programmble Pattern User Codes"
    
        def description(self):
            return "check fdl message codes"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0bfd
            
        def endAddress(self):
            return 0xffffffff

        class _updo_fdl_mess(AtRegister.AtRegisterField):
            def stopBit(self):
                return 55
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_fdl_mess"
            
            def description(self):
                return "mess codes"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_fdl_mess"] = _AF6CCI0021_RD_PDH_LPC._upen_fdlpat._updo_fdl_mess()
            return allFields

    class _upen_len_mess(AtRegister.AtRegister):
        def name(self):
            return "Number of message"
    
        def description(self):
            return "Indicate number of message codes is detected"
            
        def width(self):
            return 128
        
        def type(self):
            return "Status "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0bfe
            
        def endAddress(self):
            return 0xffffffff

        class _info_len_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 0
        
            def name(self):
                return "info_len_l"
            
            def description(self):
                return "number patt detected"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["info_len_l"] = _AF6CCI0021_RD_PDH_LPC._upen_len_mess._info_len_l()
            return allFields

    class _upen_fdl_info(AtRegister.AtRegister):
        def name(self):
            return "Message FDL Detected"
    
        def description(self):
            return "Info fdl message detected"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return "$len 0-1023"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _mess_info_l(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mess_info_l"
            
            def description(self):
                return "message info bit [14:4] chanel id, bit[3:0]== 0x0 11111111_01111110 bit[3:0]== 0x1 line loop up bit[3:0]== 0x2 line loop down bit[3:0]== 0x3 payload loop up bit[3:0]== 0x4 payload loop down bit[3:0]== 0x5 network loop up bit[3:0]== 0x6 network loop down"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mess_info_l"] = _AF6CCI0021_RD_PDH_LPC._upen_fdl_info._mess_info_l()
            return allFields

    class _upen_th_stt_int(AtRegister.AtRegister):
        def name(self):
            return "Config Theshold Pattern Report"
    
        def description(self):
            return "Maximum Pattern Report"
            
        def width(self):
            return 128
        
        def type(self):
            return "Config "
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000e0006
            
        def endAddress(self):
            return 0xffffffff

        class _updo_th_stt_int(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "updo_th_stt_int"
            
            def description(self):
                return "if number of pattern detected more than threshold, interrupt enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["updo_th_stt_int"] = _AF6CCI0021_RD_PDH_LPC._upen_th_stt_int._updo_th_stt_int()
            return allFields

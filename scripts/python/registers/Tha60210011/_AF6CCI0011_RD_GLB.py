import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0011_RD_GLB(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["ProductID"] = _AF6CCI0011_RD_GLB._ProductID()
        allRegisters["YYMMDD_VerID"] = _AF6CCI0011_RD_GLB._YYMMDD_VerID()
        allRegisters["InternalID"] = _AF6CCI0011_RD_GLB._InternalID()
        allRegisters["Active"] = _AF6CCI0011_RD_GLB._Active()
        allRegisters["Timeout"] = _AF6CCI0011_RD_GLB._Timeout()
        allRegisters["DebugPwControl"] = _AF6CCI0011_RD_GLB._DebugPwControl()
        allRegisters["DebugPwSticky"] = _AF6CCI0011_RD_GLB._DebugPwSticky()
        allRegisters["DebugPwPlaHoSpeed"] = _AF6CCI0011_RD_GLB._DebugPwPlaHoSpeed()
        allRegisters["DebugPwPdaHoSpeed"] = _AF6CCI0011_RD_GLB._DebugPwPdaHoSpeed()
        allRegisters["DebugPwPlaLoSpeed"] = _AF6CCI0011_RD_GLB._DebugPwPlaLoSpeed()
        allRegisters["DebugPwPdaLoSpeed"] = _AF6CCI0011_RD_GLB._DebugPwPdaLoSpeed()
        allRegisters["DebugPwPweSpeed"] = _AF6CCI0011_RD_GLB._DebugPwPweSpeed()
        allRegisters["DebugPwClaLoSpeed"] = _AF6CCI0011_RD_GLB._DebugPwClaLoSpeed()
        allRegisters["ChipTempSticky"] = _AF6CCI0011_RD_GLB._ChipTempSticky()
        return allRegisters

    class _ProductID(AtRegister.AtRegister):
        def name(self):
            return "Device Product ID"
    
        def description(self):
            return "This register indicates Product ID."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _ProductID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ProductID"
            
            def description(self):
                return "ProductId"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ProductID"] = _AF6CCI0011_RD_GLB._ProductID._ProductID()
            return allFields

    class _YYMMDD_VerID(AtRegister.AtRegister):
        def name(self):
            return "Device Year Month Day Version ID"
    
        def description(self):
            return "This register indicates Year Month Day and main version ID."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0xffffffff

        class _Year(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "Year"
            
            def description(self):
                return "Year"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Month(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Month"
            
            def description(self):
                return "Month"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Day(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "Day"
            
            def description(self):
                return "Day"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _Version(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Version"
            
            def description(self):
                return "Version"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Year"] = _AF6CCI0011_RD_GLB._YYMMDD_VerID._Year()
            allFields["Month"] = _AF6CCI0011_RD_GLB._YYMMDD_VerID._Month()
            allFields["Day"] = _AF6CCI0011_RD_GLB._YYMMDD_VerID._Day()
            allFields["Version"] = _AF6CCI0011_RD_GLB._YYMMDD_VerID._Version()
            return allFields

    class _InternalID(AtRegister.AtRegister):
        def name(self):
            return "Device Internal ID"
    
        def description(self):
            return "This register indicates internal ID."
            
        def width(self):
            return 16
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _InternalID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "InternalID"
            
            def description(self):
                return "InternalID"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["InternalID"] = _AF6CCI0011_RD_GLB._InternalID._InternalID()
            return allFields

    class _Active(AtRegister.AtRegister):
        def name(self):
            return "GLB Sub-Core Active"
    
        def description(self):
            return "This register indicates the active ports."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000001
            
        def endAddress(self):
            return 0xffffffff

        class _SubCoreEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "SubCoreEn"
            
            def description(self):
                return "Enable per sub-core"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SubCoreEn"] = _AF6CCI0011_RD_GLB._Active._SubCoreEn()
            return allFields

    class _Timeout(AtRegister.AtRegister):
        def name(self):
            return "GLB External 8Khz Clock Selection"
    
        def description(self):
            return "This register configures 8Khz clock selection"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0xffffffff

        class _SecondRef8KhzDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "SecondRef8KhzDis"
            
            def description(self):
                return "SecondRef8Khz Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SecondRef8KhzOcnId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 4
        
            def name(self):
                return "SecondRef8KhzOcnId"
            
            def description(self):
                return "SecondRef8Khz OCN port ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FirstRef8KhzDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "FirstRef8KhzDis"
            
            def description(self):
                return "FirstRef8Khz Disable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _FirstRef8KhzOcnId(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "FirstRef8KhzOcnId"
            
            def description(self):
                return "FirstRef8Khz OCN port ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["SecondRef8KhzDis"] = _AF6CCI0011_RD_GLB._Timeout._SecondRef8KhzDis()
            allFields["SecondRef8KhzOcnId"] = _AF6CCI0011_RD_GLB._Timeout._SecondRef8KhzOcnId()
            allFields["FirstRef8KhzDis"] = _AF6CCI0011_RD_GLB._Timeout._FirstRef8KhzDis()
            allFields["FirstRef8KhzOcnId"] = _AF6CCI0011_RD_GLB._Timeout._FirstRef8KhzOcnId()
            return allFields

    class _DebugPwControl(AtRegister.AtRegister):
        def name(self):
            return "GLB Debug PW Control"
    
        def description(self):
            return "This register is used to configure debug PW parameters"
            
        def width(self):
            return 30
        
        def type(self):
            return "Config|Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _LoTdmPwID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 20
        
            def name(self):
                return "LoTdmPwID"
            
            def description(self):
                return "Lo TDM PW ID or HO Master STS ID corresponding to global PW ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _GlobalPwID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 7
        
            def name(self):
                return "GlobalPwID"
            
            def description(self):
                return "Global PW ID need to debug"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _OC48ID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 4
        
            def name(self):
                return "OC48ID"
            
            def description(self):
                return "OC48 ID"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LoOC24Slice(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "LoOC24Slice"
            
            def description(self):
                return "Low Order OC24 slice if the PW belong to low order path"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _HoPwType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HoPwType"
            
            def description(self):
                return "High Order PW type if the PW belong to high order path 0: STS1/VC3 1: STS3/VC4 2: STS12/VC4-4C Others: STS48/VC4-16C"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["LoTdmPwID"] = _AF6CCI0011_RD_GLB._DebugPwControl._LoTdmPwID()
            allFields["GlobalPwID"] = _AF6CCI0011_RD_GLB._DebugPwControl._GlobalPwID()
            allFields["OC48ID"] = _AF6CCI0011_RD_GLB._DebugPwControl._OC48ID()
            allFields["LoOC24Slice"] = _AF6CCI0011_RD_GLB._DebugPwControl._LoOC24Slice()
            allFields["HoPwType"] = _AF6CCI0011_RD_GLB._DebugPwControl._HoPwType()
            return allFields

    class _DebugPwSticky(AtRegister.AtRegister):
        def name(self):
            return "GLB Debug PW Control"
    
        def description(self):
            return "This register is used to debug PW modules"
            
        def width(self):
            return 29
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000024
            
        def endAddress(self):
            return 0xffffffff

        class _ClaPrbsErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "ClaPrbsErr"
            
            def description(self):
                return "CLA output PRBS error"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        class _PwePrbsErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "PwePrbsErr"
            
            def description(self):
                return "PWE input PRBS error"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoPrbsErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "PlaLoPrbsErr"
            
            def description(self):
                return "Low Order PLA input PRBS error"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        class _PdaHoPrbsErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "PdaHoPrbsErr"
            
            def description(self):
                return "High Order PDA output PRBS error"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaHoPrbsErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "PlaHoPrbsErr"
            
            def description(self):
                return "High Order PLA input PRBS error"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        class _ClaPrbsSyn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "ClaPrbsSyn"
            
            def description(self):
                return "CLA output PRBS sync"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        class _PwePrbsSyn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PwePrbsSyn"
            
            def description(self):
                return "PWE input PRBS sync"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaLoPrbsSyn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "PlaLoPrbsSyn"
            
            def description(self):
                return "Low Order PLA input PRBS sync"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        class _PdaHoPrbsSyn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "PdaHoPrbsSyn"
            
            def description(self):
                return "High Order PDA output PRBS sync"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        class _PlaHoPrbsSyn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaHoPrbsSyn"
            
            def description(self):
                return "High Order PLA input PRBS sync"
            
            def type(self):
                return "RC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ClaPrbsErr"] = _AF6CCI0011_RD_GLB._DebugPwSticky._ClaPrbsErr()
            allFields["PwePrbsErr"] = _AF6CCI0011_RD_GLB._DebugPwSticky._PwePrbsErr()
            allFields["PlaLoPrbsErr"] = _AF6CCI0011_RD_GLB._DebugPwSticky._PlaLoPrbsErr()
            allFields["PdaHoPrbsErr"] = _AF6CCI0011_RD_GLB._DebugPwSticky._PdaHoPrbsErr()
            allFields["PlaHoPrbsErr"] = _AF6CCI0011_RD_GLB._DebugPwSticky._PlaHoPrbsErr()
            allFields["ClaPrbsSyn"] = _AF6CCI0011_RD_GLB._DebugPwSticky._ClaPrbsSyn()
            allFields["PwePrbsSyn"] = _AF6CCI0011_RD_GLB._DebugPwSticky._PwePrbsSyn()
            allFields["PlaLoPrbsSyn"] = _AF6CCI0011_RD_GLB._DebugPwSticky._PlaLoPrbsSyn()
            allFields["PdaHoPrbsSyn"] = _AF6CCI0011_RD_GLB._DebugPwSticky._PdaHoPrbsSyn()
            allFields["PlaHoPrbsSyn"] = _AF6CCI0011_RD_GLB._DebugPwSticky._PlaHoPrbsSyn()
            return allFields

    class _DebugPwPlaHoSpeed(AtRegister.AtRegister):
        def name(self):
            return "GLB Debug PW PLA HO Speed"
    
        def description(self):
            return "This register is used to show PLA HO PW speed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000030
            
        def endAddress(self):
            return 0xffffffff

        class _PlaHoPwSpeed(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaHoPwSpeed"
            
            def description(self):
                return "High order PLA input PW speed"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaHoPwSpeed"] = _AF6CCI0011_RD_GLB._DebugPwPlaHoSpeed._PlaHoPwSpeed()
            return allFields

    class _DebugPwPdaHoSpeed(AtRegister.AtRegister):
        def name(self):
            return "GLB Debug PW PDA HO Speed"
    
        def description(self):
            return "This register is used to show PDA HO PW speed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000031
            
        def endAddress(self):
            return 0xffffffff

        class _PdaHoPwSpeed(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PdaHoPwSpeed"
            
            def description(self):
                return "High order PDA output PW speed"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PdaHoPwSpeed"] = _AF6CCI0011_RD_GLB._DebugPwPdaHoSpeed._PdaHoPwSpeed()
            return allFields

    class _DebugPwPlaLoSpeed(AtRegister.AtRegister):
        def name(self):
            return "GLB Debug PW PLA LO Speed"
    
        def description(self):
            return "This register is used to show PLA LO PW speed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000032
            
        def endAddress(self):
            return 0xffffffff

        class _PlaLoPwSpeed(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PlaLoPwSpeed"
            
            def description(self):
                return "Low order PLA input PW speed"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PlaLoPwSpeed"] = _AF6CCI0011_RD_GLB._DebugPwPlaLoSpeed._PlaLoPwSpeed()
            return allFields

    class _DebugPwPdaLoSpeed(AtRegister.AtRegister):
        def name(self):
            return "GLB Debug PW PDA LO Speed"
    
        def description(self):
            return "This register is used to show PDA LO PW speed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000033
            
        def endAddress(self):
            return 0xffffffff

        class _PdaLoPwSpeed(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PdaLoPwSpeed"
            
            def description(self):
                return "Low order PDA output PW speed"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PdaLoPwSpeed"] = _AF6CCI0011_RD_GLB._DebugPwPdaLoSpeed._PdaLoPwSpeed()
            return allFields

    class _DebugPwPweSpeed(AtRegister.AtRegister):
        def name(self):
            return "GLB Debug PW PWE Speed"
    
        def description(self):
            return "This register is used to show PWE PW speed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000034
            
        def endAddress(self):
            return 0xffffffff

        class _PwePwSpeed(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PwePwSpeed"
            
            def description(self):
                return "PWE input PW speed"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwePwSpeed"] = _AF6CCI0011_RD_GLB._DebugPwPweSpeed._PwePwSpeed()
            return allFields

    class _DebugPwClaLoSpeed(AtRegister.AtRegister):
        def name(self):
            return "GLB Debug PW CLA Speed"
    
        def description(self):
            return "This register is used to show PWE PW speed"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000035
            
        def endAddress(self):
            return 0xffffffff

        class _ClaPwSpeed(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ClaPwSpeed"
            
            def description(self):
                return "CLA output PW speed"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ClaPwSpeed"] = _AF6CCI0011_RD_GLB._DebugPwClaLoSpeed._ClaPwSpeed()
            return allFields

    class _ChipTempSticky(AtRegister.AtRegister):
        def name(self):
            return "Chip Temperature Sticky"
    
        def description(self):
            return "This register is used to sticky temperature"
            
        def width(self):
            return 3
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000020
            
        def endAddress(self):
            return 0xffffffff

        class _ChipSlr2TempStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "ChipSlr2TempStk"
            
            def description(self):
                return "Chip SLR2 temperature"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        class _ChipSlr1TempStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "ChipSlr1TempStk"
            
            def description(self):
                return "Chip SLR1 temperature"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        class _ChipSlr0TempStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ChipSlr0TempStk"
            
            def description(self):
                return "Chip SLR0 temperature"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ChipSlr2TempStk"] = _AF6CCI0011_RD_GLB._ChipTempSticky._ChipSlr2TempStk()
            allFields["ChipSlr1TempStk"] = _AF6CCI0011_RD_GLB._ChipTempSticky._ChipSlr1TempStk()
            allFields["ChipSlr0TempStk"] = _AF6CCI0011_RD_GLB._ChipTempSticky._ChipSlr0TempStk()
            return allFields

import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CNC0022_RD_PWE(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["pw_txeth_hdr_mode"] = _AF6CNC0022_RD_PWE._pw_txeth_hdr_mode()
        allRegisters["Eth_cnt0_64_ro"] = _AF6CNC0022_RD_PWE._Eth_cnt0_64_ro()
        allRegisters["Eth_cnt0_64_rc"] = _AF6CNC0022_RD_PWE._Eth_cnt0_64_rc()
        allRegisters["Eth_cnt65_127_ro"] = _AF6CNC0022_RD_PWE._Eth_cnt65_127_ro()
        allRegisters["Eth_cnt65_127_rc"] = _AF6CNC0022_RD_PWE._Eth_cnt65_127_rc()
        allRegisters["Eth_cnt128_255_ro"] = _AF6CNC0022_RD_PWE._Eth_cnt128_255_ro()
        allRegisters["Eth_cnt128_255_rc"] = _AF6CNC0022_RD_PWE._Eth_cnt128_255_rc()
        allRegisters["Eth_cnt256_511_ro"] = _AF6CNC0022_RD_PWE._Eth_cnt256_511_ro()
        allRegisters["Eth_cnt256_511_rc"] = _AF6CNC0022_RD_PWE._Eth_cnt256_511_rc()
        allRegisters["Eth_cnt512_1024_ro"] = _AF6CNC0022_RD_PWE._Eth_cnt512_1024_ro()
        allRegisters["Eth_cnt512_1024_rc"] = _AF6CNC0022_RD_PWE._Eth_cnt512_1024_rc()
        allRegisters["Eth_cnt1025_1528_ro"] = _AF6CNC0022_RD_PWE._Eth_cnt1025_1528_ro()
        allRegisters["Eth_cnt1025_1528_rc"] = _AF6CNC0022_RD_PWE._Eth_cnt1025_1528_rc()
        allRegisters["Eth_cnt1529_2047_ro"] = _AF6CNC0022_RD_PWE._Eth_cnt1529_2047_ro()
        allRegisters["Eth_cnt1529_2047_rc"] = _AF6CNC0022_RD_PWE._Eth_cnt1529_2047_rc()
        allRegisters["Eth_cnt_jumbo_ro"] = _AF6CNC0022_RD_PWE._Eth_cnt_jumbo_ro()
        allRegisters["Eth_cnt_jumbo_rc"] = _AF6CNC0022_RD_PWE._Eth_cnt_jumbo_rc()
        allRegisters["Eth_cnt_Unicast_ro"] = _AF6CNC0022_RD_PWE._Eth_cnt_Unicast_ro()
        allRegisters["Eth_cnt_Unicast_rc"] = _AF6CNC0022_RD_PWE._Eth_cnt_Unicast_rc()
        allRegisters["eth_tx_pkt_cnt_ro"] = _AF6CNC0022_RD_PWE._eth_tx_pkt_cnt_ro()
        allRegisters["eth_tx_pkt_cnt_rc"] = _AF6CNC0022_RD_PWE._eth_tx_pkt_cnt_rc()
        allRegisters["eth_tx_bcast_pkt_cnt_ro"] = _AF6CNC0022_RD_PWE._eth_tx_bcast_pkt_cnt_ro()
        allRegisters["eth_tx_bcast_pkt_cnt_rc"] = _AF6CNC0022_RD_PWE._eth_tx_bcast_pkt_cnt_rc()
        allRegisters["eth_tx_mcast_pkt_cnt_ro"] = _AF6CNC0022_RD_PWE._eth_tx_mcast_pkt_cnt_ro()
        allRegisters["eth_tx_mcast_pkt_cnt_rc"] = _AF6CNC0022_RD_PWE._eth_tx_mcast_pkt_cnt_rc()
        allRegisters["Eth_cnt_byte_ro"] = _AF6CNC0022_RD_PWE._Eth_cnt_byte_ro()
        allRegisters["Eth_cnt_byte_rc"] = _AF6CNC0022_RD_PWE._Eth_cnt_byte_rc()
        allRegisters["pwe_Parity_control"] = _AF6CNC0022_RD_PWE._pwe_Parity_control()
        allRegisters["pwe_Parity_Disable_control"] = _AF6CNC0022_RD_PWE._pwe_Parity_Disable_control()
        allRegisters["pwe_Parity_stk_err"] = _AF6CNC0022_RD_PWE._pwe_Parity_stk_err()
        return allRegisters

    class _pw_txeth_hdr_mode(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Transmit Ethernet Header Mode Control"
    
        def description(self):
            return "This register is used to control read/write Pseudowire Transmit Ethernet Header Value from/to DDR"
            
        def width(self):
            return 9
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00000000 +  PWID"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0xffffffff

        class _TxEthPadMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 7
        
            def name(self):
                return "TxEthPadMode"
            
            def description(self):
                return "this is configuration for insertion PAD in short packets 0: Insert PAD when total Ethernet packet length is less than 64 bytes. Refer to IEEE 802.3 1: Insert PAD when control word plus payload length is less than 64 bytes 2: Insert PAD when total Ethernet packet length minus VLANs and MPLS outer labels(if exist) is less than 64 bytes"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxEthPwRtpEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "TxEthPwRtpEn"
            
            def description(self):
                return "this is RTP enable for TDM PW packet 1: Enable RTP 0: Disable RTP"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxEthPwPsnType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 2
        
            def name(self):
                return "TxEthPwPsnType"
            
            def description(self):
                return "this is Transmit PSN header mode working 1: PW PSN header is UDP/IPv4 2: PW PSN header is UDP/IPv6 3: PW MPLS no outer label over Ipv4 (total 1 MPLS label) 4: PW MPLS no outer label over Ipv6 (total 1 MPLS label) 5: PW MPLS one outer label over Ipv4 (total 2 MPLS label) 6: PW MPLS one outer label over Ipv6 (total 2 MPLS label) 7: PW MPLS two outer label over Ipv4 (total 3 MPLS label) 8: PW MPLS two outer label over Ipv6 (total 3 MPLS label) Others: for other PW PSN header type"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _TxEthPwNumVlan(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TxEthPwNumVlan"
            
            def description(self):
                return "This is number of vlan in Transmit Ethernet packet 0: no vlan 1: 1 vlan 2: 2 vlan"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxEthPadMode"] = _AF6CNC0022_RD_PWE._pw_txeth_hdr_mode._TxEthPadMode()
            allFields["TxEthPwRtpEn"] = _AF6CNC0022_RD_PWE._pw_txeth_hdr_mode._TxEthPwRtpEn()
            allFields["TxEthPwPsnType"] = _AF6CNC0022_RD_PWE._pw_txeth_hdr_mode._TxEthPwPsnType()
            allFields["TxEthPwNumVlan"] = _AF6CNC0022_RD_PWE._pw_txeth_hdr_mode._TxEthPwNumVlan()
            return allFields

    class _Eth_cnt0_64_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count0_64 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 0 to 64 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0004000 + eth_port"
            
        def startAddress(self):
            return 0x00004000
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt0_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt0_64"
            
            def description(self):
                return "This is statistic counter for the packet having 0 to 64 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt0_64"] = _AF6CNC0022_RD_PWE._Eth_cnt0_64_ro._CLAEthCnt0_64()
            return allFields

    class _Eth_cnt0_64_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count0_64 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 0 to 64 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0004800 + eth_port"
            
        def startAddress(self):
            return 0x00004800
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt0_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt0_64"
            
            def description(self):
                return "This is statistic counter for the packet having 0 to 64 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt0_64"] = _AF6CNC0022_RD_PWE._Eth_cnt0_64_rc._CLAEthCnt0_64()
            return allFields

    class _Eth_cnt65_127_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count65_127 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 65 to 127 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0004002 + eth_port"
            
        def startAddress(self):
            return 0x00004002
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt65_127(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt65_127"
            
            def description(self):
                return "This is statistic counter for the packet having 65 to 127 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt65_127"] = _AF6CNC0022_RD_PWE._Eth_cnt65_127_ro._CLAEthCnt65_127()
            return allFields

    class _Eth_cnt65_127_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count65_127 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 65 to 127 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0004802 + eth_port"
            
        def startAddress(self):
            return 0x00004802
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt65_127(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt65_127"
            
            def description(self):
                return "This is statistic counter for the packet having 65 to 127 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt65_127"] = _AF6CNC0022_RD_PWE._Eth_cnt65_127_rc._CLAEthCnt65_127()
            return allFields

    class _Eth_cnt128_255_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count128_255 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 128 to 255 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0004004 + eth_port"
            
        def startAddress(self):
            return 0x00004004
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt128_255(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt128_255"
            
            def description(self):
                return "This is statistic counter for the packet having 128 to 255 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt128_255"] = _AF6CNC0022_RD_PWE._Eth_cnt128_255_ro._CLAEthCnt128_255()
            return allFields

    class _Eth_cnt128_255_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count128_255 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 128 to 255 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0004804 + eth_port"
            
        def startAddress(self):
            return 0x00004804
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt128_255(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt128_255"
            
            def description(self):
                return "This is statistic counter for the packet having 128 to 255 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt128_255"] = _AF6CNC0022_RD_PWE._Eth_cnt128_255_rc._CLAEthCnt128_255()
            return allFields

    class _Eth_cnt256_511_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count256_511 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 256 to 511 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0004006 + eth_port"
            
        def startAddress(self):
            return 0x00004006
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt256_511(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt256_511"
            
            def description(self):
                return "This is statistic counter for the packet having 256 to 511 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt256_511"] = _AF6CNC0022_RD_PWE._Eth_cnt256_511_ro._CLAEthCnt256_511()
            return allFields

    class _Eth_cnt256_511_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count256_511 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 256 to 511 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0004806 + eth_port"
            
        def startAddress(self):
            return 0x00004806
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt256_511(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt256_511"
            
            def description(self):
                return "This is statistic counter for the packet having 256 to 511 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt256_511"] = _AF6CNC0022_RD_PWE._Eth_cnt256_511_rc._CLAEthCnt256_511()
            return allFields

    class _Eth_cnt512_1024_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count512_1023 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 512 to 1023 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0004008 + eth_port"
            
        def startAddress(self):
            return 0x00004008
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt512_1024(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt512_1024"
            
            def description(self):
                return "This is statistic counter for the packet having 512 to 1023 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt512_1024"] = _AF6CNC0022_RD_PWE._Eth_cnt512_1024_ro._CLAEthCnt512_1024()
            return allFields

    class _Eth_cnt512_1024_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count512_1023 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 512 to 1023 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0004808 + eth_port"
            
        def startAddress(self):
            return 0x00004808
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt512_1024(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt512_1024"
            
            def description(self):
                return "This is statistic counter for the packet having 512 to 1023 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt512_1024"] = _AF6CNC0022_RD_PWE._Eth_cnt512_1024_rc._CLAEthCnt512_1024()
            return allFields

    class _Eth_cnt1025_1528_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count1024_1518 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 1024 to 1518 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000400A + eth_port"
            
        def startAddress(self):
            return 0x0000400a
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt1025_1528(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt1025_1528"
            
            def description(self):
                return "This is statistic counter for the packet having 1024 to 1518 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt1025_1528"] = _AF6CNC0022_RD_PWE._Eth_cnt1025_1528_ro._CLAEthCnt1025_1528()
            return allFields

    class _Eth_cnt1025_1528_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count1024_1518 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 1024 to 1518 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000480A + eth_port"
            
        def startAddress(self):
            return 0x0000480a
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt1025_1528(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt1025_1528"
            
            def description(self):
                return "This is statistic counter for the packet having 1024 to 1518 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt1025_1528"] = _AF6CNC0022_RD_PWE._Eth_cnt1025_1528_rc._CLAEthCnt1025_1528()
            return allFields

    class _Eth_cnt1529_2047_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count1519_2047 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 1519 to 2047 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000400C + eth_port"
            
        def startAddress(self):
            return 0x0000400c
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt1529_2047(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt1529_2047"
            
            def description(self):
                return "This is statistic counter for the packet having 1519 to 2047 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt1529_2047"] = _AF6CNC0022_RD_PWE._Eth_cnt1529_2047_ro._CLAEthCnt1529_2047()
            return allFields

    class _Eth_cnt1529_2047_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count1519_2047 bytes packet"
    
        def description(self):
            return "This register is statistic counter for the packet having 1519 to 2047 bytes"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000480C + eth_port"
            
        def startAddress(self):
            return 0x0000480c
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCnt1529_2047(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCnt1529_2047"
            
            def description(self):
                return "This is statistic counter for the packet having 1519 to 2047 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCnt1529_2047"] = _AF6CNC0022_RD_PWE._Eth_cnt1529_2047_rc._CLAEthCnt1529_2047()
            return allFields

    class _Eth_cnt_jumbo_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count Jumbo packet"
    
        def description(self):
            return "This register is statistic counter for the packet having more than 2048 bytes (jumbo)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000400E + eth_port"
            
        def startAddress(self):
            return 0x0000400e
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntJumbo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntJumbo"
            
            def description(self):
                return "This is statistic counter for the packet more than 2048 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntJumbo"] = _AF6CNC0022_RD_PWE._Eth_cnt_jumbo_ro._CLAEthCntJumbo()
            return allFields

    class _Eth_cnt_jumbo_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count Jumbo packet"
    
        def description(self):
            return "This register is statistic counter for the packet having more than 2048 bytes (jumbo)"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000480E + eth_port"
            
        def startAddress(self):
            return 0x0000480e
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntJumbo(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntJumbo"
            
            def description(self):
                return "This is statistic counter for the packet more than 2048 bytes"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntJumbo"] = _AF6CNC0022_RD_PWE._Eth_cnt_jumbo_rc._CLAEthCntJumbo()
            return allFields

    class _Eth_cnt_Unicast_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count Unicast packet"
    
        def description(self):
            return "This register is statistic counter for the unicast packet"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0004010 + eth_port"
            
        def startAddress(self):
            return 0x00004010
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntUnicast(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntUnicast"
            
            def description(self):
                return "This is statistic counter for the unicast packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntUnicast"] = _AF6CNC0022_RD_PWE._Eth_cnt_Unicast_ro._CLAEthCntUnicast()
            return allFields

    class _Eth_cnt_Unicast_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count Unicast packet"
    
        def description(self):
            return "This register is statistic counter for the unicast packet"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0004810 + eth_port"
            
        def startAddress(self):
            return 0x00004810
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntUnicast(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntUnicast"
            
            def description(self):
                return "This is statistic counter for the unicast packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntUnicast"] = _AF6CNC0022_RD_PWE._Eth_cnt_Unicast_rc._CLAEthCntUnicast()
            return allFields

    class _eth_tx_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count Total packet"
    
        def description(self):
            return "This register is statistic counter for the total packet at Transmit side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0004012 + eth_port"
            
        def startAddress(self):
            return 0x00004012
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntTotal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntTotal"
            
            def description(self):
                return "This is statistic counter total packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntTotal"] = _AF6CNC0022_RD_PWE._eth_tx_pkt_cnt_ro._CLAEthCntTotal()
            return allFields

    class _eth_tx_pkt_cnt_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count Total packet"
    
        def description(self):
            return "This register is statistic counter for the total packet at Transmit side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0004812 + eth_port"
            
        def startAddress(self):
            return 0x00004812
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntTotal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntTotal"
            
            def description(self):
                return "This is statistic counter total packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntTotal"] = _AF6CNC0022_RD_PWE._eth_tx_pkt_cnt_rc._CLAEthCntTotal()
            return allFields

    class _eth_tx_bcast_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count Broadcast packet"
    
        def description(self):
            return "This register is statistic counter for the Broadcast packet at Transmit side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0004014 + eth_port"
            
        def startAddress(self):
            return 0x00004014
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntBroadcast(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntBroadcast"
            
            def description(self):
                return "This is statistic counter broadcast packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntBroadcast"] = _AF6CNC0022_RD_PWE._eth_tx_bcast_pkt_cnt_ro._CLAEthCntBroadcast()
            return allFields

    class _eth_tx_bcast_pkt_cnt_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count Broadcast packet"
    
        def description(self):
            return "This register is statistic counter for the Broadcast packet at Transmit side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0004814 + eth_port"
            
        def startAddress(self):
            return 0x00004814
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntBroadcast(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntBroadcast"
            
            def description(self):
                return "This is statistic counter broadcast packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntBroadcast"] = _AF6CNC0022_RD_PWE._eth_tx_bcast_pkt_cnt_rc._CLAEthCntBroadcast()
            return allFields

    class _eth_tx_mcast_pkt_cnt_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count Multicast packet"
    
        def description(self):
            return "This register is statistic counter for the Multicast packet at Transmit side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0004016 + eth_port"
            
        def startAddress(self):
            return 0x00004016
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntMulticast(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntMulticast"
            
            def description(self):
                return "This is statistic counter multicast packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntMulticast"] = _AF6CNC0022_RD_PWE._eth_tx_mcast_pkt_cnt_ro._CLAEthCntMulticast()
            return allFields

    class _eth_tx_mcast_pkt_cnt_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count Multicast packet"
    
        def description(self):
            return "This register is statistic counter for the Multicast packet at Transmit side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0004816 + eth_port"
            
        def startAddress(self):
            return 0x00004816
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntMulticast(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntMulticast"
            
            def description(self):
                return "This is statistic counter multicast packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntMulticast"] = _AF6CNC0022_RD_PWE._eth_tx_mcast_pkt_cnt_rc._CLAEthCntMulticast()
            return allFields

    class _Eth_cnt_byte_ro(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count number of bytes of packet"
    
        def description(self):
            return "This register is statistic count number of bytes of packet at Transmit side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000401E + eth_port"
            
        def startAddress(self):
            return 0x0000401e
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntByte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntByte"
            
            def description(self):
                return "This is statistic counter number of bytes of packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntByte"] = _AF6CNC0022_RD_PWE._Eth_cnt_byte_ro._CLAEthCntByte()
            return allFields

    class _Eth_cnt_byte_rc(AtRegister.AtRegister):
        def name(self):
            return "Transmit Ethernet port Count number of bytes of packet"
    
        def description(self):
            return "This register is statistic count number of bytes of packet at Transmit side"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x000481E + eth_port"
            
        def startAddress(self):
            return 0x0000481e
            
        def endAddress(self):
            return 0xffffffff

        class _CLAEthCntByte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CLAEthCntByte"
            
            def description(self):
                return "This is statistic counter number of bytes of packet"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLAEthCntByte"] = _AF6CNC0022_RD_PWE._Eth_cnt_byte_rc._CLAEthCntByte()
            return allFields

    class _pwe_Parity_control(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Parity Register Control"
    
        def description(self):
            return "This register using for Force Parity"
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000f000
            
        def endAddress(self):
            return 0xffffffff

        class _PwForceErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PwForceErr"
            
            def description(self):
                return "Force parity error enable for \"Pseudowire Transmit Ethernet Header Mode Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwForceErr"] = _AF6CNC0022_RD_PWE._pwe_Parity_control._PwForceErr()
            return allFields

    class _pwe_Parity_Disable_control(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Parity Disable register Control"
    
        def description(self):
            return "This register using for Disable Parity"
            
        def width(self):
            return 1
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000f001
            
        def endAddress(self):
            return 0xffffffff

        class _PwDisChkErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PwDisChkErr"
            
            def description(self):
                return "Disable parity error check for \"Pseudowire Transmit Ethernet Header Mode Control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwDisChkErr"] = _AF6CNC0022_RD_PWE._pwe_Parity_Disable_control._PwDisChkErr()
            return allFields

    class _pwe_Parity_stk_err(AtRegister.AtRegister):
        def name(self):
            return "Pseudowire Parity sticky error"
    
        def description(self):
            return "This register using for checking sticky error"
            
        def width(self):
            return 1
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000f002
            
        def endAddress(self):
            return 0xffffffff

        class _PwStkErr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PwStkErr"
            
            def description(self):
                return "parity error for \"Pseudowire Transmit Ethernet Header Mode Control\""
            
            def type(self):
                return "W1C"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PwStkErr"] = _AF6CNC0022_RD_PWE._pwe_Parity_stk_err._PwStkErr()
            return allFields

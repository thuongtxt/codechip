import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0011_RD_PDH_PRM(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_cfg_ctrl0"] = _AF6CCI0011_RD_PDH_PRM._upen_cfg_ctrl0()
        allRegisters["upen_txcfg_force"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force()
        allRegisters["upen_txcfg_dis"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis()
        allRegisters["upen_txsticky_par"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par()
        allRegisters["upen_prm_txcfgcr"] = _AF6CCI0011_RD_PDH_PRM._upen_prm_txcfgcr()
        allRegisters["upen_prm_txcfglb"] = _AF6CCI0011_RD_PDH_PRM._upen_prm_txcfglb()
        allRegisters["upen_txprm_cnt_byte"] = _AF6CCI0011_RD_PDH_PRM._upen_txprm_cnt_byte()
        allRegisters["upen_txprm_cnt_pkt"] = _AF6CCI0011_RD_PDH_PRM._upen_txprm_cnt_pkt()
        allRegisters["upen_txprm_cnt_info"] = _AF6CCI0011_RD_PDH_PRM._upen_txprm_cnt_info()
        allRegisters["upen_rxprm_cfgstd"] = _AF6CCI0011_RD_PDH_PRM._upen_rxprm_cfgstd()
        allRegisters["upen_destuff_ctrl0"] = _AF6CCI0011_RD_PDH_PRM._upen_destuff_ctrl0()
        allRegisters["upen_rxprm_gmess"] = _AF6CCI0011_RD_PDH_PRM._upen_rxprm_gmess()
        allRegisters["upen_rxprm_drmess"] = _AF6CCI0011_RD_PDH_PRM._upen_rxprm_drmess()
        allRegisters["upen_rxprm_mmess"] = _AF6CCI0011_RD_PDH_PRM._upen_rxprm_mmess()
        allRegisters["upen_rxprm_cnt_byte"] = _AF6CCI0011_RD_PDH_PRM._upen_rxprm_cnt_byte()
        allRegisters["upen_rxcfg_force"] = _AF6CCI0011_RD_PDH_PRM._upen_rxcfg_force()
        allRegisters["upen_rxcfg_dis"] = _AF6CCI0011_RD_PDH_PRM._upen_rxcfg_dis()
        allRegisters["upen_rxsticky_par"] = _AF6CCI0011_RD_PDH_PRM._upen_rxsticky_par()
        allRegisters["prm_cfg_lben_int"] = _AF6CCI0011_RD_PDH_PRM._prm_cfg_lben_int()
        allRegisters["prm_lb_int_sta"] = _AF6CCI0011_RD_PDH_PRM._prm_lb_int_sta()
        allRegisters["prm_lb_int_crrsta"] = _AF6CCI0011_RD_PDH_PRM._prm_lb_int_crrsta()
        allRegisters["prm_lb_intsta"] = _AF6CCI0011_RD_PDH_PRM._prm_lb_intsta()
        allRegisters["prm_lb_sta_int"] = _AF6CCI0011_RD_PDH_PRM._prm_lb_sta_int()
        allRegisters["prm_lb_en_int"] = _AF6CCI0011_RD_PDH_PRM._prm_lb_en_int()
        return allRegisters

    class _upen_cfg_ctrl0(AtRegister.AtRegister):
        def name(self):
            return "CONFIG CONTROL TX STUFF 0"
    
        def description(self):
            return "config control Stuff global"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00006000
            
        def endAddress(self):
            return 0xffffffff

        class _out_txcfg_ctrl0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 8
        
            def name(self):
                return "out_txcfg_ctrl0"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fcs_err_ins(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "fcs_err_ins"
            
            def description(self):
                return "(0): disable,(1): enable insert error"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 2
        
            def name(self):
                return "reserve1"
            
            def description(self):
                return "reserve1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fcsinscfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "fcsinscfg"
            
            def description(self):
                return "(1) enable insert FCS, (0) disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["out_txcfg_ctrl0"] = _AF6CCI0011_RD_PDH_PRM._upen_cfg_ctrl0._out_txcfg_ctrl0()
            allFields["fcs_err_ins"] = _AF6CCI0011_RD_PDH_PRM._upen_cfg_ctrl0._fcs_err_ins()
            allFields["reserve1"] = _AF6CCI0011_RD_PDH_PRM._upen_cfg_ctrl0._reserve1()
            allFields["fcsinscfg"] = _AF6CCI0011_RD_PDH_PRM._upen_cfg_ctrl0._fcsinscfg()
            allFields["reserve"] = _AF6CCI0011_RD_PDH_PRM._upen_cfg_ctrl0._reserve()
            return allFields

    class _upen_txcfg_force(AtRegister.AtRegister):
        def name(self):
            return "TX CONFIG FORCE ERROR PARITY"
    
        def description(self):
            return "config control FORCE ERROR PARITY"
            
        def width(self):
            return 26
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00006c00
            
        def endAddress(self):
            return 0xffffffff

        class _cfgforce_mdlbuff2_eng8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "cfgforce_mdlbuff2_eng8"
            
            def description(self):
                return "force error MDL buffer 2, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_mdlbuff1_eng8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "cfgforce_mdlbuff1_eng8"
            
            def description(self):
                return "force error MDL buffer 1, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_mdl_eng8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "cfgforce_mdl_eng8"
            
            def description(self):
                return "force error MDL config, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_mdlbuff2_eng7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "cfgforce_mdlbuff2_eng7"
            
            def description(self):
                return "force error MDL buffer 2, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_mdlbuff1_eng7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "cfgforce_mdlbuff1_eng7"
            
            def description(self):
                return "force error MDL buffer 1, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_mdl_eng7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "cfgforce_mdl_eng7"
            
            def description(self):
                return "force error MDL config, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_mdlbuff2_eng6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "cfgforce_mdlbuff2_eng6"
            
            def description(self):
                return "force error MDL buffer 2, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_mdlbuff1_eng6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "cfgforce_mdlbuff1_eng6"
            
            def description(self):
                return "force error MDL buffer 1, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_mdl_eng6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "cfgforce_mdl_eng6"
            
            def description(self):
                return "force error MDL config, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_mdlbuff2_eng5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "cfgforce_mdlbuff2_eng5"
            
            def description(self):
                return "force error MDL buffer 2, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_mdlbuff1_eng5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "cfgforce_mdlbuff1_eng5"
            
            def description(self):
                return "force error MDL buffer 1, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_mdl_eng5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "cfgforce_mdl_eng5"
            
            def description(self):
                return "force error MDL config, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_mdlbuff2_eng4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "cfgforce_mdlbuff2_eng4"
            
            def description(self):
                return "force error MDL buffer 2, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_mdlbuff1_eng4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "cfgforce_mdlbuff1_eng4"
            
            def description(self):
                return "force error MDL buffer 1, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_mdl_eng4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "cfgforce_mdl_eng4"
            
            def description(self):
                return "force error MDL config, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_mdlbuff2_eng3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "cfgforce_mdlbuff2_eng3"
            
            def description(self):
                return "force error MDL buffer 2, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_mdlbuff1_eng3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "cfgforce_mdlbuff1_eng3"
            
            def description(self):
                return "force error MDL buffer 1, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_mdl_eng3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "cfgforce_mdl_eng3"
            
            def description(self):
                return "force error MDL config, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_mdlbuff2_eng2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "cfgforce_mdlbuff2_eng2"
            
            def description(self):
                return "force error MDL buffer 2, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_mdlbuff1_eng2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "cfgforce_mdlbuff1_eng2"
            
            def description(self):
                return "force error MDL buffer 1, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_mdl_eng2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "cfgforce_mdl_eng2"
            
            def description(self):
                return "force error MDL config, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_mdlbuff2_eng1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "cfgforce_mdlbuff2_eng1"
            
            def description(self):
                return "force error MDL buffer 2, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_mdlbuff1_eng1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "cfgforce_mdlbuff1_eng1"
            
            def description(self):
                return "force error MDL buffer 1, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_mdl_eng1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfgforce_mdl_eng1"
            
            def description(self):
                return "force error MDL config, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_lbprm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfgforce_lbprm"
            
            def description(self):
                return "force error PRM LB config, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfgforce_prm"
            
            def description(self):
                return "force error PRM config, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfgforce_mdlbuff2_eng8"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_mdlbuff2_eng8()
            allFields["cfgforce_mdlbuff1_eng8"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_mdlbuff1_eng8()
            allFields["cfgforce_mdl_eng8"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_mdl_eng8()
            allFields["cfgforce_mdlbuff2_eng7"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_mdlbuff2_eng7()
            allFields["cfgforce_mdlbuff1_eng7"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_mdlbuff1_eng7()
            allFields["cfgforce_mdl_eng7"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_mdl_eng7()
            allFields["cfgforce_mdlbuff2_eng6"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_mdlbuff2_eng6()
            allFields["cfgforce_mdlbuff1_eng6"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_mdlbuff1_eng6()
            allFields["cfgforce_mdl_eng6"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_mdl_eng6()
            allFields["cfgforce_mdlbuff2_eng5"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_mdlbuff2_eng5()
            allFields["cfgforce_mdlbuff1_eng5"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_mdlbuff1_eng5()
            allFields["cfgforce_mdl_eng5"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_mdl_eng5()
            allFields["cfgforce_mdlbuff2_eng4"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_mdlbuff2_eng4()
            allFields["cfgforce_mdlbuff1_eng4"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_mdlbuff1_eng4()
            allFields["cfgforce_mdl_eng4"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_mdl_eng4()
            allFields["cfgforce_mdlbuff2_eng3"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_mdlbuff2_eng3()
            allFields["cfgforce_mdlbuff1_eng3"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_mdlbuff1_eng3()
            allFields["cfgforce_mdl_eng3"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_mdl_eng3()
            allFields["cfgforce_mdlbuff2_eng2"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_mdlbuff2_eng2()
            allFields["cfgforce_mdlbuff1_eng2"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_mdlbuff1_eng2()
            allFields["cfgforce_mdl_eng2"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_mdl_eng2()
            allFields["cfgforce_mdlbuff2_eng1"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_mdlbuff2_eng1()
            allFields["cfgforce_mdlbuff1_eng1"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_mdlbuff1_eng1()
            allFields["cfgforce_mdl_eng1"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_mdl_eng1()
            allFields["cfgforce_lbprm"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_lbprm()
            allFields["cfgforce_prm"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_force._cfgforce_prm()
            return allFields

    class _upen_txcfg_dis(AtRegister.AtRegister):
        def name(self):
            return "TX CONFIG DIS PARITY"
    
        def description(self):
            return "config control DIS PARITY"
            
        def width(self):
            return 26
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00006c01
            
        def endAddress(self):
            return 0xffffffff

        class _cfgdis_mdlbuff2_eng8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "cfgdis_mdlbuff2_eng8"
            
            def description(self):
                return "dis error MDL buffer 2, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_mdlbuff1_eng8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "cfgdis_mdlbuff1_eng8"
            
            def description(self):
                return "dis error MDL buffer 1, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_mdl_eng8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "cfgdis_mdl_eng8"
            
            def description(self):
                return "dis error MDL config, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_mdlbuff2_eng7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "cfgdis_mdlbuff2_eng7"
            
            def description(self):
                return "dis error MDL buffer 2, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_mdlbuff1_eng7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "cfgdis_mdlbuff1_eng7"
            
            def description(self):
                return "dis error MDL buffer 1, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_mdl_eng7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "cfgdis_mdl_eng7"
            
            def description(self):
                return "dis error MDL config, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_mdlbuff2_eng6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "cfgdis_mdlbuff2_eng6"
            
            def description(self):
                return "dis error MDL buffer 2, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_mdlbuff1_eng6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "cfgdis_mdlbuff1_eng6"
            
            def description(self):
                return "dis error MDL buffer 1, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_mdl_eng6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "cfgdis_mdl_eng6"
            
            def description(self):
                return "dis error MDL config, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_mdlbuff2_eng5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "cfgdis_mdlbuff2_eng5"
            
            def description(self):
                return "dis error MDL buffer 2, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_mdlbuff1_eng5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "cfgdis_mdlbuff1_eng5"
            
            def description(self):
                return "dis error MDL buffer 1, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_mdl_eng5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "cfgdis_mdl_eng5"
            
            def description(self):
                return "dis error MDL config, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_mdlbuff2_eng4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "cfgdis_mdlbuff2_eng4"
            
            def description(self):
                return "dis error MDL buffer 2, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_mdlbuff1_eng4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "cfgdis_mdlbuff1_eng4"
            
            def description(self):
                return "dis error MDL buffer 1, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_mdl_eng4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "cfgdis_mdl_eng4"
            
            def description(self):
                return "dis error MDL config, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_mdlbuff2_eng3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "cfgdis_mdlbuff2_eng3"
            
            def description(self):
                return "dis error MDL buffer 2, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_mdlbuff1_eng3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "cfgdis_mdlbuff1_eng3"
            
            def description(self):
                return "dis error MDL buffer 1, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_mdl_eng3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "cfgdis_mdl_eng3"
            
            def description(self):
                return "dis error MDL config, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_mdlbuff2_eng2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "cfgdis_mdlbuff2_eng2"
            
            def description(self):
                return "dis error MDL buffer 2, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_mdlbuff1_eng2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "cfgdis_mdlbuff1_eng2"
            
            def description(self):
                return "dis error MDL buffer 1, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_mdl_eng2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "cfgdis_mdl_eng2"
            
            def description(self):
                return "dis error MDL config, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_mdlbuff2_eng1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "cfgdis_mdlbuff2_eng1"
            
            def description(self):
                return "dis error MDL buffer 2, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_mdlbuff1_eng1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "cfgdis_mdlbuff1_eng1"
            
            def description(self):
                return "dis error MDL buffer 1, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_mdl_eng1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfgdis_mdl_eng1"
            
            def description(self):
                return "dis error MDL config, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_lbprm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfgdis_lbprm"
            
            def description(self):
                return "dis error PRM LB config, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfgdis_prm"
            
            def description(self):
                return "dis error PRM config, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfgdis_mdlbuff2_eng8"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_mdlbuff2_eng8()
            allFields["cfgdis_mdlbuff1_eng8"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_mdlbuff1_eng8()
            allFields["cfgdis_mdl_eng8"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_mdl_eng8()
            allFields["cfgdis_mdlbuff2_eng7"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_mdlbuff2_eng7()
            allFields["cfgdis_mdlbuff1_eng7"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_mdlbuff1_eng7()
            allFields["cfgdis_mdl_eng7"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_mdl_eng7()
            allFields["cfgdis_mdlbuff2_eng6"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_mdlbuff2_eng6()
            allFields["cfgdis_mdlbuff1_eng6"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_mdlbuff1_eng6()
            allFields["cfgdis_mdl_eng6"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_mdl_eng6()
            allFields["cfgdis_mdlbuff2_eng5"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_mdlbuff2_eng5()
            allFields["cfgdis_mdlbuff1_eng5"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_mdlbuff1_eng5()
            allFields["cfgdis_mdl_eng5"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_mdl_eng5()
            allFields["cfgdis_mdlbuff2_eng4"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_mdlbuff2_eng4()
            allFields["cfgdis_mdlbuff1_eng4"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_mdlbuff1_eng4()
            allFields["cfgdis_mdl_eng4"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_mdl_eng4()
            allFields["cfgdis_mdlbuff2_eng3"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_mdlbuff2_eng3()
            allFields["cfgdis_mdlbuff1_eng3"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_mdlbuff1_eng3()
            allFields["cfgdis_mdl_eng3"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_mdl_eng3()
            allFields["cfgdis_mdlbuff2_eng2"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_mdlbuff2_eng2()
            allFields["cfgdis_mdlbuff1_eng2"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_mdlbuff1_eng2()
            allFields["cfgdis_mdl_eng2"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_mdl_eng2()
            allFields["cfgdis_mdlbuff2_eng1"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_mdlbuff2_eng1()
            allFields["cfgdis_mdlbuff1_eng1"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_mdlbuff1_eng1()
            allFields["cfgdis_mdl_eng1"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_mdl_eng1()
            allFields["cfgdis_lbprm"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_lbprm()
            allFields["cfgdis_prm"] = _AF6CCI0011_RD_PDH_PRM._upen_txcfg_dis._cfgdis_prm()
            return allFields

    class _upen_txsticky_par(AtRegister.AtRegister):
        def name(self):
            return "TX STICKY PARITY"
    
        def description(self):
            return "config control DeStuff global"
            
        def width(self):
            return 26
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00006c02
            
        def endAddress(self):
            return 0xffffffff

        class _stk_mdlbuff2_eng8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "stk_mdlbuff2_eng8"
            
            def description(self):
                return "dis error MDL buffer 2, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_mdlbuff1_eng8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "stk_mdlbuff1_eng8"
            
            def description(self):
                return "dis error MDL buffer 1, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_mdl_eng8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "stk_mdl_eng8"
            
            def description(self):
                return "dis error MDL config, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_mdlbuff2_eng7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 22
        
            def name(self):
                return "stk_mdlbuff2_eng7"
            
            def description(self):
                return "dis error MDL buffer 2, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_mdlbuff1_eng7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 21
        
            def name(self):
                return "stk_mdlbuff1_eng7"
            
            def description(self):
                return "dis error MDL buffer 1, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_mdl_eng7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 20
                
            def startBit(self):
                return 20
        
            def name(self):
                return "stk_mdl_eng7"
            
            def description(self):
                return "dis error MDL config, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_mdlbuff2_eng6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 19
        
            def name(self):
                return "stk_mdlbuff2_eng6"
            
            def description(self):
                return "dis error MDL buffer 2, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_mdlbuff1_eng6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 18
        
            def name(self):
                return "stk_mdlbuff1_eng6"
            
            def description(self):
                return "dis error MDL buffer 1, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_mdl_eng6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "stk_mdl_eng6"
            
            def description(self):
                return "dis error MDL config, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_mdlbuff2_eng5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "stk_mdlbuff2_eng5"
            
            def description(self):
                return "dis error MDL buffer 2, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_mdlbuff1_eng5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "stk_mdlbuff1_eng5"
            
            def description(self):
                return "dis error MDL buffer 1, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_mdl_eng5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 14
        
            def name(self):
                return "stk_mdl_eng5"
            
            def description(self):
                return "dis error MDL config, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_mdlbuff2_eng4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 13
        
            def name(self):
                return "stk_mdlbuff2_eng4"
            
            def description(self):
                return "dis error MDL buffer 2, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_mdlbuff1_eng4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "stk_mdlbuff1_eng4"
            
            def description(self):
                return "dis error MDL buffer 1, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_mdl_eng4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "stk_mdl_eng4"
            
            def description(self):
                return "dis error MDL config, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_mdlbuff2_eng3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 10
        
            def name(self):
                return "stk_mdlbuff2_eng3"
            
            def description(self):
                return "dis error MDL buffer 2, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_mdlbuff1_eng3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "stk_mdlbuff1_eng3"
            
            def description(self):
                return "dis error MDL buffer 1, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_mdl_eng3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "stk_mdl_eng3"
            
            def description(self):
                return "dis error MDL config, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_mdlbuff2_eng2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "stk_mdlbuff2_eng2"
            
            def description(self):
                return "dis error MDL buffer 2, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_mdlbuff1_eng2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "stk_mdlbuff1_eng2"
            
            def description(self):
                return "dis error MDL buffer 1, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_mdl_eng2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "stk_mdl_eng2"
            
            def description(self):
                return "dis error MDL config, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_mdlbuff2_eng1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "stk_mdlbuff2_eng1"
            
            def description(self):
                return "dis error MDL buffer 2, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_mdlbuff1_eng1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "stk_mdlbuff1_eng1"
            
            def description(self):
                return "dis error MDL buffer 1, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_mdl_eng1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "stk_mdl_eng1"
            
            def description(self):
                return "dis error MDL config, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_lbprm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "stk_lbprm"
            
            def description(self):
                return "dis error PRM LB config, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stk_prm"
            
            def description(self):
                return "dis error PRM config, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["stk_mdlbuff2_eng8"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_mdlbuff2_eng8()
            allFields["stk_mdlbuff1_eng8"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_mdlbuff1_eng8()
            allFields["stk_mdl_eng8"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_mdl_eng8()
            allFields["stk_mdlbuff2_eng7"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_mdlbuff2_eng7()
            allFields["stk_mdlbuff1_eng7"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_mdlbuff1_eng7()
            allFields["stk_mdl_eng7"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_mdl_eng7()
            allFields["stk_mdlbuff2_eng6"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_mdlbuff2_eng6()
            allFields["stk_mdlbuff1_eng6"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_mdlbuff1_eng6()
            allFields["stk_mdl_eng6"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_mdl_eng6()
            allFields["stk_mdlbuff2_eng5"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_mdlbuff2_eng5()
            allFields["stk_mdlbuff1_eng5"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_mdlbuff1_eng5()
            allFields["stk_mdl_eng5"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_mdl_eng5()
            allFields["stk_mdlbuff2_eng4"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_mdlbuff2_eng4()
            allFields["stk_mdlbuff1_eng4"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_mdlbuff1_eng4()
            allFields["stk_mdl_eng4"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_mdl_eng4()
            allFields["stk_mdlbuff2_eng3"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_mdlbuff2_eng3()
            allFields["stk_mdlbuff1_eng3"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_mdlbuff1_eng3()
            allFields["stk_mdl_eng3"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_mdl_eng3()
            allFields["stk_mdlbuff2_eng2"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_mdlbuff2_eng2()
            allFields["stk_mdlbuff1_eng2"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_mdlbuff1_eng2()
            allFields["stk_mdl_eng2"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_mdl_eng2()
            allFields["stk_mdlbuff2_eng1"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_mdlbuff2_eng1()
            allFields["stk_mdlbuff1_eng1"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_mdlbuff1_eng1()
            allFields["stk_mdl_eng1"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_mdl_eng1()
            allFields["stk_lbprm"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_lbprm()
            allFields["stk_prm"] = _AF6CCI0011_RD_PDH_PRM._upen_txsticky_par._stk_prm()
            return allFields

    class _upen_prm_txcfgcr(AtRegister.AtRegister):
        def name(self):
            return "CONFIG PRM BIT C/R & STANDARD Tx"
    
        def description(self):
            return "Config PRM bit C/R & Standard"
            
        def width(self):
            return 4
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x08000+$STSID[4:3]*7 + $VTGID*256 + $SLICEID*32 + $STSID[2:0]*4 + $VTID"
            
        def startAddress(self):
            return 0x00008000
            
        def endAddress(self):
            return 0x000094ff

        class _cfg_prmen_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "cfg_prmen_tx"
            
            def description(self):
                return "config enable Tx, (0) is disable (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_prmfcs_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfg_prmfcs_tx"
            
            def description(self):
                return "config mode transmit FCS, (1) is T.403-MSB, (0) is T.107-LSB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_prmstd_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_prmstd_tx"
            
            def description(self):
                return "config standard Tx, (0) is ANSI, (1) is AT&T"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_prm_cr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_prm_cr"
            
            def description(self):
                return "config bit command/respond PRM message"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_prmen_tx"] = _AF6CCI0011_RD_PDH_PRM._upen_prm_txcfgcr._cfg_prmen_tx()
            allFields["cfg_prmfcs_tx"] = _AF6CCI0011_RD_PDH_PRM._upen_prm_txcfgcr._cfg_prmfcs_tx()
            allFields["cfg_prmstd_tx"] = _AF6CCI0011_RD_PDH_PRM._upen_prm_txcfgcr._cfg_prmstd_tx()
            allFields["cfg_prm_cr"] = _AF6CCI0011_RD_PDH_PRM._upen_prm_txcfgcr._cfg_prm_cr()
            return allFields

    class _upen_prm_txcfglb(AtRegister.AtRegister):
        def name(self):
            return "CONFIG PRM BIT L/B Tx"
    
        def description(self):
            return "CONFIG PRM BIT L/B Tx"
            
        def width(self):
            return 2
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0A000+$SLICEID*1024 + $STSID*32 + $VTGID*4 + $VTID"
            
        def startAddress(self):
            return 0x0000a000
            
        def endAddress(self):
            return 0x0000b4ff

        class _cfg_prm_enlb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_prm_enlb"
            
            def description(self):
                return "config enable CPU config LB bit, (0) is disable, (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_prm_lb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_prm_lb"
            
            def description(self):
                return "config bit Loopback PRM message"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_prm_enlb"] = _AF6CCI0011_RD_PDH_PRM._upen_prm_txcfglb._cfg_prm_enlb()
            allFields["cfg_prm_lb"] = _AF6CCI0011_RD_PDH_PRM._upen_prm_txcfglb._cfg_prm_lb()
            return allFields

    class _upen_txprm_cnt_byte(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE TX PRM"
    
        def description(self):
            return "counter byte message PRM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x10000+$STSID[4:3]*7 + $VTGID*256 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 8192"
            
        def startAddress(self):
            return 0x00010000
            
        def endAddress(self):
            return 0x000134ff

        class _cnt_byte_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_byte_prm"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_byte_prm"] = _AF6CCI0011_RD_PDH_PRM._upen_txprm_cnt_byte._cnt_byte_prm()
            return allFields

    class _upen_txprm_cnt_pkt(AtRegister.AtRegister):
        def name(self):
            return "COUNTER PACKET MESSAGE TX PRM"
    
        def description(self):
            return "counter packet message PRM"
            
        def width(self):
            return 15
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x14000+$STSID[4:3]*7 + $VTGID*256 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 8192"
            
        def startAddress(self):
            return 0x00014000
            
        def endAddress(self):
            return 0x000174ff

        class _cnt_pkt_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_pkt_prm"
            
            def description(self):
                return "value counter packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_pkt_prm"] = _AF6CCI0011_RD_PDH_PRM._upen_txprm_cnt_pkt._cnt_pkt_prm()
            return allFields

    class _upen_txprm_cnt_info(AtRegister.AtRegister):
        def name(self):
            return "COUNTER VALID INFO TX PRM"
    
        def description(self):
            return "counter valid info PRM"
            
        def width(self):
            return 15
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x18000+$STSID[4:3]*7 + $VTGID*256 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 8192"
            
        def startAddress(self):
            return 0x00018000
            
        def endAddress(self):
            return 0x0001b4ff

        class _cnt_vld_prm_info(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_vld_prm_info"
            
            def description(self):
                return "value counter valid info"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_vld_prm_info"] = _AF6CCI0011_RD_PDH_PRM._upen_txprm_cnt_info._cnt_vld_prm_info()
            return allFields

    class _upen_rxprm_cfgstd(AtRegister.AtRegister):
        def name(self):
            return "Config Buff Message PRM STANDARD"
    
        def description(self):
            return "config standard message PRM"
            
        def width(self):
            return 3
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x20000+$STSID[4:3]*7 + $VTGID*256 + $SLICEID*32 + $STSID[2:0]*4 + $VTID"
            
        def startAddress(self):
            return 0x00020000
            
        def endAddress(self):
            return 0x000214ff

        class _cfg_prm_cr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfg_prm_cr"
            
            def description(self):
                return "config C/R bit expected"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_prmfcs_rx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_prmfcs_rx"
            
            def description(self):
                return "config mode receive FCS, (1) is T.403-MSB, (0) is T.107-LSB"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_std_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_std_prm"
            
            def description(self):
                return "config standard   (0) is ANSI, (1) is AT&T"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_prm_cr"] = _AF6CCI0011_RD_PDH_PRM._upen_rxprm_cfgstd._cfg_prm_cr()
            allFields["cfg_prmfcs_rx"] = _AF6CCI0011_RD_PDH_PRM._upen_rxprm_cfgstd._cfg_prmfcs_rx()
            allFields["cfg_std_prm"] = _AF6CCI0011_RD_PDH_PRM._upen_rxprm_cfgstd._cfg_std_prm()
            return allFields

    class _upen_destuff_ctrl0(AtRegister.AtRegister):
        def name(self):
            return "CONFIG CONTROL DESTUFF 0"
    
        def description(self):
            return "config control DeStuff global"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00023200
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_crmon(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "cfg_crmon"
            
            def description(self):
                return "(0) : disable, (1) : enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "reserve1"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fcsmon(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "fcsmon"
            
            def description(self):
                return "(0) : disable monitor, (1) : enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 1
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _headermon_en(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "headermon_en"
            
            def description(self):
                return "(1) : enable monitor, (0) disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_crmon"] = _AF6CCI0011_RD_PDH_PRM._upen_destuff_ctrl0._cfg_crmon()
            allFields["reserve1"] = _AF6CCI0011_RD_PDH_PRM._upen_destuff_ctrl0._reserve1()
            allFields["fcsmon"] = _AF6CCI0011_RD_PDH_PRM._upen_destuff_ctrl0._fcsmon()
            allFields["reserve"] = _AF6CCI0011_RD_PDH_PRM._upen_destuff_ctrl0._reserve()
            allFields["headermon_en"] = _AF6CCI0011_RD_PDH_PRM._upen_destuff_ctrl0._headermon_en()
            return allFields

    class _upen_rxprm_gmess(AtRegister.AtRegister):
        def name(self):
            return "COUNTER GOOD MESSAGE RX PRM"
    
        def description(self):
            return "counter good message PRM"
            
        def width(self):
            return 15
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x30000+$STSID[4:3]*7 + $VTGID*256 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 8192"
            
        def startAddress(self):
            return 0x00030000
            
        def endAddress(self):
            return 0x000334ff

        class _cnt_gmess_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_gmess_prm"
            
            def description(self):
                return "value counter packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_gmess_prm"] = _AF6CCI0011_RD_PDH_PRM._upen_rxprm_gmess._cnt_gmess_prm()
            return allFields

    class _upen_rxprm_drmess(AtRegister.AtRegister):
        def name(self):
            return "COUNTER DROP MESSAGE RX PRM"
    
        def description(self):
            return "counter drop message PRM"
            
        def width(self):
            return 15
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x34000+$STSID[4:3]*7 + $VTGID*256 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 8192"
            
        def startAddress(self):
            return 0x00034000
            
        def endAddress(self):
            return 0x000374ff

        class _cnt_drmess_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_drmess_prm"
            
            def description(self):
                return "value counter packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_drmess_prm"] = _AF6CCI0011_RD_PDH_PRM._upen_rxprm_drmess._cnt_drmess_prm()
            return allFields

    class _upen_rxprm_mmess(AtRegister.AtRegister):
        def name(self):
            return "COUNTER MISS MESSAGE RX PRM"
    
        def description(self):
            return "counter miss message PRM"
            
        def width(self):
            return 15
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x38000+$STSID[4:3]*7 + $VTGID*256 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 8192"
            
        def startAddress(self):
            return 0x00038000
            
        def endAddress(self):
            return 0x0003b4ff

        class _cnt_mmess_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_mmess_prm"
            
            def description(self):
                return "value counter packet"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_mmess_prm"] = _AF6CCI0011_RD_PDH_PRM._upen_rxprm_mmess._cnt_mmess_prm()
            return allFields

    class _upen_rxprm_cnt_byte(AtRegister.AtRegister):
        def name(self):
            return "COUNTER BYTE MESSAGE RX PRM"
    
        def description(self):
            return "counter byte message PRM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x3C000+$STSID[4:3]*7 + $VTGID*256 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 8192"
            
        def startAddress(self):
            return 0x0003c000
            
        def endAddress(self):
            return 0x0003f4ff

        class _cnt_byte_rxprm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cnt_byte_rxprm"
            
            def description(self):
                return "value counter byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cnt_byte_rxprm"] = _AF6CCI0011_RD_PDH_PRM._upen_rxprm_cnt_byte._cnt_byte_rxprm()
            return allFields

    class _upen_rxcfg_force(AtRegister.AtRegister):
        def name(self):
            return "RX CONFIG FORCE ERROR PARITY"
    
        def description(self):
            return "config control DeStuff global"
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00023600
            
        def endAddress(self):
            return 0xffffffff

        class _cfgforce_rxmdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfgforce_rxmdl"
            
            def description(self):
                return "force error MDL config, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgforce_rxprm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfgforce_rxprm"
            
            def description(self):
                return "force error PRM config, (0) : disable, (1) : enable force"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfgforce_rxmdl"] = _AF6CCI0011_RD_PDH_PRM._upen_rxcfg_force._cfgforce_rxmdl()
            allFields["cfgforce_rxprm"] = _AF6CCI0011_RD_PDH_PRM._upen_rxcfg_force._cfgforce_rxprm()
            return allFields

    class _upen_rxcfg_dis(AtRegister.AtRegister):
        def name(self):
            return "RX CONFIG DIS PARITY"
    
        def description(self):
            return "config control DeStuff global"
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00023601
            
        def endAddress(self):
            return 0xffffffff

        class _cfgdis_rxmdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfgdis_rxmdl"
            
            def description(self):
                return "dis error MDL config, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfgdis_rxprm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfgdis_rxprm"
            
            def description(self):
                return "dis error PRM config, (0) : disable, (1) : enable dis"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfgdis_rxmdl"] = _AF6CCI0011_RD_PDH_PRM._upen_rxcfg_dis._cfgdis_rxmdl()
            allFields["cfgdis_rxprm"] = _AF6CCI0011_RD_PDH_PRM._upen_rxcfg_dis._cfgdis_rxprm()
            return allFields

    class _upen_rxsticky_par(AtRegister.AtRegister):
        def name(self):
            return "RX STICKY PARITY"
    
        def description(self):
            return "config control DeStuff global"
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00023602
            
        def endAddress(self):
            return 0xffffffff

        class _stk_rxmdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "stk_rxmdl"
            
            def description(self):
                return "sticky for mdl config error parity"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_rxprm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stk_rxprm"
            
            def description(self):
                return "sticky for prm config error parity"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["stk_rxmdl"] = _AF6CCI0011_RD_PDH_PRM._upen_rxsticky_par._stk_rxmdl()
            allFields["stk_rxprm"] = _AF6CCI0011_RD_PDH_PRM._upen_rxsticky_par._stk_rxprm()
            return allFields

    class _prm_cfg_lben_int(AtRegister.AtRegister):
        def name(self):
            return "PRM LB per Channel Interrupt Enable Control"
    
        def description(self):
            return "This is the per Channel interrupt enable"
            
        def width(self):
            return 8
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x28000+ $STSID*32 + $VTGID*4 + $VTID"
            
        def startAddress(self):
            return 0x00028000
            
        def endAddress(self):
            return 0x000283ff

        class _prm_cfglben_int7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "prm_cfglben_int7"
            
            def description(self):
                return "Set 1 to enable change event to generate an interrupt slice 7"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_cfglben_int6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "prm_cfglben_int6"
            
            def description(self):
                return "Set 1 to enable change event to generate an interrupt slice 6"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_cfglben_int5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "prm_cfglben_int5"
            
            def description(self):
                return "Set 1 to enable change event to generate an interrupt slice 5"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_cfglben_int4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "prm_cfglben_int4"
            
            def description(self):
                return "Set 1 to enable change event to generate an interrupt slice 4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_cfglben_int3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "prm_cfglben_int3"
            
            def description(self):
                return "Set 1 to enable change event to generate an interrupt slice 3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_cfglben_int2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "prm_cfglben_int2"
            
            def description(self):
                return "Set 1 to enable change event to generate an interrupt slice 2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_cfglben_int1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "prm_cfglben_int1"
            
            def description(self):
                return "Set 1 to enable change event to generate an interrupt slice 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_cfglben_int0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_cfglben_int0"
            
            def description(self):
                return "Set 1 to enable change event to generate an interrupt slice 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_cfglben_int7"] = _AF6CCI0011_RD_PDH_PRM._prm_cfg_lben_int._prm_cfglben_int7()
            allFields["prm_cfglben_int6"] = _AF6CCI0011_RD_PDH_PRM._prm_cfg_lben_int._prm_cfglben_int6()
            allFields["prm_cfglben_int5"] = _AF6CCI0011_RD_PDH_PRM._prm_cfg_lben_int._prm_cfglben_int5()
            allFields["prm_cfglben_int4"] = _AF6CCI0011_RD_PDH_PRM._prm_cfg_lben_int._prm_cfglben_int4()
            allFields["prm_cfglben_int3"] = _AF6CCI0011_RD_PDH_PRM._prm_cfg_lben_int._prm_cfglben_int3()
            allFields["prm_cfglben_int2"] = _AF6CCI0011_RD_PDH_PRM._prm_cfg_lben_int._prm_cfglben_int2()
            allFields["prm_cfglben_int1"] = _AF6CCI0011_RD_PDH_PRM._prm_cfg_lben_int._prm_cfglben_int1()
            allFields["prm_cfglben_int0"] = _AF6CCI0011_RD_PDH_PRM._prm_cfg_lben_int._prm_cfglben_int0()
            return allFields

    class _prm_lb_int_sta(AtRegister.AtRegister):
        def name(self):
            return "PRM LB Interrupt per Channel Interrupt Status"
    
        def description(self):
            return "This is the per Channel interrupt status."
            
        def width(self):
            return 8
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x28400+ $STSID*32 + $VTGID*4 + $VTID"
            
        def startAddress(self):
            return 0x00028400
            
        def endAddress(self):
            return 0x000287ff

        class _prm_lbint_sta7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "prm_lbint_sta7"
            
            def description(self):
                return "Set 1 if there is a change event slice 7"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_lbint_sta6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "prm_lbint_sta6"
            
            def description(self):
                return "Set 1 if there is a change event slice 6"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_lbint_sta5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "prm_lbint_sta5"
            
            def description(self):
                return "Set 1 if there is a change event slice 5"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_lbint_sta4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "prm_lbint_sta4"
            
            def description(self):
                return "Set 1 if there is a change event slice 4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_lbint_sta3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "prm_lbint_sta3"
            
            def description(self):
                return "Set 1 if there is a change event slice 3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_lbint_sta2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "prm_lbint_sta2"
            
            def description(self):
                return "Set 1 if there is a change event slice 2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_lbint_sta1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "prm_lbint_sta1"
            
            def description(self):
                return "Set 1 if there is a change event slice 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_lbint_sta0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_lbint_sta0"
            
            def description(self):
                return "Set 1 if there is a change event slice 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_lbint_sta7"] = _AF6CCI0011_RD_PDH_PRM._prm_lb_int_sta._prm_lbint_sta7()
            allFields["prm_lbint_sta6"] = _AF6CCI0011_RD_PDH_PRM._prm_lb_int_sta._prm_lbint_sta6()
            allFields["prm_lbint_sta5"] = _AF6CCI0011_RD_PDH_PRM._prm_lb_int_sta._prm_lbint_sta5()
            allFields["prm_lbint_sta4"] = _AF6CCI0011_RD_PDH_PRM._prm_lb_int_sta._prm_lbint_sta4()
            allFields["prm_lbint_sta3"] = _AF6CCI0011_RD_PDH_PRM._prm_lb_int_sta._prm_lbint_sta3()
            allFields["prm_lbint_sta2"] = _AF6CCI0011_RD_PDH_PRM._prm_lb_int_sta._prm_lbint_sta2()
            allFields["prm_lbint_sta1"] = _AF6CCI0011_RD_PDH_PRM._prm_lb_int_sta._prm_lbint_sta1()
            allFields["prm_lbint_sta0"] = _AF6CCI0011_RD_PDH_PRM._prm_lb_int_sta._prm_lbint_sta0()
            return allFields

    class _prm_lb_int_crrsta(AtRegister.AtRegister):
        def name(self):
            return "PRM LB Interrupt per Channel Current Status"
    
        def description(self):
            return "This is the per Channel Current status."
            
        def width(self):
            return 8
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x28800+ $STSID*32 + $VTGID*4 + $VTID"
            
        def startAddress(self):
            return 0x00028800
            
        def endAddress(self):
            return 0x00028bff

        class _prm_lbint_crrsta7(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "prm_lbint_crrsta7"
            
            def description(self):
                return "Current status of event slice 7"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_lbint_crrsta6(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "prm_lbint_crrsta6"
            
            def description(self):
                return "Current status of event slice 6"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_lbint_crrsta5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "prm_lbint_crrsta5"
            
            def description(self):
                return "Current status of event slice 5"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_lbint_crrsta4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "prm_lbint_crrsta4"
            
            def description(self):
                return "Current status of event slice 4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_lbint_crrsta3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "prm_lbint_crrsta3"
            
            def description(self):
                return "Current status of event slice 3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_lbint_crrsta2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "prm_lbint_crrsta2"
            
            def description(self):
                return "Current status of event slice 2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_lbint_crrsta1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "prm_lbint_crrsta1"
            
            def description(self):
                return "Current status of event slice 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _prm_lbint_crrsta0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_lbint_crrsta0"
            
            def description(self):
                return "Current status of event slice 0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_lbint_crrsta7"] = _AF6CCI0011_RD_PDH_PRM._prm_lb_int_crrsta._prm_lbint_crrsta7()
            allFields["prm_lbint_crrsta6"] = _AF6CCI0011_RD_PDH_PRM._prm_lb_int_crrsta._prm_lbint_crrsta6()
            allFields["prm_lbint_crrsta5"] = _AF6CCI0011_RD_PDH_PRM._prm_lb_int_crrsta._prm_lbint_crrsta5()
            allFields["prm_lbint_crrsta4"] = _AF6CCI0011_RD_PDH_PRM._prm_lb_int_crrsta._prm_lbint_crrsta4()
            allFields["prm_lbint_crrsta3"] = _AF6CCI0011_RD_PDH_PRM._prm_lb_int_crrsta._prm_lbint_crrsta3()
            allFields["prm_lbint_crrsta2"] = _AF6CCI0011_RD_PDH_PRM._prm_lb_int_crrsta._prm_lbint_crrsta2()
            allFields["prm_lbint_crrsta1"] = _AF6CCI0011_RD_PDH_PRM._prm_lb_int_crrsta._prm_lbint_crrsta1()
            allFields["prm_lbint_crrsta0"] = _AF6CCI0011_RD_PDH_PRM._prm_lb_int_crrsta._prm_lbint_crrsta0()
            return allFields

    class _prm_lb_intsta(AtRegister.AtRegister):
        def name(self):
            return "PRM LB Interrupt per Channel Interrupt OR Status"
    
        def description(self):
            return ""
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x28C00 +  $GID"
            
        def startAddress(self):
            return 0x00028c00
            
        def endAddress(self):
            return 0x00028c20

        class _prm_lbintsta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_lbintsta"
            
            def description(self):
                return "Set to 1 if any interrupt status bit of corresponding channel is set and its interrupt is enabled."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_lbintsta"] = _AF6CCI0011_RD_PDH_PRM._prm_lb_intsta._prm_lbintsta()
            return allFields

    class _prm_lb_sta_int(AtRegister.AtRegister):
        def name(self):
            return "PRM LB Interrupt OR Status"
    
        def description(self):
            return "The register consists of 2 bits. Each bit is used to store Interrupt OR status."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00028fff
            
        def endAddress(self):
            return 0xffffffff

        class _prm_lbsta_int(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_lbsta_int"
            
            def description(self):
                return "Set to 1 if any interrupt status bit is set and its interrupt is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_lbsta_int"] = _AF6CCI0011_RD_PDH_PRM._prm_lb_sta_int._prm_lbsta_int()
            return allFields

    class _prm_lb_en_int(AtRegister.AtRegister):
        def name(self):
            return "PRM LB Interrupt Enable Control"
    
        def description(self):
            return "The register consists of 2 interrupt enable bits ."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00028ffe
            
        def endAddress(self):
            return 0xffffffff

        class _prm_lben_int(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "prm_lben_int"
            
            def description(self):
                return "Set to 1 to enable to generate interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["prm_lben_int"] = _AF6CCI0011_RD_PDH_PRM._prm_lb_en_int._prm_lben_int()
            return allFields

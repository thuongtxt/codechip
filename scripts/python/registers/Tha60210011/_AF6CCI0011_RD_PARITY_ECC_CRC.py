import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0011_RD_PARITY_ECC_CRC(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["qdr_ecc_force_error_control"] = _AF6CCI0011_RD_PARITY_ECC_CRC._qdr_ecc_force_error_control()
        allRegisters["qdr_ecc_sticky"] = _AF6CCI0011_RD_PARITY_ECC_CRC._qdr_ecc_sticky()
        allRegisters["qdr_ecc_correctable_counter"] = _AF6CCI0011_RD_PARITY_ECC_CRC._qdr_ecc_correctable_counter()
        allRegisters["qdr_ecc_noncorrectable_counter"] = _AF6CCI0011_RD_PARITY_ECC_CRC._qdr_ecc_noncorrectable_counter()
        allRegisters["ddrno1_ecc_force_error_control"] = _AF6CCI0011_RD_PARITY_ECC_CRC._ddrno1_ecc_force_error_control()
        allRegisters["ddrno1_ecc_sticky"] = _AF6CCI0011_RD_PARITY_ECC_CRC._ddrno1_ecc_sticky()
        allRegisters["ddrno1_ecc_correctable_counter"] = _AF6CCI0011_RD_PARITY_ECC_CRC._ddrno1_ecc_correctable_counter()
        allRegisters["ddrno1_ecc_noncorrectable_counter"] = _AF6CCI0011_RD_PARITY_ECC_CRC._ddrno1_ecc_noncorrectable_counter()
        allRegisters["ddrno1_crc_force_error_control"] = _AF6CCI0011_RD_PARITY_ECC_CRC._ddrno1_crc_force_error_control()
        allRegisters["ddrno1_crc_sticky"] = _AF6CCI0011_RD_PARITY_ECC_CRC._ddrno1_crc_sticky()
        allRegisters["ddrno1_crc_counter"] = _AF6CCI0011_RD_PARITY_ECC_CRC._ddrno1_crc_counter()
        allRegisters["ddrno2_crc_force_error_control"] = _AF6CCI0011_RD_PARITY_ECC_CRC._ddrno2_crc_force_error_control()
        allRegisters["ddrno2_crc_sticky"] = _AF6CCI0011_RD_PARITY_ECC_CRC._ddrno2_crc_sticky()
        allRegisters["ddrno2_crc_counter"] = _AF6CCI0011_RD_PARITY_ECC_CRC._ddrno2_crc_counter()
        return allRegisters

    class _qdr_ecc_force_error_control(AtRegister.AtRegister):
        def name(self):
            return "QDR ECC Force Error Control"
    
        def description(self):
            return "This register configures QDR ECC force error."
            
        def width(self):
            return 30
        
        def type(self):
            return "Config|Enable"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00f26000
            
        def endAddress(self):
            return 0xffffffff

        class _QDREccNonCorrectCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "QDREccNonCorrectCfg"
            
            def description(self):
                return "Ecc error mode  1: Non-correctable  0: Correctable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _QDREccErrForeverCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "QDREccErrForeverCfg"
            
            def description(self):
                return "Force ecc error mode  1: forever when field QDREccErrNumber differ zero 0: burst"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _QDREccErrNumberCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "QDREccErrNumberCfg"
            
            def description(self):
                return "number of ECC error inserted to QDR"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["QDREccNonCorrectCfg"] = _AF6CCI0011_RD_PARITY_ECC_CRC._qdr_ecc_force_error_control._QDREccNonCorrectCfg()
            allFields["QDREccErrForeverCfg"] = _AF6CCI0011_RD_PARITY_ECC_CRC._qdr_ecc_force_error_control._QDREccErrForeverCfg()
            allFields["QDREccErrNumberCfg"] = _AF6CCI0011_RD_PARITY_ECC_CRC._qdr_ecc_force_error_control._QDREccErrNumberCfg()
            return allFields

    class _qdr_ecc_sticky(AtRegister.AtRegister):
        def name(self):
            return "QDR ECC Error Sticky"
    
        def description(self):
            return "This register report sticky of QDR ECC error."
            
        def width(self):
            return 2
        
        def type(self):
            return "Config|Enable"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00f26001
            
        def endAddress(self):
            return 0xffffffff

        class _QDREccNonCorrectError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "QDREccNonCorrectError"
            
            def description(self):
                return "QDR non-correctable error"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        class _QDREccCorrectError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "QDREccCorrectError"
            
            def description(self):
                return ""
            
            def type(self):
                return "QDR correctable error"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["QDREccNonCorrectError"] = _AF6CCI0011_RD_PARITY_ECC_CRC._qdr_ecc_sticky._QDREccNonCorrectError()
            allFields["QDREccCorrectError"] = _AF6CCI0011_RD_PARITY_ECC_CRC._qdr_ecc_sticky._QDREccCorrectError()
            return allFields

    class _qdr_ecc_correctable_counter(AtRegister.AtRegister):
        def name(self):
            return "QDR ECC Correctable Error Counter"
    
        def description(self):
            return "This register counts QDR ECC error."
            
        def width(self):
            return 28
        
        def type(self):
            return "Config|Enable"
            
        def fomular(self):
            return "0xF26002 + R2C"
            
        def startAddress(self):
            return 0x00f26002
            
        def endAddress(self):
            return 0xffffffff

        class _QDREccErrorCounter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "QDREccErrorCounter"
            
            def description(self):
                return ""
            
            def type(self):
                return "QDR Ecc error counter"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["QDREccErrorCounter"] = _AF6CCI0011_RD_PARITY_ECC_CRC._qdr_ecc_correctable_counter._QDREccErrorCounter()
            return allFields

    class _qdr_ecc_noncorrectable_counter(AtRegister.AtRegister):
        def name(self):
            return "QDR ECC NonCorrectable Error Counter"
    
        def description(self):
            return "This register counts QDR ECC error."
            
        def width(self):
            return 28
        
        def type(self):
            return "Config|Enable"
            
        def fomular(self):
            return "0xF26004 + R2C"
            
        def startAddress(self):
            return 0x00f26004
            
        def endAddress(self):
            return 0xffffffff

        class _QDREccErrorCounter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "QDREccErrorCounter"
            
            def description(self):
                return ""
            
            def type(self):
                return "QDR Ecc error counter"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["QDREccErrorCounter"] = _AF6CCI0011_RD_PARITY_ECC_CRC._qdr_ecc_noncorrectable_counter._QDREccErrorCounter()
            return allFields

    class _ddrno1_ecc_force_error_control(AtRegister.AtRegister):
        def name(self):
            return "DDRno1 PDA ECC Force Error Control"
    
        def description(self):
            return "This register configures DDRno1 ECC force error."
            
        def width(self):
            return 30
        
        def type(self):
            return "Config|Enable"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00500020
            
        def endAddress(self):
            return 0xffffffff

        class _DDRno1EccNonCorrectCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 29
        
            def name(self):
                return "DDRno1EccNonCorrectCfg"
            
            def description(self):
                return "Ecc error mode  1: Non-correctable  0: Correctable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DDRno1EccErrForeverCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "DDRno1EccErrForeverCfg"
            
            def description(self):
                return "Force ecc error mode  1: forever when field DDRno1EccErrNumber differ zero 0: burst"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DDRno1EccErrNumberCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DDRno1EccErrNumberCfg"
            
            def description(self):
                return "number of ECC error inserted to DDRno1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DDRno1EccNonCorrectCfg"] = _AF6CCI0011_RD_PARITY_ECC_CRC._ddrno1_ecc_force_error_control._DDRno1EccNonCorrectCfg()
            allFields["DDRno1EccErrForeverCfg"] = _AF6CCI0011_RD_PARITY_ECC_CRC._ddrno1_ecc_force_error_control._DDRno1EccErrForeverCfg()
            allFields["DDRno1EccErrNumberCfg"] = _AF6CCI0011_RD_PARITY_ECC_CRC._ddrno1_ecc_force_error_control._DDRno1EccErrNumberCfg()
            return allFields

    class _ddrno1_ecc_sticky(AtRegister.AtRegister):
        def name(self):
            return "DDRno1 PDA ECC Error Sticky"
    
        def description(self):
            return "This register report sticky of DDRno1 ECC error."
            
        def width(self):
            return 2
        
        def type(self):
            return "Config|Enable"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00500021
            
        def endAddress(self):
            return 0xffffffff

        class _DDRno1EccNonCorrectError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "DDRno1EccNonCorrectError"
            
            def description(self):
                return "DDRno1 non-correctable error"
            
            def type(self):
                return "WC"
            
            def resetValue(self):
                return 0xffffffff

        class _DDRno1EccCorrectError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DDRno1EccCorrectError"
            
            def description(self):
                return ""
            
            def type(self):
                return "DDRno1 correctable error"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DDRno1EccNonCorrectError"] = _AF6CCI0011_RD_PARITY_ECC_CRC._ddrno1_ecc_sticky._DDRno1EccNonCorrectError()
            allFields["DDRno1EccCorrectError"] = _AF6CCI0011_RD_PARITY_ECC_CRC._ddrno1_ecc_sticky._DDRno1EccCorrectError()
            return allFields

    class _ddrno1_ecc_correctable_counter(AtRegister.AtRegister):
        def name(self):
            return "DDRno1 PDA ECC Correctable Error Counter"
    
        def description(self):
            return "This register counts DDRno1 ECC error."
            
        def width(self):
            return 28
        
        def type(self):
            return "Config|Enable"
            
        def fomular(self):
            return "0x500022 + R2C"
            
        def startAddress(self):
            return 0x00500022
            
        def endAddress(self):
            return 0xffffffff

        class _DDRno1EccErrorCounter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DDRno1EccErrorCounter"
            
            def description(self):
                return ""
            
            def type(self):
                return "DDRno1 Ecc error counter"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DDRno1EccErrorCounter"] = _AF6CCI0011_RD_PARITY_ECC_CRC._ddrno1_ecc_correctable_counter._DDRno1EccErrorCounter()
            return allFields

    class _ddrno1_ecc_noncorrectable_counter(AtRegister.AtRegister):
        def name(self):
            return "DDRno1 PDA ECC NonCorrectable Error Counter"
    
        def description(self):
            return "This register counts DDRno1 ECC error."
            
        def width(self):
            return 28
        
        def type(self):
            return "Config|Enable"
            
        def fomular(self):
            return "0x500024 + R2C"
            
        def startAddress(self):
            return 0x00500024
            
        def endAddress(self):
            return 0xffffffff

        class _DDRno1EccErrorCounter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DDRno1EccErrorCounter"
            
            def description(self):
                return ""
            
            def type(self):
                return "DDRno1 Ecc error counter"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DDRno1EccErrorCounter"] = _AF6CCI0011_RD_PARITY_ECC_CRC._ddrno1_ecc_noncorrectable_counter._DDRno1EccErrorCounter()
            return allFields

    class _ddrno1_crc_force_error_control(AtRegister.AtRegister):
        def name(self):
            return "DDRno1 PLA CRC Force Error Control"
    
        def description(self):
            return "This register configures DDRno1 PLA CRC force error."
            
        def width(self):
            return 29
        
        def type(self):
            return "Config|Enable"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00488050
            
        def endAddress(self):
            return 0xffffffff

        class _DDRno1CrcErrForeverCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "DDRno1CrcErrForeverCfg"
            
            def description(self):
                return "Force crc error mode  1: forever when field DDRno1CrcErrNumber differ zero 0: burst"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DDRno1CrcErrNumberCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DDRno1CrcErrNumberCfg"
            
            def description(self):
                return "number of CRC error inserted to DDRno1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DDRno1CrcErrForeverCfg"] = _AF6CCI0011_RD_PARITY_ECC_CRC._ddrno1_crc_force_error_control._DDRno1CrcErrForeverCfg()
            allFields["DDRno1CrcErrNumberCfg"] = _AF6CCI0011_RD_PARITY_ECC_CRC._ddrno1_crc_force_error_control._DDRno1CrcErrNumberCfg()
            return allFields

    class _ddrno1_crc_sticky(AtRegister.AtRegister):
        def name(self):
            return "DDRno1 PLA CRC Error Sticky"
    
        def description(self):
            return "This register report sticky of DDRno1 CRC error."
            
        def width(self):
            return 1
        
        def type(self):
            return "Config|Enable"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00488051
            
        def endAddress(self):
            return 0xffffffff

        class _DDRno1CrcError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DDRno1CrcError"
            
            def description(self):
                return ""
            
            def type(self):
                return "DDRno1 CRC error"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DDRno1CrcError"] = _AF6CCI0011_RD_PARITY_ECC_CRC._ddrno1_crc_sticky._DDRno1CrcError()
            return allFields

    class _ddrno1_crc_counter(AtRegister.AtRegister):
        def name(self):
            return "DDRno1 CRC Error Counter"
    
        def description(self):
            return "This register counts DDRno1 CRC error."
            
        def width(self):
            return 28
        
        def type(self):
            return "Config|Enable"
            
        def fomular(self):
            return "0x488052 + R2C"
            
        def startAddress(self):
            return 0x00488052
            
        def endAddress(self):
            return 0xffffffff

        class _DDRno1CrcErrorCounter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DDRno1CrcErrorCounter"
            
            def description(self):
                return ""
            
            def type(self):
                return "DDRno1 Crc error counter"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DDRno1CrcErrorCounter"] = _AF6CCI0011_RD_PARITY_ECC_CRC._ddrno1_crc_counter._DDRno1CrcErrorCounter()
            return allFields

    class _ddrno2_crc_force_error_control(AtRegister.AtRegister):
        def name(self):
            return "DDRno2 PDA CRC Force Error Control"
    
        def description(self):
            return "This register configures DDRno2 PDA CRC force error."
            
        def width(self):
            return 29
        
        def type(self):
            return "Config|Enable"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00500030
            
        def endAddress(self):
            return 0xffffffff

        class _DDRno2CrcErrForeverCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 28
                
            def startBit(self):
                return 28
        
            def name(self):
                return "DDRno2CrcErrForeverCfg"
            
            def description(self):
                return "Force crc error mode  1: forever when field DDRno2CrcErrNumber differ zero 0: burst"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DDRno2CrcErrNumberCfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DDRno2CrcErrNumberCfg"
            
            def description(self):
                return "number of CRC error inserted to DDRno2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DDRno2CrcErrForeverCfg"] = _AF6CCI0011_RD_PARITY_ECC_CRC._ddrno2_crc_force_error_control._DDRno2CrcErrForeverCfg()
            allFields["DDRno2CrcErrNumberCfg"] = _AF6CCI0011_RD_PARITY_ECC_CRC._ddrno2_crc_force_error_control._DDRno2CrcErrNumberCfg()
            return allFields

    class _ddrno2_crc_sticky(AtRegister.AtRegister):
        def name(self):
            return "DDRno2 PDA CRC Error Sticky"
    
        def description(self):
            return "This register report sticky of DDRno2 CRC error."
            
        def width(self):
            return 1
        
        def type(self):
            return "Config|Enable"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00500031
            
        def endAddress(self):
            return 0xffffffff

        class _DDRno2CrcError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DDRno2CrcError"
            
            def description(self):
                return ""
            
            def type(self):
                return "DDRno2 CRC error"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DDRno2CrcError"] = _AF6CCI0011_RD_PARITY_ECC_CRC._ddrno2_crc_sticky._DDRno2CrcError()
            return allFields

    class _ddrno2_crc_counter(AtRegister.AtRegister):
        def name(self):
            return "DDRno2 CRC Error Counter"
    
        def description(self):
            return "This register counts DDRno2 CRC error."
            
        def width(self):
            return 28
        
        def type(self):
            return "Config|Enable"
            
        def fomular(self):
            return "0x500032 + R2C"
            
        def startAddress(self):
            return 0x00500032
            
        def endAddress(self):
            return 0xffffffff

        class _DDRno2CrcErrorCounter(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DDRno2CrcErrorCounter"
            
            def description(self):
                return ""
            
            def type(self):
                return "DDRno2 Crc error counter"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DDRno2CrcErrorCounter"] = _AF6CCI0011_RD_PARITY_ECC_CRC._ddrno2_crc_counter._DDRno2CrcErrorCounter()
            return allFields

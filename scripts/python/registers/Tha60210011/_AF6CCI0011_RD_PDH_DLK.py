import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0011_RD_PDH_DLK(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_dlk_cnv_de1"] = _AF6CCI0011_RD_PDH_DLK._upen_dlk_cnv_de1()
        allRegisters["upen_dlk_cnv_de3"] = _AF6CCI0011_RD_PDH_DLK._upen_dlk_cnv_de3()
        allRegisters["upen_cfg_thres"] = _AF6CCI0011_RD_PDH_DLK._upen_cfg_thres()
        allRegisters["upen_sta_fifo_ddr"] = _AF6CCI0011_RD_PDH_DLK._upen_sta_fifo_ddr()
        allRegisters["upen_stk_alrm_thres"] = _AF6CCI0011_RD_PDH_DLK._upen_stk_alrm_thres()
        allRegisters["upen_stk_alrm_thres_col"] = _AF6CCI0011_RD_PDH_DLK._upen_stk_alrm_thres_col()
        allRegisters["upen_stk_alrm_mess"] = _AF6CCI0011_RD_PDH_DLK._upen_stk_alrm_mess()
        allRegisters["upen_stk_alrm_mess_col"] = _AF6CCI0011_RD_PDH_DLK._upen_stk_alrm_mess_col()
        allRegisters["upen_stk_alrm"] = _AF6CCI0011_RD_PDH_DLK._upen_stk_alrm()
        return allRegisters

    class _upen_dlk_cnv_de1(AtRegister.AtRegister):
        def name(self):
            return "Convert CHID to DLK ID DE1"
    
        def description(self):
            return "Config table convert channel ID to DLK ID"
            
        def width(self):
            return 13
        
        def type(self):
            return "COnfig"
            
        def fomular(self):
            return "0x0000+sliceid<<10+stsid<<5+vtgid<<2+vtid"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x00003fff

        class _dlken(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "dlken"
            
            def description(self):
                return "dlk enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _dlkid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dlkid"
            
            def description(self):
                return "dlk ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dlken"] = _AF6CCI0011_RD_PDH_DLK._upen_dlk_cnv_de1._dlken()
            allFields["dlkid"] = _AF6CCI0011_RD_PDH_DLK._upen_dlk_cnv_de1._dlkid()
            return allFields

    class _upen_dlk_cnv_de3(AtRegister.AtRegister):
        def name(self):
            return "Convert CHID to DLK ID DE3"
    
        def description(self):
            return "Config table convert channel ID to DLK ID"
            
        def width(self):
            return 13
        
        def type(self):
            return "COnfig"
            
        def fomular(self):
            return "0x4000+sliceid<<5+stsid"
            
        def startAddress(self):
            return 0x00004000
            
        def endAddress(self):
            return 0x000041ff

        class _dlken_de3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 12
                
            def startBit(self):
                return 12
        
            def name(self):
                return "dlken_de3"
            
            def description(self):
                return "dlk enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _dlkid_de3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 0
        
            def name(self):
                return "dlkid_de3"
            
            def description(self):
                return "dlk ID"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["dlken_de3"] = _AF6CCI0011_RD_PDH_DLK._upen_dlk_cnv_de3._dlken_de3()
            allFields["dlkid_de3"] = _AF6CCI0011_RD_PDH_DLK._upen_dlk_cnv_de3._dlkid_de3()
            return allFields

    class _upen_cfg_thres(AtRegister.AtRegister):
        def name(self):
            return "Convert CHID to DLK ID DE3"
    
        def description(self):
            return "config threshold of FIFO DDR messasge to alarm CPU"
            
        def width(self):
            return 13
        
        def type(self):
            return "COnfig"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004200
            
        def endAddress(self):
            return 0xffffffff

        class _cfg_thres(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_thres"
            
            def description(self):
                return "threshold FIFO DDR"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_thres"] = _AF6CCI0011_RD_PDH_DLK._upen_cfg_thres._cfg_thres()
            return allFields

    class _upen_sta_fifo_ddr(AtRegister.AtRegister):
        def name(self):
            return "Convert CHID to DLK ID DE3"
    
        def description(self):
            return "Config table convert channel ID to DLK ID"
            
        def width(self):
            return 19
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x5000+sliceid<<5+stsid"
            
        def startAddress(self):
            return 0x00005000
            
        def endAddress(self):
            return 0x00005a7f

        class _wr_pnt(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 10
        
            def name(self):
                return "wr_pnt"
            
            def description(self):
                return "write pointer"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _len_ffddr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 0
        
            def name(self):
                return "len_ffddr"
            
            def description(self):
                return "len fifo ddr message"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["wr_pnt"] = _AF6CCI0011_RD_PDH_DLK._upen_sta_fifo_ddr._wr_pnt()
            allFields["len_ffddr"] = _AF6CCI0011_RD_PDH_DLK._upen_sta_fifo_ddr._len_ffddr()
            return allFields

    class _upen_stk_alrm_thres(AtRegister.AtRegister):
        def name(self):
            return "Stiky ALARM LEN FIFO Mesasge over threshold"
    
        def description(self):
            return "Config table convert channel ID to DLK ID"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004300
            
        def endAddress(self):
            return 0x00004353

        class _stk_alrm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stk_alrm"
            
            def description(self):
                return "sticky corresponfing DLK ID (2688 DLK ID = 32 width x 84 location)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["stk_alrm"] = _AF6CCI0011_RD_PDH_DLK._upen_stk_alrm_thres._stk_alrm()
            return allFields

    class _upen_stk_alrm_thres_col(AtRegister.AtRegister):
        def name(self):
            return "Stiky ALARM LEN FIFO Mesasge over threshold column"
    
        def description(self):
            return "Config table convert channel ID to DLK ID"
            
        def width(self):
            return 84
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004354
            
        def endAddress(self):
            return 0xffffffff

        class _stk_alrm_col(AtRegister.AtRegisterField):
            def stopBit(self):
                return 83
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stk_alrm_col"
            
            def description(self):
                return "sticky corresponfing 84 location of sticky array alarm len FIFO"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["stk_alrm_col"] = _AF6CCI0011_RD_PDH_DLK._upen_stk_alrm_thres_col._stk_alrm_col()
            return allFields

    class _upen_stk_alrm_mess(AtRegister.AtRegister):
        def name(self):
            return "Stiky ALARM  Mesasge"
    
        def description(self):
            return "Config table convert channel ID to DLK ID"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004400
            
        def endAddress(self):
            return 0x00004453

        class _stk_alrm_mess(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stk_alrm_mess"
            
            def description(self):
                return "sticky corresponfing DLK ID (2688 DLK ID = 32 width x 84 location)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["stk_alrm_mess"] = _AF6CCI0011_RD_PDH_DLK._upen_stk_alrm_mess._stk_alrm_mess()
            return allFields

    class _upen_stk_alrm_mess_col(AtRegister.AtRegister):
        def name(self):
            return "Stiky ALARM  Mesasge column"
    
        def description(self):
            return "Config table convert channel ID to DLK ID"
            
        def width(self):
            return 84
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004454
            
        def endAddress(self):
            return 0xffffffff

        class _stk_alrm_mess_col(AtRegister.AtRegisterField):
            def stopBit(self):
                return 83
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stk_alrm_mess_col"
            
            def description(self):
                return "sticky corresponfing 84 location of sticky array alarm message"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["stk_alrm_mess_col"] = _AF6CCI0011_RD_PDH_DLK._upen_stk_alrm_mess_col._stk_alrm_mess_col()
            return allFields

    class _upen_stk_alrm(AtRegister.AtRegister):
        def name(self):
            return "Stiky ALARM LEN FIFO Mesasge over threshold column"
    
        def description(self):
            return "Config table convert channel ID to DLK ID"
            
        def width(self):
            return 2
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00004355
            
        def endAddress(self):
            return 0xffffffff

        class _stk_alrm_over(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "stk_alrm_over"
            
            def description(self):
                return "sticky alarm len FIFO over threshold"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _stk_alrm_mess(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "stk_alrm_mess"
            
            def description(self):
                return "sticky alarm message"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["stk_alrm_over"] = _AF6CCI0011_RD_PDH_DLK._upen_stk_alrm._stk_alrm_over()
            allFields["stk_alrm_mess"] = _AF6CCI0011_RD_PDH_DLK._upen_stk_alrm._stk_alrm_mess()
            return allFields

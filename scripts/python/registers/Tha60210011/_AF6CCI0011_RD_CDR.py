import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0011_RD_CDR(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["cdr_cpu_hold_ctrl"] = _AF6CCI0011_RD_CDR._cdr_cpu_hold_ctrl()
        allRegisters["cdr_line_mode_ctrl"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl()
        allRegisters["cdr_timing_extref_ctrl"] = _AF6CCI0011_RD_CDR._cdr_timing_extref_ctrl()
        allRegisters["cdr_sts_timing_ctrl"] = _AF6CCI0011_RD_CDR._cdr_sts_timing_ctrl()
        allRegisters["cdr_vt_timing_ctrl"] = _AF6CCI0011_RD_CDR._cdr_vt_timing_ctrl()
        allRegisters["cdr_top_timing_frmsync_ctrl"] = _AF6CCI0011_RD_CDR._cdr_top_timing_frmsync_ctrl()
        allRegisters["cdr_refsync_master_octrl"] = _AF6CCI0011_RD_CDR._cdr_refsync_master_octrl()
        allRegisters["cdr_refsync_slaver_octrl"] = _AF6CCI0011_RD_CDR._cdr_refsync_slaver_octrl()
        allRegisters["cdr_acr_timing_extref_prc_ctrl"] = _AF6CCI0011_RD_CDR._cdr_acr_timing_extref_prc_ctrl()
        allRegisters["cdr_acr_eng_timing_ctrl"] = _AF6CCI0011_RD_CDR._cdr_acr_eng_timing_ctrl()
        allRegisters["cdr_adj_state_stat"] = _AF6CCI0011_RD_CDR._cdr_adj_state_stat()
        allRegisters["cdr_adj_holdover_value_stat"] = _AF6CCI0011_RD_CDR._cdr_adj_holdover_value_stat()
        allRegisters["dcr_tx_eng_active_ctrl"] = _AF6CCI0011_RD_CDR._dcr_tx_eng_active_ctrl()
        allRegisters["dcr_prc_src_sel_cfg"] = _AF6CCI0011_RD_CDR._dcr_prc_src_sel_cfg()
        allRegisters["dcr_prd_freq_cfg"] = _AF6CCI0011_RD_CDR._dcr_prd_freq_cfg()
        allRegisters["dcr_prc_freq_cfg"] = _AF6CCI0011_RD_CDR._dcr_prc_freq_cfg()
        allRegisters["dcr_rtp_freq_cfg"] = _AF6CCI0011_RD_CDR._dcr_rtp_freq_cfg()
        allRegisters["dcr_tx_eng_timing_ctrl"] = _AF6CCI0011_RD_CDR._dcr_tx_eng_timing_ctrl()
        allRegisters["dcr_txsh_glb_cfg"] = _AF6CCI0011_RD_CDR._dcr_txsh_glb_cfg()
        allRegisters["RAM_TimingGen_Parity_Force_Control"] = _AF6CCI0011_RD_CDR._RAM_TimingGen_Parity_Force_Control()
        allRegisters["RAM_TimingGen_Parity_Disbale_Control"] = _AF6CCI0011_RD_CDR._RAM_TimingGen_Parity_Disbale_Control()
        allRegisters["RAM_TimingGen_Parity_Error_Sticky"] = _AF6CCI0011_RD_CDR._RAM_TimingGen_Parity_Error_Sticky()
        allRegisters["RAM_ACR_Parity_Force_Control"] = _AF6CCI0011_RD_CDR._RAM_ACR_Parity_Force_Control()
        allRegisters["RAM_ACR_Parity_Disable_Control"] = _AF6CCI0011_RD_CDR._RAM_ACR_Parity_Disable_Control()
        allRegisters["RAM_ACR_Parity_Error_Sticky"] = _AF6CCI0011_RD_CDR._RAM_ACR_Parity_Error_Sticky()
        allRegisters["rdha3_0_control"] = _AF6CCI0011_RD_CDR._rdha3_0_control()
        allRegisters["rdha7_4_control"] = _AF6CCI0011_RD_CDR._rdha7_4_control()
        allRegisters["rdha11_8_control"] = _AF6CCI0011_RD_CDR._rdha11_8_control()
        allRegisters["rdha15_12_control"] = _AF6CCI0011_RD_CDR._rdha15_12_control()
        allRegisters["rdha19_16_control"] = _AF6CCI0011_RD_CDR._rdha19_16_control()
        allRegisters["rdha23_20_control"] = _AF6CCI0011_RD_CDR._rdha23_20_control()
        allRegisters["rdha24data_control"] = _AF6CCI0011_RD_CDR._rdha24data_control()
        allRegisters["rdha_hold63_32"] = _AF6CCI0011_RD_CDR._rdha_hold63_32()
        allRegisters["rdindr_hold95_64"] = _AF6CCI0011_RD_CDR._rdindr_hold95_64()
        allRegisters["rdindr_hold127_96"] = _AF6CCI0011_RD_CDR._rdindr_hold127_96()
        allRegisters["cdr_per_chn_intr_en_ctrl"] = _AF6CCI0011_RD_CDR._cdr_per_chn_intr_en_ctrl()
        allRegisters["cdr_per_chn_intr_stat"] = _AF6CCI0011_RD_CDR._cdr_per_chn_intr_stat()
        allRegisters["cdr_per_chn_curr_stat"] = _AF6CCI0011_RD_CDR._cdr_per_chn_curr_stat()
        allRegisters["cdr_per_chn_intr_or_stat"] = _AF6CCI0011_RD_CDR._cdr_per_chn_intr_or_stat()
        allRegisters["cdr_per_stsvc_intr_or_stat"] = _AF6CCI0011_RD_CDR._cdr_per_stsvc_intr_or_stat()
        allRegisters["cdr_per_stsvc_intr_en_ctrl"] = _AF6CCI0011_RD_CDR._cdr_per_stsvc_intr_en_ctrl()
        return allRegisters

    class _cdr_cpu_hold_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR CPU  Reg Hold Control"
    
        def description(self):
            return "The register provides hold register for three word 32-bits MSB when CPU access to engine."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x030000 + HoldId"
            
        def startAddress(self):
            return 0x00030000
            
        def endAddress(self):
            return 0x00030002

        class _HoldReg0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HoldReg0"
            
            def description(self):
                return "Hold 32 bits"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HoldReg0"] = _AF6CCI0011_RD_CDR._cdr_cpu_hold_ctrl._HoldReg0()
            return allFields

    class _cdr_line_mode_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR line mode control"
    
        def description(self):
            return "This register is used to configure line mode for CDR engine."
            
        def width(self):
            return 128
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0000000 + Slide"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x00000001

        class _VC3Mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 123
                
            def startBit(self):
                return 112
        
            def name(self):
                return "VC3Mode"
            
            def description(self):
                return "VC3 or TU3 mode, each bit is used configured for each STS (only valid in TU3 CEP) 1: TU3 CEP 0: Other mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3Mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 107
                
            def startBit(self):
                return 96
        
            def name(self):
                return "DE3Mode"
            
            def description(self):
                return "DS3 or E3 mode, each bit is used configured for each STS. 1: DS3 mode 0: E3 mod"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS12STSMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 95
                
            def startBit(self):
                return 95
        
            def name(self):
                return "STS12STSMode"
            
            def description(self):
                return "STS mode of STS12 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS12VTType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 94
                
            def startBit(self):
                return 88
        
            def name(self):
                return "STS12VTType"
            
            def description(self):
                return "VT Type of 7 VTG in STS12, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS11STSMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 87
                
            def startBit(self):
                return 87
        
            def name(self):
                return "STS11STSMode"
            
            def description(self):
                return "STS mode of STS11 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS11VTType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 86
                
            def startBit(self):
                return 80
        
            def name(self):
                return "STS11VTType"
            
            def description(self):
                return "VT Type of 7 VTG in STS11, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS10STSMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 79
                
            def startBit(self):
                return 79
        
            def name(self):
                return "STS10STSMode"
            
            def description(self):
                return "STS mode of STS10 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS10VTType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 78
                
            def startBit(self):
                return 72
        
            def name(self):
                return "STS10VTType"
            
            def description(self):
                return "VT Type of 7 VTG in STS10, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS9STSMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 71
                
            def startBit(self):
                return 71
        
            def name(self):
                return "STS9STSMode"
            
            def description(self):
                return "STS mode of STS9 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS9VTType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 70
                
            def startBit(self):
                return 64
        
            def name(self):
                return "STS9VTType"
            
            def description(self):
                return "VT Type of 7 VTG in STS9, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS8STSMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 63
        
            def name(self):
                return "STS8STSMode"
            
            def description(self):
                return "STS mode of STS8 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS8VTType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 62
                
            def startBit(self):
                return 56
        
            def name(self):
                return "STS8VTType"
            
            def description(self):
                return "VT Type of 7 VTG in STS8, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS7STSMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 55
                
            def startBit(self):
                return 55
        
            def name(self):
                return "STS7STSMode"
            
            def description(self):
                return "STS mode of STS7 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS7VTType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 54
                
            def startBit(self):
                return 48
        
            def name(self):
                return "STS7VTType"
            
            def description(self):
                return "VT Type of 7 VTG in STS7, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS6STSMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 47
        
            def name(self):
                return "STS6STSMode"
            
            def description(self):
                return "STS mode of STS6 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS6VTType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 46
                
            def startBit(self):
                return 40
        
            def name(self):
                return "STS6VTType"
            
            def description(self):
                return "VT Type of 7 VTG in STS6, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS5STSMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 39
                
            def startBit(self):
                return 39
        
            def name(self):
                return "STS5STSMode"
            
            def description(self):
                return "STS mode of STS5 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS5VTType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 38
                
            def startBit(self):
                return 32
        
            def name(self):
                return "STS5VTType"
            
            def description(self):
                return "VT Type of 7 VTG in STS5, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS4STSMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 31
        
            def name(self):
                return "STS4STSMode"
            
            def description(self):
                return "STS mode of STS4 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS4VTType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 30
                
            def startBit(self):
                return 24
        
            def name(self):
                return "STS4VTType"
            
            def description(self):
                return "VT Type of 7 VTG in STS4, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS3STSMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "STS3STSMode"
            
            def description(self):
                return "STS mode of STS3 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS3VTType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 16
        
            def name(self):
                return "STS3VTType"
            
            def description(self):
                return "VT Type of 7 VTG in STS3, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS2STSMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 15
        
            def name(self):
                return "STS2STSMode"
            
            def description(self):
                return "STS mode of STS2 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS2VTType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 14
                
            def startBit(self):
                return 8
        
            def name(self):
                return "STS2VTType"
            
            def description(self):
                return "VT Type of 7 VTG in STS2, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS1STSMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "STS1STSMode"
            
            def description(self):
                return "STS mode of STS1 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _STS1VTType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 0
        
            def name(self):
                return "STS1VTType"
            
            def description(self):
                return "VT Type of 7 VTG in STS1, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VC3Mode"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._VC3Mode()
            allFields["DE3Mode"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._DE3Mode()
            allFields["STS12STSMode"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._STS12STSMode()
            allFields["STS12VTType"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._STS12VTType()
            allFields["STS11STSMode"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._STS11STSMode()
            allFields["STS11VTType"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._STS11VTType()
            allFields["STS10STSMode"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._STS10STSMode()
            allFields["STS10VTType"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._STS10VTType()
            allFields["STS9STSMode"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._STS9STSMode()
            allFields["STS9VTType"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._STS9VTType()
            allFields["STS8STSMode"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._STS8STSMode()
            allFields["STS8VTType"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._STS8VTType()
            allFields["STS7STSMode"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._STS7STSMode()
            allFields["STS7VTType"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._STS7VTType()
            allFields["STS6STSMode"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._STS6STSMode()
            allFields["STS6VTType"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._STS6VTType()
            allFields["STS5STSMode"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._STS5STSMode()
            allFields["STS5VTType"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._STS5VTType()
            allFields["STS4STSMode"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._STS4STSMode()
            allFields["STS4VTType"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._STS4VTType()
            allFields["STS3STSMode"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._STS3STSMode()
            allFields["STS3VTType"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._STS3VTType()
            allFields["STS2STSMode"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._STS2STSMode()
            allFields["STS2VTType"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._STS2VTType()
            allFields["STS1STSMode"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._STS1STSMode()
            allFields["STS1VTType"] = _AF6CCI0011_RD_CDR._cdr_line_mode_ctrl._STS1VTType()
            return allFields

    class _cdr_timing_extref_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR Timing External reference control"
    
        def description(self):
            return "This register is used to configure for the 2Khz coefficient of two external reference signal in order to generate timing. The mean that, if the reference signal is 8Khz, the  coefficient must be configured 4 (I.e 8Khz  = 4x2Khz)"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _Ext3N2k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 32
        
            def name(self):
                return "Ext3N2k"
            
            def description(self):
                return "The 2Khz coefficient of the third external reference signal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Ext2N2k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Ext2N2k"
            
            def description(self):
                return "The 2Khz coefficient of the second external reference signal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Ext1N2k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Ext1N2k"
            
            def description(self):
                return "The 2Khz coefficient of the first external reference signal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Ext3N2k"] = _AF6CCI0011_RD_CDR._cdr_timing_extref_ctrl._Ext3N2k()
            allFields["Ext2N2k"] = _AF6CCI0011_RD_CDR._cdr_timing_extref_ctrl._Ext2N2k()
            allFields["Ext1N2k"] = _AF6CCI0011_RD_CDR._cdr_timing_extref_ctrl._Ext1N2k()
            return allFields

    class _cdr_sts_timing_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR STS Timing control"
    
        def description(self):
            return "This register is used to configure timing mode for per STS"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0000800+STS"
            
        def startAddress(self):
            return 0x00000800
            
        def endAddress(self):
            return 0x0000081f

        class _CDRChid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 29
                
            def startBit(self):
                return 20
        
            def name(self):
                return "CDRChid"
            
            def description(self):
                return "CDR channel ID in CDR mode."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VC3EParEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 17
                
            def startBit(self):
                return 17
        
            def name(self):
                return "VC3EParEn"
            
            def description(self):
                return "VC3 EPAR mode 0 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapSTSMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "MapSTSMode"
            
            def description(self):
                return "Map STS mode 0: Payload STS mode 1: Payload DE3 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE3TimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DE3TimeMode"
            
            def description(self):
                return "DE3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR timing mode 9: OCN System timing mode 10: DCR timing mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VC3TimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VC3TimeMode"
            
            def description(self):
                return "VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR timing mode for CEP mode 9: OCN System timing mode 10: DCR timing mode for CEP mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRChid"] = _AF6CCI0011_RD_CDR._cdr_sts_timing_ctrl._CDRChid()
            allFields["VC3EParEn"] = _AF6CCI0011_RD_CDR._cdr_sts_timing_ctrl._VC3EParEn()
            allFields["MapSTSMode"] = _AF6CCI0011_RD_CDR._cdr_sts_timing_ctrl._MapSTSMode()
            allFields["DE3TimeMode"] = _AF6CCI0011_RD_CDR._cdr_sts_timing_ctrl._DE3TimeMode()
            allFields["VC3TimeMode"] = _AF6CCI0011_RD_CDR._cdr_sts_timing_ctrl._VC3TimeMode()
            return allFields

    class _cdr_vt_timing_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR VT Timing control"
    
        def description(self):
            return "This register is used to configure timing mode for per VT"
            
        def width(self):
            return 24
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0000C00 + STS*32 + TUG*4 + VT"
            
        def startAddress(self):
            return 0x00000c00
            
        def endAddress(self):
            return 0x00000fff

        class _CDRChid(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 12
        
            def name(self):
                return "CDRChid"
            
            def description(self):
                return "CDR channel ID in CDR mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VTEParEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "VTEParEn"
            
            def description(self):
                return "VT EPAR mode 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _MapVTMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "MapVTMode"
            
            def description(self):
                return "Map VT mode 0: Payload VT15/VT2 mode 1: Payload DS1/E1 mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DE1TimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "DE1TimeMode"
            
            def description(self):
                return "DE1 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR timing mode 9: OCN System timing mode 10: DCR timing mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VTTimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "VTTimeMode"
            
            def description(self):
                return "VT time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR timing mode for CEP mode 9: OCN System timing mode 10: DCR timing mode for CEP mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRChid"] = _AF6CCI0011_RD_CDR._cdr_vt_timing_ctrl._CDRChid()
            allFields["VTEParEn"] = _AF6CCI0011_RD_CDR._cdr_vt_timing_ctrl._VTEParEn()
            allFields["MapVTMode"] = _AF6CCI0011_RD_CDR._cdr_vt_timing_ctrl._MapVTMode()
            allFields["DE1TimeMode"] = _AF6CCI0011_RD_CDR._cdr_vt_timing_ctrl._DE1TimeMode()
            allFields["VTTimeMode"] = _AF6CCI0011_RD_CDR._cdr_vt_timing_ctrl._VTTimeMode()
            return allFields

    class _cdr_top_timing_frmsync_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR ToP Timing Frame sync 8K control"
    
        def description(self):
            return "This register is used to configure timing source to generate the 125us signal frame sync for ToP"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000820
            
        def endAddress(self):
            return 0xffffffff

        class _ToPPDHLineType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ToPPDHLineType"
            
            def description(self):
                return "Line type of DS1/E1 loop time or PDH CDR 0: E1 1: DS1 2: VT2 3: VT15 4: E3 5: DS3 6: STS1 7: Reserve"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ToPPDHLineID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 4
        
            def name(self):
                return "ToPPDHLineID"
            
            def description(self):
                return "Line ID of DS1/E1 loop time or PDH/CEP CDR"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _ToPTimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ToPTimeMode"
            
            def description(self):
                return "Time mode  for ToP 0: System timing mode 1: DS1/E1 Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: PDH CDR timing mode 9: OCN System timing mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ToPPDHLineType"] = _AF6CCI0011_RD_CDR._cdr_top_timing_frmsync_ctrl._ToPPDHLineType()
            allFields["ToPPDHLineID"] = _AF6CCI0011_RD_CDR._cdr_top_timing_frmsync_ctrl._ToPPDHLineID()
            allFields["ToPTimeMode"] = _AF6CCI0011_RD_CDR._cdr_top_timing_frmsync_ctrl._ToPTimeMode()
            return allFields

    class _cdr_refsync_master_octrl(AtRegister.AtRegister):
        def name(self):
            return "CDR Reference Sync 8K Master Output control"
    
        def description(self):
            return "This register is used to configure timing source to generate the reference sync master output signal"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000821
            
        def endAddress(self):
            return 0xffffffff

        class _RefOut1PDHLineType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RefOut1PDHLineType"
            
            def description(self):
                return "Line type of DS1/E1 loop time or PDH/CEP CDR 0: E1 1: DS1 2: VT2 3: VT15 4: E3 5: DS3 6: STS1 7: Reserve"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RefOut1PDHLineID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RefOut1PDHLineID"
            
            def description(self):
                return "Line ID of DS1/E1 loop time or PDH CDR"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RefOut1TimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RefOut1TimeMode"
            
            def description(self):
                return "Time mode for RefOut1 0: System timing mode 1: DS1/E1 Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: PDH CDR timing mode 9: External Clock sync"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RefOut1PDHLineType"] = _AF6CCI0011_RD_CDR._cdr_refsync_master_octrl._RefOut1PDHLineType()
            allFields["RefOut1PDHLineID"] = _AF6CCI0011_RD_CDR._cdr_refsync_master_octrl._RefOut1PDHLineID()
            allFields["RefOut1TimeMode"] = _AF6CCI0011_RD_CDR._cdr_refsync_master_octrl._RefOut1TimeMode()
            return allFields

    class _cdr_refsync_slaver_octrl(AtRegister.AtRegister):
        def name(self):
            return "CDR Reference Sync 8k Slaver Output control"
    
        def description(self):
            return "This register is used to configure timing source to generate the reference sync slaver output signal"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000822
            
        def endAddress(self):
            return 0xffffffff

        class _RefOut2PDHLineType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RefOut2PDHLineType"
            
            def description(self):
                return "Line type of DS1/E1 loop time or PDH/CEP CDR 0: E1 1: DS1 2: VT2 3: VT15 4: E3 5: DS3 6: STS1 7: Reserve"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RefOut2PDHLineID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 13
                
            def startBit(self):
                return 4
        
            def name(self):
                return "RefOut2PDHLineID"
            
            def description(self):
                return "Line ID of DS1/E1 loop time or PDH CDR"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _RefOut2TimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RefOut2TimeMode"
            
            def description(self):
                return "Time mode for RefOut1 0: System timing mode 1: DS1/E1 Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: PDH CDR timing mode 9: External Clock sync"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["RefOut2PDHLineType"] = _AF6CCI0011_RD_CDR._cdr_refsync_slaver_octrl._RefOut2PDHLineType()
            allFields["RefOut2PDHLineID"] = _AF6CCI0011_RD_CDR._cdr_refsync_slaver_octrl._RefOut2PDHLineID()
            allFields["RefOut2TimeMode"] = _AF6CCI0011_RD_CDR._cdr_refsync_slaver_octrl._RefOut2TimeMode()
            return allFields

    class _cdr_acr_timing_extref_prc_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR ACR Timing External reference  and PRC control"
    
        def description(self):
            return "This register is used to configure for the 2Khz coefficient of external reference signal, PRC clock in order to generate the sync 125us timing. The mean that, if the reference signal is 8Khz, the  coefficient must be configured 4 (I.e 8Khz  = 4x2Khz)"
            
        def width(self):
            return 64
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00020000
            
        def endAddress(self):
            return 0xffffffff

        class _TxOCNN2k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 48
        
            def name(self):
                return "TxOCNN2k"
            
            def description(self):
                return "The 2Khz coefficient of the Ethernet clock signal, default 125Mhz input clock"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Ext2N2k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 32
        
            def name(self):
                return "Ext2N2k"
            
            def description(self):
                return "The 2Khz coefficient of the second external reference signal, default 8khz input signal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Ext1N2k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Ext1N2k"
            
            def description(self):
                return "The 2Khz coefficient of the first external reference signal, default 8khz input signal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Ocn1N2k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Ocn1N2k"
            
            def description(self):
                return "The 2Khz coefficient of the SONET interface signal, default 8khz input signal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TxOCNN2k"] = _AF6CCI0011_RD_CDR._cdr_acr_timing_extref_prc_ctrl._TxOCNN2k()
            allFields["Ext2N2k"] = _AF6CCI0011_RD_CDR._cdr_acr_timing_extref_prc_ctrl._Ext2N2k()
            allFields["Ext1N2k"] = _AF6CCI0011_RD_CDR._cdr_acr_timing_extref_prc_ctrl._Ext1N2k()
            allFields["Ocn1N2k"] = _AF6CCI0011_RD_CDR._cdr_acr_timing_extref_prc_ctrl._Ocn1N2k()
            return allFields

    class _cdr_acr_eng_timing_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR ACR Engine Timing control"
    
        def description(self):
            return "This register is used to configure timing mode for per STS"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0020800 + CHID"
            
        def startAddress(self):
            return 0x00020800
            
        def endAddress(self):
            return 0x00020bff

        class _VC4_4c(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "VC4_4c"
            
            def description(self):
                return "VC4_4c/TU3 mode 0: STS1/VC4 1: TU3/VC4-4c"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PktLen_ind(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "PktLen_ind"
            
            def description(self):
                return "The payload packet  length for jumbo frame"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _HoldValMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "HoldValMode"
            
            def description(self):
                return "Hold value mode of NCO, default value 0 0: Hardware calculated and auto update 1: Software calculated and update, hardware is disabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SeqMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "SeqMode"
            
            def description(self):
                return "Sequence mode mode, default value 0 0: Wrap zero 1: Skip zero"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 20
        
            def name(self):
                return "LineType"
            
            def description(self):
                return "Line type mode 0: E1 1: DS1 2: VT2 3: VT15 4: E3 5: DS3 6: STS1/TU3 7: VC4/VC4-4c"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PktLen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PktLen"
            
            def description(self):
                return "The payload packet  length parameter to create a packet. SAToP mode: The number payload of bit. CESoPSN mode: The number of bit which converted to full DS1/E1 rate mode. In CESoPSN mode, the payload is assembled by NxDS0 with M frame, the value configured to this register is Mx256 bits for E1 mode, Mx193 bits for DS1 mode."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRTimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRTimeMode"
            
            def description(self):
                return "CDR time mode 0: System mode 1: Loop timing mode, transparency service Rx clock to service Tx clock 2: LIU timing mode, using service Rx clock for CDR source to generate service Tx clock 3: Prc timing mode, using Prc clock for CDR source to generate service Tx clock 4: Ext#1 timing mode, using Ext#1 clock for CDR source to generate service Tx clock 5: Ext#2 timing mode, using Ext#2 clock for CDR source to generate service Tx clock 6: Tx SONET timing mode, using Tx Line OCN clock for CDR source to generate service Tx clock 7: Free timing mode, using system clock for CDR source to generate service Tx clock 8: ACR timing mode 9: Reserve 10: DCR timing mode 11: Reserve 12: ACR Fast lock mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VC4_4c"] = _AF6CCI0011_RD_CDR._cdr_acr_eng_timing_ctrl._VC4_4c()
            allFields["PktLen_ind"] = _AF6CCI0011_RD_CDR._cdr_acr_eng_timing_ctrl._PktLen_ind()
            allFields["HoldValMode"] = _AF6CCI0011_RD_CDR._cdr_acr_eng_timing_ctrl._HoldValMode()
            allFields["SeqMode"] = _AF6CCI0011_RD_CDR._cdr_acr_eng_timing_ctrl._SeqMode()
            allFields["LineType"] = _AF6CCI0011_RD_CDR._cdr_acr_eng_timing_ctrl._LineType()
            allFields["PktLen"] = _AF6CCI0011_RD_CDR._cdr_acr_eng_timing_ctrl._PktLen()
            allFields["CDRTimeMode"] = _AF6CCI0011_RD_CDR._cdr_acr_eng_timing_ctrl._CDRTimeMode()
            return allFields

    class _cdr_adj_state_stat(AtRegister.AtRegister):
        def name(self):
            return "CDR Adjust State status"
    
        def description(self):
            return "This register is used to store status or configure  some parameter of per CDR engine"
            
        def width(self):
            return 8
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0021000 + CHID"
            
        def startAddress(self):
            return 0x00021000
            
        def endAddress(self):
            return 0x000213ff

        class _Adjstate(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Adjstate"
            
            def description(self):
                return "Adjust state 0: Load state 1: Holdover state 2: Initialization state 3: Learn State 4: Rapid State 5: Lock State"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Adjstate"] = _AF6CCI0011_RD_CDR._cdr_adj_state_stat._Adjstate()
            return allFields

    class _cdr_adj_holdover_value_stat(AtRegister.AtRegister):
        def name(self):
            return "CDR Adjust Holdover value status"
    
        def description(self):
            return "This register is used to store status or configure  some parameter of per CDR engine"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0021800 + CHID"
            
        def startAddress(self):
            return 0x00021800
            
        def endAddress(self):
            return 0x00021bff

        class _HoldVal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HoldVal"
            
            def description(self):
                return "NCO Holdover value parameter E1RATE   = 32'd3817748707;   Resolution =    0.262 parameter DS1RATE  = 32'd3837632815;   Resolution =    0.260 parameter VT2RATE  = 32'd4175662648;   Resolution =    0.239 parameter VT15RATE = 32'd4135894433;   Resolution =    0.241 parameter E3RATE   = 32'd2912117977;   Resolution =    0.343 parameter DS3RATE  = 32'd3790634015;   Resolution =    0.263 parameter TU3RATE  = 32'd4148547956;   Resolution =    0.239 parameter STS1RATE = 32'd4246160849;   Resolution =    0.239"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HoldVal"] = _AF6CCI0011_RD_CDR._cdr_adj_holdover_value_stat._HoldVal()
            return allFields

    class _dcr_tx_eng_active_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DCR TX Engine Active Control"
    
        def description(self):
            return "This register is used to activate the DCR TX Engine. Active high."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00010000
            
        def endAddress(self):
            return 0xffffffff

        class _data(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "data"
            
            def description(self):
                return "DCR TX Engine Active Control"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["data"] = _AF6CCI0011_RD_CDR._dcr_tx_eng_active_ctrl._data()
            return allFields

    class _dcr_prc_src_sel_cfg(AtRegister.AtRegister):
        def name(self):
            return "DCR PRC Source Select Configuration"
    
        def description(self):
            return "This register is used to configure to select PRC source for PRC timer. The PRC clock selected must be less than 19.44 Mhz."
            
        def width(self):
            return 8
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00010001
            
        def endAddress(self):
            return 0xffffffff

        class _DcrPrcDirectMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DcrPrcDirectMode"
            
            def description(self):
                return "0: Select RTP frequency lower than 65535 and use value DCRRTPFreq as RTP frequency 1: Select RTP frequency value 155.52Mhz divide by DcrRtpFreqDivide. In this mode, below DcrPrcSourceSel[2:0] must set to 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DcrPrcSourceSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DcrPrcSourceSel"
            
            def description(self):
                return "PRC source selection. 0: PRC Reference Clock 1: System Clock 19Mhz 2: External Reference Clock 1. 3: External Reference Clock 2. 4: Ocn Line Clock Port 1 5: Ocn Line Clock Port 2 6: Ocn Line Clock Port 3 7: Ocn Line Clock Port 4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DcrPrcDirectMode"] = _AF6CCI0011_RD_CDR._dcr_prc_src_sel_cfg._DcrPrcDirectMode()
            allFields["DcrPrcSourceSel"] = _AF6CCI0011_RD_CDR._dcr_prc_src_sel_cfg._DcrPrcSourceSel()
            return allFields

    class _dcr_prd_freq_cfg(AtRegister.AtRegister):
        def name(self):
            return "DCR PRC Frequency Configuration"
    
        def description(self):
            return "This register is used to select RTP frequency 77.76Mhz or 155.52Mhz"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00010002
            
        def endAddress(self):
            return 0xffffffff

        class _DcrRtpFreqDivide(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DcrRtpFreqDivide"
            
            def description(self):
                return "Dcr RTP Frequency = 155.52Mhz/ DcrRtpFreqDivide"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DcrRtpFreqDivide"] = _AF6CCI0011_RD_CDR._dcr_prd_freq_cfg._DcrRtpFreqDivide()
            return allFields

    class _dcr_prc_freq_cfg(AtRegister.AtRegister):
        def name(self):
            return "DCR PRC Frequency Configuration"
    
        def description(self):
            return "This register is used to configure the frequency of PRC clock."
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0001000b
            
        def endAddress(self):
            return 0xffffffff

        class _DCRPrcFrequency(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DCRPrcFrequency"
            
            def description(self):
                return "Frequency of PRC clock. This is used to configure the frequency of PRC clock in Khz. Unit is Khz. Exp: The PRC clock is 19.44Mhz. This register will be configured to 19440."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DCRPrcFrequency"] = _AF6CCI0011_RD_CDR._dcr_prc_freq_cfg._DCRPrcFrequency()
            return allFields

    class _dcr_rtp_freq_cfg(AtRegister.AtRegister):
        def name(self):
            return "DCR RTP Frequency Configuration"
    
        def description(self):
            return "This register is used to configure the frequency of PRC clock."
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0001000c
            
        def endAddress(self):
            return 0xffffffff

        class _DCRRTPFreq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DCRRTPFreq"
            
            def description(self):
                return "Frequency of RTP clock. This is used to configure the frequency of RTP clock in Khz. Unit is Khz. Exp: The RTP clock is 19.44Mhz. This register will be configured to 19440."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DCRRTPFreq"] = _AF6CCI0011_RD_CDR._dcr_rtp_freq_cfg._DCRRTPFreq()
            return allFields

    class _dcr_tx_eng_timing_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DCR Tx Engine Timing control"
    
        def description(self):
            return "This register is used to configure timing mode for per STS"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0010400 + CHID"
            
        def startAddress(self):
            return 0x00010400
            
        def endAddress(self):
            return 0xffffffff

        class _PktLen_bit(AtRegister.AtRegisterField):
            def stopBit(self):
                return 21
                
            def startBit(self):
                return 19
        
            def name(self):
                return "PktLen_bit"
            
            def description(self):
                return "The payload packet length BIT, PktLen[2:0]"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 18
                
            def startBit(self):
                return 16
        
            def name(self):
                return "LineType"
            
            def description(self):
                return "Line type mode 0: DS1 1: E1 2: DS3 3: E3 4: VT15 5: VT2 6: STS1/TU3 7: Reserve"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PktLen_byte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "PktLen_byte"
            
            def description(self):
                return "The payload packet length Byte, PktLen[17:3]"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["PktLen_bit"] = _AF6CCI0011_RD_CDR._dcr_tx_eng_timing_ctrl._PktLen_bit()
            allFields["LineType"] = _AF6CCI0011_RD_CDR._dcr_tx_eng_timing_ctrl._LineType()
            allFields["PktLen_byte"] = _AF6CCI0011_RD_CDR._dcr_tx_eng_timing_ctrl._PktLen_byte()
            return allFields

    class _dcr_txsh_glb_cfg(AtRegister.AtRegister):
        def name(self):
            return "DCR TxShapper Global Configuration"
    
        def description(self):
            return "This register is used to configure the frequency of PRC clock."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00010800
            
        def endAddress(self):
            return 0xffffffff

        class _DCRTxShDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "DCRTxShDis"
            
            def description(self):
                return "Set 1 to Disable TxShapper engine"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DCRTxShDis"] = _AF6CCI0011_RD_CDR._dcr_txsh_glb_cfg._DCRTxShDis()
            return allFields

    class _RAM_TimingGen_Parity_Force_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM TimingGen Parity Force Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000082c
            
        def endAddress(self):
            return 0xffffffff

        class _CDRVTTimingCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "CDRVTTimingCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"CDR VT Timing control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRSTSTimingCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRSTSTimingCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"CDR STS Timing control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRVTTimingCtrl_ParErrFrc"] = _AF6CCI0011_RD_CDR._RAM_TimingGen_Parity_Force_Control._CDRVTTimingCtrl_ParErrFrc()
            allFields["CDRSTSTimingCtrl_ParErrFrc"] = _AF6CCI0011_RD_CDR._RAM_TimingGen_Parity_Force_Control._CDRSTSTimingCtrl_ParErrFrc()
            return allFields

    class _RAM_TimingGen_Parity_Disbale_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM TimingGen Parity Disable Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000082d
            
        def endAddress(self):
            return 0xffffffff

        class _CDRVTTimingCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "CDRVTTimingCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"CDR VT Timing control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRSTSTimingCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRSTSTimingCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"CDR STS Timing control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRVTTimingCtrl_ParErrDis"] = _AF6CCI0011_RD_CDR._RAM_TimingGen_Parity_Disbale_Control._CDRVTTimingCtrl_ParErrDis()
            allFields["CDRSTSTimingCtrl_ParErrDis"] = _AF6CCI0011_RD_CDR._RAM_TimingGen_Parity_Disbale_Control._CDRSTSTimingCtrl_ParErrDis()
            return allFields

    class _RAM_TimingGen_Parity_Error_Sticky(AtRegister.AtRegister):
        def name(self):
            return "RAM TimingGen parity Error Sticky"
    
        def description(self):
            return "This register configures disable parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000082e
            
        def endAddress(self):
            return 0xffffffff

        class _CDRVTTimingCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "CDRVTTimingCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"CDR VT Timing control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRSTSTimingCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRSTSTimingCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"CDR STS Timing control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRVTTimingCtrl_ParErrStk"] = _AF6CCI0011_RD_CDR._RAM_TimingGen_Parity_Error_Sticky._CDRVTTimingCtrl_ParErrStk()
            allFields["CDRSTSTimingCtrl_ParErrStk"] = _AF6CCI0011_RD_CDR._RAM_TimingGen_Parity_Error_Sticky._CDRSTSTimingCtrl_ParErrStk()
            return allFields

    class _RAM_ACR_Parity_Force_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM ACR Parity Force Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0002000c
            
        def endAddress(self):
            return 0xffffffff

        class _CDRACRTimingCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRACRTimingCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"CDR ACR Engine Timing control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRACRTimingCtrl_ParErrFrc"] = _AF6CCI0011_RD_CDR._RAM_ACR_Parity_Force_Control._CDRACRTimingCtrl_ParErrFrc()
            return allFields

    class _RAM_ACR_Parity_Disable_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM ACR Parity Disable Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0002000d
            
        def endAddress(self):
            return 0xffffffff

        class _CDRACRTimingCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRACRTimingCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"CDR ACR Engine Timing control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRACRTimingCtrl_ParErrDis"] = _AF6CCI0011_RD_CDR._RAM_ACR_Parity_Disable_Control._CDRACRTimingCtrl_ParErrDis()
            return allFields

    class _RAM_ACR_Parity_Error_Sticky(AtRegister.AtRegister):
        def name(self):
            return "RAM ACR parity Error Sticky"
    
        def description(self):
            return "This register configures disable parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0002000e
            
        def endAddress(self):
            return 0xffffffff

        class _CDRACRTimingCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRACRTimingCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"CDR ACR Engine Timing control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRACRTimingCtrl_ParErrStk"] = _AF6CCI0011_RD_CDR._RAM_ACR_Parity_Error_Sticky._CDRACRTimingCtrl_ParErrStk()
            return allFields

    class _rdha3_0_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit3_0 Control"
    
        def description(self):
            return "This register is used to send HA read address bit3_0 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x30200 + HaAddr3_0"
            
        def startAddress(self):
            return 0x00030200
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr3_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr3_0"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr3_0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr3_0"] = _AF6CCI0011_RD_CDR._rdha3_0_control._ReadAddr3_0()
            return allFields

    class _rdha7_4_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit7_4 Control"
    
        def description(self):
            return "This register is used to send HA read address bit7_4 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x30210 + HaAddr7_4"
            
        def startAddress(self):
            return 0x00030210
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr7_4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr7_4"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr7_4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr7_4"] = _AF6CCI0011_RD_CDR._rdha7_4_control._ReadAddr7_4()
            return allFields

    class _rdha11_8_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit11_8 Control"
    
        def description(self):
            return "This register is used to send HA read address bit11_8 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x30220 + HaAddr11_8"
            
        def startAddress(self):
            return 0x00030220
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr11_8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr11_8"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr11_8"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr11_8"] = _AF6CCI0011_RD_CDR._rdha11_8_control._ReadAddr11_8()
            return allFields

    class _rdha15_12_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit15_12 Control"
    
        def description(self):
            return "This register is used to send HA read address bit15_12 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x30230 + HaAddr15_12"
            
        def startAddress(self):
            return 0x00030230
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr15_12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr15_12"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr15_12"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr15_12"] = _AF6CCI0011_RD_CDR._rdha15_12_control._ReadAddr15_12()
            return allFields

    class _rdha19_16_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit19_16 Control"
    
        def description(self):
            return "This register is used to send HA read address bit19_16 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x30240 + HaAddr19_16"
            
        def startAddress(self):
            return 0x00030240
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr19_16(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr19_16"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr19_16"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr19_16"] = _AF6CCI0011_RD_CDR._rdha19_16_control._ReadAddr19_16()
            return allFields

    class _rdha23_20_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit23_20 Control"
    
        def description(self):
            return "This register is used to send HA read address bit23_20 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x30250 + HaAddr23_20"
            
        def startAddress(self):
            return 0x00030250
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr23_20(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr23_20"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr23_20"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr23_20"] = _AF6CCI0011_RD_CDR._rdha23_20_control._ReadAddr23_20()
            return allFields

    class _rdha24data_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit24 and Data Control"
    
        def description(self):
            return "This register is used to send HA read address bit24 to HA engine to read data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x30260 + HaAddr24"
            
        def startAddress(self):
            return 0x00030260
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData31_0"
            
            def description(self):
                return "HA read data bit31_0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData31_0"] = _AF6CCI0011_RD_CDR._rdha24data_control._ReadHaData31_0()
            return allFields

    class _rdha_hold63_32(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data63_32"
    
        def description(self):
            return "This register is used to read HA dword2 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00030270
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData63_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData63_32"
            
            def description(self):
                return "HA read data bit63_32"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData63_32"] = _AF6CCI0011_RD_CDR._rdha_hold63_32._ReadHaData63_32()
            return allFields

    class _rdindr_hold95_64(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data95_64"
    
        def description(self):
            return "This register is used to read HA dword3 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00030271
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData95_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData95_64"
            
            def description(self):
                return "HA read data bit95_64"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData95_64"] = _AF6CCI0011_RD_CDR._rdindr_hold95_64._ReadHaData95_64()
            return allFields

    class _rdindr_hold127_96(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data127_96"
    
        def description(self):
            return "This register is used to read HA dword4 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00030272
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData127_96(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData127_96"
            
            def description(self):
                return "HA read data bit127_96"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData127_96"] = _AF6CCI0011_RD_CDR._rdindr_hold127_96._ReadHaData127_96()
            return allFields

    class _cdr_per_chn_intr_en_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR per Channel Interrupt Enable Control"
    
        def description(self):
            return "This is the per Channel interrupt enable of CDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00025000 +  StsID*32 + VtnID"
            
        def startAddress(self):
            return 0x00025000
            
        def endAddress(self):
            return 0x000253ff

        class _CDRUnlokcedIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRUnlokcedIntrEn"
            
            def description(self):
                return "Set 1 to enable change UnLocked te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRUnlokcedIntrEn"] = _AF6CCI0011_RD_CDR._cdr_per_chn_intr_en_ctrl._CDRUnlokcedIntrEn()
            return allFields

    class _cdr_per_chn_intr_stat(AtRegister.AtRegister):
        def name(self):
            return "CDR per Channel Interrupt Status"
    
        def description(self):
            return "This is the per Channel interrupt tus of CDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00025400 +  StsID*32 + VtnID"
            
        def startAddress(self):
            return 0x00025400
            
        def endAddress(self):
            return 0x000257ff

        class _CDRUnLockedIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRUnLockedIntr"
            
            def description(self):
                return "Set 1 if there is a change in UnLocked the event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRUnLockedIntr"] = _AF6CCI0011_RD_CDR._cdr_per_chn_intr_stat._CDRUnLockedIntr()
            return allFields

    class _cdr_per_chn_curr_stat(AtRegister.AtRegister):
        def name(self):
            return "CDR per Channel Current Status"
    
        def description(self):
            return "This is the per Channel Current tus of CDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00025800 +  StsID*32 + VtnID"
            
        def startAddress(self):
            return 0x00025800
            
        def endAddress(self):
            return 0x00025bff

        class _CDRUnLockedCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRUnLockedCurrSta"
            
            def description(self):
                return "Current tus of UnLocked event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRUnLockedCurrSta"] = _AF6CCI0011_RD_CDR._cdr_per_chn_curr_stat._CDRUnLockedCurrSta()
            return allFields

    class _cdr_per_chn_intr_or_stat(AtRegister.AtRegister):
        def name(self):
            return "CDR per Channel Interrupt OR Status"
    
        def description(self):
            return "The register consists of 32 bits for 32 VT/TUs of the related STS/VC in the CDR. Each bit is used to store Interrupt OR tus of the related DS1/E1."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x00025C00 +  StsID"
            
        def startAddress(self):
            return 0x00025c00
            
        def endAddress(self):
            return 0x00025c1f

        class _CDRVtIntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRVtIntrOrSta"
            
            def description(self):
                return "Set to 1 if any interrupt status bit of corresponding DS1/E1 is set and its interrupt is enabled."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRVtIntrOrSta"] = _AF6CCI0011_RD_CDR._cdr_per_chn_intr_or_stat._CDRVtIntrOrSta()
            return allFields

    class _cdr_per_stsvc_intr_or_stat(AtRegister.AtRegister):
        def name(self):
            return "CDR per STS/VC Interrupt OR Status"
    
        def description(self):
            return "The register consists of 24 bits for 24 STS/VCs of the CDR. Each bit is used to store Interrupt OR tus of the related STS/VC."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00025fff
            
        def endAddress(self):
            return 0xffffffff

        class _CDRStsIntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRStsIntrOrSta"
            
            def description(self):
                return "Set to 1 if any interrupt status bit of corresponding STS/VC is set and its interrupt is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRStsIntrOrSta"] = _AF6CCI0011_RD_CDR._cdr_per_stsvc_intr_or_stat._CDRStsIntrOrSta()
            return allFields

    class _cdr_per_stsvc_intr_en_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR per STS/VC Interrupt Enable Control"
    
        def description(self):
            return "The register consists of 24 interrupt enable bits for 24 STS/VCs in the Rx DS1/E1/J1 Framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00025ffe
            
        def endAddress(self):
            return 0xffffffff

        class _CDRStsIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRStsIntrEn"
            
            def description(self):
                return "Set to 1 to enable the related STS/VC to generate interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRStsIntrEn"] = _AF6CCI0011_RD_CDR._cdr_per_stsvc_intr_en_ctrl._CDRStsIntrEn()
            return allFields

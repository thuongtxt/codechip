import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0011_RD_CDR_HO(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["cdr_cpu_hold_ctrl"] = _AF6CCI0011_RD_CDR_HO._cdr_cpu_hold_ctrl()
        allRegisters["cdr_timing_extref_ctrl"] = _AF6CCI0011_RD_CDR_HO._cdr_timing_extref_ctrl()
        allRegisters["cdr_sts_timing_ctrl"] = _AF6CCI0011_RD_CDR_HO._cdr_sts_timing_ctrl()
        allRegisters["cdr_acr_eng_timing_ctrl"] = _AF6CCI0011_RD_CDR_HO._cdr_acr_eng_timing_ctrl()
        allRegisters["cdr_adj_state_stat"] = _AF6CCI0011_RD_CDR_HO._cdr_adj_state_stat()
        allRegisters["cdr_adj_holdover_value_stat"] = _AF6CCI0011_RD_CDR_HO._cdr_adj_holdover_value_stat()
        allRegisters["dcr_tx_eng_active_ctrl"] = _AF6CCI0011_RD_CDR_HO._dcr_tx_eng_active_ctrl()
        allRegisters["dcr_prc_src_sel_cfg"] = _AF6CCI0011_RD_CDR_HO._dcr_prc_src_sel_cfg()
        allRegisters["dcr_prd_freq_cfg"] = _AF6CCI0011_RD_CDR_HO._dcr_prd_freq_cfg()
        allRegisters["dcr_prc_freq_cfg"] = _AF6CCI0011_RD_CDR_HO._dcr_prc_freq_cfg()
        allRegisters["dcr_rtp_freq_cfg"] = _AF6CCI0011_RD_CDR_HO._dcr_rtp_freq_cfg()
        allRegisters["RAM_TimingGen_Parity_Force_Control"] = _AF6CCI0011_RD_CDR_HO._RAM_TimingGen_Parity_Force_Control()
        allRegisters["RAM_TimingGen_Parity_Diable_Control"] = _AF6CCI0011_RD_CDR_HO._RAM_TimingGen_Parity_Diable_Control()
        allRegisters["RAM_TimingGen_Parity_Error_Sticky"] = _AF6CCI0011_RD_CDR_HO._RAM_TimingGen_Parity_Error_Sticky()
        allRegisters["RAM_ACR_Parity_Force_Control"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Force_Control()
        allRegisters["RAM_ACR_Parity_Disable_Control"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Disable_Control()
        allRegisters["RAM_ACR_Parity_Error_Sticky"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Error_Sticky()
        allRegisters["rdha3_0_control"] = _AF6CCI0011_RD_CDR_HO._rdha3_0_control()
        allRegisters["rdha7_4_control"] = _AF6CCI0011_RD_CDR_HO._rdha7_4_control()
        allRegisters["rdha11_8_control"] = _AF6CCI0011_RD_CDR_HO._rdha11_8_control()
        allRegisters["rdha15_12_control"] = _AF6CCI0011_RD_CDR_HO._rdha15_12_control()
        allRegisters["rdha19_16_control"] = _AF6CCI0011_RD_CDR_HO._rdha19_16_control()
        allRegisters["rdha23_20_control"] = _AF6CCI0011_RD_CDR_HO._rdha23_20_control()
        allRegisters["rdha24data_control"] = _AF6CCI0011_RD_CDR_HO._rdha24data_control()
        allRegisters["rdha_hold63_32"] = _AF6CCI0011_RD_CDR_HO._rdha_hold63_32()
        allRegisters["rdindr_hold95_64"] = _AF6CCI0011_RD_CDR_HO._rdindr_hold95_64()
        allRegisters["rdindr_hold127_96"] = _AF6CCI0011_RD_CDR_HO._rdindr_hold127_96()
        allRegisters["cdr_per_chn_intr_en_ctrl"] = _AF6CCI0011_RD_CDR_HO._cdr_per_chn_intr_en_ctrl()
        allRegisters["cdr_per_chn_intr_stat"] = _AF6CCI0011_RD_CDR_HO._cdr_per_chn_intr_stat()
        allRegisters["cdr_per_chn_curr_stat"] = _AF6CCI0011_RD_CDR_HO._cdr_per_chn_curr_stat()
        allRegisters["cdr_per_chn_intr_or_stat"] = _AF6CCI0011_RD_CDR_HO._cdr_per_chn_intr_or_stat()
        allRegisters["cdr_per_stsvc_intr_or_stat"] = _AF6CCI0011_RD_CDR_HO._cdr_per_stsvc_intr_or_stat()
        allRegisters["cdr_per_stsvc_intr_en_ctrl"] = _AF6CCI0011_RD_CDR_HO._cdr_per_stsvc_intr_en_ctrl()
        allRegisters["cdr_input_seq_cap_control"] = _AF6CCI0011_RD_CDR_HO._cdr_input_seq_cap_control()
        allRegisters["cdr_input_seq_cap_status"] = _AF6CCI0011_RD_CDR_HO._cdr_input_seq_cap_status()
        return allRegisters

    class _cdr_cpu_hold_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR CPU  Reg Hold Control"
    
        def description(self):
            return "The register provides hold register for three word 32-bits MSB when CPU access to engine."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x030000 + HoldId"
            
        def startAddress(self):
            return 0x00030000
            
        def endAddress(self):
            return 0x00030002

        class _HoldReg0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HoldReg0"
            
            def description(self):
                return "Hold 32 bits"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HoldReg0"] = _AF6CCI0011_RD_CDR_HO._cdr_cpu_hold_ctrl._HoldReg0()
            return allFields

    class _cdr_timing_extref_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR Timing External reference control"
    
        def description(self):
            return "This register is used to configure for the 2Khz coefficient of two external reference signal in order to generate timing. The mean that, if the reference signal is 8Khz, the  coefficient must be configured 4 (I.e 8Khz  = 4x2Khz)"
            
        def width(self):
            return 48
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _Ext3N2k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 32
        
            def name(self):
                return "Ext3N2k"
            
            def description(self):
                return "The 2Khz coefficient of the third external reference signal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Ext2N2k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "Ext2N2k"
            
            def description(self):
                return "The 2Khz coefficient of the second external reference signal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _Ext1N2k(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Ext1N2k"
            
            def description(self):
                return "The 2Khz coefficient of the first external reference signal"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Ext3N2k"] = _AF6CCI0011_RD_CDR_HO._cdr_timing_extref_ctrl._Ext3N2k()
            allFields["Ext2N2k"] = _AF6CCI0011_RD_CDR_HO._cdr_timing_extref_ctrl._Ext2N2k()
            allFields["Ext1N2k"] = _AF6CCI0011_RD_CDR_HO._cdr_timing_extref_ctrl._Ext1N2k()
            return allFields

    class _cdr_sts_timing_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR STS Timing control"
    
        def description(self):
            return "This register is used to configure timing mode for per STS"
            
        def width(self):
            return 40
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0000100+STS"
            
        def startAddress(self):
            return 0x00000100
            
        def endAddress(self):
            return 0x0000011f

        class _slc7VC3EParEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 39
                
            def startBit(self):
                return 39
        
            def name(self):
                return "slc7VC3EParEn"
            
            def description(self):
                return "VC3 EPAR mode 0 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _slc6VC3EParEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 38
                
            def startBit(self):
                return 38
        
            def name(self):
                return "slc6VC3EParEn"
            
            def description(self):
                return "VC3 EPAR mode 0 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _slc5VC3EParEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 37
                
            def startBit(self):
                return 37
        
            def name(self):
                return "slc5VC3EParEn"
            
            def description(self):
                return "VC3 EPAR mode 0 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _slc4VC3EParEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 36
                
            def startBit(self):
                return 36
        
            def name(self):
                return "slc4VC3EParEn"
            
            def description(self):
                return "VC3 EPAR mode 0 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _slc3VC3EParEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 35
                
            def startBit(self):
                return 35
        
            def name(self):
                return "slc3VC3EParEn"
            
            def description(self):
                return "VC3 EPAR mode 0 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _slc2VC3EParEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 34
                
            def startBit(self):
                return 34
        
            def name(self):
                return "slc2VC3EParEn"
            
            def description(self):
                return "VC3 EPAR mode 0 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _slc1VC3EParEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 33
                
            def startBit(self):
                return 33
        
            def name(self):
                return "slc1VC3EParEn"
            
            def description(self):
                return "VC3 EPAR mode 0 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _slc0VC3EParEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 32
                
            def startBit(self):
                return 32
        
            def name(self):
                return "slc0VC3EParEn"
            
            def description(self):
                return "VC3 EPAR mode 0 0: Disable 1: Enable"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _slc7VC3TimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 28
        
            def name(self):
                return "slc7VC3TimeMode"
            
            def description(self):
                return "VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: CDR timing mode for CEP mode (ACR/DCR) 9: Line EXT#3 timing mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _slc6VC3TimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 24
        
            def name(self):
                return "slc6VC3TimeMode"
            
            def description(self):
                return "VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: CDR timing mode for CEP mode (ACR/DCR) 9: Line EXT#3 timing mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _slc5VC3TimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 20
        
            def name(self):
                return "slc5VC3TimeMode"
            
            def description(self):
                return "VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: CDR timing mode for CEP mode (ACR/DCR) 9: Line EXT#3 timing mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _slc4VC3TimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "slc4VC3TimeMode"
            
            def description(self):
                return "VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: CDR timing mode for CEP mode (ACR/DCR) 9: Line EXT#3 timing mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _slc3VC3TimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "slc3VC3TimeMode"
            
            def description(self):
                return "VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: CDR timing mode for CEP mode (ACR/DCR) 9: Line EXT#3 timing mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _slc2VC3TimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "slc2VC3TimeMode"
            
            def description(self):
                return "VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: CDR timing mode for CEP mode (ACR/DCR) 9: Line EXT#3 timing mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _slc1VC3TimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "slc1VC3TimeMode"
            
            def description(self):
                return "VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: CDR timing mode for CEP mode (ACR/DCR) 9: Line EXT#3 timing mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _slc0VC3TimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "slc0VC3TimeMode"
            
            def description(self):
                return "VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: CDR timing mode for CEP mode (ACR/DCR) 9: Line EXT#3 timing mode"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["slc7VC3EParEn"] = _AF6CCI0011_RD_CDR_HO._cdr_sts_timing_ctrl._slc7VC3EParEn()
            allFields["slc6VC3EParEn"] = _AF6CCI0011_RD_CDR_HO._cdr_sts_timing_ctrl._slc6VC3EParEn()
            allFields["slc5VC3EParEn"] = _AF6CCI0011_RD_CDR_HO._cdr_sts_timing_ctrl._slc5VC3EParEn()
            allFields["slc4VC3EParEn"] = _AF6CCI0011_RD_CDR_HO._cdr_sts_timing_ctrl._slc4VC3EParEn()
            allFields["slc3VC3EParEn"] = _AF6CCI0011_RD_CDR_HO._cdr_sts_timing_ctrl._slc3VC3EParEn()
            allFields["slc2VC3EParEn"] = _AF6CCI0011_RD_CDR_HO._cdr_sts_timing_ctrl._slc2VC3EParEn()
            allFields["slc1VC3EParEn"] = _AF6CCI0011_RD_CDR_HO._cdr_sts_timing_ctrl._slc1VC3EParEn()
            allFields["slc0VC3EParEn"] = _AF6CCI0011_RD_CDR_HO._cdr_sts_timing_ctrl._slc0VC3EParEn()
            allFields["slc7VC3TimeMode"] = _AF6CCI0011_RD_CDR_HO._cdr_sts_timing_ctrl._slc7VC3TimeMode()
            allFields["slc6VC3TimeMode"] = _AF6CCI0011_RD_CDR_HO._cdr_sts_timing_ctrl._slc6VC3TimeMode()
            allFields["slc5VC3TimeMode"] = _AF6CCI0011_RD_CDR_HO._cdr_sts_timing_ctrl._slc5VC3TimeMode()
            allFields["slc4VC3TimeMode"] = _AF6CCI0011_RD_CDR_HO._cdr_sts_timing_ctrl._slc4VC3TimeMode()
            allFields["slc3VC3TimeMode"] = _AF6CCI0011_RD_CDR_HO._cdr_sts_timing_ctrl._slc3VC3TimeMode()
            allFields["slc2VC3TimeMode"] = _AF6CCI0011_RD_CDR_HO._cdr_sts_timing_ctrl._slc2VC3TimeMode()
            allFields["slc1VC3TimeMode"] = _AF6CCI0011_RD_CDR_HO._cdr_sts_timing_ctrl._slc1VC3TimeMode()
            allFields["slc0VC3TimeMode"] = _AF6CCI0011_RD_CDR_HO._cdr_sts_timing_ctrl._slc0VC3TimeMode()
            return allFields

    class _cdr_acr_eng_timing_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR ACR Engine Timing control"
    
        def description(self):
            return "This register is used to configure timing mode for per STS"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0020800 + 64*slice + stsid"
            
        def startAddress(self):
            return 0x00020800
            
        def endAddress(self):
            return 0x00020bff

        class _VC4_4c(AtRegister.AtRegisterField):
            def stopBit(self):
                return 26
                
            def startBit(self):
                return 26
        
            def name(self):
                return "VC4_4c"
            
            def description(self):
                return "VC4_4c/TU3 mode 0: STS1/VC4 1: TU3/VC4-4c"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PktLen_ind(AtRegister.AtRegisterField):
            def stopBit(self):
                return 25
                
            def startBit(self):
                return 25
        
            def name(self):
                return "PktLen_ind"
            
            def description(self):
                return "The payload packet  length for jumbo frame"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _HoldValMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 24
                
            def startBit(self):
                return 24
        
            def name(self):
                return "HoldValMode"
            
            def description(self):
                return "Hold value mode of NCO, default value 0 0: Hardware calculated and auto update 1: Software calculated and update, hardware is disabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _SeqMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 23
        
            def name(self):
                return "SeqMode"
            
            def description(self):
                return "Sequence mode mode, default value 0 0: Wrap zero 1: Skip zero"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _LineType(AtRegister.AtRegisterField):
            def stopBit(self):
                return 22
                
            def startBit(self):
                return 20
        
            def name(self):
                return "LineType"
            
            def description(self):
                return "Line type mode 0-5: unused 6: STS1/TU3 7: VC4/VC4-4c"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PktLen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 4
        
            def name(self):
                return "PktLen"
            
            def description(self):
                return "The payload packet  length parameter to create a packet. SAToP mode: The number payload of bit. CESoPSN mode: The number of bit which converted to full DS1/E1 rate mode. In CESoPSN mode, the payload is assembled by NxDS0 with M frame, the value configured to this register is Mx256 bits for E1 mode, Mx193 bits for DS1 mode."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRTimeMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRTimeMode"
            
            def description(self):
                return "CDR time mode 0-7: unused 8: ACR timing mode 9: unused 10: DCR timing mode 11: unused"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["VC4_4c"] = _AF6CCI0011_RD_CDR_HO._cdr_acr_eng_timing_ctrl._VC4_4c()
            allFields["PktLen_ind"] = _AF6CCI0011_RD_CDR_HO._cdr_acr_eng_timing_ctrl._PktLen_ind()
            allFields["HoldValMode"] = _AF6CCI0011_RD_CDR_HO._cdr_acr_eng_timing_ctrl._HoldValMode()
            allFields["SeqMode"] = _AF6CCI0011_RD_CDR_HO._cdr_acr_eng_timing_ctrl._SeqMode()
            allFields["LineType"] = _AF6CCI0011_RD_CDR_HO._cdr_acr_eng_timing_ctrl._LineType()
            allFields["PktLen"] = _AF6CCI0011_RD_CDR_HO._cdr_acr_eng_timing_ctrl._PktLen()
            allFields["CDRTimeMode"] = _AF6CCI0011_RD_CDR_HO._cdr_acr_eng_timing_ctrl._CDRTimeMode()
            return allFields

    class _cdr_adj_state_stat(AtRegister.AtRegister):
        def name(self):
            return "CDR Adjust State status"
    
        def description(self):
            return "This register is used to store status or configure  some parameter of per CDR engine"
            
        def width(self):
            return 8
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0021000 + 64*slice + stsid"
            
        def startAddress(self):
            return 0x00021000
            
        def endAddress(self):
            return 0x000213ff

        class _Adjstate(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "Adjstate"
            
            def description(self):
                return "Adjust state 0: Load state 1: Wait state 2: Initialization state 3: Learn State 4: Rapid State 5: Lock State 6: Freeze State 7: Holdover State"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["Adjstate"] = _AF6CCI0011_RD_CDR_HO._cdr_adj_state_stat._Adjstate()
            return allFields

    class _cdr_adj_holdover_value_stat(AtRegister.AtRegister):
        def name(self):
            return "CDR Adjust Holdover value status"
    
        def description(self):
            return "This register is used to store status or configure  some parameter of per CDR engine"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0021800 + 64*slice + stsid"
            
        def startAddress(self):
            return 0x00021800
            
        def endAddress(self):
            return 0x00021bff

        class _HoldVal(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "HoldVal"
            
            def description(self):
                return "NCO Holdover value parameter STS1RATE = 32'd4246160849;   Resolution =    0.239"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["HoldVal"] = _AF6CCI0011_RD_CDR_HO._cdr_adj_holdover_value_stat._HoldVal()
            return allFields

    class _dcr_tx_eng_active_ctrl(AtRegister.AtRegister):
        def name(self):
            return "DCR TX Engine Active Control"
    
        def description(self):
            return "This register is used to activate the DCR TX Engine. Active high."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00010000
            
        def endAddress(self):
            return 0xffffffff

        class _data(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "data"
            
            def description(self):
                return "DCR TX Engine Active Control"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["data"] = _AF6CCI0011_RD_CDR_HO._dcr_tx_eng_active_ctrl._data()
            return allFields

    class _dcr_prc_src_sel_cfg(AtRegister.AtRegister):
        def name(self):
            return "DCR PRC Source Select Configuration"
    
        def description(self):
            return "This register is used to configure to select PRC source for PRC timer. The PRC clock selected must be less than 19.44 Mhz."
            
        def width(self):
            return 8
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00010001
            
        def endAddress(self):
            return 0xffffffff

        class _DcrPrcDirectMode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "DcrPrcDirectMode"
            
            def description(self):
                return "0: Select RTP frequency lower than 65535 and use value DCRRTPFreq as RTP frequency 1: Select RTP frequency value 155.52Mhz divide by DcrRtpFreqDivide. In this mode, below DcrPrcSourceSel[2:0] must set to 1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _DcrPrcSourceSel(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DcrPrcSourceSel"
            
            def description(self):
                return "PRC source selection. 0: PRC Reference Clock 1: System Clock 19Mhz 2: External Reference Clock 1. 3: External Reference Clock 2. 4: Ocn Line Clock Port 1 5: Ocn Line Clock Port 2 6: Ocn Line Clock Port 3 7: Ocn Line Clock Port 4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DcrPrcDirectMode"] = _AF6CCI0011_RD_CDR_HO._dcr_prc_src_sel_cfg._DcrPrcDirectMode()
            allFields["DcrPrcSourceSel"] = _AF6CCI0011_RD_CDR_HO._dcr_prc_src_sel_cfg._DcrPrcSourceSel()
            return allFields

    class _dcr_prd_freq_cfg(AtRegister.AtRegister):
        def name(self):
            return "DCR PRC Frequency Configuration"
    
        def description(self):
            return "This register is used to select RTP frequency 77.76Mhz or 155.52Mhz"
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00010002
            
        def endAddress(self):
            return 0xffffffff

        class _DcrRtpFreqDivide(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DcrRtpFreqDivide"
            
            def description(self):
                return "Dcr RTP Frequency = 155.52Mhz/ DcrRtpFreqDivide"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DcrRtpFreqDivide"] = _AF6CCI0011_RD_CDR_HO._dcr_prd_freq_cfg._DcrRtpFreqDivide()
            return allFields

    class _dcr_prc_freq_cfg(AtRegister.AtRegister):
        def name(self):
            return "DCR PRC Frequency Configuration"
    
        def description(self):
            return "This register is used to configure the frequency of PRC clock."
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0001000b
            
        def endAddress(self):
            return 0xffffffff

        class _DCRPrcFrequency(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DCRPrcFrequency"
            
            def description(self):
                return "Frequency of PRC clock. This is used to configure the frequency of PRC clock in Khz. Unit is Khz. Exp: The PRC clock is 19.44Mhz. This register will be configured to 19440."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DCRPrcFrequency"] = _AF6CCI0011_RD_CDR_HO._dcr_prc_freq_cfg._DCRPrcFrequency()
            return allFields

    class _dcr_rtp_freq_cfg(AtRegister.AtRegister):
        def name(self):
            return "DCR RTP Frequency Configuration"
    
        def description(self):
            return "This register is used to configure the frequency of PRC clock."
            
        def width(self):
            return 16
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0001000c
            
        def endAddress(self):
            return 0xffffffff

        class _DCRRTPFreq(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "DCRRTPFreq"
            
            def description(self):
                return "Frequency of RTP clock. This is used to configure the frequency of RTP clock in Khz. Unit is Khz. Exp: The RTP clock is 19.44Mhz. This register will be configured to 19440."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["DCRRTPFreq"] = _AF6CCI0011_RD_CDR_HO._dcr_rtp_freq_cfg._DCRRTPFreq()
            return allFields

    class _RAM_TimingGen_Parity_Force_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM TimingGen Parity Force Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000188
            
        def endAddress(self):
            return 0xffffffff

        class _CDRSTSTimingCtrl_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRSTSTimingCtrl_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"CDR STS Timing control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRSTSTimingCtrl_ParErrFrc"] = _AF6CCI0011_RD_CDR_HO._RAM_TimingGen_Parity_Force_Control._CDRSTSTimingCtrl_ParErrFrc()
            return allFields

    class _RAM_TimingGen_Parity_Diable_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM TimingGen Parity Disable Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000189
            
        def endAddress(self):
            return 0xffffffff

        class _CDRSTSTimingCtrl_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRSTSTimingCtrl_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"CDR STS Timing control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRSTSTimingCtrl_ParErrDis"] = _AF6CCI0011_RD_CDR_HO._RAM_TimingGen_Parity_Diable_Control._CDRSTSTimingCtrl_ParErrDis()
            return allFields

    class _RAM_TimingGen_Parity_Error_Sticky(AtRegister.AtRegister):
        def name(self):
            return "RAM TimingGen parity Error Sticky"
    
        def description(self):
            return "This register configures disable parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0000018a
            
        def endAddress(self):
            return 0xffffffff

        class _CDRSTSTimingCtrl_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRSTSTimingCtrl_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"CDR STS Timing control\""
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRSTSTimingCtrl_ParErrStk"] = _AF6CCI0011_RD_CDR_HO._RAM_TimingGen_Parity_Error_Sticky._CDRSTSTimingCtrl_ParErrStk()
            return allFields

    class _RAM_ACR_Parity_Force_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM ACR Parity Force Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0002000c
            
        def endAddress(self):
            return 0xffffffff

        class _CDRACRTimingCtrlSlc7_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "CDRACRTimingCtrlSlc7_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"CDR ACR Engine Timing control\" Slice7"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrlSlc6_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "CDRACRTimingCtrlSlc6_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"CDR ACR Engine Timing control\" Slice6"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrlSlc5_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "CDRACRTimingCtrlSlc5_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"CDR ACR Engine Timing control\" Slice5"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrlSlc4_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "CDRACRTimingCtrlSlc4_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"CDR ACR Engine Timing control\" Slice4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrlSlc3_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "CDRACRTimingCtrlSlc3_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"CDR ACR Engine Timing control\" Slice3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrlSlc2_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "CDRACRTimingCtrlSlc2_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"CDR ACR Engine Timing control\" Slice2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrlSlc1_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "CDRACRTimingCtrlSlc1_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"CDR ACR Engine Timing control\" Slice1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrlSlc0_ParErrFrc(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRACRTimingCtrlSlc0_ParErrFrc"
            
            def description(self):
                return "Force parity For RAM Control \"CDR ACR Engine Timing control\" Slice0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRACRTimingCtrlSlc7_ParErrFrc"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Force_Control._CDRACRTimingCtrlSlc7_ParErrFrc()
            allFields["CDRACRTimingCtrlSlc6_ParErrFrc"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Force_Control._CDRACRTimingCtrlSlc6_ParErrFrc()
            allFields["CDRACRTimingCtrlSlc5_ParErrFrc"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Force_Control._CDRACRTimingCtrlSlc5_ParErrFrc()
            allFields["CDRACRTimingCtrlSlc4_ParErrFrc"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Force_Control._CDRACRTimingCtrlSlc4_ParErrFrc()
            allFields["CDRACRTimingCtrlSlc3_ParErrFrc"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Force_Control._CDRACRTimingCtrlSlc3_ParErrFrc()
            allFields["CDRACRTimingCtrlSlc2_ParErrFrc"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Force_Control._CDRACRTimingCtrlSlc2_ParErrFrc()
            allFields["CDRACRTimingCtrlSlc1_ParErrFrc"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Force_Control._CDRACRTimingCtrlSlc1_ParErrFrc()
            allFields["CDRACRTimingCtrlSlc0_ParErrFrc"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Force_Control._CDRACRTimingCtrlSlc0_ParErrFrc()
            return allFields

    class _RAM_ACR_Parity_Disable_Control(AtRegister.AtRegister):
        def name(self):
            return "RAM ACR Parity Disable Control"
    
        def description(self):
            return "This register configures force parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0002000d
            
        def endAddress(self):
            return 0xffffffff

        class _CDRACRTimingCtrlSlc7_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "CDRACRTimingCtrlSlc7_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"CDR ACR Engine Timing control\" Slice7"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrlSlc6_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "CDRACRTimingCtrlSlc6_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"CDR ACR Engine Timing control\" Slice6"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrlSlc5_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "CDRACRTimingCtrlSlc5_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"CDR ACR Engine Timing control\" Slice5"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrlSlc4_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "CDRACRTimingCtrlSlc4_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"CDR ACR Engine Timing control\" Slice4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrlSlc3_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "CDRACRTimingCtrlSlc3_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"CDR ACR Engine Timing control\" Slice3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrlSlc2_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "CDRACRTimingCtrlSlc2_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"CDR ACR Engine Timing control\" Slice2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrlSlc1_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "CDRACRTimingCtrlSlc1_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"CDR ACR Engine Timing control\" Slice1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrlSlc0_ParErrDis(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRACRTimingCtrlSlc0_ParErrDis"
            
            def description(self):
                return "Disable parity For RAM Control \"CDR ACR Engine Timing control\" Slice0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRACRTimingCtrlSlc7_ParErrDis"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Disable_Control._CDRACRTimingCtrlSlc7_ParErrDis()
            allFields["CDRACRTimingCtrlSlc6_ParErrDis"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Disable_Control._CDRACRTimingCtrlSlc6_ParErrDis()
            allFields["CDRACRTimingCtrlSlc5_ParErrDis"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Disable_Control._CDRACRTimingCtrlSlc5_ParErrDis()
            allFields["CDRACRTimingCtrlSlc4_ParErrDis"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Disable_Control._CDRACRTimingCtrlSlc4_ParErrDis()
            allFields["CDRACRTimingCtrlSlc3_ParErrDis"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Disable_Control._CDRACRTimingCtrlSlc3_ParErrDis()
            allFields["CDRACRTimingCtrlSlc2_ParErrDis"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Disable_Control._CDRACRTimingCtrlSlc2_ParErrDis()
            allFields["CDRACRTimingCtrlSlc1_ParErrDis"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Disable_Control._CDRACRTimingCtrlSlc1_ParErrDis()
            allFields["CDRACRTimingCtrlSlc0_ParErrDis"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Disable_Control._CDRACRTimingCtrlSlc0_ParErrDis()
            return allFields

    class _RAM_ACR_Parity_Error_Sticky(AtRegister.AtRegister):
        def name(self):
            return "RAM ACR parity Error Sticky"
    
        def description(self):
            return "This register configures disable parity for internal RAM"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x0002000e
            
        def endAddress(self):
            return 0xffffffff

        class _CDRACRTimingCtrlSlc7_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 7
        
            def name(self):
                return "CDRACRTimingCtrlSlc7_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"CDR ACR Engine Timing control\" Slice7"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrlSlc6_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "CDRACRTimingCtrlSlc6_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"CDR ACR Engine Timing control\" Slice6"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrlSlc5_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "CDRACRTimingCtrlSlc5_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"CDR ACR Engine Timing control\" Slice5"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrlSlc4_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "CDRACRTimingCtrlSlc4_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"CDR ACR Engine Timing control\" Slice4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrlSlc3_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "CDRACRTimingCtrlSlc3_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"CDR ACR Engine Timing control\" Slice3"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrlSlc2_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "CDRACRTimingCtrlSlc2_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"CDR ACR Engine Timing control\" Slice2"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrlSlc1_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "CDRACRTimingCtrlSlc1_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"CDR ACR Engine Timing control\" Slice1"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _CDRACRTimingCtrlSlc0_ParErrStk(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRACRTimingCtrlSlc0_ParErrStk"
            
            def description(self):
                return "Error parity For RAM Control \"CDR ACR Engine Timing control\" Slice0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRACRTimingCtrlSlc7_ParErrStk"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Error_Sticky._CDRACRTimingCtrlSlc7_ParErrStk()
            allFields["CDRACRTimingCtrlSlc6_ParErrStk"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Error_Sticky._CDRACRTimingCtrlSlc6_ParErrStk()
            allFields["CDRACRTimingCtrlSlc5_ParErrStk"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Error_Sticky._CDRACRTimingCtrlSlc5_ParErrStk()
            allFields["CDRACRTimingCtrlSlc4_ParErrStk"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Error_Sticky._CDRACRTimingCtrlSlc4_ParErrStk()
            allFields["CDRACRTimingCtrlSlc3_ParErrStk"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Error_Sticky._CDRACRTimingCtrlSlc3_ParErrStk()
            allFields["CDRACRTimingCtrlSlc2_ParErrStk"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Error_Sticky._CDRACRTimingCtrlSlc2_ParErrStk()
            allFields["CDRACRTimingCtrlSlc1_ParErrStk"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Error_Sticky._CDRACRTimingCtrlSlc1_ParErrStk()
            allFields["CDRACRTimingCtrlSlc0_ParErrStk"] = _AF6CCI0011_RD_CDR_HO._RAM_ACR_Parity_Error_Sticky._CDRACRTimingCtrlSlc0_ParErrStk()
            return allFields

    class _rdha3_0_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit3_0 Control"
    
        def description(self):
            return "This register is used to send HA read address bit3_0 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x30200 + HaAddr3_0"
            
        def startAddress(self):
            return 0x00030200
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr3_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr3_0"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr3_0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr3_0"] = _AF6CCI0011_RD_CDR_HO._rdha3_0_control._ReadAddr3_0()
            return allFields

    class _rdha7_4_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit7_4 Control"
    
        def description(self):
            return "This register is used to send HA read address bit7_4 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x30210 + HaAddr7_4"
            
        def startAddress(self):
            return 0x00030210
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr7_4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr7_4"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr7_4"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr7_4"] = _AF6CCI0011_RD_CDR_HO._rdha7_4_control._ReadAddr7_4()
            return allFields

    class _rdha11_8_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit11_8 Control"
    
        def description(self):
            return "This register is used to send HA read address bit11_8 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x30220 + HaAddr11_8"
            
        def startAddress(self):
            return 0x00030220
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr11_8(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr11_8"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr11_8"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr11_8"] = _AF6CCI0011_RD_CDR_HO._rdha11_8_control._ReadAddr11_8()
            return allFields

    class _rdha15_12_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit15_12 Control"
    
        def description(self):
            return "This register is used to send HA read address bit15_12 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x30230 + HaAddr15_12"
            
        def startAddress(self):
            return 0x00030230
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr15_12(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr15_12"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr15_12"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr15_12"] = _AF6CCI0011_RD_CDR_HO._rdha15_12_control._ReadAddr15_12()
            return allFields

    class _rdha19_16_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit19_16 Control"
    
        def description(self):
            return "This register is used to send HA read address bit19_16 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x30240 + HaAddr19_16"
            
        def startAddress(self):
            return 0x00030240
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr19_16(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr19_16"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr19_16"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr19_16"] = _AF6CCI0011_RD_CDR_HO._rdha19_16_control._ReadAddr19_16()
            return allFields

    class _rdha23_20_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit23_20 Control"
    
        def description(self):
            return "This register is used to send HA read address bit23_20 to HA engine"
            
        def width(self):
            return 20
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x30250 + HaAddr23_20"
            
        def startAddress(self):
            return 0x00030250
            
        def endAddress(self):
            return 0xffffffff

        class _ReadAddr23_20(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadAddr23_20"
            
            def description(self):
                return "Read value will be 0x01000 plus HaAddr23_20"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadAddr23_20"] = _AF6CCI0011_RD_CDR_HO._rdha23_20_control._ReadAddr23_20()
            return allFields

    class _rdha24data_control(AtRegister.AtRegister):
        def name(self):
            return "Read HA Address Bit24 and Data Control"
    
        def description(self):
            return "This register is used to send HA read address bit24 to HA engine to read data"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x30260 + HaAddr24"
            
        def startAddress(self):
            return 0x00030260
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData31_0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData31_0"
            
            def description(self):
                return "HA read data bit31_0"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData31_0"] = _AF6CCI0011_RD_CDR_HO._rdha24data_control._ReadHaData31_0()
            return allFields

    class _rdha_hold63_32(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data63_32"
    
        def description(self):
            return "This register is used to read HA dword2 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00030270
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData63_32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData63_32"
            
            def description(self):
                return "HA read data bit63_32"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData63_32"] = _AF6CCI0011_RD_CDR_HO._rdha_hold63_32._ReadHaData63_32()
            return allFields

    class _rdindr_hold95_64(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data95_64"
    
        def description(self):
            return "This register is used to read HA dword3 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00030271
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData95_64(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData95_64"
            
            def description(self):
                return "HA read data bit95_64"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData95_64"] = _AF6CCI0011_RD_CDR_HO._rdindr_hold95_64._ReadHaData95_64()
            return allFields

    class _rdindr_hold127_96(AtRegister.AtRegister):
        def name(self):
            return "Read HA Hold Data127_96"
    
        def description(self):
            return "This register is used to read HA dword4 of data."
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00030272
            
        def endAddress(self):
            return 0xffffffff

        class _ReadHaData127_96(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "ReadHaData127_96"
            
            def description(self):
                return "HA read data bit127_96"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ReadHaData127_96"] = _AF6CCI0011_RD_CDR_HO._rdindr_hold127_96._ReadHaData127_96()
            return allFields

    class _cdr_per_chn_intr_en_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR per Channel Interrupt Enable Control"
    
        def description(self):
            return "This is the per Channel interrupt enable of CDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x00025000 +  StsID*32 + VtnID"
            
        def startAddress(self):
            return 0x00025000
            
        def endAddress(self):
            return 0x000251ff

        class _CDRUnlokcedIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRUnlokcedIntrEn"
            
            def description(self):
                return "Set 1 to enable change UnLocked te event to generate an interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRUnlokcedIntrEn"] = _AF6CCI0011_RD_CDR_HO._cdr_per_chn_intr_en_ctrl._CDRUnlokcedIntrEn()
            return allFields

    class _cdr_per_chn_intr_stat(AtRegister.AtRegister):
        def name(self):
            return "CDR per Channel Interrupt Status"
    
        def description(self):
            return "This is the per Channel interrupt tus of CDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00025200 +  StsID*32 + VtnID"
            
        def startAddress(self):
            return 0x00025200
            
        def endAddress(self):
            return 0x000253ff

        class _CDRUnLockedIntr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRUnLockedIntr"
            
            def description(self):
                return "Set 1 if there is a change in UnLocked the event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRUnLockedIntr"] = _AF6CCI0011_RD_CDR_HO._cdr_per_chn_intr_stat._CDRUnLockedIntr()
            return allFields

    class _cdr_per_chn_curr_stat(AtRegister.AtRegister):
        def name(self):
            return "CDR per Channel Current Status"
    
        def description(self):
            return "This is the per Channel Current tus of CDR"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00025400 +  StsID*32 + VtnID"
            
        def startAddress(self):
            return 0x00025400
            
        def endAddress(self):
            return 0x000255ff

        class _CDRUnLockedCurrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRUnLockedCurrSta"
            
            def description(self):
                return "Current tus of UnLocked event."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRUnLockedCurrSta"] = _AF6CCI0011_RD_CDR_HO._cdr_per_chn_curr_stat._CDRUnLockedCurrSta()
            return allFields

    class _cdr_per_chn_intr_or_stat(AtRegister.AtRegister):
        def name(self):
            return "CDR per Channel Interrupt OR Status"
    
        def description(self):
            return "The register consists of 32 bits for 32 VT/TUs of the related STS/VC in the CDR. Each bit is used to store Interrupt OR tus of the related DS1/E1."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return "0x00025600 +  StsID"
            
        def startAddress(self):
            return 0x00025600
            
        def endAddress(self):
            return 0x0002560f

        class _CDRVtIntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRVtIntrOrSta"
            
            def description(self):
                return "Set to 1 if any interrupt status bit of corresponding DS1/E1 is set and its interrupt is enabled."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRVtIntrOrSta"] = _AF6CCI0011_RD_CDR_HO._cdr_per_chn_intr_or_stat._CDRVtIntrOrSta()
            return allFields

    class _cdr_per_stsvc_intr_or_stat(AtRegister.AtRegister):
        def name(self):
            return "CDR per STS/VC Interrupt OR Status"
    
        def description(self):
            return "The register consists of 16 bits for 16 STS/VCs of the CDR. Each bit is used to store Interrupt OR tus of the related STS/VC."
            
        def width(self):
            return 32
        
        def type(self):
            return "Interrupt"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000257ff
            
        def endAddress(self):
            return 0xffffffff

        class _CDRStsIntrOrSta(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRStsIntrOrSta"
            
            def description(self):
                return "Set to 1 if any interrupt status bit of corresponding STS/VC is set and its interrupt is enabled"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRStsIntrOrSta"] = _AF6CCI0011_RD_CDR_HO._cdr_per_stsvc_intr_or_stat._CDRStsIntrOrSta()
            return allFields

    class _cdr_per_stsvc_intr_en_ctrl(AtRegister.AtRegister):
        def name(self):
            return "CDR per STS/VC Interrupt Enable Control"
    
        def description(self):
            return "The register consists of 16 interrupt enable bits for 16 STS/VCs in the Rx DS1/E1/J1 Framer."
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x000257fe
            
        def endAddress(self):
            return 0xffffffff

        class _CDRStsIntrEn(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CDRStsIntrEn"
            
            def description(self):
                return "Set to 1 to enable the related STS/VC to generate interrupt."
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CDRStsIntrEn"] = _AF6CCI0011_RD_CDR_HO._cdr_per_stsvc_intr_en_ctrl._CDRStsIntrEn()
            return allFields

    class _cdr_input_seq_cap_control(AtRegister.AtRegister):
        def name(self):
            return "CDR Input Sequence Capture Control"
    
        def description(self):
            return "This register is used to control capture input information from CLA to CDR engine"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00023007
            
        def endAddress(self):
            return 0xffffffff

        class _CapEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 16
                
            def startBit(self):
                return 16
        
            def name(self):
                return "CapEnb"
            
            def description(self):
                return "Trigger 1->0 to capture"
            
            def type(self):
                return ""
            
            def resetValue(self):
                return 0xffffffff

        class _CapID(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CapID"
            
            def description(self):
                return "Same value of bit[13:0] of Classify Per Pseudowire Identification to CDR Control Register"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CapEnb"] = _AF6CCI0011_RD_CDR_HO._cdr_input_seq_cap_control._CapEnb()
            allFields["CapID"] = _AF6CCI0011_RD_CDR_HO._cdr_input_seq_cap_control._CapID()
            return allFields

    class _cdr_input_seq_cap_status(AtRegister.AtRegister):
        def name(self):
            return "CDR Input Sequence Capture Status"
    
        def description(self):
            return "This register is used to capture input information from CLA to CDR engine"
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0023100 + word"
            
        def startAddress(self):
            return 0x00023100
            
        def endAddress(self):
            return 0xffffffff

        class _CLA2CDRHOEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 65
                
            def startBit(self):
                return 65
        
            def name(self):
                return "CLA2CDRHOEnb"
            
            def description(self):
                return "CLA2CDR HO path Enable"
            
            def type(self):
                return ""
            
            def resetValue(self):
                return 0xffffffff

        class _CLA2CDRError(AtRegister.AtRegisterField):
            def stopBit(self):
                return 64
                
            def startBit(self):
                return 64
        
            def name(self):
                return "CLA2CDRError"
            
            def description(self):
                return "CLA2CDR Error due to Lbit only"
            
            def type(self):
                return ""
            
            def resetValue(self):
                return 0xffffffff

        class _RTPTSVAl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 32
        
            def name(self):
                return "RTPTSVAl"
            
            def description(self):
                return "RTP Sequence, TS_Offset = T1-T0"
            
            def type(self):
                return ""
            
            def resetValue(self):
                return 0xffffffff

        class _RTPSeqNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 16
        
            def name(self):
                return "RTPSeqNum"
            
            def description(self):
                return "RTP Sequence"
            
            def type(self):
                return ""
            
            def resetValue(self):
                return 0xffffffff

        class _CWSeqNum(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CWSeqNum"
            
            def description(self):
                return "Control Word Sequence"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["CLA2CDRHOEnb"] = _AF6CCI0011_RD_CDR_HO._cdr_input_seq_cap_status._CLA2CDRHOEnb()
            allFields["CLA2CDRError"] = _AF6CCI0011_RD_CDR_HO._cdr_input_seq_cap_status._CLA2CDRError()
            allFields["RTPTSVAl"] = _AF6CCI0011_RD_CDR_HO._cdr_input_seq_cap_status._RTPTSVAl()
            allFields["RTPSeqNum"] = _AF6CCI0011_RD_CDR_HO._cdr_input_seq_cap_status._RTPSeqNum()
            allFields["CWSeqNum"] = _AF6CCI0011_RD_CDR_HO._cdr_input_seq_cap_status._CWSeqNum()
            return allFields

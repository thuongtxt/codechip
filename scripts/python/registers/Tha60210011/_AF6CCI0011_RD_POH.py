import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0011_RD_POH(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["pcfg_trshglbctr"] = _AF6CCI0011_RD_POH._pcfg_trshglbctr()
        allRegisters["pohffarbi"] = _AF6CCI0011_RD_POH._pohffarbi()
        allRegisters["pohffarbi"] = _AF6CCI0011_RD_POH._pohffarbi()
        allRegisters["pohffarbi"] = _AF6CCI0011_RD_POH._pohffarbi()
        allRegisters["pohcpecontrol"] = _AF6CCI0011_RD_POH._pohcpecontrol()
        allRegisters["pohcpecontrol"] = _AF6CCI0011_RD_POH._pohcpecontrol()
        allRegisters["pohcpecontrol"] = _AF6CCI0011_RD_POH._pohcpecontrol()
        allRegisters["pohmsgstsexp"] = _AF6CCI0011_RD_POH._pohmsgstsexp()
        allRegisters["pohmsgstscur"] = _AF6CCI0011_RD_POH._pohmsgstscur()
        allRegisters["pohmsgtu3exp"] = _AF6CCI0011_RD_POH._pohmsgtu3exp()
        allRegisters["pohmsgtu3cur"] = _AF6CCI0011_RD_POH._pohmsgtu3cur()
        allRegisters["pohmsgvtexp"] = _AF6CCI0011_RD_POH._pohmsgvtexp()
        allRegisters["pohmsgvtcur"] = _AF6CCI0011_RD_POH._pohmsgvtcur()
        allRegisters["pohmsgstsins"] = _AF6CCI0011_RD_POH._pohmsgstsins()
        allRegisters["pohmsgtu3ins"] = _AF6CCI0011_RD_POH._pohmsgtu3ins()
        allRegisters["pohmsgvtins"] = _AF6CCI0011_RD_POH._pohmsgvtins()
        return allRegisters

    class _pcfg_trshglbctr(AtRegister.AtRegister):
        def name(self):
            return "POH Threshold Global Control"
    
        def description(self):
            return "This register is used to set Threshold for stable detection."
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0xffffffff

        class _V5RFIStbTrsh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 27
                
            def startBit(self):
                return 24
        
            def name(self):
                return "V5RFIStbTrsh"
            
            def description(self):
                return "V5 RDI Stable Thershold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _V5RDIStbTrsh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 20
        
            def name(self):
                return "V5RDIStbTrsh"
            
            def description(self):
                return "V5 RDI Stable Thershold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _V5SlbStbTrsh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 19
                
            def startBit(self):
                return 16
        
            def name(self):
                return "V5SlbStbTrsh"
            
            def description(self):
                return "V5 Signal Lable Stable Thershold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _G1RDIStbTrsh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "G1RDIStbTrsh"
            
            def description(self):
                return "G1 RDI Path Stable Thershold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _C2PlmStbTrsh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "C2PlmStbTrsh"
            
            def description(self):
                return "C2 Path Signal Lable Stable Thershold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _JnStbTrsh(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "JnStbTrsh"
            
            def description(self):
                return "J1/J2 Message Stable Threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _debound(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "debound"
            
            def description(self):
                return "Debound Threshold"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["V5RFIStbTrsh"] = _AF6CCI0011_RD_POH._pcfg_trshglbctr._V5RFIStbTrsh()
            allFields["V5RDIStbTrsh"] = _AF6CCI0011_RD_POH._pcfg_trshglbctr._V5RDIStbTrsh()
            allFields["V5SlbStbTrsh"] = _AF6CCI0011_RD_POH._pcfg_trshglbctr._V5SlbStbTrsh()
            allFields["G1RDIStbTrsh"] = _AF6CCI0011_RD_POH._pcfg_trshglbctr._G1RDIStbTrsh()
            allFields["C2PlmStbTrsh"] = _AF6CCI0011_RD_POH._pcfg_trshglbctr._C2PlmStbTrsh()
            allFields["JnStbTrsh"] = _AF6CCI0011_RD_POH._pcfg_trshglbctr._JnStbTrsh()
            allFields["debound"] = _AF6CCI0011_RD_POH._pcfg_trshglbctr._debound()
            return allFields

    class _pohffarbi(AtRegister.AtRegister):
        def name(self):
            return "POH Hi-order Path Over Head Grabber"
    
        def description(self):
            return "This register is used to grabber Hi-Order Path Overhead"
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x02_4000 + $sliceid * 48 + $stsid"
            
        def startAddress(self):
            return 0x00024000
            
        def endAddress(self):
            return 0xffffffff

        class _K3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 56
        
            def name(self):
                return "K3"
            
            def description(self):
                return "K3 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _F3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 55
                
            def startBit(self):
                return 48
        
            def name(self):
                return "F3"
            
            def description(self):
                return "F3 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _H4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 40
        
            def name(self):
                return "H4"
            
            def description(self):
                return "H4 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _F2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 39
                
            def startBit(self):
                return 32
        
            def name(self):
                return "F2"
            
            def description(self):
                return "F2 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _G1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "G1"
            
            def description(self):
                return "G1 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _C2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "C2"
            
            def description(self):
                return "C2 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _N1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "N1"
            
            def description(self):
                return "N1 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _J1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "J1"
            
            def description(self):
                return "J1 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["K3"] = _AF6CCI0011_RD_POH._pohffarbi._K3()
            allFields["F3"] = _AF6CCI0011_RD_POH._pohffarbi._F3()
            allFields["H4"] = _AF6CCI0011_RD_POH._pohffarbi._H4()
            allFields["F2"] = _AF6CCI0011_RD_POH._pohffarbi._F2()
            allFields["G1"] = _AF6CCI0011_RD_POH._pohffarbi._G1()
            allFields["C2"] = _AF6CCI0011_RD_POH._pohffarbi._C2()
            allFields["N1"] = _AF6CCI0011_RD_POH._pohffarbi._N1()
            allFields["J1"] = _AF6CCI0011_RD_POH._pohffarbi._J1()
            return allFields

    class _pohffarbi(AtRegister.AtRegister):
        def name(self):
            return "POH Hi-order Path Over Head Grabber"
    
        def description(self):
            return "This register is used to grabber Lo-Order Tu3 Path Overhead"
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x02_6000 + $sliceid * 1344 + $stsid * 28"
            
        def startAddress(self):
            return 0x00026000
            
        def endAddress(self):
            return 0xffffffff

        class _K3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 56
        
            def name(self):
                return "K3"
            
            def description(self):
                return "K3 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _F3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 55
                
            def startBit(self):
                return 48
        
            def name(self):
                return "F3"
            
            def description(self):
                return "F3 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _H4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 47
                
            def startBit(self):
                return 40
        
            def name(self):
                return "H4"
            
            def description(self):
                return "H4 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _F2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 39
                
            def startBit(self):
                return 32
        
            def name(self):
                return "F2"
            
            def description(self):
                return "F2 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _G1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "G1"
            
            def description(self):
                return "G1 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _C2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "C2"
            
            def description(self):
                return "C2 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _N1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "N1"
            
            def description(self):
                return "N1 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _J1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "J1"
            
            def description(self):
                return "J1 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["K3"] = _AF6CCI0011_RD_POH._pohffarbi._K3()
            allFields["F3"] = _AF6CCI0011_RD_POH._pohffarbi._F3()
            allFields["H4"] = _AF6CCI0011_RD_POH._pohffarbi._H4()
            allFields["F2"] = _AF6CCI0011_RD_POH._pohffarbi._F2()
            allFields["G1"] = _AF6CCI0011_RD_POH._pohffarbi._G1()
            allFields["C2"] = _AF6CCI0011_RD_POH._pohffarbi._C2()
            allFields["N1"] = _AF6CCI0011_RD_POH._pohffarbi._N1()
            allFields["J1"] = _AF6CCI0011_RD_POH._pohffarbi._J1()
            return allFields

    class _pohffarbi(AtRegister.AtRegister):
        def name(self):
            return "POH Hi-order Path Over Head Grabber"
    
        def description(self):
            return "This register is used to grabber Lo-Order Tu3 Path Overhead"
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x02_6000 + $sliceid * 1344 + $stsid * 28 + $vtid"
            
        def startAddress(self):
            return 0x00026000
            
        def endAddress(self):
            return 0xffffffff

        class _N2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "N2"
            
            def description(self):
                return "N2 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _K4(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "K4"
            
            def description(self):
                return "K4 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _J2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "J2"
            
            def description(self):
                return "J2 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        class _V5(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "V5"
            
            def description(self):
                return "V5 byte"
            
            def type(self):
                return "RO"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["N2"] = _AF6CCI0011_RD_POH._pohffarbi._N2()
            allFields["K4"] = _AF6CCI0011_RD_POH._pohffarbi._K4()
            allFields["J2"] = _AF6CCI0011_RD_POH._pohffarbi._J2()
            allFields["V5"] = _AF6CCI0011_RD_POH._pohffarbi._V5()
            return allFields

    class _pohcpecontrol(AtRegister.AtRegister):
        def name(self):
            return "POH CPE STS Control Register"
    
        def description(self):
            return "This register is used to configure the POH Hi-order Path Monitoring."
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x02_8000 + 0x00_1F80 + $sliceid * 48 + $stsid"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _ERDIenb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "ERDIenb"
            
            def description(self):
                return "Enable E-RDI"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PslExp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PslExp"
            
            def description(self):
                return "C2 Expected Path Signal Lable Value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _J1MonEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "J1MonEnb"
            
            def description(self):
                return "Enable Monitor J1 byte"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _J1mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "J1mode"
            
            def description(self):
                return "0: 1Byte 1:16Byte 2:64byte 3:Floating"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ERDIenb"] = _AF6CCI0011_RD_POH._pohcpecontrol._ERDIenb()
            allFields["PslExp"] = _AF6CCI0011_RD_POH._pohcpecontrol._PslExp()
            allFields["J1MonEnb"] = _AF6CCI0011_RD_POH._pohcpecontrol._J1MonEnb()
            allFields["J1mode"] = _AF6CCI0011_RD_POH._pohcpecontrol._J1mode()
            return allFields

    class _pohcpecontrol(AtRegister.AtRegister):
        def name(self):
            return "POH CPE STS Control Register"
    
        def description(self):
            return "This register is used to configure the POH Lo-order TU3 Path Monitoring."
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x02_8000 + $sliceid * 1344 + $stsid * 28"
            
        def startAddress(self):
            return 0x00028000
            
        def endAddress(self):
            return 0xffffffff

        class _ERDIenb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 11
        
            def name(self):
                return "ERDIenb"
            
            def description(self):
                return "Enable E-RDI"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _PslExp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 10
                
            def startBit(self):
                return 3
        
            def name(self):
                return "PslExp"
            
            def description(self):
                return "C2 Expected Path Signal Lable Value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _J1MonEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "J1MonEnb"
            
            def description(self):
                return "Enable Monitor J1 byte"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _J1mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "J1mode"
            
            def description(self):
                return "0: 1Byte 1:16Byte 2:64byte 3:Floating"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ERDIenb"] = _AF6CCI0011_RD_POH._pohcpecontrol._ERDIenb()
            allFields["PslExp"] = _AF6CCI0011_RD_POH._pohcpecontrol._PslExp()
            allFields["J1MonEnb"] = _AF6CCI0011_RD_POH._pohcpecontrol._J1MonEnb()
            allFields["J1mode"] = _AF6CCI0011_RD_POH._pohcpecontrol._J1mode()
            return allFields

    class _pohcpecontrol(AtRegister.AtRegister):
        def name(self):
            return "POH CPE STS Control Register"
    
        def description(self):
            return "This register is used to configure the POH Lo-order TU3 Path Monitoring."
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x02_8000 + $sliceid * 1344 + $stsid * 28"
            
        def startAddress(self):
            return 0x00028000
            
        def endAddress(self):
            return 0xffffffff

        class _ERDIenb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "ERDIenb"
            
            def description(self):
                return "Enable E-RDI"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _VslExp(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 3
        
            def name(self):
                return "VslExp"
            
            def description(self):
                return "V5 Expected Path Signal Lable Value"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _J2MonEnb(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "J2MonEnb"
            
            def description(self):
                return "Enable Monitor J2 byte"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        class _J2mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "J2mode"
            
            def description(self):
                return "0: 1Byte 1:16Byte 2:64byte 3:Floating"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["ERDIenb"] = _AF6CCI0011_RD_POH._pohcpecontrol._ERDIenb()
            allFields["VslExp"] = _AF6CCI0011_RD_POH._pohcpecontrol._VslExp()
            allFields["J2MonEnb"] = _AF6CCI0011_RD_POH._pohcpecontrol._J2MonEnb()
            allFields["J2mode"] = _AF6CCI0011_RD_POH._pohcpecontrol._J2mode()
            return allFields

    class _pohmsgstsexp(AtRegister.AtRegister):
        def name(self):
            return "POH CPE J1 Expected Message buffer"
    
        def description(self):
            return "The J1 Expected Message Buffer."
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x08_0000 + 0x03_0000 + $sliceid * 240 + $stsid * 8 + $msgid"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _J1ExpMsg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 0
        
            def name(self):
                return "J1ExpMsg"
            
            def description(self):
                return "J1 Expected Message"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["J1ExpMsg"] = _AF6CCI0011_RD_POH._pohmsgstsexp._J1ExpMsg()
            return allFields

    class _pohmsgstscur(AtRegister.AtRegister):
        def name(self):
            return "POH CPE J1 Expected Message buffer"
    
        def description(self):
            return "The J1 Current Message Buffer."
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x08_0000 + 0x03_1000 + $sliceid * 240 + $stsid * 8 + $msgid"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _J1CurMsg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 0
        
            def name(self):
                return "J1CurMsg"
            
            def description(self):
                return "J1 Current Message"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["J1CurMsg"] = _AF6CCI0011_RD_POH._pohmsgstscur._J1CurMsg()
            return allFields

    class _pohmsgtu3exp(AtRegister.AtRegister):
        def name(self):
            return "POH CPE J1 TU3 Expected Message buffer"
    
        def description(self):
            return "The J1 TU3 Expected Message Buffer."
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x08_0000 + $sliceid * 10752 + $stsid * 224 + $msgid"
            
        def startAddress(self):
            return 0x00080000
            
        def endAddress(self):
            return 0xffffffff

        class _J1Tu3ExpMsg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 0
        
            def name(self):
                return "J1Tu3ExpMsg"
            
            def description(self):
                return "J1 TU3 Expected Message"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["J1Tu3ExpMsg"] = _AF6CCI0011_RD_POH._pohmsgtu3exp._J1Tu3ExpMsg()
            return allFields

    class _pohmsgtu3cur(AtRegister.AtRegister):
        def name(self):
            return "POH CPE J1 TU3 Current Message buffer"
    
        def description(self):
            return "The J1 TU3 Current Message Buffer."
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x08_0000 + 0x01_0000 + $sliceid * 10752 + $stsid * 224 +$msgid"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _J1Tu3CurMsg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 0
        
            def name(self):
                return "J1Tu3CurMsg"
            
            def description(self):
                return "J1 TU3 Current Message"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["J1Tu3CurMsg"] = _AF6CCI0011_RD_POH._pohmsgtu3cur._J1Tu3CurMsg()
            return allFields

    class _pohmsgvtexp(AtRegister.AtRegister):
        def name(self):
            return "POH CPE J2 Expected Message buffer"
    
        def description(self):
            return "The J2 Expected Message Buffer."
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x08_0000 + $sliceid * 10752 + $stsid * 224 + $vtid * 8 + $msgid"
            
        def startAddress(self):
            return 0x00080000
            
        def endAddress(self):
            return 0xffffffff

        class _J2ExpMsg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 0
        
            def name(self):
                return "J2ExpMsg"
            
            def description(self):
                return "J2 Expected Message"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["J2ExpMsg"] = _AF6CCI0011_RD_POH._pohmsgvtexp._J2ExpMsg()
            return allFields

    class _pohmsgvtcur(AtRegister.AtRegister):
        def name(self):
            return "POH CPE J2 Current Message buffer"
    
        def description(self):
            return "The J2 Current Message Buffer."
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x08_0000 + 0x01_0000 + $sliceid * 10752 + $stsid * 224 + $vtid * 8 + $msgid"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _J2CurMsg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 0
        
            def name(self):
                return "J2CurMsg"
            
            def description(self):
                return "J2 Current Message"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["J2CurMsg"] = _AF6CCI0011_RD_POH._pohmsgvtcur._J2CurMsg()
            return allFields

    class _pohmsgstsins(AtRegister.AtRegister):
        def name(self):
            return "POH CPE J1 Insert Message buffer"
    
        def description(self):
            return "The J1 Current Message Buffer."
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x08_0000 + 0x03_2000 + $sliceid * 240 + $stsid * 8 + $msgid"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _J1InsMsg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 0
        
            def name(self):
                return "J1InsMsg"
            
            def description(self):
                return "J1 Insert Message"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["J1InsMsg"] = _AF6CCI0011_RD_POH._pohmsgstsins._J1InsMsg()
            return allFields

    class _pohmsgtu3ins(AtRegister.AtRegister):
        def name(self):
            return "POH CPE J1 TU3 Insert Message buffer"
    
        def description(self):
            return "The J1 TU3 Insert Message Buffer."
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x08_0000 + 0x02_0000 + $sliceid * 10752 + $stsid * 224 +$msgid"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _J1Tu3InsMsg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 0
        
            def name(self):
                return "J1Tu3InsMsg"
            
            def description(self):
                return "J1 TU3 Insert Message"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["J1Tu3InsMsg"] = _AF6CCI0011_RD_POH._pohmsgtu3ins._J1Tu3InsMsg()
            return allFields

    class _pohmsgvtins(AtRegister.AtRegister):
        def name(self):
            return "POH CPE J2 Insert Message buffer"
    
        def description(self):
            return "The J2 Insert Message Buffer."
            
        def width(self):
            return 128
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x08_0000 + 0x02_0000 + $sliceid * 10752 + $stsid * 224 + $vtid * 8 + $msgid"
            
        def startAddress(self):
            return 0xffffffff
            
        def endAddress(self):
            return 0xffffffff

        class _J2InsMsg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 63
                
            def startBit(self):
                return 0
        
            def name(self):
                return "J2InsMsg"
            
            def description(self):
                return "J2 Insert Message"
            
            def type(self):
                return "RW"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["J2InsMsg"] = _AF6CCI0011_RD_POH._pohmsgvtins._J2InsMsg()
            return allFields

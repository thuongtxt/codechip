import python.arrive.atsdk.AtRegister as AtRegister

class _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["RXAUI_DRP"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._RXAUI_DRP()
        allRegisters["SERDES_LoopBack"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_LoopBack()
        allRegisters["SERDES_POWER_DOWN"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_POWER_DOWN()
        allRegisters["SERDES_PLL_Status"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_PLL_Status()
        allRegisters["SERDES_TX_TXELECIDLE"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_TX_TXELECIDLE()
        allRegisters["SERDES_TX_Reset"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_TX_Reset()
        allRegisters["SERDES_RX_Reset"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_RX_Reset()
        allRegisters["SERDES_LPMDFE_Mode"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_LPMDFE_Mode()
        allRegisters["SERDES_LPMDFE_Reset"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_LPMDFE_Reset()
        allRegisters["SERDES_TXDIFFCTRL"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_TXDIFFCTRL()
        allRegisters["SERDES_TXPOSTCURSOR"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_TXPOSTCURSOR()
        allRegisters["SERDES_TXPRECURSOR"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_TXPRECURSOR()
        return allRegisters

    class _RXAUI_DRP(AtRegister.AtRegister):
        def name(self):
            return "RXAUI DRP"
    
        def description(self):
            return "Read/Write DRP address of SERDES,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x1000+$DRP"
            
        def startAddress(self):
            return 0x00001000
            
        def endAddress(self):
            return 0xffffffff

        class _drp_rw(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 0
        
            def name(self):
                return "drp_rw"
            
            def description(self):
                return "DRP read/write value"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["drp_rw"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._RXAUI_DRP._drp_rw()
            return allFields

    class _SERDES_LoopBack(AtRegister.AtRegister):
        def name(self):
            return "SERDES LoopBack"
    
        def description(self):
            return "Configurate LoopBack, there is 4 group (0-3), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,Group 2 => Port8-11, Group 1 => Port 12-15,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x002+$G*0x100"
            
        def startAddress(self):
            return 0x00000002
            
        def endAddress(self):
            return 0x00000302

        class _lpback_subport3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 12
        
            def name(self):
                return "lpback_subport3"
            
            def description(self):
                return "Unused"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lpback_subport2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 11
                
            def startBit(self):
                return 8
        
            def name(self):
                return "lpback_subport2"
            
            def description(self):
                return "Unused"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lpback_subport1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 4
        
            def name(self):
                return "lpback_subport1"
            
            def description(self):
                return "Loopback subport 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _lpback_subport0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lpback_subport0"
            
            def description(self):
                return "Loopback subport 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lpback_subport3"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_LoopBack._lpback_subport3()
            allFields["lpback_subport2"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_LoopBack._lpback_subport2()
            allFields["lpback_subport1"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_LoopBack._lpback_subport1()
            allFields["lpback_subport0"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_LoopBack._lpback_subport0()
            return allFields

    class _SERDES_POWER_DOWN(AtRegister.AtRegister):
        def name(self):
            return "SERDES POWER DOWN"
    
        def description(self):
            return "Configurate Power Down , there is 4 group (0-3), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,Group 2 => Port8-11, Group 1 => Port 12-15,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x003+$G*0x100"
            
        def startAddress(self):
            return 0x00000003
            
        def endAddress(self):
            return 0x00000303

        class _TXPD1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 6
        
            def name(self):
                return "TXPD1"
            
            def description(self):
                return "Power Down subport 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXPD0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "TXPD0"
            
            def description(self):
                return "Power Down subport 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _RXPD1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 2
        
            def name(self):
                return "RXPD1"
            
            def description(self):
                return "Power Down subport 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _RXPD0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "RXPD0"
            
            def description(self):
                return "Power Down subport 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXPD1"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_POWER_DOWN._TXPD1()
            allFields["TXPD0"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_POWER_DOWN._TXPD0()
            allFields["RXPD1"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_POWER_DOWN._RXPD1()
            allFields["RXPD0"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_POWER_DOWN._RXPD0()
            return allFields

    class _SERDES_PLL_Status(AtRegister.AtRegister):
        def name(self):
            return "SERDES PLL Status"
    
        def description(self):
            return "QPLL/CPLL status, there is 4 group (0-3), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,Group 2 => Port8-11, Group 1 => Port 12-15,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00B+$G*0x100"
            
        def startAddress(self):
            return 0x0000000b
            
        def endAddress(self):
            return 0x0000030b

        class _QPLL1_Lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "QPLL1_Lock"
            
            def description(self):
                return "QPLL0 is Locked, Group 0-1"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        class _QPLL0_Lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "QPLL0_Lock"
            
            def description(self):
                return "QPLL0 is Locked, Group 0-1"
            
            def type(self):
                return ""
            
            def resetValue(self):
                return 0xffffffff

        class _CPLL_Lock(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "CPLL_Lock"
            
            def description(self):
                return "CPLL is Locked, bit per sub port,"
            
            def type(self):
                return "R_O"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["QPLL1_Lock"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_PLL_Status._QPLL1_Lock()
            allFields["QPLL0_Lock"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_PLL_Status._QPLL0_Lock()
            allFields["CPLL_Lock"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_PLL_Status._CPLL_Lock()
            return allFields

    class _SERDES_TX_TXELECIDLE(AtRegister.AtRegister):
        def name(self):
            return "SERDES TX TXELECIDLE"
    
        def description(self):
            return "TX TXELECIDLE, there is 4 group (0-3), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,Group 2 => Port8-11, Group 1 => Port 12-15,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x004"
            
        def startAddress(self):
            return 0x00000004
            
        def endAddress(self):
            return 0xffffffff

        class _txelecidle(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txelecidle"
            
            def description(self):
                return "Trige 0->1 to enable Tx elecidle, bit per sub port"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txelecidle"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_TX_TXELECIDLE._txelecidle()
            return allFields

    class _SERDES_TX_Reset(AtRegister.AtRegister):
        def name(self):
            return "SERDES TX Reset"
    
        def description(self):
            return "Reset TX SERDES, there is 4 group (0-3), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,Group 2 => Port8-11, Group 1 => Port 12-15,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00C"
            
        def startAddress(self):
            return 0x0000000c
            
        def endAddress(self):
            return 0xffffffff

        class _txrst_trig(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "txrst_trig"
            
            def description(self):
                return "Trige 0->1 to start reset TX SERDES, bit per sub port"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["txrst_trig"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_TX_Reset._txrst_trig()
            return allFields

    class _SERDES_RX_Reset(AtRegister.AtRegister):
        def name(self):
            return "SERDES RX Reset"
    
        def description(self):
            return "Reset RX SERDES, there is 4 group (0-3), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,Group 2 => Port8-11, Group 1 => Port 12-15,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x00D"
            
        def startAddress(self):
            return 0x0000000d
            
        def endAddress(self):
            return 0xffffffff

        class _rxrst_trig(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "rxrst_trig"
            
            def description(self):
                return "Trige 0->1 to start reset RX SERDES, bit per sub port"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["rxrst_trig"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_RX_Reset._rxrst_trig()
            return allFields

    class _SERDES_LPMDFE_Mode(AtRegister.AtRegister):
        def name(self):
            return "SERDES LPMDFE Mode"
    
        def description(self):
            return "Configure LPM/DFE mode , there is 4 group (0-3), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,Group 2 => Port8-11, Group 1 => Port 12-15,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x00E+$G*0x100"
            
        def startAddress(self):
            return 0x0000000e
            
        def endAddress(self):
            return 0x0000030e

        class _lpmdfe_mode(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lpmdfe_mode"
            
            def description(self):
                return "bit per sub port"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lpmdfe_mode"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_LPMDFE_Mode._lpmdfe_mode()
            return allFields

    class _SERDES_LPMDFE_Reset(AtRegister.AtRegister):
        def name(self):
            return "SERDES LPMDFE Reset"
    
        def description(self):
            return "Reset LPM/DFE , there is 4 group (0-3), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,Group 2 => Port8-11, Group 1 => Port 12-15,"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x00F"
            
        def startAddress(self):
            return 0x0000000f
            
        def endAddress(self):
            return 0xffffffff

        class _lpmdfe_reset(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 0
        
            def name(self):
                return "lpmdfe_reset"
            
            def description(self):
                return "bit per sub port, Must be toggled after switching between modes to initialize adaptation"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["lpmdfe_reset"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_LPMDFE_Reset._lpmdfe_reset()
            return allFields

    class _SERDES_TXDIFFCTRL(AtRegister.AtRegister):
        def name(self):
            return "SERDES TXDIFFCTRL"
    
        def description(self):
            return "Driver Swing Control, see \"Table 3-35: TX Configurable Driver Ports\" page 158 of UG578 for more detail, there is 4 group (0-3), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x0010"
            
        def startAddress(self):
            return 0x00000010
            
        def endAddress(self):
            return 0xffffffff

        class _TXDIFFCTRL_subport1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 5
        
            def name(self):
                return "TXDIFFCTRL_subport1"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXDIFFCTRL_subport0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TXDIFFCTRL_subport0"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXDIFFCTRL_subport1"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_TXDIFFCTRL._TXDIFFCTRL_subport1()
            allFields["TXDIFFCTRL_subport0"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_TXDIFFCTRL._TXDIFFCTRL_subport0()
            return allFields

    class _SERDES_TXPOSTCURSOR(AtRegister.AtRegister):
        def name(self):
            return "SERDES TXPOSTCURSOR"
    
        def description(self):
            return "Transmitter post-cursor TX pre-emphasis control, see \"Table 3-35: TX Configurable Driver Ports\" page 160 of UG578 for more detail, there is 4 group (0-3), each group has 2 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x0011"
            
        def startAddress(self):
            return 0x00000011
            
        def endAddress(self):
            return 0xffffffff

        class _TXPOSTCURSOR_subport1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 5
        
            def name(self):
                return "TXPOSTCURSOR_subport1"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXPOSTCURSOR_subport0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TXPOSTCURSOR_subport0"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXPOSTCURSOR_subport1"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_TXPOSTCURSOR._TXPOSTCURSOR_subport1()
            allFields["TXPOSTCURSOR_subport0"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_TXPOSTCURSOR._TXPOSTCURSOR_subport0()
            return allFields

    class _SERDES_TXPRECURSOR(AtRegister.AtRegister):
        def name(self):
            return "SERDES TXPRECURSOR"
    
        def description(self):
            return "Transmitter pre-cursor TX pre-emphasis control, see \"Table 3-35: TX Configurable Driver Ports\" page 161 of UG578 for more detail, there is 4 group (0-3), each group has 2 sub ports"
            
        def width(self):
            return 32
        
        def type(self):
            return "Configure"
            
        def fomular(self):
            return "0x0012"
            
        def startAddress(self):
            return 0x00000012
            
        def endAddress(self):
            return 0xffffffff

        class _TXPRECURSOR_subport1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 5
        
            def name(self):
                return "TXPRECURSOR_subport1"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _TXPRECURSOR_subport0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 0
        
            def name(self):
                return "TXPRECURSOR_subport0"
            
            def description(self):
                return ""
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["TXPRECURSOR_subport1"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_TXPRECURSOR._TXPRECURSOR_subport1()
            allFields["TXPRECURSOR_subport0"] = _3G_XAUI_Xilinx_Serdes_Turning_Configuration_RD._SERDES_TXPRECURSOR._TXPRECURSOR_subport0()
            return allFields

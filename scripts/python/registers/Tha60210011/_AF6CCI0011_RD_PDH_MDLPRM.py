import python.arrive.atsdk.AtRegister as AtRegister

class _AF6CCI0011_RD_PDH_MDLPRM(AtRegister.AtRegisterProvider):
    @classmethod
    def _allRegisters(cls):
        allRegisters = {}
        allRegisters["upen_cfg_ctrl0"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_cfg_ctrl0()
        allRegisters["upen_cfg_ctrl1"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_cfg_ctrl1()
        allRegisters["upen_prm_txcfgcr"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_prm_txcfgcr()
        allRegisters["upen_mdl_idle"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_mdl_idle()
        allRegisters["upen_mdl_tp"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_mdl_tp()
        allRegisters["upen_sta_idle_alren"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_sta_idle_alren()
        allRegisters["upen_sta_tp_alren"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_sta_tp_alren()
        allRegisters["upen_cfg_mdl"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_cfg_mdl()
        allRegisters["upen_rxprm_cfgstd"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_rxprm_cfgstd()
        allRegisters["upen_rxmdl_typebuff"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_rxmdl_typebuff()
        allRegisters["upen_rxmdl_ntypebuff"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_rxmdl_ntypebuff()
        allRegisters["upen_rxmdl_cfgtype"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_rxmdl_cfgtype()
        allRegisters["upen_destuff_ctrl0"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_destuff_ctrl0()
        allRegisters["upen_destuff_ctrl1"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_destuff_ctrl1()
        allRegisters["upen_mdl_stk_ncfg"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_mdl_stk_ncfg()
        allRegisters["upen_mdl_stk_cfg"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_mdl_stk_cfg()
        allRegisters["upen_mdl_alr_ncfg"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_mdl_alr_ncfg()
        allRegisters["upen_mdl_alr_cfg"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_mdl_alr_cfg()
        return allRegisters

    class _upen_cfg_ctrl0(AtRegister.AtRegister):
        def name(self):
            return "CONFIG CONTROL STUFF 0"
    
        def description(self):
            return "config control Stuff global"
            
        def width(self):
            return 7
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00006000
            
        def endAddress(self):
            return 0xffffffff

        class _idlemod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 6
                
            def startBit(self):
                return 6
        
            def name(self):
                return "idlemod"
            
            def description(self):
                return "(0): transmit flag,(1): transmit ABORT: 1 pattern"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _sapimod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 5
        
            def name(self):
                return "sapimod"
            
            def description(self):
                return "mode byte sapi"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _sapiinscfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "sapiinscfg"
            
            def description(self):
                return "byte sapi insert"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ctraddinscfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "ctraddinscfg"
            
            def description(self):
                return "byte ctradd insert"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fcsmod32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "fcsmod32"
            
            def description(self):
                return "select FCS 32 or 16,  (0) : 16, (1) : 32"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fcsinscfg(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "fcsinscfg"
            
            def description(self):
                return "(1) enable insert FCS, (0) disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _flagmod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "flagmod"
            
            def description(self):
                return "(0): 2 flag; 1 start flag and 1 end flag, (1): 1 flag: shared flag between two package"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["idlemod"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_cfg_ctrl0._idlemod()
            allFields["sapimod"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_cfg_ctrl0._sapimod()
            allFields["sapiinscfg"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_cfg_ctrl0._sapiinscfg()
            allFields["ctraddinscfg"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_cfg_ctrl0._ctraddinscfg()
            allFields["fcsmod32"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_cfg_ctrl0._fcsmod32()
            allFields["fcsinscfg"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_cfg_ctrl0._fcsinscfg()
            allFields["flagmod"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_cfg_ctrl0._flagmod()
            return allFields

    class _upen_cfg_ctrl1(AtRegister.AtRegister):
        def name(self):
            return "CONFIG CONTROL STUFF 1"
    
        def description(self):
            return "config control Stuff global"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00006001
            
        def endAddress(self):
            return 0xffffffff

        class _addbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "addbyte"
            
            def description(self):
                return "add byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ctrbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ctrbyte"
            
            def description(self):
                return "control byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _sapi0byte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "sapi0byte"
            
            def description(self):
                return "first sapi byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _sapi1byte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "sapi1byte"
            
            def description(self):
                return "seconde sapi byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["addbyte"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_cfg_ctrl1._addbyte()
            allFields["ctrbyte"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_cfg_ctrl1._ctrbyte()
            allFields["sapi0byte"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_cfg_ctrl1._sapi0byte()
            allFields["sapi1byte"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_cfg_ctrl1._sapi1byte()
            return allFields

    class _upen_prm_txcfgcr(AtRegister.AtRegister):
        def name(self):
            return "CONFIG PRM BIT C/R & STANDARD Tx"
    
        def description(self):
            return "Config PRM bit C/R & Standard"
            
        def width(self):
            return 4
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x08000+$PRMID"
            
        def startAddress(self):
            return 0x00008000
            
        def endAddress(self):
            return 0x000094ff

        class _cfg_prmen_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "cfg_prmen_tx"
            
            def description(self):
                return "config enable Tx, (0) is disable (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_prm_txenstuff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfg_prm_txenstuff"
            
            def description(self):
                return "config enable stuff message, (0) is disable, (1) is enbale"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_prmstd_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_prmstd_tx"
            
            def description(self):
                return "config standard Tx, (0) is ANSI, (1) is AT&T"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_prm_cr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_prm_cr"
            
            def description(self):
                return "config bit command/respond PRM message"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_prmen_tx"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_prm_txcfgcr._cfg_prmen_tx()
            allFields["cfg_prm_txenstuff"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_prm_txcfgcr._cfg_prm_txenstuff()
            allFields["cfg_prmstd_tx"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_prm_txcfgcr._cfg_prmstd_tx()
            allFields["cfg_prm_cr"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_prm_txcfgcr._cfg_prm_cr()
            return allFields

    class _upen_mdl_idle(AtRegister.AtRegister):
        def name(self):
            return "Buff Message MDL IDLE"
    
        def description(self):
            return "config message MDL IDLE"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0000+19*$DE3ID + $DWORDID"
            
        def startAddress(self):
            return 0x00000000
            
        def endAddress(self):
            return 0x000001c7

        class _idle_byte3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "idle_byte3"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ilde_byte2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ilde_byte2"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_byte1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "idle_byte1"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _idle_byte0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "idle_byte0"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["idle_byte3"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_mdl_idle._idle_byte3()
            allFields["ilde_byte2"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_mdl_idle._ilde_byte2()
            allFields["idle_byte1"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_mdl_idle._idle_byte1()
            allFields["idle_byte0"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_mdl_idle._idle_byte0()
            return allFields

    class _upen_mdl_tp(AtRegister.AtRegister):
        def name(self):
            return "Config Buff Message MDL TESTPATH"
    
        def description(self):
            return "config message MDL TESTPATH"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0200+19*$DE3ID + $DWORDID"
            
        def startAddress(self):
            return 0x00000200
            
        def endAddress(self):
            return 0x000003c7

        class _tp_byte3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "tp_byte3"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "tp_byte2"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "tp_byte1"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _tp_byte0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tp_byte0"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tp_byte3"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_mdl_tp._tp_byte3()
            allFields["tp_byte2"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_mdl_tp._tp_byte2()
            allFields["tp_byte1"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_mdl_tp._tp_byte1()
            allFields["tp_byte0"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_mdl_tp._tp_byte0()
            return allFields

    class _upen_sta_idle_alren(AtRegister.AtRegister):
        def name(self):
            return "SET TO HAVE PACKET MDL BUFFER 1"
    
        def description(self):
            return "SET/CLEAR to ALRM STATUS MESSAGE in BUFFER 1"
            
        def width(self):
            return 1
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0420+$DE3ID"
            
        def startAddress(self):
            return 0x00000420
            
        def endAddress(self):
            return 0x00000437

        class _idle_cfgen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "idle_cfgen"
            
            def description(self):
                return "(0) : engine clear for indication to have sent, (1) CPU set for indication to have new message which must send for buffer 0"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["idle_cfgen"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_sta_idle_alren._idle_cfgen()
            return allFields

    class _upen_sta_tp_alren(AtRegister.AtRegister):
        def name(self):
            return "SET TO HAVE PACKET MDL BUFFER 2"
    
        def description(self):
            return "SET/CLEAR to ALRM STATUS MESSAGE in BUFFER 2"
            
        def width(self):
            return 1
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return "0x0440+$DE3ID"
            
        def startAddress(self):
            return 0x00000440
            
        def endAddress(self):
            return 0x00000457

        class _tp_cfgen(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "tp_cfgen"
            
            def description(self):
                return "(0) : engine clear for indication to have sent, (1) CPU set for indication to have new message which must send for buffer 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["tp_cfgen"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_sta_tp_alren._tp_cfgen()
            return allFields

    class _upen_cfg_mdl(AtRegister.AtRegister):
        def name(self):
            return "CONFIG MDL Tx"
    
        def description(self):
            return "Config Tx MDL"
            
        def width(self):
            return 6
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x0460+$DE3ID"
            
        def startAddress(self):
            return 0x00000460
            
        def endAddress(self):
            return 0x00000477

        class _reserve(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 4
        
            def name(self):
                return "reserve"
            
            def description(self):
                return "reserve"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_entx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "cfg_entx"
            
            def description(self):
                return "config enable Tx, (0) is disable, (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_enstuff_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "cfg_enstuff_tx"
            
            def description(self):
                return "config enable stuff Tx, (0) is disable, (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdlstd_tx(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_mdlstd_tx"
            
            def description(self):
                return "config standard Tx, (0) is ANSI, (1) is AT&T"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_cr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_mdl_cr"
            
            def description(self):
                return "config bit command/respond MDL message, bit 0 is channel 0 of DS3"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["reserve"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_cfg_mdl._reserve()
            allFields["cfg_entx"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_cfg_mdl._cfg_entx()
            allFields["cfg_enstuff_tx"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_cfg_mdl._cfg_enstuff_tx()
            allFields["cfg_mdlstd_tx"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_cfg_mdl._cfg_mdlstd_tx()
            allFields["cfg_mdl_cr"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_cfg_mdl._cfg_mdl_cr()
            return allFields

    class _upen_rxprm_cfgstd(AtRegister.AtRegister):
        def name(self):
            return "Config Buff Message PRM STANDARD"
    
        def description(self):
            return "config standard message PRM"
            
        def width(self):
            return 2
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x20000+$PRMID"
            
        def startAddress(self):
            return 0x00020000
            
        def endAddress(self):
            return 0x000214ff

        class _cfg_prm_rxenstuff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "cfg_prm_rxenstuff"
            
            def description(self):
                return "enable destuff   (0) is disable, (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_std_prm(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_std_prm"
            
            def description(self):
                return "config standard   (0) is ANSI, (1) is AT&T"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["cfg_prm_rxenstuff"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_rxprm_cfgstd._cfg_prm_rxenstuff()
            allFields["cfg_std_prm"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_rxprm_cfgstd._cfg_std_prm()
            return allFields

    class _upen_rxmdl_typebuff(AtRegister.AtRegister):
        def name(self):
            return "Buff Message MDL with configuration type"
    
        def description(self):
            return "buffer message MDL which is configured type"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x24000+20*$MDLID + $DWORDID"
            
        def startAddress(self):
            return 0x00024000
            
        def endAddress(self):
            return 0x00024eff

        class _mdl_tbyte3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "mdl_tbyte3"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _mdl_tbyte2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "mdl_tbyte2"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _mdl_tbyte1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "mdl_tbyte1"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _mdl_tbyte0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_tbyte0"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_tbyte3"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_rxmdl_typebuff._mdl_tbyte3()
            allFields["mdl_tbyte2"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_rxmdl_typebuff._mdl_tbyte2()
            allFields["mdl_tbyte1"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_rxmdl_typebuff._mdl_tbyte1()
            allFields["mdl_tbyte0"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_rxmdl_typebuff._mdl_tbyte0()
            return allFields

    class _upen_rxmdl_ntypebuff(AtRegister.AtRegister):
        def name(self):
            return "Buff Message MDL without configuration type"
    
        def description(self):
            return "buffer message MDL which is not configured type"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x26000+20*$MDLID + $DWORDID"
            
        def startAddress(self):
            return 0x00026000
            
        def endAddress(self):
            return 0x00026eff

        class _mdl_ntbyte3(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "mdl_ntbyte3"
            
            def description(self):
                return "MS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _mdl_ntbyte2(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "mdl_ntbyte2"
            
            def description(self):
                return "BYTE 2"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _mdl_ntbyte1(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "mdl_ntbyte1"
            
            def description(self):
                return "BYTE 1"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _mdl_ntbyte0(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_ntbyte0"
            
            def description(self):
                return "LS BYTE"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_ntbyte3"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_rxmdl_ntypebuff._mdl_ntbyte3()
            allFields["mdl_ntbyte2"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_rxmdl_ntypebuff._mdl_ntbyte2()
            allFields["mdl_ntbyte1"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_rxmdl_ntypebuff._mdl_ntbyte1()
            allFields["mdl_ntbyte0"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_rxmdl_ntypebuff._mdl_ntbyte0()
            return allFields

    class _upen_rxmdl_cfgtype(AtRegister.AtRegister):
        def name(self):
            return "Config Buff Message MDL TYPE"
    
        def description(self):
            return "config type message MDL"
            
        def width(self):
            return 10
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return "0x28000+$MDLID"
            
        def startAddress(self):
            return 0x00028000
            
        def endAddress(self):
            return 0x000280bf

        class _mdl_cfg_enstuff(AtRegister.AtRegisterField):
            def stopBit(self):
                return 9
                
            def startBit(self):
                return 9
        
            def name(self):
                return "mdl_cfg_enstuff"
            
            def description(self):
                return "(0) is disable de stuff, (1) is enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_mdl_std(AtRegister.AtRegisterField):
            def stopBit(self):
                return 8
                
            def startBit(self):
                return 8
        
            def name(self):
                return "cfg_mdl_std"
            
            def description(self):
                return "(0) is ANSI, (1) is AT&T"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _cfg_type_mdl(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "cfg_type_mdl"
            
            def description(self):
                return "IDLE, TEST, PATH"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfg_enstuff"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_rxmdl_cfgtype._mdl_cfg_enstuff()
            allFields["cfg_mdl_std"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_rxmdl_cfgtype._cfg_mdl_std()
            allFields["cfg_type_mdl"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_rxmdl_cfgtype._cfg_type_mdl()
            return allFields

    class _upen_destuff_ctrl0(AtRegister.AtRegister):
        def name(self):
            return "CONFIG CONTROL DESTUFF 0"
    
        def description(self):
            return "config control DeStuff global"
            
        def width(self):
            return 5
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00028400
            
        def endAddress(self):
            return 0xffffffff

        class _fcsmod32(AtRegister.AtRegisterField):
            def stopBit(self):
                return 4
                
            def startBit(self):
                return 4
        
            def name(self):
                return "fcsmod32"
            
            def description(self):
                return "select FCS 32 or 16, (0) : 16, (1) : 32"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _fcsmon(AtRegister.AtRegisterField):
            def stopBit(self):
                return 3
                
            def startBit(self):
                return 3
        
            def name(self):
                return "fcsmon"
            
            def description(self):
                return "(0) : disable monitor, (1) : enable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _sapimod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 2
                
            def startBit(self):
                return 2
        
            def name(self):
                return "sapimod"
            
            def description(self):
                return "mode byte sapi"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _sapimon(AtRegister.AtRegisterField):
            def stopBit(self):
                return 1
                
            def startBit(self):
                return 1
        
            def name(self):
                return "sapimon"
            
            def description(self):
                return "(1) : enable sapi byte monitor, (0) disable"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _addctrmod(AtRegister.AtRegisterField):
            def stopBit(self):
                return 0
                
            def startBit(self):
                return 0
        
            def name(self):
                return "addctrmod"
            
            def description(self):
                return "(1) : enable add/ctrol byte monitor"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["fcsmod32"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_destuff_ctrl0._fcsmod32()
            allFields["fcsmon"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_destuff_ctrl0._fcsmon()
            allFields["sapimod"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_destuff_ctrl0._sapimod()
            allFields["sapimon"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_destuff_ctrl0._sapimon()
            allFields["addctrmod"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_destuff_ctrl0._addctrmod()
            return allFields

    class _upen_destuff_ctrl1(AtRegister.AtRegister):
        def name(self):
            return "CONFIG CONTROL DESTUFF 1"
    
        def description(self):
            return "config control DeStuff global"
            
        def width(self):
            return 32
        
        def type(self):
            return "Config"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00028401
            
        def endAddress(self):
            return 0xffffffff

        class _addbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 24
        
            def name(self):
                return "addbyte"
            
            def description(self):
                return "add byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _ctrbyte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 23
                
            def startBit(self):
                return 16
        
            def name(self):
                return "ctrbyte"
            
            def description(self):
                return "control byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _sapi0byte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 15
                
            def startBit(self):
                return 8
        
            def name(self):
                return "sapi0byte"
            
            def description(self):
                return "first sapi byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        class _sapi1byte(AtRegister.AtRegisterField):
            def stopBit(self):
                return 7
                
            def startBit(self):
                return 0
        
            def name(self):
                return "sapi1byte"
            
            def description(self):
                return "seconde sapi byte"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["addbyte"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_destuff_ctrl1._addbyte()
            allFields["ctrbyte"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_destuff_ctrl1._ctrbyte()
            allFields["sapi0byte"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_destuff_ctrl1._sapi0byte()
            allFields["sapi1byte"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_destuff_ctrl1._sapi1byte()
            return allFields

    class _upen_mdl_stk_ncfg(AtRegister.AtRegister):
        def name(self):
            return "STICKY TO RECEIVE PACKET MDL BUFF NOT CONFIG"
    
        def description(self):
            return "engine set to alarm message event, and CPU clear to be received new messase"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00028402
            
        def endAddress(self):
            return 0xffffffff

        class _mdl_ncfg_alr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_ncfg_alr"
            
            def description(self):
                return "sticky row for buffer of type message is not configured, (row x col = 32 x 6)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_ncfg_alr"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_mdl_stk_ncfg._mdl_ncfg_alr()
            return allFields

    class _upen_mdl_stk_cfg(AtRegister.AtRegister):
        def name(self):
            return "STICKY TO RECEIVE PACKET MDL BUFF CONFI"
    
        def description(self):
            return "engine set to alarm message event, and CPU clear to be received new messase"
            
        def width(self):
            return 32
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00028403
            
        def endAddress(self):
            return 0xffffffff

        class _mdl_cfg_alr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 31
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_cfg_alr"
            
            def description(self):
                return "sticky row for buffer of type message is configured, (row x col = 32 x 6)"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfg_alr"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_mdl_stk_cfg._mdl_cfg_alr()
            return allFields

    class _upen_mdl_alr_ncfg(AtRegister.AtRegister):
        def name(self):
            return "CLEAR/SET TO RECEIVE PACKET MDL NOT CONFIG"
    
        def description(self):
            return "engine set to alarm message event, and CPU clear to be received new messase"
            
        def width(self):
            return 6
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00028600
            
        def endAddress(self):
            return 0x0002861f

        class _mdl_ncfg_alr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_ncfg_alr"
            
            def description(self):
                return "corresponding coloum with sticky row, to alarm new message which received"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_ncfg_alr"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_mdl_alr_ncfg._mdl_ncfg_alr()
            return allFields

    class _upen_mdl_alr_cfg(AtRegister.AtRegister):
        def name(self):
            return "CLEAR/SET TO RECEIVE PACKET MDL CONFIG"
    
        def description(self):
            return "engine set to alarm message event, and CPU clear to be received new messase"
            
        def width(self):
            return 6
        
        def type(self):
            return "Status"
            
        def fomular(self):
            return ""
            
        def startAddress(self):
            return 0x00028640
            
        def endAddress(self):
            return 0x0002865f

        class _mdl_cfg_alr(AtRegister.AtRegisterField):
            def stopBit(self):
                return 5
                
            def startBit(self):
                return 0
        
            def name(self):
                return "mdl_cfg_alr"
            
            def description(self):
                return "corresponding coloum with sticky row, to alarm new message which received"
            
            def type(self):
                return "R/W"
            
            def resetValue(self):
                return 0xffffffff

        def _allFieldDicts(self):
            allFields = {}
            allFields["mdl_cfg_alr"] = _AF6CCI0011_RD_PDH_MDLPRM._upen_mdl_alr_cfg._mdl_cfg_alr()
            return allFields

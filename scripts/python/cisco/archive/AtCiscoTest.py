import sys
from os import path

atsdkDir = "%s/../../.." % path.dirname(__file__)
pythonScriptDir = atsdkDir + "/scripts/python" 
sys.path.append(pythonScriptDir)
sys.path.append(pythonScriptDir + "/arrive/AtAppManager")

import unittest
from AtCisco import AtCiscoSystem
from os import remove
from AtCisco import AtCiscoSlot

class AtCiscoSystemTest(unittest.TestCase):
    
    ciscoSystem = None
    
    def system(self):
        return self.__class__.ciscoSystem
    
    @classmethod
    def setUpClass(cls):
        super(AtCiscoSystemTest, cls).setUpClass()
        cls.ciscoSystem.connect()
    
    @classmethod
    def tearDownClass(cls):
        super(AtCiscoSystemTest, cls).tearDownClass()
        cls.ciscoSystem.disconnect()
    
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.system().connect()
        
    def testConnect(self):
        self.system().connect()
        self.assertTrue(self.system().connected())
        
    def testSlotsMustBeDetectedAfterConnecting(self):
        self.assertGreater(len(self.system().allSlots()), 0, "At last one slot must be detected")
    
    def testCanDisconnect(self):
        self.system().disconnect()
        self.assertFalse(self.system().connected())
    
    def testAccessSlot(self):
        slots = self.system().allSlots()
        for slot_i in range(0, len(slots)):
            slot = slots[slot_i]
            self.assertGreaterEqual(slot.slotId, 0, "Slot ID must be >= 0")
            self.assertGreaterEqual(slot.subSlotId, 0, "Slot ID must be >= 0")
            self.assertIsNotNone(slot.type, "Slot type must be known")
            
            # Can also access by type
            self.assertEqual(slot, self.system().slotWithId("%s/%s" % (slot.slotId, slot.subSlotId)), "Slot must be accessible by its ID")
            
    def testTransferFile(self):
        # Create a file
        filePath = "text.txt"
        fileHandle = open(filePath, "w")
        fileHandle.write("Just a test file")
        fileHandle.close()
        
        # Transfer, if transfer fail, except will raise
        self.system()._transferFile(filePath)
        
        # Cleanup
        remove(filePath)
    
    def testApplyConfigureFile(self):
        # Make configure file
        filePath = "text.conf"
        fileHandle = open(filePath, "w")
        fileHandle.write("show platform\n")
        fileHandle.write("configure terminal\n")
        fileHandle.write("show connection all\n")
        fileHandle.close()
        
        # Apply it, exception will raise on failure
        self.system().applyScript(filePath)
        remove(filePath)
    
    def testFastScripting(self):
        filePath = "text.conf"
        self.system().startScript(filePath)
        self.system()._executeConfigureCommand("show platform")
        self.system()._executeConfigureCommand("show connection all")
        self.system().endScript()
        
        # Script file can be accessed and its content must be correct
        self.assertEqual(self.system().scriptFile(), filePath, "Script file mismatch")
        fileHandle = open(filePath, "r")
        lines = fileHandle.readlines()
        self.assertEqual(lines[0].strip(), "show platform", "CLI mismatch")
        self.assertEqual(lines[1].strip(), "show connection all", "CLI mismatch")
        fileHandle.close()
        
        # Cleanup
        remove(filePath)
    
    def testInit(self):
        self.system().initialize()
        self.assertTrue(self.system().isInitialized(), "System has not been initialized properly")
    
    def testMplsManagerMustExist(self):
        self.assertIsNotNone(self.system().mplsManager, "MPLS manager must exist")
    
    def _anyTdmSlot(self):
        slots = []
        for slot in self.system().allSlots():
            if slot.type in [AtCiscoSlot.SLOT_TYPE_DS1, AtCiscoSlot.SLOT_TYPE_DS3, AtCiscoSlot.SLOT_TYPE_OCN]:
                slots.append(slot)
        
        import random
        return random.choice(slots)
    
    def testRefresh(self):
        # Pick any TDM slot and initialize it to have default configuration
        slot = self._anyTdmSlot()
        if not slot.isInitialized():
            slot.initialize()
        
        # Then fresh
        self.system().fresh()
        self.assertEqual(len(self.system().mplsManager.allLabels()), 0, "No label after initializing")
    
    def testActiveRspMustBeValid(self):
        self.assertGreaterEqual(self.system().activeRspId(), 0, "Active RSP must be valid")
    
class AtCiscoMplsManagerTest(unittest.TestCase):
    mplsManager = None
    
    def manager(self):
        return self.__class__.mplsManager
    
    @classmethod
    def setUpClass(cls):
        super(AtCiscoMplsManagerTest, cls).setUpClass()
        
        '''
        To make sure that we have MPLS label space, just pick one card and
        initialize it
        '''
        system = cls.mplsManager.system()
        slot = None
        for aSlot in system.allSlots():
            if aSlot.type in [AtCiscoSlot.SLOT_TYPE_DS1, AtCiscoSlot.SLOT_TYPE_DS3, AtCiscoSlot.SLOT_TYPE_OCN]:
                slot = aSlot
                break;
        assert(slot)
        
        if not slot.isInitialized():
            slot.initialize()
    
    def notestAccessAllLabels(self):
        self.fail("Open for test")
        manager = self.manager()
        labels = manager.allLabels()
        self.assertGreater(len(labels), 0, "There must be some labels")
    
    def notestLabelConnect(self):
        self.fail("Open for test")
        manager = self.manager()
        labels = manager.allLabels()
        
        # Just pick two labels, backup their connections first
        label1 = labels[0]
        connectedLabel1 = manager.connectedLabel(label1)
        label2 = labels[len(labels) / 2]
        connectedLabel2 = manager.connectedLabel(label2)
        
        # Try disconnect operation
        manager.disconnect(label1)
        self.assertIsNone(manager.connectedLabel(label1), "Connection may not be disconnected properly")
        
        # Connect label 1 and 2 regardless label 2 may be connected to other one
        manager.connect(label1, label2)
        self.assertEqual(manager.connectedLabel(label1), label2, "Connection may not be made correctly")
        self.assertEqual(manager.connectedLabel(label2), label1, "Connection may not be made correctly")
        
        # Revert previous configuration
        manager.connect(label1, connectedLabel1)
        manager.connect(label2, connectedLabel2)
    
    def notestRetrieveSystem(self):
        self.fail("Open for test")
        self.assertIsNotNone(self.manager().system, "System object must be accessible")
    
class AtCiscoSlotTest(unittest.TestCase):
    slot = None
    fpgaFilePath = None
    sdkLibPath = None
    
    @classmethod
    def setUpClass(cls):
        super(AtCiscoSlotTest, cls).setUpClass()
        cls.slot.system.connect()
        if not cls.slot.isInitialized():
            cls.slot.initialize()
        
    def testedSlot(self):
        return self.__class__.slot
    
    def testInitSlot(self):
        self.testedSlot().initialize()
        self.assertTrue(self.testedSlot().isInitialized(), "Slot has not been initialized properly")
        
    def testAccessId(self):
        self.assertGreaterEqual(self.testedSlot().slotId, 0, "Slot ID must be valid")
        self.assertGreaterEqual(self.testedSlot().subSlotId, 0, "Sub slot ID must be valid")
        
    def notestLoadFpga(self):
        fpga = self.__class__.fpgaFilePath
        if fpga is None:
            unittest.skip("FPGA file has not been specified")
        self.testedSlot().loadFpga(fpga)
        
    def notestLoadSdk(self):
        self.fail("Open for test")
        sdkLibPath = self.__class__.sdkLibPath
        if sdkLibPath is None:
            unittest.skip("FPGA file has not been specified")
        self.testedSlot().loadSdk(sdkLibPath)
    
    def testAccessSystem(self):
        self.assertIsNotNone(self.testedSlot().system, "System must be accessible")
        
    def testAccessMplsManager(self):
        self.assertIsNotNone(self.testedSlot().mplsManager, "MPLS manager must be accessible")
        
    def testAccessLabelSpace(self):
        labels = self.testedSlot().allMplsLabels()
        self.assertGreater(len(labels), 0, "There must be some labels")

    def testRefresh(self):
        slot = self.testedSlot()
        if not slot.isInitialized():
            slot.initialize()
        self.assertGreater(len(slot.allMplsLabels()), 0, "No label after initializing")
        
        # Then fresh
        slot.fresh()
        self.assertEqual(len(slot.allMplsLabels()), 0, "No label after initializing")
        
    def testCanRunArriveClis(self):
        cliResult = self.slot.runArriveCli("show driver")
        self.assertIsNotNone(cliResult.cellContent(0, "Device"), "Arrive CLI may not run properly")
        
def runTestCase(testCase):
    suite = unittest.TestLoader().loadTestsFromTestCase(testCase)
    unittest.TextTestRunner(verbosity=2).run(suite)

def testSystem(system):
    AtCiscoSystemTest.ciscoSystem = system
    runTestCase(AtCiscoSystemTest)

def testMplsManager(ipAddress):
    ciscoSystem = AtCiscoSystem.system(ipAddress)
    AtCiscoMplsManagerTest.mplsManager = ciscoSystem.mplsManager
    runTestCase(AtCiscoMplsManagerTest)

def testSlot(slot):
    if slot.type in [AtCiscoSlot.SLOT_TYPE_DS1, AtCiscoSlot.SLOT_TYPE_DS3, AtCiscoSlot.SLOT_TYPE_OCN]:
        AtCiscoSlotTest.slot = slot
        runTestCase(AtCiscoSlotTest)
        
def testSlots(ciscoSystem):
    for slot in ciscoSystem.allSlots():
        testSlot(slot) 
    
if __name__ == '__main__':
    # Try simulation
    system = AtCiscoSystem.simulationSystem()
    system.epappPath = "%s/.x86/apps/ep_app/epapp" % atsdkDir
    #testSystem(system)
    testSlots(system)
    
    ## Try real platform
    #ipAddress = "172.33.42.21"
    #system = AtCiscoSystem.system(ipAddress)
    #system = AtCiscoSystem.system(ipAddress)
    #testSystem(system)
    #testSlots(system)

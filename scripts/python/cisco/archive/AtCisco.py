import threading
from AtConnection import AtConnection
import re
from tftpy import TftpServer
from tftpy import TftpClient
import socket
from os import path, remove
from AtColor import AtColor
from EpApp import EpApp

class AtCiscoSystem(object):
    def _tftpPort(self):
        return 69
    
    def __init__(self, ipAddress):
        self.ipAddress = ipAddress
        self._telnetConnection_ = None
        self._slotDict = None
        self._slotArray = None
        self.connections_ = None
        self._lastConnectionId = 0
        self._scriptFileHandle = None
        self._scriptStarted = False
        self._initialized = False
        self.mplsManager = AtCiscoMplsManager(self)
        self.verbose = False
        self._scriptFileName = None
        self._fileServer = _FileServer(self._tftpPort())
        self._activeRspId = -1
        self._standbyRspId = -1
        self._epappPath = None
    
    def getEpappPath(self):
        if self._epappPath is None:
            self._epappPath = self._defaultEpappPath()
        return self._epappPath
    
    def setEpappPath(self, epappPath):
        self._epappPath = epappPath
        
    epappPath = property(fget=getEpappPath, fset=setEpappPath, fdel=None, doc="epapp path")
    
    def _defaultEpappPath(self):
        return "/tmp/sw/spa/%s/0/links/usr/binos/bin/atcli" % self.activeRspId()
    
    def _defaultScriptFileName(self):
        return "cisco_default.cfg"
    
    @classmethod
    def system(cls, ipAddress):
        return _IOSSystem(ipAddress)
    
    @classmethod
    def simulationSystem(cls):
        return _IOSSimulationSystem()
    
    def _telnetConnection(self):
        if self._telnetConnection_ is None:
            self._telnetConnection_ = AtConnection.ciscoConnection(self.ipAddress)
        return self._telnetConnection_
    
    def _startConnectionIdForSubslot(self, subSlotId):
        maxNumConnectionPerSlots = 6000
        return subSlotId * maxNumConnectionPerSlots
    
    def _allocateConnectionId(self):
        self._lastConnectionId = self._lastConnectionId + 1
        connectionId = "LocalConnect_%d" % self._lastConnectionId
        return connectionId
    
    def _retrievePlatformInformation(self):
        return self._execute("show platform")
    
    def _canHandleSlotWithType(self, slotType):
        return True
    
    def _detectSlots(self):
        if not self.connected():
            self.connect()
        
        self._endConfigure()
        result = self._retrievePlatformInformation()
        
        '''
        ^\s*([0-9]+)[/]([0-9]+)\s+(\S+)\s+(\S+)\s+(\S+)
            --------   --------   -----   -----   -----
              slot      subSlot    Type   State   Time
        '''
        imPattern = re.compile("^\s*([0-9]+)[/]([0-9]+)\s+(\S+)\s+(\S+)\s+(\S+)")
        
        '''
        ^\s*R([0-9]+)\s+(\S+)\s+(\S+),\s*?(\S+)\s+(\S+)
            --------    -----   -----     -----   ----
              slot      Type    State      Role   Time
        '''
        rspPattern = re.compile("^\s*R([0-9]+)\s+(\S+)\s+(\S+),\s*?(\S+)\s+(\S+)")
        
        slotDict = {}
        slotArray = []
        
        for line in result.splitlines():
            # This can be slot
            match = imPattern.match(line)
            if match is not None:
                slotId = int(match.group(1))
                subSlotId = int(match.group(2))
                slotType = match.group(3)
                if self._canHandleSlotWithType(slotType):
                    slot = AtCiscoSlot._slot(system=self, slotType=slotType, slotId=slotId, subSlotId=subSlotId, state=match.group(4), insertTime=match.group(5))
                    slotDict["%d/%d" % (slotId, subSlotId)] = slot
                    slotArray.append(slot)
            
            # This can be RSPs
            match = rspPattern.match(line)
            if match is not None:
                rspId = int(match.group(1))
                role = match.group(4)
                if role == "active":
                    self._activeRspId = rspId
                if role == "standby":
                    self._standbyRspId = rspId
            
        self._slotDict = slotDict
        self._slotArray = slotArray
    
    def activeRspId(self):
        return self._activeRspId
    def standbyRspId(self):
        return self._standbyRspId
    
    def connect(self):
        self._telnetConnection().connect()
        if self.connected():
            self._detectSlots()
            self.mplsManager._detectAllConnections()
    
    def disconnect(self):
        self._telnetConnection().disconnect()
        self._telnetConnection_ = None
    
    def connected(self):
        return self._telnetConnection().connected
    
    def _sendCommand(self, command):
        return self._telnetConnection()._execute(command)
    
    def _execute(self, command):
        if self.verbose:
            print command
        return self._sendCommand(command)
    
    def _logCommand(self, command):
        self._scriptFileHandle.write("%s\n" % command)
    
    def _executeConfigureCommand(self, command, checkSuccess = True):
        if self._scriptStarted:
            self._logCommand(command)
            return
        
        result = self._execute(command)
        if checkSuccess and result.strip() != command.strip():
            raise Exception("Execute \"%s\" fail: %s" % (command, result))
    
    def slotWithId(self, slotId):
        try:
            if self._slotDict is None:
                self._detectSlots()
            return self._slotDict[slotId]
        except:
            return None

    def _startConfigure(self):
        self._executeConfigureCommand("configure terminal", checkSuccess=False)
            
    def _endConfigure(self):
        self._executeConfigureCommand("end")
    
    def _telnetSession(self):
        return self._telnetConnection().session
    
    def _detectConnections(self):
        self._lastConnectionId = 0
        
        self._execute("end")
        result = self._execute("show connection all")
        
        connections = []
        
        '''
        \s*([0-9]+)\s+(\S+)\s+CE([0-9][/][0-9][/][0-9]).+?CE([0-9][/][0-9][/][0-9]).+[\s]{2}\s+(.+)
           --------   ----      -----------------------     -----------------------            -----
              ID      Name             Segment 1                    Segment 2                  State
        '''
        pattern = re.compile("\s*([0-9]+)\s+(\S+)\s+CE([0-9][/][0-9][/][0-9]).+?CE([0-9][/][0-9][/][0-9]).+[\s]{2}\s+(.+)")
        for line in result.splitlines():
            match = pattern.match(line)
            if match is None:
                continue
            
            connection = _Connection()
            connection.id = match.group(1)
            connection.name = match.group(2)
            connection.segment1 = match.group(3)
            connection.segment2 = match.group(4)
            connection.state = match.group(5)
            connections.append(connection)
            
            # Need to determine last connection ID so that new connection can be setup
            idPattern = re.compile("LocalConnect_(LocalConnect_)")
            connectionId = int(idPattern.match(connection.name))
            if connectionId > self._lastConnectionId:
                self._lastConnectionId = connectionId
            
        self.connections_ = connections
        return connections
    
    def _connections(self):
        if self.connections_ is None:
            self._detectConnections()
        return self.connections_
    
    def _deleteAllConnections(self):
        connections = self._detectConnections()
        self._startConfigure()
        for connection in connections:
            self._executeConfigureCommand("no connect " + connection.name)
        self._endConfigure()
    
    def startScript(self, scriptFileName = None):
        if self._scriptStarted:
            raise Exception("Already start configuration file")
        
        fileName = scriptFileName
        if scriptFileName is None:
            fileName = self._defaultScriptFileName()
        self._scriptFileHandle = open(fileName, 'w')
        
        if self._scriptFileHandle is None:
            raise Exception("Cannot open %s for writting" % fileName)
        
        self._scriptFileName = fileName
        self._scriptStarted = True
        
    def endScript(self):
        if not self._scriptStarted:
            raise Exception("Has not started configuration file")
        
        self._scriptFileHandle.close()
        self._scriptStarted = False
        
    def scriptStarted(self):
        return self._scriptStarted
    
    def scriptFile(self):
        return self._scriptFileName
    
    def _enterShell(self):
        self._startConfigure()
        self._execute("platform shell")
        self._endConfigure()
        
        connection = self._telnetConnection()
        session = connection.session
        session.sendline("request platform software system shell")
        session.expect("Are you sure you want to continue? [y/n]")
        session.send("y")
        session.expect("\[Router_RP_[0-9]:/\]\$")
    def _exitShell(self):
        self._execute("exit")
    def _shellExecute(self, command):
        connection = self._telnetConnection()
        session = connection.session
        session.sendline(command)
        session.expect("\[Router_RP_[0-9]:/\]\$")
        return session.before
    
    def _downloadFile(self, fileName):
        # Get file from IOS context
        self._endConfigure()
        session = self._telnetSession()
        session.sendline("copy tftp://%s/%s %s" % (self._fileServer.serverIpAddress, fileName, "bootflash:"))
        session.expect("Destination filename .*?[?]")
        session.sendline()
        matchIndex = session.expect([session.prompt, ".*\[confirm\]"])
        if matchIndex == 1:
            session.send("y")
            
        # Make sure that file is received properly
        self._enterShell()
        command = "ls /bootflash/%s" % fileName
        result = self._shellExecute(command)
        exception = None
        if "No such file" in result:
            exception = Exception("File transfer fail")
        self._exitShell()
        
        return exception
    
    def _transferFile(self, filePath):
        if not path.exists(filePath):
            raise Exception("File %s does not exist" % filePath)
        
        # Start server
        dirName = path.dirname(filePath)
        fileName = path.basename(filePath)
        self._fileServer.start(dirName)
        
        # Download file
        exception = None
        try:
            self._downloadFile(fileName)
        except Exception as e:
            exception = e 
        
        # Stop it
        self._fileServer.stop()
        
        if exception is not None:
            raise exception
    
    def _executeScript(self, fileName):
        self._startConfigure()
        self._execute("tclsh")
        self._execute("source bootflash:%s" % fileName)
        self._endConfigure()
        
        # Cleanup
        self._enterShell()
        self._shellExecute("rm -fr /bootflash/%s" + fileName)
        self._exitShell()
    
    def applyScript(self, localScriptFile):
        fileName = path.basename(localScriptFile)
        self._transferFile(localScriptFile)
        self._executeScript(fileName)
    
    def allSlots(self):
        if self._slotArray is None:
            self._detectSlots()
        return self._slotArray
    
    def _slotsApply(self, slotApplyFunction):
        needStartScript = False
        if not self.scriptStarted():
            self.startScript()
            needStartScript = True
            
        for slot in self.allSlots():
            slotApplyFunction(slot)
            
        if needStartScript:
            self.endScript()
            self.applyScript(self.scriptFile())
    
    def initialize(self):
        def _slotApply(slot):
            slot.initialize()
        
        self._slotsApply(_slotApply)
        self._initialized = True
    
    def isInitialized(self):
        return self._initialized
    
    def fresh(self):
        def _slotApply(slot):
            slot.fresh()
        self._slotsApply(_slotApply)
            
    def _arriveCliOptionForSlot(self, slot):
        startCliPortNumber = 4567
        if self._activeRspId == 1:
            startCliPortNumber = 4581
        
        cliPortNumber = startCliPortNumber + slot.subSlotId
        return "-c 127.0.0.1:%d" % cliPortNumber
    
class AtCiscoSlot(object):
    
    SLOT_TYPE_DS1 = "NCS4200-48T1E1-CE"
    SLOT_TYPE_DS3 = "NCS4200-48T3E3-CE"
    SLOT_TYPE_OCN = "NCS4200-1T8S-10CS"
    SLOT_TYPE_GE  = "NCS4200-TBD"
    
    def __init__(self, system, slotId, subSlotId, state, insertTime):
        self.system = system
        self.type = None
        self.state = state
        self.insertTime = insertTime
        self.slotId    = slotId
        self.subSlotId = subSlotId
        self.mplsManager = system.mplsManager
        self._scriptStarted = False
        self.__scriptFile = "%d_%d.cfg" % (slotId, subSlotId)
        self._initialized = False
        self._atCliSession = _SdkCliSession(self)
    
    def _initializeAllTdmChannels(self):
        raise Exception('Not implemented')
    def _deinitializeAllTdmChannels(self):
        raise Exception('Not implemented')
    
    def _makeDefaultMplsConnection(self):
        raise Exception('Not implemented')
    def _disconnectAllMplsConnections(self):
        for mplsLabel in self.allMplsLabels():
            self.mplsManager.disconnect(mplsLabel)
    
    def _startScript(self):
        # If scripting is already started, just continue this scripting
        if self.system.scriptStarted():
            return
        
        if self._scriptStarted:
            raise Exception("Scripting already started")
        
        # Otherwise, use its own scripting
        self.system.startScript(self.__scriptFile)
        self._scriptStarted = True
    
    def _endAndApplyScript(self):
        # If scripting is already started, just continue this scripting, the 
        # outside will be responsible for ending scripting
        if self.system.scriptStarted():
            return
    
        if not self._scriptStarted:
            raise Exception("Scripting has not been started")
        
        self.system.endScript(self.__scriptFile)
        self.system.applyScript(self.__scriptFile)
        self._scriptStarted = False
    
    def initialize(self):
        self._startScript()
        self._initializeAllTdmChannels()
        self._makeDefaultMplsConnection()
        self._endAndApplyScript()
        self._initialized = True
    
    def isInitialized(self):
        return self._initialized
    
    def fresh(self):
        self._startScript()
        self._disconnectAllMplsConnections()
        self._deinitializeAllTdmChannels()
        self._endAndApplyScript()
    
    @classmethod
    def _createSlot(cls, system, slotType, slotId, subSlotId, state, insertTime):
        if slotType == AtCiscoSlot.SLOT_TYPE_DS1:
            return _Ds1Slot(system, slotId, subSlotId, state, insertTime)
        if slotType == AtCiscoSlot.SLOT_TYPE_DS3:
            return _Ds3Slot(system, slotId, subSlotId, state, insertTime)
        if slotType == AtCiscoSlot.SLOT_TYPE_OCN:
            return _OcnSlot(system, slotId, subSlotId, state, insertTime)
        
        # unknown slot
        slot = _GenericSlot(system, slotId, subSlotId, state, insertTime)
        slot.type = slotType
        return slot
    
    @classmethod
    def _slot(cls, system, slotType, slotId, subSlotId, state, insertTime):
        newSlot = cls._createSlot(system, slotType, slotId, subSlotId, state, insertTime)
        newSlot.slotId = slotId
        newSlot.subSlotId = subSlotId
        newSlot.state = state
        newSlot.insertTime = insertTime
        return newSlot
    
    def _startConfigure(self):
        self.system._startConfigure()
    def _endConfigure(self):
        self.system._endConfigure()
        
    def _execute(self, cli):
        return self.system._execute(cli)
    def _executeConfigureCommand(self, command, checkSuccess = True):
        return self.system._executeConfigureCommand(command, checkSuccess)
    
    def _telnetConnection(self):
        return self.system._telnetConnection()
    def _telnetSession(self):
        return self._telnetConnection().session()
    
    def _restart(self):
        session = self._telnetSession()
        session.sendline("hw-module subslot %d/%d reload")
        session.expect("Proceed with reload of module? [confirm]")
        session.sendline("y")
        session.expect(self._telnetConnection().prompt)
        
        # TODO: this has not been completed yet, it should timeout wait reload done.
        
    def loadFpga(self, fpgaFilePath):
        self._restart()
        raise Exception('Not fully implemented')
    
    def loadSdk(self, sdkLibFilePath):
        self._restart()
        raise Exception('Not fully implemented')
    
    def allMplsLabels(self):
        return []
    
    def runArriveCli(self, cli, timeout = -1):
        return self._atCliSession.runCli(cli, timeout)
    
class AtCiscoMplsManager(object):
    def __init__(self, system):
        self.system = system
        
    def allLabels(self):
        return []
    
    def connectedLabel(self, label):
        raise Exception('TODO')
    
    def disconnect(self, label):
        raise Exception('TODO')
    
    def connect(self, label1, label2):
        raise Exception('TODO')
    
    def _detectAllConnections(self):
        raise Exception('TODO')
    
class _OcnSlot(AtCiscoSlot):
    def __init__(self, system, slotId, subSlotId, state, insertTime):
        AtCiscoSlot.__init__(self, system, slotId, subSlotId, state, insertTime)
        self.type = AtCiscoSlot.SLOT_TYPE_OCN
    
    def _numLines(self):
        return 4
    
    def _cemGroupForVt(self, lineId, stsId, vtgId, vtId):
        return (stsId * 28) + (vtgId * 4) + vtId
    
    def _initLine(self, lineId):
        
        self._startConfigure()
        controllerId = "%d/%d/%d" % (self.slotId, self.subSlotId, lineId)
        self._executeConfigureCommand("controller mediaType " + controllerId)
        self._executeConfigureCommand("mode sonet")
        self._executeConfigureCommand("controller sonet " + controllerId)
        
        # Configure the line itself
        self._executeConfigureCommand("rate OC48")
        self._executeConfigureCommand("framing sonet")
        self._executeConfigureCommand("no loopback")
        self._executeConfigureCommand("no shutdown")
        
        # Mapping
        for sts_i in range(0, 48):
            # Configure STS
            stsId = `sts_i + 1`
            self._executeConfigureCommand("sts-1 " + stsId)
            self._executeConfigureCommand("no loopback")
            self._executeConfigureCommand("no shutdown")
            self._executeConfigureCommand("clock source internal")
            self._executeConfigureCommand("mode vt-15")
            
            # Configure VTs
            for vtg_i in range(0, 7):
                vtgId = vtg_i + 1
                for vt_i in range(0, 4):
                    vtId = vt_i + 1
                    cemGroupId = self._cemGroupForVt(lineId, sts_i, vtg_i, vt_i) 
                    self._executeConfigureCommand("no vtg %d vt %d loopback" % (vtgId, vtId))
                    self._executeConfigureCommand("no vtg %d vt %d shutdown" % (vtgId, vtId))
                    self._executeConfigureCommand("vtg %d vt %d clock source internal" % (vtgId, vtId))
        
        self._endConfigure()
    
    def _vtConnectionPoint(self, lineId, stsId, vtgId, vtId):
        connectionStartId = self.system._startConnectionIdForSubslot(self.subSlotId)
        vtFlatId = (lineId * 1344) + (stsId * 28) + (vtgId * 4) + vtId
        return connectionStartId + vtFlatId
    
    def _connectAllVtsInLine(self, lineId):
        self._startConfigure()
        
        for sts_i in range(0, 48):
            stsId = `sts_i + 1`
            self._executeConfigureCommand("sts-1 " + stsId)
            
            for vtg_i in range(0, 7):
                for vt_i in range(0, 2):
                    vt1 = (vt_i * 2)
                    vt2 = (vt_i * 2) + 1
                    
                    # Provision CEM group
                    vt1CemGroupId = self._cemGroupForVt(lineId, sts_i, vtg_i, vt1)
                    vt2CemGroupId = self._cemGroupForVt(lineId, sts_i, vtg_i, vt2)
                    self._executeConfigureCommand("vtg %d vt %d cem-group %d cep" % (vtg_i + 1, vt1 + 1, vt1CemGroupId))
                    self._executeConfigureCommand("vtg %d vt %d cem-group %d cep" % (vtg_i + 1, vt2 + 1, vt2CemGroupId))
                    
                    # Connect them
                    endPoint1 = "cem %d/%d/%d %d" % (self.slotId, self.subSlotId, lineId, vt1CemGroupId)
                    endPoint2 = "cem %d/%d/%d %d" % (self.slotId, self.subSlotId, lineId, vt2CemGroupId)
                    connectionName = "%d_%d" % (self._vtConnectionPoint(lineId, sts_i, vtg_i, vt1), self._vtConnectionPoint(lineId, sts_i, vtg_i, vt2))
                    self._executeConfigureCommand("connect %s %s %s" % (connectionName, endPoint1, endPoint2))
        
        self._endConfigure()
    
    def _disconnectAllVtsInLine(self, lineId):
        self._startConfigure()
        
        for sts_i in range(0, 48):
            stsId = `sts_i + 1`
            self._executeConfigureCommand("sts-1 " + stsId)
            
            for vtg_i in range(0, 7):
                for vt_i in range(0, 2):
                    vt1 = (vt_i * 2)
                    vt2 = (vt_i * 2) + 1
                    
                    vt1CemGroupId = self._cemGroupForVt(lineId, sts_i, vtg_i, vt1)
                    vt2CemGroupId = self._cemGroupForVt(lineId, sts_i, vtg_i, vt2)
                    endPoint1 = "cem %d/%d/%d %d" % (self.slotId, self.subSlotId, lineId, vt1CemGroupId)
                    endPoint2 = "cem %d/%d/%d %d" % (self.slotId, self.subSlotId, lineId, vt2CemGroupId)
                    connectionName = "%d_%d" % (self._vtConnectionPoint(lineId, sts_i, vtg_i, vt1), self._vtConnectionPoint(lineId, sts_i, vtg_i, vt2))
                    self._executeConfigureCommand("no connect %s %s %s" % (connectionName, endPoint1, endPoint2))
        
        self._endConfigure()
    
    def _deinitializeAllVtsInLine(self, lineId):
        self._startConfigure()
        
        for sts_i in range(0, 48):
            stsId = `sts_i + 1`
            self._executeConfigureCommand("sts-1 " + stsId)
            
            for vtg_i in range(0, 7):
                for vt_i in range(0, 4):
                    cemGroupId = self._cemGroupForVt(lineId, sts_i, vtg_i, vt_i)
                    self._executeConfigureCommand("no vtg %d vt %d cem-group %d cep" % (vtg_i + 1, vt_i + 1, cemGroupId))
        
        self._endConfigure()
    
    def _makeDefaultMplsConnection(self):
        for lineId in range(0, self._numLines()):
            self._connectAllVtsInLine(lineId)
    
    def _disconnectAllMplsConnections(self):
        for lineId in range(0, self._numLines()):
            self._disconnectAllVtsInLine(lineId)
    
    def _initializeAllTdmChannels(self):
        for lineId in range(0, self._numLines()):
            self._initLine(lineId)
            
    def _deinitializeAllTdmChannels(self):
        for lineId in range(0, self._numLines()):
            self._deinitializeAllVtsInLine(lineId)
    
class _Ds1Slot(AtCiscoSlot):
    def __init__(self, system, slotId, subSlotId, state, insertTime):
        AtCiscoSlot.__init__(self, system, slotId, subSlotId, state, insertTime)
        self.type = AtCiscoSlot.SLOT_TYPE_DS1
        self._cardType = None
    
    def _setCardTypeType(self, cardType):
        self._startConfigure()
        self._executeConfigureCommand("card type %s %d %d" % (cardType, self.slotId, self.subSlotId))
        self._cardType = cardType
        self._endConfigure()
    
    def _numDe1s(self):
        return 48
    
    def _pwGroup(self):
        return 0
    
    def _controllerId(self, flatDe1Id):
        return "%d/%d/%d" % (self.slotId, self.subSlotId, flatDe1Id)
    
    def _initializeDe1(self, de1Id):
        self._startConfigure()
        
        self._executeConfigureCommand("controller %s %s" % (self._cardType, self._controllerId(de1Id)))
        self._executeConfigureCommand("no shutdown")
        self._executeConfigureCommand("framing unframed")
        self._executeConfigureCommand("clock source internal")
        self._executeConfigureCommand("no loopback")
        
        self._endConfigure()
    
    def _deinitializeDe1(self, de1Id):
        self._startConfigure()
        self._executeConfigureCommand("controller %s %s" % (self._cardType, self._controllerId(de1Id)))
        self._executeConfigureCommand("no cem-group %d" % (self._pwGroup()))
        self._endConfigure()
    
    def _de1ConnectionPoint(self, de1Id):
        return self.system._startConnectionIdForSubslot(self.subSlotId) + de1Id
    
    def _makeDefaultMplsConnection(self):
        for i in range(0, self._numDe1s() / 2):
            de1_1 = i * 2
            de1_2 = de1_1 + 1
            
            self._startConfigure()
            self._executeConfigureCommand("controller %s %s" % (self._cardType, self._controllerId(de1_1)))
            self._executeConfigureCommand("cem-group %d unframed" % self._pwGroup())
            self._executeConfigureCommand("controller %s %s" % (self._cardType, self._controllerId(de1_2)))
            self._executeConfigureCommand("cem-group %d unframed" % self._pwGroup())
            self._endConfigure()
            
            self._startConfigure()
            connectionName = "%s_%s" % (self._de1ConnectionPoint(de1_1), self._de1ConnectionPoint(de1_2))
            endPoint1 = "cem %s %d" % (self._controllerId(de1_1), self._pwGroup())
            endPoint2 = "cem %s %d" % (self._controllerId(de1_2), self._pwGroup())
            self._executeConfigureCommand("connect %s %s %s" % (connectionName, endPoint1, endPoint2))
            self._executeConfigureCommand("no shutdown")
            self._endConfigure()

    def _setDefaultCardType(self):
        if self._cardType is None:
            self._setCardTypeType("t1")

    def _initializeAllTdmChannels(self):
        self._setDefaultCardType()
        for de1_i in range(0, self._numDe1s()):
            self._initializeDe1(de1_i)
    
    def _deinitializeAllTdmChannels(self):
        self._setDefaultCardType()
        for de1_i in range(0, self._numDe1s()):
            self._deinitializeDe1(de1_i)
        
class _Ds3Slot(AtCiscoSlot):
    def __init__(self, system, slotId, subSlotId, state, insertTime):
        AtCiscoSlot.__init__(self, system, slotId, subSlotId, state, insertTime)
        self.type = AtCiscoSlot.SLOT_TYPE_DS3

class _GenericSlot(AtCiscoSlot):
    def _initializeAllTdmChannels(self):
        pass
    def _deinitializeAllTdmChannels(self):
        pass
    def _makeDefaultMplsConnection(self):
        pass

    def runArriveCli(self, cli, timeout = -1):
        pass

class _IOSSystem(AtCiscoSystem):
    pass

class _IOSSimulationSystem(AtCiscoSystem):
    def _tftpPort(self):
        return 70
    
    def __init__(self):
        AtCiscoSystem.__init__(self, None)
        self.__connected = False
        
    def connect(self):
        self.__connected = True
        self._detectSlots()
    def connected(self):
        return self.__connected
    def disconnect(self):
        self.__connected = False
        
    def _sendCommand(self, command):
        return command    
    
    def _downloadFile(self, fileName):
        host = socket.gethostbyname(socket.gethostname())
        outPutFile = "output_" + fileName
        client = TftpClient(host, self._tftpPort())
        client.download(fileName, outPutFile)
        if not path.exists(outPutFile):
            raise Exception("File downloading fail")
    
    def _executeScript(self, fileName):
        outputFile = "output_" + fileName 
        file = open(outputFile)
        lines = file.readlines()
        print "%sExecuting script %s (%d lines) %s" % (AtColor.GREEN, outputFile, len(lines), AtColor.CLEAR)
        for line in lines:
            print line.strip()
        remove(outputFile)
    
    def _retrievePlatformInformation(self):
        result = \
'''
  Slot      Type                State                 Insert time (ago)
  --------- ------------------- --------------------- -----------------
   0/0      NCS4200-48T1E1-CE   ok                    02:27:10
   0/1      NCS4200-48T1E1-CE   ok                    02:27:10
   0/2      A900-IMA2Z          ok                    02:27:10
   0/3      NCS4200-1T8S-10CS   ok                    02:27:09
   0/4      NCS4200-1T8S-10CS   ok                    02:27:09
   0/5      NCS4200-1T8S-10CS   ok                    02:27:09
  R0        NCS420X-RSP         ok, active            02:30:42
  R1        A900-RSP3C-400-S    ok, standby           02:24:55
  F0                            ok, active            02:30:42
  F1                            ok, standby           02:24:55
  P0        A900-PWR550-A       ps, fail              02:29:08
  P1        A900-PWR550-A       ok                    02:29:07
  P2        A9X3-FAN-H          ok                    02:29:06
'''
        return result
    
    def _canHandleSlotWithType(self, slotType):
        # Ds3 is not ready
        if slotType == AtCiscoSlot.SLOT_TYPE_DS3:
            return False
        
        return AtCiscoSystem._canHandleSlotWithType(self, slotType)
    
    def _enterShell(self):
        pass
    
    def _productCodeForSlot(self, slot):
        if slot.type == AtCiscoSlot.SLOT_TYPE_DS1:
            return "60210021"
        if slot.type == AtCiscoSlot.SLOT_TYPE_DS3:
            return "60210031"
        if slot.type == AtCiscoSlot.SLOT_TYPE_OCN:
            return "60210011"
        
        return None
    
    def _arriveCliOptionForSlot(self, slot):
        return "-s %s" % self._productCodeForSlot(slot)
    
class _Connection(object):
    def __init__(self):
        self.id = None
        self.name = None
        self.segment1 = None
        self.segment2 = None
        self.state = None

class _FileServerListenThread(threading.Thread):
    def __init__(self, listenPort=None):
        threading.Thread.__init__(self, group=None, target=None, name="FileServer", args=None, kwargs=None, verbose=False)
        self.listenPort = 69 if listenPort is None else listenPort
        self.rootDir = "."
    
    def run(self):
        self.server = TftpServer(self.rootDir)
        self.serverIpAddress = socket.gethostbyname(socket.gethostname())
        self.server.listen(self.serverIpAddress, self.listenPort)

    def start(self, rootDir = None):
        self.rootDir = rootDir
        threading.Thread.start(self)
    
    def stop(self):
        self.server.stop()
        self.join(10)

class _FileServer(object):
    def __init__(self, port=None):
        self._thread = None
        self._port = port
    
    def start(self, rootDir = None):
        if self._thread is not None:
            raise Exception("File server already started")
        
        self._thread = _FileServerListenThread(self._port)
        self._thread.start(rootDir)
        
    def stop(self):
        self._thread.stop()
        self._thread = None
        
class _SdkCliSession(object):
    def __init__(self, slot):
        self._slot = slot;
        self._connection = AtConnection.ciscoConnection(slot.system.ipAddress)
        self._app = None
    
    def _epapp(self):
        if self._app is None:
            self._app = EpApp(self._command(), None)
        return self._app
    
    def __del(self):
        self._close()
    
    def _system(self):
        return self._slot.system
    
    def runCli(self, cli, timeout = -1):
        if not self._epapp().started():
            self._open()
        return self._epapp().runCli(cli, timeout)
    
    def _command(self):
        option = self._system()._arriveCliOptionForSlot(self._slot)
        return "%s %s" % (self._system().epappPath, option)
    
    def _open(self):
        system = self._system()
        system._enterShell()
        self._epapp().start()
    
    def _close(self):
        AtColor.printColor(AtColor.GREEN, "Closing Arrive CLI session...")
        self._epapp().exit()

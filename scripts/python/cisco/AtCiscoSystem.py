from AtConnection import AtConnection
from EpApp import EpApp
import os
import time

class _CiscoEpapp(EpApp):
    def __init__(self, ciscoSystem, slotId, rspId = 0, epappPath=None):
        self.epappPath = epappPath
        self.portId = 4567 + slotId + (rspId * 14)
        self.ciscoSystem = ciscoSystem
        self.rspId= rspId
        
        # Make a connection and active its shell mode
        connection = AtConnection.ciscoConnection(ciscoSystem.ipAddress)
        connection.connect()
        ciscoSystem.enterShellForConnection(connection)
        if epappPath is None:
            command = "/tmp/sw/spa/0/0/links/usr/binos/bin/atcli -c 127.0.0.1:%d" % self.portId
        else:
            command = "%s -n" % epappPath
        
        EpApp.__init__(self, command, connection=connection)
    
    def start(self):
        EpApp.start(self)
        if self.epappPath is not None:
            self.runCli("textui remote connect %d" % self.portId)
        self.autotestEnable()
        
    def exit(self):
        self.connection.session.sendline("exit")
        self.ciscoSystem.exitShellForConnection(self.connection)
        self.connection.disconnect()

class AtCiscoSystem():
    def __init__(self, ipAddress):
        self.connection = AtConnection.ciscoConnection(ipAddress)
        self.ipAddress = ipAddress
        self.shellIsActive = False
        self.connect()
        self.tftpServer = None
        self.verbose = False
        self.sdkSourceDir = None
        
    def connect(self):
        self.connection.connect()
        
    def disconnect(self):
        if self.shellIsActive:
            self.exitShell()
        self.connection.disconnect()
    
    def enterShellForConnection(self, connection):
        connection.execute("end")
        connection.execute("request platform software system shell", ".*?\[y/n\] ")
        connection.session.send("y")
        connection.session.expect(".*\[.*?\]\$ ")
    
    def enterShell(self):
        if self.shellIsActive:
            return
        self.enterShellForConnection(self.connection)
        self.shellIsActive = True
    
    def exitShellForConnection(self, connection):
        connection.session.sendline("exit")
        connection.session.expect(self.connection.prompt)
    
    def exitShell(self):
        if self.shellIsActive:
            self.exitShellForConnection(self.connection)
            self.shellIsActive = False
    
    def shellExecute(self, command):
        if not self.shellIsActive:
            raise Exception("Shell is not active")
        
        self.connection.session.sendline(command)
        self.connection.session.expect("\[.*?\]\$")
    
    def downloadFile(self, file, destination="bootflash:"):
        self.connection.execute("copy tftp://%s/%s %s" % (self.tftpServer, file, destination), ".*?\? ")
        self.connection.session.sendline(file)

        prompts = [".*?[confirm]"]
        prompts.extend(self.connection.prompt)
        if self.connection.session.expect(prompts) == 0: # First prompt 
            self.connection.session.send("y")
            self.connection.session.expect("#", timeout=120)

    def execute(self, command):
        if self.verbose:
            print command
        self.connection.execute(command)
    
    def reloadSlot(self, slotId):
        self.execute("hw-module subslot 0/%d reload force" % slotId)    
        
    def epappForSlot(self, slotId, rspId = 0, epappPath=None):
        return _CiscoEpapp(self, slotId, rspId, epappPath)
    
    def _sdkDirCheck(self):
        if self.sdkSourceDir is None:
            raise Exception("SDK source directory has not been set")
    
    def _tftpServerCheck(self):
        if self.tftpServer is None:
            raise Exception("TFTP server has not been set")
    
    def compileSdk(self):
        self._sdkDirCheck()
        os.system("make -C %s/apps/sample_app/ COMPILE_PREFIX=powerpc-fsl_networking-linux- ADDITIONAL_FLAGS=\"--sysroot=/opt/tools/fsl-networking/QorIQ-SDK-V1.5/sysroots/ppce5500-fsl_networking-linux\" GEN_CMD_SRC_FILE=no -j 10 clean all" % self.sdkSourceDir)
    
    def uploadSdk(self):
        self._sdkDirCheck()
        self._tftpServerCheck()
        os.chdir("%s/.build" % self.sdkSourceDir)
        os.system("tar -cjvf atsdkall.so.tar.gz atsdkall.so")
        os.system("tftp %s -m binary -c put atsdkall.so.tar.gz atsdkall.so.tar.gz" % self.tftpServer)
    
    def updateSdkForSlot(self, slotId, downloadFirst=True):
        self._tftpServerCheck()
        
        if downloadFirst:
            self.downloadFile("atsdkall.so.tar.gz")
            
        self.enterShell()
        self.shellExecute("cd /bootflash")
        self.shellExecute("tar -xjvf atsdkall.so.tar.gz")
        self.shellExecute("mkdir -p /bootflash/iomd_restart/im_0_%d" % slotId)
        self.shellExecute("mv ./atsdkall.so /bootflash/iomd_restart/im_0_%d/libarrive.so" % slotId)
        self.exitShell()
        
if __name__ == '__main__':
    system = AtCiscoSystem("172.33.42.21")
    system.connect()
    epapp = system.epappForSlot(5, epappPath="/bootflash/epapp")
    epapp.start()
    print epapp.runCli("show driver")
    epapp.exit()
    system.disconnect()

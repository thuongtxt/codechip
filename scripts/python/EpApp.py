import os

from AtConnection import AtConnection
from python.arrive.AtAppManager.AtCliResult import AtCliResult
from python.jenkin.AtOs import AtOs

class App(object):
    _tempFileCount = 0

    def __init__(self, command, connection=None):
        """
        :rtype: App
        """
        self.command = command
        self.connection = connection
        self._started = False
        self.autotestIsEnabled = False
        self.verbosed = False
        self.logPrefix = None
        self.child = None
        self.binaryPath = None
        self.simulated = False # To be able to work when there is no real connection

    @staticmethod
    def sdkPrompt():
        return "Invalid"

    def _waitSdkPrompt(self, timeout=-1):
        retryTimes = 5
        latestException = None
        for _ in range(retryTimes):
            try:
                self.child.expect(self.sdkPrompt(), timeout)
                return
            except Exception as e:
                latestException = e

        raise latestException

    def didStart(self):
        pass

    def connectionDidConnect(self, connection):
        pass

    def start(self):
        if self.simulated:
            return

        # User want to start the app directly from the console
        if self.connection is None:
            import pexpect
            self.child = pexpect.spawn(self.command)
            self._waitSdkPrompt()
        else:
            self.connection.connect()

            # Need to change prompt to SDK prompt
            self.connectionDidConnect(self.connection)
            self.connection.execute(self.command, self.sdkPrompt())

        self.didStart()
        self._started = True

    def exit(self):
        if self.simulated:
            return

        if self.connection is None:
            import pexpect
            self.child.sendline("exit")
            self.child.expect(pexpect.EOF)
        else:
            self.connection.execute("exit")
            self.connection.disconnect()

        self._started = False

    def started(self):
        return self._started

    def _logCli(self, cli):
        if not (self.verbosed or self.simulated):
            return

        if self.logPrefix:
            print "%s: %s" % (self.logPrefix, cli)
        else:
            print cli

    def _cliResultFromConsoleOutput(self, consoleOutput):
        return consoleOutput

    def _runCli(self, cli, timeout=-1):
        self._logCli(cli)

        if self.simulated:
            return cli

        if self.connection is None:
            self.child.sendline(cli)
            self._waitSdkPrompt(timeout)
            return self._cliResultFromConsoleOutput(consoleOutput=self.child.before)

        output = self.connection.execute(cli, self.sdkPrompt(), timeout)
        if self.autotestIsEnabled:
            return self._cliResultFromConsoleOutput(consoleOutput=output)

        return output

    def autotestEnable(self):
        raise Exception("Not supported")

    @staticmethod
    def _makeScript(cli):
        tempFilePath = "%s/.temp.%d.script" % (AtOs.pwd(), App._tempFileCount)
        tempFile = open(tempFilePath, "w")
        tempFile.write(cli)
        tempFile.close()

        App._tempFileCount = App._tempFileCount + 1
        return tempFilePath

    @classmethod
    def rawScriptRunCli(cls):
        return "Invalid"

    def runCli(self, cli, timeout=-1):
        if cli.count("\n") == 0:
            return self._runCli(cli, timeout)

        # Make temp script file
        tempFilePath = self._makeScript(cli)

        # Run it
        ret = self._runCli("%s %s" % (self.rawScriptRunCli(), tempFilePath), timeout)

        AtOs.deleteFile(tempFilePath)
        return ret

    @classmethod
    def _commandForScript(cls, script):
        _, extension = os.path.splitext(script)

        if extension == ".tcl":
            return "tcl %s" % script
        if extension == ".py":
            return "python %s" % script

        # Raw script
        if (script.count("\n") == 0) and (len(extension) > 0):
            return "%s %s" % (cls.rawScriptRunCli(), script)

        return None

    def runScript(self, script, timeout=-1, options=None):
        command = self._commandForScript(script)
        if command and options:
            command += " %s" % options

        if command:
            return self.runCli(command, timeout)

        return self.runCli(script, timeout)

class EpApp(App):
    @staticmethod
    def sdkPrompt():
        return "AT SDK >"

    def didStart(self):
        self.runCli("autotest enable")

    def autotestEnable(self):
        self.runCli("autotest enable")
        self.autotestIsEnabled = True

    def _cliResultFromConsoleOutput(self, consoleOutput):
        return AtCliResult.xmlResult(consoleOutput)

    @classmethod
    def rawScriptRunCli(cls):
        return "run"

class At4848App(App):
    @staticmethod
    def sdkPrompt():
        return "AT4848BSDK >"

    @classmethod
    def rawScriptRunCli(cls):
        return "bp"

    @classmethod
    def app(cls, boardIp, binaryPath, optionString = "-tlvai poll -f"):
        connection = AtConnection.telnetConnection(boardIp)
        command = binaryPath + " " + optionString
        app = At4848App(command, connection=connection)
        app.binaryPath = binaryPath
        return app

    def connectionDidConnect(self, connection):
        """
        :type connection: AtConnection
        """
        workingDirectory = os.path.dirname(self.binaryPath)
        connection.execute("killall -9 at4848")
        connection.execute("cd %s" % workingDirectory)

    @classmethod
    def test(cls, boardIp, binaryPath):
        app = At4848App.app(boardIp, binaryPath)
        app.start()
        cliResult = app.runCli("dlk cfgmastget")
        assert cliResult is not None
        print cliResult
        app.exit()

if __name__ == '__main__':
    At4848App.test(boardIp="172.33.44.71", binaryPath="/home/wdb/users/sw/chitt/run/ep4848v5/customers/keymile/at4848_sdk_04.06/at4848")
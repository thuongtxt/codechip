import pexpect

class AtConnection(object):
    def __init__(self, ipAddress=None, username=None, password=None, prompt=None):
        self.ipAddress = ipAddress
        self.username = username
        self.password = password
        self.connected = False
        self.session = None
        
        if prompt is None:
            self.prompt = ["\$", "#"]
        else:
            self.prompt = prompt
    
    @classmethod
    def telnetConnection(cls, ipAddress=None, username=None, password=None, prompt=None):
        return _Telnet(ipAddress, username, password, prompt)
    
    @classmethod
    def sshConnection(cls, ipAddress=None, username=None, password=None, prompt=None):
        return _Ssh(ipAddress, username, password, prompt)
    
    @classmethod
    def ciscoConnection(cls, ipAddress=None):
        return _CiscoIOSSystem(ipAddress)

    @classmethod
    def annaConnection(cls, ipAddress, username = "root", password="root", prompt=">"):
        return _AnnaSystem(ipAddress, username=username, password=password, prompt=prompt)

    def _connect(self):
        pass
    
    def _disconnect(self):
        pass
    
    def connect(self):
        if self.connected:
            return
        self._connect()
        self.connected = True
    
    def disconnect(self):
        if not self.connected:
            return
        
        self._disconnect()
        self.connected = False

    def _execute(self, command, prompt = None, timeout = -1):
        pass

    def execute(self, command, prompt = None, timeout = -1):
        if not self.connected:
            raise Exception("Not connected yet")
        return self._execute(command, prompt, timeout)

class _Ssh(AtConnection):
    
    def _connect(self):
        command = "ssh " + self.username + "@" + self.ipAddress
        self.session = pexpect.spawn(command)
        self.session.expect("password:")
        self.session.sendline(self.password)
        self.session.expect(self.prompt)
    
    def _execute(self, command, prompt = None, timeout = -1):
        waitPromp = prompt
        if prompt is None:
            waitPromp = self.prompt
            
        self.session.sendline(command)
        self.session.expect(waitPromp, timeout)
        return self.session.before
    
    def _disconnect(self):
        self.session.sendline("exit")
        self.session.wait()
    
class _Telnet(_Ssh):
    connected = False
    
    @staticmethod
    def usernamePrompt():
        return "username:"
    def passwordPrompt(self):
        return "password:"
    
    def _connect(self):
        command = "telnet " + self.ipAddress
        self.session = pexpect.spawn(command)
        
        if self.username is not None:
            self.session.expect(self.usernamePrompt())
            self.session.sendline(self.username)
            
        if self.password is not None:
            self.session.expect(self.passwordPrompt())
            self.session.sendline(self.password)
        
        self.session.expect(self.prompt)

class _AnnaSystem(_Telnet):
    @staticmethod
    def usernamePrompt():
        return "login:"

    def passwordPrompt(self):
        return "Password:"

class _CiscoIOSSystem(_Telnet):
    def __init__(self, ipAddress):
        _Telnet.__init__(self, ipAddress=ipAddress, username=None, password="lab", prompt="Router>")
    
    def passwordPrompt(self):
        return "Password:" 
    
    def _disableDnsLookup(self):
        self.execute("configure terminal")
        self.execute("no ip domain lookup")
        self.execute("end")
    
    def _connect(self):
        _Telnet._connect(self)
        self.session.sendline("enable")
        self.session.expect("Password:")
        self.session.sendline("lab")
        self.prompt = ["Router\S*#", "--More--"]
        self.session.expect(self.prompt)
    
    def connect(self):
        if self.connected:
            return
        
        _Telnet.connect(self)
        self._disableDnsLookup()

    def _execute(self, command, prompt=None, timeout=-1):
        allOutput = _Telnet._execute(self, command, prompt=prompt, timeout=timeout)
        if self.session.match_index == 0:
            return allOutput
        
        while True:
            self.session.send(" ")
            self.session.expect(self.prompt)
            allOutput = allOutput + self.session.before
            if self.session.match_index == 0:
                break
        
        return allOutput
    
    def _disconnect(self):
        self.execute("end")
        _Telnet._disconnect(self)

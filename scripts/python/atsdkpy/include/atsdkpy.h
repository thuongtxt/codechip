/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Python
 * 
 * File        : atsdkpy.h
 * 
 * Created Date: Feb 18, 2017
 *
 * Description : Python wraper
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDKPY_H_
#define _ATSDKPY_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void AtSdkPyInit(void);

#ifdef __cplusplus
}
#endif
#endif /* _ATSDKPY_H_ */


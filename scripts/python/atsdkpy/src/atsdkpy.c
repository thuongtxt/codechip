/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Python
 *
 * File        : atsdkpy.c
 *
 * Created Date: Feb 18, 2017
 *
 * Description : Python wraper
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atsdkpy.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern void initatsdkpy(void);
extern void init_cffi_backend(void);

/*--------------------------- Implementation ---------------------------------*/
void AtSdkPyInit(void)
    {
    init_cffi_backend();
    initatsdkpy();
    }

#!/usr/bin/env python

'''
Created on Feb 14, 2017

@author: namnng
'''

import os, re, sys

RED    = "\033[1;31m"
PINK   = "\033[1;35m"
YELLOW = "\033[1;33m"
CYAN   = "\033[1;36m"
GREEN  = "\033[1;32m"
CLEAR  = "\033[0m"

class HeaderProcessor(object):
    def __init__(self, *args, **kwargs):
        object.__init__(self, *args, **kwargs)
        self.processedHeader = None
        self._headerFilesNeedAdditionalProcess = {}
    
    def _removeDefines(self, definedNames, sourceCode):
        for definedName in definedNames:
            pattern = "#define\s+%s.*[\r\n|\n]" % definedName
            sourceCode = re.sub(pattern, r"", sourceCode)
            
        return sourceCode
    
    def _removeFunctions(self, functionNames, sourceCode):
        for functionName in functionNames:
            pattern = ".+\s+%s\(.*?\).*;" % functionName
            sourceCode = re.sub(pattern, r"", sourceCode)
        return sourceCode
    
    def _correctEnum(self, sourceCode):
        return re.sub("(cAt.+)\s+=\s[|+\-_a-zA-Z0-9 \r\n\|\(\)]+", r"\1", sourceCode)
    
    def _process_attypes(self, fileContent):
        removedDefines = ["atprivate",
                          "AtSourceLocation",
                          "AtCast",
                          "mMax", 
                          "mMin", 
                          "mOutOfRange", 
                          "mSwap", 
                          "mOffset",
                          "AtFileLocation", 
                          "AtLineNumber", 
                          "AtFunction",
                          "atfriend",
                          "atpublic",
                          "AtUnused",
                          "null",
                          "mCount",
                          "mInRange"]
        return self._removeDefines(removedDefines, fileContent)
    
    def _process_AtRet(self, fileContent):
        removedDefineds = ["mNextStartRet"]
        fileContent = self._removeDefines(removedDefineds, fileContent)
        return re.sub(r"(cAtError.+?StartCode\s*)=\s*mNextStartRet\(.+\).*", r"\1,", fileContent)
    
    def _process_AtCommon(self, fileContent):
        removedDefineds = ["mNextStartRet"]
        fileContent = self._removeDefines(removedDefineds, fileContent)
        return fileContent
    
    def _process_atclib(self, fileContent):
        defines = ["AT_EOF", 
                   "AT_CLOCK_REALTIME", 
                   "AT_TIMER_ABSTIME",
                   "AT_CLOCK_MONOTONIC",
                   "AT_SIGRTMIN",
                   "AT_SIGRTMAX",
                   "AtSprintf",
                   "AtPrintf",
                   "AtSnprintf"]
        fileContent = self._removeDefines(defines, fileContent)
        return fileContent
    
    def _process_AtFile(self, fileContent):
        fileContent = self._removeFunctions(["AtFileVPrintf"], fileContent)
        return fileContent
    
    def _process_AtStd(self, fileContent):
        removedFunctions = ["AtStdSnprintf", 
                            "AtStdSprintf",
                            "AtStdPrintf"]
        fileContent = self._removeFunctions(removedFunctions, fileContent)
        return fileContent
    
    def _process_AtObject(self, fileContent):
        removedDefines = ["mMethodsGet",
                          "mMethodsSet",
                          "mMethodOverride"]
        return self._removeDefines(removedDefines, fileContent)
    
    def _process_AtOsal(self, fileContent):
        removedFunctions = ["AtOsalTraceRedirectSet"]
        return self._removeFunctions(removedFunctions, fileContent)
    
    def _process_AtLogger(self, fileContent):
        removedFunctions = ["AtLoggerLog"]
        return self._removeFunctions(removedFunctions, fileContent)
    
    def _process_AtPw(self, fileContent):
        removedDefines = ["cAtInvalidPwEthPortQueueId"]
        return self._removeDefines(removedDefines, fileContent)
    
    def _process_AtTextUI(self, fileContent):
        removedFunction = ["AtTextUIFormatCmdProcess"]
        return self._removeFunctions(removedFunction, fileContent)
    
    def _process_AtTcl(self, fileContent):
        return self._removeFunctions(["AtDefaultTclNew"], fileContent)
    
    def _isDeprecatedFile(self, filePath):
        if "Deprecated" in filePath:
            return True
        return False
    
    def _ignoredFiles(self):
        return ["atidparse.h", "commacro.h", "AtHalAlu.h", "AtHalBdcom.h", "AtHalLoop.h"]
    
    def _shouldIgnoreFile(self, headerFilePath):
        if os.path.basename(headerFilePath) in self._ignoredFiles():
            return True
        if self._isDeprecatedFile(headerFilePath):
            return True
        
        return False
    
    def _remove_CR(self, sourceCode):
        return sourceCode.replace("\r\n", "\n")
    
    def _remove_ifndef(self, sourceCode):
        return re.sub(r"#ifndef\s+.+[\r\n|\n]+#define\s+.+[\r\n|\n]+", r"", sourceCode)
    
    def _remove_ifdef(self, sourceCode):
        return re.sub(r"#ifdef\s+.+[\r\n|\n](.*[\r\n|\n])*?#endif", r"", sourceCode)
    
    def _remove_include(self, sourceCode):
        return re.sub(r"(#\s*include.+[\r\n|\n])", r"", sourceCode)
    
    def _remove_extern_c(self, sourceCode):
        sourceCode = re.sub(r"(#ifdef.*[\r\n|\n]*extern\s+\"C\".*[\r\n|\n]*\s*#endif[\r\n|\n])", r"", sourceCode)
        sourceCode = re.sub(r"(#ifdef\s+__cplusplus[\r\n|\n]+\})", r"", sourceCode)
        return sourceCode
    
    def _remove_endif(self, sourceCode):
        return re.sub(r"(#endif.*)", r"", sourceCode)
    
    def _remove_compiler_attribute(self, sourceCode):
        return re.sub(r"AtAttributePrintf\s*\(\d+\s*,\s*\d+\)", r"", sourceCode)
    
    def _remove_macros(self, sourceCode):
        return re.sub(r"#define\s+.+\(.+\).*", r"", sourceCode)
    
    def _loadHeaderProcessingMethods(self):
        import inspect
        methods = inspect.getmembers(HeaderProcessor, inspect.ismethod)
        for name, method in methods:
            pattern = "_process_(.+)"
            match = re.match(pattern, name)
            if match:
                self._headerFilesNeedAdditionalProcess["%s.h" % match.group(1)] = method
    
    def _do_additionalProcess(self, headerFilePath, sourceCode):
        if len(self._headerFilesNeedAdditionalProcess) == 0:
            self._loadHeaderProcessingMethods()
                    
        try:
            fileName = os.path.basename(headerFilePath)
            method = self._headerFilesNeedAdditionalProcess[fileName]
            sourceCode = method(self, sourceCode)
        except:
            pass
        
        return sourceCode
    
    def importFromHeaderFile(self, headerFilePath):
        self.processedHeader = None
        
        if not os.path.isfile(headerFilePath):
            print (RED + "Ignore" + CLEAR + " file: %s " % headerFilePath) + RED + "(not exist)" + CLEAR
            return
        
        if self._shouldIgnoreFile(headerFilePath):
            print YELLOW + "Ignore" + CLEAR + " file: %s" % headerFilePath
            return
        
        print GREEN + "Import" + CLEAR + " file: %s" % headerFilePath
        
        headerFile = open(headerFilePath)
        sourceCode = headerFile.read()
        headerFile.close()
        
        sourceCode = self._remove_CR(sourceCode)
        sourceCode = self._remove_ifndef(sourceCode)
        sourceCode = self._remove_include(sourceCode)
        sourceCode = self._remove_extern_c(sourceCode)
        sourceCode = self._remove_ifdef(sourceCode)
        sourceCode = self._remove_endif(sourceCode)
        sourceCode = self._remove_macros(sourceCode)
        sourceCode = self._remove_compiler_attribute(sourceCode)
        sourceCode = self._do_additionalProcess(headerFilePath, sourceCode)
        sourceCode = self._correctEnum(sourceCode)

        self.processedHeader = sourceCode
    
class PythonWraperGenerator(object):
    def _parseInputHeaderFiles(self, headerFiles):
        if type(headerFiles) is list:
            self.inputHeaderFiles = headerFiles
            
        if type(headerFiles) is str:
            self.inputHeaderFiles = headerFiles.split() 
    
    def __init__(self, headerFiles, outputSource, *args, **kwargs):
        object.__init__(self, *args, **kwargs)
        assert(headerFiles)
        self.outputSource = outputSource
        self.inputHeaderFiles = None
        self._parseInputHeaderFiles(headerFiles)
    
    def excludeHeaders(self):
        return ["AtOsalLeakTracker.h"]

    def generate(self):
        from cffi import FFI
        ffibuilder = FFI()
        headerProcessor = HeaderProcessor()
        
        includes = ""
        
        for fileName in self.inputHeaderFiles:
            headerProcessor.importFromHeaderFile(fileName)
            if headerProcessor.processedHeader:
                ffibuilder.cdef(headerProcessor.processedHeader)
                baseName = os.path.basename(fileName)
                if baseName not in self.excludeHeaders():
                    includes += "#include \"%s\"\n" % baseName
        
        ffibuilder.set_source("atsdkpy", includes)
        ffibuilder.emit_c_code(self.outputSource)

def Main():
    import getopt
    def printUsage(exitCode = None):
        print "Usage: %s --header-files=<file1.h file2.h ...> --output=<output.c> [--help]" % sys.argv[0]
        if exitCode:
            sys.exit(exitCode)
    
    try:
        opts, _ = getopt.getopt(sys.argv[1:],"",["help", "output=", "header-files="])
    except getopt.GetoptError:
        printUsage(exitCode=1)

    output = None
    headerFiles = None
    
    for opt, arg in opts:
        if opt == "--help":
            printUsage(exitCode = 0)
        if opt == "--output":
            output = arg
        if opt == "--header-files":
            headerFiles = arg
    
    if headerFiles is None:
        printUsage(exitCode=1)
    
    if output is None:
        print "Output C source must be specified"
        printUsage(1)
    
    generator = PythonWraperGenerator(headerFiles, output)
    generator.generate()

if __name__ == "__main__":
    Main()

from python.arrive.AtAppManager.AtColor import AtColor
from python.arrive.atsdk.cli import AtCliRunnable
from python.arrive.atsdk.device import AtDevice
import python.arrive.atsdk.pdh as pdh
import python.arrive.atsdk.sdh as sdh

class CheckerFactory(object):
    @staticmethod
    def createDeviceChecker(device):
        """
        :rtype: Checker
        """
        if device.productCode == 0x60210051:
            return Tha60210051DeviceChecker(device)

        return None

class Checker(AtCliRunnable):
    def __init__(self, device):
        """
        :type device: AtDevice
        """
        self.device = device
        self.registerProviderFactory = self._createRegisterProviderFactory()

    def check(self):
        pass

    def _createRegisterProviderFactory(self):
        """
        :rtype: python.arrive.atsdk.AtRegister.AtRegisterProviderFactory
        """
        return None

    @staticmethod
    def notImplemented():
        AtColor.printColor(AtColor.RED, "Not implemented")
        assert 0

class DefaultChecker(Checker):
    def _checkPwPda(self, pw):
        """
        :type pw: python.arrive.atsdk.pw.AtPw
        """
        self.notImplemented()

    def _checkPw(self, pw):
        self._checkPwPda(pw)

    def check(self):
        for pw in self.device.pwModule().pws():
            self._checkPw(pw)

class Tha60210051DefaultChecker(DefaultChecker):
    def _createRegisterProviderFactory(self):
        """
        :rtype: python.arrive.atsdk.AtRegister.AtRegisterProviderFactory
        """
        import python.registers.Tha60210011.RegisterProviderFactory as Factory
        return Factory.RegisterProviderFactory()

    @staticmethod
    def _isFromLoBus(circuit):
        if isinstance(circuit, pdh.AtPdhChannel):
            return True

        if isinstance(circuit, sdh.AtSdhVc):
            if circuit.isLoVc():
                return True

            if isinstance(circuit, sdh.AtSdhVc4):
                return True if circuit.mapType == circuit.MAP_3xTUG3s else False

            if isinstance(circuit, sdh.AtSdhVc3):
                return False if circuit.mapType == circuit.MAP_C3 else True

            return False
        
class Tha60210051PdaChecker(Tha60210051DefaultChecker):
    @staticmethod
    def pdaBaseAddress():
        return 0x0500000

    def _pdaRegisterProvider(self):
        """
        :rtype: python.arrive.atsdk.AtRegister.AtRegisterProvider
        """
        provider = self.registerProviderFactory.providerByName("_AF6CCI0011_RD_PDA")
        provider.baseAddress = self.pdaBaseAddress()
        return provider

    def _getPdaRegister(self, regName, offset):
        """
        :rtype: python.arrive.atsdk.AtRegister.AtRegister
        """
        return self._pdaRegisterProvider().getRegister(regName, offset, readHw=True)

    def _tdmPwId(self):
        cli = "debug pw table %s" % self.pw.cliId()
        result = self.runCli(cli)
        return int(result.cellContent(0, "tdmPwId"))

    def __init__(self, device, pw):
        """
        :type pw: python.arrive.atsdk.pw.AtPw
        """
        super(Tha60210051PdaChecker, self).__init__(device)
        self.pw = pw

    @staticmethod
    def _circuitHwId(circuit):
        return circuit.channelHwId(["pda"])[0]

    def _checkPda_ramjitbufcfg(self):
        pw = self.pw
        reg = self._getPdaRegister("ramjitbufcfg", offset=pw.channelId)
        reg.assertFieldEqual("PwLowDs0Mode", 0)
        reg.assertFieldEqual("PwCEPMode", 1)

        circuit = pw.circuit
        hwSlice, hwSts = self._circuitHwId(circuit)
        reg.assertFieldEqual("PwHoLoOc48Id", hwSlice)
        fromLoBus = self._isFromLoBus(circuit)
        if fromLoBus:
            reg.assertFieldEqual("PwLoSlice24Sel", hwSlice % 2)

        reg.assertFieldEqual("PwHiLoPathInd", 0 if fromLoBus else 1)
        reg.assertFieldEqual("PwPayloadLen", pw.payloadSize)

        jitterBuffer = pw.jitterBuffer
        reg.assertFieldEqual("PdvSizeInPkUnit", jitterBuffer.jitterBufferDelayInPackets)
        reg.assertFieldEqual("JitBufSizeInPkUnit", jitterBuffer.jitterBufferSizeInPackets)

    @staticmethod
    def _expectedPdaMode(circuit):
        if isinstance(circuit, pdh.AtPdhDe1):
            return 0
        if isinstance(circuit, pdh.AtPdhNxDs0):
            return 2
        if isinstance(circuit, sdh.AtSdhVc3):
            return 8
        if isinstance(circuit, sdh.AtSdhVc1x):
            return 12
        if isinstance(circuit, pdh.AtPdhDe3):
            return 15

        return 0xFF

    def _checkPda_ramlotdmmodecfg(self):
        pw = self.pw
        circuit = pw.circuit

        if not self._isFromLoBus(circuit):
            return

        hwSlice, hwSts = self._circuitHwId(circuit)
        slice48 = hwSlice / 2
        slice24 = hwSlice % 2
        regOffset = slice48 * 0x1000 + slice24 * 0x400 + self._tdmPwId()
        reg = self._getPdaRegister("ramlotdmmodecfg", offset=regOffset)
        reg.assertFieldEqual("PDAStsId", hwSts)
        reg.assertFieldEqual("PDAMode", self._expectedPdaMode(circuit))
        reg.assertFieldEqual("PDAFracEbmEn", 0)

    def check(self):
        self._checkPda_ramjitbufcfg()
        self._checkPda_ramlotdmmodecfg()

class Tha60210051DeviceChecker(Tha60210051DefaultChecker):
    def _checkPwPda(self, pw):
        pdaChecker = Tha60210051PdaChecker(self.device, pw)
        pdaChecker.check()

if __name__ == '__main__':
    factory = CheckerFactory()
    checker = factory.createDeviceChecker(AtDevice.currentDevice())
    checker.check()
    AtColor.printColor(AtColor.GREEN, "No problems are found")

import os
import re

from AtColor import AtColor

try:
    import atsdk
except:
    pass

from AtCliResult import AtCliResult

def runCli(cli):
    return atsdk.runCli(cli)

class AtDeviceVersion(object):
    def _parse(self):
        if self.versionString is None:
            return

        numFormat = "[0-9a-zA-Z]+"
        expression = "(%s)[.](%s)\s+[(]on\s+(%s)[/](%s)[/](%s)[)],\s+built:\s*(%s).\s*(%s)" % \
                     (numFormat, numFormat, numFormat, numFormat, numFormat, numFormat, numFormat)
        match = re.match(expression, self.versionString)
        
        def hexFromString(string):
            newString = "0x%s" % string.strip()
            return int(newString, 16)
        
        self.major = hexFromString(match.group(1))
        self.minor = hexFromString(match.group(2))
        self.year = hexFromString(match.group(3))
        self.month = hexFromString(match.group(4))
        self.day = hexFromString(match.group(5))
        
        builtString = "%s%s" % (match.group(6).strip(), match.group(7).strip())
        self.built = hexFromString(builtString) 

    def toNumber(self):
        return (self.major << 24) | (self.minor << 16) | self.built

    def __init__(self, versionString):
        object.__init__(self)
        self.versionString = versionString
        self.major = None
        self.minor = None
        self.year = None
        self.month = None
        self.day = None
        self.built = None
        self._parse()

    def __cmp__(self, other):
        if self.toNumber() > other.toNumber():
            return 1
        if self.toNumber() < other.toNumber():
            return -1

        return 0

class AtAppManager(object):
    _localApp = None
    _remoteApps = {}
    selectedApp = None
    
    @classmethod
    def localApp(cls):
        """
        :rtype: AtApp
        """
        if cls._localApp is None:
            cls._localApp = _LocalApp()
        return cls._localApp
    
    @classmethod
    def remoteApp(cls, ipAddress, port):
        # It may exist
        app = None
        try:
            app = cls._allRemoteApps()[ipAddress]
        except:
            pass
        
        # Or just create a new one
        if app is None:
            app = _RemoteApp(ipAddress, port)
            cls._allRemoteApps()[ipAddress] = app
        
        return app
    
    @classmethod
    def _allRemoteApps(cls):
        return cls._remoteApps
    
    @classmethod
    def selectApp(cls, app):
        if cls.selectedApp == app:
            return app
        
        if cls.selectedApp:
            cls.selectedApp.disconnect()
        app.connect()
        cls.selectedApp = app
        return app

class AtApp(object):
    def detectSimulation(self):
        self._simulated = self.isSimulatedProductCode(self.productCode(returnActualCode=True))

    def __init__(self):
        self.verbose = False
        self.cliLog = []
        self.cliLogEnabled = False
        self._simulated = False

    def _connected(self):
        pass
    def connect(self):
        pass
    def disconnect(self):
        pass
        
    def enableCliLog(self, enabled = True):
        self.cliLogEnabled = enabled 
    def showCliLog(self, color=None):
        for cli in self.cliLog:
            if color is None:
                print cli
            else:
                AtColor.printColor(color, cli)
        
    def _logCli(self, cli):
        if self.cliLogEnabled:
            self.cliLog.append(cli)
        
        if self.verbose:
            print cli
    
    def _willRunCli(self, cli):
        pass
    
    def _runCli(self, cli, timeout=10):
        AtAppManager.selectApp(self)
        return AtCliResult.localResult(runCli(cli))

    @staticmethod
    def _isComment(cli):
        cli = cli.strip()
        
        if re.match(r'^[#].*', cli) is None:
            return False
        return True
    
    def runCli(self, cli, timeout = 10):
        def notifyAndLog(cli_):
            self._willRunCli(cli_)
            self._logCli(cli_)
        
        if cli.count("\n") == 0:
            notifyAndLog(cli)
            return self._runCli(cli, timeout)

        results = []
        for aCli in cli.splitlines():
            aCli = aCli.strip()
            if len(aCli) == 0:
                continue
            
            if self._isComment(aCli):
                continue
            
            notifyAndLog(aCli)
            cliResult = aCli, self._runCli(aCli, timeout)
            results.append(cliResult)
            
            if not cliResult[1].success():
                break
        
        return results
    
    @staticmethod
    def _commandForScript(script):
        _, extension = os.path.splitext(script)
        
        if extension == ".tcl":
            return "tcl %s" % script
        if extension == ".py":
            return "python %s" % script
        
        if (script.count("\n") == 0) and (len(extension) > 0):
            return "run %s" % script
        
        return None 
    
    def runScript(self, script, timeout=-1, options=None):
        command = self._commandForScript(script)
        if command and options:
            command += " %s" % options 
            
        if command:
            return self.runCli(command, timeout)
        
        return self.runCli(script, timeout)

    @staticmethod
    def productCodeFamilyMask():
        from python.arrive.atsdk.AtRegister import cBit31_28
        return cBit31_28
    
    def productCodeFamily(self, productCode):
        return (productCode & self.productCodeFamilyMask()) >> 28
    
    def productCode(self, returnActualCode = False):
        cliResult = self.runCli("show driver")
        productCode = int(cliResult.cellContent(0, "Product code"), 16)
        if returnActualCode:
            return productCode
        
        productCode = (productCode & (~self.productCodeFamilyMask())) | (0x6 << 28)
        return productCode
    
    def isSimulatedProductCode(self, productCode):
        if self.productCodeFamily(productCode) == 0xF:
            return True
        return False

    def isSimulated(self):
        return self._simulated

    def deviceVersion(self):
        """
        :rtype: AtDeviceVersion
        """
        cliResult = self.runCli("show driver")
        version = AtDeviceVersion(cliResult.cellContent(0, "Version"))
        return version
    
class _LocalApp(AtApp):
    def __init__(self):
        AtApp.__init__(self)
        self._isConnected = True
        runCli("textui remote disconnect")
        self.detectSimulation()

    def _connected(self):
        return self._isConnected
    
    def connect(self):
        if self._connected():
            return
        runCli("textui remote disconnect")
        self._isConnected = True
        
    def disconnect(self):
        self._isConnected = False
    
class _RemoteApp(AtApp):
    def __init__(self, ipAddress, port):
        AtApp.__init__(self)
        self._ipAddress = ipAddress
        self._port      = port
        self._isConnected = False
    
    def _connected(self):
        return self._isConnected
    
    def connect(self):
        if self._connected():
            return
        runCli("textui remote connect " + self._ipAddress + ":" + `self._port`)
        runCli("autotest enable")
        self._isConnected = True
        self.detectSimulation()
        
    def disconnect(self):
        if not self._connected():
            return

        runCli("autotest disable")
        runCli("textui remote disconnect ")
        self._isConnected = False
    
    def _willRunCli(self, cli):
        AtAppManager.selectApp(self)
    
    def _runCli(self, cli, timeout=60):
        atsdk.remoteTimeoutSet(timeout)
        runCli(cli)
        cliResult = atsdk.remoteCliResult()
        if cliResult is None:
            return None
        
        return AtCliResult.xmlResult(cliResult) 

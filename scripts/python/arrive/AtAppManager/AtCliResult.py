_hasAtSdk = True

try:
    import re
    import xmltodict
except:
    pass

try:
    import atsdk
    import globalTable
except:
    _hasAtSdk = False

class AtCliResult(object):
    def __init__(self):
        self._rowNames = None
        self.name = None
    
    @classmethod
    def localResult(cls, ret):
        if _hasAtSdk:
            return _DefaultResult(ret)
        return None
    
    @classmethod
    def xmlResult(cls, cliXmlResult):
        return _XmlResult(cliXmlResult)
    
    def success(self):
        return False
    
    def columnId(self, columnName):
        return None
    
    def columnName(self, columnId):
        return None
    
    def _cellContent(self, rowId, columnIdOrName):
        return None
    
    def _cacheRowNames(self):
        self._rowNames = {}
        
        for row_i in range(0, self.numRows()):
            rowName = self._cellContent(row_i, 0)
            self._rowNames[rowName] = row_i
    
    def _rowNameToRowId(self, rowName):
        if self._rowNames is None:
            self._cacheRowNames()
        return self._rowNames[rowName]
    
    def cellContent(self, rowId, columnIdOrName):
        """
        :rtype: str
        """
        if type(rowId) == str:
            rowId = self._rowNameToRowId(rowId)
        
        return self._cellContent(rowId, columnIdOrName)
            
    def numRows(self):
        return 0
    
    def numColumns(self):
        return None
    
    def raw(self):
        return None
    
class _DefaultResult(AtCliResult):
    def __init__(self, ret):
        AtCliResult.__init__(self)
        self._ret = ret
        self._columnName2IdDict = dict()
        self._columnId2NameDict = dict()
        self._rows = list()
        self._copyData()

    def _copyData(self):
        for columnId in range(globalTable.numColumns()):
            columnName = globalTable.columnName(columnId)
            self._columnName2IdDict[columnName] = columnId
            self._columnId2NameDict[columnId] = columnName

        for rowId in range(globalTable.numRows()):
            # Copy current row
            row = list()
            for columnId in range(globalTable.numColumns()):
                cellContent = globalTable.cellContent(rowId, columnId)
                row.append(cellContent)

            self._rows.append(row)

    def success(self):
        return self._ret == 0
    
    def columnId(self, columnName):
        return self._columnName2IdDict[columnName]

    def columnName(self, columnId):
        return self._columnId2NameDict[columnId]

    def _cellContent(self, rowId, columnIdOrName):
        columnId = columnIdOrName
        if isinstance(columnId, str):
            columnId = self.columnId(columnIdOrName)
        return self._rows[rowId][columnId]

    def numRows(self):
        try:
            return len(self._rows)
        except:
            return 0
    
    def numColumns(self):
        try:
            return len(self._rows[0])
        except:
            return 0

    def raw(self):
        if self.success():
            return "OK"
        return "FAIL"

class _XmlResult(AtCliResult):
    def __init__(self, cliXmlResult):
        AtCliResult.__init__(self)
        
        self.normalizedOutput = self._normalizeOutput(cliXmlResult)
        xmlFilterExpression = re.compile(r'(<Command name=.*?</Command>)', flags=re.MULTILINE|re.DOTALL)
        xmlString = xmlFilterExpression.findall(self.normalizedOutput)
        if len(xmlString) == 0:
            self._dict = None
            return
        
        commandDict = None
        xmlDict = None

        # Parse all of elements
        try:
            xmlDict = xmltodict.parse(xmlString[0])
            commandDict = xmlDict["Command"]
            self.name = commandDict["@name"]
            self.returnCode = commandDict["Cli_Result"]["@retcode"]
                
        except:
            self.returnCode = "Cannot Parse XML"
        
        # See if we can parse table output
        try:
            rows = commandDict["Table"]["Row"]
            if isinstance(rows, xmlDict.__class__):
                self.rows = [rows]
            else:
                self.rows = rows
        except:
            self.rows = list()
            pass
        
    @staticmethod
    def _normalizeOutput(outputString):
        try:
            import unicodedata
            outputString = unicode(outputString, errors = 'replace')
            outputString = unicodedata.normalize('NFKD', outputString).encode('ascii','ignore')
        except:
            pass
        
        retStr = outputString
        retStr = re.sub("(\\x1b\[J)|(\\x1b\[\d{1,2}m)", "", retStr)
        retStr = re.sub("\033\[[0-9;]+m", "", retStr)
        
        return retStr.strip() 

    def _cellsAtRow(self, rowIndex):
        return self.rows[rowIndex]["Cell"]
    
    def _cellContent(self, rowIndex, columnIdOrName):
        columName = columnIdOrName
        if not isinstance(columnIdOrName, str):
            columName = self.columnName(columnIdOrName)
        
        cells = self._cellsAtRow(rowIndex)
        for cellDict in cells:
            if cellDict["@name"] == columName:
                return cellDict["@value"]
            
        return None
    
    def columnId(self, columnName):
        cells = self._cellsAtRow(0)
        for i in range(0, len(cells)):
            cellDict = cells[i]
            if cellDict["@name"] == columnName:
                return i
        return -1
    
    def columnName(self, columnId):
        cells = self._cellsAtRow(0)
        cellDict = cells[columnId]
        return cellDict["@name"]
    
    def numColumns(self):
        try:
            cells = self._cellsAtRow(0)
            return len(cells)
        except:
            return 0
    
    def numRows(self):
        return len(self.rows)
    
    def success(self):
        return self.returnCode == "OK"
    
    def raw(self):
        return self.normalizedOutput

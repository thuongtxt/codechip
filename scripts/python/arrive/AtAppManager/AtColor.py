class AtColor(object):
    _enabled = True

    RED    = "\033[1;31m"
    PINK   = "\033[1;35m"
    YELLOW = "\033[1;33m"
    CYAN   = "\033[1;36m"
    GREEN  = "\033[1;32m"
    CLEAR  = "\033[0m"
    
    @classmethod
    def printColor(cls, color, string):
        if cls._enabled:
            print color + string + AtColor.CLEAR
        else:
            print string

    @classmethod
    def enableColor(cls, enabled = True):
        cls._enabled = enabled

    @classmethod
    def colorIsEnabled(cls):
        return cls._enabled

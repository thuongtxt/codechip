"""
Created on Apr 15, 2017

@author: namnn
"""
from module import AtModule
from cli import AtCliRunnable
from sdh import AtSdhVc
from pdh import AtPdhChannel, AtPdhNxDs0
import time
from python.arrive.AtAppManager.AtColor import AtColor

class AtPrbsEngineFactory(object):
    @staticmethod
    def createEngineForCircuit(amodule, engineId, circuit):
        return AtPrbsEngine(amodule, engineId, circuit)

class AtModulePrbs(AtModule):
    def __init__(self, device):
        AtModule.__init__(self, device)
        self.prbsEngineFactory = AtPrbsEngineFactory()

    @staticmethod
    def _cliToCreatePrbsEngineFromCircuitIdString(engineId, circuit):
        tokens = circuit.split(".")
        circuitType = tokens[0]
        circuitId = ".".join(tokens[1:len(tokens)])
        
        if circuitType in ["de1", "de3"]:
            return "prbs engine create %s %d %s" % (circuitType, engineId + 1, circuitId)
        
        if circuitType == "nxds0":
            circuitId = ".".join(tokens[1:len(tokens) - 1])
            timeslots = tokens[len(tokens) - 1]
            return "prbs engine create nxds0 %d %s %s" % (engineId + 1, circuitId, timeslots)
        
        if "vc" in circuitType:
            return "prbs engine create vc %d %s" % (engineId + 1, circuit)
        
        return None

    @staticmethod
    def _cliToCreatePrbsEngineFromCircuitObject(engineId, circuit):
        if isinstance(circuit, AtSdhVc):
            return "prbs engine create vc %d %s.%s" % (engineId + 1, circuit.cliType(), circuit.cliId()) 
        
        if isinstance(circuit, AtPdhNxDs0):
            return "prbs engine create nxds0 %d %s %s" % (engineId + 1, circuit.parent.cliId(), circuit.timeslots())
        
        if isinstance(circuit, AtPdhChannel):
            return "prbs engine create %s %d %s" % (circuit.cliType(), engineId + 1, circuit.cliId())
        
        return None

    def _cliToCreatePrbsEngine(self, engineId, circuit):
        if type(circuit) is str:
            return self._cliToCreatePrbsEngineFromCircuitIdString(engineId, circuit)
        
        return self._cliToCreatePrbsEngineFromCircuitObject(engineId, circuit)
        
    def createPrbsEngine(self, engineId, circuit):
        cli = self._cliToCreatePrbsEngine(engineId, circuit)
        assert(cli is not None)
        self.runCli(cli)
        return self.prbsEngineFactory.createEngineForCircuit(self, engineId, circuit)
    
    def deletePrbsEngine(self, engine):
        self.runCli("prbs engine delete %s" % engine.cliId())

class AtPrbsEngineCounters(object):
    def __init__(self):
        object.__init__(self)
        self.txFrame = None
        self.txBit = None
        self.rxFrame = None
        self.rxBit = None
        self.rxBitError = None
        self.rxBitLastSync = None
        self.rxBitErrorLastSync = None
        self.rxSync = None
        self.rxLoss = None
        self.rxLostFrame = None
        self.rxErrorFrame = None
    
    def good(self):
        def checkGoodCounter(counterValue):
            if counterValue is not None and counterValue <= 0:
                return False
            return True
        
        def checkErrorCounter(counterValue):
            if counterValue is not None and counterValue > 0:
                return True
            return False
        
        if not checkGoodCounter(self.txFrame) or \
           not checkGoodCounter(self.txBit)   or \
           not checkGoodCounter(self.rxFrame) or \
           not checkGoodCounter(self.rxBit)   or \
           not checkGoodCounter(self.rxSync):
            return False
            
        if checkErrorCounter(self.rxBitError)   or \
           checkErrorCounter(self.rxLoss)       or \
           checkErrorCounter(self.rxLostFrame)  or \
           checkErrorCounter(self.rxErrorFrame):
            return False
        
        return True
    
    @classmethod
    def countersFromCliResult(cls, cliResult):
        def counterFromString(string):
            if string in ["N/A", "N/S", "xxx"]:
                return None
            return int(string, 10)
        
        counters = AtPrbsEngineCounters()
        counters.txFrame = counterFromString(cliResult.cellContent(0, "txFrame"))
        counters.txBit = counterFromString(cliResult.cellContent(0, "TxBit"))
        counters.rxFrame = counterFromString(cliResult.cellContent(0, "rxFrame"))
        counters.rxBit = counterFromString(cliResult.cellContent(0, "RxBit"))
        counters.rxBitError = counterFromString(cliResult.cellContent(0, "RxBitError"))
        counters.rxBitLastSync = counterFromString(cliResult.cellContent(0, "RxBitLastSync"))
        counters.rxBitErrorLastSync = counterFromString(cliResult.cellContent(0, "RxBitErrorLastSync"))
        counters.rxSync = counterFromString(cliResult.cellContent(0, "RxSync"))
        counters.rxLoss = counterFromString(cliResult.cellContent(0, "RxLoss"))
        counters.rxLostFrame = counterFromString(cliResult.cellContent(0, "RxLostFrame"))
        counters.rxErrorFrame = counterFromString(cliResult.cellContent(0, "RxErrorFrame"))
        
        return counters

class AtPrbsEngine(AtCliRunnable):
    SIDE_PSN = "psn"
    SIDE_TDM = "tdm"
    
    BIT_ORDER_MSB = "msb"
    BIT_ORDER_LSB = "lsb"
    
    PRBS_MODE_PRBS9 = "prbs9"
    PRBS_MODE_PRBS15 = "prbs15"
    PRBS_MODE_PRBS23 = "prbs23"
    PRBS_MODE_PRBS31 = "prbs31"
    PRBS_MODE_PRBS11 = "prbs11"
    PRBS_MODE_PRBS20 = "prbs20"
    PRBS_MODE_PRBS20QRSS = "prbs20qrss"
    PRBS_MODE_PRBS20R = "prbs20r"
    PRBS_MODE_SEQUENCE = "sequence"
    PRBS_MODE_FIXEDPATTERN1BYTE = "fixedpattern1byte"
    PRBS_MODE_FIXEDPATTERN2BYTES = "fixedpattern2bytes"
    PRBS_MODE_FIXEDPATTERN3BYTES = "fixedpattern3bytes"
    PRBS_MODE_FIXEDPATTERN4BYTES = "fixedpattern4bytes"
    PRBS_MODE_PRBS7 = "prbs7"
    
    def __init__(self, amodule, engineId, circuit):
        AtCliRunnable.__init__(self)
        self.module = amodule
        self.circuit = circuit
        self.engineId = engineId
        self.simulated = self.module.device.simulated
        
    def cliId(self):
        return "%d" % (self.engineId + 1)

    def enable(self, enabled = True):
        self.runCli("prbs engine %s %s" % ("enable" if enabled else "disable", self.cliId()))

    @property
    def monitoringSide(self):
        return

    @monitoringSide.setter
    def monitoringSide(self, value):
        self.runCli("prbs engine side monitoring %s %s" % (self.cliId(), value))

    @property
    def generatingSide(self):
        return

    @generatingSide.setter
    def generatingSide(self, value):
        self.runCli("prbs engine side generating %s %s" % (self.cliId(), value))

    def clearStatus(self):
        self.runCli("show prbs engine %s" % self.cliId())
        self.runCli("show prbs engine counters %s r2c" % self.cliId())
    
    def alarmStatus(self):
        """
        @return tuple(alarm, sticky)
        """
        result = self.runCli("show prbs engine %s" % self.cliId())
        
        alarm = result.cellContent(0, "alarm")
        if alarm == "none":
            alarm = None
        
        sticky = result.cellContent(0, "sticky")
        if sticky == "none":
            sticky = None
        
        if self.simulated:
            sticky = None
            alarm = None
        
        return alarm, sticky
        
    def getCounters(self, clear = False):
        cli = "show prbs engine counters %s %s" % (self.cliId(), "r2c" if clear else "ro")
        result = self.runCli(cli)
        return AtPrbsEngineCounters.countersFromCliResult(result)
    
    def good(self):
        alarm, sticky = self.alarmStatus()
        if alarm is not None or sticky is not None:
            return False
        
        counters = self.getCounters()
        return counters.good()
    
    def disruptionTimeInUsCalculate(self, clearCounters = True, waitTillSeeError = False):
        # Too bad, BERT counters are not updated in realtime manner, need the below block
        if waitTillSeeError:
            timeout = 10
            startTime = time.time()
            while True:
                counters = self.getCounters()
                hasErrors = (counters.rxBitError + counters.rxLoss) > 0
                if hasErrors:
                    break
                
                if time.time() - startTime >= timeout:
                    break
                
                time.sleep(0.5)
        
        self.getCounters() # Use first time to latch???
        counters = self.getCounters(clear = clearCounters)
        numBitErrors = (counters.rxBitError + counters.rxLoss) * 1.0
        rateInBps = self.circuit.rateInBps * 1.0
        disruptionTimeSeconds = numBitErrors / rateInBps
        return disruptionTimeSeconds * 1000000

    @property
    def bitOrder(self):
        return

    @bitOrder.setter
    def bitOrder(self, value):
        self.runCli("prbs engine bitorder %s %s" % (self.cliId(), value))

    @property
    def inverted(self):
        return None

    @inverted.setter
    def inverted(self, value):
        self.runCli("prbs engine %s %s" % ("invert" if value else "noinvert", self.cliId()))

    @property
    def mode(self):
        return

    @mode.setter
    def mode(self, value):
        self.runCli("prbs engine mode %s %s" % (self.cliId(), value))

    @property
    def fixPattern(self):
        return

    @fixPattern.setter
    def fixPattern(self, value):
        self.runCli("prbs engine fixpattern %s 0x%x 0x%x" % (self.cliId(), value, value))

    @property
    def forcedBer(self):
        return

    @forcedBer.setter
    def forcedBer(self, value):
        self.runCli("prbs engine force ber %s %s" % (self.cliId(), value))

class AtPrbsStableChecker(AtCliRunnable):
    STATE_FIRST_STABLE_WAIT = "first stable wait"
    STATE_STABLE_CONFIRM = "stable confirm"
    STATE_COMPLETE = "complete"
    
    def __init__(self, engines, timeout = 30):
        AtCliRunnable.__init__(self)
        self.engines = engines
        self.state = None
        self.good = False
        self.timeout = timeout
        self.simulated = False
        self.quickCheck = False
        self.firstStableWaitTime = 5
        self.postStableWaitTime = 20
    
    def _engineIsGood(self, engine):
        good = engine.good()
        return True if self.simulated else good
    
    def _firstStableWait(self):
        startTime = time.time()
        
        while 1:
            allGood = True
            for engine in self.engines:
                if not self._engineIsGood(engine):
                    engine.clearStatus()
                    allGood = False
            
            if allGood:
                AtColor.printColor(AtColor.GREEN, "All PRBS engines are good, move to stable confirm state")
                self.state = self.STATE_STABLE_CONFIRM
                return self.state
            
            # Retry
            time.sleep(1)
            elapseTime = time.time() - startTime
            if elapseTime > self.firstStableWaitTime:
                break
        
        # Fail to wait
        AtColor.printColor(AtColor.RED, "PRBS engines are not stable within %s (s)" % (time.time() - startTime))
        self.state = self.STATE_COMPLETE
        return self.state
    
    def _stableConfirm(self):
        startTime = time.time()
        
        while 1:
            # If at least one engine is bad, move back to first stable wait state 
            # so that BERT status can be clear and state machine will run over again
            for engine in self.engines:
                if not self._engineIsGood(engine):
                    AtColor.printColor(AtColor.YELLOW, "Engine %s is not stable, move to first state" % engine.cliId())
                    self.state = self.STATE_FIRST_STABLE_WAIT
                    return self.state
            
            if self.simulated:
                break
            
            # Continue
            time.sleep(1)
            elapseTime = time.time() - startTime
            if elapseTime > self.postStableWaitTime:
                break
    
        # Stable
        self.good = True
        self.state = self.STATE_COMPLETE
        return self.state
    
    def _runStateMachine(self):
        if self.state is None:
            self.state = self.STATE_FIRST_STABLE_WAIT
            
        if self.state == self.STATE_FIRST_STABLE_WAIT:
            self._firstStableWait()
        
        if self.state == self.STATE_STABLE_CONFIRM:
            self._stableConfirm()
        
        return self.state
    
    def _checkStable(self):
        startTime = time.time()
        
        while 1:
            if self._runStateMachine() == self.STATE_COMPLETE:
                return self.good
            
            # Retry
            elapseTime = time.time() - startTime
            if elapseTime > self.timeout:
                break 
            
        return self.good

    def check(self):
        if self.quickCheck:
            self.firstStableWaitTime = 2
            self.postStableWaitTime = 2
        
        return self._checkStable()

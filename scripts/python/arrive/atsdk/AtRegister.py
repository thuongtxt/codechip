"""
Note: when this source file is changed, need to copy it to /home/ATVN_Eng/af6xxx/atsdk/python/arrive/atsdk/
"""
from python.arrive.AtAppManager.AtAppManager import AtAppManager

hasSdk = False
try:
    import atsdk
    hasSdk = True
except:
    pass

class AtRegisterFieldAssertError(Exception):
    pass

class AtRegisterField(object):
    def __init__(self, start = None, stop = None, name = None):
        object.__init__(self)
        self._start = start
        self._stop = stop
        self._name = name
    
    def startBit(self):
        return self._start
    
    def stopBit(self):
        return self._stop
    
    def name(self):
        return self._name
    
    def description(self):
        return None
    
    def type(self):
        return None
    
    def resetValue(self):
        return None
    
    def test(self):
        assert(type(self.startBit()) is int)
        assert(type(self.stopBit()) is int)
        assert(type(self.name()) is str)
        assert(type(self.description()) is str)
        assert(type(self.type()) is str)
        assert(type(self.resetValue()) is int)
    
    @staticmethod
    def _mask(startBit, stopBit):
        mask = 0
        for bit in range(startBit, stopBit + 1):
            mask |= (1 << bit)
        return mask
    
    def mask(self):
        return self._mask(self.startBit(), self.stopBit())
    
    def shift(self):
        return self.startBit()
    
    def lsbDwordIndex(self):
        return self.startBit() / 32
    
    def msbDwordIndex(self):
        return self.stopBit() / 32
    
    def lsbStartBit(self):
        return self.startBit() % 32
    
    def lsbStopBit(self):
        if self.lsbDwordIndex() == self.msbDwordIndex():
            return self.stopBit() % 32
        return 31
    
    def lsbNumBits(self):
        return self.lsbStopBit() - self.lsbStartBit() + 1
    
    def lsbMask(self):
        return self._mask(self.lsbStartBit(), self.lsbStopBit())
    
    def lsbShift(self):
        return self.lsbStartBit()
    
    def msbStartBit(self):
        if self.lsbDwordIndex() == self.msbDwordIndex():
            return None
        return 0
    
    def msbStopBit(self):
        return self.stopBit() % 32
    
    def msbNumBits(self):
        return self.msbStopBit() - self.msbStartBit() + 1
    
    def msbMask(self):
        if self.lsbDwordIndex() == self.msbDwordIndex():
            return None
        return self._mask(self.msbStartBit(), self.msbStopBit())
    
    def msbShift(self):
        if self.lsbDwordIndex() == self.msbDwordIndex():
            return None
        return self.msbStartBit()
    
    def subField(self, start, stop = None, name = None):
        startBit = self.startBit() + start
        
        if stop is None:
            stop = start
            defaultName = "%s[%d]" % (self.name(), stop)
        else:
            defaultName = "%s[%d:%d]" % (self.name(), stop, start)
           
        if name is None:
            name = defaultName
            
        stopBit = self.startBit() + stop
        return AtRegisterField(startBit, stopBit, name)

class AtRegister(object):
    _fieldDicts = None

    def _allFieldDicts(self):
        return None
    
    def allFields(self):
        if self._fieldDicts is None:
            self._fieldDicts = self._allFieldDicts()
        return self._fieldDicts
    
    def width(self):
        return 32
    
    def fieldByName(self, fieldName):
        """
        :rtype: AtRegisterField
        """
        name = fieldName
        name = name.replace(" ", "_")
        name = name.replace("[", "_")
        name = name.replace("]", "_")
        return self.allFields()[fieldName.replace(" ", "_")]
    
    def _shortFieldValue(self, field):
        return self.getField(field.mask(), field.shift())
    
    def _longFieldValue(self, field):
        lsb = self._getField(self.values[field.lsbDwordIndex()], field.lsbMask(), field.lsbShift())
        value = lsb
        
        if field.msbMask():
            msb = self._getField(self.values[field.msbDwordIndex()], field.msbMask(), field.msbShift())
            value = msb << field.lsbNumBits() | lsb
            
        return value
        
    def fieldValue(self, field):
        if field.stopBit() < 32:
            return self.getField(field.mask(), field.shift())
        return self._longFieldValue(field)
        
    def fieldValueByName(self, fieldName):
        field = self.fieldByName(fieldName)
        return self.fieldValue(field)
    
    def setFieldValueByName(self, fieldName, value, applyHw = False):
        field = self.fieldByName(fieldName)
        self.setField(field.mask(), field.shift(), value)
        if applyHw:
            self.apply()
    
    def __init__(self, value = None):
        self.values = None
        self.address = None
        self.name = "Register"
        
        if isinstance(value, AtRegister):
            self.value = value.value
        else:
            self.value = value
    
    @staticmethod
    def _getField(value, mask, shift):
        return (value & mask) >> shift
    
    def getField(self, mask, shift):
        return self._getField(self.value, mask, shift)
    
    def setField(self, mask, shift, value):
        self.value = ((self.value & (~mask)) | ((value << shift) & mask)) 
        return self.value
    
    def getBit(self, bitPosition):
        mask = cBit0 << bitPosition
        return self.getField(mask, bitPosition)  
    
    def setBit(self, bitPosition, bitValue, applyHw=False):
        mask = cBit0 << bitPosition
        newValue = self.setField(mask, bitPosition, bitValue)
        
        if applyHw:
            self.apply()
        
        return newValue

    def rd(self, address = None, core = 0):
        if address is None:
            address = self.address
            
        self.value = atsdk.rd(address, core)
        return self.value
    
    def apply(self):
        self.wr(self.address, self.value)
    
    def wr(self, address, value):
        AtAppManager.localApp().runCli("wr %x %x" % (address, value))
        if self.address == address:
            self.value = value
        
    def lrd(self, address = None, core = 0):
        if address is None:
            address = self.address
        
        self.values = [value & cBit31_0 for value in atsdk.lrd(address, core)]
        self.value = self.values[0]
        return self.values
    
    @staticmethod
    def lwr(address, valueOrString):
        aString = None        
        if type(valueOrString) == list:
            for value in valueOrString:
                if aString is None:
                    aString = "0x%x" % value
                else:
                    aString += ".%x" % value
                    
            AtAppManager.localApp().runCli("lwr %x %s" % (address, valueOrString))
            return
        
        AtAppManager.localApp().runCli("lwr %x %s" % (address, valueOrString))

    def _testAccessHw(self):
        self.wr(0, 1)
        assert(self.rd(0) == 1)
        self.wr(0, 2)
        assert(self.rd(0) == 2)
        
        self.lwr(0, "0x1.2.3")
        values = self.lrd(0)
        assert(values[0] == 3)
        assert(values[1] == 2)
        assert(values[2] == 1)
        
    def _testAllFields(self):
        for _, field in self.allFields().iteritems():
            field.test()
        
    def test(self):
        global hasSdk
        if hasSdk:
            self._testAccessHw()
            
        self._testAllFields()
    
    def display(self):
        pass

    def startAddress(self):
        return None
            
    def endAddress(self):
        return None
    
    def assertFieldEqual(self, field, expectedValue, message = None):
        if type(field) is str:
            field = self.fieldByName(field)
            
        fieldValue = self.fieldValue(field)
        if fieldValue == expectedValue:
            return
        
        if message is None:
            message = "expected: 0x%x, actual: 0x%x" % (expectedValue, fieldValue)
        reportedMessage = "%s@0x%08x[%s]: %s" % (self.name, self.address, field.name(), message)
            
        raise AtRegisterFieldAssertError(reportedMessage)

    @classmethod
    def longValueFromString(cls, stringValue):
        """
        :type stringValue: str
        """
        tokens = stringValue.split(".")
        tokens.reverse()
        return [int(valueString, 16) for valueString in tokens]

    @classmethod
    def longValueToString(cls, longValue):
        """
        :type longValue: list
        """
        longValueString = ".".join(["%08x" % value for value in reversed(longValue)])
        return "0x" + longValueString

class AtRegisterProvider(object):
    _allRegisterDicts = None
    
    def __init__(self):
        object.__init__(self)
        self.baseAddress = None
    
    @classmethod
    def _allRegisters(cls):
        """
        :rtype: dict
        """
        return dict()
    
    @classmethod
    def allRegisters(cls):
        """
        :rtype: dict
        """
        if cls._allRegisterDicts is None:
            cls._allRegisterDicts = cls._allRegisters()
        return cls._allRegisterDicts
    
    @classmethod
    def regByName(cls, name):
        """
        :rtype: AtRegister
        """
        return cls.allRegisters()[name]
    
    def test(self):
        allRegisters = self.allRegisters()
        assert(len(allRegisters) > 0)
        for _, aReg in allRegisters.iteritems():
            aReg.test()
    
    def getRegister(self, name, addressOffset, readHw = True):
        """
        :rtype: AtRegister
        """
        aReg = self.regByName(name)
        localAddress = aReg.startAddress() + addressOffset
        aReg.address = self.baseAddress + localAddress
        aReg.name = name
        
        if readHw:
            if aReg.width() <= 32:
                aReg.rd()
            else:
                aReg.lrd()
                
        return aReg
    
class AtRegisterProviderFactory(object):
    _providers = None
    
    def _allRegisterProviders(self):
        return None
    
    def allProviders(self):
        if self._providers is None:
            self._providers = self._allRegisterProviders()
        return self._providers
        
    def providerByName(self, providerName):
        """
        :rtype: AtRegisterProvider
        """
        return self.allProviders()[providerName]
        
    def test(self):
        for _, provider in self.allProviders().iteritems():
            provider.test()
            
        
# Predefine mask and shift
cBit0     = 0x00000001
cBit1     = 0x00000002
cBit2     = 0x00000004
cBit3     = 0x00000008
cBit4     = 0x00000010
cBit5     = 0x00000020
cBit6     = 0x00000040
cBit7     = 0x00000080
cBit8     = 0x00000100
cBit9     = 0x00000200
cBit10    = 0x00000400
cBit11    = 0x00000800
cBit12    = 0x00001000
cBit13    = 0x00002000
cBit14    = 0x00004000
cBit15    = 0x00008000
cBit16    = 0x00010000
cBit17    = 0x00020000
cBit18    = 0x00040000
cBit19    = 0x00080000
cBit20    = 0x00100000
cBit21    = 0x00200000
cBit22    = 0x00400000
cBit23    = 0x00800000
cBit24    = 0x01000000
cBit25    = 0x02000000
cBit26    = 0x04000000
cBit27    = 0x08000000
cBit28    = 0x10000000
cBit29    = 0x20000000
cBit30    = 0x40000000
cBit31    = 0x80000000
cBit0_0   = 0x00000001
cBit1_0   = 0x00000003
cBit2_0   = 0x00000007
cBit3_0   = 0x0000000F
cBit4_0   = 0x0000001F
cBit5_0   = 0x0000003F
cBit6_0   = 0x0000007F
cBit7_0   = 0x000000FF
cBit8_0   = 0x000001FF
cBit9_0   = 0x000003FF
cBit10_0  = 0x000007FF
cBit11_0  = 0x00000FFF
cBit12_0  = 0x00001FFF
cBit13_0  = 0x00003FFF
cBit14_0  = 0x00007FFF
cBit15_0  = 0x0000FFFF
cBit16_0  = 0x0001FFFF
cBit17_0  = 0x0003FFFF
cBit18_0  = 0x0007FFFF
cBit19_0  = 0x000FFFFF
cBit20_0  = 0x001FFFFF
cBit21_0  = 0x003FFFFF
cBit22_0  = 0x007FFFFF
cBit23_0  = 0x00FFFFFF
cBit24_0  = 0x01FFFFFF
cBit25_0  = 0x03FFFFFF
cBit26_0  = 0x07FFFFFF
cBit27_0  = 0x0FFFFFFF
cBit28_0  = 0x1FFFFFFF
cBit29_0  = 0x3FFFFFFF
cBit30_0  = 0x7FFFFFFF
cBit31_0  = 0xFFFFFFFF
cBit1_1   = 0x00000002
cBit2_1   = 0x00000006
cBit3_1   = 0x0000000E
cBit4_1   = 0x0000001E
cBit5_1   = 0x0000003E
cBit6_1   = 0x0000007E
cBit7_1   = 0x000000FE
cBit8_1   = 0x000001FE
cBit9_1   = 0x000003FE
cBit10_1  = 0x000007FE
cBit11_1  = 0x00000FFE
cBit12_1  = 0x00001FFE
cBit13_1  = 0x00003FFE
cBit14_1  = 0x00007FFE
cBit15_1  = 0x0000FFFE
cBit16_1  = 0x0001FFFE
cBit17_1  = 0x0003FFFE
cBit18_1  = 0x0007FFFE
cBit19_1  = 0x000FFFFE
cBit20_1  = 0x001FFFFE
cBit21_1  = 0x003FFFFE
cBit22_1  = 0x007FFFFE
cBit23_1  = 0x00FFFFFE
cBit24_1  = 0x01FFFFFE
cBit25_1  = 0x03FFFFFE
cBit26_1  = 0x07FFFFFE
cBit27_1  = 0x0FFFFFFE
cBit28_1  = 0x1FFFFFFE
cBit29_1  = 0x3FFFFFFE
cBit30_1  = 0x7FFFFFFE
cBit31_1  = 0xFFFFFFFE
cBit2_2   = 0x00000004
cBit3_2   = 0x0000000C
cBit4_2   = 0x0000001C
cBit5_2   = 0x0000003C
cBit6_2   = 0x0000007C
cBit7_2   = 0x000000FC
cBit8_2   = 0x000001FC
cBit9_2   = 0x000003FC
cBit10_2  = 0x000007FC
cBit11_2  = 0x00000FFC
cBit12_2  = 0x00001FFC
cBit13_2  = 0x00003FFC
cBit14_2  = 0x00007FFC
cBit15_2  = 0x0000FFFC
cBit16_2  = 0x0001FFFC
cBit17_2  = 0x0003FFFC
cBit18_2  = 0x0007FFFC
cBit19_2  = 0x000FFFFC
cBit20_2  = 0x001FFFFC
cBit21_2  = 0x003FFFFC
cBit22_2  = 0x007FFFFC
cBit23_2  = 0x00FFFFFC
cBit24_2  = 0x01FFFFFC
cBit25_2  = 0x03FFFFFC
cBit26_2  = 0x07FFFFFC
cBit27_2  = 0x0FFFFFFC
cBit28_2  = 0x1FFFFFFC
cBit29_2  = 0x3FFFFFFC
cBit30_2  = 0x7FFFFFFC
cBit31_2  = 0xFFFFFFFC
cBit3_3   = 0x00000008
cBit4_3   = 0x00000018
cBit5_3   = 0x00000038
cBit6_3   = 0x00000078
cBit7_3   = 0x000000F8
cBit8_3   = 0x000001F8
cBit9_3   = 0x000003F8
cBit10_3  = 0x000007F8
cBit11_3  = 0x00000FF8
cBit12_3  = 0x00001FF8
cBit13_3  = 0x00003FF8
cBit14_3  = 0x00007FF8
cBit15_3  = 0x0000FFF8
cBit16_3  = 0x0001FFF8
cBit17_3  = 0x0003FFF8
cBit18_3  = 0x0007FFF8
cBit19_3  = 0x000FFFF8
cBit20_3  = 0x001FFFF8
cBit21_3  = 0x003FFFF8
cBit22_3  = 0x007FFFF8
cBit23_3  = 0x00FFFFF8
cBit24_3  = 0x01FFFFF8
cBit25_3  = 0x03FFFFF8
cBit26_3  = 0x07FFFFF8
cBit27_3  = 0x0FFFFFF8
cBit28_3  = 0x1FFFFFF8
cBit29_3  = 0x3FFFFFF8
cBit30_3  = 0x7FFFFFF8
cBit31_3  = 0xFFFFFFF8
cBit4_4   = 0x00000010
cBit5_4   = 0x00000030
cBit6_4   = 0x00000070
cBit7_4   = 0x000000F0
cBit8_4   = 0x000001F0
cBit9_4   = 0x000003F0
cBit10_4  = 0x000007F0
cBit11_4  = 0x00000FF0
cBit12_4  = 0x00001FF0
cBit13_4  = 0x00003FF0
cBit14_4  = 0x00007FF0
cBit15_4  = 0x0000FFF0
cBit16_4  = 0x0001FFF0
cBit17_4  = 0x0003FFF0
cBit18_4  = 0x0007FFF0
cBit19_4  = 0x000FFFF0
cBit20_4  = 0x001FFFF0
cBit21_4  = 0x003FFFF0
cBit22_4  = 0x007FFFF0
cBit23_4  = 0x00FFFFF0
cBit24_4  = 0x01FFFFF0
cBit25_4  = 0x03FFFFF0
cBit26_4  = 0x07FFFFF0
cBit27_4  = 0x0FFFFFF0
cBit28_4  = 0x1FFFFFF0
cBit29_4  = 0x3FFFFFF0
cBit30_4  = 0x7FFFFFF0
cBit31_4  = 0xFFFFFFF0
cBit5_5   = 0x00000020
cBit6_5   = 0x00000060
cBit7_5   = 0x000000E0
cBit8_5   = 0x000001E0
cBit9_5   = 0x000003E0
cBit10_5  = 0x000007E0
cBit11_5  = 0x00000FE0
cBit12_5  = 0x00001FE0
cBit13_5  = 0x00003FE0
cBit14_5  = 0x00007FE0
cBit15_5  = 0x0000FFE0
cBit16_5  = 0x0001FFE0
cBit17_5  = 0x0003FFE0
cBit18_5  = 0x0007FFE0
cBit19_5  = 0x000FFFE0
cBit20_5  = 0x001FFFE0
cBit21_5  = 0x003FFFE0
cBit22_5  = 0x007FFFE0
cBit23_5  = 0x00FFFFE0
cBit24_5  = 0x01FFFFE0
cBit25_5  = 0x03FFFFE0
cBit26_5  = 0x07FFFFE0
cBit27_5  = 0x0FFFFFE0
cBit28_5  = 0x1FFFFFE0
cBit29_5  = 0x3FFFFFE0
cBit30_5  = 0x7FFFFFE0
cBit31_5  = 0xFFFFFFE0
cBit6_6   = 0x00000040
cBit7_6   = 0x000000C0
cBit8_6   = 0x000001C0
cBit9_6   = 0x000003C0
cBit10_6  = 0x000007C0
cBit11_6  = 0x00000FC0
cBit12_6  = 0x00001FC0
cBit13_6  = 0x00003FC0
cBit14_6  = 0x00007FC0
cBit15_6  = 0x0000FFC0
cBit16_6  = 0x0001FFC0
cBit17_6  = 0x0003FFC0
cBit18_6  = 0x0007FFC0
cBit19_6  = 0x000FFFC0
cBit20_6  = 0x001FFFC0
cBit21_6  = 0x003FFFC0
cBit22_6  = 0x007FFFC0
cBit23_6  = 0x00FFFFC0
cBit24_6  = 0x01FFFFC0
cBit25_6  = 0x03FFFFC0
cBit26_6  = 0x07FFFFC0
cBit27_6  = 0x0FFFFFC0
cBit28_6  = 0x1FFFFFC0
cBit29_6  = 0x3FFFFFC0
cBit30_6  = 0x7FFFFFC0
cBit31_6  = 0xFFFFFFC0
cBit7_7   = 0x00000080
cBit8_7   = 0x00000180
cBit9_7   = 0x00000380
cBit10_7  = 0x00000780
cBit11_7  = 0x00000F80
cBit12_7  = 0x00001F80
cBit13_7  = 0x00003F80
cBit14_7  = 0x00007F80
cBit15_7  = 0x0000FF80
cBit16_7  = 0x0001FF80
cBit17_7  = 0x0003FF80
cBit18_7  = 0x0007FF80
cBit19_7  = 0x000FFF80
cBit20_7  = 0x001FFF80
cBit21_7  = 0x003FFF80
cBit22_7  = 0x007FFF80
cBit23_7  = 0x00FFFF80
cBit24_7  = 0x01FFFF80
cBit25_7  = 0x03FFFF80
cBit26_7  = 0x07FFFF80
cBit27_7  = 0x0FFFFF80
cBit28_7  = 0x1FFFFF80
cBit29_7  = 0x3FFFFF80
cBit30_7  = 0x7FFFFF80
cBit31_7  = 0xFFFFFF80
cBit8_8   = 0x00000100
cBit9_8   = 0x00000300
cBit10_8  = 0x00000700
cBit11_8  = 0x00000F00
cBit12_8  = 0x00001F00
cBit13_8  = 0x00003F00
cBit14_8  = 0x00007F00
cBit15_8  = 0x0000FF00
cBit16_8  = 0x0001FF00
cBit17_8  = 0x0003FF00
cBit18_8  = 0x0007FF00
cBit19_8  = 0x000FFF00
cBit20_8  = 0x001FFF00
cBit21_8  = 0x003FFF00
cBit22_8  = 0x007FFF00
cBit23_8  = 0x00FFFF00
cBit24_8  = 0x01FFFF00
cBit25_8  = 0x03FFFF00
cBit26_8  = 0x07FFFF00
cBit27_8  = 0x0FFFFF00
cBit28_8  = 0x1FFFFF00
cBit29_8  = 0x3FFFFF00
cBit30_8  = 0x7FFFFF00
cBit31_8  = 0xFFFFFF00
cBit9_9   = 0x00000200
cBit10_9  = 0x00000600
cBit11_9  = 0x00000E00
cBit12_9  = 0x00001E00
cBit13_9  = 0x00003E00
cBit14_9  = 0x00007E00
cBit15_9  = 0x0000FE00
cBit16_9  = 0x0001FE00
cBit17_9  = 0x0003FE00
cBit18_9  = 0x0007FE00
cBit19_9  = 0x000FFE00
cBit20_9  = 0x001FFE00
cBit21_9  = 0x003FFE00
cBit22_9  = 0x007FFE00
cBit23_9  = 0x00FFFE00
cBit24_9  = 0x01FFFE00
cBit25_9  = 0x03FFFE00
cBit26_9  = 0x07FFFE00
cBit27_9  = 0x0FFFFE00
cBit28_9  = 0x1FFFFE00
cBit29_9  = 0x3FFFFE00
cBit30_9  = 0x7FFFFE00
cBit31_9  = 0xFFFFFE00
cBit10_10 = 0x00000400
cBit11_10 = 0x00000C00
cBit12_10 = 0x00001C00
cBit13_10 = 0x00003C00
cBit14_10 = 0x00007C00
cBit15_10 = 0x0000FC00
cBit16_10 = 0x0001FC00
cBit17_10 = 0x0003FC00
cBit18_10 = 0x0007FC00
cBit19_10 = 0x000FFC00
cBit20_10 = 0x001FFC00
cBit21_10 = 0x003FFC00
cBit22_10 = 0x007FFC00
cBit23_10 = 0x00FFFC00
cBit24_10 = 0x01FFFC00
cBit25_10 = 0x03FFFC00
cBit26_10 = 0x07FFFC00
cBit27_10 = 0x0FFFFC00
cBit28_10 = 0x1FFFFC00
cBit29_10 = 0x3FFFFC00
cBit30_10 = 0x7FFFFC00
cBit31_10 = 0xFFFFFC00
cBit11_11 = 0x00000800
cBit12_11 = 0x00001800
cBit13_11 = 0x00003800
cBit14_11 = 0x00007800
cBit15_11 = 0x0000F800
cBit16_11 = 0x0001F800
cBit17_11 = 0x0003F800
cBit18_11 = 0x0007F800
cBit19_11 = 0x000FF800
cBit20_11 = 0x001FF800
cBit21_11 = 0x003FF800
cBit22_11 = 0x007FF800
cBit23_11 = 0x00FFF800
cBit24_11 = 0x01FFF800
cBit25_11 = 0x03FFF800
cBit26_11 = 0x07FFF800
cBit27_11 = 0x0FFFF800
cBit28_11 = 0x1FFFF800
cBit29_11 = 0x3FFFF800
cBit30_11 = 0x7FFFF800
cBit31_11 = 0xFFFFF800
cBit12_12 = 0x00001000
cBit13_12 = 0x00003000
cBit14_12 = 0x00007000
cBit15_12 = 0x0000F000
cBit16_12 = 0x0001F000
cBit17_12 = 0x0003F000
cBit18_12 = 0x0007F000
cBit19_12 = 0x000FF000
cBit20_12 = 0x001FF000
cBit21_12 = 0x003FF000
cBit22_12 = 0x007FF000
cBit23_12 = 0x00FFF000
cBit24_12 = 0x01FFF000
cBit25_12 = 0x03FFF000
cBit26_12 = 0x07FFF000
cBit27_12 = 0x0FFFF000
cBit28_12 = 0x1FFFF000
cBit29_12 = 0x3FFFF000
cBit30_12 = 0x7FFFF000
cBit31_12 = 0xFFFFF000
cBit13_13 = 0x00002000
cBit14_13 = 0x00006000
cBit15_13 = 0x0000E000
cBit16_13 = 0x0001E000
cBit17_13 = 0x0003E000
cBit18_13 = 0x0007E000
cBit19_13 = 0x000FE000
cBit20_13 = 0x001FE000
cBit21_13 = 0x003FE000
cBit22_13 = 0x007FE000
cBit23_13 = 0x00FFE000
cBit24_13 = 0x01FFE000
cBit25_13 = 0x03FFE000
cBit26_13 = 0x07FFE000
cBit27_13 = 0x0FFFE000
cBit28_13 = 0x1FFFE000
cBit29_13 = 0x3FFFE000
cBit30_13 = 0x7FFFE000
cBit31_13 = 0xFFFFE000
cBit14_14 = 0x00004000
cBit15_14 = 0x0000C000
cBit16_14 = 0x0001C000
cBit17_14 = 0x0003C000
cBit18_14 = 0x0007C000
cBit19_14 = 0x000FC000
cBit20_14 = 0x001FC000
cBit21_14 = 0x003FC000
cBit22_14 = 0x007FC000
cBit23_14 = 0x00FFC000
cBit24_14 = 0x01FFC000
cBit25_14 = 0x03FFC000
cBit26_14 = 0x07FFC000
cBit27_14 = 0x0FFFC000
cBit28_14 = 0x1FFFC000
cBit29_14 = 0x3FFFC000
cBit30_14 = 0x7FFFC000
cBit31_14 = 0xFFFFC000
cBit15_15 = 0x00008000
cBit16_15 = 0x00018000
cBit17_15 = 0x00038000
cBit18_15 = 0x00078000
cBit19_15 = 0x000F8000
cBit20_15 = 0x001F8000
cBit21_15 = 0x003F8000
cBit22_15 = 0x007F8000
cBit23_15 = 0x00FF8000
cBit24_15 = 0x01FF8000
cBit25_15 = 0x03FF8000
cBit26_15 = 0x07FF8000
cBit27_15 = 0x0FFF8000
cBit28_15 = 0x1FFF8000
cBit29_15 = 0x3FFF8000
cBit30_15 = 0x7FFF8000
cBit31_15 = 0xFFFF8000
cBit16_16 = 0x00010000
cBit17_16 = 0x00030000
cBit18_16 = 0x00070000
cBit19_16 = 0x000F0000
cBit20_16 = 0x001F0000
cBit21_16 = 0x003F0000
cBit22_16 = 0x007F0000
cBit23_16 = 0x00FF0000
cBit24_16 = 0x01FF0000
cBit25_16 = 0x03FF0000
cBit26_16 = 0x07FF0000
cBit27_16 = 0x0FFF0000
cBit28_16 = 0x1FFF0000
cBit29_16 = 0x3FFF0000
cBit30_16 = 0x7FFF0000
cBit31_16 = 0xFFFF0000
cBit17_17 = 0x00020000
cBit18_17 = 0x00060000
cBit19_17 = 0x000E0000
cBit20_17 = 0x001E0000
cBit21_17 = 0x003E0000
cBit22_17 = 0x007E0000
cBit23_17 = 0x00FE0000
cBit24_17 = 0x01FE0000
cBit25_17 = 0x03FE0000
cBit26_17 = 0x07FE0000
cBit27_17 = 0x0FFE0000
cBit28_17 = 0x1FFE0000
cBit29_17 = 0x3FFE0000
cBit30_17 = 0x7FFE0000
cBit31_17 = 0xFFFE0000
cBit18_18 = 0x00040000
cBit19_18 = 0x000C0000
cBit20_18 = 0x001C0000
cBit21_18 = 0x003C0000
cBit22_18 = 0x007C0000
cBit23_18 = 0x00FC0000
cBit24_18 = 0x01FC0000
cBit25_18 = 0x03FC0000
cBit26_18 = 0x07FC0000
cBit27_18 = 0x0FFC0000
cBit28_18 = 0x1FFC0000
cBit29_18 = 0x3FFC0000
cBit30_18 = 0x7FFC0000
cBit31_18 = 0xFFFC0000
cBit19_19 = 0x00080000
cBit20_19 = 0x00180000
cBit21_19 = 0x00380000
cBit22_19 = 0x00780000
cBit23_19 = 0x00F80000
cBit24_19 = 0x01F80000
cBit25_19 = 0x03F80000
cBit26_19 = 0x07F80000
cBit27_19 = 0x0FF80000
cBit28_19 = 0x1FF80000
cBit29_19 = 0x3FF80000
cBit30_19 = 0x7FF80000
cBit31_19 = 0xFFF80000
cBit20_20 = 0x00100000
cBit21_20 = 0x00300000
cBit22_20 = 0x00700000
cBit23_20 = 0x00F00000
cBit24_20 = 0x01F00000
cBit25_20 = 0x03F00000
cBit26_20 = 0x07F00000
cBit27_20 = 0x0FF00000
cBit28_20 = 0x1FF00000
cBit29_20 = 0x3FF00000
cBit30_20 = 0x7FF00000
cBit31_20 = 0xFFF00000
cBit21_21 = 0x00200000
cBit22_21 = 0x00600000
cBit23_21 = 0x00E00000
cBit24_21 = 0x01E00000
cBit25_21 = 0x03E00000
cBit26_21 = 0x07E00000
cBit27_21 = 0x0FE00000
cBit28_21 = 0x1FE00000
cBit29_21 = 0x3FE00000
cBit30_21 = 0x7FE00000
cBit31_21 = 0xFFE00000
cBit22_22 = 0x00400000
cBit23_22 = 0x00C00000
cBit24_22 = 0x01C00000
cBit25_22 = 0x03C00000
cBit26_22 = 0x07C00000
cBit27_22 = 0x0FC00000
cBit28_22 = 0x1FC00000
cBit29_22 = 0x3FC00000
cBit30_22 = 0x7FC00000
cBit31_22 = 0xFFC00000
cBit23_23 = 0x00800000
cBit24_23 = 0x01800000
cBit25_23 = 0x03800000
cBit26_23 = 0x07800000
cBit27_23 = 0x0F800000
cBit28_23 = 0x1F800000
cBit29_23 = 0x3F800000
cBit30_23 = 0x7F800000
cBit31_23 = 0xFF800000
cBit24_24 = 0x01000000
cBit25_24 = 0x03000000
cBit26_24 = 0x07000000
cBit27_24 = 0x0F000000
cBit28_24 = 0x1F000000
cBit29_24 = 0x3F000000
cBit30_24 = 0x7F000000
cBit31_24 = 0xFF000000
cBit25_25 = 0x02000000
cBit26_25 = 0x06000000
cBit27_25 = 0x0E000000
cBit28_25 = 0x1E000000
cBit29_25 = 0x3E000000
cBit30_25 = 0x7E000000
cBit31_25 = 0xFE000000
cBit26_26 = 0x04000000
cBit27_26 = 0x0C000000
cBit28_26 = 0x1C000000
cBit29_26 = 0x3C000000
cBit30_26 = 0x7C000000
cBit31_26 = 0xFC000000
cBit27_27 = 0x08000000
cBit28_27 = 0x18000000
cBit29_27 = 0x38000000
cBit30_27 = 0x78000000
cBit31_27 = 0xF8000000
cBit28_28 = 0x10000000
cBit29_28 = 0x30000000
cBit30_28 = 0x70000000
cBit31_28 = 0xF0000000
cBit29_29 = 0x20000000
cBit30_29 = 0x60000000
cBit31_29 = 0xE0000000
cBit30_30 = 0x40000000
cBit31_30 = 0xC0000000
cBit31_31 = 0x80000000

if __name__ == '__main__':
    reg = AtRegister(0)
    reg.test()

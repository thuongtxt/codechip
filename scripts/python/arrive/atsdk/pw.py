from module import AtModule
from channel import AtChannel
from python.arrive.atsdk.cli import AtCliRunnable


class AtPwFactory(object):
    @staticmethod
    def createPw(amodule, pwType, pwId):
        return AtPw(amodule, pwType, pwId)

class AtModulePw(AtModule):
    def __init__(self, device):
        AtModule.__init__(self, device)
        self.transmitSubPortVlans = None
        self.expectSubPortVlans = None
        self.pwFactory = AtPwFactory()

    @staticmethod
    def _maxPws():
        return 1000

    def subportVlansRecache(self):
        cliResult = self.runCli("show ciena module pw subport vlan")
        
        self.transmitSubPortVlans = []
        self.expectSubPortVlans = []
        for row_i in range(0, cliResult.numRows()):
            self.transmitSubPortVlans.append(cliResult.cellContent(row_i, "Transmit"))
            self.expectSubPortVlans.append(cliResult.cellContent(row_i, "Expect"))

    def setTransmitSubportVlan(self, index, vlanString):
        return self.runCli("ciena module pw subport vlan transmit %s %s" % (index + 1, vlanString))

    def setExpectSubportVlan(self, index, vlanString):
        return self.runCli("ciena module pw subport vlan expect %s %s" % (index + 1, vlanString))
    
    def createCep(self, idString, mode = "basic"):
        return self.runCli("pw create cep %s %s" % (idString, mode))
    
    def deletePws(self, idString):
        return self.runCli("pw delete %s" % idString)

    def pws(self):
        self.silent()

        pws = list()
        cli = "show pw 1-%d" % self._maxPws()
        result = self.runCli(cli)
        for rowId in range(result.numRows()):
            pwCliId = result.cellContent(rowId, "PW")
            tokens = pwCliId.split(".")
            pw = self.pwFactory.createPw(self, pwType=tokens[0], pwId=int(tokens[1]) - 1)
            pws.append(pw)

        self.silentRestore()

        return pws

class AtPwJitterBuffer(AtCliRunnable):
    def __init__(self, pw):
        """
        :type pw: AtPw
        """
        self.pw = pw

    def cliId(self):
        return self.pw.cliId()

    def _getCliAttribute(self, attributeName, silent=True):
        if silent:
            self.silent()

        result = self.runCli("show pw jitterbuffer %s" % self.cliId())

        if silent:
            self.silentRestore()

        return result.cellContent(0, attributeName)

    @property
    def jitterBufferSize(self):
        return self._getCliAttribute("jitter(us)")

    @jitterBufferSize.setter
    def jitterBufferSize(self, value):
        self.runCli("pw jitterbuffer %s %d" % (self.cliId(), value))

    @property
    def jitterBufferSizeInPackets(self):
        return self._getCliAttribute("jitter(pkt)")

    @jitterBufferSizeInPackets.setter
    def jitterBufferSizeInPackets(self, value):
        self.runCli("pw jitterbuffer packet %s %d" % (self.cliId(), value))

    @property
    def jitterBufferDelayInPackets(self):
        return self._getCliAttribute("jitterDelay(pkt)")

    @jitterBufferDelayInPackets.setter
    def jitterBufferDelayInPackets(self, value):
        self.runCli("pw jitterdelay packet %s %d" % (self.cliId(), value))

class AtPw(AtChannel):
    def __init__(self, amodule, pwType, channelId):
        AtChannel.__init__(self, amodule, channelId)
        self.pwType = pwType
        self._circuit = None
        self._jitterBuffer = None
    
    def cliType(self):
        return self.pwType

    def _sdhModule(self):
        return self.device.sdhModule()

    def _circuitFromCliId(self, circuitCliId):
        circuit = self._sdhModule().getChannel(circuitCliId)
        if circuit is not None:
            return circuit

        circuit = self._pdhModule().getChannel(circuitCliId)
        if circuit is not None:
            return circuit

        self.notImplemented()

    def _loadCircuit(self):
        self.silent()
        cli = "show pw %d" % self.cliId()
        result = self.runCli(cli)
        circuitCliId = result.cellContent(0, "circuit")
        circuit = self._circuitFromCliId(circuitCliId)
        self.silentRestore()
        return circuit

    def _getCliAttribute(self, attributeName, silent=True):
        if silent:
            self.silent()

        result = self.runCli("show pw %s" % self.cliId())

        if silent:
            self.silentRestore()

        return result.cellContent(0, attributeName)

    @property
    def jitterBuffer(self):
        """
        :rtype: AtPwJitterBuffer
        """
        if self._jitterBuffer is None:
            self._jitterBuffer = AtPwJitterBuffer(self)
        return self._jitterBuffer

    @property
    def circuit(self):
        """
        :rtype: AtChannel
        """
        if self._circuit is None:
            self._circuit = self._loadCircuit()
        return self._circuit

    @circuit.setter
    def circuit(self, circuit):
        """
        :type circuit: AtChannel
        """
        self.runCli("pw circuit bind %s %s.%s" % (self.cliId(), circuit.cliType(), circuit.cliId()))

    @property
    def payloadSize(self):
        return int(self._getCliAttribute("PayloadSize"))

    @payloadSize.setter
    def payloadSize(self, value):
        self.runCli("pw payloadsize %s %d" % (self.cliId(), value))

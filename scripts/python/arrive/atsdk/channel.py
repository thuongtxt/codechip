"""
Created on Apr 14, 2017

@author: namnng
"""
from cli import AtCliRunnable

class AtChannel(AtCliRunnable):
    LOOPBACK_MODE_RELEASE = "release"
    LOOPBACK_MODE_LOCAL   = "local"
    LOOPBACK_MODE_REMOTE  = "remote"
    LOOPBACK_MODE_DUAL    = "dual"
    
    def __init__(self, sdkModule, channelId):
        AtCliRunnable.__init__(self)
        self.module = sdkModule
        self.channelId = channelId
        self._boundPw = None

    @property
    def device(self):
        """
        :rtype: python.arrive.atsdk.device.AtDevice
        """
        return self.module.device

    @classmethod
    def alarmTypes(cls):
        return []

    def channelHwId(self, moduleNames = None):
        """
        To get HW ID at context of specific input modules
        
        @param moduleNames Array of module names. If only module needs to be queried, just input a string
        @return 
        - If only one module is input, HwIds is an array of (slice, sts) tuples
        - If more than one modules are input, dict of {moduleName:HwIds} are returned. 
          HwIds is an array of (slice, sts) tuples
        """
        return None

    @classmethod
    def _channelHwIdWithCliBuider(cls, cliBuilder, moduleNames = None):
        assert (type(moduleNames) is list)
        
        if moduleNames is None:
            moduleNames = ""
        
        if type(moduleNames) is str:
            moduleNames = [moduleNames]
        
        cli = cliBuilder(",".join(moduleNames))
        result = cls.runCli(cli)
        assert(result.numRows() == 1)
        
        def hwIdFromString(idString):
            idTerms = idString.split("|")
            hwIds_ = []
            for idTerm in idTerms:
                hwIdTerms = idTerm.split(",")
                hwId = int(hwIdTerms[0]), int(hwIdTerms[1])
                hwIds_.append(hwId)
                
            return hwIds_
        
        hwIds = {}
        if moduleNames is not None:
            for moduleName in moduleNames:
                hwIdString = result.cellContent(0, moduleName)
                if hwIdString:
                    hwIds[moduleName] = hwIdFromString(hwIdString)
            return hwIds
        
        for column in range(1, result.numColumns()):
            moduleName = result.columnName(column)
            hwIdString = result.cellContent(0, column)
            hwIds[moduleName] = hwIdFromString(hwIdString)
        
        return hwIds
    
    def cliId(self):
        return "%d" % (self.channelId + 1)
    
    def cliType(self):
        return None
    
    def cliToShowPwBindingInfo(self):
        return None
    
    def _createPw(self, pwFullIdString):
        tokens = pwFullIdString.split(".")
        pwTypeString  = tokens[0]
        pwCliIdString = tokens[1]
        pwModule = self.module.device.pwModule()
        pwFactory = pwModule.pwFactory
        return pwFactory.createPw(pwModule, pwTypeString, int(pwCliIdString) - 1)
    
    def _loadBoundPw(self):
        result = self.runCli(self.cliToShowPwBindingInfo())
        pwCellContent = result.cellContent(0, "Bound")
        if pwCellContent == "None":
            return None
        return self._createPw(pwCellContent)
    
    def reloadBoundPw(self):
        self._boundPw = self._loadBoundPw()
        return self._boundPw
    
    def boundPw(self):
        if self._boundPw is None:
            self._boundPw = self._loadBoundPw()
        return self._boundPw

    def currentAlarm(self):
        return []
    
    def noAlarm(self):
        return len(self.currentAlarm()) == 0

    @staticmethod
    def boolValFromString(string):
        string = string.lower()
        if string in ["set", "raise", "yes", "en"]:
            return True
        if string in ["clear", "no", "dis"]:
            return False
        
        assert()

    def _setLoopback(self, value):
        self.notImplemented()

    def _getLoopback(self):
        self.notImplemented()
        return None

    @property
    def loopback(self):
        return self._getLoopback()

    @loopback.setter
    def loopback(self, value):
        self._setLoopback(value)

    @staticmethod
    def _getRateInBps():
        return None

    @property
    def rateInBps(self):
        return self._getRateInBps()

    def _enable(self, enabled):
        self.notImplemented()

    def _isEnabled(self):
        self.notImplemented()
        return False

    @property
    def enabled(self):
        return self._isEnabled()

    @enabled.setter
    def enabled(self, value):
        self._enable(value)

    @property
    def sourceChannel(self):
        return None

    @property
    def destinationChannels(self):
        return None

    @classmethod
    def parseCliId(cls, cliId):
        """
        :return Tuple of (channelType, [channelIds])
        :rtype: tuple(str, list)
        """
        tokens = cliId.split(".")
        ids = [int(idString, 10) - 1 for idString in tokens[1:]]
        channelType = tokens[0]
        return channelType, ids
"""
Created on Apr 14, 2017

@author: namnng
"""
from cli import AtCliRunnable
from sdh import AtModuleSdh
from pdh import AtModulePdh
from pw import AtModulePw
from prbs import AtModulePrbs
from aps import AtModuleAps
import python.arrive.atsdk.AtRegister as AtRegister
import atsdk

class AtModuleFactory(object):
    @staticmethod
    def createSdhModule(device):
        return AtModuleSdh(device)
    
    @staticmethod
    def createPdhModule(device):
        return AtModulePdh(device)

    @staticmethod
    def createPwModule(device):
        return AtModulePw(device)
    
    @staticmethod
    def createPrbsModule(device):
        return AtModulePrbs(device)
    
    @staticmethod
    def createApsModule(device):
        return AtModuleAps(device)
    
class AtDevice(AtCliRunnable):
    def __init__(self, productCode):
        AtCliRunnable.__init__(self)
        self.moduleFactory = AtModuleFactory()
        self.simulated = False
        self.productCode = productCode
    
    @classmethod
    def _productCodeFamilyMask(cls):
        from AtRegister import cBit31_28
        return cBit31_28
    
    @classmethod
    def _productCodeFamily(cls, productCode):
        return (productCode & cls._productCodeFamilyMask()) >> 28
    
    @classmethod
    def _isSimulatedProductCode(cls, productCode):
        if cls._productCodeFamily(productCode) == 0xF:
            return True
        return False
    
    @classmethod
    def currentDevice(cls):
        cliResult = cls.runCli("show driver")
        actualProductCode = int(cliResult.cellContent(0, "Product code"), 16)
        productCode = (actualProductCode & (~cls._productCodeFamilyMask())) | (0x6 << 28)
        
        device = cls.deviceCreate(productCode)
        if cls._isSimulatedProductCode(actualProductCode):
            device.simulated = True
        return device 
    
    def sdhModule(self):
        """
        :rtype: AtModuleSdh
        """
        return self.moduleFactory.createSdhModule(self)
    
    def pdhModule(self):
        """
        :rtype: AtModulePdh
        """
        return self.moduleFactory.createPdhModule(self)
    
    def pwModule(self):
        """
        :rtype: AtModulePw
        """
        return self.moduleFactory.createPwModule(self)
    
    def prbsModule(self):
        """
        :rtype: AtModulePrbs
        """
        return self.moduleFactory.createPrbsModule(self)
    
    def apsModule(self):
        """
        :rtype: AtModuleAps
        """
        return self.moduleFactory.createApsModule(self)
    
    @classmethod
    def deviceCreate(cls, productCode):
        return AtDevice(productCode)

class AtDeviceListener(object):
    _deviceListeners = []
    _deviceListened = False
    
    def didRead(self, address, value, coreId):
        pass
    
    def didWrite(self, address, value, coreId):
        pass
    
    def didLongRead(self, address, value, coreId):
        pass
    
    def didLongWrite(self, address, value, coreId):
        pass
    
    @classmethod
    def addListener(cls, aListener):
        def int2Uint(value):
            return value & AtRegister.cBit31_0

        if aListener not in cls._deviceListeners:
            cls._deviceListeners.append(aListener)
            
        if not cls._deviceListened:
            def didReadNotify(address, value, coreId):
                for listener_ in AtDeviceListener._deviceListeners:
                    listener_.didRead(address, int2Uint(value), coreId)
                
            def didWriteNotify(address, value, coreId):
                for listener_ in AtDeviceListener._deviceListeners:
                    listener_.didWrite(address, int2Uint(value), coreId)
                    
            def didLongReadNotify(address, value, coreId):
                uintValues = [int2Uint(val) for val in value]
                for listener_ in AtDeviceListener._deviceListeners:
                    listener_.didLongRead(address, uintValues, coreId)
                
            def didLongWriteNotify(address, value, coreId):
                uintValues = [int2Uint(val) for val in value]
                for listener_ in AtDeviceListener._deviceListeners:
                    listener_.didLongWrite(address, uintValues, coreId)
            
            atsdk.DeviceDidReadCallbackSet(didReadNotify)
            atsdk.DeviceDidWriteCallbackSet(didWriteNotify)
            atsdk.DeviceDidLongReadCallbackSet(didLongReadNotify)
            atsdk.DeviceDidLongWriteCallbackSet(didLongWriteNotify)
            cls._deviceListened = True

    @classmethod
    def removeListener(cls, aListener):
        if aListener in cls._deviceListeners:
            cls._deviceListeners.remove(aListener)
            
    def test(self):
        class _Listener(AtDeviceListener):
            def reset(self):
                self.readAddress      = None
                self.readValue        = None
                self.longReadAddress  = None
                self.longReadValue    = None
                
                self.writeAddress     = None
                self.writeValue       = None
                self.longWriteAddress = None
                self.longWriteValue   = None
                
                self.accessCount = 0
            
            def __init__(self):
                self.reset()
            
            def didRead(self, address, value, coreId):
                self.readAddress = address
                self.readValue = value
                self.accessCount = self.accessCount + 1
            
            def didWrite(self, address, value, coreId):
                self.writeAddress = address
                self.writeValue = value
                self.accessCount = self.accessCount + 1
                
            def didLongRead(self, address, value, coreId):
                self.longReadAddress = address
                self.longReadValue = value
                self.accessCount = self.accessCount + 1
            
            def didLongWrite(self, address, value, coreId):
                self.longWriteAddress = address
                self.longWriteValue = value
                self.accessCount = self.accessCount + 1
        
        listener = _Listener()
        AtDeviceListener.addListener(listener)
        AtDeviceListener.addListener(listener)
        
        stringValue = "0x11111111.22222222.33333333.44444444"
        longValues  = [int(value, 16) for value in stringValue.split(".")]
        longValues.reverse()
        listener.reset()
        atsdk.runCli("lwr 1 %s" % stringValue)
        assert listener.longWriteAddress == 0x1
        assert listener.longWriteValue == longValues
        assert listener.accessCount  == 1
        
        listener.reset()
        atsdk.runCli("lrd 1")
        assert listener.longReadAddress == 0x1
        assert listener.longReadValue == longValues
        assert listener.accessCount  == 1
        
        listener.reset()
        AtDeviceListener.removeListener(listener)
        AtDeviceListener.removeListener(listener)
        atsdk.runCli("rd 12")
        atsdk.runCli("wr 0 1")
        atsdk.runCli("lwr 1 %s" % stringValue)
        atsdk.runCli("lrd 1")
        assert listener.accessCount == 0
        
if __name__ == '__main__':
    listener = AtDeviceListener()
    listener.test()
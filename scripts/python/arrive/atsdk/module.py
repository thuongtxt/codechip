"""
Created on Apr 14, 2017

@author: namnng
"""
from cli import AtCliRunnable

class AtModule(AtCliRunnable):
    def __init__(self, device):
        """
        :type device: python.arrive.atsdk.device.AtDevice
        """
        AtCliRunnable.__init__(self)
        self.device = device

    def getChannel(self, cliChannelId):
        """
        :rtype: python.arrive.atsdk.channel.AtChannel
        """
        self.notImplemented()
        return None
"""
Created on May 1, 2017

@author: namnng
"""

class eAtBerRate(object):
    RATE_1E2  = "1e-2"
    RATE_1E3  = "1e-3"
    RATE_1E4  = "1e-4"
    RATE_1E5  = "1e-5"
    RATE_1E6  = "1e-6"
    RATE_1E7  = "1e-7"
    RATE_1E8  = "1e-8"
    RATE_1E9  = "1e-9"
    RATE_1E10 = "1e-10"

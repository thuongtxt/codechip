"""
Created on Apr 14, 2017

@author: namnng
"""
from module import AtModule
from channel import AtChannel

class AtPdhChannelFactory(object):
    @staticmethod
    def createVcDe3(module_, vc3):
        de3 = AtPdhDe3(module_, parent = None, channelId = 0)
        de3.vc = vc3
        return de3
    
    @staticmethod
    def createVcDe1(module_, vc1x):
        de1 = AtPdhDe1(module_, parent = None, channelId = 0)
        de1.vc = vc1x
        return de1
    
    @staticmethod
    def createDe2De1(module_, parent, de1Id):
        return AtPdhDe1(module_, parent, channelId = de1Id)
    
    @staticmethod
    def createDe2(module_, parent, de2Id):
        return AtPdhDe2(module_, parent, de2Id)
    
    @staticmethod
    def createNxDs0(module_, parent, timeslotIdString):
        return AtPdhNxDs0(module_, parent, timeslotIdString)

class AtModulePdh(AtModule):
    def __init__(self, device):
        AtModule.__init__(self, device)
        self.channelFactory = AtPdhChannelFactory()

    def _getLiuDe1(self, de1Id):
        self.notImplemented()

    def _getLiuDe3(self, de1Id):
        self.notImplemented()

    def _sdhModule(self):
        return self.device.sdhModule()

    def _getMapDe1(self, cliId):
        sdhModule = self._sdhModule()

        # See if it is mapped inside VC1x
        vc1xCliId = "vc1x.%s" % cliId
        vc1x = sdhModule.getChannel(vc1xCliId)
        if vc1x is not None:
            if vc1x.mapType == vc1x.MAP_DE1:
                mapChannel = vc1x.mapChannel()
                assert mapChannel is not None
                return mapChannel

        # It must be in DE3
        tokens = cliId.split(".")
        vc3CliId = "vc3.%s" % (".".join(tokens[:3]))
        vc3 = sdhModule.getChannel(vc3CliId)
        de3 = vc3.mapChannel()
        de1 = de3.subChannels[int(tokens[3]) - 1][int(tokens[4]) - 1]
        assert de1 is not None
        return de1

    def _getMapDe3(self, cliId):
        tokens = cliId.split(".")
        vc3CliId = "vc3.%s" % (".".join(tokens[:3]))
        vc3 = self._sdhModule().getChannel(vc3CliId)
        de3 = vc3.mapChannel()
        assert de3 is not None
        return de3

    def _getDe1(self, cliId):
        """
        :rtype: AtPdhDe1
        """
        tokens = cliId.split(".")

        if len(tokens) == 1:
            return self._getLiuDe1(int(tokens[0]) - 1)
        return self._getMapDe1(cliId)

    def _getDe3(self, cliId):
        tokens = cliId.split(".")

        if len(tokens) == 1:
            return self._getLiuDe1(int(tokens[0]) - 1)
        return self._getMapDe3(cliId)

    def _getNxDs0(self, cliChannelId):
        tokens = cliChannelId.split(".")
        de1CliId = ".".join(tokens[1:len(tokens) - 1])
        nxds0CliId = tokens[len(tokens) - 1]
        de1 = self._getDe1(de1CliId)
        return de1.getNxDs0(nxds0CliId)

    def getChannel(self, cliChannelId):
        tokens = cliChannelId.split(".")
        channelType = tokens[0]
        channelId = tokens[1:]

        if channelType == "de1":
            return self._getDe1(channelId)
        if channelType == "de3":
            return self._getDe3(channelId)

        if channelType == "nxds0":
            return self._getNxDs0(channelId)

        return None

class AtPdhChannel(AtChannel):
    def __init__(self, sdkModule, parent, channelId):
        super(AtPdhChannel, self).__init__(sdkModule, channelId)
        self.vc = None
        self.parent = parent
        self._subChannels_ = None
    
    def channelHwId(self, moduleNames = None):
        def cliBuilder(_moduleNames):
            return "debug show pdh channel hardware id %s.%s %s" % (self.cliType(), self.cliId(), _moduleNames)
        return self._channelHwIdWithCliBuider(cliBuilder, moduleNames)
    
    def _loadSubChannels(self):
        return None
    
    def reloadSubChannels(self):
        self._subChannels_ = self._loadSubChannels()
        return self._subChannels_

    @property
    def subChannels(self):
        if self._subChannels_ is None:
            self._subChannels_ = self._loadSubChannels()
        return self._subChannels_

    def _frameModeCliTitle(self):
        return None

    @property
    def frameType(self):
        cli = "show pdh %s %s" % (self.cliType(), self.cliId())
        result = self.runCli(cli)
        return result.cellContent(0, self._frameModeCliTitle())

    @frameType.setter
    def frameType(self, value):
        cli = "pdh %s framing %s %s" % (self.cliType(), self.cliId(), value)
        self.runCli(cli)

    def channelFactory(self):
        return self.module.channelFactory
    
    def cliId(self):
        if self.vc:
            return self.vc.cliId()
        
        if self.parent:
            return "%s.%d" % (self.parent.cliId(), self.channelId + 1)
        
        return "%s" % (self.channelId + 1)
    
    def cliToShowPwBindingInfo(self):
        return "show pdh %s %s" % (self.cliType(), self.cliId())

class AtPdhDe2(AtPdhChannel):
    def cliType(self):
        return "de2"
    
    def numDe1ForDe3FrameType(self, de3FrameType):
        de3 = self.parent
        
        if de3FrameType in [de3.FRAMING_TYPE_DS3_CBIT_28DS1, 
                            de3.FRAMING_TYPE_DS3_M13_28DS1,
                            de3.FRAMING_TYPE_E3_G751_16E1S]:
            return 4
        
        if de3FrameType in [de3.FRAMING_TYPE_DS3_CBIT_21E1,
                            de3.FRAMING_TYPE_DS3_M13_21E1]:
            return 3
        
        return 0
        
    def _loadSubChannels(self):
        subChannels = []
        de3FrameType = self.parent.frameType
        
        for de1Id in range(self.numDe1ForDe3FrameType(de3FrameType)):
            de1 = self.channelFactory().createDe2De1(self.module, self, de1Id)
            subChannels.append(de1)
        
        return subChannels

class AtPdhDe3(AtPdhChannel):
    FRAMING_TYPE_DS3_UNFRAMED = "ds3_unframed"
    FRAMING_TYPE_DS3_CBIT_UNCHANNELIZE = "ds3_cbit_unchannelize"
    FRAMING_TYPE_DS3_CBIT_28DS1 = "ds3_cbit_28ds1"
    FRAMING_TYPE_DS3_CBIT_21E1 = "ds3_cbit_21e1"
    FRAMING_TYPE_DS3_M13_28DS1 = "ds3_m13_28ds1"
    FRAMING_TYPE_DS3_M13_21E1 = "ds3_m13_21e1"
    FRAMING_TYPE_E3_UNFRAMED = "e3_unframed"
    FRAMING_TYPE_E3_G832 = "e3_g832"
    FRAMING_TYPE_E3_G751 = "e3_g751"
    FRAMING_TYPE_E3_G751_16E1S = "e3_g751_16e1s"

    @property
    def isE3(self):
        return self.frameType in [self.FRAMING_TYPE_E3_UNFRAMED,
                                  self.FRAMING_TYPE_E3_G832,
                                  self.FRAMING_TYPE_E3_G751,
                                  self.FRAMING_TYPE_E3_G751_16E1S]

    @property
    def isDs3(self):
        return False if self.isE3 else True

    def cliType(self):
        return "de3"
    
    def _frameModeCliTitle(self):
        return "Framingmode"
    
    def _numDe2ForFrameType(self, frameType):
        if frameType in [self.FRAMING_TYPE_DS3_CBIT_28DS1,
                         self.FRAMING_TYPE_DS3_CBIT_21E1,
                         self.FRAMING_TYPE_DS3_M13_28DS1,
                         self.FRAMING_TYPE_DS3_M13_21E1]:
            return 7
        
        if frameType == self.FRAMING_TYPE_E3_G751_16E1S:
            return 4
        
        return 0
    
    def _loadSubChannels(self):
        frameType = self.frameType
        subChannels = []
        
        for de2Id in range(self._numDe2ForFrameType(frameType)):
            subChannels.append(self.channelFactory().createDe2(self.module, self, de2Id))
            
        return subChannels

class AtPdhNxDs0(AtPdhChannel):
    def cliType(self):
        return "nxds0"
    
    def timeslots(self):
        return self.channelId
    
    def cliId(self):
        return "%s.%s" % (self.parent.cliId(), self.timeslots())
    
    def _loadSubChannels(self):
        return []
    
    def _loadBoundPw(self):
        cli = "show pdh de1 nxds0 %s" % self.parent.cliId()
        result = self.runCli(cli)
        for rowId in range(result.numRows()):
            nxds0IdString = result.cellContent(rowId, "NxDS0")
            if nxds0IdString == ("%s.%s" % (self.cliType(), self.cliId())):
                return self._createPw(result.cellContent(rowId, "Bound"))
            
        return None
    
class AtPdhDe1(AtPdhChannel):
    FRAMING_TYPE_DS1_UNFRAMED = "ds1_unframed"
    FRAMING_TYPE_DS1_SF = "ds1_sf"
    FRAMING_TYPE_DS1_ESF = "ds1_esf"
    FRAMING_TYPE_E1_UNFRAMED = "e1_unframed"
    FRAMING_TYPE_E1_BASIC = "e1_basic"
    FRAMING_TYPE_E1_CRC = "e1_crc"

    def __init__(self, sdkModule, parent, channelId):
        super(AtPdhDe1, self).__init__(sdkModule, parent, channelId)
        self._nxDs0Dict = dict()

    @property
    def isE1(self):
        return self.frameType in [self.FRAMING_TYPE_E1_UNFRAMED,
                                  self.FRAMING_TYPE_E1_BASIC,
                                  self.FRAMING_TYPE_E1_CRC]

    @property
    def isDs1(self):
        return False if self.isE1 else True

    def cliType(self):
        return "de1"

    def _frameModeCliTitle(self):
        return "FramingMode"
    
    def _frameTypeHasNxDs0(self, frameType):
        if frameType in [self.FRAMING_TYPE_DS1_UNFRAMED,
                         self.FRAMING_TYPE_E1_UNFRAMED]:
            return False
        return True
    
    def _loadSubChannels(self):
        frameType = self.frameType
        if not self._frameTypeHasNxDs0(frameType):
            return []
        
        cli = "show pdh de1 nxds0 %s" % self.cliId()
        result = self.runCli(cli)
        nxds0s = []
        self._nxDs0Dict = dict()
        factory = self.channelFactory()
        for rowId in range(result.numRows()):
            nxds0Cell = result.cellContent(rowId, "NxDS0")
            tokens = nxds0Cell.split(".")
            timeslots = tokens[len(tokens) - 1]
            nxds0 = factory.createNxDs0(self.module, self, timeslots)
            nxds0s.append(nxds0)
            self._nxDs0Dict[timeslots] = nxds0
            
        return nxds0s

    def getNxDs0(self, timeslots):
        if self._subChannels_ is None:
            self.reloadSubChannels()
        return self._nxDs0Dict[timeslots]

    def rateInBps(self):
        frameType = self.frameType
        if frameType in [self.FRAMING_TYPE_DS1_UNFRAMED, 
                         self.FRAMING_TYPE_DS1_SF, 
                         self.FRAMING_TYPE_DS1_ESF]:
            return 1544000
        
        if frameType in [self.FRAMING_TYPE_E1_UNFRAMED, 
                         self.FRAMING_TYPE_E1_BASIC, 
                         self.FRAMING_TYPE_E1_CRC]:
            return 2048000
        
        return None

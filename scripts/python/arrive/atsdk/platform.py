import atsdk

class AtHalListener(object):
    _halListeners = []
    _halListened = False
    
    def didRead(self, address, value):
        pass
    
    def didWrite(self, address, value):
        pass
    
    @classmethod
    def addListener(cls, aListener):
        if aListener not in cls._halListeners:
            cls._halListeners.append(aListener)
            
        if not cls._halListened:
            def didReadNotify(address, value):
                for listener_ in AtHalListener._halListeners:
                    listener_.didRead(address, value)
                
            def didWriteNotify(address, value):
                for listener_ in AtHalListener._halListeners:
                    listener_.didWrite(address, value)
            
            atsdk.HalDidReadCallbackSet(didReadNotify)
            atsdk.HalDidWriteCallbackSet(didWriteNotify)
            cls._halListened = True

    @classmethod
    def removeListener(cls, aListener):
        if aListener in cls._halListeners:
            cls._halListeners.remove(aListener)

if __name__ == '__main__':
    class _Listener(AtHalListener):
        def didRead(self, address, value):
            print "(*) rd %x %x" % (address, value)
        
        def didWrite(self, address, value):
            print "(*) wr %x %x" % (address, value)
    
    listener = _Listener()
    AtHalListener.addListener(listener)
    AtHalListener.addListener(listener)
    
    atsdk.runCli("rd 12")
    atsdk.runCli("wr 0 1")
    
    print "Remove listener"
    AtHalListener.removeListener(listener)
    AtHalListener.removeListener(listener)
    atsdk.runCli("rd 12")
    atsdk.runCli("wr 0 1")

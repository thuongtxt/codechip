"""
Created on Apr 14, 2017

@author: namnng
"""
from module import AtModule
from channel import AtChannel
from pdh import AtPdhChannelFactory

class AtTti(object):
    MODE_1Byte  = "1byte"
    MODE_16Byte = "16bytes"
    MODE_64Byte = "64bytes"
    
    PADDING_NULL  = "null_padding"
    PADDING_SPACE = "space_padding"
    
    def __init__(self, mode, message, padding = PADDING_NULL):
        self.mode = mode
        self.message = message
        self.padding = padding

class AtSdhChannelFactory(object):
    @staticmethod
    def createAug1(module_, parent, aug1Id):
        return AtSdhAug1(module_, parent, aug1Id)
    
    @staticmethod
    def createAug4(module_, parent, aug4Id):
        return AtSdhAug4(module_, parent, aug4Id)
    
    @staticmethod
    def createAug16(module_, parent, aug16Id):
        return AtSdhAug16(module_, parent, aug16Id)

    @staticmethod
    def createAug64(module_, parent, aug64Id):
        return AtSdhAug64(module_, parent, aug64Id)

    @staticmethod
    def createVc1x(module_, parent, vc1xId):
        return AtSdhVc1x(module_, parent, vc1xId)

    @staticmethod
    def createVc11(module_, parent, vc1xId):
        return AtSdhVc11(module_, parent, vc1xId)

    @staticmethod
    def createVc12(module_, parent, vc1xId):
        return AtSdhVc12(module_, parent, vc1xId)

    @staticmethod
    def createVc3(module_, parent, vc3Id):
        return AtSdhVc3(module_, parent, vc3Id)
    
    @staticmethod
    def createVc4(module_, parent, vc4Id):
        return AtSdhVc4(module_, parent, vc4Id)
    
    @staticmethod
    def createVc4_4c(module_, parent, vc4_4cId):
        return AtSdhVc4_4c(module_, parent, vc4_4cId)
    
    @staticmethod
    def createVc4_16c(module_, parent, vc4_16cId):
        return AtSdhVc4_16c(module_, parent, vc4_16cId)
    
    @staticmethod
    def createVc4_64c(module_, parent, vc4_64cId):
        return AtSdhVc4_64c(module_, parent, vc4_64cId)
    
    @staticmethod
    def createAu3(module_, parent, au3Id):
        return AtSdhAu3(module_, parent, au3Id)
    
    @staticmethod
    def createAu4(module_, parent, au4Id):
        return AtSdhAu4(module_, parent, au4Id)
    
    @staticmethod
    def createAu4_4c(module_, parent, au4_4cId):
        return AtSdhAu4_4c(module_, parent, au4_4cId)
    
    @staticmethod
    def createAu4_16c(module_, parent, au4_16cId):
        return AtSdhAu4_16c(module_, parent, au4_16cId)
    
    @staticmethod
    def createAu4_64c(module_, parent, au4_64cId):
        return AtSdhAu4_64c(module_, parent, au4_64cId)
    
    @staticmethod
    def createTug2(module_, parent, tug2Id):
        return AtSdhTug2(module_, parent, tug2Id)
    
    @staticmethod
    def createTug3(module_, parent, tug3Id):
        return AtSdhTug3(module_, parent, tug3Id)
    
    @staticmethod
    def createTu3(module_, parent, tu3Id):
        return AtSdhTu3(module_, parent, tu3Id)
    
    @staticmethod
    def createTu11(module_, parent, tu1xId):
        return AtSdhTu11(module_, parent, tu1xId)

    @staticmethod
    def createTu12(module_, parent, tu1xId):
        return AtSdhTu12(module_, parent, tu1xId)
    
    @staticmethod
    def createLine(module_, lineId):
        return AtSdhLine(module_, None, lineId)

class AtModuleSdh(AtModule):
    def __init__(self, device):
        AtModule.__init__(self, device)
        self.channelFactory = AtSdhChannelFactory()
        self.pdhChannelFactory = AtPdhChannelFactory()
    
    def lines(self):
        self.silent()
        
        lines = []
        cli = "show sdh line 1-256"
        result = self.runCli(cli)
        for rowId in range(result.numRows()):
            lineId = int(result.cellContent(rowId, "LineId"), 10) - 1
            line = self.channelFactory.createLine(self, lineId)
            lines.append(line)
            
        self.silentRestore()
            
        return lines
    
    def getLine(self, lineId):
        lines = self.lines()
        for line in lines:
            if line.channelId == lineId:
                return line
            
        return None
    
    def getChannel(self, cliChannelId):
        """
        :rtype: AtSdhChannel
        """
        _, ids = AtChannel.parseCliId(cliChannelId)
        line = self.getLine(ids[0])
        return line.getChannel(cliChannelId)
    
class AtSdhChannel(AtChannel):
    def __init__(self, sdkModule, parent, channelId):
        AtChannel.__init__(self, sdkModule, channelId)
        self.parent = parent
        self._subChannels_ = None

    def channelHwId(self, moduleNames = None):
        def cliBuilder(moduleNames_):
            return "debug show sdh channel hardware id %s.%s %s" % (self.cliType(), self.cliId(), moduleNames_)
        return self._channelHwIdWithCliBuider(cliBuilder, moduleNames)

    @property
    def subChannels(self):
        if self._subChannels_ is None:
            self._subChannels_ = self._loadSubChannels()
        return self._subChannels_

    def _loadSubChannels(self):
        self.notImplemented()
        
    def reloadSubChannels(self):
        self._subChannels_ = self._loadSubChannels()
        return self._subChannels_
        
    def channelFactory(self):
        """
        :rtype: AtSdhChannelFactory
        """
        return self.module.channelFactory
    
    def pdhChannelFactory(self):
        return self.module.pdhChannelFactory

    @property
    def mapType(self):
        self.silent()
        cli = "show sdh map %s.%s" % (self.cliType(), self.cliId())
        result = self.runCli(cli)
        mapType = result.cellContent(0, "MappingType")
        self.silentRestore()
        return mapType

    @mapType.setter
    def mapType(self, value):
        cli = "sdh map %s.%s %s" % (self.cliType(), self.cliId(), value)
        self.runCli(cli)

    def line(self):
        return self.parent.line()
    
    def mapChannel(self):
        return None
    
    def _cliGenericType(self):
        return None
    
    def _sdhCliId(self):
        return "%s.%s" % (self.cliType(), self.cliId())

    @property
    def txTti(self):
        result = self.runCli("show sdh %s tti %s" % (self._cliGenericType(), self._sdhCliId()))
        mode = result.cellContent(0, "txTtiMode")
        message = result.cellContent(0, "txTti")
        padding = result.cellContent(0, "txPadding")
        return AtTti(mode, message, padding)

    @txTti.setter
    def txTti(self, value):
        self.runCli("sdh %s tti transmit %s %s %s %s" % (
        self._cliGenericType(), self._sdhCliId(), value.mode, value.message, value.padding))

    @property
    def expectedTti(self):
        result = self.runCli("show sdh %s tti %s" % (self._cliGenericType(), self._sdhCliId()))
        mode = result.cellContent(0, "expTtiMode")
        message = result.cellContent(0, "expTti")
        padding = result.cellContent(0, "expPadding")
        return AtTti(mode, message, padding)

    @expectedTti.setter
    def expectedTti(self, value):
        self.runCli("sdh %s tti expect %s %s %s %s" % (
        self._cliGenericType(), self._sdhCliId(), value.mode, value.message, value.padding))

    @property
    def rxTti(self):
        result = self.runCli("show sdh %s tti %s" % (self._cliGenericType(), self._sdhCliId()))
        mode = result.cellContent(0, "rxTtiMode")
        message = result.cellContent(0, "rxTti")
        padding = result.cellContent(0, "rxPadding")
        return AtTti(mode, message, padding)

    def currentAlarm(self):
        result = self.runCli("show sdh %s alarm %s" % (self._cliGenericType(), self._sdhCliId()))
        alarms = []
        
        for columnId in range(1, result.numColumns()):
            alarmName = result.columnName(columnId)
            status = result.cellContent(0, alarmName)
            if self.boolValFromString(status):
                alarms.append(alarmName)
        
        return alarms
    
    def _correctAlarmNameFromString(self, alarmString):
        return alarmString
    
    @property
    def alarmAffects(self):
        self.silent()
        result = self.runCli("show sdh %s alarm affect %s" % (self._cliGenericType(), self._sdhCliId()))

        alarms = []
        for column in range(1, result.numColumns()):
            cellContent = result.cellContent(0, column)
            if self.boolFromString(cellContent):
                alarmName = self._correctAlarmNameFromString(result.columnName(column))
                alarms.append(alarmName)
                
        self.silentRestore()
                
        return alarms

    def _setLoopback(self, mode):
        self.runCli("sdh %s loopback %s %s" % (self._cliGenericType(), self._sdhCliId(), mode))

    def _getLoopback(self):
        result = self.runCli("show sdh %s loopback %s" % (self._cliGenericType(), self._sdhCliId()))
        return result.cellContent(0, "Loopback")

    def _autoRdiIsEnabled(self):
        self.notImplemented()
        return False

    def _autoRdiEnable(self, enabled):
        self.notImplemented()

    def _getTug2Id(self):
        return None

    def _getStsId(self):
        return None

    def _getTu1xId(self):
        return None

    @property
    def autoRdiEnabled(self):
        return self._autoRdiIsEnabled()

    @autoRdiEnabled.setter
    def autoRdiEnabled(self, value):
        self._autoRdiEnable(value)

    @property
    def stsId(self):
        return self._getStsId()

    @property
    def tug2Id(self):
        return self._getTug2Id()

    @property
    def tu1xId(self):
        return self._getTu1xId()

class AtSdhAug(AtSdhChannel):
    def flatId(self):
        return self.channelId
    
    def cliId(self):
        line = self.line()
        if line is None or self.flatId() is None:
            assert()
        return "%d.%d" % (line.channelId + 1, self.flatId() + 1)

    def _getTug2Id(self):
        return 0

    def _getTu1xId(self):
        return 0
    
class AtSdhAug1(AtSdhAug):
    MAP_VC4    = "vc4"
    MAP_3xVC3s = "3xvc3s"
    
    def cliType(self):
        return "aug1"
    
    def flatId(self):
        flatId = AtSdhAug.flatId(self)
        if isinstance(self.parent, AtSdhAug4):
            flatId = (self.parent.flatId() * 4) + flatId
        return flatId
    
    def _loadSubChannels(self):
        mapType = self.mapType
        factory = self.channelFactory()
        
        if mapType == self.MAP_VC4:
            return [factory.createVc4(self.module, self, 0)]
        
        if mapType == self.MAP_3xVC3s:
            subChannels = []
            for au3Id in range(3):
                subChannels.append(factory.createAu3(self.module, self, au3Id))
            
            return subChannels

    def _getStsId(self):
        return self.flatId() * 3

class AtSdhAug4(AtSdhAug):
    MAP_VC4_4c  = "vc4_4c"
    MAP_4xAUG1s = "4xaug1s"
    
    def cliType(self):
        return "aug4"
    
    def flatId(self):
        flatId = self.channelId
        if isinstance(self.parent, AtSdhAug16):
            flatId = (self.parent.flatId() * 4) + self.channelId
        return flatId
    
    def _loadSubChannels(self):
        mapType = self.mapType
        factory = self.channelFactory()
        
        if mapType == self.MAP_VC4_4c:
            return [factory.createVc4_4c(self.module, self, 0)]
        
        if mapType == self.MAP_4xAUG1s:
            subChannels = []
            for aug1Id in range(4):
                subChannels.append(factory.createAug1(self.module, self, aug1Id))
            
            return subChannels
        
        return None

    def _getStsId(self):
        return self.flatId() * 12
    
class AtSdhAug16(AtSdhAug):
    MAP_VC4_16c = "vc4_16c"
    MAP_4xAUG4s = "4xaug4s"
    
    def cliType(self):
        return "aug16"
    
    def _loadSubChannels(self):
        mapType = self.mapType
        factory = self.channelFactory()
        
        if mapType == self.MAP_VC4_16c:
            return [factory.createVc4_16c(self.module, self, 0)]
        
        if mapType == self.MAP_4xAUG4s:
            subChannels = []
            for aug4Id in range(4):
                subChannels.append(factory.createAug4(self.module, self, aug4Id))
            
            return subChannels
        
        return None

    def _getStsId(self):
        return self.flatId() * 48

class AtSdhAug64(AtSdhAug):
    MAP_VC4_64c  = "vc4_64c"
    MAP_4xAUG16s = "4xaug16s"
    
    def cliType(self):
        return "aug64"
    
    def _loadSubChannels(self):
        mapType = self.mapType
        factory = self.channelFactory()
        
        if mapType == self.MAP_VC4_64c:
            return [factory.createVc4_64c(self.module, self, 0)]
        
        if mapType == self.MAP_4xAUG16s:
            subChannels = []
            for aug16Id in range(4):
                subChannels.append(factory.createAug16(self.module, self, aug16Id))
            
            return subChannels
        
        return None

    def _getStsId(self):
        return self.flatId() * 192
    
class AtSdhAu(AtSdhChannel):
    def cliId(self):
        return self.parent.cliId()

    def _getStsId(self):
        return self.parent.stsId

class AtSdhAu3(AtSdhAu):
    def cliType(self):
        return "au3"
    
    def cliId(self):
        if isinstance(self.parent, AtSdhLine):
            return "%d.%d" % (self.parent.line().channelId + 1, self.channelId + 1)
        return "%s.%d" % (self.parent.cliId(), self.channelId + 1)
    
    def _loadSubChannels(self):
        return [self.channelFactory().createVc3(self.module, self, 0)]

    def _getStsId(self):
        if isinstance(self.parent, AtSdhLine):
            return 0

        return self.parent.stsId + self.channelId
    
class AtSdhAu4(AtSdhAu):
    def cliType(self):
        return "au4"
    
    def _loadSubChannels(self):
        return [self.channelFactory().createVc4(self.module, self, 0)]

class AtSdhAu4_4c(AtSdhAu):
    def cliType(self):
        return "au4_4c"
    
    def _loadSubChannels(self):
        return [self.channelFactory().createVc4_4c(self.module, self, 0)]
        
class AtSdhAu4_16c(AtSdhAu):
    def cliType(self):
        return "au4_16c"
    
    def _loadSubChannels(self):
        return [self.channelFactory().createVc4_16c(self.module, self, 0)]

class AtSdhAu4_64c(AtSdhAu):
    def cliType(self):
        return "au4_64c"
    
    def _loadSubChannels(self):
        return [self.channelFactory().createVc4_64c(self.module, self, 0)]

class AtSdhTug(AtSdhChannel):
    def cliId(self):
        return "%s.%d" % (self.parent.cliId(), self.channelId + 1)

    def _getStsId(self):
        return self.parent.stsId
    
class AtSdhTug2(AtSdhTug):
    MAP_TU11 = "tu11" 
    MAP_TU12 = "tu12"
    
    def cliType(self):
        return "tug2"
    
    def numSubChannelWithMapType(self, mapType):
        if mapType == self.MAP_TU11: return 4
        if mapType == self.MAP_TU12: return 3
        return 0

    def _createSubChannel(self, mapType, tu1xId):
        if mapType == self.MAP_TU11: return self.channelFactory().createTu11(self.module, self, tu1xId)
        if mapType == self.MAP_TU12: return self.channelFactory().createTu12(self.module, self, tu1xId)
        return None

    def _loadSubChannels(self):
        mapType = self.mapType
        numSubChannels = self.numSubChannelWithMapType(mapType)
        subChannels = []
        for tu1xId in range(numSubChannels):
            tu1x = self._createSubChannel(mapType, tu1xId)
            subChannels.append(tu1x)
            
        return subChannels

    def _getTug2Id(self):
        return self.channelId

    def _getTu1xId(self):
        return 0
    
class AtSdhTug3(AtSdhTug):
    MAP_VC3     = "vc3" 
    MAP_7xTUG2s = "7xtug2s"
    
    def cliType(self):
        return "tug3"
    
    def _loadSubChannels(self):
        mapType = self.mapType
        subChannels = []
        factory = self.channelFactory()
        
        if mapType == self.MAP_7xTUG2s:
            for tug2Id in range(7):
                subChannels.append(factory.createTug2(self.module, self, tug2Id))
            return subChannels
        
        if mapType == self.MAP_VC3:
            return [self.channelFactory().createVc3(self.module, self, 0)]
            
        return []

    def _getStsId(self):
        return self.parent.stsId + self.channelId

    def _getTug2Id(self):
        return 0

    def _getTu1xId(self):
        return 0
    
class AtSdhVc(AtSdhChannel):
    def isHoVc(self):
        return True

    def isLoVc(self):
        return not self.isHoVc()

    def _cliGenericType(self):
        return "path"
    
    def pdhModule(self):
        return self.module.device.pdhModule()
    
    def _loadSubChannels(self):
        return []
        
    def cliId(self):
        return self.parent.cliId()
    
    def cliToShowPwBindingInfo(self):
        return "show sdh path %s.%s" % (self.cliType(), self.cliId())
    
    def _getPathAttribute(self, name, silent = False):
        if silent:
            self.silent()
            
        result = self.runCli("show sdh path %s" % self._sdhCliId())
        attributeValue = result.cellContent(0, name)
        
        if silent:
            self.silentRestore()
            
        return attributeValue

    @property
    def txPsl(self):
        return int(self._getPathAttribute("txPsl"), 16)

    @txPsl.setter
    def txPsl(self, value):
        self.runCli("sdh path psl transmit %s 0x%x" % (self._sdhCliId(), value))

    @property
    def expectedPsl(self):
        return int(self._getPathAttribute("expPsl"), 16)

    @expectedPsl.setter
    def expectedPsl(self, value):
        self.runCli("sdh path psl expect %s 0x%x" % (self._sdhCliId(), value))

    @property
    def rxPsl(self):
        return int(self._getPathAttribute("rxPsl"), 16)

    @property
    def holdOnTimer(self):
        return int(self._getPathAttribute("Hold-on", silent=True), 10)

    @holdOnTimer.setter
    def holdOnTimer(self, value):
        self.runCli("sdh path holdon time %s %d" % (self._sdhCliId(), value))

    @property
    def holdOffTime(self):
        return int(self._getPathAttribute("Hold-off", silent=True), 10)

    @holdOffTime.setter
    def holdOffTime(self, value):
        self.runCli("sdh path holdoff time %s %d" % (self._sdhCliId(), value))

    @AtChannel.sourceChannel.getter
    def sourceChannel(self):
        self.silent()
        result = self.runCli("show xc vc source %s" % self._sdhCliId())
        source = result.cellContent(0, "Source")
        if self.isNoneStringValue(source):
            return None
        
        channel = self.module.getChannel(source)
        self.silentRestore()
        
        return channel

    @AtChannel.destinationChannels.getter
    def destinationChannels(self):
        self.silent()
        result = self.runCli("show xc vc dest %s full" % self._sdhCliId())

        dests = result.cellContent(0, "Destinations")
        destChannels = []
        for dest in dests.split(","):
            destChannels.append(self.module.getChannel(dest))
            
        self.silentRestore()
            
        return destChannels

    def _getStsId(self):
        return self.parent.stsId

    def _getTug2Id(self):
        return self.parent.tug2Id

    def _getTu1xId(self):
        return self.parent.tu1xId
    
class AtSdhVc1x(AtSdhVc):
    MAP_C1x = "c1x"
    MAP_DE1 = "de1"
    
    def cliType(self):
        return "vc1x"
        
    def mapChannel(self):
        if self.mapType == self.MAP_DE1:
            return self.pdhChannelFactory().createVcDe1(self.pdhModule(), self)
        return None
    
    def isHoVc(self):
        return False

class AtSdhVc11(AtSdhVc1x):
    pass

class AtSdhVc12(AtSdhVc1x):
    pass

class AtSdhVc3(AtSdhVc):
    MAP_C3 = "c3"
    MAP_7xTUG2s = "7xtug2s"
    MAP_DE3 = "de3" 
    
    def cliType(self):
        return "vc3"
    
    def _loadSubChannels(self):
        mapType = self.mapType
        subChannels = []
        factory = self.channelFactory()
        
        if mapType == self.MAP_7xTUG2s:
            for tug2Id in range(7):
                subChannels.append(factory.createTug2(self.module, self, tug2Id))
            return subChannels
        
        return []
    
    def mapChannel(self):
        if self.mapType == self.MAP_DE3:
            return self.pdhChannelFactory().createVcDe3(self.pdhModule(), self)
        return None
    
    def isHoVc(self):
        if isinstance(self.parent, AtSdhTug3):
            return False
        return True
    
class AtSdhVc4(AtSdhVc):
    MAP_C4 = "c4"
    MAP_3xTUG3s = "3xtug3s"
    
    def cliType(self):
        return "vc4"
    
    def _loadSubChannels(self):
        mapType = self.mapType
        subChannels = []
        factory = self.channelFactory()
        
        if mapType == self.MAP_3xTUG3s:
            for tug3Id in range(3):
                subChannels.append(factory.createTug3(self.module, self, tug3Id))
            return subChannels
        
        return []

class AtSdhVc4_4c(AtSdhVc):
    def cliType(self):
        return "vc4_4c"

class AtSdhVc4_16c(AtSdhVc):
    def cliType(self):
        return "vc4_16c"

class AtSdhVc4_64c(AtSdhVc):
    def cliType(self):
        return "vc4_64c"
        
class AtSdhTu(AtSdhChannel):
    def cliId(self):
        return "%s.%d" % (self.parent.cliId(), self.channelId + 1)

class AtSdhTu3(AtSdhTu):
    def cliType(self):
        return "tu3"
    
    def _loadSubChannels(self):
        return [self.channelFactory().createVc3(self.module, self, 0)]

    def cliId(self):
        return self.parent.cliId()

    def _getTug2Id(self):
        return 0

    @AtSdhChannel.tu1xId.getter
    def tu1xId(self):
        return 0
    
class AtSdhTu1x(AtSdhTu):
    def cliType(self):
        return "tu1x"

    def _createVc1x(self):
        if isinstance(self, AtSdhTu11):
            return self.channelFactory().createVc11(self.module, self, 0)
        if isinstance(self, AtSdhTu12):
            return self.channelFactory().createVc12(self.module, self, 0)

        return None

    def _loadSubChannels(self):
        return [self._createVc1x()]

    def _getTu1xId(self):
        return self.channelId

class AtSdhTu11(AtSdhTu1x):
    pass

class AtSdhTu12(AtSdhTu1x):
    pass

class AtSdhLine(AtSdhChannel):
    # Rates
    RATE_STM0  = "stm0"
    RATE_STM1  = "stm1"
    RATE_STM4  = "stm4"
    RATE_STM16 = "stm16"
    RATE_STM64 = "stm64"
    
    # Alarms
    ALARM_LOS         = "los"
    ALARM_OOF         = "oof"
    ALARM_LOF         = "lof"
    ALARM_TIM         = "tim"
    ALARM_AIS         = "ais"
    ALARM_RDI         = "rdi"
    ALARM_BERSD       = "bersd"
    ALARM_BERSF       = "bersf"
    ALARM_KBYTECHANGE = "kbytechange"
    ALARM_KBYTEFAIL   = "kbytefail"
    ALARM_S1CHANGE    = "s1change"
    ALARM_RSBERSD     = "rsbersd"
    ALARM_RSBERSF     = "rsbersf"
    ALARM_K1CHANGE    = "k1change"
    ALARM_K2CHANGE    = "k2change"
    ALARM_BERTCA      = "bertca"
    ALARM_RFI         = "rfi"   
    
    @classmethod
    def alarmTypes(cls):
        return [cls.ALARM_LOS,         
                cls.ALARM_OOF,         
                cls.ALARM_LOF,         
                cls.ALARM_TIM,         
                cls.ALARM_AIS,         
                cls.ALARM_RDI,         
                cls.ALARM_BERSD,       
                cls.ALARM_BERSF,       
                cls.ALARM_KBYTECHANGE,
                cls.ALARM_KBYTEFAIL,  
                cls.ALARM_S1CHANGE,   
                cls.ALARM_RSBERSD,    
                cls.ALARM_RSBERSF,    
                cls.ALARM_K1CHANGE,   
                cls.ALARM_K2CHANGE,   
                cls.ALARM_BERTCA,      
                cls.ALARM_RFI]
    
    def _sdhCliId(self):
        return self.cliId()
    
    def _cliGenericType(self):
        return "line"
    
    def _getCliAttribute(self, attributeName, silent = True):
        if silent:
            self.silent()
            
        result = self.runCli("show sdh line %s" % self.cliId())
        attributeValue = result.cellContent(0, attributeName)
        
        if silent:
            self.silentRestore()
            
        return attributeValue

    @property
    def rate(self):
        return self._getCliAttribute("rate")

    @rate.setter
    def rate(self, value):
        self.runCli("sdh line rate %s %s" % (self.cliId(), value))

    @property
    def scrambleEnabled(self):
        return self.boolFromString(self._getCliAttribute("scramble"))

    @scrambleEnabled.setter
    def scrambleEnabled(self, value):
        self.runCli("sdh line scramble %s %s" % ("enable" if value is True else "disable", self.cliId()))

    def _loadSubChannels(self):
        rate = self.rate
        channelFactory = self.channelFactory()
        
        if rate == AtSdhLine.RATE_STM0:
            au3 = channelFactory.createAu3(self.module, self, 0)
            return [au3]
        
        if rate == AtSdhLine.RATE_STM1:
            aug1 = channelFactory.createAug1(self.module, self, 0)
            return [aug1]
        
        if rate == AtSdhLine.RATE_STM4:
            aug4 = channelFactory.createAug4(self.module, self, 0)
            return [aug4]
        
        if rate == AtSdhLine.RATE_STM16:
            aug16 = channelFactory.createAug16(self.module, self, 0)
            return [aug16]
        
        if rate == AtSdhLine.RATE_STM64:
            aug64 = channelFactory.createAug64(self.module, self, 0)
            return [aug64]
        
        return None
    
    def line(self):
        return self
    
    def getAug16(self, aug16Id):
        if self.rate in [self.RATE_STM0, self.RATE_STM1, self.RATE_STM4]:
            return None
        if self.rate == self.RATE_STM16:
            return self.subChannels[0]
        
        return self.subChannels[aug16Id]
    
    def getAug4(self, aug4Id):
        if self.rate in [self.RATE_STM0, self.RATE_STM1]:
            return None
        if self.rate == self.RATE_STM4:
            return self.subChannels[0]
        
        aug16 = self.getAug16(aug4Id / 4)
        if aug16 is None:
            return None
        
        return aug16.subChannels[aug4Id % 4]
    
    def getAug1(self, aug1Id):
        if self.rate == self.RATE_STM0:
            return None
        
        if self.rate == self.RATE_STM1:
            return self.subChannels[0]
        
        aug4 = self.getAug4(aug1Id / 4)
        if aug4 is None:
            return None
        
        return aug4.subChannels[aug1Id % 4]

    def getAu3Tu3(self, aug1Id, au3Tu3Id):
        if self.rate == self.RATE_STM0:
            return self.subChannels[0]
        
        aug1 = self.getAug1(aug1Id)
        if aug1 is None:
            return None
        
        if aug1.mapType == AtSdhAug1.MAP_3xVC3s:
            return aug1.subChannels[au3Tu3Id]
        
        vc4 = self.subChannels[0]
        tug3 = vc4.subChannels[au3Tu3Id]
        if tug3.mapType == AtSdhTug3.MAP_VC3:
            return tug3.subChannels[0]
        
        return None

    def getVc3(self, aug1Id, au3Tu3Id):
        au3Tu3 = self.getAu3Tu3(aug1Id, au3Tu3Id)
        if au3Tu3 is None:
            return None
        return au3Tu3.subChannels[0]
    
    def getTug2(self, aug1Id, au3Tu3Id, tug2Id):
        aug1 = self.getAug1(aug1Id)
        
        if aug1.mapType == AtSdhAug1.MAP_3xVC3s:
            au3 = aug1.subChannels[au3Tu3Id]
            vc3 = au3.subChannels[0]
            return vc3.subChannels[tug2Id]
        
        if aug1.mapType == AtSdhAug1.MAP_VC4:
            vc4 = aug1.subChannels[0]
            tug3 = vc4.subChannels[au3Tu3Id]
            return tug3.subChannels[tug2Id]
        
        return None
    
    def getTu1x(self, aug1Id, au3Tu3Id, tug2Id, tu1xId):
        tug2 = self.getTug2(aug1Id, au3Tu3Id, tug2Id)
        return tug2.subChannels[tu1xId]
    
    def getChannel(self, cliChannelId):
        channelType, ids = AtChannel.parseCliId(cliChannelId)
        if channelType == "vc3":
            return self.getVc3(ids[1], ids[2])
        
        if channelType == "vc1x":
            tu1x = self.getTu1x(ids[1], ids[2], ids[3], ids[4])
            return tu1x.subChannels[0]
            
        # Will be added more
        self.notImplemented()
        
    def _correctAlarmNameFromString(self, alarmString):
        if alarmString in self.alarmTypes():
            return alarmString
        
        alarmString = alarmString.lower()
        if alarmString in self.alarmTypes():
            return alarmString
        
        # Can't convert
        assert()

    def _autoRdiEnable(self, enabled):
        self.runCli("sdh line autordi %s %s" % ("enable" if enabled else "disable", self.cliId()))

    def _autoRdiIsEnabled(self):
        return self.boolFromString(self._getCliAttribute("autoRdi"))

    def _enable(self, value):
        self.runCli("sdh line %s %s" % ("enable" if value else "disable", self.cliId()))

    def _isEnabled(self):
        return self.boolValFromString(self._getCliAttribute("txEnable"))

    def _getStsId(self):
        return 0

    def _getTug2Id(self):
        return 0

    def _getTu1xId(self):
        return 0
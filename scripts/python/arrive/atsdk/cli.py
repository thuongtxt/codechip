"""
Created on Apr 14, 2017

@author: namnng
"""

from python.arrive.AtAppManager.AtAppManager import AtAppManager
from python.arrive.AtAppManager.AtColor import AtColor


class AtCliListener(object):
    def willRunCli(self, cli):
        pass
    
    def didRunCliWithResult(self, cli, result):
        pass

class AtCliRunnable(object):
    @classmethod
    def cliResultIsSuccess(cls, cliResult):
        if type(cliResult) is list:
            for _, result in cliResult:
                if not result.success():
                    return False

            return True

        return cliResult.success()

    @classmethod
    def cliProcessor(cls):
        """
        :rtype: AtCliProcessor
        """
        return AtTextUI.sharedUI().cliProcessor
    
    @classmethod
    def runCli(cls, cli):
        """
        :rtype: Union[python.arrive.AtAppManager.AtCliResult.AtCliResult, list]
        """
        return cls.cliProcessor().runCli(cli)
    
    def addListener(self, listener):
        self.cliProcessor().addListener(listener)
            
    def removeListener(self, listener):
        self.cliProcessor().removeListener(listener)

    @classmethod
    def silentOutput(cls, silent = True):
        AtTextUI.sharedUI().silentOutput(silent)
    
    @classmethod
    def outputIsSilent(cls):
        return AtTextUI.sharedUI().outputSilent
    
    @classmethod
    def silentCli(cls, silent = True):
        AtTextUI.sharedUI().silentCli(silent)

    @classmethod
    def cliIsSilent(cls):
        return AtTextUI.sharedUI().cliSilent

    @classmethod
    def silent(cls, silent = True):
        AtTextUI.sharedUI().silent(silent)
        
    @classmethod
    def silentRestore(cls):
        AtTextUI.sharedUI().silentRestore()

    @classmethod
    def isSilent(cls):
        AtTextUI.sharedUI().isSilent()
    
    @classmethod
    def app(cls):
        """
        :rtype: python.arrive.AtAppManager.AtAppManager.AtApp
        """
        return cls.cliProcessor().app
    
    @classmethod
    def boolFromString(cls, string):
        return AtTextUI.sharedUI().boolFromString(string)
    
    @classmethod
    def isNoneStringValue(cls, string):
        string = string.lower()
        return string == "none"

    @staticmethod
    def notImplemented():
        AtColor.printColor(AtColor.RED, "This feature has not implemented")
        assert 0


class AtTextUI(object):
    _sharedUI = None
    
    def __init__(self):
        object.__init__(self)
        self.outputSilent = None
        self.cliSilent = None
        self.silentBackup = None
        self.cliProcessor = AtCliProcessor()
        self._loadTextUIInfo()
    
    def _loadTextUIInfo(self):
        result = self.runCli("show textui")

        try:
            self.cliSilent = self.boolFromString(result.cellContent("CLI verbosed", "Value"))
        except:
            pass

        cliMode = result.cellContent("CLI Mode", "Value")
        self.outputSilent = True if cliMode == "silent" else False
    
    def silent(self, silent = True):
        self.silentBackup = self.isSilent()
        self.silentOutput(silent)
        self.silentCli(silent)
    
    def isSilent(self):
        return self.outputSilent and self.cliSilent
    
    def silentOutput(self, silent = True):
        self.runCli("textui mode %s" % ("silent" if silent else "normal"))
    
    def silentCli(self, silent = True):
        self.runCli("textui verbose %s" % ("disable" if silent else "enable"))
    
    def silentRestore(self):
        if self.silentBackup is not None:
            self.silent(self.silentBackup)
    
    @staticmethod
    def boolFromString(string):
        if string.lower() in ["set", "raise", "yes", "en"]:
            return True
        if string.lower() in ["clear", "no", "dis"]:
            return False
        
        assert()
    
    def runCli(self, cli):
        return self.cliProcessor.runCli(cli)
    
    @classmethod
    def sharedUI(cls):
        if cls._sharedUI is None:
            cls._sharedUI = AtTextUI()
        return cls._sharedUI
    
class AtCliProcessor(object):
    _sharedProcessor = None
    
    def __init__(self):
        object.__init__(self)
        self.listeners = []
        self.app = AtAppManager.localApp()
    
    def runCli(self, cli):
        for listener in self.listeners:
            listener.willRunCli(cli)
        
        result = AtAppManager.localApp().runCli(cli)
        
        for listener in self.listeners:
            listener.didRunCliWithResult(cli, result)
        
        if type(result) is list:
            for _, cliResult in result:
                assert(cliResult.success())
        else:   
            assert(result.success())
             
        return result
    
    def addListener(self, listener):
        if listener is None:
            return
        
        if listener not in self.listeners:
            self.listeners.append(listener)
    
    def removeListener(self, listener):
        if listener is None:
            return
        
        if listener in self.listeners:
            self.listeners.remove(listener)

"""
Created on May 4, 2017

@author: namnn
"""
from channel import AtChannel
from module import AtModule
from sdh import AtSdhChannel


class AtApsEngineFactory(object):
    @staticmethod
    def createUpsrEngine(aModule, engineId, workingPath, protectionPath):
        return AtUpsrEngine(aModule, engineId, workingPath, protectionPath)

class AtModuleAps(AtModule):
    def __init__(self, device):
        AtModule.__init__(self, device)
        self.engineFactory = AtApsEngineFactory()
    
    @staticmethod
    def pathCliId(path):
        if type(path) is str:
            return path
        
        return "%s.%s" % (path.cliType(), path.cliId())
        
    def createUpsrEngine(self, engineId, workingPath, protectionPath):
        cliId = engineId
        
        if isinstance(workingPath, AtSdhChannel):
            cliId = engineId + 1
        
        cli = "aps upsr engine create %d %s %s" % (cliId, self.pathCliId(workingPath), self.pathCliId(protectionPath))
        self.runCli(cli)
        
        return self.engineFactory.createUpsrEngine(self, engineId, workingPath, protectionPath)

class AtApsEngine(AtChannel):
    STATE_START = "start"
    STATE_STOP  = "stop"
    
    def start(self):
        self.notImplemented()
    
    def stop(self):
        self.notImplemented()

    def _setWtr(self, value):
        self.notImplemented()

    def _getWtr(self):
        self.notImplemented()
        return 0

    @property
    def wtr(self):
        return self._getWtr()

    @wtr.setter
    def wtr(self, value):
        self._setWtr(value)

    def _getSwitchType(self):
        self.notImplemented()
        return None

    def _setSwitchType(self, value):
        self.notImplemented()

    @property
    def switchType(self):
        return self._getSwitchType()

    @switchType.setter
    def switchType(self, value):
        self._setSwitchType(value)

class AtUpsrEngine(AtApsEngine):
    EXTERNAL_COMMAND_CLEAR = "clear"
    EXTERNAL_COMMAND_FS2WRK = "fs2wrk"
    EXTERNAL_COMMAND_FS2PRT = "fs2prt"
    EXTERNAL_COMMAND_MS2WRK = "ms2wrk"
    EXTERNAL_COMMAND_MS2PRT = "ms2prt"
    EXTERNAL_COMMAND_LP     = "lp"
    
    SWITCHING_TYPE_REVERTIVE     = "revertive"
    SWITCHING_TYPE_NON_REVERTIVE = "non-revertive"
    
    DEFECT_AIS    = "ais"
    DEFECT_LOP    = "lop"
    DEFECT_TIM    = "tim"
    DEFECT_UNEQ   = "uneq"
    DEFECT_PLM    = "plm"
    DEFECT_BER_SD = "ber-sd"
    DEFECT_BER_SF = "ber-sf"
    
    SWITCHING_CONDITION_SD = "sd"
    SWITCHING_CONDITION_SF = "sf"
    
    SWITCHING_REASON_LP   = "lp"
    SWITCHING_REASON_FS   = "fs"
    SWITCHING_REASON_SF   = "sf"
    SWITCHING_REASON_SD   = "sd"
    SWITCHING_REASON_MS   = "ms"
    SWITCHING_REASON_WTR  = "wtr"
    SWITCHING_REASON_NONE = "none"

    def __init__(self, amodule, engineId, workingPath, protectionPath):
        if type(workingPath) is str:
            engineId = engineId - 1
        
        AtChannel.__init__(self, amodule, engineId)
        self.workingPath = workingPath
        self.protectionPath = protectionPath

    def _getAttribute(self, attributeName, silent = False):
        if silent:
            self.silent()
            
        result = self.runCli("show aps upsr engine %s" % self.cliId())
        
        if silent:
            self.silentRestore()
        
        return result.cellContent(0, attributeName)

    def start(self):
        self.runCli("aps upsr engine start %s" % self.cliId())
        
    def stop(self):
        self.runCli("aps upsr engine stop %s" % self.cliId())

    @property
    def state(self):
        return self._getAttribute("State")

    @property
    def externalCommand(self):
        return self._getAttribute("ExternalCommand")

    @externalCommand.setter
    def externalCommand(self, value):
        self.runCli("aps upsr engine extcmd %s %s" % (self.cliId(), value))

    def _sdhModule(self):
        device = self.module.device
        return device.sdhModule()

    @property
    def activePath(self):
        pathCliId = self._getAttribute("active")
        if type(self.workingPath) is str:
            return pathCliId

        sdhModule = self._sdhModule()
        return sdhModule.getChannel(pathCliId)

    def setSwitchingCondition(self, defects, condition):
        self.runCli("aps upsr engine condition %s %s %s" % (self.cliId(), "|".join(defects), condition))

    @property
    def switchingReason(self):
        return self._getAttribute("RequestState")

    def _getWtr(self):
        self.silent()
        wtr = int(self._getAttribute("wtr (seconds)", silent=True), 10)
        self.silentRestore()
        return wtr

    def _setWtr(self, value):
        self.runCli("aps upsr engine wtr %s %s" % (self.cliId(), value))

    def _setSwitchType(self, switchType):
        self.runCli("aps upsr engine switchtype %s %s" % (self.cliId(), switchType))

    def _getSwitchType(self):
        return self._getAttribute("SwitchingType", silent=True)

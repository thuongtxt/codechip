from BranchesAccessCompare import BranchesAccessCompare
from utils.access_compare import RegisterProvider

class _RegisterProvider(RegisterProvider):
    """
    Note: this class is supposed to be updated whenever new upgrading needs to 
    be check. And when upgrading is done, new codes are all merged to main branch,
    then this class should be reverted to super logic status by removing the 
    shouldIgnoreAddress overriding
    """
    pass

class CiscoBranchesAccessCompare(BranchesAccessCompare):
    def _createRegisterProvider(self, ignoredAddresses=None, ignoreAddressesInFile=None):
        return _RegisterProvider(ignoredAddresses, ignoreAddressesInFile)
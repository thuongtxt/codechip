import utils.access_compare as access_compare
import os
from threading import Lock
from DefaultScriptGenerator import DefaultScriptGenerator
from python.EpApp import EpApp
from python.arrive.AtAppManager.AtColor import AtColor
from python.jenkin.AtOs import AtOs


class BranchesAccessCompare(object):
    _tempScriptFileCount = 0
    
    def __init__(self, git, sdkHome, buildDir = ".x86"):
        self.git = git
        self.sdkHome = sdkHome
        self.productCode = None
        self.buildDir = buildDir
        self.scripts = None
        self.compare = None
        self.ignoredAddresses = None
        self.ignoredAddressesInFile = None
        self.alsoLogWhenExit = False
        self.compareOnly = False
        self.version = "ff/ff/ff ee.ee.eeee"
        self._locker = Lock()
        
    def _lock(self):
        self._locker.acquire()
    def _unlock(self):
        self._locker.release()
        
    def _defaultScripts(self, productCode = None):
        return DefaultScriptGenerator.generate(productCode)
        
    @staticmethod
    def _make():
        AtOs.run("make -C ../ atclitable -j 10")
        AtOs.run("make -j 10")
        
    def _buildBranch(self, branchName):
        self.git.checkout(branchName)
        self._make()
    
    def _absoluteBuildDir(self):
        return "%s/%s" % (self.sdkHome, self.buildDir)
        
    def _firstEpappPath(self):
        return "%s/apps/ep_app/epapp_firstBranch" % self._absoluteBuildDir()
        
    def _secondEpappPath(self):
        return "%s/apps/ep_app/epapp_secondBranch" % self._absoluteBuildDir()
        
    def _firstBranchLogFile(self):
        return "%s/firstBranch_access.log" % self._absoluteBuildDir()
        
    def _secondBranchLogFile(self):
        return "%s/secondBranch_access.log" % self._absoluteBuildDir()
        
    def _cleanupLogFiles(self):
        AtOs.run("rm -fr %s" % self._firstBranchLogFile())
        AtOs.run("rm -fr %s" % self._secondBranchLogFile())
    
    def _createRegisterProvider(self, ignoredAddresses = None, ignoreAddressesInFile = None):
        return access_compare.RegisterProvider(ignoredAddresses, ignoreAddressesInFile)
    
    def _compareAccessLogFiles(self):
        self.compare = access_compare.Compare(self._firstBranchLogFile(), self._secondBranchLogFile())
        self.compare.registerProvider = self._createRegisterProvider(self.ignoredAddresses, self.ignoredAddressesInFile)
        if self.compare.compare():
            return None
        return self.compare
    
    @staticmethod
    def _commandForScript(script):
        _, extension = os.path.splitext(script)
        
        if extension == ".tcl":
            return "tcl %s" % script
        if extension == ".py":
            return "python %s" % script
        
        if len(extension) > 0:
            return "run %s" % script
        
        return None 
    
    @staticmethod
    def _verbosed():
        return True
    
    @staticmethod
    def _compareEnabled():
        return True
    
    def _halLogEnabled(self):
        return self._compareEnabled()
    
    @staticmethod
    def _threadEnabled():
        return True
    
    @staticmethod
    def _makeScript(script):
        tempFilePath = "%s/.temp.%d.script" % (AtOs.pwd(), BranchesAccessCompare._tempScriptFileCount)
        tempFile = open(tempFilePath, "w")
        tempFile.write(script)
        tempFile.close()
        BranchesAccessCompare._tempScriptFileCount = BranchesAccessCompare._tempScriptFileCount + 1
        return tempFilePath
    
    def _applyConfigurations(self):
        scripts = self.scripts
        if scripts is None:
            scripts = self._defaultScripts(self.productCode)
            
        if self.compareOnly:
            return self._compareAccessLogFiles()
            
        def runScript(appPath, appName, logFile, script_):
            versionString = "\"%s\"" % self.version
            app = EpApp("%s -s %s -w %s" % (appPath, self.productCode, versionString), None)
            app.logPrefix = appName
            app.verbosed = self._verbosed()
            app.start()
            if self._halLogEnabled():
                app.runCli("hal log enable")
                app.runCli("hal log file %s" % logFile)
            
            timeout = 5 * 60
            app.runScript(script_, timeout)
            
            if not self.alsoLogWhenExit:
                app.runCli("hal log disable")
                
            app.exit()

        for script in scripts:
            self._cleanupLogFiles()
            
            scriptFilePath = None
            if script.count("\n"):
                scriptFilePath = self._makeScript(script)
            
            if self._verbosed():
                self._lock()
                AtColor.printColor(AtColor.GREEN, "========================================================")
                print "Run scripts:"
                print "- First app : %s, logFile: %s" % (self._firstEpappPath(), self._firstBranchLogFile())
                print "- Second app: %s, logFile: %s" % (self._secondEpappPath(), self._secondBranchLogFile())
                print script
                AtColor.printColor(AtColor.GREEN, "========================================================")
                self._unlock()
            
            scriptFile = scriptFilePath if (scriptFilePath is not None) else script 
                        
            def runFirstApp():
                runScript(self._firstEpappPath(), "firstApp ", self._firstBranchLogFile(), scriptFile)
            def runSecondApp():
                runScript(self._secondEpappPath(), "secondApp", self._secondBranchLogFile(), scriptFile)
            
            if self._threadEnabled():
                thread1 = AtOs.createThreadWithFunction(runFirstApp)
                thread2 = AtOs.createThreadWithFunction(runSecondApp)
                thread1.start()
                thread2.start()
                thread1.join()
                thread2.join()
            else:
                runFirstApp()
                runSecondApp()
            
            if self._compareEnabled():
                compare = self._compareAccessLogFiles()
                if compare is not None:
                    return compare
            
            if scriptFilePath:
                AtOs.deleteFile(scriptFilePath)
            
        return None
            
    def compareAccess(self, firstBranch, secondBranch, buildFirstBranch=True, buildSecondBranch=True):
        buildDir = self._absoluteBuildDir()
        AtOs.cd(buildDir)
        
        epappDefaultPath = "%s/apps/ep_app/epapp" % self._absoluteBuildDir()
        
        if buildFirstBranch:
            self._buildBranch(firstBranch)
            AtOs.run("cp -f %s %s" % (epappDefaultPath, self._firstEpappPath()))
            
        if buildSecondBranch:
            self._buildBranch(secondBranch)
            AtOs.run("cp -f %s %s" % (epappDefaultPath, self._secondEpappPath()))
        
        return self._applyConfigurations()

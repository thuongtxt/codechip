"""
Created on Feb 15, 2017

@author: namnn
"""

from ha.Af6ConfigurationRandomizer import Af6ConfigurationRandomizer
from python.arrive.AtAppManager.AtColor import AtColor


class DefaultScriptGenerator(object):
    @classmethod
    def _createGenerator(cls, aProductCode):
        if aProductCode == 0x60290021:
            return _AnnaDefaultScriptGenerator()
    
        if aProductCode in [0x60210021, 0x60210031, 0x60210051, 0x60210011, 0x60210061, 0x60210012]:
            return _CiscoDefaultScriptsGenerator()
    
    @classmethod
    def _defaultScripts(cls):
        return ["device init"]
    
    def _generate(self, aProductCode, numScripts = None):
        return None
    
    @classmethod
    def generate(cls, productCode, numScripts = None):
        if type(productCode) is str:
            productCode = int(productCode, 16)
        
        generator = cls._createGenerator(productCode)
        if generator:
            return generator._generate(productCode, numScripts)
        
        return cls._defaultScripts()
        
class _AnnaDefaultScriptGenerator(DefaultScriptGenerator):
    @staticmethod
    def _mixWithFaceplateAndMate():
        return \
            """
            device init
            sdh line rate 1 stm64
            sdh line mode 1 sonet
            sdh line loopback 1 release
            sdh line tti transmit 1 16bytes ATVN_j0 null_padding
            sdh line tti expect 1 16bytes ATVN_j0 null_padding
            sdh line mode 17,18,25-28 sonet
            sdh line loopback 17,18,25-28 release
            sdh map aug64.1.1 4xaug16s
            sdh map aug16.1.1-1.2 4xaug4s
            sdh map aug4.1.1-1.8 4xaug1s
            sdh map aug1.1.1-1.32 3xvc3s
            sdh map vc3.1.1.1-1.32.3 c3
            
            sdh map aug16.17.1-18.1 4xaug4s
            sdh map aug4.17.1-18.4 4xaug1s
            sdh map aug1.17.1-18.16 3xvc3s
            sdh map vc3.17.1.1-18.16.3 c3
            
            sdh map aug16.25.1-26.1,27.1-28.1 4xaug4s
            sdh map aug4.25.1-26.4,27.1-28.4 4xaug1s
            sdh map aug1.25.1-26.16,27.1-28.16 3xvc3s
            sdh map vc3.25.1.1-26.16.3,27.1.1-28.16.3 c3
            
            xc vc connect vc3.17.1.1-18.16.3 vc3.25.1.1-26.16.3 two-way
            xc vc connect vc3.27.1.1-28.16.3 vc3.1.1.1-1.32.3 two-way
            
            debug ciena mro poh side mate
            sdh path tti transmit vc3.17.1.1-18.16.3 16bytes ATVN null_padding
            sdh path tti expect vc3.17.1.1-18.16.3 16bytes ATVN null_padding
            sdh path psl expect vc3.17.1.1-18.16.3 0xfe
            sdh path psl transmit vc3.17.1.1-18.16.3 0xfe
            
            debug ciena mro poh side faceplate
            sdh path tti transmit vc3.1.1.1-1.32.3 16bytes ATVN null_padding
            sdh path tti expect vc3.1.1.1-1.32.3 16bytes ATVN null_padding
            sdh path psl expect vc3.1.1.1-1.32.3 0xfe
            sdh path psl transmit vc3.1.1.1-1.32.3 0xfe
            pw create cep 1-192 basic
            pw circuit bind 1-192 vc3.25.1.1-26.16.3,27.1.1-28.16.3
            pw jitterbuffer 1-192 4000
            pw jitterdelay 1-192 2000
            pw payloadsize 1-192 783
            pw psn 1-192 mpls
            eth port enable 1
            eth port srcmac 1 22.44.22.44.22.44
            pw ethport 1-192 1
            pw ethheader 1-192 11.22.11.22.11.22 none none
            pw enable 1-192
            """
    
    @staticmethod
    def _serdesSetupScript():
        return \
            """
            device init
            serdes mode 1-16 stm16
            serdes powerup 1-16
            serdes enable 1-16
            sdh line mode 1,3,5,7 sonet
            sdh line rate 1,3,5,7 stm16
            """
    
    @staticmethod
    def _mateVc3BasicScript():
        return \
            """
            device init
            
            xc vc connect vc3.17.1.1-20.16.3 vc3.25.1.1-28.16.3 two-way
            pw create cep 1-192 basic
            pw circuit bind 1-192 vc3.25.1.1-28.16.3
            pw ethport 1-192 1
            pw psn 1-192 mpls
            pw enable 1-192
            """
    @staticmethod
    def _faceplateVc3BasicScript():
        return \
            """
            device init
            
            xc vc connect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 vc3.25.1.1-28.16.3 two-way
            pw create cep 1-192 basic
            pw circuit bind 1-192 vc3.25.1.1-28.16.3
            pw ethport 1-192 1
            pw psn 1-192 mpls
            pw enable 1-192
            """
    
    def _faceplateVc4BasicScript(self):
        return \
            """
            device init
            
            sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,25.1-28.16 vc4
            sdh map vc4.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,25.1-28.16 c4
            
            xc vc connect vc4.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16 vc4.25.1-28.16 two-way
            
            pw create cep 1-64 basic
            pw psn 1-64 mpls
            pw ethport 1-64 1
            pw jitterbuffer 1-64 4000
            pw jitterdelay 1-64  2000
            pw circuit bind 1-64 vc4.25.1-28.16
            pw enable 1-64
            """
            
    def _mateVc4BasicScript(self):
        return \
            """
            device init
            
            sdh map aug1.17.1-20.16,25.1-28.16 vc4
            sdh map vc4.17.1-20.16,25.1-28.16 c4
            
            xc vc connect vc4.17.1-20.16 vc4.25.1-28.16 two-way
            
            pw create cep 1-64 basic
            pw psn 1-64 mpls
            pw ethport 1-64 1
            pw jitterbuffer 1-64 4000
            pw jitterdelay 1-64  2000
            pw circuit bind 1-64 vc4.25.1-28.16
            pw enable 1-64
            """
       
    def _mateVc4_4cBasicScript(self):
        return \
            """
            device init
            
            sdh map aug4.17.1-20.425.1-28.4 vc4_4c
            sdh map vc4_4c.17.1-20.4,25.1-28.4 c4_4c
            
            xc vc connect vc4_4c.17.1-20.4 vc4_4c.25.1-28.4 two-way
            
            pw create cep 1-16 basic
            pw psn 1-16 mpls
            pw ethport 1-16 1
            pw jitterbuffer 1-16 4000
            pw jitterdelay 1-16  2000
            pw circuit bind 1-16 vc4_4c.25.1-28.16
            pw enable 1-16
            """
        
    def _faceplateVc4_16cBasicScript(self):
        return \
            """
            device init
            
            sdh map aug16.1.1,3.1,5.1,7.1,25.1-28.1 vc4_16c
            sdh map vc4_16c.1.1,3.1,5.1,7.1,25.1-28.1 c4_16c
            
            xc vc connect vc4_16c.1.1,3.1,5.1,7.1 vc4_16c.25.1-28.1 two-way
            
            pw create cep 1-4 basic
            pw psn 1-4 mpls
            pw ethport 1-4 1
            pw jitterbuffer 1-4 4000
            pw jitterdelay 1-4  2000
            pw circuit bind 1-4 vc4_16c.25.1-28.1
            pw enable 1-4
            """
            
    def _mateVc4_16cBasicScript(self):
        return \
            """
            device init
            
            sdh map aug16.17.1-20.1,25.1-28.1 vc4_16c
            sdh map vc4_16c.17.1-20.1,25.1-28.1 c4_16c
            
            xc vc connect vc4_16c.17.1-20.1 vc4_16c.25.1-28.1 two-way
            
            pw create cep 1-4 basic
            pw psn 1-4 mpls
            pw ethport 1-4 1
            pw jitterbuffer 1-4 4000
            pw jitterdelay 1-4  2000
            pw circuit bind 1-4 vc4_16c.25.1-28.1
            pw enable 1-4
            """
    
    def _faceplateVt15BasicScript(self):
        return \
            """
            device init
            
            sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,25.1-28.16 3xvc3s
            sdh map vc3.25.1.1-28.16.3 7xtug2s
            sdh map tug2.25.1.1.1-28.16.3.7 tu11
            sdh map vc1x.25.1.1.1.1-28.16.3.7.4 c1x
            
            xc vc connect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 vc3.25.1.1-28.16.3 two-way
            
            pw create cep 1-5376 basic
            pw psn 1-5376 mpls
            pw ethport 1-5376 1
            pw jitterbuffer 1-5376 8000
            pw jitterdelay 1-5376  4000
            pw circuit bind 1-5376 vc1x.25.1.1.1.1-28.16.3.7.4
            pw enable 1-5376
            """
    
    def _faceplateVt2BasicScript(self):
        return \
            """
            device init
            
            sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,25.1-28.16 3xvc3s
            sdh map vc3.25.1.1-28.16.3 7xtug2s
            sdh map tug2.25.1.1.1-28.16.3.7 tu12
            sdh map vc1x.25.1.1.1.1-28.16.3.7.3 c1x
            
            xc vc connect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 vc3.25.1.1-28.16.3 two-way
            
            sdh path tti expect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 16bytes MRO null_padding
            sdh path tti transmit vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 16bytes MRO null_padding
            sdh path tti monitor disable vc1x.25.1.1.1.1-28.16.3.7.3
            sdh path psl expect vc1x.25.1.1.1.1-28.16.3.7.3 0x6
            
            pw create cep 1-4032 basic
            pw psn 1-4032 mpls
            pw ethport 1-4032 1
            pw circuit bind 1-4032 vc1x.25.1.1.1.1-28.16.3.7.3
            pw enable 1-4032
            """
    
    def _faceplateDs1SatopScript(self):
        return \
            """
            device init

            sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,25.1-28.16 3xvc3s
            sdh map vc3.25.1.1-28.16.3 7xtug2s
            sdh map tug2.25.1.1.1-28.16.3.7 tu11
            sdh map vc1x.25.1.1.1.1-28.16.3.7.4 de1
            pdh de1 framing 25.1.1.1.1-28.16.3.7.4 ds1_unframed
            
            sdh path tti expect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 16bytes MRO null_padding
            sdh path tti transmit vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 16bytes MRO null_padding
            
            xc vc connect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 vc3.25.1.1-28.16.3 two-way
            
            pw create satop 1-5376
            pw psn 1-5376 mpls
            pw ethport 1-5376 1
            pw circuit bind 1-5376 de1.25.1.1.1.1-28.16.3.7.4
            pw enable 1-5376
            """
        
    def _faceplateE1SatopScript(self):
        return \
            """
            device init

            sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,25.1-28.16 3xvc3s
            sdh map vc3.25.1.1-28.16.3 7xtug2s
            sdh map tug2.25.1.1.1-28.16.3.7 tu12
            sdh map vc1x.25.1.1.1.1-28.16.3.7.3 de1
            pdh de1 framing 25.1.1.1.1-28.16.3.7.3 e1_unframed
            
            sdh path tti expect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 16bytes MRO null_padding
            sdh path tti transmit vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 16bytes MRO null_padding
            
            xc vc connect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 vc3.25.1.1-28.16.3 two-way
            
            pw create satop 1-4032
            pw psn 1-4032 mpls
            pw ethport 1-4032 1
            pw circuit bind 1-4032 de1.25.1.1.1.1-28.16.3.7.3
            pw enable 1-4032
            """
    
    def _faceplateDs3SatopScript(self):
        return \
            """
            device init

            sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,25.1-28.16 3xvc3s
            sdh map vc3.25.1.1-28.16.3 de3
            
            sdh path psl transmit vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 0x4
            sdh path psl expect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 0x4
            
            pdh de3 framing 25.1.1-28.16.3 ds3_unframed 
            
            xc vc connect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 vc3.25.1.1-28.16.3 two-way
            
            pw create satop 1-192
            pw psn 1-192 mpls
            pw ethport 1-192 1
            pw jitterbuffer 1-192 8000
            pw jitterdelay 1-192  4000
            pw circuit bind 1-192 de3.25.1.1-28.16.3
            pw enable 1-192
            """
    
    def _faceplateDs3CbitDs1SatopScript(self):
        return \
            """
            device init

            sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,25.1-28.16 3xvc3s
            sdh map vc3.25.1.1-28.16.3 de3
            
            sdh path psl transmit vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 0x4
            sdh path psl expect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 0x4
            
            pdh de3 framing 25.1.1-28.16.3 ds3_cbit_28ds1
            pdh de1 framing 25.1.1.1.1-28.16.3.7.4 ds1_unframed 
            
            xc vc connect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 vc3.25.1.1-28.16.3 two-way
            
            pw create satop 1-5376
            pw psn 1-5376 mpls
            pw ethport 1-5376 1
            pw jitterbuffer 1-5376 8000
            pw jitterdelay 1-5376  4000
            pw circuit bind 1-5376 de1.25.1.1.1.1-28.16.3.7.4
            pw enable 1-5376
            """
    
    def _faceplateDs3M13Ds1SatopScript(self):
        return \
            """
            device init

            sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,25.1-28.16 3xvc3s
            sdh map vc3.25.1.1-28.16.3 de3
            
            sdh path psl transmit vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 0x4
            sdh path psl expect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 0x4
            
            pdh de3 framing 25.1.1-28.16.3 ds3_m13_28ds1
            pdh de1 framing 25.1.1.1.1-28.16.3.7.4 ds1_unframed 
            
            xc vc connect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 vc3.25.1.1-28.16.3 two-way
            
            pw create satop 1-5376
            pw psn 1-5376 mpls
            pw ethport 1-5376 1
            pw jitterbuffer 1-5376 8000
            pw jitterdelay 1-5376  4000
            pw circuit bind 1-5376 de1.25.1.1.1.1-28.16.3.7.4
            pw enable 1-5376
            """
    
    def _faceplateE3SatopScript(self):
        return \
            """
            device init

            sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,25.1-28.16 3xvc3s
            sdh map vc3.25.1.1-28.16.3 de3
            
            sdh path psl transmit vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 0x4
            sdh path psl expect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 0x4
            
            pdh de3 framing 25.1.1-28.16.3 e3_unframed  
            
            xc vc connect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 vc3.25.1.1-28.16.3 two-way
            
            pw create satop 1-192
            pw psn 1-192 mpls
            pw ethport 1-192 1
            pw jitterbuffer 1-192 8000
            pw jitterdelay 1-192  4000
            pw circuit bind 1-192 de3.25.1.1-28.16.3
            pw enable 1-192
            """
    
    def _faceplateE3M13SatopScript(self):
        return \
            """
            device init

            sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,25.1-28.16 3xvc3s
            sdh map vc3.25.1.1-28.16.3 de3
            
            sdh path psl transmit vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 0x4
            sdh path psl expect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 0x4
            
            pdh de3 framing 25.1.1-28.16.3 e3_g751_16e1s
            pdh de1 framing 25.1.1.1.1-28.16.3.4.4 e1_unframed  
            
            xc vc connect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 vc3.25.1.1-28.16.3 two-way
            
            pw create satop 1-3072
            pw psn 1-3072 mpls
            pw ethport 1-3072 1
            pw circuit bind 1-3072 de1.25.1.1.1.1-28.16.3.7.4
            pw enable 1-3072
            """
        
    def _generate(self, aProductCode, numScripts):
        allScripts = [self._serdesSetupScript(),
                      self._mixWithFaceplateAndMate(),
                      self._faceplateVc3BasicScript(),
                      self._faceplateVc4BasicScript(),
                      self._faceplateVc4_16cBasicScript(),
                      self._faceplateVt15BasicScript(),
                      self._faceplateVt2BasicScript(),
                      self._faceplateDs1SatopScript(),
                      self._faceplateE1SatopScript(),
                      self._faceplateDs3SatopScript(),
                      self._faceplateDs3CbitDs1SatopScript(),
                      self._faceplateDs3M13Ds1SatopScript(),
                      self._faceplateE3SatopScript(),
                      self._faceplateE3M13SatopScript(),
                      self._mateVc3BasicScript(),
                      self._mateVc4BasicScript(),
                      self._mateVc4_4cBasicScript(),
                      self._mateVc4_16cBasicScript()]
        if numScripts:
            numScripts = min([numScripts, len(allScripts)])
            return allScripts[:numScripts]
        
        return allScripts

class _CiscoDefaultScriptsGenerator(DefaultScriptGenerator):
    def _generate(self, aProductCode, numScripts = None):
        scripts = []
        randomizer = Af6ConfigurationRandomizer.createRandomizerForProductCode(aProductCode)
        
        if numScripts is None:
            numScripts = 5
        
        for _ in range(numScripts):
            configuration = randomizer.defaultRandom()
            
            self._script = "device init"
            
            def _applyDict(aDict):
                for cli in aDict["cli"]:
                    self._script = self._script + "\n" + cli
                
                subChannels = aDict["subChannels"]
                for subChannel in subChannels:
                    _applyDict(subChannel)
            
            for line in configuration:
                _applyDict(line)
                
            scripts.append(self._script)
            self._script = None
                
        return scripts

if __name__ == '__main__':
    import getopt, sys
    
    expectedOptions = ["product-code=", "num-scripts=", "help"]
    def printUsage():
        print "Usage: %s --product-code=<productCode> [--num-scripts=<numScripts>]" % sys.argv[0] 
    
    try:
        opts, _ = getopt.getopt(sys.argv[1:], "", expectedOptions)
    except getopt.GetoptError:
        printUsage()
        sys.exit(2)
        
    productCode = None
    numScripts = 5
        
    for opt, arg in opts:
        if opt == "--product-code":
            productCode = int(arg, 16)
        if opt == "--num-scripts":
            numScripts = int(arg)
        if opt == "--help":
            printUsage()
            sys.exit(0)
            
    if productCode is None:
        printUsage()
        sys.exit(1)
    
    scripts = DefaultScriptGenerator.generate(productCode, numScripts)
    assert(len(scripts) == numScripts)
    for script in scripts:
        AtColor.printColor(AtColor.GREEN, "Script start: ============================================================")
        print script
        AtColor.printColor(AtColor.GREEN, "Script end  : ============================================================")
        
        print "\n" * 5
    
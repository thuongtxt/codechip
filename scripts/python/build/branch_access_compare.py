import sys
import getopt
from AnnaBranchesAccessCompare import AnnaBranchesAccessCompare
from BranchesAccessCompare import BranchesAccessCompare  
from CiscoBranchesAccessCompare import CiscoBranchesAccessCompare
from python.arrive.AtAppManager.AtColor import AtColor
from python.jenkin.AtGit import AtGit

class OptionParser(object):
    def __init__(self):
        self.sdkHome = None
        self.firstBranch = None
        self.secondBranch = None
        self.productCode = None
        self.buildFirstBranch = True
        self.buildSecondBranch = True
        self.ignoredAddresses = None
        self.ignoredAddressesInFile = None
        self.scripts = None
        self.customer = None
        self.git = AtGit(None)
        self.compareOnly = False
        self.version = None
    
    @staticmethod
    def _printUsage():
        print "Usage: %s --sdk-home=<SDK home> --first-branch=<first branch name> "        \
              "--second-branch=<second branch name> --product-code=<productCode> "         \
              "--ignored-addresses=<address1,address2,...> --ignored-addresses-in-file=<file> " \
              "[--no-build] [--no-build-first-branch] [--no-build-second-branch] [--scripts=<script1.tcl,script2.py,...>] "\
              "[--customer=<customer>] [--compare-only] [--version=<fpga version>]" % sys.argv[0]
              
    def parse(self):
        expectedOptions = ["sdk-home=", "first-branch=", "second-branch=",
                           "product-code=", "no-build", "no-build-first-branch",
                           "no-build-second-branch", "ignored-addresses=",
                           "ignored-addresses-in-file=", "scripts=", "customer=",
                           "compare-only", "version="]
        
        try:
            opts, _ = getopt.getopt(sys.argv[1:], "", expectedOptions)
        except getopt.GetoptError:
            self._printUsage()
            sys.exit(2)
            
        for opt, arg in opts:
            if opt == "--sdk-home":
                self.sdkHome = arg
            if opt == "--first-branch":
                self.firstBranch = arg
            if opt == "--second-branch":
                self.secondBranch = arg
            if opt == "--product-code":
                self.productCode = arg
            if opt == "--no-build":
                self.buildFirstBranch = False
                self.buildSecondBranch = False
            if opt == "--no-build-first-branch":
                self.buildFirstBranch = False
            if opt == "--no-build-second-branch":
                self.buildSecondBranch = False
            if opt == "--ignored-addresses":
                self.ignoredAddresses = arg
            if opt == "--ignored-addresses-in-file":
                self.ignoredAddressesInFile = arg
            if opt == "--scripts":
                self.scripts = arg.split(",")
            if opt == "--customer":
                self.customer = arg
            if opt == "--compare-only":
                self.compareOnly = True
            if opt == "--version":
                self.version = arg
        
        if (self.firstBranch is None) or (self.secondBranch is None) or (self.sdkHome is None) or (self.productCode is None):
            self._printUsage()
            sys.exit(1)

def createBranchAccessCompare(option):
    if option.customer == "anna":
        return AnnaBranchesAccessCompare(option.git, option.sdkHome)
    if option.customer == "cisco":
        return CiscoBranchesAccessCompare(option.git, option.sdkHome)
    
    return BranchesAccessCompare(option.git, option.sdkHome)

if __name__ == '__main__':
    options = OptionParser()
    options.parse()
    
    # Configure branch compare
    branchCompare = createBranchAccessCompare(options)
    if options.version is not None:
        branchCompare.version = options.version
    
    branchCompare.productCode = options.productCode
    branchCompare.ignoredAddresses = options.ignoredAddresses
    branchCompare.ignoredAddressesInFile = options.ignoredAddressesInFile
    branchCompare.scripts = options.scripts
    branchCompare.compareOnly = options.compareOnly
    
    # Compare
    compare = branchCompare.compareAccess(options.firstBranch, options.secondBranch, buildFirstBranch = options.buildFirstBranch, buildSecondBranch=options.buildSecondBranch)
    if compare is None:
        AtColor.printColor(AtColor.GREEN, "No different in configuration found on the two input branches")
        exit(0)
    else:
        AtColor.printColor(AtColor.RED, "Different in configuration found on the two input branches")
        compare.showDifferents()
        exit(1)

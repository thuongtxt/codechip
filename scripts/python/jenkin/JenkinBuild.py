import getopt
import sys
from collections import namedtuple

from AtGit import AtGit
import pexpect
import os
from AtOs import AtOs

'''
NOTE: whenever this is updated, it should be copied to /home/ATVN/namnn/libs/python/jenkin/JenkinBuild.py
just because Jenkins is configured to refer that part. This is bad but IT has not 
give us full control on the system.
cp -f scripts/python/jenkin/JenkinBuild.py /home/ATVN/namnn/libs/python/jenkin/JenkinBuild.py
'''

ExceptionInfo = namedtuple("ExceptionInfo", ["productCode", "script", "exception"])

class JenkinBuilder(object):
    def __init__(self, git, mainBranch, workingDir=None, scriptDir=None):
        self.git = git
        if workingDir is None:
            workingDir = os.getcwd()
        self.workingDir = workingDir
        self.scriptDir = scriptDir if scriptDir is not None else self.workingDir
        self.mainBranch = mainBranch
        self.buildDisabled = False
        self.unittestDisabled = False
        self.coverageDisabled = False
        self.pytestDisabled = False
        self.fetchCode = True
        self.specifiedProducts = None
        self.makeVerbose = False

    @staticmethod
    def _execute(cmd):
        child = pexpect.spawn(cmd)
        child.expect(pexpect.EOF)
        return child.before
    
    def shouldBuild(self):
        self.git.fetch()
        if self.mainBranch in self.git.updatedBranches:
            return True 
        return False
    
    def _x86BuildDir(self):
        return "%s/.x86" % self.workingDir
    
    def buildDir(self):
        return self._x86BuildDir()
    
    def productLists(self):
        return None
    
    def allProductCodes(self):
        return None

    def x86Build(self):
        buildDir = self._x86BuildDir()
        sourceDir = self.workingDir
        
        print "Current directory: " + sourceDir
        self._runOsCommand("rm -fr %s" % buildDir)
        self._runOsCommand("sh cleanautomake.sh")
        self._runOsCommand("sh bootstrap.sh")
        self._runOsCommand("mkdir %s" % buildDir)
        self._runOsCommand("make atclitable clean")
        os.chdir(buildDir)
        self._runOsCommand("sh ../configure %s --srcdir=%s CFLAGS=\"-g3 -O0 -m32 -fprofile-arcs -ftest-coverage\"" % 
                           ("--enable-unittest" if not self.unittestDisabled else "", sourceDir))
        self._runOsCommand("make -j 10 %s" % ("V=1" if self.makeVerbose else ""))
        self._runOsCommand("make serializecheck")
        self._runOsCommand("make classcheck")

    def unittestProducts(self):
        return self.productLists()

    def unittest(self):
        if self.unittestDisabled:
            return
        
        # We test a product at the time to split unittest report xml
        for productCode in self.allProductCodes():
            self._runOsCommand("%s/test/unit_test/driverunittest -s %x -q -x" % (self._x86BuildDir(), productCode))
    
    def coverage(self):
        if self.coverageDisabled:
            return
        
        self._runOsCommand("rm -fr *.html")
        self._runOsCommand("mkdir -p coverage")
        self._runOsCommand("gcovr --root=driver/ --filter=\".*\" --xml-pretty --output=coverage/coverage.xml")
    
    def pytest(self):
        pass
    
    def test(self):
        self.unittest()
        
        if not self.pytestDisabled:
            self.pytest()

    def compile(self):
        if self.buildDisabled:
            return
        
        self.x86Build()
    
    @staticmethod
    def _runOsCommand(command):
        AtOs.run(command)
    
    def canFetchCode(self):
        if self.git is None or self.git.password is None:
            return False
        return True
    
    def haveLatestCode(self):
        if not self.canFetchCode():
            return
        
        self.git.fetch()
        self._runOsCommand("git reset --hard")
        if self.git.localBranchExist(self.mainBranch):
            self._runOsCommand("git branch -D test/%s" % self.mainBranch)
        self._runOsCommand("git checkout test/%s" % self.mainBranch)
        self._runOsCommand("git merge --no-edit origin/test/%s" % self.mainBranch)
        self._runOsCommand("git merge --no-edit origin/%s" % self.mainBranch)
        
    def build(self):
        if self.fetchCode:
            self.haveLatestCode()
        self.compile()
        self.test()
        self.coverage()
    
    def epappPath(self):
        return "%s/apps/ep_app/epapp" % self._x86BuildDir()

    def unittestScriptDir(self):
        return "%s/scripts/test" % self.scriptDir
    
    @classmethod
    def builder(cls, git, mainBranch, workingDir=None, scriptDir = None):
        if mainBranch is None:
            mainBranch = git.currentBranch()
        
        if "cisco" in mainBranch:
            return CiscoBuilder(git, mainBranch, workingDir, scriptDir)
        if "anna" in mainBranch:
            return AnnaBuilder(git, mainBranch, workingDir, scriptDir)
        return JenkinBuilder(git, mainBranch, workingDir, scriptDir)
    
    @staticmethod
    def verboseOutput():
        # Turn this to True for debugging table output
        return False

    def runTestScript(self, productCode, scriptPath):
        from python.EpApp import EpApp

        AtOs.cd(self.buildDir())
        command = "%s -s %x -w \"ff/ff/ff ee.ee.eeee\"" % (self.epappPath(), productCode)
        print command
        app = EpApp(command)
        app.start()
        app.runCli("autotest disable")
        if not self.verboseOutput():
            app.runCli("textui mode silent")  # To speedup output, do not need to display huge table

        command = "python %s" % scriptPath
        print command
        app.runCli(command, timeout=120 * 60)
        app.exit()

class CiscoBuilder(JenkinBuilder):
    def productLists(self):
        return "60210051,60210031,60210021,60210012,60210061"

    def allProductCodes(self):
        return [0x60210051, 0x60210031, 0x60210021, 0x60210012, 0x60210061]
    
    def testHa(self):
        buildDir = self._x86BuildDir()
        os.chdir(buildDir)
        self._runOsCommand("/opt/tools/python2.7/bin/python %s/scripts/ha/hatest.py --epapp=%s/apps/ep_app/epapp" % (self.scriptDir, buildDir))
    
    def pytest(self):
        self.testHa()

class AnnaBuilder(JenkinBuilder):
    def allProductCodes(self):
        return [0x60290021, 0x60290011, 0x60290022, 0x60291011, 0x60291022, 0x60290061, 0x60290051, 0x60290081]

    def unittestProductCodes(self):
        return self.allProductCodes()

    @staticmethod
    def productCodeString(productCodes):
        return ",".join(["%x" % productCode for productCode in productCodes])

    def productLists(self):
        return self.productCodeString(self.allProductCodes())

    def unittestProducts(self):
        return self.productCodeString(self.unittestProductCodes())

    def testPdh(self):
        pass

    def runAllMroTestCases(self):
        if self.specifiedProducts is None:
            productCodes = self.allProductCodes()
        else:
            productCodes = self.specifiedProducts

        scriptRelativePaths = ["scripts/test/anna/run.py"]
        exceptions = list()
        for relativePath in scriptRelativePaths:
            scriptPath = "%s/%s" % (self.scriptDir, relativePath)
            for productCode in productCodes:
                try:
                    self.runTestScript(productCode=productCode, scriptPath=scriptPath)
                except Exception as e:
                    exceptions.append(ExceptionInfo(productCode=productCode, script=scriptPath, exception=e))

        return exceptions

    def testMroWarmRestore(self):
        processes = list()

        startPortId = 4561
        AtOs.cd(self.buildDir())
        scriptPath = "%s/scripts/test/anna/AnnaWarmRestore.py" % self.scriptDir

        if self.specifiedProducts is None:
            productCodes = self.allProductCodes()
        else:
            productCodes = self.specifiedProducts

        for productCode in productCodes:
            command = "python %s --hal-port=%d --product-code=%x --epapp=%s --repeat-times=1 --simulate" % (scriptPath, startPortId, productCode, self.epappPath())
            startPortId = startPortId + 1
            testProcess = AtOs.asyncRun(command, autoStart=True)
            processes.append(testProcess)

        for process in processes:
            process.join()

    def testMro(self):
        exceptions = list()
        exceptions.extend(self.runAllMroTestCases())
        self.testMroWarmRestore()
        return exceptions

    def pytest(self):
        exceptions = self.testMro()
        self.testPdh()

        if len(exceptions):
            for exceptionInfo in exceptions:
                print "Testing product {productCode} with script {scriptPath} got exception {exception}".format(
                    productCode=exceptionInfo.productCode,
                    scriptPath=exceptionInfo.script,
                    exception=exceptionInfo.exception
                )

            raise Exception("Python test fail because of exception")

def printUsage():
    print "Usage: %s [--help] [--gitpassword=<git password>] --mainBranch=<main branch> [--no-build] [--no-unittest] " \
          "[--no-coverage] [--no-pytest] [--work-dir=<directory>] [--script-dir=<scriptDir>] [--no-authenticate]"      \
          "[--no-fetch] [--products=<product1,product2>] [--verbose]" % sys.argv[0]

if __name__ == '__main__':
    # Parse input
    try:
        opts, args = getopt.getopt(sys.argv[1:],"",["help", "gitpassword=", "mainBranch=",
                                                    "no-build", "no-unittest", "no-coverage", "no-pytest",
                                                    "no-build", "no-unittest", "no-coverage",
                                                    "work-dir=", "script-dir=", "no-authenticate", 
                                                    "no-fetch", "products=", "verbose"])
    except getopt.GetoptError:
        printUsage()
        sys.exit(1)
        
    # Parse all options
    gitPassword = None
    mainBranch = None
    buildDisabled = False
    unittestDisabled = False
    coverageDisabled = False
    pytestDisabled = False
    workingDir = None
    scriptDir = None
    authenticateEnabled = True
    fetchCode = True
    products = None
    makeVerbose = False
    
    for opt, arg in opts:
        if opt == '--help':
            printUsage()
            sys.exit(0)
        elif opt == "--gitpassword":
            gitPassword = arg
        elif opt == "--mainBranch":
            mainBranch = arg
        elif opt == "--no-build":
            buildDisabled = True
        elif opt == "--no-unittest":
            unittestDisabled = True
        elif opt == "--no-coverage":
            coverageDisabled = True
        elif opt == "--no-pytest":
            pytestDisabled = True
        elif opt == "--work-dir":
            workingDir = arg
        elif opt == "--script-dir=":
            scriptDir = arg
        elif opt == "--no-authenticate":
            authenticateEnabled = False
        elif opt == "--no-fetch":
            fetchCode = False
        elif opt == "--products":
            products = [int(productCodeString, 16) for productCodeString in arg.split(",")]
        elif opt == "--verbose":
            makeVerbose = True

    git = AtGit(gitPassword, authenticateEnabled=authenticateEnabled)
    builder = JenkinBuilder.builder(git, mainBranch, workingDir, scriptDir)
    if builder is None:
        print "Builder has not been supported for this mainBranch"
        sys.exit(1)
        
    builder.unittestDisabled = unittestDisabled
    builder.buildDisabled = buildDisabled
    builder.coverageDisabled = coverageDisabled
    builder.pytestDisabled = pytestDisabled
    builder.fetchCode = fetchCode
    builder.specifiedProducts = products
    builder.makeVerbose = makeVerbose

    builder.build()

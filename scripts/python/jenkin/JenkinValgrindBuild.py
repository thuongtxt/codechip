import getopt
import sys
from AtGit import AtGit
from JenkinBuild import CiscoBuilder
import os
import xml.etree.ElementTree

class JenkinValgrindBuilder(CiscoBuilder):
    def __init__(self, git_, mainBranch_, outputXmlFile):
        CiscoBuilder.__init__(self, git_, mainBranch_)
        self.outputXmlFile = outputXmlFile
        
    def hasHa(self):
        return False
    
    def _backupPreviousBuildLog(self):
        buildDir = self._x86BuildDir()
        logFile = buildDir + '/' + self.outputXmlFile
        if os.path.exists(logFile):
            self._runOsCommand("cp %s ./" % logFile)
        
    def x86Build(self):
        buildDir = self._x86BuildDir()
        sourceDir = self.workingDir
        print "Current directory: " + sourceDir
        self._backupPreviousBuildLog()
        self._runOsCommand("rm -fr %s" % buildDir)
        self._runOsCommand("sh cleanautomake.sh")
        self._runOsCommand("sh bootstrap.sh")
        self._runOsCommand("mkdir %s" % buildDir)
        self._runOsCommand("make atclitable clean")
        os.chdir(buildDir)
        self._runOsCommand("sh ../configure --enable-unittest --srcdir=%s CFLAGS=\"-g3 -O0 -m32\"" % (sourceDir))
        self._runOsCommand("make -j 10")
        self._runOsCommand("valgrind --tool=memcheck --leak-check=full --show-reachable=yes --track-origins=yes --xml=yes --xml-file=%s test/unit_test/driverunittest -s %s -q -x" % (self.outputXmlFile, self.productLists()))
        
    def _pidStr(self, pid):
        return '==' + pid.text + '== '

    def _elementStr(self, parent, elementName):
        element = parent.find(elementName)
        if element is not None:
            return element.text
        return ''
        
    def _printStack(self, pid, stack):
        firstFrame = True
        for frame in stack.findall('frame'):
            if firstFrame == True:
                print self._pidStr(pid) + "  at " + self._elementStr(frame, 'ip') + ': ' + self._elementStr(frame, 'fn'),
                firstFrame = False 
            else:
                print self._pidStr(pid) + "  by " + self._elementStr(frame, 'ip') + ': ' + self._elementStr(frame, 'fn'),
                
            file = frame.find('file')
            if file is not None:
                print '(' + file.text + ':' + self._elementStr(frame, 'line') + ')'
            else:
                obj = frame.find('obj')
                if obj is not None:
                    print '(in ' + obj.text + ')'
    
    def _nextStack(self, iterator):   
        return iterator.next()
                 
    def _printError(self, pid, error):
        stackIter = error.iter('stack')
        msg = error.find('what')
        if msg is None:
            return
        print self._pidStr(pid) + msg.text
        
        stack = self._nextStack(stackIter)
        if stack is not None:
            self._printStack(pid, stack)
                        
        msg = error.find('auxwhat')
        if msg is None:
            return
        print self._pidStr(pid) + msg.text
            
        stack = self._nextStack(stackIter)
        if stack is not None:
            self._printStack(pid, stack)    
        
        print self._pidStr(pid)            
 
    def _printLeakError(self, pid, error):
        stackIter = error.iter('stack')
        msg = error.find('xwhat')
        if msg is None:
            return
        
        print self._pidStr(pid) + self._elementStr(msg, 'text')
        stack = self._nextStack(stackIter)
        if stack is not None:
            self._printStack(pid, stack)
                        
        print self._pidStr(pid) 
                   
    def _valgrindIsSuccess(self):
        buildDir = self._x86BuildDir()
        logFile = buildDir + '/' + self.outputXmlFile
        if os.path.exists(logFile) == False:
            return False
        
        root = xml.etree.ElementTree.parse(logFile).getroot()
        success = True
        
        pid = root.find('pid')
        if pid is None:
            pid = 'Unknown'
        
        print self._pidStr(pid) + "VALGRIND RESULT:"
        print self._pidStr(pid)
        
        if root.find('error') is not None:
            print self._pidStr(pid) + "VALGRIND ERROR:"    
            
        for error in root.findall('error'):
            self._printError(pid, error)
            self._printLeakError(pid, error)
            success = False
        
        if success:
            print self._pidStr(pid) + "VALGRIND SUCCESS"
                
        return success
         
         
def printUsage():
    print "Usage: %s [--help] --gitpassword=<git password> --mainBranch=<main branch>" % sys.argv[0]
    
if __name__ == '__main__':
    # Parse input
    try:
        opts, args = getopt.getopt(sys.argv[1:],"",["help", "gitpassword=", "mainBranch="])
    except getopt.GetoptError:
        printUsage()
        sys.exit(1)
        
    # Parse all options
    gitPassword = None
    mainBranch = None
    for opt, arg in opts:
        if opt == '-h':
            printUsage()
            sys.exit(2)
        elif opt == "--gitpassword":
            gitPassword = arg
        elif opt == "--mainBranch":
            mainBranch = arg
        
    if gitPassword is None:
        printUsage()
        sys.exit(1)
    
    if mainBranch is None:
        printUsage()
        sys.exit(1)
    
    git = AtGit(gitPassword)
    builder = JenkinValgrindBuilder(git, mainBranch, "valgrind_output.xml")
    if builder is None:
        print "Builder has not been supported for this mainBranch"
        sys.exit(1)
        
    builder.build()
    if builder._valgrindIsSuccess() == False:
        sys.exit(1)

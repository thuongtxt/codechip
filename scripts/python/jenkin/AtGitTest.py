"""
Created on Feb 10, 2017

@author: namnn
"""
import getopt
import sys
from AtGit import AtGit
from test.AtTestCase import AtTestCase, AtTestCaseRunner


class AtGitTest(AtTestCase):
    git = None
    
    def testRemoteBranches(self):
        self.assertGreater(len(self.git.remoteBranches()), 0, None)

    def testLocalBranch(self):
        self.assertGreater(len(self.git.localBranches()), 0, None)
        
    def testFetch(self):
        if self.git.password:
            self.git.fetch()
            print "%d updated branches, %d deleted branches, %d new branches" % (len(self.git.updatedBranches), len(self.git.deletedBranches), len(self.git.newBranches))
    
    def testBranchExist(self):
        branchName = "test/product/cisco"
        self.assertTrue(self.git.localBranchExist(branchName), None)
        self.assertFalse(self.git.localBranchExist("Just an invalid branch for test"), None)
    
    def testCurrentBranch(self):
        currentBranch = self.git.currentBranch()
        self.assertIsNotNone(currentBranch, None)
        print "currentBranch = %s\n" % currentBranch
    
if __name__ == '__main__':
    def printUsage():
        print "Usage: %s [--help] --gitpassword=<git password>" % sys.argv[0]
    
    try:
        opts, _ = getopt.getopt(sys.argv[1:],"",["help", "gitpassword="])
    except getopt.GetoptError:
        print ""
        sys.exit(1)
        
    gitPassword = None
    
    for opt, arg in opts:
        if opt == '-h':
            printUsage()
            sys.exit(2)
        elif opt == "--gitpassword":
            gitPassword = arg
    
    runner = AtTestCaseRunner.runner()
    AtGitTest.git = AtGit(gitPassword)
    runner.run(AtGitTest)
    runner.summary()

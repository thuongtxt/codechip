import sys
import getopt
import pexpect
import re
import pickle
from AtOs import AtOs

'''
NOTE: whenever this is updated, it should be copied to /home/ATVN/namnn/libs/python/jenkin/
just because Jenkins is configured to refer that part. This is bad but IT has not 
give us full control on the system.
cp -f scripts/python/jenkin/AtGit.py /home/ATVN/namnn/libs/python/jenkin/
'''

class AtGit(object):
    def __init__(self, password=None, authenticateEnabled=True):
        self.password = password
        self.prompt = "$"
        self.newBranches = []
        self.updatedBranches = []
        self.deletedBranches = []
        self.authenticateEnabled = authenticateEnabled

    @staticmethod
    def _execute(cmd):
        child = pexpect.spawn(cmd)
        child.expect(pexpect.EOF)
        return child.before

    @staticmethod
    def runCommand(command):
        AtOs.run(command)

    def authenticate(self, child):
        if self.password is None:
            raise Exception("Git fetching requires password")

        child.expect(".*?password: ")
        child.sendline(self.password)
        child.expect(pexpect.EOF, timeout=120)
        
    def fetch(self):
        child = pexpect.spawn("git fetch -p")
        
        if self.authenticateEnabled:
            self.authenticate(child)
        else:
            child.expect(pexpect.EOF, timeout=120)
        
        '''
        \s*(\*\s*\[new\s*branch\])\s+(\S+)\s*\->\s*(\S+)
           -----------------------   -----         ----
            New branch               local        remote
        '''
        newBranchPatern = re.compile("\s*(\*\s*\[new\s*branch\])\s+(\S+)\s*->\s*(\S+)")
        
        '''
        \s*(\S+\.\.\S+)\s+(\S+)\s*\->\s*(\S+)
           ------------   -----         ----
             Updated      local        remote
        '''
        updatedBranchPatern = re.compile("\s*(\S+\.\.\S+)\s+(\S+)\s*->\s*(\S+)")
        deletedBranchPatern = re.compile(".*?\[deleted\].*?->\s*(\S+)")
        
        self.newBranches = []
        self.updatedBranches = []
        self.deletedBranches = []
        for line in child.before.splitlines():
            match = newBranchPatern.match(line)
            if match is not None:
                self.newBranches.append(match.group(2))
                continue
            
            match = updatedBranchPatern.match(line)
            if match is not None:
                self.updatedBranches.append(match.group(2))
                continue
            
            match = deletedBranchPatern.match(line)
            if match is not None:
                self.deletedBranches.append(match.group(1))
                continue
        
    @staticmethod
    def _currentBranchPattern():
        return re.compile("\s*[*](.+)")
        
    def _parseBranches(self, text):
        currentBranchPatern = self._currentBranchPattern()
        headPatern = re.compile("origin/HEAD -> .*")
        branches = []
        for line in text.splitlines():
            if currentBranchPatern.match(line) is None and headPatern.match(line) is None:
                branches.append(line.strip())
            
        return branches
    
    def remoteBranches(self):
        return self._parseBranches(self._execute("git branch -r --no-color"))
    
    def localBranches(self):
        return self._parseBranches(self._execute("git branch --no-color"))
    
    def serialize(self, fileName="AtGit"):
        pickle.dump(self, open(fileName, "wb"))
        
    def localBranchExist(self, branchName):
        branches = self.localBranches()
        for branch in branches:
            if branch == branchName:
                return True
            
        return False
    
    def checkout(self, branchName):
        self.runCommand("git checkout %s" % branchName)
    
    def currentBranch(self):
        output = self._execute("git branch --no-color")
        currentBranchPatern = self._currentBranchPattern()
        for line in output.splitlines():
            match = currentBranchPatern.match(line)
            if match:
                branch = match.group(1)
                return branch
            
        return None
    
    @classmethod
    def load(cls, fileName = "AtGit"):
        return pickle.load(open(fileName, "rb"))
    
def selfTest():
    def printUsage():
        print "Usage: %s [--help] --gitpassword=<git password>" % sys.argv[0]
    
    try:
        opts, _ = getopt.getopt(sys.argv[1:],"",["help", "gitpassword="])
    except getopt.GetoptError:
        print ""
        sys.exit(1)
        
    gitPassword = None
        
    for opt, arg in opts:
        if opt == '-h':
            printUsage()
            sys.exit(2)
        elif opt == "--gitpassword":
            gitPassword = arg
        
    if gitPassword is None:
        printUsage()
        sys.exit(1)
        
    git = AtGit(gitPassword)
    print "%d remote branches" % len(git.remoteBranches())
    print "%d local branches" % len(git.localBranches())
    
    git.fetch()
    print "%d updated branches, %d deleted branches, %d new branches" % (len(git.updatedBranches), len(git.deletedBranches), len(git.newBranches))
    
    git.serialize()
    newGit = AtGit.load()
    print "%d updated branches, %d deleted branches, %d new branches" % (len(newGit.updatedBranches), len(newGit.deletedBranches), len(newGit.newBranches))
    
    branchName = "test/product/cisco"
    exist = git.localBranchExist(branchName)
    print "%s: %s" % (branchName, "exist" if exist else "not exist")
    
    branchName = "test/product/cisco_abc"
    exist = git.localBranchExist(branchName)
    print "%s: %s" % (branchName, "exist" if exist else "not exist")
    
if __name__ == '__main__':
    selfTest()

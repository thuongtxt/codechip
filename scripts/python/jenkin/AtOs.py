import os
import subprocess
import unittest
from threading import Thread

import sys

from python.arrive.AtAppManager.AtColor import AtColor

class AtOs(object):
    @classmethod
    def run(cls, command):
        print "Execute: %s" % command

        process = subprocess.Popen(command, stdout=sys.stdout, shell=True, executable="/bin/bash")
        process.communicate()
        result = process.returncode
        if result != 0:
            raise Exception("Run OS command: \"%s\" fail with return code: %d" % (command, result))
        
        return True
    
    @classmethod
    def asyncRun(cls, command, autoStart=True):
        try:
            from multiprocessing.process import Process
        except:
            AtColor.printColor(AtColor.RED, "Multi processing is not supported")
            return None
        
        class NewProcess(Process):
            def __init__(self, runFunction_, group=None, target=None, name=None, args=(), kwargs=None):
                if kwargs is None:
                    kwargs = {}
                Process.__init__(self, group=group, target=target, name=name, args=args, kwargs=kwargs)
                self.runFunction = runFunction_
                
            def run(self):
                self.runFunction()
                
        def runFunction():
            AtOs.run(command)
        
        process = NewProcess(runFunction)
        if autoStart:
            process.start()

        return process
        
    @classmethod
    def cd(cls, directory):
        os.chdir(directory)
        return True
    
    @classmethod
    def pwd(cls):
        return os.getcwd()
    
    @classmethod
    def createThreadWithFunction(cls, aFunction):
        class NewThread(Thread):
            def __init__(self, runFunction, group=None, target=None, name=None, 
                args=(), kwargs=None, verbose=None):
                if kwargs is None:
                    kwargs = list()
                Thread.__init__(self, group=group, target=target, name=name, args=args, kwargs=kwargs, verbose=verbose)
                self.runFunction = runFunction
                
            def run(self):
                self.runFunction()
                
        return NewThread(aFunction)
    
    @classmethod
    def deleteFile(cls, filepath):
        os.remove(filepath)

class AtOsTest(unittest.TestCase):
    @staticmethod
    def testCannotRunInvalidCommand():
        def func():
            AtOs.run("this command will be fail")
        unittest.case.expectedFailure(func)
    
    def testCanRunValidCommand(self):
        self.assertTrue(AtOs.run("ls -la"), None)
    
    @staticmethod
    def testCannotGotoInvalidDirectory():
        def func():
            AtOs.cd("this is invalid directory")
        unittest.case.expectedFailure(func)
        
    def testCanGotoValidDirectory(self):
        self.assertTrue(AtOs.cd("."), None)
    
    def testCanGetCurrentDirectory(self):
        self.assertIsNotNone(AtOs.pwd(), None)
        
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(AtOsTest)
    unittest.TextTestRunner(verbosity=2).run(suite)

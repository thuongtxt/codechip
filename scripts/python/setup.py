import os
import sys
import getopt

def printOnly():
    return False

def runOsCommand(command):
    if printOnly():
        print command
        return None
    
    os.system(command)

def install(installDir):
    pwd = os.getcwd()
    
    # First clean up everything
    runOsCommand("rm -fR " + installDir + "/*")
    runOsCommand("mkdir -p " + installDir)
    
    # Create library tree
    runOsCommand("mkdir " + installDir + "/libs")
    
    # Copy Python lib
    runOsCommand("tar -xjvf ./libs/python2.7.tar.gz --directory=" + installDir + "/libs/")
    
    # Copy pdev lib for remote debugging
    runOsCommand("tar -xjvf ./libs/pysrc.tar.gz --directory=" + installDir + "/libs/")
    
    # Copy Arrive lib
    runOsCommand("cp -fR arrive " + installDir + "/")
    
    # Copy document
    runOsCommand("cp -fR docs " + installDir + "/")
    runOsCommand("rm -fr " + installDir + "/docs/*.graphml " + installDir + "/docs/*.textile")

def printUsage():
    print "Usage: " + sys.argv[0] + " --help [--directory=<installDirectory>]"

def main():
    installDirectory = "/home/ATVN_Eng/af6xxx/atsdk/python/"
    
    # Have user inputs
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hd:",["help", "directory="])
    except getopt.GetoptError:
        printUsage()
        sys.exit(2)
        
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            printUsage()
            sys.exit()
        elif opt in ("-d", "--directory"):
            installDirectory = arg
    
    if installDirectory is None:
        printUsage()
        exit(2)
    
    install(installDirectory)
    
if __name__ == "__main__":
   main()

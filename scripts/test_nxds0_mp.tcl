source e1_common_configure.tcl

proc de1Id { } { return [startDe1Port] }
proc e1FrameMode { } { return e1_basic }
proc nxDs0List { } { return {1-3 4-6 7-9 10-11 12-13 14-15 16-17 18-19} } 

# Initialize
configureEp
initDevice

# Configure PDH
atsdk::pdh de1 framing [de1Id] [e1FrameMode]
atsdk::pdh de1 timing  [de1Id] system 1 ; # Note, the last parameter does not mater 

# Configure Ethernet port
ethPortSetDefault

# Create links
proc createNxDs0Link { linkId de1Id ds0List } {
	atsdk::pdh de1 nxds0create [de1Id] $ds0List
	atsdk::encap channel create $linkId ppp
	atsdk::encap channel bind   $linkId "nxds0.[de1Id].$ds0List"
}

# Create flows
proc createFlow { flowId } {
	atsdk::eth flow create $flowId eop
	atsdk::eth flow egress destmac $flowId "C0.CA.C0.CA.C0.CA"
	set vlan "0.0.$flowId"
}

proc setVlanForFlow { flowId } {
	set vlan "0.0.$flowId"
	atsdk::eth flow egress  vlan     $flowId [ethPort] $vlan none
	atsdk::eth flow ingress vlan add $flowId [ethPort] $vlan none
}

# Enable PPP link traffic
proc enablePppTrafficOnLink { linkId } {
	atsdk::encap hdlc link traffic txenable $linkId
	atsdk::encap hdlc link traffic rxenable $linkId
	atsdk::ppp link phase $linkId networkactive	
}

# Create links
set linkId 1
foreach nxDs0 [nxDs0List] {
	createNxDs0Link $linkId [de1Id] $nxDs0
	enablePppTrafficOnLink $linkId
	incr linkId
}

# Create two bundles
atsdk::ppp bundle create 1
atsdk::ppp bundle create 2

# Add these 8 links to bundle
atsdk::ppp bundle add link 1 1-4
atsdk::ppp bundle add link 2 5-8

# Create two flows for two bundles
createFlow 1
createFlow 2
setVlanForFlow 1
setVlanForFlow 2

# Add these flows to these bundles
atsdk::ppp bundle add flow 1 1 0
atsdk::ppp bundle add flow 2 2 0

proc showStatus {} {
	atsdk::show ppp bundle counters 1-2 r2c
	atsdk::show ppp link counters 1-8 r2c
	atsdk::show eth flow counters 1-8 r2c
}

# Clear status
after 1
showStatus

# Show status
after 1
showStatus

source e1_common_configure.tcl

# Changable
set numLinksPerBundle 8

configureEp
initDevice
setDefaultConfiguration

# Create bundles and add links for them
set numBundles [expr (([stopDe1Port]-[startDe1Port])+1)/$numLinksPerBundle]
for {set i 0} {$i < $numBundles} {incr i} {
	# Create bundle
	set bundleId [expr $i+1]
	atsdk::ppp bundle create $bundleId
	
	# Add links
	set startLink [expr ($i*$numLinksPerBundle)+[startDe1Port]]
	for {set j 0} {$j < $numLinksPerBundle} {incr j} {
		set linkId [expr $startLink+$j]
		atsdk::ppp bundle add link $bundleId $linkId
		atsdk::ppp link phase $linkId networkactive
	}
	
	# Add flow to this bundle
	set flowId $bundleId
	set classNumber 0
	atsdk::ppp bundle add flow $bundleId $flowId $classNumber
	
	# Enable traffic to flow on bundle
	atsdk::ppp bundle enable $bundleId
}

# Set VLANs for these flows
setVlansForAllFlows

# Show bundle counters
proc bundles {} {
	global numBundles
	if {$numBundles == 1} {
		return 1
	} else {
		return 1-$numBundles
	}
}

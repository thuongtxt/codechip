
atsdk::device init

atsdk::ptp device type boundary
atsdk::ptp device destmac ca.c0.cc.aa.bb.dd

atsdk::ptp correct mode standard
atsdk::ptp correct mode assistant

atsdk::show module ptp

atsdk::ptp port transport 1-17 ipv4
atsdk::ptp port state 1,10-17 master
atsdk::ptp port state 2-9 slave
atsdk::ptp port step mode 1-10 one-step
atsdk::ptp port step mode 11-17 two-step
atsdk::ptp port enable 1-17

atsdk::show ptp port 1-17
atsdk::show ptp port counters 1-17 r2c

# PSN
# Note: port #1 only support unicast address
atsdk::ptp port eth expect unicast dest 1 en ca.ca.da.da.0e.0f
atsdk::ptp port eth expect unicast source 1 en ca.ca.da.da.0f.0e
atsdk::ptp port eth expect anycast source 1 en
atsdk::show ptp port eth 1

atsdk::ptp port ipv4 expect unicast dest 1 1 en 172.33.32.11
atsdk::ptp port ipv4 expect unicast dest 1 2 en 172.33.32.12
atsdk::ptp port ipv4 expect unicast dest 1 3 en 172.33.32.13
atsdk::ptp port ipv4 expect unicast dest 1 4 en 172.33.32.14
atsdk::ptp port ipv4 expect unicast source 1 1 en 172.33.32.111
atsdk::ptp port ipv4 expect unicast source 1 2 en 172.33.32.121
atsdk::ptp port ipv4 expect unicast source 1 3 en 172.33.32.131
atsdk::ptp port ipv4 expect unicast source 1 4 en 172.33.32.141
atsdk::ptp port ipv4 expect anycast source 1 en
atsdk::show ptp port ipv4 1

# Faceplate
atsdk::ptp port eth expect unicast dest 2 en ca.ca.da.da.0e.0f
atsdk::ptp port eth expect multicast dest 2 en 01.ca.da.da.0e.0f
atsdk::ptp port eth expect anycast dest 2 en
atsdk::show ptp port eth 2

atsdk::ptp port ipv4 expect unicast dest 2 1 en 172.33.32.21
atsdk::ptp port ipv4 expect unicast dest 2 2 en 172.33.32.22
atsdk::ptp port ipv4 expect unicast dest 2 3 en 172.33.32.23
atsdk::ptp port ipv4 expect unicast dest 2 4 en 172.33.32.24
atsdk::ptp port ipv4 expect multicast dest 2 en 224.33.32.24
atsdk::ptp port ipv4 expect anycast dest 2 en
atsdk::show ptp port ipv4 2

# Multicast group
atsdk::ptp eth multicast group address 1 01.1b.19.00.00.00
atsdk::ptp eth multicast group address 2 01.80.c2.00.00.0e
atsdk::show ptp eth multicast group 1-2

atsdk::ptp ipv4 multicast group address 1 224.0.1.129
atsdk::ptp ipv4 multicast group address 2 224.0.0.107
atsdk::ptp ipv4 multicast group add 1 2-10
atsdk::ptp ipv4 multicast group remove 1 2-3
atsdk::show ptp ipv4 multicast group 1-2

atsdk::ptp ipv6 multicast group address 1 ff00::181
atsdk::ptp ipv6 multicast group address 2 ff02::6b
atsdk::show ptp ipv6 multicast group 1-2


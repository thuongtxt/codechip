source sdh_util.tcl

set lineId 1
set lineRate stm4
proc frameMode {} { return e1_unframed }

proc numAug1s {} {
	global lineRate
	return [numAug1sByRate $lineRate]
}

# Test mapping CLIs
proc setMappingForLine { lineId } {
	atsdk::sdh map "aug1.$lineId.1-$lineId.[numAug1s]" 3xvc3s
	atsdk::sdh map "vc3.$lineId.1.1-$lineId.[numAug1s].3" 7xtug2s
	atsdk::sdh map "tug2.$lineId.1.1.1-$lineId.[numAug1s].3.7" tu12
	atsdk::sdh map "vc1x.$lineId.1.1.1.1-$lineId.[numAug1s].3.7.3" de1
}

proc createAllLinks {lineId} {
	set de1s "$lineId.1.1.1.1-$lineId.[numAug1s].3.7.3"
	configureDe1 $de1s [frameMode]
	
	set numLinks [expr [numAug1s] * 3 * 21]
	set links "1-$numLinks"
	atsdk::encap channel create $links ppp
	atsdk::encap channel bind $links "de1.$de1s"
}

proc createOneLink {lineId} {
	set de1s "$lineId.1.1.1.1"
	configureDe1 $de1s [frameMode]
	
	set links "1"
	atsdk::encap channel create $links ppp
	atsdk::encap channel bind $links "de1.$de1s"
}

proc showStatus {lineId} {
	showLineStatus $lineId
	showPathStatus "vc3.$lineId.1.1-$lineId.[numAug1s].3" 
	showPathStatus "vc1x.$lineId.1.1.1.1-$lineId.[numAug1s].3.7.3"  
	
	# Show PDH
	showDe1Status "$lineId.1.1.1.1-$lineId.[numAug1s].3.7.3" 
}

# Test STM-1
#atsdk::wr f00010 0x108; # To temporary solve the CPU issue
atsdk::device init

# Temporary disable TIM function, hardware does not work
configureLine $lineId $lineRate
setMappingForLine $lineId
#createAllLinks $lineId
createOneLink $lineId

# Configure all paths
configurePath "vc3.$lineId.1.1-$lineId.[numAug1s].1"
configurePath "vc1x.$lineId.1.1.1.1-$lineId.[numAug1s].3.7.3"



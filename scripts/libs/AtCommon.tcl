proc alarmRaised { alarmStatus } {
	if {"\"$alarmStatus\"" == "Set" || "\"$alarmStatus\"" == "set"} { return 1 }
	
	return 0
}

proc tclListToString { tclList } {
	set idListString ""
	foreach value tclList {
	   if { [string bytelength $idListString] == 0 } {
		   set idListString $value
	   } else {
		   set idListString $idListString,$value
	   }
	}
	
	return $idListString
}

source libs/AtCommon.tcl

proc mpBundlesAreOk { bundleList } {
	atsdk::show ppp bundle counters $bundleList r2c

	set globalTable 0
	atsdk::table foreach $globalTable row columnName counterValue {
		# Good counters must exist
		if {$columnName == "txGoodByte"  && $counterValue == 0} {return 0}
		if {$columnName == "txGoodPkt"   && $counterValue == 0} {return 0}
		if {$columnName == "rxGoodBytes" && $counterValue == 0} {return 0}
		if {$columnName == "rxGoodPkt"   && $counterValue == 0} {return 0}
		
		# No error counter
		if {$columnName == "txDiscardPkt"      && $counterValue != 0} {return 0}
		if {$columnName == "rxQueueDiscardPkt" && $counterValue != 0} {return 0}
		if {$columnName == "rxDiscardFragment" && $counterValue != 0} {return 0}
	}
	
	return 1
}

# Get TCL list of bundle's links
proc getLinksOfBundle { bundleId } {
	atsdk::show ppp bundle links $bundleId
	
	set links {}
	set globalTable 0
	atsdk::table foreach $globalTable row col cellContent {
		if { $col == "LinkId" } {
			lappend links $cellContent
		}
	}
	
	return $links
}

proc removeAllLinksOfBundle { bundleId } {
	set links       = [getLinksOfBundle $bundleId]
	set linksString = [tclListToString $links]
	atsdk::ppp bundle remove link $bundleId $linksString
}

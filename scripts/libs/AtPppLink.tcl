proc pppLinksAreOk { links } {
	atsdk::show ppp link counters $links r2c
	
    set globalTable 0
	atsdk::table foreach $globalTable row columnName cellContent {
		# Good counter must exist
		if {$columnName == "txGoodBytes"  && $cellContent == 0} { return 0 }
		if {$columnName == "txGoodPkt"    && $cellContent == 0} { return 0 }
		if {$columnName == "rxGoodBytes"  && $cellContent == 0} { return 0 }
		if {$columnName == "rxGoodPkt"    && $cellContent == 0} { return 0 }
		                                                                             
		# No error counter                                                           
		if {$columnName == "txDiscardPkt" && $cellContent != 0} { return 0 }
		if {$columnName == "rxDiscardPkt" && $cellContent != 0} { return 0 }
	}
	
	return 1
}

proc pppLinkFlow {linkId} {
	atsdk::show encap hdlc link 1
	set globalTable 0
	atsdk::table cellContent $globalTable 0 "EthFlowId" flowId
	
	return $flowId
}

proc hdlcChannelsAreOk { hdlcChannels } {
	atsdk::show encap hdlc counters $hdlcChannels r2c

	set globalTable 0
	atsdk::table foreach $globalTable row columnName counterValue {
		# Good counters must exist
		if {$columnName == "txGoodPkt"        && $counterValue == 0} {return 0}
		if {$columnName == "rxGoodPkt"        && $counterValue == 0} {return 0}
		
		# No error counter
		if {$columnName == "txAbortPkt"       && $counterValue != 0} {return 0}
		if {$columnName == "rxAbortPkt"       && $counterValue != 0} {return 0}
		if {$columnName == "rxFcsErrPkt"      && $counterValue != 0} {return 0}
		if {$columnName == "rxAddrCtrlErrPkt" && $counterValue != 0} {return 0}
		if {$columnName == "rxSapiErrPkt"     && $counterValue != 0} {return 0}
		if {$columnName == "rxErrPkt"   	  && $counterValue != 0} {return 0}
	}
	
	return 1
}

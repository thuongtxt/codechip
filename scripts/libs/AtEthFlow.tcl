proc flowsAreOk { flowList } {
	atsdk::show eth flow counters $flowList r2c

	set globalTable 0
	atsdk::table foreach $globalTable row columnName counterValue {
		# Good counter must exist
		if {$columnName == "txGoodPkt" && $counterValue == 0} {return 0} 
		if {$columnName == "rxGoodPkt" && $counterValue == 0} {return 0}
		
		# No error counter
		if {$columnName == "mrruExceed"      && $counterValue != 0} {return 0}
		if {$columnName == "windowViolation" && $counterValue != 0} {return 0}
		if {$columnName == "rxDiscardPkt"    && $counterValue != 0} {return 0}
	}
	
	return 1
}

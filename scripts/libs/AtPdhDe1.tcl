source libs/AtCommon.tcl

proc de1sHaveNoAlarm { de1List } {
	atsdk::show pdh de1 alarm $de1List
	
	set globalTable 0
	atsdk::table foreach $globalTable row columnName alarmStatus {
		if {[alarmRaised $alarmStatus]} { return 0 }
	}
	
	return 1
}

proc de1sHaveNoErrorCounter { de1List } {
	atsdk::show pdh de1 counters $de1List r2c
	
	set globalTable 0
	atsdk::table foreach $globalTable row columnName errorCounter {
		if {$columnName == "DE1 ID"} { continue }
		if {$errorCounter > 0} { return 0 }
	}
	
	return 1
}

proc de1sAreOk { de1List } {
	if { [de1sHaveNoAlarm $de1List] && 
		 [de1sHaveNoErrorCounter $de1List] } { return 1 }
	
	return 0
}

source e1_ppp.tcl

# Libraries
source libs/AtEthPort.tcl
source libs/AtEthFlow.tcl
source libs/AtPppLink.tcl
source libs/AtHdlcChannel.tcl

proc noProblem {} {
	if {[pppLinksAreOk [links]] &&
		[flowsAreOk [flows]]    &&
		[hdlcChannelsAreOk [links]]} { return 1 }
	
	return 0
}

proc problem {} {
	if {[noProblem]} { return 0 }
	return 1
}

proc fail {} { puts "Testing fail" }

proc clearStatus {} {
	atsdk::show pdh de1 counters [de1Ports] r2c
	atsdk::show eth port counters [ethPort] r2c
	atsdk::show ppp link counters [links] r2c
	atsdk::show encap hdlc counters [links] r2c
}

# Check if traffic is OK
after 1
clearStatus
after 1
puts "Check channel status"
if {[problem]} {
	fail
	return
}

proc testFlowBinding {testingTimes, link1, link2} {
	flow1 = [pppLinkFlow $link1]
	flow2 = [pppLinkFlow $link2]
	
	for {set i 0} {$i < $testingTimes} {incr i} {
		# Unbind first
		atsdk::encap hdlc link flowbind $link1 none
		atsdk::encap hdlc link flowbind $link2 none
		
		# Swap flows
		if {[expr $i%2] == 0} {
			atsdk::encap hdlc link flowbind $link1 $flow2
			atsdk::encap hdlc link flowbind $link2 $flow1
		} else {
			atsdk::encap hdlc link flowbind $link1 $flow1
			atsdk::encap hdlc link flowbind $link2 $flow2
		}
		
		# No problem
		after 1
		clearStatus
		after 1
		if {[problem]} { return 0 }
	}
	
	return 1
}

# Test flow binding
set testingTimes 10
set portList {}
for {set i [startDe1Port]} {$i <= [stopDe1Port]} {incr i} {
	lappend $portList $i
}

foreach link1 $portList {
	foreach link2 $portList {
		if {$link1 == $link2} {continue}
		if {![testFlowBinding $testingTimes $link1 $link2]} {
			puts "Test flow binding between link $link1 and link $link2: FAIL"
			return
		}
	}
}

puts "PASS"

	atsdk::app polling enable
	atsdk::encap hdlc link oammode 1 tocpu
	atsdk::ppp link oamcapture 1 en
	

	for {set i 0} {$i < 1000} {incr i} {
		atsdk::ipcp send ipaddress 1 172.33.34.35
		atsdk::lcp send mru 1 1500
		}
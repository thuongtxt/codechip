set lineId 1
proc frameMode    {} { return e1_unframed }

# Test Line CLIs
proc testCliForLine { lineId rate } {
	atsdk::sdh line rate              $lineId $rate
	atsdk::sdh line mode              $lineId sdh
	atsdk::sdh line k1                $lineId 0x1
	atsdk::sdh line k2                $lineId 0x2
	atsdk::sdh line s1                $lineId 0x3
	atsdk::sdh line loopback          $lineId local
	atsdk::sdh line loopback          $lineId remote
	atsdk::sdh line loopback          $lineId release
	atsdk::sdh line tti transmit      $lineId 64bytes 0123456789_0123456789_0123456789 space_padding
	atsdk::sdh line tti expect        $lineId 64bytes 0123456789_0123456789_0123456789 space_padding
	atsdk::show sdh line 	          $lineId
	atsdk::sdh line tti transmit      $lineId 16bytes Hello space_padding
	atsdk::sdh line tti expect        $lineId 16bytes Hello space_padding
	atsdk::show sdh line 	          $lineId
	atsdk::sdh line tti transmit      $lineId 1byte a space_padding
	atsdk::sdh line tti expect        $lineId 1byte a space_padding
	atsdk::show sdh line 	          $lineId
	atsdk::show sdh line tti receive  $lineId 
	atsdk::show sdh line 	          $lineId
	atsdk::show sdh line alarm        $lineId
	atsdk::show sdh line interrupt    $lineId r2c
	atsdk::show sdh line interrupt    $lineId ro
	atsdk::show sdh line counters     $lineId r2c
	atsdk::show sdh line counters     $lineId ro
}

# Test mapping CLIs
proc testMappingForLine { lineId } {
	atsdk::sdh map "aug1.$lineId.1" vc4
	atsdk::show sdh map "aug1.$lineId.1"
	atsdk::sdh map "vc4.$lineId.1" 3xtug3s
	atsdk::show sdh map "vc4.$lineId.1"
	atsdk::sdh map "tug3.$lineId.1.1-$lineId.1.3" 7xtug2s
	atsdk::show sdh map "tug3.$lineId.1.1-$lineId.1.3"
	atsdk::sdh map "tug2.$lineId.1.1.1-$lineId.1.3.7" tu12
	atsdk::show sdh map "tug2.$lineId.1.1.1-$lineId.1.3.7"
	atsdk::sdh map "vc1x.$lineId.1.1.1.1-$lineId.1.3.7.3" de1
	atsdk::show sdh map "vc1x.$lineId.1.1.1.1-$lineId.1.3.7.3"
} 

proc testCliForPath { pathId } {
	atsdk::sdh path psl transmit     $pathId 0x2
	atsdk::sdh path psl expect       $pathId 0x2
	atsdk::sdh path tti transmit     $pathId 16bytes Hello space_padding
	atsdk::sdh path tti expect       $pathId 16bytes Hello space_padding
	atsdk::show sdh path 	         $pathId
	atsdk::sdh path tti transmit     $pathId 64bytes 0123456789_0123456789_0123456789 space_padding
	atsdk::sdh path tti expect       $pathId 64bytes 0123456789_0123456789_0123456789 space_padding
	atsdk::show sdh path 	         $pathId
	atsdk::sdh path tti transmit     $pathId 1byte a space_padding
	atsdk::sdh path tti expect       $pathId 1byte a space_padding
	atsdk::show sdh path 	         $pathId
	atsdk::show sdh path tti receive $pathId
	atsdk::show sdh path 	         $pathId
	atsdk::show sdh path alarm       $pathId
	atsdk::show sdh path interrupt   $pathId r2c
	atsdk::show sdh path interrupt   $pathId ro
	atsdk::show sdh path counters    $pathId r2c
	atsdk::show sdh path counters    $pathId ro
}

proc configureDe1 {de1 frameMode} {
	atsdk::pdh de1 framing $de1 $frameMode
	atsdk::pdh de1 timing  $de1 system 1 ; # Note, the last parameter does not mater
}

proc createAllLinks {lineId} {
	set linkId 1
	for {set tug3Id 1} {$tug3Id <= 3} {incr tug3Id} {
		for {set tug2Id 1} {$tug2Id <= 7} {incr tug2Id} {
			for {set tuId 1} {$tuId <= 3} {incr tuId} {
				configureDe1 "$lineId.1.$tug3Id.$tug2Id.$tuId" [frameMode]
				atsdk::encap channel create $linkId ppp
				atsdk::encap channel bind   $linkId "de1.$lineId.1.$tug3Id.$tug2Id.$tuId"
				incr linkId
			}
		}
	}
}

# Test STM-1
atsdk::device init

# Hardware has problem on DDR that causes Jn messages all wrong. Disable Ethernet traffic
# to so that Jn can be tested
atsdk::wr 0x4022E 0

# Temporary disable TIM function, hardware does not work
#atsdk::wr 0x0004022E 0x0
testCliForLine $lineId stm1
testMappingForLine $lineId
testCliForPath "vc4.$lineId.1"
createAllLinks $lineId

#testCliForPath "au4.$lineId.1"
#
## Test all VC-12 paths
#for {set tug3Id 1} {$tug3Id <= 3} {incr tug3Id} {
#	for {set tug2Id 1} {$tug2Id <= 7} {incr tug2Id} {
#		for {set tuId 1} {$tuId <= 3} {incr tuId} {
#			set pathId "vc1x.$lineId.1.$tug3Id.$tug2Id.$tuId"
#			testCliForPath $pathId
#		}
#	}
#}
atsdk::device show readwrite dis dis
atsdk::device init

atsdk::sdh line rate 1 stm1
atsdk::sdh line mode 1 sdh
atsdk::sdh line loopback 1 release
atsdk::sdh line tti transmit 1 16bytes a space_padding
atsdk::sdh line tti expect 1 16bytes a space_padding
atsdk::sdh map aug1.1.1-1.1 vc4
atsdk::sdh map vc4.1.1-1.1 3xtug3s
atsdk::sdh map tug3.1.1.1-1.1.3 7xtug2s
atsdk::sdh map tug2.1.1.1.1-1.1.3.7 tu12
atsdk::sdh map vc1x.1.1.1.1.1-1.1.3.7.3 de1
atsdk::sdh path tti transmit vc4.1.1-1.1 64bytes None space_padding
atsdk::sdh path tti expect vc4.1.1-1.1 64bytes None space_padding
atsdk::sdh path psl expect vc4.1.1-1.1 0x2
atsdk::sdh path psl transmit vc4.1.1-1.1 0x2
atsdk::sdh path tti transmit vc1x.1.1.1.1.1-1.1.3.7.3 64bytes None space_padding
atsdk::sdh path tti expect vc1x.1.1.1.1.1-1.1.3.7.3 64bytes None space_padding
atsdk::sdh path psl expect vc1x.1.1.1.1.1-1.1.3.7.3 0x2
atsdk::sdh path psl transmit vc1x.1.1.1.1.1-1.1.3.7.3 0x2
atsdk::pdh de1 framing 1.1.1.1.1-1.1.3.7.3 e1_unframed
atsdk::pdh de1 timing 1.1.1.1.1-1.1.3.7.3 system 1
atsdk::encap channel create 1-63 ppp
atsdk::encap channel bind 1-63 de1.1.1.1.1.1-1.1.3.7.3

atsdk::sdh line rate 2 stm1
atsdk::sdh line mode 2 sdh
atsdk::sdh line loopback 2 release
atsdk::sdh line tti transmit 2 16bytes a space_padding
atsdk::sdh line tti expect 2 16bytes a space_padding
atsdk::sdh map aug1.2.1-2.1 vc4
atsdk::sdh map vc4.2.1-2.1 3xtug3s
atsdk::sdh map tug3.2.1.1-2.1.3 7xtug2s
atsdk::sdh map tug2.2.1.1.1-2.1.3.7 tu12
atsdk::sdh map vc1x.2.1.1.1.1-2.1.3.7.3 de1
atsdk::sdh path tti transmit vc4.2.1-2.1 64bytes None space_padding
atsdk::sdh path tti expect vc4.2.1-2.1 64bytes None space_padding
atsdk::sdh path psl expect vc4.2.1-2.1 0x2
atsdk::sdh path psl transmit vc4.2.1-2.1 0x2
atsdk::sdh path tti transmit vc1x.2.1.1.1.1-2.1.3.7.3 64bytes None space_padding
atsdk::sdh path tti expect vc1x.2.1.1.1.1-2.1.3.7.3 64bytes None space_padding
atsdk::sdh path psl expect vc1x.2.1.1.1.1-2.1.3.7.3 0x2
atsdk::sdh path psl transmit vc1x.2.1.1.1.1-2.1.3.7.3 0x2
atsdk::pdh de1 framing 2.1.1.1.1-2.1.3.7.3 e1_unframed
atsdk::pdh de1 timing 2.1.1.1.1-2.1.3.7.3 system 1
atsdk::encap channel create 64-126 ppp
atsdk::encap channel bind 64-126 de1.2.1.1.1.1-2.1.3.7.3


atsdk::eth flow create 1 nop
atsdk::encap hdlc link flowbind 1 1
atsdk::encap hdlc link oammode 1 topsn
atsdk::encap hdlc link oamegress destmac 1 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 1 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 1 networkactive
atsdk::encap hdlc link traffic txenable 1
atsdk::encap hdlc link traffic rxenable 1
atsdk::eth flow egress destmac 1 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 1 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 1 1 0.0.1.0.0.0
atsdk::bdcom eth flow header transmit 1 1 0.0.1.0.0.0


atsdk::eth flow create 2 nop
atsdk::encap hdlc link flowbind 2 2
atsdk::encap hdlc link oammode 2 topsn
atsdk::encap hdlc link oamegress destmac 2 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 2 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 2 networkactive
atsdk::encap hdlc link traffic txenable 2
atsdk::encap hdlc link traffic rxenable 2
atsdk::eth flow egress destmac 2 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 2 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 2 1 0.0.1.0.1.0
atsdk::bdcom eth flow header transmit 2 1 0.0.1.0.1.0


atsdk::eth flow create 3 nop
atsdk::encap hdlc link flowbind 3 3
atsdk::encap hdlc link oammode 3 topsn
atsdk::encap hdlc link oamegress destmac 3 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 3 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 3 networkactive
atsdk::encap hdlc link traffic txenable 3
atsdk::encap hdlc link traffic rxenable 3
atsdk::eth flow egress destmac 3 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 3 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 3 1 0.0.1.0.2.0
atsdk::bdcom eth flow header transmit 3 1 0.0.1.0.2.0


atsdk::eth flow create 4 nop
atsdk::encap hdlc link flowbind 4 4
atsdk::encap hdlc link oammode 4 topsn
atsdk::encap hdlc link oamegress destmac 4 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 4 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 4 networkactive
atsdk::encap hdlc link traffic txenable 4
atsdk::encap hdlc link traffic rxenable 4
atsdk::eth flow egress destmac 4 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 4 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 4 1 0.0.1.0.3.0
atsdk::bdcom eth flow header transmit 4 1 0.0.1.0.3.0


atsdk::eth flow create 5 nop
atsdk::encap hdlc link flowbind 5 5
atsdk::encap hdlc link oammode 5 topsn
atsdk::encap hdlc link oamegress destmac 5 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 5 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 5 networkactive
atsdk::encap hdlc link traffic txenable 5
atsdk::encap hdlc link traffic rxenable 5
atsdk::eth flow egress destmac 5 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 5 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 5 1 0.0.1.0.4.0
atsdk::bdcom eth flow header transmit 5 1 0.0.1.0.4.0


atsdk::eth flow create 6 nop
atsdk::encap hdlc link flowbind 6 6
atsdk::encap hdlc link oammode 6 topsn
atsdk::encap hdlc link oamegress destmac 6 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 6 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 6 networkactive
atsdk::encap hdlc link traffic txenable 6
atsdk::encap hdlc link traffic rxenable 6
atsdk::eth flow egress destmac 6 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 6 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 6 1 0.0.1.0.5.0
atsdk::bdcom eth flow header transmit 6 1 0.0.1.0.5.0


atsdk::eth flow create 7 nop
atsdk::encap hdlc link flowbind 7 7
atsdk::encap hdlc link oammode 7 topsn
atsdk::encap hdlc link oamegress destmac 7 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 7 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 7 networkactive
atsdk::encap hdlc link traffic txenable 7
atsdk::encap hdlc link traffic rxenable 7
atsdk::eth flow egress destmac 7 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 7 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 7 1 0.0.1.0.6.0
atsdk::bdcom eth flow header transmit 7 1 0.0.1.0.6.0


atsdk::eth flow create 8 nop
atsdk::encap hdlc link flowbind 8 8
atsdk::encap hdlc link oammode 8 topsn
atsdk::encap hdlc link oamegress destmac 8 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 8 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 8 networkactive
atsdk::encap hdlc link traffic txenable 8
atsdk::encap hdlc link traffic rxenable 8
atsdk::eth flow egress destmac 8 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 8 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 8 1 0.0.1.0.7.0
atsdk::bdcom eth flow header transmit 8 1 0.0.1.0.7.0


atsdk::eth flow create 9 nop
atsdk::encap hdlc link flowbind 9 9
atsdk::encap hdlc link oammode 9 topsn
atsdk::encap hdlc link oamegress destmac 9 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 9 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 9 networkactive
atsdk::encap hdlc link traffic txenable 9
atsdk::encap hdlc link traffic rxenable 9
atsdk::eth flow egress destmac 9 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 9 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 9 1 0.0.1.0.8.0
atsdk::bdcom eth flow header transmit 9 1 0.0.1.0.8.0


atsdk::eth flow create 10 nop
atsdk::encap hdlc link flowbind 10 10
atsdk::encap hdlc link oammode 10 topsn
atsdk::encap hdlc link oamegress destmac 10 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 10 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 10 networkactive
atsdk::encap hdlc link traffic txenable 10
atsdk::encap hdlc link traffic rxenable 10
atsdk::eth flow egress destmac 10 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 10 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 10 1 0.0.1.0.9.0
atsdk::bdcom eth flow header transmit 10 1 0.0.1.0.9.0


atsdk::eth flow create 11 nop
atsdk::encap hdlc link flowbind 11 11
atsdk::encap hdlc link oammode 11 topsn
atsdk::encap hdlc link oamegress destmac 11 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 11 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 11 networkactive
atsdk::encap hdlc link traffic txenable 11
atsdk::encap hdlc link traffic rxenable 11
atsdk::eth flow egress destmac 11 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 11 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 11 1 0.0.1.0.10.0
atsdk::bdcom eth flow header transmit 11 1 0.0.1.0.10.0


atsdk::eth flow create 12 nop
atsdk::encap hdlc link flowbind 12 12
atsdk::encap hdlc link oammode 12 topsn
atsdk::encap hdlc link oamegress destmac 12 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 12 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 12 networkactive
atsdk::encap hdlc link traffic txenable 12
atsdk::encap hdlc link traffic rxenable 12
atsdk::eth flow egress destmac 12 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 12 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 12 1 0.0.1.0.11.0
atsdk::bdcom eth flow header transmit 12 1 0.0.1.0.11.0


atsdk::eth flow create 13 nop
atsdk::encap hdlc link flowbind 13 13
atsdk::encap hdlc link oammode 13 topsn
atsdk::encap hdlc link oamegress destmac 13 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 13 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 13 networkactive
atsdk::encap hdlc link traffic txenable 13
atsdk::encap hdlc link traffic rxenable 13
atsdk::eth flow egress destmac 13 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 13 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 13 1 0.0.1.0.12.0
atsdk::bdcom eth flow header transmit 13 1 0.0.1.0.12.0


atsdk::eth flow create 14 nop
atsdk::encap hdlc link flowbind 14 14
atsdk::encap hdlc link oammode 14 topsn
atsdk::encap hdlc link oamegress destmac 14 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 14 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 14 networkactive
atsdk::encap hdlc link traffic txenable 14
atsdk::encap hdlc link traffic rxenable 14
atsdk::eth flow egress destmac 14 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 14 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 14 1 0.0.1.0.13.0
atsdk::bdcom eth flow header transmit 14 1 0.0.1.0.13.0


atsdk::eth flow create 15 nop
atsdk::encap hdlc link flowbind 15 15
atsdk::encap hdlc link oammode 15 topsn
atsdk::encap hdlc link oamegress destmac 15 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 15 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 15 networkactive
atsdk::encap hdlc link traffic txenable 15
atsdk::encap hdlc link traffic rxenable 15
atsdk::eth flow egress destmac 15 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 15 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 15 1 0.0.1.0.14.0
atsdk::bdcom eth flow header transmit 15 1 0.0.1.0.14.0


atsdk::eth flow create 16 nop
atsdk::encap hdlc link flowbind 16 16
atsdk::encap hdlc link oammode 16 topsn
atsdk::encap hdlc link oamegress destmac 16 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 16 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 16 networkactive
atsdk::encap hdlc link traffic txenable 16
atsdk::encap hdlc link traffic rxenable 16
atsdk::eth flow egress destmac 16 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 16 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 16 1 0.0.1.0.15.0
atsdk::bdcom eth flow header transmit 16 1 0.0.1.0.15.0


atsdk::eth flow create 17 nop
atsdk::encap hdlc link flowbind 17 17
atsdk::encap hdlc link oammode 17 topsn
atsdk::encap hdlc link oamegress destmac 17 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 17 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 17 networkactive
atsdk::encap hdlc link traffic txenable 17
atsdk::encap hdlc link traffic rxenable 17
atsdk::eth flow egress destmac 17 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 17 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 17 1 0.0.1.0.16.0
atsdk::bdcom eth flow header transmit 17 1 0.0.1.0.16.0


atsdk::eth flow create 18 nop
atsdk::encap hdlc link flowbind 18 18
atsdk::encap hdlc link oammode 18 topsn
atsdk::encap hdlc link oamegress destmac 18 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 18 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 18 networkactive
atsdk::encap hdlc link traffic txenable 18
atsdk::encap hdlc link traffic rxenable 18
atsdk::eth flow egress destmac 18 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 18 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 18 1 0.0.1.0.17.0
atsdk::bdcom eth flow header transmit 18 1 0.0.1.0.17.0


atsdk::eth flow create 19 nop
atsdk::encap hdlc link flowbind 19 19
atsdk::encap hdlc link oammode 19 topsn
atsdk::encap hdlc link oamegress destmac 19 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 19 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 19 networkactive
atsdk::encap hdlc link traffic txenable 19
atsdk::encap hdlc link traffic rxenable 19
atsdk::eth flow egress destmac 19 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 19 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 19 1 0.0.1.0.18.0
atsdk::bdcom eth flow header transmit 19 1 0.0.1.0.18.0


atsdk::eth flow create 20 nop
atsdk::encap hdlc link flowbind 20 20
atsdk::encap hdlc link oammode 20 topsn
atsdk::encap hdlc link oamegress destmac 20 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 20 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 20 networkactive
atsdk::encap hdlc link traffic txenable 20
atsdk::encap hdlc link traffic rxenable 20
atsdk::eth flow egress destmac 20 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 20 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 20 1 0.0.1.0.19.0
atsdk::bdcom eth flow header transmit 20 1 0.0.1.0.19.0


atsdk::eth flow create 21 nop
atsdk::encap hdlc link flowbind 21 21
atsdk::encap hdlc link oammode 21 topsn
atsdk::encap hdlc link oamegress destmac 21 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 21 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 21 networkactive
atsdk::encap hdlc link traffic txenable 21
atsdk::encap hdlc link traffic rxenable 21
atsdk::eth flow egress destmac 21 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 21 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 21 1 0.0.1.0.20.0
atsdk::bdcom eth flow header transmit 21 1 0.0.1.0.20.0


atsdk::eth flow create 22 nop
atsdk::encap hdlc link flowbind 22 22
atsdk::encap hdlc link oammode 22 topsn
atsdk::encap hdlc link oamegress destmac 22 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 22 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 22 networkactive
atsdk::encap hdlc link traffic txenable 22
atsdk::encap hdlc link traffic rxenable 22
atsdk::eth flow egress destmac 22 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 22 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 22 1 0.0.1.0.21.0
atsdk::bdcom eth flow header transmit 22 1 0.0.1.0.21.0


atsdk::eth flow create 23 nop
atsdk::encap hdlc link flowbind 23 23
atsdk::encap hdlc link oammode 23 topsn
atsdk::encap hdlc link oamegress destmac 23 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 23 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 23 networkactive
atsdk::encap hdlc link traffic txenable 23
atsdk::encap hdlc link traffic rxenable 23
atsdk::eth flow egress destmac 23 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 23 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 23 1 0.0.1.0.22.0
atsdk::bdcom eth flow header transmit 23 1 0.0.1.0.22.0


atsdk::eth flow create 24 nop
atsdk::encap hdlc link flowbind 24 24
atsdk::encap hdlc link oammode 24 topsn
atsdk::encap hdlc link oamegress destmac 24 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 24 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 24 networkactive
atsdk::encap hdlc link traffic txenable 24
atsdk::encap hdlc link traffic rxenable 24
atsdk::eth flow egress destmac 24 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 24 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 24 1 0.0.1.0.23.0
atsdk::bdcom eth flow header transmit 24 1 0.0.1.0.23.0


atsdk::eth flow create 25 nop
atsdk::encap hdlc link flowbind 25 25
atsdk::encap hdlc link oammode 25 topsn
atsdk::encap hdlc link oamegress destmac 25 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 25 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 25 networkactive
atsdk::encap hdlc link traffic txenable 25
atsdk::encap hdlc link traffic rxenable 25
atsdk::eth flow egress destmac 25 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 25 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 25 1 0.0.1.0.24.0
atsdk::bdcom eth flow header transmit 25 1 0.0.1.0.24.0


atsdk::eth flow create 26 nop
atsdk::encap hdlc link flowbind 26 26
atsdk::encap hdlc link oammode 26 topsn
atsdk::encap hdlc link oamegress destmac 26 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 26 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 26 networkactive
atsdk::encap hdlc link traffic txenable 26
atsdk::encap hdlc link traffic rxenable 26
atsdk::eth flow egress destmac 26 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 26 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 26 1 0.0.1.0.25.0
atsdk::bdcom eth flow header transmit 26 1 0.0.1.0.25.0


atsdk::eth flow create 27 nop
atsdk::encap hdlc link flowbind 27 27
atsdk::encap hdlc link oammode 27 topsn
atsdk::encap hdlc link oamegress destmac 27 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 27 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 27 networkactive
atsdk::encap hdlc link traffic txenable 27
atsdk::encap hdlc link traffic rxenable 27
atsdk::eth flow egress destmac 27 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 27 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 27 1 0.0.1.0.26.0
atsdk::bdcom eth flow header transmit 27 1 0.0.1.0.26.0


atsdk::eth flow create 28 nop
atsdk::encap hdlc link flowbind 28 28
atsdk::encap hdlc link oammode 28 topsn
atsdk::encap hdlc link oamegress destmac 28 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 28 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 28 networkactive
atsdk::encap hdlc link traffic txenable 28
atsdk::encap hdlc link traffic rxenable 28
atsdk::eth flow egress destmac 28 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 28 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 28 1 0.0.1.0.27.0
atsdk::bdcom eth flow header transmit 28 1 0.0.1.0.27.0


atsdk::eth flow create 29 nop
atsdk::encap hdlc link flowbind 29 29
atsdk::encap hdlc link oammode 29 topsn
atsdk::encap hdlc link oamegress destmac 29 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 29 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 29 networkactive
atsdk::encap hdlc link traffic txenable 29
atsdk::encap hdlc link traffic rxenable 29
atsdk::eth flow egress destmac 29 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 29 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 29 1 0.0.1.0.28.0
atsdk::bdcom eth flow header transmit 29 1 0.0.1.0.28.0


atsdk::eth flow create 30 nop
atsdk::encap hdlc link flowbind 30 30
atsdk::encap hdlc link oammode 30 topsn
atsdk::encap hdlc link oamegress destmac 30 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 30 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 30 networkactive
atsdk::encap hdlc link traffic txenable 30
atsdk::encap hdlc link traffic rxenable 30
atsdk::eth flow egress destmac 30 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 30 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 30 1 0.0.1.0.29.0
atsdk::bdcom eth flow header transmit 30 1 0.0.1.0.29.0


atsdk::eth flow create 31 nop
atsdk::encap hdlc link flowbind 31 31
atsdk::encap hdlc link oammode 31 topsn
atsdk::encap hdlc link oamegress destmac 31 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 31 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 31 networkactive
atsdk::encap hdlc link traffic txenable 31
atsdk::encap hdlc link traffic rxenable 31
atsdk::eth flow egress destmac 31 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 31 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 31 1 0.0.1.0.30.0
atsdk::bdcom eth flow header transmit 31 1 0.0.1.0.30.0


atsdk::eth flow create 32 nop
atsdk::encap hdlc link flowbind 32 32
atsdk::encap hdlc link oammode 32 topsn
atsdk::encap hdlc link oamegress destmac 32 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 32 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 32 networkactive
atsdk::encap hdlc link traffic txenable 32
atsdk::encap hdlc link traffic rxenable 32
atsdk::eth flow egress destmac 32 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 32 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 32 1 0.0.1.0.31.0
atsdk::bdcom eth flow header transmit 32 1 0.0.1.0.31.0


atsdk::eth flow create 33 nop
atsdk::encap hdlc link flowbind 33 33
atsdk::encap hdlc link oammode 33 topsn
atsdk::encap hdlc link oamegress destmac 33 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 33 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 33 networkactive
atsdk::encap hdlc link traffic txenable 33
atsdk::encap hdlc link traffic rxenable 33
atsdk::eth flow egress destmac 33 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 33 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 33 1 0.0.1.0.32.0
atsdk::bdcom eth flow header transmit 33 1 0.0.1.0.32.0


atsdk::eth flow create 34 nop
atsdk::encap hdlc link flowbind 34 34
atsdk::encap hdlc link oammode 34 topsn
atsdk::encap hdlc link oamegress destmac 34 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 34 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 34 networkactive
atsdk::encap hdlc link traffic txenable 34
atsdk::encap hdlc link traffic rxenable 34
atsdk::eth flow egress destmac 34 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 34 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 34 1 0.0.1.0.33.0
atsdk::bdcom eth flow header transmit 34 1 0.0.1.0.33.0


atsdk::eth flow create 35 nop
atsdk::encap hdlc link flowbind 35 35
atsdk::encap hdlc link oammode 35 topsn
atsdk::encap hdlc link oamegress destmac 35 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 35 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 35 networkactive
atsdk::encap hdlc link traffic txenable 35
atsdk::encap hdlc link traffic rxenable 35
atsdk::eth flow egress destmac 35 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 35 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 35 1 0.0.1.0.34.0
atsdk::bdcom eth flow header transmit 35 1 0.0.1.0.34.0


atsdk::eth flow create 36 nop
atsdk::encap hdlc link flowbind 36 36
atsdk::encap hdlc link oammode 36 topsn
atsdk::encap hdlc link oamegress destmac 36 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 36 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 36 networkactive
atsdk::encap hdlc link traffic txenable 36
atsdk::encap hdlc link traffic rxenable 36
atsdk::eth flow egress destmac 36 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 36 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 36 1 0.0.1.0.35.0
atsdk::bdcom eth flow header transmit 36 1 0.0.1.0.35.0


atsdk::eth flow create 37 nop
atsdk::encap hdlc link flowbind 37 37
atsdk::encap hdlc link oammode 37 topsn
atsdk::encap hdlc link oamegress destmac 37 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 37 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 37 networkactive
atsdk::encap hdlc link traffic txenable 37
atsdk::encap hdlc link traffic rxenable 37
atsdk::eth flow egress destmac 37 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 37 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 37 1 0.0.1.0.36.0
atsdk::bdcom eth flow header transmit 37 1 0.0.1.0.36.0


atsdk::eth flow create 38 nop
atsdk::encap hdlc link flowbind 38 38
atsdk::encap hdlc link oammode 38 topsn
atsdk::encap hdlc link oamegress destmac 38 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 38 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 38 networkactive
atsdk::encap hdlc link traffic txenable 38
atsdk::encap hdlc link traffic rxenable 38
atsdk::eth flow egress destmac 38 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 38 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 38 1 0.0.1.0.37.0
atsdk::bdcom eth flow header transmit 38 1 0.0.1.0.37.0


atsdk::eth flow create 39 nop
atsdk::encap hdlc link flowbind 39 39
atsdk::encap hdlc link oammode 39 topsn
atsdk::encap hdlc link oamegress destmac 39 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 39 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 39 networkactive
atsdk::encap hdlc link traffic txenable 39
atsdk::encap hdlc link traffic rxenable 39
atsdk::eth flow egress destmac 39 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 39 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 39 1 0.0.1.0.38.0
atsdk::bdcom eth flow header transmit 39 1 0.0.1.0.38.0


atsdk::eth flow create 40 nop
atsdk::encap hdlc link flowbind 40 40
atsdk::encap hdlc link oammode 40 topsn
atsdk::encap hdlc link oamegress destmac 40 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 40 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 40 networkactive
atsdk::encap hdlc link traffic txenable 40
atsdk::encap hdlc link traffic rxenable 40
atsdk::eth flow egress destmac 40 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 40 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 40 1 0.0.1.0.39.0
atsdk::bdcom eth flow header transmit 40 1 0.0.1.0.39.0


atsdk::eth flow create 41 nop
atsdk::encap hdlc link flowbind 41 41
atsdk::encap hdlc link oammode 41 topsn
atsdk::encap hdlc link oamegress destmac 41 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 41 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 41 networkactive
atsdk::encap hdlc link traffic txenable 41
atsdk::encap hdlc link traffic rxenable 41
atsdk::eth flow egress destmac 41 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 41 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 41 1 0.0.1.0.40.0
atsdk::bdcom eth flow header transmit 41 1 0.0.1.0.40.0


atsdk::eth flow create 42 nop
atsdk::encap hdlc link flowbind 42 42
atsdk::encap hdlc link oammode 42 topsn
atsdk::encap hdlc link oamegress destmac 42 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 42 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 42 networkactive
atsdk::encap hdlc link traffic txenable 42
atsdk::encap hdlc link traffic rxenable 42
atsdk::eth flow egress destmac 42 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 42 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 42 1 0.0.1.0.41.0
atsdk::bdcom eth flow header transmit 42 1 0.0.1.0.41.0


atsdk::eth flow create 43 nop
atsdk::encap hdlc link flowbind 43 43
atsdk::encap hdlc link oammode 43 topsn
atsdk::encap hdlc link oamegress destmac 43 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 43 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 43 networkactive
atsdk::encap hdlc link traffic txenable 43
atsdk::encap hdlc link traffic rxenable 43
atsdk::eth flow egress destmac 43 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 43 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 43 1 0.0.1.0.42.0
atsdk::bdcom eth flow header transmit 43 1 0.0.1.0.42.0


atsdk::eth flow create 44 nop
atsdk::encap hdlc link flowbind 44 44
atsdk::encap hdlc link oammode 44 topsn
atsdk::encap hdlc link oamegress destmac 44 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 44 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 44 networkactive
atsdk::encap hdlc link traffic txenable 44
atsdk::encap hdlc link traffic rxenable 44
atsdk::eth flow egress destmac 44 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 44 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 44 1 0.0.1.0.43.0
atsdk::bdcom eth flow header transmit 44 1 0.0.1.0.43.0


atsdk::eth flow create 45 nop
atsdk::encap hdlc link flowbind 45 45
atsdk::encap hdlc link oammode 45 topsn
atsdk::encap hdlc link oamegress destmac 45 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 45 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 45 networkactive
atsdk::encap hdlc link traffic txenable 45
atsdk::encap hdlc link traffic rxenable 45
atsdk::eth flow egress destmac 45 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 45 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 45 1 0.0.1.0.44.0
atsdk::bdcom eth flow header transmit 45 1 0.0.1.0.44.0


atsdk::eth flow create 46 nop
atsdk::encap hdlc link flowbind 46 46
atsdk::encap hdlc link oammode 46 topsn
atsdk::encap hdlc link oamegress destmac 46 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 46 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 46 networkactive
atsdk::encap hdlc link traffic txenable 46
atsdk::encap hdlc link traffic rxenable 46
atsdk::eth flow egress destmac 46 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 46 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 46 1 0.0.1.0.45.0
atsdk::bdcom eth flow header transmit 46 1 0.0.1.0.45.0


atsdk::eth flow create 47 nop
atsdk::encap hdlc link flowbind 47 47
atsdk::encap hdlc link oammode 47 topsn
atsdk::encap hdlc link oamegress destmac 47 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 47 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 47 networkactive
atsdk::encap hdlc link traffic txenable 47
atsdk::encap hdlc link traffic rxenable 47
atsdk::eth flow egress destmac 47 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 47 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 47 1 0.0.1.0.46.0
atsdk::bdcom eth flow header transmit 47 1 0.0.1.0.46.0


atsdk::eth flow create 48 nop
atsdk::encap hdlc link flowbind 48 48
atsdk::encap hdlc link oammode 48 topsn
atsdk::encap hdlc link oamegress destmac 48 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 48 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 48 networkactive
atsdk::encap hdlc link traffic txenable 48
atsdk::encap hdlc link traffic rxenable 48
atsdk::eth flow egress destmac 48 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 48 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 48 1 0.0.1.0.47.0
atsdk::bdcom eth flow header transmit 48 1 0.0.1.0.47.0


atsdk::eth flow create 49 nop
atsdk::encap hdlc link flowbind 49 49
atsdk::encap hdlc link oammode 49 topsn
atsdk::encap hdlc link oamegress destmac 49 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 49 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 49 networkactive
atsdk::encap hdlc link traffic txenable 49
atsdk::encap hdlc link traffic rxenable 49
atsdk::eth flow egress destmac 49 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 49 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 49 1 0.0.1.0.48.0
atsdk::bdcom eth flow header transmit 49 1 0.0.1.0.48.0


atsdk::eth flow create 50 nop
atsdk::encap hdlc link flowbind 50 50
atsdk::encap hdlc link oammode 50 topsn
atsdk::encap hdlc link oamegress destmac 50 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 50 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 50 networkactive
atsdk::encap hdlc link traffic txenable 50
atsdk::encap hdlc link traffic rxenable 50
atsdk::eth flow egress destmac 50 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 50 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 50 1 0.0.1.0.49.0
atsdk::bdcom eth flow header transmit 50 1 0.0.1.0.49.0


atsdk::eth flow create 51 nop
atsdk::encap hdlc link flowbind 51 51
atsdk::encap hdlc link oammode 51 topsn
atsdk::encap hdlc link oamegress destmac 51 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 51 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 51 networkactive
atsdk::encap hdlc link traffic txenable 51
atsdk::encap hdlc link traffic rxenable 51
atsdk::eth flow egress destmac 51 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 51 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 51 1 0.0.1.0.50.0
atsdk::bdcom eth flow header transmit 51 1 0.0.1.0.50.0


atsdk::eth flow create 52 nop
atsdk::encap hdlc link flowbind 52 52
atsdk::encap hdlc link oammode 52 topsn
atsdk::encap hdlc link oamegress destmac 52 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 52 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 52 networkactive
atsdk::encap hdlc link traffic txenable 52
atsdk::encap hdlc link traffic rxenable 52
atsdk::eth flow egress destmac 52 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 52 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 52 1 0.0.1.0.51.0
atsdk::bdcom eth flow header transmit 52 1 0.0.1.0.51.0


atsdk::eth flow create 53 nop
atsdk::encap hdlc link flowbind 53 53
atsdk::encap hdlc link oammode 53 topsn
atsdk::encap hdlc link oamegress destmac 53 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 53 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 53 networkactive
atsdk::encap hdlc link traffic txenable 53
atsdk::encap hdlc link traffic rxenable 53
atsdk::eth flow egress destmac 53 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 53 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 53 1 0.0.1.0.52.0
atsdk::bdcom eth flow header transmit 53 1 0.0.1.0.52.0


atsdk::eth flow create 54 nop
atsdk::encap hdlc link flowbind 54 54
atsdk::encap hdlc link oammode 54 topsn
atsdk::encap hdlc link oamegress destmac 54 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 54 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 54 networkactive
atsdk::encap hdlc link traffic txenable 54
atsdk::encap hdlc link traffic rxenable 54
atsdk::eth flow egress destmac 54 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 54 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 54 1 0.0.1.0.53.0
atsdk::bdcom eth flow header transmit 54 1 0.0.1.0.53.0


atsdk::eth flow create 55 nop
atsdk::encap hdlc link flowbind 55 55
atsdk::encap hdlc link oammode 55 topsn
atsdk::encap hdlc link oamegress destmac 55 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 55 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 55 networkactive
atsdk::encap hdlc link traffic txenable 55
atsdk::encap hdlc link traffic rxenable 55
atsdk::eth flow egress destmac 55 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 55 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 55 1 0.0.1.0.54.0
atsdk::bdcom eth flow header transmit 55 1 0.0.1.0.54.0


atsdk::eth flow create 56 nop
atsdk::encap hdlc link flowbind 56 56
atsdk::encap hdlc link oammode 56 topsn
atsdk::encap hdlc link oamegress destmac 56 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 56 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 56 networkactive
atsdk::encap hdlc link traffic txenable 56
atsdk::encap hdlc link traffic rxenable 56
atsdk::eth flow egress destmac 56 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 56 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 56 1 0.0.1.0.55.0
atsdk::bdcom eth flow header transmit 56 1 0.0.1.0.55.0


atsdk::eth flow create 57 nop
atsdk::encap hdlc link flowbind 57 57
atsdk::encap hdlc link oammode 57 topsn
atsdk::encap hdlc link oamegress destmac 57 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 57 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 57 networkactive
atsdk::encap hdlc link traffic txenable 57
atsdk::encap hdlc link traffic rxenable 57
atsdk::eth flow egress destmac 57 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 57 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 57 1 0.0.1.0.56.0
atsdk::bdcom eth flow header transmit 57 1 0.0.1.0.56.0


atsdk::eth flow create 58 nop
atsdk::encap hdlc link flowbind 58 58
atsdk::encap hdlc link oammode 58 topsn
atsdk::encap hdlc link oamegress destmac 58 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 58 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 58 networkactive
atsdk::encap hdlc link traffic txenable 58
atsdk::encap hdlc link traffic rxenable 58
atsdk::eth flow egress destmac 58 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 58 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 58 1 0.0.1.0.57.0
atsdk::bdcom eth flow header transmit 58 1 0.0.1.0.57.0


atsdk::eth flow create 59 nop
atsdk::encap hdlc link flowbind 59 59
atsdk::encap hdlc link oammode 59 topsn
atsdk::encap hdlc link oamegress destmac 59 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 59 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 59 networkactive
atsdk::encap hdlc link traffic txenable 59
atsdk::encap hdlc link traffic rxenable 59
atsdk::eth flow egress destmac 59 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 59 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 59 1 0.0.1.0.58.0
atsdk::bdcom eth flow header transmit 59 1 0.0.1.0.58.0


atsdk::eth flow create 60 nop
atsdk::encap hdlc link flowbind 60 60
atsdk::encap hdlc link oammode 60 topsn
atsdk::encap hdlc link oamegress destmac 60 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 60 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 60 networkactive
atsdk::encap hdlc link traffic txenable 60
atsdk::encap hdlc link traffic rxenable 60
atsdk::eth flow egress destmac 60 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 60 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 60 1 0.0.1.0.59.0
atsdk::bdcom eth flow header transmit 60 1 0.0.1.0.59.0


atsdk::eth flow create 61 nop
atsdk::encap hdlc link flowbind 61 61
atsdk::encap hdlc link oammode 61 topsn
atsdk::encap hdlc link oamegress destmac 61 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 61 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 61 networkactive
atsdk::encap hdlc link traffic txenable 61
atsdk::encap hdlc link traffic rxenable 61
atsdk::eth flow egress destmac 61 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 61 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 61 1 0.0.1.0.60.0
atsdk::bdcom eth flow header transmit 61 1 0.0.1.0.60.0


atsdk::eth flow create 62 nop
atsdk::encap hdlc link flowbind 62 62
atsdk::encap hdlc link oammode 62 topsn
atsdk::encap hdlc link oamegress destmac 62 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 62 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 62 networkactive
atsdk::encap hdlc link traffic txenable 62
atsdk::encap hdlc link traffic rxenable 62
atsdk::eth flow egress destmac 62 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 62 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 62 1 0.0.1.0.61.0
atsdk::bdcom eth flow header transmit 62 1 0.0.1.0.61.0


atsdk::eth flow create 63 nop
atsdk::encap hdlc link flowbind 63 63
atsdk::encap hdlc link oammode 63 topsn
atsdk::encap hdlc link oamegress destmac 63 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 63 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 63 networkactive
atsdk::encap hdlc link traffic txenable 63
atsdk::encap hdlc link traffic rxenable 63
atsdk::eth flow egress destmac 63 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 63 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 63 1 0.0.1.0.62.0
atsdk::bdcom eth flow header transmit 63 1 0.0.1.0.62.0


atsdk::eth flow create 64 nop
atsdk::encap hdlc link flowbind 64 64
atsdk::encap hdlc link oammode 64 topsn
atsdk::encap hdlc link oamegress destmac 64 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 64 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 64 networkactive
atsdk::encap hdlc link traffic txenable 64
atsdk::encap hdlc link traffic rxenable 64
atsdk::eth flow egress destmac 64 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 64 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 64 1 0.0.1.0.63.0
atsdk::bdcom eth flow header transmit 64 1 0.0.1.0.63.0


atsdk::eth flow create 65 nop
atsdk::encap hdlc link flowbind 65 65
atsdk::encap hdlc link oammode 65 topsn
atsdk::encap hdlc link oamegress destmac 65 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 65 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 65 networkactive
atsdk::encap hdlc link traffic txenable 65
atsdk::encap hdlc link traffic rxenable 65
atsdk::eth flow egress destmac 65 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 65 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 65 1 0.0.1.0.64.0
atsdk::bdcom eth flow header transmit 65 1 0.0.1.0.64.0


atsdk::eth flow create 66 nop
atsdk::encap hdlc link flowbind 66 66
atsdk::encap hdlc link oammode 66 topsn
atsdk::encap hdlc link oamegress destmac 66 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 66 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 66 networkactive
atsdk::encap hdlc link traffic txenable 66
atsdk::encap hdlc link traffic rxenable 66
atsdk::eth flow egress destmac 66 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 66 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 66 1 0.0.1.0.65.0
atsdk::bdcom eth flow header transmit 66 1 0.0.1.0.65.0


atsdk::eth flow create 67 nop
atsdk::encap hdlc link flowbind 67 67
atsdk::encap hdlc link oammode 67 topsn
atsdk::encap hdlc link oamegress destmac 67 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 67 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 67 networkactive
atsdk::encap hdlc link traffic txenable 67
atsdk::encap hdlc link traffic rxenable 67
atsdk::eth flow egress destmac 67 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 67 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 67 1 0.0.1.0.66.0
atsdk::bdcom eth flow header transmit 67 1 0.0.1.0.66.0


atsdk::eth flow create 68 nop
atsdk::encap hdlc link flowbind 68 68
atsdk::encap hdlc link oammode 68 topsn
atsdk::encap hdlc link oamegress destmac 68 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 68 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 68 networkactive
atsdk::encap hdlc link traffic txenable 68
atsdk::encap hdlc link traffic rxenable 68
atsdk::eth flow egress destmac 68 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 68 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 68 1 0.0.1.0.67.0
atsdk::bdcom eth flow header transmit 68 1 0.0.1.0.67.0


atsdk::eth flow create 69 nop
atsdk::encap hdlc link flowbind 69 69
atsdk::encap hdlc link oammode 69 topsn
atsdk::encap hdlc link oamegress destmac 69 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 69 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 69 networkactive
atsdk::encap hdlc link traffic txenable 69
atsdk::encap hdlc link traffic rxenable 69
atsdk::eth flow egress destmac 69 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 69 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 69 1 0.0.1.0.68.0
atsdk::bdcom eth flow header transmit 69 1 0.0.1.0.68.0


atsdk::eth flow create 70 nop
atsdk::encap hdlc link flowbind 70 70
atsdk::encap hdlc link oammode 70 topsn
atsdk::encap hdlc link oamegress destmac 70 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 70 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 70 networkactive
atsdk::encap hdlc link traffic txenable 70
atsdk::encap hdlc link traffic rxenable 70
atsdk::eth flow egress destmac 70 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 70 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 70 1 0.0.1.0.69.0
atsdk::bdcom eth flow header transmit 70 1 0.0.1.0.69.0


atsdk::eth flow create 71 nop
atsdk::encap hdlc link flowbind 71 71
atsdk::encap hdlc link oammode 71 topsn
atsdk::encap hdlc link oamegress destmac 71 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 71 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 71 networkactive
atsdk::encap hdlc link traffic txenable 71
atsdk::encap hdlc link traffic rxenable 71
atsdk::eth flow egress destmac 71 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 71 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 71 1 0.0.1.0.70.0
atsdk::bdcom eth flow header transmit 71 1 0.0.1.0.70.0


atsdk::eth flow create 72 nop
atsdk::encap hdlc link flowbind 72 72
atsdk::encap hdlc link oammode 72 topsn
atsdk::encap hdlc link oamegress destmac 72 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 72 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 72 networkactive
atsdk::encap hdlc link traffic txenable 72
atsdk::encap hdlc link traffic rxenable 72
atsdk::eth flow egress destmac 72 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 72 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 72 1 0.0.1.0.71.0
atsdk::bdcom eth flow header transmit 72 1 0.0.1.0.71.0


atsdk::eth flow create 73 nop
atsdk::encap hdlc link flowbind 73 73
atsdk::encap hdlc link oammode 73 topsn
atsdk::encap hdlc link oamegress destmac 73 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 73 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 73 networkactive
atsdk::encap hdlc link traffic txenable 73
atsdk::encap hdlc link traffic rxenable 73
atsdk::eth flow egress destmac 73 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 73 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 73 1 0.0.1.0.72.0
atsdk::bdcom eth flow header transmit 73 1 0.0.1.0.72.0


atsdk::eth flow create 74 nop
atsdk::encap hdlc link flowbind 74 74
atsdk::encap hdlc link oammode 74 topsn
atsdk::encap hdlc link oamegress destmac 74 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 74 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 74 networkactive
atsdk::encap hdlc link traffic txenable 74
atsdk::encap hdlc link traffic rxenable 74
atsdk::eth flow egress destmac 74 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 74 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 74 1 0.0.1.0.73.0
atsdk::bdcom eth flow header transmit 74 1 0.0.1.0.73.0


atsdk::eth flow create 75 nop
atsdk::encap hdlc link flowbind 75 75
atsdk::encap hdlc link oammode 75 topsn
atsdk::encap hdlc link oamegress destmac 75 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 75 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 75 networkactive
atsdk::encap hdlc link traffic txenable 75
atsdk::encap hdlc link traffic rxenable 75
atsdk::eth flow egress destmac 75 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 75 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 75 1 0.0.1.0.74.0
atsdk::bdcom eth flow header transmit 75 1 0.0.1.0.74.0


atsdk::eth flow create 76 nop
atsdk::encap hdlc link flowbind 76 76
atsdk::encap hdlc link oammode 76 topsn
atsdk::encap hdlc link oamegress destmac 76 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 76 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 76 networkactive
atsdk::encap hdlc link traffic txenable 76
atsdk::encap hdlc link traffic rxenable 76
atsdk::eth flow egress destmac 76 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 76 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 76 1 0.0.1.0.75.0
atsdk::bdcom eth flow header transmit 76 1 0.0.1.0.75.0


atsdk::eth flow create 77 nop
atsdk::encap hdlc link flowbind 77 77
atsdk::encap hdlc link oammode 77 topsn
atsdk::encap hdlc link oamegress destmac 77 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 77 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 77 networkactive
atsdk::encap hdlc link traffic txenable 77
atsdk::encap hdlc link traffic rxenable 77
atsdk::eth flow egress destmac 77 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 77 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 77 1 0.0.1.0.76.0
atsdk::bdcom eth flow header transmit 77 1 0.0.1.0.76.0


atsdk::eth flow create 78 nop
atsdk::encap hdlc link flowbind 78 78
atsdk::encap hdlc link oammode 78 topsn
atsdk::encap hdlc link oamegress destmac 78 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 78 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 78 networkactive
atsdk::encap hdlc link traffic txenable 78
atsdk::encap hdlc link traffic rxenable 78
atsdk::eth flow egress destmac 78 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 78 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 78 1 0.0.1.0.77.0
atsdk::bdcom eth flow header transmit 78 1 0.0.1.0.77.0


atsdk::eth flow create 79 nop
atsdk::encap hdlc link flowbind 79 79
atsdk::encap hdlc link oammode 79 topsn
atsdk::encap hdlc link oamegress destmac 79 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 79 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 79 networkactive
atsdk::encap hdlc link traffic txenable 79
atsdk::encap hdlc link traffic rxenable 79
atsdk::eth flow egress destmac 79 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 79 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 79 1 0.0.1.0.78.0
atsdk::bdcom eth flow header transmit 79 1 0.0.1.0.78.0


atsdk::eth flow create 80 nop
atsdk::encap hdlc link flowbind 80 80
atsdk::encap hdlc link oammode 80 topsn
atsdk::encap hdlc link oamegress destmac 80 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 80 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 80 networkactive
atsdk::encap hdlc link traffic txenable 80
atsdk::encap hdlc link traffic rxenable 80
atsdk::eth flow egress destmac 80 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 80 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 80 1 0.0.1.0.79.0
atsdk::bdcom eth flow header transmit 80 1 0.0.1.0.79.0


atsdk::eth flow create 81 nop
atsdk::encap hdlc link flowbind 81 81
atsdk::encap hdlc link oammode 81 topsn
atsdk::encap hdlc link oamegress destmac 81 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 81 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 81 networkactive
atsdk::encap hdlc link traffic txenable 81
atsdk::encap hdlc link traffic rxenable 81
atsdk::eth flow egress destmac 81 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 81 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 81 1 0.0.1.0.80.0
atsdk::bdcom eth flow header transmit 81 1 0.0.1.0.80.0


atsdk::eth flow create 82 nop
atsdk::encap hdlc link flowbind 82 82
atsdk::encap hdlc link oammode 82 topsn
atsdk::encap hdlc link oamegress destmac 82 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 82 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 82 networkactive
atsdk::encap hdlc link traffic txenable 82
atsdk::encap hdlc link traffic rxenable 82
atsdk::eth flow egress destmac 82 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 82 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 82 1 0.0.1.0.81.0
atsdk::bdcom eth flow header transmit 82 1 0.0.1.0.81.0


atsdk::eth flow create 83 nop
atsdk::encap hdlc link flowbind 83 83
atsdk::encap hdlc link oammode 83 topsn
atsdk::encap hdlc link oamegress destmac 83 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 83 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 83 networkactive
atsdk::encap hdlc link traffic txenable 83
atsdk::encap hdlc link traffic rxenable 83
atsdk::eth flow egress destmac 83 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 83 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 83 1 0.0.1.0.82.0
atsdk::bdcom eth flow header transmit 83 1 0.0.1.0.82.0


atsdk::eth flow create 84 nop
atsdk::encap hdlc link flowbind 84 84
atsdk::encap hdlc link oammode 84 topsn
atsdk::encap hdlc link oamegress destmac 84 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 84 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 84 networkactive
atsdk::encap hdlc link traffic txenable 84
atsdk::encap hdlc link traffic rxenable 84
atsdk::eth flow egress destmac 84 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 84 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 84 1 0.0.1.0.83.0
atsdk::bdcom eth flow header transmit 84 1 0.0.1.0.83.0


atsdk::eth flow create 85 nop
atsdk::encap hdlc link flowbind 85 85
atsdk::encap hdlc link oammode 85 topsn
atsdk::encap hdlc link oamegress destmac 85 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 85 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 85 networkactive
atsdk::encap hdlc link traffic txenable 85
atsdk::encap hdlc link traffic rxenable 85
atsdk::eth flow egress destmac 85 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 85 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 85 1 0.0.1.0.84.0
atsdk::bdcom eth flow header transmit 85 1 0.0.1.0.84.0


atsdk::eth flow create 86 nop
atsdk::encap hdlc link flowbind 86 86
atsdk::encap hdlc link oammode 86 topsn
atsdk::encap hdlc link oamegress destmac 86 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 86 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 86 networkactive
atsdk::encap hdlc link traffic txenable 86
atsdk::encap hdlc link traffic rxenable 86
atsdk::eth flow egress destmac 86 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 86 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 86 1 0.0.1.0.85.0
atsdk::bdcom eth flow header transmit 86 1 0.0.1.0.85.0


atsdk::eth flow create 87 nop
atsdk::encap hdlc link flowbind 87 87
atsdk::encap hdlc link oammode 87 topsn
atsdk::encap hdlc link oamegress destmac 87 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 87 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 87 networkactive
atsdk::encap hdlc link traffic txenable 87
atsdk::encap hdlc link traffic rxenable 87
atsdk::eth flow egress destmac 87 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 87 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 87 1 0.0.1.0.86.0
atsdk::bdcom eth flow header transmit 87 1 0.0.1.0.86.0


atsdk::eth flow create 88 nop
atsdk::encap hdlc link flowbind 88 88
atsdk::encap hdlc link oammode 88 topsn
atsdk::encap hdlc link oamegress destmac 88 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 88 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 88 networkactive
atsdk::encap hdlc link traffic txenable 88
atsdk::encap hdlc link traffic rxenable 88
atsdk::eth flow egress destmac 88 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 88 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 88 1 0.0.1.0.87.0
atsdk::bdcom eth flow header transmit 88 1 0.0.1.0.87.0


atsdk::eth flow create 89 nop
atsdk::encap hdlc link flowbind 89 89
atsdk::encap hdlc link oammode 89 topsn
atsdk::encap hdlc link oamegress destmac 89 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 89 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 89 networkactive
atsdk::encap hdlc link traffic txenable 89
atsdk::encap hdlc link traffic rxenable 89
atsdk::eth flow egress destmac 89 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 89 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 89 1 0.0.1.0.88.0
atsdk::bdcom eth flow header transmit 89 1 0.0.1.0.88.0


atsdk::eth flow create 90 nop
atsdk::encap hdlc link flowbind 90 90
atsdk::encap hdlc link oammode 90 topsn
atsdk::encap hdlc link oamegress destmac 90 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 90 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 90 networkactive
atsdk::encap hdlc link traffic txenable 90
atsdk::encap hdlc link traffic rxenable 90
atsdk::eth flow egress destmac 90 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 90 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 90 1 0.0.1.0.89.0
atsdk::bdcom eth flow header transmit 90 1 0.0.1.0.89.0


atsdk::eth flow create 91 nop
atsdk::encap hdlc link flowbind 91 91
atsdk::encap hdlc link oammode 91 topsn
atsdk::encap hdlc link oamegress destmac 91 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 91 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 91 networkactive
atsdk::encap hdlc link traffic txenable 91
atsdk::encap hdlc link traffic rxenable 91
atsdk::eth flow egress destmac 91 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 91 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 91 1 0.0.1.0.90.0
atsdk::bdcom eth flow header transmit 91 1 0.0.1.0.90.0


atsdk::eth flow create 92 nop
atsdk::encap hdlc link flowbind 92 92
atsdk::encap hdlc link oammode 92 topsn
atsdk::encap hdlc link oamegress destmac 92 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 92 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 92 networkactive
atsdk::encap hdlc link traffic txenable 92
atsdk::encap hdlc link traffic rxenable 92
atsdk::eth flow egress destmac 92 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 92 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 92 1 0.0.1.0.91.0
atsdk::bdcom eth flow header transmit 92 1 0.0.1.0.91.0


atsdk::eth flow create 93 nop
atsdk::encap hdlc link flowbind 93 93
atsdk::encap hdlc link oammode 93 topsn
atsdk::encap hdlc link oamegress destmac 93 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 93 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 93 networkactive
atsdk::encap hdlc link traffic txenable 93
atsdk::encap hdlc link traffic rxenable 93
atsdk::eth flow egress destmac 93 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 93 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 93 1 0.0.1.0.92.0
atsdk::bdcom eth flow header transmit 93 1 0.0.1.0.92.0


atsdk::eth flow create 94 nop
atsdk::encap hdlc link flowbind 94 94
atsdk::encap hdlc link oammode 94 topsn
atsdk::encap hdlc link oamegress destmac 94 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 94 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 94 networkactive
atsdk::encap hdlc link traffic txenable 94
atsdk::encap hdlc link traffic rxenable 94
atsdk::eth flow egress destmac 94 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 94 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 94 1 0.0.1.0.93.0
atsdk::bdcom eth flow header transmit 94 1 0.0.1.0.93.0


atsdk::eth flow create 95 nop
atsdk::encap hdlc link flowbind 95 95
atsdk::encap hdlc link oammode 95 topsn
atsdk::encap hdlc link oamegress destmac 95 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 95 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 95 networkactive
atsdk::encap hdlc link traffic txenable 95
atsdk::encap hdlc link traffic rxenable 95
atsdk::eth flow egress destmac 95 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 95 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 95 1 0.0.1.0.94.0
atsdk::bdcom eth flow header transmit 95 1 0.0.1.0.94.0


atsdk::eth flow create 96 nop
atsdk::encap hdlc link flowbind 96 96
atsdk::encap hdlc link oammode 96 topsn
atsdk::encap hdlc link oamegress destmac 96 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 96 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 96 networkactive
atsdk::encap hdlc link traffic txenable 96
atsdk::encap hdlc link traffic rxenable 96
atsdk::eth flow egress destmac 96 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 96 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 96 1 0.0.1.0.95.0
atsdk::bdcom eth flow header transmit 96 1 0.0.1.0.95.0


atsdk::eth flow create 97 nop
atsdk::encap hdlc link flowbind 97 97
atsdk::encap hdlc link oammode 97 topsn
atsdk::encap hdlc link oamegress destmac 97 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 97 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 97 networkactive
atsdk::encap hdlc link traffic txenable 97
atsdk::encap hdlc link traffic rxenable 97
atsdk::eth flow egress destmac 97 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 97 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 97 1 0.0.1.0.96.0
atsdk::bdcom eth flow header transmit 97 1 0.0.1.0.96.0


atsdk::eth flow create 98 nop
atsdk::encap hdlc link flowbind 98 98
atsdk::encap hdlc link oammode 98 topsn
atsdk::encap hdlc link oamegress destmac 98 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 98 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 98 networkactive
atsdk::encap hdlc link traffic txenable 98
atsdk::encap hdlc link traffic rxenable 98
atsdk::eth flow egress destmac 98 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 98 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 98 1 0.0.1.0.97.0
atsdk::bdcom eth flow header transmit 98 1 0.0.1.0.97.0


atsdk::eth flow create 99 nop
atsdk::encap hdlc link flowbind 99 99
atsdk::encap hdlc link oammode 99 topsn
atsdk::encap hdlc link oamegress destmac 99 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 99 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 99 networkactive
atsdk::encap hdlc link traffic txenable 99
atsdk::encap hdlc link traffic rxenable 99
atsdk::eth flow egress destmac 99 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 99 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 99 1 0.0.1.0.98.0
atsdk::bdcom eth flow header transmit 99 1 0.0.1.0.98.0


atsdk::eth flow create 100 nop
atsdk::encap hdlc link flowbind 100 100
atsdk::encap hdlc link oammode 100 topsn
atsdk::encap hdlc link oamegress destmac 100 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 100 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 100 networkactive
atsdk::encap hdlc link traffic txenable 100
atsdk::encap hdlc link traffic rxenable 100
atsdk::eth flow egress destmac 100 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 100 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 100 1 0.0.1.0.99.0
atsdk::bdcom eth flow header transmit 100 1 0.0.1.0.99.0


atsdk::eth flow create 101 nop
atsdk::encap hdlc link flowbind 101 101
atsdk::encap hdlc link oammode 101 topsn
atsdk::encap hdlc link oamegress destmac 101 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 101 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 101 networkactive
atsdk::encap hdlc link traffic txenable 101
atsdk::encap hdlc link traffic rxenable 101
atsdk::eth flow egress destmac 101 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 101 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 101 1 0.0.1.0.100.0
atsdk::bdcom eth flow header transmit 101 1 0.0.1.0.100.0


atsdk::eth flow create 102 nop
atsdk::encap hdlc link flowbind 102 102
atsdk::encap hdlc link oammode 102 topsn
atsdk::encap hdlc link oamegress destmac 102 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 102 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 102 networkactive
atsdk::encap hdlc link traffic txenable 102
atsdk::encap hdlc link traffic rxenable 102
atsdk::eth flow egress destmac 102 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 102 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 102 1 0.0.1.0.101.0
atsdk::bdcom eth flow header transmit 102 1 0.0.1.0.101.0


atsdk::eth flow create 103 nop
atsdk::encap hdlc link flowbind 103 103
atsdk::encap hdlc link oammode 103 topsn
atsdk::encap hdlc link oamegress destmac 103 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 103 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 103 networkactive
atsdk::encap hdlc link traffic txenable 103
atsdk::encap hdlc link traffic rxenable 103
atsdk::eth flow egress destmac 103 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 103 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 103 1 0.0.1.0.102.0
atsdk::bdcom eth flow header transmit 103 1 0.0.1.0.102.0


atsdk::eth flow create 104 nop
atsdk::encap hdlc link flowbind 104 104
atsdk::encap hdlc link oammode 104 topsn
atsdk::encap hdlc link oamegress destmac 104 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 104 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 104 networkactive
atsdk::encap hdlc link traffic txenable 104
atsdk::encap hdlc link traffic rxenable 104
atsdk::eth flow egress destmac 104 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 104 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 104 1 0.0.1.0.103.0
atsdk::bdcom eth flow header transmit 104 1 0.0.1.0.103.0


atsdk::eth flow create 105 nop
atsdk::encap hdlc link flowbind 105 105
atsdk::encap hdlc link oammode 105 topsn
atsdk::encap hdlc link oamegress destmac 105 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 105 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 105 networkactive
atsdk::encap hdlc link traffic txenable 105
atsdk::encap hdlc link traffic rxenable 105
atsdk::eth flow egress destmac 105 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 105 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 105 1 0.0.1.0.104.0
atsdk::bdcom eth flow header transmit 105 1 0.0.1.0.104.0


atsdk::eth flow create 106 nop
atsdk::encap hdlc link flowbind 106 106
atsdk::encap hdlc link oammode 106 topsn
atsdk::encap hdlc link oamegress destmac 106 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 106 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 106 networkactive
atsdk::encap hdlc link traffic txenable 106
atsdk::encap hdlc link traffic rxenable 106
atsdk::eth flow egress destmac 106 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 106 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 106 1 0.0.1.0.105.0
atsdk::bdcom eth flow header transmit 106 1 0.0.1.0.105.0


atsdk::eth flow create 107 nop
atsdk::encap hdlc link flowbind 107 107
atsdk::encap hdlc link oammode 107 topsn
atsdk::encap hdlc link oamegress destmac 107 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 107 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 107 networkactive
atsdk::encap hdlc link traffic txenable 107
atsdk::encap hdlc link traffic rxenable 107
atsdk::eth flow egress destmac 107 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 107 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 107 1 0.0.1.0.106.0
atsdk::bdcom eth flow header transmit 107 1 0.0.1.0.106.0


atsdk::eth flow create 108 nop
atsdk::encap hdlc link flowbind 108 108
atsdk::encap hdlc link oammode 108 topsn
atsdk::encap hdlc link oamegress destmac 108 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 108 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 108 networkactive
atsdk::encap hdlc link traffic txenable 108
atsdk::encap hdlc link traffic rxenable 108
atsdk::eth flow egress destmac 108 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 108 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 108 1 0.0.1.0.107.0
atsdk::bdcom eth flow header transmit 108 1 0.0.1.0.107.0


atsdk::eth flow create 109 nop
atsdk::encap hdlc link flowbind 109 109
atsdk::encap hdlc link oammode 109 topsn
atsdk::encap hdlc link oamegress destmac 109 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 109 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 109 networkactive
atsdk::encap hdlc link traffic txenable 109
atsdk::encap hdlc link traffic rxenable 109
atsdk::eth flow egress destmac 109 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 109 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 109 1 0.0.1.0.108.0
atsdk::bdcom eth flow header transmit 109 1 0.0.1.0.108.0


atsdk::eth flow create 110 nop
atsdk::encap hdlc link flowbind 110 110
atsdk::encap hdlc link oammode 110 topsn
atsdk::encap hdlc link oamegress destmac 110 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 110 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 110 networkactive
atsdk::encap hdlc link traffic txenable 110
atsdk::encap hdlc link traffic rxenable 110
atsdk::eth flow egress destmac 110 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 110 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 110 1 0.0.1.0.109.0
atsdk::bdcom eth flow header transmit 110 1 0.0.1.0.109.0


atsdk::eth flow create 111 nop
atsdk::encap hdlc link flowbind 111 111
atsdk::encap hdlc link oammode 111 topsn
atsdk::encap hdlc link oamegress destmac 111 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 111 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 111 networkactive
atsdk::encap hdlc link traffic txenable 111
atsdk::encap hdlc link traffic rxenable 111
atsdk::eth flow egress destmac 111 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 111 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 111 1 0.0.1.0.110.0
atsdk::bdcom eth flow header transmit 111 1 0.0.1.0.110.0


atsdk::eth flow create 112 nop
atsdk::encap hdlc link flowbind 112 112
atsdk::encap hdlc link oammode 112 topsn
atsdk::encap hdlc link oamegress destmac 112 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 112 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 112 networkactive
atsdk::encap hdlc link traffic txenable 112
atsdk::encap hdlc link traffic rxenable 112
atsdk::eth flow egress destmac 112 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 112 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 112 1 0.0.1.0.111.0
atsdk::bdcom eth flow header transmit 112 1 0.0.1.0.111.0


atsdk::eth flow create 113 nop
atsdk::encap hdlc link flowbind 113 113
atsdk::encap hdlc link oammode 113 topsn
atsdk::encap hdlc link oamegress destmac 113 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 113 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 113 networkactive
atsdk::encap hdlc link traffic txenable 113
atsdk::encap hdlc link traffic rxenable 113
atsdk::eth flow egress destmac 113 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 113 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 113 1 0.0.1.0.112.0
atsdk::bdcom eth flow header transmit 113 1 0.0.1.0.112.0


atsdk::eth flow create 114 nop
atsdk::encap hdlc link flowbind 114 114
atsdk::encap hdlc link oammode 114 topsn
atsdk::encap hdlc link oamegress destmac 114 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 114 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 114 networkactive
atsdk::encap hdlc link traffic txenable 114
atsdk::encap hdlc link traffic rxenable 114
atsdk::eth flow egress destmac 114 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 114 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 114 1 0.0.1.0.113.0
atsdk::bdcom eth flow header transmit 114 1 0.0.1.0.113.0


atsdk::eth flow create 115 nop
atsdk::encap hdlc link flowbind 115 115
atsdk::encap hdlc link oammode 115 topsn
atsdk::encap hdlc link oamegress destmac 115 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 115 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 115 networkactive
atsdk::encap hdlc link traffic txenable 115
atsdk::encap hdlc link traffic rxenable 115
atsdk::eth flow egress destmac 115 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 115 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 115 1 0.0.1.0.114.0
atsdk::bdcom eth flow header transmit 115 1 0.0.1.0.114.0


atsdk::eth flow create 116 nop
atsdk::encap hdlc link flowbind 116 116
atsdk::encap hdlc link oammode 116 topsn
atsdk::encap hdlc link oamegress destmac 116 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 116 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 116 networkactive
atsdk::encap hdlc link traffic txenable 116
atsdk::encap hdlc link traffic rxenable 116
atsdk::eth flow egress destmac 116 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 116 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 116 1 0.0.1.0.115.0
atsdk::bdcom eth flow header transmit 116 1 0.0.1.0.115.0


atsdk::eth flow create 117 nop
atsdk::encap hdlc link flowbind 117 117
atsdk::encap hdlc link oammode 117 topsn
atsdk::encap hdlc link oamegress destmac 117 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 117 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 117 networkactive
atsdk::encap hdlc link traffic txenable 117
atsdk::encap hdlc link traffic rxenable 117
atsdk::eth flow egress destmac 117 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 117 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 117 1 0.0.1.0.116.0
atsdk::bdcom eth flow header transmit 117 1 0.0.1.0.116.0


atsdk::eth flow create 118 nop
atsdk::encap hdlc link flowbind 118 118
atsdk::encap hdlc link oammode 118 topsn
atsdk::encap hdlc link oamegress destmac 118 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 118 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 118 networkactive
atsdk::encap hdlc link traffic txenable 118
atsdk::encap hdlc link traffic rxenable 118
atsdk::eth flow egress destmac 118 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 118 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 118 1 0.0.1.0.117.0
atsdk::bdcom eth flow header transmit 118 1 0.0.1.0.117.0


atsdk::eth flow create 119 nop
atsdk::encap hdlc link flowbind 119 119
atsdk::encap hdlc link oammode 119 topsn
atsdk::encap hdlc link oamegress destmac 119 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 119 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 119 networkactive
atsdk::encap hdlc link traffic txenable 119
atsdk::encap hdlc link traffic rxenable 119
atsdk::eth flow egress destmac 119 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 119 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 119 1 0.0.1.0.118.0
atsdk::bdcom eth flow header transmit 119 1 0.0.1.0.118.0


atsdk::eth flow create 120 nop
atsdk::encap hdlc link flowbind 120 120
atsdk::encap hdlc link oammode 120 topsn
atsdk::encap hdlc link oamegress destmac 120 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 120 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 120 networkactive
atsdk::encap hdlc link traffic txenable 120
atsdk::encap hdlc link traffic rxenable 120
atsdk::eth flow egress destmac 120 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 120 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 120 1 0.0.1.0.119.0
atsdk::bdcom eth flow header transmit 120 1 0.0.1.0.119.0


atsdk::eth flow create 121 nop
atsdk::encap hdlc link flowbind 121 121
atsdk::encap hdlc link oammode 121 topsn
atsdk::encap hdlc link oamegress destmac 121 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 121 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 121 networkactive
atsdk::encap hdlc link traffic txenable 121
atsdk::encap hdlc link traffic rxenable 121
atsdk::eth flow egress destmac 121 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 121 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 121 1 0.0.1.0.120.0
atsdk::bdcom eth flow header transmit 121 1 0.0.1.0.120.0


atsdk::eth flow create 122 nop
atsdk::encap hdlc link flowbind 122 122
atsdk::encap hdlc link oammode 122 topsn
atsdk::encap hdlc link oamegress destmac 122 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 122 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 122 networkactive
atsdk::encap hdlc link traffic txenable 122
atsdk::encap hdlc link traffic rxenable 122
atsdk::eth flow egress destmac 122 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 122 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 122 1 0.0.1.0.121.0
atsdk::bdcom eth flow header transmit 122 1 0.0.1.0.121.0


atsdk::eth flow create 123 nop
atsdk::encap hdlc link flowbind 123 123
atsdk::encap hdlc link oammode 123 topsn
atsdk::encap hdlc link oamegress destmac 123 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 123 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 123 networkactive
atsdk::encap hdlc link traffic txenable 123
atsdk::encap hdlc link traffic rxenable 123
atsdk::eth flow egress destmac 123 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 123 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 123 1 0.0.1.0.122.0
atsdk::bdcom eth flow header transmit 123 1 0.0.1.0.122.0


atsdk::eth flow create 124 nop
atsdk::encap hdlc link flowbind 124 124
atsdk::encap hdlc link oammode 124 topsn
atsdk::encap hdlc link oamegress destmac 124 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 124 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 124 networkactive
atsdk::encap hdlc link traffic txenable 124
atsdk::encap hdlc link traffic rxenable 124
atsdk::eth flow egress destmac 124 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 124 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 124 1 0.0.1.0.123.0
atsdk::bdcom eth flow header transmit 124 1 0.0.1.0.123.0


atsdk::eth flow create 125 nop
atsdk::encap hdlc link flowbind 125 125
atsdk::encap hdlc link oammode 125 topsn
atsdk::encap hdlc link oamegress destmac 125 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 125 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 125 networkactive
atsdk::encap hdlc link traffic txenable 125
atsdk::encap hdlc link traffic rxenable 125
atsdk::eth flow egress destmac 125 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 125 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 125 1 0.0.1.0.124.0
atsdk::bdcom eth flow header transmit 125 1 0.0.1.0.124.0


atsdk::eth flow create 126 nop
atsdk::encap hdlc link flowbind 126 126
atsdk::encap hdlc link oammode 126 topsn
atsdk::encap hdlc link oamegress destmac 126 C0.CA.C0.CA.C0.CA
atsdk::encap hdlc link oamegress srcmac 126 CA.FE.CA.FE.CA.FE
atsdk::ppp link phase 126 networkactive
atsdk::encap hdlc link traffic txenable 126
atsdk::encap hdlc link traffic rxenable 126
atsdk::eth flow egress destmac 126 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 126 CA.FE.CA.FE.CA.FE
atsdk::bdcom eth flow header expect 126 1 0.0.1.0.125.0
atsdk::bdcom eth flow header transmit 126 1 0.0.1.0.125.0

atsdk::eth port srcmac 1 CA.FE.CA.FE.CA.FE

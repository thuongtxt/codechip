namespace import atsdk::* 
pdh de3 framing 1 ds3_m13ds1
pdh de1 framing 1.1.1-1.4.4 ds1_esf

pdh de3 loopback 1 loopin
xc create de1.1  de1.33
xc create de1.2  de1.34
xc create de1.3  de1.35
xc create de1.4  de1.36
xc create de1.5  de1.37
xc create de1.6  de1.38
xc create de1.7  de1.39
xc create de1.8  de1.40
xc create de1.9  de1.41
xc create de1.10  de1.42
xc create de1.11  de1.43
xc create de1.12  de1.44
xc create de1.13  de1.45
xc create de1.14  de1.46
xc create de1.15  de1.47
xc create de1.16  de1.48
xc create de1.17  de1.49
xc create de1.18  de1.50
xc create de1.19  de1.51
xc create de1.20  de1.52
xc create de1.21  de1.53
xc create de1.22  de1.54
xc create de1.23  de1.55
xc create de1.24  de1.56
xc create de1.25  de1.57
xc create de1.26  de1.58
xc create de1.27  de1.59
xc create de1.28  de1.60
xc create de1.29  de1.61
xc create de1.30  de1.62
xc create de1.31  de1.63
xc create de1.32  de1.64

xc create de1.33  de1.1 
xc create de1.34  de1.2 
xc create de1.35  de1.3 
xc create de1.36  de1.4 
xc create de1.37  de1.5 
xc create de1.38  de1.6 
xc create de1.39  de1.7 
xc create de1.40  de1.8 
xc create de1.41  de1.9 
xc create de1.42  de1.10
xc create de1.43  de1.11
xc create de1.44  de1.12
xc create de1.45  de1.13
xc create de1.46  de1.14
xc create de1.47  de1.15
xc create de1.48  de1.16
xc create de1.49  de1.17
xc create de1.50  de1.18
xc create de1.51  de1.19
xc create de1.52  de1.20
xc create de1.53  de1.21
xc create de1.54  de1.22
xc create de1.55  de1.23
xc create de1.56  de1.24
xc create de1.57  de1.25
xc create de1.58  de1.26
xc create de1.59  de1.27
xc create de1.60  de1.28
xc create de1.61  de1.29
xc create de1.62  de1.30
xc create de1.63  de1.31
xc create de1.64  de1.32

pdh de3 loopback 1 release

pdh de3 loopback 1 loopin
 
vcg delete 1
vcg create 1 vcat
vcg memprov 1 de1.1.1.1 

vcg delete 2
vcg create 2 vcat
vcg memprov 2 de1.1.1.2

vcg delete 3
vcg create 3 vcat
vcg memprov 3 de1.1.1.3

vcg delete 4
vcg create 4 vcat
vcg memprov 4 de1.1.1.4

vcg delete 5
vcg create 5 vcat
vcg memprov 5 de1.1.2.1

vcg delete 6
vcg create 6 vcat
vcg memprov 6 de1.1.2.2

vcg delete 7
vcg create 7 vcat
vcg memprov 7 de1.1.2.3

vcg delete 8
vcg create 8 vcat
vcg memprov 8 de1.1.2.4

vcg delete 9
vcg create 9 vcat
vcg memprov 9 de1.1.3.1

vcg delete 10
vcg create 10 vcat
vcg memprov 10 de1.1.3.2

vcg delete 11
vcg create 11 vcat
vcg memprov 11 de1.1.3.3

vcg delete 12
vcg create 12 vcat
vcg memprov 12 de1.1.3.4

vcg delete  13
vcg create  13 vcat
vcg memprov 13 de1.1.4.1
 
vcg delete  14
vcg create  14 vcat
vcg memprov 14 de1.1.4.2

vcg delete  15
vcg create  15 vcat
vcg memprov 15 de1.1.4.3

vcg delete  16
vcg create  16 vcat
vcg memprov 16 de1.1.4.4


eth flow ingress vlan add 1  1  0.0.0  0.0.0
eth flow ingress vlan add 2  1  0.0.1  0.0.1 
eth flow ingress vlan add 3  1  0.0.2  0.0.2 
eth flow ingress vlan add 4  1  0.0.3  0.0.3 
eth flow ingress vlan add 5  1  0.0.4  0.0.4
eth flow ingress vlan add 6  1  0.0.5  0.0.5
eth flow ingress vlan add 7  1  0.0.6  0.0.6
eth flow ingress vlan add 8  1  0.0.7  0.0.7
eth flow ingress vlan add 9  1  0.0.8  0.0.8
eth flow ingress vlan add 10  1  0.0.9  0.0.9
eth flow ingress vlan add 11  1  0.0.10  0.0.10
eth flow ingress vlan add 12  1  0.0.11  0.0.11
eth flow ingress vlan add 13  1  0.0.12  0.0.12
eth flow ingress vlan add 14  1  0.0.13  0.0.13
eth flow ingress vlan add 15  1  0.0.14  0.0.14
eth flow ingress vlan add 16  1  0.0.15  0.0.15


eth port flow 1-16 en
 
show encap gfp counters 1-16 r2c
show encap gfp counters 1-16 r2c
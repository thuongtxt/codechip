atsdk::device init
atsdk::serdes mode 1 stm4
atsdk::sdh line rate 1 stm4
atsdk::sdh line mode 1 sonet

atsdk::sdh line dcc create 1-4 section
atsdk::encap hdlc frametype dcc.1-4.section ppp
atsdk::encap hdlc fcs dcc.1-4.section fcs32
atsdk::pw create hdlc dcc.1-4.section

# Data flow
atsdk::pw ethport dcc.1-4.section 1
atsdk::pw ethheader dcc.1-4.section AA.BB.CC.DD.EE.EE 1.1.1 1.1.100
atsdk::pw vlan expect dcc.1.section 1.1.1 none
atsdk::pw vlan expect dcc.2.section 1.1.2 none
atsdk::pw vlan expect dcc.3.section 1.1.3 none
atsdk::pw vlan expect dcc.4.section 1.1.4 none
atsdk::pw enable dcc.1-4.section

# Control flow
 # Common OAM ethernet header elements
atsdk::cisco control flow egress destmac C0.CA.C0.CA.C0.CA
atsdk::cisco control flow egress srcmac C0.CA.C0.CA.C0.CA
atsdk::cisco control flow egress control vlan 0x8100 1.1.4095
atsdk::cisco control flow egress ethtype 0x8857
atsdk::cisco control flow egress circuit vlan tpid 0x8100

 # VLAN specific for this DCC
atsdk::encap hdlc link oamegress vlan add dcc.1.section 1 1.1.1 none

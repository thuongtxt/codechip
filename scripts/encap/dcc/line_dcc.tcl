atsdk::device init
atsdk::serdes mode 1 stm16
atsdk::sdh line rate 1 stm16
atsdk::sdh line mode 1 sonet

atsdk::sdh line dcc create 1 line
atsdk::encap hdlc frametype dcc.1.line ppp
atsdk::encap hdlc fcs dcc.1.line fcs32
atsdk::pw create hdlc dcc.1.line

# Data flow
atsdk::pw ethport dcc.1.line 1
atsdk::pw ethheader dcc.1.line CA.FE.CA.FE.CA.FE 0.0.1 none
atsdk::pw vlan expect dcc.1.line 0.0.1 none
atsdk::pw enable dcc.1.line

# Control flow
 # Common OAM ethernet header elements
atsdk::cisco control flow egress destmac C0.CA.C0.CA.C0.CA
atsdk::cisco control flow egress srcmac C0.CA.C0.CA.C0.CA
atsdk::cisco control flow egress control vlan 0x8100 1.1.4095
atsdk::cisco control flow egress ethtype 0x8857
atsdk::cisco control flow egress circuit vlan tpid 0x8100

 # VLAN specific for this DCC
atsdk::encap hdlc link oamegress vlan add dcc.1.line 1 1.1.1 none

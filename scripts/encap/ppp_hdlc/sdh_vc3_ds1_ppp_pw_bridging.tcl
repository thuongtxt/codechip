atsdk::device init

atsdk::sdh line mode 1 sdh
atsdk::sdh line rate 1 stm16
atsdk::sdh map aug16.1.1 4xaug4s
atsdk::sdh map aug4.1.1 4xaug1s

atsdk::sdh map aug1.1.1 3xvc3s
atsdk::sdh map vc3.1.1.1-1.1.3 7xtug2s
atsdk::sdh map tug2.1.1.1.1-1.1.3.7 tu12
atsdk::sdh map vc1x.1.1.1.1.1-1.1.3.7.3 de1
atsdk::pdh de1 framing 1.1.1.1.1 e1_unframed

atsdk::encap channel create 1 ppp
atsdk::encap channel bind 1 de1.1.1.1.1.1     
atsdk::ppp link phase 1 networkactive
atsdk::encap hdlc link pdu 1 ethernet

atsdk::pw create ppp 1
atsdk::pw payloadtype ppp.1 pdu
atsdk::pw ethport ppp.1 1
atsdk::pw psn ppp.1 mpls
atsdk::pw ethheader ppp.1 CA.FE.CA.FE.CA.FE 0.0.1 none
atsdk::pw vlan expect ppp.1 1.1.1 none
atsdk::pw mpls innerlabel ppp.1 1.0.255
atsdk::pw mpls expectedlabel ppp.1 1
atsdk::pw enable ppp.1
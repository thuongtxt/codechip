atsdk::device init

atsdk::sdh line mode 1 sdh
atsdk::sdh line rate 1 stm16
atsdk::sdh map aug16.1.1 4xaug4s
atsdk::sdh map aug4.1.1 4xaug1s
atsdk::sdh map aug1.1.1 3xvc3s
atsdk::sdh map vc3.1.1.1-1.1.3 7xtug2s
atsdk::sdh map tug2.1.1.1.1-1.1.3.7 tu12
atsdk::sdh map vc1x.1.1.1.1.1-1.1.3.7.3 de1
atsdk::pdh de1 framing 1.1.1.1.1 e1_unframed

atsdk::encap channel create 1 ppp
atsdk::encap channel bind 1 de1.1.1.1.1.1
atsdk::encap hdlc link pdu 1 ethernet
atsdk::ppp link phase 1 network

atsdk::ppp bundle create 1
atsdk::ppp bundle add link 1 1
atsdk::ppp bundle enable 1

atsdk::pw create mlppp 1
atsdk::pw ethport mlppp.1 1
atsdk::pw payloadtype mlppp.1 pdu
atsdk::pw ethheader mlppp.1 CA.FE.CA.FE.CA.FE 0.0.1 none
atsdk::pw psn mlppp.1 mpls
atsdk::pw mpls innerlabel mlppp.1 1.0.255
atsdk::pw mpls expectedlabel mlppp.1 1
atsdk::pw vlan expect mlppp.1 0.0.1 none
atsdk::pw enable mlppp.1

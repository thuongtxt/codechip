atsdk::cisco control flow egress destmac c0.ca.c0.ca.c0.ca
atsdk::cisco control flow egress srcmac ca.fe.ca.fe.ca.fe
atsdk::cisco control flow egress control vlan 0x8100 0.0.1
atsdk::cisco control flow ingress control vlan 0x8100 0.0.1
atsdk::cisco control flow egress ethtype 0x8847

atsdk::cisco control flow egress circuit vlan tpid 0x8100  
atsdk::ppp bundle oamegress vlan add 1 1 0.0.1 none
atsdk::ppp bundle oamingress vlan add 1 1 0.0.1 none

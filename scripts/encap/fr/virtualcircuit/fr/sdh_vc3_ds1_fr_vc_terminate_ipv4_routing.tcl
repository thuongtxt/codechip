atsdk::device init

atsdk::sdh line mode 1 sdh
atsdk::sdh line rate 1 stm16
atsdk::sdh map aug16.1.1 4xaug4s
atsdk::sdh map aug4.1.1 4xaug1s
atsdk::sdh map aug1.1.1 3xvc3s
atsdk::sdh map vc3.1.1.1-1.1.3 7xtug2s
atsdk::sdh map tug2.1.1.1.1-1.1.3.7 tu12
atsdk::sdh map vc1x.1.1.1.1.1-1.1.3.7.3 de1
atsdk::pdh de1 framing 1.1.1.1.1 e1_unframed

# Create Frame relay
atsdk::encap channel create 1 framerelay
atsdk::encap channel bind 1 de1.1.1.1.1.1
atsdk::encap hdlc link pdu 1 ipv4

#Create Virtual Circuit
atsdk::fr virtualcircuit create 1 1

atsdk::eth flow create 1
atsdk::fr virtualcircuit flowbind 1.1 1
atsdk::eth flow egress destmac 1 C0.CA.C0.CA.C0.CA
atsdk::eth flow egress srcmac 1 CA.FE.CA.FE.CA.FE
atsdk::eth flow egress vlan add 1 1 0.0.1 none
atsdk::eth flow ingress vlan add 1 1 0.0.1 none
atsdk::encap hdlc link traffic txenable 1
atsdk::encap hdlc link traffic rxenable 1

# Frame Relay over MLPS(one to one)
atsdk::device init

atsdk::sdh line mode 1 sdh
atsdk::sdh line rate 1 stm16
atsdk::sdh map aug16.1.1 4xaug4s
atsdk::sdh map aug4.1.1 4xaug1s

atsdk::sdh map aug1.1.1 3xvc3s
atsdk::sdh map vc3.1.1.1-1.1.3 7xtug2s
atsdk::sdh map tug2.1.1.1.1-1.1.3.7 tu12
atsdk::sdh map vc1x.1.1.1.1.1-1.1.3.7.3 de1
atsdk::pdh de1 framing 1.1.1.1.1 ds1_unframed

# Create Frame relay
atsdk::encap channel create 1 framerelay
atsdk::encap channel bind 1 de1.1.1.1.1.1
atsdk::encap hdlc link pdu 1 any
atsdk::fr virtualcircuit create 1 1

#Create Virtual Circuit
atsdk::pw create frvc fr.1.1
atsdk::pw payloadtype fr.1.1 full
atsdk::pw ethport fr.1.1 1
atsdk::pw ethheader fr.1.1 CA.FE.CA.FE.CA.FE 0.0.1 none
atsdk::pw psn fr.1.1 mpls
atsdk::pw mpls innerlabel fr.1.1 1.0.255
atsdk::pw mpls expectedlabel fr.1.1 1
atsdk::pw vlan expect fr.1.1 0.0.1 none
atsdk::pw enable fr.1.1

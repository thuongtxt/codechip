source sdh_util.tcl

set lineId 1
set lineRate stm4
proc frameMode {} { return e1_unframed }

proc numAug1s {} {
	global lineRate
	return [numAug1sByRate $lineRate]
}

# Test mapping CLIs
proc setMappingForLine { lineId } {
	# AU-4
	atsdk::sdh map "aug1.$lineId.1,$lineId.3" vc4
	atsdk::sdh map "vc4.$lineId.1,$lineId.3" 3xtug3s
	atsdk::sdh map "tug3.$lineId.1.1-$lineId.1.3,$lineId.3.1-$lineId.3.3" 7xtug2s
	atsdk::sdh map "tug2.$lineId.1.1.1-$lineId.1.3.7,$lineId.3.1.1-$lineId.3.3.7" tu12
	atsdk::sdh map "vc1x.$lineId.1.1.1.1-$lineId.1.3.7.3,$lineId.3.1.1.1-$lineId.3.3.7.3" de1
	
	# AU-3
	atsdk::sdh map "aug1.$lineId.2,$lineId.2" 3xvc3s
	atsdk::sdh map "vc3.$lineId.2.1-$lineId.2.3,$lineId.4.1-$lineId.4.3" 7xtug2s
	atsdk::sdh map "tug2.$lineId.2.1.1-$lineId.2.3.7,$lineId.4.1.1-$lineId.4.3.7" tu12
	atsdk::sdh map "vc1x.$lineId.2.1.1.1-$lineId.2.3.7.3,$lineId.4.1.1.1-$lineId.4.3.7.3" de1
} 

proc createAllLinks {lineId} {
	set de1s "$lineId.1.1.1.1-$lineId.[numAug1s].3.7.3"
	configureDe1 $de1s [frameMode]
	
	set numLinks [expr [numAug1s] * 3 * 21]
	set links "1-$numLinks"
	atsdk::encap channel create $links ppp
	atsdk::encap channel bind $links "de1.$de1s"
}

proc showStatus {lineId} {
	showLineStatus $lineId
	showPathStatus "vc4.$lineId.1,$lineId.3" 
	showPathStatus "vc3.$lineId.2.1-$lineId.2.3,$lineId.4.1-$lineId.4.3"
	showPathStatus "vc1x.$lineId.1.1.1.1-$lineId.[numAug1s].3.7.3"  
	
	# Show PDH
	showDe1Status "$lineId.1.1.1.1-$lineId.[numAug1s].3.7.3" 
}

# Test STM-1
atsdk::device init

# Hardware has problem on DDR that causes Jn messages all wrong. Disable Ethernet traffic
# to so that Jn can be tested
atsdk::wr 0x4022E 0

# Temporary disable TIM function, hardware does not work
configureLine $lineId $lineRate
setMappingForLine $lineId
createAllLinks $lineId

# Configure all paths
configurePath "vc4.$lineId.1,$lineId.3"
configurePath "vc1x.$lineId.1.1.1.1-$lineId.[numAug1s].3.7.3"

#showStatus $lineId
#after 1
#showStatus $lineId

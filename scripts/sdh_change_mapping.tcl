set lineId 1
set lineRate stm1

proc showLineStatus {lineId} {
	atsdk::show sdh line $lineId
	atsdk::show sdh line alarm $lineId
	atsdk::show sdh line counters $lineId r2c
	atsdk::show sdh line tti receive $lineId
}

proc showPathStatus { paths } {
	atsdk::show sdh path $paths
	atsdk::show sdh path alarm $paths
	atsdk::show sdh path counters $paths r2c
	atsdk::show sdh path tti receive $paths
}

proc showDe1Status { de1s } {
	atsdk::show pdh de1 alarm $de1s
	atsdk::show pdh de1 counters $de1s r2c
}

atsdk::sdh line rate $lineId $lineRate

atsdk::sdh map "aug1.$lineId.1" vc4
atsdk::sdh map "vc4.$lineId.1" 3xtug3s
atsdk::sdh map "tug3.$lineId.1.1-$lineId.1.3" 7xtug2s
atsdk::sdh map "tug2.$lineId.1.1.1-$lineId.1.3.7" tu12
atsdk::sdh map "vc1x.$lineId.1.1.1.1-$lineId.1.3.7.3" de1

showLineStatus $lineId
showPathStatus "vc4.$lineId.1"
showPathStatus "vc1x.$lineId.1.1.1.1-$lineId.1.3.7.3"
showDe1Status "$lineId.1.1.1.1-$lineId.1.3.7.3"

# Change mapping
atsdk::sdh map "aug1.$lineId.1" 3xvc3s
atsdk::sdh map "vc3.$lineId.1.1-$lineId.1.3" 7xtug2s
atsdk::sdh map "tug2.$lineId.1.1.1-$lineId.1.3.7" tu12
atsdk::sdh map "vc1x.$lineId.1.1.1.1-$lineId.1.3.7.3" de1

showPathStatus "vc3.$lineId.1.1-$lineId.1.3"
showPathStatus "vc1x.$lineId.1.1.1.1-$lineId.1.3.7.3"
showDe1Status "$lineId.1.1.1.1-$lineId.1.3.7.3"

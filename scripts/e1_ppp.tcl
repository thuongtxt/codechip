source e1_common_configure.tcl
source libs/AtPppLink.tcl
source libs/AtEthFlow.tcl

#configureEp
initDevice

setDefaultConfiguration

# Bind these flows to PPP links
atsdk::encap hdlc link flowbind [flows] [links]

# Enable PPP link traffic
atsdk::encap hdlc link traffic txenable [links]
atsdk::encap hdlc link traffic rxenable [links]
atsdk::ppp link phase [links] networkactive

# Set VLANs for these flows
setVlansForAllFlows

source e1_common_configure.tcl

proc de1Id { } { return [startDe1Port] }
proc e1FrameMode { } { return e1_basic }
# proc nxDs0List { } { return {1-15 17-31} }
proc nxDs0List { } { return {1-3} }

# Initialize
configureEp
initDevice

# Configure PDH
atsdk::pdh de1 framing [de1Id] [e1FrameMode]
atsdk::pdh de1 timing  [de1Id] system 1 ; # Note, the last parameter does not mater 

# Configure Ethernet port
ethPortSetDefault

# Create links
proc createNxDs0Link { linkId de1Id ds0List } {
	atsdk::pdh de1 nxds0create [de1Id] $ds0List
	atsdk::encap channel create $linkId ppp
	atsdk::encap channel bind   $linkId "nxds0.[de1Id].$ds0List"
}

# Create flows
proc createFlow { flowId } {
	atsdk::eth flow create $flowId eop
	atsdk::eth flow egress destmac $flowId "C0.CA.C0.CA.C0.CA"
	set vlan "0.0.$flowId"
}

proc setVlanForFlow { flowId } {
	set vlan "0.0.$flowId"
	atsdk::eth flow egress  vlan     $flowId [ethPort] $vlan none
	atsdk::eth flow ingress vlan add $flowId [ethPort] $vlan none
}

# Enable PPP link traffic
proc enablePppTrafficOnLink { linkId } {
	atsdk::encap hdlc link traffic txenable $linkId
	atsdk::encap hdlc link traffic rxenable $linkId
	atsdk::ppp link phase $linkId networkactive	
}

set linkId 1
foreach nxDs0 [nxDs0List] {
	createNxDs0Link $linkId [de1Id] $nxDs0
	createFlow $linkId
	atsdk::encap hdlc link flowbind $linkId $linkId
	enablePppTrafficOnLink $linkId
	setVlanForFlow $linkId
	incr linkId
}

source ep.tcl
source util.tcl

# Changable options
proc startDe1Port {} { return 1 }
proc stopDe1Port  {} { return 16 }
proc destMac      {} { return C0.CA.C0.CA.C0.CA }
proc ethPort      {} { return 1 }
proc frameMode    {} { return e1_basic }

# Build list of DS1/E1 ports
proc de1Ports     {} {
	if {[startDe1Port] == [stopDe1Port]} {
		return [startDe1Port]
	} else {
		return [startDe1Port]-[stopDe1Port]
	}
}

proc flows {} { return [de1Ports] }
proc links {} { return [de1Ports] }

# To set default configuration for EP
proc configureEp {} {
	# Configure LIUs
	EP::configureAllPortsWithMode "e1"
	atsdk::ep de1 loopback [EP::allPorts] analog; # So that RJ45 cables do not need
}

proc initDevice {} {
	atsdk::device show readwrite dis dis
	atsdk::device init
}

proc ethPortSetDefault {} {
	atsdk::eth port srcmac [ethPort] C0.FE.C0.FE.C0.FE
}

proc setDefaultConfiguration {} {
	# Configure PDH E1 de1Ports
	atsdk::pdh de1 framing [de1Ports] [frameMode]
	atsdk::pdh de1 timing  [de1Ports] system 1 ; # Note, the last parameter does not mater
	
	# Allocate encapsulation channel and use these DS1/E1 ports as physical layers
	atsdk::encap channel create [links] ppp
	atsdk::encap channel bind   [links] de1.[de1Ports]
	
	# Configure Ethernet port
	ethPortSetDefault
	
	# Create Ethernet traffic flow
	set flows [de1Ports]
	atsdk::eth flow create         [flows] eop
	atsdk::eth flow egress destmac [flows] "C0.CA.C0.CA.C0.CA"
}

proc setVlansForAllFlows {} {
	# Set VLANs for these flows
	for {set flow_i [startDe1Port]} {$flow_i <= [stopDe1Port]} {incr flow_i} {
		# Use flow ID as VLAN ID
		set vlan "0.0.$flow_i"
		
		# Set VLAN for two directions (use only one VLAN)
		atsdk::eth flow egress  vlan     $flow_i [ethPort] $vlan none
		atsdk::eth flow ingress vlan add $flow_i [ethPort] $vlan none
	}
}

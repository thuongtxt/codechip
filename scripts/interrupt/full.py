from AtAppManager import *
from AtRegister import *

class InterruptSimulate():
    def shouldCaptureAlarms(self):
        return False
    
    def run(self, cli):
        app = AtAppManager.localApp()
        return app.runCli(cli)
    
    def setup(self):
        self.run("device show readwrite dis dis")
        self.run("driver role active")
        self.run("device init")
        self.run("sdh map aug1.1.1-4.16 vc4")
        self.run("sdh map vc4.1.1-4.16 3xtug3s")
        self.run("sdh map tug3.1.1.1-4.16.3 7xtug2s")
        self.run("sdh map tug2.1.1.1.1-4.16.3.7 tu11")
        self.run("sdh map vc1x.1.1.1.1.1-4.16.3.7.4 de1")
        self.run("pw create satop 1-5376")
        self.run("pw circuit bind 1-5376 de1.1.1.1.1.1-4.16.3.7.4")
        self.run("pw ethport 1-2688    1")
        self.run("pw ethport 2689-5376 2")
        self.run("pw psn 1-5376 mpls")
        self.run("pw enable 1-5376")
        
        self.run("sdh path interruptmask vc1x.1.1.1.1.1-4.16.3.7.4 ais|lop|tim|uneq|p-uneq|plm|rdi|erdi-s|erdi-p|erdi-c|ber-sd|ber-sf|lom|rfi|clock-state-change en")
        self.run("sdh path interruptmask tu1x.1.1.1.1.1-4.16.3.7.4 ais|lop|tim|uneq|p-uneq|plm|rdi|erdi-s|erdi-p|erdi-c|ber-sd|ber-sf|lom|rfi|clock-state-change en")
        self.run("sdh path interruptmask vc4.1.1-4.16 ais|lop|tim|uneq|p-uneq|plm|rdi|erdi-s|erdi-p|erdi-c|ber-sd|ber-sf|lom|rfi|clock-state-change  en")
        self.run("pdh de1 interruptmask 1.1.1.1.1-4.16.3.7.4 los|lof|ais|rai|lomf|siglof|sigrai|ebit|ber-sd|ber-sf|ber-tca|bom-change|inband-loopcode-change|ssm-change|prm-lbbit-change  en")
        
        if self.shouldCaptureAlarms():
            self.run("sdh path alarm capture vc1x.1.1.1.1.1-4.16.3.7.4 en")
            self.run("sdh path alarm capture tu1x.1.1.1.1.1-4.16.3.7.4 en")
            self.run("sdh path alarm capture vc4.1.1-4.16 en")
            self.run("sdh path alarm capture au4.1.1-4.16 en")
            self.run("pdh de1 alarm capture 1.1.1.1.1-4.16.3.7.4 en")
        
        self.run("sdh interrupt enable")
        self.run("pdh interrupt enable")
        self.run("clock interrupt enable")
        self.run("device interrupt enable")
    
    def wr(self, address, value):
        self.run("wr %x %x" % (address, value))
    
    def simulateInterruptRaise(self):
        return True
    
    def makeFullSdhInterruptTree(self):
        # POH
        self.wr(0x200006, 0xffffffff)
        self.wr(0x200000, 0xffffffff)
        intrBase = 0x200000;
        
        for slice in range(0, 8):
            def cAf6Reg_alm_glbchghi(Slice24):
                return (0x0D007F+(Slice24)*128)
            self.wr(intrBase + cAf6Reg_alm_glbchghi(slice), 0xFFFFFFFF)
            
        for Slice24 in range(0, 16):
            def cAf6Reg_alm_chghi(STS, Slice24):
                return (0x0D0020+(STS)+(Slice24)*128)
            for STS in range(0, 24):
                self.wr(intrBase + cAf6Reg_alm_chghi(STS, Slice24), 0xFFFFFFFF)
        
        for Slice24 in range(0, 8):
            def cAf6Reg_alm_glbchglo(Slice24):
                return (0x0E0FFF+(Slice24)*4096)
            self.wr(intrBase + cAf6Reg_alm_glbchglo(Slice24), 0xFFFFFFFF)
        
        for Slice24 in range(0, 8):
            def cAf6Reg_alm_orstalo(STS, Slice24):
                return 0x0E0C00+(STS)+(Slice24)*4096
            
            for STS in range(0, 24):
                self.wr(intrBase + cAf6Reg_alm_orstalo(STS, Slice24), 0xFFFFFFFF)
        
        for Slice24 in range(0, 8):
            def cAf6Reg_alm_chglo(STS, Slice24, VTID):
                return (0x0E0400+(STS)*32+(Slice24)*4096+(VTID))
            
            for STS in range(0, 24):
                for VTID in range(0, 28):
                     self.wr(intrBase + cAf6Reg_alm_chglo(STS, Slice24, VTID), 0xFFFFFFFF)
    
        currentStatus = 0xFFFFFFFF if self.simulateInterruptRaise() else 0x0     
        for Slice24 in range(0, 8):
            def cAf6Reg_alm_stahi(STS, Slice24):
                return (0x0D0040+(STS)+(Slice24)*128)
            for STS in range(0, 24):
                self.wr(intrBase + cAf6Reg_alm_stahi(STS, Slice24), currentStatus)
                
        for Slice24 in range(0, 8):
            def cAf6Reg_alm_stalo(STS, Slice24, VTID):
                return (0x0E0800+(STS)*32+(Slice24)*4096+(VTID))
            for STS in range(0, 24):
                for VTID in range(0, 28):
                    self.wr(intrBase + cAf6Reg_alm_stalo(STS, Slice24, VTID), currentStatus)
            
    def makeFullPdhInterruptTree(self):
        def pdhBase():
            return 0x1000000
        
        def sliceBase(slice):
            return pdhBase() + (slice * 0x100000)
        
        def sliceOffset(slice):
            return ((0x1000000 + (slice * 0x100000)) - 0x700000)
        
        def cAf6Reg_dej1_rx_framer_per_stsvc_intr_or_stat():
            return 0x00055FFF
        
        def De1RxFrmrPerChnInterruptAlmRegister():
            return 0x755400
        def De1RxFrmrPerChnCurrentAlmRegister():
            return 0x755800
        
        for slice in range(0, 8):
            baseAddress = sliceBase(slice)
            self.wr(baseAddress + cAf6Reg_dej1_rx_framer_per_stsvc_intr_or_stat(), 0xFFFFFFFF)
        
        for slice in range(0, 8):
            def cAf6Reg_dej1_rx_framer_per_chn_intr_or_stat(StsID):
                return (0x00055C00+(StsID))
            baseAddress = sliceBase(slice)
            for StsID in range(0, 24):
                address = baseAddress + cAf6Reg_dej1_rx_framer_per_chn_intr_or_stat(StsID)
                self.wr(address, 0xFFFFFFFF)
        
        for slice in range(0, 8):
            sliceOffet = sliceOffset(slice)
            for de3 in range(0, 24):
                for de2 in range(0, 7):
                    for de1 in range(0, 4):
                        de1Offset = sliceOffet + (de3 * 32) + (de2 * 4) + de1
                        address = De1RxFrmrPerChnInterruptAlmRegister() + de1Offset
                        self.wr(address, 0xFFFFFFFF)
                        
                        address = De1RxFrmrPerChnCurrentAlmRegister() + de1Offset
                        currentStatus = 0xFFFFFFFF if self.simulateInterruptRaise() else 0
                        self.wr(address, currentStatus)
    
    def makeFullClockInterruptTree(self):
        self.wr(0x1e80009, 0xFFFFFFFF)
        
        baseAddress = 0xc00000;
            
        def SliceOffset(sliceId):
            return sliceId * 0x40000
        
        def SliceBase(sliceId):
            return baseAddress + SliceOffset(sliceId)
        
        for sliceId in range(0, 8):
            def cAf6Reg_cdr_per_stsvc_intr_or_stat_Base():
                return 0x00025FFF
            address = cAf6Reg_cdr_per_stsvc_intr_or_stat_Base() + SliceBase(sliceId)
            self.wr(address, 0xFFFFFFFF)
    
        for sliceId in range(0, 8):
            def cAf6Reg_cdr_per_chn_intr_or_stat(StsID):
                return (0x00025C00+(StsID))
            
            for StsID in range(0, 24):
                address = SliceBase(sliceId) + cAf6Reg_cdr_per_chn_intr_or_stat(StsID)
                self.wr(address, 0xFFFFFFFF)
    
    def makeFullInterruptTree(self):
        # Simulate interrupt
        self.wr(0x1e80002, 0xffffffff)
        self.wr(0x1e80004, 0xffffffff)
        self.makeFullSdhInterruptTree()
        self.makeFullPdhInterruptTree()
        self.makeFullClockInterruptTree()

class InterruptSimulateMro(InterruptSimulate):
    def setup(self):
        self.run("device show readwrite dis dis")
        self.run("debug device platform 60210051")
        self.run("device init")
        self.run("sdh map aug1.17.1-20.16,25.1-28.16 vc4")
        self.run("sdh map vc4.25.1-28.16 3xtug3s")
        self.run("sdh map tug3.25.1.1-28.16.3 7xtug2s")
        self.run("sdh map tug2.25.1.1.1-28.16.3.7 tu11")
        self.run("sdh map vc1x.25.1.1.1.1-28.16.3.7.4 de1")
        self.run("pw create satop 1-5376")
        self.run("pw circuit bind 1-5376 de1.25.1.1.1.1-28.16.3.7.4")
        self.run("pw ethport 1-5376 1")
        self.run("pw psn 1-5376 mpls")
        self.run("pw enable 1-5376")
        
        self.run("sdh path interruptmask vc1x.25.1.1.1.1-28.16.3.7.4 ais|lop|tim|uneq|p-uneq|plm|rdi|erdi-s|erdi-p|erdi-c|ber-sd|ber-sf|lom|rfi|clock-state-change en")
        self.run("sdh path interruptmask tu1x.25.1.1.1.1-28.16.3.7.4 ais|lop|tim|uneq|p-uneq|plm|rdi|erdi-s|erdi-p|erdi-c|ber-sd|ber-sf|lom|rfi|clock-state-change en")
        self.run("sdh path interruptmask au4.17.1-20.16 ais|lop en")
        self.run("pdh de1 interruptmask 25.1.1.1.1-28.16.3.7.4 los|lof|ais|rai|lomf|siglof|sigrai|ebit|ber-sd|ber-sf|ber-tca|bom-change|inband-loopcode-change|ssm-change|prm-lbbit-change  en")
        
        if self.shouldCaptureAlarms():
            self.run("sdh path alarm capture vc1x.25.1.1.1.1-28.16.3.7.4 en")
            self.run("sdh path alarm capture tu1x.25.1.1.1.1-28.16.3.7.4 en")
            self.run("sdh path alarm capture vc4.17.1-20.16 en")
            self.run("sdh path alarm capture au4.17.1-20.16 en")
            self.run("pdh de1 alarm capture 25.1.1.1.1-28.16.3.7.4 en")
        
        self.run("sdh interrupt enable")
        self.run("pdh interrupt enable")
        self.run("clock interrupt enable")
        self.run("device interrupt enable")
    
    def shouldCaptureAlarms(self):
        return True

def interruptSimulateCreate(productCode):
    if productCode == '60290021':
        return InterruptSimulateMro()
    
    return InterruptSimulate()

def productCodeString():
    app = AtAppManager.localApp()
    output = app.runCli("show driver")
    productCodeValue = int(output.cellContent(0, "Product code"), 16)
    register = AtRegister(productCodeValue)
    return "%x" % register.setField(cBit31_28, 28, 6)

if __name__ == '__main__':
    datapath = interruptSimulateCreate(productCodeString())
    datapath.setup()
    datapath.makeFullInterruptTree()

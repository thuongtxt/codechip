atsdk::device init

set timingMode system
set timingSource none
set frameMode e1_basic
set de1Id 1
set ds0List(1) 1-5
set ds0List(2) 6-18
set ds0List(3) 19-20
set ds0List(4) 21-31

atsdk::eth port srcmac 1 C0.CA.C0.CA.C0.CA

# Set up PDH 
atsdk::pdh de1 framing $de1Id $frameMode
atsdk::pdh de1 timing $de1Id $timingMode $timingSource

# Create NxDS0
for  {set i 1} {$i <= 4} { incr i} {
	atsdk::pdh de1 nxds0create $de1Id $ds0List($i)
}

# Create PW
for  {set pw_i 1} {$pw_i <= 4} { incr pw_i} {
	set circuit nxds0.$de1Id.$ds0List($pw_i)
	atsdk::pw create cesop $pw_i basic
	atsdk::pw circuit bind $pw_i $circuit
	atsdk::pw psn $pw_i mpls
	atsdk::pw ethport $pw_i 1

	# PSN			
	atsdk::pw ethheader $pw_i C0.CA.C0.CA.C0.CA 0.0.$pw_i none
	atsdk::pw mpls innerlabel $pw_i $pw_i.5.255
	atsdk::pw enable $pw_i				
}				

atsdk::eth port loopback 1 local

## Clear status
#set pws $pw_i
#after 1000
#atsdk::show pw counters $pws r2c
#after 1000
#
## Show status and configuration
#atsdk::show pw $pws
#atsdk::show pw psn $pws
#atsdk::show pw controlword $pws
#atsdk::show pw rtp $pws
#atsdk::show pw counters $pws r2c

device init
sdh line rate 1 stm1
sdh map aug1.1.1 3xvc3s
sdh map vc3.1.1.1-1.1.3 7xtug2s
sdh map tug2.1.1.1.1-1.1.3.7 tu12
sdh map vc1x.1.1.1.1.1-1.1.3.7.3 de1
sdh line rate 2 stm1
sdh map aug1.2.1 3xvc3s
sdh map vc3.2.1.1-2.1.3 7xtug2s
sdh map tug2.2.1.1.1-2.1.3.7 tu12
sdh map vc1x.2.1.1.1.1-2.1.3.7.3 de1
sdh line rate 3 stm1
sdh map aug1.3.1 3xvc3s
sdh map vc3.3.1.1-3.1.3 7xtug2s
sdh map tug2.3.1.1.1-3.1.3.7 tu12
sdh map vc1x.3.1.1.1.1-3.1.3.7.3 de1
sdh line rate 4 stm1
sdh map aug1.4.1 3xvc3s
sdh map vc3.4.1.1-4.1.3 7xtug2s
sdh map tug2.4.1.1.1-4.1.3.7 tu12
sdh map vc1x.4.1.1.1.1-4.1.3.7.3 de1
eth port srcmac 1 C0.CA.C0.CA.C0.CA

pw create satop 1
pw circuit bind 1 de1.1.1.1.1.1
pw ethport 1 1
pw psn 1 mpls
pw mpls innerlabel 1 1.1.255
pw mpls expectedlabel 1 1.1.255
pw ethheader 1 C0.CA.C0.CA.C0.CA 0.0.1 none
pw enable 1
sdh path timing de1.1.1.1.1.1 system xxx
#pw create satop 2
#pw circuit bind 2 de1.1.1.1.1.2
#pw ethport 2 1
#pw psn 2 mpls
#pw mpls innerlabel 2 2.2.255
#pw mpls expectedlabel 2 2.2.255
#pw ethheader 2 C0.CA.C0.CA.C0.CA 0.0.2 none
#pw enable 2
#sdh path timing de1.1.1.1.1.2 system xxx
#pw create satop 3
#pw circuit bind 3 de1.1.1.1.1.3
#pw ethport 3 1
#pw psn 3 mpls
#pw mpls innerlabel 3 3.3.255
#pw mpls expectedlabel 3 3.3.255
#pw ethheader 3 C0.CA.C0.CA.C0.CA 0.0.3 none
#pw enable 3
#sdh path timing de1.1.1.1.1.3 system xxx
#pw create satop 4
#pw circuit bind 4 de1.1.1.1.2.1
#pw ethport 4 1
#pw psn 4 mpls
#pw mpls innerlabel 4 4.4.255
#pw mpls expectedlabel 4 4.4.255
#pw ethheader 4 C0.CA.C0.CA.C0.CA 0.0.4 none
#pw enable 4
#sdh path timing de1.1.1.1.2.1 system xxx
#pw create satop 5
#pw circuit bind 5 de1.1.1.1.2.2
#pw ethport 5 1
#pw psn 5 mpls
#pw mpls innerlabel 5 5.5.255
#pw mpls expectedlabel 5 5.5.255
#pw ethheader 5 C0.CA.C0.CA.C0.CA 0.0.5 none
#pw enable 5
#sdh path timing de1.1.1.1.2.2 system xxx
#pw create satop 6
#pw circuit bind 6 de1.1.1.1.2.3
#pw ethport 6 1
#pw psn 6 mpls
#pw mpls innerlabel 6 6.6.255
#pw mpls expectedlabel 6 6.6.255
#pw ethheader 6 C0.CA.C0.CA.C0.CA 0.0.6 none
#pw enable 6
#sdh path timing de1.1.1.1.2.3 system xxx
#pw create satop 7
#pw circuit bind 7 de1.1.1.1.3.1
#pw ethport 7 1
#pw psn 7 mpls
#pw mpls innerlabel 7 7.7.255
#pw mpls expectedlabel 7 7.7.255
#pw ethheader 7 C0.CA.C0.CA.C0.CA 0.0.7 none
#pw enable 7
#sdh path timing de1.1.1.1.3.1 system xxx
#pw create satop 8
#pw circuit bind 8 de1.1.1.1.3.2
#pw ethport 8 1
#pw psn 8 mpls
#pw mpls innerlabel 8 8.0.255
#pw mpls expectedlabel 8 8.0.255
#pw ethheader 8 C0.CA.C0.CA.C0.CA 0.0.8 none
#pw enable 8
#sdh path timing de1.1.1.1.3.2 system xxx
#pw create satop 9
#pw circuit bind 9 de1.1.1.1.3.3
#pw ethport 9 1
#pw psn 9 mpls
#pw mpls innerlabel 9 9.1.255
#pw mpls expectedlabel 9 9.1.255
#pw ethheader 9 C0.CA.C0.CA.C0.CA 0.0.9 none
#pw enable 9
#sdh path timing de1.1.1.1.3.3 system xxx
#pw create satop 10
#pw circuit bind 10 de1.1.1.1.4.1
#pw ethport 10 1
#pw psn 10 mpls
#pw mpls innerlabel 10 10.2.255
#pw mpls expectedlabel 10 10.2.255
#pw ethheader 10 C0.CA.C0.CA.C0.CA 0.0.10 none
#pw enable 10
#sdh path timing de1.1.1.1.4.1 system xxx
#pw create satop 11
#pw circuit bind 11 de1.1.1.1.4.2
#pw ethport 11 1
#pw psn 11 mpls
#pw mpls innerlabel 11 11.3.255
#pw mpls expectedlabel 11 11.3.255
#pw ethheader 11 C0.CA.C0.CA.C0.CA 0.0.11 none
#pw enable 11
#sdh path timing de1.1.1.1.4.2 system xxx
#pw create satop 12
#pw circuit bind 12 de1.1.1.1.4.3
#pw ethport 12 1
#pw psn 12 mpls
#pw mpls innerlabel 12 12.4.255
#pw mpls expectedlabel 12 12.4.255
#pw ethheader 12 C0.CA.C0.CA.C0.CA 0.0.12 none
#pw enable 12
#sdh path timing de1.1.1.1.4.3 system xxx
#pw create satop 13
#pw circuit bind 13 de1.1.1.1.5.1
#pw ethport 13 1
#pw psn 13 mpls
#pw mpls innerlabel 13 13.5.255
#pw mpls expectedlabel 13 13.5.255
#pw ethheader 13 C0.CA.C0.CA.C0.CA 0.0.13 none
#pw enable 13
#sdh path timing de1.1.1.1.5.1 system xxx
#pw create satop 14
#pw circuit bind 14 de1.1.1.1.5.2
#pw ethport 14 1
#pw psn 14 mpls
#pw mpls innerlabel 14 14.6.255
#pw mpls expectedlabel 14 14.6.255
#pw ethheader 14 C0.CA.C0.CA.C0.CA 0.0.14 none
#pw enable 14
#sdh path timing de1.1.1.1.5.2 system xxx
#pw create satop 15
#pw circuit bind 15 de1.1.1.1.5.3
#pw ethport 15 1
#pw psn 15 mpls
#pw mpls innerlabel 15 15.7.255
#pw mpls expectedlabel 15 15.7.255
#pw ethheader 15 C0.CA.C0.CA.C0.CA 0.0.15 none
#pw enable 15
#sdh path timing de1.1.1.1.5.3 system xxx
#pw create satop 16
#pw circuit bind 16 de1.1.1.1.6.1
#pw ethport 16 1
#pw psn 16 mpls
#pw mpls innerlabel 16 16.0.255
#pw mpls expectedlabel 16 16.0.255
#pw ethheader 16 C0.CA.C0.CA.C0.CA 0.0.16 none
#pw enable 16
#sdh path timing de1.1.1.1.6.1 system xxx
#pw create satop 17
#pw circuit bind 17 de1.1.1.1.6.2
#pw ethport 17 1
#pw psn 17 mpls
#pw mpls innerlabel 17 17.1.255
#pw mpls expectedlabel 17 17.1.255
#pw ethheader 17 C0.CA.C0.CA.C0.CA 0.0.17 none
#pw enable 17
#sdh path timing de1.1.1.1.6.2 system xxx
#pw create satop 18
#pw circuit bind 18 de1.1.1.1.6.3
#pw ethport 18 1
#pw psn 18 mpls
#pw mpls innerlabel 18 18.2.255
#pw mpls expectedlabel 18 18.2.255
#pw ethheader 18 C0.CA.C0.CA.C0.CA 0.0.18 none
#pw enable 18
#sdh path timing de1.1.1.1.6.3 system xxx
#pw create satop 19
#pw circuit bind 19 de1.1.1.1.7.1
#pw ethport 19 1
#pw psn 19 mpls
#pw mpls innerlabel 19 19.3.255
#pw mpls expectedlabel 19 19.3.255
#pw ethheader 19 C0.CA.C0.CA.C0.CA 0.0.19 none
#pw enable 19
#sdh path timing de1.1.1.1.7.1 system xxx
#pw create satop 20
#pw circuit bind 20 de1.1.1.1.7.2
#pw ethport 20 1
#pw psn 20 mpls
#pw mpls innerlabel 20 20.4.255
#pw mpls expectedlabel 20 20.4.255
#pw ethheader 20 C0.CA.C0.CA.C0.CA 0.0.20 none
#pw enable 20
#sdh path timing de1.1.1.1.7.2 system xxx
#pw create satop 21
#pw circuit bind 21 de1.1.1.1.7.3
#pw ethport 21 1
#pw psn 21 mpls
#pw mpls innerlabel 21 21.5.255
#pw mpls expectedlabel 21 21.5.255
#pw ethheader 21 C0.CA.C0.CA.C0.CA 0.0.21 none
#pw enable 21
#sdh path timing de1.1.1.1.7.3 system xxx
#pw create satop 22
#pw circuit bind 22 de1.1.1.2.1.1
#pw ethport 22 1
#pw psn 22 mpls
#pw mpls innerlabel 22 22.6.255
#pw mpls expectedlabel 22 22.6.255
#pw ethheader 22 C0.CA.C0.CA.C0.CA 0.0.22 none
#pw enable 22
#sdh path timing de1.1.1.2.1.1 system xxx
#pw create satop 23
#pw circuit bind 23 de1.1.1.2.1.2
#pw ethport 23 1
#pw psn 23 mpls
#pw mpls innerlabel 23 23.7.255
#pw mpls expectedlabel 23 23.7.255
#pw ethheader 23 C0.CA.C0.CA.C0.CA 0.0.23 none
#pw enable 23
#sdh path timing de1.1.1.2.1.2 system xxx
#pw create satop 24
#pw circuit bind 24 de1.1.1.2.1.3
#pw ethport 24 1
#pw psn 24 mpls
#pw mpls innerlabel 24 24.0.255
#pw mpls expectedlabel 24 24.0.255
#pw ethheader 24 C0.CA.C0.CA.C0.CA 0.0.24 none
#pw enable 24
#sdh path timing de1.1.1.2.1.3 system xxx
#pw create satop 25
#pw circuit bind 25 de1.1.1.2.2.1
#pw ethport 25 1
#pw psn 25 mpls
#pw mpls innerlabel 25 25.1.255
#pw mpls expectedlabel 25 25.1.255
#pw ethheader 25 C0.CA.C0.CA.C0.CA 0.0.25 none
#pw enable 25
#sdh path timing de1.1.1.2.2.1 system xxx
#pw create satop 26
#pw circuit bind 26 de1.1.1.2.2.2
#pw ethport 26 1
#pw psn 26 mpls
#pw mpls innerlabel 26 26.2.255
#pw mpls expectedlabel 26 26.2.255
#pw ethheader 26 C0.CA.C0.CA.C0.CA 0.0.26 none
#pw enable 26
#sdh path timing de1.1.1.2.2.2 system xxx
#pw create satop 27
#pw circuit bind 27 de1.1.1.2.2.3
#pw ethport 27 1
#pw psn 27 mpls
#pw mpls innerlabel 27 27.3.255
#pw mpls expectedlabel 27 27.3.255
#pw ethheader 27 C0.CA.C0.CA.C0.CA 0.0.27 none
#pw enable 27
#sdh path timing de1.1.1.2.2.3 system xxx
#pw create satop 28
#pw circuit bind 28 de1.1.1.2.3.1
#pw ethport 28 1
#pw psn 28 mpls
#pw mpls innerlabel 28 28.4.255
#pw mpls expectedlabel 28 28.4.255
#pw ethheader 28 C0.CA.C0.CA.C0.CA 0.0.28 none
#pw enable 28
#sdh path timing de1.1.1.2.3.1 system xxx
#pw create satop 29
#pw circuit bind 29 de1.1.1.2.3.2
#pw ethport 29 1
#pw psn 29 mpls
#pw mpls innerlabel 29 29.5.255
#pw mpls expectedlabel 29 29.5.255
#pw ethheader 29 C0.CA.C0.CA.C0.CA 0.0.29 none
#pw enable 29
#sdh path timing de1.1.1.2.3.2 system xxx
#pw create satop 30
#pw circuit bind 30 de1.1.1.2.3.3
#pw ethport 30 1
#pw psn 30 mpls
#pw mpls innerlabel 30 30.6.255
#pw mpls expectedlabel 30 30.6.255
#pw ethheader 30 C0.CA.C0.CA.C0.CA 0.0.30 none
#pw enable 30
#sdh path timing de1.1.1.2.3.3 system xxx
#pw create satop 31
#pw circuit bind 31 de1.1.1.2.4.1
#pw ethport 31 1
#pw psn 31 mpls
#pw mpls innerlabel 31 31.7.255
#pw mpls expectedlabel 31 31.7.255
#pw ethheader 31 C0.CA.C0.CA.C0.CA 0.0.31 none
#pw enable 31
#sdh path timing de1.1.1.2.4.1 system xxx
#pw create satop 32
#pw circuit bind 32 de1.1.1.2.4.2
#pw ethport 32 1
#pw psn 32 mpls
#pw mpls innerlabel 32 32.0.255
#pw mpls expectedlabel 32 32.0.255
#pw ethheader 32 C0.CA.C0.CA.C0.CA 0.0.32 none
#pw enable 32
#sdh path timing de1.1.1.2.4.2 system xxx
#pw create satop 33
#pw circuit bind 33 de1.1.1.2.4.3
#pw ethport 33 1
#pw psn 33 mpls
#pw mpls innerlabel 33 33.1.255
#pw mpls expectedlabel 33 33.1.255
#pw ethheader 33 C0.CA.C0.CA.C0.CA 0.0.33 none
#pw enable 33
#sdh path timing de1.1.1.2.4.3 system xxx
#pw create satop 34
#pw circuit bind 34 de1.1.1.2.5.1
#pw ethport 34 1
#pw psn 34 mpls
#pw mpls innerlabel 34 34.2.255
#pw mpls expectedlabel 34 34.2.255
#pw ethheader 34 C0.CA.C0.CA.C0.CA 0.0.34 none
#pw enable 34
#sdh path timing de1.1.1.2.5.1 system xxx
#pw create satop 35
#pw circuit bind 35 de1.1.1.2.5.2
#pw ethport 35 1
#pw psn 35 mpls
#pw mpls innerlabel 35 35.3.255
#pw mpls expectedlabel 35 35.3.255
#pw ethheader 35 C0.CA.C0.CA.C0.CA 0.0.35 none
#pw enable 35
#sdh path timing de1.1.1.2.5.2 system xxx
#pw create satop 36
#pw circuit bind 36 de1.1.1.2.5.3
#pw ethport 36 1
#pw psn 36 mpls
#pw mpls innerlabel 36 36.4.255
#pw mpls expectedlabel 36 36.4.255
#pw ethheader 36 C0.CA.C0.CA.C0.CA 0.0.36 none
#pw enable 36
#sdh path timing de1.1.1.2.5.3 system xxx
#pw create satop 37
#pw circuit bind 37 de1.1.1.2.6.1
#pw ethport 37 1
#pw psn 37 mpls
#pw mpls innerlabel 37 37.5.255
#pw mpls expectedlabel 37 37.5.255
#pw ethheader 37 C0.CA.C0.CA.C0.CA 0.0.37 none
#pw enable 37
#sdh path timing de1.1.1.2.6.1 system xxx
#pw create satop 38
#pw circuit bind 38 de1.1.1.2.6.2
#pw ethport 38 1
#pw psn 38 mpls
#pw mpls innerlabel 38 38.6.255
#pw mpls expectedlabel 38 38.6.255
#pw ethheader 38 C0.CA.C0.CA.C0.CA 0.0.38 none
#pw enable 38
#sdh path timing de1.1.1.2.6.2 system xxx
#pw create satop 39
#pw circuit bind 39 de1.1.1.2.6.3
#pw ethport 39 1
#pw psn 39 mpls
#pw mpls innerlabel 39 39.7.255
#pw mpls expectedlabel 39 39.7.255
#pw ethheader 39 C0.CA.C0.CA.C0.CA 0.0.39 none
#pw enable 39
#sdh path timing de1.1.1.2.6.3 system xxx
#pw create satop 40
#pw circuit bind 40 de1.1.1.2.7.1
#pw ethport 40 1
#pw psn 40 mpls
#pw mpls innerlabel 40 40.0.255
#pw mpls expectedlabel 40 40.0.255
#pw ethheader 40 C0.CA.C0.CA.C0.CA 0.0.40 none
#pw enable 40
#sdh path timing de1.1.1.2.7.1 system xxx
#pw create satop 41
#pw circuit bind 41 de1.1.1.2.7.2
#pw ethport 41 1
#pw psn 41 mpls
#pw mpls innerlabel 41 41.1.255
#pw mpls expectedlabel 41 41.1.255
#pw ethheader 41 C0.CA.C0.CA.C0.CA 0.0.41 none
#pw enable 41
#sdh path timing de1.1.1.2.7.2 system xxx
#pw create satop 42
#pw circuit bind 42 de1.1.1.2.7.3
#pw ethport 42 1
#pw psn 42 mpls
#pw mpls innerlabel 42 42.2.255
#pw mpls expectedlabel 42 42.2.255
#pw ethheader 42 C0.CA.C0.CA.C0.CA 0.0.42 none
#pw enable 42
#sdh path timing de1.1.1.2.7.3 system xxx
#pw create satop 43
#pw circuit bind 43 de1.1.1.3.1.1
#pw ethport 43 1
#pw psn 43 mpls
#pw mpls innerlabel 43 43.3.255
#pw mpls expectedlabel 43 43.3.255
#pw ethheader 43 C0.CA.C0.CA.C0.CA 0.0.43 none
#pw enable 43
#sdh path timing de1.1.1.3.1.1 system xxx
#pw create satop 44
#pw circuit bind 44 de1.1.1.3.1.2
#pw ethport 44 1
#pw psn 44 mpls
#pw mpls innerlabel 44 44.4.255
#pw mpls expectedlabel 44 44.4.255
#pw ethheader 44 C0.CA.C0.CA.C0.CA 0.0.44 none
#pw enable 44
#sdh path timing de1.1.1.3.1.2 system xxx
#pw create satop 45
#pw circuit bind 45 de1.1.1.3.1.3
#pw ethport 45 1
#pw psn 45 mpls
#pw mpls innerlabel 45 45.5.255
#pw mpls expectedlabel 45 45.5.255
#pw ethheader 45 C0.CA.C0.CA.C0.CA 0.0.45 none
#pw enable 45
#sdh path timing de1.1.1.3.1.3 system xxx
#pw create satop 46
#pw circuit bind 46 de1.1.1.3.2.1
#pw ethport 46 1
#pw psn 46 mpls
#pw mpls innerlabel 46 46.6.255
#pw mpls expectedlabel 46 46.6.255
#pw ethheader 46 C0.CA.C0.CA.C0.CA 0.0.46 none
#pw enable 46
#sdh path timing de1.1.1.3.2.1 system xxx
#pw create satop 47
#pw circuit bind 47 de1.1.1.3.2.2
#pw ethport 47 1
#pw psn 47 mpls
#pw mpls innerlabel 47 47.7.255
#pw mpls expectedlabel 47 47.7.255
#pw ethheader 47 C0.CA.C0.CA.C0.CA 0.0.47 none
#pw enable 47
#sdh path timing de1.1.1.3.2.2 system xxx
#pw create satop 48
#pw circuit bind 48 de1.1.1.3.2.3
#pw ethport 48 1
#pw psn 48 mpls
#pw mpls innerlabel 48 48.0.255
#pw mpls expectedlabel 48 48.0.255
#pw ethheader 48 C0.CA.C0.CA.C0.CA 0.0.48 none
#pw enable 48
#sdh path timing de1.1.1.3.2.3 system xxx
#pw create satop 49
#pw circuit bind 49 de1.1.1.3.3.1
#pw ethport 49 1
#pw psn 49 mpls
#pw mpls innerlabel 49 49.1.255
#pw mpls expectedlabel 49 49.1.255
#pw ethheader 49 C0.CA.C0.CA.C0.CA 0.0.49 none
#pw enable 49
#sdh path timing de1.1.1.3.3.1 system xxx
#pw create satop 50
#pw circuit bind 50 de1.1.1.3.3.2
#pw ethport 50 1
#pw psn 50 mpls
#pw mpls innerlabel 50 50.2.255
#pw mpls expectedlabel 50 50.2.255
#pw ethheader 50 C0.CA.C0.CA.C0.CA 0.0.50 none
#pw enable 50
#sdh path timing de1.1.1.3.3.2 system xxx
#pw create satop 51
#pw circuit bind 51 de1.1.1.3.3.3
#pw ethport 51 1
#pw psn 51 mpls
#pw mpls innerlabel 51 51.3.255
#pw mpls expectedlabel 51 51.3.255
#pw ethheader 51 C0.CA.C0.CA.C0.CA 0.0.51 none
#pw enable 51
#sdh path timing de1.1.1.3.3.3 system xxx
#pw create satop 52
#pw circuit bind 52 de1.1.1.3.4.1
#pw ethport 52 1
#pw psn 52 mpls
#pw mpls innerlabel 52 52.4.255
#pw mpls expectedlabel 52 52.4.255
#pw ethheader 52 C0.CA.C0.CA.C0.CA 0.0.52 none
#pw enable 52
#sdh path timing de1.1.1.3.4.1 system xxx
#pw create satop 53
#pw circuit bind 53 de1.1.1.3.4.2
#pw ethport 53 1
#pw psn 53 mpls
#pw mpls innerlabel 53 53.5.255
#pw mpls expectedlabel 53 53.5.255
#pw ethheader 53 C0.CA.C0.CA.C0.CA 0.0.53 none
#pw enable 53
#sdh path timing de1.1.1.3.4.2 system xxx
#pw create satop 54
#pw circuit bind 54 de1.1.1.3.4.3
#pw ethport 54 1
#pw psn 54 mpls
#pw mpls innerlabel 54 54.6.255
#pw mpls expectedlabel 54 54.6.255
#pw ethheader 54 C0.CA.C0.CA.C0.CA 0.0.54 none
#pw enable 54
#sdh path timing de1.1.1.3.4.3 system xxx
#pw create satop 55
#pw circuit bind 55 de1.1.1.3.5.1
#pw ethport 55 1
#pw psn 55 mpls
#pw mpls innerlabel 55 55.7.255
#pw mpls expectedlabel 55 55.7.255
#pw ethheader 55 C0.CA.C0.CA.C0.CA 0.0.55 none
#pw enable 55
#sdh path timing de1.1.1.3.5.1 system xxx
#pw create satop 56
#pw circuit bind 56 de1.1.1.3.5.2
#pw ethport 56 1
#pw psn 56 mpls
#pw mpls innerlabel 56 56.0.255
#pw mpls expectedlabel 56 56.0.255
#pw ethheader 56 C0.CA.C0.CA.C0.CA 0.0.56 none
#pw enable 56
#sdh path timing de1.1.1.3.5.2 system xxx
#pw create satop 57
#pw circuit bind 57 de1.1.1.3.5.3
#pw ethport 57 1
#pw psn 57 mpls
#pw mpls innerlabel 57 57.1.255
#pw mpls expectedlabel 57 57.1.255
#pw ethheader 57 C0.CA.C0.CA.C0.CA 0.0.57 none
#pw enable 57
#sdh path timing de1.1.1.3.5.3 system xxx
#pw create satop 58
#pw circuit bind 58 de1.1.1.3.6.1
#pw ethport 58 1
#pw psn 58 mpls
#pw mpls innerlabel 58 58.2.255
#pw mpls expectedlabel 58 58.2.255
#pw ethheader 58 C0.CA.C0.CA.C0.CA 0.0.58 none
#pw enable 58
#sdh path timing de1.1.1.3.6.1 system xxx
#pw create satop 59
#pw circuit bind 59 de1.1.1.3.6.2
#pw ethport 59 1
#pw psn 59 mpls
#pw mpls innerlabel 59 59.3.255
#pw mpls expectedlabel 59 59.3.255
#pw ethheader 59 C0.CA.C0.CA.C0.CA 0.0.59 none
#pw enable 59
#sdh path timing de1.1.1.3.6.2 system xxx
#pw create satop 60
#pw circuit bind 60 de1.1.1.3.6.3
#pw ethport 60 1
#pw psn 60 mpls
#pw mpls innerlabel 60 60.4.255
#pw mpls expectedlabel 60 60.4.255
#pw ethheader 60 C0.CA.C0.CA.C0.CA 0.0.60 none
#pw enable 60
#sdh path timing de1.1.1.3.6.3 system xxx
#pw create satop 61
#pw circuit bind 61 de1.1.1.3.7.1
#pw ethport 61 1
#pw psn 61 mpls
#pw mpls innerlabel 61 61.5.255
#pw mpls expectedlabel 61 61.5.255
#pw ethheader 61 C0.CA.C0.CA.C0.CA 0.0.61 none
#pw enable 61
#sdh path timing de1.1.1.3.7.1 system xxx
#pw create satop 62
#pw circuit bind 62 de1.1.1.3.7.2
#pw ethport 62 1
#pw psn 62 mpls
#pw mpls innerlabel 62 62.6.255
#pw mpls expectedlabel 62 62.6.255
#pw ethheader 62 C0.CA.C0.CA.C0.CA 0.0.62 none
#pw enable 62
#sdh path timing de1.1.1.3.7.2 system xxx
#pw create satop 63
#pw circuit bind 63 de1.1.1.3.7.3
#pw ethport 63 1
#pw psn 63 mpls
#pw mpls innerlabel 63 63.7.255
#pw mpls expectedlabel 63 63.7.255
#pw ethheader 63 C0.CA.C0.CA.C0.CA 0.0.63 none
#pw enable 63
#sdh path timing de1.1.1.3.7.3 system xxx
#pw create satop 64
#pw circuit bind 64 de1.2.1.1.1.1
#pw ethport 64 1
#pw psn 64 mpls
#pw mpls innerlabel 64 64.0.255
#pw mpls expectedlabel 64 64.0.255
#pw ethheader 64 C0.CA.C0.CA.C0.CA 0.0.64 none
#pw enable 64
#sdh path timing de1.2.1.1.1.1 system xxx
#pw create satop 65
#pw circuit bind 65 de1.2.1.1.1.2
#pw ethport 65 1
#pw psn 65 mpls
#pw mpls innerlabel 65 65.1.255
#pw mpls expectedlabel 65 65.1.255
#pw ethheader 65 C0.CA.C0.CA.C0.CA 0.0.65 none
#pw enable 65
#sdh path timing de1.2.1.1.1.2 system xxx
#pw create satop 66
#pw circuit bind 66 de1.2.1.1.1.3
#pw ethport 66 1
#pw psn 66 mpls
#pw mpls innerlabel 66 66.2.255
#pw mpls expectedlabel 66 66.2.255
#pw ethheader 66 C0.CA.C0.CA.C0.CA 0.0.66 none
#pw enable 66
#sdh path timing de1.2.1.1.1.3 system xxx
#pw create satop 67
#pw circuit bind 67 de1.2.1.1.2.1
#pw ethport 67 1
#pw psn 67 mpls
#pw mpls innerlabel 67 67.3.255
#pw mpls expectedlabel 67 67.3.255
#pw ethheader 67 C0.CA.C0.CA.C0.CA 0.0.67 none
#pw enable 67
#sdh path timing de1.2.1.1.2.1 system xxx
#pw create satop 68
#pw circuit bind 68 de1.2.1.1.2.2
#pw ethport 68 1
#pw psn 68 mpls
#pw mpls innerlabel 68 68.4.255
#pw mpls expectedlabel 68 68.4.255
#pw ethheader 68 C0.CA.C0.CA.C0.CA 0.0.68 none
#pw enable 68
#sdh path timing de1.2.1.1.2.2 system xxx
#pw create satop 69
#pw circuit bind 69 de1.2.1.1.2.3
#pw ethport 69 1
#pw psn 69 mpls
#pw mpls innerlabel 69 69.5.255
#pw mpls expectedlabel 69 69.5.255
#pw ethheader 69 C0.CA.C0.CA.C0.CA 0.0.69 none
#pw enable 69
#sdh path timing de1.2.1.1.2.3 system xxx
#pw create satop 70
#pw circuit bind 70 de1.2.1.1.3.1
#pw ethport 70 1
#pw psn 70 mpls
#pw mpls innerlabel 70 70.6.255
#pw mpls expectedlabel 70 70.6.255
#pw ethheader 70 C0.CA.C0.CA.C0.CA 0.0.70 none
#pw enable 70
#sdh path timing de1.2.1.1.3.1 system xxx
#pw create satop 71
#pw circuit bind 71 de1.2.1.1.3.2
#pw ethport 71 1
#pw psn 71 mpls
#pw mpls innerlabel 71 71.7.255
#pw mpls expectedlabel 71 71.7.255
#pw ethheader 71 C0.CA.C0.CA.C0.CA 0.0.71 none
#pw enable 71
#sdh path timing de1.2.1.1.3.2 system xxx
#pw create satop 72
#pw circuit bind 72 de1.2.1.1.3.3
#pw ethport 72 1
#pw psn 72 mpls
#pw mpls innerlabel 72 72.0.255
#pw mpls expectedlabel 72 72.0.255
#pw ethheader 72 C0.CA.C0.CA.C0.CA 0.0.72 none
#pw enable 72
#sdh path timing de1.2.1.1.3.3 system xxx
#pw create satop 73
#pw circuit bind 73 de1.2.1.1.4.1
#pw ethport 73 1
#pw psn 73 mpls
#pw mpls innerlabel 73 73.1.255
#pw mpls expectedlabel 73 73.1.255
#pw ethheader 73 C0.CA.C0.CA.C0.CA 0.0.73 none
#pw enable 73
#sdh path timing de1.2.1.1.4.1 system xxx
#pw create satop 74
#pw circuit bind 74 de1.2.1.1.4.2
#pw ethport 74 1
#pw psn 74 mpls
#pw mpls innerlabel 74 74.2.255
#pw mpls expectedlabel 74 74.2.255
#pw ethheader 74 C0.CA.C0.CA.C0.CA 0.0.74 none
#pw enable 74
#sdh path timing de1.2.1.1.4.2 system xxx
#pw create satop 75
#pw circuit bind 75 de1.2.1.1.4.3
#pw ethport 75 1
#pw psn 75 mpls
#pw mpls innerlabel 75 75.3.255
#pw mpls expectedlabel 75 75.3.255
#pw ethheader 75 C0.CA.C0.CA.C0.CA 0.0.75 none
#pw enable 75
#sdh path timing de1.2.1.1.4.3 system xxx
#pw create satop 76
#pw circuit bind 76 de1.2.1.1.5.1
#pw ethport 76 1
#pw psn 76 mpls
#pw mpls innerlabel 76 76.4.255
#pw mpls expectedlabel 76 76.4.255
#pw ethheader 76 C0.CA.C0.CA.C0.CA 0.0.76 none
#pw enable 76
#sdh path timing de1.2.1.1.5.1 system xxx
#pw create satop 77
#pw circuit bind 77 de1.2.1.1.5.2
#pw ethport 77 1
#pw psn 77 mpls
#pw mpls innerlabel 77 77.5.255
#pw mpls expectedlabel 77 77.5.255
#pw ethheader 77 C0.CA.C0.CA.C0.CA 0.0.77 none
#pw enable 77
#sdh path timing de1.2.1.1.5.2 system xxx
#pw create satop 78
#pw circuit bind 78 de1.2.1.1.5.3
#pw ethport 78 1
#pw psn 78 mpls
#pw mpls innerlabel 78 78.6.255
#pw mpls expectedlabel 78 78.6.255
#pw ethheader 78 C0.CA.C0.CA.C0.CA 0.0.78 none
#pw enable 78
#sdh path timing de1.2.1.1.5.3 system xxx
#pw create satop 79
#pw circuit bind 79 de1.2.1.1.6.1
#pw ethport 79 1
#pw psn 79 mpls
#pw mpls innerlabel 79 79.7.255
#pw mpls expectedlabel 79 79.7.255
#pw ethheader 79 C0.CA.C0.CA.C0.CA 0.0.79 none
#pw enable 79
#sdh path timing de1.2.1.1.6.1 system xxx
#pw create satop 80
#pw circuit bind 80 de1.2.1.1.6.2
#pw ethport 80 1
#pw psn 80 mpls
#pw mpls innerlabel 80 80.0.255
#pw mpls expectedlabel 80 80.0.255
#pw ethheader 80 C0.CA.C0.CA.C0.CA 0.0.80 none
#pw enable 80
#sdh path timing de1.2.1.1.6.2 system xxx
#pw create satop 81
#pw circuit bind 81 de1.2.1.1.6.3
#pw ethport 81 1
#pw psn 81 mpls
#pw mpls innerlabel 81 81.1.255
#pw mpls expectedlabel 81 81.1.255
#pw ethheader 81 C0.CA.C0.CA.C0.CA 0.0.81 none
#pw enable 81
#sdh path timing de1.2.1.1.6.3 system xxx
#pw create satop 82
#pw circuit bind 82 de1.2.1.1.7.1
#pw ethport 82 1
#pw psn 82 mpls
#pw mpls innerlabel 82 82.2.255
#pw mpls expectedlabel 82 82.2.255
#pw ethheader 82 C0.CA.C0.CA.C0.CA 0.0.82 none
#pw enable 82
#sdh path timing de1.2.1.1.7.1 system xxx
#pw create satop 83
#pw circuit bind 83 de1.2.1.1.7.2
#pw ethport 83 1
#pw psn 83 mpls
#pw mpls innerlabel 83 83.3.255
#pw mpls expectedlabel 83 83.3.255
#pw ethheader 83 C0.CA.C0.CA.C0.CA 0.0.83 none
#pw enable 83
#sdh path timing de1.2.1.1.7.2 system xxx
#pw create satop 84
#pw circuit bind 84 de1.2.1.1.7.3
#pw ethport 84 1
#pw psn 84 mpls
#pw mpls innerlabel 84 84.4.255
#pw mpls expectedlabel 84 84.4.255
#pw ethheader 84 C0.CA.C0.CA.C0.CA 0.0.84 none
#pw enable 84
#sdh path timing de1.2.1.1.7.3 system xxx
#pw create satop 85
#pw circuit bind 85 de1.2.1.2.1.1
#pw ethport 85 1
#pw psn 85 mpls
#pw mpls innerlabel 85 85.5.255
#pw mpls expectedlabel 85 85.5.255
#pw ethheader 85 C0.CA.C0.CA.C0.CA 0.0.85 none
#pw enable 85
#sdh path timing de1.2.1.2.1.1 system xxx
#pw create satop 86
#pw circuit bind 86 de1.2.1.2.1.2
#pw ethport 86 1
#pw psn 86 mpls
#pw mpls innerlabel 86 86.6.255
#pw mpls expectedlabel 86 86.6.255
#pw ethheader 86 C0.CA.C0.CA.C0.CA 0.0.86 none
#pw enable 86
#sdh path timing de1.2.1.2.1.2 system xxx
#pw create satop 87
#pw circuit bind 87 de1.2.1.2.1.3
#pw ethport 87 1
#pw psn 87 mpls
#pw mpls innerlabel 87 87.7.255
#pw mpls expectedlabel 87 87.7.255
#pw ethheader 87 C0.CA.C0.CA.C0.CA 0.0.87 none
#pw enable 87
#sdh path timing de1.2.1.2.1.3 system xxx
#pw create satop 88
#pw circuit bind 88 de1.2.1.2.2.1
#pw ethport 88 1
#pw psn 88 mpls
#pw mpls innerlabel 88 88.0.255
#pw mpls expectedlabel 88 88.0.255
#pw ethheader 88 C0.CA.C0.CA.C0.CA 0.0.88 none
#pw enable 88
#sdh path timing de1.2.1.2.2.1 system xxx
#pw create satop 89
#pw circuit bind 89 de1.2.1.2.2.2
#pw ethport 89 1
#pw psn 89 mpls
#pw mpls innerlabel 89 89.1.255
#pw mpls expectedlabel 89 89.1.255
#pw ethheader 89 C0.CA.C0.CA.C0.CA 0.0.89 none
#pw enable 89
#sdh path timing de1.2.1.2.2.2 system xxx
#pw create satop 90
#pw circuit bind 90 de1.2.1.2.2.3
#pw ethport 90 1
#pw psn 90 mpls
#pw mpls innerlabel 90 90.2.255
#pw mpls expectedlabel 90 90.2.255
#pw ethheader 90 C0.CA.C0.CA.C0.CA 0.0.90 none
#pw enable 90
#sdh path timing de1.2.1.2.2.3 system xxx
#pw create satop 91
#pw circuit bind 91 de1.2.1.2.3.1
#pw ethport 91 1
#pw psn 91 mpls
#pw mpls innerlabel 91 91.3.255
#pw mpls expectedlabel 91 91.3.255
#pw ethheader 91 C0.CA.C0.CA.C0.CA 0.0.91 none
#pw enable 91
#sdh path timing de1.2.1.2.3.1 system xxx
#pw create satop 92
#pw circuit bind 92 de1.2.1.2.3.2
#pw ethport 92 1
#pw psn 92 mpls
#pw mpls innerlabel 92 92.4.255
#pw mpls expectedlabel 92 92.4.255
#pw ethheader 92 C0.CA.C0.CA.C0.CA 0.0.92 none
#pw enable 92
#sdh path timing de1.2.1.2.3.2 system xxx
#pw create satop 93
#pw circuit bind 93 de1.2.1.2.3.3
#pw ethport 93 1
#pw psn 93 mpls
#pw mpls innerlabel 93 93.5.255
#pw mpls expectedlabel 93 93.5.255
#pw ethheader 93 C0.CA.C0.CA.C0.CA 0.0.93 none
#pw enable 93
#sdh path timing de1.2.1.2.3.3 system xxx
#pw create satop 94
#pw circuit bind 94 de1.2.1.2.4.1
#pw ethport 94 1
#pw psn 94 mpls
#pw mpls innerlabel 94 94.6.255
#pw mpls expectedlabel 94 94.6.255
#pw ethheader 94 C0.CA.C0.CA.C0.CA 0.0.94 none
#pw enable 94
#sdh path timing de1.2.1.2.4.1 system xxx
#pw create satop 95
#pw circuit bind 95 de1.2.1.2.4.2
#pw ethport 95 1
#pw psn 95 mpls
#pw mpls innerlabel 95 95.7.255
#pw mpls expectedlabel 95 95.7.255
#pw ethheader 95 C0.CA.C0.CA.C0.CA 0.0.95 none
#pw enable 95
#sdh path timing de1.2.1.2.4.2 system xxx
#pw create satop 96
#pw circuit bind 96 de1.2.1.2.4.3
#pw ethport 96 1
#pw psn 96 mpls
#pw mpls innerlabel 96 96.0.255
#pw mpls expectedlabel 96 96.0.255
#pw ethheader 96 C0.CA.C0.CA.C0.CA 0.0.96 none
#pw enable 96
#sdh path timing de1.2.1.2.4.3 system xxx
#pw create satop 97
#pw circuit bind 97 de1.2.1.2.5.1
#pw ethport 97 1
#pw psn 97 mpls
#pw mpls innerlabel 97 97.1.255
#pw mpls expectedlabel 97 97.1.255
#pw ethheader 97 C0.CA.C0.CA.C0.CA 0.0.97 none
#pw enable 97
#sdh path timing de1.2.1.2.5.1 system xxx
#pw create satop 98
#pw circuit bind 98 de1.2.1.2.5.2
#pw ethport 98 1
#pw psn 98 mpls
#pw mpls innerlabel 98 98.2.255
#pw mpls expectedlabel 98 98.2.255
#pw ethheader 98 C0.CA.C0.CA.C0.CA 0.0.98 none
#pw enable 98
#sdh path timing de1.2.1.2.5.2 system xxx
#pw create satop 99
#pw circuit bind 99 de1.2.1.2.5.3
#pw ethport 99 1
#pw psn 99 mpls
#pw mpls innerlabel 99 99.3.255
#pw mpls expectedlabel 99 99.3.255
#pw ethheader 99 C0.CA.C0.CA.C0.CA 0.0.99 none
#pw enable 99
#sdh path timing de1.2.1.2.5.3 system xxx
#pw create satop 100
#pw circuit bind 100 de1.2.1.2.6.1
#pw ethport 100 1
#pw psn 100 mpls
#pw mpls innerlabel 100 100.4.255
#pw mpls expectedlabel 100 100.4.255
#pw ethheader 100 C0.CA.C0.CA.C0.CA 0.0.100 none
#pw enable 100
#sdh path timing de1.2.1.2.6.1 system xxx
#pw create satop 101
#pw circuit bind 101 de1.2.1.2.6.2
#pw ethport 101 1
#pw psn 101 mpls
#pw mpls innerlabel 101 101.5.255
#pw mpls expectedlabel 101 101.5.255
#pw ethheader 101 C0.CA.C0.CA.C0.CA 0.0.101 none
#pw enable 101
#sdh path timing de1.2.1.2.6.2 system xxx
#pw create satop 102
#pw circuit bind 102 de1.2.1.2.6.3
#pw ethport 102 1
#pw psn 102 mpls
#pw mpls innerlabel 102 102.6.255
#pw mpls expectedlabel 102 102.6.255
#pw ethheader 102 C0.CA.C0.CA.C0.CA 0.0.102 none
#pw enable 102
#sdh path timing de1.2.1.2.6.3 system xxx
#pw create satop 103
#pw circuit bind 103 de1.2.1.2.7.1
#pw ethport 103 1
#pw psn 103 mpls
#pw mpls innerlabel 103 103.7.255
#pw mpls expectedlabel 103 103.7.255
#pw ethheader 103 C0.CA.C0.CA.C0.CA 0.0.103 none
#pw enable 103
#sdh path timing de1.2.1.2.7.1 system xxx
#pw create satop 104
#pw circuit bind 104 de1.2.1.2.7.2
#pw ethport 104 1
#pw psn 104 mpls
#pw mpls innerlabel 104 104.0.255
#pw mpls expectedlabel 104 104.0.255
#pw ethheader 104 C0.CA.C0.CA.C0.CA 0.0.104 none
#pw enable 104
#sdh path timing de1.2.1.2.7.2 system xxx
#pw create satop 105
#pw circuit bind 105 de1.2.1.2.7.3
#pw ethport 105 1
#pw psn 105 mpls
#pw mpls innerlabel 105 105.1.255
#pw mpls expectedlabel 105 105.1.255
#pw ethheader 105 C0.CA.C0.CA.C0.CA 0.0.105 none
#pw enable 105
#sdh path timing de1.2.1.2.7.3 system xxx
#pw create satop 106
#pw circuit bind 106 de1.2.1.3.1.1
#pw ethport 106 1
#pw psn 106 mpls
#pw mpls innerlabel 106 106.2.255
#pw mpls expectedlabel 106 106.2.255
#pw ethheader 106 C0.CA.C0.CA.C0.CA 0.0.106 none
#pw enable 106
#sdh path timing de1.2.1.3.1.1 system xxx
#pw create satop 107
#pw circuit bind 107 de1.2.1.3.1.2
#pw ethport 107 1
#pw psn 107 mpls
#pw mpls innerlabel 107 107.3.255
#pw mpls expectedlabel 107 107.3.255
#pw ethheader 107 C0.CA.C0.CA.C0.CA 0.0.107 none
#pw enable 107
#sdh path timing de1.2.1.3.1.2 system xxx
#pw create satop 108
#pw circuit bind 108 de1.2.1.3.1.3
#pw ethport 108 1
#pw psn 108 mpls
#pw mpls innerlabel 108 108.4.255
#pw mpls expectedlabel 108 108.4.255
#pw ethheader 108 C0.CA.C0.CA.C0.CA 0.0.108 none
#pw enable 108
#sdh path timing de1.2.1.3.1.3 system xxx
#pw create satop 109
#pw circuit bind 109 de1.2.1.3.2.1
#pw ethport 109 1
#pw psn 109 mpls
#pw mpls innerlabel 109 109.5.255
#pw mpls expectedlabel 109 109.5.255
#pw ethheader 109 C0.CA.C0.CA.C0.CA 0.0.109 none
#pw enable 109
#sdh path timing de1.2.1.3.2.1 system xxx
#pw create satop 110
#pw circuit bind 110 de1.2.1.3.2.2
#pw ethport 110 1
#pw psn 110 mpls
#pw mpls innerlabel 110 110.6.255
#pw mpls expectedlabel 110 110.6.255
#pw ethheader 110 C0.CA.C0.CA.C0.CA 0.0.110 none
#pw enable 110
#sdh path timing de1.2.1.3.2.2 system xxx
#pw create satop 111
#pw circuit bind 111 de1.2.1.3.2.3
#pw ethport 111 1
#pw psn 111 mpls
#pw mpls innerlabel 111 111.7.255
#pw mpls expectedlabel 111 111.7.255
#pw ethheader 111 C0.CA.C0.CA.C0.CA 0.0.111 none
#pw enable 111
#sdh path timing de1.2.1.3.2.3 system xxx
#pw create satop 112
#pw circuit bind 112 de1.2.1.3.3.1
#pw ethport 112 1
#pw psn 112 mpls
#pw mpls innerlabel 112 112.0.255
#pw mpls expectedlabel 112 112.0.255
#pw ethheader 112 C0.CA.C0.CA.C0.CA 0.0.112 none
#pw enable 112
#sdh path timing de1.2.1.3.3.1 system xxx
#pw create satop 113
#pw circuit bind 113 de1.2.1.3.3.2
#pw ethport 113 1
#pw psn 113 mpls
#pw mpls innerlabel 113 113.1.255
#pw mpls expectedlabel 113 113.1.255
#pw ethheader 113 C0.CA.C0.CA.C0.CA 0.0.113 none
#pw enable 113
#sdh path timing de1.2.1.3.3.2 system xxx
#pw create satop 114
#pw circuit bind 114 de1.2.1.3.3.3
#pw ethport 114 1
#pw psn 114 mpls
#pw mpls innerlabel 114 114.2.255
#pw mpls expectedlabel 114 114.2.255
#pw ethheader 114 C0.CA.C0.CA.C0.CA 0.0.114 none
#pw enable 114
#sdh path timing de1.2.1.3.3.3 system xxx
#pw create satop 115
#pw circuit bind 115 de1.2.1.3.4.1
#pw ethport 115 1
#pw psn 115 mpls
#pw mpls innerlabel 115 115.3.255
#pw mpls expectedlabel 115 115.3.255
#pw ethheader 115 C0.CA.C0.CA.C0.CA 0.0.115 none
#pw enable 115
#sdh path timing de1.2.1.3.4.1 system xxx
#pw create satop 116
#pw circuit bind 116 de1.2.1.3.4.2
#pw ethport 116 1
#pw psn 116 mpls
#pw mpls innerlabel 116 116.4.255
#pw mpls expectedlabel 116 116.4.255
#pw ethheader 116 C0.CA.C0.CA.C0.CA 0.0.116 none
#pw enable 116
#sdh path timing de1.2.1.3.4.2 system xxx
#pw create satop 117
#pw circuit bind 117 de1.2.1.3.4.3
#pw ethport 117 1
#pw psn 117 mpls
#pw mpls innerlabel 117 117.5.255
#pw mpls expectedlabel 117 117.5.255
#pw ethheader 117 C0.CA.C0.CA.C0.CA 0.0.117 none
#pw enable 117
#sdh path timing de1.2.1.3.4.3 system xxx
#pw create satop 118
#pw circuit bind 118 de1.2.1.3.5.1
#pw ethport 118 1
#pw psn 118 mpls
#pw mpls innerlabel 118 118.6.255
#pw mpls expectedlabel 118 118.6.255
#pw ethheader 118 C0.CA.C0.CA.C0.CA 0.0.118 none
#pw enable 118
#sdh path timing de1.2.1.3.5.1 system xxx
#pw create satop 119
#pw circuit bind 119 de1.2.1.3.5.2
#pw ethport 119 1
#pw psn 119 mpls
#pw mpls innerlabel 119 119.7.255
#pw mpls expectedlabel 119 119.7.255
#pw ethheader 119 C0.CA.C0.CA.C0.CA 0.0.119 none
#pw enable 119
#sdh path timing de1.2.1.3.5.2 system xxx
#pw create satop 120
#pw circuit bind 120 de1.2.1.3.5.3
#pw ethport 120 1
#pw psn 120 mpls
#pw mpls innerlabel 120 120.0.255
#pw mpls expectedlabel 120 120.0.255
#pw ethheader 120 C0.CA.C0.CA.C0.CA 0.0.120 none
#pw enable 120
#sdh path timing de1.2.1.3.5.3 system xxx
#pw create satop 121
#pw circuit bind 121 de1.2.1.3.6.1
#pw ethport 121 1
#pw psn 121 mpls
#pw mpls innerlabel 121 121.1.255
#pw mpls expectedlabel 121 121.1.255
#pw ethheader 121 C0.CA.C0.CA.C0.CA 0.0.121 none
#pw enable 121
#sdh path timing de1.2.1.3.6.1 system xxx
#pw create satop 122
#pw circuit bind 122 de1.2.1.3.6.2
#pw ethport 122 1
#pw psn 122 mpls
#pw mpls innerlabel 122 122.2.255
#pw mpls expectedlabel 122 122.2.255
#pw ethheader 122 C0.CA.C0.CA.C0.CA 0.0.122 none
#pw enable 122
#sdh path timing de1.2.1.3.6.2 system xxx
#pw create satop 123
#pw circuit bind 123 de1.2.1.3.6.3
#pw ethport 123 1
#pw psn 123 mpls
#pw mpls innerlabel 123 123.3.255
#pw mpls expectedlabel 123 123.3.255
#pw ethheader 123 C0.CA.C0.CA.C0.CA 0.0.123 none
#pw enable 123
#sdh path timing de1.2.1.3.6.3 system xxx
#pw create satop 124
#pw circuit bind 124 de1.2.1.3.7.1
#pw ethport 124 1
#pw psn 124 mpls
#pw mpls innerlabel 124 124.4.255
#pw mpls expectedlabel 124 124.4.255
#pw ethheader 124 C0.CA.C0.CA.C0.CA 0.0.124 none
#pw enable 124
#sdh path timing de1.2.1.3.7.1 system xxx
#pw create satop 125
#pw circuit bind 125 de1.2.1.3.7.2
#pw ethport 125 1
#pw psn 125 mpls
#pw mpls innerlabel 125 125.5.255
#pw mpls expectedlabel 125 125.5.255
#pw ethheader 125 C0.CA.C0.CA.C0.CA 0.0.125 none
#pw enable 125
#sdh path timing de1.2.1.3.7.2 system xxx
#pw create satop 126
#pw circuit bind 126 de1.2.1.3.7.3
#pw ethport 126 1
#pw psn 126 mpls
#pw mpls innerlabel 126 126.6.255
#pw mpls expectedlabel 126 126.6.255
#pw ethheader 126 C0.CA.C0.CA.C0.CA 0.0.126 none
#pw enable 126
#sdh path timing de1.2.1.3.7.3 system xxx
#pw create satop 127
#pw circuit bind 127 de1.3.1.1.1.1
#pw ethport 127 1
#pw psn 127 mpls
#pw mpls innerlabel 127 127.7.255
#pw mpls expectedlabel 127 127.7.255
#pw ethheader 127 C0.CA.C0.CA.C0.CA 0.0.127 none
#pw enable 127
#sdh path timing de1.3.1.1.1.1 system xxx
pw create satop 128
pw circuit bind 128 de1.3.1.1.1.2
pw ethport 128 1
pw psn 128 mpls
pw mpls innerlabel 128 128.0.255
pw mpls expectedlabel 128 128.0.255
pw ethheader 128 C0.CA.C0.CA.C0.CA 0.0.128 none
pw enable 128
sdh path timing de1.3.1.1.1.2 system xxx
pw create satop 129
pw circuit bind 129 de1.3.1.1.1.3
pw ethport 129 1
pw psn 129 mpls
pw mpls innerlabel 129 129.1.255
pw mpls expectedlabel 129 129.1.255
pw ethheader 129 C0.CA.C0.CA.C0.CA 0.0.129 none
pw enable 129
sdh path timing de1.3.1.1.1.3 system xxx
eth port loopback 1 local
show pw counters 1-129 r2c

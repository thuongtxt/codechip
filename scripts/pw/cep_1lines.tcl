atsdk::device init

set lineId 1
set vc4s "$lineId.1"
set pwId  $lineId

# Configure SDH
atsdk::sdh line rate $pwId stm1
atsdk::sdh map "aug1.$vc4s vc4"

# Configure PW
atsdk::pw create cep $pwId
atsdk::pw circuit bind "$pwId vc4.$vc4s"
atsdk::pw ethport $pwId 1

# Ethernet header
atsdk::eth port srcmac 1 C0.CA.C0.CA.C0.CA
atsdk::pw ethheader $pwId C0.CA.C0.CA.C0.CA "$pwId.0.$pwId" none

# MPLS PSN
atsdk::pw psn $pwId mpls
atsdk::pw mpls innerlabel $pwId "$pwId.1.1"
atsdk::pw mpls outerlabel add  $pwId "$pwId.1.1"
atsdk::pw mpls outerlabel add  $pwId "[expr $pwId + 1].1.1"

# MEF PSN
#atsdk::pw psn $pwId mef
#atsdk::pw mef ecid transmit $pwId "$pwId"
#atsdk::pw mef ecid expect $pwId "$pwId"

# UDP/IPv4 PSN
#atsdk::pw psn $pwId udp.ipv4
#atsdk::pw ipv4 source 1 172.33.44.107
#atsdk::pw ipv4 dest   1 172.33.44.107
#atsdk::pw udp  source 1 unused
#atsdk::pw udp  dest   1 123

# Enable PW
atsdk::pw enable $pwId

# Loopback for unit-testing
atsdk::eth port loopback 1 local

# Timing
atsdk::sdh path timing vc4.$vc4s epar xxx

after 1000
atsdk::show pw $pwId
atsdk::show pw psn $pwId
atsdk::show pw controlword $pwId
atsdk::show pw rtp $pwId
atsdk::show pw counters $pwId r2c

set numPws 1
set startPw 3
set stopPw [expr $startPw + $numPws - 1]
set pws "$startPw-$stopPw"
set de1s $pws

set frameMode e1_unframed

atsdk::device init

# Loop PDH
atsdk::pdh de1 loopback $de1s lineloopin

# Ethernet header
atsdk::eth port srcmac 1 C0.CA.C0.CA.C0.CA
atsdk::eth port loopback 1 local

# Setup PW
# Configure PW
for {set pw_i 0} {$pw_i < $numPws} {incr pw_i} {
	set pwId [expr $pw_i + $startPw]
	atsdk::pw create satop $pwId
	atsdk::pw circuit bind "$pwId de1.$pwId"
	atsdk::pw ethport $pwId 1
	atsdk::pw ethheader $pwId C0.CA.C0.CA.C0.CA "[expr $pwId % 8].[expr $pwId % 2].$pwId" none

	# MPLS PSN
	atsdk::pw psn $pwId mpls
	atsdk::pw mpls innerlabel $pwId "$pwId.[expr $pwId % 8].255"
	
	# Enable PW
	atsdk::pw enable $pwId
}

after 1000
atsdk::show pw $pws
atsdk::show pw psn $pws
atsdk::show pw controlword $pws
atsdk::show pw rtp $pws
atsdk::show pw counters $pws r2c

source sdh_util.tcl

set numberLine 1
set lineRate stm1
proc frameMode {} { return e1_unframed }

proc numAug1s {} {
	global lineRate
	return [numAug1sByRate $lineRate]
}

# Setup mapping
proc setMappingForLine { lineId } {
	atsdk::sdh map "aug1.$lineId.1-$lineId.[numAug1s]" vc4
	atsdk::sdh map "vc4.$lineId.1-$lineId.[numAug1s]" 3xtug3s
	atsdk::sdh map "tug3.$lineId.1.1-$lineId.[numAug1s].3" 7xtug2s
	atsdk::sdh map "tug2.$lineId.1.1.1-$lineId.[numAug1s].3.7" tu12
	atsdk::sdh map "vc1x.$lineId.1.1.1.1-$lineId.[numAug1s].3.7.3" de1
}

# Init sdk
atsdk::device init

# Setup SDH/SONET
for {set lineId 1} {$lineId <= $numberLine} {incr lineId} {
	configureLine $lineId $lineRate
	setMappingForLine $lineId
		
	# Configure all paths
        configurePath "vc4.$lineId.1-$lineId.[numAug1s]"
	configurePath "vc1x.$lineId.1.1.1.1-$lineId.[numAug1s].3.7.3"
}	

# Setup Ethernet port
set ethPort 1
atsdk::eth port srcmac $ethPort CA.FE.CA.FE.CA.FE

#Setup pseudo-wire
set numberPw  $numberLine

for {set pwId 1} {$pwId < $numberPw} {incr pwId} {
	atsdk::pw create satop $pwId
	atsdk::pw circuit bind "$pwId vc1x.$pwId.1.1.1.1"
	atsdk::pw psn $pwId mpls
	atsdk::pw ethport $pwId 1
	atsdk::pw ethheader $pwId CA.FE.CA.FE.CA.FE "$pwId.0.$pwId" none
	atsdk::pw mpls innerlabel $pwId "$pwId.$pwId.$pwId"	
	atsdk::pw priority $pwId 1
	atsdk::pw controlword enable $pwId
	atsdk::pw controlword sequence 1 wrapzero
	atsdk::pw controlword length 1 fullpkt
	atsdk::pw controlword pktreplace 1 ais
	atsdk::pw enable $pwId
}

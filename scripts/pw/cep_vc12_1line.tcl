atsdk::device init

set line 1
set timingMode system

# Set mapping
atsdk::sdh line rate $line stm1
atsdk::sdh map aug1.$line.1 3xvc3s
atsdk::sdh map vc3.$line.1.1-$line.1.3 7xtug2s
atsdk::sdh map tug2.$line.1.1.1-$line.1.3.7 tu12

atsdk::eth port srcmac 1 C0.CA.C0.CA.C0.CA

set numPws 63
set pw_i 1
for {set tug3Id 1} {$tug3Id <= 3} {incr tug3Id} {
	for {set tug2Id 1} {$tug2Id <= 7} {incr tug2Id} {
		for {set tuId 1} {$tuId <= 3} {incr tuId} {
			# Create PW
			set circuit "vc1x.$line.1.$tug3Id.$tug2Id.$tuId"
			atsdk::pw create cep $pw_i
			atsdk::pw circuit bind "$pw_i $circuit"
			atsdk::pw psn $pw_i mpls
			atsdk::pw ethport $pw_i 1

			# PSN			
			atsdk::pw ethheader $pw_i C0.CA.C0.CA.C0.CA "[expr $pw_i % 8].0.$pw_i" none
			atsdk::pw mpls innerlabel $pw_i "$pw_i.[expr $pw_i % 8].$pw_i"
			atsdk::pw enable $pw_i
			
			# Timing mode
			atsdk::sdh path timing $circuit $timingMode xxx
			
			incr pw_i
			if {$pw_i > $numPws} { break }
			
		}
		if {$pw_i > $numPws} { break }
	}
	if {$pw_i > $numPws} { break }
}

atsdk::eth port loopback 1 local

# Clear status
set pws 1-$numPws
after 1000
atsdk::show pw counters $pws r2c
after 1000

# Show status and configuration
atsdk::show pw $pws
atsdk::show pw psn $pws
atsdk::show pw controlword $pws
atsdk::show pw rtp $pws
atsdk::show pw counters $pws r2c
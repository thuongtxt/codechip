atsdk::device init

#===============================================================================
# Configuration section
#===============================================================================
# ID: changable
set startPw 1
set numPws 129
set startLine 1
set numLine 4

# AUG-1 mapping: 3xvc3s, vc4 
set aug1MapType vc4

# Other configuration (changable)
set timingMode system
set frameMode e1_unframed

# Lookup mode: psn, tags (changable)
set lookupMode "psn"

#===============================================================================
# Program section
#===============================================================================
# Set mapping
set lineCounts 0
for {set line $startLine} {$lineCounts < $numLine} {incr line} {
	atsdk::sdh line rate $line stm1
	atsdk::sdh map aug1.$line.1 $aug1MapType
	
	if {$aug1MapType == "3xvc3s"} {
		atsdk::sdh map vc3.$line.1.1-$line.1.3 7xtug2s
	} else {
		atsdk::sdh map vc4.$line.1 3xtug3s
		atsdk::sdh map tug3.$line.1.1-$line.1.3 7xtug2s
	}
		
	atsdk::sdh map tug2.$line.1.1.1-$line.1.3.7 tu12
	atsdk::sdh map vc1x.$line.1.1.1.1-$line.1.3.7.3 de1
	incr lineCounts
}

atsdk::eth port srcmac 1 C0.CA.C0.CA.C0.CA
set pwCounts 0
set pw_i $startPw
set lineCounts 0
for {set line $startLine} {$lineCounts < $numLine} {incr line} {
	for {set tug3Id 1} {$tug3Id <= 3} {incr tug3Id} {
		for {set tug2Id 1} {$tug2Id <= 7} {incr tug2Id} {
			for {set tuId 1} {$tuId <= 3} {incr tuId} {
				# Create PW
				set de1Id "$line.1.$tug3Id.$tug2Id.$tuId"
				set circuit "de1.$de1Id"
				atsdk::pw create satop $pw_i
				atsdk::pw circuit bind "$pw_i $circuit"
				
				atsdk::pw ethport $pw_i 1
	
				# Use ZTE tags
				if {$lookupMode == "tags"} {
					# ZTE tags (common)
					set priority [expr $pw_i % 8]
					set cfi [expr $pw_i % 2]
					
					# Tag 1
					set cpuPktIndicator 0
					set encapType 10
					set pktLen 0
					set tag1 "$priority.$cfi.$cpuPktIndicator.$encapType.$pktLen"
					
					# Tag 2
					set stmPort [expr $line - 1]
					set mlppp 0
					set channelId [expr $pw_i - 1]
					set tag2 "$priority.$cfi.$stmPort.$mlppp.$channelId"
					
					# Apply these two tags
					atsdk::zte pw ethheader $pw_i C0.CA.C0.CA.C0.CA $tag1 $tag2
					atsdk::zte pw tags expect $pw_i $tag1 $tag2
				} else {
					atsdk::pw psn $pw_i mpls
					set mplsLabel "$pw_i.[expr $pw_i % 8].255"	
					atsdk::pw mpls innerlabel $pw_i $mplsLabel
					atsdk::pw mpls expectedlabel $pw_i $mplsLabel
					atsdk::pw ethheader $pw_i C0.CA.C0.CA.C0.CA 0.0.$pw_i none
				}
				
				# Other configurations
				atsdk::pw jitterbuffer $pw_i 32000
				atsdk::pw jitterdelay $pw_i 16000
				atsdk::pw enable $pw_i
				atsdk::pdh de1 timing $de1Id $timingMode xxx
				
				incr pw_i
				incr pwCounts
				if {$pwCounts >= $numPws} { break }
			}
			if {$pwCounts >= $numPws} { break }
		}
		if {$pwCounts >= $numPws} { break }
	}
	incr lineCounts
	if {$pwCounts >= $numPws} { break }
}

atsdk::eth port loopback 1 local

# Clear status
set pws $startPw-[expr $startPw + $numPws - 1]
after 1000
atsdk::show pw counters $pws r2c
after 1000

# Show status and configuration
atsdk::show pw $pws
atsdk::show pw psn $pws
atsdk::show pw controlword $pws
atsdk::show pw rtp $pws
atsdk::show pw counters $pws r2c

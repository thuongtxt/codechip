atsdk::device init

set numLines 2
set vc4s "1.1,2.1"
set pws  1-2

# Configure SDH
atsdk::sdh line rate $pws stm1
atsdk::sdh map "aug1.$vc4s vc4"

# Configure PW
atsdk::pw create cep $pws
atsdk::pw circuit bind "$pws vc4.$vc4s"
atsdk::pw psn $pws mpls
atsdk::pw ethport $pws 1

for {set pw_i 1} {$pw_i <= $numLines} {incr pw_i} {
	atsdk::pw ethheader $pw_i C0.CA.C0.CA.C0.CA "$pw_i.0.$pw_i" none
	atsdk::pw mpls innerlabel $pw_i "$pw_i.$pw_i.$pw_i"
	atsdk::pw enable $pw_i
}

atsdk::eth port srcmac 1 C0.CA.C0.CA.C0.CA
atsdk::eth port loopback 1 local
after 1000

atsdk::show pw $pws
atsdk::show pw psn $pws
atsdk::show pw controlword $pws
atsdk::show pw rtp $pws
atsdk::show pw counters $pws r2c

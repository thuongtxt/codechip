set numPws 1
set startPw 1
set stopPw [expr $startPw + $numPws - 1]
set pws "$startPw-$stopPw"
set de1s $pws

set frameMode e1_unframed

atsdk::device init

# Loop PDH
atsdk::pdh de1 loopback $de1s lineloopin

# Ethernet header
atsdk::eth port srcmac 1 C0.CA.C0.CA.C0.CA
atsdk::eth port loopback 1 local

# Setup PW
# Configure PW
set pwCount 0
for {set pw_i $startPw} {$pwCount < $numPws} {incr pw_i} {
	atsdk::pw create satop $pw_i
	atsdk::pw circuit bind "$pw_i de1.$pw_i"
	atsdk::pw ethport $pw_i 1
	
	# ZTE tags (common)
	set priority [expr $pw_i % 8]
	set cfi [expr $pw_i % 2]
	
	# Tag 1
	set cpuPktIndicator 0
	set encapType 10
	set pktLen 0
	set tag1 "$priority.$cfi.$cpuPktIndicator.$encapType.$pktLen"
	
	# Tag 2
	set stmPort 0
	set mlppp 0
	set channelId [expr $pw_i - 1]
	set tag2 "$priority.$cfi.$stmPort.$mlppp.$channelId"
	
	# Apply these two tags
	atsdk::zte pw ethheader $pw_i C0.CA.C0.CA.C0.CA $tag1 $tag2
	atsdk::zte pw tags expect $pw_i $tag1 $tag2
				
	# Enable PW
	atsdk::pw enable $pw_i
	
	incr pwCount
}

after 1000
atsdk::show pw $pws
atsdk::show pw psn $pws
atsdk::show pw controlword $pws
atsdk::show pw rtp $pws
atsdk::show pw counters $pws r2c

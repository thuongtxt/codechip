source sdh_util.tcl

set lineId 1
set lineRate stm4
proc frameMode {} { return e1_unframed }

proc numAug1s {} {
	global lineRate
	return [numAug1sByRate $lineRate]
}

# Test mapping CLIs
proc setMappingForLine { lineId } {
	atsdk::sdh map "aug1.$lineId.1-$lineId.[numAug1s]" vc4
	atsdk::sdh map "vc4.$lineId.1-$lineId.[numAug1s]" 3xtug3s
	atsdk::sdh map "tug3.$lineId.1.1-$lineId.[numAug1s].3" 7xtug2s
	atsdk::sdh map "tug2.$lineId.1.1.1-$lineId.[numAug1s].3.7" tu12
	atsdk::sdh map "vc1x.$lineId.1.1.1.1-$lineId.[numAug1s].3.7.3" de1
} 

proc createAllLinks {lineId} {
	set de1s "$lineId.1.1.1.1-$lineId.[numAug1s].3.7.3"
	configureDe1 $de1s [frameMode]
	
	set numLinks [expr [numAug1s] * 3 * 21]
	set links "1-$numLinks"
	atsdk::encap channel create $links ppp
	atsdk::encap channel bind $links "de1.$de1s"
}

proc showStatus {lineId} {
	showLineStatus $lineId
	showPathStatus "vc4.$lineId.1-$lineId.[numAug1s]" 
	showPathStatus "vc1x.$lineId.1.1.1.1-$lineId.[numAug1s].3.7.3"  
	
	# Show PDH
	showDe1Status "$lineId.1.1.1.1-$lineId.[numAug1s].3.7.3" 
}

# Test STM-1
atsdk::device init

# Hardware has problem on DDR that causes Jn messages all wrong. Disable Ethernet traffic
# to so that Jn can be tested
atsdk::wr 0x4022E 0

# Loopout OCN to separate TDM site
atsdk::wr 0x4000B 1

# Temporary disable TIM function, hardware does not work
configureLine $lineId $lineRate
setMappingForLine $lineId
#createAllLinks $lineId

# Configure all paths
configurePath "vc4.$lineId.1-$lineId.[numAug1s]"
configurePath "vc1x.$lineId.1.1.1.1-$lineId.[numAug1s].3.7.3"

showStatus $lineId
after 1
showStatus $lineId

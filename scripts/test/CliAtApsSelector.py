from python.arrive.AtAppManager.AtAppManager import *
import xmlrunner
import unittest

class CliAtApsSeletor(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        return AtAppManager.localApp().runCli("device init")
    
    @staticmethod
    def runCli(cli):
        return AtAppManager.localApp().runCli(cli)
    
    def assertCliSuccess(self, cli):
        result = self.runCli(cli)
        self.assertTrue(result.success(), "Run CLI \"%s\" fail" % cli)
        
    def assertCliFailure(self, cli):
        result = self.runCli(cli)
        self.assertFalse(result.success(), "Run CLI \"%s\" must be fail" % cli)
        
    def createApsSelectorWithVc4PathOn8FacePlateLines(self):
        #Create path
        self.assertCliSuccess("sdh line rate 1-8 stm4")
        self.assertCliSuccess("sdh map aug4.1.1-8.1 4xaug1s")
        self.assertCliSuccess("sdh map aug1.1.1-8.4 vc4")
        #Create engine
        for line in range(1, 5):
            for vc4 in range(1, 5):
                self.assertCliSuccess("aps selector create %d vc4.%d.%d vc4.%d.%d vc4.%d.%d" % (((line-1)*4+vc4), line, vc4, (line+4), vc4, line, vc4))

    def checkApsSelectorEnable(self, numRow, isEnabel):
        for engine in range(0, numRow):
            self.assertCliSuccess("aps selector %s %d" % (isEnabel, (engine+1)))
        #Check
        result = self.runCli("show aps selector 1-32")
        self.assertEqual(result.numRows(), numRow, None)
        if isEnabel == "enable":
            getEnable = "en"
        else:
            getEnable = "dis"
        for row in range(0, numRow):
            self.assertEqual(result.cellContent(row, 1), "%s" % getEnable, None)

    def checkApsSelectorChannelSelect(self, numRow):
        #Set
        for engine in range(0, numRow):
            self.assertCliSuccess("aps selector channel %d vc4.%d.%d" % ((engine+1), ((engine/4)+1),((engine%4)+1)))
        #Check
        result = self.runCli("show aps selector 1-32")
        self.assertEqual(result.numRows(), numRow, None)
        for row in range(0, numRow):
            self.assertEqual(result.cellContent(row, 3), "vc4.%d.%d" %(((row/4)+5),((row%4)+1)), None)
    
    def testApsSelectorMustBeCreatedAndDeletedProperly(self):
        self.createApsSelectorWithVc4PathOn8FacePlateLines()
        result = self.runCli("show aps selector 1-32")        
        self.assertEqual(result.numRows(), 16, None)
        for row in range(0, 16):
            self.assertEqual(result.cellContent(row, 0), "%d" % (row + 1), None)
            self.assertEqual(result.cellContent(row, 1), "dis", None)
            self.assertEqual(result.cellContent(row, 2), "vc4.%d.%d" % ((row/4)+1,(row%4)+1), None)
            self.assertEqual(result.cellContent(row, 3), "vc4.%d.%d" % ((row/4)+5,(row%4)+1), None)
            self.assertEqual(result.cellContent(row, 4), "vc4.%d.%d" % ((row/4)+1,(row%4)+1), None)
            
        self.assertCliSuccess("aps selector delete 5-7,10")
        
        result = self.runCli("show aps selector 1-32")
        self.assertEqual(result.numRows(), 12, None)
        for row in range(0, 16):
            groupId = result.cellContent(row, 0)
            self.assertFalse((groupId == "5") or (groupId == "6") or (groupId == "7") or (groupId == "10"), None)
            
        self.assertCliSuccess("aps selector delete 1-32")

    def testApsSeletorEnable(self):
        self.createApsSelectorWithVc4PathOn8FacePlateLines()
        #Test
        self.checkApsSelectorEnable(16, "enable")
        self.checkApsSelectorEnable(16, "disable")

        # Cleanup
        self.assertCliSuccess("aps selector delete 1-32")

    def testApsSeletorChannelSelect(self):
        self.createApsSelectorWithVc4PathOn8FacePlateLines()
        self.checkApsSelectorChannelSelect(16)
        self.assertCliSuccess("aps selector delete 1-32")

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(CliAtApsSeletor)
    xmlrunner.XMLTestRunner().run(suite)

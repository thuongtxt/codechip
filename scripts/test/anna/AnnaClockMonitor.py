import random

from test.AtTestCase import AtTestCaseRunner
from test.anna.AnnaTestCase import AnnaMroTestCase

class AbstractTest(AnnaMroTestCase):
    @classmethod
    def portsCliId(cls):
        return "Invalid Port"

    @staticmethod
    def thresholdFromString(string):
        """
        :type string: str
        :rtype: int, int
        """
        string = string.replace("(max)", "")
        [valueString, maxString] = string.split("/")
        return int(valueString), int(maxString)

    @staticmethod
    def thresholdTypes():
        return [
            "loss-of-clock",
            "frequency-out-of-range"
        ]

    def thresholdInfo(self, thresholdType):
        """
        :rtype: int, int
        """
        cli = "show eth port clock monitor %s" % self.portsCliId()
        result = self.runCli(cli)
        cellContent = result.cellContent(0, "%s-threshold(ppm)" % thresholdType)
        return self.thresholdFromString(cellContent)

    def setThreshold(self, thresholdType, value):
        cli = "eth port clock monitor threshold %s %s %d" % (self.portsCliId(), thresholdType, value)
        self.assertCliSuccess(cli)

class EthClockMonitorTest(AbstractTest):
    @classmethod
    def portsCliId(cls):
        return "2"

    @classmethod
    def setUpClass(cls):
        cls.classRunCli("device init")
        cls.classRunCli("serdes mode 1 10G")

    def testDefaultThresholds(self):
        self.assertCliSuccess("device init")
        self.assertCliSuccess("serdes mode 1 10G")
        defaultThresholds = {"loss-of-clock": 150,
                             "frequency-out-of-range": 100}
        for thresholdType in self.thresholdTypes():
            value, _ = self.thresholdInfo(thresholdType)
            self.assertEqual(value, defaultThresholds[thresholdType])

    def TestThreshold(self, thresholdType):
        _, maxValue = self.thresholdInfo(thresholdType)
        for threshold in random.sample(range(1, maxValue), 10):
            self.setThreshold(thresholdType, threshold)
            value, _ = self.thresholdInfo(thresholdType)
            self.assertEqual(value, threshold)
            self.assertLessEqual(value, maxValue)

    def testInRangeThresholdSet(self):
        for thresholdType in self.thresholdTypes():
            self.TestThreshold(thresholdType)

    def testOutOfRangeThresholdSet(self):
        for thresholdType in self.thresholdTypes():
            _, maxValue = self.thresholdInfo(thresholdType)
            def Execute():
                self.setThreshold(thresholdType, maxValue + 1)
            self.assertRaises(Exception, Execute)

    def testCanDisplayCurrentPpm(self):
        cli = "show eth port clock monitor %s" % self.portsCliId()
        result = self.runCli(cli)
        self.assertGreaterEqual(int(result.cellContent(0, "current(ppm)")), 0)

class OnlySupportFor10gTest(AbstractTest):
    @classmethod
    def portsCliId(cls):
        return "2-%d" % (cls.numFaceplatePorts() + 1)

    @classmethod
    def setUpClass(cls):
        cls.classRunCli("device init")
        cls.classRunCli("serdes mode 1-%d 1G" % cls.numFaceplatePorts())

    def testCannotSetThreshold(self):
        for thresholdType in self.thresholdTypes():
            def Execute():
                self.setThreshold(thresholdType, 2)
            self.assertRaises(Exception, Execute)

    def testNoClockMonitorWouldBeDisplayed(self):
        cli = "show eth port clock monitor %s" % self.portsCliId()
        result = self.runCli(cli)
        self.assertEqual(result.numRows(), 0)

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(EthClockMonitorTest)
    runner.run(OnlySupportFor10gTest)
    runner.summary()

if __name__ == '__main__':
    TestMain()

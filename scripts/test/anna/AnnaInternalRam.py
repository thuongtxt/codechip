from test.AtTestCase import AtTestCaseRunner
from test.anna.AnnaTestCase import AnnaMroTestCase

class InternalRamTest(AnnaMroTestCase):
    @classmethod
    def canRun(cls):
        return cls.is20G()

    @staticmethod
    def supportedModules():
        return ["ocn", "poh", "cdr", "sur", "pdh", "map", "pwe", "cla", "pda"]
    
    def ramIdListOnModule(self, moduleName):
        ramIdList = []
        cliResult = self.runCli("show ram internal " + moduleName)
        numRows = cliResult.numRows()
        for row in range(0, numRows):
            ramIdList.append(int(cliResult.cellContent(row, "RamId")))
        return ramIdList
    
    def forceErrorTypeIsSupported(self, ramId, errorType):
        cliResult = self.classRunCli("show ram internal " + str(ramId))
        
        if errorType == "parity":
            if cliResult.cellContent(0, "Parity Mon") == "N/S":
                return False
            else:
                return True
            
        if errorType == "crc":
            if cliResult.cellContent(0, "CRC Mon") == "N/S":
                return False
            else:
                return True
        
        return False
    
    @staticmethod
    def supportedForceErrorTypes():
        return ["parity", "crc", "ecc-correctable", "ecc-uncorrectable"]
    
    def ramIsCrc(self, ramId):
        cliResult = self.classRunCli("show ram internal " + str(ramId))
        if "CRC" in cliResult.cellContent(0, "Description"):
            return True
            
        if cliResult.cellContent(0, "CRC Mon") == "en":
            return True
        
        return False
    
    def testForceError(self):
        def TestForceErrorOnRam(aRamId):
            for errorType in self.supportedForceErrorTypes():
                if not self.forceErrorTypeIsSupported(aRamId, errorType):
                    continue
                self.assertCliSuccess("ram internal force error " + str(aRamId) + ' ' + errorType + " en")
                cliResult = self.classRunCli("show ram internal " + str(aRamId))
                self.assertEqual(cliResult.cellContent(0, "ErrorForced"), errorType, None)
                
                self.assertCliSuccess("ram internal force error " + str(aRamId) + ' ' + errorType + " dis")
                cliResult = self.classRunCli("show ram internal " + str(aRamId))
                self.assertEqual(cliResult.cellContent(0, "ErrorForced"), "None", None)
        
        def TestForceErrorOnCrcRam(aRamId):
            self.assertCliSuccess("ram internal error generator type " + str(aRamId) + ' ' + " crc")

            self.disableLogger()
            cliResult = self.classRunCli("ram internal error generator mode " + str(aRamId) + ' ' + " continuous")
            modeIsSupported = cliResult.success()
            self.enableLogger()

            cliResult = self.classRunCli("show ram internal error generator " + str(aRamId))
            if modeIsSupported:
                self.assertEqual(cliResult.cellContent(0, "gen-mode"), "continuous", None)
            self.assertEqual(cliResult.cellContent(0, "error-type"), "crc", None)

            cliResult = self.classRunCli("ram internal error generator mode " + str(aRamId) + ' ' + " oneshot")
            self.assertTrue(cliResult.success())
            cliResult = self.classRunCli("ram internal error generator errornum " + str(aRamId) + ' ' + " 500")
            self.assertTrue(cliResult.success())
            cliResult = self.classRunCli("show ram internal error generator " + str(aRamId))
            self.assertEqual(cliResult.cellContent(0, "gen-mode"), "oneshot", None)
            self.assertEqual(cliResult.cellContent(0, "error-num"), "500", None)
            self.assertEqual(cliResult.cellContent(0, "error-type"), "crc", None)
            
            cliResult = self.classRunCli("ram internal error generator start " + str(aRamId))
            self.assertTrue(cliResult.success())
            cliResult = self.classRunCli("show ram internal error generator " + str(aRamId))
            self.assertEqual(cliResult.cellContent(0, "is-started"), "en", None)
            
            cliResult = self.classRunCli("ram internal error generator stop " + str(aRamId))
            self.assertTrue(cliResult.success())
            cliResult = self.classRunCli("show ram internal error generator " + str(aRamId))
            self.assertEqual(cliResult.cellContent(0, "is-started"), "dis", None)
            
        for moduleName in self.supportedModules():
            ramIdList = self.ramIdListOnModule(moduleName)
            for ramId in ramIdList:
                if self.ramIsCrc(ramId):
                    TestForceErrorOnCrcRam(ramId)
                else:
                    TestForceErrorOnRam(ramId)
              
    def testShowRam(self):
        for moduleName in self.supportedModules():
            self.classRunCli("show ram internal " + moduleName)
            
    def testShowHistory(self):
        for moduleName in self.supportedModules():
            self.classRunCli("show ram internal history " + moduleName + " r2c")
            self.classRunCli("show ram internal history " + moduleName + " ro")

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(InternalRamTest)
    runner.summary()

if __name__ == '__main__':
    TestMain()

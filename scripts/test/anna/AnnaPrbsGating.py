"""
Created on Apr 6, 2017

@author: namnng
"""
from python.arrive.AtAppManager.AtAppManager import AtAppManager
from test.AtTestCase import AtTestCase, AtTestCaseRunner

#import pydevd
#pydevd.settrace('localhost')

class AbstractObject(object):
    @staticmethod
    def abort():
        assert 0
        
    @staticmethod
    def runCli(cli):
        return AtAppManager.localApp().runCli(cli)

    def assertCliSuccess(self, cli):
        cliResult = self.runCli(cli) 
        assert(cliResult.success())

class PrbsGatingController(AbstractObject):
    def canControl(self):
        self.abort()
    
    def setDuration(self, duration):
        self.abort()
    
    def getDuration(self):
        self.abort()
    
    def elapsedTime(self):
        self.abort()
    
    def start(self):
        self.abort()
        
    def stop(self):
        self.abort()
        
    def started(self):
        self.abort()
        
    def isRunning(self):
        self.abort()

class CliSerdesPrbsGatingController(PrbsGatingController):
    def canControl(self):
        return True
    
    def __init__(self, cliSerdesId):
        self.cliSerdesId = cliSerdesId
    
    def setDuration(self, duration):
        cli = "serdes prbs duration %d %d" % (self.cliSerdesId, duration)
        self.assertCliSuccess(cli)
    
    @staticmethod
    def boolFromString(string):
        if string in ["yes", "set", "en"]:
            return True
        return False
    
    @staticmethod
    def numberFromString(string, base = 10):
        if string in ["N/A", "N/S", "NA", "xxx"]:
            return 0
        return int(string, base)
    
    def _getAllInfo(self):
        """
        channel,engineId,controllable,duration,elapsedTime,started,running
        """
        cli = "show serdes prbs gating %d" % self.cliSerdesId
        result = self.runCli(cli)
        return result.cellContent(0, "channel"), \
               int(result.cellContent(0, "engineId"), 10), \
               self.boolFromString(result.cellContent(0, "controllable")), \
               self.numberFromString(result.cellContent(0, "duration(ms)")), \
               self.numberFromString(result.cellContent(0, "elapsedTime(ms)")), \
               self.boolFromString(result.cellContent(0, "started")), \
               self.boolFromString(result.cellContent(0, "running"))
    
    def getDuration(self):
        _,_,_,duration,_,_,_ = self._getAllInfo()
        return duration
    
    def elapsedTime(self):
        _,_,_,_,elapsedTime,_,_ = self._getAllInfo()
        return elapsedTime
    
    def start(self):
        cli = "serdes prbs start %d" % self.cliSerdesId
        self.assertCliSuccess(cli)
        
    def stop(self):
        cli = "serdes prbs stop %d" % self.cliSerdesId
        self.assertCliSuccess(cli)
        
    def started(self):
        _,_,_,_,_,started,_ = self._getAllInfo()
        return started
        
    def isRunning(self):
        _,_,_,_,_,_,running = self._getAllInfo()
        return running
        
    @classmethod
    def gatingController(cls, cliSerdesId):
        return CliSerdesPrbsGatingController(cliSerdesId)

class SerdesController(AbstractObject):
    def modeIsSupported(self, mode):
        self.abort()
    
    def setMode(self, mode):
        self.abort()

class CliSerdesController(SerdesController):
    def __init__(self, cliId):
        self.cliId = cliId
        
    def setMode(self, mode):
        result = self.runCli("serdes mode %d %s" % (self.cliId, mode))
        assert(result.success())
        
class MroCliSerdesController(CliSerdesController):
    @classmethod
    def faceplateSerdesController(cls, cliId):
        return _MroCliFaceplateSerdesController(cliId)
        
    @classmethod
    def mateSerdesController(cls, cliId):
        return _MroCliMateSerdesController(cliId)
    
    @classmethod
    def sgmiiSerdesController(cls, cliId):
        return _MroCliSgmiiSerdesController(cliId)
    
    @classmethod
    def backplaneSerdesController(cls, cliId):
        return _MroCliBackplaneSerdesController(cliId)

class _MroCliFaceplateSerdesController(MroCliSerdesController):
    def modeIsSupported(self, mode):
        allModes = ["stm0",
                    "stm1",
                    "stm4",
                    "stm16",
                    "stm64",
                    "100M",
                    "1G",
                    "10G"]
        if not mode in allModes:
            return False
        
        if mode in ["stm64", "10G"]:
            return self.cliId == 1
        
        return True
        
class _MroCliMateSerdesController(MroCliSerdesController):
    def modeIsSupported(self, mode):
        return mode == "stm16" 
        
class _MroCliBackplaneSerdesController(MroCliSerdesController):
    def modeIsSupported(self, mode):
        return mode == "40G"
    
    def setMode(self, mode):
        assert(mode == "40G")

class _MroCliSgmiiSerdesController(MroCliSerdesController):
    def modeIsSupported(self, mode):
        return mode == "1G"

class PdhCliSerdesController(CliSerdesController):
    @classmethod
    def backplaneSerdesController(cls, cliId):
        return _PdhCliBackplaneSerdesController(cliId)

class _PdhCliBackplaneSerdesController(PdhCliSerdesController):
    def modeIsSupported(self, mode):
        return mode == "10G"

class PrbsGatingTest(AtTestCase):
    controller = None
    
    def testDuration(self):
        for duration in [1000, 2000, 3000]:
            self.controller.setDuration(duration)
            self.assertEqual(self.controller.getDuration(), duration, None)
    
    def testElapsedTime(self):
        self.assertGreaterEqual(self.controller.elapsedTime(), 0, None)
    
    def testStart(self):
        self.controller.start()
        self.assertTrue(self.controller.started(), None)
        
    def testStop(self):
        self.controller.stop()
        self.assertFalse(self.controller.started(), None)
    
    def testRunning(self):
        self.controller.start()
        self.assertTrue(self.controller.isRunning(), None)
        self.controller.stop()
        self.assertFalse(self.controller.isRunning(), None)
    
class Runner(object):
    @classmethod
    def TestSerdes(cls, unittestRunner, serdesIds, serdesControllerCreateFunction):
        for serdesId in serdesIds:
            cliSerdesId = serdesId + 1
            serdesController = serdesControllerCreateFunction(cliSerdesId)
            for mode in cls.allSerdesModes():
                if not serdesController.modeIsSupported(mode):
                    continue
                if cliSerdesId == 27 and not cls.canRunPort27():
                    continue
                
                serdesController.setMode(mode)
                gatingController = CliSerdesPrbsGatingController.gatingController(cliSerdesId)
                PrbsGatingTest.controller = gatingController
                unittestRunner.run(PrbsGatingTest)
                
    @classmethod
    def allSerdesModes(cls):
        return ["stm0",
                "stm1",
                "stm4",
                "stm16",
                "stm64",
                "10M",
                "100M",
                "2500M",
                "1G",
                "10G",
                "40G",
                "ocn",
                "ge"]
    
    @classmethod
    def run(cls, unittestRunner):
        pass
    
    @classmethod
    def runner(cls):
        productCode = AtAppManager.localApp().productCode()
        if productCode in [0x60290021, 0x60290022]:
            return _MroRunner
        
        if productCode == 0x60290011:
            return _PdhRunner
        
        return None
    
class _MroRunner(Runner):
    @classmethod
    def is20G(cls):
        productCode = AtAppManager.localApp().productCode()
        return productCode == 0x60290022
    
    @classmethod
    def canRunPort27(cls):
        version = AtAppManager.localApp().deviceVersion()
        if version.major >= 3 and version.major >= 3:            
            return False
        return False
    
    @classmethod
    def faceplateSerdesIds(cls):
        if cls.is20G():
            return range(0, 16)
        return range(0, 8)
    
    @classmethod
    def mateSerdesIds(cls):
        if cls.is20G():
            return range(16, 24)
        return range(16, 20)
    
    @classmethod
    def sgmiiSerdesIds(cls):
        serdesIds = [26,27]

        if cls.is20G() and not cls.canRunPort27():
            serdesIds.remove(27)

        return serdesIds

    @classmethod
    def backplaneSerdesIds(cls):
        return [24, 25]
    
    @classmethod
    def testFaceplateSerdes(cls, unittestRunner):
        def createSerdes(cliSerdesId):
            return MroCliSerdesController.faceplateSerdesController(cliSerdesId)
        
        cls.TestSerdes(unittestRunner, cls.faceplateSerdesIds(), createSerdes)
    
    @classmethod
    def testMateSerdes(cls, unittestRunner):
        def createSerdes(cliSerdesId):
            return MroCliSerdesController.mateSerdesController(cliSerdesId)
        
        cls.TestSerdes(unittestRunner, cls.mateSerdesIds(), createSerdes)
        
    @classmethod
    def testSgmiiSerdes(cls, unittestRunner):
        def createSerdes(cliSerdesId):
            return MroCliSerdesController.sgmiiSerdesController(cliSerdesId)
        
        cls.TestSerdes(unittestRunner, cls.sgmiiSerdesIds(), createSerdes)
        
    @classmethod
    def testBackplaneSerdes(cls, unittestRunner):
        def createSerdes(cliSerdesId):
            return MroCliSerdesController.backplaneSerdesController(cliSerdesId)
        
        cls.TestSerdes(unittestRunner, cls.backplaneSerdesIds(), createSerdes)
        
    @classmethod
    def run(cls, unittestRunner):
        cls.testFaceplateSerdes(unittestRunner)
        cls.testMateSerdes(unittestRunner)
        cls.testSgmiiSerdes(unittestRunner)
        cls.testBackplaneSerdes(unittestRunner)

class _PdhRunner(Runner):
    @classmethod
    def backplaneSerdesIds(cls):
        return range(0, 2)
    
    @classmethod
    def axi4SerdesIds(cls):
        return [2]
    
    @classmethod
    def sgmiiSerdesIds(cls):
        return range(3, 3)
    
    @classmethod
    def testBackplaneSerdes(cls, unittestRunner):
        def createSerdes(cliSerdesId):
            return PdhCliSerdesController.backplaneSerdesController(cliSerdesId)
        
        cls.TestSerdes(unittestRunner, cls.backplaneSerdesIds(), createSerdes)
    
    @classmethod
    def run(cls, unittestRunner):
        cls.testBackplaneSerdes(unittestRunner)
    
def TestMain():
    unittestRunner = AtTestCaseRunner.runner()
    productRunner = Runner.runner()
    productRunner.run(unittestRunner)
    unittestRunner.summary()

if __name__ == '__main__':
    TestMain()

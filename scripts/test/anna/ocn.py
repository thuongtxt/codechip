import python.arrive.atsdk.AtRegister as AtRegister
from test.AtTestCase import AtTestCaseRunner
from AnnaTestCase import AnnaMroTestCase

class _RegisterFramerLocatedSlicePointerSelection(AtRegister.AtRegister):
    @staticmethod
    def _lineLocalId(lineId):
        return lineId % 4
    
    def frmslcppenb(self, lineId):
        mask = AtRegister.cBit6 << (self._lineLocalId(lineId) * 8)
        shift = 6 + (self._lineLocalId(lineId) * 8)
        return self.getField(mask, shift)
    
    def _frmslcppsel(self, lineId):
        mask = AtRegister.cBit5_0 << (self._lineLocalId(lineId) * 8)
        shift = (self._lineLocalId(lineId) * 8)
        return self.getField(mask, shift)
    
    def Slc48Id(self, lineId):
        return (self._frmslcppsel(lineId) & AtRegister.cBit5_4) >> 4
    
    def Slc12Id(self, lineId):
        return (self._frmslcppsel(lineId) & AtRegister.cBit3_2) >> 2
    
    def Slc3Id(self, lineId):
        return self._frmslcppsel(lineId) & AtRegister.cBit1_0

class _RegisterFramerControl(AtRegister.AtRegister):
    def oc192enb(self):
        return self.getBit(24)
    
    def scren(self, lineId):
        return self.getBit(16 + lineId)
    
    def stmocnmode(self, lineid):
        mask = AtRegister.cBit1_0 << (lineid * 2)
        shift = lineid * 2
        return self.getField(mask, shift)

class _RegisterPointerInterpreterControl(AtRegister.AtRegister):
    pass

class TestCaseOcn(AnnaMroTestCase):
    @staticmethod
    def baseAddress():
        return 0x0100000
    
    def localRead(self, localAddress):
        return self.rd(localAddress + self.baseAddress())
    
class TestCaseOcnFramer(TestCaseOcn):
    @classmethod
    def allLineIdStringForRate(cls, rate):
        if rate == "stm64":
            return "1,9" if cls.is20G() else "1"
        if rate == "stm16":
            return "1,3,5,7,9,11,13,15" if cls.is20G() else "1,3,5,7"
        return "1-16" if cls.is20G() else "1-8"
        
    @classmethod
    def setUpClass(cls):
        cls.classRunCli("device init")
    
    def testStm64(self):
        lineIdString = self.allLineIdStringForRate("stm64")
        self.assertCliSuccess("sdh line rate %s stm64" % lineIdString)
        
        def checkFramerReg(address):
            reg = _RegisterFramerControl(self.localRead(address))
            
            # OC-192 mode must be enabled
            self.assertEqual(reg.oc192enb(), 1, None)
            
            # Scramble must be enabled by default
            self.assertEqual(reg.scren(0), 1, None)
        
        def checkLocatedSlicePointerSelection():
            for lineId in range(0, 8):
                registers = [0x20010, 0x20012] if (lineId < 4) else [0x20011, 0x20013]
                
                for address in registers:
                    reg = _RegisterFramerLocatedSlicePointerSelection(self.localRead(address))
                    enabled = 1 if (lineId == 0) else 0
                    self.assertEqual(reg.frmslcppenb(lineId), enabled, None)
                    self.assertEqual(reg.Slc48Id(lineId), lineId / 2, None)
                    self.assertEqual(reg.Slc12Id(lineId), lineId % 2, None)
                    self.assertEqual(reg.Slc3Id(lineId), 0, None)
        
        checkFramerReg(0x20000)
        checkFramerReg(0x20001)
        checkLocatedSlicePointerSelection()
    
    def testStm16(self):
        self.assertCliSuccess("sdh line rate 1,3,5,7 stm16")
        
        def checkFramerReg(address):
            reg = _RegisterFramerControl(self.localRead(address))
            
            # OC-192 mode must be disabled
            self.assertEqual(reg.oc192enb(), 0, None)
            
            # Scramble must be enabled by default
            for i in range(0, 8):
                if i % 2 == 0:
                    self.assertEqual(reg.scren(i), 1, None)
            
            # Frame mode must be OC-48
            for i in range(0, 8):
                if i % 2 == 0:
                    self.assertEqual(reg.stmocnmode(i), 0, None)
        
        def checkLocatedSlicePointerSelection():
            for lineId in range(0, 8):
                registers = [0x20010, 0x20012] if (lineId < 4) else [0x20011, 0x20013]
                
                for address in registers:
                    reg = _RegisterFramerLocatedSlicePointerSelection(self.localRead(address))
                    enabled = 1 if (lineId % 2 == 0) else 0
                    self.assertEqual(reg.frmslcppenb(lineId), enabled, None)
                    self.assertEqual(reg.Slc48Id(lineId), lineId / 2, None)
                    self.assertEqual(reg.Slc12Id(lineId), lineId % 2, None)
                    self.assertEqual(reg.Slc3Id(lineId), 0, None)
        
        checkFramerReg(0x20000)
        checkFramerReg(0x20001)
        checkLocatedSlicePointerSelection()
    
    def checkLocatedSlicePointerSelectionForLowRate(self):
        for lineId in range(0, 8):
            registers = [0x20010, 0x20012] if (lineId < 4) else [0x20011, 0x20013]
            
            for address in registers:
                reg = _RegisterFramerLocatedSlicePointerSelection(self.localRead(address))
                self.assertEqual(reg.frmslcppenb(lineId), 1, None)
                self.assertEqual(reg.Slc48Id(lineId), lineId / 2, None)
                self.assertEqual(reg.Slc12Id(lineId), lineId % 2, None)
                self.assertEqual(reg.Slc3Id(lineId), 0, None) 
    
    def testStm4(self):
        lineIdString = self.allLineIdStringForRate("stm4")
        self.assertCliSuccess("sdh line rate %s stm4" % lineIdString)
        
        def checkFramerReg(address):
            reg = AtRegister.AtRegister(self.localRead(address))
            
            # OC-192 mode must be disabled
            self.assertEqual(reg.getBit(24), 0, None)
            
            # Scramble must be enabled by default
            for i in range(0, 8):
                bitOffset = 16 + i
                self.assertEqual(reg.getBit(bitOffset), 1, None)
            
            # Frame mode must be OC-12
            for i in range(0, 8):
                shift = (i * 2)
                mask = AtRegister.cBit1_0 << shift
                self.assertEqual(reg.getField(mask, shift), 1, None)
        
        checkFramerReg(0x20000)
        checkFramerReg(0x20001)
        self.checkLocatedSlicePointerSelectionForLowRate()
        
    def testStm1(self):
        lineIdString = self.allLineIdStringForRate("stm1")
        self.assertCliSuccess("sdh line rate %s stm1" % lineIdString)
        
        def checkFramerReg(address):
            reg = AtRegister.AtRegister(self.localRead(address))
            
            # OC-192 mode must be disabled
            self.assertEqual(reg.getBit(24), 0, None)
            
            # Scramble must be enabled by default
            for i in range(0, 8):
                bitOffset = 16 + i
                self.assertEqual(reg.getBit(bitOffset), 1, None)
            
            # Frame mode must be OC-3
            for i in range(0, 8):
                shift = (i * 2)
                mask = AtRegister.cBit1_0 << shift
                self.assertEqual(reg.getField(mask, shift), 2, None)
        
        checkFramerReg(0x20000)
        checkFramerReg(0x20001)
        self.checkLocatedSlicePointerSelectionForLowRate()
        
    def testStm0(self):
        lineIdString = self.allLineIdStringForRate("stm0")
        self.assertCliSuccess("sdh line rate %s stm0" % lineIdString)
        
        def checkFramerReg(address):
            reg = AtRegister.AtRegister(self.localRead(address))
            
            # OC-192 mode must be disabled
            self.assertEqual(reg.getBit(24), 0, None)
            
            # Scramble must be enabled by default
            for i in range(0, 8):
                bitOffset = 16 + i
                self.assertEqual(reg.getBit(bitOffset), 1, None)
            
            # Frame mode must be OC-1
            for i in range(0, 8):
                shift = (i * 2)
                mask = AtRegister.cBit1_0 << shift
                self.assertEqual(reg.getField(mask, shift), 3, None)
        
        checkFramerReg(0x20000)
        checkFramerReg(0x20001)
        self.checkLocatedSlicePointerSelectionForLowRate()
    
    def testChangeDifferentRateMustNotCauseAnyTrouble(self):        
        rates = ["stm0", "stm1", "stm4", "stm16", "stm64"]        
        self.assertCliSuccess("logger flush")
        for rate1 in rates:
            lineIdString = self.allLineIdStringForRate(rate1)
            self.assertCliSuccess("sdh line rate %s %s" % (lineIdString, rate1))
            self.assertCliSuccess("show logger")
            for rate2 in rates:
                lineIdString = self.allLineIdStringForRate(rate2)
                self.assertCliSuccess("sdh line rate %s %s" % (lineIdString, rate2))
                self.assertCliSuccess("show logger")
    
def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(TestCaseOcnFramer)
    return runner.summary()
    
if __name__ == '__main__':
    TestMain()

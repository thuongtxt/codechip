"""
Created on Apr 9, 2017

@author: namnng
"""
import getopt
import sys
import time

from AnnaChannelScanner import CircuitScanner
from AnnaScriptProvider import AnnaMroScriptProvider
from AnnaTestCase import AnnaMroTestCase
from python.arrive.atsdk.cli import AtCliRunnable, AtCliListener
from python.arrive.atsdk.device import AtDevice
from python.arrive.atsdk.platform import AtHalListener
from python.arrive.atsdk.prbs import AtPrbsStableChecker
from test.AtTestCase import AtTestCaseRunner
from test.anna.randomizer import AnnaRandomizer


class ScriptGenerator(AtCliRunnable):
    def __init__(self, productCode):
        AtCliRunnable.__init__(self)
        self.randomize = AnnaRandomizer.randomizer(productCode)
    
    def run(self):
        lines = self.randomize.makeDefault()
        
        def _applyDict(aDict):
            for cli in aDict["cli"]:
                self.runCli(cli)
            
            subChannels = aDict["subChannels"]
            for subChannel in subChannels:
                _applyDict(subChannel) 
        
        self.runCli("device services destroy")
        for line in lines:
            _applyDict(line)
        
        # Loopback to selftest
        self.runCli("serdes loopback 25,26 local")
        self.runCli("serdes loopback 1-8 local")

class TrafficChecker(AtCliRunnable):
    def __init__(self):
        AtCliRunnable.__init__(self)
        self.simulated = False
        self.quickCheck = False
        self.device = AtDevice.currentDevice()
    
    @staticmethod
    def numBertEngines():
        return 28
    
    def prbsModule(self):
        return self.device.prbsModule()
    
    def provisionPrbs(self, circuits):
        bertEngines = []
        bertEngineId = 0
        prbsModule = self.prbsModule()
        
        for circuit in circuits:
            engine = prbsModule.createPrbsEngine(bertEngineId, circuit)
            engine.generatingSide = engine.SIDE_TDM
            
            if circuit.boundPw() is None:
                engine.monitoringSide = engine.SIDE_TDM
            else:
                engine.monitoringSide = engine.SIDE_PSN
            
            bertEngines.append(engine)
            bertEngineId = bertEngineId + 1
            
        return bertEngines
    
    def checkPrbs(self, bertEngines):
        stableChecker = AtPrbsStableChecker(bertEngines)
        stableChecker.simulated = self.simulated
        stableChecker.quickCheck = self.quickCheck
        return stableChecker.check()
    
    def cleanupPrbs(self, bertEngines):
        prbsModule = self.prbsModule()
        for engine in bertEngines:
            prbsModule.deletePrbsEngine(engine)
    
    def checkCircuits(self, circuits):
        if len(circuits) == 0:
            return
        
        bertEngines = self.provisionPrbs(circuits)
        good = self.checkPrbs(bertEngines)
        if good:
            self.cleanupPrbs(bertEngines)
        
        return good
    
    def check(self):
        channelScanner = CircuitScanner()
        channelScanner.scan()
        
        numCircuitsToCheck = self.numBertEngines()
        while len(channelScanner.channels) > 0:
            circuitsToCheck = []
            for _ in range(numCircuitsToCheck):
                try:
                    circuitsToCheck.append(channelScanner.channels.pop())
                except:
                    break
            
            good = self.checkCircuits(circuitsToCheck)
            if not good:
                return False

        return True
            
class CliLogger(AtCliListener):
    def __init__(self, logFile):
        AtCliListener.__init__(self)
        self.logFile = logFile
    
    def willRunCli(self, cli):
        self.logFile.write("%s\n" % cli)
        self.logFile.flush()

class HalListener(AtHalListener):
    def __init__(self):
        AtHalListener.__init__(self)
        self.invalidAccessCount = 0
        self.enabled = True
    
    def didRead(self, address, value):
        if self.enabled:
            assert(value not in [0xCAFECAFE, 0xC0CAC0CA, 0xDEADC0FE])

class TrafficAbstractTest(AnnaMroTestCase):
    halListener = None
    noConfigure = False
    quickCheck = False
    noCheck = False
    disableInvalidAccessCheck = False
    
    def createTrafficChecker(self):
        checker = TrafficChecker()
        checker.quickCheck = self.quickCheck
        checker.simulated = AtDevice.currentDevice().simulated
        return checker
    
    def needConfigure(self):
        if self.noConfigure:
            return False
        return True
    
    def needCheckTraffic(self):
        if self.noCheck:
            return False
        return True
    
    @classmethod
    def setUpClass(cls):
        super(TrafficAbstractTest, cls).setUpClass()
        cls.halListener = HalListener()
        if cls.disableInvalidAccessCheck:
            cls.halListener.enabled = False
        AtHalListener.addListener(cls.halListener)
        
    @classmethod
    def tearDownClass(cls):
        super(TrafficAbstractTest, cls).tearDownClass()
        AtHalListener.removeListener(cls.halListener)
        cls.halListener = None
        
    def plaBlocksAreNormal(self):
        simulated = AtDevice.currentDevice().simulated
        result = self.runCli("debug show module pwe cache")
        
        for rowId in range(result.numRows()):
            cellContent = result.cellContent(rowId, "used")
            if cellContent in ["N/A", "N/S"]:
                used = 0
            else:
                used = int(cellContent)
            
            if simulated:
                used = 0
            
            if used != 0:
                return False
            
        return True

class TrafficRandomTest(TrafficAbstractTest):
    logFileName = None
    repeatTime = 1
    
    def testRandomTraffic(self):
        cliLogger = None
        logFile = None
        if self.logFileName:
            logFile = open(self.logFileName, "w")
        
        if logFile:
            cliLogger = CliLogger(logFile)
            
        cliRunnable = AtCliRunnable()
        
        cliRunnable.addListener(cliLogger)
        if self.needConfigure():
            cliRunnable.runCli("device init")
            cliRunnable.runCli("debug ciena mro xc hide direct")
        
        scriptGenerator = ScriptGenerator(self.productCode())
        scriptGenerator.addListener(cliLogger)
        trafficChecker = self.createTrafficChecker()
        trafficChecker.addListener(cliLogger)
        
        for i in range(self.repeatTime):
            if logFile:
                logFile.write("#====================================================\n")
                logFile.write("# Run time %d\n" % (i + 1))
                logFile.write("#====================================================\n")
            
            if self.needConfigure():
                scriptGenerator.run()
                
            if self.needCheckTraffic():
                if self.quickCheck:
                    time.sleep(5) # Just to give hardware a stable time
                    
                self.assertTrue(trafficChecker.check(), None)
                
            self.runCli("device services destroy")
            self.assertTrue(self.plaBlocksAreNormal(), None)
                
        if logFile:
            logFile.close()

class TrafficBasicTest(TrafficAbstractTest):
    def loopback(self):
        self.assertCliSuccess("serdes loopback 1-8,25,26 local")
    
    def TestWithScript(self, script):
        if self.needConfigure():
            self.assertCliSuccess(script)
            self.loopback()
        
        if self.needCheckTraffic():
            if self.quickCheck:
                time.sleep(5) # Just to give hardware a stable time
            trafficChecker = self.createTrafficChecker()
            self.assertTrue(trafficChecker.check(), None)
    
    @staticmethod
    def basicScripts():
        scriptProvider = AnnaMroScriptProvider()
        return [scriptProvider.stm16Vc15CepScript()]
    
    def testBasicPaths(self):
        for script in self.basicScripts():
            self.assertTrue(self.plaBlocksAreNormal(), None)
            self.TestWithScript(script)
    
def TestMain():
    def printHelp():
        print "Usage: %s [--help] [--repeat-times=<repeatTime>] [--cli-log-file=<cli log file>] "\
              "[--no-configure] [--no-check] [--quick-check] [--no-catch-invalid-access] [--scan-circuits]" % sys.argv[0]
    try:
        opts, _ = getopt.getopt(sys.argv[1:],"",["help", "repeat-times=", "cli-log-file=", "no-configure",
                                                 "quick-check", "no-check", "no-catch-invalid-access", "scan-circuits"])
    except getopt.GetoptError:
        printHelp()
        return

    repeatTime = 10
    logFileName = None
    noConfigure = False
    quickCheck = False
    noCheck = False
    noCatchInvalidAccess = False
    scanOnly = False

    for opt, arg in opts:
        if opt == "--help":
            printHelp()
            return
        
        if opt == "--repeat-times":
            repeatTime = int(arg, 10)
            
        if opt == "--cli-log-file":
            logFileName = arg
            
        if opt == "--no-configure":
            noConfigure = True
            
        if opt == "--quick-check":
            quickCheck = True
            
        if opt == "--no-check":
            noCheck = True
            
        if opt == "--no-catch-invalid-access":
            noCatchInvalidAccess = True

        if opt == "--scan-circuits":
            scanOnly = True

    if scanOnly:
        scanner = CircuitScanner()
        circuits = scanner.scan()
        for circuit in circuits:
            print "%s.%s" % (circuit.cliType(), circuit.cliId())
        return 

    runner = AtTestCaseRunner.runner()
    
    TrafficBasicTest.noConfigure = noConfigure
    TrafficBasicTest.quickCheck = quickCheck
    runner.run(TrafficBasicTest)

    TrafficRandomTest.repeatTime = repeatTime
    TrafficRandomTest.logFileName = logFileName
    TrafficRandomTest.noConfigure = noConfigure
    TrafficRandomTest.quickCheck = quickCheck
    TrafficRandomTest.noCheck = noCheck
    TrafficRandomTest.disableInvalidAccessCheck = noCatchInvalidAccess
    runner.run(TrafficRandomTest)

    runner.summary()
        
if __name__ == '__main__':
    TestMain()
from test.AtTestCase import AtTestCaseRunner
from AnnaTestCase import AnnaMroTestCase

class Aps(AnnaMroTestCase):
    def testPathCanJoinToAnySelectors(self):
        self.assertCliSuccess("device init")
        self.assertCliSuccess("xc vc connect vc3.1.1.1-1.16.3 vc3.25.1.1-25.16.3")
        
        # BP/MT --> FP
        self.assertCliSuccess("aps selector create 1 vc3.25.1.1 vc3.17.1.1 vc3.1.1.1")
        
        # FP/MT --> BP 
        self.assertCliSuccess("aps upsr engine create 1 vc3.1.1.1 vc3.17.1.1")
        
        # FP/BP --> MT
        self.assertCliSuccess("aps selector create 2 vc3.1.1.1 vc3.25.1.1 vc3.17.1.1")
        
        result = self.runCli("show sdh path aps engine vc3.1.1.1,17.1.1,25.1.1")
        self.assertEqual(result.cellContent(0, "APS Engine"), "1", None)
        self.assertEqual(result.cellContent(1, "APS Engine"), "1", None)
        self.assertEqual(result.cellContent(2, "APS Engine"), "none", None)
        
        result = self.runCli("show sdh path aps selectors vc3.1.1.1,17.1.1,25.1.1")
        self.assertEqual(result.cellContent(0, "APS Selectors"), "1-2", None)
        self.assertEqual(result.cellContent(1, "APS Selectors"), "1-2", None)
        self.assertEqual(result.cellContent(2, "APS Selectors"), "1-2", None)
        
        self.assertCliSuccess("aps upsr engine delete 1")
        result = self.runCli("show sdh path aps engine vc3.1.1.1,17.1.1,25.1.1")
        self.assertEqual(result.cellContent(0, "APS Engine"), "none", None)
        self.assertEqual(result.cellContent(1, "APS Engine"), "none", None)
        self.assertEqual(result.cellContent(2, "APS Engine"), "none", None)
        
        self.assertCliSuccess("aps selector delete 1")
        result = self.runCli("show sdh path aps selectors vc3.1.1.1,17.1.1,25.1.1")
        self.assertEqual(result.cellContent(0, "APS Selectors"), "2", None)
        self.assertEqual(result.cellContent(1, "APS Selectors"), "2", None)
        self.assertEqual(result.cellContent(2, "APS Selectors"), "2", None)
        
        self.assertCliSuccess("aps selector delete 2")
        result = self.runCli("show sdh path aps selectors vc3.1.1.1,17.1.1,25.1.1")
        self.assertEqual(result.cellContent(0, "APS Selectors"), "none", None)
        self.assertEqual(result.cellContent(1, "APS Selectors"), "none", None)
        self.assertEqual(result.cellContent(2, "APS Selectors"), "none", None)

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(Aps)
    runner.summary()
        
if __name__ == '__main__':
    TestMain()

import random
import sys
import time
import getopt

from python.arrive.atsdk.cli import AtCliRunnable
from test.AtTestCase import AtTestCase, AtTestCaseRunner


class BertChecker(AtCliRunnable):
    @classmethod
    def runCli(cls, cli):
        result = AtCliRunnable.runCli(cli)
        assert result.success()
        return result

    def __init__(self, cliIds, simulated=False, ignoreBertErrorCounters=False):
        self.cliIds = cliIds
        self.simulated = simulated
        self.ignoreErrorCounters = ignoreBertErrorCounters

    def clearStatus(self):
        self.runCli("show prbs engine counters %s r2c silent" % self.cliIds)
        self.runCli("show prbs engine %s" % self.cliIds)

    def assertGoodCounters(self):
        def greaterThanZero(value):
            return value > 0

        def equalToZero(value):
            return value == 0

        counters = self.runCli("show prbs engine counters %s r2c" % self.cliIds)
        for row in range(counters.numRows()):
            def checkCounter(name, checkFunction):
                cellContent = counters.cellContent(row, name).lower()
                if cellContent in ["n/s", "n/a"]:
                    return

                assert checkFunction(int(cellContent))

            def checkGoodCounter(counterName):
                if self.simulated:
                    return True
                checkCounter(counterName, greaterThanZero)

            def checkErrorCounter(counterName):
                if self.ignoreErrorCounters:
                    return
                checkCounter(counterName, equalToZero)

            checkGoodCounter("txFrame")
            checkGoodCounter("TxBit")
            checkGoodCounter("rxFrame")
            checkGoodCounter("RxBit")
            checkErrorCounter("RxBitError")
            checkErrorCounter("RxBitErrorLastSync")
            checkGoodCounter("RxSync")
            checkErrorCounter("RxLoss")
            checkErrorCounter("RxLostFrame")
            checkErrorCounter("RxErrorFrame")

    def assertGoodAlarms(self):
        cliResult = self.runCli("show prbs engine %s" % self.cliIds)
        for row in range(cliResult.numRows()):
            if self.simulated:
                return
            assert cliResult.cellContent(row, "alarm").lower()  == "none"
            assert cliResult.cellContent(row, "sticky").lower() == "none"

    def assertGood(self):
        self.assertGoodCounters()
        self.assertGoodAlarms()

class Configuration(AtCliRunnable):
    def makeAllCircuitCliIds(self):
        return list()

    @staticmethod
    def maxNumTerminatedLines():
        return sys.maxint

    def __init__(self, simulated=False, randomEnabled=True, workAroundEnabled=False, numTerminatedLines=None, ignoreBertErrorCounters=False):
        if numTerminatedLines is None:
            numTerminatedLines = self.maxNumTerminatedLines()
        self.numTerminatedLines = numTerminatedLines
        self.bertChecker = BertChecker(cliIds=self.bertCliIds(), simulated=simulated, ignoreBertErrorCounters=ignoreBertErrorCounters)
        self.allCircuits = self.makeAllCircuitCliIds()
        self.bertIds = list()
        self.randomEnabled = randomEnabled
        self.workAroundEnabled = workAroundEnabled

    @staticmethod
    def maxNumBertEngines():
        return 28

    def numBertEngines(self):
        return sys.maxint

    @classmethod
    def runCli(cls, cli):
        result = AtCliRunnable.runCli(cli)

        if type(result) is list:
            for _, cliResult in result:
                assert cliResult.success()
        else:
            assert result.success()

        return result

    def apply(self):
        assert 0

    def bertCliIds(self):
        """
        :rtype: str
        """
        return "1-%d" % self.numBertEngines()

    def clearStatus(self):
        self.bertChecker.clearStatus()

    def assertBertGood(self):
        checker = self.bertChecker
        checker.assertGood()

    @classmethod
    def allConfigurations(cls, simulated=False, randomEnabled=True, workAroundEnabled=False, ignoreBertErrorCounters=False):
        return list()

class Mro20GConfiguration(Configuration):
    @staticmethod
    def lineSetupScript():
        return """
        serdes mode 1-16 stm16
        serdes powerup 1-16
        serdes enable 1-16
        sdh line mode 1,3,5,7,9,11,13,15 sonet
        sdh line rate 1,3,5,7,9,11,13,15 stm16
        sdh line loopback 1-16 local
        """

    def tdmSetupScript(self):
        return ""

    @staticmethod
    def maxNumPws():
        return 10752

    def numBertEngines(self):
        return 28

    @staticmethod
    def maxNumTerminatedLines():
        return 8

    @staticmethod
    def startTerminatedLineId():
        return 24

    def allLineIds(self):
        return range(self.startTerminatedLineId(), self.stopTerminatedLineId() + 1)

    @staticmethod
    def allAug1Ids():
        return range(16)

    def stopTerminatedLineId(self):
        return self.startTerminatedLineId() + self.numTerminatedLines - 1

    def randomPws(self, numPws):
        allPws = range(self.maxNumPws())
        if self.shouldRandom():
            return random.sample(allPws, numPws)
        return allPws[:numPws]

    def apply(self):
        self.runCli(self.tdmSetupScript())
        self.provisionBertsAndPws()

    def randomBertIds(self):
        allBertIds = range(self.maxNumBertEngines())
        if self.shouldRandom():
            return random.sample(allBertIds, self.numBertEngines())
        return allBertIds[:self.numBertEngines()]

    def provisionBertsAndPws(self):
        circuits = self.randomCircuits(numCircuits=self.numBertEngines())
        pws = self.randomPws(numPws=self.numBertEngines())
        bertIds = self.randomBertIds()
        self.bertIds = bertIds

        for bertId in bertIds:
            circuitCliId = "%s.%s" % (self.circuitType(), circuits.pop(0))
            bertCliId = bertId + 1
            self.createPrbsEngine(bertCliId, circuitCliId)
            self.runCli("prbs engine enable %s" % bertCliId)

            pwCliId = "%d" % (pws.pop(0) + 1)
            self.createPw(pwCliId)
            script = """
                pw psn %s mpls
                pw ethport %s 1
                pw jitterbuffer %s 4000
                pw jitterdelay %s  2000
                pw circuit bind %s %s
                pw enable %s
                """ % (pwCliId, pwCliId, pwCliId, pwCliId, pwCliId, circuitCliId, pwCliId)
            self.runCli(script)

    def createPrbsEngine(self, bertCliId, circuitCliId):
        assert 0

    def createPw(self, pwCliId):
        assert 0

    @staticmethod
    def circuitType():
        return "Invalid"

    def shouldRandom(self):
        return self.randomEnabled

    def nextCircuit(self):
        if self.shouldRandom():
            circuit = random.choice(self.allCircuits)
        else:
            circuit = self.allCircuits[0]

        self.allCircuits.remove(circuit)
        return circuit

    def randomCircuits(self, numCircuits):
        circuits = list()
        for _ in range(numCircuits):
            circuits.append(self.nextCircuit())
        return circuits

    @classmethod
    def allConfigurations(cls, simulated=False, randomEnabled=True, workAroundEnabled=False, ignoreBertErrorCounters=False):
        configurations = list()

        def createConfiguration(className):
            return className(simulated=simulated, randomEnabled=randomEnabled, workAroundEnabled=workAroundEnabled, ignoreBertErrorCounters=ignoreBertErrorCounters)

        configurations.append(createConfiguration(Mro20GVc4Configuration))
        configurations.append(createConfiguration(Mro20GVc3Configuration))
        configurations.append(createConfiguration(Mro20GVc4_4cConfiguration))
        configurations.append(createConfiguration(Mro20GVc4_16cConfiguration))
        configurations.append(createConfiguration(Mro20GVc11Configuration))
        configurations.append(createConfiguration(Mro20GVc12Configuration))
        
        return configurations

class Mro20GVcConfiguration(Mro20GConfiguration):
    def createPrbsEngine(self, bertCliId, circuitCliId):
        cli = "prbs engine create vc %s %s" % (bertCliId, circuitCliId)
        self.runCli(cli)

    def createPw(self, pwCliId):
        self.runCli("pw create cep %s basic" % pwCliId)

class Mro20GVc4Configuration(Mro20GVcConfiguration):
    def makeAllCircuitCliIds(self):
        cliIds = list()
        for lineId in range(self.startTerminatedLineId(), self.stopTerminatedLineId() + 1):
            for aug1Id in range(16):
                vc4CliId = "%d.%d" % (lineId + 1, aug1Id + 1)
                cliIds.append(vc4CliId)

        return cliIds

    def tdmSetupScript(self):
        return """
        # Initialize device
        device init

        # Configure Line
        %s

        # Configure mapping
        sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,9.1-9.16,11.1-11.16,13.1-13.16,15.1-15.16,25.1-32.16 vc4
        sdh map vc4.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,9.1-9.16,11.1-11.16,13.1-13.16,15.1-15.16,25.1-32.16 c4

        # TSI
        xc vc connect vc4.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,9.1-9.16,11.1-11.16,13.1-13.16,15.1-15.16 vc4.25.1-32.16 two-way
        """  % self.lineSetupScript()

    @staticmethod
    def circuitType():
        return "vc4"

class Mro20GVc4_4cConfiguration(Mro20GVcConfiguration):
    def makeAllCircuitCliIds(self):
        cliIds = list()
        for lineId in range(self.startTerminatedLineId(), self.stopTerminatedLineId() + 1):
            for aug4Id in range(4):
                vcCliId = "%d.%d" % (lineId + 1, aug4Id + 1)
                cliIds.append(vcCliId)

        return cliIds

    def tdmSetupScript(self):
        return """
        # Initialize device
        device init
        
        # Configure Line. 
        %s
        
        # Configure mapping
        sdh map aug4.1.1-1.4,3.1-3.4,5.1-5.4,7.1-7.4,9.1-9.4,11.1-11.4,13.1-13.4,15.1-15.4,25.1-32.4 vc4_4c
        sdh map vc4_4c.1.1-1.4,3.1-3.4,5.1-5.4,7.1-7.4,9.1-9.4,11.1-11.4,13.1-13.4,15.1-15.4,25.1-32.4 c4_4c
        
        # TSI
        xc vc connect vc4_4c.1.1-1.4,3.1-3.4,5.1-5.4,7.1-7.4 vc4_4c.25.1-28.4 two-way
        """ % self.lineSetupScript()

    @staticmethod
    def circuitType():
        return "vc4_4c"

    def numBertEngines(self):
        return min(self.numTerminatedLines * 4, self.maxNumBertEngines())

class Mro20GVc4_16cConfiguration(Mro20GVcConfiguration):
    def makeAllCircuitCliIds(self):
        cliIds = list()
        for lineId in range(self.startTerminatedLineId(), self.stopTerminatedLineId() + 1):
            vcCliId = "%d.1" % (lineId + 1)
            cliIds.append(vcCliId)

        return cliIds

    def tdmSetupScript(self):
        return """
        # Initialize device
        device init

        # Configure Line. 
        %s

        # Configure mapping
        sdh map aug16.1.1,3.1,5.1,7.1,9.1,11.1,13.1,15.1,25.1-32.1 vc4_16c
        sdh map vc4_16c.1.1,3.1,5.1,7.1,9.1,11.1,13.1,15.1,25.1-32.1 c4_16c
        
        # TSI
        xc vc connect vc4_16c.1.1,3.1,5.1,7.1,9.1,11.1,13.1,15.1 vc4_16c.25.1-32.1 two-way
        """ % self.lineSetupScript()

    @staticmethod
    def circuitType():
        return "vc4_16c"

    def numBertEngines(self):
        return self.numTerminatedLines

class Mro20GVc3Configuration(Mro20GVcConfiguration):
    def makeAllCircuitCliIds(self):
        cliIds = list()
        for lineId in range(self.startTerminatedLineId(), self.stopTerminatedLineId() + 1):
            for aug1Id in range(16):
                for au3Id in range(3):
                    vcCliId = "%d.%d.%d" % (lineId + 1, aug1Id + 1, au3Id + 1)
                    cliIds.append(vcCliId)

        return cliIds

    def tdmSetupScript(self):
        return """
        # Initialize device
        device init

        # Configure Line
        %s

        # Configure mapping
        sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,25.1-28.16 3xvc3s

        # TSI
        xc vc connect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 vc3.25.1.1-28.16.3 two-way
        """ % self.lineSetupScript()

    @staticmethod
    def circuitType():
        return "vc3"

class Mro20GVc11Configuration(Mro20GVcConfiguration):
    def makeAllCircuitCliIds(self):
        cliIds = list()
        for lineId in range(self.startTerminatedLineId(), self.stopTerminatedLineId() + 1):
            for aug1Id in range(16):
                for au3Id in range(3):
                    for tug2Id in range(7):
                        for tu1xId in range(4):
                            vcCliId = "%d.%d.%d.%d.%d" % (lineId + 1, aug1Id + 1, au3Id + 1, tug2Id + 1, tu1xId + 1)
                            cliIds.append(vcCliId)

        return cliIds

    def workAroundTdmSetupScript(self):
        return """
                # Initialize device
                device init

                # Configure Line
                %s

                # Configure mapping
                sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,9.1-9.16,11.1-11.16,13.1-13.16,15.1-15.16,25.1-32.16 3xvc3s
                
                # TSI
                xc vc connect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3,9.1.1-9.16.3,11.1.1-11.16.3,13.1.1-13.16.3,15.1.1-15.16.3 vc3.25.1.1-32.16.3 two-way
                
                sdh map vc3.25.1.1-32.16.3 7xtug2s
                sdh map tug2.25.1.1.1-32.16.3.7 tu11
                sdh map vc1x.25.1.1.1.1-28.16.3.7.4 c1x
                sdh map vc1x.29.1.1.1.1-32.16.3.7.4 c1x
                """ % self.lineSetupScript()

    def normalTdmSetupScript(self):
        return """
        # Initialize device
        device init

        # Configure Line
        %s

        # Configure mapping
        sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,9.1-9.16,11.1-11.16,13.1-13.16,15.1-15.16,25.1-32.16 3xvc3s
        sdh map vc3.25.1.1-32.16.3 7xtug2s
        sdh map tug2.25.1.1.1-32.16.3.7 tu11
        sdh map vc1x.25.1.1.1.1-28.16.3.7.4 c1x
        sdh map vc1x.29.1.1.1.1-32.16.3.7.4 c1x
        
        # TSI
        xc vc connect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3,9.1.1-9.16.3,11.1.1-11.16.3,13.1.1-13.16.3,15.1.1-15.16.3 vc3.25.1.1-32.16.3 two-way
        """ % self.lineSetupScript()

    def tdmSetupScript(self):
        if self.workAroundEnabled:
            return self.workAroundTdmSetupScript()
        return self.normalTdmSetupScript()

    @staticmethod
    def circuitType():
        return "vc1x"

class Mro20GVc12Configuration(Mro20GVcConfiguration):
    def makeAllCircuitCliIds(self):
        cliIds = list()
        for lineId in range(self.startTerminatedLineId(), self.stopTerminatedLineId() + 1):
            for aug1Id in range(16):
                for au3Id in range(3):
                    for tug2Id in range(7):
                        for tu1xId in range(3):
                            vcCliId = "%d.%d.%d.%d.%d" % (lineId + 1, aug1Id + 1, au3Id + 1, tug2Id + 1, tu1xId + 1)
                            cliIds.append(vcCliId)

        return cliIds

    def workAroundTdmSetupScript(self):
        return """
                # Initialize device
                device init
        
                # Configure Line
                %s
        
                # Configure mapping
                sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,9.1-9.16,11.1-11.16,13.1-13.16,15.1-15.16,25.1-32.16 3xvc3s
                
                # TSI
                xc vc connect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3,9.1.1-9.16.3,11.1.1-11.16.3,13.1.1-13.16.3,15.1.1-15.16.3 vc3.25.1.1-32.16.3 two-way
                
                sdh map vc3.25.1.1-32.16.3 7xtug2s
                sdh map tug2.25.1.1.1-32.16.3.7 tu12
                sdh map vc1x.25.1.1.1.1-28.16.3.7.3 c1x
                sdh map vc1x.29.1.1.1.1-32.16.3.7.3 c1x
                """ % self.lineSetupScript()

    def normalTdmSetupScript(self):
        return """
        # Initialize device
        device init

        # Configure Line
        %s

        # Configure mapping
        sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,9.1-9.16,11.1-11.16,13.1-13.16,15.1-15.16,25.1-32.16 3xvc3s
        sdh map vc3.25.1.1-32.16.3 7xtug2s
        sdh map tug2.25.1.1.1-32.16.3.7 tu12
        sdh map vc1x.25.1.1.1.1-28.16.3.7.3 c1x
        sdh map vc1x.29.1.1.1.1-32.16.3.7.3 c1x
        
        # TSI
        xc vc connect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3,9.1.1-9.16.3,11.1.1-11.16.3,13.1.1-13.16.3,15.1.1-15.16.3 vc3.25.1.1-32.16.3 two-way
        """ % self.lineSetupScript()

    def tdmSetupScript(self):
        if self.workAroundEnabled:
            return self.workAroundTdmSetupScript()
        else:
            return self.normalTdmSetupScript()

    @staticmethod
    def circuitType():
        return "vc1x"

class AbstractTest(AtTestCase):
    _configuration = None
    _randomEnabled = True
    _workAroundEnabled = False
    _numTerminatedLines = None
    _accessWarningEnabled = True
    _ignoreErrorCounters = False

    @classmethod
    def enableRandom(cls, randomEnabled):
        cls._randomEnabled = randomEnabled

    @classmethod
    def enableWorkAround(cls, enabled):
        cls._workAroundEnabled = enabled

    @classmethod
    def enableAccessWarning(cls, enabled):
        cls._accessWarningEnabled = enabled

    @classmethod
    def ignoreErrorCounters(cls, ignored):
        cls._ignoreErrorCounters = ignored

    @classmethod
    def setNumTerminatedLines(cls, numLines):
        cls._numTerminatedLines = numLines

    @classmethod
    def simulated(cls):
        return cls.app().isSimulated()

    @classmethod
    def sleep(cls, seconds):
        if not cls.simulated():
             time.sleep(seconds)

    @classmethod
    def createConfiguration(cls):
        return Configuration(simulated=cls.simulated(), ignoreBertErrorCounters=cls._ignoreErrorCounters)

    @classmethod
    def configuration(cls):
        """
        :rtype: Configuration
        """
        return cls._configuration

    @classmethod
    def setUpClass(cls):
        if not cls._accessWarningEnabled:
            cls.runCli("hal debug dis")

        cls.runCli("prbs engine disable 1-28")
        cls.configuration().apply()

    def waitStable(self):
        self.sleep(5)

    def assertBertGood(self):
        self.waitStable()
        self.configuration().clearStatus()

        # Give it a short time
        self.sleep(1)
        self.configuration().assertBertGood()

    def setUp(self):
        self.assertBertGood()

    def testMustHaveBertEngines(self):
        self.assertLess(self.configuration().numBertEngines(), sys.maxint)

    @classmethod
    def configurations(cls):
        productCode = cls.productCode()
        simulated = cls.simulated()
        if productCode == 0x60290022:
            return Mro20GConfiguration.allConfigurations(simulated, cls._randomEnabled, cls._workAroundEnabled, cls._ignoreErrorCounters)

        return list()

    @classmethod
    def TestWithRunner(cls, runner):
        for configuration in cls.configurations():
            cls._configuration = configuration
            runner.run(cls)

def TestMain():
    randomEnabled = True
    needWordAround = False
    numTerminatedLines = None
    accessWarningEnabled = True
    ignoreErrorCounters = False

    def PrintUsage():
        print "Usage: " + sys.argv[0] + "[--help] [--no-random] [--need-work-around] " \
                                        "[--num-terminated-lines=<numLines>] [--no-access-warning] " \
                                        "[--ignore-error-counters]"

    try:
        options = ["help", "no-random", "need-work-around", "num-terminated-lines=", "no-access-warning", "ignore-error-counters"]
        opts, _ = getopt.getopt(sys.argv[1:], "", options)
    except:
        PrintUsage()
        return 1

    for opt, arg in opts:
        if opt == "--no-random":
            randomEnabled = False
        if opt == "--need-work-around":
            needWordAround = True
        if opt == "--num-terminated-lines":
            numTerminatedLines = int(arg)
        if opt == "--no-access-warning":
            accessWarningEnabled = False
        if opt == "--ignore-error-counters":
            ignoreErrorCounters = True
        if opt == "--help":
            PrintUsage()
            return 0

    runner = AtTestCaseRunner.runner()
    AbstractTest.enableRandom(randomEnabled)
    AbstractTest.enableWorkAround(needWordAround)
    AbstractTest.setNumTerminatedLines(numTerminatedLines)
    AbstractTest.enableAccessWarning(accessWarningEnabled)
    AbstractTest.ignoreErrorCounters(ignoreErrorCounters)
    AbstractTest.TestWithRunner(runner)
    runner.summary()

if __name__ == '__main__':
    TestMain()
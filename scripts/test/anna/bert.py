from test.AtTestCase import AtTestCaseRunner
from test.anna.AnnaTestCase import AnnaTestCase, AnnaProductCodeProvider

class SeparateInversion(AnnaTestCase):
    @classmethod
    def vc3CliId(cls):
        return "Invalid"

    @classmethod
    def setUpClass(cls):
        vc3Id = cls.vc3CliId()
        script = """
                device init
                sdh map vc3.%s c3
                prbs engine create vc 1 vc3.%s
                prbs engine enable 1
                """ % (vc3Id, vc3Id)
        cls.assertClassCliSuccess(script)

    def invertOnDirection(self, inverted, direction):
        action = "invert" if inverted else "noinvert"
        self.assertCliSuccess("prbs engine %s %s 1" % (action, direction))

    def invert(self, inverted):
        self.invertOnDirection(inverted, "")

    def invertRx(self, inverted):
        self.invertOnDirection(inverted, "rx")

    def invertTx(self, inverted):
        self.invertOnDirection(inverted, "tx")

    @staticmethod
    def boolFromString(string):
        return True if string.strip() == "en" else False

    def getInvert(self):
        result = self.runCli("show prbs engine 1")
        cellContent = result.cellContent(0, "invert")
        if cellContent in ["en", "dis"]:
            inverted = self.boolFromString(cellContent)
            return inverted, inverted

        [tx, rx] = [string.strip() for string in cellContent.split(",")]
        [_, txInvertString] = tx.split(":")
        [_, rxInvertString] = rx.split(":")

        return self.boolFromString(txInvertString), self.boolFromString(rxInvertString)


    def assertRxInverted(self, inverted):
        _, rxInverted = self.getInvert()
        self.assertEqual(rxInverted, inverted)

    def assertTxInverted(self, inverted):
        txInverted, _ = self.getInvert()
        self.assertEqual(txInverted, inverted)

    def setUp(self):
        self.invert(False)
        self.assertTxInverted(False)
        self.assertRxInverted(False)

    def testInvertTx(self):
        for inverted in [True, False, True]:
            self.invertTx(inverted)
            self.assertTxInverted(inverted)
            self.assertRxInverted(False)

    def testInvertRx(self):
        for inverted in [True, False, True]:
            self.invertRx(inverted)
            self.assertRxInverted(inverted)
            self.assertTxInverted(False)

    def testInvertBoth(self):
        for inverted in [True, False, True]:
            self.invert(inverted)
            self.assertRxInverted(inverted)
            self.assertTxInverted(inverted)

class MroSeparateInversion(SeparateInversion):
    @classmethod
    def vc3CliId(cls):
        return "25.1.1"

    @classmethod
    def canRun(cls):
        return cls.isMroProduct()

class PdhSeparateInversion(SeparateInversion):
    @classmethod
    def vc3CliId(cls):
        return "1"

    @classmethod
    def canRun(cls):
        return cls.isPdhProduct()

class PdhDe3SeparateInversion(PdhSeparateInversion):
    @classmethod
    def setUpClass(cls):
        script = """
                 device init
                 prbs engine create de3 1 1
                 prbs engine enable 1
                 """
        cls.assertClassCliSuccess(script)


def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(MroSeparateInversion)
    runner.run(PdhSeparateInversion)
    runner.run(PdhDe3SeparateInversion)
    runner.summary()

if __name__ == '__main__':
    TestMain()

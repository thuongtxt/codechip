from test.AtTestCase import AtTestCaseRunner
from test.cisco.force_alarm import PathAbstractTest


class MroPathAbstractTest(PathAbstractTest):
    @classmethod
    def canRun(cls):
        return cls.productCode() in [0x60290021, 0x60290022]

    @classmethod
    def setUpClass(cls):
        cls.assertClassCliSuccess("device init")
        cls.assertClassCliSuccess("sdh map aug1.1.1,25.1 vc4")
        cls.assertClassCliSuccess("xc vc connect vc4.1.1 vc4.25.1 two-way")
        cls.assertClassCliSuccess("sdh map vc4.25.1 3xtug3s")
        cls.assertClassCliSuccess("sdh map tug3.25.1.1-25.1.3 7xtug2s")
        cls.assertClassCliSuccess("sdh map tug2.25.1.1.1-25.1.3.7 tu11")
        cls.assertClassCliSuccess("sdh map vc1x.25.1.1.1.1-25.1.3.7.4 de1")

class MroAuTest(MroPathAbstractTest):
    def pathCliId(self):
        return "au4.1.1"

class MroTu1xTest(MroPathAbstractTest):
    def pathCliId(self):
        return "tu1x.25.1.2.4.2"

class MroTerminatedAuTest(MroPathAbstractTest):
    def pathCliId(self):
        return "au4.25.1"

    def forceAbleAlarms(self):
        return list()

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(MroAuTest)
    runner.run(MroTu1xTest)
    runner.run(MroTerminatedAuTest)
    runner.summary()

if __name__ == '__main__':
    TestMain()

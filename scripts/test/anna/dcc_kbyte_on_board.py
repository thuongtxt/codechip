import random
import time

from python.EpApp import At4848App
from python.arrive.AtAppManager.AtColor import AtColor
import test.AtTestCase as AtTestCase
import python.arrive.atsdk.AtRegister as AtRegister
from python.arrive.atsdk.cli import AtCliRunnable
from test.anna.AnnaTestCase import AnnaTestCase

class DccCliController(AtCliRunnable):
    _controller = None

    @classmethod
    def controller(cls):
        """
        :rtype: DccCliController
        """
        if cls._controller is None:
            cls._controller = DccCliController()
        return cls._controller

    @classmethod
    def runCli(cls, cli):
        """
        :rtype: python.arrive.AtAppManager.AtCliResult.AtCliResult
        """
        result = AtCliRunnable.runCli(cli)
        assert result.success()
        return result

    def setExpectedLabel(self, hdlcIdString, labelValue):
        self.runCli("ciena pw dcc expectedlabel %s 0x%x" % (hdlcIdString, labelValue))

    @staticmethod
    def layers():
        return ["section", "line"]

    def createHdlc(self, lineCliId, layer):
        self.runCli("sdh line dcc create %s %s" % (lineCliId, layer))

    def deleteHdlc(self, hdlcIdString):
        self.runCli("sdh line dcc delete %s" % hdlcIdString)

    def init(self, hdlcIdString):
        self.runCli("sdh line dcc init %s" % hdlcIdString)

    def getHdlcInfo(self, hdlcIdString):
        """
        :rtype: python.arrive.AtAppManager.AtCliResult.AtCliResult
        """
        return self.runCli("show sdh line dcc hdlc %s" % hdlcIdString)

    def setGlobalMac(self, ethPortCliId, mac):
        self.runCli("ciena pw dcc mac global %s %s" % (ethPortCliId, mac))

    def getGlobalMac(self, ethPortCliId):
        cliResult = self.runCli("show ciena eth port dcc_kbyte %s" % ethPortCliId)
        return cliResult.cellContent(0, "expectedGlobalMac")

    def setTxHeader(self, hdlcIdString, dmac, vlan):
        self.runCli("ciena pw dcc ethheader %s %s %s none" % (hdlcIdString, dmac, vlan))

    def enableHdlc(self, hdlcCliId, enabled = True):
        command = "enable" if enabled else "disable"
        self.runCli("sdh line dcc tx %s %s" % (command, hdlcCliId))
        self.runCli("sdh line dcc rx %s %s" % (command, hdlcCliId))

    def setFcsOrdering(self, hdlcCliId, fcsOrdering):
        self.runCli("sdh line dcc hdlc fcs bitorder %s %s" % (hdlcCliId, fcsOrdering))

    def enableMacCheck(self, hdlcCliId, enabled=True):
        cli = "ciena pw dcc mac check %s %s" % ("enable" if enabled else "disable", hdlcCliId)
        self.runCli(cli)

    def getPwAttribute(self, hdlcCliId, cellName, valueConvertFunction):
        cliResult = self.runCli("show ciena pw hdlc %s" % hdlcCliId)
        results = dict()
        for row in range(cliResult.numRows()):
            cliId = cliResult.cellContent(row, "PW")
            valueString = cliResult.cellContent(row, cellName)
            results[cliId] = valueConvertFunction(valueString)

        if len(results) == 1:
            return results.values()[0]

        return results

    def macCheckIsEnabled(self, hdlcCliId):
        def valueConvertFunction(valueString):
            return True if valueString == "en" else False
        return self.getPwAttribute(hdlcCliId, "MacCheckEn", valueConvertFunction)

    def enableVlanCheck(self, hdlcCliId, enabled=True):
        cli = "ciena pw dcc vlan check %s %s" % ("enable" if enabled else "disable", hdlcCliId)
        self.runCli(cli)

    def vlanCheckIsEnabled(self, hdlcCliId):
        def valueConvertFunction(valueString):
            return True if valueString == "en" else False
        return self.getPwAttribute(hdlcCliId, "VlanCheckEn", valueConvertFunction)

    def setExpectedVlan(self, ethPortId, expectedVlan):
        self.runCli("ciena eth port cvlan expect %s 0x%x" % (ethPortId, expectedVlan))

    @staticmethod
    def _parseAlarm(cliResult):
        """
        :type cliResult: python.arrive.AtAppManager.AtCliResult.AtCliResult
        """
        alarms = list()
        for column in range(1, cliResult.numColumns()):
            alarmName = cliResult.columnName(column)
            status = cliResult.cellContent(rowId=0, columnIdOrName=alarmName)
            if status == "set":
                alarms.append(alarmName)

        return alarms

    def getEthAlarm(self, ethPortCliId):
        cliResult = self.runCli("show ciena eth port dcc_kbyte alarm %s" % ethPortCliId)
        return self._parseAlarm(cliResult)

    def getEthInterrupt(self, ethPortCliId, read2Clear=True):
        cliResult = self.runCli("show ciena eth port dcc_kbyte interrupt %s %s" % (ethPortCliId, "r2c" if read2Clear else "ro"))
        return self._parseAlarm(cliResult)

class AbstractTest(AnnaTestCase):
    _productCode = None
    _simulated = None
    _randomEnabled = True

    @classmethod
    def sleep(cls, seconds):
        if not cls.simulated():
            time.sleep(seconds)

    @classmethod
    def msleep(cls, ms):
        cls.sleep(ms / 1000.0)

    @classmethod
    def usleep(cls, us):
        cls.sleep(us / 1000000.0)

    @classmethod
    def shouldRunOnBoard(cls):
        return False

    @classmethod
    def enableRandom(cls, enabled=True):
        cls._randomEnabled = enabled

    @classmethod
    def randomIsEnabled(cls):
        return cls._randomEnabled

    @classmethod
    def simulated(cls):
        if cls._simulated is None:
            cls._simulated = cls.app().isSimulated()
        return cls._simulated

    @classmethod
    def ethPortCliId(cls):
        return "18"

    @classmethod
    def numLines(cls):
        if cls.is20G():
            return 16
        return 8

    @classmethod
    def numKByteChannels(cls):
        return 16
    
    @classmethod
    def lineCliIdString(cls):
        return "1-%d" % cls.numLines()

    @classmethod
    def allKbyteChannelCliIds(cls):
        return "1-%d" % cls.numKByteChannels()

    @classmethod
    def initDevice(cls):
        cls.runCli("device show readwrite dis dis")
        if not OptionParser.parser().accessWarningEnabled:
            cls.runCli("hal debug dis")
        if not OptionParser.parser().deviceInitDisabled:
            cls.assertClassCliSuccess("device init")

    @classmethod
    def anyMacBytes(cls):
        numbers = list()
        for _ in range(6):
            numbers.append(random.choice(range(256)))
        return numbers

    @staticmethod
    def macBytes2String(macBytes):
        return ".".join(["%02x" % value for value in macBytes])

    @staticmethod
    def anyMac():
        return AbstractTest.macBytes2String(AbstractTest.anyMacBytes())

    @staticmethod
    def anyVlanString(tpid=None):
        priority = random.randint(0, 7)
        cfi = random.randint(0, 1)
        vlanId = random.randint(0, 4095)
        vlanString = "%d.%d.%d" % (priority, cfi, vlanId)

        if tpid is None:
            return vlanString

        return "0x%x.%s" % (tpid, vlanString)

    def ethCounters(self, read2Clear = False):
        if self.hasEthCounters():
            return SgmiiEthCounter.counter(self.ethPortCliId(), read2Clear=read2Clear)
        return None

    def assertEthNumPackets(self, numPackets, read2Clear = False):
        counter = self.ethCounters(read2Clear)

        if self.simulated() or counter is None:
            return

        self.assertEqual(counter.txPackets, numPackets)
        self.assertEqual(counter.rxPackets, numPackets)
        self.assertEqual(counter.rxErrFcsPackets, 0)
        self.assertEqual(counter.txErrFcsPackets, 0)

        if numPackets == 0:
            self.assertEqual(counter.txBytes, 0)
            self.assertEqual(counter.rxBytes, 0)
        else:
             self.assertGreater(counter.txBytes, numPackets)
             self.assertGreater(counter.rxBytes, numPackets)
            
    def assertEthGoodPackets(self):
        counter = SgmiiEthCounter.counter(self.ethPortCliId())

        if not self.simulated():
            self.assertGreater(counter.txPackets, 0)
            self.assertGreater(counter.txBytes, 0)
            self.assertGreater(counter.rxPackets, 0)
            self.assertGreater(counter.rxBytes, 0)
            self.assertEqual(counter.rxErrFcsPackets, 0)
            self.assertEqual(counter.txErrFcsPackets, 0)

    @classmethod
    def hasEthCounters(cls):
        return cls.is20G()

    @classmethod
    def clearEthCounters(cls):
        if cls.hasEthCounters():
            SgmiiEthCounter(cls.ethPortCliId()).clearCounter()

    def assertNoEthPackets(self):
        if self.hasEthCounters():
            self.assertEthNumPackets(numPackets=0, read2Clear=True)

    @staticmethod
    def sgmiiSerdesCliId():
        return 28

    @classmethod
    def loopbackEthernet(cls):
        cls.assertClassCliSuccess("serdes loopback %d local" % cls.sgmiiSerdesCliId())

    @classmethod
    def maxLabelValue(cls):
        return 255

    @classmethod
    def anyLabel(cls, maxLabel=None):
        if maxLabel is None:
            maxLabel = cls.maxLabelValue()
        return random.randint(0, maxLabel)

    @classmethod
    def anyEthType(cls):
        return random.choice(range(65536))

class DccAbstractTest(AbstractTest):
    @classmethod
    def canRun(cls):
        # TODO: open this if PDH needs to be tested and of course, this script has to be updated
        return cls.isMroProduct()

    @classmethod
    def loopbackLine(cls):
        cls.assertClassCliSuccess("serdes loopback %s local" % cls.lineCliIdString())

    @staticmethod
    def layers():
        return ["section", "line"]

    @classmethod
    def lineRate(cls):
        return "stm4"

    @classmethod
    def dccCliController(cls):
        return DccCliController.controller()

    @classmethod
    def setUpClass(cls):
        AbstractTest.setUpClass()

        # Configure lines
        cls.initDevice()
        cls.runCli("serdes mode %s %s" % (cls.lineCliIdString(), cls.lineRate()))
        cls.runCli("sdh line rate %s %s" % (cls.lineCliIdString(), cls.lineRate()))  # So that full 16 lines can be used

        controller = cls.dccCliController()
        if OptionParser.parser().deviceInitDisabled:
            for layer in cls.dccCliController().layers():
                controller.deleteHdlc("%s.%s" % (cls.lineCliIdString(), layer))

        # Create HDLC
        for layer in DccCliController.layers():
            controller.createHdlc(cls.lineCliIdString(), layer)

        # Create PWs
        controller.init(cls.hdlcIdsString())

    @classmethod
    def hdlcIdsString(cls):
        return "%s.section|%s.line" % (cls.lineCliIdString(), cls.lineCliIdString())

    @classmethod
    def allHdlcCliIds(cls):
        """
        :rtype: list
        """
        ids = []
        for lineId in range(cls.numLines()):
            cliId = lineId + 1
            for layer in DccCliController.layers():
                ids.append("%s.%s" % (cliId, layer))

        return ids

    @classmethod
    def numHdlcs(cls):
        return cls.numLines() * 2

    @classmethod
    def labelWidth(cls):
        return 5 if cls.isMroProduct() else 6

    @classmethod
    def maxLabelValue(cls):
        return (2 ** cls.labelWidth()) - 1

class DccHdlcTest(DccAbstractTest):
    def retrieveInfo(self):
        """
        :rtype: python.arrive.AtAppManager.AtCliResult.AtCliResult
        """
        return self.dccCliController().getHdlcInfo(self.hdlcIdsString())

    def testEnabling(self):
        for direction in ["rx", "tx"]:
            for enable in ["enable", "disable"]:
                cli = "sdh line dcc %s %s %s" % (direction, enable, self.hdlcIdsString())
                self.assertCliSuccess(cli)
                result = self.runCli("show sdh line dcc hdlc %s" % self.hdlcIdsString())
                cellName = "TxEnable" if direction == "tx" else "RxEnable"
                cellExpectedValue = "en" if enable == "enable" else "dis"
                for row in range(result.numRows()):
                    self.assertEqual(result.cellContent(row, cellName), cellExpectedValue)

    def testFlags(self):
        for flag in [0x7E, 0xFF]:
            cli = "sdh line dcc hdlc flag %s %x" % (self.hdlcIdsString(), flag)
            self.assertCliSuccess(cli)
            result = self.retrieveInfo()
            for row in range(result.numRows()):
                cellContent = result.cellContent(row, "Flag")
                self.assertEqual(int(cellContent, 16), flag)

    def testFcs(self):
        def TestFcs(fcsMode, expectFailure):
            cli = "sdh line dcc hdlc fcs %s %s" % (self.hdlcIdsString(), fcsMode)
            if expectFailure:
                self.assertCliFailure(cli)
                return

            self.assertCliSuccess(cli)
            result = self.retrieveInfo()
            for row in range(result.numRows()):
                cellContent = result.cellContent(row, "FcsMode")
                self.assertEqual(cellContent, fcsMode)

        TestFcs("fcs16", expectFailure=False)
        if self.is10G():
            TestFcs("fcs32", expectFailure=True)
        else:
            TestFcs("fcs32", expectFailure=False)

    def testCannotChangeToNotSupportFlag(self):
        cli = "sdh line dcc hdlc flag %s 0x1" % self.hdlcIdsString()
        self.assertCliFailure(cli)

    def testScramble(self):
        for scramble in ["enable", "disable"]:
            cli = "sdh line dcc hdlc scramble %s %s" % (scramble, self.hdlcIdsString())
            self.assertCliSuccess(cli)
            result = self.retrieveInfo()
            for row in range(result.numRows()):
                cellContent = result.cellContent(row, "Scramble")
                expectValue = "en" if scramble == "enable" else "dis"
                self.assertEqual(cellContent, expectValue)

    def testStuffing(self):
        for stuffing in ["bit", "byte"]:
            cli = "sdh line dcc hdlc stuffing %s %s" % (self.hdlcIdsString(), stuffing)
            self.assertCliSuccess(cli)
            result = self.retrieveInfo()
            for row in range(result.numRows()):
                cellContent = result.cellContent(row, "stuffing")
                self.assertEqual(cellContent, stuffing)

    def testInterruptMask(self):
        masks = ["fcs",
                 "undersize",
                 "oversize",
                 "decbuffull",
                 "encbuffull"]

        def assertMask(masks_):
            """
            :type masks_: list
            """
            result = self.retrieveInfo()
            for row in range(result.numRows()):
                cellContent = result.cellContent(row, "intrMask")
                if len(masks_) == 0:
                    self.assertEqual(cellContent, "none")
                else:
                    currentMasks = cellContent.split("|")
                    currentMasks.sort()
                    masks_.sort()
                    self.assertEqual(currentMasks, masks_)

        def createCliMask(masks_):
            return "|".join(masks_)

        cliMask = createCliMask(masks)
        self.assertCliSuccess("sdh line dcc hdlc interruptmask %s %s en" % (self.hdlcIdsString(), cliMask))
        assertMask(masks)
        self.assertCliSuccess("sdh line dcc hdlc interruptmask %s %s dis" % (self.hdlcIdsString(), cliMask))
        assertMask(list())

class DccPwDefaultSettingTest(DccAbstractTest):
    def testDefaultSettingMustBeRight(self):
        cliResult = self.runCli("show ciena pw hdlc %s" % self.hdlcIdsString())
        for row in range(cliResult.numRows()):
            self.assertEqual(int(cliResult.cellContent(row, "Version"), 16), 0x1)
            self.assertEqual(int(cliResult.cellContent(row, "Type"), 16), 0x41)
            self.assertEqual(cliResult.cellContent(row, "MacCheckEn"), "dis")
            self.assertEqual(cliResult.cellContent(row, "VlanCheckEn"), "dis")
            self.assertEqual(int(cliResult.cellContent(row, "EthType"), 16), 0x880b)

class DccPwTest(DccAbstractTest):
    def retrieveInfo(self, hdlcCliId = None):
        """
        :rtype: python.arrive.AtAppManager.AtCliResult.AtCliResult
        """
        if hdlcCliId is None:
            hdlcCliId = self.hdlcIdsString()
        return self.runCli("show ciena pw hdlc %s" % hdlcCliId)

    def testSourceMac(self):
        for mac in ["11.22.33.44.55.66", "66.55.44.33.22.11"]:
            cli = "ciena pw dcc sourcemac %s %s" % (self.hdlcIdsString(), mac)
            self.assertCliSuccess(cli)
            result = self.retrieveInfo()
            for row in range(result.numRows()):
                cellContent = result.cellContent(row, "SMAC")
                self.assertEqual(cellContent, mac)

    def testTxHeader(self):
        def TestWithHeader(_dmac, _vlan):
            cli = "ciena pw dcc ethheader %s %s %s none" % (self.hdlcIdsString(), _dmac, _vlan)
            self.assertCliSuccess(cli)
            result = self.retrieveInfo()
            for row in range(result.numRows()):
                self.assertEqual(result.cellContent(row, "DMAC"), _dmac)
                self.assertEqual(result.cellContent(row, "VLAN"), _vlan)

        TestWithHeader("77.66.55.44.33.22", "3.1.123")
        TestWithHeader("22.44.55.77.11.99", "4.1.1234")

    def testChangeVersion(self):
        def TestVersion(version_):
            self.assertCliSuccess("ciena pw dcc version %s %x" % (self.hdlcIdsString(), version_))
            result = self.retrieveInfo()
            for row in range(result.numRows()):
                self.assertEqual(int(result.cellContent(row, "Version"), 16), version_)

        for version in random.sample(range(256), 10):
            TestVersion(version)

    def testChangeType(self):
        def TestType(headerType):
            self.assertCliSuccess("ciena pw dcc type %s %x" % (self.hdlcIdsString(), headerType))
            result = self.retrieveInfo()
            for row in range(result.numRows()):
                self.assertEqual(int(result.cellContent(row, "Type"), 16), headerType)

        for version in random.sample(range(256), 10):
            TestType(version)

    def testMacCheck(self):
        controller = self.dccCliController()

        for enabled in [True, False]:
            controller.enableMacCheck(self.hdlcIdsString(), enabled)
            for cliId, value in controller.macCheckIsEnabled(self.hdlcIdsString()).iteritems():
                self.assertEqual(value, enabled)

    def testVlanCheck(self):
        controller = self.dccCliController()

        for enabled in [True, False, True]:
            controller.enableVlanCheck(self.hdlcIdsString(), enabled)
            for cliId, value in controller.vlanCheckIsEnabled(self.hdlcIdsString()).iteritems():
                self.assertEqual(value, enabled)

    def testPortExpectedVlan(self):
        def TestVlan(vlan_):
            self.assertCliSuccess("ciena eth port cvlan expect %s 0x%x" % (self.ethPortCliId(), vlan_))
            result = self.retrieveInfo()
            for row in range(result.numRows()):
                self.assertEqual(int(result.cellContent(row, "ExpectCVlan"), 16), vlan_)

        for vlan in random.sample(range(4096), 10):
            TestVlan(vlan)

    @classmethod
    def maxLabelValue(cls):
        return 31

    def testExpectedLabel(self):
        labels = list()
        controller = self.dccCliController()
        for hdlcId in self.allHdlcCliIds():
            label = self.anyLabel()

            while True:
                if label in labels:
                    label = self.anyLabel()
                labels.append(label)
                break

            controller.setExpectedLabel(hdlcId, label)
            result = self.retrieveInfo(hdlcId)
            self.assertEqual(int(result.cellContent(0, "ExpectLabel"), 16), label)

    def testInterruptMask(self):
        masks = "|".join(["rx-pkt-discard"]) # More masks to be added
        
        cli = "ciena pw dcc interruptmask %s %s en" % (self.hdlcIdsString(), masks)
        self.assertCliSuccess(cli)
        result = self.retrieveInfo()
        for row in range(result.numRows()):
            maskString = result.cellContent(row, "intrMask")
            self.assertEqual(maskString, masks)
            
        cli = "ciena pw dcc interruptmask %s %s dis" % (self.hdlcIdsString(), masks)
        self.assertCliSuccess(cli)
        result = self.retrieveInfo()
        for row in range(result.numRows()):
            maskString = result.cellContent(row, "intrMask")
            self.assertEqual(maskString, "none")

class EthPortTest(DccAbstractTest):
    def testSubPortVlanIsNotSupported(self):
        self.assertCliFailure("ciena eth port subport vlan expect %s 0x123" % self.ethPortCliId())
        self.assertCliFailure("ciena eth port subport vlan transmit %s 0x123" % self.ethPortCliId())

    def testExpectedGlobalMac(self):
        def TestWithMac(mac):
            controller = self.dccCliController()
            controller.setGlobalMac(self.ethPortCliId(), mac)
            globalMac = controller.getGlobalMac(self.ethPortCliId())

            expectedMac = [int(byteString, 16) for byteString in mac.split(".")]
            expectedMac[len(expectedMac) - 1] = expectedMac[len(expectedMac) - 1] & (~AtRegister.cBit4_0)
            expectedMacString = ".".join(["%02x" % byteValue for byteValue in expectedMac])
            self.assertEqual(globalMac, expectedMacString)

        TestWithMac("11.22.33.44.55.66")
        TestWithMac("66.55.44.33.22.11")

class KbytePwAbstractTest(AbstractTest):
    @classmethod
    def canRun(cls):
        return cls.is20G()

    def enableValidation(self, enabled, channelCliIds):
        cli = "ciena pw kbyte channel validation %s %s" % ("enable" if enabled else "disable", channelCliIds)
        self.assertCliSuccess(cli)

    @classmethod
    def suppress(cls, suppressed, channelCliIds):
        cli = "ciena pw kbyte channel suppress %s %s" % ("enable" if suppressed else "disable", channelCliIds)
        cls.assertClassCliSuccess(cli)

    def setKbyteSource(self, lineCliId, source):
        cli = "ciena sdh line kbyte source %s %s" % (lineCliId, source)
        self.assertCliSuccess(cli)

    def setWatchdog(self, timerValue):
        cli = "ciena pw kbyte watchdog %d" % timerValue
        self.assertCliSuccess(cli)

    def setTxEthType(self, ethType):
        self.assertCliSuccess("ciena pw kbyte ethtype transmit 0x%x" % ethType)

    def setExpectedEthType(self, ethType):
        self.assertCliSuccess("ciena pw kbyte ethtype expect 0x%x" % ethType)

    def setTxVersion(self, version):
        self.assertCliSuccess("ciena pw kbyte version transmit 0x%x" % version)

    def setExpectedVersion(self, version):
        self.assertCliSuccess("ciena pw kbyte version expect 0x%x" % version)

    def setTxType(self, pwType):
        self.assertCliSuccess("ciena pw kbyte type transmit 0x%x" % pwType)

    def setExpectedType(self, pwType):
        self.assertCliSuccess("ciena pw kbyte type expect 0x%x" % pwType)

    def setTransmitLabel(self, channelCliId, label):
        self.assertCliSuccess("ciena pw kbyte label transmit %s 0x%x" % (channelCliId, label))

    def setExpectLabel(self, channelCliId, label):
        self.assertCliSuccess("ciena pw kbyte label expect %s 0x%x" % (channelCliId, label))

    def getTransmitLabel(self, channelCliId):
        result = self.runCli("show ciena pw kbyte channel %s" % channelCliId)
        return int(result.cellContent(0, "TxLabel"), 16)

    def getExpectLabel(self, channelCliId):
        result = self.runCli("show ciena pw kbyte channel %s" % channelCliId)
        return int(result.cellContent(0, "ExpectedLabel"), 16)

    def enableKByteOverride(self, channelCliId, enabled):
        cli = "ciena pw kbyte channel override %s %s" % ("enable" if enabled else "disable", channelCliId)
        self.assertCliSuccess(cli)

    def overrideKByteWithName(self, channelCliId, kByteName, value):
        cli = "ciena pw kbyte channel override %s %s 0x%x" % (kByteName.lower(), channelCliId, value)
        self.assertCliSuccess(cli)

    def setRemoteMac(self, mac):
        self.assertCliSuccess("ciena pw kbyte remotemac %s" % mac)

    def setCentralMac(self, mac):
        self.assertCliSuccess("ciena pw kbyte centralmac %s" % mac)

    def retrieveInfo(self):
        """
        :rtype: python.arrive.AtAppManager.AtCliResult.AtCliResult
        """
        return self.runCli("show ciena pw kbyte")

    def getRemoteMac(self):
        return self.retrieveInfo().cellContent(0, "RemoteMAC")

    def getCentralMac(self):
        return self.retrieveInfo().cellContent(0, "CentralMAC")

class KbytePwTest(KbytePwAbstractTest):
    @classmethod
    def canRun(cls):
        return cls.is20G()

    @classmethod
    def setUpClass(cls):
        # Note: STM-16 is used in this test, just because when STM-16 is specified,
        # there are some hidden lines. And this test will enforce number of PW k-byte
        # channel of K-Byte PWs will not depend on number of visible lines
        cls.initDevice()
        cls.runCli("serdes mode %s stm16" % cls.lineCliIdString())
        cls.runCli("sdh line rate %s stm16" % cls.lineCliIdString())

    def testTxHeader(self):
        def TestWithHeader(_dmac, _vlan):
            cli = "ciena pw kbyte ethheader %s %s none" % (_dmac, _vlan)
            self.assertCliSuccess(cli)
            result = self.retrieveInfo()
            self.assertEqual(result.cellContent(0, "CentralMAC"), _dmac)
            self.assertEqual(result.cellContent(0, "VLAN"), _vlan)

        TestWithHeader("77.66.55.44.33.22", "3.1.123")
        TestWithHeader("22.44.55.77.11.99", "4.1.1234")

    def testChangeVersion(self):
        def TestVersion(version_):
            self.assertCliSuccess("ciena pw kbyte version %x" % version_)
            result = self.retrieveInfo()
            self.assertEqual(int(result.cellContent(0, "Version"), 16), version_)

        for version in random.sample(range(256), 10):
            TestVersion(version)

    def testChangeType(self):
        def TestType(headerType):
            self.assertCliSuccess("ciena pw kbyte type %x" % headerType)
            result = self.retrieveInfo()
            self.assertEqual(int(result.cellContent(0, "Type"), 16), headerType)

        for version in random.sample(range(256), 10):
            TestType(version)

    def testInterruptMask(self):
        masks = "|".join(["da-mismatch",
                          "ethtype-mismatch",
                          "ver-mismatch",
                          "type-mismatch",
                          "length-field-mismatch",
                          "length-count-mismatch",
                          "channel-mismatch",
                          "watchdog-expire"])

        cli = "ciena pw kbyte interruptmask %s en" % masks
        self.assertCliSuccess(cli)
        result = self.retrieveInfo()
        maskString = result.cellContent(0, "intrMask")
        self.assertEqual(maskString, masks)

        cli = "ciena pw kbyte interruptmask %s dis" % masks
        self.assertCliSuccess(cli)
        result = self.retrieveInfo()
        maskString = result.cellContent(0, "intrMask")
        self.assertEqual(maskString, "none")

    def testThereMustBeEnoughChannels(self):
        result = self.runCli("show ciena pw kbyte channel %s" % self.allKbyteChannelCliIds())
        self.assertEqual(result.numRows(), self.numKByteChannels())

    @classmethod
    def maxLabelValue(cls):
        return (2 ** 12) - 1

    def testLabel(self):
        def TestWithLabel(testLabel):
            for channelId in range(self.numKByteChannels()):
                channelCliId = "%d" % (channelId + 1)
                self.setTransmitLabel(channelCliId, testLabel)
                self.setExpectLabel(channelCliId, testLabel)
                result = self.runCli("show ciena pw kbyte channel %s" % self.allKbyteChannelCliIds())
                self.assertEqual(int(result.cellContent(channelId, "TxLabel"), 16), testLabel)
                self.assertEqual(int(result.cellContent(channelId, "ExpectedLabel"), 16), testLabel)

        for label in random.sample(range(self.maxLabelValue()), 10):
            TestWithLabel(label)

    def testCannotChangeOverRangeLabel(self):
        invalidValue = self.maxLabelValue() + 1
        self.assertCliFailure("ciena pw kbyte label transmit %s 0x%x" % (self.allKbyteChannelCliIds(), invalidValue))
        self.assertCliFailure("ciena pw kbyte label expect %s 0x%x" % (self.allKbyteChannelCliIds(), invalidValue))

    def testExtendKByteEnabling(self):
        for enable in ["enable", "disable"]:
            cli = "ciena sdh line extendedkbyte %s %s" % (enable, self.lineCliIdString())
            self.assertCliSuccess(cli)
            result = self.runCli("show ciena sdh line extendedkbyte %s" % self.lineCliIdString())
            for row in range(result.numRows()):
                cellContent = result.cellContent(row, "enable")
                expectedValue = "en" if enable == "enable" else "dis"
                self.assertEqual(cellContent, expectedValue)

    def testKbytePosition(self):
        for position in ["d1-sts1-4", "d1-sts1-10"]:
            cli = "ciena sdh line extendedkbyte position %s %s" % (self.lineCliIdString(), position)
            self.assertCliSuccess(cli)
            result = self.runCli("show ciena sdh line extendedkbyte %s" % self.lineCliIdString())
            for row in range(result.numRows()):
                cellContent = result.cellContent(row, "position")
                self.assertEqual(cellContent, position)

    def testKByteValidation(self):
        for enable in [True, False]:
            self.enableValidation(enable, self.allKbyteChannelCliIds())
            result = self.runCli("show ciena pw kbyte channel %s" % self.allKbyteChannelCliIds())
            for row in range(result.numRows()):
                cellContent = result.cellContent(row, "Validation")
                expectedValue = "en" if enable else "dis"
                self.assertEqual(cellContent, expectedValue)

    def retrievePwInfo(self):
        return self.runCli("show ciena pw kbyte")

    def testInterval(self):
        minIntervalUs = 0
        maxIntervalUs = 8000
        randomIntervals = random.sample(range(maxIntervalUs + 1), 10)
        intervals = [minIntervalUs]
        intervals.extend(randomIntervals)
        intervals.append(maxIntervalUs)
        for interval in intervals:
            cli = "ciena pw kbyte interval 0x%x" % interval
            self.assertCliSuccess(cli)
            result = self.retrieveInfo()
            cellContent = result.cellContent(0, "Interval[us]")
            self.assertEqual(int(cellContent, 10), interval)

    def testSetOutOfRangeInterval(self):
        self.assertCliFailure("ciena pw kbyte interval 8001")

    def testSuppress(self):
        for enable in [True, False]:
            self.suppress(enable, self.allKbyteChannelCliIds())
            result = self.runCli("show ciena pw kbyte channel %s" % self.allKbyteChannelCliIds())
            for row in range(result.numRows()):
                cellContent = result.cellContent(row, "Suppress")
                expectedValue = "en" if enable else "dis"
                self.assertEqual(cellContent, expectedValue)

    def testWatchdog(self):
        minTimerUs = 0
        maxTimerUs = 16000
        randomTimers = random.sample(range(maxTimerUs + 1), 10)
        timers = [minTimerUs, maxTimerUs]
        timers.extend(randomTimers)
        for timerValue in timers:
            self.setWatchdog(timerValue)
            result = self.retrieveInfo()
            cellContent = result.cellContent(0, "WatchDog[us]")
            self.assertEqual(int(cellContent, 10), timerValue)

    def testKByteSource(self):
        for source in ["cpu", "sgmii"]:
            self.setKbyteSource(self.lineCliIdString(), source)
            result = self.runCli("show ciena sdh line kbyte %s" % self.lineCliIdString())
            for row in range(result.numRows()):
                cellContent = result.cellContent(row, "Source")
                self.assertEqual(cellContent, source)

    def TestTxEKn(self, commandToken, cellName):
        self.skipTest("Hardware has not support to configure TX EK1/EK2")

        for value in [0, 1]:
            cli = "ciena sdh line %s %s %d" % (commandToken, self.lineCliIdString(), value)
            self.assertCliSuccess(cli)
            result = self.runCli("show ciena sdh line kbyte %s" % self.lineCliIdString())
            for row in range(result.numRows()):
                cellContent = result.cellContent(row, cellName)
                self.assertEqual(int(cellContent, 16), value)

    def testTxEK1(self):
        self.TestTxEKn(commandToken="ek1", cellName="TxEK1")

    def testTxEK2(self):
        self.TestTxEKn(commandToken="ek2", cellName="TxEK2")

    def TestMac(self, commandToken, cellName):
        for mac in ["11.22.33.44.55.66", "66.55.44.33.22.11"]:
            self.assertCliSuccess("ciena pw kbyte %s %s" % (commandToken, mac))
            result = self.retrieveInfo()
            self.assertEqual(result.cellContent(0, cellName), mac)

    def testRemoteMac(self):
        self.TestMac("remotemac", "RemoteMAC")

    def testCentralMac(self):
        self.TestMac("centralmac", "CentralMAC")

    def testOverride(self):
        for enable in [True, False]:
            self.enableKByteOverride(self.allKbyteChannelCliIds(), enable)
            result = self.runCli("show ciena pw kbyte channel %s" % self.allKbyteChannelCliIds())
            for row in range(result.numRows()):
                cellContent = result.cellContent(row, "Override")
                expectedValue = "en" if enable else "dis"
                self.assertEqual(cellContent, expectedValue)

    def TestOverrideKn(self, values, commandToken, cellName):
        for value in values:
            self.overrideKByteWithName(self.allKbyteChannelCliIds(), commandToken, value)
            result = self.runCli("show ciena pw kbyte channel %s" % self.allKbyteChannelCliIds())
            for row in range(result.numRows()):
                cellContent = result.cellContent(row, cellName)
                self.assertEqual(int(cellContent, 16), value)

    def testOverrideK1(self):
        self.TestOverrideKn(random.sample(range(256), 10), commandToken="k1", cellName="OverrideK1")

    def testOverrideK2(self):
        self.TestOverrideKn(random.sample(range(256), 10), commandToken="k2", cellName="OverrideK2")

    def testOverrideEK1(self):
        self.TestOverrideKn([0, 1], commandToken="ek1", cellName="OverrideEK1")

    def testOverrideEK2(self):
        self.TestOverrideKn([0, 1], commandToken="ek2", cellName="OverrideEK2")

    def testTrigger(self):
        for _ in range(10):
            self.assertCliSuccess("ciena pw kbyte trigger")

class KbytePwChannelManagementTest(AbstractTest):
    @classmethod
    def canRun(cls):
        return cls.isMroProduct()

    def testNumberOfChannelsDoNotDependOnExistingLines(self):
        self.initDevice()
        result = self.runCli("show ciena pw kbyte channel %s" % self.allKbyteChannelCliIds())
        self.assertEqual(result.numRows(), self.numKByteChannels())

    def testOnlyValidChannelsAreDisplayed(self):
        self.initDevice()
        result = self.runCli("show ciena pw kbyte channel 1-1000")
        self.assertEqual(result.numRows(), self.numKByteChannels())

class Counter(AtCliRunnable):
    def __init__(self, cliId):
        self.cliId = cliId

    def readCounter(self, read2Clear = False):
        pass

    def clearCounter(self):
        return self.readCounter(read2Clear=True)
    
    @classmethod
    def counter(cls, cliId, read2Clear=False):
        return None

class HdlcCounter(Counter):
    def __init__(self, cliId):
        super(HdlcCounter, self).__init__(cliId)
        
        self.txByte = 0
        self.txPacket = 0
        self.txGoodByte = 0
        self.txGoodPacket = 0
        self.rxByte = 0
        self.rxPacket = 0
        self.txDiscardPacket = 0
        self.rxFcsErr = 0
        self.rxDiscardPacket = 0

    @staticmethod
    def valueFromString(string, base = 10):
        if string in ["N/S", "N/A"]:
            return 0
        return int(string, base)

    def readCounter(self, read2Clear = False):
        cli = "show sdh line dcc hdlc counters %s %s" % (self.cliId, "r2c" if read2Clear else "ro")
        result = self.runCli(cli)

        self.txByte          = self.valueFromString(result.cellContent(0, "txByte"))
        self.txPacket        = self.valueFromString(result.cellContent(0, "txPacket"))
        self.txGoodByte      = self.valueFromString(result.cellContent(0, "txGoodByte"))
        self.txGoodPacket    = self.valueFromString(result.cellContent(0, "txGoodPacket"))
        self.rxByte          = self.valueFromString(result.cellContent(0, "rxByte"))
        self.rxPacket        = self.valueFromString(result.cellContent(0, "rxPacket"))
        self.txDiscardPacket = self.valueFromString(result.cellContent(0, "txDiscardPacket"))
        self.rxFcsErr        = self.valueFromString(result.cellContent(0, "rxFcsErr"))
        self.rxDiscardPacket = self.valueFromString(result.cellContent(0, "rxDiscardPacket"))

    @classmethod
    def counter(cls, cliId, read2Clear=False):
        """
        :rtype: HdlcCounter
        """
        counter = HdlcCounter(cliId)
        counter.readCounter(read2Clear)
        return counter

class HdlcPwCounter(Counter):
    def __init__(self, cliId):
        super(HdlcPwCounter, self).__init__(cliId)

        self.txPackets = 0
        self.txPayloadBytes = 0
        self.rxPackets = 0
        self.rxPayloadBytes = 0
        self.rxDiscardedPackets = 0

    def readCounter(self, read2Clear=False):
        cli = "show ciena pw dcc counters %s %s" % (self.cliId, "r2c" if read2Clear else "ro")
        result = self.runCli(cli)
        self.txPackets          = int(result.cellContent(rowId="TxPackets", columnIdOrName=self.cliId))
        self.txPayloadBytes     = int(result.cellContent(rowId="TxPayloadBytes", columnIdOrName=self.cliId))
        self.rxPackets          = int(result.cellContent(rowId="RxPackets", columnIdOrName=self.cliId))
        self.rxPayloadBytes     = int(result.cellContent(rowId="RxPayloadBytes", columnIdOrName=self.cliId))
        self.rxDiscardedPackets = int(result.cellContent(rowId="RxDiscardedPackets", columnIdOrName=self.cliId))

    @classmethod
    def counter(cls, hdlcCliId, read2Clear = False):
        """
        :rtype: HdlcPwCounter
        """
        counter = HdlcPwCounter(hdlcCliId)
        counter.readCounter(read2Clear)
        return counter

class SgmiiEthCounter(Counter):
    def __init__(self, cliId):
        super(SgmiiEthCounter, self).__init__(cliId)
        self.txPackets       = 0
        self.txBytes         = 0
        self.rxPackets       = 0
        self.rxBytes         = 0
        self.rxErrFcsPackets = 0
        self.txErrFcsPackets = 0

    def readCounter(self, read2Clear = False):
        result = self.runCli("show eth port counters %s %s" % (self.cliId, "r2c" if read2Clear else "ro"))

        def counterValue(name):
            return int(result.cellContent(name, columnIdOrName=1))

        self.txPackets       = counterValue("txPackets")
        self.txBytes         = counterValue("txBytes")
        self.rxPackets       = counterValue("rxPackets")
        self.rxBytes         = counterValue("rxBytes")
        self.rxErrFcsPackets = counterValue("rxErrFcsPackets")
        self.txErrFcsPackets = counterValue("txErrFcsPackets")

        return self

    @classmethod
    def counter(cls, cliId, read2Clear=False):
        counter = SgmiiEthCounter(cliId)
        counter.readCounter(read2Clear)
        return counter

class KByteDatapathAbstractTest(KbytePwAbstractTest):
    @classmethod
    def lineRate(cls):
        return "stm4"

    @staticmethod
    def sinkLineCliId():
        return 1

    @staticmethod
    def sourceLineCliId():
        return 3

    @staticmethod
    def sgmiiEthPortCliId():
        return 18

    @classmethod
    def setupLine(cls, lineCliId):
        cls.assertClassCliSuccess("serdes mode %d %s" % (lineCliId, cls.lineRate()))
        cls.assertClassCliSuccess("sdh line rate %d %s" % (lineCliId, cls.lineRate()))
        cls.assertClassCliSuccess("ciena sdh line kbyte source %d cpu" % lineCliId)
        cls.setTxK1(lineCliId, 0x0)
        cls.setTxK2(lineCliId, 0x0)
        cls.suppress(suppressed=False, channelCliIds=lineCliId)

    @classmethod
    def assertLineGood(cls, lineCliId):
        cliResult = cls.runCli("show sdh line alarm %d" % lineCliId)
        for column in range(1, cliResult.numColumns()):
            statusString = cliResult.cellContent(0, columnIdOrName=column).lower()
            assert statusString == "clear"

    @classmethod
    def setTxK1(cls, lineCliId, value):
        cls.assertClassCliSuccess("sdh line k1 %d 0x%x" % (lineCliId, value))

    @classmethod
    def setTxK2(cls, lineCliId, value):
        cls.assertClassCliSuccess("sdh line k2 %d 0x%x" % (lineCliId, value))

    @classmethod
    def getLineAttribute(cls, lineCliId, attributeName):
        cliResult = cls.runCli("show sdh line %d" % lineCliId)
        return int(cliResult.cellContent(0, attributeName), 16)

    @classmethod
    def getRxSgmiiAttribute(cls, channelCliId, attributeName):
        result = cls.runCli("show ciena pw kbyte channel %s" % channelCliId)
        return result.cellContent("%d" % channelCliId, attributeName)

    @classmethod
    def getRxSgmiiK1(cls, channelCliId):
        return int(cls.getRxSgmiiAttribute(channelCliId, "RxK1"), 16)

    @classmethod
    def getRxSgmiiK2(cls, channelCliId):
        return int(cls.getRxSgmiiAttribute(channelCliId, "RxK2"), 16)

    @classmethod
    def getRxK1(cls, lineCliId):
        return cls.getLineAttribute(lineCliId, "rxK1")

    @classmethod
    def getRxK2(cls, lineCliId):
        return cls.getLineAttribute(lineCliId, "rxK2")

    @classmethod
    def assertRxK1(cls, lineCliId, expectedValue):
        if cls.simulated():
            return
        assert cls.getRxK1(lineCliId) == expectedValue

    @classmethod
    def assertSgmiiRxK1(cls, channelCliId, expectedValue):
        if cls.simulated():
            return

        assert cls.getRxSgmiiK1(channelCliId) == expectedValue

    @classmethod
    def assertSgmiiRxK2(cls, channelCliId, expectedValue):
        if cls.simulated():
            return

        assert cls.getRxSgmiiK2(channelCliId) == expectedValue

    @classmethod
    def assertRxK2(cls, lineCliId, expectedValue):
        if cls.simulated():
            return
        assert cls.getRxK2(lineCliId) == expectedValue

    @classmethod
    def assertLinesConnected(cls):
        if cls.simulated():
            return

        cls.setTxK1(cls.sourceLineCliId(), value=cls.sourceLineCliId())
        cls.setTxK1(cls.sinkLineCliId(), value=cls.sinkLineCliId())
        cls.msleep(500)
        cls.assertRxK1(cls.sourceLineCliId(), cls.sinkLineCliId())
        cls.assertRxK1(cls.sinkLineCliId(), cls.sourceLineCliId())

    @classmethod
    def setupAllLines(cls):
        cls.setupLine(cls.sinkLineCliId())
        cls.setupLine(cls.sourceLineCliId())
        cls.sleep(2)
        cls.assertLineGood(cls.sourceLineCliId())
        cls.assertLineGood(cls.sinkLineCliId())
        cls.assertLinesConnected()

    def enableAutoRdi(self, lineCliId, enabled=True):
        cli = "sdh line autordi %s %d" % ("enable" if enabled else "disable", lineCliId)
        self.assertCliSuccess(cli)

    @staticmethod
    def defaultEthType():
        return 0x88B7

    @staticmethod
    def defaultVersion():
        return 0x0

    @staticmethod
    def defaultType():
        return 0x54

    def defaultSetup(self):
        self.enableValidation(enabled=True, channelCliIds=self.sinkLineCliId())
        self.enableKByteOverride(self.sinkLineCliId(), enabled=False)
        self.setTxK2(self.sourceLineCliId(), 0)
        self.setTxK1(self.sourceLineCliId(), 0)

        lineId = self.sinkLineCliId() - 1
        self.setTransmitLabel(self.sinkLineCliId(), lineId)
        self.setExpectLabel(self.sinkLineCliId(), lineId)
        self.enableAutoRdi(self.sinkLineCliId())
        self.enableAutoRdi(self.sourceLineCliId())
        self.setTxEthType(self.defaultEthType())
        self.setExpectedEthType(self.defaultEthType())
        self.setTxType(self.defaultType())
        self.setExpectedType(self.defaultType())
        self.setInterval(1000)
        self.setKbyteSource(self.sinkLineCliId(), "sgmii")
        self.suppress(suppressed=False, channelCliIds=self.sinkLineCliId())

    @classmethod
    def defaultInterval(cls):
        return 1000

    @classmethod
    def setInterval(cls, interval):
        cls.assertClassCliSuccess("ciena pw kbyte interval 0x%x" % interval)

    @classmethod
    def setupKBytePw(cls):
        cls.setInterval(cls.defaultInterval())
        cls.assertClassCliSuccess("ciena pw kbyte remotemac 11.11.11.11.11.11")
        cls.assertClassCliSuccess("ciena pw kbyte centralmac 11.11.11.11.11.11")
        cls.assertClassCliSuccess("ciena sdh line kbyte source %d sgmii" % cls.sinkLineCliId())
        cls.assertClassCliSuccess("ciena sdh line kbyte source %d cpu" % cls.sourceLineCliId())

    @staticmethod
    def anyK1():
        return random.choice(range(255))

    @staticmethod
    def anyK2(excludeAisValue=False):
        while True:
            value = random.choice(range(255))
            if excludeAisValue and KByteDatapathAbstractTest.isAisK2(value):
                continue

            return value

    @staticmethod
    def isAisK2(k2Value):
        return True if k2Value & AtRegister.cBit2_0 == AtRegister.cBit2_0 else False

    @staticmethod
    def setAisCode(k2):
        return k2 & (~AtRegister.cBit2_0) | 0x7

    @staticmethod
    def setRdiCode(k2):
        return k2 & (~AtRegister.cBit2_0) | 0x6

    @staticmethod
    def aisRdiCode(k2):
        return k2 & AtRegister.cBit2_0

    def frameDelay(self, numFrames=5):
        self.usleep(numFrames * 125)

    def getInteruptCliResult(self, read2Clear=False):
        return self.runCli("show ciena pw kbyte interrupt %s" % ("r2c" if read2Clear else "ro"))

    def getAlarmCliResult(self):
        return self.runCli("show ciena pw kbyte alarm")

    def clearInterrupt(self):
        return self.getInteruptCliResult(read2Clear=True)

    def assertNoSetAlarm(self, cliResult):
        for column in range(cliResult.numColumns()):
            statusString = cliResult.cellContent(0, columnIdOrName=column).lower()
            self.assertEqual(statusString, "clear")

    def assertNoInterrupt(self, read2Clear=False):
        self.assertNoSetAlarm(self.getInteruptCliResult(read2Clear))

    def assertNoAlarm(self):
        self.assertNoSetAlarm(self.getAlarmCliResult())

    @classmethod
    def printBar(cls, title):
        AtColor.printColor(AtColor.GREEN, "======================================================")
        AtColor.printColor(AtColor.GREEN, title)
        AtColor.printColor(AtColor.GREEN, "======================================================")

    @classmethod
    def setUpClass(cls):
        cls.printBar("Configure datapath")
        cls.initDevice()
        cls.setupAllLines()
        cls.setupKBytePw()
        cls.loopbackEthernet()

class KByteDatapathDefaultTest(KByteDatapathAbstractTest):
    txValue = None
    expectedValue = None

    @classmethod
    def shouldRunOnBoard(cls):
        return True

    def setUp(self):
        self.printBar("Run test case")
        super(KByteDatapathAbstractTest, self).setUp()
        self.defaultSetup()

    def waitForSgmiiKByteUpdate(self):
        # Without this, test case will be fail. As status at SGMII is only for debug purpose, so accept it for now
        self.msleep(15)

    def testK1MustBeBasicallyTransparent(self):
        for value in random.sample(range(255), 20):
            self.setTxK1(self.sourceLineCliId(), value)
            self.usleep(self.defaultInterval())
            self.assertRxK1(self.sourceLineCliId(), value)
            self.waitForSgmiiKByteUpdate()
            self.assertSgmiiRxK1(self.sinkLineCliId(), value)

    def testK2MustBeBasicallyTransparent(self):
        for value in random.sample(range(255), 20):
            if self.isAisK2(value):
                continue

            self.setTxK2(self.sourceLineCliId(), value)
            self.usleep(self.defaultInterval())
            self.assertRxK2(self.sourceLineCliId(), value)
            self.waitForSgmiiKByteUpdate()
            self.assertSgmiiRxK2(self.sinkLineCliId(), value)

    def TestKByteValidation(self):
        for value in random.sample(range(255), 20):
            self.setTxK1(self.sourceLineCliId(), value)
            self.frameDelay()
            self.assertRxK1(self.sourceLineCliId(), value)

        for value in random.sample(range(255), 20):
            if self.isAisK2(value):
                continue

            self.setTxK2(self.sourceLineCliId(), value)
            self.frameDelay()
            self.assertRxK2(self.sourceLineCliId(), value)

    def testKByteValidation(self):
        self.setInterval(0) # To disable auto transmitting
        self.enableValidation(enabled=True, channelCliIds=self.sinkLineCliId())
        self.TestKByteValidation()

    def testWhenValidationAndIntervalAreEnabled(self):
        self.setInterval(2000) # Give it long time
        self.enableValidation(enabled=True, channelCliIds=self.sinkLineCliId())
        self.TestKByteValidation()

    def testSuppress(self):
        def assertSuppress():
            rxK2 = self.getRxK2(self.sourceLineCliId())
            self.assertEqual(self.aisRdiCode(rxK2), 0x0)

        # Let disable consequence action, so that RDI will not be inserted when AIS happen
        self.enableAutoRdi(self.sourceLineCliId(), enabled=False)
        self.enableAutoRdi(self.sinkLineCliId(), enabled=False)

        # Have valid K2 first
        k2 = self.anyK2(excludeAisValue=True)
        self.setTxK2(self.sourceLineCliId(), k2)
        self.frameDelay()
        self.assertRxK2(self.sourceLineCliId(), k2)

        # AIS must not be forwarded when suppress is enabled
        self.suppress(suppressed=True, channelCliIds=self.sinkLineCliId())
        self.msleep(10)
        aisK2 = self.setAisCode(self.anyK2())
        self.setTxK2(self.sourceLineCliId(), aisK2)
        self.msleep(500)
        assertSuppress()

        # RDI must not be forwarded when suppress is enabled
        rdiK2 = self.setRdiCode(self.anyK2())
        self.setTxK2(self.sourceLineCliId(), rdiK2)
        self.msleep(500)
        assertSuppress()

    def testKByteSource(self):
        self.frameDelay()
        rxK1 = self.getRxK1(self.sourceLineCliId())
        rxK2 = self.getRxK2(self.sourceLineCliId())

        # When this is from SGMII, configuring any KBYTE from CPU will not have any affect
        self.setKbyteSource(self.sinkLineCliId(), "sgmii")
        txK1 = self.anyK1()
        txK2 = self.anyK2(excludeAisValue=True)
        self.setTxK1(self.sinkLineCliId(), txK1)
        self.setTxK2(self.sinkLineCliId(), txK2)
        self.frameDelay()
        self.assertRxK1(self.sourceLineCliId(), rxK1)
        self.assertRxK2(self.sourceLineCliId(), rxK2)

        # But this will have affect when changing the source to CPU
        self.setKbyteSource(self.sinkLineCliId(), "cpu")
        self.frameDelay()
        self.assertRxK1(self.sourceLineCliId(), txK1)
        self.assertRxK2(self.sourceLineCliId(), txK2)

    def testInterval(self):
        self.enableValidation(enabled=False, channelCliIds=self.sinkLineCliId())

        # There must be packets when having interval > 0
        self.setInterval(500)
        self.runCli("show eth port counters %s r2c" % self.ethPortCliId())
        self.msleep(100)
        self.assertEthGoodPackets()

        # If no interval, then there will be no packets
        self.setInterval(0)
        self.msleep(100)
        self.clearEthCounters()
        self.assertNoEthPackets()

    def testWatchdog(self):
        def assertWatchDogStatus(result, raised, enabledChecking):
            statusString = result.cellContent(0, "watchdog-expire")
            if enabledChecking:
                self.assertEqual(statusString, "set" if raised else "clear")

        self.makeValidState()
        self.msleep(100)  # Just a moment
        self.clearInterrupt()
        self.assertNoAlarm()
        self.assertNoInterrupt()

        watchDogTimer = min(self.defaultInterval() * 2, 16000)
        self.setWatchdog(watchDogTimer)
        self.setInterval(0) # To stop transmitting to SGMII
        self.msleep(100) # Just a moment
        self.clearEthCounters()
        self.assertNoEthPackets()

        # For sure, after watchdog time, alarm and interrupt must set
        self.usleep(watchDogTimer)
        self.assertNoEthPackets()
        assertWatchDogStatus(self.getInteruptCliResult(read2Clear=True), raised=True, enabledChecking=self.shouldCheckInterrupt())
        assertWatchDogStatus(self.getAlarmCliResult(), raised=True, enabledChecking=self.shouldCheckAlarm())

        # Still need to make sure that interrupt will not raise forever
        cliResult = self.getInteruptCliResult(read2Clear=True)
        assertWatchDogStatus(cliResult, raised=False, enabledChecking=self.shouldCheckInterrupt())

        # Now get packets go through
        self.setInterval(self.defaultInterval())
        self.msleep(100) # Just a moment
        self.assertEthGoodPackets()

        # There must be interrupt change
        cliResult = self.getInteruptCliResult(read2Clear=True)
        assertWatchDogStatus(cliResult, raised=True, enabledChecking=self.shouldCheckInterrupt())

        # Alarm must be clear
        cliResult = self.getAlarmCliResult()
        assertWatchDogStatus(cliResult, raised=False, enabledChecking=self.shouldCheckAlarm())

    def makeValidState(self):
        k1 = self.anyK1()
        k2 = self.anyK2(excludeAisValue=True)
        self.setTxK1(self.sourceLineCliId(), k1)
        self.setTxK2(self.sourceLineCliId(), k2)
        self.frameDelay()
        self.assertRxK1(self.sourceLineCliId(), k1)
        self.assertRxK2(self.sourceLineCliId(), k2)
        self.clearInterrupt()

        return k1, k2

    def shouldCheckAlarm(self):
        if self.simulated():
            return False
        return OptionParser.parser().testAlarmEnabled

    def shouldCheckInterrupt(self):
        if self.simulated():
            return False
        return OptionParser.parser().testInterruptEnabled

    def TestLookupMismatch(self, alarmName, attributeSetFunction, attributeGetFunction, valueRandomFunction):
        def assertMismatchStatus(cliResult, raised, enableChecking):
            statusString = cliResult.cellContent(0, alarmName)
            if enableChecking:
                self.assertEqual(statusString, "set" if raised else "clear")

        k1, k2 = self.makeValidState()
        assertMismatchStatus(self.getInteruptCliResult(), raised=False, enableChecking=self.shouldCheckInterrupt())

        originalValue = attributeGetFunction()
        anyValue = valueRandomFunction()
        attributeSetFunction(anyValue)
        self.frameDelay(numFrames=10)  # just a bonus

        # Now try sending any value, this will not be transparent
        self.setTxK1(self.sourceLineCliId(), self.anyK1())
        self.setTxK2(self.sourceLineCliId(), self.anyK2(excludeAisValue=True))
        self.frameDelay(numFrames=10)  # just a bonus
        self.assertRxK1(self.sourceLineCliId(), k1)
        self.assertRxK2(self.sourceLineCliId(), k2)
        assertMismatchStatus(self.getInteruptCliResult(), raised=True, enableChecking=self.shouldCheckInterrupt())
        assertMismatchStatus(self.getAlarmCliResult(), raised=True, enableChecking=self.shouldCheckAlarm())
        self.clearInterrupt()
        attributeSetFunction(originalValue)
        self.frameDelay(numFrames=10)  # just a bonus
        assertMismatchStatus(self.getInteruptCliResult(), raised=True, enableChecking=self.shouldCheckInterrupt())
        assertMismatchStatus(self.getAlarmCliResult(), raised=False, enableChecking=self.shouldCheckAlarm())

    def failtestLabel(self):
        def setTransmitLabel(value):
            self.setTransmitLabel(self.sinkLineCliId(), value)
        def getTransmitLabel():
            return self.getTransmitLabel(self.sinkLineCliId())
        def setExpectLabel(value):
            self.setExpectLabel(self.sinkLineCliId(), value)
        def getExpectLabel():
            return self.getExpectLabel(self.sinkLineCliId())

        self.TestLookupMismatch(alarmName="channel-mismatch",
                                attributeSetFunction=setTransmitLabel,
                                attributeGetFunction=getTransmitLabel,
                                valueRandomFunction=self.anyLabel)
        self.TestLookupMismatch(alarmName="channel-mismatch",
                                attributeSetFunction=setExpectLabel,
                                attributeGetFunction=getExpectLabel,
                                valueRandomFunction=self.anyLabel)

    def testOverrideKByte(self):
        self.makeValidState()

        # Enable overridden
        overrideK1 = self.anyK1()
        overrideK2 = self.anyK2(excludeAisValue=True)
        self.overrideKByteWithName(self.sinkLineCliId(), "K1", overrideK1)
        self.overrideKByteWithName(self.sinkLineCliId(), "K2", overrideK2)
        self.enableKByteOverride(self.sinkLineCliId(), enabled=True)

        # RX KBytes from Framer will not be used to transmit
        k1 = self.anyK1()
        k2 = self.anyK2(excludeAisValue=True)
        self.setTxK1(self.sourceLineCliId(), k1)
        self.setTxK2(self.sourceLineCliId(), k2)
        self.frameDelay()
        self.assertRxK1(self.sourceLineCliId(), overrideK1)
        self.assertRxK2(self.sourceLineCliId(), overrideK2)
        self.waitForSgmiiKByteUpdate()
        self.assertSgmiiRxK1(self.sinkLineCliId(), overrideK1)
        self.assertSgmiiRxK2(self.sinkLineCliId(), overrideK2)

        # Disable overriding, KBytes will be transparent
        self.enableKByteOverride(self.sinkLineCliId(), enabled=False)
        self.frameDelay()
        self.assertRxK1(self.sourceLineCliId(), k1)
        self.assertRxK2(self.sourceLineCliId(), k2)
        self.waitForSgmiiKByteUpdate()
        self.assertSgmiiRxK1(self.sinkLineCliId(), k1)
        self.assertSgmiiRxK2(self.sinkLineCliId(), k2)
        
    @staticmethod
    def anyMac():
        numbers = list()
        for _ in range(6):
            numbers.append(random.choice(range(256)))
        return ".".join(["%02x" % value for value in numbers])

    def testRemote_Central_Mac(self):
        self.TestLookupMismatch("da-mismatch", attributeSetFunction=self.setRemoteMac, attributeGetFunction=self.getRemoteMac, valueRandomFunction=self.anyMac)
        self.TestLookupMismatch("da-mismatch", attributeSetFunction=self.setCentralMac, attributeGetFunction=self.getCentralMac, valueRandomFunction=self.anyMac)

    def testEthtypeMismatch(self):
        self.txValue = None
        self.expectedValue = None

        def setTxValue(value):
            self.setTxEthType(value)
            self.txValue = value

        def getTxValue():
            return self.txValue

        def setExpectedValue(value):
            self.setExpectedEthType(value)
            self.expectedValue = value

        def getExpectedValue():
            return self.expectedValue

        # Have init state
        setTxValue(self.defaultEthType())
        setExpectedValue(self.defaultEthType())

        self.TestLookupMismatch("ethtype-mismatch", attributeSetFunction=setTxValue, attributeGetFunction=getTxValue, valueRandomFunction=self.anyEthType)
        self.TestLookupMismatch("ethtype-mismatch", attributeSetFunction=setExpectedValue, attributeGetFunction=getExpectedValue, valueRandomFunction=self.anyEthType)

    def testVerMismatch(self):
        self.txValue = None
        self.expectedValue = None

        def setTxValue(value):
            self.setTxVersion(value)
            self.txValue = value

        def getTxValue():
            return self.txValue

        def setExpectedValue(value):
            self.setExpectedVersion(value)
            self.expectedValue = value

        def getExpectedValue():
            return self.expectedValue

        def anyValue():
            return random.choice(range(255))

        # Have init state
        setTxValue(self.defaultVersion())
        setExpectedValue(self.defaultVersion())

        self.TestLookupMismatch("ver-mismatch", attributeSetFunction=setTxValue, attributeGetFunction=getTxValue,
                                valueRandomFunction=anyValue)
        self.TestLookupMismatch("ver-mismatch", attributeSetFunction=setExpectedValue,
                                attributeGetFunction=getExpectedValue, valueRandomFunction=anyValue)

    def testTypeMismatch(self):
        self.txValue = None
        self.expectedValue = None

        def setTxValue(value):
            self.setTxType(value)
            self.txValue = value

        def getTxValue():
            return self.txValue

        def setExpectedValue(value):
            self.setExpectedType(value)
            self.expectedValue = value

        def getExpectedValue():
            return self.expectedValue

        def anyValue():
            return random.choice(range(255))

        # Have init state
        setTxValue(self.defaultType())
        setExpectedValue(self.defaultType())

        self.TestLookupMismatch("type-mismatch", attributeSetFunction=setTxValue, attributeGetFunction=getTxValue,
                                valueRandomFunction=anyValue)
        self.TestLookupMismatch("type-mismatch", attributeSetFunction=setExpectedValue,
                                attributeGetFunction=getExpectedValue, valueRandomFunction=anyValue)

class OptionParser(AtTestCase.OptionParser):
    _parser = None

    def __init__(self):
        super(OptionParser, self).__init__()
        self.testAlarmEnabled = True
        self.testInterruptEnabled = True
        self.workAroundEnabled = False

    @classmethod
    def options(cls):
        opts = AtTestCase.OptionParser.options()
        opts.extend(["no-test-alarm", "no-test-interrupt", "need-work-around"])
        return opts

    @staticmethod
    def usageDescription():
        return AtTestCase.OptionParser.usageDescription() + " [--no-test-alarm] [--no-test-interrupt] [--need-work-around]"

    def parseOption(self, opt, unused_arg):
        super(OptionParser, self).parseOption(opt, unused_arg)

        if opt == "--no-test-alarm":
            self.testAlarmEnabled = False

        if opt == "--no-test-interrupt":
            self.testInterruptEnabled = False

        if opt == "--need-work-around":
            self.workAroundEnabled = True

    @classmethod
    def parser(cls):
        """
        :rtype: OptionParser
        """
        if cls._parser is None:
            cls._parser = OptionParser()
        return cls._parser

class DccAbstractGenerator(AtCliRunnable):
    def generate(self, layer = None):
        raise Exception("Not implemented yet")

    def init(self):
        pass

    def restart(self):
        pass

    def enableChannel(self, hdlcCliId, enabled=True):
        pass

    def enable(self):
        pass

    def disable(self):
        pass

    def cleanup(self):
        pass

    def canGenerateContinuosly(self):
        return False

class At4848DccGenerator(DccAbstractGenerator):
    _at4848App = None

    def __init__(self, boardIp="172.33.44.71", binaryPath="/home/wdb/users/sw/chitt/run/ep4848v5/customers/keymile/at4848_sdk_04.06/at4848"):
        super(At4848DccGenerator, self).__init__()
        self.at4848App = At4848App.app(boardIp, binaryPath)
        if AtColor.colorIsEnabled():
            self.at4848App.logPrefix = AtColor.GREEN + "AT4848" + AtColor.CLEAR
        else:
            self.at4848App.logPrefix = "AT4848"

        self.at4848App.verbosed = True
        self.defaultLayer = self.layerToAt4848("section")
        self.lineCliId = "ad_line#1"

    def simulate(self, simulated):
        self.at4848App.simulated = simulated

    def runCli(self, cli):
        """
        :rtype: str
        """
        result = self.at4848App.runCli(cli)
        success = "error" not in result.lower()

        if not success:
            print result.lower()

        assert success
        return result

    @staticmethod
    def rate():
        return "oc12_stm4"

    def flush(self):
        self.runCli("dlk diagevtfifoflush all")
        self.runCli("dlk diagfreealldatblkset")

    def init(self):
        self.at4848App.start()
        lineId = self.lineCliId
        self.runCli("sonet init dis dis en")
        self.runCli("dlk hlinit")
        self.runCli("at48 dlkdccfixinit")
        self.runCli("dlk cfgmastportset en dis dis dis dis dis dis dis dis dis")

        self.runCli("sonet cfglineconfset %s %s sdh" % (lineId, self.rate()))
        self.runCli("sonet cfglineinstrmsgset %s 16byte at48-%s" % (lineId, lineId))
        self.runCli("sonet cfglinemontrmsgset %s en 16byte mro-port-1" % lineId)
            
        self.runCli("dlk hlcfgchntxprovision dccsect - %s -" % lineId)
        self.runCli("dlk hlcfgchnrxprovision dccsect - %s -" % lineId)
        self.runCli("dlk hlcfgchntxprovision dccline - %s -" % lineId)
        self.runCli("dlk hlcfgchnrxprovision dccline - %s -" % lineId)

        self.flush()
        self.disable()

    def forceAis(self, forced = True):
        cli = "sonet diaglinetxalmfrcset %s aisl %s" % (self.lineCliId, "en" if forced else "dis")
        self.runCli(cli)

    def enable(self):
        self.forceAis(forced=False)

    def disable(self):
        self.forceAis(forced=True)

    def restart(self):
        self.at4848App.exit()
        self.init()

    @staticmethod
    def layerToAt4848(layerName):
        if layerName == "section":
            return "dccsect"
        if layerName == "line":
            return "dccline"
        return None

    def layer(self):
        return self.layerToAt4848(self.defaultLayer)

    @staticmethod
    def retryTimes():
        return 50

    def allEventsProcessed(self, retryTimes = 256):
        for i in range(retryTimes):
            try:
                result = self.runCli("dlk hldiagmsgreceive dis")
                if "direction" not in result.lower():
                    return True

                if i % 10 == 0:
                    self.flush()
            except:
                pass

        return False

    def generate(self, layer = None):
        if layer is None:
            layer = self.defaultLayer

        def _send():
            self.runCli("dlk hldiaglapdmsgsend %s - %s 0x1f 0x2 at4848-%s-%s dis" % (layer, self.lineCliId, self.lineCliId, layer))
            if not self.allEventsProcessed():
                self.flush()

        for _ in range(self.retryTimes()):
            try:
                _send()
                return
            except:
                pass

    def cleanup(self):
        self.at4848App.exit()

class DccGenerator(DccAbstractGenerator):
    GEN_MODE_ONESHOT = "oneshot"
    GEN_MODE_CONTINUOUS = "continuous"

    def canGenerateContinuosly(self):
        return True

    @classmethod
    def runCli(cls, cli):
        result = AtCliRunnable.runCli(cli)
        assert result.success()
        return result

    def _enable(self, enabled):
        enableString = "enable" if enabled else "disable"
        self.runCli("debug dcc monitor %s" % enableString)
        self.runCli("debug dcc generator %s" % enableString)

    def enable(self):
        self._enable(True)

    def disable(self):
        self._enable(False)

    def setInterval(self, us):
        assert us % 125 == 0
        counter = us * 155.0
        self.runCli("debug dcc monitor interval 0x%x" % counter)
        self.runCli("debug dcc generator interval 0x%x" % counter)

    def setLength(self, length):
        self.runCli("debug dcc generator length %d" % length)

    def setMinLength(self, minLength):
        self.runCli("debug dcc generator length min %d" % minLength)

    def setMaxLength(self, maxLength):
        self.runCli("debug dcc generator length max %d" % maxLength)

    def setGenMode(self, mode):
        assert mode in [self.GEN_MODE_CONTINUOUS, self.GEN_MODE_ONESHOT]
        self.runCli("debug dcc generator mode %s" % mode)

    def _force(self, errorTypeString, forced):
        command = "force" if forced else "unforce"
        self.runCli("debug dcc generator error %s %s" % (command, errorTypeString))

    def forceFcs(self, forced = True):
        self._force("fcs", forced)

    def forceLengthError(self, forced = True):
        self._force("length", forced)

    def forcePayloadError(self, forced = True):
        self._force("payload", forced)

    def init(self):
        self.runCli("debug dcc init")
        self.disable()
        self.setInterval(us=125)
        self.setGenMode(self.GEN_MODE_CONTINUOUS)

    def enableChannel(self, hdlcCliId, enabled=True):
        command = "enable" if enabled else "disable"
        self.runCli("debug dcc generator channel %s %s" % (command, hdlcCliId))

    def setChannelVlanId(self, hdlcCliId, vlanId):
        self.runCli("debug dcc generator channel header vlanid %s %s" % (hdlcCliId, vlanId))

    def setChannelTxLabel(self, hdlcCliId, label):
        self.runCli("debug dcc generator channel header maclsb %s %s" % (hdlcCliId, label))

    def generate(self, layer = None):
        self.enable()

class DccDatapathAbstractTest(DccAbstractTest):
    @classmethod
    def shouldRunOnBoard(cls):
        return True

    @classmethod
    def randomLabel(cls):
        return random.randint(0, cls.maxLabelValue())

    @classmethod
    def putLabelToMac(cls, macBytes, label):
        labelMask = AtRegister.cBit4_0 if cls.isMroProduct() else AtRegister.cBit5_0
        reg = AtRegister.AtRegister(macBytes[-1])
        reg.setField(labelMask, 0, label)
        macBytes[-1] = reg.value

    @classmethod
    def createGenerator(cls):
        """
        :rtype: DccAbstractGenerator
        """
        return None

    @classmethod
    def generator(cls):
        """
        :rtype: DccGenerator
        """
        if cls._generator is None:
            cls._generator = cls.createGenerator()
        return cls._generator

    @classmethod
    def setupPacketAnalyzer(cls):
        cls.assertClassCliSuccess("pktanalyzer create eport.18")

    @classmethod
    def remoteLineTti(cls):
        return "Invalid"

    @classmethod
    def connectedLineCliId(cls):
        return 1

    @classmethod
    def lineIsLoopback(cls, lineCliId):
        return False if lineCliId == cls.connectedLineCliId() else True

    @classmethod
    def expectedTtiForLine(cls, lineCliId):
        if cls.lineIsLoopback(lineCliId):
            return "mro-port-%d" % lineCliId

        return cls.remoteLineTti()

    @classmethod
    def setupLines(cls):
        lineIdString = cls.lineCliIdString()
        cls.runCli("serdes mode %s %s" % (lineIdString, cls.lineRate()))
        cls.runCli("sdh line rate %s %s" % (lineIdString, cls.lineRate()))  # So that full 16 lines can be used

        for lineId in range(cls.numLines()):
            cliId = lineId + 1
            message = cls.expectedTtiForLine(cliId)
            cls.runCli("sdh line tti transmit %d 16bytes %s null_padding" % (cliId, message))
            cls.runCli("sdh line tti expect %d 16bytes %s null_padding" % (cliId, message))

    @classmethod
    def deleteAllHdlcs(cls):
        controller = cls.dccCliController()
        controller.deleteHdlc(cls.hdlcIdsString())

    @classmethod
    def squel(cls, enabled=True):
        cli = "sdh line force alarm %s ais rx %s" % (cls.lineCliIdString(), "en" if enabled else "dis")
        cls.assertClassCliSuccess(cli)

    @classmethod
    def needWorkAround(cls):
        return OptionParser.parser().workAroundEnabled

    @classmethod
    def generatorIsLocal(cls):
        return True

    @classmethod
    def enableGeneratingOnChannels(cls):
        pass

    @classmethod
    def setupGenerator(cls, enableByDefault=True):
        generator = cls.generator()
        generator.init()
        if enableByDefault:
            generator.enable()
        else:
            generator.disable()

        cls.enableGeneratingOnChannels()

    @classmethod
    def setupHdlcs(cls):
        controller = cls.dccCliController()
        for lineId in range(cls.numLines()):
            cliId = lineId + 1
            for layer in cls.layers():
                controller.createHdlc(cliId, layer)
                hdlcCliId = "%d.%s" % (cliId, layer)
                controller.init(hdlcCliId)
                controller.enableMacCheck(hdlcCliId, enabled=False)
                controller.enableVlanCheck(hdlcCliId, enabled=False)

    @classmethod
    def enableHdlcs(cls):
        pass

    @classmethod
    def setupTraffic(cls):
        if cls.needWorkAround():
            cls.squel()

        if not cls.generatorIsLocal():
            enabledDefault = False if cls.needWorkAround() else True
            cls.setupGenerator(enableByDefault=enabledDefault)

        cls.initDevice()
        cls.setupLines()
        cls.deleteAllHdlcs()
        cls.setupHdlcs()
        cls.loopbackEthernet()

        cls.enableHdlcs()

        if cls.generatorIsLocal():
            cls.setupGenerator()

        if cls.needWorkAround():
            cls.generator().enable()
            cls.squel(enabled=False)

        cls.setupPacketAnalyzer()

    @classmethod
    def setUpClass(cls):
        cls.setupTraffic()

class DccDatapathBasicTest(DccDatapathAbstractTest):
    _generator = None
    globalMacBytes = None
    vlanString = None
    dmacBytes = None
    ethType = None
    _label = None
    _selectedLineCliId = 1
    _selectedLayer = "section"

    @classmethod
    def numHdlcs(cls):
        return 1

    @classmethod
    def connectedLineCliId(cls):
        return cls._selectedLineCliId

    @classmethod
    def selectedLayer(cls):
        return cls._selectedLayer

    @classmethod
    def setSelectedLayer(cls, layerName):
        cls._selectedLayer = layerName

    @classmethod
    def hdlcIdString(cls):
        return "%d.%s" % (cls.connectedLineCliId(), cls.selectedLayer())

    @classmethod
    def defaultLabel(cls):
        return cls.connectedLineCliId()

    @classmethod
    def label(cls):
        if cls._label is None:
            cls._label = cls.randomLabel() if cls.randomIsEnabled() else cls.defaultLabel()
        return cls._label

    @classmethod
    def setupLabel(cls):
        cls.dccCliController().setExpectedLabel(cls.hdlcIdString(), cls.label())

    @classmethod
    def enableHdlcs(cls):
        cls.dccCliController().enableHdlc(hdlcCliId=cls.hdlcIdString(), enabled=True)

    @classmethod
    def setupHeader(cls):
        controler = cls.dccCliController()

        # Global MAC
        cls.globalMacBytes = cls.anyMacBytes()
        globalMac = cls.macBytes2String(cls.globalMacBytes)
        controler.setGlobalMac(ethPortCliId=cls.ethPortCliId(), mac=globalMac)

        # DMAC and VLAN
        cls.vlanString = cls.anyVlanString()
        dmacBytes = list(cls.globalMacBytes)
        cls.putLabelToMac(dmacBytes, cls.label())
        cls.dmacBytes = dmacBytes
        controler.setTxHeader(cls.hdlcIdString(), cls.macBytes2String(cls.dmacBytes), cls.vlanString)

        [_, _, vlanIdString] = cls.vlanString.split(".")
        controler.setExpectedVlan(cls.ethPortCliId(), int(vlanIdString, 10))

    @classmethod
    def setupGenerator(cls, enableByDefault = True):
        generator = cls.generator()
        generator.init()
        if enableByDefault:
            generator.enable()
        else:
            generator.disable()
        generator.enableChannel(cls.hdlcIdString(), enabled=True)

    @classmethod
    def clearInterruptStatus(cls):
        cls.runCli("show ciena pw dcc interrupt %s r2c" % cls.hdlcIdsString())

    @classmethod
    def assertHdlcPwNoInterrupt(cls):
        result = cls.runCli("show ciena pw dcc interrupt %s r2c" % cls.hdlcIdsString())
        for rowId in range(result.numRows()):
            cellContent = result.cellContent(rowId, "discard")
            assert cellContent == "clear"

    def clearStatus(self):
        self.readHdlcCounter(read2Clear=True)
        self.readHdlcPwCounter(read2Clear=True)
        self.clearEthCounters()
        self.clearInterruptStatus()

    def setUp(self):
        self.setupLabel()
        self.setupHeader()

        # Wait for stable time
        self.sleep(1)

        # Then clear status
        self.clearStatus()

        # Make sure that nothing is running in the background
        self.sleep(1)
        self.assertHdlcPwNumPackets(numPackets=0)
        self.assertHdlcNumPackets(numPackets=0)
        self.assertNoEthPackets()
        self.assertHdlcPwNoInterrupt()

    @classmethod
    def setTxLabel(cls, label):
        dmac = list(cls.globalMacBytes)
        cls.putLabelToMac(dmac, label)
        cls.dccCliController().setTxHeader(cls.hdlcIdString(), cls.macBytes2String(dmac), cls.vlanString)

    @classmethod
    def readHdlcCounter(cls, read2Clear = False):
        return HdlcCounter.counter(cls.hdlcIdString(), read2Clear)

    @classmethod
    def readHdlcPwCounter(cls, read2Clear = False):
        return HdlcPwCounter.counter(cls.hdlcIdString(), read2Clear=read2Clear)

    @classmethod
    def tearDownClass(cls):
        cls.generator().cleanup()

    def generate(self):
        self.generator().generate()

    def assertHdlcRxNumPackets(self, numPackets, counter = None, read2Clear=False):
        if counter is None:
            counter = self.readHdlcCounter(read2Clear)

        if self.simulated():
            return

        self.assertEqual(counter.rxPacket, numPackets)

        if numPackets > 0:
            self.assertGreater(counter.rxByte, read2Clear)
        else:
            self.assertEqual(counter.rxByte, numPackets)

        self.assertEqual(counter.rxDiscardPacket, 0)
        self.assertEqual(counter.rxFcsErr, 0)

    def assertHdlcTxNumPackets(self, numPackets, counter = None, read2Clear=False):
        if counter is None:
            counter = self.readHdlcCounter(read2Clear)

        if self.simulated():
            return

        self.assertEqual(counter.txPacket, numPackets)

        if numPackets > 0:
            self.assertGreater(counter.txByte, read2Clear)
        else:
            self.assertEqual(counter.txByte, numPackets)

        self.assertEqual(counter.txDiscardPacket, 0)

    def assertHdlcNumPackets(self, numPackets, read2Clear = False):
        counter = self.readHdlcCounter(read2Clear)

        self.assertHdlcRxNumPackets(numPackets=numPackets, counter=counter)
        self.assertHdlcTxNumPackets(numPackets=numPackets, counter=counter)

    def assertHdlcPwRxNumPackets(self, numPackets, counter = None, read2Clear = False):
        if counter is None:
            counter = self.readHdlcPwCounter(read2Clear)

        if self.simulated():
            return

        self.assertEqual(counter.rxPackets, numPackets)

        if numPackets == 0:
            self.assertEqual(counter.rxPayloadBytes, 0)
        else:
            self.assertGreater(counter.rxPayloadBytes, numPackets)

        self.assertEqual(counter.rxDiscardedPackets, 0)

    def assertHdlcPwTxNumPackets(self, numPackets, counter = None, read2Clear = False):
        if counter is None:
            counter = self.readHdlcPwCounter(read2Clear)

        if self.simulated():
            return

        self.assertEqual(counter.txPackets, numPackets)

        if numPackets == 0:
            self.assertEqual(counter.txPayloadBytes, 0)
        else:
            self.assertGreater(counter.txPayloadBytes, numPackets)

    def assertHdlcPwNumPackets(self, numPackets, read2Clear = False):
        counter = self.readHdlcPwCounter(read2Clear)
        self.assertHdlcPwRxNumPackets(numPackets=numPackets, counter=counter)
        self.assertHdlcPwTxNumPackets(numPackets=numPackets, counter=counter)

    def assertNumPackets(self, numPackets):
        self.assertHdlcNumPackets(numPackets)
        self.assertHdlcPwNumPackets(numPackets)
        self.assertEthNumPackets(numPackets)

    def assignedChannelWithLabel(self, label):
        lineId = label / 2
        layerId = label % 2
        return "%d.%s" % (lineId + 1, self.layers()[layerId])

    def assertHdlcPwInterruptStatus(self, hdlcCliId, bitSet, read2Clear=False):
        result = self.runCli("show ciena pw dcc interrupt %s %s" % (hdlcCliId, "r2c" if read2Clear else "ro"))
        expected = "set" if bitSet else "clear"
        cellContent = result.cellContent(rowId=0, columnIdOrName="discard")
        if not self.simulated():
            self.assertEqual(cellContent, expected)

    def assertHdlcPwCurrentStatus(self, hdlcCliId, bitSet):
        result = self.runCli("show ciena pw dcc alarm %s" % hdlcCliId)
        expected = "set" if bitSet else "clear"
        cellContent = result.cellContent(rowId=0, columnIdOrName="discard")
        if not self.simulated():
            self.assertEqual(cellContent, expected)

    def globlalMacString(self):
        return ".".join(["%x" % macByte for macByte in self.globalMacBytes])

    def _assertStatus(self, alarms, alarmName, bitSet):
        if bitSet:
            if len(alarms) == 0:
                self.fail("No alarm has been set")

            self.assertEqual(len(alarms), 1, "There are more than one expected alarm %s: %s" % (alarmName, alarms))
            self.assertTrue(alarmName in alarms)
        else:
            self.assertEqual(len(alarms), 0)

    def assertEthCurrentStatus(self, alarmName, bitSet):
        alarms = self.dccCliController().getEthAlarm(self.ethPortCliId())
        if not self.simulated():
            self._assertStatus(alarms, alarmName, bitSet)

    def assertEthInterruptStatus(self, alarmName, bitSet, read2Clear=True):
        alarms = self.dccCliController().getEthInterrupt(self.ethPortCliId(), read2Clear)
        if not self.simulated():
            self._assertStatus(alarms, alarmName, bitSet)

    def TestHeaderMismatch(self, alarmName, trigger):
        trigger(True)
        self.assertNumPackets(numPackets=0)

        # Counters must be right
        self.generate()
        self.assertHdlcRxNumPackets(numPackets=1)
        self.assertHdlcPwTxNumPackets(numPackets=1)
        self.assertHdlcPwRxNumPackets(numPackets=0)
        self.assertHdlcTxNumPackets(numPackets=0)

        # Alarm/interrupt must be right
        if OptionParser.parser().testAlarmEnabled:
            self.assertEthCurrentStatus(alarmName, bitSet=True)
        if OptionParser.parser().testInterruptEnabled:
            self.assertEthInterruptStatus(alarmName, bitSet=True, read2Clear=True)
            self.assertEthInterruptStatus(alarmName, bitSet=False, read2Clear=True)

        # Give a moment and alarm must be there
        if OptionParser.parser().testAlarmEnabled:
            self.sleep(1)
            self.assertEthCurrentStatus(alarmName, bitSet=True)

        self.clearStatus()

        # Get it back to normal
        trigger(False)
        self.generate()
        self.assertNumPackets(numPackets=1)

        # Alarm/interrupt must be right
        if OptionParser.parser().testAlarmEnabled:
            self.assertEthCurrentStatus(alarmName, bitSet=False)
        if OptionParser.parser().testInterruptEnabled:
            self.assertEthInterruptStatus(alarmName, bitSet=True, read2Clear=True)
            self.assertEthInterruptStatus(alarmName, bitSet=False, read2Clear=True)

    def generateAnyMac(self):
        while True:
            mac = self.anyMac()
            if mac != self.globlalMacString():
                return mac

    def generateAnyVlan(self):
        while True:
            vlan = self.anyVlanString()
            if vlan != self.vlanString:
                return vlan

    def testTrafficMustBasicallyFlow(self):
        self.generate()
        self.assertNumPackets(numPackets=1)

    def testLabelMismatch(self):
        def generateLabel():
            label = self.anyLabel()
            while True:
                if label != self.label():
                    return label

        anyLabel = generateLabel()
        self.setTxLabel(anyLabel)
        self.assertNumPackets(numPackets=0)

        # Counters must be right
        self.generate()
        self.assertHdlcRxNumPackets(numPackets=1)
        self.assertHdlcPwTxNumPackets(numPackets=1)
        self.assertHdlcPwRxNumPackets(numPackets=0)
        self.assertHdlcTxNumPackets(numPackets=0)

        # This packet must go to other channel but they are in disabled state, so that channel must raise "disable" alarm.
        channelToCheck = self.assignedChannelWithLabel(anyLabel)
        if OptionParser.parser().testInterruptEnabled:
            self.assertHdlcPwInterruptStatus(channelToCheck, bitSet=True, read2Clear=True)
            self.assertHdlcPwInterruptStatus(channelToCheck, bitSet=False, read2Clear=True)

        if OptionParser.parser().testAlarmEnabled:
            self.assertHdlcPwCurrentStatus(channelToCheck, bitSet=True)

    def testExpectedGlobalMac(self):
        controller = self.dccCliController()
        controller.enableMacCheck(self.hdlcIdString(), enabled=True)

        def trigger(triggered):
            if triggered:
                controller.setGlobalMac(self.ethPortCliId(), self.generateAnyMac())
            else:
                controller.setGlobalMac(self.ethPortCliId(), self.globlalMacString())

        self.TestHeaderMismatch(alarmName="da-mismatch", trigger=trigger)

    def testDmac(self):
        controller = self.dccCliController()
        controller.enableMacCheck(self.hdlcIdString(), enabled=True)

        def trigger(triggered):
            if triggered:
                macBytes = self.anyMacBytes()
                self.putLabelToMac(macBytes, self.label())
                macString = self.macBytes2String(macBytes)
                controller.setTxHeader(self.hdlcIdString(), macString, self.vlanString)
            else:
                macString = self.macBytes2String(self.dmacBytes)
                controller.setTxHeader(self.hdlcIdString(), macString, self.vlanString)

        self.TestHeaderMismatch(alarmName="da-mismatch", trigger=trigger)

    def testMacCheckEnabling(self):
        controller = self.dccCliController()
        controller.setGlobalMac(self.ethPortCliId(), self.generateAnyMac())

        def trigger(triggered):
            controller.enableMacCheck(self.hdlcIdString(), enabled=triggered)

        self.TestHeaderMismatch(alarmName="da-mismatch", trigger=trigger)

    def testExpectedVlan(self):
        controller = self.dccCliController()
        controller.enableVlanCheck(self.hdlcIdString(), enabled=True)

        def trigger(triggered):
            macString = self.macBytes2String(self.dmacBytes)
            if triggered:
                controller.setTxHeader(self.hdlcIdString(), macString, self.generateAnyVlan())
            else:
                controller.setTxHeader(self.hdlcIdString(), macString, self.vlanString)

        self.TestHeaderMismatch(alarmName="cvlan-mismatch", trigger=trigger)

    def testVlanCheckEnabling(self):
        controller = self.dccCliController()
        controller.setTxHeader(self.hdlcIdString(), self.macBytes2String(self.dmacBytes), self.generateAnyVlan())

        def trigger(triggered):
            controller.enableVlanCheck(self.hdlcIdString(), enabled=triggered)

        self.TestHeaderMismatch(alarmName="cvlan-mismatch", trigger=trigger)

    @classmethod
    def runWithRunner(cls, runner):
        """
        :type runner: test.AtTestCase.AtTestCaseRunner
        """
        runner.run(cls)

class DccDatapathAbstractTestWithAt4848(DccDatapathBasicTest):
    @classmethod
    def createGenerator(cls):
        """
        :rtype: At4848DccGenerator
        """
        generator = At4848DccGenerator()
        generator.simulate(cls.simulated())
        return generator

    @classmethod
    def remoteLineTti(cls):
        generator = cls.generator()  # type: At4848DccGenerator
        return "at48-%s" % generator.lineCliId

    @classmethod
    def generatorIsLocal(cls):
        return False

class DccDatapathBasicTestWithAt4848(DccDatapathAbstractTestWithAt4848):
    @classmethod
    def setupHdlcs(cls):
        DccDatapathBasicTest.setupHdlcs()
        cls.dccCliController().setFcsOrdering(cls.hdlcIdString(), "msb") # To interwork with AT4848

    @classmethod
    def runWithRunner(cls, runner):
        """
        :type runner: test.AtTestCase.AtTestCaseRunner
        """
        generator = cls.generator()
        for layer in cls.layers():
            generator.selectedLayer = layer
            cls.setSelectedLayer(layer)

            runner.run(cls)

class DccDatapathDaisyChainTestWithAt4848(DccDatapathAbstractTestWithAt4848):
    @classmethod
    def connectedLineCliId(cls):
        return 1

class DccDatapathBasicTestWithInternalGenerator(DccDatapathBasicTest):
    @classmethod
    def globalMacBytes(cls):
        return [0xCA, 0xFE, 0xDA, 0xBA, 0x00, 0x00]

    @classmethod
    def createGenerator(cls):
        return DccGenerator()

    @classmethod
    def setupLines(cls):
        DccDatapathBasicTest.setupLines()
        cls.loopbackLine()

    @classmethod
    def enableGeneratingOnChannels(cls):
        cls.generator().enableChannel(cls.hdlcIdString(), enabled=True)

def TestMain():
    parser = OptionParser.parser()
    parser.parse()
    if parser.returnCode is not None:
        return parser.returnCode

    runner = AtTestCase.AtTestCaseRunner.runner()
    runner.run(DccHdlcTest)
    runner.run(DccPwTest)
    runner.run(EthPortTest)
    runner.run(KbytePwChannelManagementTest)
    runner.run(KbytePwTest)
    runner.run(KByteDatapathDefaultTest)
    runner.run(DccPwDefaultSettingTest)
    DccDatapathBasicTestWithAt4848.runWithRunner(runner)
    runner.run(DccDatapathBasicTestWithInternalGenerator)
    runner.summary()

if __name__ == '__main__':
    TestMain()

from python.arrive.AtAppManager.AtColor import AtColor
from python.arrive.atsdk.platform import AtHalListener
from test.AtTestCase import AtTestCaseRunner
from test.anna.AnnaTestCase import AnnaMroTestCase


class HalListener(AtHalListener):
    def __init__(self):
        self.invalidReadAddresses  = dict()
        self.invalidWriteAddresses = dict()
        self._allTerminatedRanges = None

    def start(self):
        AtHalListener.addListener(self)

    def stop(self):
        AtHalListener.removeListener(self)

    @staticmethod
    def rangeFromString(rangeString):
        """
        :type rangeString: str
        """
        rangeString = rangeString.replace("_", "")
        rangeString = rangeString.replace(" ", "")
        [start, stop] = [int(aString, 16) for aString in rangeString.split("-")]
        return start, stop

    def allTerminatedRanges(self):
        if self._allTerminatedRanges:
            return self._allTerminatedRanges

        rangeStrings = [
            "0x002_0000-0x002_FFFF",
            "0x003_0000-0x003_FFFF",
            "0x004_0000-0x004_FFFF",
            "0x007_0000-0x007_FFFF",
            "0x008_0000-0x008_FFFF",
            "0x00C_0000-0x00C_FFFF",
            "0x00D_0000-0x00D_FFFF",
            "0x00E_0000-0x00E_0FFF",
            "0x00E_1000-0x00E_1FFF",
            "0x035_0000-0x035_FFFF",
            "0x038_0000-0x03B_FFFF",
            "0x040_0000-0x04F_FFFF",
            "0x050_0000-0x05F_FFFF",
            "0x060_0000-0x06F_FFFF",
            "0x080_0000-0x083_FFFF",
            "0x084_0000-0x087_FFFF",
            "0x088_0000-0x08B_FFFF",
            "0x08C_0000-0x08F_FFFF",
            "0x090_0000-0x093_FFFF",
            "0x094_0000-0x097_FFFF",
            "0x098_0000-0x09B_FFFF",
            "0x09C_0000-0x09F_FFFF",
            "0x0C0_0000-0x0C3_FFFF",
            "0x0C4_0000-0x0C7_FFFF",
            "0x0C8_0000-0x0CB_FFFF",
            "0x0CC_0000-0x0CF_FFFF",
            "0x0D0_0000-0x0D3_FFFF",
            "0x0D4_0000-0x0D7_FFFF",
            "0x0D8_0000-0x0DB_FFFF",
            "0x0DC_0000-0x0DF_FFFF",
            "0x100_0000-0x10F_FFFF",
            "0x110_0000-0x11F_FFFF",
            "0x120_0000-0x12F_FFFF",
            "0x130_0000-0x13F_FFFF",
            "0x140_0000-0x14F_FFFF",
            "0x150_0000-0x15F_FFFF",
            "0x160_0000-0x16F_FFFF",
            "0x170_0000-0x17F_FFFF",
            "0xF20_000-0xF20_FFF",
            "0xF21_000-0xF21_FFF",
            "0xF22_000-0xF22_FFF",
            "0xF23_000-0xF23_FFF",
            "0xF24_000-0xF24_FFF",
            "0xF54_000-0xF54_FFF",
            "0xF44_000-0xF44_0FF",
            "0xF45_000-0xF45_FFF",
            "0xF50_000-0xF50_FFF",
            "0xF51_000-0xF51_FFF",
            "0xF52_000-0xF52_FFF",
            "0xF53_000-0xF53_FFF",
            "0xF6_0000-0xF6_1FFF",
            "0xF8_0000-0xF8_FFFF",
            "0xF9_0000-0xF9_FFFF",
            "0xFA_2000-0xFA_27FF",
            "0xFA_6000-0xFA_67FF"
        ]

        self._allTerminatedRanges = [self.rangeFromString(rangeString) for rangeString in rangeStrings]
        return self._allTerminatedRanges

    def isTerminatedAddress(self, address):
        for start, stop in self.allTerminatedRanges():
            if start <= address <= stop:
                return True

        return False

    def didRead(self, address, value):
        if not self.isTerminatedAddress(address):
            return

        key = address
        if key in self.invalidReadAddresses:
            self.invalidReadAddresses[address] = self.invalidReadAddresses[address] + 1
        else:
            self.invalidReadAddresses[address] = 1

    def didWrite(self, address, value):
        if not self.isTerminatedAddress(address):
            return

        key = address
        if key in self.invalidWriteAddresses:
            self.invalidWriteAddresses[address] = self.invalidWriteAddresses[address] + 1
        else:
            self.invalidWriteAddresses[address] = 1

class FaceplateInvalidAccess(AnnaMroTestCase):
    def setUp(self):
        self.assertCliSuccess("device init")
        self.listener = HalListener()
        self.listener.start()

    def tearDown(self):
        self.listener.stop()

    @staticmethod
    def displayInvalidAccess(invalidAccesses, indent):
        for address, accessCount in invalidAccesses.iteritems():
            AtColor.printColor(AtColor.RED, "%s* Address 0x%08x, count: %d" % (" " * indent, address, accessCount))

    def assertNoInvalidAccess(self):
        if len(self.listener.invalidWriteAddresses) == 0 and len(self.listener.invalidReadAddresses) == 0:
            return

        if len(self.listener.invalidWriteAddresses) > 0:
            AtColor.printColor(AtColor.RED, "* Invalid write accesses:")
            self.displayInvalidAccess(self.listener.invalidWriteAddresses, indent=4)

        if len(self.listener.invalidReadAddresses) > 0:
            AtColor.printColor(AtColor.RED, "* Invalid read accesses:")
            self.displayInvalidAccess(self.listener.invalidReadAddresses, indent=4)

        self.fail("There are %d invalid read and %d invalid write" % (len(self.listener.invalidReadAddresses), len(self.listener.invalidWriteAddresses)))

    def testNoTerminatedAccessesWhenMapVc3OnFaceplate(self):
        for lineCliId in self.stm16FaceplateLineCliIdList():
            self.assertCliSuccess("sdh line rate %d stm16" % lineCliId)
            self.assertCliSuccess("sdh map vc3.%d.1.1-%d.16.3 c3" % (lineCliId, lineCliId))
        self.assertNoInvalidAccess()

    def testNoTerminatedAccessesWhenMapVc4OnFaceplate(self):
        for lineCliId in self.stm16FaceplateLineCliIdList():
            self.assertCliSuccess("sdh line rate %d stm16" % lineCliId)
            self.assertCliSuccess("sdh map aug1.%d.1-%d.16 vc4" % (lineCliId, lineCliId))
            self.assertCliSuccess("sdh map vc4.%d.1-%d.16 c4" % (lineCliId, lineCliId))
        self.assertNoInvalidAccess()

    def testNoTerminatedAccessesWhenMapVc4_4cOnFaceplate(self):
        for lineCliId in self.stm16FaceplateLineCliIdList():
            self.assertCliSuccess("sdh line rate %d stm16" % lineCliId)
            self.assertCliSuccess("sdh map aug4.%d.1-%d.4 vc4_4c" % (lineCliId, lineCliId))
            self.assertCliSuccess("sdh map vc4_4c.%d.1-%d.16 c4_4c" % (lineCliId, lineCliId))
        self.assertNoInvalidAccess()

    def testNoTerminatedAccessesWhenMapVc4_16cOnFaceplate(self):
        for lineCliId in self.stm16FaceplateLineCliIdList():
            self.assertCliSuccess("sdh line rate %d stm16" % lineCliId)
            self.assertCliSuccess("sdh map aug16.%d.1-%d.4 vc4_16c" % (lineCliId, lineCliId))
            self.assertCliSuccess("sdh map vc4_16c.%d.1-%d.16 c4_16c" % (lineCliId, lineCliId))
        self.assertNoInvalidAccess()

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(FaceplateInvalidAccess)
    runner.summary()

if __name__ == '__main__':
    TestMain()

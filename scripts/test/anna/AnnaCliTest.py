#import pydevd
#pydevd.settrace("172.33.47.2")

from test.AtTestCase import AtTestCaseRunner
from AnnaTestCase import AnnaTestCase

class AnnaCliTest(AnnaTestCase):
    @classmethod
    def setUpClass(cls):
        cls.classRunCli("device init")
        
    def checkAllCellsOfColumn(self, result, columnHeader, expectValue):
        for row_i in range(0, result.numRows()):
            value = result.cellContent(row_i, columnHeader)
            self.assertEqual(value, expectValue, None)
        
    def testDeviceVersion (self):
        self.assertCliSuccess("wr f45000 0x20003456")

        self.assertCliSuccess("wr f45001 0x00000002")
        
        self.assertCliSuccess("wr f45002 0x7DD")
        self.assertCliSuccess("wr f45003 0x25")
        
        self.assertCliSuccess("wr f45004 0x12345678")
        
        self.assertCliSuccess("wr f45008 0x32303133")
        self.assertCliSuccess("wr f45009 0x2d31322d")
        self.assertCliSuccess("wr f4500a 0x30385430")
        self.assertCliSuccess("wr f4500b 0x353a3237")
        self.assertCliSuccess("wr f4500c 0x3a35392b")
        self.assertCliSuccess("wr f4500d 0x30303a30")
        self.assertCliSuccess("wr f4500e 0x30000000")
        
        self.assertCliSuccess("wr f45010 0x46504741")
        self.assertCliSuccess("wr f45011 0x00000000")
        
        self.assertCliSuccess("wr f45014 0x43454d00")
        
        self.assertCliSuccess("wr f45018 0x4d524f00")
        
        self.assertCliSuccess("wr f4501c 0x41727269")
        self.assertCliSuccess("wr f4501d 0x76650000")
        
        self.assertCliSuccess("show ciena device version")
        
    def testDeviceMro (self):
        if not self.isMroProduct():
            return
            
        self.assertCliSuccess("ciena serdes group faceplate 1")
        
        result = self.runCli("show ciena device mro")
        faceplateGroup = result.cellContent(0, "FaceplateGroup")
        self.assertEqual(faceplateGroup, "1", None)
        
        self.assertCliSuccess("ciena serdes group faceplate 2")
        
        result = self.runCli("show ciena device mro")
        faceplateGroup = result.cellContent(0, "FaceplateGroup")
        self.assertEqual(faceplateGroup, "2", None)
        
    def testDevicePdh (self):
        if not self.isPdhProduct():
            return
        
        self.assertCliSuccess("ciena pdh interface de1")
        
        result = self.runCli("show ciena device pdh")
        interfaceType = result.cellContent(0, "PdhInterface")
        self.assertEqual(interfaceType, "de1", None)
        
        self.assertCliSuccess("ciena pdh interface de3")
        
        result = self.runCli("show ciena device pdh")
        interfaceType = result.cellContent(0, "PdhInterface")
        self.assertEqual(interfaceType, "de3", None)
    
    def testEthPort (self):
        self.assertCliSuccess("ciena eth port bypass enable 2-17")
        
        result = self.runCli("show ciena eth port 2-17")
        self.checkAllCellsOfColumn(result, "Bypass", "en")
        
        self.assertCliSuccess("ciena eth port bypass disable 2-17")
        
        result = self.runCli("show ciena eth port 2-17")
        self.checkAllCellsOfColumn(result, "Bypass", "dis")
            
        self.assertCliSuccess("ciena eth subport vlan tpid transmit 1 0x8100")
        self.assertCliSuccess("ciena eth subport vlan tpid transmit 2 0x8100")
        
        self.assertCliSuccess("ciena eth subport vlan tpid expect 1 0x8100")
        self.assertCliSuccess("ciena eth subport vlan tpid expect 2 0x8100")

        result = self.runCli("show ciena eth subport vlan tpid")
        self.checkAllCellsOfColumn( result, "Transmit", "0x8100")
        self.checkAllCellsOfColumn( result, "Expect", "0x8100")
        
    def testKByte (self):
        if not self.isMroProduct():
            self.skipTest("Kbyte is only applicable on MRO")

        self.assertCliSuccess("ciena pw kbyte ethtype 0x8100")
        self.assertCliSuccess("ciena pw kbyte type 0x55")
        self.assertCliSuccess("ciena pw kbyte version 0xBB")
        
        result = self.runCli("show ciena pw kbyte")
        ethType = result.cellContent(0, "EthType")
        self.assertEqual(ethType, "0x8100", None)
        version = result.cellContent(0, "Version")
        self.assertEqual(version, "0xbb", None)
        typeField = result.cellContent(0, "Type")
        self.assertEqual(typeField, "0x55", None)
        
        self.assertCliSuccess("ciena pw kbyte remotemac C0.CA.C0.CA.C0.CA")
        self.assertCliSuccess("ciena pw kbyte ethheader C0.CA.C0.CA.C0.CA 1.1.1 none")
        result = self.runCli("show ciena pw kbyte")
        smac = result.cellContent(0, "CentralMAC")
        self.assertEqual(smac, "c0.ca.c0.ca.c0.ca", None)
        
        dmac = result.cellContent(0, "RemoteMAC")
        self.assertEqual(dmac, "c0.ca.c0.ca.c0.ca", None)
        
        vlan = result.cellContent(0, "VLAN")
        self.assertEqual(vlan, "1.1.1", None)
        
        self.assertCliSuccess("ciena pw kbyte ethheader C0.CA.C0.CA.C0.CA 1.1.1 none")
        result = self.runCli("show ciena pw kbyte")
        vlan = result.cellContent(0, "VLAN")
        self.assertEqual(vlan, "1.1.1", None)
                
        self.assertCliSuccess("ciena sdh line extendedkbyte enable 1-16")
        result = self.runCli("show ciena sdh line extendedkbyte 1-16")
        self.checkAllCellsOfColumn( result, "enable", "en")
        
        self.assertCliSuccess("ciena sdh line extendedkbyte disable 1-16")
        result = self.runCli("show ciena sdh line extendedkbyte 1-16")
        self.checkAllCellsOfColumn( result, "enable", "dis")

        self.assertCliSuccess("ciena sdh line extendedkbyte position 1-16 d1-sts1-4")
        result = self.runCli("show ciena sdh line extendedkbyte 1-16")
        self.checkAllCellsOfColumn( result, "position", "d1-sts1-4")
        
        self.assertCliSuccess("ciena sdh line extendedkbyte position 1-16 d1-sts1-10")
        result = self.runCli("show ciena sdh line extendedkbyte 1-16")
        self.checkAllCellsOfColumn( result, "position", "d1-sts1-10")
        
def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(AnnaCliTest)
    runner.summary()
    
if __name__ == '__main__':
    TestMain()

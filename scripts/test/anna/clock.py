import sys

import python.arrive.atsdk.AtRegister as AtRegister
from test.AtTestCase import AtTestCaseRunner
from test.anna.AnnaTestCase import AnnaMroTestCase

class SerdesClockExtractorWithHotChangeSerdesModeTest(AnnaMroTestCase):
    @classmethod
    def defaultRegisterProvider(cls):
        factory = cls.diagRegisterFactory()
        if cls.is10G():
            provider = factory.providerByName("_AF6CNC0021_RD_TOP_GLB")
        else:
            provider = factory.providerByName("_AF6CNC0022_RD_TOP_GLB")
        provider.baseAddress = 0xF00000
        return provider

    @classmethod
    def setUpClass(cls):
        cls.runCli("device init")

    def oversamplingMode(self, serdesMode):
        if serdesMode == "stm0":
            return "stm4"

        if serdesMode == "stm1":
            if self.is20G():
                return "stm4"
            return "stm16"

        return serdesMode

    def expectedHwRateForClockOutput(self, serdesMode):
        oversamplingMode = self.oversamplingMode(serdesMode)
        if oversamplingMode == "stm4":
            return 0
        if oversamplingMode in ["100M", "1G"]:
            return 1
        if oversamplingMode in ["stm64", "stm16"]:
            return 2
        if oversamplingMode == "10G":
            return 3

        return sys.maxint

    @staticmethod
    def allSerdesModes():
        return ["stm0",
                "stm1",
                "stm4",
                "stm16",
                "stm64",
                "100M",
                "1G",
                "10G"]

    @staticmethod
    def serdesModeIsApplicable(serdesId, serdesMode):
        if serdesMode in ["stm64", "10G"]:
            return serdesId in [0, 8]
        return True

    def getRegister(self, name, addressOffset = 0, readHw = True):
        return self.defaultRegisterProvider().getRegister(name, addressOffset=addressOffset, readHw=readHw)

    def hwClockOutputRate(self, serdesId):
        reg = self.getRegister("o_control8")
        mask = AtRegister.cBit1_0 << (serdesId * 2)
        shift = (serdesId * 2)
        return reg.getField(mask, shift)

    def assertSerdesHwClockOutputRate(self, serdesId, serdesMode):
        self.assertEqual(self.hwClockOutputRate(serdesId), self.expectedHwRateForClockOutput(serdesMode))

    def testChangeSerdesMode(self):
        for serdesId in range(self.numFaceplatePorts()):
            self.assertCliSuccess("clock extract serdes 1 %d" % (serdesId + 1))
            self.assertCliSuccess("clock extract enable 1")

            for serdesMode in self.allSerdesModes():
                if self.serdesModeIsApplicable(serdesId, serdesMode):
                    self.assertCliSuccess("serdes mode %d %s" % (serdesId + 1, serdesMode))
                    self.assertSerdesHwClockOutputRate(serdesId, serdesMode)

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(SerdesClockExtractorWithHotChangeSerdesModeTest)
    runner.summary()

if __name__ == '__main__':
    TestMain()


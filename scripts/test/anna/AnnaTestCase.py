"""
Created on Feb 10, 2017

@author: namnn
"""
import sys

import python.registers.Tha60290021.RegisterProviderFactory as RegisterProviderFactory_10G
import python.registers.Tha60290022.RegisterProviderFactory as RegisterProviderFactory_20G
import python.registers.Tha60290011.RegisterProviderFactory as RegisterProviderFactory_Pdh
from test.AtTestCase import AtTestCase

class AnnaProductCodeProvider(object):
    @classmethod
    def mro10GProductCode(cls):
        return 0x60290021

    @classmethod
    def mro20GProductCode(cls):
        return 0x60290022

    @classmethod
    def pdhProductCode(cls):
        return 0x60290011

    @classmethod
    def isMro10G(cls, productCode):
        return productCode == cls.mro10GProductCode()

    @classmethod
    def isMro20G(cls, productCode):
        return productCode == cls.mro20GProductCode()

    @classmethod
    def isMroProduct(cls, productCode):
        return productCode in [cls.mro10GProductCode(), cls.mro20GProductCode()]

    @classmethod
    def isPdhProduct(cls, productCode):
        return productCode == cls.pdhProductCode()

class RegisterManager(object):
    @classmethod
    def factory(cls):
        """
        :rtype: python.arrive.atsdk.AtRegister.AtRegisterProviderFactory
        """
        return None

    @classmethod
    def manager(cls, productCode):
        if AnnaProductCodeProvider.isMro10G(productCode):
            return _Mro10GRegisterManager()
        if AnnaProductCodeProvider.isMro20G(productCode):
            return _Mro20GRegisterManager()
        if AnnaProductCodeProvider.isPdhProduct(productCode):
            return _PdhRegisterManager()

        return None

    def cdrLoRdFileName(self):
        return "Invalid"

    def plaRdFileName(self):
        return "Invalid"

    def bertRdFileName(self):
        return "Invalid"

    def cdrHoRdFileName(self):
        return "Invalid"

    def ocnRdFileName(self):
        return "Invalid"

    def providerByName(self, name):
        return self.factory().providerByName("_%s" % name)

    def plaBaseAddress(self):
        return sys.maxint

    def pdaBaseAddress(self):
        return sys.maxint

    def claBaseAddress(self):
        return sys.maxint

    def bertBaseAddress(self):
        return sys.maxint

    def cdrLoBaseAddress(self, sliceId = 0):
        return sys.maxint

    def cdrHoBaseAddress(self):
        return self.cdrHoAddressRanges()[0][0]

    @staticmethod
    def ocnBaseAddress():
        return 0x0100000

    @staticmethod
    def pohBaseAddress():
        return 0x0200000

    def cdrLoAddressRanges(self):
        return list()

    def cdrHoAddressRanges(self):
        return list()

    def cdrLoRegisterProvider(self, sliceId = 0):
        provider = self.providerByName(self.cdrLoRdFileName())
        provider.baseAddress = self.cdrLoBaseAddress(sliceId)
        return provider

    def cdrHoRegisterProvider(self):
        provider = self.providerByName(self.cdrHoRdFileName())
        provider.baseAddress = self.cdrHoBaseAddress()
        return provider

    def plaAddressRanges(self):
        return list()

    def claAddressRanges(self):
        return list()

    def pdaAddressRanges(self):
        return list()

    def bertAddressRanges(self):
        return list()

    def plaRegisterProvider(self):
        provider = self.providerByName(self.plaRdFileName())
        provider.baseAddress = self.plaBaseAddress()
        return provider

    def bertRegisterProvider(self):
        provider = self.providerByName(self.bertRdFileName())
        provider.baseAddress = self.bertBaseAddress()
        return provider

    @staticmethod
    def pdhAddressRanges():
        return list()

    @staticmethod
    def pohAddressRanges():
        return [(0x0200000, 0x02BFFFF)]

    @staticmethod
    def ocnAddressRanges():
        return [(0x0100000, 0x01FFFFF)]

    @staticmethod
    def mapHoAddressRanges():
        return [(0x0060000, 0x006FFFF)]

    @staticmethod
    def mapLoAddressRanges():
        return list()

    def pdhBaseAddress(self, sliceId = 0):
        return sys.maxint

    def pdhRdFileName(self):
        return "Invalid"

    def mapHoRdFileName(self):
        return "Invalid"

    def pohRdFileName(self):
        return "Invalid"

    def mapLoRdFileName(self):
        return "Invalid"

    def pdhRegisterProvider(self, sliceId = 0):
        provider = self.providerByName(self.pdhRdFileName())
        provider.baseAddress = self.pdhBaseAddress(sliceId)
        return provider

    def pohRegisterProvider(self):
        provider = self.providerByName(self.pohRdFileName())
        provider.baseAddress = self.pohBaseAddress()
        return provider

    def mapLoRegisterProvider(self, sliceId = 0):
        provider = self.providerByName(self.mapLoRdFileName())
        provider.baseAddress = self.mapLoBaseAddress(sliceId)
        return provider

    @staticmethod
    def _addressInRanges(address, ranges):
        for start, end in ranges:
            if start <= address <= end:
                return True
        return False

    def isPdhAddress(self, address):
        return self._addressInRanges(address, self.pdhAddressRanges())

    def isLoCdrAddress(self, address):
        return self._addressInRanges(address, self.cdrLoAddressRanges())

    def isHoCdrAddress(self, address):
        return self._addressInRanges(address, self.cdrHoAddressRanges())

    def isCdrAddress(self, address):
        return self.isHoCdrAddress(address) or self.isLoCdrAddress(address)

    def isOcnAddress(self, address):
        return self._addressInRanges(address, self.ocnAddressRanges())

    def isPlaAddress(self, address):
        return self._addressInRanges(address, self.plaAddressRanges())

    def isClaAddress(self, address):
        return self._addressInRanges(address, self.claAddressRanges())

    def isPdaAddress(self, address):
        return self._addressInRanges(address, self.pdaAddressRanges())

    def isBertAddress(self, address):
        return self._addressInRanges(address, self.bertAddressRanges())

    def ocnRegisterProvider(self):
        provider = self.providerByName(self.ocnRdFileName())
        provider.baseAddress = self.ocnBaseAddress()
        return provider

    def isPohAddress(self, address):
        return self._addressInRanges(address, self.pohAddressRanges())

    def isMapHoAddress(self, address):
        return self._addressInRanges(address, self.mapHoAddressRanges())

    def mapHoLocalAddress(self, address):
        return self._localAddressWithRanges(address, self.mapHoAddressRanges())

    def isMapLoAddress(self, address):
        return self._addressInRanges(address, self.mapLoAddressRanges())

    def mapLoBaseAddress(self, sliceId = 0):
        return self.mapLoAddressRanges()[sliceId][0]

    def mapLoLocalAddress(self, address):
        return self._localAddressWithRanges(address, self.mapLoAddressRanges())

    @staticmethod
    def addressSliceId(address, ranges):
        sliceId = 0
        for start, end in ranges:
            if start <= address <= end:
                return sliceId
            sliceId = sliceId + 1

        return sys.maxint

    def mapLoAddressSliceId(self, address):
        return self.addressSliceId(address, self.mapLoAddressRanges())

    @staticmethod
    def _localAddressWithRanges(address, ranges):
        for start, end in ranges:
            if start <= address <= end:
                return address - start

        return sys.maxint

    def cdrLocalAddress(self, address):
        return self._localAddressWithRanges(address, self.cdrLoAddressRanges())

    def pdhLocalAddress(self, address):
        return self._localAddressWithRanges(address, self.pdhAddressRanges())

    def pdhAddressSliceId(self, address):
        return self.addressSliceId(address, self.pdhAddressRanges())

    def cdrLoAddressSliceId(self, address):
        return self.addressSliceId(address, self.cdrLoAddressRanges())

    def ocnLocalAddress(self, address):
        return self._localAddressWithRanges(address, self.ocnAddressRanges())

    def pohLocalAddress(self, address):
        return self._localAddressWithRanges(address, self.pohAddressRanges())

    def plaLocalAddress(self, address):
        return self._localAddressWithRanges(address, self.plaAddressRanges())

    def pdaLocalAddress(self, address):
        return self._localAddressWithRanges(address, self.pdaAddressRanges())

    def claLocalAddress(self, address):
        return self._localAddressWithRanges(address, self.claAddressRanges())

    def bertLocalAddress(self, address):
        return self._localAddressWithRanges(address, self.bertAddressRanges())

class _MroRegisterManager(RegisterManager):
    def pdhBaseAddress(self, sliceId = 0):
        return self.pdhAddressRanges()[sliceId][0]

    def pdaBaseAddress(self):
        return 0x0500000

    def plaBaseAddress(self):
        return 0x0400000

    def claBaseAddress(self):
        return 0x0600000

    def bertBaseAddress(self):
        return 0x0350000

    def plaAddressRanges(self):
        return [(0x0400000, 0x04FFFFF)]

    def claAddressRanges(self):
        return [(0x0600000, 0x06FFFFF)]

    def pdaAddressRanges(self):
        return [(0x0500000, 0x05FFFFF)]

    def bertAddressRanges(self):
        return [(0x0350000, 0x035FFFF)]

class _PdhRegisterManager(RegisterManager):
    @classmethod
    def factory(cls):
        import python.registers.Tha60290011.RegisterProviderFactory as RegisterProviderFactory
        return RegisterProviderFactory.RegisterProviderFactory()

    def cdrLoRdFileName(self):
        return "AF6CNC0011_RD_CDR"

    def cdrHoRdFileName(self):
        return self.cdrLoRdFileName()

    def pdhRdFileName(self):
        return "AF6CNC0011_RD_PDH"

class _Mro10GRegisterManager(_MroRegisterManager):
    @classmethod
    def factory(cls):
        import python.registers.Tha60290021.RegisterProviderFactory as RegisterProviderFactory
        return RegisterProviderFactory.RegisterProviderFactory()

    def cdrLoRdFileName(self):
        return "AF6CCI0011_RD_CDR"

    def plaRdFileName(self):
        return "AF6CCI0011_RD_PLA"

    def claRdFileName(self):
        return "AF6CCI0011_RD_CLA"

    def pdaRdFileName(self):
        return "AF6CCI0011_RD_PLA"

    def cdrHoRdFileName(self):
        return "AF6CCI0011_RD_CDR_HO"

    def pdhRdFileName(self):
        return "AF6CCI0011_RD_PDH"

    def ocnRdFileName(self):
        return "AF6CNC0021_RD_OCN"

    def mapHoRdFileName(self):
        return "AF6CCI0011_RD_MAP_HO"

    def pohRdFileName(self):
        return "AF6CNC0021_RD_POH_BER"

    def mapLoRdFileName(self):
        return "AF6CCI0011_RD_MAP"

    @staticmethod
    def mapLoAddressRanges():
        return [(0x0800000, 0x083FFFF),
                (0x0840000, 0x087FFFF),
                (0x0880000, 0x08BFFFF),
                (0x08C0000, 0x08FFFFF),
                (0x0900000, 0x093FFFF),
                (0x0940000, 0x097FFFF),
                (0x0980000, 0x09BFFFF),
                (0x09C0000, 0x09FFFFF),
                (0x0A00000, 0x0A3FFFF),
                (0x0A40000, 0x0A7FFFF),
                (0x0A80000, 0x0ABFFFF),
                (0x0AC0000, 0x0AFFFFF)]

    def cdrLoAddressRanges(self):
        return [(0x0C00000, 0x0C3FFFF),
                (0x0C40000, 0x0C7FFFF),
                (0x0C80000, 0x0CBFFFF),
                (0x0CC0000, 0x0CFFFFF),
                (0x0D00000, 0x0D3FFFF),
                (0x0D40000, 0x0D7FFFF),
                (0x0D80000, 0x0DBFFFF),
                (0x0DC0000, 0x0DFFFFF),
                (0x0E00000, 0x0E3FFFF),
                (0x0E40000, 0x0E7FFFF),
                (0x0E80000, 0x0EBFFFF),
                (0x0EC0000, 0x0EFFFFF)]

    def cdrHoAddressRanges(self):
        return [(0x0300000, 0x033FFFF)]

    def cdrLoBaseAddress(self, sliceId = 0):
        return self.cdrLoAddressRanges()[sliceId][0]

    @staticmethod
    def pdhAddressRanges():
        return [(0x1000000, 0x10FFFFF),
                (0x1100000, 0x11FFFFF),
                (0x1200000, 0x12FFFFF),
                (0x1300000, 0x13FFFFF),
                (0x1400000, 0x14FFFFF),
                (0x1500000, 0x15FFFFF),
                (0x1600000, 0x16FFFFF),
                (0x1700000, 0x17FFFFF),
                (0x1800000, 0x18FFFFF),
                (0x1900000, 0x19FFFFF),
                (0x1A00000, 0x1AFFFFF),
                (0x1B00000, 0x1BFFFFF)]

class _Mro20GRegisterManager(_MroRegisterManager):
    def cdrLoRdFileName(self):
        return "AF6CNC0022_RD_CDR"

    def plaRdFileName(self):
        return "AF6CNC0022_RD_PLA"

    def claRdFileName(self):
        return "AF6CNC0022_RD_CLA"

    def pdaRdFileName(self):
        return "AF6CNC0022_RD_PDA"

    def bertRdFileName(self):
        return "AF6CNC0022_RD_BERT_GEN"

    def ocnRdFileName(self):
        return "AF6CNC0022_RD_OCN"

    def pohRdFileName(self):
        return "AF6CNC0022_RD_POH_BER"

    def isMapHoAddress(self, address):
        # This product does not have MAP HO
        return False

    def pdhRdFileName(self):
        return "AF6CNC0022_RD_PDH"

    @classmethod
    def factory(cls):
        import python.registers.Tha60290022.RegisterProviderFactory as RegisterProviderFactory
        return RegisterProviderFactory.RegisterProviderFactory()

    def cdrLoAddressRanges(self):
        return [(0x0C00000, 0x0C3FFFF),
                (0x0C40000, 0x0C7FFFF),
                (0x0C80000, 0x0CBFFFF),
                (0x0CC0000, 0x0CFFFFF),
                (0x0D00000, 0x0D3FFFF),
                (0x0D40000, 0x0D7FFFF),
                (0x0D80000, 0x0DBFFFF),
                (0x0DC0000, 0x0DFFFFF)]

    def cdrHoAddressRanges(self):
        return self.cdrLoAddressRanges()

    def cdrHoRdFileName(self):
        return self.cdrLoRdFileName()

    @staticmethod
    def pdhAddressRanges():
        return [(0x1000000, 0x10FFFFF),
                (0x1100000, 0x11FFFFF),
                (0x1200000, 0x12FFFFF),
                (0x1300000, 0x13FFFFF),
                (0x1400000, 0x14FFFFF),
                (0x1500000, 0x15FFFFF),
                (0x1600000, 0x16FFFFF),
                (0x1700000, 0x17FFFFF)]

    @staticmethod
    def mapLoAddressRanges():
        return [(0x0800000, 0x083FFFF),
                (0x0840000, 0x087FFFF),
                (0x0880000, 0x08BFFFF),
                (0x08C0000, 0x08FFFFF),
                (0x0900000, 0x093FFFF),
                (0x0940000, 0x097FFFF),
                (0x0980000, 0x09BFFFF),
                (0x09C0000, 0x09FFFFF)]

    def mapLoRdFileName(self):
        return "AF6CNC0022_RD_MAP"

class AnnaTestCase(AtTestCase):
    _registerProvider = None
    _registerManager = None

    def __init__(self, methodName='runTest'):
        AtTestCase.__init__(self, methodName=methodName)
        self._registerProvider = None

    @classmethod
    def createDefaultRegisterProvider(cls):
        """
        :rtype: python.arrive.atsdk.AtRegister.AtRegisterProvider
        """
        return None
    
    @classmethod
    def defaultRegisterProvider(cls):
        """
        :rtype: python.arrive.atsdk.AtRegister.AtRegisterProvider
        """
        if cls._registerProvider:
            return cls._registerProvider
        cls._registerProvider = cls.createDefaultRegisterProvider()
        return cls._registerProvider
        
    @classmethod
    def isMroProduct(cls):
        return AnnaProductCodeProvider.isMroProduct(cls.productCode())

    @classmethod
    def is20G(cls):
        return cls.productCode() == AnnaProductCodeProvider.mro20GProductCode()

    @classmethod
    def is10G(cls):
        return AnnaProductCodeProvider.isMro10G(cls.productCode())

    @classmethod
    def isPdhProduct(cls):
        return AnnaProductCodeProvider.isPdhProduct(cls.productCode())

    @classmethod
    def canRun(cls):
        if cls.isMroProduct() or cls.isPdhProduct():
            return True
        return False

    @classmethod
    def registerManager(cls):
        """
        :rtype: RegisterManager
        """
        if cls._registerManager is None:
            cls._registerManager = RegisterManager.manager(cls.productCode())
        return cls._registerManager

class AnnaMroTestCase(AnnaTestCase):
    @classmethod
    def diagRegisterFactory(cls):
        """
        :rtype: python.arrive.atsdk.AtRegister.AtRegisterProviderFactory
        """
        if cls.is20G():
            import python.registers.Tha60290022.diag.RegisterProviderFactory as Mro20GDiagRegisterProviderFactory
            return Mro20GDiagRegisterProviderFactory.RegisterProviderFactory()

        import python.registers.Tha60290021.diag.RegisterProviderFactory as Mro10GDiagRegisterProviderFactory
        return Mro10GDiagRegisterProviderFactory.RegisterProviderFactory()

    @classmethod
    def numFaceplatePorts(cls):
        if AnnaProductCodeProvider.isMro10G(cls.productCode()):
            return 8
        return 16

    @classmethod
    def numTerminatedLines(cls):
        if AnnaProductCodeProvider.isMro10G(cls.productCode()):
            return 4
        return 8

    @classmethod
    def terminatedLineStartCliId(cls):
        return 25

    def stm16FaceplateLineCliIdList(self):
        """
        :rtype: list
        """
        return [lineId + 1 for lineId in range(0, self.numFaceplatePorts(), 2)]

    @classmethod
    def canRun(cls):
        return cls.isMroProduct()

    @classmethod
    def registerProviderFactory(cls):
        if AnnaProductCodeProvider.isMro10G(cls.productCode()):
            return RegisterProviderFactory_10G.RegisterProviderFactory()
        if AnnaProductCodeProvider.isMro20G(cls.productCode()):
            return RegisterProviderFactory_20G.RegisterProviderFactory()

        return None

    @staticmethod
    def faceplateGroupId(portId):
        return portId / 4

    @staticmethod
    def faceplateLocalIdInGroup(portId):
        return portId % 4

    @classmethod
    def faceplateDrpBaseAddress(cls, portId):
        topBase = 0xF60000
        drpRelativeBase = 0x1000
        return cls.faceplateLocalIdInGroup(portId) * 0x400 + cls.faceplateGroupId(portId) * 0x2000 + topBase + drpRelativeBase

class AnnaPdhTestCase(AnnaTestCase):
    @classmethod
    def canRun(cls):
        return cls.isPdhProduct()

    @classmethod
    def registerProviderFactory(cls):
        return RegisterProviderFactory_Pdh.RegisterProviderFactory()

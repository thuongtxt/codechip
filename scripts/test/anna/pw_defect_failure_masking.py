import sys
from python.arrive.atsdk.cli import AtCliRunnable
from test.AtTestCase import AtTestCaseRunner
from test.anna.AnnaTestCase import AnnaTestCase, AnnaProductCodeProvider


class AbstractCliRunnable(AtCliRunnable):
    def __init__(self, productCode):
        self.productCode = productCode

    @classmethod
    def runCli(cls, cli):
        """
        :rtype: python.arrive.AtAppManager.AtCliResult.AtCliResult
        """
        result = AtCliRunnable.runCli(cli)
        assert result.success()
        return result

class ConfigurationProvider(AbstractCliRunnable):
    def script(self):
        return "Invalid"

    def alarmAddress(self):
        return sys.maxint

    def failureAddress(self):
        return sys.maxint

    def makeAlarm(self, make = True):
        value = 0xFFFFFFFF if make else 0x0
        self.runCli("wr 0x%x 0x%x" % (self.alarmAddress(), value))

    def makeFailure(self, make = True):
        value = 0xFFFFFFFF if make else 0x0
        self.runCli("wr 0x%x 0x%x" % (self.failureAddress(), value))

    @staticmethod
    def assertStatus(statusSet, cliResult):
        for col in range(1, cliResult.numColumns()):
            cellContent = cliResult.cellContent(0, col)
            if cellContent == "N/A":
                continue

            assert cellContent in ["set", "clear"]

            status = True if cellContent == "set" else False
            assert status == statusSet

    def assertHasAlarm(self, hasAlarms):
        cliResult = self.runCli("show pw alarm 1")
        self.assertStatus(hasAlarms, cliResult)

    def assertHasFailure(self, hasFailures):
        cliResult = self.runCli("show pw failure 1")
        self.assertStatus(hasFailures, cliResult)

    def _enablePw(self, enabled):
        self.runCli("pw %s 1" % ("enable" if enabled else "disable"))

    def enablePw(self):
        self._enablePw(enabled=True)

    def disablePw(self):
        self._enablePw(enabled=False)


class MroConfigurationProvider(ConfigurationProvider):
    def script(self):
        return """
        device init

        xc vc connect vc3.1.1.1 vc3.25.1.1 two-way

        pw create cep 1 basic
        pw circuit bind 1 vc3.25.1.1
        pw ethport 1 1
        pw psn 1 mpls
        pw enable 1
        """

    def alarmAddress(self):
        if AnnaProductCodeProvider.isMro20G(self.productCode):
            return 0x01e68000
        return 0x01e61000

    def failureAddress(self):
        if AnnaProductCodeProvider.isMro20G(self.productCode):
            return 0x0073c000
        return 0x00720000

class PdhConfigurationProvider(ConfigurationProvider):
    def script(self):
        return """
        device init

        ciena pdh interface de3
        pdh serline mode 1 ec1
        sdh map vc3.1 de3

        pw create satop 1
        pw circuit bind 1 de3.1
        pw ethport 1 1
        pw psn 1 mpls
        pw enable 1
        """

    def alarmAddress(self):
        return 0x00541000

    def failureAddress(self):
        return 0x00920000

class DefectFailureMasking(AnnaTestCase):
    _provider = None

    @classmethod
    def setProvider(cls, provider):
        cls._provider = provider

    @classmethod
    def getProvider(cls):
        """
        :rtype: ConfigurationProvider
        """
        return cls._provider

    @classmethod
    def setUpClass(cls):
        cls.assertClassCliSuccess(cls.getProvider().script())

    def testMakeAlarm(self):
        provider = self.getProvider()

        for make in [True, False, True, False]:
            provider.makeAlarm(make=make)
            provider.assertHasAlarm(hasAlarms=make)

    def testMakeFailure(self):
        provider = self.getProvider()

        for make in [True, False, True, False]:
            provider.makeFailure(make=make)
            provider.assertHasFailure(hasFailures=make)

    def testMaskDefect(self):
        provider = self.getProvider()
        provider.makeAlarm(make=True)
        provider.assertHasAlarm(hasAlarms=True)
        provider.disablePw()
        provider.assertHasAlarm(hasAlarms=False)
        provider.enablePw()
        provider.makeAlarm(make=True)
        provider.assertHasAlarm(hasAlarms=True)

    def testMaskFailure(self):
        provider = self.getProvider()
        provider.makeFailure(make=True)
        provider.assertHasFailure(hasFailures=True)
        provider.disablePw()
        provider.assertHasFailure(hasFailures=False)
        provider.enablePw()
        provider.assertHasFailure(hasFailures=True)

    @classmethod
    def createProvider(cls, productCode):
        if cls.isMroProduct():
            return MroConfigurationProvider(productCode)

        if cls.isPdhProduct():
            return PdhConfigurationProvider(productCode)

        return None

    @classmethod
    def runWithRunner(cls, runner):
        provider = cls.createProvider(cls.productCode())
        cls.setProvider(provider)
        runner.run(cls)

def TestMain():
    runner = AtTestCaseRunner.runner()
    DefectFailureMasking.runWithRunner(runner)
    runner.summary()

if __name__ == '__main__':
    TestMain()

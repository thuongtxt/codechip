"""
Created on Apr 21, 2017

@author: namnn
"""
import sys
import getopt
from test.AtTestCase import AtTestCaseRunner, AtTestCase
import utils.access_compare as access_compare
from python.EpApp import EpApp

class AnnaConcateTest(AtTestCase):
    epappWorkingPath = None
    epappNewPath = None
    compareOnly = False
    
    def workingEpappCommand(self):
        return "%s -s 60290021 -w \"ff/ff/ff ee.ee.eeee\"" % self.epappWorkingPath
    
    def newEpappCommand(self):
        return "%s -s 60290021 -w \"ff/ff/ff ee.ee.eeee\"" % self.epappNewPath
    
    @staticmethod
    def compare(logFile1, logFile2, registerProvider = None):
        compare = access_compare.Compare(logFile1, logFile2)
        if registerProvider is not None:
            compare.registerProvider = registerProvider
        same = compare.compare()
        if not same:
            compare.showDifferents()
        return same
    
    @staticmethod
    def runScript(app, script):
        app.verbosed = True
        app.start()
        app.runCli(script)
        app.exit()
    
    @staticmethod
    def createRegisterProvider():
        return AnnaConcateTestRegisterProvider()
    
    def directScript(self, logFile):
        return None
    
    def concateScript(self, logFile):
        return None
    
    def testAccessMustBeSameBetweenWorkingAndNewBuilds(self):
        workingLog = ".working.log"
        newLog = ".new.log"
        
        if not self.compareOnly:
            self.runScript(EpApp(self.workingEpappCommand()), self.directScript(workingLog))
            self.runScript(EpApp(self.newEpappCommand()), self.directScript(newLog))
            
        same = self.compare(workingLog, newLog)
        self.assertTrue(same, "Access are different. See %s, %s" % (workingLog, newLog))
    
    def testDirectConcateAndRandomConcateMustHaveSameXcConfiguration(self):
        directLog = ".direct_concate.log"
        concateLog = ".concate.log"
        
        if not self.compareOnly:
            self.runScript(EpApp(self.workingEpappCommand()), self.directScript(directLog))
            self.runScript(EpApp(self.newEpappCommand()), self.concateScript(concateLog))
            
        same = self.compare(directLog, concateLog, registerProvider=AnnaConcateTestRegisterProvider())
        self.assertTrue(same, "Access are different. See %s, %s" % (directLog, concateLog))
    
class AnnaConcateTestRegisterProvider(access_compare.RegisterProvider):
    @staticmethod
    def ocnBaseAddress():
        return 0x0100000
    
    def isXcAddress(self, address):
        localAddress = address - self.ocnBaseAddress()
        if (0x44000 <= localAddress <= 0x44b2f) or \
           (0x44040 <= localAddress <= 0x44b6f):
            return True
        return False
    
    def shouldCompleteIgnoreAddress(self, address):
        if self.isXcAddress(address):
            return False
        return True
    
class AnnaConcateTest_vc4_16c(AnnaConcateTest):
    def directScript(self, logFile):
        return """
        device init
        
        hal log enable
        hal log file %s
        sdh map aug16.1.1 vc4_16c
        sdh map aug16.25.1 vc4_16c
        
        xc vc connect vc4_16c.25.1 vc4_16c.1.1 two-way
        
        hal log disable
        """ % logFile
    
    def concateScript(self, logFile):
        return """
        device init

        hal log enable
        hal log file %s
        
        sdh map aug16.25.1 4xaug4s
        sdh map aug4.25.1-25.4 4xaug1s
        sdh map aug1.25.1-25.16 vc4
        
        sdh map aug16.1.1 4xaug4s
        sdh map aug4.1.1-1.4 4xaug1s
        sdh map aug1.1.1-1.16 vc4
        
        sdh path concate au4.25.1 au4.25.2-25.16
        sdh path concate au4.1.1 au4.1.2-1.16
        
        xc vc connect vc4_nc.25.1 vc4_nc.1.1 two-way
        
        hal log disable
        """ % logFile

class AnnaConcateTest_vc4_4c(AnnaConcateTest):
    def directScript(self, logFile):
        return """
        device init

        hal log enable
        hal log file %s
        sdh map aug16.1.1,25.1 4xaug4s
        sdh map aug4.1.1-1.4,25.1-25.4 vc4_4c
        xc vc connect vc4_4c.25.1-25.4 vc4_4c.1.1-1.4 two-way
        hal log disable
        """ % logFile
    
    def concateScript(self, logFile):
        return """
        device init

        hal log enable
        hal log file %s
        sdh map aug16.1.1,25.1 4xaug4s
        sdh map aug4.1.1-1.4,25.1-25.4 4xaug1s
        sdh map aug1.1.1-1.16,25.1-25.16 vc4
        xc vc connect vc4.25.1-25.16 vc4.1.1-1.16 two-way
        hal log disable
        """ % logFile

class AnnaConcateTest_vc4(AnnaConcateTest):
    def directScript(self, logFile):
        return """
        device init

        hal log enable
        hal log file %s
        sdh map aug16.1.1,25.1 4xaug4s
        sdh map aug4.1.1-1.4,25.1-25.4 4xaug1s
        sdh map aug1.1.1-1.16,25.1-25.16 vc4
        xc vc connect vc4.25.1-25.16 vc4.1.1-1.16 two-way
        hal log disable
        """ % logFile
    
    def concateScript(self, logFile):
        return self.directScript(logFile)

def Main():
    def printUsage():
        print "Usage: %s [--help] --epapp-working=<epappWorkingPath> --epapp-new=<epappWorkingPath> [--compare-only]" % \
              sys.argv[0]

    opts = list()
    try:
        opts, _ = getopt.getopt(sys.argv[1:], "", ["help", "epapp-working=", "epapp-new=", "compare-only"])
    except getopt.GetoptError:
        return 1

    epappWorkingPath = None
    epappNewPath = None
    compareOnly = False

    for opt, arg in opts:
        if opt == "--help":
            printUsage()
            return 0

        if opt == "--epapp-working":
            epappWorkingPath = arg

        if opt == "--epapp-new":
            epappNewPath = arg

        if opt == "--compare-only":
            compareOnly = True

    if epappWorkingPath is None or epappNewPath is None:
        print "Two epapp builds must be specified"
        printUsage()
        return 1

    def runTest(testCase):
        testCase.epappWorkingPath = epappWorkingPath
        testCase.epappNewPath = epappNewPath
        testCase.compareOnly = compareOnly
        runner.run(testCase)

    runner = AtTestCaseRunner.runner()
    runTest(AnnaConcateTest_vc4_16c)
    runTest(AnnaConcateTest_vc4_4c)
    runTest(AnnaConcateTest_vc4)
    return 0 if runner.summary() else 1

if __name__ == '__main__':
    sys.exit(Main())

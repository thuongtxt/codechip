from python.arrive.atsdk.cli import AtCliRunnable
from test.AtTestCase import AtTestCaseRunner
from test.anna.AnnaTestCase import AnnaTestCase, AnnaMroTestCase

class AbstractCliRunnable(AtCliRunnable):
    @classmethod
    def runCli(cls, cli):
        """
        :rtype: python.arrive.AtAppManager.AtCliResult.AtCliResult
        """
        result = AtCliRunnable.runCli(cli)
        assert result.success()
        return result

class PwController(AbstractCliRunnable):
    @classmethod
    def enablePw(cls, cliId, enabled=True):
        cli = "pw %s %s" % ("enable" if enabled else "disable", cliId)
        cls.runCli(cli)

    @classmethod
    def disablePw(cls, cliId):
        cls.enablePw(cliId, enabled=False)

    @classmethod
    def bindEth(cls, cliId):
        cls.runCli("pw ethport %s 1" % cliId)

    @classmethod
    def unbindEth(cls, cliId):
        cls.runCli("pw ethport %s none" % cliId)

class HoXcController(AbstractCliRunnable):
    @classmethod
    def connect(cls, source, dest, connected = True, twoWays=True):
        action = "connect" if connected else "disconnect"
        cli = "xc vc %s %s %s" % (action, source, dest)
        if twoWays: cli = cli + " two-way"
        cls.runCli(cli)

class ConfigurationProvider(AbstractCliRunnable):
    def __init__(self, productCode):
        self.productCode = productCode

    @staticmethod
    def pwCliIdString():
        return "1"

    def disablePw(self):
        self.enablePw(enabled=False)

    def enablePw(self, enabled=True):
        PwController.enablePw(self.pwCliIdString(), enabled)

    def bindEth(self):
        PwController.bindEth(self.pwCliIdString())

    def unbindEth(self):
        PwController.unbindEth(self.pwCliIdString())

    def disconnect(self):
        self._connect(connected=False)

    def connect(self):
        self._connect(connected=True)

    def setupScript(self):
        return "Invalid"

    def forceAis(self, forced = True):
        assert 0

    def assertAisForced(self, forced):
        assert 0

    def _connect(self, connected):
        assert 0

    def hasXc(self):
        return False

    @staticmethod
    def providersOfClasses(classes, productCode):
        return [providerClass(productCode) for providerClass in classes]

    @classmethod
    def mroAllProviders(cls, productCode):
        allClasses = [MroCardAu3Vc3CepConfigurationProvider,
                      MroCardVc4CepConfigurationProvider,
                      MroCardVc4_4cCepConfigurationProvider,
                      MroCardTu3Vc3CepConfigurationProvider,
                      MroCardVc1xCepConfigurationProvider,
                      MroCardDs1SatopConfigurationProvider,
                      MroCardDe3SatopConfigurationProvider]

        return cls.providersOfClasses(allClasses, productCode)

    @classmethod
    def pdhAllProviders(cls, productCode):
        allClasses = [PdhCardAu3Vc3CepConfigurationProvider,
                      PdhCardVc1xCepConfigurationProvider,
                      PdhCardDs1SatopConfigurationProvider,
                      PdhCardDe3SatopConfigurationProvider]
        return cls.providersOfClasses(allClasses, productCode)

class SdhPathAisController(AbstractCliRunnable):
    @classmethod
    def forceAis(cls, cliId, forced = True):
        cls.runCli("sdh path force alarm %s ais tx %s" % (cliId, "en" if forced else "dis"))

    @classmethod
    def assertAisForced(cls, cliId, forced):
        result = cls.runCli("show sdh path forcedalarms %s" % cliId)
        cellContent = result.cellContent(0, "ForcedTxAlarms")
        txForcedAlarms = [alarmString.lower() for alarmString in cellContent.split("|")]
        aisForced = "ais" in txForcedAlarms
        assert forced == aisForced

class SdhPathCepConfigurationProvider(ConfigurationProvider):
    @staticmethod
    def channelForAisForcing():
        return "invalid"

    def forceAis(self, forced = True):
        SdhPathAisController.forceAis(self.channelForAisForcing(), forced)

    def assertAisForced(self, forced):
        SdhPathAisController.assertAisForced(self.channelForAisForcing(), forced)

class PdhSatopConfigurationProvider(ConfigurationProvider):
    @staticmethod
    def channelType():
        return "Invalid"

    @staticmethod
    def channelForAisForcing():
        return "Invalid"

    def forceAis(self, forced = True):
        self.runCli("pdh %s force alarm %s tx ais %s" % (self.channelType(), self.channelForAisForcing(), "en" if forced else "dis"))

    def assertAisForced(self, forced):
        result = self.runCli("show pdh %s %s" % (self.channelType(), self.channelForAisForcing()))
        cellContent = result.cellContent(0, "ForcedTxAlarms")
        txForcedAlarms = [alarmString.lower() for alarmString in cellContent.split("|")]
        aisForced = "ais" in txForcedAlarms
        assert forced == aisForced

class MroCardAu3Vc3CepConfigurationProvider(SdhPathCepConfigurationProvider):
    def setupScript(self):
        return """
        device init

        xc vc connect vc3.1.1.1 vc3.25.1.1 two-way

        pw create cep 1 basic
        pw circuit bind 1 vc3.25.1.1
        pw ethport 1 1
        pw psn 1 mpls
        pw enable 1
        """
        
    def setupWithoutEthScript(self):
        return """
        device init

        xc vc connect vc3.1.1.1 vc3.25.1.1 two-way

        pw create cep 1 basic
        pw circuit bind 1 vc3.25.1.1
        """
        
    def hasXc(self):
        return True

    def _connect(self, connected):
        HoXcController.connect(source="vc3.1.1.1", dest="vc3.25.1.1", connected=connected, twoWays=True)

    @staticmethod
    def channelForAisForcing():
        return "au3.1.1.1"

class MroCardVc4CepConfigurationProvider(SdhPathCepConfigurationProvider):
    def setupScript(self):
        return """
        device init
        
        sdh map aug1.1.1,25.1 vc4
        sdh map vc4.1.1,25.1 c4
        xc vc connect vc4.1.1 vc4.25.1 two-way

        pw create cep 1 basic
        pw circuit bind 1 vc4.25.1
        pw ethport 1 1
        pw psn 1 mpls
        pw enable 1
        """
    def setupWithoutEthScript(self):
        return """
        device init
        
        sdh map aug1.1.1,25.1 vc4
        sdh map vc4.1.1,25.1 c4
        xc vc connect vc4.1.1 vc4.25.1 two-way

        pw create cep 1 basic
        pw circuit bind 1 vc4.25.1
        """

    def hasXc(self):
        return True

    def _connect(self, connected):
        HoXcController.connect(source="vc4.1.1", dest="vc4.25.1", connected=connected, twoWays=True)

    @staticmethod
    def channelForAisForcing():
        return "au4.1.1"


class MroCardVc4_4cCepConfigurationProvider(SdhPathCepConfigurationProvider):
    def setupScript(self):
        return """
        device init

        sdh map aug4.1.1,25.1 vc4_4c
        sdh map vc4_4c.1.1,25.1 c4_4c
        xc vc connect vc4_4c.1.1 vc4_4c.25.1 two-way

        pw create cep 1 basic
        pw circuit bind 1 vc4_4c.25.1
        pw ethport 1 1
        pw psn 1 mpls
        pw enable 1
        """
    def setupWithoutEthScript(self):
        return """
        device init

        sdh map aug4.1.1,25.1 vc4_4c
        sdh map vc4_4c.1.1,25.1 c4_4c
        xc vc connect vc4_4c.1.1 vc4_4c.25.1 two-way

        pw create cep 1 basic
        pw circuit bind 1 vc4_4c.25.1
        """
    def hasXc(self):
        return True

    def _connect(self, connected):
        HoXcController.connect(source="vc4_4c.1.1", dest="vc4_4c.25.1", connected=connected, twoWays=True)

    @staticmethod
    def channelForAisForcing():
        return "au4_4c.1.1"


class MroCardTu3Vc3CepConfigurationProvider(SdhPathCepConfigurationProvider):
    def setupScript(self):
        return """
        device init

        sdh map aug1.1.1,25.1 vc4
        sdh map vc4.1.1,25.1 c4
        xc vc connect vc4.1.1 vc4.25.1 two-way
    
        sdh map vc4.25.1 3xtug3s
        sdh map tug3.25.1.1 vc3
        sdh map vc3.25.1.1 c3
    
        pw create cep 1 basic
        pw circuit bind 1 vc3.25.1.1
        pw ethport 1 1
        pw psn 1 mpls
        pw enable 1
        """
    def setupWithoutEthScript(self):
        return """
        device init

        sdh map aug1.1.1,25.1 vc4
        sdh map vc4.1.1,25.1 c4
        xc vc connect vc4.1.1 vc4.25.1 two-way
    
        sdh map vc4.25.1 3xtug3s
        sdh map tug3.25.1.1 vc3
        sdh map vc3.25.1.1 c3
    
        pw create cep 1 basic
        pw circuit bind 1 vc3.25.1.1
        """
    @staticmethod
    def channelForAisForcing():
        return "tu3.25.1.1"

class MroCardVc1xCepConfigurationProvider(SdhPathCepConfigurationProvider):
    def setupScript(self):
        return """
        device init

        sdh map aug1.1.1,25.1 vc4
        sdh map vc4.1.1,25.1 c4
        xc vc connect vc4.1.1 vc4.25.1 two-way
        
        sdh map vc4.25.1 3xtug3s
        sdh map tug3.25.1.1 7xtug2s
        sdh map tug2.25.1.1.1 tu11
        sdh map vc1x.25.1.1.1.1 c1x

        pw create cep 1 basic
        pw circuit bind 1 vc1x.25.1.1.1.1
        pw ethport 1 1
        pw psn 1 mpls
        pw enable 1
        """
    def setupWithoutEthScript(self):
        return """
        device init

        sdh map aug1.1.1,25.1 vc4
        sdh map vc4.1.1,25.1 c4
        xc vc connect vc4.1.1 vc4.25.1 two-way
        
        sdh map vc4.25.1 3xtug3s
        sdh map tug3.25.1.1 7xtug2s
        sdh map tug2.25.1.1.1 tu11
        sdh map vc1x.25.1.1.1.1 c1x

        pw create cep 1 basic
        pw circuit bind 1 vc1x.25.1.1.1.1
        """
    @staticmethod
    def channelForAisForcing():
        return "tu1x.25.1.1.1.1"


class MroCardDs1SatopConfigurationProvider(PdhSatopConfigurationProvider):
    def setupScript(self):
        return """
        device init

        sdh map aug1.1.1,25.1 vc4
        sdh map vc4.1.1,25.1 c4
        xc vc connect vc4.1.1 vc4.25.1 two-way

        sdh map vc4.25.1 3xtug3s
        sdh map tug3.25.1.1 7xtug2s
        sdh map tug2.25.1.1.1 tu11
        sdh map vc1x.25.1.1.1.1 de1

        pw create satop 1
        pw circuit bind 1 de1.25.1.1.1.1
        pw ethport 1 1
        pw psn 1 mpls
        pw enable 1
        """
    def setupWithoutEthScript(self):
        return """
        device init

        sdh map aug1.1.1,25.1 vc4
        sdh map vc4.1.1,25.1 c4
        xc vc connect vc4.1.1 vc4.25.1 two-way

        sdh map vc4.25.1 3xtug3s
        sdh map tug3.25.1.1 7xtug2s
        sdh map tug2.25.1.1.1 tu11
        sdh map vc1x.25.1.1.1.1 de1

        pw create satop 1
        pw circuit bind 1 de1.25.1.1.1.1
        """
    @staticmethod
    def channelForAisForcing():
        return "25.1.1.1.1"

    @staticmethod
    def channelType():
        return "de1"

class MroCardDe3SatopConfigurationProvider(PdhSatopConfigurationProvider):
    def setupScript(self):
        return """
        device init

        sdh map aug1.1.1,25.1 vc4
        sdh map vc4.1.1,25.1 c4
        xc vc connect vc4.1.1 vc4.25.1 two-way

        sdh map vc4.25.1 3xtug3s
        sdh map tug3.25.1.1 vc3
        sdh map vc3.25.1.1 de3

        pw create satop 1
        pw circuit bind 1 de3.25.1.1
        pw ethport 1 1
        pw psn 1 mpls
        pw enable 1
        """
    def setupWithoutEthScript(self):
        return """
        device init

        sdh map aug1.1.1,25.1 vc4
        sdh map vc4.1.1,25.1 c4
        xc vc connect vc4.1.1 vc4.25.1 two-way

        sdh map vc4.25.1 3xtug3s
        sdh map tug3.25.1.1 vc3
        sdh map vc3.25.1.1 de3

        pw create satop 1
        pw circuit bind 1 de3.25.1.1
        """
    @staticmethod
    def channelForAisForcing():
        return "25.1.1"

    @staticmethod
    def channelType():
        return "de3"


class PdhCardAu3Vc3CepConfigurationProvider(SdhPathCepConfigurationProvider):
    def setupScript(self):
        return """
        device init

        ciena pdh interface de3
        pdh serline mode 1 ec1
        sdh map vc3.1 c3

        pw create cep 1 basic
        pw circuit bind 1 vc3.1
        pw ethport 1 1
        pw psn 1 mpls
        pw enable 1
        """
    def setupWithoutEthScript(self):
        return """
        device init

        ciena pdh interface de3
        pdh serline mode 1 ec1
        sdh map vc3.1 c3

        pw create cep 1 basic
        pw circuit bind 1 vc3.1
        """
    @staticmethod
    def channelForAisForcing():
        return "au3.1"


class PdhCardVc1xCepConfigurationProvider(SdhPathCepConfigurationProvider):
    def setupScript(self):
        return """
        device init

        ciena pdh interface de3
        pdh serline mode 1 ec1
        sdh map vc3.1 c3

        sdh map vc3.1 7xtug2s
        sdh map tug2.1.1 tu11
        sdh map vc1x.1.1.1 c1x

        pw create cep 1 basic
        pw circuit bind 1 vc1x.1.1.1
        pw ethport 1 1
        pw psn 1 mpls
        pw enable 1
        """
    def setupWithoutEthScript(self):
        return """
        device init

        ciena pdh interface de3
        pdh serline mode 1 ec1
        sdh map vc3.1 c3

        sdh map vc3.1 7xtug2s
        sdh map tug2.1.1 tu11
        sdh map vc1x.1.1.1 c1x

        pw create cep 1 basic
        pw circuit bind 1 vc1x.1.1.1
        """
    @staticmethod
    def channelForAisForcing():
        return "tu1x.1.1.1"


class PdhCardDs1SatopConfigurationProvider(PdhSatopConfigurationProvider):
    def setupScript(self):
        return """
        device init

        ciena pdh interface de3
        pdh serline mode 1 ec1
        sdh map vc3.1 c3

        sdh map vc3.1 7xtug2s
        sdh map tug2.1.1 tu11
        sdh map vc1x.1.1.1 c1x
        sdh map vc1x.1.1.1 de1

        pw create satop 1
        pw circuit bind 1 de1.1.1.1
        pw ethport 1 1
        pw psn 1 mpls
        pw enable 1
        """
    def setupWithoutEthScript(self):
        return """
        device init

        ciena pdh interface de3
        pdh serline mode 1 ec1
        sdh map vc3.1 c3

        sdh map vc3.1 7xtug2s
        sdh map tug2.1.1 tu11
        sdh map vc1x.1.1.1 c1x
        sdh map vc1x.1.1.1 de1

        pw create satop 1
        pw circuit bind 1 de1.1.1.1
        """
    @staticmethod
    def channelForAisForcing():
        return "1.1.1"

    @staticmethod
    def channelType():
        return "de1"


class PdhCardDe3SatopConfigurationProvider(PdhSatopConfigurationProvider):
    def setupScript(self):
        return """
        device init

        ciena pdh interface de3
        pdh serline mode 1 ec1
        sdh map vc3.1 de3

        pw create satop 1
        pw circuit bind 1 de3.1
        pw ethport 1 1
        pw psn 1 mpls
        pw enable 1
        """
    def setupWithoutEthScript(self):
        return """
        device init

        ciena pdh interface de3
        pdh serline mode 1 ec1
        sdh map vc3.1 de3

        pw create satop 1
        pw circuit bind 1 de3.1
        """

    @staticmethod
    def channelForAisForcing():
        return "1"

    @staticmethod
    def channelType():
        return "de3"


class CircuitAisForceWhenDisablePw(AnnaTestCase):
    _provider = None

    @classmethod
    def setProvider(cls, provider):
        cls._provider = provider

    @classmethod
    def getProvider(cls):
        """
        :rtype: ConfigurationProvider
        """
        return cls._provider

    @classmethod
    def createProviders(cls):
        if cls.isMroProduct():
            return ConfigurationProvider.mroAllProviders(cls.productCode())

        if cls.isPdhProduct():
            return ConfigurationProvider.pdhAllProviders(cls.productCode())

        return list()

    @classmethod
    def runWithRunner(cls, runner):
        for provider in cls.createProviders():
            cls.setProvider(provider)
            runner.run(cls)

    @classmethod
    def setUpClass(cls):
        cls.assertClassCliSuccess(cls.getProvider().setupScript())

    def setUp(self):
        provider = self.getProvider()
        if provider.hasXc():
            provider.connect()
        provider.forceAis(forced=False)
        provider.enablePw(enabled=True)
        provider.bindEth()

    def testAisForcingMustBasicallyWork(self):
        provider = self.getProvider()
        for forced in [True, False, True]:
            provider.forceAis(forced)
            provider.assertAisForced(forced)

    def testAisForcedWhenDisablePw(self):
        provider = self.getProvider()
        provider.disablePw()
        provider.assertAisForced(forced=True)

        provider.enablePw()
        provider.assertAisForced(forced=False)

    def testAisForcedWhenNoEthPort(self):
        provider = self.getProvider()
        provider.unbindEth()
        provider.assertAisForced(forced=True)

        provider.bindEth()
        provider.assertAisForced(forced=False)

    def testIfAisIsExplicitlyForced_DoNotOverrideIt(self):
        provider = self.getProvider()
        provider.forceAis(forced=True)
        provider.assertAisForced(forced=True)

        provider.disablePw()
        provider.assertAisForced(forced=True)
        provider.enablePw()
        provider.assertAisForced(forced=True)

        provider.unbindEth()
        provider.assertAisForced(forced=True)
        provider.bindEth()
        provider.assertAisForced(forced=True)

    def testApplicationForceAisAfterDisablingPw(self):
        provider = self.getProvider()

        # Disable PW, AIS will be forced
        provider.disablePw()
        provider.assertAisForced(forced=True)

        # Explicitly force AIS on circuit
        provider.forceAis(forced=True)
        provider.assertAisForced(forced=True)

        # When re-enabling PW, AIS forcing must still be there
        provider.enablePw()
        provider.assertAisForced(forced=True)

    def testApplicationForceAisAfterUnbindEthPort(self):
        provider = self.getProvider()

        # Unbind PW from ETH, AIS will be forced
        provider.unbindEth()
        provider.assertAisForced(forced=True)

        # Explicitly force AIS on circuit
        provider.forceAis(forced=True)
        provider.assertAisForced(forced=True)

        # When re-binding ETH, AIS forcing must still be there
        provider.bindEth()
        provider.assertAisForced(forced=True)

    def testAisWillBeForcedAfterCrossConnectAsWell(self):
        provider = self.getProvider()

        if not provider.hasXc():
            self.skipTest("This will be only testable on MRO where cross-connect exist")

        provider.disconnect()
        provider.assertAisForced(forced=False)
        provider.disablePw()
        provider.assertAisForced(forced=False)
        provider.connect()
        provider.assertAisForced(forced=True)

        provider.disconnect()
        provider.assertAisForced(forced=False)
        provider.connect()
        provider.assertAisForced(forced=True)


class CircuitAisForceWhenFirstBindDisabledPw(AnnaTestCase):
    _provider = None

    @classmethod
    def setProvider(cls, provider):
        cls._provider = provider

    @classmethod
    def getProvider(cls):
        """
        :rtype: ConfigurationProvider
        """
        return cls._provider

    @classmethod
    def createProviders(cls):
        if cls.isMroProduct():
            return ConfigurationProvider.mroAllProviders(cls.productCode())

        if cls.isPdhProduct():
            return ConfigurationProvider.pdhAllProviders(cls.productCode())

        return list()

    @classmethod
    def runWithRunner(cls, runner):
        for provider in cls.createProviders():
            cls.setProvider(provider)
            runner.run(cls)

    @classmethod
    def setUpClass(cls):
        cls.assertClassCliSuccess(cls.getProvider().setupWithoutEthScript())

    def setUp(self):
        provider = self.getProvider()
        if provider.hasXc():
            provider.connect()

    def testAisForcedWhenDisablePw(self):
        provider = self.getProvider()
        provider.assertAisForced(forced=True)

class MroXcBridge(AnnaMroTestCase):
    @classmethod
    def setUpClass(cls):
        script = """
        device init

        xc vc connect vc3.1.1.1 vc3.25.1.1 two-way
        xc vc connect vc3.25.1.1 vc3.1.1.2

        pw create cep 1 basic
        pw circuit bind 1 vc3.25.1.1
        pw ethport 1 1
        pw psn 1 mpls
        pw enable 1
        """
        cls.assertClassCliSuccess(script)

    def setUp(self):
        self.assertCliSuccess("xc vc connect vc3.1.1.1 vc3.25.1.1 two-way")
        self.assertCliSuccess("xc vc connect vc3.25.1.1 vc3.1.1.2")
        SdhPathAisController.forceAis("au3.1.1.1,1.1.2", forced=False)
        self.enablePw()

    @staticmethod
    def enablePw():
        PwController.enablePw(cliId="1", enabled=True)

    @staticmethod
    def disablePw():
        PwController.enablePw(cliId="1", enabled=False)

    def testAisNeedToBeForcedOnAllDestWhenPwIsDisabled(self):
        self.disablePw()
        SdhPathAisController.assertAisForced("au3.1.1.1", forced=True)
        SdhPathAisController.assertAisForced("au3.1.1.2", forced=True)

        self.enablePw()
        SdhPathAisController.assertAisForced("au3.1.1.1", forced=False)
        SdhPathAisController.assertAisForced("au3.1.1.2", forced=False)

    def testAisNeedToBeForcedOnNewDestWhenPwIsDisabled(self):
        self.disablePw()
        HoXcController.connect(source="vc3.25.1.1", dest="vc3.1.1.3", connected=True, twoWays=False)
        SdhPathAisController.assertAisForced("au3.1.1.3", forced=True)

    def testAisIsUnforcedWhenDisconnect(self):
        self.disablePw()
        HoXcController.connect(source="vc3.25.1.1", dest="vc3.1.1.2", connected=False, twoWays=False)
        SdhPathAisController.assertAisForced("au3.1.1.2", forced=False)

    def testIfAisIsExplicitlyForced_DoNotOverrideIt(self):
        SdhPathAisController.forceAis(cliId="au3.1.1.1,1.1.2", forced=True)

        self.disablePw()
        SdhPathAisController.assertAisForced("au3.1.1.1", forced=True)
        SdhPathAisController.assertAisForced("au3.1.1.2", forced=True)

        self.enablePw()
        SdhPathAisController.assertAisForced("au3.1.1.1", forced=True)
        SdhPathAisController.assertAisForced("au3.1.1.2", forced=True)

def TestMain():
    runner = AtTestCaseRunner.runner()
    CircuitAisForceWhenFirstBindDisabledPw.runWithRunner(runner)
    CircuitAisForceWhenDisablePw.runWithRunner(runner)
    runner.run(MroXcBridge)
    runner.summary()

if __name__ == '__main__':
    TestMain()

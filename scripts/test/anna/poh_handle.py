from test.AtTestCase import AtTestCaseRunner
from AnnaTestCase import AnnaMroTestCase

class AbstractAlarmDownstream(AnnaMroTestCase):
    @staticmethod
    def mustDownstreamAlarm():
        return ["AIS", "LOP"]
    
    def checkAlarmAffect(self, path, downstreamAlarms):
        result = self.runCli("show sdh path alarm affect %s" % path)
        
        for columnId in range(1, result.numColumns()):
            columnName = result.columnName(columnId)
            if columnName in downstreamAlarms:
                self.assertEqual(result.cellContent(0, columnName), "en", None)
            else:
                self.assertEqual(result.cellContent(0, columnName), "dis", None)
    
class AlarmDownstreamDefault(AbstractAlarmDownstream):
    def testWhenPohSideSetToFaceplate_OnlyAisLopCanBeDownstream(self):
        self.runCli("device init")
        self.runCli("debug ciena mro poh side faceplate")
        self.checkAlarmAffect("vc3.17.1.1", self.mustDownstreamAlarm())
        
    def testWhenPohSideSetToMate_MostAlarmsCanBeDownstream(self):
        self.runCli("device init")
        self.runCli("debug ciena mro poh side mate")
        self.checkAlarmAffect("vc3.17.1.1", ["AIS", "LOP", "UNEQ", "PLM"])
    
class AlarmDownstream(AbstractAlarmDownstream):
    @classmethod
    def setUpClass(cls):
        cls.classRunCli("device init")
        cls.classRunCli("debug ciena mro poh side mate")
        cls.classRunCli("pw create cep 1 basic")
        cls.classRunCli("pw ethport 1 1")
        cls.classRunCli("pw psn 1 mpls")
    
    def tearDown(self):
        self.runCli("pw circuit bind 1 none")
        self.runCli("xc vc disconnect vc3.25.1.1 vc3.17.1.1 two-way")
    
    @staticmethod
    def defaultDownstreamAlarms():
        return ["AIS", "LOP", "UNEQ", "PLM"]
    
    def testPohAlarmDownstreamMustBeDisabledWhenCrossConnectToVcWhichIsBoundToCep(self):
        self.runCli("pw circuit bind 1 vc3.25.1.1")
        self.runCli("xc vc connect vc3.25.1.1 vc3.17.1.1 two-way")
        self.checkAlarmAffect("vc3.17.1.1", self.mustDownstreamAlarm())
        
    def testPohAlarmDownstreamMustBeRevertedAfterDisconnecting(self):
        self.testPohAlarmDownstreamMustBeDisabledWhenCrossConnectToVcWhichIsBoundToCep()
        self.runCli("xc vc disconnect vc3.25.1.1 vc3.17.1.1 two-way")
        self.checkAlarmAffect("vc3.17.1.1", self.defaultDownstreamAlarms())
        
    def testPohAlarmDownstreamMustBeDisabledWhenTerVcIsBoundToCepAndXcWasConnected(self):
        self.runCli("xc vc connect vc3.25.1.1 vc3.17.1.1 two-way")
        self.checkAlarmAffect("vc3.17.1.1", self.defaultDownstreamAlarms())
        self.runCli("pw circuit bind 1 vc3.25.1.1")
        self.checkAlarmAffect("vc3.17.1.1", self.mustDownstreamAlarm())
    
    def testPohAlarmDownstreamMustBeRevertedAfterUnbindPw(self):
        self.testPohAlarmDownstreamMustBeDisabledWhenCrossConnectToVcWhichIsBoundToCep()
        self.runCli("pw circuit bind 1 none")
        self.checkAlarmAffect("vc3.17.1.1", self.defaultDownstreamAlarms())
    
class AbstractPohInsertion(AnnaMroTestCase):
    @classmethod
    def pohSide(cls):
        return "invalid"

    @classmethod
    def setUpClass(cls):
        cls.classRunCli("device init")
        cls.classRunCli("debug ciena mro poh side %s" % cls.pohSide())
        cls.classRunCli("pw create cep 1 basic")
        cls.classRunCli("pw ethport 1 1")
        cls.classRunCli("pw psn 1 mpls")

    def tearDown(self):
        self.runCli("pw circuit bind 1 none")
        self.runCli("xc vc disconnect vc3.25.1.1 %s two-way" % self.path())

    def pohInsertionIsEnabled(self, path):
        result = self.runCli("show sdh path poh %s" % path)
        if result.cellContent(0, "insertion") == "en":
            return True
        return False
    
    @staticmethod
    def path():
        return "invalid"

class PohInsertionOnPassThroughConnection(AbstractPohInsertion):
    @classmethod
    def setUpClass(cls):
        cls.classRunCli("device init")

    def tearDown(self):
        self.runCli("xc vc disconnect %s %s two-way" % (self.path(), self.partnerPath()))

    @staticmethod
    def path():
        return "vc3.1.1.1"

    @staticmethod
    def partnerPath():
        return "vc3.17.1.1"

    def testPohInsertionMustBeDisabledWhenXcConnected(self):
        self.runCli("xc vc connect %s %s two-way" % (self.path(), self.partnerPath()))
        self.assertFalse(self.pohInsertionIsEnabled(self.path()))
        self.assertFalse(self.pohInsertionIsEnabled(self.partnerPath()))

    def testPohInsertionMustBeEnabledWhenXcDisconnected(self):
        self.runCli("xc vc disconnect %s %s two-way" % (self.path(), self.partnerPath()))
        self.assertTrue(self.pohInsertionIsEnabled(self.path()))
        self.assertTrue(self.pohInsertionIsEnabled(self.partnerPath()))

class PohInsertionOnXc(AbstractPohInsertion):
    def testPohInsertionMustBeEnabledAsDefault(self):
        self.assertTrue(self.pohInsertionIsEnabled(self.path()), None)

    def testPohInsertionMustBeDisableWhenConnectTerminatedVcBoundToCep(self):
        self.runCli("pw circuit bind 1 vc3.25.1.1")
        self.assertTrue(self.pohInsertionIsEnabled(self.path()), None)
        self.runCli("xc vc connect vc3.25.1.1 %s two-way" % self.path())
        self.assertFalse(self.pohInsertionIsEnabled(self.path()), None)

    def testPohInsertionMustBeEnableWhenDisconnectTerminatedVcBoundToCep(self):
        self.testPohInsertionMustBeDisableWhenConnectTerminatedVcBoundToCep()
        self.runCli("xc vc disconnect vc3.25.1.1 %s two-way" % self.path())
        self.assertTrue(self.pohInsertionIsEnabled(self.path()), None)

    def testPohInsertionMustBeDisableWhenTerVcIsBoundToCepAndConnectionWasMadeBefore(self):
        self.runCli("xc vc connect vc3.25.1.1 %s two-way" % self.path())
        self.assertTrue(self.pohInsertionIsEnabled(self.path()), None)
        self.runCli("pw circuit bind 1 vc3.25.1.1")
        self.assertFalse(self.pohInsertionIsEnabled(self.path()), None)

    def testPohInsertionMustBeEnabledWhenTerVcIsUnboundToCepAndConnectionWasMadeBefore(self):
        self.testPohInsertionMustBeDisableWhenTerVcIsBoundToCepAndConnectionWasMadeBefore()
        self.runCli("pw circuit bind 1 none")
        self.assertTrue(self.pohInsertionIsEnabled(self.path()), None)

class PohInsertionOnBert(AbstractPohInsertion):
    @classmethod
    def setUpClass(cls):
        super(PohInsertionOnBert, cls).setUpClass()
        cls.classRunCli("pw circuit bind 1 vc3.25.1.1")
        cls.classRunCli("xc vc connect vc3.25.1.1 %s two-way" % cls.path())
    
    def tearDown(self):
        self.runCli("prbs engine delete 1")
    
    def testPohInsertionMustBeEnabledWhenBertIsEnabled(self):
        self.runCli("prbs engine create vc 1 vc3.25.1.1")
        self.assertFalse(self.pohInsertionIsEnabled(self.path()), None)
        self.runCli("prbs engine enable 1")
        self.assertTrue(self.pohInsertionIsEnabled(self.path()), None)
        
    def testPohInsertionMustBeDisabledWhenBertIsDisabled(self):
        self.runCli("prbs engine create vc 1 vc3.25.1.1")
        self.assertFalse(self.pohInsertionIsEnabled(self.path()), None)
        self.runCli("prbs engine disable 1")
        self.assertFalse(self.pohInsertionIsEnabled(self.path()), None)
        
    def testPohInsertionMustBeDisabledWhenBertIsDeleted(self):
        self.runCli("prbs engine create vc 1 vc3.25.1.1")
        self.assertFalse(self.pohInsertionIsEnabled(self.path()), None)
        self.runCli("prbs engine delete 1")
        self.assertFalse(self.pohInsertionIsEnabled(self.path()), None)

class MatePohInsertionOnXc(PohInsertionOnXc):
    @staticmethod
    def path():
        return "vc3.17.1.1"

    @classmethod
    def pohSide(cls):
        return "mate"

class MatePohInsertionOnBert(PohInsertionOnBert):
    @staticmethod
    def path():
        return "vc3.17.1.1"

    @classmethod
    def pohSide(cls):
        return "mate"

class FaceplatePohInsertionOnXc(PohInsertionOnXc):
    @staticmethod
    def path():
        return "vc3.1.1.1"

    @classmethod
    def pohSide(cls):
        return "faceplate"

class FaceplatePohInsertionOnBert(PohInsertionOnBert):
    @staticmethod
    def path():
        return "vc3.1.1.1"

    @classmethod
    def pohSide(cls):
        return "faceplate"

class PohInsertOnBertAndPw(AbstractPohInsertion):
    @classmethod
    def setUpClass(cls):
        script = """
        device init
        xc vc connect vc3.1.1.1 vc3.25.1.1 two-way
        pw create cep 1 basic
        pw circuit bind 1 vc3.25.1.1
        pw ethport 1 1
        pw psn 1 mpls
        pw enable 1
        prbs engine create vc 1 vc3.25.1.1
        prbs engine enable 1
        """
        cls.assertClassCliSuccess(script)

    def assertPohInsertionEnabled(self, enabled):
        self.assertEqual(self.pohInsertionIsEnabled("vc3.1.1.1"), enabled)

    def setUp(self):
        self.assertPohInsertionEnabled(enabled=True)
    
    def tearDown(self):
        pass
    
    def testPohInsertMustBeEnabledWhenReconnectingWithBertProvisioned(self):
        clis = """
        xc vc disconnect vc3.1.1.1 vc3.25.1.1 two-way
        xc vc connect vc3.1.1.1 vc3.25.1.1 two-way
        """
        self.assertCliSuccess(clis)
        self.assertPohInsertionEnabled(enabled=True)

class Tu3PohOnMapping(AbstractPohInsertion):
    @classmethod
    def setUpClass(cls):
        pass
    
    def commonSetUp(self):
        script = """
        device init
        sdh map aug1.25.1 vc4
        sdh map vc4.25.1 3xtug3s
        sdh map tug3.25.1.1-25.1.3 vc3
        """
        self.assertCliSuccess(script)

    def assertPohInsertion(self, path, enabled):
        self.assertEqual(self.pohInsertionIsEnabled(path), enabled) 

    def assertPohMonitor(self, path, enabled):
        self.assertEqual(self.pohMonitoringIsEnabled(path), enabled) 

    def testTu3PohInsertionEnabled(self):
        self.commonSetUp()
        self.assertPohInsertion("vc3.25.1.2", True)
        self.assertPohInsertion("vc3.25.1.2", True)
        self.assertPohInsertion("vc3.25.1.3", True)

    def testTu3PohMonitoringEnabled(self):
        self.commonSetUp()
        self.assertPohMonitor("vc3.25.1.1", True)
        self.assertPohMonitor("vc3.25.1.2", True)
        self.assertPohMonitor("vc3.25.1.3", True)

    def pohMonitoringIsEnabled(self, path):
        result = self.runCli("show sdh path poh %s" % path)
        if result.cellContent(0, "monitor") == "en":
            return True
        return False

    def testTu3PohInsertionDisabledWhenBindToCep(self):
        script = """
        device init
        sdh map aug1.25.1 vc4
        sdh map vc4.25.1 3xtug3s
        sdh map tug3.25.1.1-25.1.3 vc3
        sdh map vc3.25.1.1-25.1.3 c3
        pw create cep 1-3 basic
        pw ethport 1-3 1
        pw circuit bind 1-3 vc3.25.1.1-25.1.3
        """
        self.assertCliSuccess(script)
        self.assertPohInsertion("vc3.25.1.1", False)
        self.assertPohInsertion("vc3.25.1.2", False)
        self.assertPohInsertion("vc3.25.1.3", False)

    def testTu3PohInsertionWhenPrbsEnabled(self):
        script = """
        device init
        sdh map aug1.25.1 vc4
        sdh map vc4.25.1 3xtug3s
        sdh map tug3.25.1.1-25.1.3 vc3
        sdh map vc3.25.1.1-25.1.3 c3
        pw create cep 1-3 basic
        pw ethport 1-3 1
        pw circuit bind 1-3 vc3.25.1.1-25.1.3
        prbs engine create vc 1-3 vc3.25.1.1-25.1.3
        prbs engine enable 1-3
        """
        self.assertCliSuccess(script)
        self.assertPohInsertion("vc3.25.1.1", True)
        self.assertPohInsertion("vc3.25.1.2", True)
        self.assertPohInsertion("vc3.25.1.3", True)

        script = """
        prbs engine disable 1-3
        """
        self.assertCliSuccess(script)
        self.assertPohInsertion("vc3.25.1.1", False)
        self.assertPohInsertion("vc3.25.1.2", False)
        self.assertPohInsertion("vc3.25.1.3", False)

    def testTu3PohInsertionEnabledWhenUnbindCep(self):
        script = """
        device init
        sdh map aug1.25.1 vc4
        sdh map vc4.25.1 3xtug3s
        sdh map tug3.25.1.1-25.1.3 vc3
        sdh map vc3.25.1.1-25.1.3 c3
        pw create cep 1-3 basic
        pw ethport 1-3 1
        pw circuit bind 1-3 vc3.25.1.1-25.1.3
        pw circuit unbind 1-3
        """
        self.assertCliSuccess(script)
        self.assertPohInsertion("vc3.25.1.1", True)
        self.assertPohInsertion("vc3.25.1.2", True)
        self.assertPohInsertion("vc3.25.1.2", True)

    def setUp(self):
        pass
    
    def tearDown(self):
        pass
    

class ApsSelectorPohHandling(AnnaMroTestCase):
    def assertPohInsertionEnabled(self, pathCliId, enabled):
        result = self.runCli("show sdh path poh %s" % pathCliId)
        actual = True if result.cellContent(0, "insertion") == "en" else False
        self.assertEqual(enabled, actual)

    def testProvisionSelectorAfterMapping_TerminatedWorkingMateProtection(self):
        script = """
        device init
        sdh path poh insertion disable vc3.1.1.1
        sdh map vc3.25.1.1 7xtug2s
        aps selector create 1 vc3.25.1.1 vc3.17.1.1 vc3.1.1.1
        """
        self.runCli(script)
        self.assertPohInsertionEnabled("vc3.1.1.1", enabled=True)

    def testProvisionSelectorAfterMapping_MateWorkingTerminatedProtection(self):
        script = """
        device init
        sdh path poh insertion disable vc3.1.1.1
        sdh map vc3.25.1.1 7xtug2s
        aps selector create 1 vc3.17.1.1 vc3.25.1.1 vc3.1.1.1
        """
        self.runCli(script)
        self.assertPohInsertionEnabled("vc3.1.1.1", enabled=True)

    def testPohInsertionDisableWhenBindPw(self):
        script = """
        device init

        aps selector create 1 vc3.25.1.1 vc3.17.1.1 vc3.1.1.1
        
        # Enable POH insertion and expect it will be disabled when PW binding happen
        sdh path poh insertion enable vc3.1.1.1
        
        # Provision CEP, POH insertion must be disabled automatically        
        pw create cep 1 basic
        pw ethport 1 1
        pw psn 1 mpls
        pw circuit bind 1 vc3.25.1.1
        """
        self.runCli(script)
        self.assertPohInsertionEnabled("vc3.1.1.1", enabled=False)

    def testChangeMappingToTugAfterProvisionSelector(self):
        script = """
        device init

        aps selector create 1 vc3.25.1.1 vc3.17.1.1 vc3.1.1.1
        sdh path poh insertion disable vc3.1.1.1
        sdh map vc3.25.1.1 7xtug2s
        """
        self.runCli(script)
        self.assertPohInsertionEnabled("vc3.1.1.1", enabled=True)

    def testChangeMappingAndBindPwAfterProvisioningSelector(self):
        script = """
        device init

        aps selector create 1 vc3.25.1.1 vc3.17.1.1 vc3.1.1.1
        sdh path poh insertion disable vc3.1.1.1
        sdh map vc3.25.1.1 7xtug2s
        
        # Let assume that there is issue in software and POH insertion is still not enabled after change 
        # to TUG-2, this issue is coverred by of of test case in this script. Now just force POH insertion 
        # enabled
        sdh map vc3.25.1.1 c3
        sdh path poh insertion enable vc3.1.1.1
        
        # Provision CEP, POH insertion must be back to disabled state
        pw create cep 1 basic
        pw ethport 1 1
        pw psn 1 mpls
        pw circuit bind 1 vc3.25.1.1
        """
        self.runCli(script)
        self.assertPohInsertionEnabled("vc3.1.1.1", enabled=False)

    def testProvisionSelectorAfterBindingPw(self):
        script = """
        device init

        sdh path poh insertion enable vc3.1.1.1
        pw create cep 1 basic
        pw ethport 1 1
        pw psn 1 mpls
        pw circuit bind 1 vc3.25.1.1
        
        aps selector create 1 vc3.25.1.1 vc3.17.1.1 vc3.1.1.1
        """
        self.runCli(script)
        self.assertPohInsertionEnabled("vc3.1.1.1", enabled=False)

    def testChangeMappingAfterProvisionPw(self):
        script = """
        device init

        sdh path poh insertion enable vc3.1.1.1
        pw create cep 1 basic
        pw ethport 1 1
        pw psn 1 mpls
        pw circuit bind 1 vc3.25.1.1
        
        aps selector create 1 vc3.25.1.1 vc3.17.1.1 vc3.1.1.1
        
        sdh path poh insertion disable vc3.1.1.1
        pw circuit unbind 1
        sdh map vc3.25.1.1 7xtug2s
        """
        self.runCli(script)
        self.assertPohInsertionEnabled("vc3.1.1.1", enabled=True)

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(AlarmDownstreamDefault)
    runner.run(AlarmDownstream)
    runner.run(MatePohInsertionOnXc)
    runner.run(MatePohInsertionOnBert)
    runner.run(FaceplatePohInsertionOnXc)
    runner.run(FaceplatePohInsertionOnBert)
    runner.run(PohInsertOnBertAndPw)
    runner.run(PohInsertionOnPassThroughConnection)
    runner.run(ApsSelectorPohHandling)
    runner.run(Tu3PohOnMapping)
    return runner.summary()
    
if __name__ == '__main__':
    TestMain()

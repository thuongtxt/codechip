import time
from random import randint

from python.arrive.AtAppManager.AtAppManager import AtAppManager
from python.arrive.AtAppManager.AtColor import AtColor
from python.arrive.atsdk.channel import AtChannel
from python.arrive.atsdk.cli import AtCliRunnable
from python.arrive.atsdk.device import AtDevice
from python.arrive.atsdk.aps import AtApsEngine, AtUpsrEngine
from python.arrive.atsdk.prbs import AtPrbsStableChecker
from python.arrive.atsdk.sdh import AtTti
from test.AtTestCase import AtTestCase, AtTestCaseRunner
from test.anna.AnnaTestCase import AnnaProductCodeProvider


class ConfigurationProvider(AtCliRunnable):
    def __init__(self):
        AtCliRunnable.__init__(self)
        self.workingPath = None
        self.protectionPath = None
        self.addedPath = None
        self.dropPaths = None
        self.upsrEngineId = self._anyUpsrEngine()
        self.engine = None
        self.device = AtDevice.currentDevice()
        self.simulated = self.device.simulated
        self.bertEngines = None
    
    def setupNormalDatapath(self):
        assert 0
    
    def apsModule(self):
        return self.device.apsModule()
    
    def sdhModule(self):
        return self.device.sdhModule()
    
    def prbsModule(self):
        return self.device.prbsModule()
    
    def pdhModule(self):
        return self.device.pdhModule()
    
    def createUpsr(self):
        self.engine = self.apsModule().createUpsrEngine(self.upsrEngineId, self.workingPath, self.protectionPath)
        return self.engine
    
    def buildInfo(self):
        assert 0
    
    def setupBert(self):
        pass
    
    def clearTraffic(self):
        for engine in self.bertEngines:
            engine.clearStatus()
    
    def setupTraffic(self):
        self.runCli("device show readwrite dis dis")
        self.setupNormalDatapath()
        self.buildInfo()
        self.setupBert()
        self.makeLoopback()
        self.createUpsr()
    
    def sleep(self, seconds):
        if not self.simulated:
            time.sleep(seconds)
    
    @staticmethod
    def _anyUpsrEngine(maxNumEngine = 384):
        return randint(1, maxNumEngine)
    
    def _makeLoopbackForLine(self, line):
        tti = AtTti(AtTti.MODE_16Byte, "line_%s" % line.cliId(), AtTti.PADDING_NULL)
        line.txTti = tti
        line.expectedTti = tti
        
        for loopbackMode in [AtChannel.LOOPBACK_MODE_RELEASE, AtChannel.LOOPBACK_MODE_LOCAL]:
            line.loopback = loopbackMode
                
            self.sleep(1)
            rxTti = line.rxTti
            if rxTti.message == tti.message and line.noAlarm():
                return loopbackMode
            
        if self.simulated:
            return AtChannel.LOOPBACK_MODE_LOCAL
            
        return None
    
    def workingLine(self):
        return self.workingPath.line()
    
    def protectionLine(self):
        return self.protectionPath.line()
    
    def makeLoopback(self):
        assert(self._makeLoopbackForLine(self.workingLine()) is not None)
        assert(self._makeLoopbackForLine(self.protectionLine()) is not None)
    
    def trafficGood(self):
        for engine in self.bertEngines:
            if not engine.good() and not self.simulated:
                return False
            
        return True
    
    @staticmethod
    def disruptionCheckingIsBuggy():
        return True
    
    def checkServiceDisruption(self):
        if self.disruptionCheckingIsBuggy():
            return
        
        good = True
        
        for engine in self.bertEngines:
            disruptionTimeMs = engine.disruptionTimeInUsCalculate() / 1000
            if disruptionTimeMs > 50:
                AtColor.printColor(AtColor.RED, "Engine %s disruption time: %d (ms)\n" % (engine.cliId(), disruptionTimeMs))
                good = False
            
        assert good
    
    def checkNoDisruption(self):
        good = True
        
        for engine in self.bertEngines:
            if not engine.good():
                return False

        assert good
    
class FaceplateStm4ConfigurationProvider(ConfigurationProvider):
    def setupNormalDatapath(self):
        script = """
        device init
        
        serdes mode 1-8 stm4
        sdh line rate 1-8 stm4
        
        sdh map vc3.25.1.1 7xtug2s
        sdh map tug2.25.1.1.1 tu11
        sdh map vc1x.25.1.1.1.1 de1
        
        # Add/drop traffic
        xc vc connect vc3.1.1.1  vc3.25.1.1 two-way
        
        # Bridge traffic
        xc vc connect vc3.25.1.1 vc3.2.1.1
        """
        self.runCli(script)
        
    def vc3FromCliId(self, pathCliId):
        tokens = pathCliId.split(".")
        lineId = int(tokens[1], 10) - 1
        aug1Id = int(tokens[2], 10) - 1
        au3Id  = int(tokens[3], 10) - 1
        line = self.sdhModule().getLine(lineId)
        return line.getVc3(aug1Id, au3Id)
        
    def setupBert(self):
        prbsModule = self.prbsModule()
        engineId = 0
        
        AtCliRunnable.silent()
        vc1x = self.sdhModule().getChannel("vc1x.25.1.1.1.1")
        de1 = vc1x.mapChannel()
        AtCliRunnable.silentRestore()
        
        bertEngine = prbsModule.createPrbsEngine(engineId, de1)
        self.bertEngines = [bertEngine]
        
    def buildInfo(self):
        self.silent()
        self.workingPath = self.vc3FromCliId("vc3.1.1.1")
        self.protectionPath = self.vc3FromCliId("vc3.2.1.1")
        self.addedPath = self.vc3FromCliId("vc3.25.1.1")
        self.dropPaths = [self.vc3FromCliId("vc3.25.1.1")]
        AtCliRunnable.silentRestore()
    
class AtUpsrEngineAbstractTestCase(AtTestCase):
    confProvider = None 
    ignoreTrafficForVeryQuickCheckStateMachine = False

    @classmethod
    def canRun(cls):
        return AnnaProductCodeProvider.isMroProduct(cls.productCode())

    @classmethod
    def isSimulated(cls):
        return cls.confProvider.simulated
    
    @classmethod
    def sleep(cls, seconds):
        if not cls.isSimulated():
            time.sleep(seconds)
    
    @classmethod
    def setUpClass(cls):
        cls.enableLogger()
        cls.confProvider.setupTraffic()

    def engine(self):
        return self.confProvider.engine
    
    def workingPath(self):
        return self.confProvider.workingPath
    
    def protectionPath(self):
        return self.confProvider.protectionPath
    
    def activePath(self):
        return self.engine().activePath
        
    def switchingReason(self):
        return self.engine().switchingReason
    
    def currentState(self):
        return self.switchingReason()
    
    def clearTraffic(self):
        self.confProvider.clearTraffic()
    
    def trafficGood(self):
        return self.confProvider.trafficGood()
    
    def checkNoDisruption(self):
        return self.confProvider.checkNoDisruption()
    
    def checkCurrentRequest(self, reason):
        timeoutMs = 100
        startTime = time.time()

        while True:
            actualReason = self.switchingReason()
            self.assertIsNotNone(actualReason, None)
            if actualReason == reason:
                break
            
            elapseTimeMs = (time.time() - startTime) * 1000
            if elapseTimeMs >= timeoutMs:
                break
            
            if self.isSimulated():
                break
        
        if not self.isSimulated():
            self.assertEqual(actualReason, reason, None)
        
    def checkPathActive(self, expectedActivePath, reason = None):
        timeoutMs = 100
        startTime = time.time()
        elapseTimeMs = 0
        
        while True:
            activePathCliId = self.activePath().cliId()
            self.assertIsNotNone(activePathCliId, None)
            if activePathCliId == expectedActivePath.cliId():
                break
            
            elapseTimeMs = (time.time() - startTime) * 1000
            if elapseTimeMs >= timeoutMs:
                break
            
            if self.isSimulated():
                break
        
        if not self.isSimulated():
            self.assertEqual(activePathCliId, expectedActivePath.cliId(), "Traffic must be on %s, it is now on %s" % (expectedActivePath.cliId(), activePathCliId))
            
        self.assertTrue(elapseTimeMs <= 50, 
                        "It takes %d (ms) when waiting for switching to %s" % (elapseTimeMs, expectedActivePath.cliId()))
            
        if reason is not None:
            actualReason = self.switchingReason()
            self.assertIsNotNone(actualReason, None)
            if not self.isSimulated():
                self.assertEqual(actualReason, reason, None) 
              
    def checkDisruption(self):
        self.confProvider.checkServiceDisruption()

    def checkWorkingPathActive(self, reason = None):
        self.checkPathActive(self.workingPath(), reason)
    
    def checkProtectionPathActive(self, reason = None):
        self.checkPathActive(self.protectionPath(), reason)
    
    def unforceAllAlarms(self):
        self.forceSd(self.workingPath(), forced = False)
        self.forceSd(self.protectionPath(), forced = False)
        self.forceSf(self.workingPath(), forced = False)
        self.forceSf(self.protectionPath(), forced = False)
    
    def waitForTrafficGood(self):
        if self.ignoreTrafficForVeryQuickCheckStateMachine:
            return
        
        checker = AtPrbsStableChecker(self.confProvider.bertEngines, timeout = 20)
        checker.simulated = self.isSimulated()
        checker.quickCheck = True
        assert(checker.check())
    
    @staticmethod
    def getAu(path):
        return path.parent
    
    def _forceTxAlarm(self, path, alarmType, forced):
        cliId = "%s.%s" % (path.cliType(), path.cliId())
        self.runCli("sdh path force alarm %s %s tx %s" % (cliId, alarmType, "en" if forced else "dis"))
    
    def forceSf(self, path, forced = True):
        self._forceTxAlarm(self.getAu(path), "ais", forced)
    
    @staticmethod
    def _uneqForceIsBuggy():
        # TODO: 
        return True
    
    def _forceUneq(self, path, forced, _):
        if self._uneqForceIsBuggy():
            path.txPsl = 0x0 if forced else 0x2
        else:
            self._forceTxAlarm(path, "uneq", forced)
    
    def forceSd(self, path, forced = True, durationUs = 0):
        self._forceUneq(path, forced, durationUs)
    
    def useUpsrEngine(self):
        return False
    
    def setUp(self):
        self.unforceAllAlarms()
        engine = self.engine()
        self.workingPath().holdOnTimer = 10
        self.protectionPath().holdOnTimer = 10
        
        if self.useUpsrEngine():
            if engine.state == AtApsEngine.STATE_STOP:
                engine.start()
        
            engine.externalCommand = engine.EXTERNAL_COMMAND_CLEAR
            engine.externalCommand = engine.EXTERNAL_COMMAND_FS2WRK
            if not self.isSimulated():
                self.checkPathActive(self.workingPath())
            engine.externalCommand = engine.EXTERNAL_COMMAND_CLEAR
            engine.switchingType = engine.SWITCHING_TYPE_NON_REVERTIVE
            
            # For easy forcing
            engine.setSwitchingCondition([engine.DEFECT_AIS, engine.DEFECT_LOP], engine.SWITCHING_CONDITION_SF)
            engine.setSwitchingCondition([engine.DEFECT_UNEQ], engine.SWITCHING_CONDITION_SD)
        
        self.assertTrue(self.workingPath().noAlarm(), None)
        self.assertTrue(self.protectionPath().noAlarm(), None)
        self.waitForTrafficGood()
    
class AtUpsrEngineTestCase(AtUpsrEngineAbstractTestCase):
    def lockout(self):
        self.engine().externalCommand = AtUpsrEngine.EXTERNAL_COMMAND_LP
    
    def lockoutActive(self):
        self.assertEqual(self.engine().externalCommand, AtUpsrEngine.EXTERNAL_COMMAND_LP, None)
    
    def clearCommand(self):
        self.engine().externalCommand = AtUpsrEngine.EXTERNAL_COMMAND_CLEAR
    
    def noActiveCommand(self):
        if not self.isSimulated():
            self.assertEqual(self.engine().externalCommand, AtUpsrEngine.EXTERNAL_COMMAND_CLEAR, None)
    
    def fs2prt(self):
        self.engine().externalCommand = AtUpsrEngine.EXTERNAL_COMMAND_FS2PRT
        
    def fs2prtActive(self):
        self.assertEqual(self.engine().externalCommand, AtUpsrEngine.EXTERNAL_COMMAND_FS2PRT, None)
        
    def fs2wrk(self):
        self.engine().externalCommand = AtUpsrEngine.EXTERNAL_COMMAND_FS2WRK
        
    def fs2wrkActive(self):
        self.assertEqual(self.engine().externalCommand, AtUpsrEngine.EXTERNAL_COMMAND_FS2WRK, None) 
        
    def ms2prt(self):
        self.engine().externalCommand = AtUpsrEngine.EXTERNAL_COMMAND_MS2PRT
        
    def ms2prtActive(self):
        self.assertEqual(self.engine().externalCommand, AtUpsrEngine.EXTERNAL_COMMAND_MS2PRT, None)
        
    def ms2wrk(self):
        self.engine().externalCommand = AtUpsrEngine.EXTERNAL_COMMAND_MS2WRK
        
    def ms2wrkActive(self):
        self.assertEqual(self.engine().externalCommandm, AtUpsrEngine.EXTERNAL_COMMAND_MS2WRK, None)
    
    def forceWorkingSd(self):
        self.forceSd(self.workingPath())
        
    def unforceWorkingSd(self):
        self.forceSd(self.workingPath(), forced = False)
        
    def forceProtectionSd(self):
        self.forceSd(self.protectionPath())
        
    def unforceProtectionSd(self):
        self.forceSd(self.protectionPath(), forced = False)
        
    def forceWorkingSf(self):
        self.forceSf(self.workingPath())
        
    def unforceWorkingSf(self):
        self.forceSf(self.workingPath(), forced = False)
        
    def forceProtectionSf(self):
        self.forceSf(self.protectionPath())
        
    def unforceProtectionSf(self):
        self.forceSf(self.protectionPath(), forced = False)
        
    def useUpsrEngine(self):
        return True
    
    def executeFail(self, aFunction):
        self.disableLogger()
        self.assertRaises(Exception, aFunction)
        self.enableLogger()
    
class DisruptionMeasureTestCase(AtUpsrEngineAbstractTestCase):
    def useUpsrEngine(self):
        return False
    
    def failtestDisruptionDetectMustWork(self):
        workingPath = self.workingPath()
        
        for forcingTimeMs in [20, 60]:
            self.forceSf(workingPath)
            for engine in self.confProvider.bertEngines:
                disruptionTime = engine.disruptionTimeInUsCalculate(waitTillSeeError=True) / 1000
                maxDisruptionTime = forcingTimeMs + 5
                message = "Force alarm within %s (ms), but disruption time: %d (ms)" % (forcingTimeMs, disruptionTime)
                within = True if forcingTimeMs <= disruptionTime <= maxDisruptionTime else False
                self.assertTrue(within, message)

class AutomaticallyTestCase(AtUpsrEngineTestCase):
    def TestSdSfOnWorkingPath(self, forceFunction, reason):
        forceFunction(self.workingPath())
        self.checkProtectionPathActive(reason)
        self.checkDisruption()
        self.trafficGood()
    
    def TestSdSfOnProtectionPath(self, forceFunction, reason):
        forceFunction(self.workingPath())
        self.checkProtectionPathActive(reason)
        self.checkDisruption()
        
        forceFunction(self.workingPath(), forced = False)
        self.checkNoDisruption()
        forceFunction(self.protectionPath())
        self.checkWorkingPathActive(reason)
        self.checkDisruption()
    
    def testSdOnWorkingPath(self):
        self.TestSdSfOnWorkingPath(self.forceSd, AtUpsrEngine.SWITCHING_REASON_SD)

    def testSfOnWorkingPath(self):
        self.TestSdSfOnWorkingPath(self.forceSf, AtUpsrEngine.SWITCHING_REASON_SF)
        
    def testSdOnProtectionPath(self):
        self.TestSdSfOnProtectionPath(self.forceSd, AtUpsrEngine.SWITCHING_REASON_SD)

    def testSfOnProtectionPath(self):
        self.TestSdSfOnProtectionPath(self.forceSf, AtUpsrEngine.SWITCHING_REASON_SF)

class ExternalCommandTestCase(AtUpsrEngineTestCase):
    def TestCommandSwitchFromWorkingToProtection(self, command, reason):
        self.engine().externalCommand = command
        self.checkPathActive(self.protectionPath(), reason)
        self.checkDisruption()
    
    def TestCommandSwitchFromProtectionToWorking(self, commandToPrt, commandToWrk, reason):
        self.engine().externalCommand = commandToPrt
        self.checkPathActive(self.protectionPath(), reason)
        self.checkDisruption()
        self.engine().externalCommand = AtUpsrEngine.EXTERNAL_COMMAND_CLEAR
        
        self.engine().externalCommand = commandToWrk
        self.checkPathActive(self.workingPath(), reason)
        self.checkDisruption()
    
    def testLockoutOfProtection(self):
        self.forceSf(self.workingPath())
        self.checkPathActive(self.protectionPath())
        self.checkDisruption()
        self.engine().externalCommand = AtUpsrEngine.EXTERNAL_COMMAND_LP
        self.checkPathActive(self.workingPath(), AtUpsrEngine.SWITCHING_REASON_LP)
        self.checkDisruption()
    
    def testForceSwitchFromWorkingToProtection(self):
        self.TestCommandSwitchFromWorkingToProtection(AtUpsrEngine.EXTERNAL_COMMAND_FS2PRT, 
                                                      AtUpsrEngine.SWITCHING_REASON_FS)

    def testForceSwitchFromProtectionToWorking(self):
        self.TestCommandSwitchFromProtectionToWorking(AtUpsrEngine.EXTERNAL_COMMAND_FS2PRT, 
                                                      AtUpsrEngine.EXTERNAL_COMMAND_FS2WRK, 
                                                      AtUpsrEngine.SWITCHING_REASON_FS)
        

    def testManualSwitchFromWorkingToProtection(self):
        self.TestCommandSwitchFromWorkingToProtection(AtUpsrEngine.EXTERNAL_COMMAND_MS2PRT, 
                                                      AtUpsrEngine.SWITCHING_REASON_MS)

    def testManualSwitchFromProtectionToWorking(self):
        self.TestCommandSwitchFromProtectionToWorking(AtUpsrEngine.EXTERNAL_COMMAND_MS2PRT, 
                                                      AtUpsrEngine.EXTERNAL_COMMAND_MS2WRK, 
                                                      AtUpsrEngine.SWITCHING_REASON_MS)
        
class LockoutOfProtectionTestCase(AtUpsrEngineTestCase):
    def checkSwitchBack(self):
        self.checkPathActive(self.workingPath(), AtUpsrEngine.SWITCHING_REASON_LP)
        
    def TestMakeSwitchToProtectionThenIssueLP_TrafficMustSwitchBackToWorking(self, requestFunction):
        requestFunction()
        self.lockout()
        self.checkSwitchBack()
        self.lockoutActive()    
        
    def TestIssueCommandOnWorking_ThenIssueLP_TrafficIsOnWorkingAndCurrentExternalCommandMustBeLP(self, commandFunction):
        commandFunction()
        self.lockout()
        self.checkWorkingPathActive()
        self.lockoutActive()
        
    def TestLPIsActive_TriggerRequest_SwitchingMustNotBeTriggeredAndWorkingPathIsStillSelected(self, requestFunction):
        self.lockout()
        requestFunction()
        self.checkWorkingPathActive()
        self.lockoutActive()    
    
    def TestCommand2PrtMustNotBeRetained(self, issueFunction):
        issueFunction()
        self.lockout()
        self.clearCommand()
        self.checkWorkingPathActive()
        self.noActiveCommand()
        
    def testMakeSDOnWorkingThenIssueLP_TrafficMustSwitchBackToWorking(self):
        self.TestMakeSwitchToProtectionThenIssueLP_TrafficMustSwitchBackToWorking(self.forceWorkingSd)
    
    def testMakeSFOnWorkingThenIssueLP_TrafficMustSwitchBackToWorking(self):
        self.TestMakeSwitchToProtectionThenIssueLP_TrafficMustSwitchBackToWorking(self.forceWorkingSf)
    
    def testIssueFS2PRT_ThenIssueLP_TrafficMustSwitchBackToWorking(self):
        self.TestMakeSwitchToProtectionThenIssueLP_TrafficMustSwitchBackToWorking(self.fs2prt)
    
    def testIssueMS2PRT_ThenIssueLP_TrafficMustSwitchBackToWorking(self):
        self.TestMakeSwitchToProtectionThenIssueLP_TrafficMustSwitchBackToWorking(self.ms2prt)
        
    def testIssueFS2WRK_ThenIssueLP_TrafficIsOnWorkingAndCurrentExternalCommandMustBeLP(self):
        self.TestIssueCommandOnWorking_ThenIssueLP_TrafficIsOnWorkingAndCurrentExternalCommandMustBeLP(self.fs2wrk)
    
    def testIssueMS2WRK_ThenIssueLP_TrafficIsOnWorkingAndCurrentExternalCommandMustBeLP(self):
        self.TestIssueCommandOnWorking_ThenIssueLP_TrafficIsOnWorkingAndCurrentExternalCommandMustBeLP(self.ms2wrk)
    
    def testLPIsActive_TriggerSDCondition_SwitchingMustNotBeTriggeredAndWorkingPathIsStillSelected(self):
        self.TestLPIsActive_TriggerRequest_SwitchingMustNotBeTriggeredAndWorkingPathIsStillSelected(self.forceWorkingSd)
        
    def testLPIsActive_TriggerSFCondition_SwitchingMustNotBeTriggeredAndWorkingPathIsStillSelected(self):
        self.TestLPIsActive_TriggerRequest_SwitchingMustNotBeTriggeredAndWorkingPathIsStillSelected(self.forceWorkingSf)
        
    def testLPIsActive_ForceSwitchFromWorkingToProtection_SwitchingMustNotBeTriggeredAndWorkingPathIsStillSelected(self):
        def requestFunction():
            self.executeFail(self.fs2prt)
        self.TestLPIsActive_TriggerRequest_SwitchingMustNotBeTriggeredAndWorkingPathIsStillSelected(requestFunction)
        
    def testLPIsActive_ManualSwitchFromWorkingToProtection_SwitchingMustNotBeTriggeredAndWorkingPathIsStillSelected(self):
        def requestFunction():
            self.executeFail(self.ms2prt)
        self.TestLPIsActive_TriggerRequest_SwitchingMustNotBeTriggeredAndWorkingPathIsStillSelected(requestFunction)
    
    def testFS2PRTMustNotBeRetained(self):
        self.TestCommand2PrtMustNotBeRetained(self.fs2prt)
        
    def testFS2WRKMustNotBeRetained(self):
        self.fs2wrk()
        self.lockout()
        self.clearCommand()
        self.forceWorkingSf()
        self.checkProtectionPathActive(AtUpsrEngine.SWITCHING_REASON_SF)
        self.noActiveCommand()
        
    def testMS2PRTMustNotBeRetained(self):
        self.TestCommand2PrtMustNotBeRetained(self.ms2prt)
    
    def testMS2WRKMustNotBeRetained(self):
        self.ms2wrk()
        self.lockout()
        self.clearCommand()
        self.noActiveCommand()
    
class ForceSwitchTestCase(AtUpsrEngineTestCase):
    def TestMakeAlarmOnWorkingThenIssueFS2WRK_TrafficMustSwitchBackToWorking(self, forceFunction, alarmReason):
        forceFunction()
        self.checkPathActive(self.protectionPath(), alarmReason)
        self.fs2wrk()
        self.checkPathActive(self.workingPath(), AtUpsrEngine.SWITCHING_REASON_FS)
    
    def TestMakeSwitchToWorkingThenIssueFS2PRT_TrafficMustSwitchToProtection(self, issueFunction):
        issueFunction()
        self.checkWorkingPathActive()
        self.fs2prt()
        self.checkProtectionPathActive(AtUpsrEngine.SWITCHING_REASON_FS)
    
    def TestFS2PRTIsActive_TriggerAlarmOnProtection_ProtectionPathIsStillSelected(self, alarmTriggerFunction):
        self.fs2prt()
        self.checkProtectionPathActive(AtUpsrEngine.SWITCHING_REASON_FS)
        alarmTriggerFunction()
        self.checkProtectionPathActive()
        self.fs2prtActive()
        
    def TestFS2WRKIsActive_TriggerAlarmOnWorking_WorkingPathIsStillSelected(self, alarmTriggerFunction):
        self.fs2wrk()
        self.checkWorkingPathActive()
        alarmTriggerFunction()
        self.checkWorkingPathActive()
        
    def testFS2WRKCannotPreemptFS2PRT(self):
        self.fs2prt()
        self.executeFail(self.fs2wrk)
        self.fs2prtActive()
    
    def testFS2PRTCannotPreemptFS2WRK(self):
        self.fs2wrk()
        self.executeFail(self.fs2prt)
        self.fs2wrkActive()
    
    def testMakeSDOnWorkingThenIssueFS2WRK_TrafficMustSwitchBackToWorking(self):
        self.TestMakeAlarmOnWorkingThenIssueFS2WRK_TrafficMustSwitchBackToWorking(self.forceWorkingSd, AtUpsrEngine.SWITCHING_REASON_SD)
    
    def testMakeSFOnWorkingThenIssueFS2WRK_TrafficMustSwitchBackToWorking(self):
        self.TestMakeAlarmOnWorkingThenIssueFS2WRK_TrafficMustSwitchBackToWorking(self.forceWorkingSf, AtUpsrEngine.SWITCHING_REASON_SF)
        
    def testMakeSDOnProtectionThenIssueFS2PRT_TrafficMustSwitchToProtection(self):
        self.TestMakeSwitchToWorkingThenIssueFS2PRT_TrafficMustSwitchToProtection(self.forceProtectionSd)
        
    def testMakeSFOnProtectionThenIssueFS2PRT_TrafficMustSwitchToProtection(self):
        self.TestMakeSwitchToWorkingThenIssueFS2PRT_TrafficMustSwitchToProtection(self.forceProtectionSf)
     
    def testMS2PRTThenIssueFS2WRK_TrafficMustSwitchBackToWorking(self):
        self.ms2prt()
        self.checkProtectionPathActive(AtUpsrEngine.SWITCHING_REASON_MS)
        self.fs2wrk()
        self.checkWorkingPathActive(AtUpsrEngine.SWITCHING_REASON_FS)
        
    def testMS2WRKThenIssueFS2PRT_TrafficMustSwitchToProtection(self):
        self.ms2wrk()
        self.fs2prt()
        self.checkProtectionPathActive(AtUpsrEngine.SWITCHING_REASON_FS)

    def testFS2PRTIsActive_TriggerSDOnProtection_ProtectionPathIsStillSelected(self):
        self.TestFS2PRTIsActive_TriggerAlarmOnProtection_ProtectionPathIsStillSelected(self.forceProtectionSd)
    
    def testFS2PRTIsActive_TriggerSFOnProtection_ProtectionPathIsStillSelected(self):
        self.TestFS2PRTIsActive_TriggerAlarmOnProtection_ProtectionPathIsStillSelected(self.forceProtectionSf)
    
    def testFS2PRTIsActive_IssueMS2WRKOrMS2PRTWillBeRejected(self):
        self.fs2prt()
        self.checkProtectionPathActive(AtUpsrEngine.SWITCHING_REASON_FS)
        self.executeFail(self.ms2wrk)
        self.executeFail(self.ms2prt)
    
    def testFS2WRKIsActive_TriggerSDOnWorking_WorkingPathIsStillSelected(self):
        self.TestFS2WRKIsActive_TriggerAlarmOnWorking_WorkingPathIsStillSelected(self.forceWorkingSd)
        
    def testFS2WRKIsActive_TriggerSFOnWorking_WorkingPathIsStillSelected(self):
        self.TestFS2WRKIsActive_TriggerAlarmOnWorking_WorkingPathIsStillSelected(self.forceWorkingSf)
    
    def testFS2WRKIsActive_IssueMS2PRTOrMS2WRKWillBeRejected(self):
        self.fs2wrk()
        self.checkWorkingPathActive()
        self.executeFail(self.ms2prt)
        self.executeFail(self.ms2wrk)
        self.checkWorkingPathActive()
    
    def testMS2PRTMustNotBeRetained(self):
        self.ms2prt()
        self.checkProtectionPathActive(AtUpsrEngine.SWITCHING_REASON_MS)
        self.fs2wrk()
        self.checkWorkingPathActive(AtUpsrEngine.SWITCHING_REASON_FS)
        self.clearCommand()
        self.checkWorkingPathActive()
        self.noActiveCommand()
        
    def testMS2WRKMustNotBeRetained(self):
        self.ms2wrk()
        self.checkWorkingPathActive()
        self.fs2prt()
        self.checkProtectionPathActive(AtUpsrEngine.SWITCHING_REASON_FS)
        self.clearCommand()
        self.noActiveCommand()

class SfTestCase(AtUpsrEngineTestCase):
    def testTriggerSFOnWorkingThenSFOnProtection_ProtectionPathIsStillSelected(self):
        self.forceWorkingSf()
        self.checkProtectionPathActive(AtUpsrEngine.SWITCHING_REASON_SF)
        self.forceProtectionSf()
        self.checkProtectionPathActive()
    
    def testTriggerSFProtectionThenSFOnWorking_WorkingPathIsStillSelected(self):
        self.forceProtectionSf()
        self.checkWorkingPathActive()
        self.forceWorkingSf()
        self.checkWorkingPathActive()

    def testMakeSDOnWorkingThenTriggerSFOnProtection_TrafficMustSwitchBackToWorking(self):
        self.forceWorkingSd()
        self.checkProtectionPathActive(AtUpsrEngine.SWITCHING_REASON_SD)
        self.forceProtectionSf()
        self.checkWorkingPathActive(AtUpsrEngine.SWITCHING_REASON_SF)

    def testMakeSDOnProtectionThenTriggerSFOnWorking_TrafficMustSwitchToProtection(self):
        self.forceProtectionSd()
        self.checkWorkingPathActive()
        self.forceWorkingSf()
        self.checkProtectionPathActive(AtUpsrEngine.SWITCHING_REASON_SF)

    def testMS2PRTThenTriggerSFOnProtection_TrafficMustSwitchBackToWorking(self):
        self.ms2prt()
        self.checkProtectionPathActive(AtUpsrEngine.SWITCHING_REASON_MS)
        self.forceProtectionSf()
        self.checkWorkingPathActive(AtUpsrEngine.SWITCHING_REASON_SF)

    def testMS2WRKThenTriggerSFOnWorking_TrafficMustSwitchToProtection(self):
        self.ms2wrk()
        self.checkWorkingPathActive()
        self.forceWorkingSf()
        self.checkProtectionPathActive(AtUpsrEngine.SWITCHING_REASON_SF)

    def testTriggerSFOnWorking_TriggerSDOnProtection_ProtectionPathIsStillSelected(self):
        self.forceWorkingSf()
        self.checkProtectionPathActive(AtUpsrEngine.SWITCHING_REASON_SF)
        self.forceProtectionSd()
        self.checkProtectionPathActive()
        
    def testTriggerSFOnWorking_IssueMS2WRKOrMS2PRTWillBeRejected(self):
        self.forceWorkingSf()
        self.checkWorkingPathActive(AtUpsrEngine.SWITCHING_REASON_SF)
        if not self.isSimulated():
            self.executeFail(self.ms2wrk)
            self.executeFail(self.ms2prt)
        
    def testTriggerSFOnProtection_TriggerSDOnWorking_WorkingPathIsStillSelected(self):
        self.forceProtectionSf()
        self.checkWorkingPathActive()
        self.forceWorkingSd()
        self.checkWorkingPathActive()
        
    def testTriggerSFOnProtection_IssueMS2PRTOrMS2WRKWillBeRejected(self):
        self.forceProtectionSf()
        self.checkCurrentRequest(AtUpsrEngine.SWITCHING_REASON_SF)
        if not self.isSimulated():
            self.executeFail(self.ms2wrk)
            self.executeFail(self.ms2prt)
    
    def testMS2PRTMustNotBeRetained(self):
        self.ms2prt()
        self.checkProtectionPathActive(AtUpsrEngine.SWITCHING_REASON_MS)
        self.forceProtectionSf()
        self.checkWorkingPathActive(AtUpsrEngine.SWITCHING_REASON_SF)
        self.noActiveCommand()
        self.unforceProtectionSf()
        self.checkWorkingPathActive()
        self.noActiveCommand()
    
    def testMS2WRKMustNotBeRetained(self):
        self.ms2wrk()
        self.checkWorkingPathActive()
        self.forceWorkingSf()
        self.checkProtectionPathActive(AtUpsrEngine.SWITCHING_REASON_SF)
        self.noActiveCommand()
        self.unforceWorkingSf()
        self.checkProtectionPathActive()
        self.noActiveCommand()
        
class SdTestCase(AtUpsrEngineTestCase):
    def testTriggerSDOnWorkingThenSDOnProtection_ProtectionPathIsStillSelected(self):
        self.forceWorkingSd()
        self.checkProtectionPathActive(AtUpsrEngine.SWITCHING_REASON_SD)
        self.forceProtectionSd()
        self.checkProtectionPathActive()
    
    def testTriggerSDProtectionThenSDOnWorking_WorkingPathIsStillSelected(self):
        self.forceProtectionSd()
        self.checkWorkingPathActive()
        self.forceWorkingSd()
        self.checkWorkingPathActive()
        
    def testMS2PRTThenTriggerSDOnProtection_TrafficMustSwitchBackToWorking(self):
        self.ms2prt()
        self.checkProtectionPathActive(AtUpsrEngine.SWITCHING_REASON_MS)
        self.forceProtectionSd()
        self.checkWorkingPathActive(AtUpsrEngine.SWITCHING_REASON_SD)
    
    def testMS2WRKThenTriggerSDOnWorking_TrafficMustSwitchToProtection(self):
        self.ms2wrk()
        self.checkWorkingPathActive()
        self.forceWorkingSd()
        self.checkProtectionPathActive(AtUpsrEngine.SWITCHING_REASON_SD)
        
    def testTriggerSDOnWorking_IssueMS2WRKOrMS2PRTWillBeRejected(self):
        self.forceWorkingSd()
        self.checkProtectionPathActive(AtUpsrEngine.SWITCHING_REASON_SD)
        if not self.isSimulated():
            self.executeFail(self.ms2wrk)
            self.executeFail(self.ms2prt)
        
    def testTriggerSDOnProtection_IssueMS2PRTOrMS2WRKWillBeRejected(self):
        self.forceProtectionSd()
        self.checkCurrentRequest(AtUpsrEngine.SWITCHING_REASON_SD)
        if not self.isSimulated():
            self.executeFail(self.ms2wrk)
            self.executeFail(self.ms2prt)
    
    def testMS2PRTMustNotBeRetained(self):
        self.ms2prt()
        self.checkProtectionPathActive(AtUpsrEngine.SWITCHING_REASON_MS)
        self.forceProtectionSd()
        self.checkWorkingPathActive(AtUpsrEngine.SWITCHING_REASON_SD)
        self.unforceProtectionSd()
        self.noActiveCommand()
        self.checkWorkingPathActive()
       
    def testMS2WRKMustNotBeRetained(self):
        self.ms2wrk()
        self.checkWorkingPathActive()
        self.forceWorkingSd()
        self.checkProtectionPathActive(AtUpsrEngine.SWITCHING_REASON_SD)
        self.noActiveCommand()
        self.unforceWorkingSd()
        self.noActiveCommand()
        self.checkProtectionPathActive()
        
class MsTestCase(AtUpsrEngineTestCase):
    def testIssueMS2PRT_ThenIssueMS2WRKWillBeRejected(self):
        self.ms2prt()
        self.executeFail(self.ms2wrk)
        
    def testIssueMS2WRK_ThenIssueMS2PRTWillBeRejected(self):
        self.ms2wrk()
        self.executeFail(self.ms2prt)
    
class NonRevertiveTestCase(AtUpsrEngineTestCase):
    def testWTRMustNotBeTriggered(self):
        wtr = 30
        self.engine().wtr = wtr
        self.forceWorkingSf()
        self.checkProtectionPathActive(AtUpsrEngine.SWITCHING_REASON_SF)
        self.unforceWorkingSf()
        self.sleep(wtr + 2) # Just to make sure that engine will not switch back
        self.checkProtectionPathActive()  

class RevertiveTestCase(AtUpsrEngineTestCase):
    @classmethod
    def wtrTime(cls):
        return 120
    
    @classmethod
    def wtrOdd(cls):
        return 35
    
    def setUp(self):
        AtUpsrEngineTestCase.setUp(self)
        self.engine().switchingType = AtUpsrEngine.SWITCHING_TYPE_REVERTIVE
        self.engine().wtr = self.wtrTime()
    
    def triggerWTR(self):
        self.forceWorkingSf()
        self.checkProtectionPathActive(AtUpsrEngine.SWITCHING_REASON_SF)
        self.unforceWorkingSf()
        self.checkCurrentRequest(AtUpsrEngine.SWITCHING_REASON_WTR)
    
    def wtrSwitchBack(self):
        if self.isSimulated():
            return
        
        startTime = time.time()
        while True:
            currentTime = time.time()
            switchedBack = self.activePath().cliId() == self.workingPath().cliId()
            
            elapsedTime = currentTime - startTime
            
            def expired():
                if elapsedTime >= (self.wtrTime() - self.wtrOdd()):
                    return True
                return False
            
            if switchedBack:
                if expired():
                    break
                AtColor.printColor(AtColor.RED, "WTR expires after %d (s), really ???" % elapsedTime)
            
            self.assertFalse(switchedBack, None)
            self.assertEqual(self.currentState(), AtUpsrEngine.SWITCHING_REASON_WTR, None)
            
            self.sleep(1)
            if expired():
                break
        
        self.sleep(self.wtrOdd())
        self.checkWorkingPathActive()
        
    def TestWTRWillAlwaysBeRestartedWhenWorkingChangeStateSF_SDToClearCondition(self, forceFunction):
        self.triggerWTR()
        
        # Let it run for a while
        halfWtr = self.wtrTime() / 2
        self.sleep(halfWtr)
        
        # Terminate it by triggering SD/SF
        forceFunction(self.workingPath(), forced = True)
        self.sleep(0.5)
        forceFunction(self.workingPath(), forced = False)
        
        self.checkProtectionPathActive()
        self.wtrSwitchBack()
        
    def testWTRWillBeTriggeredWhenWorkingIsInClearCondition(self):
        self.triggerWTR()
        self.wtrSwitchBack()
        
    def testWTRWillAlwaysBeRestartedWhenWorkingChangeStateSFToClearCondition(self):
        self.TestWTRWillAlwaysBeRestartedWhenWorkingChangeStateSF_SDToClearCondition(self.forceSf)
        
    def testWTRWillAlwaysBeRestartedWhenWorkingChangeStateSDToClearCondition(self):
        self.TestWTRWillAlwaysBeRestartedWhenWorkingChangeStateSF_SDToClearCondition(self.forceSd)
    
    def testTriggerWTR_TriggerSDOnProtectionThenTrafficWillSwitchToWorking(self):
        self.triggerWTR()
        self.forceProtectionSd()
        self.checkWorkingPathActive(AtUpsrEngine.SWITCHING_REASON_SD)
    
    def testTriggerWTR_TriggerSFOnProtectionThenTrafficWillSwitchToWorking(self):
        self.triggerWTR()
        self.forceProtectionSf()
        self.checkWorkingPathActive(AtUpsrEngine.SWITCHING_REASON_SF)
    
    def testTriggerWTR_IssueLP_ThenTrafficWillSwitchToWorking(self):
        self.triggerWTR()
        self.lockout()
        self.checkWorkingPathActive(AtUpsrEngine.SWITCHING_REASON_LP)
        
    def testTriggerWTR_IssueMS2WRK_ThenTrafficWillSwitchToWorking(self):
        self.triggerWTR()
        self.ms2wrk()
        self.checkWorkingPathActive(AtUpsrEngine.SWITCHING_REASON_MS)
        
    def testTriggerWTR_IssueFS2WRK_ThenTrafficWillSwitchToWorking(self):
        self.triggerWTR()
        self.fs2wrk()
        self.checkWorkingPathActive(AtUpsrEngine.SWITCHING_REASON_FS)
    
    def testTriggerWTR_IssueMS2PRT_ThenIssueCLEAR_TrafficMustSwitchBackToWorkingImmediately(self):
        self.triggerWTR()
        self.ms2prt()
        self.clearCommand()
        self.checkWorkingPathActive()
    
    def testTriggerWTR_IssueFS2PRT_ThenIssueCLEAR_TrafficMustSwitchBackToWorkingImmediately(self):
        self.triggerWTR()
        self.fs2prt()
        self.clearCommand()
        self.checkWorkingPathActive()
    
    def testFS2PRT_ThenCLEAR_TrafficMustBeSwitchedBackToWorkingImmediately(self):
        self.fs2prt()
        self.clearCommand()
        self.checkWorkingPathActive()
    
    def testMSPRT_ThenCLEAR_TrafficMustBeSwitchedBackToWorkingImmediately(self):
        self.ms2prt()
        self.clearCommand()
        self.checkWorkingPathActive()

def TestMain():
    runner = AtTestCaseRunner.runner()
    
    provider = FaceplateStm4ConfigurationProvider()
    
    def run(test):
        productCode = AtAppManager.localApp().productCode()
        if productCode == 0x60290011:
            return
        
        test.confProvider = provider
        test.ignoreTrafficForVeryQuickCheckStateMachine = True # TODO
        runner.run(test)
    
    run(AutomaticallyTestCase)
    run(ExternalCommandTestCase)
    run(LockoutOfProtectionTestCase)
    run(ForceSwitchTestCase)
    run(SfTestCase)
    run(SdTestCase)
    run(MsTestCase)
    run(NonRevertiveTestCase)
    run(RevertiveTestCase)
    
    runner.summary()
    
if __name__ == '__main__':
    TestMain()

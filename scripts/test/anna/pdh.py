import random
from python.arrive.atsdk.platform import AtHalListener
from test.AtTestCase import AtTestCaseRunner
from test.anna.AnnaTestCase import AnnaMroTestCase
import python.arrive.atsdk.AtRegister as AtRegister

class Mro20GMdlPrmAbstractTest(AnnaMroTestCase, AtHalListener):
    _watchedAddresses = None
    _cliIdTokens = None
    _channels = None
    hwSlice = None
    hwSts = None
    hwVtg = None
    hwVt = None
    cliIdString = None

    @classmethod
    def canRun(cls):
        return cls.is20G()

    @classmethod
    def watchedAddresses(cls):
        """
        :rtype: dict
        """
        if cls._watchedAddresses is None:
            cls._watchedAddresses = dict()
        return cls._watchedAddresses

    def tearDown(self):
        self.stopWatching()

    @staticmethod
    def makeCliIdString(cliId):
        return ".".join(["%d" % value for value in cliId])

    @classmethod
    def watchAddress(cls, address):
        if address in cls.watchedAddresses():
            return
        cls.watchedAddresses()[address] = False

    @classmethod
    def watchAddressByNameAndOffset(cls, name, offset):
        reg = cls.getRegister(name, offset)
        cls.watchAddress(reg.address)

    def didAccessAddress(self, address):
        if address in self.watchedAddresses():
            self.watchedAddresses()[address] = True

    def didRead(self, address, value):
        self.didAccessAddress(address)

    def didWrite(self, address, value):
        self.didAccessAddress(address)

    def startWatching(self):
        AtHalListener.addListener(self)

    def stopWatching(self):
        AtHalListener.removeListener(self)
        self.watchedAddresses().clear()

    def assertAllAddressesCatched(self):
        for address, catched in self.watchedAddresses().iteritems():
            self.assertTrue(catched, "Address 0x%08x has not been accessed" % address)

    @classmethod
    def getRegister(cls, name, offset, readHw=False):
        return cls.defaultRegisterProvider().getRegister(name=name, addressOffset=offset, readHw=readHw)

    @classmethod
    def generateAnyChannel(cls):
        channel = random.choice(cls.channels())
        cls.channels().remove(channel)
        return channel

    @classmethod
    def buildAllChannelCliIds(cls):
        return list()

    @classmethod
    def channels(cls):
        """
        :rtype: list
        """
        if cls._channels is None:
            cls._channels = cls.buildAllChannelCliIds()
        return cls._channels

    @classmethod
    def pdhChannelType(cls):
        return "Invalid"

    @classmethod
    def detectHwId(cls, pdhChannelType, channelCliId):
        cliId = ".".join(["%d" % value for value in channelCliId])
        result = cls.runCli("debug show pdh channel hardware id %s.%s" % (pdhChannelType, cliId))
        idString = result.cellContent(0, "pdh")
        [hwSlice, hwSts] = [int(string) for string in idString.split(",")]
        return hwSlice, hwSts

    def setUp(self):
        self._cliIdTokens = self.generateAnyChannel()
        (self.hwSlice, self.hwSts) = self.detectHwId(self.pdhChannelType(), self._cliIdTokens)
        self.cliIdString = self.makeCliIdString(self._cliIdTokens)

    @classmethod
    def baseAddress(cls):
        return 0x0380000

    @classmethod
    def atregName(cls):
        return "Invalid"

    @classmethod
    def defaultRegisterProvider(cls):
        provider = cls.registerProvider(atregName=cls.atregName())
        provider.baseAddress = cls.baseAddress()
        return provider

class MdlTest(Mro20GMdlPrmAbstractTest):
    @classmethod
    def setUpClass(cls):
        script = """
        device show readwrite dis dis
        device init
        sdh map vc3.25.1.1-32.16.3 de3
        pdh de3 framing 25.1.1-32.16.3 ds3_cbit_unchannelize 
        """
        cls.assertClassCliSuccess(script)

    def setUp(self):
        super(MdlTest, self).setUp()
        self.enableAllMdl()

    @classmethod
    def buildAllChannelCliIds(cls):
        de3CliIds = []
        for line_i in range(8):
            lineId = line_i + 25
            for aug1_i in range(16):
                aug1Id = aug1_i + 1
                for au3_i in range(3):
                    au3Id = au3_i + 1
                    de3Id = (lineId, aug1Id, au3Id)
                    de3CliIds.append(de3Id)

        return de3CliIds

    @classmethod
    def pdhChannelType(cls):
        return "de3"

    @classmethod
    def atregName(cls):
        return "AF6CNC0022_RD_PDH_MDL"

    def sendMdl(self):
        for mdlType in self.mdlTypes():
            self.assertCliSuccess("pdh de3 mdl %s enable %s" % (mdlType, self.cliIdString))
            self.assertCliSuccess("pdh de3 mdl %s transmit %s" % (mdlType, self.cliIdString))

    def sendMdlCheckWatchedAddress(self):
        self.startWatching()
        self.sendMdl()
        self.stopWatching()
        self.assertAllAddressesCatched()

    def test_upen_mdl_buffer1(self):
        numDwordsToCare = 3

        regName = "upen_mdl_buffer1" if self.hwSts < 32 else "upen_mdl_buffer1_2"

        for dwordId in range(numDwordsToCare):
            offset = (self.hwSlice * 0x1000) + (dwordId * 32 + (self.hwSts % 32))
            self.watchAddressByNameAndOffset(regName, offset)

        self.sendMdlCheckWatchedAddress()

    def commonOffset(self):
        return (self.hwSlice * 0x1000) + self.hwSts

    def test_upen_sta_idle_alren(self):
        self.watchAddressByNameAndOffset("upen_sta_idle_alren", self.commonOffset())
        self.sendMdlCheckWatchedAddress()

    def enableMdl(self, mdlType, enabled=True):
        self.assertCliSuccess("pdh de3 mdl %s %s %s" % (mdlType, "enable" if enabled else "disable", self.cliIdString))

    def enableAllMdl(self):
        for mdlType in self.mdlTypes():
            self.enableMdl(mdlType)

    def TestEnable(self, mdlType, regName, offset=None):
        if offset is None:
            offset = self.commonOffset()

        self.watchAddressByNameAndOffset(regName, offset)
        self.startWatching()
        self.enableMdl(mdlType, enabled=False)
        self.enableMdl(mdlType, enabled=True)
        self.assertAllAddressesCatched()
        self.stopWatching()

    @staticmethod
    def mdlTypes():
        return ["idlesignal", "path", "testsignal"]

    def TestAllMdlEnable(self, regName, offset=None):
        for mdlType in self.mdlTypes():
            self.TestEnable(mdlType=mdlType, regName=regName, offset=offset)

    def test_upen_cfg_mdl(self):
        self.TestAllMdlEnable(regName="upen_cfg_mdl")

    def test_upen_rxmdl_cfgtype(self):
        offset = self.hwSts*4 + self.hwSlice
        self.TestAllMdlEnable(regName="upen_rxmdl_cfgtype", offset=offset)

    def TestTxByteCounter(self, mdlType, regName):
        for ro in [0, 1]:
            self.watchAddressByNameAndOffset(regName, self.commonOffset() + ro * 256)

        self.startWatching()
        for readMode in ["ro", "r2c"]:
            self.assertCliSuccess("show pdh de3 mdl %s counters %s %s" % (mdlType, self.cliIdString, readMode))

        self.assertAllAddressesCatched()

    def TestRxCounter(self, mdlType, regName, roFactor=2048):
        offset = self.hwSts * 8 + self.hwSlice

        for ro in [0, 1]:
            self.watchAddressByNameAndOffset(regName, offset + ro * roFactor)

        self.startWatching()
        for readMode in ["ro", "r2c"]:
            self.assertCliSuccess("show pdh de3 mdl %s counters %s %s" % (mdlType, self.cliIdString, readMode))

        self.assertAllAddressesCatched()

    def test_upen_txmdl_cntr2c_byteidle1(self):
        self.TestTxByteCounter(mdlType="idlesignal", regName="upen_txmdl_cntr2c_byteidle1")

    def test_upen_txmdl_cntr2c_bytepath(self):
        self.TestTxByteCounter(mdlType="path", regName="upen_txmdl_cntr2c_bytepath")

    def test_upen_txmdl_cntr2c_bytetest(self):
        self.TestTxByteCounter(mdlType="testsignal", regName="upen_txmdl_cntr2c_bytetest")

    def test_upen_txmdl_cntr2c_valididle(self):
        self.TestTxByteCounter(mdlType="idlesignal", regName="upen_txmdl_cntr2c_valididle")

    def test_upen_txmdl_cntr2c_validpath(self):
        self.TestTxByteCounter(mdlType="path", regName="upen_txmdl_cntr2c_validpath")

    def test_upen_txmdl_cntr2c_validtest(self):
        self.TestTxByteCounter(mdlType="testsignal", regName="upen_txmdl_cntr2c_validtest")

    def test_upen_rxmdl_cntr2c_byteidle(self):
        self.TestRxCounter(mdlType="idlesignal", regName="upen_rxmdl_cntr2c_byteidle")

    def test_upen_rxmdl_cntr2c_bytepath(self):
        self.TestRxCounter(mdlType="path", regName="upen_rxmdl_cntr2c_bytepath")

    def test_upen_rxmdl_cntr2c_bytetest(self):
        self.TestRxCounter(mdlType="testsignal", regName="upen_rxmdl_cntr2c_bytetest")

    def test_upen_rxmdl_cntr2c_goodidle(self):
        self.TestRxCounter(mdlType="idlesignal", regName="upen_rxmdl_cntr2c_goodidle", roFactor=4096)

    def test_upen_rxmdl_cntr2c_goodpath(self):
        self.TestRxCounter(mdlType="path", regName="upen_rxmdl_cntr2c_goodpath", roFactor=4096)

    def test_upen_rxmdl_cntr2c_goodtest(self):
        self.TestRxCounter(mdlType="testsignal", regName="upen_rxmdl_cntr2c_goodtest", roFactor=4096)

    def test_upen_rxmdl_cntr2c_dropidle(self):
        self.TestRxCounter(mdlType="idlesignal", regName="upen_rxmdl_cntr2c_dropidle", roFactor=4096)

    def test_upen_rxmdl_cntr2c_droppath(self):
        self.TestRxCounter(mdlType="path", regName="upen_rxmdl_cntr2c_droppath", roFactor=4096)

    def test_upen_rxmdl_cntr2c_droptest(self):
        self.TestRxCounter(mdlType="testsignal", regName="upen_rxmdl_cntr2c_droptest", roFactor=4096)

    def alarmOffset(self):
        return self.hwSts + (self.hwSlice * 64)

    def test_mdl_cfgen_int(self):
        offset = self.alarmOffset()
        self.watchAddressByNameAndOffset(name="mdl_cfgen_int", offset=offset)

        self.startWatching()
        self.assertCliSuccess("pdh de3 interruptmask %s mdl-change en" % self.cliIdString)
        self.assertAllAddressesCatched()

    def TestReceive(self, mdlType, regName):
        def offsetWithDword(dwordIndex):
            return self.hwSts * 256 + self.hwSlice * 32 + dwordIndex

        for dwordId in range(3):  # 3 dwords are enough to check address
            offset = offsetWithDword(dwordId)
            self.watchAddressByNameAndOffset(name=regName, offset=offset)

        def makeMessage():
            reg = self.getRegister(regName, offset=offsetWithDword(0))
            reg.value = 0
            reg.setField(AtRegister.cBit7_0, 0, 0x3C)  # Command
            reg.setField(AtRegister.cBit15_8, 8, 0x08) # ATT

            hwMdlType = {"path": 0x38,
                         "idlesignal": 0x34,
                         "testsignal": 0x32}
            reg.setField(AtRegister.cBit23_16, 16, hwMdlType[mdlType])

            reg.apply()

        makeMessage()
        self.startWatching()
        self.assertCliSuccess("pdh de3 mdl %s receive %s" % (mdlType, self.cliIdString))
        self.assertAllAddressesCatched()
        self.stopWatching()

    def test_upen_rxmdl_typebuff(self):
        for mdlType in self.mdlTypes():
            self.TestReceive(mdlType=mdlType, regName="upen_rxmdl_typebuff")

    def test_upen_destuff_ctrl0(self):
        self.watchAddressByNameAndOffset("upen_destuff_ctrl0", offset=0)
        self.startWatching()
        self.assertCliSuccess("pdh de1 prm cr compare enable")
        self.assertCliSuccess("pdh de1 prm cr compare disable")
        self.assertAllAddressesCatched()

    def test_upen_mdl_stk_cfg1(self):
        numTerminatedLines = 8
        mdlId = (self.hwSts * numTerminatedLines) + self.hwSlice
        offset = mdlId / 32 # 32 channels/reg

        self.watchAddressByNameAndOffset(name="upen_mdl_stk_cfg1", offset=offset)
        self.startWatching()
        for mdlType in self.mdlTypes():
            self.assertCliSuccess("pdh de3 mdl %s receive %s" % (mdlType, self.cliIdString))

        self.assertAllAddressesCatched()

    def test_mdl_int_sta(self):
        self.watchAddressByNameAndOffset(name="mdl_int_sta", offset=self.alarmOffset())
        self.startWatching()
        self.assertCliSuccess("show pdh de3 interrupt %s r2c" % self.cliIdString)
        self.assertAllAddressesCatched()

    def test_mdl_en_int(self):
        self.watchAddressByNameAndOffset(name="mdl_en_int", offset=0)
        self.startWatching()
        self.assertCliSuccess("pdh interrupt enable")
        self.assertAllAddressesCatched()

class PrmTest(Mro20GMdlPrmAbstractTest):
    @classmethod
    def setUpClass(cls):
        scripts = """
        device show readwrite dis dis
        device init
        sdh map vc3.25.1.1-32.16.3 7xtug2s
        sdh map tug2.25.1.1.1-32.16.3.7 tu11
        sdh map vc1x.25.1.1.1.1-28.16.3.7.4 de1
        sdh map vc1x.29.1.1.1.1-32.16.3.7.4 de1
        pdh de1 framing 25.1.1.1.1-28.16.3.7.4 ds1_esf
        pdh de1 framing 29.1.1.1.1-32.16.3.7.4 ds1_esf 
        """
        cls.assertClassCliSuccess(scripts)

    @classmethod
    def pdhChannelType(cls):
        return "de1"

    @classmethod
    def buildAllChannelCliIds(cls):
        de1CliIds = []
        for line_i in range(8):
            lineId = line_i + 25
            for aug1_i in range(16):
                aug1Id = aug1_i + 1
                for au3_i in range(3):
                    au3Id = au3_i + 1
                    for tug2_i in range(7):
                        tug2Id = tug2_i + 1
                        for tu_i in range(4):
                            tuId = tu_i + 1
                            de1Id = (lineId, aug1Id, au3Id, tug2Id, tuId)
                            de1CliIds.append(de1Id)

        return de1CliIds

    @classmethod
    def detectHwId(cls, pdhChannelType, channelCliId):
        result = Mro20GMdlPrmAbstractTest.detectHwId(pdhChannelType, channelCliId)
        cls.hwVtg = channelCliId[3] - 1
        cls.hwVt  = channelCliId[4] - 1
        return result

    @classmethod
    def atregName(cls):
        return "AF6CNC0022_RD_PDH_PRM"

    def commonOffset(self):
        return self.hwSts * 256 + self.hwVtg * 32 + self.hwVt * 8 + self.hwSlice

    def test_upen_prm_txcfgcr(self):
        self.startWatching()
        self.watchAddressByNameAndOffset("upen_prm_txcfgcr", offset=self.commonOffset())
        self.assertCliSuccess("pdh de1 prm enable %s" % self.cliIdString)
        self.assertAllAddressesCatched()

    def counterOffset(self, r2c):
        return self.commonOffset() + ((0 if r2c else 1)  * 16384)

    def TestCounter(self, regName):
        for r2c in [True, False]:
            self.startWatching()
            self.watchAddressByNameAndOffset(name=regName, offset=self.counterOffset(r2c=r2c))
            cli = "show pdh de1 debug prm counters %s %s" % (self.cliIdString, "r2c" if r2c else "ro")
            self.assertCliSuccess(cli)
            self.assertAllAddressesCatched()
            self.stopWatching()

    def test_upen_txprm_cnt_byte(self):
        return self.TestCounter(regName="upen_txprm_cnt_byte")

    def test_upen_txprm_cnt_pkt(self):
        return self.TestCounter(regName="upen_txprm_cnt_pkt")

    def test_upen_rxprm_gmess(self):
        return self.TestCounter(regName="upen_rxprm_gmess")

    def test_upen_rxprm_drmess(self):
        return self.TestCounter(regName="upen_rxprm_drmess")

    def test_upen_rxprm_mmess(self):
        return self.TestCounter(regName="upen_rxprm_mmess")

    def test_upen_rxprm_cnt_byte(self):
        return self.TestCounter(regName="upen_rxprm_cnt_byte")

    def test_upen_rxprm_cfgstd(self):
        self.startWatching()
        self.watchAddressByNameAndOffset("upen_rxprm_cfgstd", offset=self.commonOffset())
        self.assertCliSuccess("show pdh de1 prm %s" % self.cliIdString)
        self.assertAllAddressesCatched()

    def test_upen_destuff_ctrl0(self):
        self.startWatching()
        self.watchAddressByNameAndOffset("upen_destuff_ctrl0", offset=0)
        self.assertCliSuccess("pdh de1 prm cr compare enable")
        self.assertCliSuccess("pdh de1 prm cr compare disable")
        self.assertAllAddressesCatched()

    def interruptOffset(self):
        return self.hwSts * 32 + self.hwVtg * 4 + self.hwVt

    def test_prm_cfg_lben_int(self):
        self.startWatching()
        self.watchAddressByNameAndOffset("prm_cfg_lben_int", offset=self.interruptOffset())
        self.assertCliSuccess("pdh de1 interruptmask %s prm-lbbit-change en" % self.cliIdString)
        self.assertCliSuccess("pdh de1 interruptmask %s prm-lbbit-change dis" % self.cliIdString)
        self.assertAllAddressesCatched()

    def test_prm_lb_int_crrsta(self):
        self.startWatching()
        self.watchAddressByNameAndOffset("prm_lb_int_crrsta", offset=self.interruptOffset())
        self.assertCliSuccess("show pdh de1 prm %s" % self.cliIdString)
        self.assertAllAddressesCatched()

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(MdlTest)
    runner.run(PrmTest)
    runner.summary()

if __name__ == '__main__':
    TestMain()

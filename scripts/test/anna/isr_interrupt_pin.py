from test.AtTestCase import AtTestCaseRunner
from test.anna.AnnaTestCase import AnnaTestCase


class AbstractTest(AnnaTestCase):
    @staticmethod
    def numExpectedInterruptCores():
        return 1

    @staticmethod
    def boolFromString(string):
        """
        :type string: str
        """
        return True if string.lower() in ["en", "enable"] else False

    def testInterruptCoreMustBeEnough(self):
        result = self.runCli("show isr interrupt core")
        self.assertEqual(result.numRows(), self.numExpectedInterruptCores())

    def testEnableDisableInterruptCore(self):
        def TestCore(coreId):
            for enabled in [True, False, True]:
                token = "enable" if enabled else "disable"
                cli = "isr interrupt core %s %d" % (token, coreId + 1)
                self.assertCliSuccess(cli)

                result = self.runCli("show isr interrupt core")
                cellContent = result.cellContent(coreId, "enabled")
                self.assertEqual(enabled, self.boolFromString(cellContent))

        for core_i in range(self.numExpectedInterruptCores()):
            TestCore(core_i)

    def testWhenDeviceInterruptIsEnabled_AllCoresWillBeEnabledAsWell(self):
        for enabled in [True, False]:
            self.assertCliSuccess("device interrupt %s" % ("enable" if enabled else "disable"))
            result = self.runCli("show isr interrupt core")
            for row in range(result.numRows()):
                cellContent = result.cellContent(row, "enabled")
                self.assertEqual(self.boolFromString(cellContent), enabled)

class MroCardTest(AbstractTest):
    @classmethod
    def canRun(cls):
        return cls.isMroProduct()

class PdhCardTest(AbstractTest):
    @classmethod
    def canRun(cls):
        return cls.isPdhProduct()

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(MroCardTest)
    runner.run(PdhCardTest)
    runner.summary()

if __name__ == '__main__':
    TestMain()
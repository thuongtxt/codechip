from python.arrive.atsdk.cli import AtCliRunnable
from test.AtTestCase import AtTestCase, AtTestCaseRunner
from test.anna.AnnaTestCase import AnnaProductCodeProvider


class ConfigureProvider(AtCliRunnable):
    def __init__(self, productCode):
        self.productCode = productCode

    @classmethod
    def runCli(cls, cli):
        """
        :rtype: python.arrive.AtAppManager.AtCliResult.AtCliResult
        """
        result = AtCliRunnable.runCli(cli)
        assert result.success()
        return result

    @classmethod
    def setup(cls):
        pass

    def localLoopbackMode(self):
        return "local"

    def remoteLoopbackMode(self):
        return "remote"

    def remoteLoopbackSupported(self):
        return True

    def currentLoopback(self):
        return None

    def loopback(self, mode):
        assert 0

    def prbsEngineCreate(self):
        assert 0
        
    @staticmethod
    def mroProviders(productCode):
        return [VcDe1BertProvider(productCode),
                PtsMroVc1xBertProvider(productCode),
                PtsMroHoVcBertProvider(productCode),
                PtsMroLoVc3BertProvider(productCode),
                VcDe1NxDs0BertProvider(productCode)]

    @staticmethod
    def pdhProviders(productCode):
        return [PtsPdhDe1BertProvider(productCode),
                PtsPdhVc1xBertProvider(productCode),
                PtsPdhHoVcBertProvider(productCode),
                PtsPdhVcDe1NxDs0BertProvider(productCode)]

    @classmethod
    def providers(cls, productCode):
        if AnnaProductCodeProvider.isMroProduct(productCode):
            return cls.mroProviders(productCode)

        if AnnaProductCodeProvider.isPdhProduct(productCode):
            return cls.pdhProviders(productCode)

        return list()

    def canMakeLocalLoopback(self):
        return True

class PdhChannelConfigureProvider(ConfigureProvider):
    def localLoopbackMode(self):
        return "lineloopin"

    def remoteLoopbackMode(self):
        return "lineloopout"


class De1BertProvider(PdhChannelConfigureProvider):
    def de1CliId(self):
        return "Invalid"

    def currentLoopback(self):
        result = self.runCli("show pdh de1 %s" % self.de1CliId())
        return result.cellContent(0, "LoopbackMode")

    def loopback(self, mode):
        self.runCli("pdh de1 loopback %s %s" % (self.de1CliId(), mode))

class VcDe1BertProvider(De1BertProvider):
    @classmethod
    def vc3CliId(cls):
        return "25.1.1"

    def de1CliId(self):
        return "%s.1.1" % self.vc3CliId()

    @classmethod
    def setup(cls):
        vc3CliId = cls.vc3CliId()
        cls.runCli("device init")
        cls.runCli("sdh map vc3.%s 7xtug2s" % vc3CliId)
        cls.runCli("sdh map tug2.%s.1 tu11" % vc3CliId)
        cls.runCli("sdh map vc1x.%s.1.1 de1" % vc3CliId)
    
    def prbsEngineCreate(self):    
        self.runCli("prbs engine create de1 1 %s.1.1" % self.vc3CliId())


class PtsPdhDe1BertProvider(De1BertProvider):
    def de1CliId(self):
        return "1"

    @classmethod
    def setup(cls):
        cls.runCli("device init")
        cls.runCli("ciena pdh interface de1")
        
    def prbsEngineCreate(self):    
        self.runCli("prbs engine create de1 1 1")

class VcDe1NxDs0BertProvider(VcDe1BertProvider):
    @classmethod
    def setup(cls):
        vc3CliId = cls.vc3CliId()
        cls.runCli("device init")
        cls.runCli("sdh map vc3.%s 7xtug2s" % vc3CliId)
        cls.runCli("sdh map tug2.%s.1 tu11" % vc3CliId)
        cls.runCli("sdh map vc1x.%s.1.1 de1" % vc3CliId)
        cls.runCli("pdh de1 framing %s.1.1 ds1_sf" % vc3CliId)
        cls.runCli("pdh de1 nxds0create %s.1.1 0-3" % vc3CliId)
        
    def prbsEngineCreate(self):    
        self.runCli("prbs engine create nxds0 1 %s.1.1 0-3" % self.vc3CliId())

class PtsPdhVcDe1NxDs0BertProvider(VcDe1NxDs0BertProvider):
    @classmethod
    def vc3CliId(cls):
        return "1"

class De3BertProvider(PdhChannelConfigureProvider):
    @classmethod
    def setUpClass(cls):
        cls.runCli("device init")
        cls.runCli("sdh map vc3.25.1.1 de3")
    
    def prbsEngineCreate(self):
        self.runCli("prbs engine create de3 1 25.1.1")

    def currentLoopback(self):
        result = self.runCli("show pdh de3 25.1.1")
        return result.cellContent(0, "LoopbackMode")

    def loopback(self, mode):
        self.runCli("pdh de1 loopback 25.1.1 %s" % mode)


class VcBertProvider(ConfigureProvider):
    def pathCliId(self):
        return None

    def currentLoopback(self):
        result = self.runCli("show sdh path loopback %s" % self.pathCliId())
        return result.cellContent(0, "Loopback")

    def loopback(self, mode):
        self.runCli("sdh path loopback %s %s" % (self.pathCliId(), mode))

    def canMakeLoVcLocalLoopback(self):
        if AnnaProductCodeProvider.isMro10G(self.productCode):
            cliResult = self.runCli("show sdh path %s" % self.pathCliId())
            bound = cliResult.cellContent(rowId=0, columnIdOrName="Bound")
            if bound.lower() == "none":
                return False

        return True

class Vc1xBertProvider(VcBertProvider):
    @classmethod
    def vc3CliId(cls):
        return "Invalid"

    @classmethod
    def setup(cls):
        cls.runCli("device init")
        cls.runCli("sdh map vc3.%s 7xtug2s" % cls.vc3CliId())
        cls.runCli("sdh map tug2.%s.1 tu11" % cls.vc3CliId())
        cls.runCli("sdh map vc1x.%s.1.1 c1x" % cls.vc3CliId())

    def prbsEngineCreate(self):        
        self.runCli("prbs engine create vc 1 vc1x.%s.1.1" % self.vc3CliId())

    def pathCliId(self):
        return "vc1x.%s.1.1" % self.vc3CliId()

class PtsMroVc1xBertProvider(Vc1xBertProvider):
    @classmethod
    def vc3CliId(cls):
        return "25.1.1"

    def canMakeLocalLoopback(self):
        return self.canMakeLoVcLocalLoopback()


class PtsPdhVc1xBertProvider(Vc1xBertProvider):
    @classmethod
    def vc3CliId(cls):
        return "1"


class HoVcBertProvider(VcBertProvider):
    @classmethod
    def vc3CliId(cls):
        return "Invalid"

    @classmethod
    def setup(cls):
        vc3CliId = cls.vc3CliId()
        cls.runCli("device init")
        cls.runCli("sdh map vc3.%s c3" % cls.vc3CliId())
    
    def prbsEngineCreate(self):        
        self.runCli("prbs engine create vc 1 vc3.%s" % self.vc3CliId())

    def remoteLoopbackSupported(self):
        return False

    def pathCliId(self):
        return "vc3.%s" % self.vc3CliId()

class PtsMroHoVcBertProvider(HoVcBertProvider):
    @classmethod
    def vc3CliId(cls):
        return "25.1.1"

class PtsPdhHoVcBertProvider(HoVcBertProvider):
    @classmethod
    def vc3CliId(cls):
        return "1"

class PtsMroLoVc3BertProvider(VcBertProvider):
    @classmethod
    def setup(cls):
        cls.runCli("device init")
        cls.runCli("sdh map aug1.25.1 vc4")
        cls.runCli("sdh map vc4.25.1 3xtug3s")
        cls.runCli("sdh map tug3.25.1.1 vc3")
        cls.runCli("sdh map vc3.25.1.1 c3")
    
    def prbsEngineCreate(self):        
        self.runCli("prbs engine create vc 1 vc3.25.1.1")

    def pathCliId(self):
        return "vc3.25.1.1"

    def canMakeLocalLoopback(self):
        return self.canMakeLoVcLocalLoopback()

class TdmBert(AtTestCase):
    _provider = None

    @classmethod
    def setProvider(cls, provider):
        """
        :type provider: ConfigureProvider
        """
        cls._provider = provider

    @classmethod
    def provider(cls):
        """
        :rtype: ConfigureProvider
        """
        return cls._provider

    def setUp(self):
        self.assertIsNotNone(self.provider(), "Provider must be configured")
        if self.provider().canMakeLocalLoopback():
            self.localLoopback()

    @classmethod
    def setUpClass(cls):
        cls.provider().setup()

    def engineAttribute(self, name):
        result = self.runCli("show prbs engine 1")
        return result.cellContent(0, name)

    def assertTdmSide(self):
        self.assertEqual(self.engineAttribute("genSide"), "tdm")

    def assertLocalLoopback(self):
        self.assertEqual(self.provider().currentLoopback(), self.provider().localLoopbackMode())

    def assertRemoteLoopback(self):
        self.assertEqual(self.provider().currentLoopback(), self.provider().remoteLoopbackMode())

    def assertLoopbackRelease(self):
        self.assertEqual(self.provider().currentLoopback(), "release")

    def assertTxEnabled(self):
        self.assertEqual(self.engineAttribute("txEnabled"), "en")

    def assertTxDisabled(self):
        self.assertEqual(self.engineAttribute("txEnabled"), "dis")

    def prbsEngineCreate(self):
        self.provider().prbsEngineCreate()
        
    def genToTdm(self):
        self.assertCliSuccess("prbs engine side generating 1 tdm")

    def enableTx(self):
        self.assertCliSuccess("prbs engine tx enable 1")

    def disableTx(self):
        self.assertCliSuccess("prbs engine tx disable 1")

    def localLoopback(self):
        self.provider().loopback(self.provider().localLoopbackMode())

    def remoteLoopback(self):
        self.provider().loopback(self.provider().remoteLoopbackMode())

    def releaseLoopback(self):
        self.provider().loopback("release")

    def testPrbsCreateThenCheckLoopback(self):
        self.prbsEngineCreate()
        if self.provider().canMakeLocalLoopback():
            self.assertLocalLoopback()

    @classmethod
    def runWithRunner(cls, runner):
        """
        :type runner: AtTestCaseRunner
        """
        providers = ConfigureProvider.providers(cls.productCode())
        for provider in providers:
            cls.setProvider(provider)
            runner.run(cls)

def TestMain():
    runner = AtTestCaseRunner.runner()
    TdmBert.runWithRunner(runner)
    runner.summary()

if __name__ == '__main__':
    TestMain()
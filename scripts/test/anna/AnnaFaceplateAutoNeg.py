"""
Created on Mar 23, 2017

@author: namnn
"""
from AnnaTestCase import AnnaMroTestCase
import python.arrive.atsdk.AtRegister as AtRegister
from test.AtTestCase import AtTestCaseRunner

class FaceplateAutoNeg(AnnaMroTestCase):
    pass

class an_rxab_pen_register(AtRegister.AtRegister):
    @staticmethod
    def localPort(portId):
        return portId % 2
    
    def HduplexBitPosition(self, portId):
        if self.localPort(portId) == 0:
            return 6
        return 22
        
    def FduplexBitPosition(self, portId):
        if self.localPort(portId) == 0:
            return 5
        return 21
    
    def ReFaultMask(self, portId):
        if self.localPort(portId) == 0:
            return AtRegister.cBit13_12
        return AtRegister.cBit29_28
    
    def ReFaultShift(self, portId):
        if self.localPort(portId) == 0:
            return 12
        return 28

class Faceplate1000BasexAutoNeg(FaceplateAutoNeg):
    @classmethod
    def allSerdesCliIds(cls):
        return "1-8"
    
    @classmethod
    def allEthPortCliIds(cls):
        return "2-9"
        
    @classmethod
    def driverPortIds(cls):
        return range(8)
        
    @classmethod
    def setUpClass(cls):
        cls.classRunCli("device init")
        cls.classRunCli("serdes mode %s 1G" % cls.allSerdesCliIds())
        cls.classRunCli("eth port interface %s 1000basex" % cls.allEthPortCliIds())
        cls.classRunCli("eth port autoneg enable %s" % cls.allEthPortCliIds())
    
    @staticmethod
    def multirateBaseAddress(portId):
        part = portId / 8 
        if part == 0:
            return 0x0030000
        if part == 1:
            return 0x0050000
        
        return None
    
    def an_rxab_pen_address(self, portId):
        return self.multirateBaseAddress(portId) + 0x0A00 + (portId / 2)
    
    @staticmethod
    def cliPortId(portId):
        return portId + 2
    
    def testDuplexAbility(self):
        for portId in self.driverPortIds():
            address = self.an_rxab_pen_address(portId)
            reg = an_rxab_pen_register(self.rd(address))
            reg.address = address
            
            reg.wr(address, 0)
            reg.setBit(reg.HduplexBitPosition(portId), 1, applyHw = True)
            cliResult = self.runCli("show eth port autoneg %d" % self.cliPortId(portId))
            cell = cliResult.cellContent(0, "duplex")
            self.assertEqual(cell, "half-duplex", None)
            
            reg.wr(address, 0)
            reg.setBit(reg.FduplexBitPosition(portId), 1, applyHw = True)
            cliResult = self.runCli("show eth port autoneg %d" % self.cliPortId(portId))
            cell = cliResult.cellContent(0, "duplex")
            self.assertEqual(cell, "full-duplex", None)
            
            reg.wr(address, 0)
            reg.setBit(reg.HduplexBitPosition(portId), 1, applyHw = True)
            reg.setBit(reg.FduplexBitPosition(portId), 1, applyHw = True)
            cliResult = self.runCli("show eth port autoneg %d" % self.cliPortId(portId))
            cell = cliResult.cellContent(0, "duplex")
            self.assertEqual(cell, "half-duplex|full-duplex", None)
            
            reg.wr(address, 0)
            cliResult = self.runCli("show eth port autoneg %d" % self.cliPortId(portId))
            cell = cliResult.cellContent(0, "duplex")
            self.assertEqual(cell, "none", None)
    
    def testAccessRemoteFault(self):
        for portId in self.driverPortIds():
            address = self.an_rxab_pen_address(portId)
            reg = an_rxab_pen_register(self.rd(address))
            reg.address = address
            
            def _test(hwValue, cliValue):
                reg.wr(address, 0)
                reg.setField(reg.ReFaultMask(portId), reg.ReFaultShift(portId), hwValue)
                reg.apply()
                
                cliResult = self.runCli("show eth port autoneg %d" % self.cliPortId(portId))
                cell = cliResult.cellContent(0, "remote-fault")
                self.assertEqual(cell, cliValue, None)
            
            _test(0, "no-error")
            _test(1, "link-failure")
            _test(2, "offline")
            _test(3, "auto-neg-error")
    
def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(Faceplate1000BasexAutoNeg)
    runner.summary()

if __name__ == '__main__':
    TestMain()
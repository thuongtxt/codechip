import random

from test.AtTestCase import AtTestCaseRunner
from test.anna.AnnaTestCase import AnnaMroTestCase


class AbstractTest(AnnaMroTestCase):
    @classmethod
    def setUpClass(cls):
        cls.runCli("device init")
        cls.runCli("sdh map vc3.25.1.1 7xtug2s")
        cls.runCli("sdh map tug2.25.1.1.1 tu11")
        cls.runCli("sdh map vc1x.25.1.1.1.1 de1")

    def setUp(self):
        self.channelAutoRdiEnable(False)
        self.assertCliSuccess("sdh %s alarm affect enable %s tim" % (self.cliChannelType(), self.channelCliId()))

    def supportedAlarms(self):
        return list()

    def channelCliId(self):
        return "AnInvalidId"

    def _enableWithAlarmsString(self, alarmString, enabled = True):
        commandToken = "enable" if enabled else "disable"
        cli = "sdh %s autordi %s %s %s" % (self.cliChannelType(), commandToken, self.channelCliId(), alarmString)
        self.assertCliSuccess(cli)

    def assertAlarmEnabled(self, alarms, expectedEnable):
        cli = "show sdh %s alarm autordi %s" % (self.cliChannelType(), self.channelCliId())
        result = self.runCli(cli)
        self.assertEqual(result.numRows(), 1)
        for alarm in alarms:
            cellContent = result.cellContent(0, self.upperCaseAlarmName(alarm))
            expected = "en" if expectedEnable else "dis"
            self.assertEqual(cellContent, expected)

    def perAlarmEnable(self, alarms, enabled = True):
        alarmString = "|".join(alarms)
        self._enableWithAlarmsString(alarmString, enabled)

    def channelAutoRdiEnable(self, enabled = True):
        commandToken = "enable" if enabled else "disable"
        cli = "sdh %s autordi %s %s" % (self.cliChannelType(), commandToken, self.channelCliId())
        self.assertCliSuccess(cli)

    def assertChannelRdiEnabled(self, expectedEnabled):
        cli = "show sdh %s %s" % (self.cliChannelType(), self.channelCliId())
        result = self.runCli(cli)
        self.assertEqual(result.numRows(), 1)
        autoRdi = result.cellContent(0, "autoRdi")
        expected = "en" if expectedEnabled else "dis"
        self.assertEqual(autoRdi, expected)

    @staticmethod
    def upperCaseAlarmName(lowerCaseAlarmName):
        """
        :type lowerCaseAlarmName: str
        """
        assert type(lowerCaseAlarmName) == str
        return lowerCaseAlarmName.upper()

    def cliChannelType(self):
        return "InvalidChannelType"

    def passtestMustSupportMoreThanOneAlarms(self):
        self.assertGreater(len(self.supportedAlarms()), 0)

    def testAlarmAutoRdi_OneAlarm(self):
        alarms = self.supportedAlarms()
        for enabled in [True, False, True]:
            for alarm in alarms:
                self.perAlarmEnable([alarm], enabled)
                self.assertAlarmEnabled([alarm], enabled)

    def testAlarmAutoRdi_MuliAlarms(self):
        supportedAlarms = self.supportedAlarms()

        for enabled in [True, False, True]:
            for _ in range(10):
                self.channelAutoRdiEnable(False if enabled else True)
                numAlarms = random.choice(range(len(supportedAlarms)))
                if numAlarms == 0:
                    numAlarms = 1

                alarms = random.sample(supportedAlarms, numAlarms)
                self.perAlarmEnable(alarms, enabled)
                self.assertAlarmEnabled(alarms, enabled)

    def testEnableRdiAtChannelLevel(self):
        for enabled in [True, False, True]:
            self.channelAutoRdiEnable(enabled)
            self.assertChannelRdiEnabled(enabled)
            self.assertAlarmEnabled(self.supportedAlarms(), enabled)

    def testIfAnyAlarmIsEnabled_ThenAutoRdiAtChannelLayerWillBeEnabled(self):
        self.channelAutoRdiEnable(False)

        alarms = self.supportedAlarms()
        for alarm in alarms:
            for enabled in [True, False]:
                self.perAlarmEnable([alarm], enabled)
                self.assertChannelRdiEnabled(enabled)

class LineTest(AbstractTest):
    def supportedAlarms(self):
        return ["los", "lof", "tim", "ais"]

    def channelCliId(self):
        return "1"

    def cliChannelType(self):
        return "line"

class PathTest(AbstractTest):
    def cliChannelType(self):
        return "path"
    
    def supportedAlarms(self):
        return ["ais", "lop", "tim", "uneq", "plm"]

class HoPathTest(PathTest):
    def channelCliId(self):
        return "vc3.1.1.1"

class LoPathTest(PathTest):
    def channelCliId(self):
        return "vc1x.25.1.1.1.1"

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(LineTest)
    runner.run(HoPathTest)
    runner.run(LoPathTest)
    runner.summary()

if __name__ == '__main__':
    TestMain()

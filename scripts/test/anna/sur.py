from python.arrive.atsdk.cli import AtCliRunnable
from test.AtTestCase import AtTestCaseRunner
from test.anna.AnnaTestCase import AnnaTestCase


class ThresholdProfile(AtCliRunnable):
    def __init__(self):
        self._load = False

    @classmethod
    def channelTypeString(cls):
        return "Invalid channel type"

    @staticmethod
    def lowerCaseParams():
        return list()

    @staticmethod
    def upperCaseParams():
        return list()

    @classmethod
    def upperCaseParamNameFromLowerCaseName(cls, lowerCaseParamName):
        paramIndex = cls.lowerCaseParams().index(lowerCaseParamName)
        return cls.upperCaseParams()[paramIndex]

    @classmethod
    def numParams(cls):
        return len(cls.upperCaseParams())

    @classmethod
    def lineProfile(cls):
        return LineProfile()

    @classmethod
    def hoPathProfile(cls):
        return HoPathProfile()
    
    @classmethod
    def loPathProfile(cls):
        return LoPathProfile()

    @classmethod
    def de1Profile(cls):
        return De1Profile()

    @classmethod
    def de3Profile(cls):
        return De3Profile()

    def profileCliIds(self):
        return "1-%d" % self.numProfiles()

    def setThreshold(self, entry, paramName, value):
        cli = "sur threshold profile %s %d %s %d" % (self.channelTypeString(), entry + 1, paramName, value)
        assert self.runCli(cli).success()

    def getThreshold(self, entry, paramName):
        cli = "show sur threshold profile %s %s" % (self.channelTypeString(), self.profileCliIds())
        result = self.runCli(cli)
        assert result.numRows() == self.numProfiles()
        stringValue = result.cellContent(entry, self.upperCaseParamNameFromLowerCaseName(paramName))
        return int(stringValue)

    @classmethod
    def paramIndex(cls, paramName):
        return cls.lowerCaseParams().index(paramName)

    @staticmethod
    def numProfiles():
        return 8

class De3Profile(ThresholdProfile):
    @classmethod
    def channelTypeString(cls):
        return "de3"

    @staticmethod
    def lowerCaseParams():
        return [
                 "cvl",
                 "esl",
                 "sesl",
                 "lossl",
                 "cvpp",
                 "cvcpp",
                 "espp",
                 "escpp",
                 "sespp",
                 "sescpp",
                 "sasp",
                 "aissp",
                 "uaspp",
                 "uascpp",
                 "cvcppfe",
                 "escppfe",
                 "esbcppfe",
                 "sescppfe",
                 "sacppfe",
                 "uascppfe",
                 "esal",
                 "esbl",
                 "esapp",
                 "esacpp",
                 "esbpp",
                 "esbcpp",
                 "fcp",
                 "esacppfe",
                 "fccppfe"
                ]

    @staticmethod
    def upperCaseParams():
        return [
            "CV-L",
            "ES-L",
            "SES-L",
            "LOSS-L",
            "CVP-P",
            "CVCP-P",
            "ESP-P",
            "ESCP-P",
            "SESP-P",
            "SESCP-P",
            "SAS-P",
            "AISS-P",
            "UASP-P",
            "UASCP-P",
            "CVCP-PFE",
            "ESCP-PFE",
            "ESBCP-PFE",
            "SESCP-PFE",
            "SASCP-PFE",
            "UASCP-PFE",
            "ESA-L",
            "ESB-L",
            "ESAP-P",
            "ESACP-P",
            "ESBP-P",
            "ESBCP-P",
            "FC-P",
            "ESACP-PFE",
            "FCCP-PFE"
            ]

class De1Profile(ThresholdProfile):
    @classmethod
    def channelTypeString(cls):
        return "de1"

    @staticmethod
    def lowerCaseParams():
        return [
                "cvl",
                "esl",
                "sesl",
                "lossl",
                "eslfe",
                "cvp",
                "esp",
                "sesp",
                "aissp",
                "sasp",
                "cssp",
                "uasp",
                "sefspfe",
                "espfe",
                "sespfe",
                "csspfe",
                "fcpfe",
                "uaspfe",
                "fcp"
                ]

    @staticmethod
    def upperCaseParams():
        return [
                "CV-L",
                "ES-L",
                "SES-L",
                "LOSS-L",
                "ES-LFE",
                "CV-P",
                "ES-P",
                "SES-P",
                "AISS-P",
                "SAS-P",
                "CSS-P",
                "UAS-P",
                "SEFS-PFE",
                "ES-PFE",
                "SES-PFE",
                "CSS-PFE",
                "FC-PFE",
                "UAS-PFE",
                "FC-P"
                ]

class PathProfile(ThresholdProfile):
    @staticmethod
    def lowerCaseParams():
        return [
                "ppjcpdet",
                "npjcpdet",
                "ppjcpgen",
                "npjcpgen",
                "pjcdiff",
                "pjcspdet",
                "pjcspgen",
                "cvp",
                "esp",
                "sesp",
                "uasp",
                "fcp",
                "cvpfe",
                "espfe",
                "sespfe",
                "uaspfe",
                "fcpfe"]

    @staticmethod
    def upperCaseParams():
        return [
                "PPJC-PDet",
                "NPJC-PDet",
                "PPJC-PGen",
                "NPJC-PGen",
                "PJC-Diff",
                "PJCS-PDet",
                "PJCS-PGen",
                "CV-P",
                "ES-P",
                "SES-P",
                "UAS-P",
                "FC-P",
                "CV-PFE",
                "ES-PFE",
                "SES-PFE",
                "UAS-PFE",
                "FC-PFE"]

class HoPathProfile(PathProfile):
    @classmethod
    def channelTypeString(cls):
        return "hopath"

class LoPathProfile(PathProfile):
    @classmethod
    def channelTypeString(cls):
        return "lopath"

class LineProfile(ThresholdProfile):
    @classmethod
    def channelTypeString(cls):
        return "line"

    @staticmethod
    def lowerCaseParams():
        return ["sefss",
                "cvs",
                "ess",
                "sess",
                "cvl",
                "esl",
                "sesl",
                "uasl",
                "fcl",
                "cvlfe",
                "eslfe",
                "seslfe",
                "uaslfe",
                "fclfe"]

    @staticmethod
    def upperCaseParams():
        return ["SEFS-S",
                "CV-S",
                "ES-S",
                "SES-S",
                "CV-L",
                "ES-L",
                "SES-L",
                "UAS-L",
                "FC-L",
                "CV-LFE",
                "ES-LFE",
                "SES-LFE",
                "UAS-LFE",
                "FC-LFE"]

class AbstractTest(AnnaTestCase):
    _thresholdProfile  = None

    @classmethod
    def canRun(cls):
        # This script needs to be modify to run on PDH card
        return cls.isMroProduct()

    @staticmethod
    def cliChannelId():
        return "Invalid ID"

    @staticmethod
    def commandPrefix():
        return "Invalid prefix"

    @staticmethod
    def createThresholdProfile():
        return None

    @classmethod
    def thresholdProfile(cls):
        """
        :rtype: ThresholdProfile
        """
        if cls._thresholdProfile is None:
            cls._thresholdProfile = cls.createThresholdProfile()
        return cls._thresholdProfile

    @classmethod
    def valueForParam(cls, profileId, paramName):
        profile = cls.thresholdProfile()
        return profileId * profile.numParams() + profile.paramIndex(paramName) + 1

    @classmethod
    def defaultProfileSetup(cls):
        profile = cls.thresholdProfile()
        paramNames = profile.lowerCaseParams()
        for entryId in range(profile.numProfiles()):
            for paramName in paramNames:
                thresholdValue = cls.valueForParam(entryId, paramName)
                profile.setThreshold(entryId, paramName, thresholdValue)
                assert profile.getThreshold(entryId, paramName) == thresholdValue

    @classmethod
    def setUpClass(cls):
        cls.runCli("device init")
        cls.runCli("sdh map vc3.25.1.1-25.16.3 7xtug2s")
        cls.runCli("sdh map tug2.25.1.1.1-25.16.3.7 tu11")
        cls.runCli("sdh map vc1x.25.1.1.1.1-25.16.3.7.4 de1")
        cls.runCli("sdh map vc3.26.1.1-26.16.3 de3")
        cls.runCli("sdh map aug1.27.1-27.16 vc4")
        cls.runCli("sdh map vc4.27.1-27.16 3xtug3s")
        cls.runCli("sdh map tug3.27.1.1-27.16.3 vc3")

        cls.defaultProfileSetup()

    def setProfileId(self, entryIndex):
        cli = "%s surveillance threshold profile %s %d" % (self.commandPrefix(), self.cliChannelId(), entryIndex + 1)
        self.assertCliSuccess(cli)

    def getProfileId(self):
        cli  = "show %s surveillance threshold profile %s" % (self.commandPrefix(), self.cliChannelId())
        result = self.runCli(cli)
        cellContent = result.cellContent(0, "profileId")
        return int(cellContent) - 1

    def getPeriodThreshold(self):
        """
        :rtype: python.arrive.AtAppManager.AtCliResult.AtCliResult
        """
        cli = "show %s pm threshold period %s" % (self.commandPrefix(), self.cliChannelId())
        return self.runCli(cli)

    def testNumberOfParamsMustGreaterThanZero(self):
        profile = self.thresholdProfile()

        self.assertGreater(len(profile.lowerCaseParams()), 0)
        self.assertGreater(len(profile.upperCaseParams()), 0)
        self.assertEqual(len(profile.lowerCaseParams()), len(profile.upperCaseParams()))

    def testNumberOfThresholdProfileMustBeEnough(self):
        self.assertEqual(self.thresholdProfile().numProfiles(), 8)

    def testCanAssignAnyThresholdProfile(self):
        for profileId in range(self.thresholdProfile().numProfiles()):
            self.setProfileId(profileId)
            self.assertEqual(self.getProfileId(), profileId)

    def testCannotAssignInvalidThresholdProfile(self):
        def run():
            self.setProfileId(self.thresholdProfile().numProfiles())
        self.assertRaises(Exception, run)

    def testCannotAccessInvalidProfileIndex(self):
        profile = self.thresholdProfile()
        paramNames = profile.lowerCaseParams()
        invalidEntry = profile.numProfiles()
        for paramName in paramNames:
            def Try():
                profile.setThreshold(invalidEntry, paramName, 10)
            self.assertRaises(Exception, Try)

    def testParamThresholdMustMatchThresholdProfile(self):
        profile = self.thresholdProfile()
        
        for profileId in range(profile.numProfiles()):
            self.setProfileId(profileId)
            for paramName in self.thresholdProfile().lowerCaseParams():
                upperCaseName = profile.upperCaseParamNameFromLowerCaseName(paramName)
                result = self.getPeriodThreshold()
                cellContent = result.cellContent(upperCaseName, 1) # column 1 for first channel
                thresholdValue = int(cellContent)
                if thresholdValue != profile.getThreshold(profileId, paramName):
                    profile.getThreshold(profileId, paramName)
                self.assertEqual(thresholdValue, profile.getThreshold(profileId, paramName))

class SdhChannelTest(AbstractTest):
    pass

class SdhLineTest(SdhChannelTest):
    @staticmethod
    def cliChannelId():
        return "3"

    @staticmethod
    def createThresholdProfile():
        return ThresholdProfile.lineProfile()

    @staticmethod
    def commandPrefix():
        return "sdh line"

class SdhPathTest(SdhChannelTest):
    @staticmethod
    def commandPrefix():
        return "sdh path"

class SdhHoPathTest(SdhPathTest):
    @staticmethod
    def cliChannelId():
        return "vc3.1.2.2"
    
    @staticmethod
    def createThresholdProfile():
        return ThresholdProfile.hoPathProfile()
    
class SdhLoPathTest(SdhPathTest):
    @staticmethod
    def cliChannelId():
        return "vc1x.25.4.2.5.4"
    
    @staticmethod
    def createThresholdProfile():
        return ThresholdProfile.loPathProfile()

class SdhLoVc3PathTest(SdhPathTest):
    @staticmethod
    def cliChannelId():
        return "vc3.27.4.2"
    
    @staticmethod
    def createThresholdProfile():
        return ThresholdProfile.loPathProfile()

class PdhChannelTest(AbstractTest):
    pass

class PdhDe1Test(PdhChannelTest):
    @staticmethod
    def cliChannelId():
        return "25.4.2.7.3"

    @staticmethod
    def createThresholdProfile():
        return ThresholdProfile.de1Profile()

    @staticmethod
    def commandPrefix():
        return "pdh de1"

class PdhDe3Test(PdhChannelTest):
    @staticmethod
    def cliChannelId():
        return "26.13.2"

    @staticmethod
    def commandPrefix():
        return "pdh de3"

    @staticmethod
    def createThresholdProfile():
        return ThresholdProfile.de3Profile()

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(SdhLineTest)
    runner.run(SdhLoPathTest)
    runner.run(SdhHoPathTest)
    runner.run(SdhLoVc3PathTest)
    runner.run(PdhDe1Test)
    runner.run(PdhDe3Test)
    runner.summary()

if __name__ == '__main__':
    TestMain()

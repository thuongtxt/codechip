from AtAppManager import AtDeviceVersion
from arrive.AtAppManager.AtColor import AtColor
from python.arrive.atsdk.platform import AtHalListener
from test.AtTestCase import AtTestCaseRunner
import python.arrive.atsdk.AtRegister as AtRegister
from test.AutoNegTestCase import AutoNegTestCase
from AnnaTestCase import AnnaMroTestCase, AnnaTestCase

class _Register(AtRegister.AtRegister):
    def __init__(self, address):
        super(_Register, self).__init__()
        self.rd(address)

class an_mod_pen0(_Register):
    def tx_enb0(self, portId):
        return self.getBit(16 + portId)

    def an_mod0(self, portId):
        return self.getBit(portId)

class an_spd_pen0(_Register):
    def cfg_spd(self, portId):
        mask = AtRegister.cBit1_0 << (portId * 2)
        shift = portId * 2
        return self.getField(mask, shift)

class BitField(object):
    def __init__(self, bitMaskString):
        self.bitMask = bitMaskString
        
        bits = bitMaskString.split("bit")[1].split("_")
        stopBit = int(bits[0])
        startBit = int(bits[1])
        
        numBits = stopBit - startBit + 1
        assert(numBits <= 32)
        self.bitMask = "bit%d_%d" % (stopBit, startBit)
        
        maxValue = 0
        for bit_i in range(0, numBits):
            maxValue = maxValue | (AtRegister.cBit0 << bit_i)
        self.maxValue = maxValue

class Counter(object):
    def __init__(self, counterName, hwName, offset, bitField, roAddress, r2cAddress):
        self.counterName = counterName
        self.offset = offset
        self.bitField = bitField
        self.roAddress = roAddress
        self.r2cAddress = r2cAddress
        self.hwName = hwName
        
    @classmethod
    def allCounters(cls):
        dicts = {}
        
        def _addCounter(counterName, hwName, offset, bitField_, roAddress_, r2cAddress_):
            dicts[counterName] = Counter(counterName, hwName, offset, bitField_, roAddress_, r2cAddress_)
        
        roAddress = 0xA40
        r2cAddress = 0xA00
        bitField = BitField("bit47_24")
        _addCounter("txPacketsLen128_255" , "txpkt255byte" , 0, bitField, roAddress, r2cAddress)
        _addCounter("txPacketsLen512_1023", "txpkt1023byte", 1, bitField, roAddress, r2cAddress)
        _addCounter("txPacketsJumbo"      , "txpktjumbo"   , 2, bitField,  roAddress, r2cAddress)
        bitField = BitField("bit23_0")
        _addCounter("txPacketsLen65_127"   , "txpkt127byte" , 0, bitField,  roAddress, r2cAddress)
        _addCounter("txPacketsLen256_511"  , "txpkt511byte" , 1, bitField,  roAddress, r2cAddress)   
        _addCounter("txPacketsLen1024_1518", "txpkt1518byte", 2, bitField,  roAddress, r2cAddress)

        roAddress  = 0xAC0
        r2cAddress = 0xA80
        bitField = BitField("bit63_32")
        _addCounter("txPackets",         "txpktgood",   0, bitField,  roAddress, r2cAddress)
        _addCounter("txPacketsLen0_64",  "txpkt64byte", 1, bitField,  roAddress, r2cAddress)
        _addCounter("txOversizePackets", "txoversize",  2, bitField,  roAddress, r2cAddress)
        bitField = BitField("bit31_0")
        _addCounter("txBytes",            "txbytecnt",     0, bitField,  roAddress, r2cAddress)
        _addCounter("txDiscardedPackets", "txpkttotaldis", 1, bitField,  roAddress, r2cAddress)
        _addCounter("txUndersizePackets", "txundersize",   2, bitField,  roAddress, r2cAddress)
        
        roAddress  = 0xB40
        r2cAddress = 0xB00
        bitField = BitField("bit47_24")
        _addCounter("txOverFlowDroppedPackets", "txpktoverrun",   0, bitField,  roAddress, r2cAddress)
        _addCounter("txMultCastPackets",        "txpktmulticast", 1, bitField,  roAddress, r2cAddress)
        bitField = BitField("bit23_0")
        _addCounter("txPauFramePackets", "txpktpausefrm", 0, bitField,  roAddress, r2cAddress)
        _addCounter("txBrdCastPackets", "txpktbroadcast", 1, bitField,  roAddress, r2cAddress)
        
        roAddress  = 0xE40
        r2cAddress = 0xE00
        bitField = BitField("bit47_24")
        _addCounter("rxPacketsLen128_255",  "rxpkt255byte",  0, bitField,  roAddress, r2cAddress)
        _addCounter("rxPacketsLen512_1023", "rxpkt1023byte", 1, bitField,  roAddress, r2cAddress)
        _addCounter("rxPacketsJumbo",       "rxpktjumbo",    2, bitField,  roAddress, r2cAddress)
        bitField = BitField("bit23_0")
        _addCounter("rxPacketsLen65_127",    "rxpkt127byte",  0, bitField,  roAddress, r2cAddress)
        _addCounter("rxPacketsLen256_511",   "rxpkt511byte",  1, bitField,  roAddress, r2cAddress)
        _addCounter("rxPacketsLen1024_1518", "rxpkt1518byte", 2, bitField,  roAddress, r2cAddress)
        
        roAddress  = 0xEC0
        r2cAddress = 0xE80
        bitField = BitField("bit63_32") 
        _addCounter("rxPackets",        "rxpkttotal",  0, bitField,  roAddress, r2cAddress)
        _addCounter("rxPacketsLen0_64", "rxpkt64byte", 1, bitField,  roAddress, r2cAddress)
        _addCounter("rxLoopDaPackets",  "rxpktdaloop", 2, bitField,  roAddress, r2cAddress)
        bitField = BitField("bit31_0")
        _addCounter("rxBytes",            "rxbytecnt",     0, bitField,  roAddress, r2cAddress)
        _addCounter("rxErrFcsPackets",    "rxpktfcserr",   1, bitField,  roAddress, r2cAddress) 
        _addCounter("rxDiscardedPackets", "rxpkttotaldis", 2, bitField,  roAddress, r2cAddress)
        
        roAddress  = 0xF40
        r2cAddress = 0xF00
        bitField = BitField("bit47_24")
        _addCounter("rxJabberPackets",   "rxpktjabber",   0, bitField,  roAddress, r2cAddress)
        _addCounter("rxPausePackets", "rxpktpausefrm", 1, bitField,  roAddress, r2cAddress)
        _addCounter("rxOversizePackets", "rxpktoversize", 2, bitField,  roAddress, r2cAddress)
        bitField = BitField("bit23_0")
        _addCounter("rxPcsErrPackets",    "rxpktpcserr",    0, bitField,  roAddress, r2cAddress)
        _addCounter("rxFragmentPackets",  "rxpktfragment",  1, bitField,  roAddress, r2cAddress)
        _addCounter("rxUndersizePackets", "rxpktundersize", 2, bitField,  roAddress, r2cAddress)
        
        roAddress  = 0xFC0
        r2cAddress = 0xF80
        bitField = BitField("bit47_24")
        _addCounter("rxBrdCastPackets", "rxpktbroadcast", 0, bitField,  roAddress, r2cAddress)
        bitField = BitField("bit23_0")
        _addCounter("rxOverFlowDroppedPackets", "rxpktoverrun",   0, bitField,  roAddress, r2cAddress)
        _addCounter("rxMultCastPackets",        "rxpktmulticast", 1, bitField,  roAddress, r2cAddress) 
    
        return dicts

class FaceplateEthPassThrough(AnnaMroTestCase):
    @classmethod
    def setUpClass(cls):
        cls.classRunCli("device show readwrite dis dis")
        cls.classRunCli("device init")
        cls.classRunCli("serdes powerup 1-16")
        cls.classRunCli("serdes mode 1-16 1G")
        cls.classRunCli("serdes reset 1-16")
        cls.classRunCli("serdes enable 1-16")
    
    @classmethod
    def _numPorts(cls):
        return 16 if cls.is20G() else 8

    @staticmethod
    def _startId():
        return 2
    def _stopId(self):
        return self._startId() + self._numPorts() - 1
    
    @staticmethod
    def _multiRateBaseAddress(portId):
        return 0x30000
    
    @staticmethod
    def ethPassThroughBaseAddress():
        return 0xC0000
    
    def checkAttribute(self, portIds, attributeName, expected, cli=None):
        if cli is None:
            cli = "show eth port %s" % portIds
        
        cliResult = self.runCli(cli)
        self.assertGreater(cliResult.numRows(), 0, None)
        
        for column in range(1, cliResult.numColumns()):
            columName = cliResult.columnName(column)
            cellContent = cliResult.cellContent(attributeName, columName)
            self.assertEqual(cellContent, expected, "Check %s fail" % attributeName)
    
    def _allPorts(self):
        return "%d-%d" % (self._startId(), self._stopId())
    
    def testDefaultInterfaceShouldBe1000basex(self):
        self.assertCliSuccess("device init")
        self.assertCliSuccess("serdes mode 1-16 stm4")
        self.assertCliSuccess("serdes mode 1-16 1G")
        
        self.checkAttribute(self._allPorts(), "Interface", "1000basex")
    
    def test1000BasexAndSgmiiMustBeSupported(self):
        portIds = self._allPorts()
        
        def checkInterface(interfaceName):
            self.assertCliSuccess("eth port interface %s %s" % (portIds, interfaceName))
            self.checkAttribute(portIds, "Interface", interfaceName)
            
            expectedMode = 1 if interfaceName == "sgmii" else 0
            for port_i in range(0, self._numPorts()):
                reg = an_mod_pen0(self._multiRateBaseAddress(port_i) + 0x0801)
                self.assertEqual(reg.an_mod0(port_i), expectedMode, None)
                
        checkInterface("sgmii")
        checkInterface("1000basex")
        
    def testCanEnableDisable(self):
        portIds = self._allPorts()
        
        def _enable(enabled):
            command = "enable" if enabled else "disable"
            self.assertCliSuccess("eth port %s %s" % (command, portIds))
            self.checkAttribute(portIds, "Enabled", "en" if enabled else "dis")
            for port_i in range(0, self._numPorts()):
                reg = an_mod_pen0(self._multiRateBaseAddress(port_i) + 0x0801)
                self.assertEqual(reg.tx_enb0(port_i), 1 if enabled else 0, None)
        
        _enable(True)
        _enable(False)
    
    def testCanSpecifySpeed(self):
        portIds = self._allPorts()
        self.assertCliSuccess("eth port interface %s sgmii" % portIds)
        
        def _speedSet(speed):
            self.assertCliSuccess("eth port speed %s %s" % (portIds, speed))
            self.checkAttribute(portIds, "Speed", speed)
            
            speeds = {"1000m":2, "100m":1, "10m":0}
            
            for port_i in range(0, self._numPorts()):
                reg = an_spd_pen0(self._multiRateBaseAddress(port_i) + 0x0804)
                self.assertEqual(reg.cfg_spd(port_i), speeds[speed], None)
            
        _speedSet("1000m")
        _speedSet("100m")
        _speedSet("10m")
        
    def testCannotSpecifySpeedWith1000Basex(self):
        portIds = self._allPorts()
        self.assertCliSuccess("eth port interface %s 1000basex" % portIds)
        
        for speed in ["100m", "10m"]:
            self.assertCliFailure("eth port speed %s %s" % (portIds, speed))
            
        self.assertCliSuccess("eth port speed %s 1000m" % portIds)
        
    def _addressWithocalAddress(self, localAddress, portId, offset):
        return self.ethPassThroughBaseAddress() + localAddress + 16 * offset + portId
    
    def upen_txepagrp1cnt(self, portId, offset):
        return self._addressWithocalAddress(0xA40, portId, offset) 

    def counter64ByteAddressChanged(self):
        if self.is20G():
            return True

        versionThatHasThis = AtDeviceVersion(versionString=None)
        versionThatHasThis.major = 0x2
        versionThatHasThis.minor = 0x5
        versionThatHasThis.built = 0
        if self.app().deviceVersion() >= versionThatHasThis:
            return True

        return False

    def shouldTestCounter(self, counterName):
        if not self.counter64ByteAddressChanged():
            return True

        if counterName in ["txPacketsLen0_64", "rxPacketsLen0_64"]:
            return False

        return True

    def testCounter(self):
        counters = Counter.allCounters()
        for _, counterInfo in counters.iteritems():
            if not self.shouldTestCounter(counterInfo.counterName):
                AtColor.printColor(AtColor.YELLOW, "Ingore testing counter type %s because address is changed" % counterInfo.counterName)
                continue

            for portId in range(0, self._numPorts()):
                portString = "%d" % (portId + self._startId())
                
                localAddress = counterInfo.r2cAddress 
                address = self._addressWithocalAddress(localAddress, portId, counterInfo.offset)
                bitField = counterInfo.bitField
                
                for value in [bitField.maxValue, bitField.maxValue / 2, 0]:
                    self.runCli("lwr %x %x %s" % (address, value, bitField.bitMask))
                    cli = "show eth port counters %s r2c" % portString
                    expected = "%d" % value
                    self.checkAttribute(portString, counterInfo.counterName, expected, cli)
                    value = bitField.maxValue / 2
                    self.runCli("lwr %x %x %s" % (address, value, bitField.bitMask))
                    cli = "show eth port counters %s r2c" % portString
                    expected = "%d" % value
                    self.checkAttribute(portString, counterInfo.counterName, expected, cli)
                    
    
    def testSetSMAC(self):
        portIds = self._allPorts()
        mac = "de.ad.c0.ca.ca.cf"
        self.assertCliSuccess("eth port srcmac %s %s" % (portIds, mac))
        self.checkAttribute(portIds, "Source MAC", mac, "show eth port %s" % portIds)
        mac = "ca.ce.de.ad.c0.ca"
        self.assertCliSuccess("eth port srcmac %s %s" % (portIds, mac))
        self.checkAttribute(portIds, "Source MAC", mac, "show eth port %s" % portIds)
    
    def testEnableDisableBypass(self):
        for enabled in [True, False]:
            for port_i in range(0, self._numPorts()):
                portId = "%d" % (port_i + self._startId())
                command = "enable" if enabled else "disable"
                self.assertCliSuccess("ciena eth port bypass %s %s" % (command, portId))
                result = self.runCli("show ciena eth port %s" % portId)
                cellContent = result.cellContent(0, "Bypass")
                self.assertEqual(cellContent, "en" if enabled else "dis", None)
    
    def testVlansAreAlwaysAccessible(self):
        for port_i in range(0, self._numPorts()):
            portId = "%d" % (port_i + self._startId())
            result = self.runCli("show ciena eth port %s" % portId)
            
            def _check(vlanName):
                cellContent = result.cellContent(0, vlanName)
                error = cellContent.lower().find("error") != -1
                self.assertFalse(error, None)
                self.assertNotEqual(cellContent.lower(), "none", None)
                
            _check("Transmit Vlan")
            _check("Expected Vlan")
    
    def testAccessTwoSubPortVlanTpids(self):
        result = self.runCli("show ciena eth subport vlan tpid")
        self.assertEqual(result.numRows(), 2, None)
        for tpid_i in range(0, result.numRows()):
            for side in ["transmit", "expect"]:
                tpid = 0xA000 + tpid_i
                self.assertCliSuccess("ciena eth subport vlan tpid %s %d 0x%x" % (side, tpid_i + 1, tpid))
                result = self.runCli("show ciena eth subport vlan tpid")
                columnName = "Transmit" if side == "transmit" else "Expect"
                cellContent = result.cellContent(tpid_i, columnName)
                self.assertEqual(int(cellContent, 16), tpid, None)
    
    def testCounterRegistersMustNotBeAccessedMoreThanOneTimes(self):
        """
        This test is necessary because when displaying all counters in a table,
        even with read-only mode, hardware will clear counters in the same long 
        register
        """
        class _HalListener(AtHalListener):
            def __init__(self, testCase):
                AtHalListener.__init__(self)
                self.accessedAddresses = {}
                self.testCase = testCase
            
            def didRead(self, anAddress, value):
                holdAddress = self.testCase.ethPassThroughBaseAddress() + 0x80
                if anAddress == holdAddress:
                    return

                try:
                    accessInfo_ = self.accessedAddresses[anAddress]
                except:
                    accessInfo_ = {"address": anAddress, "readCount" : 0}
                    self.accessedAddresses[anAddress] = accessInfo_
                    
                accessInfo_["readCount"] = accessInfo_["readCount"] + 1
                
        listener = _HalListener(self)
        AtHalListener.addListener(listener)
        self.runCli("show eth port counters %s r2c" % self._allPorts())
        AtHalListener.removeListener(listener)
        
        for _, accessInfo in listener.accessedAddresses.iteritems():
            address = accessInfo["address"]
            readCount = accessInfo["readCount"]
            self.assertEqual(readCount, 1, "Register 0x%x is accessed for %d times" % (address, readCount))
           
    def testCannotChangeCounterTickMode(self):
        ports = self._allPorts()
        self.assertCliSuccess("debug ciena eth port counters tick %s auto" % ports)
        self.assertCliFailure("debug ciena eth port counters tick %s manual" % ports)
            
class BackplaneXilinxMac(AnnaMroTestCase):
    @classmethod
    def setUpClass(cls):
        cls.classRunCli("device show readwrite dis dis")
        cls.classRunCli("device init")

    @staticmethod
    def _numPorts():
        return 2

    @staticmethod
    def _baseAddress(portId):
        if portId == 0:
            return 0xF80000
        if portId == 1:
            return 0xF90000
        
        return None 

    def testAccessMacAttributes(self):
        self.skipTest("This test case has not been implemented. Implement this please")
        
    @staticmethod
    def _allCountersInfo():
        counters = {}
        
        def _addCounter(localAddress, hwCounterName, cliCounterName):
            counterInfo = Counter(cliCounterName, hwCounterName, 0, "bit31_0", localAddress, None)
            counters[cliCounterName] = counterInfo
            
        # TX counters
        _addCounter(0x2000, "tx_total_pkt_cnt", "txPackets")
        _addCounter(0x2001, "tx_total_byte_cnt", "txBytes")
        _addCounter(0x2002, "tx_total_gpkt_cnt", "txGoodPackets")
        _addCounter(0x2003, "tx_total_gbyte_cnt", "txGoodBytes")
        _addCounter(0x2004, "tx_pkt64_cnt", "txPacketsLen0_64")
        _addCounter(0x2005, "tx_pkt65_127_cnt", "txPacketsLen65_127")
        _addCounter(0x2006, "tx_pkt128_255_cnt", "txPacketsLen128_255")
        _addCounter(0x2007, "tx_pkt256_511_cnt", "txPacketsLen256_511")
        _addCounter(0x2008, "tx_pkt512_1023_cnt", "txPacketsLen512_1023")
        _addCounter(0x2009, "tx_pkt1024_1518_cnt", "txPacketsLen1024_1518")
        _addCounter(0x200A, "tx_pkt1519_1522_cnt", "txPacketsLen1519_1522")
        _addCounter(0x200B, "tx_pkt1523_1548_cnt", "txPacketsLen1523_1548")
        _addCounter(0x200C, "tx_pkt1549_2047_cnt", "txPacketsLen1549_2047")
        _addCounter(0x200D, "tx_pkt2048_4095_cnt", "txPacketsLen2048_4095")
        _addCounter(0x200E, "tx_pkt4096_8191_cnt", "txPacketsLen4096_8191")
        _addCounter(0x200F, "tx_pkt8192_9215_cnt", "txPacketsLen8192_9215")
        _addCounter(0x2010, "tx_spkt_cnt", "txPacketsSmall")
        _addCounter(0x2011, "tx_lpkt_cnt", "txPacketsLarge")
        _addCounter(0x2012, "tx_bad_fcs_cnt", "txErrFcsPackets")
        _addCounter(0x2013, "tx_frame_error_cnt", "txErrPackets")
        _addCounter(0x2014, "tx_unicast_cnt", "txUniCastPackets")
        _addCounter(0x2015, "tx_multicast_cnt", "txMultCastPackets")
        _addCounter(0x2016, "tx_broadcast_cnt", "txBrdCastPackets")
        _addCounter(0x2017, "tx_vlan_cnt", "txVlanPackets")
        _addCounter(0x2018, "tx_pause_cnt", "txPauFramePackets")
        _addCounter(0x2019, "tx_user_pause_cnt", "txUserPausePackets")
        
        # RX counters
        _addCounter(0x2080, "rx_total_pkt_cnt", "rxPackets")
        _addCounter(0x2081, "rx_total_byte_cnt", "rxBytes")
        _addCounter(0x2082, "rx_total_gpkt_cnt", "rxGoodPackets")
        _addCounter(0x2083, "rx_total_gbyte_cnt", "rxGoodBytes")
        _addCounter(0x2084, "rx_pkt64_cnt", "rxPacketsLen0_64")
        _addCounter(0x2085, "rx_pkt65_127_cnt", "rxPacketsLen65_127")
        _addCounter(0x2086, "rx_pkt128_255_cnt", "rxPacketsLen128_255")
        _addCounter(0x2087, "rx_pkt256_511_cnt", "rxPacketsLen256_511")
        _addCounter(0x2088, "rx_pkt512_1023_cnt", "rxPacketsLen512_1023")
        _addCounter(0x2089, "rx_pkt1024_1518_cnt", "rxPacketsLen1024_1518")
        _addCounter(0x208A, "rx_pkt1519_1522_cnt", "rxPacketsLen1519_1522")
        _addCounter(0x208B, "rx_pkt1523_1548_cnt", "rxPacketsLen1523_1548")
        _addCounter(0x208C, "rx_pkt1549_2047_cnt", "rxPacketsLen1549_2047")
        _addCounter(0x208D, "rx_pkt2048_4095_cnt", "rxPacketsLen2048_4095")
        _addCounter(0x208E, "rx_pkt4096_8191_cnt", "rxPacketsLen4096_8191")
        _addCounter(0x208F, "rx_pkt8192_9215_cnt", "rxPacketsLen8192_9215")
        _addCounter(0x2090, "rx_spkt_cnt", "rxPacketsSmall")
        _addCounter(0x2091, "rx_lpkt_cnt", "rxPacketsLarge")
        _addCounter(0x2093, "rx_bad_fcs_cnt", "rxErrFcsPackets")
        _addCounter(0x2094, "rx_unicast_cnt", "rxUniCastPackets")
        _addCounter(0x2095, "rx_multicast_cnt", "rxMultCastPackets")
        _addCounter(0x2096, "rx_broadcast_cnt", "rxBrdCastPackets")
        _addCounter(0x2097, "rx_vlan_cnt", "rxVlanPackets")
        _addCounter(0x2098, "rx_pause_cnt", "rxPausePackets")
        _addCounter(0x2099, "rx_user_pause_cnt", "rxUserPausePackets")
        _addCounter(0x209A, "rx_undersize_cnt", "rxUndersizePackets")
        _addCounter(0x209B, "rx_fragment_cnt", "rxFragmentPackets")
        _addCounter(0x209C, "rx_oversize_cnt", "rxOversizePackets")
        _addCounter(0x209D, "rx_toolong_cnt", "rxPacketsTooLong")
        _addCounter(0x209E, "rx_jabber_cnt", "rxJabberPackets")
        _addCounter(0x209F, "rx_stomped_fcs_cnt", "rxStompedFcsPackets")
        _addCounter(0x20A0, "rx_inrangeerr_cnt", "rxInRangeErrPackets")
        _addCounter(0x20A1, "rx_truncated_cnt", "rxTruncatedPackets")
        
        return counters
        
    def _portAddress(self, portId, localAddress):
        return self._baseAddress(portId) + localAddress
        
    @staticmethod
    def _macCliStartPort():
        return 22
        
    def _macCliPort(self, portId):
        return portId + self._macCliStartPort()
        
    def testAccessCounters(self):
        allCountersInfo = self._allCountersInfo()
        for port_i in range(0, self._numPorts()):
            cliMacPort = self._macCliPort(port_i)
            for counterName, counterInfo in allCountersInfo.iteritems():
                localAddress = counterInfo.roAddress
                address = self._portAddress(port_i, localAddress)
                for value in [AtRegister.cBit31_0, AtRegister.cBit31_0 / 2, 1]:
                    self.assertCliSuccess("wr %x %x" % (address, value))
                    result = self.runCli("show eth port counters %s ro" % cliMacPort)
                    counter = int(result.cellContent(counterName, 1))
                    self.assertEqual(counter, value, None)

    def testCanChangeCounterTickMode(self):
        ports = "%s-%s" % (self._macCliStartPort(), self._macCliStartPort() + self._numPorts() - 1)
        self.assertCliSuccess("debug ciena eth port counters tick %s auto" % ports)
        self.assertCliSuccess("debug eth port %s" % ports)
        self.assertCliSuccess("debug ciena eth port counters tick %s manual" % ports)
        self.assertCliSuccess("debug eth port %s" % ports)

class FaceplateAutoneg(AutoNegTestCase):
    @classmethod
    def canRun(cls):
        return AnnaTestCase.isMroProduct()
    
    @classmethod
    def setUpClass(cls):
        cls.classRunCli("device show readwrite dis dis")
        cls.classRunCli("device init")
        cls.classRunCli("serdes powerup 1-16")
        cls.classRunCli("serdes mode 1-16 1G")
        cls.classRunCli("serdes reset 1-16")
        cls.classRunCli("serdes enable 1-16")
        cls.classRunCli("eth port interface 2-17 sgmii") # To have full autoneg functionality
        
    def numPorts(self):
        return 16
    
    @staticmethod
    def startCliPort():
        return 2
    
    def stopCliPort(self):
        return self.startCliPort() + self.numPorts() - 1
    
    def cliPorts(self):
        return "%d-%d" % (self.startCliPort(), self.stopCliPort())
    
class BackplaneAutoneg(AutoNegTestCase):
    @classmethod
    def canRun(cls):
        return AnnaTestCase.isMroProduct()
    
    @classmethod
    def setUpClass(cls):
        cls.classRunCli("device show readwrite dis dis")
        cls.classRunCli("device init")
        
    def numPorts(self):
        return 2
    
    @staticmethod
    def startCliPort():
        return 22
    
    def stopCliPort(self):
        return self.startCliPort() + self.numPorts() - 1
    
    def cliPorts(self):
        return "%d-%d" % (self.startCliPort(), self.stopCliPort())

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(FaceplateEthPassThrough)
    runner.run(BackplaneXilinxMac)
    runner.run(FaceplateAutoneg)
    runner.run(BackplaneAutoneg)
    runner.summary()
    
if __name__ == '__main__':
    TestMain()

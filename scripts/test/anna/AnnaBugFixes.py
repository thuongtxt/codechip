"""
Created on Apr 20, 2017

@author: namnn
"""
import python.arrive.atsdk.AtRegister as AtRegister
from AnnaTestCase import AnnaMroTestCase
from test.AtTestCase import AtTestCaseRunner
from test.anna.AnnaTestCase import RegisterManager
from utils.audit import RegisterCollector, Expector


class MroBugTest(AnnaMroTestCase):
    def setUp(self):
        self.runCli("device init")
        
    def testTerminatedVcLocalLoopback(self):
        self.assertCliSuccess("xc vc connect vc3.25.1.1 vc3.17.1.1 two-way")
        self.assertCliSuccess("pw create cep 1 basic")
        self.assertCliSuccess("pw circuit bind 1 vc3.25.1.1")
        self.assertCliSuccess("pw psn 1 mpls")
        self.assertCliSuccess("eth port enable 1")
        self.assertCliSuccess("eth port srcmac 1 22.44.22.44.22.44")
        self.assertCliSuccess("pw ethport 1 1")
        self.assertCliSuccess("pw ethheader 1 11.22.11.22.11.22 none none")
        self.assertCliSuccess("pw enable 1")
        self.assertCliSuccess("sdh path loopback vc3.25.1.1 local")
        self.assertCliSuccess("sdh path loopback vc3.25.1.1 release")

class MroDs1FarEndTcaMask(AnnaMroTestCase):
    @staticmethod
    def cliId():
        return "25.1.1.1.1"

    @classmethod
    def setUpClass(cls):
        cls.runCli("device init")
        cls.runCli("sdh map vc3.25.1.1 7xtug2s")
        cls.runCli("sdh map tug2.25.1.1.1 tu11")
        cls.runCli("sdh map vc1x.25.1.1.1.1 de1")

    def _enableTcaMask(self, enabled):
        mask = "|".join(self.allFarEndTcaMask())
        command = "pdh de1 pm notificationmask %s %s %s" % (self.cliId(), mask, "en" if enabled else "dis")
        self.assertCliSuccess(command)

    @staticmethod
    def allFarEndTcaMask():
        return ["eslfe", "sefspfe", "espfe", "sespfe", "csspfe", "fcpfe", "uaspfe"]

    @staticmethod
    def failureDict():
        return {"ES-LFE":"eslfe", "SEFS-PFE":"sefspfe", "ES-PFE":"espfe",
                "SES-PFE":"sespfe", "CSS-PFE":"csspfe", "FC-PFE":"fcpfe",
                "UAS-PFE":"uaspfe"}

    def failureType(self, aString):
        try:
            return self.failureDict()[aString]

        except:
            return None

    def getFarEndTcaMask(self):
        cli = "show pdh de1 pm notificationmask %s" % self.cliId()
        result = self.runCli(cli)
        farEndFailureName = self.failureDict().keys()
        enabledMasks = list()
        for failureName in farEndFailureName:
            stringVal = result.cellContent(0, failureName)
            if stringVal == "set":
                enabledMasks.append(self.failureType(failureName))

        return enabledMasks

    def enableTcaMask(self):
        self._enableTcaMask(True)

    def disableTcaMask(self):
        self._enableTcaMask(False)

    def testSpecifyFullTcaMaskForDs1Esf(self):
        self.assertCliSuccess("pdh de1 framing 25.1.1.1.1 ds1_esf")

        self.enableTcaMask()
        allMask = self.allFarEndTcaMask()
        enabledMask = self.getFarEndTcaMask()
        enabledMask.sort()
        allMask.sort()
        self.assertEqual(enabledMask, allMask)

        self.disableTcaMask()
        enabledMask = self.getFarEndTcaMask()
        self.assertEqual(len(enabledMask), 0)

    def testChangeFrameTypeMustClearNotApplicableMask(self):
        self.assertCliSuccess("pdh de1 framing 25.1.1.1.1 ds1_esf")
        self.enableTcaMask()
        self.assertCliSuccess("pdh de1 framing 25.1.1.1.1 ds1_sf")
        enabledMask = self.getFarEndTcaMask()
        self.assertEqual(len(enabledMask), 0)

    def testIgnoreNotApplicableMaskWhenSetting(self):
        self.assertCliSuccess("pdh de1 framing 25.1.1.1.1 ds1_sf")
        self.enableTcaMask()
        enabledMask = self.getFarEndTcaMask()
        self.assertEqual(len(enabledMask), 0)

class MroE1LofThreshold(AnnaMroTestCase):
    def lofThreshold(self):
        reg = AtRegister.AtRegister(self.rd(0x01054000))
        return reg.getField(AtRegister.cBit16_13, 13)

    def testThresholdMustBeRight(self):
        self.assertCliSuccess("device init")
        self.assertCliSuccess("sdh map vc3.25.1.1 7xtug2s")
        self.assertCliSuccess("sdh map tug2.25.1.1.1 tu11")
        self.assertCliSuccess("sdh map vc1x.25.1.1.1.1 de1")

        self.assertEqual(self.lofThreshold(), 2)

        self.assertCliSuccess("sdh map tug2.25.1.1.1 tu12")
        self.assertCliSuccess("sdh map vc1x.25.1.1.1.1 de1")

        self.assertEqual(self.lofThreshold(), 3)

class FaceplateAlarmForce(AnnaMroTestCase):
    _serdesIds = None

    @classmethod
    def serdesIds(cls):
        if cls._serdesIds is not None:
            return cls._serdesIds

        ids = [0]
        if cls.is20G():
            ids.append(8)

        cls._serdesIds = ids

        return cls._serdesIds

    @classmethod
    def serdesCliIds(cls):
        return ",".join(["%d" % (serdesId + 1) for serdesId in cls.serdesIds()])

    @classmethod
    def ethPortCliIds(cls):
        return ",".join(["%d" % (serdesId + 2) for serdesId in cls.serdesIds()])

    @classmethod
    def setUpClass(cls):
        cls.assertClassCliSuccess("device init")
        cls.assertClassCliSuccess("serdes mode %s 10G" % cls.serdesCliIds())

    def setUp(self):
        self.assertCliSuccess("eth port force alarm %s local-fault|remote-fault rx dis" % self.ethPortCliIds())
        self.assertCliSuccess("eth port force alarm %s local-fault|remote-fault tx dis" % self.ethPortCliIds())

    def TestForceAlarm(self, alarmName, direction):
        """
        :type alarmName: str
        :type direction: str
        """
        reverseDirection = "tx" if direction == "rx" else "rx"

        for enabled in [True, False, True]:
            enableString = "en" if enabled else "dis"
            self.assertCliSuccess("eth port force alarm %s %s %s %s" % (self.ethPortCliIds(), alarmName, direction, enableString))
            cliResult = self.runCli("show eth port forcedalarms %s" % self.ethPortCliIds())
            self.assertEqual(cliResult.numRows(), len(self.serdesIds()))
            for row in range(cliResult.numRows()):
                cellContent = cliResult.cellContent(row, "Forced%sAlarms" % direction.capitalize()).lower()
                expected = alarmName if enabled else "none"
                self.assertEqual(cellContent, expected)
                self.assertEqual(cliResult.cellContent(row, "Forced%sAlarms" % reverseDirection.capitalize()).lower(), "none")

    def TestCannotForceTxAlarm(self, alarmName):
        for enabled in [True, False, True]:
            enableString = "en" if enabled else "dis"
            cli = "eth port force alarm %s %s tx %s" % (self.ethPortCliIds(), alarmName, enableString)
            if enabled:
                self.assertCliFailure(cli)
            else:
                self.assertCliSuccess(cli)

            cliResult = self.runCli("show eth port forcedalarms %s" % self.ethPortCliIds())
            self.assertEqual(cliResult.numRows(), len(self.serdesIds()))

            for row in range(cliResult.numRows()):
                self.assertEqual(cliResult.cellContent(row, "ForcedRxAlarms").lower(), "none")
                self.assertEqual(cliResult.cellContent(row, "ForcedTxAlarms").lower(), "none")

    def testForceRxLf(self):
        self.TestForceAlarm(alarmName="local-fault", direction="rx")

    def testForceRxRf(self):
        self.TestForceAlarm(alarmName="remote-fault", direction="rx")

    def testForceTxLf(self):
        self.TestForceAlarm(alarmName="local-fault", direction="tx")

    def testForceTxRf(self):
        self.TestForceAlarm(alarmName="remote-fault", direction="tx")

class Mro20GDefaultPmcTickMode(AnnaMroTestCase):
    @classmethod
    def canRun(cls):
        return cls.is20G()

    def getTickMode(self):
        result = self.runCli("show module pmc")
        return result.cellContent("TickMode", "Value")

    def testTickModeMustBeSetToAutoByDefault(self):
        self.assertCliSuccess("device init")
        self.assertCliSuccess("debug module pmc tick mode manual")
        self.assertEqual(self.getTickMode(), "manual")
        self.assertCliSuccess("device init")
        self.assertEqual(self.getTickMode(), "auto")

class EthDefaultInterruptMask(AnnaMroTestCase):
    def setUp(self):
        self.assertCliSuccess("serdes mode %s %s" % (self.portCliIds(), self.defaultMode()))
        self.assertCliSuccess("eth port interruptmask %s %s en" % (self.ethPortCliId(), self.allMasksString()))

    def defaultMode(self):
        return "Invalid"

    def oneGePortIds(self):
        return range(self.numFaceplatePorts())

    @classmethod
    def tenGePortIds(cls):
        if cls.is20G():
            return [0, 8]
        return [0]

    def portIds(self):
        return list()

    @staticmethod
    def ethIdsFromPortId(portIds):
        return [portId + 1 for portId in portIds]

    def ethPortIds(self):
        return self.ethIdsFromPortId(self.portIds())

    @staticmethod
    def cliIdsFromDriverIds(driverIds):
        return ",".join(["%d" % (portId + 1) for portId in driverIds])

    def portCliIds(self):
        return self.cliIdsFromDriverIds(self.portIds())

    def ethPortCliId(self):
        return self.cliIdsFromDriverIds(self.ethPortIds())

    def allMasks(self):
        return list()

    def allMasksString(self):
        return "|".join(self.allMasks())

    @classmethod
    def setUpClass(cls):
        cls.assertClassCliSuccess("device init")

    def assertInterruptMaskDisabled(self, ethPortIds = None):
        if ethPortIds is None:
            ethPortIds = self.ethPortIds()

        for portId in ethPortIds:
            result = self.runCli("show eth port interruptmask %d" % (portId + 1))
            for alarmName in self.allMasks():
                enableString = result.cellContent(alarmName, columnIdOrName=1)
                self.assertEqual(enableString, "dis")

    def setDefaultMode(self):
        self.assertCliSuccess("serdes mode %s %s" % (self.portCliIds(), self.defaultMode()))

    def testInterruptMaskMustBeDisabledAfterDeviceInit(self):
        self.assertCliSuccess("device init")
        self.setDefaultMode()
        self.assertInterruptMaskDisabled()

    def testInterruptMaskMustBeDisabledAfterChangingToStmMode(self):
        self.assertCliSuccess("serdes mode %s stm4" % self.portCliIds())
        self.setDefaultMode()
        self.assertInterruptMaskDisabled()

    def testInterruptMaskMustBeDisabledAfterChangingBetween1G_10G(self):
        self.fail("Implement this please")

class OneGeEthDefaultInterruptMask(EthDefaultInterruptMask):
    def defaultMode(self):
        return "1G"

    def portIds(self):
        return self.oneGePortIds()

    def allMasks(self):
        return ["remote-fault", "excessive-error-ratio", "loss-of-data-sync", "auto-neg-state-change"]

    def testInterruptMaskMustBeDisabledAfterChangingBetween1G_10G(self):
        self.assertCliSuccess("serdes mode %s 10G" % self.cliIdsFromDriverIds(self.tenGePortIds()))
        self.assertCliSuccess("serdes mode %s 1G" % self.cliIdsFromDriverIds(self.oneGePortIds()))
        self.assertInterruptMaskDisabled(ethPortIds=self.ethIdsFromPortId(self.tenGePortIds()))

class TenGeEthDefaultInterruptMask(EthDefaultInterruptMask):
    def defaultMode(self):
        return "10G"

    def portIds(self):
        return self.tenGePortIds()

    def allMasks(self):
        return ["local-fault", "remote-fault", "excessive-error-ratio", "loss-of-clock", "frequency-out-of-range", "loss-of-frame"]

    def testInterruptMaskMustBeDisabledAfterChangingBetween1G_10G(self):
        self.assertCliSuccess("serdes mode %s 1G" % self.cliIdsFromDriverIds(self.oneGePortIds()))
        self.assertCliSuccess("serdes mode %s 10G" % self.cliIdsFromDriverIds(self.tenGePortIds()))
        self.assertInterruptMaskDisabled(ethPortIds=self.ethIdsFromPortId(self.tenGePortIds()))

class PlaExpector(Expector):
    def __init__(self, productCode):
        self.registerManager = RegisterManager.manager(productCode)

    def plaRegLookup(self, address, name):
        regProvider = self.registerManager.plaRegisterProvider()
        reg = regProvider.getRegister(name=name, addressOffset=0, readHw=False)
        if reg.startAddress() <= address <= reg.endAddress():
            return reg
        return None

    def plaShortCompareMask(self, address):
        mask = self.allMask()
        reg = self.plaRegLookup(address, "pla_out_psnpro_ctrl")
        if reg:
            field = reg.fieldByName("PlaOutPsnProRnWCtr")
            mask = mask & (~field.mask())

        return mask

    def shortCompareMask(self, address):
        if self.registerManager.isPlaAddress(address):
            return self.plaShortCompareMask(address)

        return self.allMask()

class BERTOnFrameSAToPBindAndUnbind(AnnaMroTestCase):
    def testTdmBertMustBeFineWhenUnbindAndRebind(self):
        script = """
            device init
            
            xc vc connect vc3.1.1.1 vc3.25.1.1 two-way
            sdh map vc3.25.1.1 de3
            
            pdh de3 framing 25.1.1 ds3_cbit_unchannelize
            
            pw create satop 1
            pw circuit bind 1 de3.25.1.1
            pw psn 1 mpls
            pw ethport 1 1
            pw enable 1
            
            prbs engine create de3 1 25.1.1
            prbs engine enable 1
            """

        regCollector = RegisterCollector()
        regCollector.start()
        self.assertCliSuccess(script)
        regCollector.stop()

        script = """
            pw circuit unbind 1
            pw circuit bind 1 de3.25.1.1
            """
        self.assertCliSuccess(script)

        regCollector.expector = PlaExpector(self.productCode())
        self.assertTrue(regCollector.audit(), "Configuration is not same as before")

class DeviceClearStatus(AnnaMroTestCase):
    @staticmethod
    def mro20GScript():
        return """
            device init
            serdes mode 1,9 stm64
            sdh line rate 1,9 stm64
            serdes powerup 1,9
            serdes enable 1,9
            sdh line mode 1,9,25-32 sonet
            sdh map aug1.1.1-1.64,9.1-9.64,25.1-32.16 3xvc3s
            xc vc connect vc3.1.1.1-1.64.3,9.1.1-9.64.3 vc3.25.1.1-32.16.3 two-way
            sdh map vc3.25.1.1-32.16.3 7xtug2s
            sdh map tug2.25.1.1.1-32.16.3.7 tu11
            sdh map vc1x.25.1.1.1.1-32.16.3.7.4 de1
            pdh de1 framing 25.1.1.1.1-32.16.3.7.4 ds1_unframed
            pdh de1 timing 25.1.1.1.1-32.16.3.7.4 system 1
            sdh path psl expect vc3.1.1.1-1.64.3,9.1.1-9.64.3 0x2
            sdh path psl transmit vc3.1.1.1-1.64.3,9.1.1-9.64.3 0x2
            pw create satop 1-10752
            pw circuit bind 1-10752 de1.25.1.1.1.1-32.16.3.7.4
            pw jitterbuffer 1-10752 8000
            pw jitterdelay 1-10752  4000
            pw psn 1-10752 mpls
            eth port enable 1
            eth port maccheck 1 dis
            pw ethport 1-10752 1
            pw enable 1-10752
            serdes loopback 25,26 local
            """

    @staticmethod
    def mro10GScript():
        return """
            device init
            serdes mode 1 stm64
            sdh line rate 1 stm64
            serdes powerup 1
            serdes enable 1
            sdh line mode 1,25-28 sonet
            sdh map aug1.1.1-1.64,9.1-9.64,25.1-28.16 3xvc3s
            xc vc connect vc3.1.1.1-1.64.3 vc3.25.1.1-28.16.3 two-way
            sdh map vc3.25.1.1-28.16.3 7xtug2s
            sdh map tug2.25.1.1.1-28.16.3.7 tu11
            sdh map vc1x.25.1.1.1.1-28.16.3.7.4 de1
            pdh de1 framing 25.1.1.1.1-28.16.3.7.4 ds1_unframed
            pdh de1 timing 25.1.1.1.1-28.16.3.7.4 system 1
            sdh path psl expect vc3.1.1.1-1.64.3 0x2
            sdh path psl transmit vc3.1.1.1-1.64.3 0x2
            pw create satop 1-5376
            pw circuit bind 1-5376 de1.25.1.1.1.1-28.16.3.7.4
            pw jitterbuffer 1-5376 8000
            pw jitterdelay 1-5376  4000
            pw psn 1-5376 mpls
            eth port enable 1
            eth port maccheck 1 dis
            pw ethport 1-5376 1
            pw enable 1-5376
            serdes loopback 25,26 local
            """

    def script(self):
        if self.is20G():
            return self.mro20GScript()
        return self.mro10GScript()

    def testClearStatusMustNotAffectConfiguration(self):
        script = self.script()
        regCollector = RegisterCollector()
        regCollector.start()
        self.assertCliSuccess(script)
        regCollector.stop()

        self.assertCliSuccess("device clear status")
        regCollector.expector = PlaExpector(self.productCode())
        self.assertTrue(regCollector.audit(), "Configuration is affected")

class BertMode(AnnaMroTestCase):
    @classmethod
    def setUpClass(cls):
        script = """
        device init
        sdh map vc3.25.1.1 c3
        prbs engine create vc 1 vc3.25.1.1
        prbs engine enable 1
        """
        cls.assertClassCliSuccess(script)

    @staticmethod
    def supportedPrbsModes():
        return ["prbs15", "prbs23", "prbs31", "prbs20", "prbs20r"]

    def assertPrbsMode(self, txMode, rxMode):
        cliResult = self.runCli("show prbs engine 1")
        self.assertEqual(txMode, cliResult.cellContent(0, "txMode"))
        self.assertEqual(rxMode, cliResult.cellContent(0, "rxMode"))

    def testChangeRxThenTx(self):
        for mode in self.supportedPrbsModes():
            self.assertCliSuccess("prbs engine mode rx 1 %s" % mode)
            self.assertCliSuccess("prbs engine mode tx 1 %s" % mode)
            self.assertPrbsMode(txMode=mode, rxMode=mode)

    def testChangeTxThenRx(self):
        for mode in self.supportedPrbsModes():
            self.assertCliSuccess("prbs engine mode tx 1 %s" % mode)
            self.assertCliSuccess("prbs engine mode rx 1 %s" % mode)
            self.assertPrbsMode(txMode=mode, rxMode=mode)

class HotChangeDe1Frame_FrameSAToP(AnnaMroTestCase):
    @classmethod
    def setUpClass(cls):
        script = """
        device init
        
        sdh map vc3.25.1.1 7xtug2s
        sdh map tug2.25.1.1.1 tu11
        sdh map vc1x.25.1.1.1.1 de1
        pw create satop 1
        pw circuit bind 1 de1.25.1.1.1.1
        pw psn 1 mpls
        pw ethport 1 1
        """
        cls.assertClassCliSuccess(script)

    def testHotChangeAnyFrame(self):
        supportedFrames = ["ds1_unframed", "ds1_sf", "ds1_esf"]
        for frame in supportedFrames:
            self.assertCliSuccess("pdh de1 framing 25.1.1.1.1 %s" % frame)
         
        self.assertCliFailure("sdh map tug2.25.1.1.1 tu12")
        self.assertCliSuccess("pw circuit bind 1 none")
        self.assertCliSuccess("sdh map tug2.25.1.1.1 tu12")
        self.assertCliSuccess("sdh map vc1x.25.1.1.1.1 de1")
        
        script = """
        pdh de1 framing 25.1.1.1.1 e1_unframed
        pw create satop 1
        pw circuit bind 1 de1.25.1.1.1.1
        pw psn 1 mpls
        pw ethport 1 1
        """
        self.assertCliSuccess(script)
        
        supportedFrames = ["e1_unframed", "e1_basic", "e1_crc"]
        for frame in supportedFrames:
            self.assertCliSuccess("pdh de1 framing 25.1.1.1.1 %s" % frame)

class BertSideSettingFail(AnnaMroTestCase):
    def testCanChangeAnySideWithoutError(self):
        script = """
        device init
        xc vc connect vc3.25.1.1-25.1.1 vc3.1.1.1 two-way
        pw create cep 1 basic
        pw circuit bind 1 vc3.25.1.1-25.1.1
        pw psn 1 mpls
        eth port enable 1
        eth port srcmac 1 00.01.44.55.66.88
        pw ethport 1 1
        pw ethheader 1 00.01.44.55.66.77 none none
        pw enable 1
        
        prbs engine create vc 1 vc3.25.1.1
        prbs engine enable 1
        prbs engine side monitoring 1 psn
        prbs engine side monitoring 1 tdm
        prbs engine side monitoring 1 psn
        
        prbs engine side generating 1 psn
        prbs engine side generating 1 tdm
        prbs engine side generating 1 psn
        """
        self.assertCliSuccess(script)

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(MroBugTest)
    runner.run(MroDs1FarEndTcaMask)
    runner.run(MroE1LofThreshold)
    runner.run(FaceplateAlarmForce)
    runner.run(Mro20GDefaultPmcTickMode)
    runner.run(OneGeEthDefaultInterruptMask)
    runner.run(TenGeEthDefaultInterruptMask)
    runner.run(BERTOnFrameSAToPBindAndUnbind)
    runner.run(DeviceClearStatus)
    runner.run(BertMode)
    runner.run(HotChangeDe1Frame_FrameSAToP)
    runner.run(BertSideSettingFail)
    runner.summary()

if __name__ == '__main__':
    TestMain()
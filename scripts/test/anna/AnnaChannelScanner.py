"""
Created on Apr 14, 2017

@author: namnng
"""
from python.arrive.AtAppManager.AtColor import AtColor
from python.arrive.atsdk.cli import AtCliRunnable
from python.arrive.atsdk.device import AtDevice
from python.arrive.atsdk.pdh import AtPdhChannel


class CircuitScanner(AtCliRunnable):
    def __init__(self):
        AtCliRunnable.__init__(self)
        self.channels = []
        
    @staticmethod
    def device():
        return AtDevice.currentDevice()
    
    def sdhModule(self):
        return self.device().sdhModule()
    
    def scanPdhSubChannel(self, pdhChannel):
        circuits = []
        subChannels = pdhChannel.subChannels
        
        if len(subChannels) == 0:
            pdhChannel.reloadBoundPw()
            return [pdhChannel]
        
        for subChannel in subChannels:
            circuits.extend(self.scanPdhSubChannel(subChannel))
        
        return circuits
    
    def scanMapChannel(self, mapChannel):
        assert(isinstance(mapChannel, AtPdhChannel))
        return self.scanPdhSubChannel(mapChannel)
        
    def scanSdhSubChannels(self, sdhChannel):
        circuits = []
        subChannels = sdhChannel.subChannels
        
        if len(subChannels) == 0:
            mapChannel = sdhChannel.mapChannel()
            if mapChannel:
                return self.scanMapChannel(mapChannel)
            sdhChannel.reloadBoundPw()
            if sdhChannel.boundPw() is not None:
                return [sdhChannel]
            return list()
        
        for subChannel in subChannels:
            circuits.extend(self.scanSdhSubChannels(subChannel))
        
        return circuits
    
    @staticmethod
    def isFaceplateLine(line):
        return line.channelId in range(16)
    
    @staticmethod
    def isMateLine(line):
        return line.channelId in range(16, 24)
        
    @staticmethod
    def isTerminatedLine(line):
        return line.channelId in range(24, 32)
    
    def xcHidingMode(self):
        result = self.runCli("show ciena mro module xc")
        return result.cellContent("XcHiding", 1)
    
    def shouldScanLine(self, line):
        xcHidingMode = self.xcHidingMode()
        
        if xcHidingMode == "none":
            if self.isTerminatedLine(line):
                return True
            
        if xcHidingMode == "direct":
            return self.isFaceplateLine(line)
        
        return False
        
    def scan(self):
        lines = self.sdhModule().lines()
        for line in lines:
            if self.shouldScanLine(line):
                self.channels.extend(self.scanSdhSubChannels(line))
        return self.channels

    def printOutput(self):
        channelsHaveNoPws = []
        
        for channel in self.channels:
            boundPw = channel.boundPw()
            if boundPw:
                print "%s.%s: %s.%s" % (channel.cliType(), channel.cliId(), boundPw.cliType(), boundPw.cliId()) 
            else:
                channelsHaveNoPws.append(channel)
                
        print "Total: %d circuits" % len(self.channels)
        
        if len(channelsHaveNoPws) == 0:
            print AtColor.GREEN + "All channels have bound PWs" + AtColor.CLEAR
        else:
            print AtColor.RED + "Channels that do not have PWs:" + AtColor.CLEAR
            for channel in channelsHaveNoPws:
                print AtColor.RED + "%s.%s" % (channel.cliType(), channel.cliId())
        
if __name__ == '__main__':
    scanner = CircuitScanner()
    scanner.scan()
    scanner.printOutput()

from test.AtTestCase import AtTestCaseRunner
from test.anna.AnnaTestCase import AnnaMroTestCase
from test.anna.randomizer import AnnaRandomizer

#import pydevd
#pydevd.settrace('localhost')

class IdConvertTest(AnnaMroTestCase):
    @classmethod
    def randomConfigureAndApply(cls):
        cls.assertClassCliSuccess("device init")
        cls.assertClassCliSuccess("debug ciena mro xc hide direct")
        
        randomizer = AnnaRandomizer.randomizer(cls.productCode())
        configure = randomizer.makeDefault()
        def applyCli(cli):
            cls.assertClassCliSuccess(cli)

        randomizer.traverse(configure, applyCli)
        
        cls.assertClassCliSuccess("debug ciena mro xc hide none")

    @classmethod
    def setUpClass(cls):
        cls.randomConfigureAndApply()

    def _scanPaths(self, idList):
        paths = list()
        for idListString in idList:
            cliResult = self.runCli("show sdh path %s" % idListString)
            for rowId in range(cliResult.numRows()):
                pathCliId = cliResult.cellContent(rowId=rowId, columnIdOrName="PathId")
                paths.append(pathCliId)

        return paths

    def scanAllAus(self):
        idList = ["au4_64c.1.1-%d.1"  % self.numFaceplatePorts(),
                  "au4_16c.1.1-%d.64" % self.numFaceplatePorts(),
                  "au4_4c.1.1-%d.64"  % self.numFaceplatePorts(),
                  "au4.1.1-%d.64"     % self.numFaceplatePorts(),
                  "au3.1.1.1-%d.64.3" % self.numFaceplatePorts(),
                  "au3.1-%d"          % self.numFaceplatePorts()]
        return self._scanPaths(idList)

    def scanAllTu3s(self):
        idList = list()

        for i in range(self.numTerminatedLines()):
            lineCliId = self.terminatedLineStartCliId() + i + 1
            idList.append("tu3.%d.1.1-%d.16.3" % (lineCliId, lineCliId))

        return self._scanPaths(idList)

    def scanAllTu1xs(self):
        idList = list()

        for i in range(self.numTerminatedLines()):
            lineCliId = self.terminatedLineStartCliId() + i + 1
            idList.append("tu1x.%d.1.1.1.1-%d.16.3.7.4" % (lineCliId, lineCliId))

        return self._scanPaths(idList)

    def testAuIdConverting(self):
        paths = self.scanAllAus()
        for pathCliId in paths:
            # Find HW ID
            cliResult = self.runCli("debug show sdh channel hardware id %s sur" % pathCliId)
            cellContent = cliResult.cellContent(rowId=0, columnIdOrName="sur")
            firstIdString = cellContent.split("|")[0]
            [sliceId, stsId] = [int(string) for string in firstIdString.split(",")]

            # The corresponding SW ID lookup from HW ID must match
            cliResult = self.runCli("debug show sdh channel software id au.%d.%d sur" % (sliceId, stsId))
            cellContent = cliResult.cellContent(rowId=0, columnIdOrName="SwId")
            self.assertEqual(cellContent, pathCliId)

    def testTu1xIdConverting(self):
        paths = self.scanAllTu1xs()
        for pathCliId in paths:
            # Find HW ID
            inputCliId = pathCliId
            inputCliId = inputCliId.replace("tu11", "tu1x")
            inputCliId = inputCliId.replace("tu12", "tu1x")
            cliResult = self.runCli("debug show sdh channel hardware id %s sur" % inputCliId)
            cellContent = cliResult.cellContent(rowId=0, columnIdOrName="sur")
            [sliceId, stsId] = [int(string) for string in cellContent.split(",")]

            # The corresponding SW ID lookup from HW ID must match
            [_, _, _, vtgCliId, vtCliId] = [int(string) for string in pathCliId.split(".")[1:]]
            cliResult = self.runCli("debug show sdh channel software id tu.%d.%d.%d.%d sur" % (sliceId, stsId, vtgCliId - 1, vtCliId - 1))
            cellContent = cliResult.cellContent(rowId=0, columnIdOrName="SwId")
            self.assertEqual(cellContent, pathCliId)

    def testTu3IdConverting(self):
        paths = self.scanAllTu3s()
        for pathCliId in paths:
            # Find HW ID
            cliResult = self.runCli("debug show sdh channel hardware id %s sur" % pathCliId)
            cellContent = cliResult.cellContent(rowId=0, columnIdOrName="sur")
            [sliceId, stsId] = [int(string) for string in cellContent.split(",")]

            # The corresponding SW ID lookup from HW ID must match
            cliResult = self.runCli("debug show sdh channel software id tu.%d.%d.0.0 sur" % (sliceId, stsId))
            cellContent = cliResult.cellContent(rowId=0, columnIdOrName="SwId")
            self.assertEqual(cellContent, pathCliId)

def TestMain():
    for _ in range(10):
        runner = AtTestCaseRunner.runner()
        runner.run(IdConvertTest)
        if not runner.summary():
            break

if __name__ == '__main__':
    TestMain()

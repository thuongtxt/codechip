"""
Created on Apr 16, 2017

@author: namnn
"""

class AnnaMroScriptProvider(object):
    @staticmethod
    def stm16Vc15CepScript():
        return """
        device init
        
        serdes mode 1-8 stm16
        serdes powerup 1-8
        serdes enable 1-8
        sdh line mode 1,3,5,7 sonet
        sdh line rate 1,3,5,7 stm16
        
        sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,25.1-28.16 3xvc3s
        sdh map vc3.25.1.1-28.16.3 7xtug2s
        sdh map tug2.25.1.1.1-28.16.3.7 tu11
        sdh map vc1x.25.1.1.1.1-28.16.3.7.4 c1x
        
        xc vc connect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 vc3.25.1.1-28.16.3 two-way
        
        sdh path tti expect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 16bytes MRO null_padding
        sdh path tti transmit vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 16bytes MRO null_padding
        sdh path tti monitor disable vc1x.25.1.1.1.1-28.16.3.7.4
        sdh path psl expect vc1x.25.1.1.1.1-28.16.3.7.4 0x6
        
        pw create cep 1-5376 basic
        pw psn 1-5376 mpls
        pw ethport 1-5376 1
        pw jitterbuffer 1-5376 8000
        pw jitterdelay 1-5376  4000
        pw circuit bind 1-5376 vc1x.25.1.1.1.1-28.16.3.7.4
        pw enable 1-5376
        """
    
    @staticmethod
    def stm16Sts1CepScript():
        return """
        device init
        
        serdes mode 1-8 stm16
        serdes powerup 1-8
        serdes enable 1-8
        sdh line mode 1,3,5,7 sonet
        sdh line rate 1,3,5,7 stm16
        
        sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,25.1-28.16 3xvc3s
        
        xc vc connect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 vc3.25.1.1-28.16.3 two-way
        
        sdh path psl expect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 0xFE
        sdh path tti monitor disable vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3
        
        pw create cep 1-192 basic
        pw psn 1-192 mpls
        pw ethport 1-192 1
        pw jitterbuffer 1-192 4000
        pw jitterdelay 1-192  2000
        pw circuit bind 1-192 vc3.25.1.1-28.16.3
        pw enable 1-192
        """
    
    @staticmethod
    def stm16Sts3cScript():
        return """
        device init
        
        serdes mode 1-8 stm16
        serdes powerup 1-8
        serdes enable 1-8
        sdh line mode 1,3,5,7 sonet
        sdh line rate 1,3,5,7 stm16
        
        sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,25.1-28.16 vc4
        sdh map vc4.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,25.1-28.16 c4
        
        xc vc connect vc4.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16 vc4.25.1-28.16 two-way
        
        sdh path psl expect vc4.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16 0xFE
        sdh path tti monitor disable vc4.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16
        
        pw create cep 1-64 basic
        pw psn 1-64 mpls
        pw ethport 1-64 1
        pw jitterbuffer 1-64 4000
        pw jitterdelay 1-64  2000
        pw circuit bind 1-64 vc4.25.1-28.16
        pw enable 1-64
        """
    
    @staticmethod
    def stm16Sts12cScript():
        return """
        device init
        
        serdes mode 1-8 stm16
        serdes powerup 1-8
        serdes enable 1-8
        sdh line mode 1,3,5,7 sonet
        sdh line rate 1,3,5,7 stm16
        
        sdh map aug4.1.1-1.4,3.1-3.4,5.1-5.4,7.1-7.4,25.1-28.4 vc4_4c
        sdh map vc4_4c.1.1-1.4,3.1-3.4,5.1-5.4,7.1-7.4,25.1-28.4 c4_4c
        
        xc vc connect vc4_4c.1.1-1.4,3.1-3.4,5.1-5.4,7.1-7.4 vc4_4c.25.1-28.4 two-way
        
        sdh path psl expect vc4_4c.1.1-1.4,3.1-3.4,5.1-5.4,7.1-7.4 0xFE
        sdh path tti monitor disable vc4_4c.1.1-1.4,3.1-3.4,5.1-5.4,7.1-7.4
        
        pw create cep 1-8 basic
        pw psn 1-8 mpls
        pw ethport 1-8 1
        pw jitterbuffer 1-8 4000
        pw jitterdelay 1-8  2000
        pw circuit bind 1-8 vc4_4c.25.1-28.16
        pw enable 1-8
        """
    
    @staticmethod
    def stm16Sts48cScript():
        return """
        device init
        
        serdes mode 1-8 stm16
        serdes powerup 1-8
        serdes enable 1-8
        sdh line mode 1,3,5,7 sonet
        sdh line rate 1,3,5,7 stm16
        
        sdh map aug16.1.1,3.1,5.1,7.1,25.1-28.1 vc4_16c
        sdh map vc4_16c.1.1,3.1,5.1,7.1,25.1-28.1 c4_16c
        
        xc vc connect vc4_16c.1.1,3.1,5.1,7.1 vc4_16c.25.1-28.1 two-way
        
        sdh path psl expect vc4_16c.1.1,3.1,5.1,7.1 0xFE
        sdh path tti monitor disable vc4_16c.1.1,3.1,5.1,7.1
        
        pw create cep 1-4 basic
        pw psn 1-4 mpls
        pw ethport 1-4 1
        pw jitterbuffer 1-4 4000
        pw jitterdelay 1-4  2000
        pw circuit bind 1-4 vc4_16c.25.1-28.1
        pw enable 1-4
        """
    
    @staticmethod
    def stm16Vc11Ds1Satop():
        return """
        device init
        
        serdes mode 1-8 stm16
        serdes powerup 1-8
        serdes enable 1-8
        sdh line mode 1,3,5,7 sonet
        sdh line rate 1,3,5,7 stm16
        
        sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,25.1-28.16 3xvc3s
        sdh map vc3.25.1.1-28.16.3 7xtug2s
        sdh map tug2.25.1.1.1-28.16.3.7 tu11
        sdh map vc1x.25.1.1.1.1-28.16.3.7.4 de1
        pdh de1 framing 25.1.1.1.1-28.16.3.7.4 ds1_unframed
        
        sdh path tti expect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 16bytes MRO null_padding
        sdh path tti transmit vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 16bytes MRO null_padding
        
        xc vc connect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 vc3.25.1.1-28.16.3 two-way
        
        pw create satop 1-5376
        pw psn 1-5376 mpls
        pw ethport 1-5376 1
        pw circuit bind 1-5376 de1.25.1.1.1.1-28.16.3.7.4
        pw enable 1-5376
        """
        
    @staticmethod
    def stm16Vt2Cep():
        return """
        device init
        
        serdes mode 1-8 stm16
        serdes powerup 1-8
        serdes enable 1-8
        sdh line mode 1,3,5,7 sonet
        sdh line rate 1,3,5,7 stm16
        
        sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,25.1-28.16 3xvc3s
        sdh map vc3.25.1.1-28.16.3 7xtug2s
        sdh map tug2.25.1.1.1-28.16.3.7 tu12
        sdh map vc1x.25.1.1.1.1-28.16.3.7.3 c1x
        
        xc vc connect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 vc3.25.1.1-28.16.3 two-way
        
        # Configure paths
        sdh path tti expect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 16bytes MRO null_padding
        sdh path tti transmit vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 16bytes MRO null_padding
        sdh path tti monitor disable vc1x.25.1.1.1.1-28.16.3.7.3
        sdh path psl expect vc1x.25.1.1.1.1-28.16.3.7.3 0x6
        
        pw create cep 1-4032 basic
        pw psn 1-4032 mpls
        pw ethport 1-4032 1
        pw circuit bind 1-4032 vc1x.25.1.1.1.1-28.16.3.7.3
        pw enable 1-4032
        """
        
    @staticmethod
    def stm16E1Satop():
        return """
        device init
        
        serdes mode 1-16 stm16
        serdes powerup 1-16
        serdes enable 1-16
        sdh line mode 1,3,5,7 sonet
        sdh line rate 1,3,5,7 stm16
        
        sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,25.1-28.16 3xvc3s
        sdh map vc3.25.1.1-28.16.3 7xtug2s
        sdh map tug2.25.1.1.1-28.16.3.7 tu12
        sdh map vc1x.25.1.1.1.1-28.16.3.7.3 de1
        pdh de1 framing 25.1.1.1.1-28.16.3.7.3 e1_unframed
        
        sdh path tti expect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 16bytes MRO null_padding
        sdh path tti transmit vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 16bytes MRO null_padding
        
        xc vc connect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 vc3.25.1.1-28.16.3 two-way
        
        pw create satop 1-4032
        pw psn 1-4032 mpls
        pw ethport 1-4032 1
        pw circuit bind 1-4032 de1.25.1.1.1.1-28.16.3.7.3
        pw enable 1-4032
        """
    
    @staticmethod
    def stm16Ds3SatoP():
        return """
        device init
        
        serdes mode 1-16 stm16
        serdes powerup 1-16
        serdes enable 1-16
        sdh line mode 1,3,5,7 sonet
        sdh line rate 1,3,5,7 stm16
        
        sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,25.1-28.16 3xvc3s
        sdh map vc3.25.1.1-28.16.3 de3
        
        sdh path psl transmit vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 0x4
        sdh path psl expect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 0x4
        
        pdh de3 framing 25.1.1-28.16.3 ds3_unframed 
        
        xc vc connect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 vc3.25.1.1-28.16.3 two-way
        
        pw create satop 1-192
        pw psn 1-192 mpls
        pw ethport 1-192 1
        pw jitterbuffer 1-192 8000
        pw jitterdelay 1-192  4000
        pw circuit bind 1-192 de3.25.1.1-28.16.3
        pw enable 1-192
        """
    
    @staticmethod
    def ds3CBit28DS1SAToP():
        return """
        # Initialize device
        device init
        
        # Configure Line
        serdes mode 1-16 stm16
        serdes powerup 1-16
        serdes enable 1-16
        sdh line mode 1,3,5,7 sonet
        sdh line rate 1,3,5,7 stm16
        
        # Configure mapping
        sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,25.1-28.16 3xvc3s
        sdh map vc3.25.1.1-28.16.3 de3
        
        # Configure path
        sdh path psl transmit vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 0x4
        sdh path psl expect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 0x4
        
        # Configure DS3
        pdh de3 framing 25.1.1-28.16.3 ds3_cbit_28ds1
        pdh de1 framing 25.1.1.1.1-28.16.3.7.4 ds1_unframed
        
        # TSI
        xc vc connect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 vc3.25.1.1-28.16.3 two-way
        
        # Setup PWs
        pw create satop 1-5376
        pw psn 1-5376 mpls
        pw ethport 1-5376 1
        pw jitterbuffer 1-5376 8000
        pw jitterdelay 1-5376  4000
        pw circuit bind 1-5376 de1.25.1.1.1.1-28.16.3.7.4
        pw enable 1-5376
        """
    
    @staticmethod
    def ds3M13DS1SAToP():
        return """
        # Initialize device
        device init
        
        # Configure Line
        serdes mode 1-16 stm16
        serdes powerup 1-16
        serdes enable 1-16
        sdh line mode 1,3,5,7 sonet
        sdh line rate 1,3,5,7 stm16
        
        # Configure mapping
        sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,25.1-28.16 3xvc3s
        sdh map vc3.25.1.1-28.16.3 de3
        
        # Configure path
        sdh path psl transmit vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 0x4
        sdh path psl expect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 0x4
        
        # Configure DS3
        pdh de3 framing 25.1.1-28.16.3 ds3_m13_28ds1
        pdh de1 framing 25.1.1.1.1-28.16.3.7.4 ds1_unframed
        
        # TSI
        xc vc connect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 vc3.25.1.1-28.16.3 two-way
        
        # Setup PWs
        pw create satop 1-5376
        pw psn 1-5376 mpls
        pw ethport 1-5376 1
        pw jitterbuffer 1-5376 8000
        pw jitterdelay 1-5376  4000
        pw circuit bind 1-5376 de1.25.1.1.1.1-28.16.3.7.4
        pw enable 1-5376
        """
        
    @staticmethod
    def e3SAToP():
        return """
        # Initialize device
        device init
        
        # Configure Line
        serdes mode 1-16 stm16
        serdes powerup 1-16
        serdes enable 1-16
        sdh line mode 1,3,5,7 sonet
        sdh line rate 1,3,5,7 stm16
        
        # Configure mapping
        sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,25.1-28.16 3xvc3s
        sdh map vc3.25.1.1-28.16.3 de3
        
        # Configure path
        sdh path psl transmit vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 0x4
        sdh path psl expect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 0x4
        
        # Configure E3
        pdh de3 framing 25.1.1-28.16.3 e3_unframed 
        
        # TSI
        xc vc connect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 vc3.25.1.1-28.16.3 two-way
        
        # Setup PWs
        pw create satop 1-192
        pw psn 1-192 mpls
        pw ethport 1-192 1
        pw jitterbuffer 1-192 8000
        pw jitterdelay 1-192  4000
        pw circuit bind 1-192 de3.25.1.1-28.16.3
        pw enable 1-192
        """
    
    @staticmethod
    def e3M13E1SAToP():
        return """
        # Initialize device
        device init
        
        # Configure Line
        serdes mode 1-16 stm16
        serdes powerup 1-16
        serdes enable 1-16
        sdh line mode 1,3,5,7 sonet
        sdh line rate 1,3,5,7 stm16
        
        # Configure mapping
        sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16,25.1-28.16 3xvc3s
        sdh map vc3.25.1.1-28.16.3 de3
        
        # Configure DS3
        pdh de3 framing 25.1.1-28.16.3 e3_g751_16e1s
        pdh de1 framing 25.1.1.1.1-28.16.3.4.4 e1_unframed
        
        # TSI
        xc vc connect vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3 vc3.25.1.1-28.16.3 two-way
        
        # Setup PWs
        pw create satop 1-3072
        pw psn 1-3072 mpls
        pw ethport 1-3072 1
        pw circuit bind 1-3072 de1.25.1.1.1.1-28.16.3.7.4
        pw enable 1-3072"""
    
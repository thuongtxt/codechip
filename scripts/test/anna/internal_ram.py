from python.arrive.AtAppManager.AtAppManager import AtAppManager
import test.cisco.internal_ram as internal_ram

class Tha60290022RamManager(internal_ram.RamManager):
    def numVisibleRams(self):
        return 284

    def eccMonSupported(self, ram):
        supportedRams = ["PDA link list ECC"]
        if ram.ramDescription in supportedRams:
            return True
        return False

    def paritySupported(self, ram):
        notSupportedRams = ["PLA CRC Error",
                            "PDA data CRC error",
                            "PDA link list ECC"]
        if ram.ramDescription in notSupportedRams:
            return False
        return True

    def crcMonSupported(self, ram):
        supportedRams = ["PLA CRC Error", "PDA data CRC error"]
        if ram.ramDescription in supportedRams:
            return True
        return False

    def errorGeneratorSupported(self, ram):
        supportedRams = ["PLA CRC Error",
                         "PDA data CRC error",
                         "PDA link list ECC"]
        if ram.ramDescription in supportedRams:
            return True
        return False

    def errorsCanBeGenerated(self, ram):
        crcGeneratableRams = ["PLA CRC Error",
                              "PDA data CRC error"]
        if ram.ramDescription in crcGeneratableRams:
            return [internal_ram.InternalRam.CRC]

        eccGeneratableRams = ["PDA link list ECC"]
        if ram.ramDescription in eccGeneratableRams:
            return [internal_ram.InternalRam.ECC_CORRECTABLE, internal_ram.InternalRam.ECC_UNCORRECTABLE]


class Tha60290021RamManager(internal_ram.Tha60210051RamManager):
    def numVisibleRams(self):
        return 337

def ramManagerCreate():
    productCode = AtAppManager.localApp().productCode()
    managerClasses = {0x60290022: Tha60290022RamManager,
                      0x60290021: Tha60290021RamManager}
    
    if productCode in managerClasses:
        return managerClasses[productCode]()
    else:
        return None
    
def TestMain():
    manager = ramManagerCreate()
    if manager is None:
        return

    internal_ram.TestWithManager(manager)

if __name__ == '__main__':
    TestMain()


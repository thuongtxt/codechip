import random
import sys
from ha.Af60210051Randomizer import Af60210051Randomizer
from ha.Af60290011Randomizer import Af60290011Randomizer

class AnnaRandomizer(Af60210051Randomizer):
    @classmethod
    def randomizer(cls, productCode):
        if productCode == 0x60290021: return Mro10GRandomizer()
        if productCode == 0x60290022: return Mro20GRandomizer()
        if productCode == 0x60290011: return Af60290011Randomizer()

        return None

    def numLines(self):
        return sys.maxint # To catch any problem

    def numPws(self):
        return self.numLines() * 1344

    def makeDefault(self):
        self.numLinesSet(self.numLines())
        self.numPwsSet(self.numPws())
        self.numEthPortsSet(1)
        self.psnLabelStartIdSet(45000)
        return self.random()

    @staticmethod
    def stm16Group(lineId):
        return lineId / 2

    def lineShouldBeHidden(self, lineId, selectedLinesAndRates):
        for selectedLineId, selectedRate in selectedLinesAndRates:
            if selectedRate == "stm64":
                return True

            if selectedRate == "stm16":
                if self.stm16Group(lineId) == self.stm16Group(selectedLineId):
                    return True

        return False

    @staticmethod
    def numLinesPerGroups():
        return 8

    def rateCanBeApplied(self, lineId, rate):
        if lineId % self.numLinesPerGroups() == 0:
            return True

        if rate == "stm64":
            return False

        if rate == "stm16":
            return (lineId % 2) == 0

        return True

    def randomLineIdsAndRates(self):
        selectedLinesAndRates = []

        for lineId in range(self.numLines()):
            if self.lineShouldBeHidden(lineId, selectedLinesAndRates):
                continue

            rates = ['stm64', 'stm16', 'stm4', 'stm1']
            while len(rates) > 0:
                rate = random.choice(rates)
                rates.remove(rate)
                if self.rateCanBeApplied(lineId, rate):
                    selectedLineAndRate = lineId, rate
                    selectedLinesAndRates.append(selectedLineAndRate)
                    break

        return selectedLinesAndRates

    def lineRateList(self, lineIdList, lineRateList):
        selectedLinesAndRates = self.randomLineIdsAndRates()
        for lineId, rate in selectedLinesAndRates:
            lineIdList.append(lineId + 1)
            lineRateList.append(rate)

    def aug64MapTypeIsSupported(self, mapType):
        if mapType == "vc4_64c":
            return False
        return True

    def makeLineRateClis(self, lineId, lineRate):
        clis = list()
        clis.append("serdes mode %d %s" % (lineId, lineRate))
        clis.append("serdes powerup %d" % lineId)
        clis.append("serdes enable %d" % lineId)
        clis.extend(Af60210051Randomizer.makeLineRateClis(self, lineId, lineRate))
        return clis

class Mro10GRandomizer(AnnaRandomizer):
    def numPws(self):
        return 5376

    def numLines(self):
        return 8

class Mro20GRandomizer(AnnaRandomizer):
    def numLines(self):
        return 16

    def numPws(self):
        return 10752

if __name__ == '__main__':
    for code in [0x60290021, 0x60290022]:
        ran = AnnaRandomizer.randomizer(code)
        print ran.randomLineIdsAndRates()

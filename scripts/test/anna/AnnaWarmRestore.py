import filecmp
import getopt
import os
import random
import sys
from time import sleep

from python.AtConnection import AtConnection
from python.EpApp import EpApp
from python.arrive.AtAppManager.AtColor import AtColor
from python.arrive.atsdk.AtRegister import AtRegister, cBit31_28
from python.jenkin.AtOs import AtOs
from test.AtTestCase import AtTestCase, AtTestCaseRunner
from test.anna.randomizer import AnnaRandomizer
from test.anna.AnnaTestCase import AnnaProductCodeProvider


class ScriptManager(object):
    _scriptCount = 0
    _manager = None

    def __init__(self, productCode, outputDir="."):
        self.outputDir = outputDir
        self.productCode = productCode

    def createRandomizer(self):
        return AnnaRandomizer.randomizer(self.productCode)

    def willInitDevice(self, scriptFile):
        pass

    def generate(self, numSameConfigureScripts = 1):
        """
        :return: path to generated script
        """

        scripts = list()
        randomizer = self.createRandomizer()
        lineConfigurations = randomizer.makeDefault()

        for _ in range(numSameConfigureScripts):
            # Create script file
            scriptPath = self._generateTempScriptFillePath()
            scriptFile = open(scriptPath, "w", )

            self.willInitDevice(scriptFile)
            scriptFile.write("device init\n")

            # Put random configuration to script
            def _applyDict(aDict):
                for cli in aDict["cli"]:
                    scriptFile.write("%s\n" % cli)

                subChannels = aDict["subChannels"]
                random.shuffle(subChannels)
                for subChannel in subChannels:
                    _applyDict(subChannel)

            for lineConfiguration in lineConfigurations:
                _applyDict(lineConfiguration)

            scriptFile.close()

            scripts.append(scriptPath)

        if numSameConfigureScripts == 1:
            return scripts[0]

        return scripts

    def _generateTempScriptFillePath(self):
        scriptName = "%s/.temp.%d.script" % (self.outputDir, ScriptManager._scriptCount)
        ScriptManager._scriptCount = ScriptManager._scriptCount + 1
        return scriptName

    @classmethod
    def manager(cls, productCode, outputDir):
        if AnnaProductCodeProvider.isMroProduct(productCode):
            return MroScriptManager(productCode, outputDir)

        if AnnaProductCodeProvider.isPdhProduct(productCode):
            return PdhScriptManager(productCode, outputDir)

        return None

class PdhScriptManager(ScriptManager):
    pass

class MroScriptManager(ScriptManager):
    def willInitDevice(self, scriptFile):
        scriptFile.write("debug ciena mro xc hide direct\n")

class OptionParser(object):
    _sharedParser = None

    def __init__(self):
        self.halPort = None
        self.simulate = False
        self.epappPath = None
        self.productCode = None
        self.repeatTimes = 1
        self.boardIpAddress = None
        self.outputFolder = "."
        self.silent = False

    @staticmethod
    def printUsage(exitStatus = None):
        print "Usage: " + sys.argv[0] + " [--hal-port=<HAL port>] [--product-code=<productCode>] --epapp=<epapp path> " \
                                        "[--simulate] [--repeat-times=<repeat times>] [--board-ip=<ipAddress>] " \
                                        "[--output-folder=<output folder>] [--silent]"
        if exitStatus is not None:
            sys.exit(exitStatus)

    def parse(self):
        try:
            options = ["help", "hal-port=", "repeat-times=", "product-code=", 
                       "epapp=", "verbose", "simulate", "output-folder=", "board-ip=", "silent"]
            opts, _ = getopt.getopt(sys.argv[1:], "", options)
        except:
            self.printUsage(exitStatus=1)
            sys.exit(1)

        for opt, arg in opts:
            if opt == "--hal-port":
                self.halPort = int(arg)
            if opt == "--simulate":
                self.simulate = True
            if opt == "--epapp":
                self.epappPath = arg
            if opt == "--product-code":
                self.productCode = int(arg, 16)
            if opt == "--help":
                self.printUsage(exitStatus=0)
            if opt == "--repeat-times":
                self.repeatTimes = int(arg, 10)
            if opt == "--board-ip":
                self.boardIpAddress = arg
            if opt == "--output-folder":
                self.outputFolder = arg
            if opt == "--silent":
                self.silent = True

        if self.epappPath is None:
            AtColor.printColor(AtColor.RED, "epapp path is required")
            self.printUsage(exitStatus=1)

        if self.simulate:
            if self.halPort is None:
                AtColor.printColor(AtColor.RED, "HAL port is required on simulation")
                self.printUsage(exitStatus=1)

            if self.productCode is None:
                AtColor.printColor(AtColor.RED, "Product code is required on simulation")
                self.printUsage(exitStatus=1)

        else:
            if self.boardIpAddress is None:
                AtColor.printColor(AtColor.RED, "Board IP address is required")
                self.printUsage(exitStatus=1)

    @classmethod
    def parser(cls):
        return OptionParser()

class AppManager(object):
    def __init__(self, optionParser):
        self.optionParser = optionParser
        self.platformName = "anna"
        self.silent = False

    @staticmethod
    def _putSimulatedProductCode(productCode):
        reg = AtRegister(productCode)
        reg.setField(cBit31_28, 28, 0xF)
        return reg.value

    @staticmethod
    def _putSimulationVersion(epappCommand, simulateVersion):
        assert isinstance(simulateVersion, str)
        epappCommand += " -w \"%s\"" % simulateVersion
        return epappCommand

    def onboardCommand(self):
        return "%s -p %s" % (self.optionParser.epappPath, self.platformName)

    def activeAppCommand(self, productCode, simulateVersion):
        if not self.optionParser.simulate:
            return self.onboardCommand()

        epappCommand = self.optionParser.epappPath

        if self.optionParser.simulate:
            assert isinstance(productCode, int)
            epappCommand += " -s %x -w \"ff/ff/ff ee.ee.eeee\"" % productCode

            if simulateVersion is not None:
                epappCommand = self._putSimulationVersion(epappCommand, simulateVersion)

        if self.optionParser.halPort is not None:
            assert isinstance(self.optionParser.halPort, int)
            epappCommand += " -L %d" % self.optionParser.halPort

        return epappCommand

    def standbyAppCommand(self, productCode, simulateVersion):
        if not self.optionParser.simulate:
            return self.onboardCommand()

        epappCommand = self.optionParser.epappPath
        if self.optionParser.simulate:
            assert isinstance(productCode, int)
            epappCommand += " -P %x" % self._putSimulatedProductCode(productCode)

            if simulateVersion is not None:
                epappCommand = self._putSimulationVersion(epappCommand, simulateVersion)

        if self.optionParser.halPort is not None:
            assert isinstance(self.optionParser.halPort, int)
            epappCommand += " -r 127.0.0.1:%d" % self.optionParser.halPort

        return epappCommand

    def _createApp(self, command, connection = None):
        app = EpApp(command=command, connection=connection)
        app.verbosed = False if self.silent else True
        return app

    def createConnection(self):
        if self.optionParser.simulate:
            return None
        return AtConnection.annaConnection(self.optionParser.boardIpAddress)

    def createActiveApp(self, productCode = None, simulateVersion = "ff/ff/ff ee.ee.eeee"):
        """
        :rtype: python.arrive.AtAppManager.AtAppManager.AtApp
        """
        return self._createApp(self.activeAppCommand(productCode, simulateVersion), self.createConnection())

    def createStandbyApp(self, productCode=None, simulateVersion="ff/ff/ff ee.ee.eeee"):
        """
        :rtype: python.arrive.AtAppManager.AtAppManager.AtApp
        """
        return self._createApp(self.standbyAppCommand(productCode, simulateVersion), self.createConnection())

    @classmethod
    def manager(cls, optionParser):
        return AppManager(optionParser)

class WarmRetoreTest(AtTestCase):
    appManager = None
    scriptManager = None
    optionParser = None
    activeApp = None
    standbyApp = None
    activeDb = None
    standbyDb = None

    @classmethod
    def runInEpAppContext(cls):
        return False

    @classmethod
    def productCode(cls):
        return cls.optionParser.productCode

    @staticmethod
    def alwaysCleanup():
        return False

    @staticmethod
    def scriptTimeout():
        return 30 * 60

    def setupApps(self, start=True):
        self.activeApp = self.appManager.createActiveApp(self.productCode())
        self.standbyApp = self.appManager.createStandbyApp(self.productCode())
        self.activeDb = "%s/.active.json" % self.optionParser.outputFolder
        self.standbyDb = "%s/.standby.json" % self.optionParser.outputFolder

        if start:
            self.activeApp.start()
            self.standbyApp.start()

    def stopAllApps(self):
        self.standbyApp.exit()
        self.activeApp.exit()

    def scriptVerboseDisable(self):
        self.activeApp.runCli("textui verbose script disable")
        self.standbyApp.runCli("textui verbose script disable")

    def serializeActive(self):
        self.activeApp.runCli("serialize %s" % self.activeDb)

    def serializeStandby(self):
        self.standbyApp.runCli("serialize %s" % self.standbyDb)

    def warmStart(self):
        self.standbyApp.runCli("device warm start")

    def warmStop(self):
        self.standbyApp.runCli("device warm stop")

    def runScriptOnApp(self, app, scriptFile):
        app.runScript(scriptFile, timeout=self.scriptTimeout())

    def assertDatabaseSame(self, message=None):
        sameDb = filecmp.cmp(self.activeDb, self.standbyDb)
        if message is None:
            message = "Database of active app (%s) and standby app (%s) must be the same. " % (
            self.activeDb, self.standbyDb)
        self.assertTrue(sameDb, message)

        if self.alwaysCleanup():
            os.remove(self.activeDb)
            os.remove(self.standbyDb)

    def TestRandomWithReconfigure(self, reconfigured, repeatTimes):
        print "\n"

        for i in range(repeatTimes):
            scriptFiles = self.scriptManager.generate(numSameConfigureScripts=2)

            AtColor.printColor(AtColor.GREEN, "* [%d] Active script: %s, standby script: %s..." % (i, scriptFiles[0], scriptFiles[1]))
            self.setupApps(start=True)
            self.scriptVerboseDisable()

            def runOnActive():
                self.runScriptOnApp(self.activeApp, scriptFiles[0])
                self.serializeActive()

            def runOnStandby():
                self.warmStart()
                self.runScriptOnApp(self.standbyApp, scriptFiles[1])

            # The purpose of this reconfigure is to catch the software issue that when reconfigure happen in SDK, there is
            # a case that database of software is not sync with HW
            for _ in range(2 if reconfigured else 1):
                # Have two threads in in parallel
                activeThread = AtOs.createThreadWithFunction(runOnActive)
                standbyThread = AtOs.createThreadWithFunction(runOnStandby)

                activeThread.start()
                sleep(5)  # Just to make sure that the active app gets started
                standbyThread.start()

                # It's time to restore
                activeThread.join()
                standbyThread.join()
                self.warmStop()

            self.serializeStandby()
            self.stopAllApps()

            message  = "Database of active app (%s) and standby app (%s) must be the same. " % (self.activeDb, self.standbyDb)
            message += "Active script: %s, standby script: %s" % (scriptFiles[0], scriptFiles[1])
            self.assertDatabaseSame(message)

            if self.alwaysCleanup():
                os.remove(scriptFiles[0])
                os.remove(scriptFiles[1])

    def TestWithScript(self, script):
        self.setupApps(start=True)

        print "Use script: "
        print script

        def runOnActive():
            self.scriptVerboseDisable()
            self.activeApp.runCli(script, self.scriptTimeout())
            self.serializeActive()

        def runOnStandby():
            self.scriptVerboseDisable()
            self.warmStart()
            self.standbyApp.runCli(script, self.scriptTimeout())

        # Have two threads in in parallel
        activeThread = AtOs.createThreadWithFunction(runOnActive)
        standbyThread = AtOs.createThreadWithFunction(runOnStandby)

        activeThread.start()
        sleep(5)  # Just to make sure that the active app gets started
        standbyThread.start()

        # It's time to restore
        activeThread.join()
        standbyThread.join()
        self.warmStop()

        self.serializeStandby()
        self.stopAllApps()
        self.assertDatabaseSame()

    def testRandom(self):
        self.TestRandomWithReconfigure(reconfigured=False, repeatTimes=self.optionParser.repeatTimes)

    def testReapplyConfigure(self):
        self.TestRandomWithReconfigure(reconfigured=True, repeatTimes=self.optionParser.repeatTimes)

class AnnaMroWarmRestoreTest(WarmRetoreTest):
    @classmethod
    def canRun(cls):
        return AnnaProductCodeProvider.isMroProduct(cls.productCode())

    def testEthPassThrough(self):
        script = """
        device init
        serdes powerup 1-16
        serdes enable 1-16
        serdes mode 2 1G
        ciena eth port subport vlan transmit 3 0x9200.1.1.3
        ciena eth port subport vlan expect 3 0x9200.1.1.3
        eth port srcmac 3 1.1.1.1.1.3
        eth port enable 3
        eth port autoneg enable 3
        ciena eth port bypass enable 3
        serdes loopback 25,26 local
        """
        self.TestWithScript(script)

    def testMustNotCrash(self):
        script = """
        device init
        debug ciena mro xc hide direct
        sur period expire method manual
        
        serdes mode 1 stm16
        serdes powerup 1
        serdes enable 1
        sdh line rate 1 stm16
        sdh line mode 1 sdh
        sdh map aug16.1.1 4xaug4s
        sdh map aug4.1.1-1.4 4xaug1s
        sdh map aug1.1.1-1.16 vc4
        sdh path concate au4.1.1 au4.1.2,1.3,1.4,1.5,1.6,1.7,1.8
        sdh path concate au4.1.9 au4.1.10,1.11,1.12,1.13,1.14,1.15,1.16
        sdh path psl transmit vc4_nc.1.1,1.9 0x2
        sdh path psl expect vc4_nc.1.1,1.9 0x2
        sdh path timing vc4_nc.1.1,1.9 system none
        
        sdh path psl transmit vc4_nc.1.1,1.9 0x2
        sdh path psl expect vc4_nc.1.1,1.9 0x2
        pw create cep 2603,7711 basic
        pw circuit bind 2603,7711 vc4_nc.1.1,1.9
        pw jitterbuffer 2603,7711 32000
        pw jitterdelay 2603,7711 16000
        pw payloadsize 2603,7711 783
        pw ethport 2603,7711 1
        eth port srcmac 1 00.00.00.00.00.ff
        eth port enable 1
        eth port serdes loopback 25-26 local
        pw psn 2603,7711 mef
        pw mef ecid transmit 2603 960525
        pw mef ecid transmit 7711 280882
        pw mef ecid expect 2603 960525
        pw mef ecid expect 7711 280882
        pw ethheader 2603,7711 00.00.00.00.00.ff none none
        sdh path timing vc4_nc.1.1,1.9 system none
        pw enable 2603,7711
        
        show pw 2603,7711
        """
        self.TestWithScript(script)

class AnnaPdhWarmRestoreTest(WarmRetoreTest):
    pass

def Main():
    parser = OptionParser.parser()
    parser.parse()
    appManager = AppManager(parser)
    appManager.silent = parser.silent
    scriptManager = ScriptManager.manager (productCode=parser.productCode, outputDir=parser.outputFolder)
    runner = AtTestCaseRunner.runner()

    def runTestCase(testCase):
        testCase.appManager = appManager
        testCase.scriptManager = scriptManager
        testCase.optionParser = parser
        runner.run(testCase)

    if AnnaProductCodeProvider.isMroProduct(parser.productCode):
        runTestCase(AnnaMroWarmRestoreTest)
        
    if AnnaProductCodeProvider.isPdhProduct(parser.productCode):
        runTestCase(AnnaPdhWarmRestoreTest)

    return 0 if runner.summary() else 1

if __name__ == '__main__':
    sys.exit(Main())

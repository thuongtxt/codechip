from test.AtTestCase import AtTestCase, AtTestCaseRunner


class AbstractBerTest(AtTestCase):
    @classmethod
    def canRun(cls):
        return cls.productCode() == 0x60290022

    @staticmethod
    def engineCliId():
        return "1"

    def allModes(self):
        return ["prbs7",
                "prbs9",
                "prbs15",
                "prbs23",
                "prbs31",
                "prbs11",
                "prbs20",
                "prbs20qrss",
                "prbs20r",
                "sequence",
                "fixedpattern1byte",
                "fixedpattern2bytes",
                "fixedpattern3bytes",
                "fixedpattern4bytes"]

    def supportedModes(self):
        return ["fixedpattern4bytes",
                "prbs15",
                "prbs23",
                "prbs31",
                "prbs20",
                "prbs20r"]

    def assertMode(self, mode):
        assert 0

    def testAllOfSupportedPrbsModes(self):
        self.assertGreater(len(self.supportedModes()), 0, None)
        
        for mode in self.allModes():
            if mode in self.supportedModes():
                self.assertCliSuccess("prbs engine mode %s %s" % (self.engineCliId(), mode))
                result = self.runCli("show prbs engine %s" % self.engineCliId())
                self.assertEqual(result.cellContent(0, "rxMode"), mode)
                self.assertEqual(result.cellContent(0, "txMode"), mode)
            else:
                self.assertCliFailure("prbs engine mode %s %s" % (self.engineCliId(), mode))

    def testFixPattern(self):
        self.assertCliSuccess("prbs engine mode %s fixedpattern4bytes" % self.engineCliId())
        for pattern in ["0xffffffff", "0x00000000"]:
            self.assertCliSuccess("prbs engine fixpattern %s %s %s" % (self.engineCliId(), pattern, pattern))
            result = self.runCli("show prbs engine %s" % self.engineCliId())
            self.assertEqual(result.cellContent(0, "txFixPattern"), pattern)
            self.assertEqual(result.cellContent(0, "rxFixPattern"), pattern)

class Vc3Bert(AbstractBerTest):
    @classmethod
    def setUpClass(cls):
        cls.runCli("device init")
        cls.runCli("sdh map vc3.25.1.1 c3")
        cls.runCli("prbs engine create vc 1 vc3.25.1.1")
        cls.runCli("prbs engine enable 1")

class Vc1xBert(AbstractBerTest):
    @classmethod
    def setUpClass(cls):
        cls.runCli("device init")
        cls.runCli("sdh map vc3.25.1.1 7xtug2s")
        cls.runCli("sdh map tug2.25.1.1.1 tu11")
        cls.runCli("sdh map vc1x.25.1.1.1.1 c1x")
        cls.runCli("prbs engine create vc 1 vc1x.25.1.1.1.1")
        cls.runCli("prbs engine enable 1")

class De1Bert(AbstractBerTest):
    @classmethod
    def setUpClass(cls):
        cls.runCli("device init")
        cls.runCli("sdh map vc3.25.1.1 7xtug2s")
        cls.runCli("sdh map tug2.25.1.1.1 tu11")
        cls.runCli("sdh map vc1x.25.1.1.1.1 de1")
        cls.runCli("prbs engine create de1 1 25.1.1.1.1")
        cls.runCli("prbs engine enable 1")

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(Vc3Bert)
    runner.run(Vc1xBert)
    runner.run(De1Bert)
    runner.summary()

if __name__ == '__main__':
    TestMain()

"""
Created on Apr 21, 2017

@author: namnn
"""
from python.arrive.AtAppManager.AtAppManager import AtAppManager
from test.AtTestCase import AtTestCase, AtTestCaseRunner

class FecTest(AtTestCase):
    cliPortId = None
    supported = None
    
    def getFec(self):
        result = self.runCli("show eth port %d" % self.cliPortId)
        return result.cellContent("FEC", 1)
    
    def testDisplayFec(self):
        fec = self.getFec()
        if self.supported:
            self.assertTrue(fec in ["rx", "tx", "en", "dis"], None)
        else:
            self.assertEqual(fec, "N/S", None)
    
    def testTwoDirection(self):
        if self.supported:
            self.assertCliSuccess("eth port fec enable %d" % self.cliPortId)
            self.assertEqual(self.getFec(), "en", None)
            self.assertCliSuccess("eth port fec disable %d" % self.cliPortId)
            self.assertEqual(self.getFec(), "dis", None)
            
        else:
            self.assertCliFailure("eth port fec enable %d" % self.cliPortId)
            self.assertCliSuccess("eth port fec disable %d" % self.cliPortId)
    
    def testEachDirection(self):
        if self.supported:
            # First disable two directions
            self.assertCliSuccess("eth port fec disable %d" % self.cliPortId)
            
            def testDirection(direction):
                self.assertCliSuccess("eth port fec enable %s %d" % (direction, self.cliPortId))
                self.assertEqual(self.getFec(), direction, None)
                self.assertCliSuccess("eth port fec disable %s %d" % (direction, self.cliPortId))
                self.assertEqual(self.getFec(), "dis", None)
            
            testDirection("rx")
            testDirection("tx")
            
            self.assertCliSuccess("eth port fec enable rx %d" % self.cliPortId)
            self.assertCliSuccess("eth port fec enable tx %d" % self.cliPortId)
            self.assertEqual(self.getFec(), "en", None)
            
class FecRunner(object):
    @staticmethod
    def shouldTestPort(portId):
        return True
    
    def portSupportFec(self, portId):
        return False
    
    def numPorts(self):
        return 0
    
    def run(self, testcaseRunner):
        for portId in range(self.numPorts()):
            if not self.shouldTestPort(portId):
                continue
            
            FecTest.cliPortId = portId + 1
            FecTest.supported = self.portSupportFec(portId)
            testcaseRunner.run(FecTest) 
            
    @classmethod
    def runner(cls):
        productCode = AtAppManager.localApp().productCode()
        if productCode in [0x60290021, 0x60290022]:
            return MroFecRunner()
        if productCode == 0x60290011:
            return PdhFecRunner()
            
class MroFecRunner(FecRunner):
    @staticmethod
    def fecSupported():
        version = AtAppManager.localApp().deviceVersion()
        if version.major >= 2:
            return True
        return False
    
    def portSupportFec(self, portId):
        if self.fecSupported():
            return portId in [21, 22]
        return False
    
    def numPorts(self):
        return 23
    
class PdhFecRunner(FecRunner):
    def numPorts(self):
        return 4
    
def TestMain():
    runner = AtTestCaseRunner.runner()
    fecRunner = FecRunner.runner()
    fecRunner.run(runner)
    runner.summary()

if __name__ == '__main__':
    TestMain()
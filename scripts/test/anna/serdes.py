import random

import sys

import python.arrive.atsdk.AtRegister as AtRegister
from test.AtTestCase import AtTestCaseRunner
from test.anna.AnnaTestCase import AnnaMroTestCase, AnnaTestCase


class AfeTrimTest(AnnaMroTestCase):
    @classmethod
    def setUpClass(cls):
        cls.assertClassCliSuccess("device init")

    @staticmethod
    def expectedTrimAttribute(serdesMode):
        if serdesMode in ["stm1", "stm0", "100M"]:
            return 3
        return 0xA

    def drpBaseAddress(self, portId):
        return self.faceplateDrpBaseAddress(portId)

    def drpAbsoluteAddress(self, portId, drpAddress):
        return self.drpBaseAddress(portId) + drpAddress

    def afeTrimAttributeGet(self, serdesId):
        trimAddress = self.drpAbsoluteAddress(serdesId, drpAddress=0x61)
        reg = AtRegister.AtRegister(self.rd(trimAddress))
        return reg.getField(AtRegister.cBit5_2, 2)

    @staticmethod
    def serdeModeIsSupported(serdesId, mode):
        if serdesId in [0, 8]:
            return True

        if mode in ["stm64", "10G"]:
            return False

        return True

class PpmAutoResetThreshold(AnnaMroTestCase):
    @classmethod
    def setUpClass(cls):
        cls.assertClassCliSuccess("device init")
        cls.assertClassCliSuccess("serdes mode 1-%d 1G" % cls.numFaceplatePorts())

    def testConfigurePpm(self):
        for serdesId in range(self.numFaceplatePorts()):
            cliId = serdesId + 1
            cliResult = self.runCli("show serdes threshold %d" % cliId)
            maxThreshold = int(cliResult.cellContent(0, "AutoResetPpmMax"))
            self.assertGreater(maxThreshold, 0)
            for value in random.sample(range(maxThreshold), 20):
                self.assertCliSuccess("serdes threshold ppm autoreset %d %d" % (cliId, value))
                cliResult = self.runCli("show serdes threshold %d" % cliId)
                self.assertEqual(int(cliResult.cellContent(0, "AutoResetPpm")), value)

    def testCannotSpecifyOutOfRangeValue(self):
        for serdesId in range(self.numFaceplatePorts()):
            cliId = serdesId + 1
            cliResult = self.runCli("show serdes threshold %d" % cliId)
            maxThreshold = int(cliResult.cellContent(0, "AutoResetPpmMax"))
            self.assertCliFailure("serdes threshold ppm autoreset %d %d" % (cliId, maxThreshold + 1))

    def testPpmThresholdIsNotSupportedForNoneFaceplateSerdes(self):
        result = self.runCli("show serdes threshold 1-100")
        for row in range(result.numRows()):
            serdesDescription = result.cellContent(row, "Port")
            if "faceplate_serdes" in serdesDescription:
                continue

            self.assertEqual(result.cellContent(row, "AutoResetPpm"), "N/S")
            self.assertEqual(result.cellContent(row, "AutoResetPpmMax"), "N/S")

            # Try configuring will have error returned
            serdesCliId = result.cellContent(row, "SerdesId")
            self.assertCliFailure("serdes threshold ppm autoreset %s 1" % serdesCliId)

class NewXilinxParams(AnnaMroTestCase):
    @classmethod
    def setUpClass(cls):
        cls.assertClassCliSuccess("device init")

    @staticmethod
    def serdesCliIds():
        return "1-16"

    @staticmethod
    def paramValuesRange():
        return {"RXLPMHFOVRDEN":(0, 1),
                "RXLPMLFKLOVRDEN":(0, 1),
                "RXOSOVRDEN":(0, 1),
                "RXLPMOSHOLD":(0, 1),
                "RXLPMOSOVRDEN":(0, 1),
                "RXLPM_OS_CFG1":(0, 65535),
                "RXLPMGCHOLD":(0, 1),
                "RXLPMGCOVRDEN":(0, 1),
                "RXLPM_GC_CFG":(0, 65535)}

    def testAccessNewParams(self):
        for param, ranges in self.paramValuesRange().iteritems():
            anyValue = random.choice(xrange(ranges[0], ranges[1] + 1))
            for serdesId in range(0, self.numFaceplatePorts()):
                self.assertCliSuccess("serdes param %d %s %d" % (serdesId + 1, param, anyValue))
                cliResult = self.runCli("show serdes param %d" % (serdesId + 1))
                cellValue = int(cliResult.cellContent(0, param), 16)
                self.assertEqual(cellValue, anyValue)

    def testCannotApplyOutOfRangeValue(self):
        for param, ranges in self.paramValuesRange().iteritems():
            for serdesId in range(self.numFaceplatePorts()):
                self.assertCliFailure("serdes param %d %s %d" % (serdesId + 1, param, ranges[1] + 1))

class DefaultSerdesSettingsAbstractTest(AnnaMroTestCase):
    @staticmethod
    def allRates():
        return ["stm0", "stm1", "stm4", "stm16", "stm64", "1G", "10G", "100M"]

    @classmethod
    def canRun(cls):
        return cls.is20G()

    @classmethod
    def faceplateSerdesIdString(cls):
        return ",".join(["%d" % (portId + 1) for portId in range(cls.numFaceplatePorts())])

    def setParam(self, paramName, value):
        self.assertCliSuccess("serdes param %s %s %d" % (self.faceplateSerdesIdString(), paramName, value))

    def setEqualizerMode(self, mode):
        self.assertCliSuccess("serdes equalizer mode %s %s" % (self.faceplateSerdesIdString(), mode))

    def invalidateAllParams(self):
        for param in self.allParams():
            self.setParam(param, self.unexpectedDefaultValueForParam(param))
        self.setEqualizerMode("dfe")

    def assertEqualizerMode(self, expectedMode):
        result = self.runCli("show serdes %s" % self.faceplateSerdesIdStringWithSerdesMode(self.serdesMode()))
        self.assertGreater(result.numRows(), 0)
        for row in range(result.numRows()):
            cellContent = result.cellContent(row, "equalizerMode")
            self.assertEqual(cellContent, expectedMode)

    def assertParam(self, paramName, expectedValue):
        result = self.runCli("show serdes param %s" % self.faceplateSerdesIdStringWithSerdesMode(self.serdesMode()))
        self.assertGreater(result.numRows(), 0)
        for row in range(result.numRows()):
            cellContent = result.cellContent(row, paramName)
            self.assertEqual(int(cellContent, 16), expectedValue, "Param %s does not have expected setting" % paramName)

    def assertTiming(self, expectedMode):
        result = self.runCli("show serdes %s" % self.faceplateSerdesIdStringWithSerdesMode(self.serdesMode()))
        self.assertGreater(result.numRows(), 0)
        for row in range(result.numRows()):
            cellContent = result.cellContent(row, "Timing")
            self.assertEqual(cellContent, expectedMode)

    def setTiming(self, timingMode):
        self.assertCliSuccess("serdes timing mode %s %s" % (self.faceplateSerdesIdString(), timingMode))

    def faceplateSerdesIdStringWithSerdesMode(self, serdesMode):
        if serdesMode in ["stm64", "10G"]:
            return "1,9"

        return self.faceplateSerdesIdString()

    def testDefaultSetting(self):
        params = self.allParams()

        for mode in self.otherRates():
            self.assertCliSuccess("serdes mode %s %s" % (self.faceplateSerdesIdStringWithSerdesMode(mode), mode))

            # Have unexpected state first
            self.invalidateAllParams()
            self.setTiming(self.unexpectedTiming())
            self.setEqualizerMode(self.unexpectedEqualizerMode())

            # Change and expect valid default state
            self.assertCliSuccess("serdes mode %s %s" % (self.faceplateSerdesIdStringWithSerdesMode(self.serdesMode()), self.serdesMode()))
            for param in params:
                self.assertParam(param, expectedValue=self.expectedValueForParam(param))
            self.assertTiming(self.expectedTiming())
            self.assertEqualizerMode(self.expectedEqualizerMode())

    def otherRates(self):
        rates = self.allRates()
        rates.remove(self.serdesMode())
        return rates

    def unexpectedDefaultValueForParam(self, paramName):
        if paramName == "RXLPM_OS_CFG1":
            return 0

        expectedValue = self.expectedValueForParam(paramName)
        if expectedValue == sys.maxint:
            return expectedValue

        if expectedValue in [0, 1]:
            return 1 - expectedValue

        return sys.maxint

    @staticmethod
    def allParams():
        return ["RXLPMHFOVRDEN", "RXLPMLFKLOVRDEN", "RXLPMOSOVRDEN", "RXLPMGCOVRDEN", "RXLPM_OS_CFG1", "RXOSOVRDEN"]

    def unexpectedTiming(self):
        if self.expectedTiming() == "lock2data":
            return "lock2ref"
        return "lock2data"

    def expectedTiming(self):
        return "Invalid"

    @staticmethod
    def expectedEqualizerMode():
        return "lpm"

    def unexpectedEqualizerMode(self):
        if self.expectedEqualizerMode() == "lpm":
            return "dfe"
        return "lpm"

    def expectedValueForParam(self, paramName):
        if paramName in ["RXOSOVRDEN", "RXLPMGCHOLD", "RXLPMOSHOLD"]:
            return 0

        if paramName == "RXLPM_OS_CFG1":
            return 0x8002

        return sys.maxint

    def serdesMode(self):
        return "Invalid"

    @classmethod
    def runWithRunner(cls, runner):
        """
        :type runner: AtTestCaseRunner
        """
        runner.run(Oc1DefaultSerdesSettings)
        runner.run(Oc3DefaultSerdesSettings)
        runner.run(Oc12DefaultSerdesSettings)
        runner.run(Oc48DefaultSerdesSettings)
        runner.run(GeDefaultSerdesSettings)
        runner.run(Oc192DefaultSerdesSettings)
        runner.run(TenGeDefaultSerdesSettings)
        runner.run(BaseFx100DefaultSerdesSettings)

class StmOversamplingDefaultSerdesSettings(DefaultSerdesSettingsAbstractTest):
    def expectedTiming(self):
        return "lock2ref"

    def expectedValueForParam(self, paramName):
        if paramName in ["RXOSOVRDEN", "RXLPMHFOVRDEN", "RXLPMGCOVRDEN", "RXLPMLFKLOVRDEN", "RXLPMOSOVRDEN"]:
            return 1
    
        return super(StmOversamplingDefaultSerdesSettings, self).expectedValueForParam(paramName)

class Oc1DefaultSerdesSettings(StmOversamplingDefaultSerdesSettings):
    def serdesMode(self):
        return "stm0"

class Oc3DefaultSerdesSettings(StmOversamplingDefaultSerdesSettings):
    def serdesMode(self):
        return "stm1"

class NoOversamplingDefaultSerdesSettings(DefaultSerdesSettingsAbstractTest):
    def expectedValueForParam(self, paramName):
        if paramName in ["RXLPMHFOVRDEN", "RXLPMGCOVRDEN", "RXOSOVRDEN", "RXLPMLFKLOVRDEN", "RXLPMGCHOLD", "RXLPMOSHOLD"]:
            return 0

        if paramName == "RXLPMOSOVRDEN":
            return 1

        return super(NoOversamplingDefaultSerdesSettings, self).expectedValueForParam(paramName)

    def expectedTiming(self):
        return "lock2data"

class Oc12DefaultSerdesSettings(NoOversamplingDefaultSerdesSettings):
    def serdesMode(self):
        return "stm4"

class Oc48DefaultSerdesSettings(NoOversamplingDefaultSerdesSettings):
    def serdesMode(self):
        return "stm16"

class GeDefaultSerdesSettings(NoOversamplingDefaultSerdesSettings):
    def serdesMode(self):
        return "1G"

class Oc192DefaultSerdesSettings(NoOversamplingDefaultSerdesSettings):
    def serdesMode(self):
        return "stm64"

    def expectedValueForParam(self, paramName):
        if paramName == "RXLPM_OS_CFG1":
            return super(Oc192DefaultSerdesSettings, self).expectedValueForParam(paramName)
        return 0

class TenGeDefaultSerdesSettings(Oc192DefaultSerdesSettings):
    def serdesMode(self):
        return "10G"


class BaseFx100DefaultSerdesSettings(DefaultSerdesSettingsAbstractTest):
    def serdesMode(self):
        return "100M"

    def expectedValueForParam(self, paramName):
        if paramName in ["RXLPMGCHOLD", "RXLPMOSHOLD"]:
            return 0
        if paramName in ["RXLPMOSOVRDEN", "RXLPMHFOVRDEN", "RXLPMLFKLOVRDEN", "RXOSOVRDEN", "RXLPMGCOVRDEN"]:
            return 1

        return super(BaseFx100DefaultSerdesSettings, self).expectedValueForParam(paramName)

    def expectedTiming(self):
        return "lock2ref"

class SerdesResetEachDirection(AnnaTestCase):
    @classmethod
    def runWithRunner(cls, runner):
        """
        :type runner: AtTestCaseRunner
        """
        if cls.isMroProduct():
            runner.run(MroSerdesResetEachDirection)

        if cls.isPdhProduct():
            runner.run(PdhSerdesResetEachDirection)

    def serdesCliIds(self):
        return list()

    def resetRx(self, serdesCliId):
        self.assertCliSuccess("serdes rx reset %d" % serdesCliId)

    def resetTx(self, serdesCliId):
        self.assertCliSuccess("serdes tx reset %d" % serdesCliId)

    def reset(self, serdesCliId):
        self.assertCliSuccess("serdes reset %d" % serdesCliId)

    def testSerdesReset(self):
        for serdesCliId in self.serdesCliIds():
            self.resetRx(serdesCliId)
            self.resetTx(serdesCliId)
            self.reset(serdesCliId)

class MroSerdesResetEachDirection(SerdesResetEachDirection):
    def serdesCliIds(self):
        return [25, 26]

class PdhSerdesResetEachDirection(SerdesResetEachDirection):
    def serdesCliIds(self):
        return [1, 2]

class RemoteLoopbackOnLock2RefInterface(AnnaMroTestCase):
    @staticmethod
    def lock2refInterfaces():
        return ["stm0", "stm1", "100M"]

    @staticmethod
    def lock2dataInterfaces():
        return [
                "stm4",
                "stm16",
                "stm64",
                "1G",
                "10G",
                ]

    @classmethod
    def setUpClass(cls):
        cls.assertClassCliSuccess("device init")

    def allSerdesIds(self):
        return "1-%d" % self.numFaceplatePorts()

    def stm16AllSerdesIds(self):
        return ",".join(["%s" % (serdesId + 1) for serdesId in range(0, self.numFaceplatePorts(), 2)])

    def stm64AllSerdesIds(self):
        return ",".join(["%s" % (serdesId + 1) for serdesId in range(0, self.numFaceplatePorts(), 8)])

    def setMode(self, mode, serdesIds = None):
        if serdesIds is None:
            serdesIds = self.allSerdesIds()
        cli = "serdes mode %s %s" % (serdesIds, mode)
        self.assertCliSuccess(cli)

    def setTimingMode(self, mode, serdesIds = None):
        if serdesIds is None:
            serdesIds = self.allSerdesIds()
        cli = "serdes timing mode %s %s" % (serdesIds, mode)
        self.assertCliSuccess(cli)

    def assertTiming(self, timingMode, serdesIds = None):
        if serdesIds is None:
            serdesIds = self.allSerdesIds()

        cliResult = self.runCli("show serdes %s" % serdesIds)
        for rowId in range(cliResult.numRows()):
            timingString = cliResult.cellContent(rowId=rowId, columnIdOrName="Timing")
            self.assertEqual(timingMode, timingString)

    def assertLock2ref(self, serdesIds = None):
        self.assertTiming("lock2ref", serdesIds)

    def assertLock2data(self, serdesIds = None):
        self.assertTiming("lock2data", serdesIds)

    def remoteLoopback(self):
        self.assertCliSuccess("serdes loopback %s remote" % self.allSerdesIds())

    def localLoopback(self):
        self.assertCliSuccess("serdes loopback %s local" % self.allSerdesIds())

    def releaseLoopback(self):
        self.assertCliSuccess("serdes loopback %s release" % self.allSerdesIds())

    def passtestWhenRemoteLoopback_NeedToChangeToLock2Data(self):
        for mode in self.lock2refInterfaces():
            self.setMode(mode)
            self.assertLock2ref()

            self.remoteLoopback()
            self.assertLock2data()

            self.releaseLoopback()
            self.assertLock2ref()

            self.remoteLoopback()
            self.assertLock2data()

            # Only remote loopback will be applied lock2data
            self.localLoopback()
            self.assertLock2ref()

    def passtestShouldNotOverrideExplicitlyTimingModeSetting(self):
        for mode in self.lock2refInterfaces():
            # Explicit set timing mode
            self.setMode(mode)
            self.setTimingMode("lock2data")
            self.assertTiming("lock2data")
            self.assertLock2data()

            # Now, do the remote loopback
            self.remoteLoopback()
            self.assertLock2data()

            # Release the loopback will not change to lock2ref
            self.releaseLoopback()
            self.assertLock2data()

    def testSerdesTimingWillNotBeChangedForLock2dataInterfaces(self):
        def TestWithSerdes(serdesId, modes):
            for mode in modes:
                self.setMode(mode, serdesId)
                self.assertLock2data(serdesId)

                self.remoteLoopback()
                self.assertLock2data(serdesId)

                self.releaseLoopback()
                self.assertLock2data(serdesId)

                self.remoteLoopback()
                self.assertLock2data(serdesId)

                self.localLoopback()
                self.assertLock2data(serdesId)

        TestWithSerdes(self.allSerdesIds(), ["stm4", "1G"])
        TestWithSerdes(self.stm16AllSerdesIds(), ["stm16"])
        TestWithSerdes(self.stm64AllSerdesIds(), ["stm64"])

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(AfeTrimTest)
    runner.run(PpmAutoResetThreshold)
    runner.run(NewXilinxParams)
    runner.run(RemoteLoopbackOnLock2RefInterface)
    DefaultSerdesSettingsAbstractTest.runWithRunner(runner)
    SerdesResetEachDirection.runWithRunner(runner)
    runner.summary()

if __name__ == '__main__':
    TestMain()
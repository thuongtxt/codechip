from AtAppManager import AtDeviceVersion
from python.arrive.AtAppManager.AtAppManager import AtAppManager
from python.arrive.AtAppManager.AtColor import AtColor
from python.arrive.atsdk.AtRegister import AtRegisterField
from test.AtTestCase import AtTestCaseRunner
import python.arrive.atsdk.AtRegister as AtRegister
from AnnaTestCase import AnnaMroTestCase

class AnnaEthAlarm(AnnaMroTestCase):
    @classmethod
    def setUpClass(cls):
        cls.assertClassCliSuccess("device show readwrite dis dis")
        cls.assertClassCliSuccess("device init")
        
    def supportedAlarms(self):
        return []
    
    def ethPortList(self):
        return None
    
    def TestDefectSupportedMustBeHandledProperlyWithCommand(self, command):
        supportedAlarms = self.supportedAlarms()
        
        result = self.runCli(command)
        numAlarms = len(supportedAlarms)
        if numAlarms == 0:
            self.assertEqual(result.numRows(), 0, None)
        else:
            self.assertEqual(result.numRows(), numAlarms + 1, None) # Plus 1 for description row
            
        if numAlarms == 0:
            return
        
        for alarm in supportedAlarms:
            cellContent = result.cellContent(alarm, 1)
            self.assertTrue(cellContent in ["set", "clear"], None)
    
    def testInterruptSupportedMustBeHandledProperly(self):
        self.TestDefectSupportedMustBeHandledProperlyWithCommand("show eth port interrupt %s r2c" % self.ethPortList())
            
    def testAlarmSupportedMustBeHandledProperly(self):
        self.TestDefectSupportedMustBeHandledProperlyWithCommand("show eth port alarm %s" % self.ethPortList())
    
    def hasLinkStatus(self):
        return True

    def canTestBackplaneInterruptAccess(self):
        newVersion = AtDeviceVersion("2.5 (on 201515/1515/1515), built: 00. 0")
        version = self.app().deviceVersion()
        if version >= newVersion:
            return False
        return True

    def testAccessLinkStatus(self):
        cliResult = self.runCli("show eth port %s" % self.ethPortList())
        for column in range(1, cliResult.numColumns()):
            linkStatus = cliResult.cellContent("Link", column)
            if self.hasLinkStatus():
                self.assertTrue(linkStatus in ["up", "down"], None)
            else:
                self.assertEqual(linkStatus, "N/S", None)
    
    def testInterruptAccess(self):
        self.fail("Implement test please")
        
    def testAlarmAccess(self):
        self.fail("Implement test please")
    
    def testAutonegStateChangeInterrupt(self):
        self.skipTest("Hardware has not supported this interrupt")
    
    def testInterruptMask(self):
        self.fail("Implement this please")
    
class AnnaInternalPortEthAlarm(AnnaEthAlarm):
    def ethPortList(self):
        return "1"
        
class AnnaFaceplateEthAlarm(AnnaEthAlarm):
    @classmethod
    def canRun(cls):
        version = AtAppManager.localApp().deviceVersion()
        if version.major >= 2 and version.major >= 2:
            AtColor.printColor(AtColor.YELLOW, "From version 2.2, hardware change this and this script needs to be updated to cover")
            return False
        
        return super(AnnaFaceplateEthAlarm, cls).canRun()
    
    @staticmethod
    def numSerdes():
        return 8

    def ethPortList(self):
        return "2-%d" % (self.numSerdes() + 1)

class upen_epage(AtRegister.AtRegister):
    @classmethod
    def GeLossDataSyncBit(cls, portId):
        return portId
    @classmethod
    def GeExErrorRatioBit(cls, portId):
        return 16 + portId

class an_interrupt_abstract_reg(AtRegister.AtRegister):
    @staticmethod
    def interruptBit(portId):
        return portId % 8

class AnnaFaceplate1GEthAlarm(AnnaFaceplateEthAlarm):
    def supportedAlarms(self):
        return ["loss-of-data-sync",
                "excessive-error-ratio",
                "auto-neg-state-change",
                "rx-remote-fault"]
    
    def setUp(self):
        ports = "1-%d" % self.numSerdes()
        self.assertCliSuccess("serdes mode %s 1G" % ports)
        self.assertCliSuccess("serdes powerup %s" % ports)
        self.assertCliSuccess("serdes enable %s" % ports)

    @staticmethod
    def passthroughBaseAddress(_):
        return 0xc0000

    @staticmethod
    def multirateBaseAddress(portId):
        part = portId / 8 
        if part == 0:
            return 0x0030000
        if part == 1:
            return 0x0050000
        
        return None
        
    def intstk_status_grp0_pen_address(self, portId):
        return self.multirateBaseAddress(portId) + 0x0040 + portId
    
    @staticmethod
    def portDriverIds():
        return range(8)
        
    def _absoluteAddress(self, portId, relativeAddress):
        return relativeAddress + self.passthroughBaseAddress(portId)
        
    def upen_epagestk0_address(self, portId):
        return self._absoluteAddress(portId, 0x090)
        
    def upen_epagesta0_address(self, portId):
        return self._absoluteAddress(portId, 0x094)
    
    @staticmethod
    def ethPortCliId(portId):
        return portId + 2
    
    def TestPassThroughInterruptAlarmAccess(self, commandBuilder, addressBuilder):
        for portId in self.portDriverIds():
            def _expect(alarmName, status):
                ethPortId = self.ethPortCliId(portId)
                result = self.runCli(commandBuilder(ethPortId))
                currentStatus = result.cellContent(alarmName, 1)
                self.assertEqual(currentStatus, status, None)
                
            def _set(set_, bit):
                reg = upen_epage(0)
                reg.address = addressBuilder(portId)
                reg.setBit(bit, 1 if set_ else 0, applyHw=True)
            
            def _test(alarmName, bit):
                _set(set_=True, bit=bit)
                _expect(alarmName, "set")
                _set(set_=False, bit=bit)
                _expect(alarmName, "clear")
                
            _test("excessive-error-ratio", upen_epage.GeExErrorRatioBit(portId))
    
    def TestMultiRateInterruptAccessOnPort(self, portId, cliAlarmName, addressBuilder, cliBuilder):
        address = addressBuilder(portId)
        reg = an_interrupt_abstract_reg(self.rd(address))
        reg.address = address
        for status in [1, 0]:
            reg.setBit(reg.interruptBit(portId), status, applyHw = True) 
            
            result = self.runCli(cliBuilder(self.ethPortCliId(portId)))
            cellContent = result.cellContent(cliAlarmName, 1)
            expected = "set" if status else "clear"
            self.assertEqual(cellContent, expected, None)
    
    def TestMultiRateInterruptAccess(self):
        def cliBuilder(aPortId):
            return "show eth port interrupt %d r2c" % aPortId
        
        for portId in self.portDriverIds():
            def _test(cliAlarmName, addressBuilder):
                self.TestMultiRateInterruptAccessOnPort(portId, cliAlarmName, addressBuilder, cliBuilder)
                
            _test("rx-remote-fault", self.an_rfi_stk_pen_address)
            _test("auto-neg-state-change", self.an_sta_stk_pen_address)
            _test("loss-of-data-sync", self.los_sync_stk_pen_address)
                
    def testInterruptAccess(self):
        def commandBuilder(ethPortId):
            return "show eth port interrupt %d r2c" % ethPortId
        
        def addressBuilder(portId):
            return self.upen_epagestk0_address(portId)
        
        self.TestPassThroughInterruptAlarmAccess(commandBuilder, addressBuilder)
        self.TestMultiRateInterruptAccess()
        
    def TestMultirateAlarmAccess(self):
        def cliBuilder(aPortId):
            return "show eth port alarm %d" % aPortId
        
        for portId in self.portDriverIds():
            self.TestMultiRateInterruptAccessOnPort(
                portId, 
                "loss-of-data-sync", 
                self.los_sync_pen_address, cliBuilder)
        
    def testAlarmAccess(self):
        def commandBuilder(ethPortId):
            return "show eth port alarm %d" % ethPortId
        
        def addressBuilder(portId):
            return self.upen_epagesta0_address(portId)
        
        self.TestPassThroughInterruptAlarmAccess(commandBuilder, addressBuilder)
    
    def an_rfi_int_enb_pen_address(self, portId):
        return self.multirateBaseAddress(portId) + 0x081A
    
    def an_int_enb_pen_address(self, portId):
        return self.multirateBaseAddress(portId) + 0x0817
    
    def los_sync_int_enb_pen_address(self, portId):
        return self.multirateBaseAddress(portId) + 0x0818
    
    def an_rfi_stk_pen_address(self, portId):
        return self.multirateBaseAddress(portId) + 0x0819
    
    def an_sta_stk_pen_address(self, portId):
        return self.multirateBaseAddress(portId) + 0x0807
    
    def los_sync_stk_pen_address(self, portId):
        return self.multirateBaseAddress(portId) + 0x080B
    
    def los_sync_pen_address(self, portId):
        return self.multirateBaseAddress(portId) + 0x080A
    
    def testInterruptMask(self):
        for portId in self.portDriverIds():
            ethPortId = self.ethPortCliId(portId)
            
            def _testAlarmType(cliAlarmType, addressBuilder):
                for status in [1, 0]:
                    address = addressBuilder(portId)
                    reg = an_interrupt_abstract_reg(self.rd(address))
                    reg.address = address
                    reg.setBit(reg.interruptBit(portId), status, applyHw = True)
                    result = self.runCli("show eth port interruptmask %d" % ethPortId)
                    cellContent = result.cellContent(cliAlarmType, 1)
                    expectedResult = "en" if status else "dis" 
                    self.assertEqual(cellContent, expectedResult, None)
            
            _testAlarmType("rx-remote-fault", self.an_rfi_int_enb_pen_address)
            _testAlarmType("auto-neg-state-change", self.an_int_enb_pen_address)
            _testAlarmType("loss-of-data-sync", self.los_sync_int_enb_pen_address)
        
    def testExcessiveErrorRatioInterruptMask(self):
        self.skipTest("Hardware has not supported")
        
class eth10g_link_fault_int_enb(AtRegister.AtRegister):
    @staticmethod
    def Eth10gRemoteFaultEnb():
        return 1
    @staticmethod
    def Eth10gLocalFaultEnb():
        return 2

class eth10g_link_fault_sticky(eth10g_link_fault_int_enb):
    pass

class AnnaFaceplate10GEthAlarm(AnnaFaceplateEthAlarm):
    class upen_epatenge(AtRegister.AtRegister):
        @classmethod
        def TenGeLossDataOrFrameSyncBitPosition(cls, portId):
            return portId
        @classmethod
        def TenGeExErrorRatioBitPosition(cls, portId):
            return portId + 2
        @classmethod
        def TenGeClockLossBitPosition(cls, portId):
            return portId + 4
        @classmethod
        def TenGeClockOutRangeBitPosition(cls, portId):
            return portId + 6
        @classmethod
        def TenGeLocalFaultBitPosition(cls, portId):
            return portId + 8
        @classmethod
        def TenGeRemoteFaultBitPosition(cls, portId):
            return portId + 10

    def setUp(self):
        ports = "1"
        self.assertCliSuccess("serdes mode %s 10G" % ports)
        self.assertCliSuccess("serdes powerup %s" % ports)
        self.assertCliSuccess("serdes enable %s" % ports) 
    
    def ethPortList(self):
        return "2"
    
    def supportedAlarms(self):
        return ["loss-of-data-sync",
                "loss-of-clock",
                "loss-of-frame",
                "excessive-error-ratio",
                "rx-fault",
                "rx-remote-fault",
                "frequency-out-of-range",
                "auto-neg-state-change"]
    
    @staticmethod
    def baseAddress():
        return 0xC0000
        
    def _absoluteAddress(self, relativeAddress):
        return relativeAddress + self.baseAddress()
        
    def upen_epatengestk0_address(self):
        return self._absoluteAddress(0x092) 
    
    def upen_epatengesta0_address(self):
        return self._absoluteAddress(0x096)
    
    @staticmethod
    def localPortId():
        return 0

    @staticmethod
    def localFaultAndRemoteFaultFromEthPassModule():
        return False

    def TestPassThroughInterruptAlarmAccess(self, commandBuilder, addressBuilder):
        def getDefectWithName(name):
            result = self.runCli(commandBuilder(self.ethPortList()))
            return result.cellContent(name, 1)
        
        def TestInterrupt(bitPosition, defectName):
            address = addressBuilder()
            reg = AnnaFaceplate10GEthAlarm.upen_epatenge(0)
            reg.setBit(bitPosition, 1)
            reg.wr(address, reg.value)
            self.assertEqual(getDefectWithName(defectName), "set", None)
            reg.setBit(bitPosition, 0)
            reg.wr(address, reg.value)
            self.assertEqual(getDefectWithName(defectName), "clear", None)
            
        localPort = self.localPortId()
        TestInterrupt(AnnaFaceplate10GEthAlarm.upen_epatenge.TenGeLossDataOrFrameSyncBitPosition(localPort), "loss-of-data-sync")
        TestInterrupt(AnnaFaceplate10GEthAlarm.upen_epatenge.TenGeExErrorRatioBitPosition(localPort), "excessive-error-ratio")
        TestInterrupt(AnnaFaceplate10GEthAlarm.upen_epatenge.TenGeClockLossBitPosition(localPort), "loss-of-clock")
        TestInterrupt(AnnaFaceplate10GEthAlarm.upen_epatenge.TenGeClockOutRangeBitPosition(localPort), "frequency-out-of-range")
        if self.localFaultAndRemoteFaultFromEthPassModule():
            TestInterrupt(AnnaFaceplate10GEthAlarm.upen_epatenge.TenGeLocalFaultBitPosition(localPort), "rx-fault")
            TestInterrupt(AnnaFaceplate10GEthAlarm.upen_epatenge.TenGeRemoteFaultBitPosition(localPort), "rx-remote-fault")

    def testInterruptAccess(self):
        def commandBuilder(portList):
            return "show eth port interrupt %s r2c" % portList
        
        def addressBuilder():
            return self.upen_epatengestk0_address()
        
        self.TestPassThroughInterruptAlarmAccess(commandBuilder, addressBuilder)
        
    def testAlarmAccess(self):
        def commandBuilder(portList):
            return "show eth port alarm %s" % portList
        
        def addressBuilder():
            return self.upen_epatengesta0_address()
        
        self.TestPassThroughInterruptAlarmAccess(commandBuilder, addressBuilder)
    
    @staticmethod
    def tengeBaseAddess():
        return 0x0020000
    
    def TestLocalFaultRemoteFault(self, address, cliBuilder, expectedStatusBuilder):
        if self.localFaultAndRemoteFaultFromEthPassModule():
            self.skipTest("Not here")
            
        reg = eth10g_link_fault_int_enb(self.rd(address))
        reg.address = address
        
        def _test(alarmName, bit):
            for status in [1, 0]:
                reg.setBit(bit, status, applyHw = True)
                cli = cliBuilder()
                result = self.runCli(cli)
                cell = result.cellContent(alarmName, 1)
                expected = expectedStatusBuilder(status)
                self.assertEqual(cell, expected, None)
        
        _test("rx-fault", reg.Eth10gLocalFaultEnb())
        _test("rx-remote-fault", reg.Eth10gRemoteFaultEnb())
    
    def testLocalFaultRemoteFaultCurrentStatus(self):
        def cliBuilder():
            return "show eth port alarm %s" % self.ethPortList()
        def expectedStatusBuilder(status):
            return "set" if status else "clear"
        
        return self.TestLocalFaultRemoteFault(self.tengeBaseAddess() + 0x100D, cliBuilder, expectedStatusBuilder)
    
    def testLocalFaultRemoteFaultInterrupt(self):
        def cliBuilder():
            return "show eth port interrupt %s r2c" % self.ethPortList()
        def expectedStatusBuilder(status):
            return "set" if status else "clear"
        
        return self.TestLocalFaultRemoteFault(self.tengeBaseAddess() + 0x100B, cliBuilder, expectedStatusBuilder)
    
    def testLocalFaultRemoteFaultInterruptMask(self):
        def cliBuilder():
            return "show eth port interruptmask %s" % self.ethPortList()
        def expectedStatusBuilder(status):
            return "en" if status else "dis"
        
        return self.TestLocalFaultRemoteFault(self.tengeBaseAddess() + 0x1003, cliBuilder, expectedStatusBuilder)
    
    def testInterruptMask(self):
        self.skipTest("Hardware has to support interrupt for alarms: %s" % self.supportedAlarms())
        
class AnnaBackplanePwEthPortAlarm(AnnaEthAlarm):
    def ethPortList(self):
        return "1"
    
    def hasLinkStatus(self):
        return False
    
    def testAlarmAccess(self):
        # This port is internal one and would not have any alarm
        pass
    
    def testInterruptAccess(self):
        # This port is internal one and would not have any alarm
        pass
    
    def testInterruptMask(self):
        # This port will not have any alarm
        pass
    
    def testAutonegStateChangeInterrupt(self):
        # This port will not have autoneg
        pass
    
class ETH_40G_RX_STICKY(AtRegister.AtRegister):
    @classmethod
    def stat_rx_local_fault_bit(cls):
        return 31
    
    @classmethod
    def stat_rx_remote_fault_bit(cls):
        return 30
    
    @classmethod
    def stat_rx_internal_local_fault_bit(cls):
        return 29
    
    @classmethod
    def stat_rx_received_local_fault_bit(cls):
        return 28
    
    @classmethod
    def stat_rx_framing_err_bit(cls, laneId):
        return 24 + laneId
        
    @classmethod
    def stat_rx_synced_err_bit(cls, laneId):
        return 20 + laneId
    
    @classmethod
    def stat_rx_mf_len_err_bit(cls, laneId):
        return 16 + laneId
    
    @classmethod
    def stat_rx_mf_repeat_err_bit(cls, laneId):
        return 12 + laneId
    
    @classmethod
    def stat_rx_aligned_err_bit(cls):
        return 11
    
    @classmethod
    def stat_rx_misaligned_bit(cls):
        return 10
    
    @classmethod
    def stat_rx_truncated_bit(cls):
        return 9
    
    @classmethod
    def stat_rx_hi_ber_bit(cls):
        return 8
    
    @classmethod
    def stat_rx_bip_err(cls, laneId):
        return 4 + laneId
    
    @classmethod
    def stat_rx_mf_err(cls, laneId):
        return laneId
    
class ETH_40G_AN_STICKY(AtRegister.AtRegister):
    @classmethod
    def stat_an_autoneg_complete_bit(cls):
        return 1
    
    @classmethod
    def stat_an_parallel_detection_fault_bit(cls):
        return 0

class ETH_40G_FEC_STICKY(AtRegister.AtRegister):
    @classmethod
    def stat_fec_inc_cant_correct_count(cls, laneId):
        return 8 + laneId
    
    @classmethod
    def stat_fec_inc_correct_count(cls, laneId):
        return 4 + laneId
    
    @classmethod
    def stat_fec_lock_error(cls, laneId):
        return laneId

class RegisterProvider40G(object):
    @classmethod
    def baseAddress(cls, localPortId):
        if localPortId == 0:
            return 0xF80000
        if localPortId == 1:
            return 0xF90000
        return None
    
    @classmethod
    def ETH_40G_RX_STICKY_Address(cls, localPortId):
        return 0x20C0 + cls.baseAddress(localPortId)

    @classmethod
    def ETH_40G_AN_STICKY_Address(cls, localPortId):
        return 0x2102 + cls.baseAddress(localPortId)
    
    @classmethod
    def numPorts(cls):
        return 2
    
    @classmethod
    def ETH_40G_FEC_STICKY_Address(cls, localPortId):
        return 0x20C1 + cls.baseAddress(localPortId)

class AnnaBackplaneEthAlarm(AnnaEthAlarm):
    def ethPortList(self):
        return "22-23"
    
    def supportedAlarms(self):
        return ["tx-fault",
                "local-fault",
                "remote-fault",
                "rx-internal-fault",
                "rx-received-local-fault",
                "rx-aligned-error",
                "rx-mis-aligned",
                "rx-truncated",
                "rx-hi-ber",
                "auto-neg-parallel-detection-fault"]

    @staticmethod
    def baseAddress(localPortId):
        return RegisterProvider40G.baseAddress(localPortId)
    
    @staticmethod
    def ETH_40G_RX_STICKY_Address(localPortId):
        return RegisterProvider40G.ETH_40G_RX_STICKY_Address(localPortId)

    @staticmethod
    def ETH_40G_AN_STICKY_Address(localPortId):
        return RegisterProvider40G.ETH_40G_AN_STICKY_Address(localPortId)
    
    @staticmethod
    def numPorts():
        return RegisterProvider40G.numPorts()
    
    @staticmethod
    def cliEthPortId(localPortId):
        return localPortId + 22

    def _TestRxInterrupt(self, localPortId):


        cliPortId = self.cliEthPortId(localPortId)
        reg = ETH_40G_RX_STICKY(0)
        reg.address = self.ETH_40G_RX_STICKY_Address(localPortId)
        
        def _testRxInterrupt(bit, alarmName):
            reg.setBit(bit, 1, applyHw = True)
            result = self.runCli("show eth port interrupt %d r2c" % cliPortId)
            status = result.cellContent(alarmName, 1)
            self.assertEqual(status, "set", None)
            
            reg.setBit(bit, 0, applyHw = True)
            result = self.runCli("show eth port interrupt %d r2c" % cliPortId)
            status = result.cellContent(alarmName, 1)
            self.assertEqual(status, "clear", None)
            
        _testRxInterrupt(ETH_40G_RX_STICKY.stat_rx_local_fault_bit(), "rx-fault")
        _testRxInterrupt(ETH_40G_RX_STICKY.stat_rx_remote_fault_bit(), "rx-remote-fault")
        _testRxInterrupt(ETH_40G_RX_STICKY.stat_rx_internal_local_fault_bit(), "rx-internal-fault")
        _testRxInterrupt(ETH_40G_RX_STICKY.stat_rx_received_local_fault_bit(), "rx-received-local-fault")
        _testRxInterrupt(ETH_40G_RX_STICKY.stat_rx_aligned_err_bit(), "rx-aligned-error")
        _testRxInterrupt(ETH_40G_RX_STICKY.stat_rx_misaligned_bit(), "rx-mis-aligned")
        _testRxInterrupt(ETH_40G_RX_STICKY.stat_rx_truncated_bit(), "rx-truncated")
        _testRxInterrupt(ETH_40G_RX_STICKY.stat_rx_hi_ber_bit(), "rx-hi-ber")
    
    def _TestAutonegInterrupt(self, localPortId):
        cliPortId = self.cliEthPortId(localPortId)
        reg = ETH_40G_AN_STICKY(0)
        reg.address = self.ETH_40G_AN_STICKY_Address(localPortId)
        
        reg.setBit(ETH_40G_AN_STICKY.stat_an_parallel_detection_fault_bit(), 1, applyHw = True)
        result = self.runCli("show eth port interrupt %d r2c" % cliPortId)
        status = result.cellContent("auto-neg-parallel-detection-fault", 1)
        self.assertEqual(status, "set", None)
        
        reg.setBit(ETH_40G_AN_STICKY.stat_an_parallel_detection_fault_bit(), 0, applyHw = True)
        result = self.runCli("show eth port interrupt %d r2c" % cliPortId)
        status = result.cellContent("auto-neg-parallel-detection-fault", 1)
        self.assertEqual(status, "clear", None)
    
    def testInterruptAccess(self):
        if not self.canTestBackplaneInterruptAccess():
            self.skipTest("Cannot test because registers are changes on new version")

        for port_i in range(self.numPorts()):
            self._TestRxInterrupt(port_i)
            self._TestAutonegInterrupt(port_i)
            
    def testAlarmAccess(self):
        self.skipTest("Hardware must support current alarm status for: %s" % self.supportedAlarms())
            
    def testTxFaultInterruptAndAlarm(self):
        self.skipTest("Hardware must support this status")
        
    def testTxUnderflowErrorInterruptAndAlarm(self):
        self.skipTest("Hardware must support this status")
            
    def hasLinkStatus(self):
        return False
    
    def testInterruptMask(self):
        self.skipTest("Hardware must support interrupt mask for: %s" % self.supportedAlarms())
    
class AnnaBackplaneLaneEthAlarm(AnnaEthAlarm):
    def ethPortList(self):
        return "22.1-23.4"
    
    def supportedAlarms(self):
        return ["rx-framing-error",
                "rx-synced-error",
                "rx-mf-len-error",
                "rx-mf-repeat-error",
                "rx-mf-error",
                "rx-bip-error",
                "auto-neg-fec-inc-cant-correct",
                "auto-neg-fec-inc-correct",
                "auto-neg-fec-lock-error"]
    
    @staticmethod
    def cliPortId(localPortId, laneId):
        return "%d.%d" % (localPortId + 22, laneId + 1)
    
    def _TestRxInterrupt(self, localPort, laneId):
        cliId = self.cliPortId(localPort, laneId)
        
        def _testRxInterrupt(bit, defectName):
            reg = ETH_40G_RX_STICKY(0)
            reg.address = RegisterProvider40G.ETH_40G_RX_STICKY_Address(localPort)
            
            reg.setBit(bit, 1, applyHw=True)
            result = self.runCli("show eth port interrupt %s r2c" % cliId)
            status = result.cellContent(defectName, 1)
            self.assertEqual(status, "set", None)
            
            reg.setBit(bit, 0, applyHw=True)
            result = self.runCli("show eth port interrupt %s r2c" % cliId)
            status = result.cellContent(defectName, 1)
            self.assertEqual(status, "clear", None)
            
        _testRxInterrupt(ETH_40G_RX_STICKY.stat_rx_framing_err_bit(laneId), "rx-framing-error")
        _testRxInterrupt(ETH_40G_RX_STICKY.stat_rx_synced_err_bit(laneId), "rx-synced-error")
        _testRxInterrupt(ETH_40G_RX_STICKY.stat_rx_mf_len_err_bit(laneId), "rx-mf-len-error")
        _testRxInterrupt(ETH_40G_RX_STICKY.stat_rx_mf_repeat_err_bit(laneId), "rx-mf-repeat-error")
        _testRxInterrupt(ETH_40G_RX_STICKY.stat_rx_mf_err(laneId), "rx-mf-error")
        _testRxInterrupt(ETH_40G_RX_STICKY.stat_rx_bip_err(laneId), "rx-bip-error")

    def _TestAutoNegInterrupt(self, localPort, laneId):
        if not self.canTestBackplaneInterruptAccess():
            self.skipTest("New version has new register fields, ignore this testing")

        cliId = self.cliPortId(localPort, laneId)
        
        def _testAutonegInterrupt(bit, defectName):
            reg = ETH_40G_FEC_STICKY(0)
            reg.address = RegisterProvider40G.ETH_40G_FEC_STICKY_Address(localPort)
            
            reg.setBit(bit, 1, applyHw=True)
            result = self.runCli("show eth port interrupt %s r2c" % cliId)
            status = result.cellContent(defectName, 1)
            self.assertEqual(status, "set", None)
            
            reg.setBit(bit, 0, applyHw=True)
            result = self.runCli("show eth port interrupt %s r2c" % cliId)
            status = result.cellContent(defectName, 1)
            self.assertEqual(status, "clear", None)
            
        _testAutonegInterrupt(ETH_40G_FEC_STICKY.stat_fec_inc_cant_correct_count(laneId), "auto-neg-fec-inc-cant-correct")
        _testAutonegInterrupt(ETH_40G_FEC_STICKY.stat_fec_inc_correct_count(laneId), "auto-neg-fec-inc-correct")
        _testAutonegInterrupt(ETH_40G_FEC_STICKY.stat_fec_lock_error(laneId), "auto-neg-fec-lock-error")
    
    def testInterruptAccess(self):
        if not self.canTestBackplaneInterruptAccess():
            self.skipTest("New registers are changed on new version")

        for localPort in range(RegisterProvider40G.numPorts()):
            for laneId in range(4):
                self._TestRxInterrupt(localPort, laneId)
                self._TestAutoNegInterrupt(localPort, laneId)
        
    def hasLinkStatus(self):
        return False
    
    def testAlarmAccess(self):
        self.skipTest("Hardware must support current alarm status for: %s" % self.supportedAlarms())

    def testInterruptMask(self):
        self.skipTest("Hardware must support interrupt masks for: %s" % self.supportedAlarms())

class Faceplate1000BaseXK30_7(AnnaMroTestCase):
    @classmethod
    def canRun(cls):
        return cls.is20G()

    @classmethod
    def setUpClass(cls):
        cls.runCli("device init")

    def portCliIds(self):
        return "1-%d" % self.numFaceplatePorts()

    def ethPortCliIds(self):
        return "2-%d" % (self.numFaceplatePorts() + 1)

    def setUp(self):
        self.assertCliSuccess("serdes mode %s 1G" % self.portCliIds())
        self.assertCliSuccess("eth port interface %s 1000basex" % self.ethPortCliIds())

    @classmethod
    def defaultRegisterProvider(cls):
        def atreg():
            if cls.productCode() == 0x60290021:
                return "AF6CNC0021_RD_SGMII_Multirate.atreg"
            else:
                return "AF6CNC0022_RD_SGMII_Multirate.atreg"

        provider = cls.registerProvider(atreg())
        provider.baseAddress = 0x0030000
        return provider

    def testForceK30_7(self):
        portsIdString = self.ethPortCliIds()

        for forced in [True, False]:
            # Force
            commandToken = "force" if forced else "unforce"
            self.assertCliSuccess("eth port %s k30_7 %s" % (commandToken, portsIdString))

            # Make sure that registers are accessed correctly
            reg = self.defaultRegisterProvider().getRegister("GE_1000basex_Force_K30_7", addressOffset=0, readHw=True)
            for portId in range(self.numFaceplatePorts()):
                field = AtRegisterField(portId, portId, "tx_fk30_7")
                reg.assertFieldEqual(field, 1 if forced else 0)

            # Output must be also displayed
            result = self.runCli("show eth port %s" % portsIdString)
            for column in range(1, result.numColumns()):
                cellContent = result.cellContent("K30.7Forced", column)
                expectedContent = "en" if forced else "dis"
                self.assertEqual(cellContent, expectedContent)

    def testCanOnlyForcedOn1000Basex(self):
        def Test():
            self.assertCliFailure("eth port force k30_7 2")
            self.assertCliSuccess("eth port unforce k30_7 2")
            result = self.runCli("show eth port 2")
            cellContent = result.cellContent("K30.7Forced", 1)
            self.assertEqual(cellContent, "N/S")

        self.assertCliSuccess("serdes mode 1 10G")
        Test()

        self.assertCliSuccess("serdes mode 1 1G")
        self.assertCliSuccess("eth port interface 2 sgmii")
        Test()


def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(AnnaFaceplate1GEthAlarm)
    runner.run(AnnaFaceplate10GEthAlarm)
    runner.run(AnnaBackplanePwEthPortAlarm)
    runner.run(AnnaBackplaneEthAlarm)
    runner.run(AnnaBackplaneLaneEthAlarm)
    runner.run(Faceplate1000BaseXK30_7)
    runner.summary()
    
if __name__ == '__main__':
    TestMain()

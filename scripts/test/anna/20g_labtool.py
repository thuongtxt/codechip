from test.AtTestCase import AtTestCaseRunner
from test.anna.AnnaTestCase import AnnaMroTestCase


class EthLoopback(AnnaMroTestCase):
    @classmethod
    def canRun(cls):
        return cls.is20G()

    @classmethod
    def setUpClass(cls):
        cls.runCli("device init")

    @classmethod
    def defaultRegisterProvider(cls):
        provider = cls.registerProvider("AF6CNC0022_RD_GLB.atreg")
        provider.baseAddress = 0
        return provider

    def testMakeBackplaneMacLoopback(self):
        reg = self.defaultRegisterProvider().getRegister("DebugMacLoopControl", addressOffset=0, readHw=True)
        
        self.assertCliSuccess("debug ciena mro backplane mac loopback local")
        reg.rd()
        reg.assertFieldEqual("Mac40gLoopIn", 1)
        self.assertCliSuccess("debug ciena mro backplane mac loopback release")
        reg.rd()
        reg.assertFieldEqual("Mac40gLoopIn", 0)

        self.assertCliFailure("debug ciena mro backplane mac loopback remote")

    def testMakeFaceplateMacLoopback(self):
        reg = self.defaultRegisterProvider().getRegister("DebugMacLoopControl", addressOffset=0, readHw=True)

        self.assertCliSuccess("debug ciena mro faceplate mac loopback remote")
        reg.rd()
        reg.assertFieldEqual("MacMroLoopOut", 1)
        self.assertCliSuccess("debug ciena mro faceplate mac loopback release")
        reg.rd()
        reg.assertFieldEqual("MacMroLoopOut", 0)

        self.assertCliFailure("debug ciena mro faceplate mac loopback local")

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(EthLoopback)
    runner.summary()

if __name__ == '__main__':
    TestMain()
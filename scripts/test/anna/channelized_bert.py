from time import sleep

import time

from python.arrive.atsdk.cli import AtCliRunnable
from test.AtTestCase import AtTestCaseRunner
from test.anna.AnnaTestCase import AnnaTestCase
from utils.audit import RegisterCollector, Expector


class BertChecker(AtCliRunnable):
    @classmethod
    def runCli(cls, cli):
        result = AtCliRunnable.runCli(cli)
        assert result.success()
        return result

    def __init__(self, cliIds, simulated = False):
        self.cliIds = cliIds
        self.simulated = simulated

    def clearCounters(self):
        self.runCli("show prbs engine counters %s r2c silent" % self.cliIds)

    def clearSticky(self):
        self.runCli("show prbs engine %s" % self.cliIds)

    def clearStatus(self):
        self.clearCounters()
        self.clearSticky()

    def sleep(self, seconds):
        if not self.simulated:
            time.sleep(seconds)

    def assertGoodCounters(self):
        def greaterThanZero(value):
            return value > 0

        def equalToZero(value):
            return value == 0

        counters = self.runCli("show prbs engine counters %s r2c" % self.cliIds)
        for row in range(counters.numRows()):
            def checkCounter(name, checkFunction):
                cellContent = counters.cellContent(row, name).lower()
                if cellContent in ["n/s", "n/a"]:
                    return

                if not self.simulated:
                    assert checkFunction(int(cellContent))

            def checkGoodCounter(counterName):
                checkCounter(counterName, greaterThanZero)

            def checkErrorCounter(counterName):
                checkCounter(counterName, equalToZero)

            checkGoodCounter("txFrame")
            checkGoodCounter("TxBit")
            checkGoodCounter("rxFrame")
            checkGoodCounter("RxBit")
            checkErrorCounter("RxBitError")
            checkErrorCounter("RxBitErrorLastSync")
            checkGoodCounter("RxSync")
            checkErrorCounter("RxLoss")
            checkErrorCounter("RxLostFrame")
            checkErrorCounter("RxErrorFrame")

    def assertGoodAlarms(self):
        cliResult = self.runCli("show prbs engine %s" % self.cliIds)
        for row in range(cliResult.numRows()):
            def check(status, expectedStatus):
                if not self.simulated:
                    assert status == expectedStatus

            check(cliResult.cellContent(row, "alarm").lower() , "none")
            check(cliResult.cellContent(row, "sticky").lower(), "none")

    def assertGood(self, retryTimes=1):
        countersGood = False
        alarmGood = False
        exception = None

        for i in range(retryTimes):
            if not countersGood:
                try:
                    self.assertGoodCounters()
                    countersGood = True
                except Exception as e:
                    self.clearCounters()
                    exception = e

            if not alarmGood:
                try:
                    self.assertGoodAlarms()
                    alarmGood = True
                except Exception as e:
                    self.clearSticky()
                    exception = e

            if countersGood and alarmGood:
                return

            self.sleep(1)

        raise exception

class ConfigureProvider(AtCliRunnable):
    PRODUCT_20G = "20g"
    PRODUCT_10G = "10g"
    PRODUCT_PDH = "PDH"

    def assertMappingNotChanged(self):
        assert 0

    def createParentBerts(self):
        assert 0

    def deleteParentBerts(self):
        self.runCli("prbs engine delete %s" % self.parentBertCliIdString())

    @classmethod
    def runCli(cls, cli):
        """
        :rtype: python.arrive.AtAppManager.AtCliResult.AtCliResult
        """
        result = AtCliRunnable.runCli(cli)
        assert result.success()
        return result

    def init(self):
        self.runCli("device init")

    def apply(self):
        self.init()
        self.setupLines()
        self.setupMapping()
        self.setupCrossConnect()
        self.setupPws()
        self.setupLeafBerts()

    def __init__(self, registerManager, simulated = False, regProvider = None, product = PRODUCT_10G):
        """
        :type registerManager: test.anna.AnnaTestCase.RegisterManager
        """
        self.simulated = simulated
        self.regProvider = regProvider
        self.registerManager = registerManager
        self.product = product

    def sleep(self, seconds):
        if not self.simulated:
            sleep(seconds)

    @staticmethod
    def faceplateLineIdList():
        """
        :rtype: list
        """
        return [1, 3]

    def faceplateLineCliIds(self):
        return ",".join(["%d" % portId for portId in self.faceplateLineIdList()])

    def leafBertCliIdString(self):
        return "1-26"  # Two last engines are for parent BERTs

    def parentBertCliIdString(self):
        return "27,28"

    def parentCliIds(self):
        return "Invalid"

    def parentType(self):
        return "Invalid"

    def parentMapType(self):
        return "Invalid"

    def pwCircuitIdString(self):
        return "Invalid"

    def pwCircuitType(self):
        return "Invalid"

    def setupMapping(self):
        assert 0

    @staticmethod
    def pwCliIds():
        return ["%d" % pwId for pwId in range(1, 29)]

    def pwCliIdsString(self):
        return ",".join(self.pwCliIds())

    def assertBertGood(self, engineCliIds):
        bertChecker = BertChecker(engineCliIds, simulated=self.simulated)
        bertChecker.clearStatus()
        self.sleep(5)
        bertChecker.assertGood()
        del bertChecker

    def assertLeafBertGood(self):
        self.assertBertGood(self.leafBertCliIdString())

    def assertParentBertGood(self):
        self.assertBertGood(self.parentBertCliIdString())

    def clearPwStatus(self):
        self.runCli("show pw counters %s r2c silent" % self.pwCliIdsString())

    def assertPwEnabled(self, pwCliIdsString, enabled):
        cliResult = self.runCli("show pw %s" % pwCliIdsString)
        for row in range(cliResult.numRows()):
            cellContent = cliResult.cellContent(row, "enabled")
            assert cellContent == ("en" if enabled else "dis")

    def assertPwPackets(self, hasPackets):
        for pwCliId in self.pwCliIds():
            cliResult = self.runCli("show pw counters %s r2c" % pwCliId)

            def assertCounter(counterName):
                cellContent = cliResult.cellContent(counterName, columnIdOrName=1)
                if self.simulated:
                    return

                if hasPackets:
                    assert int(cellContent) > 0
                else:
                    assert int(cellContent) == 0

            assertCounter("RxPackets")
            assertCounter("RxPayloadBytes")
            assertCounter("TxPackets")
            assertCounter("TxPayloadBytes")

    def assertLineGood(self, lineCliId):
        cliResult = self.runCli("show sdh line alarm %s" % lineCliId)
        assert cliResult.numRows() > 0
        for row in range(cliResult.numRows()):
            for column in range(1, cliResult.numColumns()):
                status = cliResult.cellContent(row, column).lower()
                assert status == "clear"

    def assertAllLinesGood(self):
        for lineId in self.faceplateLineIdList():
            self.assertLineGood(lineId)

    def assertLinesConnected(self):
        assert len(self.faceplateLineIdList()) == 2

        def makeTtiMessage(lineId_):
            return "Line%sTti" % lineId_

        # Send TTIs
        reversedLines = list(self.faceplateLineIdList())
        reversedLines.reverse()
        for lineId, pairLine in zip(self.faceplateLineIdList(), reversedLines):
            txMessage = makeTtiMessage(lineId)
            self.runCli("sdh line tti transmit %s 16bytes %s null_padding" % (lineId, txMessage))
            expectedMessage = makeTtiMessage(pairLine)
            self.runCli("sdh line tti expect %s 16bytes %s null_padding" % (lineId, expectedMessage))

        # Give HW a moment
        self.sleep(1)

        # Make sure that TTI is received
        for lineId, pairLine in zip(self.faceplateLineIdList(), reversedLines):
            cliResult = self.runCli("show sdh line tti %s" % lineId)
            rxMessage = cliResult.cellContent(0, "rxTti")
            expectedMessage = makeTtiMessage(pairLine)
            assert rxMessage == expectedMessage

    def assertTrafficGood(self):
        self.assertLinesConnected()
        self.assertAllLinesGood()
        self.assertLeafBertGood()

    def assertRxEnabled(self, enabled = True):
        for pwCliId in self.pwCliIds():
            cliResult = self.runCli("debug pw table %s" % pwCliId)
            cellContent = cliResult.cellContent(0, "CLA enabled")
            expectValue = "en" if enabled else "dis"
            assert cellContent == expectValue

    def bindPws(self):
        self.runCli("pw circuit bind %s %s.%s" % (self.pwCliIdsString(), self.pwCircuitType(), self.pwCircuitIdString()))

    def setupLines(self):
        lineIds = self.faceplateLineCliIds()
        self.runCli("serdes mode %s stm16" % lineIds)
        self.runCli("serdes powerup %s" % lineIds)
        self.runCli("serdes reset %s" % lineIds)
        self.runCli("sdh line rate %s stm16" % lineIds)

    def setupCrossConnect(self):
        assert 0

    def createPws(self, pwCliIds):
        assert 0

    def setupPws(self):
        pwCliIds = self.pwCliIdsString()
        self.createPws(pwCliIds)
        self.bindPws()
        self.runCli("pw ethport %s 1" % pwCliIds)
        self.runCli("pw psn %s mpls" % pwCliIds)
        self.runCli("pw enable %s" % pwCliIds)

    def setupLeafBerts(self):
        assert 0


    def pwsIdStringToSquel(self):
        return self.pwCliIdsString()

class VcChannelizedConfigureProvider(ConfigureProvider):
    def assertMappingNotChanged(self):
        cliResult = self.runCli("show sdh map %s.%s" % (self.parentType(), self.parentCliIds()))
        assert cliResult.numRows() == 2
        for row in range(cliResult.numRows()):
            mapType = cliResult.cellContent(row, "MappingType")
            assert mapType == self.parentMapType()

    def createParentBerts(self):
        self.runCli("prbs engine create vc %s %s.%s" % (self.parentBertCliIdString(), self.parentType(), self.parentCliIds()))

    @staticmethod
    def pwCliIds():
        return ["%d" % pwId for pwId in range(1, 29)]

    def pwCircuitIdString(self):
        return "25.1.1.1.1-25.1.1.7.4"

    def pwCircuitType(self):
        return "vc1x"

    def setupMapping(self):
        self.runCli("sdh map vc3.25.1.1,26.1.1 7xtug2s")
        self.runCli("sdh map tug2.25.1.1.1-25.1.1.7,26.1.1.1-26.1.1.7 tu11")
        self.runCli("sdh map vc1x.25.1.1.1.1-25.1.1.7.4,26.1.1.1.1-26.1.1.7.4 c1x")

    def setupCrossConnect(self):
        self.runCli("xc vc connect vc3.1.1.1,3.1.1 vc3.25.1.1,26.1.1 two-way")

    def createPws(self, pwCliIds):
        self.runCli("pw create cep %s basic" % pwCliIds)

    def setupLeafBerts(self):
        self.runCli("prbs engine create vc %s vc1x.26.1.1.1.1-26.1.1.7.2" % self.leafBertCliIdString())
        self.runCli("prbs engine enable %s" % self.leafBertCliIdString())


class Vc3ChannelizedConfigureProvider(VcChannelizedConfigureProvider):
    def parentCliIds(self):
        return "25.1.1,26.1.1"

    def parentType(self):
        return "vc3"

    def parentMapType(self):
        return "7xtug2s"

class Vc4ChannelizedVc1xProvider(VcChannelizedConfigureProvider):
    def parentCliIds(self):
        return "25.1,26.1"

    def parentType(self):
        return "vc4"

    def parentMapType(self):
        return "3xtug3s"

    def setupMapping(self):
        self.runCli("sdh map aug1.1.1,3.1,25.1,26.1 vc4")
        self.runCli("sdh map vc4.25.1,26.1 3xtug3s")
        self.runCli("sdh map tug3.25.1.1,26.1.1 7xtug2s")
        self.runCli("sdh map tug2.25.1.1.1-25.1.1.7,26.1.1.1-26.1.1.7 tu11")
        self.runCli("sdh map vc1x.25.1.1.1.1-25.1.1.7.4,26.1.1.1.1-26.1.1.7.4 c1x")

    def setupCrossConnect(self):
        self.runCli("xc vc connect vc4.1.1,3.1 vc4.25.1,26.1 two-way")

class Vc4ChannelizedVc3Provider(Vc4ChannelizedVc1xProvider):
    def leafBertCliIdString(self):
        return "1-3"

    def parentBertCliIdString(self):
        return "4-5"

    @staticmethod
    def pwCliIds():
        return ["%d" % (pwId + 1) for pwId in range(3)]

    def pwCircuitType(self):
        return "vc3"

    def pwCircuitIdString(self):
        return "25.1.1-25.1.3"

    def setupMapping(self):
        self.runCli("sdh map aug1.1.1,3.1,25.1,26.1 vc4")
        self.runCli("sdh map vc4.25.1,26.1 3xtug3s")
        self.runCli("sdh map tug3.25.1.1-25.1.3,26.1.1-26.1.3 vc3")
        self.runCli("sdh map vc3.25.1.1-25.1.3,26.1.1-26.1.3 c3")

    def setupLeafBerts(self):
        self.runCli("prbs engine create vc %s vc3.26.1.1-26.1.3" % self.leafBertCliIdString())
        self.runCli("prbs engine enable %s" % self.leafBertCliIdString())

class De3ChannelizedConfigureProvider(ConfigureProvider):
    def pwCircuitIdString(self):
        return "25.1.1.1.1-25.1.1.7.4"

    def pwCircuitType(self):
        return "de1"

    def parentMapType(self):
        return "ds3_cbit_28ds1"

    def setupMapping(self):
        self.runCli("sdh map vc3.25.1.1,26.1.1 de3")
        self.runCli("pdh de3 framing 25.1.1,26.1.1 %s" % self.parentMapType())

    def setupCrossConnect(self):
        self.runCli("xc vc connect vc3.1.1.1,3.1.1 vc3.25.1.1,26.1.1 two-way")

    def assertMappingNotChanged(self):
        cliResult = self.runCli("show pdh de3 %s" % self.parentCliIds())
        assert cliResult.numRows() == 2
        for row in range(cliResult.numRows()):
            frameType = cliResult.cellContent(row, "Framingmode")
            assert frameType == self.parentMapType()

    def createPws(self, pwCliIds):
        self.runCli("pw create satop %s" % pwCliIds)

    def setupLeafBerts(self):
        self.runCli("prbs engine create de1 %s 26.1.1.1.1-26.1.1.7.2" % self.leafBertCliIdString())
        self.runCli("prbs engine enable %s" % self.leafBertCliIdString())

    def parentType(self):
        return "vc3"

    def parentCliIds(self):
        return "25.1.1,26.1.1"

    def createParentBerts(self):
        self.runCli("prbs engine create de3 %s %s" % (self.parentBertCliIdString(), self.parentCliIds()))

class Vc1xMapDe1Provider(Vc4ChannelizedVc1xProvider):
    def setupMapping(self):
        super(Vc1xMapDe1Provider, self).setupMapping()
        self.runCli("sdh map vc1x.25.1.1.1.1-25.1.1.7.4,26.1.1.1.1-26.1.1.7.4 de1")

    def pwsIdStringToSquel(self):
        return "1"

    def parentCliIds(self):
        return "25.1.1.1.1,26.1.1.1.1"

    def parentType(self):
        return "vc1x"

    def parentMapType(self):
        return "de1"

    def pwCircuitType(self):
        return "de1"

    def createPws(self, pwCliIds):
        self.runCli("pw create satop %s" % pwCliIds)

    def setupLeafBerts(self):
        self.runCli("prbs engine create de1 %s 26.1.1.1.1-26.1.1.7.2" % self.leafBertCliIdString())
        self.runCli("prbs engine enable %s" % self.leafBertCliIdString())


class De1MapNxDs0Provider(Vc1xMapDe1Provider):
    @staticmethod
    def frameType():
        return "ds1_sf"

    def setupMapping(self):
        super(De1MapNxDs0Provider, self).setupMapping()
        self.runCli("pdh de1 framing 25.1.1.1.1-25.1.1.7.4,26.1.1.1.1-26.1.1.7.4 %s" % self.frameType())
        self.runCli("pdh de1 nxds0create 25.1.1.1.1-25.1.1.7.4,26.1.1.1.1-26.1.1.7.4 0-23")

    def parentCliIds(self):
        return "25.1.1.1.1,26.1.1.1.1"

    def pwsIdStringToSquel(self):
        return "1"

    def parentType(self):
        return "de1"

    def parentMapType(self):
        return self.frameType()

    def pwCircuitType(self):
        return "nxds0"

    def pwCircuitIdString(self):
        return "25.1.1.1.1-25.1.1.7.4.0-23"

    def createPws(self, pwCliIds):
        self.runCli("pw create cesop %s basic" % pwCliIds)

    def setupLeafBerts(self):
        self.runCli("prbs engine create nxds0 %s 26.1.1.1.1-26.1.1.7.2 0-23" % self.leafBertCliIdString())
        self.runCli("prbs engine enable %s" % self.leafBertCliIdString())

    def createParentBerts(self):
        self.runCli("prbs engine create de1 %s %s" % (self.parentBertCliIdString(), self.parentCliIds()))

    def assertMappingNotChanged(self):
        cliResult = self.runCli("show pdh de1 %s" % self.parentCliIds())
        assert cliResult.numRows() == 2
        for row in range(cliResult.numRows()):
            frameType = cliResult.cellContent(row, "FramingMode")
            assert frameType == self.parentMapType()

class AbstractTest(AnnaTestCase):
    _provider = None

    @classmethod
    def setUpClass(cls):
        if cls.provider() is not None:
            cls.provider().apply()

    @classmethod
    def setProvider(cls, provider):
        cls._provider = provider

    @classmethod
    def provider(cls):
        """
        :rtype: ConfigureProvider
        """
        return cls._provider

    def createParentBerts(self):
        self.provider().createParentBerts()

    def deleteParentBerts(self):
        self.provider().deleteParentBerts()

    @classmethod
    def TestWithRunner(cls, runner):
        """
        :type runner: AtTestCaseRunner
        """
        for provider in cls.defaultProviders():
            cls.setProvider(provider)
            runner.run(cls)

    @classmethod
    def defaultProviders(cls):
        simulated = cls.app().isSimulated()

        if cls.isMroProduct():
            return [Vc3ChannelizedConfigureProvider(registerManager=cls.registerManager(), simulated=simulated),
                    Vc4ChannelizedVc1xProvider(registerManager=cls.registerManager(), simulated=simulated),
                    Vc4ChannelizedVc3Provider(registerManager=cls.registerManager(), simulated=simulated),
                    De3ChannelizedConfigureProvider(registerManager=cls.registerManager(), simulated=simulated),
                    Vc1xMapDe1Provider(registerManager=cls.registerManager(), simulated=simulated),
                    De1MapNxDs0Provider(registerManager=cls.registerManager(), simulated=simulated)]

        return list()

    def bertRegLookup(self, address, name):
        localAddress = self.registerManager().bertLocalAddress(address)
        regProvider = self.registerManager().bertRegisterProvider()
        reg = regProvider.getRegister(name=name, addressOffset=0, readHw=False)
        reg.address = address

        if reg.startAddress() <= localAddress <= reg.endAddress():
            return reg

        return None

class ProvisioningTest(AbstractTest):
    @classmethod
    def canRun(cls):
        return cls.simulated()

    def tearDown(self):
        self.deleteParentBerts()

    def assertMappingNotChanged(self):
        self.provider().assertMappingNotChanged()

    def testCanCreateParentBertEngine(self):
        self.createParentBerts()
        self.deleteParentBerts()

    def testParentMappingIsNotChangedWhenCreatingOrDeletingHoBert(self):
        self.createParentBerts()
        self.assertMappingNotChanged()
        self.deleteParentBerts()
        self.assertMappingNotChanged()

    def testParentBertCannotSpecifyPsnSide(self):
        self.createParentBerts()
        bertIds = self.provider().parentBertCliIdString()
        self.assertCliFailure("prbs engine side monitoring %s psn" % bertIds)
        self.assertCliFailure("prbs engine side generating %s psn" % bertIds)

class TrafficTest(AbstractTest):
    def tearDown(self):
        super(TrafficTest, self).tearDown()
        self.deleteParentBerts()

    def setUp(self):
        self.provider().assertLeafBertGood()

    def clearStatus(self):
        self.provider().sleep(1)
        self.provider().clearPwStatus()
        self.provider().sleep(1)

    def testCreateBertWillSquelchLoPwTraffics(self):
        self.createParentBerts()
        self.provider().assertPwEnabled(pwCliIdsString=self.provider().pwsIdStringToSquel(), enabled=False)

    def testDeleteBertWillHaveLoPwTrafficRecovered(self):
        self.createParentBerts()
        self.deleteParentBerts()
        self.clearStatus()
        self.provider().assertLeafBertGood()

    def testParentBertMustBeFineWhenProvisioning(self):
        self.createParentBerts()
        self.provider().assertParentBertGood()

    def testAllPwsMustBeDisabled(self):
        self.createParentBerts()
        self.provider().assertParentBertGood()
        self.provider().assertPwEnabled(self.provider().pwsIdStringToSquel(), enabled=False)
        self.provider().assertPwPackets(hasPackets=False)
        self.deleteParentBerts()
        self.provider().assertPwEnabled(self.provider().pwsIdStringToSquel(), enabled=True)
        self.clearStatus()
        self.provider().assertPwPackets(hasPackets=True)

class SameConfigureProvider(ConfigureProvider, Expector):
    EXPECT_MODE_CHANNELIZED   = 1
    EXPECT_MODE_UNCHANNELIZED = 2
    expectMode = EXPECT_MODE_CHANNELIZED

    def unchannelizedScript(self):
        return "Invalid"

    def channelizedScript(self):
        return "Invalid"

    def provisionPrbsScript(self):
        return "Invalid"

    @staticmethod
    def deProvisionPrbsScript():
        return """
        prbs engine delete 1
        """

    @staticmethod
    def shouldCheckLocalAddress(regProvider, localAddress, ignoreRegNames):
        for regName in ignoreRegNames:
            reg = regProvider.getRegister(name=regName, addressOffset=0, readHw=False)
            if reg.startAddress() <= localAddress <= reg.endAddress():
                return False

        return True

    def pdhRegLookup(self, address, regName, readHw=False):
        localAddress = self.registerManager.pdhLocalAddress(address)
        sliceId = self.registerManager.pdhAddressSliceId(address)
        regProvider = self.registerManager.pdhRegisterProvider(sliceId)
        reg = regProvider.getRegister(name=regName, addressOffset=0, readHw=False)
        reg.address = address
        if readHw:
            reg.rd()

        if reg.startAddress() <= localAddress <= reg.endAddress():
            return reg

        return None

    def ocnRegLookup(self, address, name):
        localAddress = self.registerManager.ocnLocalAddress(address)
        regProvider = self.registerManager.ocnRegisterProvider()
        reg = regProvider.getRegister(name=name, addressOffset=0, readHw=False)
        reg.address = address

        if reg.startAddress() <= localAddress <= reg.endAddress():
            return reg

        return None

    def pohRegLookup(self, address, name):
        localAddress = self.registerManager.pohLocalAddress(address)
        regProvider = self.registerManager.pohRegisterProvider()
        reg = regProvider.getRegister(name=name, addressOffset=0, readHw=False)
        reg.address = address

        if reg.startAddress() <= localAddress <= reg.endAddress():
            return reg

        return None
    
class Vc4Vc3De3_Vc3Bert_SameConfigureProvider(SameConfigureProvider):
    def unchannelizedScript(self):
        return """
        sdh line rate 1-8 stm1
        serdes mode 1 stm64
        sdh line rate 1 stm64
        serdes powerup 1
        serdes enable 1
        sdh line mode 1 sdh
        sdh line loopback 1 release

        sdh line mode 25 sdh
        sdh line loopback 25 release

        sdh map aug1.1.1 vc4

        sdh map aug1.25.1 vc4
        sdh map vc4.25.1 3xtug3s
        sdh map tug3.25.1.1 vc3
        sdh map vc3.25.1.1 c3

        xc vc connect vc4.25.1 vc4.1.1 two-way

        sdh path tti transmit vc4.1.1 16bytes ATVN null_padding
        sdh path tti expect vc4.1.1 16bytes ATVN null_padding
        sdh path psl expect vc4.1.1 0x4
        sdh path psl transmit vc4.1.1 0x4

        serdes loopback 25,26 local

        serdes loopback 1 local
        %s
        """ % self.provisionPrbsScript()

    def provisionPrbsScript(self):
        return """
        prbs engine create vc 1 vc3.25.1.1
        prbs engine enable 1
        """

    def channelizedScript(self):
        return """
        sdh line rate 1-8 stm1
        serdes mode 1 stm64
        sdh line rate 1 stm64
        serdes powerup 1
        serdes enable 1
        sdh line mode 1 sdh
        sdh line loopback 1 release

        sdh line mode 25 sdh
        sdh line loopback 25 release

        sdh map aug1.1.1 vc4

        sdh map aug1.25.1 vc4
        sdh map vc4.25.1 3xtug3s
        sdh map tug3.25.1.1 vc3
        sdh map vc3.25.1.1 de3
        pdh de3 framing 25.1.1 ds3_unframed
        pdh de3 timing 25.1.1 system 1

        xc vc connect vc4.25.1 vc4.1.1 two-way

        sdh path tti transmit vc4.1.1 16bytes ATVN null_padding
        sdh path tti expect vc4.1.1 16bytes ATVN null_padding
        sdh path psl expect vc4.1.1 0x4
        sdh path psl transmit vc4.1.1 0x4


        pw create satop 1 
        pw circuit bind 1 de3.25.1.1
        pw jitterbuffer 1 6000
        pw jitterdelay 1 3000
        pw payloadsize 1 1024
        pw psn 1 mpls
        eth port enable 1
        eth port srcmac 1 22.44.22.44.22.44
        pw ethport 1 1
        pw ethheader 1 11.22.11.22.11.22 none none
        pw enable 1


        serdes loopback 25,26 local

        serdes loopback 1 local
        """
    def shouldCheckPohAddress(self, address):
        if self.registerManager.isPohAddress(address):
            if self.pohRegLookup(address, "pohcpestsctr") or \
                self.pohRegLookup(address, "pohcpevtctr"):
                return False
        
    def shouldCheckAddress(self, address):
        if self.registerManager.isPdaAddress(address) or \
                self.registerManager.isPlaAddress(address) or \
                self.registerManager.isClaAddress(address) or \
                not self.shouldCheckPohAddress(address):
            if self.expectMode == self.EXPECT_MODE_UNCHANNELIZED:
                return False

        return True
    
    def pdhShortCompareMask(self, address):
        if self.product == self.PRODUCT_20G:
            if self.expectMode == self.EXPECT_MODE_CHANNELIZED:
                return self.allMask()

            reg = self.pdhRegLookup(address, "stsvt_demap_ctrl")
            if reg is not None:
                return reg.fieldByName("StsVtDemapFrmtype").mask() | reg.fieldByName("StsVtDemapSigtype").mask()

        return self.allMask()

    def shortCompareMask(self, address):
        if self.registerManager.isPdhAddress(address):
            return self.pdhShortCompareMask(address)
            
        return self.allMask()
    
class Vc4Vc3De3_Vc4Bert_SameConfigureProvider(Vc4Vc3De3_Vc3Bert_SameConfigureProvider):
    def unchannelizedScript(self):
        return """
        sdh line rate 1-8 stm1
        serdes mode 1 stm64
        sdh line rate 1 stm64
        serdes powerup 1
        serdes enable 1
        sdh line mode 1 sdh
        sdh line loopback 1 release

        sdh line mode 25 sdh
        sdh line loopback 25 release

        sdh map aug1.1.1 vc4

        sdh map aug1.25.1 vc4
        sdh map vc4.25.1 c4

        xc vc connect vc4.25.1 vc4.1.1 two-way

        sdh path tti transmit vc4.1.1 16bytes ATVN null_padding
        sdh path tti expect vc4.1.1 16bytes ATVN null_padding
        sdh path psl expect vc4.1.1 0x4
        sdh path psl transmit vc4.1.1 0x4

        serdes loopback 25,26 local

        serdes loopback 1 local
        %s
        """ % self.provisionPrbsScript()

    def provisionPrbsScript(self):
        return """
        prbs engine create vc 1 vc4.25.1
        prbs engine enable 1
        """
    
class Vc3UnChannelizedSameConfigureProvider(SameConfigureProvider):
    def unchannelizedScript(self):
        return """
        sdh line loopback 1 local
        xc vc connect vc3.1.1.1 vc3.25.1.1 two-way
        sdh map vc3.25.1.1 7xtug2s
        sdh map vc3.25.1.1 c3
        %s
        """ % self.provisionPrbsScript()

    def provisionPrbsScript(self):
        return """
        prbs engine create vc 1 vc3.25.1.1
        prbs engine enable 1
        """

    def channelizedScript(self):
        return """
        sdh line loopback 1 local
        xc vc connect vc3.1.1.1 vc3.25.1.1 two-way
        sdh map vc3.25.1.1 7xtug2s
        sdh map tug2.25.1.1.1 tu11
        sdh map vc1x.25.1.1.1.1 de1
        pw create satop 1
        pw circuit bind 1 de1.25.1.1.1.1
        pw ethport 1 1
        pw psn 1 mpls
        pw enable 1
        """

    def shouldCheckCdrAddress(self, address):
        if self.expectMode == self.EXPECT_MODE_CHANNELIZED:
            return True

        if self.expectMode == self.EXPECT_MODE_UNCHANNELIZED:
            if self.product == self.PRODUCT_10G:
                if self.registerManager.isHoCdrAddress(address):
                    return True

                return False

        return True

    def shouldCheckPdhAddress(self, address):
        if self.product == self.PRODUCT_20G:
            if self.expectMode == self.EXPECT_MODE_UNCHANNELIZED:
                ignoreRegs = ["dej1_tx_framer_ctrl", "dej1_tx_framer_sign_insertion_ctrl", "dej1_rx_framer_ctrl"]
                for regName in ignoreRegs:
                    if self.pdhRegLookup(address, regName) is not None:
                        return False

                return True

        if self.product == self.PRODUCT_10G:
            if self.expectMode == self.EXPECT_MODE_UNCHANNELIZED:
                return False

        return True

    def shouldCheckAddress(self, address):
        if self.registerManager.isCdrAddress(address):
            return self.shouldCheckCdrAddress(address)

        if self.registerManager.isPdhAddress(address):
            return self.shouldCheckPdhAddress(address)

        return True

    def pdhShortCompareMask(self, address):
        if self.product == self.PRODUCT_20G:
            if self.expectMode == self.EXPECT_MODE_CHANNELIZED:
                return self.allMask()

            reg = self.pdhRegLookup(address, "stsvt_demap_ctrl")
            if reg is not None:
                return reg.fieldByName("StsVtDemapFrmtype").mask() | reg.fieldByName("StsVtDemapSigtype").mask()

        return self.allMask()

    def shortCompareMask(self, address):
        if self.registerManager.isPdhAddress(address):
            return self.pdhShortCompareMask(address)

        return self.allMask()

class Vc1xUnChannelizedSameConfigureProvider(SameConfigureProvider):
    def unchannelizedScript(self):
        return """
        sdh line loopback 1 local
        xc vc connect vc3.1.1.1 vc3.25.1.1 two-way
        sdh map vc3.25.1.1 7xtug2s
        sdh map tug2.25.1.1.1-25.1.1.7 tu11
        sdh map vc1x.25.1.1.1.1-25.1.1.7.4 c1x
        %s
        """ % self.provisionPrbsScript()

    def provisionPrbsScript(self):
        return """
        prbs engine create vc 1-28 vc1x.25.1.1.1.1-25.1.1.7.4
        prbs engine enable 1-28
        """

    @staticmethod
    def deProvisionPrbsScript():
        return "prbs engine delete 1-28"

    def channelizedScript(self):
        return """
        sdh line loopback 1 local
        xc vc connect vc3.1.1.1 vc3.25.1.1 two-way
        sdh map vc3.25.1.1 7xtug2s
        sdh map tug2.25.1.1.1-25.1.1.7 tu11
        sdh map vc1x.25.1.1.1.1-25.1.1.7.4 de1
        pw create satop 1-28
        pw circuit bind 1-28 de1.25.1.1.1.1-25.1.1.7.4
        pw ethport 1-28 1
        pw psn 1-28 mpls
        pw enable 1-28
        """

    def shouldCheckPdhAddress(self, address):
        if self.expectMode == self.EXPECT_MODE_CHANNELIZED:
            return True

        localAddress = self.registerManager.pdhLocalAddress(address)
        regProvider = self.registerManager.pdhRegisterProvider()
        ignoreRegNames = ["dej1_rx_framer_ctrl", "dej1_tx_framer_ctrl", "dej1_tx_framer_sign_insertion_ctrl"]
        return self.shouldCheckLocalAddress(regProvider, localAddress, ignoreRegNames)

    def shouldCheckPohAddress(self, address):
        localAddress = self.registerManager.pohLocalAddress(address)
        regProvider = self.registerManager.pohRegisterProvider()
        ignoreRegNames = ["ter_ctrllo"] # See https://projects.zoho.com/portal/arrivetechnologies/#bugsview/403027000000038143/6
        return self.shouldCheckLocalAddress(regProvider, localAddress, ignoreRegNames)

    def shouldCheckAddress(self, address):
        if self.registerManager.isPdhAddress(address):
            return self.shouldCheckPdhAddress(address)

        if self.registerManager.isPohAddress(address):
            return self.shouldCheckPohAddress(address)

        return True

class VcDe1UnChannelizedSameConfigureProvider(SameConfigureProvider):
    def unchannelizedScript(self):
        return """
        sdh line loopback 1 local
        xc vc connect vc3.1.1.1 vc3.25.1.1 two-way
        sdh map vc3.25.1.1 7xtug2s
        sdh map tug2.25.1.1.1-25.1.1.7 tu11
        sdh map vc1x.25.1.1.1.1-25.1.1.7.4 de1
        pdh de1 framing 25.1.1.1.1-25.1.1.7.4 ds1_sf
        %s
        """ % self.provisionPrbsScript()

    def provisionPrbsScript(self):
        return """
        prbs engine create de1 1-28 25.1.1.1.1-25.1.1.7.4
        prbs engine enable 1-28
        """

    def channelizedScript(self):
        return """
        sdh line loopback 1 local
        xc vc connect vc3.1.1.1 vc3.25.1.1 two-way
        sdh map vc3.25.1.1 7xtug2s
        sdh map tug2.25.1.1.1-25.1.1.7 tu11
        sdh map vc1x.25.1.1.1.1-25.1.1.7.4 de1
        pdh de1 framing 25.1.1.1.1-25.1.1.7.4 ds1_sf
        pdh de1 nxds0create 25.1.1.1.1-25.1.1.7.4 0-15
        pw create cesop 1-28 basic
        pw circuit bind 1-28 nxds0.25.1.1.1.1-25.1.1.7.4.0-15
        pw ethport 1-28 1
        pw psn 1-28 mpls
        pw enable 1-28
        """

    @staticmethod
    def deProvisionPrbsScript():
        return "prbs engine delete 1-28"

    def mapLoShouldCheckAddress(self, address):
        if self.expectMode == self.EXPECT_MODE_CHANNELIZED:
            return True

        sliceId = self.registerManager.mapLoAddressSliceId(address)
        regProvider = self.registerManager.mapLoRegisterProvider(sliceId=sliceId)
        ignoredRegNames = ["demap_channel_ctrl",
                           "map_channel_ctrl"] # Just because PW binding will be still in NxDs0
        return self.shouldCheckLocalAddress(regProvider, sliceId, ignoredRegNames)

    def shouldCheckAddress(self, address):
        if self.registerManager.isMapLoAddress(address):
            return self.mapLoShouldCheckAddress(address)

        return True

    def pdhShortCompareMask(self, address):
        sliceId = self.registerManager.pdhAddressSliceId(address)
        regProvider = self.registerManager.pdhRegisterProvider(sliceId=sliceId)
        localAddress = self.registerManager.pdhLocalAddress(address)

        reg = regProvider.getRegister(name="dej1_tx_framer_ctrl", addressOffset=0, readHw=False)
        if reg.startAddress() <= localAddress <= reg.endAddress():
            ignoreMask = reg.fieldByName("TxDE1AutoAis").mask()
            return ~ignoreMask

        return self.allMask()

    def shortCompareMask(self, address):
        if self.registerManager.isPdhAddress(address):
            return self.pdhShortCompareMask(address)
        return self.allMask()

class VcDe1NxDS0_Vc1xUnChannelizedSameConfigureProvider(VcDe1UnChannelizedSameConfigureProvider):
    def provisionPrbsScript(self):
        return """
        prbs engine create vc 1-28 vc1x.25.1.1.1.1-25.1.1.7.4
        prbs engine enable 1-28
        """

    @staticmethod
    def deProvisionPrbsScript():
        return "prbs engine delete 1-28"
    
class VcDe1NxDS0_Vc3UnChannelizedSameConfigureProvider(VcDe1UnChannelizedSameConfigureProvider):
    def provisionPrbsScript(self):
        return """
        prbs engine create vc 1 vc3.25.1.1
        prbs engine enable 1
        """

    @staticmethod
    def deProvisionPrbsScript():
        return "prbs engine delete 1"
    
class De3De1UnChannelizedSameConfigureAbstractProvider(SameConfigureProvider):
    def shouldCheckCdrAddress(self, address):
        if self.expectMode == self.EXPECT_MODE_CHANNELIZED:
            return True

        sliceId = self.registerManager.cdrLoAddressSliceId(address)
        regProvider = self.registerManager.cdrLoRegisterProvider(sliceId=sliceId)
        localAddress = self.registerManager.cdrLocalAddress(address)

        reg = regProvider.getRegister(name="cdr_acr_eng_timing_ctrl", addressOffset=0, readHw=False)
        if reg.startAddress() <= localAddress <= reg.endAddress():
            cdrId = localAddress - reg.startAddress()
            return True if (cdrId % 28 == 0) else False

        return True

    def pdhUnChannelizedIgnoredRegNames(self):
        return ["rxm12e12_stsvc_payload_ctrl", "dej1_tx_framer_ctrl", "dej1_rx_framer_ctrl", "dej1_tx_framer_sign_insertion_ctrl"]

    def shouldCheckPdhAddress(self, address):
        if self.expectMode == self.EXPECT_MODE_CHANNELIZED:
            return True

        sliceId = self.registerManager.pdhAddressSliceId(address)
        regProvider = self.registerManager.pdhRegisterProvider(sliceId=sliceId)
        localAddress = self.registerManager.pdhLocalAddress(address)
        return self.shouldCheckLocalAddress(regProvider, localAddress, self.pdhUnChannelizedIgnoredRegNames())
    
    def shouldCheckPohAddress(self, address):
        if self.registerManager.isPohAddress(address):
            if self.pohRegLookup(address, "imemrwpctrl2"):
                return False
        
    def shouldCheckAddress(self, address):
        if self.registerManager.isLoCdrAddress(address):
            return self.shouldCheckCdrAddress(address)

        if self.registerManager.isPdhAddress(address):
            return self.shouldCheckPdhAddress(address)

        if self.registerManager.isPohAddress(address):
            return self.shouldCheckPohAddress(address)
        
        return True

    def provisionPrbsScript(self):
        return """
        prbs engine create de3 1 25.1.1
        prbs engine enable 1
        """

    def mappingScript(self):
        return ""

    def unchannelizedScript(self):
        return """
        %s
        pdh de3 framing 25.1.1 ds3_cbit_unchannelize
        %s
        """ % (self.mappingScript(), self.provisionPrbsScript())

    def channelizedScript(self):
        return """
        %s

        pdh de3 framing 25.1.1 ds3_cbit_28ds1
        pw create satop 1
        pw circuit bind 1 de1.25.1.1.1.1
        pw ethport 1 1
        pw psn 1 mpls
        pw enable 1
        """ % self.mappingScript()

class Vc3De3De1UnChannelizedSameConfigureProvider(De3De1UnChannelizedSameConfigureAbstractProvider):
    def mappingScript(self):
        return """
        sdh line loopback 1 local
        xc vc connect vc3.1.1.1 vc3.25.1.1 two-way
        sdh map vc3.25.1.1 de3
        """

    def pdhUnChannelizedIgnoredRegNames(self):
        regs = super(Vc3De3De1UnChannelizedSameConfigureProvider, self).pdhUnChannelizedIgnoredRegNames()
        regs.extend(["dej1_tx_framer_ctrl", "dej1_rx_framer_ctrl"])
        return regs
    
class Vc3De3De1NxDs0_De3UnChannelizedSameConfigureProvider(Vc3De3De1UnChannelizedSameConfigureProvider):
    def channelizedScript(self):
        return """
        %s

        pdh de3 framing 25.1.1 ds3_cbit_28ds1
        pdh de1 framing 25.1.1.1.1-25.1.1.7.4 ds1_sf
        pdh de1 nxds0create 25.1.1.1.1-25.1.1.7.4 0-15
        pw create cesop 1-28 basic
        pw circuit bind 1-28 nxds0.25.1.1.1.1-25.1.1.7.4.0-15
        pw ethport 1-28 1
        pw psn 1-28 mpls
        pw enable 1-28
        """ % self.mappingScript()
    
class Vc3De3De1NxDs0_Vc3UnChannelizedSameConfigureProvider(Vc3De3De1NxDs0_De3UnChannelizedSameConfigureProvider):
    def provisionPrbsScript(self):
        return """
        prbs engine create vc 1 vc3.25.1.1
        prbs engine enable 1
        """
            
class Vc3De3FrameSatopUnChannelizedSameConfigureProvider(Vc3De3De1UnChannelizedSameConfigureProvider):
    def channelizedScript(self):
        return """
        %s

        pdh de3 framing 25.1.1 ds3_cbit_28ds1
        pw create satop 1
        pw circuit bind 1 de3.25.1.1
        pw ethport 1 1
        pw psn 1 mpls
        pw enable 1
        """ % self.mappingScript()

class Vc4De3De1UnChannelizedSameConfigureProvider(De3De1UnChannelizedSameConfigureAbstractProvider):
    def mappingScript(self):
        return """
        sdh line loopback 1 local
        sdh map aug1.1.1,25.1 vc4
        xc vc connect vc4.1.1 vc4.25.1 two-way
        sdh map vc4.25.1 3xtug3s
        sdh map tug3.25.1.1 vc3
        sdh map vc3.25.1.1 de3
        """

class Vc4UnChannelizedSameConfigureAbstractProvider(SameConfigureProvider):
    def unchannelizedScript(self):
        return """
        sdh line loopback 1 local
        sdh map aug1.1.1,25.1 vc4
        xc vc connect vc4.1.1 vc4.25.1 two-way
        sdh map vc4.25.1 c4
        %s
        """ % self.provisionPrbsScript()

    def provisionPrbsScript(self):
        return """
        prbs engine create vc 1 vc4.25.1
        prbs engine enable 1
        """

    def mro20GOcnShortCompareMask(self, address):
        if self.expectMode == self.EXPECT_MODE_CHANNELIZED:
            return self.allMask()

        reg = self.ocnRegLookup(address, "spgramctl")
        if reg is not None:
            reg.rd()
            mask = self.allMask()
            if reg.fieldValueByName("LineStsPgStsSlvInd") == 1:  # Slave
                mask = mask & (~reg.fieldByName("LineStsPgPohIns").mask())
                return mask

        return self.allMask()

    def ocnShortCompareMask(self, address):
        reg = self.ocnRegLookup(address, "demramctl")
        if reg is not None:
            if self.expectMode == self.EXPECT_MODE_UNCHANNELIZED:
                return reg.fieldByName("PiDemStsTerm").mask()

            return self.allMask()

        if self.product == self.PRODUCT_20G:
            return self.mro20GOcnShortCompareMask(address)

        return self.allMask()

    def pdhShortCompareMask(self, address):
        reg = self.pdhRegLookup(address, "stsvt_demap_ctrl", readHw=True)
        if reg is None:
            return self.allMask()

        mask = self.allMask()
        stsVtDemapSigtype = reg.fieldValueByName("StsVtDemapSigtype")
        if stsVtDemapSigtype in [0, 11, 12, 13, 14, 15]:
            ignoredField = reg.fieldByName("StsVtDemapFrmtype").mask() | reg.fieldByName("StsVtDemapCepMode").mask()
            mask = mask & (~ignoredField)

        return mask

    def shortCompareMask(self, address):
        if self.registerManager.isOcnAddress(address):
            return self.ocnShortCompareMask(address)

        return self.allMask()

    # noinspection PyUnusedLocal
    def shouldCheckMapHoAddress(self, address):
        if self.expectMode == self.EXPECT_MODE_CHANNELIZED:
            return False
        return True

    def shouldCheckOcnAddress(self, address):
        if self.product == self.PRODUCT_20G:
            reg = self.ocnRegLookup(address, "pgdemramctl")
            if reg is not None:
                return False

        return True

    def shouldCheckAddress(self, address):
        if self.registerManager.isMapHoAddress(address):
            return self.shouldCheckMapHoAddress(address)

        if self.registerManager.isPdhAddress(address):
            if self.expectMode == self.EXPECT_MODE_UNCHANNELIZED:
                return False

        if self.registerManager.isOcnAddress(address):
            return self.shouldCheckOcnAddress(address)

        return True

class Vc4UnChannelizedSameConfigureProvider(Vc4UnChannelizedSameConfigureAbstractProvider):
    def channelizedScript(self):
        return """
        sdh line loopback 1 local
        sdh map aug1.1.1,25.1 vc4
        xc vc connect vc4.1.1 vc4.25.1 two-way
        sdh map vc4.25.1 3xtug3s
        sdh map tug3.25.1.1-25.1.3 7xtug2s
        sdh map tug2.25.1.1.1-25.1.3.7 tu11
        sdh map vc1x.25.1.1.1.1-25.1.3.7.4 c1x
        pw create cep 1 basic
        pw circuit bind 1 vc1x.25.1.1.1.1
        pw ethport 1 1
        pw psn 1 mpls
        pw enable 1
        """

class Vc4UnChannelizedWithMixSameConfigureProvider(Vc4UnChannelizedSameConfigureAbstractProvider):
    def unchannelizedScript(self):
        script = super(Vc4UnChannelizedWithMixSameConfigureProvider, self).unchannelizedScript() + "\n"

        # Below is to make access compare pass
        additionalScript = """
        sdh map aug1.1.2,25.2 vc4
        xc vc connect vc4.1.2 vc4.25.2 two-way
        sdh map vc4.25.2 3xtug3s
        """

        script = script + additionalScript
        return script

    def channelizedScript(self):
        return """
        sdh line loopback 1 local
        sdh map aug1.1.1,1.2,25.1,25.2 vc4
        xc vc connect vc4.1.1,1.2 vc4.25.1,25.2 two-way
        sdh map vc4.25.1,25.2 3xtug3s
        
        sdh map tug3.25.1.1 vc3
        sdh map vc3.25.1.1 de3
        pdh de3 framing 25.1.1 ds3_cbit_28ds1
        pdh de1 framing 25.1.1.1.1-25.1.1.7.4 ds1_sf
        pw create satop 1-28
        pw circuit bind 1-28 de1.25.1.1.1.1-25.1.1.7.4
        
        sdh map tug3.25.1.2 7xtug2s
        sdh map tug2.25.1.2.1-25.1.2.7 tu11
        
        sdh map vc1x.25.1.2.1.1-25.1.2.2.4 c1x
        pw create cep 29-36 basic
        pw circuit bind 29-36 vc1x.25.1.2.1.1-25.1.2.2.4
        
        sdh map vc1x.25.1.2.3.1-25.1.2.7.4 de1
        pw create satop 37-56
        pw circuit bind 37-56 de1.25.1.2.3.1-25.1.2.7.4
        
        sdh map tug3.25.1.3 vc3
        sdh map vc3.25.1.3 c3
        pw create cep 57 basic
        pw circuit bind 57 vc3.25.1.3
        
        pw ethport 1-57 1
        pw psn 1-57 mpls
        pw enable 1-57
        """

    def shortCompareMask(self, address):
        if self.registerManager.isPdhAddress(address):
            return self.pdhShortCompareMask(address)

        return super(Vc4UnChannelizedWithMixSameConfigureProvider, self).shortCompareMask(address)

class Ds3UnChannelizedSameConfigureProvider(SameConfigureProvider):
    def unchannelizedScript(self):
        return """
        sdh line loopback 1 local
        sdh line ms ber enable 1
        sdh line ms ber disable 1
        xc vc connect vc3.1.1.1 vc3.25.1.1 two-way
        sdh map vc3.25.1.1 de3
        pdh de3 framing 25.1.1 ds3_cbit_unchannelize
        %s
        """ % self.provisionPrbsScript()

    def provisionPrbsScript(self):
        return """
        prbs engine create de3 1 25.1.1
        prbs engine enable 1
        """

    def channelizedScript(self):
        return """
        sdh line loopback 1 local
        sdh line ms ber enable 1
        sdh line ms ber disable 1
        xc vc connect vc3.1.1.1 vc3.25.1.1 two-way
        sdh map vc3.25.1.1 de3
        pdh de3 framing 25.1.1 ds3_cbit_28ds1
        pw create satop 1-28
        pw psn 1-28 mpls
        pw ethport 1-28 1
        pw enable 1-28
        """

    def shouldCheckCdrAddress(self, address):
        localAddress = self.registerManager.cdrLocalAddress(address)
        regProvider = self.registerManager.cdrLoRegisterProvider()
        
        reg = regProvider.getRegister(name="cdr_acr_eng_timing_ctrl", addressOffset=0, readHw=False)
        if reg.startAddress() <= localAddress <= reg.endAddress():
            CHID = localAddress - reg.startAddress()
            return True if (CHID % 28 == 0) else False

        return True

    def shouldCheckPdhAddress(self, address):
        localAddress = self.registerManager.pdhLocalAddress(address)
        regProvider = self.registerManager.pdhRegisterProvider()
        return self.shouldCheckLocalAddress(regProvider, localAddress, ["rxm12e12_stsvc_payload_ctrl"])

    def shouldCheckAddress(self, address):
        if self.registerManager.isLoCdrAddress(address):
            return self.shouldCheckCdrAddress(address)

        if self.registerManager.isPdhAddress(address):
            return self.shouldCheckPdhAddress(address)

        return True

class SameConfigureTest(AbstractTest):
    _configureProvider = None

    @classmethod
    def canRun(cls):
        return cls.simulated()

    def configureProvider(self):
        """
        :rtype: SameConfigureProvider
        """
        return self._configureProvider

    def testSameConfigure(self):
        # Capture configuration that has un-channalized PRBS work
        self.assertCliSuccess("device init")
        unchannelizedRegCollector = RegisterCollector()
        unchannelizedRegCollector.start()
        unchannelizedRegCollector.expector = self.configureProvider()
        self.assertCliSuccess(self.configureProvider().unchannelizedScript())
        unchannelizedRegCollector.stop()

        # Now start with channelized, then start PRBS on parent
        self.assertCliSuccess("device init")
        self.assertCliSuccess(self.configureProvider().channelizedScript())
        self.assertCliSuccess(self.configureProvider().provisionPrbsScript())

        # Check if we have same configuration by using audit tool
        self.configureProvider().expectMode = SameConfigureProvider.EXPECT_MODE_UNCHANNELIZED
        self.assertTrue(unchannelizedRegCollector.audit())

    def testDeProvision(self):
        # Capture channelized configuration that works
        self.assertCliSuccess("device init")
        channelizedRegCollector = RegisterCollector()
        channelizedRegCollector.expector = self.configureProvider()
        channelizedRegCollector.start()
        self.assertCliSuccess(self.configureProvider().channelizedScript())
        channelizedRegCollector.stop()

        # Now provision then de-provision and see if everything gets back to original state
        self.assertCliSuccess(self.configureProvider().provisionPrbsScript())
        self.assertCliSuccess(self.configureProvider().deProvisionPrbsScript())
        self.configureProvider().expectMode = SameConfigureProvider.EXPECT_MODE_CHANNELIZED
        self.assertTrue(channelizedRegCollector.audit())

    @classmethod
    def providerProduct(cls):
        if cls.isPdhProduct():
            return ConfigureProvider.PRODUCT_PDH
        if cls.is10G():
            return ConfigureProvider.PRODUCT_10G
        if cls.is20G():
            return ConfigureProvider.PRODUCT_20G

        return "Invalid"

    @classmethod
    def allProviders(cls):
        product = cls.providerProduct()

        def createProvider(providerClassName):
            return providerClassName(registerManager=cls.registerManager(), simulated=cls.simulated(), product=product)

        classes = [Vc3UnChannelizedSameConfigureProvider,
                   Vc4UnChannelizedSameConfigureProvider,
                   Vc4UnChannelizedWithMixSameConfigureProvider,
                   Vc1xUnChannelizedSameConfigureProvider,
                   Ds3UnChannelizedSameConfigureProvider,
                   VcDe1UnChannelizedSameConfigureProvider,
                   Vc3De3De1UnChannelizedSameConfigureProvider,
                   Vc4De3De1UnChannelizedSameConfigureProvider,
                   Vc4Vc3De3_Vc3Bert_SameConfigureProvider,
                   Vc4Vc3De3_Vc4Bert_SameConfigureProvider,
                   Vc3De3FrameSatopUnChannelizedSameConfigureProvider,
                   VcDe1NxDS0_Vc1xUnChannelizedSameConfigureProvider,
                   VcDe1NxDS0_Vc3UnChannelizedSameConfigureProvider,
                   Vc3De3De1NxDs0_De3UnChannelizedSameConfigureProvider,
                   Vc3De3De1NxDs0_Vc3UnChannelizedSameConfigureProvider]
        
        #classes = [Vc4UnChannelizedWithMixSameConfigureProvider]
        
        return [createProvider(className) for className in classes]

    @classmethod
    def TestWithRunner(cls, runner):
        providers = cls.allProviders()
        for provider in providers:
            cls._configureProvider = provider
            runner.run(cls)

class RejectTest(AbstractTest):
    def testCannotProvisionOnDs3M13(self):
        script = """
            device init
            sdh line loopback 1 local
            xc vc connect vc3.1.1.1 vc3.25.1.1 two-way
            sdh map vc3.25.1.1 de3
            pdh de3 framing 25.1.1 ds3_m13_28ds1
            pw create satop 1-28
            pw psn 1-28 mpls
            pw ethport 1-28 1
            pw enable 1-28
            """
        self.assertCliSuccess(script)
        self.assertCliFailure("prbs engine create de3 1 25.1.1")

    @classmethod
    def TestWithRunner(cls, runner):
        runner.run(cls)

class RandomTest(AbstractTest):
    @classmethod
    def setProvider(cls, provider):
        pass

class BugFix0(AbstractTest, Expector):
    @staticmethod
    def normalScript():
        return """
        device show readwrite dis dis
        device init
        
        
        sdh line rate 1-8 stm1
        serdes mode 1 stm64
        sdh line rate 1 stm64
        serdes powerup 1
        serdes enable 1
        sdh line mode 1 sonet
        sdh line loopback 1 release
        
        
        sdh line mode 25 sonet
        sdh line loopback 25 release
        
        
        sdh map aug64.1.1 4xaug16s
        sdh map aug16.1.1 4xaug4s
        sdh map aug4.1.1 4xaug1s
        sdh map aug1.1.1 3xvc3s
        sdh map vc3.1.1.1 c3
        
        
        sdh map aug16.25.1-25.1 4xaug4s
        sdh map aug4.25.1-25.1 4xaug1s
        sdh map aug1.25.1-25.1 3xvc3s
        sdh map vc3.25.1.1-25.1.1 7xtug2s
        sdh map tug2.25.1.1.1-25.1.1.7 tu11
        sdh map vc1x.25.1.1.1.1 de1
        sdh map vc1x.25.1.1.1.2-25.1.1.7.4 de1
        
        pdh de1 framing 25.1.1.1.1 ds1_esf
        pdh de1 timing 25.1.1.1.1 system 1
        pdh de1 nxds0create 25.1.1.1.1 0-23
        
        pdh de1 framing 25.1.1.1.2-25.1.1.7.4 ds1_esf
        pdh de1 timing 25.1.1.1.2-25.1.1.7.4 system 1
        pdh de1 nxds0create 25.1.1.1.2-25.1.1.7.4 0-23
        
        
        xc vc connect vc3.25.1.1-25.1.1 vc3.1.1.1 two-way
        
        
        
        
        sdh path tti transmit vc3.1.1.1 16bytes ATVN null_padding
        sdh path tti expect vc3.1.1.1 16bytes ATVN null_padding
        sdh path psl expect vc3.1.1.1 0x2
        sdh path psl transmit vc3.1.1.1 0x2
        
        
        pw create cesop 1-28 basic
        pw circuit bind 1 nxds0.25.1.1.1.1.0-23
        pw circuit bind 2 nxds0.25.1.1.1.2.0-23
        pw circuit bind 3 nxds0.25.1.1.1.3.0-23
        pw circuit bind 4 nxds0.25.1.1.1.4.0-23
        pw circuit bind 5 nxds0.25.1.1.2.1.0-23
        pw circuit bind 6 nxds0.25.1.1.2.2.0-23
        pw circuit bind 7 nxds0.25.1.1.2.3.0-23
        pw circuit bind 8 nxds0.25.1.1.2.4.0-23
        pw circuit bind 9 nxds0.25.1.1.3.1.0-23
        pw circuit bind 10 nxds0.25.1.1.3.2.0-23
        pw circuit bind 11 nxds0.25.1.1.3.3.0-23
        pw circuit bind 12 nxds0.25.1.1.3.4.0-23
        pw circuit bind 13 nxds0.25.1.1.4.1.0-23
        pw circuit bind 14 nxds0.25.1.1.4.2.0-23
        pw circuit bind 15 nxds0.25.1.1.4.3.0-23
        pw circuit bind 16 nxds0.25.1.1.4.4.0-23
        pw circuit bind 17 nxds0.25.1.1.5.1.0-23
        pw circuit bind 18 nxds0.25.1.1.5.2.0-23
        pw circuit bind 19 nxds0.25.1.1.5.3.0-23
        pw circuit bind 20 nxds0.25.1.1.5.4.0-23
        pw circuit bind 21 nxds0.25.1.1.6.1.0-23
        pw circuit bind 22 nxds0.25.1.1.6.2.0-23
        pw circuit bind 23 nxds0.25.1.1.6.3.0-23
        pw circuit bind 24 nxds0.25.1.1.6.4.0-23
        pw circuit bind 25 nxds0.25.1.1.7.1.0-23
        pw circuit bind 26 nxds0.25.1.1.7.2.0-23
        pw circuit bind 27 nxds0.25.1.1.7.3.0-23
        pw circuit bind 28 nxds0.25.1.1.7.4.0-23
        pw jitterbuffer 1-28 6000
        pw jitterdelay 1-28 3000
        pw payloadsize 1-28 8
        pw psn 1-28 mpls
        eth port enable 1
        eth port srcmac 1 22.44.22.44.22.44
        pw ethport 1-28 1
        pw ethheader 1-28 11.22.11.22.11.22 none none
        pw enable 1-28
        
        serdes loopback 25,26 local
        """

    def bertShouldCheckAddress(self, address):
        ignoredRegs = ["ctrl_pen_montdm", "ctrl_pen_gen"]
        for ignoredReg in ignoredRegs:
            reg = self.bertRegLookup(address, ignoredReg)
            if reg is not None:
                return True

        return False

    def shouldCheckAddress(self, address):
        if self.registerManager().isBertAddress(address):
            return self.bertShouldCheckAddress(address)

        return True

    def testConfigureMustBeRestoredAfterDeprovision(self):
        # Configuration that works
        registerCollector = RegisterCollector()
        registerCollector.expector = self
        registerCollector.start()
        self.assertCliSuccess(self.normalScript())
        registerCollector.stop()

        # Now not work
        script = """
        serdes loopback 1 local
        prbs engine create vc 1 vc1x.25.1.1.1.1
        prbs engine enable 1
        serdes loopback 1 release
        prbs engine delete 1
        """
        self.assertCliSuccess(script)
        self.assertTrue(registerCollector.audit(), "Configuration has not been restored properly")

    @classmethod
    def canRun(cls):
        return cls.isMroProduct()


class BugFix_DS3_Cbit_Unchannelized(AbstractTest, Expector):
    pass

def TestBugFixes(runner):
    runner.run(BugFix0)
    runner.run(BugFix_DS3_Cbit_Unchannelized)

def channelizedBertSupported():
    return False # TODO: not now, will come back later

def TestMain():
    if not channelizedBertSupported():
        return

    runner = AtTestCaseRunner.runner()
    ProvisioningTest.TestWithRunner(runner)
    SameConfigureTest.TestWithRunner(runner)
    RejectTest.TestWithRunner(runner)
    TrafficTest.TestWithRunner(runner)
    TestBugFixes(runner)
    runner.summary()

if __name__ == '__main__':
    TestMain()

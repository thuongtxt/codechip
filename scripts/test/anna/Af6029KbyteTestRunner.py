import random
from python.arrive.AtAppManager.AtAppManager import AtAppManager
from test.AtTestCase import AtTestCase, AtTestCaseRunner


class Af6029KbyteTestRunner(AtTestCase):
    @classmethod
    def setUpClass(cls):
        cls.runCli("device init")

    def testPwEthtype(self):
        for i in range(100):
            ethType = random.randint(0, 65535)
            self.assertCliSuccess("ciena pw kbyte ethtype 0x%x" % ethType)
            result = self.runCli("show ciena pw kbyte")
            cellContent = result.cellContent(0, "EthType")
            self.assertEqual(int(cellContent, 16), ethType)

    def testPwVersion(self):
        for i in range(100):
            verInt = random.randint(0, 65535)
            cli = "ciena pw kbyte version 0x%x" % verInt
            if verInt <= 255:
                self.assertCliSuccess(cli)
                result = self.runCli("show ciena pw kbyte")
                cellContent = result.cellContent(0, "Version")
                self.assertEqual(int(cellContent, 16), verInt)

            else:
                self.assertCliFailure(cli)

def TestMain():
    productCode = AtAppManager.localApp().productCode()
    if productCode == 0x60290011:
        return

    runner = AtTestCaseRunner.runner()
    runner.run(Af6029KbyteTestRunner)
    return runner.summary()
    
if __name__ == '__main__':
    TestMain()
    
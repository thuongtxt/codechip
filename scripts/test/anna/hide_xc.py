from test.AtTestCase import AtTestCaseRunner
from AnnaTestCase import AnnaMroTestCase
from python.arrive.AtAppManager.AtAppManager import AtAppManager

class LinesProvider(object):
    currentProvider = None
    
    @classmethod
    def is20G(cls):
        productCode = AtAppManager.localApp().productCode()
        return productCode == 0x60290022
    
    def lineRate(self):
        return "stm16"
    
    @staticmethod
    def terminatedLineStartId():
        return 24
    
    def terminatedLineStopId(self):
        return 31 if self.is20G() else 27
    
    def numTerminatedLines(self):
        return 8 if self.is20G() else 4
    
    def lineSideStartLineId(self):
        return 0
    
    def lineSideStopLineId(self):
        return 0
    
    def setupLineRate(self):
        return self.runCli("sdh line rate %d-%d %s" % (self.lineSideStartLineId() + 1, 
                                                       self.lineSideStopLineId() + 1,
                                                       self.lineRate()))
    
    @staticmethod
    def runCli(cli):
        from python.arrive.AtAppManager.AtAppManager import AtAppManager
        return AtAppManager.localApp().runCli(cli)
    
    def allLineSideIds(self):
        return range(self.lineSideStartLineId(), self.lineSideStopLineId() + 1)
    
    def allTerminatedLineIds(self):
        return range(self.terminatedLineStartId(), self.terminatedLineStopId() + 1)
    
    def allTerminatedLineIds_Stm64(self):
        return range(self.terminatedLineStartId(), self.terminatedLineStopId() + 1, 4)
    
    def hasAug64(self):
        return False
    
    def hasAug16(self):
        return True
    
    def hasAug4(self):
        return self.hasAug16()
    
    def hasAug1(self):
        return self.hasAug4()
    
    @staticmethod
    def _allVc3s(aug1IdPairs):
        vc3s = []
        
        for pair in aug1IdPairs:
            for au3Id in range(0, 3):
                vc3s.append("vc3.%d.%d.%d" % (pair[0] + 1, pair[1] + 1, au3Id + 1))
        
        return vc3s
    
    def allLineSideVc3s(self):
        return self._allVc3s(self._lineSideAug1IdPairs())
    
    def allTerminatedVc3s(self):
        return self._allVc3s(self._terminatedSideAug1IdPairs())
    
    def allLineSideTug2s(self):
        tug2s = []
        for pair in self._lineSideAug1IdPairs():
            for au3Id in range(0, 3):
                for tug2Id in range(0, 7):
                    tug2s.append("tug2.%d.%d.%d.%d" % (pair[0] + 1, pair[1] + 1, au3Id + 1, tug2Id + 1))
                
        
        return tug2s
    
    def allLineSideVc11s(self):
        vc11s = []
        
        for pair in self._lineSideAug1IdPairs():
            for au3Id in range(0, 3):
                for tug2Id in range(0, 7):
                    for tuId in range(0, 4):
                        vc11s.append("vc11.%d.%d.%d.%d.%d" % (pair[0] + 1, pair[1] + 1, au3Id + 1, tug2Id + 1, tuId + 1))
        
        return vc11s
    
    def allLineSideTug3s(self):
        tug3s = []
        
        for pair in self._lineSideAug1IdPairs():
            for tug3Id in range(0, 3):
                tug3s.append("tug3.%d.%d.%d" % (pair[0] + 1, pair[1] + 1, tug3Id + 1))
        
        return tug3s
    
    @staticmethod
    def _allVc4_16c(aug16IdPairs):
        channels = []
        
        for pair in aug16IdPairs:
            channels.append("vc4_16c.%d.%d" % (pair[0] + 1, pair[1] + 1))
            
        return channels
    
    @staticmethod
    def _allVc4_64c(aug64IdPairs):
        channels = []

        for pair in aug64IdPairs:
            channels.append("vc4_64c.%d.%d" % (pair[0] + 1, pair[1] + 1))

        return channels
    
    def allLineSideVc4_16c(self):
        pairs = self._lineSideAug16IdPairs()
        return self._allVc4_16c(pairs)
    
    def allTerminatedVc4_16c(self):
        pairs = self._makeAugIdPairs(self.allTerminatedLineIds(), range(1))
        return self._allVc4_16c(pairs)
    
    def allTerminatedVc4_64c(self):
        pairs = self._makeAugIdPairs(self.allTerminatedLineIds_Stm64(), range(1))
        return self._allVc4_64c(pairs)
    
    @staticmethod
    def _allVc4_4c(aug4IdPairs):
        channels = []
        
        for pair in aug4IdPairs:
            channels.append("vc4_4c.%d.%d" % (pair[0] + 1, pair[1] + 1))
        
        return channels
    
    def allLineSideVc4_4c(self):
        return self._allVc4_4c(self._lineSideAug4IdPairs())
    
    def allTerminatedVc4_4c(self):
        return self._allVc4_4c(self._terminatedSideAug4IdPairs())
    
    @staticmethod
    def _allVc4s(idPairs):
        vc4s = []

        for pair in idPairs:
            vc4s.append("vc4.%d.%d" % (pair[0] + 1, pair[1] + 1))
        
        return vc4s
    
    @staticmethod
    def _makeAugIdPairs(lineIds, aug1Ids):
        pairs = []
        for lineId in lineIds:
            for aug1Id in aug1Ids:
                pairs.append([lineId, aug1Id])
        return pairs
    
    def numAug64sInLines(self):
        rate = self.lineRate()
    
        if rate == "stm64": return 1
    
        return 0
    
    def numAug16sInLines(self):
        rate = self.lineRate()
        
        if rate == "stm64": return 4
        if rate == "stm16": return 1 
        
        return 0
    
    def numAug4sInLines(self):
        rate = self.lineRate()
        
        if rate == "stm64": return 16
        if rate == "stm16": return 4 
        if rate == "stm4" : return 1
        
        return 0
        
    def numAug1sInLines(self):
        if self.lineRate() == "stm1": return 1
        
        return self.numAug4sInLines() * 4
        
    def _lineSideAug16IdPairs(self):
        return self._makeAugIdPairs(self.allLineSideIds(), range(self.numAug16sInLines()))
        
    def _lineSideAug4IdPairs(self):
        return self._makeAugIdPairs(self.allLineSideIds(), range(self.numAug4sInLines()))
    
    def _lineSideAug1IdPairs(self):
        return self._makeAugIdPairs(self.allLineSideIds(), range(self.numAug1sInLines()))
    
    def _terminatedSideAug4IdPairs(self):
        return self._makeAugIdPairs(self.allTerminatedLineIds(), range(4))
    
    def _terminatedSideAug1IdPairs(self):
        return self._makeAugIdPairs(self.allTerminatedLineIds(), range(16))
            
    def allLineSideVc4s(self):
        return self._allVc4s(self._lineSideAug1IdPairs())
    
    def allTerminatedVc4s(self):
        return self._allVc4s(self._terminatedSideAug1IdPairs())
    
    @staticmethod
    def _cliLineSideStartId():
        return LinesProvider.currentProvider.lineSideStartLineId() + 1

    @staticmethod
    def _cliLineSideStopId():
        return LinesProvider.currentProvider.lineSideStopLineId() + 1
    
    def cliLineSideAug64Ids(self):
        lineAugs = []
        for line_i in self.allLineSideIds():
            lineAugs.append("%d.1-%d.%d" % (line_i + 1,
                                            line_i + 1,
                                            self.numAug64sInLines()))
        return "aug64.%s" % (",".join(lineAugs))

    def cliLineSideVc4_64cIds(self):
        lineVcs = []
        for line_i in self.allLineSideIds():
            lineVcs.append("%d.1-%d.%d" % (line_i + 1,
                                           line_i + 1,
                                           self.numAug64sInLines()))
        return "vc4_64c.%s" % (",".join(lineVcs))

    def cliLineSideAug16Ids(self):        
        lineAugs = []
        for line_i in self.allLineSideIds(): 
            lineAugs.append("%d.1-%d.%d" % (line_i + 1, 
                                            line_i + 1,
                                            self.numAug16sInLines()))
        return "aug16.%s" % (",".join(lineAugs))
    
    def cliLineSideVc4_16cIds(self):        
        lineVcs = []
        for line_i in self.allLineSideIds(): 
            lineVcs.append("%d.1-%d.%d" % (line_i + 1, 
                                           line_i + 1,
                                           self.numAug16sInLines()))                            
        return "vc4_16c.%s" % (",".join(lineVcs))
    
    def cliLineSideAug4Ids(self):        
        lineAugs = []
        for line_i in self.allLineSideIds(): 
            lineAugs.append("%d.1-%d.%d" % (line_i + 1, 
                                            line_i + 1,
                                            self.numAug4sInLines()))
        return "aug4.%s" % (",".join(lineAugs))
    
    def cliLineSideAug1Ids(self):
        lineAugs = []
        for line_i in self.allLineSideIds(): 
            lineAugs.append("%d.1-%d.%d" % (line_i + 1, 
                                            line_i + 1,
                                            self.numAug1sInLines()))
        return "aug1.%s" % (",".join(lineAugs))
    
    def cliLineSideVc4_4cIds(self):
        lineVcs = []
        for line_i in self.allLineSideIds(): 
            lineVcs.append("%d.1-%d.%d" % (line_i + 1, 
                                            line_i + 1,
                                            self.numAug4sInLines()))                            
        return "vc4_4c.%s" % (",".join(lineVcs))    
    
    def cliLineSideVc4Ids(self):
        lineVcs = []
        for line_i in self.allLineSideIds(): 
            lineVcs.append("%d.1-%d.%d" % (line_i + 1, 
                                            line_i + 1,
                                            self.numAug1sInLines()))                            
        return "vc4.%s" % (",".join(lineVcs))    
    
    def cliLineSideVc3Ids(self):
        lineVcs = []
        for line_i in self.allLineSideIds(): 
            lineVcs.append("%d.1.1-%d.%d.3" % (line_i + 1, 
                                            line_i + 1,
                                            self.numAug1sInLines()))                            
        return "vc3.%s" % (",".join(lineVcs))
    
    def cliLineSideTug2Ids(self):
        lineVcs = []
        for line_i in self.allLineSideIds(): 
            lineVcs.append("%d.1.1.1-%d.%d.3.7" % (line_i + 1, 
                                                   line_i + 1,
                                                   self.numAug1sInLines()))                            
        return "tug2.%s" % (",".join(lineVcs))
    
    def cliLineSideVc1xIds(self):
        lineVcs = []
        for line_i in self.allLineSideIds(): 
            lineVcs.append("%d.1.1.1.1-%d.%d.3.7.4" % (line_i + 1, 
                                                       line_i + 1,
                                                       self.numAug1sInLines()))                            
        return "vc1x.%s" % (",".join(lineVcs))    
    
    def cliLineSideTug3Ids(self):
        lineVcs = []
        for line_i in self.allLineSideIds(): 
            lineVcs.append("%d.1.1-%d.%d.3" % (line_i + 1, 
                                               line_i + 1,
                                               self.numAug1sInLines()))                            
        return "tug3.%s" % (",".join(lineVcs))    
    
    def cliTerminatedSideAug64Ids(self):
        return "aug64.25.1,29.1" if self.is20G() else "null"

    def cliTerminatedSideVc4_64cIds(self):
        return "vc4_64c.25.1,29.1" if self.is20G() else "null"

    def cliTerminatedSideAug16Ids(self):
        return "aug16.25.1-32.1" if self.is20G() else "aug16.25.1-28.1"    
    
    def cliTerminatedSideVc4_16cIds(self):        
        return "vc4_16c.25.1-32.1" if self.is20G() else "vc4_16c.25.1-28.1"
    
    def cliTerminatedSideAug4Ids(self):        
        return "aug4.25.1-32.4" if self.is20G() else "aug4.25.1-28.4"
    
    def cliTerminatedSideVc4_4cIds(self):        
        return "vc4_4c.25.1-32.4" if self.is20G() else "vc4_4c.25.1-28.4"
    
    def cliTerminatedSideAug1Ids(self):        
        return "aug1.25.1-32.16" if self.is20G() else "aug1.25.1-28.16"
    
    def cliTerminatedSideVc4Ids(self):        
        return "vc4.25.1-32.16" if self.is20G() else "vc4.25.1-28.16"         
    
    def cliTerminatedSideVc3Ids(self):        
        return "vc3.25.1.1-32.16.3" if self.is20G()  else"vc3.25.1.1-28.16.3"
    
    def numVc4_64c(self):
        return 2 if self.is20G() else 1
        
    def numVc4_16c(self):
        return 8 if self.is20G() else 4
    
    def numAug4s(self):
        return 32 if self.is20G() else 16
    
    def numAug1s(self):
        return self.numAug4s() * 4
    
    def numVc4_4c(self):
        return self.numAug4s()
    
    def numVc4s(self):
        return self.numAug1s()
    
    def numVc3s(self):
        return self.numAug1s() * 3
    
    def numTerminateVc3s(self):
        return len(self.allTerminatedLineIds())* 16 * 3
    
class MateLinesProvider(LinesProvider):    
    def lineSideStartLineId(self):
        return 16
    
    def lineSideStopLineId(self):
        return 23 if self.is20G() else 19
    
class FaceplateLinesProvider(LinesProvider):
    @classmethod
    def _numPorts(cls):
        return 16 if cls.is20G() else 8
    
    def lineSideStartLineId(self):
        return 0
    
    def lineSideStopLineId(self):
        return self._numPorts() - 1
        
class FaceplateLinesProvider_Stm64(LinesProvider):
    def lineSideStartLineId(self):
        return 0
    
    def lineSideStopLineId(self):
        return 15 if self.is20G() else 7
    
    def lineRate(self):
        return "stm64"
    
    def hasAug64(self):
        return True
    
    def numAug16sInLines(self):
        return 4
    
    def allLineSideIds(self):
        return range (0, self.lineSideStopLineId() + 1, 8)
    
    def setupLineRate(self):        
        lineId = ",".join("%d" % (_id + 1) for _id in range(0,self.lineSideStopLineId()+1, 8))        
        return self.runCli("sdh line rate %s %s" % (lineId, self.lineRate()))
    
class FaceplateLinesProvider_Stm16(FaceplateLinesProvider):
    def allLineSideIds(self):
        return range(0, self.lineSideStopLineId() + 1, 2)
    
    def setupLineRate(self):
        lineId = ",".join(["%d" %(i+1) for i in range(0,self.lineSideStopLineId()+1,2)])
        return self.runCli("sdh line rate %s %s" % (lineId, self.lineRate()))
   
class FaceplateLinesProvider_Stm4(FaceplateLinesProvider):
    def lineRate(self):
        return "stm4"
    
    def hasAug16(self):
        return False
    
    def hasAug4(self):
        return True
    
    def _lineSideAug1IdPairs(self):
        return self._makeAugIdPairs(self.allLineSideIds(), range(4))
    
    def _lineSideAug4IdPairs(self):
        return self._makeAugIdPairs(self.allLineSideIds(), range(1))
    
    def _terminatedSideAug4IdPairs(self):
        return self._makeAugIdPairs(self.allTerminatedLineIds(), [0, 2])
    
    def _terminatedSideAug1IdPairs(self):
        def makePairs(lineId, startAug1, stopAug1):
            pairs_ = []
            for i in range(startAug1, stopAug1 + 1):
                pairs_.append([lineId, i])
            return pairs_    
            
        pairs = []
        for line_i in range(self.numTerminatedLines()):
            terminatedLine = line_i + 24
            pairs.extend(makePairs(lineId = terminatedLine, startAug1 = 0, stopAug1 = 3))
            pairs.extend(makePairs(lineId = terminatedLine, startAug1 = 8, stopAug1 = 11))
        
        return pairs
    
    def cliLineSideAug16Ids(self):
        return None
    
    def cliLineSideVc4_16cIds(self):
        return None
    
    def cliTerminatedSideAug4Ids(self):
        lineAugs = []
        for lineId in range(self.terminatedLineStartId()+1, self.terminatedLineStopId()+2):   
            lineAugs.append("%d.1" % lineId)
            lineAugs.append("%d.3" % lineId)
        return "aug4.%s" % (",".join(lineAugs))
    
    def cliTerminatedSideVc4_4cIds(self):
        lineVcs = []
        for lineId in range(self.terminatedLineStartId()+1, self.terminatedLineStopId()+2):
            lineVcs.append("%d.1" % lineId)
            lineVcs.append("%d.3" % lineId)
        return "vc4_4c.%s" % (",".join(lineVcs))
    
    def cliTerminatedSideAug1Ids(self):
        lineAugs = []
        for lineId in range(self.terminatedLineStartId()+1, self.terminatedLineStopId()+2):
            lineAugs.append("%d.1-%d.4" % (lineId,lineId)) 
            lineAugs.append("%d.9-%d.12" % (lineId,lineId))
        return "aug1.%s" % (",".join(lineAugs))
    
    def cliTerminatedSideVc4Ids(self):
        lineVcs = []
        for lineId in range(self.terminatedLineStartId()+1, self.terminatedLineStopId()+2):
            lineVcs.append("%d.1-%d.4" % (lineId,lineId)) 
            lineVcs.append("%d.9-%d.12" % (lineId,lineId))
        return "vc4.%s" % (",".join(lineVcs))
    
    def cliTerminatedSideVc3Ids(self):
        lineVcs = []
        for lineId in range(self.terminatedLineStartId()+1, self.terminatedLineStopId()+2):
            lineVcs.append("%d.1.1-%d.4.3" % (lineId,lineId)) 
            lineVcs.append("%d.9.1-%d.12.3" % (lineId,lineId))
        return "vc3.%s" % (",".join(lineVcs))
    
    def numAug4s(self):
        return 16 if self.is20G() else 8
    
class FaceplateLinesProvider_Stm1(FaceplateLinesProvider):    
    def lineRate(self):
        return "stm1"
    
    def hasAug16(self):
        return False
    
    def hasAug4(self):
        return False
    
    def hasAug1(self):
        return True
    
    def cliTerminatedSideAug1Ids(self):
        lineAugs = []
        for lineId in range(self.terminatedLineStartId()+1, self.terminatedLineStopId()+2):   
            lineAugs.append("%d.1" % lineId)
            lineAugs.append("%d.9" % lineId)
        return "aug1.%s" % (",".join(lineAugs))
    
    def cliTerminatedSideVc4Ids(self):
        lineVcs = []
        for lineId in range(self.terminatedLineStartId()+1, self.terminatedLineStopId()+2):   
            lineVcs.append("%d.1" % lineId)
            lineVcs.append("%d.9" % lineId)
        return "vc4.%s" % (",".join(lineVcs))
    
    def cliTerminatedSideVc3Ids(self):
        lineVcs = []
        for lineId in range(self.terminatedLineStartId()+1, self.terminatedLineStopId()+2):
            lineVcs.append("%d.1.1-%d.1.3" % (lineId,lineId)) 
            lineVcs.append("%d.9.1-%d.9.3" % (lineId,lineId))
        return "vc3.%s" % (",".join(lineVcs))
    
    def numAug4s(self):
        return 0
    
    def numAug1s(self):
        return 16 if self.is20G() else 8
    
    def _terminatedSideAug1IdPairs(self):
        pairs = []
        
        for line_i in range(self.numTerminatedLines()):
            terminatedLine = line_i + 24
            pairs.append([terminatedLine, 0])
            pairs.append([terminatedLine, 8])
        
        return pairs
   
class FaceplateLinesProvider_Stm0(FaceplateLinesProvider):
    def lineRate(self):
        return "stm0"

    def hasAug16(self):
        return False

    def hasAug4(self):
        return False

    def hasAug1(self):
        return False

    def cliTerminatedSideAug1Ids(self):
        return "Invalid"

    def cliTerminatedSideVc4Ids(self):
        return "Invalid"

    def cliLineSideVc3Ids(self):
        return "vc3.%d-%d" % (self._cliLineSideStartId(), self._cliLineSideStopId())

    def _allTerminatedVc3Ids(self):
        lineVcs = []
        for lineId in range(self.terminatedLineStartId()+1, self.terminatedLineStopId()+2):
            lineVcs.append("%d.1.1" % lineId)
            lineVcs.append("%d.9.1" % lineId)
        return lineVcs

    def allTerminatedVc3s(self):
        return ["vc3.%s" % vc3CliId for vc3CliId in self._allTerminatedVc3Ids()]

    def cliTerminatedSideVc3Ids(self):
        return "vc3.%s" % (",".join(self._allTerminatedVc3Ids()))

    def numAug4s(self):
        return 0

    def numAug1s(self):
        return 0

    def numVc3s(self):
        return self._cliLineSideStopId() - self._cliLineSideStartId() + 1

    def numTerminateVc3s(self):
        return self._cliLineSideStopId() - self._cliLineSideStartId() + 1
    
    def _terminatedSideAug1IdPairs(self):
        return list()

    def cliLineSideTug2Ids(self):
        return "tug2.%d.1-%d.7" % (self._cliLineSideStartId(), self._cliLineSideStopId())

    def cliLineSideVc1xIds(self):
        return "vc1x.%d.1.1-%d.7.4" % (self._cliLineSideStartId(), self._cliLineSideStopId())

    def allLineSideTug2s(self):
        tug2s = []
        for lineId in self.allLineSideIds():
            for tug2Id in range(0, 7):
                tug2s.append("tug2.%d.%d" % (lineId + 1, tug2Id + 1))

        return tug2s

    def allLineSideVc11s(self):
        vc11s = []
        for lineId in self.allLineSideIds():
            for tug2Id in range(0, 7):
                for tuId in range(0, 4):
                    vc11s.append("vc11.%d.%d.%d" % (lineId + 1, tug2Id + 1, tuId + 1))

        return vc11s

    def allLineSideVc3s(self):
        return ["vc3.%d" % (lineId + 1) for lineId in self.allLineSideIds()]
       
class HideXcAbstract(AnnaMroTestCase):
    @classmethod
    def tearDownClass(cls):
        cls.classRunCli("debug ciena mro xc hide none")
        
    @classmethod
    def lineProvider(cls):
        """
        :rtype: LinesProvider
        """
        return LinesProvider.currentProvider
        
    def checkMapType(self, result, expectedNumRows, expectedMapType, expectedChannels=None):
        self.assertEqual(result.numRows(), expectedNumRows, None)
        for row_i in range(0, result.numRows()):
            if expectedChannels is not None:
                channelId = result.cellContent(row_i, "ID")
                self.assertEqual(channelId, expectedChannels[row_i], None)
                
            mapType = result.cellContent(row_i, "MappingType")
            self.assertEqual(mapType, expectedMapType, None)

    def vcsFromString(self, vcString):
        result = self.runCli("show sdh path %s" % vcString)
        vcs = []
        for row in range(0, result.numRows()):
            vc = result.cellContent(row, "PathId")
            vcs.append(vc)
        return vcs

    def checkXc(self, sourceVcs, numExpectedVcs, expectedDestVcs, twoWays=True):
        if type(expectedDestVcs) is str:
            expectedDestVcs = self.vcsFromString(expectedDestVcs)
        
        def check(direction):
            for row_i in range(0, result.numRows()):
                connectedChannel = result.cellContent(row_i, direction)
                self.assertEqual(connectedChannel, expectedDestVcs[row_i], None)
        
        result = self.runCli("show xc vc dest %s" % sourceVcs)
        self.assertEqual(result.numRows(), numExpectedVcs, None)
        check("Destinations")
        
        if twoWays:
            result = self.runCli("show xc vc source %s" % sourceVcs)
            self.assertEqual(result.numRows(), numExpectedVcs, None)
            check("Source")
        
    @staticmethod
    def terminatedLineIdFromLocal(localId):
        return localId + 25
    
    def allLineSideTug2s(self):
        return self.lineProvider().allLineSideTug2s()
    
    def allLineSideVc11s(self):
        return self.lineProvider().allLineSideVc11s()
    
    def allLineSideVc3s(self):
        return self.lineProvider().allLineSideVc3s()
    
    def allTerminatedVc3s(self):
        return self.lineProvider().allTerminatedVc3s()
    
    def allLineSideVc4s(self):
        return self.lineProvider().allLineSideVc4s()
    
    def allTerminatedVc4s(self):
        return self.lineProvider().allTerminatedVc4s()
    
    def allLineSideTug3s(self):
        return self.lineProvider().allLineSideTug3s()

    def allLineSideVc4_16c(self):
        return self.lineProvider().allLineSideVc4_16c()
    
    def allTerminatedVc4_64c(self):
        return self.lineProvider().allTerminatedVc4_64c()

    def allTerminatedVc4_16c(self):
        return self.lineProvider().allTerminatedVc4_16c()
    
    def allLineSideVc4_4c(self):
        return self.lineProvider().allLineSideVc4_4c()
    
    def allTerminatedVc4_4c(self):
        return self.lineProvider().allTerminatedVc4_4c()
    
    @classmethod
    def _cliLineSideStartId(cls):
        return LinesProvider.currentProvider.lineSideStartLineId() + 1
    
    @classmethod
    def _cliLineSideStopId(cls):
        return LinesProvider.currentProvider.lineSideStopLineId() + 1
    
    @classmethod
    def cliLineSideAug64Ids(cls):
        return cls.lineProvider().cliLineSideAug64Ids()

    @classmethod
    def cliLineSideAug16Ids(cls):
        return cls.lineProvider().cliLineSideAug16Ids()
    
    @classmethod
    def cliLineSideVc4_16cIds(cls):
        return cls.lineProvider().cliLineSideVc4_16cIds()
    
    @classmethod
    def cliLineSideVc4_64cIds(cls):
        return cls.lineProvider().cliLineSideVc4_64cIds()

    @classmethod
    def cliLineSideAug4Ids(cls):
        return cls.lineProvider().cliLineSideAug4Ids()
    
    @classmethod
    def cliLineSideAug1Ids(cls):
        return cls.lineProvider().cliLineSideAug1Ids()
    
    @classmethod
    def cliLineSideVc4_4cIds(cls):
        return cls.lineProvider().cliLineSideVc4_4cIds()
    
    @classmethod
    def cliLineSideVc4Ids(cls):
        return cls.lineProvider().cliLineSideVc4Ids()
    
    @classmethod
    def cliLineSideVc3Ids(cls):
        return cls.lineProvider().cliLineSideVc3Ids()
    
    @classmethod
    def cliLineSideTug2Ids(cls):
        return cls.lineProvider().cliLineSideTug2Ids()
    
    @classmethod
    def cliLineSideVc1xIds(cls):
        return cls.lineProvider().cliLineSideVc1xIds()
    
    @classmethod
    def cliLineSideTug3Ids(cls):
        return cls.lineProvider().cliLineSideTug3Ids()
        
    @classmethod
    def cliTerminatedSideAug64Ids(cls):
        return cls.lineProvider().cliTerminatedSideAug64Ids()

    @classmethod
    def cliTerminatedSideVc4_64cIds(cls):
        return cls.lineProvider().cliTerminatedSideVc4_64cIds()

    @classmethod
    def cliTerminatedSideAug16Ids(cls):
        return cls.lineProvider().cliTerminatedSideAug16Ids()
        
    @classmethod
    def cliTerminatedSideVc4_16cIds(cls):
        return cls.lineProvider().cliTerminatedSideVc4_16cIds()
    
    @classmethod
    def cliTerminatedSideAug4Ids(cls):
        return cls.lineProvider().cliTerminatedSideAug4Ids()
    
    @classmethod
    def cliTerminatedSideVc4_4cIds(cls):
        return cls.lineProvider().cliTerminatedSideVc4_4cIds()
    
    @classmethod
    def cliTerminatedSideAug1Ids(cls):
        return cls.lineProvider().cliTerminatedSideAug1Ids()
    
    @classmethod
    def cliTerminatedSideVc4Ids(cls):
        return cls.lineProvider().cliTerminatedSideVc4Ids()
    
    @classmethod
    def cliTerminatedSideVc3Ids(cls):
        return cls.lineProvider().cliTerminatedSideVc3Ids()
    
    @classmethod
    def allAug64MapVc4_64c(cls):
        if not cls.lineProvider().hasAug64():
            return

        cls.assertClassCliSuccess("sdh map %s vc4_64c" % cls.cliLineSideAug64Ids())

    @classmethod
    def allAug16MapAug4s(cls):
        if not cls.lineProvider().hasAug16():
            return
        
        cls.assertClassCliSuccess("sdh map %s 4xaug4s" % cls.cliLineSideAug16Ids())
    
    @classmethod
    def allAug16MapVc4_16c(cls):
        if not cls.lineProvider().hasAug16():
            return

        cls.assertClassCliSuccess("sdh map %s vc4_16c" % cls.cliLineSideAug16Ids())

    @classmethod
    def allAug4MapAug1s(cls):
        if not cls.lineProvider().hasAug4():
            return
        cls.assertClassCliSuccess("sdh map %s 4xaug1s" % cls.cliLineSideAug4Ids())
    
    @classmethod
    def allAug4MapVc4_4c(cls):
        if not cls.lineProvider().hasAug4():
            return
        cls.assertClassCliSuccess("sdh map %s vc4_4c" % cls.cliLineSideAug4Ids())

    @classmethod
    def allAug1sMapVc3s(cls):
        if not cls.lineProvider().hasAug1():
            return
        cls.assertClassCliSuccess("sdh map %s 3xvc3s" % cls.cliLineSideAug1Ids())
    
    @classmethod
    def allAug1sMapVc4(cls):
        cls.assertClassCliSuccess("sdh map %s vc4" % cls.cliLineSideAug1Ids())
    
    @classmethod
    def allVc4sMapC4(cls):
        cls.assertClassCliSuccess("sdh map %s c4" % cls.cliLineSideVc4Ids())

    @classmethod
    def allVc3sMapC3(cls):
        cls.assertClassCliSuccess("sdh map %s c3" % cls.cliLineSideVc3Ids())
    
    @classmethod
    def allVc3sMapTug2s(cls):
        cls.assertClassCliSuccess("sdh map %s 7xtug2s" % cls.cliLineSideVc3Ids())
    
    @classmethod
    def allVc4sMapTug3s(cls):
        cls.assertClassCliSuccess("sdh map %s 3xtug3s" % cls.cliLineSideVc4Ids())
    
    @classmethod
    def allTug3sMapTug2s(cls):
        cls.assertClassCliSuccess("sdh map %s 7xtug2s" % cls.cliLineSideTug3Ids())
    
    @classmethod
    def allTug3sMapVc3(cls):
        cls.assertClassCliSuccess("sdh map %s vc3" % cls.cliLineSideTug3Ids())
    
    @classmethod
    def allTug2sMapTu11(cls):
        cls.assertClassCliSuccess("sdh map %s tu11" % cls.cliLineSideTug2Ids())
    
    @classmethod
    def allVc1xMapC1x(cls):
        cls.assertClassCliSuccess("sdh map %s c1x" % cls.cliLineSideVc1xIds())
    
    @classmethod
    def allVc3sMapDe3(cls):
        cls.assertClassCliSuccess("sdh map %s de3" % cls.cliLineSideVc3Ids())
    
    @classmethod
    def allLineSideIds(cls):
        return LinesProvider.currentProvider.allLineSideIds()
    
    @classmethod
    def allTerminatedLineIds(cls):
        return LinesProvider.currentProvider.allTerminatedLineIds()
    
    @classmethod
    def numAug4s(cls):
        return LinesProvider.currentProvider.numAug4s()
    
    @classmethod
    def numAug1s(cls):
        return LinesProvider.currentProvider.numAug1s()

    @classmethod
    def numVc4_4c(cls):
        return LinesProvider.currentProvider.numVc4_4c()

    @classmethod
    def numVc4_16c(cls):
        return LinesProvider.currentProvider.numVc4_16c()

    @classmethod
    def numVc4_64c(cls):
        return LinesProvider.currentProvider.numVc4_64c()

    @classmethod
    def numVc4s(cls):
        return LinesProvider.currentProvider.numVc4s()

    @classmethod    
    def numVc3s(cls):
        return LinesProvider.currentProvider.numVc3s()
    
    @classmethod    
    def numTerminateVc3s(cls):
        return LinesProvider.currentProvider.numTerminateVc3s()
    
class HideXcDirectMappingCopy(HideXcAbstract):
    @classmethod
    def setUpClass(cls):
        cls.classRunCli("device init")
        cls.classRunCli("debug ciena mro xc hide direct")
        LinesProvider.currentProvider.setupLineRate()
        
    def testAug16MappingMustBeCopyToTerminatedLine(self):
        if not self.lineProvider().hasAug16():
            return
        
        mapTypes = ["vc4_16c", "4xaug4s"]
        numVc4_16c = self.lineProvider().numVc4_16c()
        for mapType in mapTypes:
            self.assertCliSuccess("sdh map %s %s" % (self.cliLineSideAug16Ids(), mapType))
            cliTerminatedSideAug16Ids = self.cliTerminatedSideAug16Ids()
            result = self.runCli("show sdh map %s" % cliTerminatedSideAug16Ids)
            self.checkMapType(result, numVc4_16c , mapType)
        
    def testVc4_16cMappingMustBeCopyToTerminatedLine(self):
        if not self.lineProvider().hasAug16():
            return
        
        numVc4_16c = self.lineProvider().numVc4_16c()       
        self.assertCliSuccess("sdh map %s vc4_16c" % self.cliLineSideAug16Ids())
        self.assertCliSuccess("sdh map %s c4_16c" % self.cliLineSideVc4_16cIds())
        cliTerminatedSideVc4_16cIds = self.cliTerminatedSideVc4_16cIds()
        result = self.runCli("show sdh map %s" % cliTerminatedSideVc4_16cIds)
        self.checkMapType(result, numVc4_16c, "c4_16c")

    def testAug4MappingMustBeCopyToTerminatedLine(self):
        if not self.lineProvider().hasAug4():
            return
        
        mapTypes = ["vc4_4c", "4xaug1s"]
        self.allAug16MapAug4s()
        for mapType in mapTypes:
            self.assertCliSuccess("sdh map %s %s" % (self.cliLineSideAug4Ids(), mapType))
            numAug4s = self.numAug4s()
            cliTerminatedSideAug4Ids = self.cliTerminatedSideAug4Ids()
            result = self.runCli("show sdh map %s" % cliTerminatedSideAug4Ids)
            self.checkMapType(result, numAug4s, mapType)
        
    def testVc4_4cMappingMustBeCopyToTerminatedLine(self):
        if not self.lineProvider().hasAug4():
            return
        
        self.allAug16MapAug4s()
        self.assertCliSuccess("sdh map %s vc4_4c" % self.cliLineSideAug4Ids())
        numAug4s = self.numAug4s()
        cliTerminatedSideVc4_4cIds = self.cliTerminatedSideVc4_4cIds()
        result = self.runCli("show sdh map %s" % cliTerminatedSideVc4_4cIds)
        self.checkMapType(result, numAug4s, "c4_4c")

    def testAug1MappingMustBeCopyToTerminatedLine(self):
        if not self.lineProvider().hasAug1():
            self.skipTest("No AUG1 in this datapath")

        mapTypes = ["vc4", "3xvc3s"]
        self.allAug16MapAug4s()
        self.allAug4MapAug1s()
        for mapType in mapTypes:
            self.assertCliSuccess("sdh map %s %s" % (self.cliLineSideAug1Ids(), mapType))
            numAug1s = self.numAug1s()
            cliTerminatedSideAug1Ids = self.cliTerminatedSideAug1Ids()
            result = self.runCli("show sdh map %s" % cliTerminatedSideAug1Ids)
            self.checkMapType(result, numAug1s, mapType)

    def testHoVc4MappingMustBeCopyToTerminatedLine(self):
        if not self.lineProvider().hasAug1():
            self.skipTest("No AUG1 in this datapath")

        self.allAug16MapAug4s()
        self.allAug4MapAug1s()
        self.allAug1sMapVc4()
        self.assertCliSuccess("sdh map %s c4" % self.cliLineSideVc4Ids())
        
        numAug1s = self.numAug1s()
        cliTerminatedSideVc4Ids = self.cliTerminatedSideVc4Ids()
        result = self.runCli("show sdh map %s" % cliTerminatedSideVc4Ids)
        self.checkMapType(result, numAug1s, "c4")
        
    def testHoVc3MappingMustBeCopyToTerminatedLine(self):
        self.allAug16MapAug4s()
        self.allAug4MapAug1s()
        self.allAug1sMapVc3s()
        self.allVc3sMapC3()
        
        numVc3s = self.numVc3s()
        cliTerminatedSideVc3Ids = self.cliTerminatedSideVc3Ids()
        result = self.runCli("show sdh map %s" % cliTerminatedSideVc3Ids)
        self.checkMapType(result, numVc3s, "c3")
    
class HideXc(HideXcAbstract):
    @staticmethod
    def lineId():
        return 17
    
    @classmethod
    def setUpClass(cls):
        cls.classRunCli("device init")
    
    def testXcHidingCanBeSpecified(self):
        modes = ["none", "direct", "mate", "faceplate"]
        for mode in modes:
            self.assertCliSuccess("debug ciena mro xc hide %s" % mode)
            self.assertCliSuccess("debug module xc")
        
class HideXcDirectXcDefaultConnection(HideXcAbstract):
    def setUp(self):
        self.classRunCli("device init")
        self.classRunCli("debug ciena mro xc hide direct")
        LinesProvider.currentProvider.setupLineRate()
    
    def runTestXcMustBeSetupWithVc3Mapping(self, mapType):
        self.allAug16MapAug4s()
        self.allAug4MapAug1s()
        self.allAug1sMapVc3s()
        self.assertCliSuccess("sdh map %s %s" % (self.cliLineSideVc3Ids(), mapType))
        
        # Mapping must be transfer automatically and correctly
        numVc3s = self.numVc3s()
        vc3s = self.cliLineSideVc3Ids()
        result = self.runCli("show sdh map %s" % vc3s)
        self.checkMapType(result, numVc3s, mapType)
        
        cliTerminatedSideVc3Ids = self.cliTerminatedSideVc3Ids()
        result = self.runCli("show sdh map %s" % cliTerminatedSideVc3Ids)
        self.checkMapType(result, numVc3s, mapType)
        
        self.checkXc(vc3s, numVc3s, self.allTerminatedVc3s())
    
    def testXcMustBeSetupWithVc3MapTug2s(self):
        self.runTestXcMustBeSetupWithVc3Mapping("7xtug2s")
    
    def testXcMustBeSetupWithVc3MapDe3(self):
        self.runTestXcMustBeSetupWithVc3Mapping("de3")
        
    def testXcMustBeSetupWithVc3MapC3(self):
        self.runTestXcMustBeSetupWithVc3Mapping("c3")

    def runTestXcMustBeSetupWithVc4Mapping(self, mapType, mapFunction):
        if not self.lineProvider().hasAug1():
            self.skipTest("AUG1 does not exist")

        self.allAug16MapAug4s()
        self.allAug4MapAug1s()
        self.allAug1sMapVc4()
        mapFunction()
        
        # Mapping must be transfer automatically and correctly
        numVc4s = self.numVc4s()
        cliLineSideVc4Ids = self.cliLineSideVc4Ids()
        result = self.runCli("show sdh map %s" % cliLineSideVc4Ids)
        self.checkMapType(result, numVc4s, mapType)
        cliTerminatedSideVc4Ids = self.cliTerminatedSideVc4Ids()
        result = self.runCli("show sdh map %s" % cliTerminatedSideVc4Ids)
        self.checkMapType(result, numVc4s, mapType)
        
        self.checkXc(self.cliLineSideVc4Ids(), numVc4s, self.allTerminatedVc4s())
    
    def testXcMustBeSetupWithVc4MapTug3s(self):
        self.runTestXcMustBeSetupWithVc4Mapping("3xtug3s", self.allVc4sMapTug3s)

    def testXcMustBeSetupWithVc4MapC4(self):
        self.runTestXcMustBeSetupWithVc4Mapping("c4", self.allVc4sMapC4)

    def runTestVcMapVc1x(self, mapFunction):
        mapFunction()
        
        # Check if we have mapping and the table is displayed correctly        
        numTug2s = self.numVc3s() * 7
        expectedChannels = self.allLineSideTug2s()
        cliLineSideTug2Ids = self.cliLineSideTug2Ids()
        result = self.runCli("show sdh map %s" % cliLineSideTug2Ids)
        self.checkMapType(result, numTug2s, "tu11", expectedChannels)
            
        self.assertCliSuccess("sdh map %s de1" % self.cliLineSideVc1xIds())        
        numVts = self.numVc3s() * 28
        expectedChannels = self.allLineSideVc11s()
        cliLineSideVc1xIds = self.cliLineSideVc1xIds()
        result = self.runCli("show sdh map %s" % cliLineSideVc1xIds)
        self.checkMapType(result, numVts, "de1", expectedChannels)
    
    def testVc4MapVc1x(self):
        if not self.lineProvider().hasAug1():
            self.skipTest("AUG-1 does not exist")

        def mapFunction():
            self.allAug16MapAug4s()
            self.allAug4MapAug1s()
            self.allAug1sMapVc4()
            self.allVc4sMapTug3s()
            self.allTug3sMapTug2s()
            self.allTug2sMapTu11()
            
        self.runTestVcMapVc1x(mapFunction)
    
    def testVc3MapVc1x(self):
        def mapFunction():
            self.allAug16MapAug4s()
            self.allAug4MapAug1s()
            self.allAug1sMapVc3s()
            self.allVc3sMapTug2s()
            self.allTug2sMapTu11()
            
        self.runTestVcMapVc1x(mapFunction)
        
    def testHoVc3MapDe3(self):
        self.allAug16MapAug4s()
        self.allAug4MapAug1s()
        self.allAug1sMapVc3s()
        self.allVc3sMapDe3()
        
        # Check if we have mapping and the table is displayed correctly
        expectedChannels = self.allLineSideVc3s()
        numVc3s = self.numVc3s()
        result = self.runCli("show sdh map %s" % self.cliLineSideVc3Ids())        
        self.checkMapType(result, numVc3s, "de3", expectedChannels)
        
    def testHoVc4MapDe3(self):
        if not self.lineProvider().hasAug1():
            self.skipTest("AUG-1 does not exist")

        self.allAug16MapAug4s()
        self.allAug4MapAug1s()
        self.allAug1sMapVc4()
        self.allVc4sMapTug3s()
        self.allTug3sMapVc3()
        self.allVc3sMapDe3()
        
        # Check if we have mapping and the table is displayed correctly
        numVc3s = self.numVc3s()
        expectedChannels = self.allLineSideTug3s()
        result = self.runCli("show sdh map %s" % self.cliLineSideTug3Ids())        
        self.checkMapType(result, numVc3s, "vc3", expectedChannels)
        
        expectedChannels = self.allLineSideVc3s()
        result = self.runCli("show sdh map %s" % self.cliLineSideVc3Ids())        
        self.checkMapType(result, numVc3s, "de3", expectedChannels)
        
    def testXcMustBeSetupWithVc4_4cMapC4_4c(self):
        if not self.lineProvider().hasAug4():
            self.skipTest("AUG4 does not exist")

        self.allAug16MapAug4s()
        self.allAug4MapVc4_4c()

        # Mapping must be transfer automatically and correctly
        numVc4_4cs = self.numVc4_4c()
        cliLineSideVc4_4cIds = self.cliLineSideVc4_4cIds()
        result = self.runCli("show sdh map %s" % cliLineSideVc4_4cIds)
        self.checkMapType(result, numVc4_4cs, "c4_4c")
        cliTerminatedSideVc4_4cIds = self.cliTerminatedSideVc4_4cIds()
        result = self.runCli("show sdh map %s" % cliTerminatedSideVc4_4cIds)
        self.checkMapType(result, numVc4_4cs, "c4_4c")

        self.checkXc(self.cliLineSideVc4_4cIds(), numVc4_4cs, self.allTerminatedVc4_4c())

    def testXcMustBeSetupWithVc4_16cMapC4_16c(self):
        if not self.lineProvider().hasAug16():
            self.skipTest("AUG16 does not exist")

        self.allAug16MapVc4_16c()

        # Mapping must be transfer automatically and correctly
        numVc4_16cs = self.numVc4_16c()
        cliLineSideVc4_16cIds = self.cliLineSideVc4_16cIds()
        result = self.runCli("show sdh map %s" % cliLineSideVc4_16cIds)
        self.checkMapType(result, numVc4_16cs, "c4_16c")
        cliTerminatedSideVc4_16cIds = self.cliTerminatedSideVc4_16cIds()
        result = self.runCli("show sdh map %s" % cliTerminatedSideVc4_16cIds)
        self.checkMapType(result, numVc4_16cs, "c4_16c")

        self.checkXc(self.cliLineSideVc4_16cIds(), numVc4_16cs, self.allTerminatedVc4_16c())

    def testXcMustBeSetupWithVc4_64cMapC4_64c(self):
        if not self.lineProvider().hasAug64():
            self.skipTest("AUG64 does not exist")

        if self.is10G():
            self.skipTest("VC4_64c does not support")

        self.allAug64MapVc4_64c()

        # Mapping must be transfer automatically and correctly
        numVc4_64cs = self.numVc4_64c()
        cliLineSideVc4_64cIds = self.cliLineSideVc4_64cIds()
        result = self.runCli("show sdh map %s" % cliLineSideVc4_64cIds)
        self.checkMapType(result, numVc4_64cs, "c4_64c")
        cliTerminatedSideVc4_64cIds = self.cliTerminatedSideVc4_64cIds()
        result = self.runCli("show sdh map %s" % cliTerminatedSideVc4_64cIds)
        self.checkMapType(result, numVc4_64cs, "c4_64c")

        self.checkXc(self.cliLineSideVc4_64cIds(), numVc4_64cs, self.allTerminatedVc4_64c())
        
class HideXcDirectPwBinding(HideXcAbstract):
    def setUp(self):
        self.runCli("device init")
        self.runCli("debug ciena mro xc hide direct")
        LinesProvider.currentProvider.setupLineRate()
        
    def checkCircuitPwBinding(self, result, expectedNumRows):
        self.assertEqual(result.numRows(), expectedNumRows, None)
        for row_i in range(0, result.numRows()):
            pw = result.cellContent(row_i, "Bound")
            expectedPw = "cep.%d" % (row_i + 1)
            self.assertEqual(pw, expectedPw, None)
            
    def checkPwCircuitBinding(self, result, expectedNumRows, expectCircuits):
        self.assertEqual(result.numRows(), expectedNumRows, None)
        for row_i in range(0, result.numRows()):
            circuit = result.cellContent(row_i, "circuit")
            self.assertEqual(circuit, expectCircuits[row_i], None)

    def testBindVc4_16c(self):
        if not self.lineProvider().hasAug64():
            return

        numVc4_64c = self.lineProvider().numVc4_64c()
        self.assertCliSuccess("sdh map %s vc4_16c" % self.cliLineSideAug64Ids())
        self.assertCliSuccess("sdh map %s c4_16c" % self.cliLineSideVc4_64cIds())
        self.assertCliSuccess("pw create cep 1-%d basic" % numVc4_64c)
        self.assertCliSuccess("pw psn 1-%d mpls" % numVc4_64c)
        self.assertCliSuccess("pw ethport 1-%d 1" % numVc4_64c)
        self.assertCliSuccess("pw circuit bind 1-%d %s" % (numVc4_64c, self.cliLineSideVc4_64cIds()))

        numVcs = numVc4_64c
        self.checkXc(self.cliLineSideVc4_64cIds(), numVcs, self.allTerminatedVc4_64c())

        # Check PW circuit binding
        expectCircuits = self.allLineSideVc4_64c()
        result = self.runCli("show pw 1-%d" % numVc4_64c)
        self.checkPwCircuitBinding(result, numVc4_64c, expectCircuits)

        result = self.runCli("show sdh path %s" % self.cliLineSideVc4_64cIds())
        self.checkCircuitPwBinding(result, numVc4_64c)

    def testBindVc4_16c(self):
        if not self.lineProvider().hasAug16():
            return
        
        numVc4_16c = self.lineProvider().numVc4_16c()
        self.assertCliSuccess("sdh map %s vc4_16c" % self.cliLineSideAug16Ids())
        self.assertCliSuccess("sdh map %s c4_16c" % self.cliLineSideVc4_16cIds())
        self.assertCliSuccess("pw create cep 1-%d basic" % numVc4_16c)
        self.assertCliSuccess("pw psn 1-%d mpls" % numVc4_16c)
        self.assertCliSuccess("pw ethport 1-%d 1" % numVc4_16c)
        self.assertCliSuccess("pw circuit bind 1-%d %s" % (numVc4_16c, self.cliLineSideVc4_16cIds()))
        
        numVcs = numVc4_16c
        self.checkXc(self.cliLineSideVc4_16cIds(), numVcs, self.allTerminatedVc4_16c())

        # Check PW circuit binding
        expectCircuits = self.allLineSideVc4_16c()        
        result = self.runCli("show pw 1-%d" % numVc4_16c)        
        self.checkPwCircuitBinding(result, numVc4_16c, expectCircuits)
                    
        result = self.runCli("show sdh path %s" % self.cliLineSideVc4_16cIds())
        self.checkCircuitPwBinding(result, numVc4_16c)
        
    def testBindVc4_4c(self):
        if not self.lineProvider().hasAug4():
            return
        
        pws = "1-%d" % self.numVc4_4c()
        self.allAug16MapAug4s()
        self.assertCliSuccess("sdh map %s vc4_4c" % self.cliLineSideAug4Ids())
        self.assertCliSuccess("pw create cep %s basic" % pws)
        self.assertCliSuccess("pw circuit bind %s %s" % (pws, self.cliLineSideVc4_4cIds()))
        self.assertCliSuccess("pw psn %s mpls" % pws)
        self.assertCliSuccess("pw ethport %s 1" % pws)
        
        self.checkXc(self.cliLineSideVc4_4cIds(), self.numVc4_4c(), self.allTerminatedVc4_4c())
        
        expectCircuits = self.allLineSideVc4_4c()
        numVc4_4c = self.numVc4_4c()
        result = self.runCli("show pw %s" % pws)        
        self.checkPwCircuitBinding(result, numVc4_4c, expectCircuits)
        
        cliLineSideVc4_4c = self.cliLineSideVc4_4cIds()
        numVc4_4c = self.numVc4_4c()
        result = self.runCli("show sdh path %s" % cliLineSideVc4_4c)
        self.checkCircuitPwBinding(result, numVc4_4c)
        
    def testBindVc4(self):
        if not self.lineProvider().hasAug1():
            self.skipTest("AUG-1 does not exist")

        self.allAug16MapAug4s()
        self.allAug4MapAug1s()
        self.allAug1sMapVc4()
        pws = "1-%d" % self.numVc4s()
        self.assertCliSuccess("pw create cep %s basic" % pws)
        self.assertCliSuccess("pw psn %s mpls" % pws)
        self.assertCliSuccess("pw ethport %s 1" % pws)
        self.assertCliSuccess("pw circuit bind %s %s" % (pws, self.cliLineSideVc4Ids()))
        
        self.checkXc(self.cliLineSideVc4Ids(), self.numVc4s(), self.allTerminatedVc4s())
        
        expectCircuits = self.allLineSideVc4s()
        numVc4s = self.numVc4s()
        result = self.runCli("show pw %s" % pws)        
        self.checkPwCircuitBinding(result, numVc4s, expectCircuits)
        
        cliLineSideVc4Ids = self.cliLineSideVc4Ids()
        numVc4s = self.numVc4s()
        result = self.runCli("show sdh path %s" % cliLineSideVc4Ids)
        self.checkCircuitPwBinding(result, numVc4s)
        
    def testBindAu3Vc3(self):
        numPws = self.numVc3s()
        pws = "1-%d" % numPws 
        self.allAug16MapAug4s()
        self.allAug4MapAug1s()
        self.allAug1sMapVc3s()
        self.assertCliSuccess("pw create cep %s basic" % pws)
        self.assertCliSuccess("pw circuit bind %s %s" % (pws, self.cliLineSideVc3Ids()))
        self.assertCliSuccess("pw psn %s mpls" % pws)
        self.assertCliSuccess("pw ethport %s 1" % pws)
        
        numVcs = self.numVc3s()
        print "I'm here %s, %d, %s" % (self.cliLineSideVc3Ids(), numVcs, self.allTerminatedVc3s())
        self.checkXc(self.cliLineSideVc3Ids(), numVcs, self.allTerminatedVc3s())
        
        expectCircuits = self.allLineSideVc3s()
        result = self.runCli("show pw %s" % pws)        
        self.checkPwCircuitBinding(result, numPws, expectCircuits)
            
        result = self.runCli("show sdh path %s" % self.cliLineSideVc3Ids())
        self.checkCircuitPwBinding(result, numPws)
        
    def runTestBindVc1x(self, mapFunction):
        mapFunction()
        
        numVts = self.numVc3s() * 28
        pws = "1-%d" % numVts
        vcs = self.cliLineSideVc1xIds()
        self.assertCliSuccess("pw create cep %s basic" % pws)
        self.assertCliSuccess("pw ethport %s 1" % pws)
        self.assertCliSuccess("pw psn %s mpls" % pws)
        self.assertCliSuccess("pw circuit bind %s %s" % (pws, vcs))
        self.assertCliSuccess("pw enable %s" % pws)
        
        expectCircuits = self.allLineSideVc11s()
        result = self.runCli("show pw %s" % pws)        
        self.checkPwCircuitBinding(result, numVts, expectCircuits)
            
        result = self.runCli("show sdh path %s" % vcs)
        self.checkCircuitPwBinding(result, numVts)
        
    def testBindAu3Vc1x(self):
        def mapFunction():
            self.allAug16MapAug4s()
            self.allAug4MapAug1s()
            self.allAug1sMapVc3s()
            self.allVc3sMapTug2s()
            self.allTug2sMapTu11()
            self.allVc1xMapC1x()
            
        self.runTestBindVc1x(mapFunction)
        
    def testBindAu4Vc1x(self):
        if not self.lineProvider().hasAug1():
            self.skipTest("AUG-1 does not exist")

        def mapFunction():
            self.allAug16MapAug4s()
            self.allAug4MapAug1s()
            self.allAug1sMapVc4()
            self.allVc4sMapTug3s()
            self.allTug3sMapTug2s()
            self.allTug2sMapTu11()
            self.allVc1xMapC1x()
            
        self.runTestBindVc1x(mapFunction)
        
class HideXcDirectDe3Path_Stm0(HideXcAbstract):
    @classmethod
    def setUpClass(cls):
        lineId = cls._cliLineSideStopId()
        cls.assertClassCliSuccess("device init")
        cls.assertClassCliSuccess("debug ciena mro xc hide direct")
        cls.assertClassCliSuccess("serdes mode 1-%d stm0" % lineId)
        cls.assertClassCliSuccess("sdh line rate 1-%d stm0" % lineId)
        cls.assertClassCliSuccess("sdh line mode 1-%d sonet" % lineId)        
        cls.assertClassCliSuccess("sdh map vc3.1-%d de3" % lineId)        
        
    def testCliDe3Framing(self):
        lineId = self._cliLineSideStopId()
        self.assertCliSuccess("pdh de3 framing 1-%d ds3_unframed" % lineId)
        self.assertClassCliSuccess("pdh de3 framing 1-%d ds3_cbit_28ds1"  % lineId)
        self.assertClassCliSuccess("pdh de1 framing 1.1.1-%d.7.4 ds1_esf" % lineId)
        self.assertClassCliSuccess("pdh de1 nxds0create 1.1.1-%d.7.4 0-23"    % lineId)
        
    def testCliDe3Timing(self):
        lineId = self._cliLineSideStopId()
        self.assertCliSuccess("pdh de3 timing 1-%d system none" % lineId)
        self.assertCliSuccess("pdh de3 timing 1-%d loop none" % lineId)
        
    def testCliDe3Counters(self):
        lineId = self._cliLineSideStopId()
        self.assertCliSuccess("show pdh de3 counters 1-%d r2c silent" % lineId)
        self.assertCliSuccess("show pdh de3 counters 1-%d ro" % lineId)
    
class HideXcDirectPathOverhead(HideXcAbstract):
    @classmethod
    def tearDownClass(cls):
        cls.assertClassCliSuccess("debug ciena mro poh side faceplate")
    
    @classmethod
    def cliDs1SatopLine(cls):
        return 0
    
    @classmethod
    def cliHoCepLine(cls):
        return 0
    
    @classmethod
    def cliLoCepLine(cls):
        return 0
    
    @classmethod
    def pohSideSetup(cls):
        pass
    
    @classmethod
    def setUpClass(cls):
        cls.assertClassCliSuccess("device init")
        cls.pohSideSetup()
        cls.assertClassCliSuccess("debug ciena mro xc hide direct")
        cls.allAug16MapAug4s()
        cls.allAug4MapAug1s()
        cls.allAug4MapAug1s()
        
        cliLineId = cls.cliDs1SatopLine()
        cls.assertClassCliSuccess("sdh map vc3.%d.1.1-%d.16.3 7xtug2s" % (cliLineId, cliLineId))
        cls.assertClassCliSuccess("sdh map tug2.%d.1.1.1-%d.16.3.7 tu11" % (cliLineId, cliLineId))
        cls.assertClassCliSuccess("sdh map vc1x.%d.1.1.1.1-%d.16.3.7.4 de1" % (cliLineId, cliLineId))
        cls.assertClassCliSuccess("pw create satop 1-1344")
        cls.assertClassCliSuccess("pw circuit bind 1-1344 de1.%d.1.1.1.1-%d.16.3.7.4" % (cliLineId, cliLineId))
        cls.assertClassCliSuccess("pw ethport 1-1344 1")
        cls.assertClassCliSuccess("pw psn 1-1344 mpls")
        cls.assertClassCliSuccess("pw enable 1-1344")
        
        cliLineId = cls.cliHoCepLine()
        cls.assertClassCliSuccess("sdh map vc3.%d.1.1-%d.16.3 c3" % (cliLineId, cliLineId))
        cls.assertClassCliSuccess("pw create cep 1345-1392 basic")
        cls.assertClassCliSuccess("pw circuit bind 1345-1392 vc3.%d.1.1-%d.16.3" % (cliLineId, cliLineId))
        cls.assertClassCliSuccess("pw ethport 1345-1392 1")
        cls.assertClassCliSuccess("pw psn 1345-1392 mpls")
        cls.assertClassCliSuccess("pw enable 1345-1392")
        
        cliLineId = cls.cliLoCepLine()
        cls.assertClassCliSuccess("sdh map vc3.%d.1.1-%d.16.3 7xtug2s" % (cliLineId, cliLineId))
        cls.assertClassCliSuccess("sdh map tug2.%d.1.1.1-%d.16.3.7 tu11" % (cliLineId, cliLineId))
        cls.assertClassCliSuccess("sdh map vc1x.%d.1.1.1.1-%d.16.3.7.4 c1x" % (cliLineId, cliLineId))
        cls.assertClassCliSuccess("pw create cep 1393-2736 basic")
        cls.assertClassCliSuccess("pw circuit bind 1393-2736 vc1x.%d.1.1.1.1-%d.16.3.7.4" % (cliLineId, cliLineId))
        cls.assertClassCliSuccess("pw ethport 1393-2736 1")
        cls.assertClassCliSuccess("pw psn 1393-2736 mpls")
        cls.assertClassCliSuccess("pw enable 1393-2736")
    
    @classmethod
    def is20G(cls):
        productCode = AtAppManager.localApp().productCode()
        return productCode == 0x60290022
                
    def numLines(self):
        return 8 if self.is20G() else 4
        
    def testHoOverheadInsertMustBeRedirected(self):
        numVc3s = self.numLines() * 48
        expectedVc3s = self.allLineSideVc3s()
        cliLineSideVc3Ids = self.cliLineSideVc3Ids()
        self.assertCliSuccess("sdh path psl transmit %s 0x03" % cliLineSideVc3Ids)
        result = self.runCli("show sdh path %s" % cliLineSideVc3Ids)
        self.assertEqual(numVc3s, result.numRows(), None)        
        for row_i in range(0, numVc3s):
            pathId = result.cellContent(row_i, "PathId")
            self.assertEqual(pathId, expectedVc3s[row_i], None)
            psl = result.cellContent(row_i, "txPsl")
            self.assertEqual(psl, "0x03", None)
    
    def testHoVcTimingMustBeRedirected(self):
        cliLineId = self.cliDs1SatopLine()
        self.assertCliSuccess("sdh path timing vc3.%d.1.1-%d.16.3 loop xxx" % (cliLineId, cliLineId))
        result = self.runCli("show sdh path vc3.%d.1.1-%d.16.3" % (cliLineId, cliLineId))
        for row_i in range(0, result.numRows()):
            timingMode = result.cellContent(row_i, "TimingMode")
            self.assertEqual(timingMode, "loop", None)
            
    def testHoVcAcrTimingMustBeRedirected(self):
        cliLineId = self.cliHoCepLine()
        self.assertCliSuccess("sdh path timing vc3.%d.1.1-%d.16.3 acr xxx" % (cliLineId, cliLineId))
        result = self.runCli("show sdh path vc3.%d.1.1-%d.16.3" % (cliLineId, cliLineId))
        
        # Build expected PW
        expectedPws = range(1345, 1392 + 1)
        for row_i in range(0, result.numRows()):
            timingMode = result.cellContent(row_i, "TimingMode")
            self.assertEqual(timingMode, "acr", None)
            
            pw = "cep.%d" % expectedPws[row_i]
            timingSource = result.cellContent(row_i, "TimingSource")
            self.assertEqual(pw, timingSource, None)

class HideXcDirectMatePathOverhead(HideXcDirectPathOverhead):
    @classmethod
    def pohSideSetup(cls):
        cls.assertClassCliSuccess("debug ciena mro poh side mate")
    
    @classmethod
    def cliDs1SatopLine(cls):
        return 17
    
    @classmethod
    def cliHoCepLine(cls):
        return 18
    
    @classmethod
    def cliLoCepLine(cls):
        return 19
    
class HideXcDirectFaceplatePathOverhead_Stm16(HideXcDirectPathOverhead):
    @classmethod
    def pohSideSetup(cls):
        cls.assertClassCliSuccess("debug ciena mro poh side faceplate")
        
    @classmethod
    def cliDs1SatopLine(cls):
        return 1
    
    @classmethod
    def cliHoCepLine(cls):
        return 3
    
    @classmethod
    def cliLoCepLine(cls):
        return 5
    
class HideXcMateLoopbackFaceplate(HideXcAbstract):
    def setUp(self):
        self.classRunCli("device init")
        
    def testFaceplateLineRateMustBeOc48ToEnterThisMode(self):
        self.assertCliSuccess("sdh line rate 1-8 stm1")
        self.assertCliFailure("debug ciena mro xc hide mate")
        self.assertCliSuccess("sdh line rate 1,3,5,7 stm16")
        self.assertCliSuccess("debug ciena mro xc hide mate")
        self.assertCliSuccess("logger flush silent")

class HideXcMateLoopbackFaceplateMappingCopy(HideXcAbstract):
    @classmethod
    def setUpClass(cls):
        cls.classRunCli("device init")
        cls.classRunCli("sdh line rate 1,3,5,7 stm16")
        cls.classRunCli("debug ciena mro xc hide mate")
    
    def testAug16MappingMustBeCopyToFaceplateLineAndTerminatedLine(self):
        mapTypes = ["vc4_16c", "4xaug4s"]
        for mapType in mapTypes:
            self.assertCliSuccess("sdh map aug16.17.1-18.1 %s" % mapType)
            result = self.runCli("show sdh map aug16.25.1-26.1")
            self.checkMapType(result, 2, mapType)
            result = self.runCli("show sdh map aug16.1.1,3.1,5.1,7.1")
            self.checkMapType(result, 4, mapType)
         
    def testVc4_16cMappingMustBeCopyToFaceplateLineAndTerminatedLine(self):       
        self.assertCliSuccess("sdh map aug16.17.1-18.1 vc4_16c")
        self.assertCliSuccess("sdh map vc4_16c.17.1-18.1 c4_16c")
        result = self.runCli("show sdh map vc4_16c.25.1-26.1")
        self.checkMapType(result, 2, "c4_16c")
        result = self.runCli("show sdh map vc4_16c.1.1,3.1,5.1,7.1")
        self.checkMapType(result, 4, "c4_16c")
        
    def testAug4MappingMustBeCopyToFaceplateLineAndTerminatedLine(self):
        mapTypes = ["vc4_4c", "4xaug1s"]
        self.assertCliSuccess("sdh map aug16.17.1-18.1 4xaug4s")
        for mapType in mapTypes:
            self.assertCliSuccess("sdh map aug4.17.1-18.4 %s" % mapType)
            result = self.runCli("show sdh map aug4.25.1-26.4")
            self.checkMapType(result, 8, mapType)
            result = self.runCli("show sdh map aug4.1.1-1.4,3.1-3.4,5.1-5.4,7.1-7.4")
            self.checkMapType(result, 16, mapType)
        
    def testVc4_4cMappingMustBeCopyToFaceplateLineAndTerminatedLine(self):
        self.assertCliSuccess("sdh map aug16.17.1-18.1 4xaug4s")
        self.assertCliSuccess("sdh map aug4.17.1-18.4 vc4_4c")
        result = self.runCli("show sdh map vc4_4c.25.1-26.4")
        self.checkMapType(result, 8, "c4_4c")
        result = self.runCli("show sdh map vc4_4c.1.1-1.4,3.1-3.4,5.1-5.4,7.1-7.4")
        self.checkMapType(result, 16, "c4_4c")
        
    def testAug1MappingMustBeCopyToFaceplateLineAndTerminatedLine(self):
        mapTypes = ["vc4", "3xvc3s"]
        self.assertCliSuccess("sdh map aug16.17.1-18.1 4xaug4s")
        self.assertCliSuccess("sdh map aug4.17.1-18.4 4xaug1s")
        for mapType in mapTypes:
            self.assertCliSuccess("sdh map aug1.17.1-18.16 %s" % mapType)
            result = self.runCli("show sdh map aug1.25.1-26.16")
            self.checkMapType(result, 32, mapType)
            result = self.runCli("show sdh map aug1.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16")
            self.checkMapType(result, 64, mapType)
            
    def testHoVc4MappingMustBeCopyToFaceplateLineAndTerminatedLine(self):
        self.assertCliSuccess("sdh map aug16.17.1-18.1 4xaug4s")
        self.assertCliSuccess("sdh map aug4.17.1-18.4 4xaug1s")
        self.assertCliSuccess("sdh map aug1.17.1-18.16 vc4")
        self.assertCliSuccess("sdh map vc4.17.1-18.16 c4")
        result = self.runCli("show sdh map vc4.25.1-26.16")
        self.checkMapType(result, 32, "c4")
        result = self.runCli("show sdh map vc4.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16")
        self.checkMapType(result, 64, "c4")
        
    def testHoVc3MappingMustBeCopyToFaceplateLineAndTerminatedLine(self):
        self.assertCliSuccess("sdh map aug16.17.1-18.1 4xaug4s")
        self.assertCliSuccess("sdh map aug4.17.1-18.4 4xaug1s")
        self.assertCliSuccess("sdh map aug1.17.1-18.16 3xvc3s")
        self.assertCliSuccess("sdh map vc3.17.1.1-18.16.3 c3")
        result = self.runCli("show sdh map vc3.25.1.1-26.16.3")
        self.checkMapType(result, 48 * 2, "c3")
        result = self.runCli("show sdh map vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3")
        self.checkMapType(result, 48 * 2 * 2, "c3")

class HideXcMateLoopbackFaceplateDefaultConnection(HideXcAbstract):
    def setUp(self):
        self.assertCliSuccess("device init")
        self.assertCliSuccess("sdh line rate 1,3,5,7 stm16")
        self.assertCliSuccess("debug ciena mro xc hide mate")
    
    def terminatedVc3s(self):
        return self.allTerminatedVc3s()
    
    def runTestXcMustBeSetupWithVc3LoMapping(self, loMapType):
        self.assertCliSuccess("sdh map aug16.17.1-18.1 4xaug4s")
        self.assertCliSuccess("sdh map aug4.17.1-18.4 4xaug1s")
        self.assertCliSuccess("sdh map aug1.17.1-18.16 3xvc3s")
        self.assertCliSuccess("sdh map vc3.17.1.1-18.16.3 %s" % loMapType)
        
        # Mapping must be transfer automatically and correctly
        numVc3s = 48 * 2
        result = self.runCli("show sdh map vc3.17.1.1-18.16.3")
        self.checkMapType(result, numVc3s, loMapType)
        result = self.runCli("show sdh map vc3.1.1.1-1.16.3,3.1.1-3.16.3,5.1.1-5.16.3,7.1.1-7.16.3")
        self.checkMapType(result, numVc3s * 2, "c3")
        result = self.runCli("show sdh map vc3.25.1.1-26.16.3")
        self.checkMapType(result, numVc3s, loMapType)
        
        # Drop connections
        self.checkXc("vc3.17.1.1-18.16.3", numVc3s, "vc3.3.1.1-3.16.3,7.1.1-7.16.3", twoWays=False)
        self.checkXc("vc3.3.1.1-3.16.3,7.1.1-7.16.3", numVc3s, "vc3.25.1.1-26.16.3", twoWays=False)
        
        # Add conections
        self.checkXc("vc3.1.1.1-1.16.3,5.1.1-5.16.3", numVc3s, "vc3.17.1.1-18.16.3", twoWays=False)
        self.checkXc("vc3.25.1.1-26.16.3", numVc3s, "vc3.1.1.1-1.16.3,5.1.1-5.16.3", twoWays=False)
    
    def testXcMustBeSetupWithVc3Tug2Mapping(self):
        self.runTestXcMustBeSetupWithVc3LoMapping("7xtug2s")
    
    def testXcMustBeSetupWithVc3De3Mapping(self):
        self.runTestXcMustBeSetupWithVc3LoMapping("de3")
        
    def testXcMustBeSetupWithVc4MapTug3s(self):
        self.assertCliSuccess("sdh map aug16.17.1-18.1 4xaug4s")
        self.assertCliSuccess("sdh map aug4.17.1-18.4 4xaug1s")
        self.assertCliSuccess("sdh map aug1.17.1-18.16 vc4")
        self.assertCliSuccess("sdh map vc4.17.1-18.16 3xtug3s")
        
        # Mapping must be transfer automatically and correctly
        numVc4s = 2 * 16
        result = self.runCli("show sdh map vc4.17.1-18.16")
        self.checkMapType(result, numVc4s, "3xtug3s")
        result = self.runCli("show sdh map vc4.1.1-1.16,3.1-3.16,5.1-5.16,7.1-7.16")
        self.checkMapType(result, numVc4s * 2, "c4")
        result = self.runCli("show sdh map vc4.25.1-26.16")
        self.checkMapType(result, numVc4s, "3xtug3s")
        
        # Drop connections
        self.checkXc("vc4.17.1-18.16", numVc4s, "vc4.3.1-3.16,7.1-7.16", twoWays=False)
        self.checkXc("vc4.3.1-3.16,7.1-7.16", numVc4s, "vc4.25.1-26.16", twoWays=False)
        
        # Add conections
        self.checkXc("vc4.1.1-1.16,5.1-5.16", numVc4s, "vc4.17.1-18.16", twoWays=False)
        self.checkXc("vc4.25.1-26.16", numVc4s, "vc4.1.1-1.16,5.1-5.16", twoWays=False)
    
    def runTestVcMapVc1x(self, mapFunction):
        mapFunction()
        
        # Check if we have mapping and the table is displayed correctly        
        numTug2s = 2 * 48 * 7
        expectedChannels = self.allLineSideTug2s()
        result = self.runCli("show sdh map tug2.17.1.1.1-18.16.3.7")
        self.checkMapType(result, numTug2s, "tu11", expectedChannels)
        
        self.assertCliSuccess("sdh map vc1x.17.1.1.1.1-18.16.3.7.4 de1")        
        numVts = 2 * 48 * 28
        expectedChannels = self.allLineSideVc11s()
        result = self.runCli("show sdh map vc1x.17.1.1.1.1-18.16.3.7.4")
        self.checkMapType(result, numVts, "de1", expectedChannels)
    
    def testVc4MapVc1x(self):
        def mapFunction():
            self.assertCliSuccess("sdh map aug16.17.1-18.1 4xaug4s")
            self.assertCliSuccess("sdh map aug4.17.1-18.4 4xaug1s")
            self.assertCliSuccess("sdh map aug1.17.1-18.16 vc4")
            self.assertCliSuccess("sdh map vc4.17.1-18.16 3xtug3s")
            self.assertCliSuccess("sdh map tug3.17.1.1-18.16.3 7xtug2s")
            self.assertCliSuccess("sdh map tug2.17.1.1.1-18.16.3.7 tu11")
            
        self.runTestVcMapVc1x(mapFunction)
    
    def testVc3MapVc1x(self):
        def mapFunction():
            self.assertCliSuccess("sdh map aug16.17.1-18.1 4xaug4s")
            self.assertCliSuccess("sdh map aug4.17.1-18.4 4xaug1s")
            self.assertCliSuccess("sdh map aug1.17.1-18.16 3xvc3s")
            self.assertCliSuccess("sdh map vc3.17.1.1-18.16.3 7xtug2s")
            self.assertCliSuccess("sdh map tug2.17.1.1.1-18.16.3.7 tu11")
            
        self.runTestVcMapVc1x(mapFunction)
        
    def testHoVc3MapDe3(self):
        self.assertCliSuccess("sdh map aug16.17.1-18.1 4xaug4s")
        self.assertCliSuccess("sdh map aug4.17.1-18.4 4xaug1s")
        self.assertCliSuccess("sdh map aug1.17.1-18.16 3xvc3s")
        self.assertCliSuccess("sdh map vc3.17.1.1-18.16.3 de3")
        
        # Check if we have mapping and the table is displayed correctly        
        numVc3s = 2 * 48
        expectedChannels = self.allLineSideVc3s()
        result = self.runCli("show sdh map vc3.17.1.1-18.16.3")
        self.checkMapType(result, numVc3s, "de3", expectedChannels)
        
    def testHoVc4MapDe3(self):
        self.assertCliSuccess("sdh map aug16.17.1-18.1 4xaug4s")
        self.assertCliSuccess("sdh map aug4.17.1-18.4 4xaug1s")
        self.assertCliSuccess("sdh map aug1.17.1-18.16 vc4")
        self.assertCliSuccess("sdh map vc4.17.1-18.16 3xtug3s")
        self.assertCliSuccess("sdh map tug3.17.1.1-18.16.3 vc3")
        self.assertCliSuccess("sdh map vc3.17.1.1-18.16.3 de3")
        
        # Check if we have mapping and the table is displayed correctly        
        numVc3s = 2 * 48
        expectedChannels = self.allLineSideTug3s()
        result = self.runCli("show sdh map tug3.17.1.1-18.16.3")
        self.checkMapType(result, numVc3s, "vc3", expectedChannels)
        
        expectedChannels = self.allLineSideVc3s()
        result = self.runCli("show sdh map vc3.17.1.1-18.16.3")
        self.checkMapType(result, numVc3s, "de3", expectedChannels)

class HideXcMateLoopbackFaceplatePwBinding(HideXcAbstract):
    def setUp(self):
        self.assertCliSuccess("device init")
        self.assertCliSuccess("sdh line rate 1,3,5,7 stm16")
        self.assertCliSuccess("debug ciena mro xc hide mate")
        
    def checkCircuitPwBinding(self, result, expectedNumRows):
        self.assertEqual(result.numRows(), expectedNumRows, None)
        for row_i in range(0, result.numRows()):
            pw = result.cellContent(row_i, "Bound")
            expectedPw = "cep.%d" % (row_i + 1)
            self.assertEqual(pw, expectedPw, None)
            
    def checkPwCircuitBinding(self, result, expectedNumRows, expectCircuits):
        self.assertEqual(result.numRows(), expectedNumRows, None)
        for row_i in range(0, result.numRows()):
            circuit = result.cellContent(row_i, "circuit")
            self.assertEqual(circuit, expectCircuits[row_i], None)
        
    def testBindVc4_16c(self):
        self.assertCliSuccess("sdh map aug16.17.1-18.1 vc4_16c")
        self.assertCliSuccess("sdh map vc4_16c.17.1-18.1 c4_16c")
        self.assertCliSuccess("pw create cep 1-2 basic")
        self.assertCliSuccess("pw psn 1-2 mpls")
        self.assertCliSuccess("pw ethport 1-2 1")
        self.assertCliSuccess("pw circuit bind 1-2 vc4_16c.17.1-18.1")
        
        numVcs = 2
        
        # Drop connections
        self.checkXc("vc4_16c.17.1-18.1", numVcs, "vc4_16c.3.1,7.1", twoWays=False)
        self.checkXc("vc4_16c.3.1,7.1", numVcs, "vc4_16c.25.1-26.1", twoWays=False)
        
        # Add conections
        self.checkXc("vc4_16c.1.1,5.1", numVcs, "vc4_16c.17.1,18.1", twoWays=False)
        self.checkXc("vc4_16c.25.1-26.1", numVcs, "vc4_16c.1.1,5.1", twoWays=False)

        # Check PW circuit binding
        expectCircuits = self.allLineSideVc4_16c()        
        result = self.runCli("show pw 1-2")        
        self.checkPwCircuitBinding(result, 2, expectCircuits)
                    
        result = self.runCli("show sdh path vc4_16c.17.1-18.1")
        self.checkCircuitPwBinding(result, 2)
        
    def testBindVc4_4c(self):
        self.assertCliSuccess("sdh map aug16.17.1-18.1 4xaug4s")
        self.assertCliSuccess("sdh map aug4.17.1-18.4 vc4_4c")
        self.assertCliSuccess("pw create cep 1-8 basic")
        self.assertCliSuccess("pw circuit bind 1-8 vc4_4c.17.1-18.4")
        self.assertCliSuccess("pw psn 1-8 mpls")
        self.assertCliSuccess("pw ethport 1-8 1")
        
        numVcs = 8
        
        # Drop connections
        self.checkXc("vc4_4c.17.1-18.4", numVcs, "vc4_4c.3.1-3.4,7.1-7.4", twoWays=False)
        self.checkXc("vc4_4c.3.1-3.4,7.1-7.4", numVcs, "vc4_4c.25.1-26.4", twoWays=False)
        
        # Add conections
        self.checkXc("vc4_4c.1.1-1.4,5.1-5.4", numVcs, "vc4_4c.17.1-18.4", twoWays=False)
        self.checkXc("vc4_4c.25.1-26.4", numVcs, "vc4_4c.1.1-1.4,5.1-5.4", twoWays=False)
        
        expectCircuits = self.allLineSideVc4_4c()
        result = self.runCli("show pw 1-8")        
        self.checkPwCircuitBinding(result, 8, expectCircuits)
        
        result = self.runCli("show sdh path vc4_4c.17.1-18.4")
        self.checkCircuitPwBinding(result, 8)
        
    def testBindVc4(self):
        self.assertCliSuccess("sdh map aug16.17.1-18.1 4xaug4s")
        self.assertCliSuccess("sdh map aug4.17.1-18.4 4xaug1s")
        self.assertCliSuccess("sdh map aug1.17.1-18.16 vc4")
        self.assertCliSuccess("pw create cep 1-32 basic")
        self.assertCliSuccess("pw psn 1-32 mpls")
        self.assertCliSuccess("pw ethport 1-32 1")
        self.assertCliSuccess("pw circuit bind 1-32 vc4.17.1-18.16")
        
        numVcs = 2 * 16
        
        # Drop connections
        self.checkXc("vc4.17.1-18.16", numVcs, "vc4.3.1-3.16,7.1-7.16", twoWays=False)
        self.checkXc("vc4.3.1-3.16,7.1-7.16", numVcs, "vc4.25.1-26.16", twoWays=False)
        
        # Add conections
        self.checkXc("vc4.1.1-1.16,5.1-5.16", numVcs, "vc4.17.1-18.16", twoWays=False)
        self.checkXc("vc4.25.1-26.16", numVcs, "vc4.1.1-1.16,5.1-5.16", twoWays=False)
        
        expectCircuits = self.allLineSideVc4s()
        result = self.runCli("show pw 1-32")        
        self.checkPwCircuitBinding(result, 32, expectCircuits)
        
        result = self.runCli("show sdh path vc4.17.1-18.16")
        self.checkCircuitPwBinding(result, 32)
        
    def testBindAu3Vc3(self):
        numPws = (2 * 48)
        pws = "1-%d" % numPws 
        self.assertCliSuccess("sdh map aug16.17.1-18.1 4xaug4s")
        self.assertCliSuccess("sdh map aug4.17.1-18.4 4xaug1s")
        self.assertCliSuccess("sdh map aug1.17.1-18.16 3xvc3s")
        self.assertCliSuccess("pw create cep %s basic" % pws)
        self.assertCliSuccess("pw circuit bind %s vc3.17.1.1-18.16.3" % pws)
        self.assertCliSuccess("pw psn %s mpls" % pws)
        self.assertCliSuccess("pw ethport %s 1" % pws)
        
        numVcs = 2 * 48
        
        # Drop connections
        self.checkXc("vc3.17.1.1-18.16.3", numVcs, "vc3.3.1.1-3.16.3,7.1.1-7.16.3", twoWays=False)
        self.checkXc("vc3.3.1.1-3.16.3,7.1.1-7.16.3", numVcs, "vc3.25.1.1-26.16.3", twoWays=False)
        
        # Add conections
        self.checkXc("vc3.1.1.1-1.16.3,5.1.1-5.16.3", numVcs, "vc3.17.1.1-18.16.3", twoWays=False)
        self.checkXc("vc3.25.1.1-26.16.3", numVcs, "vc3.1.1.1-1.16.3,5.1.1-5.16.3", twoWays=False)
        
        expectCircuits = self.allLineSideVc3s()
        result = self.runCli("show pw %s" % pws)        
        self.checkPwCircuitBinding(result, numPws, expectCircuits)
            
        result = self.runCli("show sdh path vc3.17.1.1-18.16.3")
        self.checkCircuitPwBinding(result, numPws)
        
    def runTestBindVc1x(self, mapFunction):
        mapFunction()
        
        numVts = 2 * 48 * 28
        pws = "1-%d" % numVts
        vcs = "vc1x.17.1.1.1.1-18.16.3.7.4"
        self.assertCliSuccess("pw create cep %s basic" % pws)
        self.assertCliSuccess("pw ethport %s 1" % pws)
        self.assertCliSuccess("pw psn %s mpls" % pws)
        self.assertCliSuccess("pw circuit bind %s %s" % (pws, vcs))
        self.assertCliSuccess("pw enable %s" % pws)
        
        expectCircuits = self.allLineSideVc11s()
        result = self.runCli("show pw %s" % pws)        
        self.checkPwCircuitBinding(result, numVts, expectCircuits)
            
        result = self.runCli("show sdh path vc1x.17.1.1.1.1-18.16.3.7.4")
        self.checkCircuitPwBinding(result, numVts)
        
    def testBindAu3Vc1x(self):
        def mapFunction():
            self.assertCliSuccess("sdh map aug16.17.1-18.1 4xaug4s")
            self.assertCliSuccess("sdh map aug4.17.1-18.4 4xaug1s")
            self.assertCliSuccess("sdh map aug1.17.1-18.16 3xvc3s")
            self.assertCliSuccess("sdh map vc3.17.1.1-18.16.3 7xtug2s")
            self.assertCliSuccess("sdh map tug2.17.1.1.1-18.16.3.7 tu11")
            self.assertCliSuccess("sdh map vc1x.17.1.1.1.1-18.16.3.7.4 c1x")
            
        self.runTestBindVc1x(mapFunction)
        
    def testBindAu4Vc1x(self):
        def mapFunction():
            self.assertCliSuccess("sdh map aug16.17.1-18.1 4xaug4s")
            self.assertCliSuccess("sdh map aug4.17.1-18.4 4xaug1s")
            self.assertCliSuccess("sdh map aug1.17.1-18.16 vc4")
            self.assertCliSuccess("sdh map vc4.17.1-18.16 3xtug3s")
            self.assertCliSuccess("sdh map tug3.17.1.1-18.16.3 7xtug2s")
            self.assertCliSuccess("sdh map tug2.17.1.1.1-18.16.3.7 tu11")
            self.assertCliSuccess("sdh map vc1x.17.1.1.1.1-18.16.3.7.4 c1x")
            
        self.runTestBindVc1x(mapFunction)

class HideXcMateLoopbackFaceplatePathOverhead(HideXcAbstract):
    @classmethod
    def setUpClass(cls):
        cls.assertClassCliSuccess("device init")
        cls.assertClassCliSuccess("debug ciena mro poh side faceplate")
        cls.assertClassCliSuccess("debug ciena mro xc hide mate")
        
        cls.assertClassCliSuccess("sdh map aug16.17.1-18.1 4xaug4s")
        cls.assertClassCliSuccess("sdh map aug4.17.1-18.4 4xaug1s")
        cls.assertClassCliSuccess("sdh map aug1.17.1-18.16 3xvc3s")
        
        cls.assertClassCliSuccess("sdh map vc3.17.1.1-17.16.3 7xtug2s")
        cls.assertClassCliSuccess("sdh map tug2.17.1.1.1-17.16.3.7 tu11")
        
        # DS1
        cls.assertClassCliSuccess("sdh map vc1x.17.1.1.1.1-17.8.3.7.4 de1")
        cls.assertClassCliSuccess("pw create satop 1-672")
        cls.assertClassCliSuccess("pw circuit bind 1-672 de1.17.1.1.1.1-17.8.3.7.4")
        cls.assertClassCliSuccess("pw ethport 1-672 1")
        cls.assertClassCliSuccess("pw psn 1-672 mpls")
        cls.assertClassCliSuccess("pw enable 1-672")
        
        # VC1x
        cls.assertClassCliSuccess("sdh map vc1x.17.9.1.1.1-17.16.3.7.4 c1x")
        cls.assertClassCliSuccess("pw create cep 673-1344 basic")
        cls.assertClassCliSuccess("pw circuit bind 673-1344 vc1x.17.9.1.1.1-17.16.3.7.4")
        cls.assertClassCliSuccess("pw ethport 673-1344 1")
        cls.assertClassCliSuccess("pw psn 673-1344 mpls")
        cls.assertClassCliSuccess("pw enable 673-1344")
        
        # Use line 18 for HO CEP
        cls.assertClassCliSuccess("sdh map vc3.18.1.1-18.16.3 c3")
        cls.assertClassCliSuccess("pw create cep 1345-1392 basic")
        cls.assertClassCliSuccess("pw circuit bind 1345-1392 vc3.18.1.1-18.16.3")
        cls.assertClassCliSuccess("pw ethport 1345-1392 1")
        cls.assertClassCliSuccess("pw psn 1345-1392 mpls")
        cls.assertClassCliSuccess("pw enable 1345-1392")
        
    def testHoOverheadMustBeRedirected(self):
        def check(overhead, label):
            result = self.runCli("show sdh path vc3.17.1.1-17.16.3")
            numVc3s = 48
            self.assertEqual(numVc3s, result.numRows(), None)
            expectedVc3s = self.allLineSideVc3s()
            for row_i in range(0, numVc3s):
                pathId = result.cellContent(row_i, "PathId")
                self.assertEqual(pathId, expectedVc3s[row_i], None)
                psl = result.cellContent(row_i, overhead)
                self.assertEqual(psl, label, None)
        
        # Standing at MATE and Faceplate would have the same result
        
        self.assertCliSuccess("sdh path psl transmit vc3.17.1.1-17.16.3 0x03")
        check("txPsl", "0x03")
        self.assertCliSuccess("sdh path psl transmit vc3.1.1.1-1.16.3 0x04")
        check("txPsl", "0x04")
        
        self.assertCliSuccess("sdh path psl expect vc3.17.1.1-17.16.3 0x03")
        check("expPsl", "0x03")
        self.assertCliSuccess("sdh path psl expect vc3.3.1.1-3.16.3 0x04")
        check("expPsl", "0x04")
    
    def testHoVcTimingMustBeRedirected(self):
        self.assertCliSuccess("sdh path timing vc3.17.1.1-17.16.3 loop xxx")
        result = self.runCli("show sdh path vc3.17.1.1-17.16.3")
        self.assertEqual(result.numRows(), 48, None)
        for row_i in range(0, result.numRows()):
            timingMode = result.cellContent(row_i, "TimingMode")
            self.assertEqual(timingMode, "loop", None)
            
    def testHoVcAcrTimingMustBeRedirected(self):
        self.assertCliSuccess("sdh path timing vc3.18.1.1-18.16.3 acr xxx")
        result = self.runCli("show sdh path vc3.18.1.1-18.16.3")
        
        # Build expected PW
        expectedPws = range(1345, 1392 + 1)
        for row_i in range(0, result.numRows()):
            timingMode = result.cellContent(row_i, "TimingMode")
            self.assertEqual(timingMode, "acr", None)
            
            pw = "cep.%d" % expectedPws[row_i]
            timingSource = result.cellContent(row_i, "TimingSource")
            self.assertEqual(pw, timingSource, None)

class HideXcFaceplateDirectMappingCopy_Stm16(HideXcDirectMappingCopy):
    @classmethod
    def setUpClass(cls):
        super(HideXcFaceplateDirectMappingCopy_Stm16, cls).setUpClass()
        cls.assertClassCliSuccess("sdh line rate 1-8 stm16")
    
class HideXcFaceplateDirectXcDefaultConnection_Stm16(HideXcAbstract):
    @classmethod
    def setUpClass(cls):
        super(HideXcFaceplateDirectXcDefaultConnection_Stm16, cls).setUpClass()
        cls.assertClassCliSuccess("sdh line rate 1-8 stm16")

class BugFixes(AnnaMroTestCase):
    def testLoopback(self):
        script = """
        debug ciena mro xc hide direct
        device init
        serdes mode 1 stm64
        sdh line rate 1 stm64
        sdh map aug1.1.1 3xvc3s
        pw create cep 1 basic
        pw circuit bind 1 vc3.1.1.1
        pw ethport 1 1
        """
        self.assertCliSuccess(script)

        def testLoopback(mode):
            self.assertCliSuccess("sdh path loopback vc3.1.1.1 %s" % mode)
            cliResult = self.runCli("show sdh path loopback vc3.1.1.1")
            cellContent = cliResult.cellContent(0, "Loopback")
            self.assertEqual(cellContent, mode)

        for loopbackMode in ["local", "remote", "local"]:
            testLoopback(loopbackMode)

        testLoopback("release")

def TestDirectHidingOverhead(runner):
    
    def is20G():
        productCode = AtAppManager.localApp().productCode()
        return productCode == 0x60290022
        
    if shouldTestMate():
        LinesProvider.currentProvider = MateLinesProvider() 
        runner.run(HideXcDirectMatePathOverhead)
    
    LinesProvider.currentProvider = FaceplateLinesProvider_Stm16()
    runner.run(HideXcDirectFaceplatePathOverhead_Stm16)

def TestDirectHidingXcStm0(runner):
    LinesProvider.currentProvider = FaceplateLinesProvider_Stm0()
    runner.run(HideXcDirectDe3Path_Stm0)
    
def shouldTestMate():
    return False # It is buggy now, will be back if we need this

def TestDirectHiding(runner):
    lineProviders = [
                     FaceplateLinesProvider_Stm64(),
                     FaceplateLinesProvider_Stm16(),
                     FaceplateLinesProvider_Stm4(),
                     FaceplateLinesProvider_Stm1(),
                     FaceplateLinesProvider_Stm0()
                     ]
    if shouldTestMate():
        lineProviders.append(MateLinesProvider())

    for lineProvider in lineProviders:
        LinesProvider.currentProvider = lineProvider

        runner.run(HideXcDirectMappingCopy)
        runner.run(HideXcDirectXcDefaultConnection)
        runner.run(HideXcDirectPwBinding)
    
    TestDirectHidingOverhead(runner)    
    
    TestDirectHidingXcStm0(runner)
    
def TestMateHiding(runner):
    LinesProvider.currentProvider = MateLinesProvider()
    
    # Hiding at MATE, but Faceplate loopback
    runner.run(HideXcMateLoopbackFaceplate)
    runner.run(HideXcMateLoopbackFaceplateMappingCopy)
    runner.run(HideXcMateLoopbackFaceplateDefaultConnection)
    runner.run(HideXcMateLoopbackFaceplatePwBinding)
    runner.run(HideXcMateLoopbackFaceplatePathOverhead)

def TestMain():
    runner = AtTestCaseRunner.runner()
    
    runner.run(HideXc)
    TestDirectHiding(runner)
    
    if shouldTestMate():
        TestMateHiding(runner)

    runner.run(BugFixes)

    return runner.summary()

if __name__ == '__main__':
    TestMain()

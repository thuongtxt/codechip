import random

import python.arrive.atsdk.AtRegister as AtRegister
from test.AtTestCase import AtTestCaseRunner
from test.anna.AnnaTestCase import AnnaPdhTestCase

class Af60290011DccTestRunner(AnnaPdhTestCase):
    idList = []
    idString = '1-24.line|1-24.section'
    sgmiiEthPortStr = '4'

    @classmethod
    def setUpClass(cls):
        for i in range(24):
            dccIdStr = '%u.section' % (i + 1)
            cls.idList.append(dccIdStr)

        for i in range(24):
            dccIdStr = '%u.line' % (i + 1)
            cls.idList.append(dccIdStr)

        cls.classRunCli("device init")
        for i in range(24):
            dccIdStr = '%u.section' % (i + 1)
            cls.classRunCli("sdh line dcc create  %u section" % (i + 1))
            cls.classRunCli("sdh line dcc init %s" % dccIdStr)
            cls.classRunCli("show ciena pw hdlc  %s" % dccIdStr)
            dccIdStr = '%u.line' % (i + 1)
            cls.classRunCli("sdh line dcc create  %u line" % (i + 1))
            cls.classRunCli("sdh line dcc init %s" % dccIdStr)
            cls.classRunCli("show ciena pw hdlc  %s" % dccIdStr)

    def testHdlcInit(self):
        cli = "show ciena pw hdlc 1-24.line|1-24.section"
        result = self.runCli(cli)
        self.assertTrue(result.success(), "Run CLI \"%s\" fail" % cli)
        for i in range(48):
            self.assertEqual(result.cellContent(i, "EthPort"), "sgmii_kbyte_dcc(%s)" % self.sgmiiEthPortStr, None)
            self.assertEqual(result.cellContent(i, "MacCheckEn"), "dis", None)
            self.assertEqual(result.cellContent(i, "VlanCheckEn"), "dis", None)

    def testHdlcDefault(self):
        cli = "show sdh line dcc hdlc " + self.idString
        result = self.runCli(cli)
        self.assertTrue(result.success(), "Run CLI \"%s\" fail" % cli)
        for i in range(48):
            self.assertEqual(result.cellContent(i, "Flag"), "0x7e", None)
            self.assertEqual(result.cellContent(i, "FcsMode"), "fcs16", None)
            self.assertEqual(result.cellContent(i, "MTU"), "1536", None)

    def testHdlcFcs(self):
        fcsValList = ['fcs16', 'fcs32']
        dicFcs = {}
        
        for i in range(len(self.idList)):
            lineId  = self.idList[i]
            fcs = random.choice(fcsValList)
            self.assertCliSuccess("sdh line dcc hdlc fcs " + lineId + " %s" % fcs)
            dicFcs[lineId] = fcs
 
        cli = "show sdh line dcc hdlc " + self.idString
        result = self.runCli(cli)
        self.assertTrue(result.success(), "Run CLI \"%s\" fail" % cli)
        for i in range(48):
            self.assertEqual(result.cellContent(i, "FcsMode"), dicFcs[result.cellContent(i, "ChannelId")], None)
    
    def testHdlcNumFlag(self):
        flagValList = ['1', '2']
        dicFlag = {}
        
        for i in range(len(self.idList)):
            lineid  = self.idList[i]
            flag = random.choice(flagValList)
            self.assertCliSuccess("sdh line dcc hdlc flag %s 0x7e %s" %(lineid, flag))
            dicFlag[lineid] = flag
 
        cli = "show sdh line dcc hdlc " + self.idString
        result = self.runCli(cli)
        self.assertTrue(result.success(), "Run CLI \"%s\" fail" % cli)
        for i in range(48):
            self.assertEqual(result.cellContent(i, "NumFlags"), dicFlag[result.cellContent(i, "ChannelId")], None)
    

    def testHdlcCounter(self):
        self.assertCliSuccess("show sdh line dcc hdlc counters %s r2c" % self.idString)
        self.assertCliSuccess("show sdh line dcc hdlc counters %s ro" % self.idString)
        self.assertCliSuccess("show sdh line dcc hdlc counters %s r2c slient" % self.idString)
        self.assertCliSuccess("show sdh line dcc hdlc counters %s ro slient" % self.idString)
                 

    def testHdlcDisable(self):
        self.assertCliSuccess("sdh line dcc disable " + self.idString)
        cli = "show sdh line dcc hdlc " + self.idString
        result = self.runCli(cli)
        self.assertTrue(result.success(), "Run CLI \"%s\" fail" % cli)
        for i in range(48):
            self.assertEqual(result.cellContent(i, "TxEnable"), "dis", None)
            self.assertEqual(result.cellContent(i, "RxEnable"), "dis", None)
    
    def testHdlcTxEnableRxDisable(self):
        self.assertCliSuccess("sdh line dcc rx disable  " + self.idString)
        self.assertCliSuccess("sdh line dcc tx enable " + self.idString)
        cli = "show sdh line dcc hdlc " + self.idString
        result = self.runCli(cli)
        self.assertTrue(result.success(), "Run CLI \"%s\" fail" % cli)
        for i in range(48):
            self.assertEqual(result.cellContent(i, "TxEnable"), "en", None)
            self.assertEqual(result.cellContent(i, "RxEnable"), "dis", None)

    def testHdlcTxDisableRxEnable(self):
        self.assertCliSuccess("sdh line dcc rx enable  " + self.idString)
        self.assertCliSuccess("sdh line dcc tx disable " + self.idString)
        cli = "show sdh line dcc hdlc " + self.idString
        result = self.runCli(cli)
        self.assertTrue(result.success(), "Run CLI \"%s\" fail" % cli)
        for i in range(48):
            self.assertEqual(result.cellContent(i, "TxEnable"), "dis", None)
            self.assertEqual(result.cellContent(i, "RxEnable"), "en", None)
    
    def testHdlcEnable(self):
        self.assertCliSuccess("sdh line dcc enable " + self.idString)
        cli = "show sdh line dcc hdlc " + self.idString
        result = self.runCli(cli)
        self.assertTrue(result.success(), "Run CLI \"%s\" fail" % cli)
        for i in range(48):
            self.assertEqual(result.cellContent(i, "TxEnable"), "en", None)
            self.assertEqual(result.cellContent(i, "RxEnable"), "en", None)
    
    def testHdlcInterrupts(self):
        self.assertCliSuccess("show sdh line dcc hdlc interrupt " + self.idString + " r2c")
        self.assertCliSuccess("show sdh line dcc hdlc interrupt " + self.idString + " ro")
        self.assertCliSuccess("show sdh line dcc hdlc interrupt " + self.idString + " r2c slient")
        self.assertCliSuccess("show sdh line dcc hdlc interrupt " + self.idString + " ro slient")

        cli = "show sdh line dcc hdlc interrupt " + self.idString + " r2c"
        result = self.runCli(cli)
        self.assertTrue(result.success(), "Run CLI \"%s\" fail" % cli)
        for i in range(48):
            self.assertEqual(result.cellContent(i, "fcs"), "clear", None)
            self.assertEqual(result.cellContent(i, "undersize"), "clear", None)
            self.assertEqual(result.cellContent(i, "oversize"), "clear", None)
            self.assertEqual(result.cellContent(i, "decbuffull"), "clear", None)
            self.assertEqual(result.cellContent(i, "encbuffull"), "clear", None)
            
    def testPwDccEthType(self):
        self.assertCliFailure("ciena pw dcc ethtype %s 0x8809" % self.idString)
        self.assertCliFailure("ciena pw dcc ethtype %s 0x8800" % self.idString)
        self.assertCliSuccess("ciena pw dcc ethtype %s 0x880b" % self.idString)
        cli = "show ciena pw hdlc " + self.idString
        result = self.runCli(cli)
        self.assertTrue(result.success(), "Run CLI \"%s\" fail" % cli)
        for i in range(48):
            self.assertEqual(result.cellContent(i, "EthType"), "0x880b", None)
    
    def testPwDccEthType1(self):
        dicEthType = {}
        
        for i in range(len(self.idList)):
            lineId  = self.idList[i]
            dccType = '0x%02x'%(random.randint(0, 255))
            self.assertCliSuccess("ciena pw dcc type  " + lineId + " %s" % dccType)
            dicEthType[lineId] = dccType
        cli = "show ciena pw hdlc " + self.idString
        result = self.runCli(cli)
        self.assertTrue(result.success(), "Run CLI \"%s\" fail" % cli)
        for i in range(48):
            self.assertEqual(result.cellContent(i, "Type"), dicEthType[result.cellContent(i, "PW")] , None)
            
    def testPwDccVersion(self):
        dicVersion = {}
        for i in range(len(self.idList)):
            lineId  = self.idList[i]
            version = '0x%02x'%(random.randint(0, 255))
            self.assertCliSuccess("ciena pw dcc version  " + lineId + " %s" % version)
            dicVersion[lineId] = version
        cli = "show ciena pw hdlc " + self.idString
        result = self.runCli(cli)
        self.assertTrue(result.success(), "Run CLI \"%s\" fail" % cli)
        for i in range(48):
            self.assertEqual(result.cellContent(i, "Version"), dicVersion[result.cellContent(i, "PW")] , None)
    
    def testPwDccCVlanMustNotNone(self):
        for i in range(len(self.idList)):
            lineId  = self.idList[i]
            self.assertCliFailure("ciena pw dcc ethheader %s 55.33.44.22.11.66 none none" % lineId)
            
    def testPwDccSVlanMustNone(self):
        for i in range(len(self.idList)):
            lineId  = self.idList[i]
            self.assertCliFailure("ciena pw dcc ethheader %s 55.33.44.22.11.66 none 1.1.1" % lineId)
            self.assertCliSuccess("ciena pw dcc ethheader %s 55.33.44.22.11.66 1.1.1 none" % lineId)
                    
    def testPwDccCVlan(self):
        dicCVlan = {}
        for i in range(len(self.idList)):
            lineId  = self.idList[i]
            vlanId =  random.randint(0, 4095)
            vlanIdStr = '1.1.%u' % vlanId
            self.assertCliSuccess("ciena pw dcc ethheader %s 55.33.44.22.11.66 %s none" %(lineId, vlanIdStr))
            dicCVlan[lineId] = vlanIdStr
        cli = "show ciena pw hdlc " + self.idString
        result = self.runCli(cli)
        self.assertTrue(result.success(), "Run CLI \"%s\" fail" % cli)
        for i in range(48):
            self.assertEqual(result.cellContent(i, "VLAN"), dicCVlan[result.cellContent(i, "PW")] , None)
    
    def testPwDccDestMac(self):
        dicMac = {}
        for i in range(len(self.idList)):
            lineId  = self.idList[i]
            macStr = '%02d.%02d.%02d.%02d.%02d.%02d'%(random.randint(0, 99), random.randint(0, 99), random.randint(0, 99), random.randint(0, 99), random.randint(0, 99), random.randint(0, 99))
            self.assertCliSuccess("ciena pw dcc ethheader %s %s 1.1.1 none" %(lineId, macStr))
            dicMac[lineId] = macStr
        cli = "show ciena pw hdlc " + self.idString
        result = self.runCli(cli)
        self.assertTrue(result.success(), "Run CLI \"%s\" fail" % cli)
        for i in range(48):
            self.assertEqual(result.cellContent(i, "DMAC"), dicMac[result.cellContent(i, "PW")] , None)
    
    def testPwDccSrcMac(self):
        dicMac = {}
        for i in range(len(self.idList)):
            lineId  = self.idList[i]
            macStr = '%02d.%02d.%02d.%02d.%02d.%02d'%(random.randint(0, 99), random.randint(0, 99), random.randint(0, 99), random.randint(0, 99), random.randint(0, 99), random.randint(0, 99))
            self.assertCliSuccess("ciena pw dcc sourcemac %s %s" %(lineId, macStr))
            dicMac[lineId] = macStr
        cli = "show ciena pw hdlc " + self.idString
        result = self.runCli(cli)
        self.assertTrue(result.success(), "Run CLI \"%s\" fail" % cli)
        for i in range(48):
            self.assertEqual(result.cellContent(i, "SMAC"), dicMac[result.cellContent(i, "PW")] , None)
    
    def testPwDccMacCheck(self):
        dicMac = {}
        valList = ['en', 'dis']
        for i in range(len(self.idList)):
            lineId  = self.idList[i]
            enable = random.choice(valList)
            if enable == 'en':
                self.assertCliSuccess("ciena pw dcc mac check enable %s" % lineId)
            else:
                self.assertCliSuccess("ciena pw dcc mac check disable %s" % lineId)
            dicMac[lineId] = enable
        cli = "show ciena pw hdlc " + self.idString
        result = self.runCli(cli)
        self.assertTrue(result.success(), "Run CLI \"%s\" fail" % cli)
        for i in range(48):
            self.assertEqual(result.cellContent(i, "MacCheckEn"), dicMac[result.cellContent(i, "PW")] , None)

    def testPwDccVlanCheck(self):
        dicVlan = {}
        valList = ['en', 'dis']
        for i in range(len(self.idList)):
            dccId  = self.idList[i]
            enable = random.choice(valList)
            if enable == 'en':
                self.assertCliSuccess("ciena pw dcc vlan check enable %s" %dccId)
            else:
                self.assertCliSuccess("ciena pw dcc vlan check disable %s" %dccId)
            dicVlan[dccId] = enable

        cli = "show ciena pw hdlc " + self.idString
        result = self.runCli(cli)
        self.assertTrue(result.success(), "Run CLI \"%s\" fail" % cli)
        for i in range(48):
            self.assertEqual(result.cellContent(i, "VlanCheckEn"), dicVlan[result.cellContent(i, "PW")] , None)

    def testPwDccExpectedLabel(self):
        dicLabelStr = {}
        labels = list()
        for i in range(len(self.idList)):
            lineId  = self.idList[i]

            while 1:
                labelStr ='0x%02x'%(random.randint(0, 63))
                if labelStr not in labels:
                    labels.append(labelStr)
                    break

            self.assertCliSuccess("ciena pw dcc expectedlabel %s %s" %(lineId, labelStr))
            dicLabelStr[lineId] = labelStr
            
        cli = "show ciena pw hdlc " + self.idString
        result = self.runCli(cli)
        self.assertTrue(result.success(), "Run CLI \"%s\" fail" % cli)
        for i in range(48):
            self.assertEqual(result.cellContent(i, "ExpectLabel"), dicLabelStr[result.cellContent(i, "PW")] , None)
    
    def testPwDccExpectedCVlan(self):
        vlanStr = '0x%04x'%(random.randint(0, 4095))
        self.assertCliSuccess("ciena eth port cvlan expect %s %s" %(self.sgmiiEthPortStr, vlanStr))
        cli = "show ciena pw hdlc " + self.idString
        result = self.runCli(cli)
        self.assertTrue(result.success(), "Run CLI \"%s\" fail" % cli)
        for i in range(48):
            self.assertEqual(result.cellContent(i, "ExpectCVlan"), vlanStr, None)
    
    def testPwDccGlobalMac(self):
        macStr = '%02d.%02d.%02d.%02d.%02d.00'%(random.randint(0, 99), random.randint(0, 99), random.randint(0, 99), random.randint(0, 99), random.randint(0, 99))

        self.assertCliSuccess("ciena pw dcc mac global %s %s" %(self.sgmiiEthPortStr, macStr))
        cli = "show ciena eth port dcc_kbyte " + self.sgmiiEthPortStr
        result = self.runCli(cli)
        self.assertTrue(result.success(), "Run CLI \"%s\" fail" % cli)

        expectedMac = [int(byteString, 16) for byteString in macStr.split(".")]
        expectedMac[len(expectedMac) - 1] = expectedMac[len(expectedMac) - 1] & (~AtRegister.cBit5_0)
        expectedMacString = ".".join(["%02x" % byteValue for byteValue in expectedMac])
        self.assertEqual(result.cellContent(0, "expectedGlobalMac"), expectedMacString, None)

    def testSgmiiPortCounters(self):
        PortStr = str(random.randint(1, 4))
        cli = "show ciena eth port dcc_kbyte counters %s r2c" % PortStr
        result = self.runCli(cli)
        self.assertTrue(result.success(), "Run CLI \"%s\" fail" % cli)
        counterVal = result.cellContent(0, "DccTxFrames")
        if PortStr == self.sgmiiEthPortStr:
            self.assertTrue(counterVal == '0', "Run CLI \"%s\" fail" % cli)
        else:
            self.assertTrue(counterVal != '0', "Run CLI \"%s\" fail" % cli)

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(Af60290011DccTestRunner)
    return runner.summary()
    
if __name__ == '__main__':
    TestMain()

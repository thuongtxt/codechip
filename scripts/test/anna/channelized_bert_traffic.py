from test.AtTestCase import AtTestCaseRunner
from test.anna.AnnaTestCase import AnnaTestCase
from test.anna.channelized_bert import BertChecker


class AbstractTest(AnnaTestCase):
    @classmethod
    def shouldRunOnBoard(cls):
        return True

    def assertBertGood(self, engineCliIds):
        bertChecker = BertChecker(engineCliIds, simulated=self.simulated())
        bertChecker.assertGood(retryTimes=10)
        del bertChecker

    def assertPwCounters(self, pwCliIdList, assertFunction):
        for pwCliId in pwCliIdList:
            cliResult = self.runCli("show pw counters %s r2c" % pwCliId)
            for row in range(cliResult.numRows()):
                counterName = cliResult.cellContent(row, columnIdOrName=0)
                counterValue = 0
                try:
                    counterValue = int(cliResult.cellContent(row, columnIdOrName=1))
                except:
                    pass

                if not self.simulated():
                    assertFunction(counterName, counterValue)

    @staticmethod
    def cliIdStringFromIdList(idList):
        return ",".join(["%d" % anId for anId in idList])

    def clearPwCounters(self, pwCliIdList):
        idString = self.cliIdStringFromIdList(pwCliIdList)
        self.runCli("show pw counters %s r2c silent" % idString)

    def allPwCliIdString(self):
        return "Invalid"

    def assertPwSquel(self, squelPwCliIds, pairPwCliIds):
        def assertCounterZero(counterName, counterValue):
            if counterName not in ["N/S", "N/A", "NumPktsInJitterBuffer"]:
                assert counterValue == 0

        def assertRxCounterZero(counterName, counterValue):
            if counterName in ["N/S", "N/A"]:
                return

            if counterName in ["RxPackets", "RxPayloadBytes"]:
                assert counterValue == 0

            if counterName == "RxLops":
                assert counterValue > 0

        self.assertPwCounters(squelPwCliIds, assertCounterZero)
        self.assertPwCounters(pairPwCliIds, assertRxCounterZero)

    def clearAllPwCounters(self):
        self.runCli("show pw counters %s r2c silent" % self.allPwCliIdString())

class Vc4ChannelizedVc1x(AbstractTest):
    def allPwCliIdString(self):
        return "1-56"

    @classmethod
    def setUpClass(cls):
        script = """
        device init
            
        # Setup SERDES and loopback all of them
        serdes mode 1 stm16
        sdh line rate 1 stm16
        sdh line mode 1 sonet
        serdes loopback 1 local
        serdes loopback 25,26 local
        
        # Setup AUG1#1
        sdh map aug1.1.1,25.1 vc4
        xc vc connect vc4.1.1 vc4.25.1 two-way
        sdh map vc4.25.1 3xtug3s
        sdh map tug3.25.1.1-25.1.3 7xtug2s
        sdh map tug2.25.1.1.1-25.1.3.7 tu11
        sdh map vc1x.25.1.1.1.1-25.1.3.7.4 c1x
        
        # Setup AUG1#1 PWs
        pw create cep 1-28 basic
        pw circuit bind 1-28 vc1x.25.1.1.1.1-25.1.1.7.4
        pw ethport 1-28 1
        pw psn 1-28 mpls
        
        # Setup AUG1#2
        sdh map aug1.1.2,25.2 vc4
        xc vc connect vc4.1.2 vc4.25.2 two-way
        sdh map vc4.25.2 3xtug3s
        sdh map tug3.25.2.1-25.2.3 7xtug2s
        sdh map tug2.25.2.1.1-25.2.3.7 tu11
        sdh map vc1x.25.2.1.1.1-25.2.3.7.4 c1x
        
        # Setup AUG1#2 PWs
        pw create cep 29-56 basic
        pw circuit bind 29-56 vc1x.25.2.1.1.1-25.2.1.7.4
        pw ethport 29-56 1
        pw psn 29-56 mpls
        """
        cls.assertClassCliSuccess(script)

        # Connect PWs of these two AUG-1s
        for i in range(28):
            pw1 = i + 1
            pw2 = pw1 + 28
            cls.assertClassCliSuccess("pw mpls innerlabel %d %d.1.255" % (pw1, pw2))
            cls.assertClassCliSuccess("pw mpls innerlabel %d %d.1.255" % (pw2, pw1))

        # Enable all of these PWs
        cls.assertClassCliSuccess("pw enable 1-56")

    def setUp(self):
        script = """
        prbs engine create vc 1-28 vc1x.25.1.1.1.1-25.1.1.7.4
        prbs engine side monitoring 1-28 psn
        prbs engine enable 1-28
        """
        self.assertCliSuccess(script)
        self.assertBertGood("1-28")

    def testVc4Bert(self):
        # Provision BERT on VC-4 layer
        script = """
        prbs engine delete 28
        prbs engine create vc 28 vc4.25.1
        prbs engine enable 28
        """
        self.assertCliSuccess(script)
        self.assertBertGood("28")
        self.clearAllPwCounters()
        self.sleep(1)
        self.assertPwSquel(squelPwCliIds=[i + 1 for i in range(28)], pairPwCliIds=[i + 29 for i in range(28)])

        # Deprovision BERT on VC-4 layer
        self.assertCliSuccess("prbs engine delete 28")
        self.assertBertGood("1-27")
        script = """
        prbs engine create vc 28 vc1x.25.1.1.7.4
        prbs engine side monitoring 28 psn
        prbs engine enable 28
        """
        self.assertCliSuccess(script)
        self.assertBertGood("1-28")

class Vc4ChannelizedDe1(AbstractTest):
    def allPwCliIdString(self):
        return "1-56"

    @classmethod
    def setUpClass(cls):
        script = """
        device init

        # Setup SERDES and loopback all of them
        serdes mode 1 stm16
        sdh line rate 1 stm16
        sdh line mode 1 sonet
        serdes loopback 1 local
        serdes loopback 25,26 local

        # Setup AUG1#1
        sdh map aug1.1.1,25.1 vc4
        xc vc connect vc4.1.1 vc4.25.1 two-way
        sdh map vc4.25.1 3xtug3s
        sdh map tug3.25.1.1-25.1.3 7xtug2s
        sdh map tug2.25.1.1.1-25.1.3.7 tu11
        sdh map vc1x.25.1.1.1.1-25.1.3.7.4 de1

        # Setup AUG1#1 PWs
        pw create satop 1-28
        pw circuit bind 1-28 de1.25.1.1.1.1-25.1.1.7.4
        pw ethport 1-28 1
        pw psn 1-28 mpls

        # Setup AUG1#2
        sdh map aug1.1.2,25.2 vc4
        xc vc connect vc4.1.2 vc4.25.2 two-way
        sdh map vc4.25.2 3xtug3s
        sdh map tug3.25.2.1-25.2.3 7xtug2s
        sdh map tug2.25.2.1.1-25.2.3.7 tu11
        sdh map vc1x.25.2.1.1.1-25.2.3.7.4 de1

        # Setup AUG1#2 PWs
        pw create satop 29-56
        pw circuit bind 29-56 de1.25.2.1.1.1-25.2.1.7.4
        pw ethport 29-56 1
        pw psn 29-56 mpls
        """
        cls.assertClassCliSuccess(script)

        # Connect PWs of these two AUG-1s
        for i in range(28):
            pw1 = i + 1
            pw2 = pw1 + 28
            cls.assertClassCliSuccess("pw mpls innerlabel %d %d.1.255" % (pw1, pw2))
            cls.assertClassCliSuccess("pw mpls innerlabel %d %d.1.255" % (pw2, pw1))

        # Enable all of these PWs
        cls.assertClassCliSuccess("pw enable 1-56")

    def setUp(self):
        self.assertCliSuccess("prbs engine delete 1-28")

        script = """
        prbs engine create de1 1-28 25.1.1.1.1-25.1.1.7.4
        prbs engine side monitoring 1-28 psn
        prbs engine enable 1-28
        """
        self.assertCliSuccess(script)
        self.assertBertGood("1-28")

    def testVc4Bert(self):
        # Provision BERT on VC-4 layer
        script = """
        prbs engine delete 28
        prbs engine create vc 28 vc4.25.1
        prbs engine enable 28
        """
        self.assertCliSuccess(script)
        self.assertBertGood("28")
        self.clearAllPwCounters()
        self.sleep(1)
        self.assertPwSquel(squelPwCliIds=[i + 1 for i in range(28)], pairPwCliIds=[i + 29 for i in range(28)])

        # Deprovision BERT on VC-4 layer
        self.assertCliSuccess("prbs engine delete 28")
        self.assertBertGood("1-27")
        script = """
        prbs engine create de1 28 25.1.1.7.4
        prbs engine side monitoring 28 psn
        prbs engine enable 28
        """
        self.assertCliSuccess(script)
        self.assertBertGood("1-28")

    def testVc1xBert(self):
        # Provision BERT on VC-1x layer
        script = """
        prbs engine delete 25-28
        prbs engine create vc 25-28 vc1x.25.1.1.7.1-25.1.1.7.4
        prbs engine enable 25-28
        """
        self.assertCliSuccess(script)
        self.assertBertGood("25-28")
        self.assertBertGood("1-24")
        self.clearAllPwCounters()
        self.sleep(1)
        squelPws = [i + 25 for i in range(4)]
        self.assertPwSquel(squelPwCliIds=squelPws, pairPwCliIds=[i + 28 for i in squelPws])

        # Deprovision BERT on VC-1x layer
        self.assertCliSuccess("prbs engine delete 25-28")
        self.assertBertGood("1-24")
        script = """
        prbs engine create de1 25-28 25.1.1.7.1-25.1.1.7.4
        prbs engine side monitoring 25-28 psn
        prbs engine enable 25-28
        """
        self.assertCliSuccess(script)
        self.assertBertGood("1-28")

class Vc4ChannelizedDe3(AbstractTest):
    @classmethod
    def setUpClass(cls):
        script = """
        device init

        # Setup SERDES and loopback all of them
        serdes mode 1 stm16
        sdh line rate 1 stm16
        sdh line mode 1 sonet
        serdes loopback 1 local
        serdes loopback 25,26 local

        # Setup AUG1#1
        sdh map aug1.1.1,25.1 vc4
        xc vc connect vc4.1.1 vc4.25.1 two-way
        sdh map vc4.25.1 3xtug3s
        sdh map tug3.25.1.1-25.1.3 vc3
        sdh map vc3.25.1.1-25.1.3 de3

        # Setup AUG1#1 PWs
        pw create satop 1-3
        pw circuit bind 1-3 de3.25.1.1-25.1.3
        pw ethport 1-3 1
        pw psn 1-3 mpls

        # Setup AUG1#2
        sdh map aug1.1.2,25.2 vc4
        xc vc connect vc4.1.2 vc4.25.2 two-way
        sdh map vc4.25.2 3xtug3s
        sdh map tug3.25.2.1-25.2.3 vc3
        sdh map vc3.25.2.1-25.2.3 de3

        # Setup AUG1#2 PWs
        pw create satop 4-6
        pw circuit bind 4-6 de3.25.2.1-25.2.3
        pw ethport 4-6 1
        pw psn 4-6 mpls
        """
        cls.assertClassCliSuccess(script)

        # Connect PWs of these two AUG-1s
        for i in range(3):
            pw1 = i + 1
            pw2 = pw1 + 3
            cls.assertClassCliSuccess("pw mpls innerlabel %d %d.1.255" % (pw1, pw2))
            cls.assertClassCliSuccess("pw mpls innerlabel %d %d.1.255" % (pw2, pw1))

        # Enable all of these PWs
        cls.assertClassCliSuccess("pw enable 1-6")

    def setUp(self):
        self.assertCliSuccess("prbs engine delete 1-3")

        script = """
        prbs engine create de3 1-3 25.1.1-25.1.3
        prbs engine side monitoring 1-3 psn
        prbs engine enable 1-3
        """
        self.assertCliSuccess(script)
        self.assertBertGood("1-3")

    def allPwCliIdString(self):
        return "1-6"

    def testVc4Bert(self):
        # Provision BERT on VC-4 layer
        script = """
        prbs engine delete 4
        prbs engine create vc 4 vc4.25.1
        prbs engine enable 4
        """
        self.assertCliSuccess(script)
        self.assertBertGood("4")
        self.clearAllPwCounters()
        self.sleep(1)
        squelPws = [i + 1 for i in range(3)]
        self.assertPwSquel(squelPwCliIds=squelPws, pairPwCliIds=[i + 3 for i in squelPws])

        # Deprovision BERT on VC-4 layer
        self.assertCliSuccess("prbs engine delete 4")
        self.assertBertGood("1-3")

class Vc4ChannelizedDe3ChannelizedDe1(AbstractTest):
    def allPwCliIdString(self):
        return "1-56"

    @classmethod
    def setUpClass(cls):
        script = """
        device init

        # Setup SERDES and loopback all of them
        serdes mode 1 stm16
        sdh line rate 1 stm16
        sdh line mode 1 sonet
        serdes loopback 1 local
        serdes loopback 25,26 local

        # Setup AUG1#1
        sdh map aug1.1.1,25.1 vc4
        xc vc connect vc4.1.1 vc4.25.1 two-way
        sdh map vc4.25.1 3xtug3s
        sdh map tug3.25.1.1-25.1.3 vc3
        sdh map vc3.25.1.1-25.1.3 de3
        pdh de3 framing 25.1.1-25.1.3 ds3_cbit_28ds1

        # Setup AUG1#1 PWs
        pw create satop 1-28
        pw circuit bind 1-28 de1.25.1.1.1.1-25.1.1.7.4
        pw ethport 1-28 1
        pw psn 1-28 mpls

        # Setup AUG1#2
        sdh map aug1.1.2,25.2 vc4
        xc vc connect vc4.1.2 vc4.25.2 two-way
        sdh map vc4.25.2 3xtug3s
        sdh map tug3.25.2.1-25.2.3 vc3
        sdh map vc3.25.2.1-25.2.3 de3
        pdh de3 framing 25.2.1-25.2.3 ds3_cbit_28ds1

        # Setup AUG1#2 PWs
        pw create satop 29-56
        pw circuit bind 29-56 de1.25.2.1.1.1-25.2.1.7.4
        pw ethport 29-56 1
        pw psn 29-56 mpls
        """
        cls.assertClassCliSuccess(script)

        # Connect PWs of these two AUG-1s
        for i in range(28):
            pw1 = i + 1
            pw2 = pw1 + 28
            cls.assertClassCliSuccess("pw mpls innerlabel %d %d.1.255" % (pw1, pw2))
            cls.assertClassCliSuccess("pw mpls innerlabel %d %d.1.255" % (pw2, pw1))

        # Enable all of these PWs
        cls.assertClassCliSuccess("pw enable 1-56")

    def setUp(self):
        self.assertCliSuccess("prbs engine delete 1-28")

        script = """
        prbs engine create de1 1-28 25.1.1.1.1-25.1.1.7.4
        prbs engine side monitoring 1-28 psn
        prbs engine enable 1-28
        """
        self.assertCliSuccess(script)
        self.assertBertGood("1-28")

    def testVc4Bert(self):
        # Provision BERT on VC-4 layer
        script = """
        prbs engine delete 28
        prbs engine create vc 28 vc4.25.1
        prbs engine enable 28
        """
        self.assertCliSuccess(script)
        self.assertBertGood("28")
        self.clearAllPwCounters()
        self.sleep(1)
        squelPws = [i + 1 for i in range(28)]
        self.assertPwSquel(squelPwCliIds=squelPws, pairPwCliIds=[i + 28 for i in squelPws])

        # Deprovision BERT on VC-4 layer
        self.assertCliSuccess("prbs engine delete 28")
        self.assertBertGood("1-27")
        script = """
        prbs engine create de1 28 25.1.1.7.4
        prbs engine side monitoring 28 psn
        prbs engine enable 28
        """
        self.assertCliSuccess(script)
        self.assertBertGood("1-28")

    def testDe3Bert(self):
        # Provision BERT on DE3 layer
        script = """
        prbs engine delete 1
        prbs engine create de3 1 25.1.1
        prbs engine enable 1
        """
        self.assertCliSuccess(script)
        self.assertBertGood("1")
        self.clearAllPwCounters()
        self.sleep(1)
        squelPws = [i + 1 for i in range(28)]
        self.assertPwSquel(squelPwCliIds=squelPws, pairPwCliIds=[i + 28 for i in squelPws])

        # Deprovision BERT on DE3 layer
        self.assertCliSuccess("prbs engine delete 1")
        self.assertBertGood("2-28")
        script = """
        prbs engine create de1 1 25.1.1.1.1
        prbs engine side monitoring 1 psn
        prbs engine enable 1-28
        """
        self.assertCliSuccess(script)
        self.assertBertGood("1-28")

class Vc4ChannelizedDe3ChannelizedDe1ChannelizedNxDs0(AbstractTest):
    def allPwCliIdString(self):
        return "1-56"

    @classmethod
    def setUpClass(cls):
        script = """
        device init

        # Setup SERDES and loopback all of them
        serdes mode 1 stm16
        sdh line rate 1 stm16
        sdh line mode 1 sonet
        serdes loopback 1 local
        serdes loopback 25,26 local

        # Setup AUG1#1
        sdh map aug1.1.1,25.1 vc4
        xc vc connect vc4.1.1 vc4.25.1 two-way
        sdh map vc4.25.1 3xtug3s
        sdh map tug3.25.1.1-25.1.3 vc3
        sdh map vc3.25.1.1-25.1.3 de3
        pdh de3 framing 25.1.1-25.1.3 ds3_cbit_28ds1
        pdh de1 framing 25.1.1.1.1-25.1.1.7.4 ds1_sf
        pdh de1 nxds0create 25.1.1.1.1-25.1.1.7.4 0-23

        # Setup AUG1#1 PWs
        pw create cesop 1-28 basic
        pw circuit bind 1-28 nxds0.25.1.1.1.1-25.1.1.7.4.0-23
        pw ethport 1-28 1
        pw psn 1-28 mpls

        # Setup AUG1#2
        sdh map aug1.1.2,25.2 vc4
        xc vc connect vc4.1.2 vc4.25.2 two-way
        sdh map vc4.25.2 3xtug3s
        sdh map tug3.25.2.1-25.2.3 vc3
        sdh map vc3.25.2.1-25.2.3 de3
        pdh de3 framing 25.2.1-25.2.3 ds3_cbit_28ds1
        pdh de1 framing 25.2.1.1.1-25.2.1.7.4 ds1_sf
        pdh de1 nxds0create 25.2.1.1.1-25.2.1.7.4 0-23

        # Setup AUG1#2 PWs
        pw create cesop 29-56 basic
        pw circuit bind 29-56 nxds0.25.2.1.1.1-25.2.1.7.4.0-23
        pw ethport 29-56 1
        pw psn 29-56 mpls
        """
        cls.assertClassCliSuccess(script)

        # Connect PWs of these two AUG-1s
        for i in range(28):
            pw1 = i + 1
            pw2 = pw1 + 28
            cls.assertClassCliSuccess("pw mpls innerlabel %d %d.1.255" % (pw1, pw2))
            cls.assertClassCliSuccess("pw mpls innerlabel %d %d.1.255" % (pw2, pw1))

        # Enable all of these PWs
        cls.assertClassCliSuccess("pw enable 1-56")

    def setUp(self):
        self.assertCliSuccess("prbs engine delete 1-28")

        script = """
        prbs engine create nxds0 1-28 25.1.1.1.1-25.1.1.7.4 0-23
        prbs engine side monitoring 1-28 psn
        prbs engine enable 1-28
        """
        self.assertCliSuccess(script)
        self.assertBertGood("1-28")

    def testDe1Bert(self):
        # Provision BERT on DE1 layer
        script = """
        prbs engine delete 1
        prbs engine create de1 1 25.1.1.1.1
        prbs engine enable 1
        """
        self.assertCliSuccess(script)
        self.assertBertGood("1")
        self.clearAllPwCounters()
        self.sleep(1)
        squelPws = [1]
        self.assertPwSquel(squelPwCliIds=squelPws, pairPwCliIds=[i + 28 for i in squelPws])

        # Deprovision BERT on DE1 layer
        self.assertCliSuccess("prbs engine delete 1")
        self.assertBertGood("2-28")
        script = """
        prbs engine create nxds0 1 25.1.1.1.1 0-23
        prbs engine side monitoring 1 psn
        prbs engine enable 1-28
        """
        self.assertCliSuccess(script)
        self.assertBertGood("1-28")

class NestBert(AbstractTest):
    @classmethod
    def setUpClass(cls):
        script = """
                device init

                # Setup SERDES and loopback all of them
                serdes mode 1 stm16
                sdh line rate 1 stm16
                sdh line mode 1 sonet
                serdes loopback 1 local
                serdes loopback 25,26 local

                # Setup AUG1#1
                sdh map aug1.1.1,25.1 vc4
                xc vc connect vc4.1.1 vc4.25.1 two-way
                sdh map vc4.25.1 3xtug3s
                sdh map tug3.25.1.1-25.1.3 vc3
                sdh map vc3.25.1.1-25.1.3 de3
                pdh de3 framing 25.1.1-25.1.3 ds3_cbit_28ds1
                pdh de1 framing 25.1.1.1.1-25.1.1.7.4 ds1_sf
                pdh de1 nxds0create 25.1.1.1.1-25.1.1.7.4 0-23

                # Setup AUG1#1 PWs
                pw create cesop 1-28 basic
                pw circuit bind 1-28 nxds0.25.1.1.1.1-25.1.1.7.4.0-23
                pw ethport 1-28 1
                pw psn 1-28 mpls

                # Setup AUG1#2
                sdh map aug1.1.2,25.2 vc4
                xc vc connect vc4.1.2 vc4.25.2 two-way
                sdh map vc4.25.2 3xtug3s
                sdh map tug3.25.2.1-25.2.3 vc3
                sdh map vc3.25.2.1-25.2.3 de3
                pdh de3 framing 25.2.1-25.2.3 ds3_cbit_28ds1
                pdh de1 framing 25.2.1.1.1-25.2.1.7.4 ds1_sf
                pdh de1 nxds0create 25.2.1.1.1-25.2.1.7.4 0-23

                # Setup AUG1#2 PWs
                pw create cesop 29-56 basic
                pw circuit bind 29-56 nxds0.25.2.1.1.1-25.2.1.7.4.0-23
                pw ethport 29-56 1
                pw psn 29-56 mpls
                """
        cls.assertClassCliSuccess(script)

    def testNestBert(self):
        self.assertCliSuccess("prbs engine create nxds0 1,2 25.1.1.1.1,25.2.1.1.1 0-23")
        self.assertCliSuccess("prbs engine enable 1,2")
        self.assertBertGood("1,2")

        self.assertCliSuccess("prbs engine create de1 3,4 25.1.1.1.1,25.2.1.1.1")
        self.assertCliSuccess("prbs engine enable 3,4")
        self.assertBertGood("3,4")

        self.assertCliSuccess("prbs engine create de3 5,6 25.1.1,25.2.1")
        self.assertCliSuccess("prbs engine enable 5,6")
        self.assertBertGood("5,6")

        self.assertCliSuccess("prbs engine create vc4 7,8 25.1,25.2")
        self.assertCliSuccess("prbs engine enable 7,8")
        self.assertBertGood("7,8")

        self.assertCliSuccess("prbs engine delete 7,8")
        self.assertBertGood("5,6")

        self.assertCliSuccess("prbs engine delete 5,6")
        self.assertBertGood("3,4")

        self.assertCliSuccess("prbs engine delete 3,4")
        self.assertBertGood("1,2")


def channelizedBertSupported():
    return False # TODO: not now, will come back later

def TestMain():
    if not channelizedBertSupported():
        return

    runner = AtTestCaseRunner.runner()
    runner.run(Vc4ChannelizedVc1x)
    runner.run(Vc4ChannelizedDe1)
    runner.run(Vc4ChannelizedDe3)
    runner.run(Vc4ChannelizedDe3ChannelizedDe1)
    runner.run(Vc4ChannelizedDe3ChannelizedDe1ChannelizedNxDs0)
    #runner.run(NestBert)
    runner.summary()

if __name__ == '__main__':
    TestMain()

import random
from test.AtTestCase import AtTestCaseRunner
from test.anna.AnnaTestCase import AnnaMroTestCase


class ExcessiveErrorRatioAbstractTest(AnnaMroTestCase):
    @staticmethod
    def ethPortCliIdFromSerdesCliId(serdesCliId):
        return serdesCliId + 1

    def ethPortCliIds(self):
        """
        :rtype: list
        """
        return [self.ethPortCliIdFromSerdesCliId(serdesId) for serdesId in self.serdesCliIds()]

    @classmethod
    def serdesCliIds(cls):
        """
        :rtype: list
        """
        if cls.is20G():
            return [1, 9]
        if cls.is10G():
            return [1]

        return list()

    @staticmethod
    def cliIdString(idList):
        return ",".join(["%d" % cliId for cliId in idList])

    def ethPortCliIdString(self):
        return self.cliIdString(self.ethPortCliIds())

    def serdesCliIdString(self):
        return self.cliIdString(self.serdesCliIds())

    @classmethod
    def setUpClass(cls):
        cls.assertClassCliSuccess("device init")

    @staticmethod
    def defaultSerdesMode():
        return "1G"

    def setupSerdesMode(self):
        self.assertCliSuccess("serdes mode %s %s" % (self.serdesCliIdString(), self.defaultSerdesMode()))

    def setUp(self):
        self.setupSerdesMode()

    def getThresholds(self, portCliId):
        """
        :rtype: python.arrive.AtAppManager.AtCliResult.AtCliResult
        """
        return self.runCli("show ciena eth port threshold excessive_error_ratio %s" % portCliId)

    def getThresholdAttribute(self, portCliId, name):
        stringValue = self.getThresholds(portCliId).cellContent(0, name)
        return int(stringValue)

    @staticmethod
    def isNumber(aString):
        if aString in ["N/A", "N/S", "xxx"]:
            return False
        return True  # Must be a number

    def TestAccessThreshold(self, portCliId, supported = True):
        result = self.getThresholds(portCliId)
        self.assertGreater(result.numRows(), 0)

        for column in range(1, result.numColumns()):
            cellContent = result.cellContent(0, column)
            isNumber = self.isNumber(cellContent)
            if supported:
                self.assertTrue(isNumber)
            else:
                self.assertFalse(isNumber)

    def testAccessThreshold(self):
        self.fail("Not implemented")

    def testChangeUpperThreshold(self):
        self.fail("Not implemented")

    def testChangeLowerThreshold(self):
        self.fail("Not implemented")

    @classmethod
    def TestCase(cls):
        if cls.isPdhProduct():
            return PdhExcessiveErrorRatioTest
        if cls.isMroProduct():
            return ExcessiveErrorRatioTestExcessiveErrorRatio

        assert 0

class ExcessiveErrorRatioTestExcessiveErrorRatio(ExcessiveErrorRatioAbstractTest):
    def testAccessThreshold(self):
        self.TestAccessThreshold(self.ethPortCliIdString(), supported=True)

    def TestChangeThreshold(self, portCliId, maxValue, threshodName):
        """
        :type threshodName: str
        """
        for value in random.sample(range(maxValue + 1), 10):
            cli = "ciena eth port threshold excessive_error_ratio %s %d %d" % (threshodName.lower(), portCliId, value)
            self.assertCliSuccess(cli)
            self.assertEqual(self.getThresholdAttribute(portCliId, threshodName), value)

    def TestCannotChangeOutOfRangeThreshold(self, portCliId, maxValue, threshodName):
        """
        :type threshodName: str
        """
        cli = "ciena eth port threshold excessive_error_ratio %s %d %d" % (threshodName.lower(), portCliId, maxValue + 1)
        self.assertCliFailure(cli)

    def maxUpper(self, portCliId):
        return self.getThresholdAttribute(portCliId, "UpperMax")

    def maxLower(self, portCliId):
        return self.getThresholdAttribute(portCliId, "LowerMax")

    def testChangeUpperThreshold(self):
        for portCliId in self.ethPortCliIds():
            self.TestChangeThreshold(portCliId, self.maxUpper(portCliId), "Upper")

    def testCannotChangeOutOfRangeUpperThreshold(self):
        for portCliId in self.ethPortCliIds():
            self.TestCannotChangeOutOfRangeThreshold(portCliId=portCliId, maxValue=self.maxUpper(portCliId), threshodName="Upper")

    def testChangeLowerThreshold(self):
        for portCliId in self.ethPortCliIds():
            self.TestChangeThreshold(portCliId, self.maxLower(portCliId), "Lower")

    def testCannotChangeOutOfRangeLowerThreshold(self):
        for portCliId in self.ethPortCliIds():
            self.TestCannotChangeOutOfRangeThreshold(portCliId, maxValue=self.maxLower(portCliId), threshodName="Lower")

    @staticmethod
    def defaultThreshold(serdesMode):
        thresholds = {"1G":(12, 1),
                      "10G":(6, 1)}
        return thresholds[serdesMode]

    def testChangeEthModeMustHaveThresholdUpdated(self):
        for mode in ["1G", "10G", "1G"]:
            for serdesCliId in self.serdesCliIds():
                self.assertCliSuccess("serdes mode %s %s" % (self.serdesCliIdString(), mode))
                ethPortCliId = self.ethPortCliIdFromSerdesCliId(serdesCliId)
                result = self.runCli("show ciena eth port threshold excessive_error_ratio %d" % ethPortCliId)
                threshold = self.defaultThreshold(mode)
                self.assertEqual(self.getThresholdAttribute(ethPortCliId, "Upper"), threshold[0])
                self.assertEqual(self.getThresholdAttribute(ethPortCliId, "Lower"), threshold[1])


class ExcessiveErrorRatioTest_NotSupportExcessiveErrorRatio(ExcessiveErrorRatioAbstractTest):
    @staticmethod
    def ethPortCliIds():
        return range(1, 100)

    def testAccessThreshold(self):
        for portCliId in self.ethPortCliIds():
            self.TestAccessThreshold(portCliId=portCliId, supported=False)

    def testChangeUpperThreshold(self):
        for portCliId in self.ethPortCliIds():
            cli = "ciena eth port threshold excessive_error_ratio upper %d 10" % portCliId
            self.assertCliFailure(cli)

    def testChangeLowerThreshold(self):
        for portCliId in self.ethPortCliIds():
            cli = "ciena eth port threshold excessive_error_ratio lower %d 10" % portCliId
            self.assertCliFailure(cli)

class PdhExcessiveErrorRatioTest(ExcessiveErrorRatioTest_NotSupportExcessiveErrorRatio):
    def setupSerdesMode(self):
        pass

    @classmethod
    def canRun(cls):
        return cls.isPdhProduct()

    @staticmethod
    def ethPortCliIds():
        return range(1, 7)

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(ExcessiveErrorRatioAbstractTest.TestCase())
    runner.summary()

if __name__ == '__main__':
    TestMain()

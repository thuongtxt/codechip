import getopt
import sys
import os

from test.AtTestCase import AtTestCaseRunner

class AnnaRunner(AtTestCaseRunner):
    @classmethod
    def shouldRunFile(cls, filePath):
        excludedScriptNames = ["AnnaWarmRestore.py", "AnnaConcateTest.py"]
        for fileName in excludedScriptNames:
            if fileName in filePath:
                return False

        return True

def Main():
    def printUsage():
        print "Usage: %s [--help] [--list-file-only]" % sys.argv[0]

    try:
        opts, args = getopt.getopt(sys.argv[1:], "", ["help", "list-file-only"])
    except getopt.GetoptError:
        printUsage()
        return 1

    listFilesOnly = False

    for opt, arg in opts:
        if opt == '--help':
            printUsage()
            return 0

        elif opt == "--list-file-only":
            listFilesOnly = True

    AnnaRunner.runDirectory(os.path.dirname(__file__), listFilesOnly=listFilesOnly)

if __name__ == '__main__':
    Main()

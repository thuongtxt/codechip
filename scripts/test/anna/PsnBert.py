from python.arrive.atsdk.cli import AtCliRunnable
from test.AtTestCase import AtTestCase, AtTestCaseRunner
from test.anna.AnnaTestCase import AnnaProductCodeProvider
from utils.audit import RegisterCollector


class ConfigureProvider(AtCliRunnable):
    def __init__(self, productCode):
        self.productCode = productCode

    @classmethod
    def runCli(cls, cli):
        result = AtCliRunnable.runCli(cli)
        assert result.success()
        return result

    @classmethod
    def deviceInit(cls):
        cls.runCli("device init")

    @classmethod
    def provision(cls):
        pass

    @classmethod
    def setupPw(cls):
        cls.runCli("pw psn 1 mpls")
        cls.runCli("pw ethport 1 1")
        cls.runCli("pw enable 1")

    @classmethod
    def setup(cls):
        cls.deviceInit()
        cls.provision()

    def localLoopbackCanBeMadeWithNoPwAssociation(self):
        return True

    def localLoopbackMode(self):
        return "local"

    def remoteLoopbackMode(self):
        return "remote"

    def remoteLoopbackSupported(self):
        return True

    def currentLoopback(self):
        return None

    def bindPw(self):
        assert 0

    def loopback(self, mode):
        assert 0

    @classmethod
    def mroProviders(cls, productCode):
        return [VcDe1PsnBertProvider(productCode),
                PtsMroVc1xPsnBertProvider(productCode),
                PtsMroHoVcBertProvider(productCode),
                PtsMroLoVc3BertProvider(productCode),
                VcDe1NxDs0PsnBertProvider(productCode)]

    @staticmethod
    def pdhProviders(productCode):
        return [PtsPdhDe1PsnBertProvider(productCode),
                PtsPdhVc1xPsnBertProvider(productCode),
                PtsPdhHoVcBertProvider(productCode),
                PtsPdhVcDe1NxDs0PsnBertProvider(productCode)]

    @classmethod
    def providers(cls, productCode):
        if AnnaProductCodeProvider.isMroProduct(productCode):
            return cls.mroProviders(productCode)

        if AnnaProductCodeProvider.isPdhProduct(productCode):
            return cls.pdhProviders(productCode)

        return list()

class PdhChannelConfigureProvider(ConfigureProvider):
    def localLoopbackMode(self):
        return "lineloopin"

    def remoteLoopbackMode(self):
        return "lineloopout"


class De1PsnBertProvider(PdhChannelConfigureProvider):
    def de1CliId(self):
        return "Invalid"

    def currentLoopback(self):
        result = self.runCli("show pdh de1 %s" % self.de1CliId())
        return result.cellContent(0, "LoopbackMode")

    def loopback(self, mode):
        self.runCli("pdh de1 loopback %s %s" % (self.de1CliId(), mode))

    def bindPw(self):
        self.runCli("pw circuit bind 1 de1.%s" % self.de1CliId())

class VcDe1PsnBertProvider(De1PsnBertProvider):
    @classmethod
    def vc3CliId(cls):
        return "25.1.1"

    def de1CliId(self):
        return "%s.1.1" % self.vc3CliId()

    @classmethod
    def provision(cls):
        vc3CliId = cls.vc3CliId()
        cls.runCli("sdh map vc3.%s 7xtug2s" % vc3CliId)
        cls.runCli("sdh map tug2.%s.1 tu11" % vc3CliId)
        cls.runCli("sdh map vc1x.%s.1.1 de1" % vc3CliId)
        cls.runCli("prbs engine create de1 1 %s.1.1" % vc3CliId)
        cls.runCli("pw create satop 1")
        cls.runCli("pw circuit bind 1 de1.%s.1.1" % vc3CliId)
        cls.setupPw()


class PtsPdhDe1PsnBertProvider(De1PsnBertProvider):
    def de1CliId(self):
        return "1"

    @classmethod
    def provision(cls):
        cls.runCli("ciena pdh interface de1")
        cls.runCli("prbs engine create de1 1 1")
        cls.runCli("pw create satop 1")
        cls.runCli("pw circuit bind 1 de1.1")
        cls.setupPw()


class VcDe1NxDs0PsnBertProvider(VcDe1PsnBertProvider):
    @classmethod
    def provision(cls):
        vc3CliId = cls.vc3CliId()
        cls.runCli("sdh map vc3.%s 7xtug2s" % vc3CliId)
        cls.runCli("sdh map tug2.%s.1 tu11" % vc3CliId)
        cls.runCli("sdh map vc1x.%s.1.1 de1" % vc3CliId)
        cls.runCli("pdh de1 framing %s.1.1 ds1_sf" % vc3CliId)
        cls.runCli("pdh de1 nxds0create %s.1.1 0-3" % vc3CliId)
        cls.runCli("prbs engine create nxds0 1 %s.1.1 0-3" % vc3CliId)
        cls.runCli("pw create cesop 1 basic")
        cls.runCli("pw circuit bind 1 nxds0.%s.1.1.0-3" % vc3CliId)
        cls.setupPw()

    def bindPw(self):
        self.runCli("pw circuit bind 1 nxds0.%s.1.1.0-3" % self.vc3CliId())


class PtsPdhVcDe1NxDs0PsnBertProvider(VcDe1NxDs0PsnBertProvider):
    @classmethod
    def vc3CliId(cls):
        return "1"

class De3PsnBertProvider(PdhChannelConfigureProvider):
    @classmethod
    def provision(cls):
        cls.runCli("sdh map vc3.25.1.1 de3")
        cls.runCli("prbs engine create de3 1 25.1.1")
        cls.runCli("pw create satop 1")
        cls.runCli("pw circuit bind 1 de3.25.1.13")
        cls.setupPw()

    def currentLoopback(self):
        result = self.runCli("show pdh de3 25.1.1")
        return result.cellContent(0, "LoopbackMode")

    def loopback(self, mode):
        self.runCli("pdh de1 loopback 25.1.1 %s" % mode)

    def bindPw(self):
        self.runCli("pw circuit bind 1 de3.25.1.13")


class VcBertProvider(ConfigureProvider):
    def pathCliId(self):
        return None

    def currentLoopback(self):
        result = self.runCli("show sdh path loopback %s" % self.pathCliId())
        return result.cellContent(0, "Loopback")

    def loopback(self, mode):
        self.runCli("sdh path loopback %s %s" % (self.pathCliId(), mode))


class Vc1xPsnBertProvider(VcBertProvider):
    def localLoopbackCanBeMadeWithNoPwAssociation(self):
        if AnnaProductCodeProvider.isMro10G(self.productCode):
            return False
        return True

    @classmethod
    def vc3CliId(cls):
        return "Invalid"

    @classmethod
    def provision(cls):
        cls.runCli("sdh map vc3.%s 7xtug2s" % cls.vc3CliId())
        cls.runCli("sdh map tug2.%s.1 tu11" % cls.vc3CliId())
        cls.runCli("sdh map vc1x.%s.1.1 c1x" % cls.vc3CliId())
        cls.runCli("prbs engine create vc 1 vc1x.%s.1.1" % cls.vc3CliId())
        cls.runCli("pw create cep 1 basic")
        cls.runCli("pw circuit bind 1 vc1x.%s.1.1" % cls.vc3CliId())
        cls.setupPw()

    def pathCliId(self):
        return "vc1x.%s.1.1" % self.vc3CliId()

    def bindPw(self):
        self.runCli("pw circuit bind 1 vc1x.%s.1.1" % self.vc3CliId())


class PtsMroVc1xPsnBertProvider(Vc1xPsnBertProvider):
    @classmethod
    def vc3CliId(cls):
        return "25.1.1"


class PtsPdhVc1xPsnBertProvider(Vc1xPsnBertProvider):
    @classmethod
    def vc3CliId(cls):
        return "1"


class HoVcBertProvider(VcBertProvider):
    @classmethod
    def vc3CliId(cls):
        return "Invalid"

    @classmethod
    def provision(cls):
        cls.runCli("sdh map vc3.%s c3" % cls.vc3CliId())
        cls.runCli("prbs engine create vc 1 vc3.%s" % cls.vc3CliId())
        cls.runCli("pw create cep 1 basic")
        cls.runCli("pw circuit bind 1 vc3.%s" % cls.vc3CliId())
        cls.setupPw()

    def remoteLoopbackSupported(self):
        return False

    def pathCliId(self):
        return "vc3.%s" % self.vc3CliId()

    def bindPw(self):
        self.runCli("pw circuit bind 1 vc3.%s" % self.vc3CliId())

class PtsMroHoVcBertProvider(HoVcBertProvider):
    @classmethod
    def vc3CliId(cls):
        return "25.1.1"

class PtsPdhHoVcBertProvider(HoVcBertProvider):
    @classmethod
    def vc3CliId(cls):
        return "1"

class PtsMroLoVc3BertProvider(VcBertProvider):
    @classmethod
    def provision(cls):
        cls.runCli("sdh map aug1.25.1 vc4")
        cls.runCli("sdh map vc4.25.1 3xtug3s")
        cls.runCli("sdh map tug3.25.1.1 vc3")
        cls.runCli("sdh map vc3.25.1.1 c3")
        cls.runCli("prbs engine create vc 1 vc3.25.1.1")
        cls.runCli("pw create cep 1 basic")
        cls.runCli("pw circuit bind 1 vc3.25.1.1")
        cls.setupPw()

    def pathCliId(self):
        return "vc3.25.1.1"

    def bindPw(self):
        self.runCli("pw circuit bind 1 vc3.25.1.1")

    def localLoopbackCanBeMadeWithNoPwAssociation(self):
        if AnnaProductCodeProvider.isMro10G(self.productCode):
            return False
        return True


class PsnBert(AtTestCase):
    _provider = None

    @classmethod
    def setProvider(cls, provider):
        """
        :type provider: ConfigureProvider
        """
        cls._provider = provider

    @classmethod
    def provider(cls):
        """
        :rtype: ConfigureProvider
        """
        return cls._provider

    def setUp(self):
        self.assertIsNotNone(self.provider(), "Provider must be configured")

        self.bindPw()
        self.genToTdm()
        self.monFromTdm()
        self.disableTx()
        self.disableRx()
        self.releaseLoopback()

    @classmethod
    def setupPw(cls):
        cls.runCli("pw psn 1 mpls")
        cls.runCli("pw ethport 1 1")
        cls.runCli("pw enable 1")

    @classmethod
    def setUpClass(cls):
        cls.provider().setup()

    def engineAttribute(self, name):
        result = self.runCli("show prbs engine 1")
        return result.cellContent(0, name)

    def assertPsnGenSide(self):
        self.assertEqual(self.engineAttribute("genSide"), "psn")

    def assertPsnMonSide(self):
        self.assertEqual(self.engineAttribute("monSide"), "psn")

    def assertTdmGenSide(self):
        self.assertEqual(self.engineAttribute("genSide"), "tdm")

    def assertTdmMonSide(self):
        self.assertEqual(self.engineAttribute("monSide"), "tdm")

    def assertLocalLoopback(self):
        self.assertEqual(self.provider().currentLoopback(), self.provider().localLoopbackMode())

    def assertRemoteLoopback(self):
        self.assertEqual(self.provider().currentLoopback(), self.provider().remoteLoopbackMode())

    def assertLoopbackRelease(self):
        self.assertEqual(self.provider().currentLoopback(), "release")

    def assertTxEnabled(self):
        self.assertEqual(self.engineAttribute("txEnabled"), "en")

    def assertTxDisabled(self):
        self.assertEqual(self.engineAttribute("txEnabled"), "dis")

    def genToPsn(self):
        self.assertCliSuccess("prbs engine side generating 1 psn")

    def monFromPsn(self):
        self.assertCliSuccess("prbs engine side monitoring 1 psn")

    def genToTdm(self):
        self.assertCliSuccess("prbs engine side generating 1 tdm")

    def monFromTdm(self):
        self.assertCliSuccess("prbs engine side monitoring 1 tdm")

    def enableTx(self):
        self.assertCliSuccess("prbs engine tx enable 1")

    def disableTx(self):
        self.assertCliSuccess("prbs engine tx disable 1")

    def enableRx(self):
        self.assertCliSuccess("prbs engine rx enable 1")

    def assertRxEnabled(self, enabled):
        value = self.engineAttribute("rxEnabled")
        expected = "en" if enabled else "dis"
        self.assertEqual(value, expected)

    def disableRx(self):
        self.assertCliSuccess("prbs engine rx disable 1")

    def localLoopback(self):
        self.provider().loopback(self.provider().localLoopbackMode())

    def remoteLoopback(self):
        self.provider().loopback(self.provider().remoteLoopbackMode())

    def releaseLoopback(self):
        self.provider().loopback("release")

    def unbindPw(self):
        self.assertCliSuccess("pw circuit bind 1 none")

    def bindPw(self):
        self.provider().bindPw()

    def _enablePw(self, enable):
        self.assertCliSuccess("pw %s 1" % ("enable" if enable else "disable"))

    def enablePw(self):
        self._enablePw(True)

    def disablePw(self):
        self._enablePw(False)

    def assertPrbsLossSync(self):
        alarms = self.engineAttribute("alarm")
        self.assertTrue("lossSync" in alarms)

    def collectActivatedPsnMonConfiguration(self):
        regCollector = RegisterCollector()
        provider = self.provider()
        provider.deviceInit()

        regCollector.start()
        provider.provision()
        self.enableRx()
        self.monFromPsn()
        self.runCli("show pw psn 1") # Just a hack to have pla_out_psnpro_ctrl be sync
        regCollector.stop()

        return regCollector

    def collectDeactivatedPsnMonConfiguration(self):
        regCollector = RegisterCollector()
        provider = self.provider()
        provider.deviceInit()

        regCollector.start()
        provider.provision()
        self.monFromPsn()
        self.enableRx()
        self.disableRx()  # Just a trick to have logged register access
        self.unbindPw()
        regCollector.stop()

        return regCollector

    def testEnableThenSetPsnSide(self):
        self.enableTx()
        self.assertTxEnabled()

        self.genToPsn()
        self.assertPsnGenSide()
        self.assertLocalLoopback()
        self.assertTxEnabled()

    def testEnableThenChangeGenSide(self):
        self.enableTx()
        self.assertTxEnabled()

        self.genToPsn()
        self.assertLocalLoopback()

        self.genToTdm()
        self.assertTdmGenSide()
        self.assertLoopbackRelease()
        self.assertTxEnabled()

    def testChangeSideWhenDisabling(self):
        self.disableTx()
        self.assertTxDisabled()

        self.genToPsn()
        self.assertPsnGenSide()
        self.assertLoopbackRelease()
        self.assertTxDisabled()

        self.genToTdm()
        self.assertTdmGenSide()
        self.assertLoopbackRelease()
        self.assertTxDisabled()

    def testSetPsnSideThenEnabling(self):
        self.genToPsn()
        self.assertPsnGenSide()
        self.assertTxDisabled()
        self.assertLoopbackRelease()

        self.enableTx()
        self.assertPsnGenSide()
        self.assertTxEnabled()
        self.assertLocalLoopback()

        self.disableTx()
        self.assertPsnGenSide()
        self.assertTxDisabled()
        self.assertLoopbackRelease()

    def testLocalLoopbackBeforeGenPsn(self):
        self.localLoopback()
        self.assertLocalLoopback()

        self.genToPsn()
        self.assertTxDisabled()
        self.enableTx()
        self.assertTxEnabled()
        self.assertLocalLoopback()

        self.disableTx()
        self.assertLocalLoopback()
        self.assertTxDisabled()

    def testRemoteLoopbackBeforeGenPsn(self):
        if not self.provider().remoteLoopbackSupported():
            self.skipTest("Remote loopback is not supported")

        self.remoteLoopback()
        self.assertRemoteLoopback()

        self.genToPsn()
        self.assertTxDisabled()
        self.enableTx()
        self.assertTxEnabled()
        self.assertLocalLoopback()

        self.disableTx()
        self.assertRemoteLoopback()
        self.assertTxDisabled()

    def testGenPsnBeforeLoopback(self):
        self.genToPsn()
        self.enableTx()
        self.assertLocalLoopback()
        self.assertTxEnabled()

        self.localLoopback()
        self.assertTxEnabled()

        if self.provider().remoteLoopbackSupported():
            self.remoteLoopback()
            self.assertTxDisabled()
            self.assertPsnGenSide()

    def testMonPsnWhenNoPw(self):
        regCollector = self.collectActivatedPsnMonConfiguration()

        # As configuration is mess because of the step that collect register configuration, so need to setup again
        self.provider().setup()

        # Make sure that there is no associated PW, then provision PSN side
        self.unbindPw()
        self.monFromPsn()
        self.enableRx()
        self.assertRxEnabled(enabled=True)
        self.assertPsnMonSide()
        self.assertPrbsLossSync()

        # Now provision PW, configuration must be right
        self.bindPw()
        self.assertRxEnabled(enabled=True)
        self.assertPsnMonSide()
        self.assertTrue(regCollector.audit())

    def testWhenUnbindingPw_ConfigurationMustBeDeactivated(self):
        regCollector = self.collectDeactivatedPsnMonConfiguration()

        # As configuration is mess because of the step that collect register configuration, so need to setup again
        self.provider().setup()

        # Provision PSN monitoring side
        self.monFromPsn()
        self.enableRx()

        # Deprovision it
        self.unbindPw()
        self.assertPsnMonSide()
        self.assertRxEnabled(enabled=True)
        self.assertPrbsLossSync()
        self.assertTrue(regCollector.audit())

    def testPsnSideWillNotBeChangedWhenPwUnbindingOrDisable(self):
        self.genToPsn()
        self.monFromPsn()

        self.disablePw()
        self.assertPsnGenSide()
        self.assertPsnMonSide()

        self.unbindPw()
        if self.provider().localLoopbackCanBeMadeWithNoPwAssociation():
            self.assertPsnGenSide()
        self.assertPsnMonSide()

    def testGenPsnWhenNoPw(self):
        if not self.provider().localLoopbackCanBeMadeWithNoPwAssociation():
            self.skipTest("Local loopback cannot be made when PW has not been provisioned, skipping test now")

        self.unbindPw()
        self.genToPsn()
        self.enableTx()
        self.assertPsnGenSide()
        self.assertLocalLoopback()

        self.bindPw()
        self.assertPsnGenSide()
        self.assertLocalLoopback()

    @classmethod
    def runWithRunner(cls, runner):
        """
        :type runner: AtTestCaseRunner
        """
        providers = ConfigureProvider.providers(cls.productCode())
        for provider in providers:
            cls.setProvider(provider)
            runner.run(cls)

def TestMain():
    runner = AtTestCaseRunner.runner()
    PsnBert.runWithRunner(runner)
    runner.summary()

if __name__ == '__main__':
    TestMain()
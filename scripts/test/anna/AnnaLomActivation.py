"""
Created on Mar 4, 2017

@author: namnng
"""
from AnnaTestCase import AnnaMroTestCase
from python.arrive.atsdk.AtRegister import cBit14
from test.AtTestCase import AtTestCaseRunner

class AnnaLomActivation(AnnaMroTestCase):
    @classmethod
    def lineSideChannel(cls):
        return None
    
    @classmethod
    def terminatedChannelizedVc1xVc(cls):
        return None
    
    @classmethod
    def terminatedUnchannelizedVc1xVc(cls):
        return None
    
    def channelizeToTu1x(self):
        assert 0
        
    def unchannelizeTu1x(self):
        assert 0
    
    @staticmethod
    def ocnBase():
        return 0x100000
    
    def lomAddress(self):
        return 0
    
    @staticmethod
    def lomBit():
        return cBit14
    
    def lomIsEnabled(self):
        address = self.lomAddress() + self.ocnBase()
        regValue = self.rd(address)
        if regValue & self.lomBit():
            return True
        return False
        
    def connect(self, terminatedVc):
        self.assertCliSuccess("xc vc connect %s %s" % (self.lineSideChannel(), terminatedVc))
    
    def disconnect(self, terminatedVc):
        self.assertCliSuccess("xc vc disconnect %s %s" % (self.lineSideChannel(), terminatedVc))
    
    def testXcConnectDisconnect(self):
        self.connect(self.terminatedChannelizedVc1xVc())
        self.assertTrue(self.lomIsEnabled(), None)
        self.disconnect(self.terminatedChannelizedVc1xVc())
        self.assertFalse(self.lomIsEnabled(), None)
        
        self.connect(self.terminatedUnchannelizedVc1xVc())
        self.assertFalse(self.lomIsEnabled(), None)
        self.disconnect(self.terminatedUnchannelizedVc1xVc())
        self.assertFalse(self.lomIsEnabled(), None)
        
    def testMappingChange(self):
        self.connect(self.terminatedChannelizedVc1xVc())
        self.assertTrue(self.lomIsEnabled(), None)
        self.unchannelizeTu1x()
        self.assertFalse(self.lomIsEnabled(), None)
        self.channelizeToTu1x()
        self.assertTrue(self.lomIsEnabled(), None)

    def remoteLoopback(self, release = False):
        loopback = "release" if release else "remote"
        self.assertCliSuccess("sdh path loopback %s %s" % (self.lineSideChannel(), loopback))

    def testLomDetectionShouldBeDisabledOnLoopback(self):
        self.connect(self.terminatedChannelizedVc1xVc())
        self.assertTrue(self.lomIsEnabled(), None)
        self.remoteLoopback()
        self.assertFalse(self.lomIsEnabled(), None)
        self.remoteLoopback(release = True)
        self.assertFalse(self.lomIsEnabled(), None)
    
    def faceplateLomAddress(self):
        return 0x22000
    
    def mateLomAddress(self):
        return 0x02000
    
class AnnaAu3LomActivation(AnnaLomActivation):
    @classmethod
    def terminatedChannelizedVc1xVc(cls):
        return "vc3.25.1.1"
    
    @classmethod
    def terminatedUnchannelizedVc1xVc(cls):
        return "vc3.25.1.2"
    
    def setUp(self):
        self.assertCliSuccess("device init")
        self.assertCliSuccess("sdh map %s c3" % self.lineSideChannel())
        self.assertCliSuccess("sdh map %s 7xtug2s" % self.terminatedChannelizedVc1xVc())
        self.assertCliSuccess("sdh map %s c3" % self.terminatedUnchannelizedVc1xVc())
        
    def channelizeToTu1x(self):
        self.assertCliSuccess("sdh map %s 7xtug2s" % self.terminatedChannelizedVc1xVc())
        
    def unchannelizeTu1x(self):
        self.assertCliSuccess("sdh map %s c3" % self.terminatedChannelizedVc1xVc())
        
class AnnaAu3LomActivationFaceplate(AnnaAu3LomActivation):
    @classmethod
    def lineSideChannel(cls):
        return "vc3.1.1.1"
    
    def lomAddress(self):
        return self.faceplateLomAddress()
    
class AnnaAu3LomActivationMate(AnnaAu3LomActivation):
    @classmethod
    def lineSideChannel(cls):
        return "vc3.17.1.1"
    
    def lomAddress(self):
        return self.mateLomAddress()

class AnnaAu4LomActivation(AnnaLomActivation):
    @classmethod
    def terminatedChannelizedVc1xVc(cls):
        return "vc4.25.1"
    
    @classmethod
    def terminatedUnchannelizedVc1xVc(cls):
        return "vc4.25.2"
    
    def lineSideAug1Id(self):
        vcId = self.lineSideChannel()
        tokens = vcId.split(".")[1:]
        return "aug1.%s" % (".".join(tokens))
    
    def setUp(self):
        self.assertCliSuccess("device init")
        self.assertCliSuccess("sdh map %s,25.1,25.2 vc4" % self.lineSideAug1Id())
        self.assertCliSuccess("sdh map vc4.25.1,25.2 3xtug3s")
        self.assertCliSuccess("sdh map tug3.25.1.1 7xtug2s")
    
    def channelizeToTu1x(self):
        self.assertCliSuccess("sdh map tug3.25.1.1 7xtug2s")
        
    def unchannelizeTu1x(self):
        self.assertCliSuccess("sdh map tug3.25.1.1 vc3")
    
class AnnaAu4LomActivationFaceplate(AnnaAu4LomActivation):
    @classmethod
    def lineSideChannel(cls):
        return "vc4.1.1"
    
    def lomAddress(self):
        return self.faceplateLomAddress()
    
class AnnaAu4LomActivationMate(AnnaAu4LomActivation):
    @classmethod
    def lineSideChannel(cls):
        return "vc4.17.1"
    
    def lomAddress(self):
        return self.mateLomAddress()
        
def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(AnnaAu3LomActivationFaceplate)
    runner.run(AnnaAu3LomActivationMate)
    runner.run(AnnaAu4LomActivationFaceplate)
    runner.run(AnnaAu4LomActivationMate)
    runner.summary()
    
if __name__ == '__main__':
    TestMain()
    
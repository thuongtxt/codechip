from test.AtTestCase import AtTestCaseRunner
from test.anna.AnnaTestCase import AnnaMroTestCase


class BertStatusInDisabledState(AnnaMroTestCase):
    @classmethod
    def canRun(cls):
        return False if cls.is20G() else AnnaMroTestCase.canRun()

    @classmethod
    def engineCliId(cls):
        return 1

    def makeErrorAndAlarm(self):
        assert 0

    def enableEngine(self, enabled=True):
        self.assertCliSuccess("prbs engine %s %d" % ("enable" if enabled else "disable", self.engineCliId()))

    def setUp(self):
        self.makeErrorAndAlarm()
        self.enableEngine(enabled=False)

    def testEngineCliIdMustMatch(self): # Just to enforce developer make change to makeErrorAndAlarm method for correct address
        self.assertEqual(self.engineCliId(), 1)

    def getCounters(self):
        """
        :rtype: python.arrive.AtAppManager.AtCliResult.AtCliResult
        """
        return self.runCli("show prbs engine counters %d r2c" % self.engineCliId())

    def testWhenBertIsInDisabledState_CountersMustBeReturnedAsZero(self):
        def assertCounter(passFunction):
            for column in range(2, result.numColumns()):
                counterString = result.cellContent(0, column)
                if counterString in ["xxx", "N/S", "N/A"]:
                    continue

                self.assertTrue(passFunction(int(counterString)))

        def counterIsZero(counterValue):
            return counterValue == 0

        def counterIsNotZero(counterValue):
            return counterValue > 0

        result = self.getCounters()
        assertCounter(counterIsZero)

        self.enableEngine()
        result = self.getCounters()
        assertCounter(counterIsNotZero)

    def getAlarm(self):
        result = self.runCli("show prbs engine %d" % self.engineCliId())
        return result.cellContent(0, "alarm").lower()

    def getSticky(self):
        result = self.runCli("show prbs engine %d" % self.engineCliId())
        return result.cellContent(0, "sticky").lower()

    def testWhenBertIsInDisabledState_AlarmsMustBeReturnedAsZero(self):
        self.assertEqual(self.getAlarm(), "none")
        self.enableEngine()
        self.assertNotEqual(self.getAlarm(), "none")

    def testWhenBertIsInDisabledState_HistoryMustBeReturnedAsZero(self):
        self.assertEqual(self.getSticky(), "none")
        self.enableEngine()
        self.assertNotEqual(self.getSticky(), "none")

class HoBertStatusInDisabledState(BertStatusInDisabledState):
    @classmethod
    def setUpClass(cls):
        cls.runCli("device init")
        cls.runCli("sdh map vc3.25.1.1 c3")
        cls.runCli("prbs engine create vc %d vc3.25.1.1" % cls.engineCliId())

    def makeErrorAndAlarm(self):
        # Sticky
        self.assertCliSuccess("wr 0x0006851f 1 bit0")

        # Alarm (3 for sync, #3 for loss sync)
        self.assertCliSuccess("wr 0x00068500 1 bit1_0")

        # Counters
        self.assertCliSuccess("wr 0x00068380 0x10")
        self.assertCliSuccess("wr 0x00068540 0x10")
        self.assertCliSuccess("wr 0x00068580 0x10")
        self.assertCliSuccess("wr 0x000685c0 0x10")

class De1BertStatusInDisabledState(BertStatusInDisabledState):
    @classmethod
    def setUpClass(cls):
        cls.runCli("device init")
        cls.runCli("sdh map vc3.25.1.1 7xtug2s")
        cls.runCli("sdh map tug2.25.1.1.1 tu11")
        cls.runCli("sdh map vc1x.25.1.1.1.1 de1")
        cls.runCli("prbs engine create de1 %d 25.1.1.1.1" % cls.engineCliId())

    def makeErrorAndAlarm(self):
        # Sticky
        self.assertCliSuccess("wr 0x00368402 1 bit0")

        # Alarm (3 for sync, #3 for loss sync)
        self.assertCliSuccess("wr 0x003684e0 1 bit1_0")

        # Counters
        self.assertCliSuccess("wr 0x00358380 0x10")
        self.assertCliSuccess("wr 0x00368540 0x10")
        self.assertCliSuccess("wr 0x00368580 0x10")
        self.assertCliSuccess("wr 0x003685c0 0x10")

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(HoBertStatusInDisabledState)
    runner.run(De1BertStatusInDisabledState)
    runner.summary()

if __name__ == '__main__':
    TestMain()

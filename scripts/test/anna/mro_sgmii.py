import random

from python.arrive.atsdk.platform import AtHalListener
from python.registers.Tha60290022.RegisterProviderFactory import RegisterProviderFactory
from test.AtTestCase import AtTestCaseRunner
from test.anna.AnnaTestCase import AnnaMroTestCase


class KByteSgmiiTest(AnnaMroTestCase):
    @classmethod
    def canRun(cls):
        return cls.is20G()

    @classmethod
    def setUpClass(cls):
        cls.assertClassCliSuccess("device init")

    def setUp(self):
        self.setLoopback("release")

    @staticmethod
    def portCliId():
        return "18"

    def setLoopback(self, mode):
        self.assertCliSuccess("eth port loopback %s %s" % (self.portCliId(), mode))

    def getPortAtribute(self, attributeName):
        result = self.runCli("show eth port %s" % self.portCliId())
        return result.cellContent(attributeName, columnIdOrName=1)

    def getLoopback(self):
        return self.getPortAtribute("Loopback")

    def setIpg(self, value):
        self.assertCliSuccess("eth port txipg %s %d" % (self.portCliId(), value))
        self.assertCliSuccess("eth port rxipg %s %d" % (self.portCliId(), value))

    def getIpg(self):
        return int(self.getPortAtribute("Tx IPG"))

    @classmethod
    def registerProvider(cls, atregName):
        """
        :rtype: python.arrive.atsdk.AtRegister.AtRegisterProvider
        """
        return RegisterProviderFactory().providerByName(atregName)

    @classmethod
    def baseAddress(cls):
        return 0x00E1000

    @classmethod
    def defaultRegisterProvider(cls):
        """
        :rtype: python.arrive.atsdk.AtRegister.AtRegisterProvider
        """
        provider = cls.registerProvider("_AF6CNC0022_RD_DCCK_GMII")
        provider.baseAddress = cls.baseAddress()
        return provider

    def getRegister(self, regName, offset = 0):
        return self.defaultRegisterProvider().getRegister(regName, offset, readHw=True)

    def testLocalLoopback(self):
        self.setLoopback("local")
        reg = self.getRegister("dcckgmiicfg")
        reg.assertFieldEqual("DcckGmiiLoopIn", 1)
        self.setLoopback("release")
        reg.rd()
        reg.assertFieldEqual("DcckGmiiLoopIn", 0)

    def testRemoteLoopback(self):
        self.setLoopback("remote")
        reg = self.getRegister("dcckgmiicfg")
        reg.assertFieldEqual("DcckGmiiLoopOut", 1)
        self.setLoopback("release")
        reg.rd()
        reg.assertFieldEqual("DcckGmiiLoopOut", 0)

    def testIpg(self):
        for ipg in random.sample(range(4,16), 10):
            self.setIpg(ipg)
            self.assertEqual(self.getIpg(), ipg)
            reg = self.getRegister("dcckgmiicfg")
            reg.assertFieldEqual("DcckGmiiIpgNum", ipg)

    def testNewDiagLogicMustBeDisabledByDefault(self):
        reg = self.getRegister("dcckgmiidiagcfg")
        reg.setFieldValueByName("DcckGmiiDiagGenPkEn", 1)
        reg.setFieldValueByName("DcckGmiiDiagEn", 1)
        reg.apply()

        self.runCli("device init")
        reg.rd()
        reg.assertFieldEqual("DcckGmiiDiagGenPkEn", 0)
        reg.assertFieldEqual("DcckGmiiDiagEn", 0)

    def testDisplayDebugStatus(self):
        class HalListener(AtHalListener):
            def __init__(self, regProvider):
                """
                :type regProvider: python.arrive.atsdk.AtRegister.AtRegisterProvider
                """
                self.access = False
                self.regProvider = regProvider

            def didAccess(self, address):
                if address == self.regProvider.getRegister("dcckgmiistk", addressOffset=0, readHw=False).address:
                    self.access = True

            def didRead(self, address, value):
                self.didAccess(address)

            def didWrite(self, address, value):
                self.didAccess(address)

        # Just make sure that debug register is accessed
        listener = HalListener(self.defaultRegisterProvider())
        AtHalListener.addListener(listener)
        self.runCli("debug eth port %s" % self.portCliId())
        self.assertTrue(listener.access)
        AtHalListener.removeListener(listener)
        del listener

    def testAccessCounters(self):
        class HalListener(AtHalListener):
            def __init__(self, regProvider):
                """
                :type regProvider: python.arrive.atsdk.AtRegister.AtRegisterProvider
                """
                self.regProvider = regProvider
                self.accessInfo = {regProvider.getRegister("dcckgmiitxpktcnt"   , addressOffset=0, readHw=False).address: False,
                                   regProvider.getRegister("dcckgmiitxbytecnt"  , addressOffset=0, readHw=False).address: False,
                                   regProvider.getRegister("dcckgmiitxfcserrcnt", addressOffset=0, readHw=False).address: False,
                                   regProvider.getRegister("dcckgmiirxpktcnt"   , addressOffset=0, readHw=False).address: False,
                                   regProvider.getRegister("dcckgmiirxbytecnt"  , addressOffset=0, readHw=False).address: False,
                                   regProvider.getRegister("dcckgmiirxfcserrcnt", addressOffset=0, readHw=False).address: False}
                
                assert len(self.accessInfo) == 6
                
            def didAccess(self, address):
                if address not in self.accessInfo:
                    return

                self.accessInfo[address] = True

            def didRead(self, address, value):
                self.didAccess(address)

            def didWrite(self, address, value):
                self.didAccess(address)

            def allCountersAccessed(self):
                for address, accessed in self.accessInfo.iteritems():
                    if not accessed:
                        print "Address 0x%x has not been accessed" % address
                        return False

                return True
            
        listener = HalListener(self.defaultRegisterProvider())
        AtHalListener.addListener(listener)
        result = self.runCli("show eth port counters %s r2c" % self.portCliId())
        self.assertGreater(result.numRows(), 0)
        self.assertTrue(listener.allCountersAccessed())
        AtHalListener.removeListener(listener)
        del listener

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(KByteSgmiiTest)
    runner.summary()

if __name__ == '__main__':
    TestMain()
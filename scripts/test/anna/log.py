from test.AtTestCase import AtTestCaseRunner
from test.anna.AnnaTestCase import AnnaMroTestCase


class ApiLog(AnnaMroTestCase):
    @classmethod
    def canRun(cls):
        return cls.is20G()

    def setUp(self):
        self.runCli("logger flush silence")
        self.runCli("logger api_log enable")
        self.runCli("textui stdout capture enable")
        self.runCli("textui stdout capture flush")

    @classmethod
    def tearDownClass(cls):
        cls.runCli("logger api_log disable")
        cls.runCli("logger flush silence")

    def showLogger(self):
        self.runCli("show logger")

    def assertTextInLogger(self, text):
        self.showLogger()
        self.assertTrue(text in self.getCapturedStdout())

    def testAtModuleApsUpsrEngineCreate(self):
        script = """
        device init
        aps upsr engine create 1 vc3.1.1.1 vc3.3.1.1
        """
        self.runCli(script)
        self.assertTextInLogger("API: AtModuleApsUpsrEngineCreate([aps], 0, vc3.1.1.1, vc3.3.1.1)")

    def testAtModuleApsSelectorCreate(self):
        script = """
        device init
        aps selector create 1 vc3.25.1.1 vc3.17.1.1 vc3.1.1.1
        """
        self.runCli(script)
        self.assertTextInLogger("API: AtModuleApsSelectorCreate([aps], 0, vc3.25.1.1, vc3.17.1.1, vc3.1.1.1)")

    def testAtModulePrbsDe1PrbsEngineCreate(self):
        script = """
        device init
        sdh map vc3.25.1.1 7xtug2s
        sdh map tug2.25.1.1.1 tu11
        sdh map vc1x.25.1.1.1.1 de1
        prbs engine create de1 1 25.1.1.1.1
        """
        self.runCli(script)
        self.assertTextInLogger("API: AtModulePrbsDe1PrbsEngineCreate([prbs], 0, [de1.25.1.1.1.1])")

    def testAtSdhChannelTxTtiSet(self):
        script = """
        device init
        sdh path tti transmit vc3.1.1.1 16bytes abcdef null_padding
        """
        self.runCli(script)
        self.assertTextInLogger("API: AtSdhChannelTxTtiSet([vc3.1.1.1], 61.62.63.64.65.66.00.00.00.00.00.00.00.00.00.00")

    def testAtSdhChannelConcate(self):
        script = """
        device init
        sdh map aug1.1.1-1.16 vc4
        sdh path concate au4.1.1 au4.1.2-1.8
        """
        self.runCli(script)
        self.assertTextInLogger("AtSdhChannelConcate([au4.1.1], [au4.1.2,au4.1.3,au4.1.4,au4.1.5,au4.1.6,au4.1.7,au4.1.8], 7)")

    def testAtPwEthHeaderSet(self):
        script = """
        device init
        pw create cep 1 basic
        pw ethport 1 1
        pw psn 1 mpls
        pw ethheader 1 C0.CA.C0.CA.C0.CA none none
        pw ethheader 1 C0.CA.C0.CA.C0.CA 3.1.2345 none
        pw ethheader 1 C0.CA.C0.CA.C0.CA 3.1.2345 4.0.2543
        """
        self.runCli(script)
        self.assertTextInLogger("API: AtPwEthHeaderSet([cep.1], c0.ca.c0.ca.c0.ca, 3.1.2345, none)")
        self.assertTextInLogger("API: AtPwEthHeaderSet([cep.1], c0.ca.c0.ca.c0.ca, 3.1.2345, 4.0.2543)")
        self.assertTextInLogger("API: AtPwEthHeaderSet([cep.1], c0.ca.c0.ca.c0.ca, none, none)")

    def testAtPwPsnSet_Mpls(self):
        script = """
        device init
        pw create cep 1 basic
        pw ethport 1 1
        pw psn 1 mpls
        """
        self.runCli(script)
        self.assertTextInLogger("API: AtPwPsnSet([cep.1], [MPLS(label.exp.ttl): Inner.1.1.255, Outters.none. Expected label: 1]")

    def testAtPwPsnSet_Mef(self):
        script = """
        device init
        pw create cep 1 basic
        pw ethport 1 1
        pw psn 1 mef
        """
        self.runCli(script)
        self.assertTextInLogger("API: AtPwPsnSet([cep.1], [MEF: TxEcId.1, ExpEcId.1])")

    def testAtPwEthExpectedCVlanSet_AtPwEthExpectedSVlanSet(self):
        script = """
        device init
        pw create cep 1 basic
        pw ethport 1 1
        pw psn 1 mef
        pw vlan expect 1 1.1.1 1.1.2
        """
        self.runCli(script)
        self.assertTextInLogger("API: AtPwEthExpectedCVlanSet([cep.1], 1.1.1)")
        self.assertTextInLogger("API: AtPwEthExpectedSVlanSet([cep.1], 1.1.2)")

    def testAtChannelTimingSet(self):
        script = """
        device init

        sdh map vc3.25.1.1 7xtug2s
        sdh map tug2.25.1.1.1 tu11
        sdh map vc1x.25.1.1.1.1 de1
        pdh de1 framing 25.1.1.1.1 ds1_sf
        pdh de1 nxds0create 25.1.1.1.1 1-3
        
        pw create cesop 1 basic
        pw ethport 1 1
        pw psn 1 mef
        pw circuit bind 1 nxds0.25.1.1.1.1.1-3
        
        pdh de1 timing 25.1.1.1.1 system xxx
        pdh de1 timing 25.1.1.1.1 loop xxx
        pdh de1 timing 25.1.1.1.1 acr 1
        """
        self.runCli(script)
        self.assertTextInLogger("API: AtChannelTimingSet([de1.25.1.1.1.1], 1, NULL)")
        self.assertTextInLogger("API: AtChannelTimingSet([de1.25.1.1.1.1], 2, NULL)")
        self.assertTextInLogger("API: AtChannelTimingSet([de1.25.1.1.1.1], 8, [cesop.1])")

    def testAtCrossConnectChannelConnect(self):
        script = """
        device init
        xc vc connect vc3.1.1.1 vc3.25.1.1 two-way
        """
        self.runCli(script)
        self.assertTextInLogger("API: AtCrossConnectChannelConnect([hovc_xc], vc3.1.1.1, vc3.25.1.1)")
        self.assertTextInLogger("API: AtCrossConnectChannelConnect([hovc_xc], vc3.25.1.1, vc3.1.1.1)")

    def testAtCrossConnectChannelDisconnect(self):
        script = """
        device init
        xc vc connect vc3.1.1.1 vc3.25.1.1 two-way
        xc vc disconnect vc3.1.1.1 vc3.25.1.1 two-way
        """
        self.runCli(script)
        self.assertTextInLogger("API: AtCrossConnectChannelDisconnect([hovc_xc], vc3.1.1.1, vc3.25.1.1)")
        self.assertTextInLogger("API: AtCrossConnectChannelDisconnect([hovc_xc], vc3.25.1.1, vc3.1.1.1)")

    def testAtBerController(self):
        script = """
        device init 
        sdh line ms ber enable 1
        sdh line ms ber sd 1 1e-4
        sdh line ms ber sf 1 1e-4
        """
        self.runCli(script)
        self.assertTextInLogger("API: AtBerControllerEnable([ber.faceplate.1], 1)")
        self.assertTextInLogger("API: AtBerControllerSdThresholdSet([ber.faceplate.1], 3)")
        self.assertTextInLogger("API: AtBerControllerSfThresholdSet([ber.faceplate.1], 3)")

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(ApiLog)
    runner.summary()

if __name__ == '__main__':
    TestMain()

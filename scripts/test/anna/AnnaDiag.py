"""
Created on May 11, 2017

@author: namnn
"""
import time

from python.arrive.AtAppManager.AtAppManager import AtAppManager
from AnnaTestCase import AnnaMroTestCase
from python.arrive.atsdk.device import AtDevice
from test.AtTestCase import AtTestCaseRunner

class DiagnosticTest(AnnaMroTestCase):
    fullPortEnabled = False
    simulated = False
    
    @classmethod
    def setUpClass(cls):
        super(DiagnosticTest, cls).setUpClass()
        cls.runCli("debug device platform 60290021")
        cls.runCli("device diag enable")
    
    @classmethod
    def tearDownClass(cls):
        super(DiagnosticTest, cls).tearDownClass()
        cls.runCli("device diag disable")
    
    def sleep(self, seconds):
        if not self.simulated:
            time.sleep(seconds)
    
class DiagnosticSerdesTest(DiagnosticTest):
    def TestSerdes(self, serdesIds, mode):
        self.assertCliSuccess("serdes powerup %s" % serdesIds)
        self.assertCliSuccess("serdes mode %s %s" % (serdesIds, mode))
        self.assertCliSuccess("serdes reset %s" % serdesIds)
        self.assertCliSuccess("serdes enable %s" % serdesIds)
        self.assertCliSuccess("serdes loopback %s local" % serdesIds)
        
        # Enable PRBS and give HW a short time
        self.assertCliSuccess("serdes prbs enable %s" % serdesIds)
        self.sleep(0.5)
        
        def checkPrbs(expectedGood = True, clearFirst = True):
            if clearFirst: 
                self.assertCliSuccess("show serdes prbs %s" % serdesIds)
                self.sleep(0.5)
                
            result = self.runCli("show serdes prbs %s" % serdesIds)
            for rowId in range(result.numRows()):
                status = result.cellContent(rowId, "sticky")
                if self.simulated:
                    break
                
                if expectedGood:
                    self.assertEqual(status, "none", None)
                else:
                    self.assertNotEqual(status, "none", None)
        
        def forceError(forced = True):
            self.assertCliSuccess("serdes prbs %s %s" % ("force" if forced else "unforce", serdesIds))
        
        checkPrbs(expectedGood = True)
        forceError(forced = True)
        checkPrbs(expectedGood = False)
        forceError(forced = False)
        checkPrbs(expectedGood = True, clearFirst = True)
    
    def fullPorts(self):
        return self.fullPortEnabled
    
    def testFaceplateStm64(self):
        serdesId = "1,9" if self.fullPorts() else "1"
        self.TestSerdes(serdesId, "stm64")
        
    def testFaceplateStm16(self):
        serdesId = "1,3,5,7,9,11,13,15" if self.fullPorts() else "1,3,5,7"
        self.TestSerdes(serdesId, "stm16")
        
    def testFaceplateStm4(self):
        serdesId = "1-16" if self.fullPorts() else "1-8"
        self.TestSerdes(serdesId, "stm4")
        
    def testFaceplateStm1(self):
        serdesId = "1-16" if self.fullPorts() else "1-8"
        self.TestSerdes(serdesId, "stm1")
    
    def testFaceplate10G(self):
        serdesId = "1,9" if self.fullPorts() else "1"
        self.TestSerdes(serdesId, "10G")
        
    def testFaceplate1G(self):
        serdesId = "1-16" if self.fullPorts() else "1-8"
        self.TestSerdes(serdesId, "1G")
    
    def testFaceplate100M(self):
        serdesId = "1-16" if self.fullPorts() else "1-8"
        self.TestSerdes(serdesId, "100M")
    
    def testMate(self):
        serdesId = "17-24" if self.fullPorts() else "17-20"
        self.TestSerdes(serdesId, "stm16")
    
    def testBackplane(self):
        serdesId = "25-26"
        self.TestSerdes(serdesId, "40G")
        
    @classmethod
    def sgmiiDiagAvailable(cls):
        if cls.is10G():
            return True

        version = AtAppManager.localApp().deviceVersion()
        if version.major >= 3 and version.major >= 3:            
            return False

        return True
    
    def testSgmii(self):
        if self.sgmiiDiagAvailable():
            serdesId = "27-28"
            self.TestSerdes(serdesId, "1G")
    
class DiagnosticRamTest(DiagnosticTest):
    def tearDown(self):
        self.assertCliSuccess("ddr test stop %s" % self.ddrIds())
        self.assertCliSuccess("qdr test stop %s" % self.qdrIds())
    
    @staticmethod
    def numDdr():
        return 3 # TODO: this should be 4
    
    def TestRamByHardwareWay(self, ramType, ramIds):
        self.assertCliSuccess("%s test start %s" % (ramType, ramIds))
        self.sleep(1)
        self.runCli("show %s test %s" % (ramType, ramIds))
        
        # See if we can have it stable for 10s
        stableCheckingTime = 10
        for _ in range(stableCheckingTime):
            result = self.runCli("show %s test %s" % (ramType, ramIds))
            for rowId in range(result.numRows()):
                if self.simulated:
                    break
                
                self.assertEqual(result.cellContent(rowId, "State"), "Started", None)
                self.assertEqual(int(result.cellContent(rowId, "Error")), 0, None)
                status = result.cellContent(rowId, "Status")
                self.assertEqual(status, "good", None)
                
            self.sleep(1)
        
        self.assertCliSuccess("%s test stop %s" % (ramType, ramIds))
    
    def TestRamBySoftwareMarchC(self, ramType, ramIds):
        self.sleep(5) # As HW test may run before and has just been stopped
        
        def Test(method):
            result = self.runCli("%s test %s %s" % (ramType, method, ramIds))
            
            if self.simulated:
                return
            
            for rowId in range(result.numRows()):
                self.assertEqual(result.cellContent(rowId, "InitStatus"), "OK", None)
                self.assertEqual(result.cellContent(rowId, "Result"), "PASS", None)
        
        Test("addressbus")
        Test("databus")
        Test("memory")
    
    def ddrIds(self):
        return "1-%d" % self.numDdr()
    
    @staticmethod
    def qdrIds():
        return "1"
    
    def testDdrHwBase(self):
        self.TestRamByHardwareWay("ddr", self.ddrIds())
        
    def testDdrSwBase(self):
        self.TestRamBySoftwareMarchC("ddr", self.ddrIds())
        
    def testQdrHwBase(self):
        self.TestRamByHardwareWay("qdr", self.qdrIds())
        
    def testQdrSwBase(self):
        self.TestRamBySoftwareMarchC("qdr", self.qdrIds())
    
def TestMain():
    runner = AtTestCaseRunner.xmlRunner()
    DiagnosticTest.simulated = AtDevice.currentDevice().simulated
    runner.run(DiagnosticSerdesTest)
    runner.run(DiagnosticRamTest)
    runner.summary()

if __name__ == '__main__':
    TestMain()
from test.AtTestCase import AtTestCaseRunner
from test.anna.AnnaTestCase import AnnaTestCase


class InterruptPinAutoEnable(AnnaTestCase):
    def assertInterruptEnabled(self, enabled):
        result = self.runCli("show device interrupt")
        self.assertEqual(result.cellContent(0, "Interrupt"), "en" if enabled else "dis")

    def testInterruptPinMustNotBeEnabledAfterProcessing(self):
        self.assertCliSuccess("device init")
        self.assertCliSuccess("device interrupt disable")
        self.assertInterruptEnabled(enabled=False)
        self.assertCliSuccess("device interrupt process")
        self.assertInterruptEnabled(enabled=False)

    def testInterruptPinMustBeDisabledBeforeProcessing(self):
        self.assertCliSuccess("device init")
        self.assertCliSuccess("device interrupt enable")
        self.assertInterruptEnabled(enabled=True)
        self.assertCliSuccess("device interrupt process")
        self.assertInterruptEnabled(enabled=False)

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(InterruptPinAutoEnable)
    runner.summary()

if __name__ == '__main__':
    TestMain()

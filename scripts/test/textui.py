from test.AtTestCase import AtTestCase, AtTestCaseRunner

class TextUICapturedOutput(AtTestCase):
    def setUp(self):
        self.autoFlushCapturedStdout(autoFlush=True)
        self.enableStdoutCapture(enabled=True)
        self.flushCapturedStdout()
        self.assertIsNone(self.getCapturedStdout())

    def makeOutput(self):
        self.runCli("show driver")

    def testEnableDisableCapture(self):
        self.makeOutput()
        self.assertGreater(len(self.getCapturedStdout()), 0)
        self.enableStdoutCapture(enabled=False)
        self.makeOutput()
        self.assertIsNone(self.getCapturedStdout())

    def testAutoFlush(self):
        self.makeOutput()
        captureLen = len(self.getCapturedStdout())

        # Auto flush enable
        numDisplayTime = 3
        self.autoFlushCapturedStdout(autoFlush=True)
        for _ in range(numDisplayTime):
            self.makeOutput()
        self.assertGreaterEqual(len(self.getCapturedStdout()), captureLen)

        # Auto flush disable
        self.flushCapturedStdout()
        self.autoFlushCapturedStdout(autoFlush=False)
        for _ in range(numDisplayTime):
            self.makeOutput()

        newLen = len(self.getCapturedStdout())
        self.assertGreaterEqual(newLen, numDisplayTime * captureLen)

    def testFlush(self):
        self.makeOutput()
        self.flushCapturedStdout()
        self.assertIsNone(self.getCapturedStdout())

    def testShowStdoutCapturing(self):
        self.enableStdoutCapture(enabled=True)
        self.autoFlushCapturedStdout(autoFlush=True)
        self.makeOutput()

        result = self.runCli("show textui stdout capture")
        self.assertEqual(result.cellContent("Enabled", "Value"), "en")
        self.assertEqual(result.cellContent("Auto flush", "Value"), "en")
        self.assertGreater(int(result.cellContent("Buffer size (bytes)", "Value")), 0)
        self.assertGreaterEqual(int(result.cellContent("Captured output (bytes)", "Value")), len(self.getCapturedStdout()))

    def testLargeOutput(self):
        self.enableStdoutCapture(enabled=False)
        self.enableStdoutCapture(enabled=True)

        self.autoFlushCapturedStdout(autoFlush=False)
        exclude = len(self.getCapturedStdout())

        basicOutputLen = 0
        numDisplayTimes = 1024
        for i in range(numDisplayTimes):
            self.makeOutput()
            if basicOutputLen == 0:
                basicOutputLen = len(self.getCapturedStdout()) - exclude

        self.assertGreaterEqual(len(self.getCapturedStdout()), basicOutputLen * numDisplayTimes)

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(TextUICapturedOutput)
    runner.summary()

if __name__ == '__main__':
    TestMain()

import getopt
import unittest
import os
import sys
from time import sleep

from python.arrive.AtAppManager.AtColor import AtColor
from python.arrive.AtAppManager.AtAppManager import AtAppManager

try:
    import atsdk
except:
    pass

class AtTestCase(unittest.TestCase):
    _productCode = None
    _simulated = None
    
    @classmethod
    def sleep(cls, seconds):
        if not cls.simulated():
            sleep(seconds)

    @classmethod
    def simulated(cls):
        """
        :rtype: bool
        """
        if cls._simulated is None:
            cls._simulated = cls.app().isSimulated()
        return cls._simulated

    @classmethod
    def app(cls):
        return AtAppManager.localApp()

    @classmethod
    def canRun(cls):
        return True

    @classmethod
    def runInEpAppContext(cls):
        return True

    @classmethod
    def shouldRunOnBoard(cls):
        return True

    @classmethod
    def normalRun(cls):
        suite = unittest.TestLoader().loadTestsFromTestCase(cls)
        return unittest.TextTestRunner(verbosity=2).run(suite)
        
    @classmethod
    def xmlRun(cls):
        import xmlrunner
        suite = unittest.TestLoader().loadTestsFromTestCase(cls)
        return xmlrunner.XMLTestRunner().run(suite)
    
    @classmethod
    def classRunCli(cls, cli):
        return cls.runCli(cli)
    
    @classmethod
    def assertClassCliSuccess(cls, cli):
        result = cls.classRunCli(cli)
        assert cls.cliResultIsSuccess(result)

    @classmethod
    def runCli(cls, cli):
        """
        :rtype: python.arrive.AtAppManager.AtCliResult.AtCliResult
        """
        return cls.app().runCli(cli)
    
    @staticmethod
    def cliResultIsSuccess(cliResult):
        if type(cliResult) is list:
            for _, result in cliResult:
                if not result.success():
                    return False
            
            return True
        
        return cliResult.success()
    
    def assertCliSuccess(self, cli):
        result = self.runCli(cli)
        self.assertTrue(self.cliResultIsSuccess(result), "Run CLI \"%s\" fail" % cli)
    
    @classmethod
    def enableLogger(cls):
        cls.runCli("logger enable")
    
    @classmethod
    def disableLogger(cls):
        cls.runCli("logger disable")
    
    def assertCliFailure(self, cli):
        self.disableLogger()
        result = self.runCli(cli)
        self.enableLogger()
        self.assertFalse(self.cliResultIsSuccess(result), "Run CLI \"%s\" must be fail" % cli)

    @staticmethod
    def rd(address, core=0):
        return atsdk.rd(address, core)

    @classmethod
    def productCode(cls):
        if cls._productCode is None:
            cls._productCode = AtAppManager.localApp().productCode()
        return cls._productCode

    @classmethod
    def registerProviderFactory(cls):
        """
        :rtype: python.arrive.atsdk.AtRegister.AtRegisterProviderFactory
        """
        return None

    @classmethod
    def registerProvider(cls, atregName):
        fileName, _ = os.path.splitext(atregName)
        return cls.registerProviderFactory().providerByName("_%s" % fileName)

    @classmethod
    def getCapturedStdout(cls):
        return atsdk.CapturedStdout()

    @classmethod
    def enableStdoutCapture(cls, enabled):
        cls.classRunCli("textui stdout capture %s" % ("enable" if enabled else "disable"))

    @classmethod
    def flushCapturedStdout(cls):
        cls.classRunCli("textui stdout capture flush")

    @classmethod
    def autoFlushCapturedStdout(cls, autoFlush):
        cls.classRunCli("textui stdout capture autoflush %s" % ("enable" if autoFlush else "disable"))

class AtTestCaseRunner(object):
    @staticmethod
    def colorEnabledAsDefault():
        return True

    def __init__(self):
        object.__init__(self)
        self._results = []
        self.colorEnabled = self.colorEnabledAsDefault()
    
    def _run(self, testCase, withColor = None):
        pass
    
    @staticmethod
    def shouldRunTest(testCase):
        # It is declared that it cannot run
        if not testCase.canRun():
            return False
        
        if not testCase.runInEpAppContext():
            return True

        # Simulate will be always runnable
        if testCase.app().isSimulated():
            return True

        # But not all of them should run on board
        return testCase.shouldRunOnBoard()

    def run(self, testCase, withColor = None):
        if self.shouldRunTest(testCase):
            self._run(testCase, withColor)
        else:
            AtColor.printColor(AtColor.YELLOW, "Test case %s is declared that cannot be run" % testCase)
    
    @classmethod
    def printResults(cls, results):
        numTestCases = 0
        numErrors = 0
        numFailures = 0
        
        for result in results:
            numTestCases = numTestCases + result.testsRun
            numErrors = numErrors + len(result.errors)
            numFailures = numFailures + len(result.failures)
        
        color = AtColor.GREEN if ((numErrors == 0) and (numFailures == 0)) else AtColor.RED
        
        AtColor.printColor(color, "============================================================================")
        AtColor.printColor(color, "= TEST SUMMARY                                                              ")
        AtColor.printColor(color, "============================================================================")
        for result in results:
            if len(result.errors) + len(result.failures) == 0:
                continue
            result.printErrors()
        
        AtColor.printColor(color, "numTestCases = %d, numErrors = %d, numFailures = %d" % (numTestCases, numErrors, numFailures))
        AtColor.printColor(color, "============================================================================")
        
        return numErrors == 0 and numFailures == 0
    
    def summary(self):
        return self.printResults(self._results)
        
    @classmethod
    def xmlRunner(cls):
        return _XmlRunner()
    
    @classmethod
    def defaultRunner(cls):
        return _DefaultRunner()
    
    @classmethod
    def runner(cls):
        if cls._xmlRunnerExist():
            return cls.xmlRunner()
        return cls.defaultRunner()
    
    @classmethod
    def _xmlRunnerExist(cls):
        try:
            import xmlrunner
            return True
        except:
            return False

    @classmethod
    def shouldRunFile(cls, filePath):
        return True

    @classmethod
    def runDirectory(cls, directory, listFilesOnly = False, colorEnabled = False):
        AtColor.enableColor(colorEnabled)

        import os
        import sys
        import inspect
        for dirpath, _, fileList in os.walk(directory):
            for fname in fileList:
                try:
                    (name, suffix, _, _)  = inspect.getmoduleinfo(fname)
                    if suffix != ".py":
                        continue
                except:
                    AtColor.printColor(AtColor.YELLOW, "Ignore file %s" % fname)
                    continue

                if not cls.shouldRunFile(fname):
                    AtColor.printColor(AtColor.YELLOW, "Ignore file: %s" % fname)

                sys.path.append(dirpath)
                try:
                    aModule = __import__(name)
                    functions = inspect.getmembers(aModule, inspect.isfunction)
                    ran = False
                    for name, aFunction in functions:
                        if name == 'TestMain':
                            if listFilesOnly:
                                AtColor.printColor(AtColor.GREEN, "%s is runnable" % fname)
                            else:
                                print "Run test from {scriptFile}".format(scriptFile=os.path.join(dirpath, fname))
                                aFunction()
                            ran = True
                            break

                    if not ran:
                        AtColor.printColor(AtColor.YELLOW, "Ignore %s, TestMain function does not exist" % fname)
                except Exception as e:
                    AtColor.printColor(AtColor.YELLOW, "Ignore %s, error happen when importing" % e)

                sys.path.remove(dirpath)
    
class _DefaultRunner(AtTestCaseRunner):
    def _run(self, testCase, withColor=None):
        suite = unittest.TestLoader().loadTestsFromTestCase(testCase)
        result = unittest.TextTestRunner().run(suite)
        self._results.append(result)
        return result

class _XmlRunner(AtTestCaseRunner):
    def _run(self, testCase, withColor=None):
        import xmlrunner

        if withColor is None:
            withColor = False # So that Jenkins can parse
        AtColor.enableColor(withColor)

        suite = unittest.TestLoader().loadTestsFromTestCase(testCase)
        result = xmlrunner.XMLTestRunner().run(suite)
        self._results.append(result)
        return result

    @staticmethod
    def colorEnabledAsDefault():
        return False

def printResult(results):
    AtTestCaseRunner.printResults(results)

class OptionParser(object):
    def __init__(self):
        self.colorEnabled = True
        self.accessWarningEnabled = True
        self.returnCode = None
        self.deviceInitDisabled = False

    @staticmethod
    def usageDescription():
        return "[--help] [--no-color] [--no-access-warning] [--no-device-init]"

    @classmethod
    def options(cls):
        return ["help", "no-color", "no-access-warning", "no-device-init"]

    def printUsage(self, returnCode=None):
        print "Usage: " + sys.argv[0] + " %s" % self.usageDescription()
        self.returnCode = returnCode

    # noinspection PyUnusedLocal
    def parseOption(self, opt, unused_arg):
        if opt == "--help":
            self.printUsage(returnCode=0)
        if opt == "--no-color":
            self.colorEnabled = False
        if opt == "--no-access-warning":
            self.accessWarningEnabled = False
        if opt == "--no-device-init":
            self.deviceInitDisabled = True

    def parse(self):
        try:
            options = self.options()
            opts, _ = getopt.getopt(sys.argv[1:], "", options)
        except:
            print "Invalid options"
            self.printUsage(returnCode=1)
            return

        for opt, arg in opts:
            self.parseOption(opt, arg)

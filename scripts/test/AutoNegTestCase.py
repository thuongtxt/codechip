from AtTestCase import AtTestCase

class AutoNegTestCase(AtTestCase):
    def numPorts(self):
        return None
    
    def cliPorts(self):
        return None
    
    def setUp(self):
        # Enable autoneg so that autoneg information can be displayed
        self.assertCliSuccess("eth port autoneg enable %s" % self.cliPorts())
    
    def testEnableAutoneg(self):
        def _testWithEnabled(expectedEnabled):
            command = "enable" if expectedEnabled else "disable"
            self.assertCliSuccess("eth port autoneg %s %s" % (command, self.cliPorts()))
            result = self.runCli("show eth port autoneg %s" % self.cliPorts())
            self.assertEqual(result.numRows(), self.numPorts(), None)
            for row in range(result.numRows()):
                enabled = result.cellContent(row, "enabled")
                self.assertEqual(enabled, "en" if expectedEnabled else "dis", None)
        
        _testWithEnabled(True)
        _testWithEnabled(False)
        
    def testAutoNegRestart(self):
        def _testWithRestartOn(expectedOn):
            command = "on" if expectedOn else "off"
            self.assertCliSuccess("eth port autoneg restart %s %s" % (command, self.cliPorts()))
            result = self.runCli("show eth port autoneg %s" % self.cliPorts())
            self.assertEqual(result.numRows(), self.numPorts(), None)
            for row in range(result.numRows()):
                enabled = result.cellContent(row, "restart")
                self.assertEqual(enabled, "on" if expectedOn else "off", None)
        
        _testWithRestartOn(True)
        _testWithRestartOn(False)
        
    @staticmethod
    def validStates():
        return ["N/S",
                "enable",
                "restart",
                "ability-etect",
                "ack-detect",
                "next-page-wait",
                "complete-ack",
                "idle-detect",
                "link-ok",
                "disable-link-ok"]
        
    @staticmethod
    def validSpeeds():
        return ["10m",
                "100m",
                "1000m",
                "2500m",
                "10g",
                "40g"]
        
    @staticmethod
    def validDuplexModes():
        return ["half-duplex",
                "full-duplex",
                "half-duplex|full-duplex"
                "autodetect"]
        
    def testAccessAutoNegStatus(self):
        result = self.runCli("show eth port autoneg %s" % self.cliPorts())
        self.assertEqual(result.numRows(), self.numPorts(), None)
        for row in range(result.numRows()):
            complete = result.cellContent(row, "completed")
            self.assertTrue(complete in ["yes", "no"], None)
            
            state = result.cellContent(row, "state")
            self.assertTrue(state in self.validStates(), None)
            
            speed = result.cellContent(row, "speed")
            self.assertTrue(speed in self.validSpeeds(), None)
            
            duplex = result.cellContent(row, "duplex")
            self.assertTrue(duplex in self.validDuplexModes(), None)
            
            linkStatus = result.cellContent(row, "link")
            self.assertTrue(linkStatus in ["up", "down", "N/S"], None)

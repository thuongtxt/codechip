"""
Created on Mar 15, 2017

@author: namnn
"""
from test.AtTestCase import AtTestCase, AtTestCaseRunner
import python.arrive.atsdk.AtRegister as AtRegister

class SpiramctlRegister(AtRegister.AtRegister):
    @classmethod
    def StsPiRemoteLoopBackBit(cls):
        return 15
    
    @classmethod
    def StsPiAisFrcBit(cls):
        return 11

    def StsPiRemoteLoopBack(self):
        return self.getBit(self.StsPiRemoteLoopBackBit())
    
    def StsPiAisFrc(self):
        return self.getBit(self.StsPiAisFrcBit())
    
class DemramctlRegister(AtRegister.AtRegister):
    @classmethod
    def PiDemAisDownstBit(cls):
        return 17
    
    def PiDemAisDownst(self):
        return self.getBit(self.PiDemAisDownstBit())
    
class RxhomapramctlRegister(AtRegister.AtRegister):
    @classmethod
    def HoMapAisDownsBit(cls):
        return 9
    
    def HoMapAisDowns(self):
        return self.getBit(self.HoMapAisDownsBit())    

class VpiramctlRegsiter(AtRegister.AtRegister):
    @classmethod
    def VtPiRemoteLoopBackBit(cls):
        return 6
    
    def VtPiRemoteLoopBack(self):
        return self.getBit(self.VtPiRemoteLoopBackBit())

class LoopbackWithAisForce(AtTestCase):
    @classmethod
    def setUpClass(cls):
        cls.runCli("device show readwrite dis dis")
        cls.runCli("device init")
    
    @classmethod
    def OcnBaseAddress(cls):
        return 0x0100000
    
    def vcCliId(self):
        return None
    
    def registerWithRelativeAddress(self, address):
        return self.OcnBaseAddress() + address + self.offset()
    
    def offset(self):
        return 0
    
    def createRegWithRelativeAddress(self, regClass, relativeAddress):
        address = self.registerWithRelativeAddress(relativeAddress)
        reg = regClass(self.rd(address))
        reg.address = address
        return reg
    
    def remoteLoopback(self):
        self.assertCliSuccess("sdh path loopback %s remote" % self.vcCliId())

    def localLoopback(self):
        self.assertCliSuccess("sdh path loopback %s local" % self.vcCliId())

    def releaseLoopback(self):
        self.assertCliSuccess("sdh path loopback %s release" % self.vcCliId())
    
    def cliChannelForAis(self):
        return None
    
    def forceRxAis(self):
        self.assertCliSuccess("sdh path force alarm %s ais rx en" % self.cliChannelForAis())
        
    def unforceRxAis(self):
        self.assertCliSuccess("sdh path force alarm %s ais rx dis" % self.cliChannelForAis())
    
    def checkAisForced(self, forced):
        expectedCellContent = "ais" if forced else "None"
        cliResult = self.runCli("show sdh path forcedalarms %s" % self.cliChannelForAis())
        cellContent = cliResult.cellContent(0, "ForcedRxAlarms")
        self.assertEqual(cellContent, expectedCellContent, None)
    
    def localLoopbackIsSupported(self):
        return True
    
    def testLoopbackMustBasicallyWork(self):
        def checkWithLoopbackMode(mode):
            cliResult = self.runCli("show sdh path loopback %s" % self.vcCliId())
            cellContent = cliResult.cellContent(0, "Loopback")
            self.assertEqual(cellContent, mode, None)
            
        self.remoteLoopback()
        checkWithLoopbackMode("remote")
        self.releaseLoopback()
        checkWithLoopbackMode("release")
    
    def checkNormalAisForced(self, forced):
        self.fail("Implement this please")
    
    def testForceAisMustBasicallyWork(self):
        def checkAisForceWithExpectedValue(forced):
            expectedCellContent = "ais" if forced else "None"
            cliResult = self.runCli("show sdh path forcedalarms %s" % self.cliChannelForAis())
            cellContent = cliResult.cellContent(0, "ForcedRxAlarms")
            self.assertEqual(cellContent, expectedCellContent, None)
            
            self.checkNormalAisForced(forced)
        
        self.forceRxAis()
        checkAisForceWithExpectedValue(forced=True)
        self.unforceRxAis()
        checkAisForceWithExpectedValue(forced=False)
    
    def checkNewControl(self, use):
        self.fail("Implement this checking please")
    
    def testNormalLoopbackWithNoAlarmForceWillHaveNewControlSetToDisabled(self):
        self.remoteLoopback()
        self.checkNewControl(use = False)
        
    def testForceAisWithNoLoopbackWillNotUseNewControl(self):
        self.forceRxAis()
        self.checkNewControl(use = False)
    
    def testRemoteLoopbackThenForceAis(self):
        self.remoteLoopback()
        self.forceRxAis()
        
        self.checkNormalAisForced(forced = False)
        self.checkNewControl(use=True)
        self.checkAisForced(forced=True)
    
    def testRemoteLoopbackThenForceAis_ReleaseLoopback(self):
        self.testRemoteLoopbackThenForceAis()
        self.releaseLoopback()
        
        self.checkNormalAisForced(forced = True)
        self.checkNewControl(use=False)
        self.checkAisForced(forced=True)
        
    def testRemoteLoopbackThenForceAis_LocalLoopback(self):
        if not self.localLoopbackIsSupported():
            self.skipTest("Local loopback is not supported")
        
        self.testRemoteLoopbackThenForceAis()
        self.localLoopback()
        
        self.checkNormalAisForced(forced = True)
        self.checkNewControl(use=False)
        self.checkAisForced(forced=True)
    
    def testForceRxAisThenRemoteLoopback(self):
        self.forceRxAis()
        self.checkNormalAisForced(forced=True)
        
        self.remoteLoopback()
        self.checkNormalAisForced(forced=False)        
        self.checkNewControl(use=True)
        
        self.checkAisForced(forced=True)
    
    def testForceRxAisThenRemoteLoopback_ThenUnforce(self):
        self.testForceRxAisThenRemoteLoopback()
        self.unforceRxAis()
        
        self.checkNormalAisForced(forced=False)
        self.checkNewControl(use=False)
        self.checkAisForced(forced=False)
    
    def testNewControllsMustBeDisabledByDefault(self):
        self.fail("Implement this please")
    
class HoVcLoopbackWithAisForce(LoopbackWithAisForce):
    def setUp(self):
        self.releaseLoopback()
        self.unforceRxAis()
    
    @staticmethod
    def cliId():
        return "1.1.1"
    
    def auCliId(self):
        return "au3.%s" % self.cliId()
    
    def vcCliId(self):
        return "vc3.%s" % self.cliId()
    
    def offset(self):
        return 0 # NOTE: when the AU CLI ID is changed, this has to be updated
    
    def spiramctl(self):
        return self.createRegWithRelativeAddress(SpiramctlRegister, 0x22000)
    
    def demramctl(self):
        return self.createRegWithRelativeAddress(DemramctlRegister, 0x40000)
    
    def homapramctl(self):
        return self.createRegWithRelativeAddress(RxhomapramctlRegister, 0x28000)
    
    def cliChannelForAis(self):
        return self.auCliId()
    
    def checkNormalAisForced(self, forced):
        reg = self.spiramctl()
        expectedForcedValue = 1 if forced else 0
        self.assertEqual(reg.StsPiAisFrc(), expectedForcedValue, None)
    
    def checkNewControl(self, use):
        expectedValue = 1 if use else 0
        
        reg = self.spiramctl()
        self.assertEqual(reg.StsPiRemoteLoopBack(), expectedValue, None)
        reg = self.demramctl()
        self.assertEqual(reg.PiDemAisDownst(), expectedValue, None)
        reg = self.homapramctl()
        self.assertEqual(reg.HoMapAisDowns(), expectedValue, None)
    
    def testNewControllsMustBeDisabledByDefault(self):
        def check():
            self.runCli("device init")
            self.checkNewControl(use = False)
            self.checkAisForced(forced=False)
        
        reg = self.spiramctl()
        reg.wr(reg.address, 0xFFFFFFFF)
        check()
        
        reg = self.demramctl()
        reg.wr(reg.address, 0xFFFFFFFF)
        check()
        
        reg = self.homapramctl()
        reg.wr(reg.address, 0xFFFFFFFF)
        check()

class LoVcLoopbackWithAisForce(LoopbackWithAisForce):
    @classmethod
    def setDefaultMapping(cls):
        pass
    
    @classmethod
    def setUpClass(cls):
        super(LoVcLoopbackWithAisForce, cls).setUpClass()
        cls.setDefaultMapping()
    
    def cliId(self):
        return "1.1.1.1.1"
    
    def tuCliId(self):
        return "tu1x.%s" % self.cliId()
    
    def vcCliId(self):
        return "vc1x.%s" % self.cliId()
    
    def cliChannelForAis(self):
        return self.vcCliId()
    
    def offset(self):
        return 0 # TODO: when VC are changed, need to change this
    
    def vpiramctl(self):
        return self.createRegWithRelativeAddress(VpiramctlRegsiter, 0x40800)
    
    def localLoopbackIsSupported(self):
        return False
    
    def checkNormalAisForced(self, forced):
        # We all know that it has been working for long time so far. Bypass this checking now
        pass
    
    def checkNewControl(self, use):
        expectedValue = 1 if use else 0
        reg = self.vpiramctl()
        self.assertEqual(reg.VtPiRemoteLoopBack(), expectedValue, None)
    
    def resetMapping(self):
        self.assertCliSuccess("sdh map vc3.1.1.1 c3")
    
    def testNewControllsMustBeDisabledByDefault(self):
        reg = self.vpiramctl()
        reg.wr(reg.address, 0xFFFFFFFF)
        
        self.resetMapping()
        self.setDefaultMapping()
        self.checkNewControl(use = False)
        self.checkAisForced(forced=False)

class Vc1xLoopbackWithAisForce(LoVcLoopbackWithAisForce):
    @classmethod
    def setDefaultMapping(cls):
        cls.runCli("sdh map vc3.1.1.1 7xtug2s")
        cls.runCli("sdh map tug2.1.1.1.1-1.1.1.7 tu11")
        cls.runCli("sdh map vc1x.1.1.1.1.1-1.1.1.7.4 c1x")

class Tu3Vc3LoopbackWithAisForce(LoVcLoopbackWithAisForce):
    @classmethod
    def setDefaultMapping(cls):
        cls.runCli("sdh map aug1.1.1 vc4")
        cls.runCli("sdh map vc4.1.1 3xtug3s")
        cls.runCli("sdh map tug3.1.1.1-1.1.3 vc3")
        cls.runCli("sdh map vc3.1.1.1-1.1.3 c3")
        
    def cliId(self):
        return "1.1.1"
    
    def tuCliId(self):
        return "tu3.%s" % self.cliId()
    
    def vcCliId(self):
        return "vc3.%s" % self.cliId() 
    
    def resetMapping(self):
        self.assertCliSuccess("sdh map vc4.1.1 c4")
    
def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(HoVcLoopbackWithAisForce)
    runner.run(Vc1xLoopbackWithAisForce)
    runner.run(Tu3Vc3LoopbackWithAisForce)
    runner.summary()

if __name__ == '__main__':
    TestMain()

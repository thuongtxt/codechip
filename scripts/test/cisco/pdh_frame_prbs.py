"""
Created on Jul 1, 2017

@author: namnng
"""
from test.AtTestCase import AtTestCase, AtTestCaseRunner
from python.arrive.atsdk.AtRegister import cBit3_0, cBit31_0, AtRegister, cBit16_15


class PdhChannelFramePrbsAndPwBinding(AtTestCase):
    @staticmethod
    def pdhAddress(localAddress):
        return 0x01000000 + localAddress

    @staticmethod
    def invalidValue():
        return cBit31_0

    @staticmethod
    def swFrameType():
        return "invlaid"
    
    @staticmethod
    def expectedPdhHwFrameType():
        return PdhChannelFramePrbsAndPwBinding.invalidValue()
    
    @staticmethod
    def pdhTxFrameAddress():
        return PdhChannelFramePrbsAndPwBinding.invalidValue()

    @staticmethod
    def map_line_ctrl_address():
        return 0x800000 + 0x010000

    @staticmethod
    def expectedMapFrameType():
        return PdhChannelFramePrbsAndPwBinding.invalidValue()

    def getPdhTxFrameType(self):
        return self.rd(self.pdhTxFrameAddress()) & cBit3_0

    def getMapFrameType(self):
        reg = AtRegister(self.rd(self.map_line_ctrl_address()))
        return reg.getField(cBit16_15, 15)

    def tearDown(self):
        self.unbind()
        self.deletePrbs()
        self.assertFramed()

    def _createPrbs(self):
        assert 0

    def createPrbs(self, enabled = False):
        self._createPrbs()
        self.enablePrbs() if enabled else self.disablePrbs()

    def deletePrbs(self):
        self.runCli("prbs engine delete 1")

    def enablePrbs(self):
        self.runCli("prbs engine enable 1")

    def disablePrbs(self):
        self.runCli("prbs engine disable 1")

    def bind(self):
        assert 0

    def unbind(self):
        self.runCli("pw circuit bind 1 none")

    def assertFramed(self):
        self.assertEqual(self.getPdhTxFrameType(), self.expectedPdhHwFrameType(), None)
        self.assertEqual(self.getMapFrameType(), self.expectedMapFrameType(), None)

    def assertUnframed(self):
        self.assertEqual(self.getPdhTxFrameType(), 0, None)
        self.assertEqual(self.getMapFrameType(), 3, None)

    def testHwFrameTypeMustBeCorrectByDefault(self):
        self.assertFramed()

    def testFrameHandlingWhenBinding(self):
        self.bind()
        self.assertUnframed()
        self.unbind()
        self.assertFramed()

    def testFrameHandlingOnPrbs(self):
        self.assertFramed()

        self.createPrbs()
        self.assertFramed()

        self.enablePrbs()
        self.assertFramed()

        self.disablePrbs()
        self.assertFramed()

        self.deletePrbs()
        self.assertFramed()

    def testPrbsAfterPwBinding(self):
        self.bind()
        self.assertUnframed()

        self.createPrbs(enabled=False)
        self.assertUnframed()

        self.enablePrbs()
        self.assertFramed()
        self.disablePrbs()
        self.assertUnframed()
        self.deletePrbs()

        self.createPrbs(enabled=True)
        self.assertFramed()
        self.deletePrbs()
        self.assertUnframed()

    def testPwBindingAfterPrbsButInDisableState(self):
        self.createPrbs(enabled=False)
        self.assertFramed()

        self.bind()
        self.assertUnframed()

        self.unbind()
        self.assertFramed()

    def testPwBindingAfterPrbsButInEnableState(self):
        self.createPrbs(enabled=True)
        self.assertFramed()

        self.bind()
        self.assertFramed()

        self.unbind()
        self.assertFramed()

    def testWhileEnalingPrbs_DeprovisionPw(self):
        self.bind()
        self.createPrbs(enabled=True)
        self.assertFramed()
        self.unbind()
        self.assertFramed()

    def testWhileDisablingPrbs_DeprovisionPw(self):
        self.bind()
        self.createPrbs(enabled=False)
        self.assertUnframed()
        self.unbind()
        self.assertFramed()

    def testPwBound_ThenDeprovisonPrbs(self):
        self.createPrbs(enabled=True)
        self.bind()
        self.assertFramed()

        self.deletePrbs()
        self.assertUnframed()


class De3FramePrbsAndPwBinding(PdhChannelFramePrbsAndPwBinding):
    @staticmethod
    def swFrameType():
        return "ds3_cbit_unchannelize"

    @staticmethod
    def expectedPdhHwFrameType():
        return 3

    @staticmethod
    def pdhTxFrameAddress():
        return PdhChannelFramePrbsAndPwBinding.pdhAddress(0x80000)

    @classmethod
    def setUpClass(cls):
        cls.runCli("device init")
        cls.runCli("sdh map vc3.1.1.1 de3")
        cls.runCli("pdh de3 framing 1.1.1 %s" % cls.swFrameType())
        cls.runCli("pw create satop 1")
        cls.runCli("pw ethport 1 1")
        cls.runCli("pw psn 1 mpls")
        cls.runCli("pw enable 1")

    def _createPrbs(self):
        self.runCli("prbs engine create de3 1 1.1.1")

    def bind(self):
        self.runCli("pw circuit bind 1 de3.1.1.1")

    @staticmethod
    def expectedMapFrameType():
        return 2

class De1FramePrbsAndPwBinding(PdhChannelFramePrbsAndPwBinding):
    @staticmethod
    def swFrameType():
        return "ds1_sf"

    @staticmethod
    def expectedPdhHwFrameType():
        return 1

    @staticmethod
    def pdhTxFrameAddress():
        return PdhChannelFramePrbsAndPwBinding.pdhAddress(0x94000)

    @classmethod
    def setUpClass(cls):
        cls.runCli("device init")
        cls.runCli("sdh map vc3.1.1.1 7xtug2s")
        cls.runCli("sdh map tug2.1.1.1.1 tu11")
        cls.runCli("sdh map vc1x.1.1.1.1.1 de1")
        cls.runCli("pdh de1 framing 1.1.1.1.1 %s" % cls.swFrameType())
        cls.runCli("pw create satop 1")
        cls.runCli("pw ethport 1 1")
        cls.runCli("pw psn 1 mpls")
        cls.runCli("pw enable 1")

    def _createPrbs(self):
        self.runCli("prbs engine create de1 1 1.1.1.1.1")

    def bind(self):
        self.runCli("pw circuit bind 1 de1.1.1.1.1.1")

    @staticmethod
    def expectedMapFrameType():
        return 0

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(De3FramePrbsAndPwBinding)
    runner.run(De1FramePrbsAndPwBinding)
    runner.summary()

if __name__ == '__main__':
    TestMain()
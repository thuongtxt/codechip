from test.AtTestCase import AtTestCase, AtTestCaseRunner


class PathAbstractTest(AtTestCase):
    def pathCliId(self):
        return None

    def setUp(self):
        self.forceAlarms(self.getForcedAlarms(), forced=False)

    def forceAbleAlarms(self):
        return ["ais", "lop"]

    def canForceAlarm(self, alarmName):
        return alarmName in self.forceAbleAlarms()

    def forceAlarms(self, alarmNames, forced):
        for alarm in alarmNames:
            cli = "sdh path force alarm %s %s tx %s" % (self.pathCliId(), alarm, "en" if forced else "dis")
            self.assertCliSuccess(cli)

    def getForcedAlarms(self):
        """
        :rtype: list
        """
        cli = "show sdh path forcedalarms %s" % self.pathCliId()
        result = self.runCli(cli)
        self.assertEqual(result.numRows(), 1)

        forcedAlarms = result.cellContent(0, "ForcedTxAlarms")
        if forcedAlarms.lower() == "none":
            return list()

        return forcedAlarms.split("|")

    def TestForceAlarm(self, alarmName):
        if self.canForceAlarm(alarmName):
            self.forceAlarms([alarmName], forced=True)
            forcedAlarms = self.getForcedAlarms()
            self.assertEqual(len(forcedAlarms), 1)
            self.assertTrue(alarmName in forcedAlarms)

            self.forceAlarms([alarmName], forced=False)
            self.assertEqual(len(self.getForcedAlarms()), 0)

        else:
            def Try():
                self.forceAlarms([alarmName], forced=True)
            self.disableLogger()
            self.assertRaises(Exception, Try)
            self.enableLogger()

    def testForceAis(self):
        self.TestForceAlarm("ais")

    def testForceLop(self):
        self.TestForceAlarm("lop")

    def testForceBothAisAndLop(self):
        alarms = self.getForcedAlarms()
        self.forceAlarms(alarms, forced=True)
        self.assertEqual(alarms.sort(), self.getForcedAlarms().sort())

        self.forceAlarms(alarms, forced=False)
        self.assertEqual(len(self.getForcedAlarms()), 0)

class OcnCardAbstractTest(PathAbstractTest):
    @classmethod
    def setUpClass(cls):
        cls.assertClassCliSuccess("device init")
        cls.assertClassCliSuccess("sdh map aug1.1.1 vc4")
        cls.assertClassCliSuccess("sdh map vc4.1.1 3xtug3s")
        cls.assertClassCliSuccess("sdh map tug3.1.1.1-1.1.3 7xtug2s")
        cls.assertClassCliSuccess("sdh map tug2.1.1.1.1-1.1.3.7 tu11")
        cls.assertClassCliSuccess("sdh map vc1x.1.1.1.1.1-1.1.3.7.4 de1")

    @classmethod
    def canRun(cls):
        return cls.productCode() in [0x60210011, 0x60210051, 0x60210012, 0x60210061]

class OcnCardAuTest(OcnCardAbstractTest):
    def pathCliId(self):
        return "au4.1.1"

class OcnCardTu1xTest(OcnCardAbstractTest):
    def pathCliId(self):
        return "tu1x.1.1.2.5.3"

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(OcnCardAuTest)
    runner.run(OcnCardTu1xTest)
    runner.summary()

if __name__ == '__main__':
    TestMain()

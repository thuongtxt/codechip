from test.AtTestCase import AtTestCase, AtTestCaseRunner

class Capacity(object):
    @classmethod
    def numTestedPws(cls):
        return 0
    
    @classmethod
    def numTestedApsGroups(cls):
        return cls.numTestedPws()
        
    @classmethod
    def numTestedHsGroups(cls):
        return None
        
    @classmethod
    def numLines(cls):
        return None
    
    @classmethod
    def numEthPorts(cls):
        return None

class Capacity10G(Capacity):
    @classmethod
    def numTestedPws(cls):
        return 5376
    
    @classmethod
    def numLines(cls):
        return 4

    @classmethod
    def numTestedHsGroups(cls):
        return 2048
        
    @classmethod
    def numEthPorts(cls):
        return 2

class Capacity5G(Capacity):
    @classmethod
    def numTestedPws(cls):
        return 2688
    
    @classmethod
    def numLines(cls):
        return 2

    @classmethod
    def numTestedHsGroups(cls):
        return 2048
        
    @classmethod
    def numEthPorts(cls):
        return 1

class AtCiscoUpsr(AtTestCase):
    _capacity = None
    
    @classmethod
    def setUpClass(cls):
        cls.mapVc1xCep()
        
    @classmethod
    def capacity(cls):
        if cls._capacity:
            return cls._capacity
        
        productCode = cls.productCode()
        
        if productCode == 0x60210051:
            cls._capacity = Capacity10G
        if productCode == 0x60210012:
            cls._capacity = Capacity5G
        
        return cls._capacity
    
    @classmethod
    def numTestedPws(cls):
        return cls.capacity().numTestedPws()
    
    @classmethod
    def numTestedApsGroups(cls):
        return cls.capacity().numTestedApsGroups()
        
    @classmethod
    def numTestedHsGroups(cls):
        return cls.capacity().numTestedHsGroups()
        
    @classmethod
    def numLines(cls):
        return cls.capacity().numLines()
    
    @classmethod
    def numEthPorts(cls):
        return cls.capacity().numEthPorts()
    
    @classmethod
    def _mapVc1x(cls):
        numLines = cls.numLines()
        cls.classRunCli("device init")
        cls.classRunCli("sdh map vc3.1.1.1-%d.16.3 7xtug2s" % numLines)
        cls.classRunCli("sdh map tug2.1.1.1.1-%d.16.3.7 tu11" % numLines)
        cls.classRunCli("sdh map vc1x.1.1.1.1.1-%d.16.3.7.4 c1x" % numLines)
    
    @classmethod
    def _setupVc1xCep(cls):
        numPws = cls.numTestedPws()
        pwList = "1-%d" % numPws
        cls.classRunCli("pw create cep %s basic" % pwList)
        cls.classRunCli("pw circuit bind %s vc1x.1.1.1.1.1-%d.16.3.7.4" % (pwList, cls.numLines()))
        cls.classRunCli("pw psn %s mpls" % pwList)
        
        numPwsPerPort = numPws / cls.numEthPorts()
        if numPwsPerPort == 0:
            numPwsPerPort = numPws
        numBoundPws = 0
        for port_i in range(0, cls.numEthPorts()):
            start = numBoundPws + 1
            stop  = start + numPwsPerPort - 1
            boundPwList = "%d-%d" % (start, stop)
            cls.classRunCli("pw ethport %s %d" % (boundPwList, port_i + 1))
            numBoundPws = numBoundPws + numPwsPerPort
        
        cls.classRunCli("pw enable %s" % pwList)
    
    @classmethod
    def _setupVc1xCepApsGroups(cls):
        numTestedGroups = cls.numTestedApsGroups()
        cls.classRunCli("pw group aps create 1-%d" % numTestedGroups)
        for group_i in range(0, numTestedGroups):
            groupId = group_i + 1
            cls.classRunCli("pw group aps add %d %d" % (groupId, groupId))
    
    @classmethod
    def _setupVc1xCepHsGroups(cls):
        numTestedGroups = cls.numTestedHsGroups()
        cls.classRunCli("pw group hs create 1-%d" % numTestedGroups)
        
        numPwsPerGroup = cls.numTestedPws() / numTestedGroups
        nextPw = 1
        for group_i in range(0, numTestedGroups):
            start = nextPw
            stop = start + numPwsPerGroup - 1
            nextPw = stop + 1
            pwList = "%d-%d" % (start, stop)
            
            groupId = group_i + 1
            cls.classRunCli("pw group hs add %d %s" % (groupId, pwList))
            
        for pw_i in range(0, cls.numTestedPws()):
            pwId = pw_i + 1
            backupLabel = cls.numTestedPws() + pwId
            innerLabel = "%d.%d.%d" % (pwId, 1, 1)
            cls.classRunCli("pw backup psn %d mpls %d %s" % (pwId, backupLabel, innerLabel))
            
    @classmethod
    def mapVc1xCep(cls):
        cls._mapVc1x()
        cls._setupVc1xCep()
        cls._setupVc1xCepApsGroups()
        cls._setupVc1xCepHsGroups()
    
    def _numTestedApsGroups(self):
        return self.__class__.numTestedApsGroups()
    
    def _numTestedHsGroups(self):
        return self.__class__.numTestedHsGroups()
    
    def testEnableDisableApsGroup(self):
        for group_i in range(0, self._numTestedApsGroups()):
            groupId = group_i + 1
            
            def checkEnabled(enabled):
                expectedCellContent = "en" if enabled else "dis"
                cliResult = self.runCli("show debug pw group aps %d" % groupId)
                self.assertEqual(cliResult.cellContent(0, "Enable"), expectedCellContent, None)
                cliResult = self.runCli("show pw group aps %d" % groupId)
                self.assertEqual(cliResult.cellContent(0, "Enable"), expectedCellContent, None)
            
            def enable(enabled):
                enableCommand = "enable" if enabled else "disable"
                
                self.runCli("debug pw group aps %s %d" % (enableCommand, groupId))
                checkEnabled(enabled)
                self.runCli("pw group aps %s %d" % (enableCommand, groupId))
                checkEnabled(enabled)
                
            enable(True)
            enable(False)
    
    def testEnableDisableHsGroup(self):
        for group_i in range(0, self._numTestedHsGroups()):
            groupId = group_i + 1
            
            for labelSet in ["primary", "backup", "primary"]:
                def expectLabel(cellTitle, labelSet):
                    cliResult = self.runCli("show debug pw group hs %d" % groupId)
                    self.assertEqual(cliResult.cellContent(0, cellTitle), labelSet, None)
                    cliResult = self.runCli("show pw group hs %d" % groupId)
                    self.assertEqual(cliResult.cellContent(0, cellTitle), labelSet, None)
                    
                # RX
                self.runCli("debug pw group hs label rx %d %s" % (groupId, labelSet))
                expectLabel("Rx label", labelSet)
                self.runCli("pw group hs label rx %d %s" % (groupId, labelSet))
                expectLabel("Rx label", labelSet)
                
                # TX
                self.runCli("debug pw group hs label tx %d %s" % (groupId, labelSet))
                expectLabel("Tx label", labelSet)
                self.runCli("pw group hs label tx %d %s" % (groupId, labelSet))
                expectLabel("Tx label", labelSet)

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(AtCiscoUpsr)
    runner.summary()

if __name__ == '__main__':
    TestMain()

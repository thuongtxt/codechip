'''
Created on Jun 30, 2017

@author: namnn
'''
from test.AtTestCase import AtTestCase, AtTestCaseRunner
from python.arrive.atsdk.AtRegister import cBit6

class Tha60210051RtpActivateBuggy(AtTestCase):
    @classmethod
    def canRun(cls):
        return True if cls.productCode() == 0x60210051 else False
    
    def testChangeEthPortMustHaveRtpUpdateAtPwe(self):
        self.runCli("device init")
        self.runCli("sdh line rate 1 stm16")
        self.runCli("sdh line mode 1 sonet")
        self.runCli("sdh line loopback 1 release")
        self.runCli("sdh map aug16.1.1 4xaug4s")
        self.runCli("sdh map aug4.1.1 4xaug1s")
        self.runCli("sdh map aug1.1.16 3xvc3s")
        self.runCli("sdh map vc3.1.1.1 de3")
        self.runCli("pdh de3 framing 1.1.1 ds3_unframed")
        self.runCli("pw create satop 1")
        self.runCli("pw circuit bind 1 de3.1.1.1")
        self.runCli("pw psn 1 mpls")
        self.runCli("pw enable 1")
        self.runCli("pdh de3 timing 1.1.1 dcr 1-192")
        self.runCli("pw rtp enable 1")
        self.runCli("pw ethport 1 2")
        self.assertEqual(self.rd(0x80000) & cBit6, cBit6, None)
        self.runCli("pw ethport 1 none")
        self.runCli("pw ethport 1 1")
        self.assertEqual(self.rd(0x70000) & cBit6, cBit6, None)

class De1AutoAis(AtTestCase):
    def setUp(self):
        self.assertCliSuccess("device init")
        self.assertCliSuccess("sdh map vc3.1.1.1 7xtug2s")
        self.assertCliSuccess("sdh map tug2.1.1.1.1 tu11")
        self.assertCliSuccess("sdh map vc1x.1.1.1.1.1 de1")
        self.assertCliSuccess("pdh de1 framing 1.1.1.1.1 ds1_sf")
        self.assertCliSuccess("pdh de1 nxds0create 1.1.1.1.1 0-2")
        self.assertCliSuccess("pdh de1 nxds0create 1.1.1.1.1 3-4")
        self.assertCliSuccess("pdh de1 nxds0create 1.1.1.1.1 5-6")
        self.assertCliSuccess("pw create cesop 1-3 basic")
        self.assertCliSuccess("pw circuit bind 1 nxds0.1.1.1.1.1.0-2")
        self.assertCliSuccess("pw circuit bind 2 nxds0.1.1.1.1.1.3-4")
        self.assertCliSuccess("pw circuit bind 3 nxds0.1.1.1.1.1.5-6")
        self.assertCliSuccess("pw ethport 1-3 1")

    def autoAisIsEnabled(self):
        result = self.runCli("show pdh de1 autoalarm 1.1.1.1.1")
        cellContent = result.cellContent(0, "AIS")
        return True if cellContent == "en" else False

    def testAutoAisMustBeDisabledByDefault(self):
        self.assertFalse(self.autoAisIsEnabled())

    def testAutoAisMustNotBeTurnOnPwUnbinding(self):
        self.assertCliSuccess("pw circuit unbind 1")
        self.assertFalse(self.autoAisIsEnabled())

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(Tha60210051RtpActivateBuggy)
    runner.run(De1AutoAis)
    runner.summary()

if __name__ == '__main__':
    TestMain()
import time

from python.arrive.atsdk.cli import AtCliRunnable
from python.arrive.AtAppManager.AtColor import AtColor
from test.AtTestCase import AtTestCase, AtTestCaseRunner

class Profiler(AtCliRunnable):
    def __init__(self, channelCliIds):
        super(Profiler, self).__init__()
        self.channelCliIds = channelCliIds

    @staticmethod
    def _commandPrefix():
        return None

    def enable(self, enabled=True):
        cli = "%s profile failure %s %s" % (self._commandPrefix(), "start" if enabled else "stop", self.channelCliIds)
        result = self.runCli(cli)
        assert result.success()

    def _inject(self, commandToken, defects, status):
        defectString = "|".join(defects)
        statusString = "set" if status else "clear"
        cli = "debug %s profile %s inject %s %s %s" % (self._commandPrefix(), commandToken, self.channelCliIds, defectString, statusString)
        result = self.runCli(cli)
        assert result.success()

    def injectDefect(self, defects, status):
        """
        :type defects: list
        :type status: bool
        """
        for defect in defects:
            isDefect = self.isDefect(defect)
            if not isDefect:
                AtColor.printColor(AtColor.RED, "%s is not defect" % defect)
            assert isDefect

        self._inject("defect", defects, status)

    def injectFailure(self, failures, status):
        """
        :type failures: list
        :type status: bool
        """
        for failure in failures:
            isFailure = self.isFailure(failure)
            if not isFailure:
                AtColor.printColor(AtColor.RED, "%s is not failure" % failure)
            assert isFailure

        self._inject("failure", failures, status)

    def getResult(self, read2Clear=False, silent=False):
        """
        :type read2Clear: bool
        :type silent: bool
        :rtype: python.arrive.AtAppManager.AtCliResult.AtCliResult
        """
        clearString = "r2c" if read2Clear else "ro"
        silentString = "silent" if silent else ""
        cli = "show %s profile failure %s %s %s" % (self._commandPrefix(), self.channelCliIds, clearString, silentString)
        return self.runCli(cli)

    def getDefects(self):
        """
        :rtype: python.arrive.AtAppManager.AtCliResult.AtCliResult
        """
        cli = "show %s profile defect %s" % (self._commandPrefix(), self.channelCliIds)
        return self.runCli(cli)

    @classmethod
    def pathProfiler(cls, channelCliIds):
        return SdhPathProfiler(channelCliIds)

    @classmethod
    def lineProfiler(cls, channelCliIds):
        return SdhLineProfiler(channelCliIds)

    @classmethod
    def de1Profiler(cls, channelCliIds):
        return De1Profiler(channelCliIds)

    @classmethod
    def de3Profiler(cls, channelCliIds):
        return De3Profiler(channelCliIds)

    @classmethod
    def pwProfiler(cls, channelCliIds):
        return PwProfiler(channelCliIds)

    def isDefect(self, name):
        return True

    def isFailure(self, name):
        return True

class SdhPathProfiler(Profiler):
    @staticmethod
    def _commandPrefix():
        return "sdh path"

    def isDefect(self, name):
        return False if name in ["rfi", "rfi-s", "rfi-c", "rfi-p"] else True

    def isFailure(self, name):
        if name in ["rdi", "erdi-s", "erdi-c", "erdi-p"]:
            return False
        return True

class SdhLineProfiler(Profiler):
    @staticmethod
    def _commandPrefix():
        return "sdh line"

    def isDefect(self, name):
        return False if name == "rfi" else True

    def isFailure(self, name):
        return False if name == "rdi" else True

class De1Profiler(Profiler):
    @staticmethod
    def _commandPrefix():
        return "pdh de1"

class De3Profiler(Profiler):
    @staticmethod
    def _commandPrefix():
        return "pdh de3"

class PwProfiler(Profiler):
    @staticmethod
    def _commandPrefix():
        return "pw"

class AbstractTest(AtTestCase):
    _profiler = None

    @staticmethod
    def softwareNotifyDefects():
        return ["ais"]

    def setUp(self):
        super(AbstractTest, self).setUp()
        self.profiler().enable()

    def tearDown(self):
        self.profiler().getResult(read2Clear=True, silent=True)
        self.profiler().enable(enabled=False)

    @classmethod
    def setupDatapath(cls):
        assert 0

    @staticmethod
    def holdOffTimeMs():
        return 700

    @staticmethod
    def holdOnTimeMs():
        return 700

    @classmethod
    def setUpClass(cls):
        AtTestCase.setUpClass()
        cls.setupDatapath()
        cls.runCli("sur failure holdoff time %d" % cls.holdOffTimeMs())
        cls.runCli("sur failure holdon time %d" % cls.holdOnTimeMs())

    def failureTypeFromDefectType(self, defectType):
        """
        :rtype: str
        :type defectType: str
        """
        if defectType == "rdi":
            return "rfi"
        return defectType

    def defectTypeFromFailureType(self, failureType):
        """
        :rtype: str
        :type failureType: str
        """
        if failureType == "rfi":
            return "rdi"
        return failureType

    @staticmethod
    def channelCliId():
        return ""

    def channelCliIdDescription(self):
        return self.channelCliId()

    def _createProfiler(self):
        return None

    def profiler(self):
        """
        :rtype: Profiler
        """
        if self._profiler is None:
            self._profiler = self._createProfiler()
        return self._profiler

    @staticmethod
    def _allDefects():
        return list()

    @staticmethod
    def _allFailures():
        return list()

    def injectDefect(self, defects, status):
        """
        :type status: bool
        :type defects: list
        """
        self.profiler().injectDefect(defects, status)

    def injectFailure(self, failures, status):
        """
        :type status: bool
        :type failures: list
        """
        self.profiler().injectFailure(failures, status)

    def getResult(self, read2clear=False, silent=False):
        self.profiler().getDefects() # Just for debugging
        return self.profiler().getResult(read2Clear=read2clear, silent=silent)

    def getDefects(self):
        return self.profiler().getDefects()

    @staticmethod
    def stringToTime(timeString):
        """
        :type timeString: str
        """
        if timeString == "xxx":
            return 0.0

        [second, microSecond] = [int(string) for string in timeString.split(".")]
        return second + (microSecond / 1000000.0)

    def testAllDefectsInfoMustBeReset(self):
        result = self.profiler().getDefects()
        self.assertGreater(result.numRows(), 0)
        for row in range(result.numRows()):
            cellContent = result.cellContent(row, 0)
            defectName = cellContent.split(".")[-1]
            if defectName in self.softwareNotifyDefects():
                continue

            self.assertEqual(result.cellContent(row, "raiseTime"), "xxx")
            self.assertEqual(result.cellContent(row, "clearTime"), "xxx")
            self.assertEqual(result.cellContent(row, "currentStatus"), "xxx")

    def getDefectInfoFromCliResult(self, defectName, cliResult):
        rowName = "%s.%s" % (self.channelCliIdDescription(), defectName)

        cellContent = cliResult.cellContent(rowName, "currentStatus")
        currentStatus = True if cellContent == "set" else False
        raiseTime = self.stringToTime(cliResult.cellContent(rowName, "raiseTime"))
        clearTime = self.stringToTime(cliResult.cellContent(rowName, "clearTime"))
        return currentStatus, raiseTime, clearTime

    def assertSameTime(self, time1Seconds, time2Seconds):
        diffUs = abs(time1Seconds - time2Seconds) * 1000000.0
        almostEqual = diffUs < 1000
        if not almostEqual:
            self.fail("time1Seconds = %.6f, time2Seconds = %.6f, diff = %0.6f(us)" % (time1Seconds, time2Seconds, diffUs))
        self.assertLess(diffUs, 1000)  # 1ms because of CLI execution time is slow

    @staticmethod
    def shortDelay():
        time.sleep(0.001)

    def testDefectInjectMustWork(self):
        for defect in self._allDefects():
            for status in [True, False]:
                injectTime = time.time()
                self.injectDefect([defect], status=status)

                result = self.getDefects()
                currentStatus, raiseTime, clearTime = self.getDefectInfoFromCliResult(defect, result)
                self.assertEqual(currentStatus, status)
                reportTime = raiseTime if status else clearTime
                self.assertSameTime(injectTime, reportTime)

                if status:
                    self.assertEqual(clearTime, 0)
                else:
                    self.assertEqual(raiseTime, 0)

                # Give a moment to see different in times
                self.shortDelay()

        # Since no failure has been injected, there will be no failure reported
        result = self.getResult(read2clear=True)
        self.assertEqual(result.numRows(), 0)

    def testInjectDefectRaiseWithSameStatus_TimeMustBeUpdated(self):
        for defect in self._allDefects():
            self.injectDefect([defect], status=True)
            time.sleep(0.01)
            self.injectDefect([defect], status=True)
            injectTime = time.time()

            result = self.getDefects()
            currentStatus, raiseTime, clearTime = self.getDefectInfoFromCliResult(defect, result)
            self.assertSameTime(injectTime, raiseTime)

    def testInjectDefectClearWithSameStatus_TimeMustBeUpdated(self):
        for defect in self._allDefects():
            self.injectDefect([defect], status=False)
            time.sleep(0.01)
            self.injectDefect([defect], status=False)
            injectTime = time.time()

            result = self.getDefects()
            currentStatus, raiseTime, clearTime = self.getDefectInfoFromCliResult(defect, result)
            self.assertSameTime(injectTime, clearTime)

    def testFailureCanBeInjected_NoDefectsInjectedBefore(self):
        for failure in self._allFailures():
            for status in [True, False]:
                defectType = self.defectTypeFromFailureType(failure)
                injectTime = time.time()
                self.injectFailure([failure], status=status)
                result = self.getResult(read2clear=True)
                self.assertEqual(result.numRows(), 1)
                self.assertEqual(result.cellContent(0, "defectType"), defectType)

                if defectType not in self.softwareNotifyDefects():
                    self.assertEqual(result.cellContent(0, "defectRaiseTime"), "xxx")
                    self.assertEqual(result.cellContent(0, "defectClearTime"), "xxx")
                    self.assertEqual(result.cellContent(0, "defectStatus"), "xxx")

                self.assertEqual(result.cellContent(0, "failureType"), failure)
                self.assertSameTime(injectTime, self.stringToTime(result.cellContent(0, "failureReportTime")))
                self.assertEqual(result.cellContent(0, "failureStatus"), "set" if status else "clear")
                self.assertEqual(result.cellContent(0, "profiledTime(ms)"), "xxx")
                self.assertEqual(result.cellContent(0, "conclude"), "pass")

    @staticmethod
    def msleep(ms):
        time.sleep(ms / 1000.0)

    def TestConcludeWhenInjectingDefectWithHoldTime(self, status, holdTime, expectedConclude):
        for defect in self._allDefects():
            failureType = self.failureTypeFromDefectType(defect)
            defectInjectTime = time.time()
            self.injectDefect([defect], status=status)
            self.msleep(holdTime)
            failureInjectTime = time.time()
            self.injectFailure([failureType], status=status)
            result = self.getResult(read2clear=True)
            statusString = "set" if status else "clear"

            self.assertEqual(result.cellContent(0, "defectType"), defect)

            if status:
                self.assertSameTime(defectInjectTime, self.stringToTime(result.cellContent(0, "defectRaiseTime")))
                self.assertEqual(result.cellContent(0, "defectClearTime"), "xxx")
            else:
                self.assertEqual(result.cellContent(0, "defectRaiseTime"), "xxx")
                self.assertSameTime(defectInjectTime, self.stringToTime(result.cellContent(0, "defectClearTime")))

            self.assertEqual(result.cellContent(0, "defectStatus"), statusString)
            self.assertEqual(result.cellContent(0, "failureType"), failureType)
            self.assertSameTime(failureInjectTime, self.stringToTime(result.cellContent(0, "failureReportTime")))
            self.assertEqual(result.cellContent(0, "failureStatus"), statusString)

            expectedProfiledTimeSeconds = failureInjectTime - defectInjectTime
            profiledTimeSeconds = float(result.cellContent(0, "profiledTime(ms)")) / 1000.0
            self.assertSameTime(expectedProfiledTimeSeconds, profiledTimeSeconds)

            self.assertEqual(result.cellContent(0, "conclude"), expectedConclude)

    def testPassWhenInjectingDefectWithinHoldOffTime(self):
        self.TestConcludeWhenInjectingDefectWithHoldTime(status=True, holdTime=self.holdOffTimeMs(),
                                                         expectedConclude="pass")

    def testPassWhenInjectingDefectWithinHoldOnTime(self):
        self.TestConcludeWhenInjectingDefectWithHoldTime(status=False, holdTime=self.holdOnTimeMs(),
                                                         expectedConclude="pass")

    def testFailWhenInjectingDefectNotWithinHoldOffTime_TooQuick(self):
        self.TestConcludeWhenInjectingDefectWithHoldTime(status=True, holdTime=self.holdOffTimeMs() - 600,
                                                         expectedConclude="fail")

    def testFailWhenInjectingDefectNotWithinHoldOffTime_TooSlow(self):
        self.TestConcludeWhenInjectingDefectWithHoldTime(status=True, holdTime=self.holdOffTimeMs() + 600,
                                                         expectedConclude="fail")

    def testFailWhenInjectingDefectWithinHoldOnTime_TooQuick(self):
        self.TestConcludeWhenInjectingDefectWithHoldTime(status=False, holdTime=self.holdOnTimeMs() - 600,
                                                         expectedConclude="fail")

    def testFailWhenInjectingDefectWithinHoldOnTime_TooSlow(self):
        self.TestConcludeWhenInjectingDefectWithHoldTime(status=False, holdTime=self.holdOnTimeMs() + 600,
                                                         expectedConclude="fail")

class SdhChannelTest(AbstractTest):
    @classmethod
    def setupDatapath(cls):
        cls.runCli("device init")

    def _createProfiler(self):
        return Profiler.pathProfiler(self.channelCliId())

class LineTest(SdhChannelTest):
    @classmethod
    def hasLineFramer(cls):
        productCode = cls.productCode()
        if productCode in [0x60210011, 0x60210051, 0x60210012]:
            return False
        return True

    @classmethod
    def canRun(cls):
        return cls.hasLineFramer()

    @staticmethod
    def _allDefects():
        return ["los",
                "lof",
                "tim",
                "ais",
                "rdi"]

    @staticmethod
    def _allFailures():
        return ["los",
                "lof",
                "tim",
                "ais",
                "rfi"]

    def _createProfiler(self):
        return Profiler.lineProfiler(self.channelCliId())

    @staticmethod
    def channelCliId():
        return "1"

    def channelCliIdDescription(self):
        return "line.%s" % LineTest.channelCliId()

class PathTest(SdhChannelTest):
    @staticmethod
    def _allFailures():
        return ["ais",
                "lop",
                "tim",
                "uneq",
                "plm",
                "ber-sd",
                "ber-sf",
                "lom",
                "rfi",
                "rfi-s",
                "rfi-c",
                "rfi-p"]

    @staticmethod
    def _allDefects():
        return ["ais",
                "lop",
                "tim",
                "uneq",
                "plm",
                "ber-sd",
                "ber-sf",
                "lom",
                "rdi",
                "erdi-s",
                "erdi-c",
                "erdi-p"]

    def failureTypeFromDefectType(self, defectType):
        """
        :rtype: str
        :type defectType: str
        """
        if defectType == "erdi-s":
            return "rfi-s"
        if defectType == "erdi-c":
            return "rfi-c"
        if defectType == "erdi-p":
            return "rfi-p"

        return super(PathTest, self).failureTypeFromDefectType(defectType)

    def defectTypeFromFailureType(self, failureType):
        """
        :rtype: str
        :type failureType: str
        """
        if failureType == "rfi-s":
            return "erdi-s"
        if failureType == "rfi-p":
            return "erdi-p"
        if failureType == "rfi-c":
            return "erdi-c"

        return super(PathTest, self).defectTypeFromFailureType(failureType)

    @staticmethod
    def channelCliId():
        return "vc3.1.1.1"

class De1Test(AbstractTest):
    @classmethod
    def setupDatapath(cls):
        cls.runCli("device init")
        cls.runCli("sdh map vc3.1.1.1 7xtug2s")
        cls.runCli("sdh map tug2.1.1.1.1 tu11")
        cls.runCli("sdh map vc1x.1.1.1.1.1 de1")
        cls.runCli("pdh de1 framing 1.1.1.1.1 ds1_sf")

    @staticmethod
    def _allDefects():
        return ["los",
                "lof",
                "ais",
                "rai",
                "lomf",
                "ber-sf",
                "ber-sd",
                "siglof",
                "sigrai",
                "ebit"]

    @staticmethod
    def _allFailures():
        return ["los",
                "lof",
                "ais",
                "rai",
                "ais-ci",
                "rai-ci"]

    def _createProfiler(self):
        return Profiler.de1Profiler(self.channelCliId())

    @staticmethod
    def channelCliId():
        return "1.1.1.1.1"

    def channelCliIdDescription(self):
        return "de1.%s" % self.channelCliId()

class De3Test(AbstractTest):
    @classmethod
    def setupDatapath(cls):
        cls.runCli("device init")
        cls.runCli("sdh map vc3.1.1.1 de3")
        cls.runCli("pdh de3 framing 1.1.1 ds3_cbit_unchannelize")

    @staticmethod
    def _allDefects():
        return ["los",
                "lof",
                "ais",
                "rai"]

    @staticmethod
    def _allFailures():
        return ["los",
                "lof",
                "ais",
                "rai"]

    def _createProfiler(self):
        return Profiler.de1Profiler(self.channelCliId())

    @staticmethod
    def channelCliId():
        return "1.1.1"

    def channelCliIdDescription(self):
        return "de3.%s" % self.channelCliId()

def TestMain():
    runner = AtTestCaseRunner.runner()
    #runner.run(PathTest)
    #runner.run(LineTest)
    runner.run(De1Test)
    #runner.run(De3Test)
    runner.summary()

if __name__ == '__main__':
    TestMain()

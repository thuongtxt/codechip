from test.AtTestCase import AtTestCase, AtTestCaseRunner


class VcMapConstrainTest(AtTestCase):
    @staticmethod
    def allVcMappings():
        return ["c4_64c",
                "c4_16c",
                "c4_4c",
                "c4",
                "3xtug3s",
                "c3",
                "7xtug2s",
                "de3",
                "c1x",
                "de1",
                "none"]

    def testVc4_16c(self):
        self.assertCliSuccess("device init")
        self.assertCliSuccess("sdh map aug16.1.1 vc4_16c")
        for mapType in self.allVcMappings():
            cli = "sdh map vc4_16c.1.1 %s" % mapType
            if mapType in ["c4_16c", "none"]:
                self.assertCliSuccess(cli)
            else:
                self.assertCliFailure(cli)

    def testVc4_4c(self):
        self.assertCliSuccess("device init")
        self.assertCliSuccess("sdh map aug16.1.1 4xaug4s")
        self.assertCliSuccess("sdh map aug4.1.1 vc4_4c")
        for mapType in self.allVcMappings():
            cli = "sdh map vc4_4c.1.1 %s" % mapType
            if mapType in ["c4_4c", "none"]:
                self.assertCliSuccess(cli)
            else:
                self.assertCliFailure(cli)

    def setupVc4Mapping(self, lineMode):
        self.assertCliSuccess("device init")
        self.assertCliSuccess("sdh line mode 1 %s" % lineMode)
        self.assertCliSuccess("sdh map aug16.1.1 4xaug4s")
        self.assertCliSuccess("sdh map aug4.1.1 4xaug1s")
        self.assertCliSuccess("sdh map aug1.1.1 vc4")

    def testVc4Sonet(self):
        self.setupVc4Mapping(lineMode="sonet")
        for mapType in self.allVcMappings():
            cli = "sdh map vc4.1.1 %s" % mapType
            if mapType in ["c4", "none"]:
                self.assertCliSuccess(cli)
            else:
                self.assertCliFailure(cli)

    def testVc4Sdh(self):
        self.setupVc4Mapping(lineMode="sdh")
        for mapType in self.allVcMappings():
            cli = "sdh map vc4.1.1 %s" % mapType
            if mapType in ["c4", "none", "3xtug3s"]:
                self.assertCliSuccess(cli)
            else:
                self.assertCliFailure(cli)

    def testTug3Vc3(self):
        self.setupVc4Mapping(lineMode="sdh")
        self.assertCliSuccess("sdh map vc4.1.1 3xtug3s")
        self.assertCliSuccess("sdh map tug3.1.1.1 vc3")
        for mapType in self.allVcMappings():
            cli = "sdh map vc3.1.1.1 %s" % mapType
            if mapType in ["c3", "de3", "none"]:
                self.assertCliSuccess(cli)
            else:
                self.assertCliFailure(cli)

    def setupAu3Mapping(self):
        self.assertCliSuccess("device init")
        self.assertCliSuccess("sdh map aug16.1.1 4xaug4s")
        self.assertCliSuccess("sdh map aug4.1.1 4xaug1s")
        self.assertCliSuccess("sdh map aug1.1.1 3xvc3s")

    def testAu3Vc3(self):
        self.setupAu3Mapping()

        for mapType in self.allVcMappings():
            cli = "sdh map vc3.1.1.1 %s" % mapType
            if mapType in ["c3", "7xtug2s", "de3", "none"]:
                self.assertCliSuccess(cli)
            else:
                self.assertCliFailure(cli)

    def TestVc1x(self, tu1xMapType):
        self.setupAu3Mapping()
        self.assertCliSuccess("sdh map vc3.1.1.1 7xtug2s")
        self.assertCliSuccess("sdh map tug2.1.1.1.1 %s" % tu1xMapType)

        for mapType in self.allVcMappings():
            cli = "sdh map vc1x.1.1.1.1.1 %s" % mapType
            if mapType in ["c1x", "de1", "none"]:
                self.assertCliSuccess(cli)
            else:
                self.assertCliFailure(cli)

    def testVc11(self):
        self.TestVc1x(tu1xMapType="tu11")

    def testVc12(self):
        self.TestVc1x(tu1xMapType="tu12")

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(VcMapConstrainTest)
    runner.summary()

if __name__ == '__main__':
    TestMain()
from test.AtTestCase import AtTestCase, AtTestCaseRunner
import python.arrive.atsdk.AtRegister as AtRegister

class Mapper(AtTestCase):
    @classmethod
    def mapVc4_16c(cls):
        cls.classRunCli("sdh map aug16.1.1-4.1 vc4_16c")
        cls.classRunCli("sdh map vc4_16c.1.1-4.1 c4_16c")
        cls.classRunCli("sdh path interruptmask vc4_16c.1.1-4.1 lop|ais en")
        cls.classRunCli("sdh path force alarm vc4_16c.1.1-4.1 uneq tx en")
        cls.classRunCli("sdh path force alarm au4_16c.1.1-4.1 ais tx en")
        cls.classRunCli("sdh path force alarm au4_16c.1.1-4.1 ais rx en")
        
    @classmethod
    def mapVc4_4c(cls):
        cls.classRunCli("sdh map aug16.1.1-4.1 4xaug4s")
        cls.classRunCli("sdh map aug4.1.1-4.4 vc4_4c")
        cls.classRunCli("sdh map vc4_4c.1.1-4.4 c4_4c")
        cls.classRunCli("sdh path interruptmask vc4_4c.1.1-4.4 lop|ais en")
        cls.classRunCli("sdh path force alarm vc4_4c.1.1-4.4 uneq tx en")
        cls.classRunCli("sdh path force alarm au4_4c.1.1-4.4 ais tx en")
        cls.classRunCli("sdh path force alarm au4_4c.1.1-4.4 ais rx en")
        
    @classmethod
    def mapAug1_vc4(cls):
        cls.classRunCli("sdh map aug16.1.1-4.1 4xaug4s")
        cls.classRunCli("sdh map aug4.1.1-4.4 4xaug1s")
        cls.classRunCli("sdh map aug1.1.1-4.16 vc4")
        cls.classRunCli("sdh path interruptmask vc4.1.1-4.16 lop|ais en")
        cls.classRunCli("sdh path force alarm vc4.1.1-4.16 uneq tx en")
        cls.classRunCli("sdh path force alarm au4.1.1-4.16 ais tx en")
        cls.classRunCli("sdh path force alarm au4.1.1-4.16 ais rx en")
        
    @classmethod
    def mapAug1_vc3(cls):
        cls.classRunCli("sdh map aug16.1.1-4.1 4xaug4s")
        cls.classRunCli("sdh map aug4.1.1-4.4 4xaug1s")
        cls.classRunCli("sdh map aug1.1.1-4.16 3xvc3s")
        cls.classRunCli("sdh path interruptmask vc3.1.1.1-4.16.3 lop|ais en")
        cls.classRunCli("sdh path force alarm vc3.1.1.1-4.16.3 uneq tx en")
        cls.classRunCli("sdh path force alarm au3.1.1.1-4.16.3 ais tx en")
        cls.classRunCli("sdh path force alarm au3.1.1.1-4.16.3 ais rx en")
        
    @classmethod
    def mapAug1_vc3_de3(cls):
        cls.classRunCli("sdh map aug16.1.1-4.1 4xaug4s")
        cls.classRunCli("sdh map aug4.1.1-4.4 4xaug1s")
        cls.classRunCli("sdh map aug1.1.1-4.16 3xvc3s")
        cls.classRunCli("sdh map vc3.1.1.1-4.16.3 de3")
        cls.classRunCli("pdh de3 framing 1.1.1-4.16.3 ds3_cbit_28ds1")
        cls.classRunCli("pdh de3 interruptmask 1.1.1-4.16.3 ais|lof en")
        cls.classRunCli("pdh de3 force alarm 1.1.1-4.16.3 tx ais en")
        cls.classRunCli("pdh de3 force alarm 1.1.1-4.16.3 rx ais en")
        
    @classmethod
    def mapAug1_vc3_de3_de1(cls):
        cls.classRunCli("sdh map aug16.1.1-4.1 4xaug4s")
        cls.classRunCli("sdh map aug4.1.1-4.4 4xaug1s")
        cls.classRunCli("sdh map aug1.1.1-4.16 3xvc3s")
        cls.classRunCli("sdh map vc3.1.1.1-4.16.3 de3")
        cls.classRunCli("pdh de3 framing 1.1.1-4.16.3 ds3_cbit_28ds1")
        cls.classRunCli("pdh de1 framing 1.1.1.1.1-4.16.3.7.4 ds1_unframed")
        cls.classRunCli("pdh de1 interruptmask 1.1.1.1.1-4.16.3.7.4 ais en")
        cls.classRunCli("pdh de1 force alarm 1.1.1.1.1-4.16.3.7.4 tx ais en")
        cls.classRunCli("pdh de1 force alarm 1.1.1.1.1-4.16.3.7.4 rx ais en")
        
    @classmethod
    def mapAug1_vc4_tu3(cls):
        cls.classRunCli("sdh map aug16.1.1-4.1 4xaug4s")
        cls.classRunCli("sdh map aug4.1.1-4.4 4xaug1s")
        cls.classRunCli("sdh map aug1.1.1-4.16 vc4")
        cls.classRunCli("sdh map vc4.1.1-4.16 3xtug3s")
        cls.classRunCli("sdh map tug3.1.1.1-4.16.3 vc3")
        
        cls.classRunCli("sdh path interruptmask vc4.1.1-4.16 lop|ais en")
        cls.classRunCli("sdh path force alarm vc4.1.1-4.16 uneq tx en")
        cls.classRunCli("sdh path force alarm au4.1.1-4.16 ais tx en")
        cls.classRunCli("sdh path force alarm au4.1.1-4.16 ais rx en")
        
        cls.classRunCli("sdh path interruptmask tu3.1.1.1-4.16.3 lop|ais en")
        cls.classRunCli("sdh path force alarm vc3.1.1.1-4.16.3 uneq tx en")
        cls.classRunCli("sdh path force alarm tu3.1.1.1-4.16.3 ais tx en")
        cls.classRunCli("sdh path force alarm tu3.1.1.1-4.16.3 ais rx en")
        
    @classmethod
    def mapAug1_vc4_tu11(cls):
        cls.classRunCli("sdh map aug16.1.1-4.1 4xaug4s")
        cls.classRunCli("sdh map aug4.1.1-4.4 4xaug1s")
        cls.classRunCli("sdh map aug1.1.1-4.16 vc4")
        cls.classRunCli("sdh map vc4.1.1-4.16 3xtug3s")
        cls.classRunCli("sdh map tug3.1.1.1-4.16.3 7xtug2s")
        cls.classRunCli("sdh map tug2.1.1.1.1-4.16.3.7 tu11")
        cls.classRunCli("sdh map vc1x.1.1.1.1.1-4.16.3.7.4 c1x")
        
        cls.classRunCli("sdh path interruptmask vc4.1.1-4.16 lop|ais en")
        cls.classRunCli("sdh path force alarm vc4.1.1-4.16 uneq tx en")
        cls.classRunCli("sdh path force alarm au4.1.1-4.16 ais tx en")
        cls.classRunCli("sdh path force alarm au4.1.1-4.16 ais rx en")
        
        cls.classRunCli("sdh path interruptmask tu1x.1.1.1.1.1-4.16.3.7.4 lop|ais en")
        cls.classRunCli("sdh path force alarm vc1x.1.1.1.1.1-4.16.3.7.4 uneq tx en")
        cls.classRunCli("sdh path force alarm tu1x.1.1.1.1.1-4.16.3.7.4 ais tx en")
        cls.classRunCli("sdh path force alarm tu1x.1.1.1.1.1-4.16.3.7.4 ais rx en")
        
    @classmethod
    def mapAug1_vc3_tu11(cls):
        cls.classRunCli("sdh map aug16.1.1-4.1 4xaug4s")
        cls.classRunCli("sdh map aug4.1.1-4.4 4xaug1s")
        cls.classRunCli("sdh map aug1.1.1-4.16 3xvc3s")
        cls.classRunCli("sdh map vc3.1.1.1-4.16.3 7xtug2s")
        cls.classRunCli("sdh map tug2.1.1.1.1-4.16.3.7 tu11")
        cls.classRunCli("sdh map vc1x.1.1.1.1.1-4.16.3.7.4 c1x")
        
        cls.classRunCli("sdh path interruptmask vc3.1.1.1-4.16.3 lop|ais en")
        cls.classRunCli("sdh path force alarm vc3.1.1.1-4.16.3 uneq tx en")
        cls.classRunCli("sdh path force alarm au3.1.1.1-4.16.3 ais tx en")
        cls.classRunCli("sdh path force alarm au3.1.1.1-4.16.3 ais rx en")
        
        cls.classRunCli("sdh path interruptmask tu1x.1.1.1.1.1-4.16.3.7.4 lop|ais en")
        cls.classRunCli("sdh path force alarm vc1x.1.1.1.1.1-4.16.3.7.4 uneq tx en")
        cls.classRunCli("sdh path force alarm tu1x.1.1.1.1.1-4.16.3.7.4 ais tx en")
        cls.classRunCli("sdh path force alarm tu1x.1.1.1.1.1-4.16.3.7.4 ais rx en")
        
    @classmethod
    def mapAug1_vc3_vc1x_de1(cls):
        cls.classRunCli("sdh map aug16.1.1-4.1 4xaug4s")
        cls.classRunCli("sdh map aug4.1.1-4.4 4xaug1s")
        cls.classRunCli("sdh map aug1.1.1-4.16 3xvc3s")
        cls.classRunCli("sdh map vc3.1.1.1-4.16.3 7xtug2s")
        cls.classRunCli("sdh map tug2.1.1.1.1-4.16.3.7 tu11")
        cls.classRunCli("sdh map vc1x.1.1.1.1.1-4.16.3.7.4 de1")
        
        cls.classRunCli("pdh de1 interruptmask 1.1.1.1.1-4.16.3.7.4 ais|lof en")
        cls.classRunCli("pdh de1 force alarm 1.1.1.1.1-4.16.3.7.4 tx ais en")
        cls.classRunCli("pdh de1 force alarm 1.1.1.1.1-4.16.3.7.4 rx ais en")

class RegisterProvider(object):
    @classmethod
    def rd(cls, address):
        return atsdk.rd(address, 0)
    
    @classmethod
    def _pohBaseAddress(cls):
        return 0x200000
    
    @classmethod
    def _pohInterruptBase(cls):
        return 0
    
    @classmethod
    def _slice24(cls, hwSlice, hwSts):
        return (hwSlice << 1) | (hwSts & 1)
    
    @classmethod
    def _sts1InSlice24(cls, hwSlice, hwSts):
        return hwSts >> 1
    
    @classmethod
    def _pohInterruptOffset(cls, hwSlice, hwSts, hwVtg, hwVt):
        return 0
    
    @classmethod
    def _interruptAddress(cls, hwSlice, hwSts, hwVtg, hwVt):
        baseAddress = cls._pohBaseAddress() + cls._pohInterruptBase()
        return baseAddress + cls._pohInterruptOffset(hwSlice, hwSts, hwVtg, hwVt)
    
    @classmethod
    def _assertEqual(cls, value, expect, message=None):
        if value == expect:
            return
        
        if message:
            raise Exception("%s: Expect %d, actual: %d" % (message, expect, value))
        else:
            raise Exception("Expect %d, actual: %d" % (expect, value))
    
    @classmethod
    def _ocnOffset(cls, hwSlice, hwSts, hwVtg, hwVt):
        return None
    
    @classmethod
    def _ocnPgBaseAddress(cls):
        return None
    
    @classmethod
    def _ocnPiBaseAddress(cls):
        return None
    
    @classmethod
    def _interruptMaskField(cls):
        return AtRegister.cBit1_0
    
    @classmethod
    def checkInterruptMask(cls, expectSet, hwSlice, hwSts, hwVtg, hwVt):
        address = cls._interruptAddress(hwSlice, hwSts, hwVtg, hwVt)
        value = cls.rd(address) & cls._interruptMaskField()
        expectedValue = cls._interruptMaskField() if expectSet else 0
        cls._assertEqual(value, expectedValue, None)
    
    @classmethod
    def _rxAisForceBit(cls):
        return None
    
    @classmethod
    def checkAlarmForce(cls, expectedForced, hwSlice, hwSts, hwVtg, hwVt):
        expect = 1 if expectedForced else 0
        offset = cls._ocnOffset(hwSlice, hwSts, hwVtg, hwVt)
        
        txAddress = cls._ocnPgBaseAddress() + offset
        reg = AtRegister.AtRegister(cls.rd(txAddress))
        cls._assertEqual(reg.getBit(4), expect, None)
        cls._assertEqual(reg.getBit(5), expect, None)
        
        rxAddress = cls._ocnPiBaseAddress() + offset
        reg = AtRegister.AtRegister(cls.rd(rxAddress))
        cls._assertEqual(reg.getBit(cls._rxAisForceBit()), expect, None)
    
class HoRegisterProvider(RegisterProvider):
    @classmethod
    def _pohInterruptOffset(cls, hwSlice, hwSts, hwVtg, hwVt):
        return cls._slice24(hwSlice, hwSts) * 128 + cls._sts1InSlice24(hwSlice, hwSts)
    
    @classmethod
    def _pohInterruptBase(cls):
        return 0xd0000
    
    @classmethod
    def _ocnOffset(cls, hwSlice, hwSts, hwVtg, hwVt):
        return (512 * hwSlice) + hwSts
    
    @classmethod
    def _ocnPgBaseAddress(cls):
        return 0x00123000
    
    @classmethod
    def _ocnPiBaseAddress(cls):
        return 0x00122000

    @classmethod
    def _rxAisForceBit(cls):
        return 11

class LoRegisterProvider(RegisterProvider):
    @classmethod
    def _pohInterruptBase(cls):
        return 0x0E0000
    
    @classmethod
    def _pohInterruptOffset(cls, hwSlice, hwSts, hwVtg, hwVt):
        return (cls._slice24(hwSlice, hwSts) * 4096) + (cls._sts1InSlice24(hwSlice, hwSts) * 32)
    
    @classmethod
    def _ocnOffset(cls, hwSlice, hwSts, hwVtg, hwVt):
        return (16384 * hwSlice) + (32 * hwSts) + (4 * hwVtg) + hwVt
    
    @classmethod
    def _ocnPgBaseAddress(cls):
        return 0x00160800
    
    @classmethod
    def _ocnPiBaseAddress(cls):
        return 0x00140800

    @classmethod
    def _rxAisForceBit(cls):
        return 4

class De3RegisterProvider(LoRegisterProvider):
    @classmethod
    def _slice24Offset(cls, slice24):
        return (0x1000000 + (slice24 * 0x100000)) - 0x700000
    
    @classmethod
    def _pdhOffset(cls, hwSlice, hwSts, hwVtg = 0, hwVt = 0):
        stsInSlice24 = cls._sts1InSlice24(hwSlice, hwSts)
        sliceOffset = cls._slice24Offset(cls._slice24(hwSlice, hwSts)) 
        return stsInSlice24 + sliceOffset
    
    @classmethod
    def _interruptAddress(cls, hwSlice, hwSts, hwVtg, hwVt):
        return 0x00740580 + cls._pdhOffset(hwSlice, hwSts)
    
    @classmethod
    def _interruptMaskField(cls):
        return AtRegister.cBit2
    
    @classmethod
    def _txAisForced(cls, hwSlice, hwSts, hwVtg, hwVt):
        address = 0x780000 + cls._pdhOffset(hwSlice, hwSts)
        value = cls.rd(address)
        if value & (AtRegister.cBit4 | AtRegister.cBit29):
            return True
        
        return False
        
    @classmethod
    def _rxAisForced(cls, hwSlice, hwSts, hwVtg, hwVt):
        address = 0x00740000 + cls._pdhOffset(hwSlice, hwSts)
        value = cls.rd(address)
        if value & (AtRegister.cBit6 | AtRegister.cBit7 | AtRegister.cBit19):
            return True
        
        return False
        
    @classmethod
    def checkAlarmForce(cls, expectedForced, hwSlice, hwSts, hwVtg, hwVt):
        cls._assertEqual(cls._txAisForced(hwSlice, hwSts, hwVtg, hwVt), expectedForced, None)
        cls._assertEqual(cls._rxAisForced(hwSlice, hwSts, hwVtg, hwVt), expectedForced, None)

class De1RegisterProvider(De3RegisterProvider):
    @classmethod
    def _pdhOffset(cls, hwSlice, hwSts, hwVtg=0, hwVt=0):
        sliceOffset = cls._slice24Offset(cls._slice24(hwSlice, hwSts))
        flatId = (cls._sts1InSlice24(hwSlice, hwSts) * 32) + (hwVtg * 4) + hwVt
        return  flatId  + sliceOffset
    
    @classmethod
    def _interruptAddress(cls, hwSlice, hwSts, hwVtg, hwVt):
        return 0x755000 + cls._pdhOffset(hwSlice, hwSts, hwVtg, hwVt) 
    
    @classmethod
    def _txAisForced(cls, hwSlice, hwSts, hwVtg, hwVt):
        address = 0x00794000 + cls._pdhOffset(hwSlice, hwSts, hwVtg, hwVt)
        value = cls.rd(address)
        if value & (AtRegister.cBit20 | AtRegister.cBit24):
            return True
        return False
    
    @classmethod
    def _rxAisForced(cls, hwSlice, hwSts, hwVtg, hwVt):
        address = 0x00754000 + cls._pdhOffset(hwSlice, hwSts, hwVtg, hwVt)
        value = cls.rd(address)
        if value & AtRegister.cBit30_28:
            return True
        return False
    
    @classmethod
    def checkAlarmForce(cls, expectedForced, hwSlice, hwSts, hwVtg, hwVt):
        cls._assertEqual(cls._txAisForced(hwSlice, hwSts, hwVtg, hwVt), expectedForced, None)
        cls._assertEqual(cls._rxAisForced(hwSlice, hwSts, hwVtg, hwVt), expectedForced, None)
    
class HardwareCleanup(AtTestCase):
    def setUp(self):
        self.runCli("device show readwrite dis dis")
        self.runCli("device init")
    
    @staticmethod
    def _assertAlarmForced(forced, hwStsIds, hwVtgs, hwVts, regProvider):
        for hwSliceId in range(0, 4):
            for hwSts in hwStsIds:
                for hwVtg in hwVtgs:
                    for hwVt in hwVts:
                        regProvider.checkAlarmForce(forced, hwSliceId, hwSts, hwVtg, hwVt)
        
    @staticmethod
    def _assertMaskSet(maskSet, hwStsIds, hwVtgs, hwVts, regProvider):
        for hwSliceId in range(0, 4):
            for hwSts in hwStsIds:
                for hwVtg in hwVtgs:
                    for hwVt in hwVts:
                        regProvider.checkInterruptMask(maskSet, hwSliceId, hwSts, hwVtg, hwVt)
        
    def _assertHoAlarmForced(self, forced, hwStsIds):
        self._assertAlarmForced(forced, hwStsIds, hwVtgs=[0], hwVts=[0], regProvider=HoRegisterProvider)
    
    def _assertHoMaskSet(self, maskSet, hwStsIds):
        hwVtgs = hwVts = [0]
        self._assertMaskSet(maskSet, hwStsIds, hwVtgs, hwVts, HoRegisterProvider)
        
    def assertVc4_16cMaskSet(self, maskSet):
        self._assertHoMaskSet(maskSet, hwStsIds=[0])

    def assertVc4_16cAlarmForced(self, forced):
        self._assertHoAlarmForced(forced, hwStsIds=[0])
            
    def assertVc4_4cMaskSet(self, maskSet):
        self._assertHoMaskSet(maskSet, hwStsIds=[0, 4, 8, 12])

    def assertVc4_4cAlarmForced(self, forced):
        self._assertHoAlarmForced(forced, hwStsIds=[0, 4, 8, 12])

    def assertVc4MaskSet(self, maskSet):
        self._assertHoMaskSet(maskSet, hwStsIds=range(0, 16))
        
    def assertVc4AlarmForced(self, forced):
        self._assertHoAlarmForced(forced, hwStsIds=range(0, 16))
        
    def assertVc3MaskSet(self, maskSet):
        self._assertHoMaskSet(maskSet, hwStsIds=range(0, 48))
        
    def assertVc3AlarmForced(self, forced):
        self._assertHoAlarmForced(forced, hwStsIds=range(0, 48))

    def assertTu3MaskSet(self, maskSet):
        hwVtgs = hwVts = [0]
        self._assertMaskSet(maskSet, hwStsIds=range(0, 48), hwVtgs=hwVtgs, hwVts=hwVts, regProvider=LoRegisterProvider)

    def assertTu3AlarmForced(self, forced):
        self._assertAlarmForced(forced, hwStsIds=range(0, 48), hwVtgs=[0], hwVts=[0], regProvider=LoRegisterProvider)
        
    def assertTu11MaskSet(self, maskSet):
        hwVtgs = range(0, 7) 
        hwVts = range(0, 4)
        self._assertMaskSet(maskSet, hwStsIds=range(0, 48), hwVtgs=hwVtgs, hwVts=hwVts, regProvider=LoRegisterProvider)

    def assertTu11AlarmForced(self, forced):
        hwVtgs = range(0, 7) 
        hwVts = range(0, 4)
        self._assertAlarmForced(forced, hwStsIds=range(0, 48), hwVtgs=hwVtgs, hwVts=hwVts, regProvider=LoRegisterProvider)
        
    def assertDe3MaskSet(self, maskSet):
        hwVtgs = hwVts = [0]
        self._assertMaskSet(maskSet, hwStsIds=range(0, 48), hwVtgs=hwVtgs, hwVts=hwVts, regProvider=De3RegisterProvider)

    def assertDe3AlarmForced(self, forced):
        self._assertAlarmForced(forced, hwStsIds=range(0, 48), hwVtgs=[0], hwVts=[0], regProvider=De3RegisterProvider)
        
    def assertDe1MaskSet(self, maskSet):
        hwVtgs = range(0, 7) 
        hwVts = range(0, 4)
        self._assertMaskSet(maskSet, hwStsIds=range(0, 48), hwVtgs=hwVtgs, hwVts=hwVts, regProvider=De1RegisterProvider)

    def assertDe1AlarmForced(self, forced):
        hwVtgs = range(0, 7) 
        hwVts = range(0, 4)
        self._assertAlarmForced(forced, hwStsIds=range(0, 48), hwVtgs=hwVtgs, hwVts=hwVts, regProvider=De1RegisterProvider)
    
class Aug16HardwareCleanup(HardwareCleanup):
    def testMapFromVc4_16cToAug4(self):
        Mapper.mapVc4_16c()
        self.assertVc4_16cMaskSet(True)
        self.assertVc4_16cAlarmForced(True)
        self.runCli("sdh map aug16.1.1-4.1 4xaug4s")
        self.assertVc4_16cMaskSet(False)
        self.assertVc4_16cAlarmForced(False)

    def testMapFromAug4ToVc4_16c(self):
        Mapper.mapVc4_4c()
        self.assertVc4_4cMaskSet(True)
        self.assertVc4_4cAlarmForced(True)
        self.runCli("sdh map aug16.1.1-4.1 vc4_16c")
        self.assertVc4_4cMaskSet(False)
        self.assertVc4_4cAlarmForced(False)

class Aug4HardwareCleanup(HardwareCleanup):
    def testMapFromVc4_4cToAug1(self):
        Mapper.mapVc4_4c()
        self.assertVc4_4cMaskSet(True)
        self.assertVc4_4cAlarmForced(True)
        self.runCli("sdh map aug4.1.1-4.4 4xaug1s")
        self.assertVc4_4cMaskSet(False)
        self.assertVc4_4cAlarmForced(False)

    def testMapFromAug1Vc4ToVc4_4c(self):
        Mapper.mapAug1_vc4()
        self.assertVc4MaskSet(True)
        self.assertVc4AlarmForced(True)
        self.runCli("sdh map aug4.1.1-4.4 vc4_4c")
        self.assertVc4MaskSet(False)
        self.assertVc4AlarmForced(False)
        
    def testMapFromAug1Vc3ToVc4_4c(self):
        Mapper.mapAug1_vc3()
        self.assertVc3MaskSet(True)
        self.assertVc3AlarmForced(True)
        self.runCli("sdh map aug4.1.1-4.4 vc4_4c")
        self.assertVc3MaskSet(False)
        self.assertVc3AlarmForced(False)

class Aug1HardwareCleanup(HardwareCleanup):
    def testMapFromVc4ToVc3(self):
        Mapper.mapAug1_vc4()
        self.assertVc4MaskSet(True)
        self.assertVc4AlarmForced(True)
        self.runCli("sdh map aug1.1.1-4.16 3xvc3s")
        self.assertVc4MaskSet(False)
        self.assertVc4AlarmForced(False)
        
    def testMapFromVc3ToVc4(self):
        Mapper.mapAug1_vc3()
        self.assertVc3MaskSet(True)
        self.assertVc3AlarmForced(True)
        self.runCli("sdh map aug1.1.1-4.16 vc4")
        self.assertVc3MaskSet(False)
        self.assertVc3AlarmForced(False)

    def testMapFromVc4Tu3ToVc3(self):
        Mapper.mapAug1_vc4_tu3()
        self.assertVc4MaskSet(True)
        self.assertVc4AlarmForced(True)
        self.assertTu3MaskSet(True)
        self.assertTu3AlarmForced(True)
        self.runCli("sdh map aug1.1.1-4.16 3xvc3s")
        self.assertVc4MaskSet(False)
        self.assertVc4AlarmForced(False)
        self.assertTu3MaskSet(False)
        self.assertTu3AlarmForced(False)
        
    def testMapFromVc4Tug2sToAu3Vc3(self):
        Mapper.mapAug1_vc4_tu11()
        self.assertVc4MaskSet(True)
        self.assertVc4AlarmForced(True)
        self.assertTu11MaskSet(True)
        self.assertTu11AlarmForced(True)
        self.runCli("sdh map aug1.1.1-4.16 3xvc3s")
        self.assertVc4MaskSet(False)
        self.assertVc4AlarmForced(False)
        self.assertTu11MaskSet(False)
        self.assertTu11AlarmForced(False)
        
    def testMapFromAug1Vc3Tug2sToVc4(self):
        Mapper.mapAug1_vc3_tu11()
        self.assertVc3MaskSet(True)
        self.assertVc3AlarmForced(True)
        self.assertTu11MaskSet(True)
        self.assertTu11AlarmForced(True)
        self.runCli("sdh map aug1.1.1-4.16 vc4")
        self.assertVc3MaskSet(False)
        self.assertVc3AlarmForced(False)
        self.assertTu11MaskSet(False)
        self.assertTu11AlarmForced(False)
        
class Vc4HardwareCleanup(HardwareCleanup):
    def testMapFromTug3Vc3ToC4(self):
        Mapper.mapAug1_vc4_tu3()
        self.assertVc4MaskSet(True)
        self.assertVc4AlarmForced(True)
        self.assertTu3MaskSet(True)
        self.assertTu3AlarmForced(True)
        self.runCli("sdh map vc4.1.1-4.16 c4")
        self.assertTu3MaskSet(False)
        self.assertTu3AlarmForced(False)
    
    def testMapFromTug3Vc11ToC4(self):
        Mapper.mapAug1_vc4_tu11()
        self.assertVc4MaskSet(True)
        self.assertVc4AlarmForced(True)
        self.assertTu11MaskSet(True)
        self.assertTu11AlarmForced(True)
        self.runCli("sdh map vc4.1.1-4.16 c4")
        self.assertTu11MaskSet(False)
        self.assertTu11AlarmForced(False)

class Vc3HardwareCleanup(HardwareCleanup):
    def testMapFromTug2ToC3(self):
        Mapper.mapAug1_vc3_tu11()
        self.assertVc3MaskSet(True)
        self.assertVc3AlarmForced(True)
        self.assertTu11MaskSet(True)
        self.assertTu11AlarmForced(True)
        self.runCli("sdh map vc3.1.1.1-4.16.3 c3")
        self.assertTu11MaskSet(False)
        self.assertTu11AlarmForced(False)
        
    def testMapDe3ToCx(self):
        Mapper.mapAug1_vc3_de3()
        self.assertDe3MaskSet(True)
        self.assertDe3AlarmForced(True)
        self.runCli("sdh map vc3.1.1.1-4.16.3 c3")
        self.assertDe3MaskSet(False)
        self.assertDe3AlarmForced(False)

class Vc1xHardwareCleanup(HardwareCleanup):
    def testMapFromDe1ToCx(self):
        Mapper.mapAug1_vc3_vc1x_de1()
        self.assertDe1MaskSet(True)
        self.assertDe1AlarmForced(True)
        self.runCli("sdh map vc1x.1.1.1.1.1-4.16.3.7.4 c1x")
        self.assertDe1MaskSet(False)
        self.assertDe1AlarmForced(False)

class Tug2HardwareCleanup(HardwareCleanup):
    def testMapFromTu11To12(self):
        Mapper.mapAug1_vc3_tu11()
        self.assertVc3MaskSet(True)
        self.assertVc3AlarmForced(True)
        self.assertTu11MaskSet(True)
        self.assertTu11AlarmForced(True)
        self.runCli("sdh map tug2.1.1.1.1-4.16.3.7 tu12")
        self.runCli("sdh map vc1x.1.1.1.1.1-4.16.3.7.3 c1x")
        self.assertTu11MaskSet(False)
        self.assertTu11AlarmForced(False)

class De3HardwareCleanup(HardwareCleanup):
    def testMapFromDe3CBitChannelizedToUnframe(self):
        Mapper.mapAug1_vc3_de3_de1()
        self.assertDe1MaskSet(True)
        self.assertDe1AlarmForced(True)
        self.runCli("pdh de3 framing 1.1.1-4.16.3 ds3_unframed")
        self.assertDe1MaskSet(False)
        self.assertDe1AlarmForced(False)

class AllLevelHardwareCleanup(HardwareCleanup):
    def testAllLevelsCleanup(self):
        Mapper.mapAug1_vc3_de3_de1()
        self.runCli("pdh de3 interruptmask 1.1.1-4.16.3 ais|lof en")
        self.runCli("pdh de3 force alarm 1.1.1-4.16.3 tx ais en")
        self.runCli("pdh de3 force alarm 1.1.1-4.16.3 rx ais en")
        self.runCli("sdh path interruptmask vc3.1.1.1-4.16.3 lop|ais en")
        self.runCli("sdh path force alarm vc3.1.1.1-4.16.3 uneq tx en")
        self.runCli("sdh path force alarm au3.1.1.1-4.16.3 ais tx en")
        self.runCli("sdh path force alarm au3.1.1.1-4.16.3 ais rx en")
        self.assertDe3MaskSet(True)
        self.assertDe3AlarmForced(True)
        self.assertDe1MaskSet(True)
        self.assertDe1AlarmForced(True)
        self.assertVc3MaskSet(True)
        self.assertVc3AlarmForced(True)
        self.runCli("sdh map aug1.1.1-4.16 vc4")
        self.assertDe1MaskSet(False)
        self.assertDe1AlarmForced(False)
        self.assertDe3MaskSet(False)
        self.assertDe3AlarmForced(False)
        self.assertVc3MaskSet(False)
        self.assertVc3AlarmForced(False)

def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(Aug16HardwareCleanup)
    runner.run(Aug4HardwareCleanup)
    runner.run(Aug1HardwareCleanup)
    runner.run(Vc4HardwareCleanup)
    runner.run(Vc3HardwareCleanup)
    runner.run(Tug2HardwareCleanup)
    runner.run(Vc1xHardwareCleanup)
    runner.run(De3HardwareCleanup)
    runner.run(AllLevelHardwareCleanup)
    runner.summary()

if __name__ == '__main__':
    TestMain()
    
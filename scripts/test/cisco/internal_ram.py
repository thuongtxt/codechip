"""
Created on Apr 1, 2017

@author: namnng
"""
from python.arrive.AtAppManager.AtAppManager import AtAppManager
from python.arrive.atsdk.platform import AtHalListener
from test.AtTestCase import AtTestCase, AtTestCaseRunner


class HalListener(AtHalListener):
    def __init__(self):
        AtHalListener.__init__(self)
        self.numRead  = 0
        self.numWrite = 0
    
    def reset(self):
        self.numRead = 0
        self.numWrite = 0

    @staticmethod
    def isValidValue(value):
        if value == 0xcafecafe:
            return False
        return True

    def didRead(self, _, value):
        assert(self.isValidValue(value))
        self.numRead = self.numRead + 1
        
    def didWrite(self, _, value):
        self.numWrite = self.numWrite + 1

class CliInfo(object):
    @classmethod
    def isBoolValue(cls, value):
        return value in ["en", "dis", "set", "clear"]
    
    @classmethod
    def isNotSupportedValue(cls, value):
        return value == "N/S"

    @classmethod
    def isValidNumber(cls, value):
        def isValidNumberWithBase(value, base):
            try:
                int(value, base)
                return True
            except:
                return False
            
        if isValidNumberWithBase(value, 10) or isValidNumberWithBase(value, 16):
            return True
        return False

class CliRunnable(object):
    @classmethod
    def runCli(cls, cli):
        return AtAppManager.localApp().runCli(cli)
    
class ErrorGenerator(CliRunnable):
    def load(self):
        cli = "show ram internal error generator %s" % self.ram.ramId
        result = self.runCli(cli)
        self.genMode = result.cellContent(0, "gen-mode")
        self.errorType = result.cellContent(0, "error-type")
        self.generatorStarted = result.cellContent(0, "is-started")
        self.errorNum = result.cellContent(0, "error-num")
    
    def __init__(self, ram):
        self.ram = ram
        self.genMode = None
        self.errorType = None
        self.generatorStarted = None
        self.errorNum = None
        self.load()
    
    def isValidError(self, error):
        return self.ram.isValidError(error)
    
    @classmethod
    def validGeneratorModes(cls): 
        return ["oneshot", "continuous", "invalid"]
    
    @classmethod
    def isValidGeneratorMode(cls, mode):
        return mode in cls.validGeneratorModes()
    
class InternalRam(CliRunnable):
    ECC_CORRECTABLE = "ecc-correctable"
    ECC_UNCORRECTABLE = "ecc-uncorrectable"
    CRC = "crc"
    PARITY = "parity"
    
    def __init__(self):
        self.ramId = None
        self.ramDescription = None
        self.parityMon = None
        self.eccMon = None
        self.crcMon = None
        self.errorForced = None
        self.eccMonSupported = True
        self.crcMonSupported = True
        self.paritySupported = True
        self.errorGeneratorSupported = True
        self.errorsCanBeGenerated = self.applicableErrors()
    
    @classmethod
    def applicableErrors(cls):
        return [cls.ECC_CORRECTABLE,
                cls.ECC_UNCORRECTABLE,
                cls.CRC,
                cls.PARITY]
    
    @classmethod
    def isValidError(cls, error):
        if error == "None":
            return True
        else:
            return error in cls.applicableErrors()
    
class RamManager(object):
    def numVisibleRams(self):
        return None

    def __init__(self):
        self.rams = None
        self.loadRams()

    @staticmethod
    def runCli(cli):
        return AtAppManager.localApp().runCli(cli)
    
    def loadRams(self):
        rams = []
        cli = "show ram internal %s" % self.ramCliIds()
        cliResult = self.runCli(cli)
        for row_i in range(cliResult.numRows()):
            ram = InternalRam()
            ram.ramId = int(cliResult.cellContent(row_i, "RamId"))
            ram.ramDescription = cliResult.cellContent(row_i, "Description")
            ram.parityMon = cliResult.cellContent(row_i, "Parity Mon") 
            ram.eccMon = cliResult.cellContent(row_i, "ECC Mon")
            ram.crcMon = cliResult.cellContent(row_i, "CRC Mon") 
            ram.errorForced = cliResult.cellContent(row_i, "ErrorForced")
            ram.eccMonSupported = self.eccMonSupported(ram)
            ram.crcMonSupported = self.crcMonSupported(ram)
            ram.paritySupported = self.paritySupported(ram)
            ram.errorsCanBeGenerated = self.errorsCanBeGenerated(ram)
            ram.errorGeneratorSupported = self.errorGeneratorSupported(ram)
            
            rams.append(ram)
        
        self.rams = rams
        
        return self.rams
        
    @staticmethod
    def maxNumRams():
        return 1000
    
    def ramCliIds(self):
        return "1-%d" % self.maxNumRams()
    
    def eccMonSupported(self, ram):
        return True
    
    def paritySupported(self, ram):
        return True
    
    def crcMonSupported(self, ram):
        return True
    
    def errorGeneratorSupported(self, ram):
        return True 
    
    def errorsCanBeGenerated(self, ram):
        return InternalRam.applicableErrors()
    
class AbstractTest(AtTestCase):
    _halListener = None

    @classmethod
    def setUpClass(cls):
        super(AtTestCase, cls).setUpClass()
        cls._halListener = HalListener()
        AtHalListener.addListener(cls._halListener)
    
    @classmethod
    def tearDownClass(cls):
        AtHalListener.removeListener(cls._halListener)
    
    def resetHalListener(self):
        self._halListener.reset()
    
    @classmethod
    def halListener(cls):
        return cls._halListener
    
class InternalRamTest(AbstractTest):
    ram = None
    
    def testRamInfoMustBeValid(self):
        ram = self.ram

        def TestSupported(value, supported):
            if supported:
                self.assertTrue(CliInfo.isBoolValue(value), None)
            else:
                self.assertTrue(CliInfo.isNotSupportedValue(value), None)

        TestSupported(ram.parityMon, ram.paritySupported)
        TestSupported(ram.eccMon, ram.eccMonSupported)
        TestSupported(ram.crcMon, ram.crcMonSupported)

        self.assertTrue(ram.isValidError(ram.errorForced), None)

    def testCounters(self):
        def TestWithMode(mode):
            self.resetHalListener()

            ram = self.ram
            cli = "show ram internal counters %s %s" % (ram.ramId, mode)
            result = self.runCli(cli)
            self.assertEqual(result.numRows(), 1, None)
            row_i = 0
            self.assertEqual(result.cellContent(row_i, "RamId"), "%d" % ram.ramId, None)
            self.assertEqual(result.cellContent(row_i, "Description"), ram.ramDescription, None)

            def checkCounter(value, supported):
                if supported:
                    self.assertTrue(CliInfo.isValidNumber(value), None)
                else:
                    self.assertTrue(CliInfo.isNotSupportedValue(value), None)

            value = result.cellContent(row_i, "ecc-correctable")
            checkCounter(value, ram.eccMonSupported)

            value = result.cellContent(row_i, "ecc-uncorrectable")
            checkCounter(value, ram.eccMonSupported)

            value = result.cellContent(row_i, "crc")
            checkCounter(value, ram.crcMonSupported)

            if ram.crcMonSupported or ram.eccMonSupported:
                self.assertGreater(self.halListener().numRead, 0, None)
                self.assertEqual(self.halListener().numWrite, 0, None)

        TestWithMode("r2c")
        TestWithMode("ro")

    def testShowRamHistory(self):
        def TestWithMode(mode):
            self.resetHalListener()

            ram = self.ram
            cli = "show ram internal history %s %s" % (ram.ramId, mode)
            result = self.runCli(cli)
            self.assertEqual(result.numRows(), 1, None)
            row_i = 0
            self.assertEqual(result.cellContent(row_i, "RamId"), "%d" % ram.ramId, None)
            self.assertEqual(result.cellContent(row_i, "Description"), ram.ramDescription, None)

            for erroType in ["ECC-Corr", "ECC-Uncorr", "CRC", "Parity"]:
                value = result.cellContent(row_i, erroType)
                self.assertTrue(CliInfo.isBoolValue(value), None)

            self.assertGreater(self.halListener().numRead, 0, None)

            if mode == "r2c":
                self.assertGreater(self.halListener().numWrite, 0, None)

        TestWithMode("r2c")
        TestWithMode("ro")

class ErrorGeneratorTest(AbstractTest):
    errorGenerator = None

    def ram(self):
        return self.errorGenerator.ram

    def ramId(self):
        return self.ram().ramId

    def supported(self):
        return self.errorGenerator.ram.errorGeneratorSupported

    def testInfoMustBeValid(self):
        generator = self.errorGenerator

        if self.supported():
            self.assertTrue(generator.isValidGeneratorMode(generator.genMode), None)
            self.assertTrue(generator.isValidError(generator.errorType), None)
            self.assertTrue(CliInfo.isBoolValue(generator.generatorStarted), None)
            self.assertTrue(CliInfo.isValidNumber(generator.errorNum), None)
        else:
            self.assertTrue(CliInfo.isNotSupportedValue(generator.genMode), None)
            self.assertTrue(CliInfo.isNotSupportedValue(generator.errorType), None)
            self.assertTrue(CliInfo.isNotSupportedValue(generator.generatorStarted), None)
            self.assertTrue(CliInfo.isNotSupportedValue(generator.errorNum), None)

    def testErrorType(self):
        generatorSupported = self.supported()

        for errorType in self.ram().applicableErrors():
            supported = generatorSupported and errorType in self.ram().errorsCanBeGenerated

            cli = "ram internal error generator type %s %s" % (self.ramId(), errorType)
            if supported:
                self.assertCliSuccess(cli)
                self.errorGenerator.load()
                self.assertEqual(self.errorGenerator.errorType, errorType, None)

            else:
                self.assertCliFailure(cli)

    def testErrorNum(self):
        supported = self.supported()

        for errorNum in [1, 100]:
            cli = "ram internal error generator errornum %s %d" % (self.ramId(), errorNum)
            if supported:
                self.assertCliSuccess(cli)
                self.errorGenerator.load()
                self.assertEqual(int(self.errorGenerator.errorNum, 10), errorNum, None)

            else:
                self.assertCliFailure(cli)

    def testGenerate(self):
        supported = self.supported()

        for enable in [True, False]:
            self.resetHalListener()
            action = "start" if enable else "stop"
            cli = "ram internal error generator %s %s" % (action, self.ramId())
            if supported:
                self.assertCliSuccess(cli)
                self.errorGenerator.load()
                expectedValue = "en" if enable else "dis"
                self.assertEqual(self.errorGenerator.generatorStarted, expectedValue, None)
                self.assertGreater(self.halListener().numRead, 0, None)
                self.assertGreater(self.halListener().numWrite, 0, None)
            else:
                self.assertCliFailure(cli)
                self.assertEqual(self.halListener().numRead, 0, None)
                self.assertEqual(self.halListener().numWrite, 0, None)

class RamModuleTest(AtTestCase):
    ramManager = None

    def __init__(self, methodName='runTest'):
        AtTestCase.__init__(self, methodName=methodName)

    def testThereMustBeEnoughRams(self):
        self.assertEqual(len(self.ramManager.rams), self.ramManager.numVisibleRams(), None)
    
class Tha60210051RamManager(RamManager):
    def numVisibleRams(self):
        return 308
    
    def eccMonSupported(self, ram):
        supportedRams = ["PDA link list ECC"]
        if ram.ramDescription in supportedRams:
            return True
        return False
    
    def paritySupported(self, ram):
        notSupportedRams = ["PLA CRC Error",
                            "PDA data CRC error", 
                            "PDA link list ECC"]
        if ram.ramDescription in notSupportedRams:
            return False
        return True
    
    def crcMonSupported(self, ram):
        supportedRams = ["PLA CRC Error", "PDA data CRC error"]
        if ram.ramDescription in supportedRams:
            return True
        return False
    
    def errorGeneratorSupported(self, ram):
        supportedRams = ["PLA CRC Error",
                         "PDA data CRC error", 
                         "PDA link list ECC"]
        if ram.ramDescription in supportedRams:
            return True
        return False
    
    def errorsCanBeGenerated(self, ram):
        crcGeneratableRams = ["PLA CRC Error",
                              "PDA data CRC error"]
        if ram.ramDescription in crcGeneratableRams:
            return [InternalRam.CRC]
        
        eccGeneratableRams = ["PDA link list ECC"]
        if ram.ramDescription in eccGeneratableRams:
            return [InternalRam.ECC_CORRECTABLE, InternalRam.ECC_UNCORRECTABLE]
    
class Tha60210021RamManager(RamManager):
    def numVisibleRams(self):
        return 26
    
    def eccMonSupported(self, ram):
        supportedRams = ["DDR-ECC"]
        if ram.ramDescription in supportedRams:
            return True
        return False
    
    def paritySupported(self, ram):
        notSupportedRams = ["DDR-CRC", 
                            "DDR-ECC"]
        if ram.ramDescription in notSupportedRams:
            return False
        return True
    
    def crcMonSupported(self, ram):
        supportedRams = ["DDR-CRC"]
        if ram.ramDescription in supportedRams:
            return True
        return False
    
    def errorGeneratorSupported(self, ram):
        supportedRams = ["DDR-CRC", 
                         "DDR-ECC"]
        if ram.ramDescription in supportedRams:
            return True
        return False
    
    def errorsCanBeGenerated(self, ram):
        crcGeneratableRams = ["DDR-CRC"]
        if ram.ramDescription in crcGeneratableRams:
            return [InternalRam.CRC]
        
        eccGeneratableRams = ["DDR-ECC"]
        if ram.ramDescription in eccGeneratableRams:
            return [InternalRam.ECC_CORRECTABLE, InternalRam.ECC_UNCORRECTABLE]

class Tha60210031RamManager(RamManager):
    def numVisibleRams(self):
        return 81
    
    def eccMonSupported(self, ram):
        supportedRams = ["Rx DDR ECC", "Tx DDR ECC"]
        if ram.ramDescription in supportedRams:
            return True
        return False
    
    def paritySupported(self, ram):
        notSupportedRams = ["PDA DDR CRC error", 
                            "Rx DDR ECC", 
                            "Tx DDR ECC"]
        if ram.ramDescription in notSupportedRams:
            return False
        return True
    
    def crcMonSupported(self, ram):
        supportedRams = ["PDA DDR CRC error"]
        if ram.ramDescription in supportedRams:
            return True
        return False
    
    def errorGeneratorSupported(self, ram):
        supportedRams = ["PDA DDR CRC error", 
                         "Rx DDR ECC", 
                         "Tx DDR ECC"]
        if ram.ramDescription in supportedRams:
            return True
        return False
    
    def errorsCanBeGenerated(self, ram):
        crcGeneratableRams = ["PDA DDR CRC error"]
        if ram.ramDescription in crcGeneratableRams:
            return [InternalRam.CRC]
        
        eccGeneratableRams = ["Rx DDR ECC", "Tx DDR ECC"]
        if ram.ramDescription in eccGeneratableRams:
            return [InternalRam.ECC_CORRECTABLE, InternalRam.ECC_UNCORRECTABLE]
    
def ramManagerCreate():
    productCode = AtAppManager.localApp().productCode()
    managerClasses = {0x60210051: Tha60210051RamManager,
                      0x60210031: Tha60210031RamManager,
                      0x60210021: Tha60210021RamManager}
    managerClass = managerClasses[productCode] 
    return managerClass()

def TestWithManager(ramManager):
    runner = AtTestCaseRunner.runner()

    RamModuleTest.ramManager = ramManager
    runner.run(RamModuleTest)

    for ram in ramManager.rams:
        InternalRamTest.ram = ram
        runner.run(InternalRamTest)

        ErrorGeneratorTest.errorGenerator = ErrorGenerator(ram)
        runner.run(ErrorGeneratorTest)

    runner.summary()

def TestMain():
    TestWithManager(ramManagerCreate())

if __name__ == '__main__':
    TestMain()

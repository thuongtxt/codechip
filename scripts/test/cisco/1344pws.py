import python.arrive.atsdk.AtRegister as AtRegister
from python.arrive.atsdk.AtRegister import cBit5_0, cBit14_6
from python.arrive.atsdk.platform import AtHalListener
from test.AtTestCase import AtTestCase, AtTestCaseRunner

class ConfigureTest(AtTestCase):
    @staticmethod
    def lineId():
        return 1

    @staticmethod
    def aug1Id():
        return 8

    @staticmethod
    def au3Tug3Id():
        return 2

    @staticmethod
    def vtgId():
        return 1

    @staticmethod
    def vtId():
        return 1

    @staticmethod
    def globalPwId():
        return 1333

    @classmethod
    def tdmPwId(cls):
        return (cls.hwStsId() / 2) * 28 + cls.vtgId() * 4 + cls.vtId()

    @staticmethod
    def oc48Slice():
        return 1

    @staticmethod
    def hwStsId():
        return 40

    @classmethod
    def oc24Slice(cls):
        localOc24slice = cls.hwStsId() % 2
        return cls.oc48Slice() * 2 + localOc24slice

    @classmethod
    def localOc24Slice(cls):
        return cls.oc24Slice() % 2

    @classmethod
    def vc3CliId(cls):
        return "%d.%d.%d" % (cls.lineId() + 1, cls.aug1Id() + 1, cls.au3Tug3Id() + 1)

    @classmethod
    def lineCliId(cls):
        return "%d" % (cls.lineId() + 1)

    @classmethod
    def tug2CliId(cls):
        return "%s.%d" % (cls.vc3CliId(), cls.vtgId() + 1)

    @classmethod
    def vc1xCliId(cls):
        return "%s.%d" % (cls.tug2CliId(), cls.vtId() + 1)

    @classmethod
    def de1CliId(cls):
        return cls.vc1xCliId()

    @classmethod
    def pwCliId(cls):
        return "%d" % (cls.globalPwId() + 1)

    @classmethod
    def mapBaseAddress(cls):
        return cls.oc24Slice() * 0x40000 + 0x0800000

    @classmethod
    def mapAddressWithLocalAddress(cls, localAddress):
        return cls.mapBaseAddress() + localAddress

    @classmethod
    def mapOffset(cls):
        return 672 * (cls.hwStsId() / 2) + 96 * cls.vtgId() + 24 * cls.vtId()

    @classmethod
    def demap_channel_ctrl_address(cls):
        localAddress = cls.mapOffset() + 0x000000
        return cls.mapAddressWithLocalAddress(localAddress)

    @classmethod
    def map_channel_ctrl_address(cls):
        localAddress = cls.mapOffset() + 0x014000
        return cls.mapAddressWithLocalAddress(localAddress)

    @staticmethod
    def expectedMapDemapChannelType():
        return AtRegister.cBit31_0

    def expectedFirstTimeSlot(self, timeslotId):
        return 0

    @classmethod
    def pdaAddressWithLocalAddress(cls, localAddress):
        return 0x0500000 + localAddress

    @classmethod
    def pdaTdmOffset(cls):
        return cls.oc48Slice() * 4096 + cls.localOc24Slice() * 2048 + cls.tdmPwId()

    @classmethod
    def ramlotdmmodecfg_address(cls):
        return cls.pdaAddressWithLocalAddress(cls.pdaTdmOffset() + 0x00030000)

    @classmethod
    def ramlotdmlkupcfg_address(cls):
        return cls.pdaAddressWithLocalAddress(cls.pdaTdmOffset() + 0x00040000)

    @classmethod
    def plaAddressWithLocalAddress(cls, localAddress):
        return 0x0400000 + localAddress

    @classmethod
    def pla_lo_pld_ctrl_address(cls):
        offset = cls.oc48Slice() * 65536 + cls.localOc24Slice() * 16384 + cls.tdmPwId()
        return cls.plaAddressWithLocalAddress(offset + 0x02000)

    @classmethod
    def pla_lo_pw_ctrl_address(cls):
        offset = cls.oc48Slice() * 65536 + cls.localOc24Slice() * 2048 + cls.tdmPwId()
        return cls.plaAddressWithLocalAddress(offset + 0x0D000)

    @classmethod
    def pla_lo_add_rmv_pw_ctrl_address(cls):
        offset = cls.oc48Slice() * 65536 + cls.localOc24Slice() * 16384 + cls.tdmPwId()
        return cls.plaAddressWithLocalAddress(offset + 0x02800)

    @staticmethod
    def expectedPdaMode():
        return AtRegister.cBit31_0

    @staticmethod
    def expectedPlaLoPldTypeCtrl():
        return AtRegister.cBit31_0

    def expectedPayloadSize(self):
        return AtRegister.cBit31_0
    
    def timeslots(self):
        return range(24)
    
    def Test_map_demap_channel_ctrl(self, startAddress):
        for timeslot in self.timeslots():
            address = startAddress + timeslot
            reg = AtRegister.AtRegister(self.rd(address))
            self.assertEqual(reg.getField(AtRegister.cBit15, 15), self.expectedFirstTimeSlot(timeslot))
            self.assertEqual(reg.getField(AtRegister.cBit14_12, 12), self.expectedMapDemapChannelType())
            self.assertEqual(reg.getField(AtRegister.cBit11, 11), 1)
            self.assertEqual(reg.getField(AtRegister.cBit10_0, 0), self.tdmPwId())

    def test_demap_channel_ctrl(self):
        self.Test_map_demap_channel_ctrl(startAddress=self.demap_channel_ctrl_address())

    def test_map_channel_ctrl(self):
        self.Test_map_demap_channel_ctrl(startAddress=self.map_channel_ctrl_address())

    def test_ramlotdmmodecfg(self):
        reg = AtRegister.AtRegister(self.rd(self.ramlotdmmodecfg_address()))
        self.assertEqual(reg.getField(AtRegister.cBit12_11, 11), 1)
        self.assertEqual(reg.getField(AtRegister.cBit4_1, 1), self.expectedPdaMode())

    def test_ramlotdmlkupcfg(self):
        reg = AtRegister.AtRegister(self.rd(self.ramlotdmlkupcfg_address()))
        self.assertEqual(reg.getField(AtRegister.cBit12_0, 0), self.globalPwId())

    def test_pla_lo_pld_ctrl(self):
        reg = AtRegister.AtRegister(self.rd(self.pla_lo_pld_ctrl_address()))
        self.assertEqual(reg.getField(AtRegister.cBit16_15, 15), self.expectedPlaLoPldTypeCtrl())
        self.assertEqual(reg.getField(AtRegister.cBit14_0, 0), self.expectedPayloadSize())

    def test_pla_lo_pw_ctrl(self):
        reg = AtRegister.AtRegister(self.rd(self.pla_lo_pw_ctrl_address()))
        self.assertEqual(reg.getField(AtRegister.cBit19_7, 7), self.globalPwId())
        self.assertEqual(reg.getField(AtRegister.cBit0, 0), 1)

    def test_pla_lo_add_rmv_pw_ctrl(self):
        class Listener(AtHalListener):
            def __init__(self, address):
                self.hitCount = 0
                self.address = address

            def didAccess(self, address, _):
                if address == self.address:
                    self.hitCount = self.hitCount + 1

            def didRead(self, address, value):
                self.didAccess(address, value)

            def didWrite(self, address, value):
                self.didAccess(address, value)

        address = self.pla_lo_add_rmv_pw_ctrl_address()
        listener = Listener(address)
        AtHalListener.addListener(listener)

        self.runCli("pw disable %s" % self.pwCliId())
        self.runCli("pw enable %s" % self.pwCliId())
        self.assertGreater(listener.hitCount, 0)

        AtHalListener.removeListener(listener)

class Ds1SAToPConfigureTest(ConfigureTest):
    @classmethod
    def setUpClass(cls):
        cls.runCli("device init")
        cls.runCli("sdh line rate %s stm16" % cls.lineCliId())
        cls.runCli("sdh map vc3.%s 7xtug2s" % cls.vc3CliId())
        cls.runCli("sdh map tug2.%s tu11" % cls.tug2CliId())
        cls.runCli("sdh map vc1x.%s de1" % cls.vc1xCliId())

        cls.runCli("pw create satop %s" % cls.pwCliId())
        cls.runCli("pw circuit bind %s de1.%s" % (cls.pwCliId(), cls.de1CliId()))
        cls.runCli("pw ethport %s 1" % cls.pwCliId())
        cls.runCli("pw psn %s mpls" % cls.pwCliId())
        cls.runCli("pw enable %s" % cls.pwCliId())

    @staticmethod
    def expectedMapDemapChannelType():
        return 0

    @staticmethod
    def expectedPdaMode():
        return 0

    @staticmethod
    def expectedPlaLoPldTypeCtrl():
        return 0

    def expectedPayloadSize(self):
        return 193

class NxDs0Test(ConfigureTest):
    def timeslots(self):
        return range(3)

    @classmethod
    def setUpClass(cls):
        timeslotId = "0-2"
        cls.runCli("device init")
        cls.runCli("sdh line rate %s stm16" % cls.lineCliId())
        cls.runCli("sdh map vc3.%s 7xtug2s" % cls.vc3CliId())
        cls.runCli("sdh map tug2.%s tu11" % cls.tug2CliId())
        cls.runCli("sdh map vc1x.%s de1" % cls.vc1xCliId())
        cls.runCli("pdh de1 framing %s ds1_sf" % cls.de1CliId())
        cls.runCli("pdh de1 nxds0create %s %s" % (cls.de1CliId(), timeslotId))

        nxds0CliId = "%s.%s" % (cls.de1CliId(), timeslotId)
        cls.runCli("pw create cesop %s basic" % cls.pwCliId())
        cls.runCli("pw circuit bind %s nxds0.%s" % (cls.pwCliId(), nxds0CliId))
        cls.runCli("pw ethport %s 1" % cls.pwCliId())
        cls.runCli("pw psn %s mpls" % cls.pwCliId())
        cls.runCli("pw enable %s" % cls.pwCliId())

    def test_ramlotdmsmallds0control(self):
        offset = self.oc48Slice() * 16384 + self.localOc24Slice() * 8192 + self.tdmPwId()
        address = self.pdaAddressWithLocalAddress(offset + 0x00080000)
        reg = AtRegister.AtRegister(self.rd(address))
        self.assertEqual(reg.getField(AtRegister.cBit0, 0), 1)

    @staticmethod
    def expectedMapDemapChannelType():
        return 1

    @staticmethod
    def expectedPdaMode():
        return 2

    @staticmethod
    def expectedPlaLoPldTypeCtrl():
        return 1
    
    @classmethod
    def tdmPwId(cls):
        return 0 # As this is allocated
    
    def expectedFirstTimeSlot(self, timeslotId):
        if timeslotId == self.timeslots()[0]:
            return 1
        return 0

    def expectedPayloadSize(self):
        reg = AtRegister.AtRegister(0)
        reg.setField(cBit5_0, 0, len(self.timeslots()))
        reg.setField(cBit14_6, 6, 8)
        return reg.value

class FullScale(AtTestCase):
    @staticmethod
    def numLines():
        return 4

    def setUp(self):
        # Create all of possible NxDS0
        self.runCli("device init")
        self.runCli("sdh line rate 1-4 stm16")
        self.runCli("sdh map vc3.1.1.1-4.16.3 7xtug2s")
        self.runCli("sdh map tug2.1.1.1.1-4.16.3.7 tu11")
        self.runCli("sdh map vc1x.1.1.1.1.1-4.16.3.7.4 de1")
        self.runCli("pdh de1 framing 1.1.1.1.1-4.16.3.7.4 ds1_sf")

        # Create all PWs
        self.runCli("pw create cesop 1-5376 basic")
        self.runCli("pw psn 1-5376 mpls")
        self.runCli("pw ethport 1-2688 1")
        self.runCli("pw ethport 2689-5376 2")

    def testScaleTo1344PwsPerOc24Slice(self):
        self.assertCliSuccess("pdh de1 nxds0create 1.1.1.1.1-4.16.3.7.4 0-11")
        self.assertCliSuccess("pdh de1 nxds0create 1.1.1.1.1-4.16.3.7.4 12-23")

        # Now binding
        totalPws = 0
        for lineId in range(self.numLines()):
            for aug1Id in range(16):
                for vc3Id in range(3):
                    for tug2Id in range(7):
                        for tuId in range(4):
                            de1CliId = "%d.%d.%d.%d.%d" % (lineId + 1, aug1Id + 1, vc3Id + 1, tug2Id + 1, tuId + 1)
                            for timeslots in ["0-11", "12-23"]:
                                if totalPws >= 5376:
                                    return

                                pwId = totalPws + 1
                                nxds0CliId = "%s.%s" % (de1CliId, timeslots)
                                cli = "pw circuit bind %d nxds0.%s" % (pwId, nxds0CliId)
                                self.assertCliSuccess(cli)
                                totalPws = totalPws + 1

    def testCannotScaleMoreThan1344PwsPerOc24Slice(self):
        self.assertCliSuccess("pdh de1 nxds0create 1.1.1.1.1-4.16.3.7.4 0-7")
        self.assertCliSuccess("pdh de1 nxds0create 1.1.1.1.1-4.16.3.7.4 8-15")
        self.assertCliSuccess("pdh de1 nxds0create 1.1.1.1.1-4.16.3.7.4 16-23")
        
        # Now binding
        totalPws = 0
        for lineId in range(self.numLines()):
            numPwsPerEvenAug1 = 0
            numPwsPerOddAug1  = 0
            for aug1Id in range(16):
                for vc3Id in range(3):
                    for tug2Id in range(7):
                        for tuId in range(4):
                            de1CliId = "%d.%d.%d.%d.%d" % (lineId + 1, aug1Id + 1, vc3Id + 1, tug2Id + 1, tuId + 1)
                            for timeslots in ["0-7", "8-15", "16-23"]:
                                if totalPws >= 5376:
                                    return

                                pwId = totalPws + 1
                                nxds0CliId = "%s.%s" % (de1CliId, timeslots)
                                cli = "pw circuit bind %d nxds0.%s" % (pwId, nxds0CliId)
                                
                                if aug1Id % 2 == 0:
                                    if numPwsPerEvenAug1 >= 1344:
                                        self.assertCliFailure(cli)
                                    else:
                                        self.assertCliSuccess(cli)
                                        totalPws = totalPws + 1
                                        numPwsPerEvenAug1 = numPwsPerEvenAug1 + 1

                                else:
                                    if numPwsPerOddAug1 >= 1344:
                                        self.assertCliFailure(cli)
                                    else:
                                        self.assertCliSuccess(cli)
                                        totalPws = totalPws + 1
                                        numPwsPerOddAug1 = numPwsPerOddAug1 + 1
def TestMain():
    runner = AtTestCaseRunner.runner()
    runner.run(Ds1SAToPConfigureTest)
    runner.run(NxDs0Test)
    runner.run(FullScale)
    runner.summary()

if __name__ == '__main__':
    TestMain()

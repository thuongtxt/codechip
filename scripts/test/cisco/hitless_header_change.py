from python.arrive.atsdk.platform import AtHalListener
import python.arrive.atsdk.AtRegister as AtRegister
from test.AtTestCase import AtTestCase

class HitlessHeaderChange(AtTestCase):
    def createDefaultHalListener(self):
        class _HalListener(AtHalListener):
            def __init__(self):
                AtHalListener.__init__(self)
                self.numRead = 0
                self.numWrite = 0
                self.restart()
            
            def didRead(self, address, value):
                self.numRead = self.numRead + 1
            
            def didWrite(self, address, value):
                self.numWrite = self.numWrite + 1
                
            def restart(self):
                self.numRead = 0
                self.numWrite = 0
        
        listener = _HalListener()
        AtHalListener.addListener(listener)
        return listener
    
    def createHwProtocolListener(self):
        class _HwProtocolListener(AtHalListener):
            def __init__(self):
                AtHalListener.__init__(self)
                self.numWrite = 0
            
            def didWrite(self, address, value):
                reg = AtRegister.AtRegister(value)
                
                # Remove state
                if address == 0x00480140 and reg.getField(AtRegister.cBit1_0, 0) == 3:
                    self.numWrite = self.numWrite + 1
                    
                # PDA jitter buffer centering
                if address == 0x00500010:
                    self.numWrite = self.numWrite + 1
        
        return _HwProtocolListener()
    
    @staticmethod
    def deleteHalListener(listener):
        AtHalListener.removeListener(listener)
    
    @classmethod
    def setupDefaultPsn(cls):
        cls.classRunCli("pw psn 1 none")
        cls.classRunCli("pw psn 1 mpls")
        cls.classRunCli("pw mpls outerlabel add 1 1.1.1")
        cls.classRunCli("pw ethheader 1 C0.CA.C0.CA.C0.CA 1.1.1 2.1.2")
    
    @classmethod
    def setUpClass(cls):
        cls.classRunCli("device init")
        cls.classRunCli("debug module pw header hitless enable")
        cls.classRunCli("pw create cep 1 basic")
        cls.classRunCli("pw circuit bind 1 vc3.1.1.1")
        cls.setupDefaultPsn()
        cls.classRunCli("pw ethport 1 1")
        cls.classRunCli("pw enable 1")
    
    def setUp(self):
        self.__class__.setupDefaultPsn()
    
    def testApplySameEthHeaderMustNotTriggerAnyWrite(self):
        listener = self.createDefaultHalListener()
        self.assertCliSuccess("pw ethheader 1 C0.CA.C0.CA.C0.CA 1.1.1 2.1.2")
        self.assertEqual(listener.numWrite, 0, None)
        self.deleteHalListener(listener)
        
    def testApplySamePsnMustNotTriggerAnyWrite(self):
        listener = self.createDefaultHalListener()
        self.assertCliSuccess("pw psn 1 mpls")
        self.assertEqual(listener.numWrite, 0, None)
        self.deleteHalListener(listener)
    
    def testChangeEthHeaderWithSameLengthMustNotTriggerHwProtocol(self):
        listener = self.createHwProtocolListener()
        AtHalListener.addListener(listener)
        self.assertCliSuccess("pw ethheader 1 c0.ca.c0.ca.c0.cf 1.1.1 2.1.2")
        cliResult = self.runCli("show pw psn 1")
        self.assertEqual(cliResult.cellContent(0, "DMAC"), "c0.ca.c0.ca.c0.cf", None)
        self.assertEqual(cliResult.cellContent(0, "vlan"), "1.1.1,2.1.2", None)
        self.assertEqual(listener.numWrite, 0, None)
        AtHalListener.removeListener(listener)
        
    def testChangeEthHeaderWithDifferentLengthMustTriggerHwProtocol(self):
        listener = self.createHwProtocolListener()
        AtHalListener.addListener(listener)
        self.assertCliSuccess("pw ethheader 1 c0.ca.c0.ca.c0.cf 1.1.1 none")
        cliResult = self.runCli("show pw psn 1")
        self.assertEqual(cliResult.cellContent(0, "DMAC"), "c0.ca.c0.ca.c0.cf", None)
        self.assertEqual(cliResult.cellContent(0, "vlan"), "1.1.1", None)
        self.assertGreater(listener.numWrite, 0, None)
        AtHalListener.removeListener(listener)
        
    def testChangePsnWithSameLengthMustNotTriggerHwProtocol(self):
        listener = self.createHwProtocolListener()
        AtHalListener.addListener(listener)
        self.assertCliSuccess("pw mpls innerlabel 1 12.1.123")
        cliResult = self.runCli("show pw psn mpls 1")
        self.assertEqual(cliResult.cellContent(0, "innerLabel"), "12.1.123", None)
        self.assertEqual(listener.numWrite, 0, None)
        AtHalListener.removeListener(listener)
        
    def testChangePsnWithDifferentLengthMustTriggerHwProtocol(self):
        listener = self.createHwProtocolListener()
        AtHalListener.addListener(listener)
        self.assertCliSuccess("pw mpls outerlabel remove 1 1.1.1")
        self.assertGreater(listener.numWrite, 0, None)
        AtHalListener.removeListener(listener)

if __name__ == '__main__':
    HitlessHeaderChange.normalRun()

from python.arrive.AtAppManager.AtAppManager import *
import sys
sys.path.append("/opt/tools/python2.7/lib/python2.7/site-packages/unittest_xml_reporting-1.4.3-py2.7.egg/")
import xmlrunner
import unittest

class CliAtApsUpsrEngine(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        return AtAppManager.localApp().runCli("device init")
    
    @staticmethod
    def runCli(cli):
        return AtAppManager.localApp().runCli(cli)
    
    def assertCliSuccess(self, cli):
        result = self.runCli(cli)
        self.assertTrue(result.success(), "Run CLI \"%s\" fail" % cli)
        
    def assertCliFailure(self, cli):
        result = self.runCli(cli)
        self.assertFalse(result.success(), "Run CLI \"%s\" must be fail" % cli)
        
    def createApsUpsrWithVc4PathOn8FacePlateLines(self):
        #Create path
        self.assertCliSuccess("sdh line rate 1-8 stm4")
        self.assertCliSuccess("sdh map aug4.1.1-8.1 4xaug1s")
        self.assertCliSuccess("sdh map aug1.1.1-8.4 vc4")
        #Create engine
        for line in range(1, 5):
            for vc4 in range(1, 5):
                self.assertCliSuccess("aps upsr engine create %d vc4.%d.%d vc4.%d.%d" % (((line-1)*4+vc4), line, vc4, (line+4), vc4))

    def checkApsUpsrSwitchType(self, numRow, switchType):
        #Set switch type
        for engine in range(0, numRow):
            self.assertCliSuccess("aps upsr engine switchtype %d %s" % ((engine+1), switchType))
        #Check
        result = self.runCli("show aps upsr engine 1-32")    
        self.assertEqual(result.numRows(), numRow, None)
        for row in range(0, numRow):
            self.assertEqual(result.cellContent(row, 2), "%s" % switchType, None)
            
    def checkApsUpsrExtCmd(self, numRow, extCmd):
        #Set: Not ready now, will be updated when HW is available
        for engine in range(0, numRow):
            self.assertCliFailure("aps upsr engine extcmd %d %s" % ((engine+1), extCmd))
        #Check
        result = self.runCli("show aps upsr engine 1-32")    
        self.assertEqual(result.numRows(), numRow, None)
        for row in range(0, numRow):
            #self.assertEqual(result.cellContent(row, 3), "%s" %(extCmd), None)
            self.assertEqual(result.cellContent(row, 3), "ms2prt", None)
            
    def checkApsUpsrWait2Restore(self, numRow, wtr):
        #Set
        for engine in range(0, numRow):
            self.assertCliSuccess("aps upsr engine wtr %d %d" % ((engine+1), wtr))
        #Check
        result = self.runCli("show aps upsr engine 1-32")    
        self.assertEqual(result.numRows(), numRow, None)
        for row in range(0, numRow):
            self.assertEqual(result.cellContent(row, 8), "%d" % wtr, None)
            
    def checkApsUpsrConditionPerType(self, numRow, position, condition, switchType):
        #Set
        for engine in range(0, numRow):
            if position != 10:
                self.assertCliSuccess("aps upsr engine condition %d %s %s" % ((engine+1), condition, switchType))
            else:
                self.assertCliSuccess("aps upsr engine condition %d ais|lop|uneq|plm|tim|bersd|bersf %s" % ((engine+1), switchType))
                
        #Check
        result = self.runCli("show aps upsr engine switch_condition 1-32")    
        self.assertEqual(result.numRows(), numRow, None)
        for row in range(0, numRow):
            if position != 10:
                self.assertEqual(result.cellContent(row, position), "%s" % switchType, None)
            else:
                for typePos in range (0, 5):        
                    self.assertEqual(result.cellContent(row, (typePos+1)), "%s" % switchType, None)
            
    def checkApsUpsrCondition(self, numRow, position, condition):
        self.checkApsUpsrConditionPerType(numRow, position, condition, "sd") 
        self.checkApsUpsrConditionPerType(numRow, position, condition, "sf") 
        self.checkApsUpsrConditionPerType(numRow, position, condition, "none") 

    def checkApsUpsrStartStop(self, numRow, isStart):
        #Set
        for engine in range(0, numRow):
            self.assertCliSuccess("aps upsr engine %s %d" % (isStart, (engine+1)))
        #Check
        result = self.runCli("show aps upsr engine 1-32")
        self.assertEqual(result.numRows(), numRow, None)
        for row in range(0, numRow):
            self.assertEqual(result.cellContent(row, 1), "%s" % isStart, None)
    
    def testApsUpsrMustBeCreatedAndDeletedProperly(self):
        self.createApsUpsrWithVc4PathOn8FacePlateLines()
        #Test show
        result = self.runCli("show aps upsr engine 1-32")        
        self.assertEqual(result.numRows(), 16, None)
        for row in range(0, 16):
            self.assertEqual(result.cellContent(row, 0), "%d" % (row + 1), None)
            self.assertEqual(result.cellContent(row, 1), "stop", None)
            self.assertEqual(result.cellContent(row, 2), "non-revertive", None)
            self.assertEqual(result.cellContent(row, 3), "ms2prt", None)
            self.assertEqual(result.cellContent(row, 4), "vc4.%d.%d" % ((row/4)+1,(row%4)+1), None)
            self.assertEqual(result.cellContent(row, 5), "vc4.%d.%d" % ((row/4)+5,(row%4)+1), None)
            
        #delete some channels
        self.assertCliSuccess("aps upsr engine delete 5-7,10")
        #Test show
        result = self.runCli("show aps upsr engine 1-32")        
        self.assertEqual(result.numRows(), 12, None)
        for row in range(0, 16):
            groupId = result.cellContent(row, 0)
            self.assertFalse((groupId == "5") or (groupId == "6") or (groupId == "7") or (groupId == "10"), None)
            
        # Cleanup
        self.assertCliSuccess("aps upsr engine delete 1-32")

    def testApsUpsrChangeSwitchType(self):
        self.createApsUpsrWithVc4PathOn8FacePlateLines()
        #Test
        self.checkApsUpsrSwitchType(16, "revertive")
        self.checkApsUpsrSwitchType(16, "non-revertive")        
            
        # Cleanup
        self.assertCliSuccess("aps upsr engine delete 1-32")

    def testApsUpsrChangeExtCmd(self):
        self.createApsUpsrWithVc4PathOn8FacePlateLines()
        #Test
        self.checkApsUpsrExtCmd(16, "fs2wrk")
        self.checkApsUpsrExtCmd(16, "fs2prt")
        self.checkApsUpsrExtCmd(16, "ms2wrk")
        self.checkApsUpsrExtCmd(16, "ms2prt")
        self.checkApsUpsrExtCmd(16, "lp")
        self.checkApsUpsrExtCmd(16, "clear")
            
        # Cleanup
        self.assertCliSuccess("aps upsr engine delete 1-32")

    def testApsUpsrWait2Restore(self):
        self.createApsUpsrWithVc4PathOn8FacePlateLines()
        self.assertCliSuccess("aps upsr engine switchtype 1-16 revertive")
        #Test
        for wtr in range(0, 64):
            self.checkApsUpsrWait2Restore(16, wtr)
            
        # Cleanup
        self.assertCliSuccess("aps upsr engine delete 1-32")

    def testApsUpsrCondition(self):
        self.createApsUpsrWithVc4PathOn8FacePlateLines()
        #Test
        self.checkApsUpsrCondition(16, 1, "ais")
        self.checkApsUpsrCondition(16, 2, "lop")
        self.checkApsUpsrCondition(16, 3, "tim")
        self.checkApsUpsrCondition(16, 4, "uneq")
        self.checkApsUpsrCondition(16, 5, "plm")
        #self.checkApsUpsrCondition(16, 6, "bersd")
        #self.checkApsUpsrCondition(16, 7, "bersf")
          
        #Test all type
        self.checkApsUpsrCondition(16, 10, "plm")   
            
        # Cleanup
        self.assertCliSuccess("aps upsr engine delete 1-32")

    def testApsUpsrStartStop(self):
        self.createApsUpsrWithVc4PathOn8FacePlateLines()
        #Test
        self.checkApsUpsrStartStop(16, "start")
        self.checkApsUpsrStartStop(16, "stop")

        # Cleanup
        self.assertCliSuccess("aps upsr engine delete 1-32")
        

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(CliAtApsUpsrEngine)
    xmlrunner.XMLTestRunner().run(suite)

from python.arrive.AtAppManager.AtAppManager import *
import xmlrunner
import unittest

class CliAtApsGroup(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        return AtAppManager.localApp().runCli("device init")
    
    @staticmethod
    def runCli(cli):
        return AtAppManager.localApp().runCli(cli)
    
    @staticmethod
    def maxNumGroups():
        return 16
    
    def assertCliSuccess(self, cli):
        result = self.runCli(cli)
        self.assertTrue(result.success(), "Run CLI \"%s\" fail" % cli)
        
    def assertCliFailure(self, cli):
        result = self.runCli(cli)
        self.assertFalse(result.success(), "Run CLI \"%s\" must be fail" % cli)
    
    def testApsGroupMustBeCreatedAndDeletedProperly(self):
        # Create all
        self.assertCliSuccess("aps group create 1-16")
        result = self.runCli("show aps group 1-1024")
        self.assertEqual(result.numRows(), 16, None)
        for row in range(0, 16):
            self.assertEqual(result.cellContent(row, 0), "%d" % (row + 1), None)
            self.assertEqual(result.cellContent(row, 1), "working", None)
            self.assertEqual(result.cellContent(row, 2), "none", None)
            
        # Delete some of thems
        self.assertCliSuccess("aps group delete 5,7")
        result = self.runCli("show aps group 1-16")
        self.assertEqual(result.numRows(), 14, None)
        for row in range(0, 16):
            groupId = result.cellContent(row, 0)
            self.assertFalse((groupId == "5") or (groupId == "7"), None)
            
        # Cleanup
        self.assertCliSuccess("aps group delete 1-16")
    
    def testAddRemoveSelectors(self):
        self.assertCliSuccess("aps group create 1-16")
        self.assertCliSuccess("aps selector create 1 vc3.1.1.1 vc3.17.1.1 vc3.25.1.1")
        self.assertCliSuccess("aps selector create 2 vc3.1.1.2 vc3.17.1.2 vc3.25.1.2")
        self.assertCliSuccess("aps selector create 5 vc3.1.1.3 vc3.17.1.3 vc3.25.1.3")
        self.assertCliSuccess("aps selector create 6 vc3.1.2.1 vc3.17.2.1 vc3.25.2.1")
        
        # Add
        self.assertCliSuccess("aps group selector add 2 1,2,5,6")
        result = self.runCli("show aps group 2")
        self.assertEqual(result.cellContent(0, 2), "1-2,5-6")
        
        # Remove two of them
        self.assertCliSuccess("aps group selector remove 2 1,5")
        result = self.runCli("show aps group 2")
        self.assertEqual(result.cellContent(0, 2), "2,6")
        
        # Remove all of them
        self.assertCliSuccess("aps group selector remove 2 2,6")
        result = self.runCli("show aps group 2")
        self.assertEqual(result.cellContent(0, 2), "none")
        
        # Cleanup
        self.assertCliSuccess("aps group delete 1-16")

    def testChangeGroupState(self):
        self.assertCliSuccess("aps group create 1-16")
        
        # Change to working state
        self.assertCliSuccess("aps group state 1-16 working")
        result = self.runCli("show aps group 1-16")
        for row in range(0, 16):
            self.assertEqual(result.cellContent(row, 1), "working", None)
            
        # Change to protection state
        self.assertCliSuccess("aps group state 1-16 protection")
        result = self.runCli("show aps group 1-16")
        for row in range(0, 16):
            self.assertEqual(result.cellContent(row, 1), "protection", None)
        
        self.assertCliSuccess("aps group delete 1-16")

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(CliAtApsGroup)
    xmlrunner.XMLTestRunner().run(suite)

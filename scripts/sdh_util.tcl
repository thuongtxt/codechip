proc configureLine { lineId rate } {
	atsdk::sdh line rate         $lineId $rate
	atsdk::sdh line mode         $lineId sdh
	atsdk::sdh line loopback     $lineId release
	atsdk::sdh line tti transmit $lineId 1byte a space_padding
	atsdk::sdh line tti expect   $lineId 1byte a space_padding
}

proc numAug1sByRate { rate } {
	if {$rate == "stm4"} { return 4 }
	if {$rate == "stm1"} { return 1 }
	
	return 0
}

proc configurePath { pathId } {
	atsdk::sdh path tti transmit $pathId 1byte a space_padding
	atsdk::sdh path tti expect   $pathId 1byte a space_padding
}

proc configureDe1 {de1 frameMode} {
	atsdk::pdh de1 framing $de1 $frameMode
	atsdk::pdh de1 timing  $de1 system 1 ; # Note, the last parameter does not mater
}

proc showLineStatus {lineId} {
	atsdk::show sdh line $lineId
	atsdk::show sdh line alarm $lineId
	atsdk::show sdh line counters $lineId r2c
	atsdk::show sdh line tti receive $lineId
}

proc showPathStatus { paths } {
	atsdk::show sdh path $paths
	atsdk::show sdh path alarm $paths
	atsdk::show sdh path counters $paths r2c
	atsdk::show sdh path tti receive $paths
}

proc showDe1Status { de1s } {
	atsdk::show pdh de1 alarm $de1s
	atsdk::show pdh de1 counters $de1s r2c
}

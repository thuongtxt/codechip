
#Common flags
AM_CFLAGS += ${POSIXDEFINE}
AT_FLAGS = -Werror
AT_FLAGS = -Wunused-parameter -Wunused-variable -Wshadow \
           -Wmissing-declarations -Wmissing-prototypes -Wunused-function \
           -Wunused-label -Wswitch-enum -Wmissing-format-attribute -Wnested-externs \
           -Wbad-function-cast -Wcast-align -Wconversion -Wwrite-strings \
           -Wsign-compare -Wswitch-default -Wmissing-noreturn -Wfloat-equal \
           -Wdeclaration-after-statement -Wcast-qual -Werror \
           -Wformat=2 -Wformat-nonliteral -Wformat-security -Wextra

#
#Lint check definition
#
AM_V_SPLINT = $(am__v_SPLINT_$(V))
am__v_SPLINT_ = $(am__v_SPLINT_$(AM_DEFAULT_VERBOSITY))
am__v_SPLINT_0 = @echo "  DO    " $@;

AM_V_SPLINT_INNER = $(am__v_SPLINT_INNER_$(V))
am__v_SPLINT_INNER_ = $(am__v_SPLINT_INNER_$(AM_DEFAULT_VERBOSITY))
am__v_SPLINT_INNER_0 = echo "  LINTCHECK   " $$file;

LINTCHECK=splint -badflag -fileextensions

#
#For lint check we always return OK for continue make
lintcheck: $(LINKCHECK_SRC)
	${AM_V_SPLINT} for file in ${LINKCHECK_SRC}; do ${AM_V_SPLINT_INNER} ${LINTCHECK} $(DEFS) $(DEFAULT_INCLUDES) $(INCLUDES) $(AM_CPPFLAGS) \
	$(CPPFLAGS) $(AM_CFLAGS) $(CFLAGS) ${abs_srcdir}/$$file; done || exit 0

define recursive_func
	@fail= failcom='exit 1'; \
	for f in x $$MAKEFLAGS; do \
	  case $$f in \
	    *=* | --[!k]*);; \
	    *k*) failcom='fail=yes';; \
	  esac; \
	done; \
	dot_seen=no; \
	target=`echo $@ | sed s/-recursive//`; \
	list='$(SUBDIRS)'; for subdir in $$list; do \
	  echo "Making $$target in $$subdir"; \
	  if test "$$subdir" = "."; then \
		continue; \
	  else \
	    local_target="$$target"; \
	  fi; \
	  ($(am__cd) $$subdir && $(MAKE) $(AM_MAKEFLAGS) $$local_target) \
	  || eval $$failcom; \
	done; \
	test -z "$$fail"
endef

lintcheck-recursive: 
	$(call recursive_func)

AM_V_SDKGEN = $(am__v_SDKGEN_$(V))
am__v_SDKGEN_ = $(am__v_SDKGEN_$(AM_DEFAULT_VERBOSITY))
am__v_SDKGEN_0 = @echo "  GEN   " $@;

AM_V_SDKSTRIP = $(am__v_SDKSTRIP_$(V))
am__v_SDKSTRIP_ = $(am__v_SDKSTRIP_$(AM_DEFAULT_VERBOSITY))
am__v_SDKSTRIP_0 = @echo "  STRIP   " $(subst -strip,,$@);

doxy-recursive:
	$(call recursive_func)

##Common rule for doxygen
doxy:

.PHONY: linkcheck lintcheck-recursive doxy doxy-recursive

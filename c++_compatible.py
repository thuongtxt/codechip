#!/usr/bin/python

import os
import re
import sys
import glob
import getopt
import ntpath

c_DefineKeyword = "Define"

def FindHeaderDefine(content):
    for line in content:
        match = re.search("#ifndef\s+(.*)", line)
        if match:
            return match.group(1)
    return None
        
def FindDefineKeyword(content):
    lineNumber = 0
    for line in content:
        match = re.search(c_DefineKeyword, line)
        if match == None:
            lineNumber = lineNumber + 1
        else:
            break
        
    return lineNumber    

def FindLastEndif(content):
    lineNumber = 0
    nestIfdef = 0
    for line in content:
        if re.search("#if.*", line):
            nestIfdef = nestIfdef + 1
        if re.search("#endif.*", line):
            nestIfdef = nestIfdef - 1
            if nestIfdef == 0:
                return lineNumber
        
        lineNumber = lineNumber + 1
    return 0
    
def AlreadyHaveCPlusPlusCompatible(content):
    for line in content:
        if re.search("#ifdef\s+__cplusplus", line):
            return True
    return False   

# process on file 
def file_modify(fileName):
    ifile = open(fileName,'r')
    content = ifile.readlines()
    ifile.close()
    
    if AlreadyHaveCPlusPlusCompatible(content) == True:
        return
    
    headerDefine = FindHeaderDefine(content)
    if headerDefine == None:
        return
    
    lineNumber = FindDefineKeyword(content)  
    
    content.insert(lineNumber + 1, "#ifdef __cplusplus\r\n")
    content.insert(lineNumber + 2, "extern \"C\" {\r\n")
    content.insert(lineNumber + 3, "#endif\r\n")
    
    lineNumber = FindLastEndif(content)
    if lineNumber == 0:
        return
    
    # Move back one line
    lineNumber = lineNumber - 1
    content.insert(lineNumber + 1, "#ifdef __cplusplus\r\n")
    content.insert(lineNumber + 2, "}\r\n")
    content.insert(lineNumber + 3, "#endif\r\n")
    
    # Write back to file
    modifiedFile = open(fileName, "w")
    content = "".join(content)
    modifiedFile.write(content)
    modifiedFile.close()
    
# Check if it is register file
arrive_prefix = ["at", "tha", "af6"]

def isHeaderFile(fname):
    head, tail = ntpath.split(fname)
    for prefix in arrive_prefix[:]:
         if tail.lower().startswith(prefix) & tail.lower().endswith(".h"):
             return True
    return False
    
def dir_modify(path):
    for currentFile in glob.glob( os.path.join(path, '*') ):
        if os.path.isdir(currentFile):
            dir_modify(currentFile)
            
        if isHeaderFile(currentFile):
            print ("\r\nProcessing file: " + currentFile)
            file_modify(currentFile)  
                
def printUsage():
    print ('usage: c++_compatible.py [file | -d <directory>]')
    print ('       -d <directory>: replace all files .h in folder')
    
def main(argv):
    global m_Debug
    global filterString
    
    inputDirectory = None
    m_Verbose = False
    
    try:
       opts, args = getopt.getopt(argv,"hd:f:v",["help"])
    except getopt.GetoptError:
       printUsage()
       sys.exit(2)
       
    for opt, arg in opts:
        if opt == '-h':
            printUsage()
            sys.exit()
        elif opt == '-v':
            m_Debug = True
        elif opt == '-d':
            inputDirectory = arg
    
    if '-v' in argv: 
       m_Verbose = True
       
    if inputDirectory:
        dir_modify(inputDirectory)
    else:
        file_modify(argv[0])   
   
if __name__ == "__main__":
   main(sys.argv[1:])
   
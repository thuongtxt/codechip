/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ISR
 *
 * File        : AtIsrInterruptController.c
 *
 * Created Date: Sep 12, 2017
 *
 * Description : To support functions that are ISR safe
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "AtIsrInterruptControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtIsrInterruptControllerMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Delete(AtIsrInterruptController self)
    {
    AtOsalMemFree(self);
    }

static uint8 NumCores(AtIsrInterruptController self)
    {
    /* There must be at least one core */
    AtUnused(self);
    return 1;
    }

static eAtRet CoreEnable(AtIsrInterruptController self, uint8 coreId, eBool enabled)
    {
    AtUnused(self);
    AtUnused(coreId);
    AtUnused(enabled);
    return cAtErrorNotImplemented;
    }

static eBool CoreIsEnabled(AtIsrInterruptController self, uint8 coreId)
    {
    AtUnused(self);
    AtUnused(coreId);
    return cAtFalse;
    }

static void MethodsInit(AtIsrInterruptController self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Delete);
        mMethodOverride(m_methods, NumCores);
        mMethodOverride(m_methods, CoreEnable);
        mMethodOverride(m_methods, CoreIsEnabled);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtIsrInterruptController);
    }

AtIsrInterruptController AtIsrInterruptControllerObjectInit(AtIsrInterruptController self, AtHal hal, uint32 productCode)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* HAL must be valid */
    if (hal == NULL)
        return NULL;

    /* Setup methods */
    MethodsInit(self);

    /* Private data */
    self->hal = hal;
    self->productCode = productCode;

    return self;
    }

/**
 * @addtogroup AtIsrInterruptController
 * @{
 */
/**
 * Create ISR interrupt controller
 *
 * @param hal HAL to access registers
 * @param productCode Product code
 *
 * @return ISR interrupt controller instance
 */
AtIsrInterruptController AtIsrInterruptControllerCreate(AtHal hal, uint32 productCode)
    {
    switch (productCode)
        {
        case 0x60290022: return Tha60290021IsrInterruptControllerNew(hal, productCode);
        case 0x60290021: return Tha60290021IsrInterruptControllerNew(hal, productCode);
        case 0x60290011: return Tha60290011IsrInterruptControllerNew(hal, productCode);

        default:
            return NULL;
        }
    }

/**
 * Delete ISR interrupt controller
 * @param self Controller to delete
 */
void AtIsrInterruptControllerDelete(AtIsrInterruptController self)
    {
    if (self)
        mMethodsGet(self)->Delete(self);
    }

/**
 * Get product code
 *
 * @param self This controller
 *
 * @return Product code
 */
uint32 AtIsrInterruptControllerProductCode(AtIsrInterruptController self)
    {
    return self ? self->productCode : cInvalidUint32;
    }

/**
 * Get HAL
 *
 * @param self This controller
 *
 * @return HAL
 */
AtHal AtIsrInterruptControllerHal(AtIsrInterruptController self)
    {
    return self ? self->hal : NULL;
    }

/**
 * Get number of Cores
 *
 * @param self This controller
 *
 * @return Number of Cores
 */
uint8 AtIsrInterruptControllerNumCores(AtIsrInterruptController self)
    {
    if (self)
        return mMethodsGet(self)->NumCores(self);
    return 0;
    }

/**
 * Enable/disable interrupt on input core
 *
 * @param self This controller
 * @param coreId Core ID
 * @param enabled Enabling
 *                - cAtTrue to enable
 *                - cAtFalse to disable
 *
 * @return AT return code
 */
eAtRet AtIsrInterruptControllerCoreEnable(AtIsrInterruptController self, uint8 coreId, eBool enabled)
    {
    if (self)
        return mMethodsGet(self)->CoreEnable(self, coreId, enabled);
    return cAtErrorObjectNotExist;
    }

/**
 * Check if interrupt on input Core is enabled or not.
 *
 * @param self This controller
 * @param coreId Core ID
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtIsrInterruptControllerCoreIsEnabled(AtIsrInterruptController self, uint8 coreId)
    {
    if (self)
        return mMethodsGet(self)->CoreIsEnabled(self, coreId);
    return cAtFalse;
    }

/**
 * @}
 */

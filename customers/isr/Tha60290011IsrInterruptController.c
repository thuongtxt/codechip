/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ISR
 *
 * File        : Tha60290011IsrInterruptController.c
 *
 * Created Date: Sep 12, 2017
 *
 * Description : ISR interrupt controller for product 60290011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtIsrInterruptControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_inten_PIN_Base                                          0x000005

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290011IsrInterruptController
    {
    tAtIsrInterruptController super;
    }tTha60290011IsrInterruptController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtIsrInterruptControllerMethods m_AtIsrInterruptControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet CoreEnable(AtIsrInterruptController self, uint8 coreId, eBool enabled)
    {
    AtHal hal = AtIsrInterruptControllerHal(self);
    AtUnused(coreId);
    AtHalNoLockWrite(hal, cAf6Reg_inten_PIN_Base, enabled ? 1 : 0);
    return cAtOk;
    }

static eBool CoreIsEnabled(AtIsrInterruptController self, uint8 coreId)
    {
    AtHal hal = AtIsrInterruptControllerHal(self);
    uint32 regVal = AtHalNoLockRead(hal, cAf6Reg_inten_PIN_Base);
    AtUnused(coreId);
    return (regVal & cBit0) ? cAtTrue : cAtFalse;
    }

static void OverrideAtIsrInterruptController(AtIsrInterruptController self)
    {
    AtIsrInterruptController controller = (AtIsrInterruptController)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtIsrInterruptControllerOverride, mMethodsGet(controller), sizeof(m_AtIsrInterruptControllerOverride));

        mMethodOverride(m_AtIsrInterruptControllerOverride, CoreEnable);
        mMethodOverride(m_AtIsrInterruptControllerOverride, CoreIsEnabled);
        }

    mMethodsSet(controller, &m_AtIsrInterruptControllerOverride);
    }

static void Override(AtIsrInterruptController self)
    {
    OverrideAtIsrInterruptController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290011IsrInterruptController);
    }

static AtIsrInterruptController ObjectInit(AtIsrInterruptController self, AtHal hal, uint32 productCode)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtIsrInterruptControllerObjectInit(self, hal, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtIsrInterruptController Tha60290011IsrInterruptControllerNew(AtHal hal, uint32 productCode)
    {
    /* Allocate memory */
    AtIsrInterruptController controller = AtOsalMemAlloc(ObjectSize());
    if (controller == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(controller, hal, productCode);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ISR
 * 
 * File        : AtIsrInterruptControllerInternal.h
 * 
 * Created Date: Sep 12, 2017
 *
 * Description : To support functions that are ISR safe
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATISRINTERRUPTCONTROLLERINTERNAL_H_
#define _ATISRINTERRUPTCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtOsal.h"
#include "AtIsrInterruptController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mMethodsGet(object) (object)->methods
#define mMethodsSet(object, methods) mMethodsGet(object) = methods
#define mMethodOverride(methods, methodName) (methods).methodName = methodName

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtIsrInterruptControllerMethods
    {
    void (*Delete)(AtIsrInterruptController self);
    uint8 (*NumCores)(AtIsrInterruptController self);
    eAtRet (*CoreEnable)(AtIsrInterruptController self, uint8 coreId, eBool enabled);
    eBool (*CoreIsEnabled)(AtIsrInterruptController self, uint8 coreId);
    }tAtIsrInterruptControllerMethods;

typedef struct tAtIsrInterruptController
    {
    uint32 productCodes;
    const tAtIsrInterruptControllerMethods *methods;

    /* Private data */
    uint32 productCode;
    AtHal hal;
    }tAtIsrInterruptController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtIsrInterruptController AtIsrInterruptControllerObjectInit(AtIsrInterruptController self, AtHal hal, uint32 productCode);

AtIsrInterruptController Tha60290021IsrInterruptControllerNew(AtHal hal, uint32 productCode);
AtIsrInterruptController Tha60290011IsrInterruptControllerNew(AtHal hal, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _ATISRINTERRUPTCONTROLLERINTERNAL_H_ */


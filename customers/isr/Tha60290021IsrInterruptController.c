/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ISR
 *
 * File        : Tha60290021IsrInterruptController.c
 *
 * Created Date: Sep 12, 2017
 *
 * Description : ISR interrupt controller for product 60290021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtIsrInterruptControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cInterruptBaseAddress 0x1E80000
#define cAf6Reg_global_interrupt_pin_disable_Base 0x000005
#define cInterruptEnableAddress (cInterruptBaseAddress + cAf6Reg_global_interrupt_pin_disable_Base)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290021IsrInterruptController
    {
    tAtIsrInterruptController super;
    }tTha60290021IsrInterruptController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtIsrInterruptControllerMethods m_AtIsrInterruptControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet CoreEnable(AtIsrInterruptController self, uint8 coreId, eBool enabled)
    {
    AtHal hal = AtIsrInterruptControllerHal(self);
    AtUnused(coreId);
    AtHalNoLockWrite(hal, cInterruptBaseAddress + cAf6Reg_global_interrupt_pin_disable_Base, enabled ? 0 : 1);
    return cAtOk;
    }

static eBool CoreIsEnabled(AtIsrInterruptController self, uint8 coreId)
    {
    AtHal hal = AtIsrInterruptControllerHal(self);
    uint32 regVal = AtHalNoLockRead(hal, cInterruptEnableAddress);
    AtUnused(coreId);
    return (regVal & cBit0) ? cAtFalse : cAtTrue;
    }

static void OverrideAtIsrInterruptController(AtIsrInterruptController self)
    {
    AtIsrInterruptController controller = (AtIsrInterruptController)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtIsrInterruptControllerOverride, mMethodsGet(controller), sizeof(m_AtIsrInterruptControllerOverride));

        mMethodOverride(m_AtIsrInterruptControllerOverride, CoreEnable);
        mMethodOverride(m_AtIsrInterruptControllerOverride, CoreIsEnabled);
        }

    mMethodsSet(controller, &m_AtIsrInterruptControllerOverride);
    }

static void Override(AtIsrInterruptController self)
    {
    OverrideAtIsrInterruptController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021IsrInterruptController);
    }

static AtIsrInterruptController ObjectInit(AtIsrInterruptController self, AtHal hal, uint32 productCode)
    {
    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtIsrInterruptControllerObjectInit(self, hal, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtIsrInterruptController Tha60290021IsrInterruptControllerNew(AtHal hal, uint32 productCode)
    {
    /* Allocate memory */
    AtIsrInterruptController controller = AtOsalMemAlloc(ObjectSize());
    if (controller == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(controller, hal, productCode);
    }

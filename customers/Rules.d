#-----------------------------------------------------------------------------
#
# COPYRIGHT (C) 2013 Arrive Technologies Inc.
#
# The information contained herein is confidential property of Arrive Tecnologies. 
# The use, copying, transfer or disclosure of such information 
# is prohibited except by express written agreement with Arrive Technologies.
#
# File        : Rules.d
#
# Created Date: May-28-2013
#
# Description : To compile customer source files. Just include this file to 
#    		    Makefile, compile then link it to 
# 
#----------------------------------------------------------------------------
# ==============================================================================
# Public
# ==============================================================================
AT_CUSTOMERS_DIR   = $(ATSDK_HOME)/customers
AT_CUSTOMERS_LIB_A = $(BUILD_DIR)/customers/atcustomers.a
AT_CUSTOMERS_LIB_O = $(BUILD_DIR)/customers/atcustomers.o

# ==============================================================================
# Private
# ==============================================================================
.PHONY: all atcustomers cleanatcustomers
all: atcustomers

AT_CUSTOMERS_SRC  = 
AT_CUSTOMERS_SRC += ${AT_CUSTOMERS_DIR}/src/AtCisco.c
AT_CUSTOMERS_SRC += ${AT_CUSTOMERS_DIR}/src/AtCiscoDe1DiagnosticExample.c
AT_CUSTOMERS_SRC += ${AT_CUSTOMERS_DIR}/src/AtCiscoDe3DiagnosticExample.c
AT_CUSTOMERS_SRC += ${AT_CUSTOMERS_DIR}/src/AtCiscoDiagnosticExample.c
AT_CUSTOMERS_SRC += ${AT_CUSTOMERS_DIR}/src/AtCiscoPrbsExample.c
AT_CUSTOMERS_SRC += ${AT_CUSTOMERS_DIR}/src/AtCiscoUpsr.c
AT_CUSTOMERS_SRC += ${AT_CUSTOMERS_DIR}/src/AtCiscoUpsrExample.c

ifneq ($(wildcard ${AT_CUSTOMERS_DIR}/stuff),)
AT_CUSTOMERS_SRC += $(shell find ${AT_CUSTOMERS_DIR}/stuff -iname "*.c")
endif

ifeq ($(AT_OS), none)
AT_CUSTOMERS_SRC         += ${AT_CUSTOMERS_DIR}/src/mempool/mempool.c
AT_CUSTOMERS_SRC         += ${AT_CUSTOMERS_DIR}/src/AtCiscoOsalNone.c
endif 

AT_CUSTOMERS_SRC        += $(shell find ${AT_CUSTOMERS_DIR}/version -iname "*.c")
AT_CUSTOMERS_SRC        += $(shell find ${AT_CUSTOMERS_DIR}/isr -iname "*.c")
AT_CUSTOMERS_FLAGS       = -I${AT_CUSTOMERS_DIR}/include -I${AT_CUSTOMERS_DIR}/version/include

AT_CUSTOMERS_ALL_OBJS    = $(call GetObjectsFromSource, $(AT_CUSTOMERS_SRC))
AT_CFLAGS += $(AT_CUSTOMERS_FLAGS)

atcustomers: ${AT_CUSTOMERS_LIB_A} ${AT_CUSTOMERS_LIB_O}

${AT_CUSTOMERS_LIB_O}: $(AT_CUSTOMERS_ALL_OBJS)
	$(AT_LD) $(LDFLAGS) $@ $^

${AT_CUSTOMERS_LIB_A}: $(AT_CUSTOMERS_ALL_OBJS)
	$(AT_AR) cvr $@ $^
	$(AT_RANLIB) $@

clean: cleanatcustomers
cleanatcustomers: 
	rm -f $(AT_CUSTOMERS_ALL_OBJS) $(AT_CUSTOMERS_LIB_A) $(AT_CUSTOMERS_LIB_O)

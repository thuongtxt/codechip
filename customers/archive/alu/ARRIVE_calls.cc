 
uint32 AluReadProductCode(uint32 baseAddress)
{
  uint32 productCode;
  AtHal hal;

  hal = AtHalAluNew(bsw_ArriveBaseAddress);
  productCode = AtProductCodeGetByHal(hal);
  AtHalDelete(hal);

  if (!AtDriverProductCodeIsValid(AtDriverSharedDriverGet(), productCode))
      AtPrintc(cSevCritical, "ERROR: product code 0x%08x is invalid\r\n", productCode);

  return productCode;
}


/* **************************** */
BSW_RetCode bsw_initArrive(
	const BSW_InstId  *instId)
/* **************************** */
/* ------------------------------------------------------------------------- */
{
  BSW_RetCode       retCode = bsw_RetCode_Success;
  ulong             regAddr, regData, regMask;
  uchar             trace_slot;
  BSW_HwDevice      trace_device;
  BSW_TrModule      trace_module;
  BSW_DeviceIndex   devInst = bsw_DeviceIndex_Collector_Arrive; 
  
  // Arrive definition
  eAtRet            atRet = cAtError;

  /* Trace parameters */
  trace_slot   = instId->boardNo;
  trace_device = bsw_HwDevice_Arrive;
  trace_module = bsw_TrModule_Init;
  

  /* Trace start procedure */
  bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO, ("bsw_initArrive: START (boardType=%u, boardNo=%u, devInst=%u)\r\n",
             instId->boardType, instId->boardNo, devInst));

  AtDriver newDriver = AtDriverCreate(1, AtOsalLinux());    
  AtDevice newDevice = AtDriverDeviceCreate(AtDriverSharedDriverGet(),AluReadProductCode(bsw_ArriveBaseAddress));

  /* And setup it: HAL creation */
  atRet = AtIpCoreHalSet(AtDeviceIpCoreGet(AddedDevice(), 0), AtHalAluNew(bsw_ArriveBaseAddress));
  if (atRet!=cAtOk) {
    bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR, ("Error in AtIpCoreHalSet.\r\n"));  
    return (bsw_RetCode_HwProblem);
  }

  /* Initialize device */
  atRet = AtDeviceInit(newDevice);
  if (atRet!=cAtOk) {
    bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR, ("Error in AtDeviceInit.\r\n"));  
    return (bsw_RetCode_HwProblem);
  }
  
  /* Trace end procedure */
  bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO,
            ("bsw_initHalley: END.\r\n"));
  
  
  return (retCode);
}
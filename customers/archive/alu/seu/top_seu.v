
`timescale 1 ps / 1 ps

/////////////////////////////////////////////////////////////////////////////
// Module
/////////////////////////////////////////////////////////////////////////////

module top_seu (
  input          user_clk,
  input          data_wr_id_seu,
  input          seu_alarms_id,
  input  [31:0]	 seu_data,
  input wire [31:0]	seu_pci_cfg0,
  input wire [31:0]	seu_pci_cfg1,
  output [31:0]	seu_pci_status,
  output [31:0]	seu_alarms_q
  ) ;



  wire [31:0]	sem_status;
//  wire [31:0]	seu_pci_alarms;
   assign seu_pci_status={15'd0, sem_status[16:4], 4'd0};
//  wire [31:0]	seu_pci_cfg0;
//  wire [31:0]	seu_pci_cfg1;



  wire        status_heartbeat;
  wire        status_initialization;
  wire        status_observation;
  wire        status_correction;
  wire        status_classification;
  wire        status_injection;
  wire        status_essential;
  wire        status_uncorrectable;
  reg [39:0] inject_address;

   wire inject_strobe = seu_pci_cfg0[8];
   reg       sem_is_active;
   reg [7:0] sem_err_cnt;
   reg 	     fatal_sem_err;
   reg [2:0] seu_state_inj=0;
   reg [2:0] seu_ret_state=0;
   wire   status_idle;
   wire [31:0] seu_pci_alarms = {22'd0, status_initialization, 6'd0, fatal_sem_err, 1'b0, status_correction};
   assign sem_status = {15'd0, status_idle,  sem_err_cnt, sem_is_active, status_uncorrectable, status_observation, status_initialization, 4'd0};
   assign status_idle = ~status_initialization & ~status_observation & ~status_correction & ~status_classification & ~status_injection;
   reg inject_strobe_d, inject_strobe_dd ;
   reg status_correction_d, status_correction_dd;
   reg [7:0] sem_activity_wdg=0;
   reg [3:0] sem_activity_cnt=0;
   reg [2:0] strobe_cnt=0;


   always @(posedge user_clk)
     begin
	inject_strobe_d  <= inject_strobe;
        status_correction_d <= status_correction;


	if (inject_strobe_d)
	  strobe_cnt <= 3'd3;
	else
	  begin
	   if (strobe_cnt>0)
		strobe_cnt <= strobe_cnt-1;
	   else strobe_cnt <= 3'd0;
	  end

	 inject_strobe_dd <= |strobe_cnt & ~inject_strobe_d;
	 inject_address[31:0]  <= seu_pci_cfg1;
	 inject_address[39:32] <= seu_pci_cfg0[7:0];

	 if(status_initialization)
	   sem_is_active = 1'b0;
	 else if(sem_activity_cnt[3])
	   sem_is_active = 1'b1;

	 if(status_heartbeat)
	   sem_activity_cnt <= ~sem_activity_cnt[3] ? sem_activity_cnt+1: 4'hf;


         status_correction_dd <= status_correction & ~status_correction_d;
	 if(status_initialization & status_observation)
	   sem_err_cnt <= 8'd0;
	 else if(status_correction_dd)
	   sem_err_cnt <= sem_err_cnt+1;

        fatal_sem_err <= status_initialization & status_observation & status_correction & status_classification & status_injection;
     end


 sem_example sem_controller(
			    .clk                     ( user_clk ),
			    .status_heartbeat        ( status_heartbeat ),
			    .status_initialization   ( status_initialization ),
			    .status_observation      ( status_observation ),
			    .status_correction       ( status_correction ),
			    .status_classification   ( status_classification ),
			    .status_injection        ( status_injection ),
			    .status_essential        ( status_essential ),
			    .status_uncorrectable    ( status_uncorrectable ),
			    .inject_strobe     ( inject_strobe_dd ),
			    .inject_address    ( inject_address )
			    );




   pci_alarm_check alm_check_a (
				.clk        ( user_clk    ),
				.i_alarm    ( seu_pci_alarms  ),
				.data_wr_id ( data_wr_id_seu  ),
				.alarm_id   ( seu_alarms_id ),
				.i_pci_data ( seu_data  ),
				.o_alarm    ( seu_alarms_q  )
				);











endmodule


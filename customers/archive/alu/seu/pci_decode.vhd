
--##########################
--##########################
--## SEU
--##########################
--##########################

component top_seu
 port (
  user_clk : in STD_LOGIC;
  data_wr_id_seu : in STD_LOGIC;
  seu_alarms_id : in STD_LOGIC;
  seu_data : in  STD_LOGIC_VECTOR(31 downto 0);
  seu_pci_cfg0 : in  STD_LOGIC_VECTOR(31 downto 0);
  seu_pci_cfg1 : in  STD_LOGIC_VECTOR(31 downto 0);
  seu_pci_status : out STD_LOGIC_VECTOR(31 downto 0);
  seu_alarms_q : out STD_LOGIC_VECTOR(31 downto 0)
);
end component;

--##########################
--##########################

seu_wr_data_p : process(pcie_clk)
begin
	if (rising_edge(pcie_clk)) then
		if (rst_n = '0') then
			seu_pci_cfg0    <= (others => '0');
			seu_pci_cfg1    <= (others => '0');
		else
			if (wr_bar(0) = '1' and seu_pci_cfg0_id='1' and o_seu_cs='1') then
				seu_pci_cfg0   <= wr_data;
			end if;
			if (wr_bar(0) = '1' and seu_pci_cfg1_id='1' and o_seu_cs='1') then
				seu_pci_cfg1   <= wr_data;
			end if;
		end if;
	end if;
end process;

select_rd_seu_data <= seu_status_id & seu_alarm_id & seu_pci_cfg1_id & seu_pci_cfg0_id;

seu_rd_data_p : process(select_rd_seu_data,seu_pci_cfg0,seu_pci_cfg1,seu_alarms_q,seu_pci_status)
begin
      case select_rd_seu_data is
              when X"1"  => data_rd_seu_i  <= seu_pci_cfg0;
              when X"2"  => data_rd_seu_i  <= seu_pci_cfg1;
              when X"4"  => data_rd_seu_i  <= seu_alarms_q;
              when others => data_rd_seu_i  <= seu_pci_status;
      end case;
end process;

 i_top_seu : top_seu
 port map (
  user_clk       =>   pcie_clk,      
  data_wr_id_seu =>   clear_alarm_seu,
  seu_alarms_id  =>   seu_id, 
  seu_data       =>   o_seu_d,    
  seu_pci_cfg0   =>   seu_pci_cfg0,   
  seu_pci_cfg1   =>   seu_pci_cfg1,   
  seu_pci_status =>   seu_pci_status, 
  seu_alarms_q   =>   seu_alarms_q  
);





end rtl;


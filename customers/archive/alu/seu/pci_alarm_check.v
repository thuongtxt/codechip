module pci_alarm_check (
			
			input             clk,
   
			input      [31:0] i_alarm,
			input             data_wr_id, 
			input             alarm_id,
			input      [31:0] i_pci_data,     

			output reg [31:0] o_alarm
			);
   

   genvar 	   i;

   generate for (i=0; i<32; i=i+1) 
     begin:bit
	always @(posedge clk or posedge i_alarm[i])
	  if (i_alarm[i])
	    o_alarm[i] <= 1'b1;
	  else if (data_wr_id & alarm_id & i_pci_data[i])
	    o_alarm[i] <= 1'b0;
end 
   endgenerate	   

endmodule

// Module Dependencies:
//
// pci_decode
// |
// top_seu
// |
// +- pci_alarm_check
// |
// sem_example (generated from vivado)
// |
// +- sem_core (macro xilinx, .xci or .dcp type)
// |
// +- sem_cfg
// |
// +- sem_mon (not used)
// |
// \- BUFG (not used)

4 micro registers are needed :

- seu_pci_cfg0(31:0)   : configuration register (RD/WR)
- seu_pci_cfg1(31:0)   : configuration register (RD/WR)
- seu_alarms_q(31:0)   : alarms register (RD)
- seu_pci_status(31:0) : status register (RD)

seu_pci_cfg0(31:0):

	Bit(7:4) OP:	Set to 0xA to point to the OBSERVATION state;
			set to 0xC for LINEAR state;
			set to 0xE for IDLE state.
	Bit(8) SEU_INJECT_STROBE: Used to move the SEU FSM through the avaiable stages.

seu_pci_cfg1(31:0) :

	Bit(30:0) ADDRESS: address used to point the error injection cell

seu_alarms_q(31:0) :

	Bit(0) CORRECTION
	Bit(2) FATAL_SEM_ERR
	Bit(9) INITIALIZATION

	To clear seu_alarms_q register set top_seu input bus called "seu_data" to 0xFFFFFFFF,
	and then make a "0 to 1" and a "1 to 0" transition on top_seu input bit called "data_wr_id_seu"   

seu_pci_status(31:0) :

	Bit(4) STATUS INITIALIZATION
	Bit(5) STATUS OBSERVATION
	Bit(6) STATUS UNCORRECTABLE
	Bit(7) SEM IS ACTIVE
	Bit(15:8) SEM ERR CNT
	Bit(16) STATUS IDLE


DESCRIPTION:
SEU validation setup order:
The CRC error detection and correction of the FPGA bitstream image are both enabled by default.
In normal condition the status bits "sem is active" and "status observation" are both set to 1 in the seu_pci_status register.
In order to validate the CRC detection and correction, there is a particular sequence to be followed; the step order is the following:

1. check if the bits "sem is active" and "status observation" are both set to 1 in the seu_pci_status register;
2. set into the seu_pci_cfg0 register the field OP to idle (0xE);
3. assert the bit seu_inject_strobe in the seu_pci_cfg0 register;
4. deassert the seu_inject_strobe;
5. wait until the "status idle" bit goes to 1 in the seu_pci_status register;
6. put a random value inside the 31-bit address field in the seu_pci_cfg1 register;
7. set into the seu_pci_cfg0 register the field OP to linear (0xC);
8. assert the bit seu_inject_strobe in the seu_pci_cfg0 register;
9. deassert the seu_inject_strobe;
10. wait until the "status idle" bit goes to 1 in the seu_pci_status register;
11. set into the seu_pci_cfg0 the field OP to observation (0xA);
12. assert the bit seu_inject_strobe in the seu_pci_cfg0 register;
13. deassert the seu_inject_strobe;
14. wait until the "status observation" bit goes to 1 in the seu_pci_status register;
15. if the "correction" alarm bit inside seu_alarms_q register is set to 1, it means that a CRC error was injected.
16. clear the seu_alarms_q register. If the field "status uncorrectalbe" inside the seu_pci_status register is set to 1,
    then the injected error has lead toward an irriversible situation : a FPGA download is required;
    otherwise the error has been corrected if also the seu_alarms_q register is all clear.

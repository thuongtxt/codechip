/* ========================================================================= */
/**
	\file		Bsw_eyeScanColl.h
	\author		D. Materia
	\brief		Eye scan function prototypes regarding Collector units.
	\date		28-Mar-2012

	History:\n
			14-Mar-2012 (D. Materia)
			- Creation.

			20-Mar-2012 (D. Materia)
			- Added 'SwingT'.

			22-Mar-2012 (D. Materia)
			- Handled GES silicon.

			28-Mar-2012 (D. Materia)
			- Following typedef enum has been elsewhere relocated:
				BSW_DrpItf
*/
/* ========================================================================= */


#ifndef _Bsw_eyeScanColl_h
#define _Bsw_eyeScanColl_h

#include "Bsw_drp.h"


#define IES_SILICON  0              // Initial Engineering Sample (1.x e 2.x)
#define GES_SILICON  1              // General Engineering Sample (3.x)

#define LPM_IES_VOLTAGE_SWING  200  // mV
#define DFE_IES_VOLTAGE_SWING  200  // mV

#define LPM_GES_VOLTAGE_SWING  380  // mV
#define DFE_GES_VOLTAGE_SWING  247  // mV


#define EVAL_SWING(vert_pt, voltage_swing)					\
	((vert_pt) & 0x80) ? (int)(-(voltage_swing) * ((vert_pt) & 0x7f) / 127)	\
	                   : (int)( (voltage_swing) * ((vert_pt) & 0x7f) / 127)


/* ------------------------------------------------------------------------- */
/**
    \brief  This function performs EYE scan only device's interfaces BKP/STM16, via
            Dynamic Reconfiguration Port (DRP).
    \param[in]  instId		logical board address
    \param[in]  devInst		device instance
    \param[in]  drp_itf		interface identification
    \param[in]  drp_lane	lane number
    \return  see BSW_RetCode
*/
/* ****************************** */
BSW_RetCode bsw_scan_eyeTest(
	const BSW_InstId  *instId,
	BSW_DeviceIndex   devInst,
	ulong             drp_itf,
	ulong             drp_lane);
/* ****************************** */
/* ------------------------------------------------------------------------- */


/* ------------------------------------------------------------------------- */
/**
    \brief  This function performs EYE scan to some device's interfaces, via
            Dynamic Reconfiguration Port (DRP).
    \param[in]  instId		logical board address
    \param[in]  devInst		device instance
    \param[in]  drp_itf		interface identification
    \param[in]  drp_lane	lane number
    \return  see BSW_RetCode
*/
/* ****************************** */
BSW_RetCode bsw_scan_eye(
	const BSW_InstId  *instId,
	BSW_DeviceIndex   devInst,
	ulong             drp_itf,
	ulong             drp_lane);
/* ****************************** */
/* ------------------------------------------------------------------------- */


/* ------------------------------------------------------------------------- */
/**
    \brief  This function enables EYE scan, via Dynamic Reconfig. Port (DRP).
    \param[in]  instId		logical board address
    \param[in]  devInst		device instance
    \param[in]  drp_itf		interface identification
    \param[in]  drp_lane	lane number
    \return  see BSW_RetCode
*/
/* ****************************** */
BSW_RetCode bsw_eyeScanEn(
	const BSW_InstId  *instId,
	BSW_DeviceIndex   devInst,
	ulong             drp_itf,
	ulong             drp_lane);
/* ****************************** */
/* ------------------------------------------------------------------------- */


/* ------------------------------------------------------------------------- */
/**
    \brief  This function displays 'two-dimensional' EYE scan status, via
            Dynamic Reconfiguration Port (DRP).
    \param[in]  instId		logical board address
    \param[in]  devInst		device instance
    \param[in]  drp_itf		interface identification
    \param[in]  drp_lane	lane number
    \param[in]  silicon_type   	silicon type:   1 = GES, 0 = IES
    \param[in]  dfe     	equalizer type: 1 = DFE, 0 = LPM
    \param[in]  prescale	prescale parameter
    \param[in]  vert_inc	vertical increment
    \param[in]  ui      	unit interval
    \param[out] cnt      	counter
    \param[out] maxEyeWidthT   	max total eye width (horizontal)
    \param[out] maxEyeSwingT   	max total eye swing (vertical)
    \return  see BSW_RetCode
*/
/* *********************************** */
BSW_RetCode bsw_statEye2d(
	const BSW_InstId  *instId,
	BSW_DeviceIndex   devInst,
	ulong             drp_itf,
	ulong             drp_lane,
	unsigned int 	  silicon_type,
	unsigned int 	  dfe,
	unsigned int 	  prescale,
	unsigned int	  vert_inc,
	unsigned int 	  ui,
	unsigned int	  *cnt,
	int               *maxEyeWidthT,
	int               *maxEyeSwingT);
/* *********************************** */
/* ------------------------------------------------------------------------- */

#endif  /* _Bsw_eyeScanColl_h */

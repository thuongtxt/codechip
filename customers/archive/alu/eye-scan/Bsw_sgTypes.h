/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Bsw_sgTypes.h
 * 
 * Created Date: Feb 25, 2015
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _BSW_SGTYPES_H_
#define _BSW_SGTYPES_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define false 0
#define true 1

#define IES_SILICON 0
#define GES_SILICON 1

enum {
    bsw_BKP_ITF,
    bsw_STM16_ITF,
    bsw_AURORA_ITF,
    bsw_SGMII_UP_ITF,
    bsw_SGMII_ER_ITF,
    bsw_XAUI_LINE_ITF,
    bsw_XAUI_MATE_ITF,
    bsw_GTX_GBE_ITF
};

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef unsigned char uchar;
typedef unsigned long ulong;

typedef enum BSW_RetCode
{
// MEMBERS
/*
COMMENT=
*/
bsw_RetCode_Success =0 ,

/*
COMMENT=
*/
bsw_RetCode_HwProblem ,

/*
COMMENT=
*/
bsw_RetCode_InvalidPara ,

/*
COMMENT=
*/
bsw_RetCode_Unavailable ,

/*
COMMENT=
*/
bsw_RetCode_Ispb_Error ,

/*
COMMENT=
*/
bsw_RetCode_Ispb_NDN ,

/*
COMMENT=
*/
bsw_RetCode_Ispb_DBE ,

/*
COMMENT=
*/
bsw_RetCode_Ispb_ABE ,

/*
COMMENT=
*/
bsw_RetCode_Ispb_RDY ,

/*
COMMENT=
*/
bsw_RetCode_Ispb_ACK ,

/*
COMMENT=
*/
bsw_RetCode_Ispb_NRS ,

/*
COMMENT=
*/
bsw_RetCode_Spi_Error ,

/*
COMMENT=
*/
bsw_RetCode_Power_Problem ,

/*
COMMENT=
*/
bsw_RetCode_Test_Fail ,

/*
COMMENT=
*/
bsw_RetCode_I2C_Error ,

/*
COMMENT=
*/
bsw_RetCode_HwCfgBus_Error ,

/*
COMMENT=
*/
bsw_RetCode_FpgaDwnl_Error ,

/*
COMMENT=For init API
*/
bsw_RetCode_EqAlm_InitProblem ,

/*
COMMENT=For init API
*/
bsw_RetCode_EqMon_InitProblem ,

/*
COMMENT=
*/
bsw_RetCode_MutexMngProblem ,

/*
COMMENT=
*/
bsw_RetCode_LinkAlm_InitProblem ,

/*
COMMENT=
*/
bsw_RetCode_FpgaDwnl_InitProblem ,

/*
COMMENT=
*/
bsw_RetCode_SpiChain_InitProblem ,

/*
COMMENT=
*/
bsw_RetCode_MismatchConf_InitProblem ,

/*
COMMENT=
*/
bsw_RetCode_TopAlm_InitProblem ,

/*
COMMENT=For init API
*/
bsw_RetCode_EqFreq_InitProblem ,

/*
COMMENT=
*/
bsw_RetCode_LastElement ,

}BSW_RetCode;

typedef enum BSW_Environment
{
// MEMBERS
/*
COMMENT=
*/
bsw_Environment_ETSI =0 ,

/*
COMMENT=
*/
bsw_Environment_SONET ,

}BSW_Environment;

typedef enum BSW_Equipment
    {
    // MEMBERS
    /*
    COMMENT=
    */
    bsw_Equipment_TSS320 =0 ,

    /*
    COMMENT=
    */
    bsw_Equipment_TSS160 ,

    /*
    COMMENT=
    */
    bsw_Equipment_TSS640 ,

    /*
    COMMENT=
    */
    bsw_Equipment_TSS100 ,

    /*
    COMMENT=
    */
    bsw_Equipment_TSS320H ,

    /*
    COMMENT=
    */
    bsw_Equipment_TSS160H ,

    /*
    COMMENT=
    */
    bsw_Equipment_TestEnv ,

    /*
    COMMENT=
    */
    bsw_Equipment_PSS64 ,

    /*
    COMMENT=
    */
    bsw_Equipment_PSS36 ,

    /*
    COMMENT=
    */
    bsw_Equipment_PSS32S ,

    /*
    COMMENT=
    */
    bsw_Equipment_IOTP ,

    /*
    COMMENT=
    */
    bsw_Equipment_MTTP ,

    }BSW_Equipment;

typedef enum BSW_BoardType
    {
    // MEMBERS
    /*
    COMMENT=matrix:Purple + Old Debussy
    */
    bsw_BoardType_MatrixOLD =0 ,

    /*
    COMMENT=matrix:Purple + New Debussy
    */
    bsw_BoardType_MatrixOLDND ,

    /*
    COMMENT=matrix: LAN switch Broadcom + New Debussy
    */
    bsw_BoardType_MatrixN ,

    /*
    COMMENT=matrix: LAN switch Broadcom + New Debussy + LO pluggable
    */
    bsw_BoardType_MatrixNLO ,

    /*
    COMMENT=
    */
    bsw_BoardType_1xSTM64 ,

    /*
    COMMENT=
    */
    bsw_BoardType_4xSTM16 ,

    /*
    COMMENT=
    */
    bsw_BoardType_8xSTM1_4 ,

    /*
    COMMENT=
    */
    bsw_BoardType_PSF ,

    /*
    COMMENT=
    */
    bsw_BoardType_Equico ,

    /*
    COMMENT=
    */
    bsw_BoardType_Service ,

    /*
    COMMENT=
    */
    bsw_BoardType_TermBus ,

    /*
    COMMENT=
    */
    bsw_BoardType_Fan ,

    /*
    COMMENT=
    */
    bsw_BoardType_Loa ,

    /*
    COMMENT=
    */
    bsw_BoardType_10xAny ,

    /*
    COMMENT=
    */
    bsw_BoardType_1xOTU2_slim ,

    /*
    COMMENT=
    */
    bsw_BoardType_Collector_10x1Gepp ,

    /*
    COMMENT=
    */
    bsw_BoardType_Collector_1x10Gepp ,

    /*
    COMMENT=
    */
    bsw_BoardType_CollectorMS ,

    /*
    COMMENT=
    */
    bsw_BoardType_11Gbar ,

    /*
    COMMENT=
    */
    bsw_BoardType_11Guar ,

    /*
    COMMENT=
    */
    bsw_BoardType_LDRXA ,

    /*
    COMMENT=
    */
    bsw_BoardType_LDRXB ,

    /*
    COMMENT=
    */
    bsw_BoardType_LDTXA ,

    /*
    COMMENT=
    */
    bsw_BoardType_LDTXB ,

    /*
    COMMENT=
    */
    bsw_BoardType_OMD44 ,

    /*
    COMMENT=
    */
    bsw_BoardType_OMON ,

    /*
    COMMENT=
    */
    bsw_BoardType_OPS ,

    /*
    COMMENT=
    */
    bsw_BoardType_WR4 ,

    /*
    COMMENT=
    */
    bsw_BoardType_WR8 ,

    /*
    COMMENT=
    */
    bsw_BoardType_OMDX ,

    /*
    COMMENT=
    */
    bsw_BoardType_Lofa ,

    /*
    COMMENT=
    */
    bsw_BoardType_DWLA10X ,

    /*
    COMMENT=
    */
    bsw_BoardType_PR10G ,

    /*
    COMMENT=
    */
    bsw_BoardType_BST10G ,

    /*
    COMMENT=
    */
    bsw_BoardType_CollectorMS_MLPPP ,

    /*
    COMMENT=
    */
    bsw_BoardType_CADM ,

    /*
    COMMENT=
    */
    bsw_BoardType_CMDX ,

    /*
    COMMENT=
    */
    bsw_BoardType_CWLA ,

    /*
    COMMENT=
    */
    bsw_BoardType_Collector_10x1GeSy ,

    /*
    COMMENT=
    */
    bsw_BoardType_Collector_1x10GeSy ,

    /*
    COMMENT=
    */
    bsw_BoardType_10xAnyV2 ,

    /*
    COMMENT=
    */
    bsw_BoardType_1xANY_10G ,

    /*
    COMMENT=to manage empty slot
    */
    bsw_BoardType_Null ,

    /*
    COMMENT=to manage Collector removal action
    */
    bsw_BoardType_NullCollector ,

    /*
    COMMENT=
    */
    bsw_BoardType_Collector_Mixed20Gepp ,

    /*
    COMMENT=
    */
    bsw_BoardType_PsFan ,

    /*
    COMMENT=
    */
    bsw_BoardType_FanDrawer ,

    /*
    COMMENT=
    */
    bsw_BoardType_Collector_1x10Gehs ,

    /*
    COMMENT=
    */
    bsw_BoardType_CollectorMS_ATM ,

    /*
    COMMENT=
    */
    bsw_BoardType_UGW ,

    /*
    COMMENT=
    */
    bsw_BoardType_PSF_F ,

    /*
    COMMENT=
    */
    bsw_BoardType_FanCostReduction ,

    /*
    COMMENT=
    */
    bsw_BoardType_Collector_20x1Gepp ,

    /*
    COMMENT=
    */
    bsw_BoardType_Collector_2x10Gepp ,

    /*
    COMMENT=
    */
    bsw_BoardType_Concentrator ,

    /*
    COMMENT=
    */
    bsw_BoardType_ConcentratorAccess_1x10Gepp ,

    /*
    COMMENT=
    */
    bsw_BoardType_ConcentratorAccess_8x1Gepp ,

    /*
    COMMENT=
    */
    bsw_BoardType_ConcentratorAccess_24x1Fepp ,

    /*
    COMMENT=
    */
    bsw_BoardType_1xOTU2xfp_CostReduction ,

    /*
    COMMENT=
    */
    bsw_BoardType_1xSTM64_CstRd ,

    /*
    COMMENT=
    */
    bsw_BoardType_MultiRate_CstRd ,

    /*
    COMMENT=
    */
    bsw_BoardType_Matrix320H ,

    /*
    COMMENT=
    */
    bsw_BoardType_Matrix160H ,

    /*
    COMMENT=
    */
    bsw_BoardType_Collector_20x1GeppE ,

    /*
    COMMENT=
    */
    bsw_BoardType_Collector_2x10GeppE ,

    /*
    COMMENT=
    */
    bsw_BoardType_Collector_10x1GeppE ,

    /*
    COMMENT=
    */
    bsw_BoardType_PSF_320H ,

    /*
    COMMENT=
    */
    bsw_BoardType_PSF_160H ,

    /*
    COMMENT=
    */
    bsw_BoardType_PTP ,

    /*
    COMMENT=
    */
    bsw_BoardType_Fan_320H ,

    /*
    COMMENT=
    */
    bsw_BoardType_Fan_160H ,

    /*
    COMMENT=
    */
    bsw_BoardType_TermBus_CR ,

    /*
    COMMENT=
    */
    bsw_BoardType_EOSMiddle ,

    /*
    COMMENT=
    */
    bsw_BoardType_Equico320H ,

    /*
    COMMENT=
    */
    bsw_BoardType_Equico160H ,

    /*
    COMMENT=
    */
    bsw_BoardType_EOSTopBottom ,

    /*
    COMMENT=3T8 pack type
    */
    bsw_BoardType_Fan_HC ,

    /*
    COMMENT=3T8 pack type
    */
    bsw_BoardType_PSF_HC ,

    /*
    COMMENT=3T8 pack type
    */
    bsw_BoardType_BTC_HC ,

    /*
    COMMENT=3T8 pack type
    */
    bsw_BoardType_BT_HC ,

    /*
    COMMENT=3T8 pack type
    */
    bsw_BoardType_HPCFAP ,

    /*
    COMMENT=3T8 pack type
    */
    bsw_BoardType_EC_HC ,

    /*
    COMMENT=3T8 pack type
    */
    bsw_BoardType_Matrix3T8 ,

    /*
    COMMENT=3T8 pack type
    */
    bsw_BoardType_Matrix640 ,

    /*
    COMMENT=3T8 pack type
    */
    bsw_BoardType_10xAny10G ,

    /*
    COMMENT=3T8 pack type
    */
    bsw_BoardType_2xAny40G ,

    /*
    COMMENT=3T8 pack type
    */
    bsw_BoardType_24xAnyMr ,

    /*
    COMMENT=
    */
    bsw_BoardType_1o10Ch10 ,

    /*
    COMMENT=
    */
    bsw_BoardType_Matrix1T9 ,

    /*
    COMMENT=
    */
    bsw_BoardType_Matrix1T9C ,

    /*
    COMMENT=
    */
    bsw_BoardType_Matrix960C ,

    /*
    COMMENT=PSS36 pack type
    */
    bsw_BoardType_TC36 ,

    /*
    COMMENT=PSS36 pack type
    */
    bsw_BoardType_BTC36 ,

    /*
    COMMENT=PSS36 pack type
    */
    bsw_BoardType_PSFC ,

    /*
    COMMENT=PSS36 pack type
    */
    bsw_BoardType_FLC36EA ,

    /*
    COMMENT=PSS36 pack type
    */
    bsw_BoardType_FLC36LA ,

    /*
    COMMENT=Nur 5.0
    */
    bsw_BoardType_43SCUP ,

    /*
    COMMENT=Nur 5.0
    */
    bsw_BoardType_11QCUP ,

    /*
    COMMENT=Nur 5.0
    */
    bsw_BoardType_SFC8 ,

    /*
    COMMENT=Nur 5.0
    */
    bsw_BoardType_130SCUP ,

    /*
    COMMENT=Nur 5.0
    */
    bsw_BoardType_100GEVPL ,

    /*
    COMMENT=
    */
    bsw_BoardType_Matrix1T6 ,

    /*
    COMMENT=
    */
    bsw_BoardType_Fan_32S ,

    /*
    COMMENT=
    */
    bsw_BoardType_10xAnyFC ,

    /*
    COMMENT=
    */
    bsw_BoardType_4xAny10G ,

    /*
    COMMENT=3T8 pack type
    */
    bsw_BoardType_8xETH1G ,

    /*
    COMMENT=3T8 pack type
    */
    bsw_BoardType_24xAnyMrB ,

    /*
    COMMENT=3T8 pack type
    */
    bsw_BoardType_8xETH1GB ,

    /*
    COMMENT=3T8 pack type
    */
    bsw_BoardType_Fan160V ,

    /*
    COMMENT=
    */
    bsw_BoardType_EquicoPSS32S ,

    /*
    COMMENT=
    */
    bsw_BoardType_Collector_10x10GEA ,

    /*
    COMMENT=10 ports at 10 Gbit - FPGA from Altera
    */
    bsw_BoardType_Collector_6x10GEA ,

    /*
    COMMENT=6 ports at 10 Gbit - FPGA from Altera
    */
    bsw_BoardType_Collector_10x10GEA_LIMITED_60G ,

    /*
    COMMENT=6 ports used on a bsw_BoardType_Collector_10x10GEA0 - FPGA from Altera
    */
    bsw_BoardType_Collector_1x100GEA ,

    /*
    COMMENT=1 port at 100 Gbit - FPGA from Altera
    */
    bsw_BoardType_PSF_2XBI ,

    /*
    COMMENT=
    */
    bsw_BoardType_LastElement ,

    }BSW_BoardType;

    typedef enum BSW_PortUsage
    {
    // MEMBERS
    /*
    COMMENT=SDH port
    */
    bsw_portUsage_ETSI =0 ,

    /*
    COMMENT=SONET port
    */
    bsw_portUsage_SONET ,

    /*
    COMMENT=Data port
    */
    bsw_portUsage_DATA ,

    /*
    COMMENT=OTH port,carrying ETSI traffic
    */
    bsw_portUsage_ETSIoverOTH ,

    /*
    COMMENT=OTH port,carrying SONET traffic
    */
    bsw_portUsage_SONEToverOTH ,

    /*
    COMMENT=not applicable
    */
    bsw_portUsage_NA ,

    }BSW_PortUsage;

typedef uchar BSW_DeviceIndex;

typedef struct BSW_InstId
{
// FIELDS


/*
COMMENT=
RANGE=
*/
BSW_Environment environment;


/*
COMMENT=
RANGE=
*/
BSW_Equipment equipment;


/*
COMMENT=Not significant starting from 0
RANGE=
*/
uchar shelfNo;


/*
COMMENT=Card type
RANGE=
*/
BSW_BoardType boardType;


/*
COMMENT=Slot number starting from 1
RANGE=
*/
uchar boardNo;


/*
COMMENT=Physical Interface numbering starting from 0
RANGE=
*/
uchar portNo;


/*
COMMENT=Intended usage of a port port,only relevant in TDM-related functions
RANGE=
*/
BSW_PortUsage portUsage;


/*
COMMENT=HW device numbering
RANGE=
*/
BSW_DeviceIndex index;


/*
COMMENT=HO Au numbering starting from 0
RANGE=
*/
ushort HONo;


/*
COMMENT=LO container numbering starting from 0
RANGE=
*/
ushort LONo;


/*
COMMENT=page numbering for ISPB (new access mode) starting from 0 Range (0..127) Forbideen Value: 127
RANGE=
*/
ushort pidNo;
}BSW_InstId;


typedef struct ER_DRP_CFG_REG_u
    {
    ulong reg;
    struct {
        ulong gtx;
        ulong address;
        ulong cmd;
        ulong valid;
        ulong data;
    }field;
    }ER_DRP_CFG_REG_u;

typedef ER_DRP_CFG_REG_u HA_XAUI_LINE_DRP_CFG_REG_u;
typedef ER_DRP_CFG_REG_u HA_XAUI_MATE_DRP_CFG_REG_u;
typedef ER_DRP_CFG_REG_u HA_GTX_GBE_DRP_CFG_REG_u;

typedef struct HAL_DRP_CFG_REG_u
    {
    ulong reg;
    struct {
        ulong gtx;
        ulong address_drp;
        ulong drp_cmd_wr;
        ulong drp_cmd_rd;
        ulong valid;
        ulong data_wr_drp;
        ulong data_rd_drp;
    }field;
    }HAL_DRP_CFG_REG_u;

typedef ER_DRP_CFG_REG_u HA_AURORA_DRP_CFG_REG_u;

typedef HAL_DRP_CFG_REG_u HAL_DRP_DATA_RD_u;

    typedef enum BSW_HwDevice
    {
    // MEMBERS
    /*
    COMMENT=
    */
    bsw_HwDevice_All =0 ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Amadeus1 ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Amadeus2 ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Debussy ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Wagner ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Chopin ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Falco ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Laplace ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Fermat ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Shannon ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Euclide ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Jake ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Elwood ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Fibonacci ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Zircone ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Corallo ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Trocadero ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Cerbero ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Gauss ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Gogol ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Jacobi ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Dirac ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Dirac_20g ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Baghira ,

    /*
    COMMENT=
    */
    bsw_HwDevice_MV82210_Baghira ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Simeto ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Sfp ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Keplero ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Archimede_Down ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Archimede_Up ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Pemaquid_Down ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Pemaquid_Up ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Jordan ,

    /*
    COMMENT=ConcentratorAccess_1x10Gepp
    */
    bsw_HwDevice_Ppexta ,

    /*
    COMMENT=ConcentratorAccess_8x1Gepp
    */
    bsw_HwDevice_Ppextb ,

    /*
    COMMENT=ConcentratorAccess_24x1Fepp
    */
    bsw_HwDevice_Ppextc ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Breeze ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Spi ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Wolfgang2_0 ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Wolfgang2_1 ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Harlock ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Sparrow ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Cayley ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Erlang_1 ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Erlang_2 ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Hamilton_1 ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Hamilton_2 ,

    /*
    COMMENT=
    */
    bsw_HwDevice_QT2035_1 ,

    /*
    COMMENT=
    */
    bsw_HwDevice_QT2035_2 ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Eraclito ,

    /*
    COMMENT=
    */
    bsw_HwDevice_Mac ,

    /*
    COMMENT=
    */
    bsw_HwDevice_PP ,

    /*
    COMMENT=
    */
    bsw_HwDevice_iTM ,

    /*
    COMMENT=
    */
    bsw_HwDevice_eTM ,

    }BSW_HwDevice;

typedef enum BSW_TrModule
    {
    // MEMBERS
    /*
    COMMENT=
    */
    bsw_TrModule_All ,

    /*
    COMMENT=
    */
    bsw_TrModule_Init ,

    /*
    COMMENT=
    */
    bsw_TrModule_Set ,

    /*
    COMMENT=
    */
    bsw_TrModule_Get ,

    /*
    COMMENT=
    */
    bsw_TrModule_IspbAccess ,

    /*
    COMMENT=
    */
    bsw_TrModule_Dwnl ,

    /*
    COMMENT=
    */
    bsw_TrModule_Pm ,

    /*
    COMMENT=
    */
    bsw_TrModule_Ppext ,

    }BSW_TrModule;

    enum BSW_XX
    {
    // MEMBERS
    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Dirac =0 ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Gogol ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Jacobi ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Euclide ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Baghira1 ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Baghira2 ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Micro ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Gauss ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Pemaquid_Down ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Pemaquid_Up ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Archimede_Down ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Archimede_Up ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Keplero ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Pitagora_Down ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Pitagora_Up ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_AD9552_1 ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_AD9552_2 ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_AD9552_3 ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_AD9552_4 ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Breeze ,

    /*
    COMMENT=Concentrator
    */
    bsw_DeviceIndex_Collector_Jordan ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Cayley ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Erlang_1 ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Erlang_2 ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Hamilton_1 ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Hamilton_2 ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_QT2035_1 ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_QT2035_2 ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Eraclito ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_AD9552_ptp_bkp ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_AD9552_ptp_100m ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_AD9517_1 ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_AD9557_1 ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_AD9557_2 ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_AD9557_3 ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Cayley2 ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_PP ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_iTM ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_eTM ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Costanze1 ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Costanze2 ,

    /*
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Gearbox ,

    /*
     * Added by Arrive:
    COMMENT=
    */
    bsw_DeviceIndex_Collector_Halley,

    };

typedef struct BSW_TraceEnh
    {
    // FIELDS


    /*
    COMMENT=global tracing enable
    RANGE=
    */
    bool enableTrace;


    /*
    COMMENT=Slot where trace is enabled
    RANGE=
    */
    uchar slotNo;


    /*
    COMMENT=Device where trace is enabled
    RANGE=
    */
    BSW_HwDevice deviceNo;


    /*
    COMMENT=Module where trace is enabled
    RANGE=
    */
    BSW_TrModule moduleNo;


    /*
    COMMENT=debugging level
    RANGE=
    */
    ulong levelNo;


    /*
    COMMENT=
    RANGE=
    */
    bool showFile;


    /*
    COMMENT=
    RANGE=
    */
    bool enableLF;
    }BSW_TraceEnh;

typedef struct as_new_RegMapPCE_t
    {

    }as_new_RegMapPCE_t;

typedef struct as_new_RegMapAlm_t
    {

    }as_new_RegMapAlm_t;

enum
    {
    BSW_TRC_INFO,
    BSW_TRC_HW_ERROR
    };

enum
    {
    HAL_DRP_CFG_REG,
    HAL_DRP_DATA_RD,
    HAL_XCVR_BKP_SOFT_RESET,
    HAL_XCVR_STM16_SOFT_RESET,
    ERL_DRP_CFG_REG,
    HAM_AURORA_DRP_CFG,
    HAM_XAUI_LINE_DRP_CFG,
    HAM_XAUI_MATE_DRP_CFG,
    HAM_GTX_GBE_DRP_CFG,
    DRP_ES_EYE_SCAN_EN_ADD,
    DRP_ES_VERT_OFFSET_ADD,
    DRP_ES_HORZ_OFFSET_ADD,
    DRP_RX_DATA_WIDTH_ADD,
    DRP_ES_CONTROL_STATUS_ADD,
    DRP_ES_ERROR_COUNT_ADD,
    DRP_ES_SAMPLE_COUNT_ADD,
    DRP_ES_QUAL_MASK_ADD,
    DRP_ES_SDATA_MASK_ADD,
    DRP_RXOUT_DIV_ADD
    };

enum
    {
    DRP_ES_QUAL_MASK_RPT = 5,
    DRP_ES_SDATA_MASK_RPT = 5
    };

enum
    {
    DFE_IES_VOLTAGE_SWING,
    DFE_GES_VOLTAGE_SWING,
    LPM_IES_VOLTAGE_SWING,
    LPM_GES_VOLTAGE_SWING
    };

typedef struct DRP_ES_EYE_SCAN_EN_u
    {
    ulong reg;
    struct
        {
        ulong es_eye_scan_en;
        ulong es_errdet_en;
        ulong es_control;
        }field;
    }DRP_ES_EYE_SCAN_EN_u;

typedef struct DRP_ES_VERT_OFFSET_u
    {
    ulong reg;
    struct
        {
        ulong es_vert_offset;
        ulong es_horz_offset;
        ulong es_ut_sign;
        ulong es_prescale;
        }field;
    }DRP_ES_VERT_OFFSET_u;

typedef struct DRP_ES_HORZ_OFFSET_u
    {
    ulong reg;
    struct
        {
        ulong es_horz_offset;
        }field;
    }DRP_ES_HORZ_OFFSET_u;

typedef struct DRP_RX_DATA_WIDTH_u
    {
    ulong reg;
    struct
        {
        ulong rx_data_width;
        }field;
    }DRP_RX_DATA_WIDTH_u;

typedef struct DRP_ES_ERROR_COUNT_u
    {
    ulong reg;
    struct
        {
        ulong es_error_count;
        }field;
    }DRP_ES_ERROR_COUNT_u;

typedef struct DRP_ES_SAMPLE_COUNT_u
    {
    ulong reg;
    struct
        {
        ulong es_sample_count;
        }field;
    }DRP_ES_SAMPLE_COUNT_u;

typedef struct DRP_ES_CONTROL_STATUS_u
    {
    ulong reg;
    struct
        {
        ulong es_control_status;
        }field;
    }DRP_ES_CONTROL_STATUS_u;

typedef struct DRP_ES_QUAL_MASK_u
    {
    ulong reg;
    struct
        {
        ulong es_qual_mask;
        }field;
    }DRP_ES_QUAL_MASK_u;

typedef struct DRP_ES_SDATA_MASK_u
    {
    ulong reg;
    struct
        {
        ulong es_sdata_mask;
        }field;
    }DRP_ES_SDATA_MASK_u;

typedef struct DRP_RXOUT_DIV_u
    {
    ulong reg;
    struct
        {
        ulong rxout_div;
        }field;
    }DRP_RXOUT_DIV_u;

enum
    {
    BSW_ALL_SLOT
    };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
extern const as_new_RegMapPCE_t *HALLEYRegCfg;
extern const as_new_RegMapAlm_t *HALLEYRegPoll;
extern BSW_TraceEnh bsw_TraceLevelEnhanced;

#ifdef __cplusplus
}
#endif
#endif /* _BSW_SGTYPES_H_ */


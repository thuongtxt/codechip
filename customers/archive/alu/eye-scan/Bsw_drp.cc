/* ========================================================================= */
/**
 \file  Bsw_drp.cc
 \author  D. Materia
 \brief  Functions regarding Dynamic Reconfiguration Port (DRP).
 \date  28-Mar-2012

 History:\n
   28-Mar-2012 (D. Materia)
   - Creation.
*/
/* ========================================================================= */


/* ------------------------------------------------------------------------- */
/* #### Include section #################################################### */
/* ------------------------------------------------------------------------- */
#include <stdio.h>

#include "Bsw_test_def.h"
#include "Bsw_sgTypes.h"
#include "Bsw_sgInterfaces.h"
#include "Bsw_traceEnhanced.h"
#include "Bsw_commonUtils.h"
#include "Bsw_drp.h"
/* ------------------------------------------------------------------------- */

#include "Bsw_ispbInterfaces.h"
#include "HALLEY.h"
#include "HALLEY_bits.h"


/* ------------------------------------------------------------------------- */
/* #### Internal functions definition ###################################### */
/* ------------------------------------------------------------------------- */

/* ------------------------------------------------------------------------- */
/**
    \brief  This function performs read access to the FPGA's
            Dynamic Reconfiguration Port (DRP).
    \param[in]  instId  logical board address
    \param[in]  devInst  device instance
    \param[in]  drp_itf  interface identification
    \param[in]  drp_lane lane number
    \param[in]  drp_addr drp address
    \param[out] drp_data drp data
    \return  see BSW_RetCode
*/
/* ****************************** */
BSW_RetCode bsw_readDrp(
  const BSW_InstId  *instId,
  BSW_DeviceIndex   devInst,
  ulong             drp_itf,
  ulong             drp_lane,
  ulong          drp_addr,
  ulong          *drp_data)
/* ****************************** */
/* ------------------------------------------------------------------------- */
{
  BSW_RetCode                 retCode = bsw_RetCode_Success;
  ulong                       regAddr, regData, regReadData, ii;
  ER_DRP_CFG_REG_u            drpCfg_er;
  HA_XAUI_LINE_DRP_CFG_REG_u  drpCfg_ha_xauiLine;
  HA_XAUI_MATE_DRP_CFG_REG_u  drpCfg_ha_xauiMate;
  HA_GTX_GBE_DRP_CFG_REG_u    drpCfg_ha_gtxGbe;
  HA_AURORA_DRP_CFG_REG_u     drpCfg_ha_aurora;
  uchar         trace_slot   = instId->boardNo;
  BSW_HwDevice  trace_device = bsw_HwDevice_All;
  BSW_TrModule  trace_module = bsw_TrModule_All;
  HAL_DRP_CFG_REG_u            drpCfg_hal;

  if ((devInst == bsw_DeviceIndex_Collector_Erlang_1) ||
      (devInst == bsw_DeviceIndex_Collector_Erlang_2) ||
      (devInst == bsw_DeviceIndex_Collector_Halley))
  {

    drpCfg_er.reg = 0;
    drpCfg_hal.reg = 0;

    switch (drp_itf)
    {
      case bsw_BKP_ITF:
        if (drp_lane >= 8)
        {
          bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
              ("*** bsw_readDrp: Invalid lane number!\r\n"));
          return (bsw_RetCode_InvalidPara);
        }
        if (devInst == bsw_DeviceIndex_Collector_Halley)
          drpCfg_hal.field.gtx = drp_lane;
        else
          drpCfg_er.field.gtx = drp_lane + 4;
        break;

      case bsw_STM16_ITF:
        // VALID only HALLEY
        if (drp_lane > 9)
        {
          bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
              ("*** bsw_readDrp: Invalid lane number!\r\n"));
          return (bsw_RetCode_InvalidPara);
        }
        // 8: XCVR STM16
        drpCfg_hal.field.gtx = drp_lane;
        break;

      case bsw_AURORA_ITF:
        if (drp_lane >= 4)
        {
          bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
              ("*** bsw_readDrp: Invalid lane number!\r\n"));
          return (bsw_RetCode_InvalidPara);
        }
        drpCfg_er.field.gtx = drp_lane;
        break;

      case bsw_SGMII_UP_ITF:
        drpCfg_er.field.gtx = 12;
        break;

      case bsw_SGMII_ER_ITF:
        drpCfg_er.field.gtx = 13;
        break;

      default:
        bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO,
            ("*** bsw_readDrp: Invalid interface!\r\n"));
        return (bsw_RetCode_InvalidPara);
    }


    if (devInst == bsw_DeviceIndex_Collector_Halley)
    {
      const as_new_RegMapPCE_t*    asicReg;
      const as_new_RegMapAlm_t*    asicRegAlm;
      HAL_DRP_DATA_RD_u            drpRead_hal;

      drpRead_hal.reg = 0;

      asicReg = &HALLEYRegCfg[HAL_DRP_CFG_REG];
      regAddr = bsw_physicalAsicAddress(asicReg,0,0,0);

      // DRP CMD RD :bit31  transition 0 to 1 =Read Operation
      drpCfg_hal.field.address_drp = drp_addr;
      drpCfg_hal.field.drp_cmd_wr    = 0;
      drpCfg_hal.field.drp_cmd_rd    = 1;
      regData = drpCfg_hal.reg;
      retCode = bsw_pBusWriteRegister(instId, devInst, regAddr, regData);
      if (retCode != bsw_RetCode_Success)  return (retCode);

      //transition return  bit 31 = 0
      drpCfg_hal.field.drp_cmd_rd    = 0;
      regData = drpCfg_hal.reg;
      retCode = bsw_pBusWriteRegister(instId, devInst, regAddr, regData);
      if (retCode != bsw_RetCode_Success)  return (retCode);

      BSW_USLEEP(100);
      asicRegAlm = &HALLEYRegPoll[HAL_DRP_DATA_RD];
      regAddr = bsw_physicalAsicAddrAlm(asicRegAlm,0,0,0);

      ii = 0;
      do
      {
        retCode = bsw_pBusReadRegister(instId, devInst, regAddr, &regReadData);
        drpRead_hal.reg = regReadData;
        if (retCode != bsw_RetCode_Success)  return (retCode);
        if (ii++ > 9)
        {
          bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
              ("*** bsw_readDrp: Timeout!\r\n"));
          return (bsw_RetCode_HwProblem);
        }
        BSW_USLEEP(10);
      }
      while (drpRead_hal.field.valid == 0);

      *drp_data = drpRead_hal.field.data_rd_drp;

    }
    else
    {  // _Erlang_1  _Erlang_2
      drpCfg_er.field.address = drp_addr;
      drpCfg_er.field.cmd     = 0;
      //  drpCfg_er.field.data    = drp_data;
      regAddr = ERL_DRP_CFG_REG;
      regData = drpCfg_er.reg;

      retCode = bsw_pBusWriteRegister(instId, devInst, regAddr, regData);
      if (retCode != bsw_RetCode_Success)  return (retCode);

      ii = 0;
      do
      {
        retCode = bsw_pBusReadRegister(instId, devInst, regAddr, &regData);
        drpCfg_er.reg = regData;
        if (retCode != bsw_RetCode_Success)  return (retCode);
        if (ii++ > 9)
        {
          bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
              ("*** bsw_readDrp: Timeout!\r\n"));
          return (bsw_RetCode_HwProblem);
        }
        BSW_USLEEP(10);
      }
      while (drpCfg_er.field.valid == 0);

      *drp_data = drpCfg_er.field.data;
    }
  }

  else if ((devInst == bsw_DeviceIndex_Collector_Hamilton_1) ||
      (devInst == bsw_DeviceIndex_Collector_Hamilton_2))
  {
    switch (drp_itf)
    {
      case bsw_AURORA_ITF:
        drpCfg_ha_aurora.reg = 0;
        if (drp_lane >= 4)
        {
          bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
              ("*** bsw_readDrp: Invalid lane number!\r\n"));
          return (bsw_RetCode_InvalidPara);
        }
        drpCfg_ha_aurora.field.gtx     = drp_lane;
        drpCfg_ha_aurora.field.address = drp_addr;
        drpCfg_ha_aurora.field.cmd     = 0;
// drpCfg_ha_aurora.field.data    = drp_data;
        regAddr = HAM_AURORA_DRP_CFG;
        regData = drpCfg_ha_aurora.reg;
        retCode = bsw_pBusWriteRegister(instId, devInst, regAddr, regData);
        if (retCode != bsw_RetCode_Success)  return (retCode);
        BSW_USLEEP(100);
        retCode = bsw_pBusReadRegister(instId, devInst, regAddr, &regData);
        if (retCode != bsw_RetCode_Success)  return (retCode);
        drpCfg_ha_aurora.reg = regData;
        *drp_data = drpCfg_ha_aurora.field.data;
        break;

      case bsw_XAUI_LINE_ITF:
        drpCfg_ha_xauiLine.reg = 0;
        if (drp_lane >= 4)
        {
          bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
              ("*** bsw_readDrp: Invalid lane number!\r\n"));
          return (bsw_RetCode_InvalidPara);
        }
        drpCfg_ha_xauiLine.field.gtx     = drp_lane;
        drpCfg_ha_xauiLine.field.address = drp_addr;
        drpCfg_ha_xauiLine.field.cmd     = 0;
// drpCfg_ha_xauiLine.field.data    = drp_data;
        regAddr = HAM_XAUI_LINE_DRP_CFG;
        regData = drpCfg_ha_xauiLine.reg;
        retCode = bsw_pBusWriteRegister(instId, devInst, regAddr, regData);
        if (retCode != bsw_RetCode_Success)  return (retCode);
        BSW_USLEEP(100);
        retCode = bsw_pBusReadRegister(instId, devInst, regAddr, &regData);
        if (retCode != bsw_RetCode_Success)  return (retCode);
        drpCfg_ha_xauiLine.reg = regData;
        *drp_data = drpCfg_ha_xauiLine.field.data;
        break;

      case bsw_XAUI_MATE_ITF:
        drpCfg_ha_xauiMate.reg = 0;
        if (drp_lane >= 4)
        {
          bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
              ("*** bsw_readDrp: Invalid lane number!\r\n"));
          return (bsw_RetCode_InvalidPara);
        }
        drpCfg_ha_xauiMate.field.gtx     = drp_lane;
        drpCfg_ha_xauiMate.field.address = drp_addr;
        drpCfg_ha_xauiMate.field.cmd     = 0;
// drpCfg_ha_xauiMate.field.data    = drp_data;
        regAddr = HAM_XAUI_MATE_DRP_CFG;
        regData = drpCfg_ha_xauiMate.reg;
        retCode = bsw_pBusWriteRegister(instId, devInst, regAddr, regData);
        if (retCode != bsw_RetCode_Success)  return (retCode);
        BSW_USLEEP(100);
        retCode = bsw_pBusReadRegister(instId, devInst, regAddr, &regData);
        if (retCode != bsw_RetCode_Success)  return (retCode);
        drpCfg_ha_xauiMate.reg = regData;
        *drp_data = drpCfg_ha_xauiMate.field.data;
        break;

      case bsw_GTX_GBE_ITF:
        drpCfg_ha_gtxGbe.reg = 0;
        if (drp_lane >= 11)
        {
          bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
              ("*** bsw_readDrp: Invalid lane number!\r\n"));
          return (bsw_RetCode_InvalidPara);
        }
        drpCfg_ha_gtxGbe.field.gtx     = drp_lane;
        drpCfg_ha_gtxGbe.field.address = drp_addr;
        drpCfg_ha_gtxGbe.field.cmd     = 0;
// drpCfg_ha_gtxGbe.field.data    = drp_data;
        regAddr = HAM_GTX_GBE_DRP_CFG;
        regData = drpCfg_ha_gtxGbe.reg;
        retCode = bsw_pBusWriteRegister(instId, devInst, regAddr, regData);
        if (retCode != bsw_RetCode_Success)  return (retCode);
        BSW_USLEEP(100);
        retCode = bsw_pBusReadRegister(instId, devInst, regAddr, &regData);
        if (retCode != bsw_RetCode_Success)  return (retCode);
        drpCfg_ha_gtxGbe.reg = regData;
        *drp_data = drpCfg_ha_gtxGbe.field.data;
        break;

      default:
        bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO,
            ("*** bsw_readDrp: Invalid interface!\r\n"));
        return (bsw_RetCode_InvalidPara);
    }
  }

  else
  {
    bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO,
        ("*** bsw_readDrp: Unknown device!\r\n"));
    return (bsw_RetCode_Unavailable);
  }

  return (retCode);
}
/* ------------------------------------------------------------------------- */


/* ------------------------------------------------------------------------- */
/**
    \brief  This function performs write access to the FPGA's
            Dynamic Reconfiguration Port (DRP).
    \param[in]  instId  logical board address
    \param[in]  devInst  device instance
    \param[in]  drp_itf  interface identification
    \param[in]  drp_lane lane number
    \param[in]  drp_addr drp address
    \param[in]  drp_data drp data
    \return  see BSW_RetCode
*/
/* ***************************** */
BSW_RetCode bsw_writeDrp(
  const BSW_InstId  *instId,
  BSW_DeviceIndex   devInst,
  ulong             drp_itf,
  ulong             drp_lane,
  ulong           drp_addr,
  ulong           drp_data)
/* ***************************** */
/* ------------------------------------------------------------------------- */
{
  BSW_RetCode                 retCode = bsw_RetCode_Success;
  ulong                       regAddr, regData;
  ER_DRP_CFG_REG_u            drpCfg_er;
  HA_XAUI_LINE_DRP_CFG_REG_u  drpCfg_ha_xauiLine;
  HA_XAUI_MATE_DRP_CFG_REG_u  drpCfg_ha_xauiMate;
  HA_GTX_GBE_DRP_CFG_REG_u    drpCfg_ha_gtxGbe;
  HA_AURORA_DRP_CFG_REG_u     drpCfg_ha_aurora;
  uchar         trace_slot   = instId->boardNo;
  BSW_HwDevice  trace_device = bsw_HwDevice_All;
  BSW_TrModule  trace_module = bsw_TrModule_All;
  const as_new_RegMapPCE_t*    asicReg;
  HAL_DRP_CFG_REG_u            drpCfg_hal;


  if ((devInst == bsw_DeviceIndex_Collector_Erlang_1) ||
      (devInst == bsw_DeviceIndex_Collector_Erlang_2) ||
      (devInst == bsw_DeviceIndex_Collector_Halley))
  {

    drpCfg_er.reg = 0;

    switch (drp_itf)
    {
      case bsw_BKP_ITF:
        if (drp_lane >= 8)
        {
          bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
              ("*** bsw_writeDrp: Invalid lane number!\r\n"));
          return (bsw_RetCode_InvalidPara);
        }
        if (devInst == bsw_DeviceIndex_Collector_Halley)
          drpCfg_hal.field.gtx = drp_lane;
        else
          drpCfg_er.field.gtx = drp_lane + 4;
        break;

      case bsw_STM16_ITF:
        // VALID only HALLEY
        if (drp_lane > 9)
        {
          bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
              ("*** bsw_readDrp: Invalid lane number!\r\n"));
          return (bsw_RetCode_InvalidPara);
        }
        // 8: XCVR STM16
        drpCfg_hal.field.gtx = drp_lane;
        break;

      case bsw_AURORA_ITF:
        if (drp_lane >= 4)
        {
          bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
              ("*** bsw_writeDrp: Invalid lane number!\r\n"));
          return (bsw_RetCode_InvalidPara);
        }
        drpCfg_er.field.gtx = drp_lane;
        break;

      case bsw_SGMII_UP_ITF:
        drpCfg_er.field.gtx = 12;
        break;

      case bsw_SGMII_ER_ITF:
        drpCfg_er.field.gtx = 13;
        break;

      default:
        bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO,
            ("*** bsw_writeDrp: Invalid interface!\r\n"));
        return (bsw_RetCode_InvalidPara);
    }

    if (devInst == bsw_DeviceIndex_Collector_Halley)
    {
      asicReg = &HALLEYRegCfg[HAL_DRP_CFG_REG];
      regAddr = bsw_physicalAsicAddress(asicReg,0,0,0);

      drpCfg_hal.field.data_wr_drp = drp_data;
      drpCfg_hal.field.address_drp = drp_addr;
      drpCfg_hal.field.drp_cmd_wr     = 1;
      drpCfg_hal.field.drp_cmd_rd    = 0;
      regData = drpCfg_hal.reg;
      retCode = bsw_pBusWriteRegister(instId, devInst, regAddr, regData);
      if (retCode != bsw_RetCode_Success)  return (retCode);

      //transition return  bit 16  = 0
      drpCfg_hal.field.drp_cmd_wr     = 0;
      regData = drpCfg_hal.reg;

    }
    else
    {
      drpCfg_er.field.address = drp_addr;
      drpCfg_er.field.cmd     = 1;
      drpCfg_er.field.data    = drp_data;
      regAddr = ERL_DRP_CFG_REG;
      regData = drpCfg_er.reg;
    }
  }

  else if ((devInst == bsw_DeviceIndex_Collector_Hamilton_1) ||
      (devInst == bsw_DeviceIndex_Collector_Hamilton_2))
  {
    switch (drp_itf)
    {
      case bsw_AURORA_ITF:
        drpCfg_ha_aurora.reg = 0;
        if (drp_lane >= 4)
        {
          bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
              ("*** bsw_writeDrp: Invalid lane number!\r\n"));
          return (bsw_RetCode_InvalidPara);
        }
        drpCfg_ha_aurora.field.gtx     = drp_lane;
        drpCfg_ha_aurora.field.address = drp_addr;
        drpCfg_ha_aurora.field.cmd     = 1;
        drpCfg_ha_aurora.field.data    = drp_data;
        regAddr = HAM_AURORA_DRP_CFG;
        regData = drpCfg_ha_aurora.reg;
        break;

      case bsw_XAUI_LINE_ITF:
        drpCfg_ha_xauiLine.reg = 0;
        if (drp_lane >= 4)
        {
          bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
              ("*** bsw_writeDrp: Invalid lane number!\r\n"));
          return (bsw_RetCode_InvalidPara);
        }
        drpCfg_ha_xauiLine.field.gtx     = drp_lane;
        drpCfg_ha_xauiLine.field.address = drp_addr;
        drpCfg_ha_xauiLine.field.cmd     = 1;
        drpCfg_ha_xauiLine.field.data    = drp_data;
        regAddr = HAM_XAUI_LINE_DRP_CFG;
        regData = drpCfg_ha_xauiLine.reg;
        break;

      case bsw_XAUI_MATE_ITF:
        drpCfg_ha_xauiMate.reg = 0;
        if (drp_lane >= 4)
        {
          bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
              ("*** bsw_writeDrp: Invalid lane number!\r\n"));
          return (bsw_RetCode_InvalidPara);
        }
        drpCfg_ha_xauiMate.field.gtx     = drp_lane;
        drpCfg_ha_xauiMate.field.address = drp_addr;
        drpCfg_ha_xauiMate.field.cmd     = 1;
        drpCfg_ha_xauiMate.field.data    = drp_data;
        regAddr = HAM_XAUI_MATE_DRP_CFG;
        regData = drpCfg_ha_xauiMate.reg;
        break;

      case bsw_GTX_GBE_ITF:
        drpCfg_ha_gtxGbe.reg = 0;
        if (drp_lane >= 11)
        {
          bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
              ("*** bsw_writeDrp: Invalid lane number!\r\n"));
          return (bsw_RetCode_InvalidPara);
        }
        drpCfg_ha_gtxGbe.field.gtx     = drp_lane;
        drpCfg_ha_gtxGbe.field.address = drp_addr;
        drpCfg_ha_gtxGbe.field.cmd     = 1;
        drpCfg_ha_gtxGbe.field.data    = drp_data;
        regAddr = HAM_GTX_GBE_DRP_CFG;
        regData = drpCfg_ha_gtxGbe.reg;
        break;

      default:
        bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO,
            ("*** bsw_writeDrp: Invalid interface!\r\n"));
        return (bsw_RetCode_InvalidPara);
    }
  }

  else
  {
    bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO,
        ("*** bsw_writeDrp: Unknown device!\r\n"));
    return (bsw_RetCode_Unavailable);
  }

  retCode = bsw_pBusWriteRegister(instId, devInst, regAddr, regData);

  return (retCode);
}
/* ------------------------------------------------------------------------- */


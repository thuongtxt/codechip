/* ========================================================================= */
/**
 \file  Bsw_eyeScanColl.cc
 \author  D. Materia
 \brief  Eye scan functions regarding devices of Collector units.
 \date  02-Apr-2012

 History:\n
   15-Mar-2012 (D. Materia)
   - Creation.

   20-Mar-2012 (D. Materia)
   - Added 'SwingT'.

   22-Mar-2012 (D. Materia)
   - Increased tolerance range for following interfaces:
      AURORA_ITF
      XAUI_LINE_ITF
      XAUI_MATE_ITF
   - Handled GES silicon.

   28-Mar-2012 (D. Materia)
   - Following functions have been elsewhere relocated:
    bsw_readDrp
    bsw_writeDrp

   02-Apr-2012 (D. Materia)
   - Eye swing lower limit has been changed (6 -> 4).
*/
/* ========================================================================= */


/* ------------------------------------------------------------------------- */
/* #### Include section #################################################### */
/* ------------------------------------------------------------------------- */
#include <stdio.h>
#include <string.h>

#include "Bsw_test_def.h"
#include "Bsw_sgTypes.h"
#include "Bsw_sgInterfaces.h"
#include "Bsw_traceEnhanced.h"
#include "Bsw_commonUtils.h"
#include "Bsw_eyeScanColl.h"

#include "Bsw_ispbInterfaces.h"
#include "HALLEY.h"
#include "HALLEY_bits.h"

#ifdef __cplusplus
extern "C"
{
#endif
#include "cpld_api.h"
#ifdef __cplusplus
}
#endif
/* ------------------------------------------------------------------------- */




/* ------------------------------------------------------------------------- */
/* #### Internal functions definition ###################################### */
/* ------------------------------------------------------------------------- */

/* ------------------------------------------------------------------------- */
/**
    \brief  This function performs EYE scan to some device's interfaces, via
            Dynamic Reconfiguration Port (DRP).
    \param[in]  instId  logical board address
    \param[in]  devInst  device instance
    \param[in]  drp_itf  interface identification
    \param[in]  drp_lane lane number
    \return  see BSW_RetCode
*/
/* ***************************** */
BSW_RetCode bsw_scan_eye(
  const BSW_InstId  *instId,
  BSW_DeviceIndex   devInst,
  ulong             drp_itf,
  ulong             drp_lane)
/* ***************************** */
/* ------------------------------------------------------------------------- */
{
  BSW_RetCode   retCode = bsw_RetCode_Success;
  char          *str_air_valid;
  int           air_version = 0;
  int           air_ext_version_MSN = 0;
  unsigned int  silicon_type = IES_SILICON;
  unsigned int  dfe = 0;
  unsigned int  prescale = 0;
//unsigned int  horz_inc = 1;
  unsigned int  vert_inc = 16;
  unsigned int  ui = 1;
  unsigned int  cnt = 0;
  int           measured_eyeWidthT;
  int           measured_eyeSwingT;
  int           expected_eyeWidthT, width_tol;
  int           expected_eyeSwingT_min = 4;
  uchar         trace_slot;
  BSW_HwDevice  trace_device;
  BSW_TrModule  trace_module;


  /* Trace parameters */
  trace_slot   = instId->boardNo;
  trace_device = bsw_HwDevice_All;
  trace_module = bsw_TrModule_All;

  bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO, ("\r\n*** bsw_scan_eye Entry point!\r\n"));

  switch (devInst)
  {
    case bsw_DeviceIndex_Collector_Erlang_2:
    case bsw_DeviceIndex_Collector_Hamilton_2:
      if (instId->boardType == bsw_BoardType_Collector_10x1GeppE)
        return (bsw_RetCode_InvalidPara);

    case bsw_DeviceIndex_Collector_Erlang_1:
    case bsw_DeviceIndex_Collector_Hamilton_1:
    case bsw_DeviceIndex_Collector_Halley:

      /* Get AIR version */
      str_air_valid = cpld_air_valid();
      if (strncmp(str_air_valid, "VALID", 5) == 0)
      {
        air_version = cpld_air_version();
        air_ext_version_MSN = (cpld_air_ext_ver()>>4)&0xF;
      }
      else
      {
        bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
            ("*** cpld_air_valid: AIR is invalid!\r\n"));
        return (bsw_RetCode_HwProblem);
      }

      switch (drp_itf)
      {
        case bsw_BKP_ITF:   // 128 taps
        case bsw_STM16_ITF:
          dfe = 1;
          width_tol = 128 / 4;
          expected_eyeWidthT = (128 - 2) - width_tol;
          break;
        case bsw_AURORA_ITF:   // 64 taps
//        width_tol = 64/4;
          width_tol = 18;
          expected_eyeWidthT = (64 - 2) - width_tol;
          break;
        case bsw_XAUI_LINE_ITF:
        case bsw_XAUI_MATE_ITF:   // 128 taps
//        width_tol = 128 / 4;
          width_tol = 38;
          expected_eyeWidthT = (128 - 2) - width_tol;
          break;
        case bsw_GTX_GBE_ITF:   // 256/2 taps (rescaled)
          width_tol = (256/2) / 4;
          expected_eyeWidthT = ((256/2) - 2) - width_tol;
          break;
        case bsw_SGMII_UP_ITF:
        case bsw_SGMII_ER_ITF:   // 256/2 taps (rescaled)
          width_tol = (256/2) / 4;
          expected_eyeWidthT = ((256/2) - 2) - width_tol;
          break;
        default:
          bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO,
              ("*** bsw_scan_eye: Invalid interface!\r\n"));
          return (bsw_RetCode_InvalidPara);
      }

      retCode = bsw_eyeScanEn(instId, devInst, drp_itf, drp_lane);
      if (retCode != bsw_RetCode_Success)  return (retCode);

      if ((air_ext_version_MSN == 0) && (air_version == 0)) 
        silicon_type = IES_SILICON;
      else
        silicon_type = GES_SILICON;      
      
      retCode = bsw_statEye2d(instId, devInst, drp_itf, drp_lane, silicon_type,
          dfe, prescale, vert_inc, ui, &cnt,
          &measured_eyeWidthT, &measured_eyeSwingT);
          
      if (retCode != bsw_RetCode_Success)  return (retCode);
      break;

    default:
      bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO,
          ("*** bsw_scan_eye: Unknown device!\r\n"));
      return (bsw_RetCode_Unavailable);
  }


  if ( (measured_eyeWidthT < (expected_eyeWidthT - width_tol)) ||
       (measured_eyeWidthT > (expected_eyeWidthT + width_tol)) )
  {
    bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
        ("*** bsw_scan_eye: Invalid total eye width: %d (%d..%d)\r\n",
         measured_eyeWidthT, (expected_eyeWidthT - width_tol), (expected_eyeWidthT + width_tol)));
    retCode = bsw_RetCode_Test_Fail;
  }

  if (measured_eyeSwingT < expected_eyeSwingT_min)
  {
    bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
        ("*** bsw_scan_eye: Invalid total eye swing: %d (>=%d)\r\n",
         measured_eyeSwingT, expected_eyeSwingT_min));
    retCode = bsw_RetCode_Test_Fail;
  }

  return (retCode);
}
/* ------------------------------------------------------------------------- */


/* ------------------------------------------------------------------------- */
/**
    \brief  This function performs EYE scanonly device's interfaces BKP/STM16, via
            Dynamic Reconfiguration Port (DRP).
    \param[in]  instId  logical board address
    \param[in]  devInst  device instance
    \param[in]  drp_itf  interface identification
    \param[in]  drp_lane lane number
    \return  see BSW_RetCode
*/
/* ***************************** */
BSW_RetCode bsw_scan_eyeTest(
  const BSW_InstId  *instId,
  BSW_DeviceIndex   devInst,
  ulong             drp_itf,
  ulong             drp_lane)
/* ***************************** */
/* ------------------------------------------------------------------------- */
{
  BSW_RetCode   retCode = bsw_RetCode_Success;
  unsigned int  silicon_type = IES_SILICON;
  unsigned int  dfe = 0;
  unsigned int  prescale = 0;
//unsigned int  horz_inc = 1;
  unsigned int  vert_inc = 16;
  unsigned int  ui = 1;
  unsigned int  cnt = 0;
  int           measured_eyeWidthT;
  int           measured_eyeSwingT;
  int           expected_eyeWidthT, width_tol;
  int           expected_eyeSwingT_min = 4;
  uchar         trace_slot;
  BSW_HwDevice  trace_device;
  BSW_TrModule  trace_module;

  HAL_DRP_CFG_REG_u            drpCfg_hal;
  ulong         regAddr, regData,regReadData, ii;
  ulong         drp_data_rd;

  /* Trace parameters */
  trace_slot   = instId->boardNo;
  trace_device = bsw_HwDevice_All;
  trace_module = bsw_TrModule_All;

  drpCfg_hal.reg = 0;

  bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO, ("\r\n*** bsw_scan_eyeTest Entry point!\r\n"));

  if (devInst == bsw_DeviceIndex_Collector_Halley)
  {
    switch (drp_itf)
    {
      case bsw_BKP_ITF:   // 128 taps
        dfe = 1;
        width_tol = 128 / 4;
        expected_eyeWidthT = (128 - 2) - width_tol;
        if (drp_lane >= 8)
        {
          bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
              ("*** bsw_readDrp: Invalid lane number!\r\n"));
          return (bsw_RetCode_InvalidPara);
        }

        drpCfg_hal.field.gtx = drp_lane;
        break;

      case bsw_STM16_ITF:
        dfe = 1;
        width_tol = 128 / 4;
        expected_eyeWidthT = (128 - 2) - width_tol;
        if (drp_lane > 9)
        {
          bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
              ("*** bsw_readDrp: Invalid lane number!\r\n"));
          return (bsw_RetCode_InvalidPara);
        }
        // 8: XCVR STM16
        drpCfg_hal.field.gtx = drp_lane;
        break;

      default:
        bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
            ("*** bsw_scan_eyeTest: Invalid interface!\r\n"));
        return (bsw_RetCode_InvalidPara);
    }

    // ogni volta che si lancia la procedura di bsw_eyeScanEn devo
    // abilitare la relativa macchina sul transceiver sotto test
    // resettare il transceiver e a fine test
    // disabilitare la macchina di eyescan
    // ridare ancora il reset al transceiver
#define DRP_PMA_RSV2  0x82  /* Register address */

    const as_new_RegMapAlm_t*    asicRegAlm;
    const as_new_RegMapPCE_t*    asicReg;
    HAL_DRP_DATA_RD_u            drpRead_hal;

    ulong                   drp_addr;

    drpRead_hal.reg = 0;
    drp_addr = DRP_PMA_RSV2;

    //**************  read  ****************
    // Now I do a reading
    asicReg = &HALLEYRegCfg[HAL_DRP_CFG_REG];
    regAddr = bsw_physicalAsicAddress(asicReg,0,0,0);

    // DRP CMD RD:bit31  transition 0 to 1 = Read Operation
    drpCfg_hal.field.address_drp = drp_addr;
    drpCfg_hal.field.drp_cmd_wr     = 0;
    drpCfg_hal.field.drp_cmd_rd    = 1;
    regData = drpCfg_hal.reg;
    retCode = bsw_pBusWriteRegister(instId, devInst, regAddr, regData);
    if (retCode != bsw_RetCode_Success)  return (retCode);

    //transition return  bit 31 = 0
    drpCfg_hal.field.drp_cmd_rd    = 0;
    regData = drpCfg_hal.reg;
    retCode = bsw_pBusWriteRegister(instId, devInst, regAddr, regData);
    if (retCode != bsw_RetCode_Success)  return (retCode);

    BSW_USLEEP(100);
    asicRegAlm = &HALLEYRegPoll[HAL_DRP_DATA_RD];
    regAddr = bsw_physicalAsicAddrAlm(asicRegAlm,0,0,0);

    ii = 0;
    do
    {
      retCode = bsw_pBusReadRegister(instId, devInst, regAddr, &regReadData);
      drpRead_hal.reg = regReadData;
      if (retCode != bsw_RetCode_Success)  return (retCode);
      if (ii++ > 9)
      {
        bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
            ("*** bsw_readDrp: Timeout!\r\n"));
        return (bsw_RetCode_HwProblem);
      }
      BSW_USLEEP(10);
    }
    while (drpRead_hal.field.valid == 0);

    drp_data_rd = drpRead_hal.field.data_rd_drp;

    BSW_USLEEP(100);

    // ***************   write   ******************
    // Now I do a writing
    asicReg = &HALLEYRegCfg[HAL_DRP_CFG_REG];
    regAddr = bsw_physicalAsicAddress(asicReg,0,0,0);

    // write bit 5 reg PMA_RSV2 enable machine eyescan
    // drp_data_rd  maschero il bit 5
    drp_data_rd = (drp_data_rd | 0x20);

    drpCfg_hal.field.address_drp = drp_addr;
    drpCfg_hal.field.data_wr_drp = drp_data_rd;
    drpCfg_hal.field.drp_cmd_wr  = 1;
    drpCfg_hal.field.drp_cmd_rd  = 0;
    regData = drpCfg_hal.reg;
    retCode = bsw_pBusWriteRegister(instId, devInst, regAddr, regData);
    if (retCode != bsw_RetCode_Success)  return (retCode);

    //transition return  bit 16  = 0
    drpCfg_hal.field.drp_cmd_wr     = 0;
    regData = drpCfg_hal.reg;
    retCode = bsw_pBusWriteRegister(instId, devInst, regAddr, regData);
    if (retCode != bsw_RetCode_Success)  return (retCode);

    if (drp_lane <8)
    {
      /* reset software del transceiver XCVR BKP SOFT RESET reg 4c4H */
      asicReg = &HALLEYRegCfg[HAL_XCVR_BKP_SOFT_RESET];
      regAddr = bsw_physicalAsicAddress(asicReg,0,0,0);
      regData = 1;
      retCode = bsw_pBusWriteRegister(instId, devInst, regAddr, regData);
      if (retCode != bsw_RetCode_Success)  return (retCode);
      regData = 0;
      retCode = bsw_pBusWriteRegister(instId, devInst, regAddr, regData);
      if (retCode != bsw_RetCode_Success)  return (retCode);
    }
    else
    {
      /* reset software del transceiver STM16 reg 554H */
      asicReg = &HALLEYRegCfg[HAL_XCVR_STM16_SOFT_RESET];
      regAddr = bsw_physicalAsicAddress(asicReg,0,0,0);
      regData = 1;
      retCode = bsw_pBusWriteRegister(instId, devInst, regAddr, regData);
      if (retCode != bsw_RetCode_Success)  return (retCode);
      BSW_USLEEP(100);
      regData = 0;
      retCode = bsw_pBusWriteRegister(instId, devInst, regAddr, regData);
      if (retCode != bsw_RetCode_Success)  return (retCode);
    }

    silicon_type = (GES_SILICON);

    retCode = bsw_statEye2d(instId, devInst, drp_itf, drp_lane, silicon_type,
        dfe, prescale, vert_inc, ui, &cnt,
        &measured_eyeWidthT, &measured_eyeSwingT);
    if (retCode != bsw_RetCode_Success)  return (retCode);

    //write bit5 reg PMA_RSV2 disable eyescan
    asicReg = &HALLEYRegCfg[HAL_DRP_CFG_REG];
    regAddr = bsw_physicalAsicAddress(asicReg,0,0,0);
    // disanble= 0 il bit 5
    ulong    mask = 0xffdf;

    drp_data_rd = (drp_data_rd & mask);

    drpCfg_hal.field.address_drp = drp_addr;
    drpCfg_hal.field.data_wr_drp = drp_data_rd;
    drpCfg_hal.field.drp_cmd_wr  = 1;
    drpCfg_hal.field.drp_cmd_rd  = 0;
    regData = drpCfg_hal.reg;
    retCode = bsw_pBusWriteRegister(instId, devInst, regAddr, regData);
    if (retCode != bsw_RetCode_Success)  return (retCode);

    //transition return  bit 16  = 0
    drpCfg_hal.field.drp_cmd_wr  = 0;
    regData = drpCfg_hal.reg;
    retCode = bsw_pBusWriteRegister(instId, devInst, regAddr, regData);
    if (retCode != bsw_RetCode_Success)  return (retCode);

    if (drp_lane <8)
    {
      /* reset software del transceiver XCVR BKP SOFT RESET reg 4c4H */
      asicReg = &HALLEYRegCfg[HAL_XCVR_BKP_SOFT_RESET];
      regAddr = bsw_physicalAsicAddress(asicReg,0,0,0);
      regData = 1;
      retCode = bsw_pBusWriteRegister(instId, devInst, regAddr, regData);
      if (retCode != bsw_RetCode_Success)  return (retCode);
      regData = 0;
      retCode = bsw_pBusWriteRegister(instId, devInst, regAddr, regData);
      if (retCode != bsw_RetCode_Success)  return (retCode);
    }
    else
    {
      /* reset software del transceiver STM16 reg 554H */
      asicReg = &HALLEYRegCfg[HAL_XCVR_STM16_SOFT_RESET];
      regAddr = bsw_physicalAsicAddress(asicReg,0,0,0);
      regData = 1;
      retCode = bsw_pBusWriteRegister(instId, devInst, regAddr, regData);
      if (retCode != bsw_RetCode_Success)  return (retCode);
      BSW_USLEEP(100);
      regData = 0;
      retCode = bsw_pBusWriteRegister(instId, devInst, regAddr, regData);
      if (retCode != bsw_RetCode_Success)  return (retCode);
    }
  }
  else
  {
    bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
        ("*** bsw_scan_eye: Unknown device!\r\n"));

    return (bsw_RetCode_Unavailable);
  }

  if ( (measured_eyeWidthT < (expected_eyeWidthT - width_tol)) ||
       (measured_eyeWidthT > (expected_eyeWidthT + width_tol)) )
  {
    bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
        ("*** bsw_scan_eyeTest: Invalid total eye width: %d (%d..%d)\r\n",
         measured_eyeWidthT, (expected_eyeWidthT - width_tol), (expected_eyeWidthT + width_tol)));
    retCode = bsw_RetCode_Test_Fail;
  }

  if (measured_eyeSwingT < expected_eyeSwingT_min)
  {
    bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
        ("*** bsw_scan_eyeTest: Invalid total eye swing: %d (>=%d)\r\n",
         measured_eyeSwingT, expected_eyeSwingT_min));
    retCode = bsw_RetCode_Test_Fail;
  }

  return (retCode);
}
/* ------------------------------------------------------------------------- */


/* ------------------------------------------------------------------------- */
/**
    \brief  This function enables EYE scan, via Dynamic Reconfig. Port (DRP).
    \param[in]  instId  logical board address
    \param[in]  devInst  device instance
    \param[in]  drp_itf  interface identification
    \param[in]  drp_lane lane number
    \return  see BSW_RetCode
*/
/* ***************************** */
BSW_RetCode bsw_eyeScanEn(
  const BSW_InstId  *instId,
  BSW_DeviceIndex   devInst,
  ulong             drp_itf,
  ulong             drp_lane)
/* ***************************** */
/* ------------------------------------------------------------------------- */
{
  BSW_RetCode           retCode = bsw_RetCode_Success;
  DRP_ES_EYE_SCAN_EN_u drpEsEyeScanEn;

  retCode = bsw_readDrp(instId, devInst, drp_itf, drp_lane, DRP_ES_EYE_SCAN_EN_ADD, &drpEsEyeScanEn.reg);
  if (retCode != bsw_RetCode_Success)  return (retCode);
  drpEsEyeScanEn.field.es_eye_scan_en = 1;
  drpEsEyeScanEn.field.es_errdet_en   = 1;
  retCode = bsw_writeDrp(instId, devInst, drp_itf, drp_lane, DRP_ES_EYE_SCAN_EN_ADD, drpEsEyeScanEn.reg);

  return (retCode);
}
/* ------------------------------------------------------------------------- */


/* ***************************** */
BSW_RetCode bsw_getEsBerRate(
  const BSW_InstId  *instId,
  BSW_DeviceIndex   devInst,
  ulong             drp_itf,
  ulong             drp_lane,
  int               horz_pt,
  int               vert_pt,
  unsigned int      dfe,
  float             *ber,
  float             *samples,
  float             *errs,
  float             *maxerrs)
/* ***************************** */
{
  BSW_RetCode              retCode = bsw_RetCode_Success;
  DRP_ES_VERT_OFFSET_u     esVertOffset;
  DRP_ES_HORZ_OFFSET_u     esHorzOffset;
  DRP_RX_DATA_WIDTH_u      rxDataWidth;
  DRP_ES_EYE_SCAN_EN_u     drpEsEyeScanEn;
  DRP_ES_ERROR_COUNT_u     esErrorCount;
  DRP_ES_SAMPLE_COUNT_u    esSampleCount;
  DRP_ES_CONTROL_STATUS_u  drpEsControlStatus;
  unsigned int             width = 0;
  unsigned int             errs1, errs2;
  unsigned int             samples1, samples2;
  unsigned int             err_overflow, sam_overflow;
  unsigned int             cnt;
  unsigned int             done;
  unsigned int             prescale;
  unsigned int             error_count, sample_count;
  unsigned int             ii;
  float                    ber1, ber2;
  uchar         trace_slot   = instId->boardNo;
  BSW_HwDevice  trace_device = bsw_HwDevice_All;
  BSW_TrModule  trace_module = bsw_TrModule_All;


  /* Vertical offset (differential voltage) */
  retCode = bsw_readDrp(instId, devInst, drp_itf, drp_lane, DRP_ES_VERT_OFFSET_ADD, &esVertOffset.reg);
  if (retCode != bsw_RetCode_Success)  return (retCode);
  esVertOffset.field.es_vert_offset = (vert_pt & 0xfff);
  retCode = bsw_writeDrp(instId, devInst, drp_itf, drp_lane, DRP_ES_VERT_OFFSET_ADD, esVertOffset.reg);
  if (retCode != bsw_RetCode_Success)  return (retCode);

  /* Horizontal offset (phase) */
  retCode = bsw_readDrp(instId, devInst, drp_itf, drp_lane, DRP_ES_HORZ_OFFSET_ADD, &esHorzOffset.reg);
  if (retCode != bsw_RetCode_Success)  return (retCode);
  esHorzOffset.field.es_horz_offset = horz_pt & 0xfff;
  retCode = bsw_writeDrp(instId, devInst, drp_itf, drp_lane, DRP_ES_HORZ_OFFSET_ADD, esHorzOffset.reg);
  if (retCode != bsw_RetCode_Success)  return (retCode);

  /* Width */
  retCode = bsw_readDrp(instId, devInst, drp_itf, drp_lane, DRP_RX_DATA_WIDTH_ADD, &rxDataWidth.reg);
  if (retCode != bsw_RetCode_Success)  return (retCode);

  switch (rxDataWidth.field.rx_data_width)
  {
    case 2:
      width = 16;
      break;
    case 3:
      width = 20;
      break;
    case 4:
      width = 32;
      break;
    case 5:
      width = 40;
      break;
    case 6:
      width = 64;
      break;
    case 7:
      width = 80;
      break;
    default:
      return (bsw_RetCode_InvalidPara);
      bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
          ("*** bsw_getEsBerRate: Invalid 'rx_data_width' value!\r\n"));
      return (bsw_RetCode_InvalidPara);
  }

  errs1 = 0;
  errs2 = 0;
  samples1 = 0;
  samples2 = 0;
  ber1 = 0.0;
  ber2 = 0.0;
  err_overflow = 0;
  sam_overflow = 0;

  for (ii = 0; ii <= dfe; ii++)
  {
    retCode = bsw_readDrp(instId, devInst, drp_itf, drp_lane, DRP_ES_VERT_OFFSET_ADD, &esVertOffset.reg);
    if (retCode != bsw_RetCode_Success)  return (retCode);
    esVertOffset.field.es_ut_sign = ii;
    retCode = bsw_writeDrp(instId, devInst, drp_itf, drp_lane, DRP_ES_VERT_OFFSET_ADD, esVertOffset.reg);
    if (retCode != bsw_RetCode_Success)  return (retCode);

    retCode = bsw_readDrp(instId, devInst, drp_itf, drp_lane, DRP_ES_EYE_SCAN_EN_ADD, &drpEsEyeScanEn.reg);
    if (retCode != bsw_RetCode_Success)  return (retCode);
    drpEsEyeScanEn.field.es_control = 0;  // Move from END to WAIT state
    retCode = bsw_writeDrp(instId, devInst, drp_itf, drp_lane, DRP_ES_EYE_SCAN_EN_ADD, drpEsEyeScanEn.reg);
    if (retCode != bsw_RetCode_Success)  return (retCode);
    drpEsEyeScanEn.field.es_control = 1;  // move from WAIT to ARM state
    retCode = bsw_writeDrp(instId, devInst, drp_itf, drp_lane, DRP_ES_EYE_SCAN_EN_ADD, drpEsEyeScanEn.reg);
    if (retCode != bsw_RetCode_Success)  return (retCode);

    cnt = 0;
    done = 0;
    do
    {
      retCode = bsw_readDrp(instId, devInst, drp_itf, drp_lane, DRP_ES_CONTROL_STATUS_ADD, &drpEsControlStatus.reg);
      if (retCode != bsw_RetCode_Success)  return (retCode);

      done = (drpEsControlStatus.field.es_control_status & 0x01);

      if (cnt++ > 19)
      {
        bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_HW_ERROR,
            ("*** bsw_getEsBerRate: drpEsControlStatus timeout!\r\n"));
        return (bsw_RetCode_HwProblem);
      }
      BSW_USLEEP(1000);
    }
    while (done == 0);

    /* Pre-scaling */
    retCode = bsw_readDrp(instId, devInst, drp_itf, drp_lane, DRP_ES_VERT_OFFSET_ADD, &esVertOffset.reg);
    if (retCode != bsw_RetCode_Success)  return (retCode);
    prescale = (2 ^ (esVertOffset.field.es_prescale + 1));

    /* Error count */
    retCode = bsw_readDrp(instId, devInst, drp_itf, drp_lane, DRP_ES_ERROR_COUNT_ADD, &esErrorCount.reg);
    if (retCode != bsw_RetCode_Success)  return (retCode);
    error_count = esErrorCount.field.es_error_count;

    /* Sample count */
    retCode = bsw_readDrp(instId, devInst, drp_itf, drp_lane, DRP_ES_SAMPLE_COUNT_ADD, &esSampleCount.reg);
    if (retCode != bsw_RetCode_Success)  return (retCode);
    sample_count = esSampleCount.field.es_sample_count;

    err_overflow = (error_count == 0xffff);
    sam_overflow = (sample_count == 0xffff);

    //thiserrs = 0;
    //thissamples = 0;

    if (ii == 1)
    {
      errs2 = error_count;
      samples2 = sample_count * prescale * width;
      ber2 = (errs2 == 0) ? ((float)(errs2 + 1) / samples2) : ((float)errs2 / samples2);
      //thiserrs = errs2;
      //thissamples = samples2;
    }
    else
    {
      errs1 = error_count;
      samples1 = sample_count * prescale * width;
      ber1 = (errs1 == 0) ? ((float)(errs1 + 1) / samples1) : ((float)errs1 / samples1);
      //thiserrs = errs1;
      //thissamples = samples1;
    }

  }

  if (ber1 == 0)
  {
    *ber     = ber2;
    *samples = samples2;
    *errs    = errs2;
    *maxerrs = 65535;
  }
  else if (ber2 == 0)
  {
    *ber     = ber1;
    *samples = samples1;
    *errs    = errs1;
    *maxerrs = 65535;
  }
  else
  {
    *ber     = (ber1 + ber2) / 2;
    *samples = (samples1 + samples2);
    *errs    = (errs1 + errs2);
    *maxerrs = 131070;
  }

  return (retCode);
}


/* **************** */
void esProcessResults(
  float errs,
  float maxerrs)
/* **************** */
{
  uchar         trace_slot   = BSW_ALL_SLOT;
  BSW_HwDevice  trace_device = bsw_HwDevice_All;
  BSW_TrModule  trace_module = bsw_TrModule_All;

  if (errs == 0)
  {
    bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO, ("_"));
  }
  else if (errs == maxerrs)
  {
    bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO, ("X"));
  }
  else
  {
    bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO, ("x"));
  }
  fflush(stdout);
}


/* ******************************* */
BSW_RetCode bsw_getGtxRate(
  const BSW_InstId  *instId,
  ulong             drp_itf,
  float     *line_rate)
/* ******************************* */
{
  switch (drp_itf)
  {
    case bsw_BKP_ITF:
      *line_rate = 3.1104;
      break;
    case bsw_STM16_ITF:
      *line_rate = 2.5;
      break;
    case bsw_AURORA_ITF:
      *line_rate = 4.0;
      break;
    case bsw_XAUI_LINE_ITF:
      *line_rate = 3.125;
      break;
    case bsw_XAUI_MATE_ITF:
      *line_rate = 3.125;
      break;
    case bsw_GTX_GBE_ITF:
      *line_rate = 1.25;
      break;
    case bsw_SGMII_UP_ITF:
      *line_rate = 1.25;
      break;
    case bsw_SGMII_ER_ITF:
      *line_rate = 1.25;
      break;
    default:
      return (bsw_RetCode_InvalidPara);
  }
  return (bsw_RetCode_Success);
}


/* ------------------------------------------------------------------------- */
/**
    \brief  This function displays 'one-dimensional' EYE scan status, via
            Dynamic Reconfiguration Port (DRP).
    \param[in]  instId  logical board address
    \param[in]  devInst  device instance
    \param[in]  drp_itf  interface identification
    \param[in]  drp_lane lane number
    \param[in]  silicon_type    silicon type:   1 = GES, 0 = IES
    \param[in]  dfe      equalizer type: 1 = DFE, 0 = LPM
    \param[in]  vert_pt  vertical points
    \param[in]  prescale  prescale parameter
    \param[in]  horz_inc horizontal increment
    \param[in]  ui       unit interval
    \param[in]  rxout_div       RxOut divider value
    \param[in]  cnt_start       counter start
    \param[out] cnt       counter
    \param[out] eyeWidthT   total eye width
    \return  see BSW_RetCode
*/
/* ********************************* */
BSW_RetCode bsw_statEye1d(
  const BSW_InstId  *instId,
  BSW_DeviceIndex   devInst,
  ulong             drp_itf,
  ulong             drp_lane,
  unsigned int    silicon_type,
  unsigned int      dfe,
  int               vert_pt,
  unsigned int      prescale,
  unsigned int      horz_inc,
  unsigned int      ui,
  unsigned int      rxout_div,
  unsigned int      cnt_start,
  unsigned int      *cnt,
  int               *eyeWidthT)
/* ********************************* */
/* ------------------------------------------------------------------------- */
{
  BSW_RetCode           retCode = bsw_RetCode_Success;
  unsigned int          es_qual_mask;
  unsigned int          es_sdata_mask;
  DRP_ES_QUAL_MASK_u    esQualMask;
  DRP_ES_SDATA_MASK_u   esSdataMask;
  DRP_ES_VERT_OFFSET_u  esVertOffset;
  //DRP_RXOUT_DIV_u     rxoutDiv;
  //unsigned int        rxout_div;
  float                 ber, samples, errs, maxerrs;
  int                   horz_min, horz_max;
  int                   ii;
  uchar         trace_slot   = instId->boardNo;
  BSW_HwDevice  trace_device = bsw_HwDevice_All;
  BSW_TrModule  trace_module = bsw_TrModule_All;


  *eyeWidthT = 0;

  /* Vertical offset (differential voltage) */
  retCode = bsw_readDrp(instId, devInst, drp_itf, drp_lane, DRP_ES_VERT_OFFSET_ADD, &esVertOffset.reg);
  if (retCode != bsw_RetCode_Success)  return (retCode);
  esVertOffset.field.es_vert_offset = (vert_pt & 0xfff);
  esVertOffset.field.es_prescale    = prescale;
  retCode = bsw_writeDrp(instId, devInst, drp_itf, drp_lane, DRP_ES_VERT_OFFSET_ADD, esVertOffset.reg);
  if (retCode != bsw_RetCode_Success)  return (retCode);

  if (dfe == 1)
  {
    bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO,
        ("(%4d mV)  ", EVAL_SWING(vert_pt, (silicon_type == IES_SILICON)
            ? DFE_IES_VOLTAGE_SWING
            : DFE_GES_VOLTAGE_SWING) ));
  }
  else
  {
    bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO,
        ("(%4d mV)  ", EVAL_SWING(vert_pt, (silicon_type == IES_SILICON)
            ? LPM_IES_VOLTAGE_SWING
            : LPM_GES_VOLTAGE_SWING) ));
  }
  fflush(stdout);


  // es_qual_mask = 0xFFFFFfffffFFFFFfffff;
  for (ii = 0; ii < DRP_ES_QUAL_MASK_RPT; ii++)
  {
    switch (ii)
    {
      case 4:
        es_qual_mask = 0xffff;
        break;
      case 3:
        es_qual_mask = 0xffff;
        break;
      case 2:
        es_qual_mask = 0xffff;
        break;
      case 1:
        es_qual_mask = 0xffff;
        break;
      case 0:
        es_qual_mask = 0xffff;
        break;
      default:
        return (bsw_RetCode_InvalidPara);
    }
    esQualMask.field.es_qual_mask = es_qual_mask;
    retCode = bsw_writeDrp(instId, devInst, drp_itf, drp_lane, (DRP_ES_QUAL_MASK_ADD + ii), esQualMask.reg);
    if (retCode != bsw_RetCode_Success)  return (retCode);
  }


  // es_sdata_mask = 0xFFFFFfffff0000000000;
  for (ii = 0; ii < DRP_ES_SDATA_MASK_RPT; ii++)
  {
    switch (ii)
    {
      case 4:
        es_sdata_mask = 0xffff;
        break;
      case 3:
        es_sdata_mask = 0xffff;
        break;
      case 2:
        es_sdata_mask = 0xff00;
        break;
      case 1:
        es_sdata_mask = 0x0000;
        break;
      case 0:
        es_sdata_mask = 0x0000;
        break;
      default:
        return (bsw_RetCode_InvalidPara);
    }
    esSdataMask.field.es_sdata_mask = es_sdata_mask;
    retCode = bsw_writeDrp(instId, devInst, drp_itf, drp_lane, (DRP_ES_SDATA_MASK_ADD + ii), esSdataMask.reg);
    if (retCode != bsw_RetCode_Success)  return (retCode);
  }


// /* Rxout div */
// retCode = bsw_readDrp(instId, devInst, drp_itf, drp_lane, DRP_RXOUT_DIV_ADD, &rxoutDiv.reg);
// if (retCode != bsw_RetCode_Success)  return (retCode);
// rxout_div = rxoutDiv.field.rxout_div;

  switch (rxout_div)
  {
    case 1:
      horz_min =  -64;
      horz_max =  64;
      break;
    case 2:
      horz_min = -128;
      horz_max = 128;
      break;
    case 3:
      horz_min = -256;
      horz_max = 256;
      break;
    case 4:
      horz_min = -512;
      horz_max = 512;
      break;
    default:
      horz_min =  -32;
      horz_max =  32;
  }
  horz_min = (horz_min * ui);
  horz_max = (horz_max * ui);

  *cnt = cnt_start;
  for (ii = horz_min; ii <= horz_max; ii += horz_inc)
  {
    retCode = bsw_getEsBerRate(instId, devInst, drp_itf, drp_lane, ii, vert_pt,
        dfe, &ber, &samples, &errs, &maxerrs);
    if (retCode != bsw_RetCode_Success)  return (retCode);
    esProcessResults(errs, maxerrs);
    if (errs == 0)  (*eyeWidthT)++;
    (*cnt)++;
  }
  bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO, ("\r\n"));
  if (*eyeWidthT > 1)  *eyeWidthT -= 1;
  *cnt -= 1;

  return (retCode);
}
/* ------------------------------------------------------------------------- */


/* ------------------------------------------------------------------------- */
/**
    \brief  This function displays 'two-dimensional' EYE scan status, via
            Dynamic Reconfiguration Port (DRP).
    \param[in]  instId  logical board address
    \param[in]  devInst  device instance
    \param[in]  drp_itf  interface identification
    \param[in]  drp_lane lane number
    \param[in]  silicon_type    silicon type:   1 = GES, 0 = IES
    \param[in]  dfe      equalizer type: 1 = DFE, 0 = LPM
    \param[in]  prescale prescale parameter
    \param[in]  vert_inc vertical increment
    \param[in]  ui       unit interval
    \param[out] cnt       counter
    \param[out] maxEyeWidthT    max total eye width (horizontal)
    \param[out] maxEyeSwingT    max total eye swing (vertical)
    \return  see BSW_RetCode
*/
/* ********************************** */
BSW_RetCode bsw_statEye2d(
  const BSW_InstId  *instId,
  BSW_DeviceIndex   devInst,
  ulong             drp_itf,
  ulong             drp_lane,
  unsigned int    silicon_type,
  unsigned int    dfe,
  unsigned int    prescale,
  unsigned int   vert_inc,
  unsigned int    ui,
  unsigned int   *cnt,
  int               *maxEyeWidthT,
  int               *maxEyeSwingT)
/* ********************************** */
/* ------------------------------------------------------------------------- */
{
  BSW_RetCode      retCode = bsw_RetCode_Success;
  DRP_RXOUT_DIV_u  rxoutDiv;
  int              ii;
  unsigned int     rxout_div;
  unsigned int     cnt_1d;
  unsigned int     horz_inc;
  int              horz_min, horz_max, horz_range;
  int              vert_pt, voltageSwing;
  int              eyeWidthT;
  float            line_rate_Gbps, phase_tap, time_tap_ps;
  BSW_TraceEnh     traceLevelEnhanced_SAVE;
  uchar         trace_slot   = instId->boardNo;
  BSW_HwDevice  trace_device = bsw_HwDevice_All;
  BSW_TrModule  trace_module = bsw_TrModule_All;


  /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  // Save default tracing parameters
  traceLevelEnhanced_SAVE = bsw_TraceLevelEnhanced;
  // Set specific tracing
  bsw_TraceLevelEnhanced.slotNo   = BSW_ALL_SLOT;
  bsw_TraceLevelEnhanced.deviceNo = bsw_HwDevice_All;
  bsw_TraceLevelEnhanced.moduleNo = bsw_TrModule_All;
  bsw_TraceLevelEnhanced.levelNo  = 0x7;
  bsw_TraceLevelEnhanced.showFile = false;
  bsw_TraceLevelEnhanced.enableLF = false;
  /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

  *cnt = 1;
  *maxEyeWidthT = 0;
  *maxEyeSwingT = 0;
  if (silicon_type == IES_SILICON)
  {
    voltageSwing = (dfe == 1) ? (2 * DFE_IES_VOLTAGE_SWING) : (2 * LPM_IES_VOLTAGE_SWING);
    bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO,("           IES SILICON\r\n"));
  }
  else
  {
    voltageSwing = (dfe == 1) ? (2 * DFE_GES_VOLTAGE_SWING) : (2 * LPM_GES_VOLTAGE_SWING);
    bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO,("           GES SILICON\r\n"));
  }

  /* Rxout div */
  retCode = bsw_readDrp(instId, devInst, drp_itf, drp_lane, DRP_RXOUT_DIV_ADD, &rxoutDiv.reg);
  if (retCode != bsw_RetCode_Success)  return (retCode);
  rxout_div = rxoutDiv.field.rxout_div;

  switch (rxout_div)
  {
    case 1:
      horz_min =  -64;
      horz_max =  64;
      horz_inc = 1;
      break;
    case 2:
      horz_min = -128;
      horz_max = 128;
      horz_inc = 2;
      break;
    case 3:
      horz_min = -256;
      horz_max = 256;
      horz_inc = 4;
      break;
    case 4:
      horz_min = -512;
      horz_max = 512;
      horz_inc = 8;
      break;
    default:
      horz_min =  -32;
      horz_max =  32;
      horz_inc = 1;
  }
  horz_min   = (horz_min * ui);
  horz_max   = (horz_max * ui);
//horz_range = (horz_max - horz_min);
  horz_range = (horz_inc > 1) ? (horz_max - horz_min) / horz_inc : (horz_max - horz_min);

  retCode = bsw_getGtxRate(instId, drp_itf, &line_rate_Gbps);
  if (retCode != bsw_RetCode_Success)  return (retCode);
  phase_tap   = (360 / horz_range);
  time_tap_ps = (1000 / line_rate_Gbps) / horz_range;
  bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO, ("\r\n"));
  bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO,
      ("           Horizontal range: %d taps  (%.1f ps, %.4f GHz)\r\n",
       horz_range, (1000 / line_rate_Gbps), line_rate_Gbps));
//bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO,
//          ("           (1 tap = %.1f ps or %.1f degree)\r\n\r\n",
//           time_tap_ps, phase_tap));
  bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO,
      ("           (1 tap = %.3f ps)\r\n\r\n",
       time_tap_ps));

  for (ii = 256; ii >= 0; ii -= vert_inc)
  {
    vert_pt = (ii / 128) ? ( (ii == 128) ? 0 : ((ii - 1) % 128) )
            : ( ~(ii % 128) & 0xff );
    retCode = bsw_statEye1d(instId, devInst, drp_itf, drp_lane, silicon_type, dfe,
        vert_pt, prescale, horz_inc, ui, rxout_div, *cnt,
        &cnt_1d, &eyeWidthT);
    if (retCode != bsw_RetCode_Success)  return (retCode);
    *cnt += cnt_1d;
    if (eyeWidthT > *maxEyeWidthT)  *maxEyeWidthT = eyeWidthT;
//  if (eyeWidthT > 0)              (*maxEyeSwingT)++;
    if (eyeWidthT > 2)              (*maxEyeSwingT)++;
  }
  (*maxEyeSwingT)--;

  bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO, ("\r\n"));
  bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO,
      ("-- eyeTapsT_horz = %3d (%.1f ps)\r\n", *maxEyeWidthT,
       (*maxEyeWidthT * time_tap_ps) ));
  bsw_trEnh(trace_slot, trace_device, trace_module, BSW_TRC_INFO,
      ("-- eyeTapsT_vert = %3d (%.0f mV)\r\n", *maxEyeSwingT,
       (*maxEyeSwingT * ((float)voltageSwing / vert_inc)) ));

  /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
  // Restore default tracing parameters
  bsw_TraceLevelEnhanced = traceLevelEnhanced_SAVE;
  /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

  return (retCode);
}
/* ------------------------------------------------------------------------- */


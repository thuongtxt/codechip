/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_pos_sdh_alarm.c
* 文件标识: 
* 其它说明: 驱动STM-1 POS PWE3模块的STM-1接口的SDH告警实现函数
* 当前版本: V1.0
* 作    者: 刘钰00130907.
* 完成日期: 2013-08-30
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/

#include "drv_pos_sdh_alarm.h"
#include "drv_pos_comm.h"

#include "bsp_cp3ban_common.h"

#include "bsp_sdh_common.h"

#include "AtSdhPath.h"


/* 定义全局变量 */
/* 监测SDH告警的使能标记,默认非使能.在初始化单板时,使能标记;在卸载单板时,非使能标记. */
static WORD32 g_drv_pos_sdh_alarm_enable[DRV_POS_MAX_SUBCARD]; 
static WORD32 g_drv_pos_sdhSecAlmEnable[DRV_POS_MAX_SUBCARD][DRV_POS_MAX_PORT];
static WORD32 g_drv_pos_vc4AlmEnable[DRV_POS_MAX_SUBCARD][DRV_POS_MAX_PORT];

/* SDH alarm线程的threadId */
static int g_drv_pos_sdh_alarm_thread_id[DRV_POS_MAX_SUBCARD]; 

/* g_drv_pos_sdh_alarm_thread用来保存SDH alarm线程的信息 */
static struct mod_thread g_drv_pos_sdh_alarm_thread[DRV_POS_MAX_SUBCARD];

/* 数组g_drv_pos_sdh_alarm用来保存STM-1接口的SDH告警 */
static DRV_POS_SDH_ALARM g_drv_pos_sdh_alarm[DRV_POS_MAX_SUBCARD][DRV_POS_MAX_PORT];


/**************************************************************************
* 函数名称: drv_pos_sdhAlmMemGet
* 功能描述: 获取保存SDH告警的内存
* 访问的表: 软件表g_drv_pos_sdh_alarm
* 修改的表: 无
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhAlmMemGet(BYTE chipId, BYTE portIdx, DRV_POS_SDH_ALARM** ppAlarm)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(ppAlarm, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] ppAlarm is NULL", chipId, portIdx);
    
    *ppAlarm = &(g_drv_pos_sdh_alarm[chipId][portIdx]);
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhAlarmClear
* 功能描述: 清空保存SDH告警的内存
* 访问的表: 软件表g_drv_pos_sdh_alarm
* 修改的表: 软件表g_drv_pos_sdh_alarm
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhAlarmClear(BYTE chipId, BYTE portIdx)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    
    memset(&(g_drv_pos_sdh_alarm[chipId][portIdx]), 0, sizeof(DRV_POS_SDH_ALARM));
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sectionAlmSave
* 功能描述: 保存STM-1帧的段告警
* 访问的表: 软件表g_drv_pos_sdh_alarm
* 修改的表: 软件表g_drv_pos_sdh_alarm
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sectionAlmSave(BYTE chipId, BYTE portIdx, const DRV_POS_SECTION_ALARM *pSectionAlm)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pSectionAlm, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pSectionAlm is NULL", chipId, portIdx);
    
    memset(&(g_drv_pos_sdh_alarm[chipId][portIdx].sectionAlm), 0, sizeof(DRV_POS_SECTION_ALARM));
    memcpy(&(g_drv_pos_sdh_alarm[chipId][portIdx].sectionAlm), pSectionAlm, sizeof(DRV_POS_SECTION_ALARM));
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_highPathAlmSave
* 功能描述: 保存STM-1帧的高阶通道的告警
* 访问的表: 软件表g_drv_pos_sdh_alarm
* 修改的表: 软件表g_drv_pos_sdh_alarm
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_highPathAlmSave(BYTE chipId, BYTE portIdx, const DRV_POS_VC4_ALARM *pVc4Alm)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pVc4Alm, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pVc4Alm is NULL");
       
    memset(&(g_drv_pos_sdh_alarm[chipId][portIdx].vc4Alm), 0, sizeof(DRV_POS_VC4_ALARM));
    memcpy(&(g_drv_pos_sdh_alarm[chipId][portIdx].vc4Alm), pVc4Alm, sizeof(DRV_POS_VC4_ALARM));
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sectionAlarmGet
* 功能描述: 获取STM-1帧的段告警
* 访问的表: 
* 修改的表: 
* 输入参数:  
* 输出参数: pSectionAlm: 保存段告警
* 返 回 值: 
* 其它说明: 包括再生段告警和复用段告警
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sectionAlarmGet(BYTE chipId, BYTE portIdx, DRV_POS_SECTION_ALARM *pSectionAlm)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    WORD32 alarmStatus = 0;  /* Section alarm status */
    AtSdhLine atSdhLine = NULL;
    AtModuleSdh atModuleSdh = NULL;
    eAtRet atRet = cAtOk;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pSectionAlm, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pSectionAlm is NULL",
         chipId, portIdx);

    /*获取sdh module ，以做信号量保护*/
    ret = drv_pos_get_atModuleSdh(chipId, &atModuleSdh);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_atModuleSdh failed, return %d", chipId, ret);

    atRet = AtModuleLock((AtModule)atModuleSdh);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtModuleLock failed, return %s",
        chipId, portIdx, AtRet2String(atRet));

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    if(DRV_POS_OK != ret)
    {
        
        atRet = AtModuleUnLock((AtModule)atModuleSdh);
        DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtModuleUnLock failed, return %s",
            chipId, portIdx, AtRet2String(atRet));
        
        DRV_POS_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed");
    }

    /* get current alarm status of section */
    alarmStatus = AtChannelAlarmGet((AtChannel)atSdhLine);


    atRet = AtModuleUnLock((AtModule)atModuleSdh);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtModuleUnLock failed, return %s",
        chipId, portIdx, AtRet2String(atRet));

    memset(pSectionAlm, 0, sizeof(DRV_POS_SECTION_ALARM)); /* 先清空内存,在赋值 */

    pSectionAlm->rsLos = (alarmStatus&cAtSdhLineAlarmLos)?1:0;  /* RS-LOS */
    pSectionAlm->rsOof = (alarmStatus&cAtSdhLineAlarmOof)?1:0;  /* RS-OOF */
    pSectionAlm->rsLof = (alarmStatus&cAtSdhLineAlarmLof)?1:0;  /* RS-LOF */
    pSectionAlm->rsTim = (alarmStatus&cAtSdhLineAlarmTim)?1:0;  /* RS-TIM */
    pSectionAlm->msAis = (alarmStatus&cAtSdhLineAlarmAis)?1:0;  /* MS-AIS */
    pSectionAlm->msRdi = (alarmStatus&cAtSdhLineAlarmRdi)?1:0;  /* MS-RDI */
    pSectionAlm->msSd = (alarmStatus&cAtSdhLineAlarmBerSd)?1:0; /* MS-SD */
    pSectionAlm->msSf = (alarmStatus&cAtSdhLineAlarmBerSf)?1:0; /* MS-SF */    
    pSectionAlm->rsSd = (alarmStatus&cAtSdhLineAlarmRsBerSd)?1:0; /* RS-SD*/
    pSectionAlm->rsSf = (alarmStatus&cAtSdhLineAlarmRsBerSf)?1:0; /* RS-SF*/
    
    return ret;
}


/**************************************************************************
* 函数名称: drv_pos_highPathAlarmGet
* 功能描述: 获取STM-1帧的高阶通道告警
* 访问的表: 
* 修改的表: 
* 输入参数:  
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_highPathAlarmGet(BYTE chipId, 
                                BYTE portIdx, 
                                DRV_POS_VC4_ALARM *pVc4Alm)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    WORD32 alarmStatus = 0;  /* VC4 alarm status */
    AtSdhVc atSdhVc4 = NULL;
    AtSdhAu atSdhAu = NULL;
    AtModuleSdh atModuleSdh = NULL;
    eAtRet atRet = cAtOk;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pVc4Alm, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pVc4Alm is NULL", chipId, portIdx);


    /*获取sdh module ，以做信号量保护*/
    ret = drv_pos_get_atModuleSdh(chipId, &atModuleSdh);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_atModuleSdh failed, return %d", chipId, ret);

    atRet = AtModuleLock((AtModule)atModuleSdh);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtModuleLock failed, return %s",
        chipId, portIdx, AtRet2String(atRet));


    ret = drv_pos_get_atSdhVC(chipId, portIdx, &atSdhVc4);
    if(DRV_POS_OK != ret)
    {
      
        atRet = AtModuleUnLock((AtModule)atModuleSdh);
        DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtModuleUnLock failed, return %s",
            chipId, portIdx, AtRet2String(atRet));
   
        
        DRV_POS_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhVC failed");
    }

    ret = drv_pos_get_atSdhAu(chipId, portIdx, &atSdhAu);
    if(DRV_POS_OK != ret)
    {
     
        atRet = AtModuleUnLock((AtModule)atModuleSdh);
        DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtModuleUnLock failed, return %s",
            chipId, portIdx, AtRet2String(atRet));
   
        
        DRV_POS_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhAu failed");
    }

    memset(pVc4Alm, 0, sizeof(DRV_POS_VC4_ALARM)); /* 先清空再赋值 */

    alarmStatus = AtChannelAlarmGet((AtChannel)atSdhAu);
    pVc4Alm->au4Lop = (alarmStatus & cAtSdhPathAlarmLop) ? 1 : 0;   /* AU4 loss of pointer */
    
    /* get current alarm status of high-path */
    alarmStatus = AtChannelAlarmGet((AtChannel)atSdhVc4);

    atRet = AtModuleUnLock((AtModule)atModuleSdh);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtModuleUnLock failed, return %s",
        chipId, portIdx, AtRet2String(atRet));

    pVc4Alm->vc4Ais = (alarmStatus & cAtSdhPathAlarmAis) ? 1 : 0;   /* AU4 alarm indication signal */
    pVc4Alm->vc4Uneq = (alarmStatus & cAtSdhPathAlarmUneq) ? 1 : 0; /* VC4 unequipped */
    pVc4Alm->vc4Plm = (alarmStatus & cAtSdhPathAlarmPlm) ? 1 : 0;   /* VC4 payload label mismatch */
    pVc4Alm->vc4Tim = (alarmStatus & cAtSdhPathAlarmTim) ? 1 : 0;   /* VC4 trace identifier mismatch */
    pVc4Alm->vc4Rdi = (alarmStatus & cAtSdhPathAlarmRdi) ? 1 : 0;   /* VC4 remote defect indication */
    pVc4Alm->vc4Sd = (alarmStatus & cAtSdhPathAlarmBerSd) ? 1 : 0;  /* VC4-SD */
    pVc4Alm->vc4Sf = (alarmStatus & cAtSdhPathAlarmBerSf) ? 1 : 0;  /* VC4-SF */
    
    return ret;
}


/**************************************************************************
* 函数名称: drv_pos_sdhAlmFlagGet
* 功能描述: 获取监测SDH告警的标记.
* 访问的表: 软件表g_drv_pos_sdh_alarm_enable.
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.           
* 输出参数: pFlag: 保存告警使能标记,DRV_POS_ENABLE使能,DRV_POS_DISABLE非使能.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhAlmFlagGet(BYTE chipId, WORD32 *pFlag)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_POINTER_CHECK(pFlag, DRV_POS_PT_ERR, "chip[%d] pFlag is NULL", chipId);
    
    *pFlag = 0;
    *pFlag = g_drv_pos_sdh_alarm_enable[chipId];
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhAlmFlagSet
* 功能描述: 设置监测SDH告警的标记.
* 访问的表: 软件表g_drv_pos_sdh_alarm_enable.
* 修改的表: 软件表g_drv_pos_sdh_alarm_enable.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           flag: 告警使能标记,DRV_POS_ENABLE使能,DRV_POS_DISABLE非使能.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhAlmFlagSet(BYTE chipId, WORD32 flag)
{
    DRV_POS_CHIPID_CHECK(chipId);
    
    g_drv_pos_sdh_alarm_enable[chipId] = flag;
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhSecAlmFlagGet
* 功能描述: 获取监测SDH段告警的标记.
* 访问的表: 软件表g_drv_pos_sdhSecAlmEnable.
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.           
* 输出参数: *pFlag: 保存告警使能标记,DRV_POS_ENABLE使能,DRV_POS_DISABLE非使能.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhSecAlmFlagGet(BYTE chipId, BYTE portIdx, WORD32 *pFlag)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pFlag, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pFlag is NULL", chipId, portIdx);

    *pFlag = 0;
    *pFlag = g_drv_pos_sdhSecAlmEnable[chipId][portIdx];
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhSecAlmFlagSet
* 功能描述: 设置监测SDH段告警的标记.
* 访问的表: 软件表g_drv_pos_sdhSecAlmEnable.
* 修改的表: 软件表g_drv_pos_sdhSecAlmEnable.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           flag: 告警使能标记,DRV_POS_ENABLE使能,DRV_POS_DISABLE非使能.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhSecAlmFlagSet(BYTE chipId, BYTE portIdx, WORD32 flag)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    
    g_drv_pos_sdhSecAlmEnable[chipId][portIdx] = flag;
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhVc4AlmFlagGet
* 功能描述: 获取监测SDH的VC4告警的标记.
* 访问的表: 软件表g_drv_pos_vc4AlmEnable.
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.           
* 输出参数: *pFlag: 保存告警使能标记,DRV_POS_ENABLE使能,DRV_POS_DISABLE非使能.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhVc4AlmFlagGet(BYTE chipId, BYTE portIdx, WORD32 *pFlag)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pFlag, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pFlag is NULL ", chipId, portIdx);
    
    *pFlag = 0;
    *pFlag = g_drv_pos_vc4AlmEnable[chipId][portIdx];
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhVc4AlmFlagSet
* 功能描述: 设置监测SDH的VC4告警的标记.
* 访问的表: 软件表g_drv_pos_vc4AlmEnable.
* 修改的表: 软件表g_drv_pos_vc4AlmEnable.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           flag: 告警使能标记,DRV_POS_ENABLE使能,DRV_POS_DISABLE非使能.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhVc4AlmFlagSet(BYTE chipId, BYTE portIdx, WORD32 flag)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    g_drv_pos_vc4AlmEnable[chipId][portIdx] = flag;
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhAlarmMonitor
* 功能描述: 监测STM-1单板的SDH告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pDwSubslotId: STM-1单板的子槽位号 
* 输出参数: 无
* 返 回 值: 
* 其它说明: SDH告警线程的入口函数
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhAlarmMonitor(const WORD32 *pDwSubslotId)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    BYTE subslotId = 0;
    BYTE chipId = 0;         /* ARRIVE芯片的芯片编号 */
    BYTE portIdx = 0;         /* STM-1单板的155M端口的编号 */
    DRV_POS_SECTION_ALARM sectionAlm;
    DRV_POS_VC4_ALARM vc4Alarm;
    BYTE portNum = 0;
    BYTE portMap[DRV_POS_MAX_PORT] = {0};
    
    WORD32 rv = BSP_E_CP3BAN_OK;
    WORD32 dwBoardStatus = 0;  /* CP3BAN单板的状态 */

    DRV_POS_POINTER_CHECK(pDwSubslotId, DRV_POS_PT_ERR, "sdhAlarmMonitor input pDwSubslotId is NULL");

    subslotId = (BYTE)(*pDwSubslotId);
    chipId = subslotId - (BYTE)1;
    DRV_POS_CHIPID_CHECK(chipId);
    
    if(NULL == pDwSubslotId)
    {
        free((void*)pDwSubslotId);
        pDwSubslotId = NULL;
    }
    
    g_drv_pos_sdh_alarm_thread_id[chipId] = __gettid();

    while (DRV_POS_TRUE)
    {
        rv = BSP_cp3banBoardStatusGet((WORD32)subslotId, &dwBoardStatus); /* 获取单板的状态 */
        if (BSP_E_CP3BAN_OK != rv)
        {
            g_drv_pos_sdh_alarm_thread_id[chipId] = 0;
            DRV_POS_ERROR_RET(DRV_POS_ERROR, "chip[%d] BSP_cp3banBoardStatusGet failed, return %d", chipId, ret);
        }
        if (BSP_CP3BAN_BOARD_OFFLINE == dwBoardStatus)  /* 单板不在位,直接返回成功 */
        {
            g_drv_pos_sdh_alarm_thread_id[chipId] = 0;
            DRV_POS_ERROR_RET(DRV_POS_ERROR, "chip[%d] board is offline, exit the overhead monitor", chipId, ret);
        }

        if (DRV_POS_DISABLE != g_drv_pos_sdh_alarm_enable[chipId])
        {
            /*获取最大端口号*/
            ret = drv_pos_get_valid_port_map(chipId, portMap, &portNum);
            DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_valid_port_map failed return %d", chipId, ret);
    
            for (portIdx = 0; portIdx < portNum; portIdx++)
            {
                 
                if (DRV_POS_DISABLE != g_drv_pos_sdhSecAlmEnable[chipId][portMap[portIdx]])
                {
                    /* Query section alarm */
                    memset(&sectionAlm, 0, sizeof(sectionAlm));

                    ret = drv_pos_sectionAlarmGet(chipId, portMap[portIdx], &sectionAlm);
                    if (DRV_POS_OK != ret)
                    {
                        continue;
                    }
                    
                    ret = drv_pos_sectionAlmSave(chipId, portMap[portIdx], &sectionAlm);
                    if (DRV_POS_OK != ret)
                    {
                        continue;
                    }
                }

                if (DRV_POS_DISABLE != g_drv_pos_vc4AlmEnable[chipId][portMap[portIdx]])
                {
                    /* Query high path alarm */
                    memset(&vc4Alarm, 0, sizeof(DRV_POS_VC4_ALARM));
                    ret = drv_pos_highPathAlarmGet(chipId, portMap[portIdx], &vc4Alarm);
                    if (DRV_POS_OK != ret)
                    {
                        continue;
                    }
                    ret = drv_pos_highPathAlmSave(chipId, portMap[portIdx], &vc4Alarm);
                    if (DRV_POS_OK != ret)
                    {
                        continue;
                    }
                }
                BSP_DelayMs(100);  /* delay 100ms. */
            }
        }
        BSP_DelayMs(100);  /* delay 100ms. */
    }

    g_drv_pos_sdh_alarm_thread_id[chipId] = 0;

    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sectionAlarmQuery
* 功能描述: 查询STM-1帧的段告警
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: ARRIVE芯片的 编号,取值为0~3. 
*           portId: STM-1端口的编号,取值为1~8.
* 输出参数: *pAlmResult: 保存段告警
* 返 回 值: 
* 其它说明: 本函数供产品管理调用.包括再生段告警和复用段告警
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sectionAlarmQuery(BYTE chipId, BYTE portIdx, WORD32 *pAlmResult)
{
    WORD32 ret = DRV_POS_OK;  /* 函数返回码 */
    WORD32 tmpResult = 0;
    DRV_POS_SDH_ALARM *pAlarm = NULL;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pAlmResult, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pAlmResult is NULL");
    
    ret = drv_pos_sdhAlmMemGet(chipId, portIdx, &pAlarm);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_sdhAlmMemGet failed ", chipId, portIdx);
        
    *pAlmResult = 0;   /* 赋值之前先清零 */
    tmpResult = tmpResult | (BSP_SDH_ALM_SECT_STM & (((WORD32)(pAlarm->sectionAlm.rsTim)) << 0));
    tmpResult = tmpResult | (BSP_SDH_ALM_SECT_SD & (((WORD32)(pAlarm->sectionAlm.msSd)) << 3));
    tmpResult = tmpResult | (BSP_SDH_ALM_SECT_AIS & (((WORD32)(pAlarm->sectionAlm.msAis)) << 4));
    tmpResult = tmpResult | (BSP_SDH_ALM_SECT_RDI & (((WORD32)(pAlarm->sectionAlm.msRdi)) << 5));
    tmpResult = tmpResult | (BSP_SDH_ALM_SECT_LOF & (((WORD32)(pAlarm->sectionAlm.rsLof)) << 6));
    tmpResult = tmpResult | (BSP_SDH_ALM_SECT_LOS & (((WORD32)(pAlarm->sectionAlm.rsLos)) << 8));
    tmpResult = tmpResult | (BSP_SDH_ALM_SECT_SF & (((WORD32)(pAlarm->sectionAlm.msSf)) << 9));
    tmpResult = tmpResult | (BSP_SDH_ALM_SECT_RSSD & (((WORD32)(pAlarm->sectionAlm.rsSd)) << 10));
    tmpResult = tmpResult | (BSP_SDH_ALM_SECT_RSSF & (((WORD32)(pAlarm->sectionAlm.rsSf)) << 11));
    tmpResult = tmpResult | (BSP_SDH_ALM_SECT_OOF & (((WORD32)(pAlarm->sectionAlm.rsOof)) << 12));
    *pAlmResult = tmpResult;
    
    return ret;
}


/**************************************************************************
* 函数名称: drv_pos_highPathAlarmQuery
* 功能描述: 查询STM-1帧的高阶通道的告警
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: ARRIVE芯片的编号,取值为0~3. 
*           portId: STM-1端口的编号,取值为1~8.
* 输出参数: *pAlmResult: 保存高阶通道告警
* 返 回 值: 
* 其它说明: 本函数供产品管理调用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_highPathAlarmQuery(BYTE chipId, BYTE portIdx, WORD32 *pAlmResult)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    WORD32 tmpResult = 0;
    DRV_POS_SDH_ALARM *pAlarm = NULL;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pAlmResult, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pAlmResult is NULL", chipId, portIdx);
    
    ret = drv_pos_sdhAlmMemGet(chipId, portIdx, &pAlarm);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_sdhAlmMemGet failed", chipId, portIdx);

    *pAlmResult = 0;   /* 赋值之前先清零 */
    tmpResult = tmpResult | (BSP_SDH_ALM_HP_PTM & (((WORD32)(pAlarm->vc4Alm.vc4Tim)) << 1));
    tmpResult = tmpResult | (BSP_SDH_ALM_HP_PSLM & (((WORD32)(pAlarm->vc4Alm.vc4Plm)) << 3));
    tmpResult = tmpResult | (BSP_SDH_ALM_HP_UNEQP & (((WORD32)(pAlarm->vc4Alm.vc4Uneq)) << 4));
    tmpResult = tmpResult | (BSP_SDH_ALM_HP_LOP & (((WORD32)(pAlarm->vc4Alm.au4Lop)) << 5));
    tmpResult = tmpResult | (BSP_SDH_ALM_HP_PAIS & (((WORD32)(pAlarm->vc4Alm.vc4Ais)) << 6));
    tmpResult = tmpResult | (BSP_SDH_ALM_HP_PRDI & (((WORD32)(pAlarm->vc4Alm.vc4Rdi)) << 7));
    *pAlmResult = tmpResult;
    
    return ret;
}

/**************************************************************************
* 函数名称: drv_pos_sdhAlmThreadCreate
* 功能描述: 创建SDH告警的线程.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM-1单板的子槽位号 
* 输出参数: 无
* 返 回 值: 
* 其它说明: 该线程用来监测STM-1单板的SDH告警.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhAlmThreadCreate(BYTE subslotId)
{
    DRV_POS_RET rv = DRV_POS_OK;
    WORD32 *pDwSubslotId = NULL;
    BYTE chipId = 0;

    chipId = subslotId - (BYTE)1;
    DRV_POS_CHIPID_CHECK(chipId);
    
    pDwSubslotId = malloc(sizeof(WORD32));
    DRV_POS_POINTER_CHECK(pDwSubslotId, DRV_POS_PT_ERR, "chip[%d] malloc failed", chipId);

    memset(pDwSubslotId, 0, sizeof(WORD32));
    *pDwSubslotId = (WORD32)subslotId;
    
    init_thread(&g_drv_pos_sdh_alarm_thread[chipId], 0, 0, 80, (void *)drv_pos_sdhAlarmMonitor, ((void *)pDwSubslotId));
    rv = start_mod_thread(&g_drv_pos_sdh_alarm_thread[chipId]);
    if (BSP_OK != rv)
    {
        if(NULL == pDwSubslotId)
        {
            free((void*)pDwSubslotId);
            pDwSubslotId = NULL;
        }
        DRV_POS_ERROR_RET(rv, "chip[%d] sdh alarm thread start_mod_thread failed, rv = %d", chipId, rv);
    }
    DRV_POS_SUCCESS_PRINT("chip[%d] drv_pos_sdhAlmThreadCreate", chipId);
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_sdhAlmThreadDelete
* 功能描述: 删除SDH告警的线程.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM-1单板的子槽位号 
* 输出参数: 无
* 返 回 值: 
* 其它说明: 该线程用来监测STM-1单板的SDH告警.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhAlmThreadDelete(BYTE subslotId)
{
    BYTE chipId = 0;
    
    chipId = subslotId - (BYTE)1;
    DRV_POS_CHIPID_CHECK(chipId);

    if(0 != g_drv_pos_sdh_alarm_thread_id)
    {
        stop_mod_thread(&g_drv_pos_sdh_alarm_thread[chipId]);
        memset(&g_drv_pos_sdh_alarm_thread[chipId], 0, sizeof(g_drv_pos_sdh_alarm_thread[chipId]));
        g_drv_pos_sdh_alarm_thread_id[chipId] = 0;
    }
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhAlmThreadIdGet
* 功能描述: 获取SDH告警线程的thread ID.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3. 
* 输出参数: *pThreadId: 用来保存thread ID.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhAlmThreadIdGet(BYTE chipId, int *pThreadId)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_POINTER_CHECK(pThreadId, DRV_POS_PT_ERR, "chip[%d] pThreadId is NULL");
    
    *pThreadId = g_drv_pos_sdh_alarm_thread_id[chipId];
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_rxRsLosForce
* 功能描述: 在RX方向强制插入RS-LOS告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_rxRsLosForce(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    eAtRet atRet = cAtOk;
    AtSdhLine atSdhLine = NULL;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d",
        chipId, portIdx, ret);
    
    /* Force RX RS-LOS alarm */
    atRet = AtChannelRxAlarmForce((AtChannel)atSdhLine, cAtSdhLineAlarmLos);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] AtChannelRxAlarmForce failed, return %s",
        chipId, portIdx, AtRet2String(atRet));
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_rxRsLosUnForce
* 功能描述: 在RX方向取消强制插入RS-LOS告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_rxRsLosUnForce(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    eAtRet atRet = cAtOk;
    AtSdhLine atSdhLine = NULL;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d",
        chipId, portIdx, ret);
    
    /* Force RX RS-LOS alarm */
    atRet = AtChannelRxAlarmUnForce((AtChannel)atSdhLine, cAtSdhLineAlarmLos);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] AtChannelRxAlarmUnForce failed, return %s", 
        chipId, portIdx, AtRet2String(atRet));
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_msAisInsert
* 功能描述: 强制插入MS-AIS告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: direction: 0表示TX方向,1表示RX方向.
* 输出参数: 
* 返 回 值: 
* 其它说明: 包括在TX方向或者RX方向强制插入MS-AIS.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_msAisForce(BYTE chipId, 
                                  BYTE portIdx, 
                                  WORD32 direction)
{
    DRV_POS_RET ret = DRV_POS_OK;
    eAtRet atRet = cAtOk;
    AtSdhLine atSdhLine = NULL;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    
    switch (direction)
    {
        case DRV_POS_TX_DIRECTION:  /* TX direction*/
            /* force MS-AIS to TX direction */
            atRet = AtChannelTxAlarmForce((AtChannel)atSdhLine, cAtSdhLineAlarmAis);
            DRV_POS_IF_AT_ERROR_RET(atRet, 
                "chip[%d] portIdx[%d] tx ais AtChannelTxAlarmForce atRet = %s", 
                chipId, portIdx, AtRet2String(atRet));
            break;
        case DRV_POS_RX_DIRECTION:  /* RX direction*/
            /* force MS-AIS to RX direction */
            atRet = AtChannelRxAlarmForce((AtChannel)atSdhLine, cAtSdhLineAlarmAis);
            DRV_POS_IF_AT_ERROR_RET(atRet, 
                "chip[%d] portIdx[%d] tx ais AtChannelTxAlarmForce atRet = %s",
                chipId, portIdx, AtRet2String(atRet)); 
            break;
        default:
            DRV_POS_ERROR_RET(DRV_POS_ERROR, "chip[%d] portIdx[%d] input direction is wrong", chipId, portIdx);
    }
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_msAisUnForce
* 功能描述: 取消强制插入MS-AIS告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: direction: 0表示TX方向,1表示RX方向.
* 输出参数: 
* 返 回 值: 
* 其它说明: 包括在TX方向或者RX方向取消强制插入MS-AIS.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_msAisUnForce(BYTE chipId, 
                                      BYTE portIdx, 
                                      WORD32 direction)
{
    DRV_POS_RET ret = DRV_POS_OK;
    eAtRet atRet = cAtOk;
    AtSdhLine atSdhLine = NULL;


    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine return %d",
        chipId, portIdx, ret);
    
    switch (direction)
    {
        case DRV_POS_TX_DIRECTION:  /* TX direction*/
            /* force MS-AIS to TX direction */
            atRet = AtChannelTxAlarmUnForce((AtChannel)atSdhLine, cAtSdhLineAlarmAis);
            DRV_POS_IF_AT_ERROR_RET(atRet, 
                "chip[%d] portIdx[%d] tx ais AtChannelTxAlarmForce failed, return %s", 
                chipId, portIdx, AtRet2String(atRet));
            break;
        case DRV_POS_RX_DIRECTION:  /* RX direction*/
            /* force MS-AIS to RX direction */
            atRet = AtChannelRxAlarmUnForce((AtChannel)atSdhLine, cAtSdhLineAlarmAis);
            DRV_POS_IF_AT_ERROR_RET(atRet, 
                "chip[%d] portIdx[%d] tx ais AtChannelTxAlarmForce failed, return %s",
                chipId, portIdx, AtRet2String(atRet));
            break;
        default:
            DRV_POS_ERROR_RET(DRV_POS_ERROR, "chip[%d] portIdx[%d] input direction is wrong", chipId, portIdx);
    }
    
    return DRV_POS_OK;
}



/**************************************************************************
* 函数名称: drv_pos_au4AisForce
* 功能描述: 强制插入AU4-AIS告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: direction: 0表示TX方向,1表示RX方向.
* 输出参数: 
* 返 回 值: 
* 其它说明: 包括在TX方向或者RX方向强制插入AU4-AIS.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_au4AisForce(BYTE chipId, 
                                   BYTE portIdx, 
                                   BYTE au4Id, 
                                   WORD32 direction)
{
    DRV_POS_RET ret = DRV_POS_OK;
    eAtRet atRet = cAtOk;
    AtSdhVc atSdhVc4 = NULL;
    BYTE aug1Index = 0;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_AU4_CHECK(au4Id);

    aug1Index = au4Id-(BYTE)1;

    ret = drv_pos_get_atSdhVC(chipId, portIdx, &atSdhVc4);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhVC return %d",
        chipId, portIdx, ret);

    switch (direction)
    {
        case DRV_POS_TX_DIRECTION:  /* TX direction*/
            /* force AU4-AIS to TX direction */
            atRet = AtChannelTxAlarmForce((AtChannel)atSdhVc4, cAtSdhPathAlarmAis);
            DRV_POS_IF_AT_ERROR_RET(atRet, 
                "chip[%d] portIdx[%d] vc4 tx ais AtChannelTxAlarmForce failed, return %s", 
                chipId, portIdx, AtRet2String(atRet));
            break;
        case DRV_POS_RX_DIRECTION:  /* RX direction*/
            /* force HP-AIS to RX direction */
            atRet = AtChannelRxAlarmForce((AtChannel)atSdhVc4, cAtSdhPathAlarmAis);
            DRV_POS_IF_AT_ERROR_RET(atRet, 
                "chip[%d] portIdx[%d] vc4 tx ais AtChannelRxAlarmForce failed, return %s", 
                chipId, portIdx, AtRet2String(atRet));
            break;
        default:
            DRV_POS_ERROR_RET(DRV_POS_ERROR, "chip[%d] portIdx[%d] input direction is wrong", chipId, portIdx);
    }
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_au4AisUnForce
* 功能描述: 取消强制插入AU4-AIS告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: direction: 0表示TX方向,1表示RX方向.
* 输出参数: 
* 返 回 值: 
* 其它说明: 包括在TX方向或者RX方向取消强制插入AU4-AIS.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_au4AisUnForce(BYTE chipId, 
                                       BYTE portIdx, 
                                       BYTE au4Id, 
                                       WORD32 direction)
{
    DRV_POS_RET ret = DRV_POS_OK;
    eAtRet atRet = cAtOk;
    AtSdhVc atSdhVc4 = NULL;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_get_atSdhVC(chipId, portIdx, &atSdhVc4);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhVC return %d",
        chipId, portIdx, ret);
    
    switch (direction)
    {
        case DRV_POS_TX_DIRECTION:  /* TX direction*/
            /* force AU4-AIS to TX direction */
            atRet = AtChannelTxAlarmUnForce((AtChannel)atSdhVc4, cAtSdhPathAlarmAis);
            DRV_POS_IF_AT_ERROR_RET(atRet, 
                "chip[%d] portIdx[%d] vc4 tx ais AtChannelTxAlarmUnForce failed, return %s", 
                chipId, portIdx, AtRet2String(atRet));
            break;
        case DRV_POS_RX_DIRECTION:  /* RX direction*/
            /* force HP-AIS to RX direction */
            atRet = AtChannelTxAlarmUnForce((AtChannel)atSdhVc4, cAtSdhPathAlarmAis);
            DRV_POS_IF_AT_ERROR_RET(atRet, 
                "chip[%d] portIdx[%d] vc4 tx ais AtChannelTxAlarmUnForce failed, return %s", 
                chipId, portIdx, AtRet2String(atRet));
          break;
        default:
            DRV_POS_ERROR_RET(DRV_POS_ERROR, "chip[%d] portIdx[%d] input direction is wrong", chipId, portIdx);
    }
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_alarmIntrStateGet
* 功能描述: 
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-12-11   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_alarmIntrClear(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtSdhLine atSdhLine = NULL;
    WORD32 intrStatus = 0;  /* 历史告警的中断状态 */

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d", chipId, portIdx);
    
    /* FPGA will save the current intrStatus and clear interrupt alarm */
    intrStatus = AtChannelAlarmInterruptClear((AtChannel)atSdhLine);

    DRV_POS_PRINT("chip[%d] portIdx[%d] sectionAlarmHistoryIntrStatus = 0x%x", chipId, portIdx, intrStatus);

    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_alarmIntrStateGet
* 功能描述: 
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-12-11   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_alarmIntrStateGet(BYTE chipId, BYTE portIdx, WORD32* pEnableMask)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtSdhLine atSdhLine = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_atSdhLine failed, return %d", chipId, ret);

    *pEnableMask = AtChannelAlarmInterruptGet((AtChannel)atSdhLine);
    
    return ret;
}

/**************************************************************************
* 函数名称: drv_pos_alarmInterruptMaskGet
* 功能描述: 
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-12-11   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_alarmIntrDefectMaskGet(BYTE chipId, BYTE portIdx, WORD32* pDefectMask)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtSdhLine atSdhLine = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_atSdhLine failed, return %d", chipId, ret);

    *pDefectMask = AtChannelInterruptMaskGet((AtChannel)atSdhLine);
    
    return ret;
}

/**************************************************************************
* 函数名称: drv_pos_alarmInterruptMaskSet
* 功能描述: 
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-12-11   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_alarmIntrMaskSet(BYTE chipId, BYTE portIdx, WORD32 defectMask, WORD32 enableMask)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtSdhLine atSdhLine = NULL;
    eAtRet atRet = cAtOk;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d", chipId, portIdx, ret);

    /*set 之前清空一下*/
    ret = drv_pos_alarmIntrClear(chipId, portIdx);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_alarmIntrClear failed, return %d", chipId, portIdx, ret);

    atRet = AtChannelInterruptMaskSet((AtChannel)atSdhLine, defectMask, enableMask);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] AtChannelInterruptMaskSet failed, return %s, atSdhLine = %p, atDefectMask = %d, atEnableMask = %d",
        chipId, atSdhLine, defectMask, enableMask);
    
    return ret;
}

/**************************************************************************
* 函数名称: drv_pos_alarmIntrAllDisable
* 功能描述: 
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-12-11   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_alarmIntrAllDisable(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    WORD32 defectMask = 0;
    WORD32 enableMask = 0;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    defectMask = DRV_POS_SDH_ALARM_INTR_ALL_MASK;
    enableMask = DRV_POS_SDH_ALARM_INTR_ALL_DISABLE;

    ret = drv_pos_alarmIntrMaskSet(chipId, portIdx, defectMask, enableMask);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_alarmIntrMaskSet failed, return %d, defectMask = 0x%x, enableMask = 0x%x",
        chipId, portIdx, ret, defectMask, enableMask);

    return ret;
}

/**************************************************************************
* 函数名称: drv_pos_alarmIntrAllEnable
* 功能描述: 
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-12-11   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_alarmIntrAllEnable(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    WORD32 defectMask = 0;
    WORD32 enableMask = 0;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    /*enable之前需要把历史的中断关闭*/
    ret = drv_pos_alarmIntrClear(chipId, portIdx);
    DRV_POS_IF_ERROR_RET(ret, 
        "chip[%d] portIdx[%d] drv_pos_alarmIntrClear failed, return %d",
        chipId, portIdx, ret);

    defectMask = DRV_POS_SDH_ALARM_INTR_ALL_MASK;
    enableMask = DRV_POS_SDH_ALARM_INTR_ALL_ENABLE;

    ret = drv_pos_alarmIntrMaskSet(chipId, portIdx, defectMask, enableMask);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_alarmIntrMaskSet failed, return %d, defectMask = 0x%x, enableMask = 0x%x",
        chipId, portIdx, ret, defectMask, enableMask);

    return ret;
}


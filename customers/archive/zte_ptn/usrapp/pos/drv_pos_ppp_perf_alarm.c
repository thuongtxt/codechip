/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_pos_ppp_perf.c
* 文件标识: 
* 其它说明: 驱动POS PPP模块的性能统计实现函数
* 当前版本: V1.0
* 作    者: 刘钰00130907.
* 完成日期: 2013-09-03
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/


#include "drv_pos_ppp_perf_alarm.h"
#include "AtHdlcChannel.h"

#include "bsp_cp3ban_common.h"


/* 定义全局变量 */
/* 监测PPP告警和性能的使能标记,默认非使能.在初始化单板时,使能标记;在卸载单板时,非使能标记. */
#if 0
static WORD32 g_drv_pos_ppp_alarm_enable[DRV_POS_MAX_SUBCARD][DRV_POS_MAX_PORT]; 
#endif
static WORD32 g_drv_pos_ppp_perf_enable[DRV_POS_MAX_SUBCARD][DRV_POS_MAX_PORT];

/* ppp alarm和perf线程的threadId */
#if 0
static int g_drv_pos_ppp_alarm_thread_id[DRV_POS_MAX_SUBCARD]; 
#endif
static int g_drv_pos_ppp_perf_thread_id[DRV_POS_MAX_SUBCARD];

#if 0
/* g_drv_pos_ppp_alarm_thread用来保存PPP alarm线程的信息 */
static struct mod_thread g_drv_pos_ppp_alarm_thread[DRV_POS_MAX_SUBCARD];
#endif
/* g_drv_pos_ppp_perf_thread用来保存PPP perf线程的信息 */
static struct mod_thread g_drv_pos_ppp_perf_thread[DRV_POS_MAX_SUBCARD];
#if 0
/* 数组g_drv_pos_ppp_alarm用来保存PPP接口的告警 */
static DRV_POS_PPP_ALARM g_drv_pos_ppp_alarm[DRV_POS_MAX_SUBCARD][DRV_POS_MAX_PORT];
#endif
/* 数组g_drv_pos_ppp_perf用来保存PPP接口的性能统计 */
static DRV_POS_PPP_PERF g_drv_pos_ppp_perf[DRV_POS_MAX_SUBCARD][DRV_POS_MAX_PORT];

#if 0
/**************************************************************************
* 函数名称: drv_pos_pppAlmMemGet
* 功能描述: 获取保存ppp告警的内存
* 访问的表: 软件表g_drv_pos_ppp_alarm
* 修改的表: 无
* 输入参数: chipId: 0~3 portIdx 0~7
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_pppAlmMemGet(BYTE chipId, BYTE portIdx, DRV_POS_PPP_ALARM** ppAlarm)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(ppAlarm, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] ppAlarm is NULL", chipId, portIdx);
    
    *ppAlarm = &(g_drv_pos_ppp_alarm[chipId][portIdx]);
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_pppAlarmClear
* 功能描述: 清空保存ppp告警的内存
* 访问的表: 软件表g_drv_pos_ppp_alarm
* 修改的表: 软件表g_drv_pos_ppp_alarm
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_pppAlarmClear(BYTE chipId, BYTE portIdx)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    
    memset(&(g_drv_pos_ppp_alarm[chipId][portIdx]), 0, sizeof(DRV_POS_PPP_ALARM));
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_pppAlarmSave
* 功能描述: 保存ppp告警
* 访问的表: 软件表g_drv_pos_ppp_alarm
* 修改的表: 软件表g_drv_pos_ppp_alarm
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_pppAlarmSave(BYTE chipId, BYTE portIdx, const DRV_POS_PPP_ALARM *pPppAlm)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pPppAlm, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pPppAlm is NULL", chipId, portIdx);
    
	memset(&(g_drv_pos_ppp_alarm[chipId][portIdx]), 0, sizeof(DRV_POS_PPP_ALARM));
	memcpy(&(g_drv_pos_ppp_alarm[chipId][portIdx]), pPppAlm, sizeof(DRV_POS_PPP_ALARM));
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_sectionAlarmGet
* 功能描述: 获取STM-1帧的段告警
* 访问的表: 
* 修改的表: 
* 输入参数:  
* 输出参数: pSectionAlm: 保存段告警
* 返 回 值: 
* 其它说明: 包括再生段告警和复用段告警
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_pppAlarmGet(BYTE chipId, BYTE portIdx, DRV_POS_PPP_ALARM *pPppAlm)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    BYTE lcpState = 0;
    BYTE ipcpState = 0;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pPppAlm, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pPppAlm is NULL",
         chipId, portIdx);

    /* get current alarm status of section */
    #if 0
    /* 从平台获取PPP协议状态*/
    #endif

    memset(pPppAlm, 0, sizeof(DRV_POS_PPP_ALARM)); /* 先清空内存,在赋值 */

    pPppAlm->lcpState = lcpState;
    pPppAlm->ipcpState = ipcpState;

    return ret;
}


/**************************************************************************
* 函数名称: drv_pos_pppAlmFlagGet
* 功能描述: 获取监测POS PPP告警的标记.
* 访问的表: 软件表g_drv_pos_ppp_alarm_enable.
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.           
* 输出参数: pFlag: 保存告警使能标记,DRV_POS_ENABLE使能,DRV_POS_DISABLE非使能.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_pppAlmFlagGet(BYTE chipId, WORD32 *pFlag)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_POINTER_CHECK(pFlag, DRV_POS_PT_ERR, "chip[%d] pFlag is NULL", chipId);
    
    *pFlag = 0;
    *pFlag = g_drv_pos_ppp_alarm_enable[chipId];
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhAlmFlagSet
* 功能描述: 设置监测SDH告警的标记.
* 访问的表: 软件表g_drv_pos_sdh_alarm_enable.
* 修改的表: 软件表g_drv_pos_sdh_alarm_enable.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           flag: 告警使能标记,DRV_POS_ENABLE使能,DRV_POS_DISABLE非使能.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_pppAlmFlagSet(BYTE chipId, BYTE portIdx, WORD32 flag)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    
    g_drv_pos_ppp_alarm_enable[chipId][portIdx] = flag;
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_sdhAlarmMonitor
* 功能描述: 监测STM-1单板的SDH告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pDwSubslotId: STM-1单板的子槽位号 
* 输出参数: 无
* 返 回 值: 
* 其它说明: SDH告警线程的入口函数
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_pppAlarmMonitor(const WORD32 *pDwSubslotId)
{
    DRV_POS_RET rv = DRV_POS_OK;  /* 函数返回码 */
    BYTE subslotId = 0;
    BYTE chipId = 0;         /* ARRIVE芯片的芯片编号 */
    BYTE portIdx = 0;         /* STM-1单板的155M端口的编号 */

    DRV_POS_PPP_ALARM pppAlm;

    DRV_POS_POINTER_CHECK(pDwSubslotId, DRV_POS_PT_ERR, "sdhAlarmMonitor input pDwSubslotId is NULL");

    subslotId = (BYTE)(*pDwSubslotId);
    chipId = subslotId - (BYTE)1;
    DRV_POS_CHIPID_CHECK(chipId);
    
    g_drv_pos_ppp_alarm_thread_id[chipId] = __gettid();
    
    while (DRV_POS_TRUE)
    {
        for (portIdx = 0; portIdx < DRV_POS_MAX_PORT; portIdx++)
        {

            if(DRV_POS_DISABLE != g_drv_pos_ppp_alarm_enable[chipId][portIdx])
            {
                memset(&pppAlm, 0, sizeof(DRV_POS_PPP_ALARM));
                rv = drv_pos_pppAlarmGet(chipId, portIdx, &pppAlm);
                if(DRV_POS_OK != rv)
                {
                    continue;
                }
                rv = drv_pos_pppAlarmSave(chipId, portIdx, &pppAlm);
                if(DRV_POS_OK != rv)
                {
                    continue;
                }            
                BSP_DelayMs(125);  /* delay 125ms. */
            }
        }
        BSP_DelayMs(1000);  /* delay 1000ms. */
    }

    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_pppAlarmQuery
* 功能描述: 查询PPP告警‘
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: ARRIVE芯片的 编号,取值为0~3. 
*           portId: STM-1端口的编号,取值为0~7.
* 输出参数: *pAlmResult: 保存段告警
* 返 回 值: 
* 其它说明: 本函数供产品管理调用.包括posPPP协议告警
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_pppAlarmQuery(BYTE chipId, BYTE portIdx, DRV_POS_PPP_ALARM *pAlmResult)
{
    WORD32 ret = DRV_POS_OK;  /* 函数返回码 */
    WORD32 tmpResult = 0;
    DRV_POS_PPP_ALARM *pAlarm = NULL;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pAlmResult, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pAlmResult is NULL");

    ret = drv_pos_pppAlmMemGet(chipId, portIdx, &pAlarm);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_pppAlmMemGet failed ", chipId, portIdx);

    memset(pAlmResult, 0, sizeof(DRV_POS_PPP_ALARM));
    memcpy(pAlmResult, pAlarm, sizeof(DRV_POS_PPP_ALARM));
    
    return ret;
}

/**************************************************************************
* 函数名称: drv_pos_pppAlmThreadCreate
* 功能描述: 创建ppp告警的线程.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM-1单板的子槽位号 
* 输出参数: 无
* 返 回 值: 
* 其它说明: 该线程用来监测pos单板的ppp协议告警.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_pppAlmThreadCreate(BYTE subslotId)
{
    DRV_POS_RET rv = DRV_POS_OK;
    WORD32 dwSubslotId = 0;
    BYTE chipId = 0;

    dwSubslotId = (WORD32)subslotId;
    chipId = subslotId - (BYTE)1;
    DRV_POS_CHIPID_CHECK(chipId);
    
    init_thread(&g_drv_pos_ppp_alarm_thread[chipId], 0, 0, 80, (void *)drv_pos_pppAlarmMonitor, ((void *)(&dwSubslotId)));
    rv = start_mod_thread(&g_drv_pos_ppp_alarm_thread[chipId]);
    if (BSP_OK != rv)
    {
        DRV_POS_ERROR_RET(rv, "chip[%d] ppp alarm thread start_mod_thread failed, rv = %d", chipId, rv);
    }
	
    DRV_POS_SUCCESS_PRINT("chip[%d] drv_pos_pppAlmThreadCreate", chipId);
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_pppAlmThreadDelete
* 功能描述: 删除posppp告警的线程.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM-1单板的子槽位号 
* 输出参数: 无
* 返 回 值: 
* 其它说明: 该线程用来监测pos单板的ppp告警.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_pppAlmThreadDelete(BYTE subslotId)
{
    BYTE chipId = 0;
    
    chipId = subslotId - (BYTE)1;
    DRV_POS_CHIPID_CHECK(chipId);

    stop_mod_thread(&g_drv_pos_ppp_alarm_thread[chipId]);
    g_drv_pos_ppp_alarm_thread_id[chipId] = 0;
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_pppAlmThreadIdGet
* 功能描述: 获取pos ppp 告警线程的thread ID.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3. 
* 输出参数: *pThreadId: 用来保存thread ID.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_pppAlmThreadIdGet(BYTE chipId, int *pThreadId)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_POINTER_CHECK(pThreadId, DRV_POS_PT_ERR, "chip[%d] pThreadId is NULL", chipId);
    
    *pThreadId = g_drv_pos_ppp_alarm_thread_id[chipId];
    
    return DRV_POS_OK;
}
#endif

/**************************************************************************
* 函数名称: drv_pos_pppPerfMemGet
* 功能描述: 获取保存ppp性能的内存
* 访问的表: 软件表g_drv_pos_ppp_perf
* 修改的表: 无
* 输入参数: chipId: 0~3 portIdx 0~7
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_pppPerfMemGet(BYTE chipId, BYTE portIdx, DRV_POS_PPP_PERF** ppPerf)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(ppPerf, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] ppAlarm is NULL", chipId, portIdx);
    
    *ppPerf = &(g_drv_pos_ppp_perf[chipId][portIdx]);
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_pppPerfClear
* 功能描述: 清空保存ppp性能的内存
* 访问的表: 软件表g_drv_pos_ppp_alarm
* 修改的表: 软件表g_drv_pos_ppp_alarm
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_pppPerfClear(BYTE chipId, BYTE portIdx)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    
    memset(&(g_drv_pos_ppp_perf[chipId][portIdx]), 0, sizeof(DRV_POS_PPP_PERF));
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_pppPerfSave
* 功能描述: 保存ppp告警
* 访问的表: 软件表g_drv_pos_ppp_perf
* 修改的表: 软件表g_drv_pos_ppp_perf
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_pppPerfSave(BYTE chipId, BYTE portIdx, const DRV_POS_PPP_PERF *pPppPerf)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pPppPerf, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pPppPerf is NULL", chipId);
    
    memset(&(g_drv_pos_ppp_perf[chipId][portIdx]), 0, sizeof(DRV_POS_PPP_PERF));
    memcpy(&(g_drv_pos_ppp_perf[chipId][portIdx]), pPppPerf, sizeof(DRV_POS_PPP_PERF));

    if(DRV_POS_PPP_PERF_DBG == g_pos_perf_debug)
    {
        ROSNG_TRACE_DEBUG("**** chip[%d] portIdx[%d] ppp perf save ****\n", chipId, portIdx);
        ROSNG_TRACE_DEBUG("pppPerf.tx_pkt = %d\n", g_drv_pos_ppp_perf[chipId][portIdx].tx_pkt);
        ROSNG_TRACE_DEBUG("pppPerf.tx_byte = %d\n", g_drv_pos_ppp_perf[chipId][portIdx].tx_byte);
        ROSNG_TRACE_DEBUG("pppPerf.tx_goodPkt %d\n", g_drv_pos_ppp_perf[chipId][portIdx].tx_goodPkt);
        ROSNG_TRACE_DEBUG("pppPerf.tx_abortPkt = %d\n", g_drv_pos_ppp_perf[chipId][portIdx].tx_abortPkt);
    
        ROSNG_TRACE_DEBUG("pppPerf.rx_pkt = %d\n", g_drv_pos_ppp_perf[chipId][portIdx].rx_pkt);
        ROSNG_TRACE_DEBUG("pppPerf.rx_byte = %d\n", g_drv_pos_ppp_perf[chipId][portIdx].rx_byte);
        ROSNG_TRACE_DEBUG("pppPerf.rx_goodPkt = %d\n", g_drv_pos_ppp_perf[chipId][portIdx].rx_goodPkt);
        ROSNG_TRACE_DEBUG("pppPerf.rx_abortPkt = %d\n", g_drv_pos_ppp_perf[chipId][portIdx].rx_abortPkt);
        ROSNG_TRACE_DEBUG("pppPerf.rx_fcsErrPkt = %d\n", g_drv_pos_ppp_perf[chipId][portIdx].rx_fcsErrPkt);
        ROSNG_TRACE_DEBUG("pppPerf.rx_addrCtrlErrPkt = %d\n", g_drv_pos_ppp_perf[chipId][portIdx].rx_addrCtrlErrPkt);
        ROSNG_TRACE_DEBUG("pppPerf.rx_sapiErrPkt = %d\n", g_drv_pos_ppp_perf[chipId][portIdx].rx_sapiErrPkt);
        ROSNG_TRACE_DEBUG("pppPerf.rx_errPkt = %d\n", g_drv_pos_ppp_perf[chipId][portIdx].rx_errPkt);
    }
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_pppPerfGet
* 功能描述: 获取ppp上性能统计
* 访问的表: 
* 修改的表: 
* 输入参数:  
* 输出参数: pSectionAlm: 保存段告警
* 返 回 值: 
* 其它说明: 包括再生段告警和复用段告警
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_pppPerfGet(BYTE chipId, BYTE portIdx, DRV_POS_PPP_PERF *pPppPerf)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtHdlcChannel atHdlcChannel = NULL;
    tAtHdlcChannelCounters hdlcCounters;
    eAtRet atRetLock = cAtOk;
    eAtRet atRet = cAtOk;
    AtModuleEncap atModuleEncap = NULL;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pPppPerf, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pPppPerf is NULL", chipId, portIdx);

    memset(&hdlcCounters, 0, sizeof(hdlcCounters));
    
    
    ret = drv_pos_get_atModuleEncap(chipId, &atModuleEncap);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_atModuleEncap failed, return %d", chipId, ret);
                
    atRetLock = AtModuleLock((AtModule)atModuleEncap);
    DRV_POS_IF_AT_ERROR_RET(atRetLock, "chip[%d] portIdx[%d] AtModuleLock failed, return %s",  chipId, portIdx, AtRet2String(atRetLock));

    ret = drv_pos_get_atHdlcChannel(chipId, portIdx, &atHdlcChannel);
    if(DRV_POS_HDLC_CHANNEL_NOT_CREATE == ret)
    {
    
        atRetLock = AtModuleUnLock((AtModule)atModuleEncap);
        DRV_POS_IF_AT_ERROR_RET(atRetLock, "chip[%d] portIdx[%d] AtModuleUnLock failed, return %s", chipId, portIdx, AtRet2String(atRetLock));
        DRV_POS_ERROR_RET(ret, "chip[%d] portIdx[%d] atHdlcChannel not been create, drv_pos_get_atHdlcChannel return %d",
            chipId, portIdx, ret);
    }

    atRet = AtChannelAllCountersClear((AtChannel)atHdlcChannel, &hdlcCounters);


    atRetLock = AtModuleUnLock((AtModule)atModuleEncap);
    DRV_POS_IF_AT_ERROR_RET(atRetLock, "chip[%d] portIdx[%d] AtModuleUnLock failed, return %s",  chipId, portIdx, AtRet2String(atRetLock));

    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] AtChannelAllCountersClear failed, return %s, atHdlcChannel = %p",
        chipId, portIdx, AtRet2String(atRet), atHdlcChannel);

    pPppPerf->tx_pkt += hdlcCounters.txPkt;
    pPppPerf->tx_byte += hdlcCounters.txByte;
    pPppPerf->tx_goodPkt += hdlcCounters.txGoodPkt;
    pPppPerf->tx_abortPkt += hdlcCounters.txAbortPkt;

    pPppPerf->rx_pkt += hdlcCounters.rxPkt;
    pPppPerf->rx_byte += hdlcCounters.rxByte;
    pPppPerf->rx_goodPkt += hdlcCounters.rxGoodPkt;
    pPppPerf->rx_abortPkt += hdlcCounters.rxAbortPkt;
    pPppPerf->rx_fcsErrPkt += hdlcCounters.rxFcsErrPkt;
    pPppPerf->rx_addrCtrlErrPkt += hdlcCounters.rxAddrCtrlErrPkt;
    pPppPerf->rx_sapiErrPkt += hdlcCounters.rxSapiErrPkt;
    pPppPerf->rx_errPkt += hdlcCounters.rxErrPkt;

    if(DRV_POS_PPP_PERF_DBG == g_pos_perf_debug)
    {
        ROSNG_TRACE_DEBUG("**** chip[%d] portIdx[%d] ppp perf get ****\n", chipId, portIdx);
        ROSNG_TRACE_DEBUG("pppPerf.tx_pkt = %d\n", pPppPerf->tx_pkt);
        ROSNG_TRACE_DEBUG("pppPerf.tx_byte = %d\n", pPppPerf->tx_byte);
        ROSNG_TRACE_DEBUG("pppPerf.tx_goodPkt %d\n", pPppPerf->tx_goodPkt);
        ROSNG_TRACE_DEBUG("pppPerf.tx_abortPkt = %d\n", pPppPerf->tx_abortPkt);
        
        ROSNG_TRACE_DEBUG("pppPerf.rx_pkt = %d\n", pPppPerf->rx_pkt);
        ROSNG_TRACE_DEBUG("pppPerf.rx_byte = %d\n", pPppPerf->rx_byte);
        ROSNG_TRACE_DEBUG("pppPerf.rx_goodPkt = %d\n", pPppPerf->rx_goodPkt);
        ROSNG_TRACE_DEBUG("pppPerf.rx_abortPkt = %d\n", pPppPerf->rx_abortPkt);
        ROSNG_TRACE_DEBUG("pppPerf.rx_fcsErrPkt = %d\n", pPppPerf->rx_fcsErrPkt);
        ROSNG_TRACE_DEBUG("pppPerf.rx_addrCtrlErrPkt = %d\n", pPppPerf->rx_addrCtrlErrPkt);
        ROSNG_TRACE_DEBUG("pppPerf.rx_sapiErrPkt = %d\n", pPppPerf->rx_sapiErrPkt);
        ROSNG_TRACE_DEBUG("pppPerf.rx_errPkt = %d\n", pPppPerf->rx_errPkt);
    }

    /*不能返回ret，ret是drv_pos_get_atHdlcChannel的结果，创建了，则返回DRV_POS_HDLC_CHANNEL_ALREADY_CREATE*/
    return DRV_POS_OK;
    
}


/**************************************************************************
* 函数名称: drv_pos_pppPerfFlagGet
* 功能描述: 获取监测POS PPP性能统计.
* 访问的表: 软件表g_drv_pos_ppp_perf_enable.
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.           
* 输出参数: pFlag: 保存告警使能标记,DRV_POS_ENABLE使能,DRV_POS_DISABLE非使能.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_pppPerfFlagGet(BYTE chipId, BYTE portIdx, WORD32 *pFlag)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_POINTER_CHECK(pFlag, DRV_POS_PT_ERR, "chip[%d] pFlag is NULL", chipId);
    
    *pFlag = 0;
    *pFlag = g_drv_pos_ppp_perf_enable[chipId][portIdx];
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_pppPerfFlagSet
* 功能描述: 设置检测ppp性能告警的标记.
* 访问的表: 软件表g_drv_pos_ppp_perf_enable.
* 修改的表: 软件表g_drv_pos_ppp_perf_enable.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           flag: 告警使能标记,DRV_POS_ENABLE使能,DRV_POS_DISABLE非使能.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_pppPerfFlagSet(BYTE chipId, BYTE portIdx, WORD32 flag)
{
    DRV_POS_CHIPID_CHECK(chipId);
    
    g_drv_pos_ppp_perf_enable[chipId][portIdx] = flag;
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_pppPerfMonitor
* 功能描述: 监测STM-1单板的Ppp性能
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pDwSubslotId: STM-1单板的子槽位号 
* 输出参数: 无
* 返 回 值: 
* 其它说明: SDH告警线程的入口函数
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_pppPerfMonitor(const WORD32 *pDwSubslotId)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    BYTE subslotId = 0;
    BYTE chipId = 0;         /* ARRIVE芯片的芯片编号 */
    BYTE portIdx = 0;         /* STM-1单板的155M端口的编号 */
    BYTE portNum = 0;
    BYTE portMap[DRV_POS_MAX_PORT] = {0};

    WORD32 rv = BSP_E_CP3BAN_OK;
    WORD32 dwBoardStatus = 0;  /* CP3BAN单板的状态 */

    DRV_POS_PPP_PERF pppPerf;
    memset(&pppPerf, 0, sizeof(pppPerf));

    DRV_POS_POINTER_CHECK(pDwSubslotId, DRV_POS_PT_ERR, "sdhAlarmMonitor input pDwSubslotId is NULL");

    subslotId = (BYTE)(*pDwSubslotId);
    chipId = subslotId - (BYTE)1;
    DRV_POS_CHIPID_CHECK(chipId);

    if(NULL == pDwSubslotId)
    {
        free((void*)pDwSubslotId);
        pDwSubslotId = NULL;
    }

    g_drv_pos_ppp_perf_thread_id[chipId] = __gettid();
    
    while (DRV_POS_TRUE)
    {
        rv = BSP_cp3banBoardStatusGet((WORD32)subslotId, &dwBoardStatus); /* 获取单板的状态 */
        if (BSP_E_CP3BAN_OK != rv)
        {
            g_drv_pos_ppp_perf_thread_id[chipId] = __gettid();
            DRV_POS_ERROR_RET(DRV_POS_ERROR, "chip[%d] BSP_cp3banBoardStatusGet failed, return %d", chipId, ret);
        }
        if (BSP_CP3BAN_BOARD_OFFLINE == dwBoardStatus)  /* 单板不在位,直接返回成功 */
        {
            g_drv_pos_ppp_perf_thread_id[chipId] = __gettid();
            DRV_POS_ERROR_RET(DRV_POS_ERROR, "chip[%d] board is offline, exit the overhead monitor", chipId, ret);
        }
        
        ret = drv_pos_get_valid_port_map(chipId, portMap, &portNum);
        DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_valid_port_map failed return %d", chipId, ret);

        for (portIdx = 0; portIdx < portNum; portIdx++)
        {
            if(DRV_POS_DISABLE != g_drv_pos_ppp_perf_enable[chipId][portMap[portIdx]])
            {
                ret = drv_pos_pppPerfGet(chipId, portMap[portIdx], &pppPerf);
                if(DRV_POS_OK != ret)
                {
                    continue;
                }
                ret = drv_pos_pppPerfSave(chipId, portMap[portIdx], &pppPerf);
                if(DRV_POS_OK != ret)
                {
                    continue;
                }
            }
            BSP_DelayMs(125);  /* delay 125ms. */
        }
    BSP_DelayMs(1000);  /* delay 1000ms. */
    }
    g_drv_pos_ppp_perf_thread_id[chipId] = __gettid();

    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_pppPerfQuery
* 功能描述: 查询PPP性能
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: ARRIVE芯片的 编号,取值为0~3. 
*           portId: STM-1端口的编号,取值为0~7.
* 输出参数: *pAlmResult: 保存段告警
* 返 回 值: 
* 其它说明: 本函数供产品管理调用.包括posPPP性能统计
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_pppPerfQuery(BYTE chipId, BYTE portIdx, DRV_POS_PPP_PERF *pPerfResult)
{
    WORD32 ret = DRV_POS_OK;  /* 函数返回码 */
    DRV_POS_PPP_PERF *pPerf = NULL;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pPerfResult, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pPerfResult is NULL");

    ret = drv_pos_pppPerfMemGet(chipId, portIdx, &pPerf);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_pppAlmMemGet failed ", chipId, portIdx);

    memset(pPerfResult, 0, sizeof(DRV_POS_PPP_PERF));
    memcpy(pPerfResult, pPerf, sizeof(DRV_POS_PPP_PERF));

    /*然后将自己表项清空*/
    memset(pPerf, 0, sizeof(DRV_POS_PPP_PERF));
    
    return ret;
}

/**************************************************************************
* 函数名称: drv_pos_pppThreadCreate
* 功能描述: 创建ppp性能的线程.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM-1单板的子槽位号 
* 输出参数: 无
* 返 回 值: 
* 其它说明: 该线程用来监测pos单板的ppp性能.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_pppPerfThreadCreate(BYTE subslotId)
{
    DRV_POS_RET rv = DRV_POS_OK;
    WORD32 *pDwSubslotId = NULL;
    BYTE chipId = 0;

    chipId = subslotId - (BYTE)1;
    DRV_POS_CHIPID_CHECK(chipId);
    
    pDwSubslotId = malloc(sizeof(WORD32));
    DRV_POS_POINTER_CHECK(pDwSubslotId, DRV_POS_PT_ERR, "chip[%d] malloc failed", chipId);

    memset(pDwSubslotId, 0, sizeof(WORD32));
    *pDwSubslotId = subslotId;
    
    init_thread(&g_drv_pos_ppp_perf_thread[chipId], 0, 0, 80, (void *)drv_pos_pppPerfMonitor, ((void *)(pDwSubslotId)));
    rv = start_mod_thread(&g_drv_pos_ppp_perf_thread[chipId]);
    if (BSP_OK != rv)
    {
        if(NULL == pDwSubslotId)
        {
            free((void*)pDwSubslotId);
            pDwSubslotId = NULL;
        }
        DRV_POS_ERROR_RET(rv, "chip[%d] ppp perf thread start_mod_thread failed, rv = %d", chipId, rv);
    }
    DRV_POS_SUCCESS_PRINT("chip[%d] drv_pos_pppPerfThreadCreate", chipId);
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_sdhAlmThreadDelete
* 功能描述: 删除SDH告警的线程.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM-1单板的子槽位号 
* 输出参数: 无
* 返 回 值: 
* 其它说明: 该线程用来监测STM-1单板的SDH告警.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_pppPerfThreadDelete(BYTE subslotId)
{
    BYTE chipId = 0;
    
    chipId = subslotId - (BYTE)1;
    DRV_POS_CHIPID_CHECK(chipId);

    if(0 != g_drv_pos_ppp_perf_thread_id[chipId])
    {
        stop_mod_thread(&g_drv_pos_ppp_perf_thread[chipId]);
        memset(&g_drv_pos_ppp_perf_thread[chipId], 0, sizeof(g_drv_pos_ppp_perf_thread[chipId]));
        g_drv_pos_ppp_perf_thread_id[chipId] = 0;
    }
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_pppPerfThreadIdGet
* 功能描述: 获取ppp性能线程的thread ID.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3. 
* 输出参数: *pThreadId: 用来保存thread ID.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_pppPerfThreadIdGet(BYTE chipId, int *pThreadId)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_POINTER_CHECK(pThreadId, DRV_POS_PT_ERR, "chip[%d] pThreadId is NULL");
    
    *pThreadId = g_drv_pos_ppp_perf_thread_id[chipId];
    
    return DRV_POS_OK;
}

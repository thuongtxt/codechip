/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_pos.c
* 文件标识: 
* 其它说明: 15k pos模块主代码源文件
* 当前版本: V1.0
* 作    者: 刘钰00130907
* 完成日期: 2013-08-15
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/
#include "drv_pwe3_main.h"

#include "drv_pos.h"
#include "drv_pos_intf.h"
#include "drv_pos_overhead.h"
#include "drv_pos_sdh_perf.h"
#include "drv_pos_sdh_alarm.h"



#include "AtEthPort.h"
#include "AtSdhChannel.h"
#include "AtSdhPath.h"
#include "AtEthFlow.h"
#include "AtPppLink.h"
#include "AtSdhAug.h"
#include "AtHalZtePtn.h"
#include "AtZtePtn.h"
#include "AtDriver.h"
#include "AtHdlcChannel.h"

extern WORD32 drv_tdm_fpgaReg16Read(WORD32 subslotId, WORD16 offsetAddr, WORD16 * pValue);
extern WORD32 drv_tdm_fpgaReg16Write(WORD32 subslotId, WORD16 offsetAddr, WORD16 value);

/*********************************************************************
* 函数名称：drv_pos_vlan_mapping
* 功能描述：根据芯片号和端口号，映射到要设置的zteHeader的两层vlan中，
*           映射到指针参数vlan1Tag和vlan2Tag中。
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           portIdx:  端口号，8口范围0~7, 4口范围0~3
* 输出参数：vlan1Tag: 指向tAtZtePtnTag1的指针，函数执行后，得到设置的vlan1的值
*           vlan2Tag: 指向tAtZtePtnTag2的指针，函数执行后，得到设置的vlan2的值
*                      主要是这个有效
* 返 回 值：返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码 
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/9/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_vlan_mapping(BYTE chipId, BYTE portIdx, tAtZtePtnTag1* vlan1Tag, tAtZtePtnTag2* vlan2Tag)
{
    
    /*check chipid*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    /*check input pointer*/
    DRV_POS_POINTER_CHECK(vlan1Tag, DRV_POS_PT_ERR, "vlan1 = NULL");
    DRV_POS_POINTER_CHECK(vlan2Tag, DRV_POS_PT_ERR, "vlan2 = NULL");

    memset(vlan1Tag, 0, sizeof(tAtZtePtnTag1));
    memset(vlan2Tag, 0, sizeof(tAtZtePtnTag2));

    /* TODO: This following code just for test current FPGA, will be removed later */
    vlan1Tag->cfi = 0;
    vlan1Tag->cpuPktIndicator = 0;
    vlan1Tag->encapType = 0;
    vlan1Tag->priority = 5;
    vlan1Tag->packetLength = 0;

    vlan2Tag->cfi = 0;
    vlan2Tag->isMlpppIma = 0;
    vlan2Tag->priority = 5;
    vlan2Tag->stmPortId = portIdx;
    vlan2Tag->zteChannelId = 255;

    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_atPos_hal_create
* 功能描述：创建arrive的Hal
*           
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
* 输出参数： 
* 返 回 值：返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码 
*          
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_hal_create(BYTE chipId)
{
    DRV_POS_RET ret = DRV_POS_OK;
    WORD32 systemType = 0;
    WORD32 *pBaseAdd = 0;
    AtHal atHal = NULL;

    /*check chipid*/
    DRV_POS_CHIPID_CHECK(chipId);

    /*get atHal*/
    ret = drv_pos_get_atHal(chipId, &atHal);
    if(DRV_POS_HAL_ALREADY_CREATE == ret)/*如果hal已经创建，则不再创建，返回已经创建*/
    {
        return DRV_POS_HAL_ALREADY_CREATE;
    }
    else if(DRV_POS_HAL_NOT_CREATE == ret) /*如果hal没有创建，则创建*/
    { 
        /*get system type*/
        ret = drv_pos_get_systemType(chipId, &systemType);
        DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_systemType failed, return %d", chipId, ret);
        if (0 == systemType)  /* 非15K-2系统 间接访问*/
        {
            DRV_POS_PRINT("no 15K-2 system");
            atHal = AtHalZtePtnEpldNew(chipId+1, drv_tdm_fpgaReg16Read, drv_tdm_fpgaReg16Write);
        }
        else  /* 15K-2系统 直接访问*/
        {
            ret = drv_pos_get_BaseAddress(chipId, &pBaseAdd);
            DRV_POS_IF_ERROR_RET(ret, "chip[%d] baseAdress get failed", chipId);

            atHal = AtHalZtePtnNew((WORD32)pBaseAdd);
        }
        
        DRV_POS_POINTER_CHECK(atHal, DRV_POS_AT_ERR, "chip[%d] hal create failed", chipId);

        ret = drv_pos_save_atHal(chipId, atHal);
        DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_save_atHal failed, return %d", chipId, ret);

        DRV_POS_PRINT("success to create hal, hal is %p", atHal);
        return DRV_POS_OK;
    }
    else/*否则其他错误，则直接返回错误码*/
    {
        return ret;
    }
}

/*********************************************************************
* 函数名称：drv_atPos_hal_delete
* 功能描述：arrive的Hal删除
*           
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3  
* 输出参数： 
* 返 回 值：返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码 
*          
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_hal_delete(BYTE chipId)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtHal atHal = NULL;
    /*check chipid*/
    DRV_POS_CHIPID_CHECK(chipId);

    ret = drv_pos_get_atHal(chipId, &atHal);
    if(DRV_POS_HAL_ALREADY_CREATE == ret)/*如果获取成功，则删除*/
    {
        AtHalDelete(atHal);
        ret = drv_pos_save_atHal(chipId, NULL);
        DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_save_atHal failed, return %d", chipId, ret);
    }
    else if(DRV_POS_HAL_NOT_CREATE ==  ret)/*否则如果返回没有创造，则也返回成功，但不执行删除操作*/
    {
        ret = drv_pos_save_atHal(chipId, NULL);
        DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_save_atHal failed, return %d", chipId, ret);
    }
    else/*否则返回失败*/
    {
        return DRV_POS_HAL_DELETE_ERROR;
    }
    
    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_atPos_get_productCode
* 功能描述：根据pos板的baseAddress获取POS卡的产品码
*           
* 访问的表：
* 修改的表：
* 输入参数：chipId:       芯片序列号，范围0~3
*           atHal :       arrive的atHal
* 输出参数：pProductCode: 产品码，用以区分不同的fpga
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_get_productCode(BYTE chipId, AtHal atHal, WORD32 *pProductCode)
{   
    DRV_POS_RET ret = DRV_POS_OK;
    AtDriver atDriver = NULL;
    /*check input pointer*/
    DRV_POS_POINTER_CHECK(pProductCode, DRV_POS_PT_ERR, "chip[%d] productCode = NULL", chipId);
    DRV_POS_POINTER_CHECK(atHal, DRV_POS_PT_ERR, "chip[%d] atHal = NULL", chipId);
    /*check chipid*/
    DRV_POS_CHIPID_CHECK(chipId);
    
    /*get product*/
    *pProductCode = AtHalRead(atHal, 0x0);  /* Get product code. */
    ROSNG_TRACE_DEBUG("AtHalRead atHal = %p, productCode = 0x%x\n", atHal, *pProductCode);

    ret = drv_pos_get_atDriver(chipId, &atDriver);
    if(DRV_POS_DRIVER_NOT_CREATE == ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] at driver not been created, drv_pos_get_atDriver return %d", chipId, ret);
    }

    /* check product code */
    if (!AtDriverProductCodeIsValid(atDriver, *pProductCode))
    {
        DRV_POS_ERROR_RET(DRV_POS_AT_ERR, "chip[%d] product code is 0x%x, invalid", chipId, *pProductCode);
    }
    return DRV_POS_OK;

}

/*********************************************************************
* 函数名称：drv_atPos_driver_create
* 功能描述：pos板创建at的driver
*           一个线卡只创建一个driver
* 访问的表：
* 修改的表：
* 输入参数：chipId:       芯片序列号，范围0~3
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_driver_create(BYTE chipId)
{
    DRV_POS_RET ret;
    AtOsal atOsal = NULL;
    AtDriver atDriver = NULL;
    BYTE devNum = 0;
    
    DRV_POS_CHIPID_CHECK(chipId);

    ret = drv_pos_get_atDriver(chipId, &atDriver);
    if(DRV_POS_DRIVER_ALREADY_CREATE == ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] pos driver has already been created, drv_pos_get_atDriver return %d", chipId, ret);
    }

    /*获取创建device的个数*/
    devNum = drv_pos_get_atMaxDeviceNum(chipId);
    if(0 == devNum)
    {
        DRV_POS_ERROR_RET(DRV_POS_DEVICE_MAX_NUM_ERROR, "chip[%d] drv_pos_get_atMaxDeviceNum failed, devNum is 0", chipId);
    }

    /*判断atdriver是否已经在其他chip中创建过，一个cpu中只创建一个driver*/
    if(NULL == AtDriverSharedDriverGet())
    {
        /*获取操作系统Osal*/
        atOsal = AtOsalLinux();
        DRV_POS_POINTER_CHECK(atOsal, DRV_POS_AT_ERR, "chip[%d] AtOsalLinux return NULL", chipId);
        /*创建Driver*/
        atDriver = AtDriverCreate(devNum, atOsal);
        DRV_POS_POINTER_CHECK(atDriver, DRV_POS_AT_ERR, 
            "chip[%d] AtDriverCreate failed, devNum is %d, atOsal is %p", chipId, devNum, atOsal);
        
        ret = drv_pos_save_atDriver(chipId, atDriver);
        DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_save_atDriver failed, return %d", chipId, ret);
        
    }
    else
    {
        /*如果已经创建过了，则直接获取，不再创建*/
        atDriver = AtDriverSharedDriverGet();
        DRV_POS_POINTER_CHECK(atDriver, DRV_POS_AT_ERR, 
            "chip[%d] AtDriverSharedDriverGet failed", chipId);
        
        ret = drv_pos_save_atDriver(chipId, atDriver);
        DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_save_atDriver failed, return %d", chipId, ret);
    }
    
    DRV_POS_PRINT("success to create driver, driver is %p", atDriver);
    return DRV_POS_OK;
}
/*********************************************************************
* 函数名称：drv_atPos_driver_delete
* 功能描述：删除arrive的driver
* 访问的表：
* 修改的表：
* 输入参数: chipId:   芯片序列号，范围0~3 
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_driver_delete(BYTE chipId)
{
    AtDriver atDriver = NULL;
    AtDevice atDevice = NULL;
    DRV_POS_RET ret = DRV_POS_OK;
    BYTE devNum = 0;

    DRV_POS_CHIPID_CHECK(chipId);

    ret = drv_pos_get_atDriver(chipId, &atDriver);
    if(DRV_POS_DRIVER_NOT_CREATE == ret)/*如果driver本身就不存在，则不用删除*/
    {
        return DRV_POS_OK;
    }
    /*否则根据device的情况进行删除*/
    ret = drv_pos_get_atDevice(chipId, &atDevice);
    if(DRV_POS_DEVICE_NOT_CREATE == ret)/*如果本子卡的device删除了，则删除driver*/
    {
        AtDriverAddedDevicesGet(atDriver, &devNum);
        if(0 == devNum)/*如果device个数为0，即driver下没有device了，则删除driver*/
        {
            AtDriverDelete(atDriver);
        }
        
        ret = drv_pos_save_atDriver(chipId, NULL);
        DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_save_atDriver failed, return %d", chipId, ret);
    }
    else/*否则返回删除错误*/
    {
        DRV_POS_ERROR_RET(DRV_POS_DRIVER_DELETE_ERROR, "chip[%d] device must be delete before driver delete", chipId);
    }
    
    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_atPos_device_create
* 功能描述：pos板创建arrive的device
* 访问的表：
* 修改的表：
* 输入参数: chipId:   芯片序列号，范围0~3  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_device_create(BYTE chipId)
{
    WORD32 productCode = 0;
    AtDevice atDevice = NULL;
    AtHal    atHal = NULL;
    AtDriver atDriver = NULL;
    DRV_POS_RET ret = DRV_POS_OK;

    DRV_POS_CHIPID_CHECK(chipId);

    ret = drv_pos_get_atDevice(chipId, &atDevice);
    if(DRV_POS_DRIVER_ALREADY_CREATE == ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] at device already been created, drv_pos_get_atDevice return %d", chipId, ret);
    }
    else if(DRV_POS_DEVICE_NOT_CREATE)
    { 
        /*Get Driver*/
        ret = drv_pos_get_atDriver(chipId, &atDriver);
        if(DRV_POS_DRIVER_NOT_CREATE == ret)
        {
            DRV_POS_ERROR_RET(ret, "chip[%d] pos driver has not been created, drv_pos_get_atDriver return %d", chipId, ret);
        }
                
        /*Get Hal*/
        ret = drv_pos_get_atHal(chipId, &atHal);
        if(DRV_POS_HAL_NOT_CREATE == ret)
        {
            DRV_POS_ERROR_RET(ret, "chip[%d] at Hal not been created, drv_pos_get_atHal return %d",
                chipId, ret);
        }

        /*Get Product code*/
        ret = drv_atPos_get_productCode(chipId, atHal, &productCode);
        DRV_POS_IF_ERROR_RET(ret, "chip[%d] hal = %p, productCode = 0x%x",chipId, atHal, productCode);
        ROSNG_TRACE_DEBUG("drv_atPos_get_productCode success, productCode = 0x%x\n", productCode);        

        /* Create atDevice*/
        atDevice = AtDriverDeviceCreate(atDriver, productCode);
        DRV_POS_POINTER_CHECK(atDevice, DRV_POS_AT_ERR, "chip[%d] AtDriverDeviceCreate failed, atDriver = %p, productCode = 0x%x",
            chipId, atDriver, productCode);

        ret = drv_pos_save_atDevice(chipId, atDevice);
        DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_save_atDevice failed", chipId);

        DRV_POS_PRINT("success to create device, device is %p", atDevice);
        return DRV_POS_OK;
    }
    else/*否则返回错误码*/
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] device get failed, return %d", chipId, ret);
    }
}

/*********************************************************************
* 函数名称：drv_atPos_device_delete
* 功能描述：pos板删除at的device
* 访问的表：
* 修改的表：
* 输入参数: chipId:   芯片序列号，范围0~3   
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_device_delete(BYTE chipId)
{
    AtDevice atDevice = NULL;
    AtDriver atDriver = NULL;
    DRV_POS_RET ret = DRV_POS_OK;
    DRV_POS_CHIPID_CHECK(chipId);
    
    ret = drv_pos_get_atDevice(chipId, &atDevice);
    if(DRV_POS_DEVICE_ALREADY_CREATE == ret)
    {
        ret = drv_pos_get_atDriver(chipId, &atDriver);
        if(DRV_POS_DRIVER_NOT_CREATE == ret)
        {
            DRV_POS_ERROR_RET(ret, "chip[%d] pos driver has not been created, drv_pos_get_atDriver return %d", chipId, ret);
        }

        AtDriverDeviceDelete(atDriver, atDevice);

        ret = drv_pos_save_atDevice(chipId, NULL);
        DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_save_atDevice failed, return %d", chipId, ret);
    }
    else if(DRV_POS_DEVICE_NOT_CREATE == ret)/*如果返回值是device还没有创建，则也认为成功*/
    {
        ret = DRV_POS_OK;
    }
    else
    {
        ret = DRV_POS_DEVICE_DELETE_ERROR;
    }
    return ret;
}


/*********************************************************************
* 函数名称：drv_atPos_device_setup
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数: chipId:   芯片序列号，范围0~3 
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/17            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_device_setup(BYTE chipId)
{
    BYTE ipCoreNum = 0;
    BYTE ipCoreIdx = 0;
    AtDevice atDevice = NULL;
    AtHal atHal = NULL;
    AtIpCore atIpCore = NULL;
    eAtRet atRet = cAtOk;
    DRV_POS_RET ret = DRV_POS_OK;
  
    DRV_POS_CHIPID_CHECK(chipId);

    /*get device*/
    ret = drv_pos_get_atDevice(chipId, &atDevice);
    if(DRV_POS_DRIVER_NOT_CREATE == ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] at driver not been created, drv_pos_get_atDriver return %d", chipId, ret);
    }

    /*get Hal*/
    ret = drv_pos_get_atHal(chipId, &atHal);
    if(DRV_POS_HAL_NOT_CREATE == ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] at Hal not been created, drv_pos_get_atHal return %d",
            chipId, ret);
    }
    
    /*get ipCoreNum of device*/
    ipCoreNum = AtDeviceNumIpCoresGet(atDevice);
    if(0 == ipCoreNum)
    {
        DRV_POS_ERROR_RET(DRV_POS_IPCORE_NUM_ERR, "chip[%d] AtDeviceNumIpCoresGet failed, return ipCoreNum is %d", chipId, ipCoreNum);
    }
    
    for (ipCoreIdx = 0; ipCoreIdx < ipCoreNum; ipCoreIdx++)
    {
        /*get ip core*/
        atIpCore = AtDeviceIpCoreGet(atDevice, ipCoreIdx);
        DRV_POS_POINTER_CHECK(atIpCore, DRV_POS_AT_ERR, 
            "chip[%d] AtDeviceIpCoreGet faile, atDevice = %p, ipCoreIdx = %d", 
            chipId, atDevice, ipCoreIdx);

        /*Set ip core hal*/
        atRet = AtIpCoreHalSet(atIpCore, atHal);
        DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] AtIpCoreHalSet error return %s, ipCore = %p, atHal = %p",
            chipId, AtRet2String(atRet), atIpCore, atHal);
            
        DRV_POS_PRINT("success to set ip core hal, ip index = %d", ipCoreIdx);

        /* Enable/disable interrupt on this core */
        atRet = AtIpCoreInterruptEnable(atIpCore, cAtTrue);
        DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] AtIpCoreInterruptEnable failed, return %s, ipCore = %p",
            chipId, AtRet2String(atRet), atIpCore);
            
        DRV_POS_PRINT("success to enable ip core interrupt, ip index = %d", ipCoreIdx);
    }

    return DRV_POS_OK;

}
/*********************************************************************
* 函数名称：drv_atPos_device_Inital
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数: chipId:   芯片序列号，范围0~3 
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/17            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_device_initial(BYTE chipId)
{
    AtDevice atDevice = NULL;
    eAtRet atRet = cAtOk;
    DRV_POS_RET ret = DRV_POS_OK;
  
    DRV_POS_CHIPID_CHECK(chipId);

    /*get device*/
    ret = drv_pos_get_atDevice(chipId, &atDevice);
    if(DRV_POS_DRIVER_NOT_CREATE == ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] at driver not been created, drv_pos_get_atDriver return %d", chipId, ret);
    }

    /* Initialize ARRIVE device */
    atRet = AtDeviceInit(atDevice);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] AtDeviceInit fail return %s", chipId, AtRet2String(atRet));

    DRV_POS_PRINT("success to initial chip[%d] device ", chipId);
    return DRV_POS_OK;

}


/*********************************************************************
* 函数名称：drv_atPos_sdhModuleInterruptEnable
* 功能描述：使能sdh模块的中断
*           
* 访问的表：
* 修改的表：
* 输入参数: chipId:   芯片序列号，范围0~3 
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/17            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_sdhModuleInterruptEnable(BYTE chipId)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtModuleSdh atModuleSdh = NULL;
    eAtRet atRet = cAtOk;

    ret = drv_pos_get_atModuleSdh(chipId, &atModuleSdh);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_atModuleSdh failed, return %d", chipId, ret);
    
    atRet = AtModuleInterruptEnable((AtModule)atModuleSdh, cAtTrue);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] AtModuleInterruptEnable failed, return %s, atModuleSdh = %p",
        chipId, AtRet2String(atRet), atModuleSdh);

    return ret;
}

/*********************************************************************
* 函数名称：drv_atPos_set_sdhModeRate
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           portIdx:  端口号，8口范围0~7, 4口范围0~3
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/17            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_set_sdhModeRate(BYTE chipId, BYTE portIdx)
{
    eAtRet atRet = cAtOk;
    AtSdhLine atSdhLine = NULL;
    DRV_POS_STM_RATE_MODE stmMode = DRV_POS_STM_RATE_INVALID;
    DRV_POS_RET ret = DRV_POS_OK;
    eAtSdhLineRate atStmMode = cAtSdhLineRateInvalid;
    DRV_POS_PORT_GROUP portGrp = DRV_POS_LEFT_PORT_GRP;
    
    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine fail, return %d", chipId, portIdx, ret);
    
    /* Set line mode to SDH */
    atRet = AtSdhLineModeSet(atSdhLine, cAtSdhLineModeSdh);
    DRV_POS_IF_AT_ERROR_RET(atRet, "SDH mode set failed, sdhLine = %p, return %s", atSdhLine, AtRet2String(atRet));

    portGrp = (portIdx < 4)? DRV_POS_LEFT_PORT_GRP : DRV_POS_RIGHT_PORT_GRP;

    ret = drv_pos_get_stmMode(chipId, portGrp, &stmMode);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_stmMode failed, portGrp = %d, return %d", chipId, portIdx, portGrp, ret);

    switch(stmMode)
    {
        case DRV_POS_STM_RATE_STM1:
            atStmMode = cAtSdhLineRateStm1;
            break;
        case DRV_POS_STM_RATE_STM4:
            atStmMode = cAtSdhLineRateStm4;
            break;
        default:
            DRV_POS_ERROR_RET(DRV_POS_ERROR, "chip[%d] stm rate mode is error!, stmMode = %d", chipId, stmMode);
    }

    atRet = AtSdhLineRateSet(atSdhLine, atStmMode); /* set SDH frame rate */
    DRV_POS_IF_AT_ERROR_RET(atRet, "SDH rate set failed, return %s, sdhLine = %p, atStmMode = %d",
        AtRet2String(atRet), atSdhLine, atStmMode);

    return DRV_POS_OK;

}

/*********************************************************************
* 函数名称：drv_atPos_set_sdhMaping
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           portIdx:  端口号，8口范围0~7, 4口范围0~3
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/17            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_set_sdhMaping(BYTE chipId, BYTE portIdx)
{
    AtSdhAug atAug1 = NULL;
    AtSdhAug atAug4 = NULL;
    BYTE aug1Id = 0;
    eAtRet atRet = cAtOk;
    AtSdhLine atSdhLine = NULL;
    DRV_POS_RET ret = DRV_POS_OK;
    DRV_POS_STM_RATE_MODE stmMode = DRV_POS_STM_RATE_INVALID;
    DRV_POS_PORT_GROUP portGrp = DRV_POS_LEFT_PORT_GRP;
    
    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine fail, return %d", chipId, portIdx, ret);

    portGrp = (portIdx < 4)? DRV_POS_LEFT_PORT_GRP : DRV_POS_RIGHT_PORT_GRP;

    ret = drv_pos_get_stmMode(chipId, portGrp, &stmMode);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_stmMode failed, portGrp = %d, return %d", 
        chipId, portIdx, portGrp, ret);

    switch(stmMode)
    {
        case DRV_POS_STM_RATE_STM1:
            /* Map desired AUG-1 to VC-4 */
            atAug1 = AtSdhLineAug1Get(atSdhLine, aug1Id); /* Get AUG-1 */
            DRV_POS_POINTER_CHECK(atAug1, DRV_POS_AT_ERR, "sdh line aug get failed, atLine = %p, aug1Id = %d", 
                atSdhLine, aug1Id);
            
            atRet = AtSdhChannelMapTypeSet((AtSdhChannel)atAug1, cAtSdhAugMapTypeAug1MapVc4);
            DRV_POS_IF_AT_ERROR_RET(atRet, "SDH channel mapping vc4 failed, return %s, atAug1 = %p", AtRet2String(atRet), atAug1 );
            break;
        case DRV_POS_STM_RATE_STM4:
            atAug4 = AtSdhLineAug4Get(atSdhLine, 0);
            DRV_POS_POINTER_CHECK(atAug4, DRV_POS_AT_ERR, "AtSdhLineAug4Get failed, atSdhLine = %p", atSdhLine);
            
            /* Get AUG-4 of this line and map it to vc4_4c */
            atRet = AtSdhChannelMapTypeSet((AtSdhChannel)atAug4, cAtSdhAugMapTypeAug4MapVc4_4c);
            DRV_POS_IF_AT_ERROR_RET(atRet, 
                "AtSdhChannelMapTypeSet failed, return %s, aug4 = %p", AtRet2String(atRet), atAug4);

            break;
        default:
            DRV_POS_ERROR_RET(DRV_POS_ERROR, "chip[%d] stm rate mode is error!, stmMode = %d", chipId, stmMode);
    }

    return DRV_POS_OK;

}


/*********************************************************************
* 函数名称：drv_atPos_set_sdhScramble
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           portIdx:  端口号，8口范围0~7, 4口范围0~3
*           isEnable: 是否使能，1为使能，0为非使能
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/17            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_sdhScramble_cfg(BYTE chipId, BYTE portIdx, BYTE isEnable)
{
    eAtRet atRet = cAtOk;
    AtSdhLine atSdhLine = NULL;
    DRV_POS_RET ret = DRV_POS_OK;
    
    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine fail, return %d", chipId, portIdx, ret);

    atRet = AtSdhLineScrambleEnable(atSdhLine, isEnable);
    DRV_POS_IF_AT_ERROR_RET(atRet, "SDH scramble enable failed, return %s, atSdhLine = %p", AtRet2String(atRet), atSdhLine);
   
    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_atPos_sdhSectionOh_cfg
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           portIdx:  端口号，8口范围0~7, 4口范围0~3
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/17            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_sdhSectionOh_cfg(BYTE chipId, BYTE portIdx)
{
    BYTE j0Msg[64] = "zte_j0";
    eAtRet atRet = cAtOk;
    AtSdhLine atSdhLine = NULL;
    DRV_POS_RET ret = DRV_POS_OK;

    /* configure TTI, both transmitted and expected */
    ret = drv_pos_txJ0Set(chipId, portIdx, DRV_POS_TRACE_MSG_16BYTE, j0Msg);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_txJ1Set failed, return %d", chipId, portIdx, ret);

    ret = drv_pos_expectedRxJ0Set(chipId, portIdx, DRV_POS_TRACE_MSG_16BYTE, j0Msg);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_expectedRxJ1Set failed, return %d", chipId, portIdx, ret);

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine fail, return %d", chipId, portIdx, ret);
    
    /* 默认时disable RS-TIM monitoring */
    atRet = AtSdhChannelTimMonitorEnable((AtSdhChannel)atSdhLine, cAtFalse);
    DRV_POS_IF_AT_ERROR_RET(atRet, "sdh line AtSdhChannelTimMonitorEnable failed, atSdhLine = %p, return %s",
        atSdhLine, AtRet2String(atRet));
    DRV_POS_PRINT("  success to set sdhLine tim monitor disable");

    /* 当有RS-TIM告警时,默认不允许下插AIS告警*/
    atRet = AtSdhChannelAlarmAffectingEnable((AtSdhChannel)atSdhLine, cAtSdhLineAlarmTim, cAtFalse);
    DRV_POS_IF_AT_ERROR_RET(atRet, "sdh line AtSdhChannelAlarmAffectingEnable failed, atSdhLine = %p, return %s",
        atSdhLine, AtRet2String(atRet));

    DRV_POS_PRINT("  success to set sdhLine alarm affecting disable");
    

    return DRV_POS_OK;

}

/*********************************************************************
* 函数名称：drv_atPos_sdhHpOh_cfg
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           portIdx:  端口号，8口范围0~7, 4口范围0~3
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/17            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_sdhHpOh_cfg(BYTE chipId, BYTE portIdx)
{
    eAtRet atRet = cAtOk;
    BYTE j1Msg[64] = "zte_j1";
    tAtSdhTti tti;
    AtSdhVc atSdhVc = NULL;
    AtSdhChannel atSdhChannel = NULL;
    DRV_POS_RET ret = DRV_POS_OK;
    memset(&tti, 0, sizeof(tAtSdhTti));

    /* configure TTI, both transmitted and expected */
    ret = drv_pos_txJ1Set(chipId, portIdx, DRV_POS_TRACE_MSG_16BYTE, j1Msg);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_txJ0Set failed, return %d", chipId, portIdx, ret);

    ret = drv_pos_expectedRxJ1Set(chipId, portIdx, DRV_POS_TRACE_MSG_16BYTE, j1Msg);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_expectedRxJ0Set failed, return %d", chipId, portIdx, ret);    

    ret = drv_pos_get_atSdhVC(chipId, portIdx, &atSdhVc);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhVC fail, return %d", chipId, portIdx, ret);

    atSdhChannel = (AtSdhChannel)atSdhVc;

    /* 默认时disable HP-TIM monitoring */
    atRet = AtSdhChannelTimMonitorEnable(atSdhChannel, cAtFalse);
    DRV_POS_IF_AT_ERROR_RET(atRet, "vc4 AtSdhChannelTimMonitorEnable failed, atSdhChannel = %p, return %s",
        atSdhChannel, AtRet2String(atRet));
    DRV_POS_PRINT("  success to set vc4 tim monitor disable");

    /* 当有HP-TIM告警时,默认不允许插AIS告警*/
    atRet = AtSdhChannelAlarmAffectingEnable(atSdhChannel, cAtSdhPathAlarmTim, cAtFalse);
    DRV_POS_IF_AT_ERROR_RET(atRet, "vc4 AtSdhChannelAlarmAffectingEnable failed, atSdhChannel = %p, return %s",
        atSdhChannel, AtRet2String(atRet));

    DRV_POS_PRINT("  success to set vc4 alarm affection disable");

    /*设置默认C2字节*/
    ret = drv_pos_txC2Set(chipId, portIdx, DRV_POS_DEFAULT_TX_C2);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_txC2Set failed, return %d", chipId, portIdx, ret);

    ret = drv_pos_expectedRxC2Set(chipId, portIdx, DRV_POS_DEFAULT_TX_C2);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_expectedRxC2Set failed, return %d", chipId, portIdx, ret);

    /*默认enable HP-PLM. */
    atRet = AtSdhChannelAlarmAffectingEnable((AtSdhChannel)atSdhChannel, cAtSdhPathAlarmPlm, cAtTrue);
    DRV_POS_IF_AT_ERROR_RET(atRet, "vc4 AtSdhChannelAlarmAffectingEnable plm true failed, return %s",
        AtRet2String(atRet));

    DRV_POS_PRINT("  success to set vc4 alarm affect enable");

    /* 默认disable HP-RDI */
    atRet = AtSdhPathERdiEnable((AtSdhPath)atSdhChannel, cAtFalse);
    DRV_POS_IF_AT_ERROR_RET(atRet, "vc4 AtSdhPathERdiEnable true failed, return %s",
        AtRet2String(atRet));
    DRV_POS_PRINT("  success to set vc4 Path ERdi disable");
   
    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_atPos_sdhOh_cfg
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           portIdx:  端口号，8口范围0~7, 4口范围0~3
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/17            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_sdhOh_cfg(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;

    ret = drv_atPos_sdhSectionOh_cfg(chipId, portIdx);
    DRV_POS_IF_ERROR_RET(ret, "Section OverHead cfg failed");

    ret = drv_atPos_sdhHpOh_cfg(chipId, portIdx);
    DRV_POS_IF_ERROR_RET(ret, "High Path OverHead cfg failed");

    return DRV_POS_OK;

}

/*********************************************************************
* 函数名称：drv_atPos_sdhBer_cfg
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           portIdx:  端口号，8口范围0~7, 4口范围0~3
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/17            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_sdhBer_cfg(BYTE chipId, BYTE portIdx)
{
    AtBerController pBerCtrl = NULL;
    AtSdhVc atSdhVc = NULL;
    AtSdhChannel atSdhChannel = NULL;
    eAtRet atRet = cAtOk;
    AtSdhLine atSdhLine = NULL;
    DRV_POS_RET ret = DRV_POS_OK;
    
    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine fail, return %d", chipId, portIdx, ret);

    pBerCtrl = AtSdhLineRsBerControllerGet(atSdhLine);
    atRet = AtBerControllerEnable(pBerCtrl, cAtTrue);
    DRV_POS_IF_AT_ERROR_RET(atRet, "rs AtBerControllerEnable failed, return %s",
        AtRet2String(atRet));

    DRV_POS_PRINT("  success to set ms Bercontrol enable");
    
    ret = drv_pos_rsSdSfThresholdSet(chipId, portIdx, DRV_POS_SD, DRV_POS_BER_RATE_1E5);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_msSdSfThresholdSet SD fail, return %d",
        chipId, portIdx, ret);

    ret = drv_pos_rsSdSfThresholdSet(chipId, portIdx, DRV_POS_SF, DRV_POS_BER_RATE_1E3);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_msSdSfThresholdSet SF fail, return %d",
        chipId, portIdx, ret);

    DRV_POS_PRINT("  success to set ms sf/sd threshold");

    /* Get BER controller that monitors BER for MS channel.*/
    pBerCtrl = NULL;
    pBerCtrl = AtSdhLineMsBerControllerGet(atSdhLine);
    DRV_POS_POINTER_CHECK(pBerCtrl, DRV_POS_AT_ERR, "ms pBerCtrl get failed, pSdhLine is %p", atSdhLine);

    atRet = AtBerControllerEnable(pBerCtrl, cAtTrue);
    DRV_POS_IF_AT_ERROR_RET(atRet, "ms AtBerControllerEnable failed, return %s",
        AtRet2String(atRet));

    DRV_POS_PRINT("  success to set ms Bercontrol enable");

    ret = drv_pos_msSdSfThresholdSet(chipId, portIdx, DRV_POS_SD, DRV_POS_BER_RATE_1E5);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_msSdSfThresholdSet SD fail, return %d",
        chipId, portIdx, ret);

    ret = drv_pos_msSdSfThresholdSet(chipId, portIdx, DRV_POS_SF, DRV_POS_BER_RATE_1E3);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_msSdSfThresholdSet SF fail, return %d",
        chipId, portIdx, ret);

    DRV_POS_PRINT("  success to set ms sf/sd threshold");

    ret = drv_pos_get_atSdhVC(chipId, portIdx, &atSdhVc);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhVC fail, return %d", chipId, portIdx, ret);

    atSdhChannel = (AtSdhChannel)atSdhVc;

    /* Get BER controller that monitors BER for high path channel.*/
    pBerCtrl = NULL;   /* 先清零再赋值 */
    pBerCtrl = AtSdhChannelBerControllerGet(atSdhChannel);
    DRV_POS_POINTER_CHECK(atSdhChannel, DRV_POS_AT_ERR, "vc4 pBerCtrl get failed");

    atRet = AtBerControllerEnable(pBerCtrl, cAtTrue);
    DRV_POS_IF_AT_ERROR_RET(atRet, "vc4 AtBerControllerEnable failed, return %s",
        AtRet2String(atRet));
    DRV_POS_PRINT("  success to set vc4 BerControl enable");

    ret = drv_pos_vc4SdSfThresholdSet(chipId, portIdx, DRV_POS_SD, DRV_POS_BER_RATE_1E5);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_vc4SdSfThresholdSet SD fail, return %d",
        chipId, portIdx, ret);

    ret = drv_pos_vc4SdSfThresholdSet(chipId, portIdx, DRV_POS_SF, DRV_POS_BER_RATE_1E3);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_vc4SdSfThresholdSet SF fail, return %d",
        chipId, portIdx, ret);

    DRV_POS_PRINT("  success to set vc4 sf/sd threshold");

    return DRV_POS_OK;

}

/*********************************************************************
* 函数名称：drv_atPos_sdhVc4Clk_cfg
* 功能描述：VC4的时钟使用外部EPLD的时钟
*           
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           portIdx:  端口号，8口范围0~7, 4口范围0~3
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/17            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_sdhVcClk_cfg(BYTE chipId, BYTE portIdx)
{
    AtSdhVc atSdhVc = NULL;
    eAtRet atRet = cAtOk;
    DRV_POS_RET ret = DRV_POS_OK;
    
    ret = drv_pos_get_atSdhVC(chipId, portIdx, &atSdhVc);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhVC fail, return %d", chipId, portIdx, ret);

    atRet = AtChannelTimingSet((AtChannel)atSdhVc, cAtTimingModeExt1Ref, NULL);
    DRV_POS_IF_AT_ERROR_RET(atRet, "SDH VC4 timingMode set failed, return %s, atVc4 = %p",
        AtRet2String(atRet), atSdhVc);

    DRV_POS_PRINT("  success to set vc4 timing mode ext#1");

    return DRV_POS_OK;

}

/*********************************************************************
* 函数名称：drv_atPos_global_sdh_cfg
* 功能描述：pos模块at，单个sdh端口创建
*           
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           portIdx:  端口号，8口范围0~7, 4口范围0~3
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/17            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_sdh_init_cfg(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    
    /*check chipId and port number*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    
    /*Set line mode and rate*/
    ret = drv_atPos_set_sdhModeRate(chipId, portIdx);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] sdhLine mode and rate set failed", 
        chipId, portIdx);
    DRV_POS_PRINT("chip[%d] portIdx[%d] success to set sdhLine mode and rate", chipId, portIdx);

    /*Set mapping mode*/
    ret = drv_atPos_set_sdhMaping(chipId, portIdx);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] line sdh mapping mode set failed", 
        chipId, portIdx);
    DRV_POS_PRINT("chip[%d] portIdx[%d] success to set sdhLine mapping", chipId, portIdx);

    /*Set line scramble enable*/
    ret = drv_atPos_sdhScramble_cfg(chipId, portIdx, cAtTrue);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] line sdh scramble enable failed", 
        chipId, portIdx);
    DRV_POS_PRINT("chip[%d] portIdx[%d] success to config sdhLine scramble", chipId, portIdx);
   
    /*Set line overhead*/
    ret = drv_atPos_sdhOh_cfg(chipId, portIdx);
    DRV_POS_IF_ERROR_RET(ret, "line sdh overhead cfg failed");
    DRV_POS_PRINT("chip[%d] port[%d] success to config sdhLine overhead", chipId, portIdx);

    /*Set line Ber*/
    ret = drv_atPos_sdhBer_cfg(chipId, portIdx);
    DRV_POS_IF_ERROR_RET(ret, "line sdh Ber cfg failed");
    DRV_POS_PRINT("chip[%d] port[%d] success to config sdhLine Ber", chipId, portIdx);

    /*设置path层的时钟，默认path使用的是FPGA内部时钟，但是LINE使用的是外部EPLD的时钟，需要锁时钟*/
    ret = drv_atPos_sdhVcClk_cfg(chipId, portIdx);
    DRV_POS_IF_ERROR_RET(ret, "VC4 CLK set ext#1 failed");
    DRV_POS_PRINT("chip[%d] port[%d] success to config SDH VC4 CLK ext#1", chipId, portIdx);

    ret = drv_pos_portShutdownStateSet(chipId, portIdx, DRV_POS_PORT_SHUTDOWN);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_portShutdownStateSet failed, return %d", chipId, portIdx, ret);  
    DRV_POS_PRINT("chip[%d] portIdx[%d] success to set stm port disable(shut down)", chipId, portIdx);

    /*disable所有的alarm中断，等待产品管理调用需要的中断*/
    ret = drv_pos_alarmIntrAllDisable(chipId, portIdx);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_alarmIntrAllDisable failed, return %d", chipId, portIdx);
    DRV_POS_PRINT("chip[%d] portIdx[%d] success to drv_pos_alarmIntrAllDisable", chipId, portIdx);

    /*clear所有的alarm中断*/
    ret = drv_pos_alarmIntrClear(chipId, portIdx);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_alarmIntrClear failed, return %d", chipId, portIdx);

    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_atPos_global_enet_cfg
* 功能描述：pos模块at，单个以太接口创建
*           
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           ethIdx:   和后拼接口以太端口号，范围0~1,
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/17            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_enet_init_cfg(BYTE chipId, BYTE ethIdx)
{ 
    AtEthPort atEthPort = NULL;
    eAtRet atRet = DRV_POS_OK;
    DRV_POS_RET ret = DRV_POS_OK;
    
    /*入参检查*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_ETHPORTIDX_CHECK(ethIdx);

    ret = drv_pos_get_atEthPort(chipId, ethIdx, &atEthPort);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] ethIdx[%d] drv_pos_get_atEthPort failed, return %d", chipId, ethIdx, ret);
    
    /* Set source MAC address */
    atRet = AtEthPortSourceMacAddressSet(atEthPort, g_drv_pos_ePortMac);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] ethPortIdx[%d] ethernet set mac address failed, return %s",
        chipId, ethIdx, AtRet2String(atRet));

    DRV_POS_PRINT("chip[%d] ethPortIdx[%d] success to set mac address", chipId, ethIdx);
    
    /* Disable MAC address check */
    atRet = AtEthPortMacCheckingEnable(atEthPort, cAtFalse);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] ethPortIdx[%d] ethernet set mac check disable failed, return %s",
        chipId, ethIdx, AtRet2String(atRet));
    DRV_POS_PRINT("chip[%d] ethPortIdx[%d] success to disable mac address check", chipId, ethIdx);
    
    /* 设置Ethernet接口的接口类型为SGMII */
    atRet = AtEthPortInterfaceSet(atEthPort, cAtEthPortInterfaceSgmii);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] ethPortIdx[%d] ethernet interface set sgmii failed, return %s",
        chipId, ethIdx, AtRet2String(atRet));
    DRV_POS_PRINT("chip[%d] ethPortIdx[%d] success to set ethernet interface SGMII", chipId, ethIdx);

    /* Set Speed for Ethernet port */
    atRet = AtEthPortSpeedSet(atEthPort, cAtEthPortSpeed1000M);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] ethPortIdx[%d] ethernet port speed set 1000M failed, return %s",
        chipId, ethIdx, AtRet2String(atRet));
    DRV_POS_PRINT("chip[%d] ethPortIdx[%d] success to set ethernet port speed 1000M", chipId, ethIdx);

    /* Set working mode for Ethernet port */
    atRet = AtEthPortDuplexModeSet(atEthPort, cAtEthPortWorkingModeFullDuplex);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] ethPortIdx[%d] ethernet port FullDuplex working mode set failed, return %s",
        chipId, ethIdx, AtRet2String(atRet));
    DRV_POS_PRINT("chip[%d] ethPortIdx[%d] success to set ethernet port duplex mode", chipId, ethIdx);

    /* Disable Ethernet port auto-negotiation */
    atRet = AtEthPortAutoNegEnable(atEthPort, cAtFalse);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] ethPortIdx[%d] ethernet port autoNegotiation disable failed, return %s",
        chipId, ethIdx, AtRet2String(atRet));
    DRV_POS_PRINT("chip[%d] ethPortIdx[%d] success to set ethernet autonegotiation", chipId, ethIdx);
    
    /* Set number of Inter Packet Gaps at Tx direction */
    atRet = AtEthPortTxIpgSet(atEthPort, DRV_POS_AT_TX_IPG);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] ethPortIdx[%d] ethernet port tx gaps set %d failed, return %s",
        chipId, ethIdx, DRV_POS_AT_TX_IPG, AtRet2String(atRet));
    DRV_POS_PRINT("chip[%d] ethPortIdx[%d] success to set tx direction packet gaps %d", chipId, ethIdx, DRV_POS_AT_TX_IPG);

    /* Set number of Inter Packet Gaps at Rx direction */
    atRet = AtEthPortRxIpgSet(atEthPort, DRV_POS_AT_RX_IPG);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] ethPortIdx[%d] ethernet port rx gaps set %d failed, return %s",
        chipId, ethIdx, DRV_POS_AT_RX_IPG, AtRet2String(atRet));
    DRV_POS_PRINT("chip[%d] ethPortIdx[%d] success to set rx direction packet gaps %d", chipId, ethIdx, DRV_POS_AT_RX_IPG);

    /* Disable flow control */
    atRet = AtEthPortFlowControlEnable(atEthPort, cAtFalse);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] ethPortIdx[%d] ethernet port set flow control disable failed, return %s",
        chipId, ethIdx, AtRet2String(atRet));
    DRV_POS_PRINT("chip[%d] ethPortIdx[%d] success to disable flow control", chipId, ethIdx);

    /* Set Pause Frame Interval */
    atRet = AtEthPortPauseFrameIntervalSet(atEthPort, 0);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] ethIdx[%d] set pause frame interval zero, return %s",
        chipId, ethIdx, AtRet2String(atRet));
    DRV_POS_PRINT("chip[%d] ethPortIdx[%d] success to set pause frame interval zero", chipId, ethIdx);

    /* Set Pause Frame expire time */
    atRet = AtEthPortPauseFrameExpireTimeSet(atEthPort, 0);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] ethIdx[%d] set pause frame expire time zero, return %s",
        chipId, ethIdx, AtRet2String(atRet));
    DRV_POS_PRINT("chip[%d] ethPortIdx[%d] success to set pause frame expire time zero", chipId, ethIdx);

    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_atPos_EncapChannel_create
* 功能描述：pos板打通一整条ppplink
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           portIdx:  端口号，8口范围0~7, 4口范围0~3
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_EncapChannel_create(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtModuleEncap atEncapModule = NULL;
    AtHdlcChannel atHdlcChannel = NULL;
    WORD32 chId = 0;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    /*检查hdlc channel是否存在，*/
    ret = drv_pos_get_atHdlcChannel(chipId, portIdx, &atHdlcChannel);
    if(DRV_POS_HDLC_CHANNEL_ALREADY_CREATE == ret)/*如果返回已经创建，则认为出错*/
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] portIdx[%d] at hdlc channel has already been created, drv_pos_get_atHdlcChannel return %d",
            chipId, portIdx, ret);
    }

    ret = drv_pos_get_atModuleEncap(chipId, &atEncapModule);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atModuleEncap fail, return %d", chipId, portIdx, ret);

    chId = (portIdx<4) ? portIdx : portIdx-4+512; /*channelId用portId映射*/

    atHdlcChannel = AtModuleEncapHdlcPppChannelCreate(atEncapModule, chId);
    DRV_POS_POINTER_CHECK(atHdlcChannel, DRV_POS_AT_ERR, 
        "chip[%d] portIdx[%d] hdlcChannel create failed, atEncapModule = %p, channel Id = %d", 
        chipId, portIdx, atEncapModule, chId);

    ret = drv_pos_save_atHdlcChannel(chipId, portIdx, atHdlcChannel);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_save_atHdlcChannel failed, return %d", chipId, portIdx, ret);

    /*设置fcs模式*/
    ret = drv_pos_portFcsModeSet(chipId, portIdx, DRV_POS_PORT_FCS16);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_portFcsModeSet failed, return %d", chipId, portIdx, ret);

    /*设置scramble模式*/
    ret = drv_pos_portScrambleSet(chipId, portIdx, DRV_POS_PORT_SCRAMBLE_ENABLE);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_portScrambleSet scramble enable failed, return %d", 
        chipId, portIdx, ret);
        
    DRV_POS_PRINT("chip[%d] portIdx[%d] success to create atHdlcChannel %p", 
        chipId, portIdx, atHdlcChannel);

    return ret;
}
/*********************************************************************
* 函数名称：drv_atPos_EncapChannel_delete
* 功能描述：pos板删除一整条ppplink
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           portIdx:  端口号，8口范围0~7, 4口范围0~3
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/24            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_EncapChannel_delete(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtModuleEncap atEncapModule = NULL;
    AtHdlcChannel atHdlcChannel = NULL;
    eAtRet atRet = cAtOk;
    WORD32 chId = 0;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    chId = (portIdx<4) ? portIdx : portIdx-4+512; /*channelId用portId映射*/
    /*检查hdlcChannel是否创建，创建了，才能调用芯片函数删除encapChannel*/
    ret = drv_pos_get_atHdlcChannel(chipId, portIdx, &atHdlcChannel);
    if(DRV_POS_HDLC_CHANNEL_NOT_CREATE == ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] portIdx[%d] atHdlcChannel not been create, drv_pos_get_atHdlcChannel return %d",
            chipId, portIdx, ret);
    }

    ret = drv_pos_get_atModuleEncap(chipId, &atEncapModule);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_atModuleEncap failed, return %d", chipId, ret);

    atRet = AtModuleEncapChannelDelete(atEncapModule, chId);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtModuleEncapChannelDelete failed, chId = %d, return %s",
        chipId, portIdx, chId, AtRet2String(atRet));

    ret = drv_pos_save_atHdlcChannel(chipId, portIdx, NULL);
    
    DRV_POS_PRINT("chip[%d] portIdx[%d] success to delete EncapChannel %d", chipId, portIdx, chId);

    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_atPos_EthFlow_vlanSet
* 功能描述：设置以太流的vlan和其他特性
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           portIdx:  端口号，8口范围0~7, 4口范围0~3
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_EthFlow_vlanSet(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtEthFlow atEthFlow = NULL;
    AtEthFlow atEthOamFlow = NULL;
    AtHdlcLink atHdlcLink = NULL;
    tAtZtePtnTag1 vlanTag1;
    tAtZtePtnTag2 vlanTag2;
    eAtRet atRet = cAtOk;
    BYTE ethPortIdx = 0;

    /*check input parameter*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ethPortIdx = (portIdx<=4)?0:1;

    memset(&vlanTag1, 0, sizeof(vlanTag1));
    memset(&vlanTag2, 0, sizeof(vlanTag2));

    /*获取enetFlow*/
    ret = drv_pos_get_atEthFlow(chipId, portIdx, &atEthFlow);
    if(DRV_POS_ETH_FLOW_NOT_CREATE == ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] portIdx[%d] atEthFlow not been create, drv_pos_get_atEthFlow return %d",
            chipId, portIdx, ret);
    }

    /*设置FlowEgress方向属性*/
    atRet = AtEthFlowEgressDestMacSet(atEthFlow, g_drv_pos_boardMac);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtEthFlowEgressDestMacSet failed, return %s",
        chipId, portIdx, AtRet2String(atRet));

    DRV_POS_PRINT("chip[%d] portIdx[%d] success to set egress DestMac", chipId, portIdx);

    /*获取hdlcLink，以便后面添加oam模式*/
    ret = drv_pos_get_atHdlcLink(chipId, portIdx, &atHdlcLink);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atHdlcLink failed, return %d");

    /*添加oam模式*/
    atRet = AtHdlcLinkOamPacketModeSet(atHdlcLink, cAtHdlcLinkOamModeToPsn);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtHdlcLinkOamPacketModeSet failed, return %s",
        chipId, portIdx, AtRet2String(atRet));

    DRV_POS_PRINT("chip[%d] portIdx[%d] success to set OamPacket to cAtHdlcLinkOamModeToPsn mode", chipId, portIdx);

    /*获取OamFlow*/
    atEthOamFlow = AtHdlcLinkOamFlowGet(atHdlcLink);
    DRV_POS_POINTER_CHECK(atEthOamFlow, DRV_POS_AT_ERR, "chip[%d] portIdx[%d] AtHdlcLinkOamFlowGet failed", chipId, portIdx);

    /*设置EthFlowDestMac*/
    atRet = AtEthFlowEgressDestMacSet(atEthOamFlow, g_drv_pos_boardMac);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtEthFlowEgressDestMacSet failed, return %s",
        chipId, portIdx, AtRet2String(atRet));
    
    DRV_POS_PRINT("chip[%d] portIdx[%d] success to set OamFlow Egress Dest Mac", chipId, portIdx);

    /*生成tAtEthVlanDesc*/
    ret = drv_pos_vlan_mapping(chipId, portIdx, &vlanTag1, &vlanTag2);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] AtZteEthFlowExpectedHeaderSet failed, return %s, ethPortIdx = %d", 
        chipId, portIdx, ret);

    /*vlan ingress add*/
    atRet = AtZteEthFlowExpectedHeaderSet(atEthFlow, ethPortIdx, &vlanTag1, &vlanTag2);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtZteEthFlowExpectedHeaderSet failed, return %s, ethPortIdx = %d",
        chipId, portIdx, AtRet2String(atRet), ethPortIdx);
    
    DRV_POS_PRINT("chip[%d] portIdx[%d] success to add ingress vlan", chipId, portIdx);
    
    /*vlan egress add*/
    atRet = AtZteEthFlowTxHeaderSet(atEthFlow, ethPortIdx, &vlanTag1, &vlanTag2);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtZteEthFlowTxHeaderSet failed, return %s, ethPortIdx = %d",
        chipId, portIdx, AtRet2String(atRet), ethPortIdx);

    DRV_POS_PRINT("chip[%d] portIdx[%d] success to add egress vlan", chipId, portIdx);

    atRet = AtZteEthFlowTxHeaderSet(atEthOamFlow, ethPortIdx, &vlanTag1, &vlanTag2);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtZteEthFlowTxHeaderSet failed, return %s, ethPortIdx = %d",
        chipId, portIdx, AtRet2String(atRet), ethPortIdx);

    DRV_POS_PRINT("chip[%d] portIdx[%d] success to add egress vlan", chipId, portIdx);
    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_atPos_EnetFlow_delete
* 功能描述：pos板打通一整条ppplink
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           portIdx:  端口号，8口范围0~7, 4口范围0~3
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_EthFlow_delete(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtModuleEth atEthModule = NULL;
    AtEthFlow   atEthFlow = NULL;
    eAtRet atRet = cAtOk;
    WORD32 flowId = 0;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    
    flowId = (portIdx<4) ? portIdx : portIdx-4+1024; /*flowId用portId映射*/

    /*检查EthFlow是否创建，创建了，才能调用芯片函数删除EthFlow*/
    ret = drv_pos_get_atEthFlow(chipId, portIdx, &atEthFlow);
    if(DRV_POS_ETH_FLOW_NOT_CREATE == ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] portIdx[%d] atEthFlow not been create, drv_pos_get_atEthFlow return %d",
            chipId, portIdx, ret);
    }

    ret = drv_pos_get_atModuleEth(chipId, &atEthModule);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atModuleEth failed, return %d", 
        chipId, portIdx, ret);

    DRV_POS_PRINT("atEthModule = %p, flowId = %d", atEthModule, flowId);

    atRet = AtModuleEthFlowDelete(atEthModule, flowId);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtModuleEthFlowDelete failed, atEthModule = %p, flowId = %d, return %s",
        chipId, portIdx, atEthModule, flowId, AtRet2String(atRet));

    ret = drv_pos_save_atEthFlow(chipId, portIdx, NULL);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_save_atHdlcChannel failed, return %d", chipId, portIdx, ret);

    DRV_POS_PRINT("chip[%d] portIdx[%d] success to delete eth Flow %d", chipId, portIdx, flowId);

    return DRV_POS_OK;
}


/*********************************************************************
* 函数名称：drv_atPos_EnetFlow_create
* 功能描述：pos板打通一整条ppplink
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           portIdx:  端口号，8口范围0~7, 4口范围0~3
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_EthFlow_create(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtModuleEth atEthModule = NULL;
    AtEthFlow atEthFlow = NULL;
    WORD32 flowId = 0;

    /*check input parameter*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    /*检查ethFlow是否创建 channel是否存在，*/
    ret = drv_pos_get_atEthFlow(chipId, portIdx, &atEthFlow);/*如果返回已经创建， 则认为出错*/
    if(DRV_POS_ETH_FLOW_ALREADY_CREATE == ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] portIdx[%d] atEthFlow has already been create, drv_pos_get_atEthFlow return %d",
            chipId, portIdx, ret);
    }

    ret = drv_pos_get_atModuleEth(chipId, &atEthModule);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atModuleEth fail, return %d", chipId, portIdx, ret);

    /*用flowId和EncapModule创建Flow*/
    flowId = (portIdx<4) ? portIdx : portIdx-4+1024; /*flowId用portId映射*/
    atEthFlow = AtModuleEthEopFlowCreate(atEthModule, flowId);
    DRV_POS_POINTER_CHECK(atEthFlow, DRV_POS_AT_ERR, 
        "chip[%d] ethFlow create failed, atEthModule = %p, flowId = %d",
        chipId, atEthModule, flowId);

    ret = drv_pos_save_atEthFlow(chipId, portIdx, atEthFlow);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_save_atEthFlow failed, return %d", chipId, portIdx, ret);

    DRV_POS_PRINT("chip[%d] portIdx[%d] success to create ethernet flow %p", chipId, portIdx, atEthFlow);

    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_atPos_link_disconnect
* 功能描述：pos板打通一整条ppplink
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           portIdx:  端口号，8口范围0~7, 4口范围0~3
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_link_disconnect(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtHdlcChannel atHdlcChannel = NULL;
    AtHdlcLink atHdlcLink = NULL;
    eAtRet atRet = cAtOk;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    /*Get hdlc link, 如果是由于hdlc channel没有创建而导致的没有获取成功，则代表本来就没有connect，直接返回成功*/
    ret = drv_pos_get_atHdlcLink(chipId, portIdx, &atHdlcLink);
    if(DRV_POS_HDLC_CHANNEL_NOT_CREATE == ret)
    {
        return DRV_POS_OK;
    }

    /*disable hdlc Link*/
    atRet = AtHdlcLinkTxTrafficEnable(atHdlcLink, cAtFalse);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtHdlcLinkTxTrafficEnable failed, atHdlcLink = %d, return %s",
        chipId, portIdx, atHdlcLink, AtRet2String(atRet));

    atRet |= AtHdlcLinkRxTrafficEnable(atHdlcLink, cAtFalse);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtHdlcLinkRxTrafficEnable failed, atHdlcLink = %d, return %s",
        chipId, portIdx, atHdlcLink, AtRet2String(atRet));

    atRet |= AtPppLinkPhaseSet((AtPppLink)atHdlcLink, cAtPppLinkPhaseDead);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtPppLinkPhaseSet failed, atHdlcLink = %d, return %s",
        chipId, portIdx, atHdlcLink, AtRet2String(atRet));

    DRV_POS_PRINT("chip[%d] portIdx[%d] link traffic enable and dead phase set success", chipId, portIdx);

    /*Get hdlc channel，如果没有创建过，则代表没有connect过，直接返回成功*/
    ret = drv_pos_get_atHdlcChannel(chipId, portIdx, &atHdlcChannel);
    if(DRV_POS_HDLC_CHANNEL_NOT_CREATE == ret)
    {
        return DRV_POS_OK;
    }

    /*unbind channel*/
    atRet |= AtEncapChannelPhyBind((AtEncapChannel)atHdlcChannel, NULL);
    atRet |= AtHdlcLinkFlowBind(atHdlcLink, NULL);    
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] link unbind phy and flow failed, return %s",
        chipId, portIdx, AtRet2String(atRet));
    DRV_POS_PRINT("chip[%d] portIdx[%d] link unbind phy and flow success", chipId, portIdx);

    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_atPos_link_connect
* 功能描述：pos板打通一整条ppplink
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           portIdx:  端口号，8口范围0~7, 4口范围0~3
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_link_connect(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    eAtRet atRet = cAtOk;
    AtSdhVc atSdhVc = NULL;
    AtChannel atChannel = NULL;
    AtEthFlow atEthFlow = NULL;
    AtHdlcChannel atHdlcChannel = NULL;
    AtHdlcLink atHdlcLink = NULL;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    /*Get Eth Flow*/
    ret = drv_pos_get_atEthFlow(chipId, portIdx, &atEthFlow);
    if(DRV_POS_ETH_FLOW_NOT_CREATE == ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] portIdx[%d] atEthFlow not been create, drv_pos_get_atEthFlow return %d",
            chipId, portIdx, ret);
    }

    /*Get EncapChannel*/
    ret = drv_pos_get_atHdlcChannel(chipId, portIdx, &atHdlcChannel);
    if(DRV_POS_HDLC_CHANNEL_NOT_CREATE == ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] portIdx[%d] atHdlcChannel not been create, drv_pos_get_atHdlcChannel return %d",
            chipId, portIdx, ret);
    }
    
    ret = drv_pos_get_atSdhVC(chipId, portIdx, &atSdhVc);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhVC fail, return %d", chipId, portIdx, ret);

    atChannel = (AtChannel)atSdhVc;
    /* bind link and sdh phy*/
    atRet = AtEncapChannelPhyBind((AtEncapChannel)atHdlcChannel, (AtChannel)atChannel);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtEncapChannelPhyBind failed, encapChannel is %p, atChannel is %p return %s",
        chipId, portIdx, atHdlcChannel, atChannel, AtRet2String(atRet));
    DRV_POS_PRINT("chip[%d] portIdx[%d] success to bind encapChannel and PHY", chipId, portIdx);
    
    /* bind link and ethernet*/
    /* Bring this flow to this link */
    ret = drv_pos_get_atHdlcLink(chipId, portIdx, &atHdlcLink);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atHdlcLink failed, return %d", chipId, portIdx, ret);
    
    atRet = AtHdlcLinkFlowBind(atHdlcLink, atEthFlow);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtHdlcLinkFlowBind failed, atHdlcLink is %p, atEthFlow is %p, return %s",
        chipId, portIdx, atHdlcLink, atEthFlow, AtRet2String(atRet));
    DRV_POS_PRINT("chip[%d] portIdx[%d] success to bind HDLC link and ethernet flow", chipId, portIdx);

    /* Enale traffic */
    atRet |= AtHdlcLinkTxTrafficEnable(atHdlcLink, cAtTrue);
    atRet |=  AtHdlcLinkRxTrafficEnable(atHdlcLink, cAtTrue);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtHdlcLinkTxRxTrafficEnable failed, atHdlcLink is %p, return %s",
        chipId, portIdx, atHdlcLink, AtRet2String(atRet));

    DRV_POS_PRINT("chip[%d] portIdx[%d] success to enable HDLC link traffic", chipId, portIdx);
    
    /* Set ppp link phase */
    atRet = AtPppLinkPhaseSet((AtPppLink)atHdlcLink, cAtPppLinkPhaseNetworkActive);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtPppLinkPhaseSet establish failed, atHdlcLink is %p, return %s",
        chipId, portIdx, atHdlcLink, AtRet2String(atRet));

    DRV_POS_PRINT("chip[%d] portIdx[%d] success to set HDLC link establish phase", chipId, portIdx);

    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_atPos_ppp_delete
* 功能描述：删除pos的ppp
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           portIdx:  端口号，8口范围0~7, 4口范围0~3
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_ppp_delete(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    
    /*check chipId and port number*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    /*connect link*/
    ret = drv_atPos_link_disconnect(chipId, portIdx);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_atPos_link_disconnect failed", chipId, portIdx);

    /*Encap Channel delete 如果本来就没有创建，则也认为成功*/
    ret = drv_atPos_EncapChannel_delete(chipId, portIdx);
    if(DRV_POS_HDLC_CHANNEL_NOT_CREATE == ret)
    {
        ret = DRV_POS_OK;
    }
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_atPos_EncapChannel_delete failed, return %d", chipId, portIdx, ret);

    /*eth Flow delete 如果本来就没有创建，则返回成功*/
    ret = drv_atPos_EthFlow_delete(chipId, portIdx);
    if(DRV_POS_ETH_FLOW_NOT_CREATE == ret)
    {
        ret = DRV_POS_OK;
    }
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_atPos_EthFlow_delete failed, return %d", chipId, portIdx, ret);
    DRV_POS_SUCCESS_PRINT("chip[%d] portIdx[%d] drv_atPos_ppp_delete success", chipId, portIdx);
 
    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_atPos_ppp_create
* 功能描述：pos板打通一整条ppplink
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           portIdx:  端口号，8口范围0~7, 4口范围0~3
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_ppp_create(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    
    /*check chipId and port number*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    /*Encap Channel Create*/
    ret = drv_atPos_EncapChannel_create(chipId, portIdx);
    if(DRV_POS_OK != ret)
    {
        DRV_POS_ERROR_RET(ret, "CHIP[%d] PORTIDX[%d] drv_atPos_EncapChannel_create failed", chipId, portIdx);
    }
    DRV_POS_SUCCESS_PRINT("CHIP[%d] PORTIDX[%d] encapChannel create success", chipId, portIdx);

    /*Flow Create*/
    ret = drv_atPos_EthFlow_create(chipId, portIdx);
    if(DRV_POS_OK != ret)
    {
        drv_atPos_ppp_delete(chipId, portIdx);
        DRV_POS_ERROR_RET(ret, "CHIP[%d] PORTIDX[%d] drv_atPos_EthFlow_create failed", chipId, portIdx);
    }
    DRV_POS_SUCCESS_PRINT("CHIP[%d] PORTIDX[%d] enetFlow create success", chipId, portIdx);

    ret = drv_atPos_EthFlow_vlanSet(chipId, portIdx);
    if(DRV_POS_OK != ret)
    {
        drv_atPos_ppp_delete(chipId, portIdx);
        DRV_POS_ERROR_RET(ret, "CHIP[%d] PORTIDX[%d] drv_atPos_EthFlow_vlanSet failed", chipId, portIdx);
    }
    DRV_POS_SUCCESS_PRINT("CHIP[%d] PORTIDX[%d] enetFlow vlan Set success", chipId, portIdx);
    
    /*connect link*/
    ret = drv_atPos_link_connect(chipId, portIdx);
    if(DRV_POS_OK != ret)
    {
        drv_atPos_ppp_delete(chipId, portIdx);
        DRV_POS_ERROR_RET(ret, "CHIP[%d] PORTIDX[%d] AT ppp link connet failed", chipId, portIdx);
    }
    DRV_POS_SUCCESS_PRINT("CHIP[%d] PORTIDX[%d] link connect success", chipId, portIdx);

    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_atPos_pppPidSet
* 功能描述：设置arrive内部的PID表
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_pppPidSet(BYTE chipId)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtDevice atDevice = NULL;
    eAtRet atRet = cAtOk;
    eAtRet atRetT = cAtOk;
    WORD32 entryId[] = {0,1,2,3,4,15};
    WORD16 pid[] = {DRV_POS_IPV4_PID, DRV_POS_IPV6_PID, DRV_POS_MPLSU_PID, DRV_POS_MPLSM_PID, DRV_POS_ISIS_PID, DRV_POS_PPP_PID};
    WORD16 ethType[] = {DRV_POS_IPV4_ETHTYPE, DRV_POS_IPV6_ETHTYPE, DRV_POS_MPLSU_ETHTYPE, DRV_POS_MPLSM_ETHTYPE, 0, 0};
    BYTE encapType = DRV_POS_PPP_ROUTE_ENCAPTYP;
    BYTE priority = DRV_POS_PPP_PRI_DEFAULT;
    BYTE cfi = DRV_POS_PPP_CFI_DEFAULT;
    BYTE i = 0;
    
    /*check chipId and port number*/
    DRV_POS_CHIPID_CHECK(chipId);
    
    ret = drv_pos_get_atDevice(chipId, &atDevice);
    if(DRV_POS_DEVICE_NOT_CREATE == ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] drv_pos_get_atDevice failed, return %d", chipId, ret);
    }

    for(i = 0; i < sizeof(entryId)/sizeof(WORD32); i++)
    {
        atRet = AtZtePtnPppLookupEntrySet(atDevice, entryId[i], pid[i], ethType[i], encapType);
        atRetT |= atRet;
        DRV_POS_IF_AT_ERROR_LOG(atRet, 
            "chip[%d] AtZtePtnPppLookupEntrySet failed, return %s, i = %d, atDevice = %x, entryId = %d, pid = 0x%x, ethType = 0x%x, encapType = 0x%x",
            chipId, AtRet2String(atRet), i, atDevice, entryId[i], pid[i], ethType[i], encapType);

        atRet = AtZtePtnPppLookupEntryCfiSet(atDevice, entryId[i], cfi);
        atRetT |= atRet;
        DRV_POS_IF_AT_ERROR_LOG(atRet, 
            "chip[%d] AtZtePtnPppLookupEntryCfiSet failed, return %s, i = %d, atDevice = %p, entryId = %d, cfi = 0x%x",
            chipId, AtRet2String(atRet), i, atDevice, entryId[i], cfi);

        atRet = AtZtePtnPppLookupEntryPrioritySet(atDevice, entryId[i], priority);
        atRetT |= atRet;
        DRV_POS_IF_AT_ERROR_LOG(atRet, 
            "chip[%d] AtZtePtnPppLookupEntryPrioritySet failed, return %s, i = %d, atDevice = %p, entryId = %d, priority = 0x%x",
            chipId, AtRet2String(atRet), i, atDevice, entryId[i], priority);

        atRet = AtZtePtnPppLookupEntryEnable(atDevice, entryId[i], cAtTrue);
        atRetT |= atRet;
        DRV_POS_IF_AT_ERROR_LOG(atRet, 
            "chip[%d] AtZtePtnPppLookupEntryEnable failed, return %s, i = %d, atDevice = %p, entryId = %d, enable = %d",
            chipId, AtRet2String(atRet), i, atDevice, entryId[i], cAtTrue);

    }
    DRV_POS_IF_AT_ERROR_RET(atRetT, "chip[%d] ppptable set failed, return %s, atDevice = %p", chipId, AtRet2String(atRetT), atDevice);

    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_atPos_pppModeSet155
* 功能描述：
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/9            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_pppModeSet155(BYTE chipId, DRV_POS_PORT_GROUP portGrp)
{
    DRV_POS_RET ret = DRV_POS_OK;
    BYTE port622Idx = 0;
    BYTE portStartIdx = 0;
    BYTE portEndIdx = 0;
    BYTE portIdx = 0;

    DRV_POS_CHIPID_CHECK(chipId);
    if(DRV_POS_LEFT_PORT_GRP == portGrp)
    {
        port622Idx = 0;
        portStartIdx = 0;
        portEndIdx = 3;
    }
    else if(DRV_POS_RIGHT_PORT_GRP == portGrp)
    {
        port622Idx = 4;
        portStartIdx = 4;
        portEndIdx = 7;
    }
    else
    {
        DRV_POS_ERROR_RET(DRV_POS_PORT_ERR, "chip[%d] portGrp[%d] drv_atPos_pppModeSet155 failed, portGrp not match", chipId, portGrp);
    }

#if 0
    /*删除进程*/
    ret = drv_pos_ppp_thread_delete(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_ppp_thread_delete failed, return %d", chipId, ret);

    ret = drv_pos_sdh_thread_delete(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_sdh_thread_delete failed, return %u", chipId, ret);
#endif
    ret = drv_pos_save_stmMode(chipId, portGrp, DRV_POS_STM_RATE_STM1);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_save_stmMode failed, return %d", chipId, portIdx, ret);

    /*创建端口的pppLink业务，并绑定到stm中*/
    for(portIdx = portStartIdx; portIdx <= portEndIdx; portIdx++)
    {
        if(portIdx == port622Idx)/*622端口，直接绑定*/
        {
            /*解绑定*/
            ret = drv_atPos_link_disconnect(chipId, portIdx);
            DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_atPos_link_disconnect failed, return %d", chipId, portIdx, ret);

            /*设置对应的622端口的默认SDH配置*/
            ret = drv_atPos_sdh_init_cfg(chipId, portIdx);
            DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_atPos_set_sdhModeRate failed, return %d", chipId, portIdx, ret);

            ret = drv_atPos_link_connect(chipId, portIdx);
            DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_atPos_link_connect failed, return %d", chipId, portIdx, ret);

        }
        else/*除622端口外，直接调用函数创建ppplink的业务, 并使能激光器*/
        {
            ret = drv_atPos_ppp_create(chipId, portIdx);
            DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_atPos_ppp_create failed, return %d", chipId, portIdx, ret);

            ret = drv_pos_portTxLaserStateSet(chipId, portIdx, DRV_POS_PORT_TX_LASER_ENABLE);
            DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_portTxLaserStateSet failed, return %d", 
                chipId, portIdx, ret);
        }
    }
#if 0
    /*创建进程*/
    ret = drv_pos_ppp_thread_create(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_ppp_thread_create failed, return %d", chipId, ret);

    ret = drv_pos_sdh_thread_create(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_sdh_thread_create failed, return %u", chipId, ret);
#endif
    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_atPos_pppModeSet622
* 功能描述：
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           portGrp:  需要切换的端口组，左边/右边
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/10            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_atPos_pppModeSet622(BYTE chipId, DRV_POS_PORT_GROUP portGrp)
{
    DRV_POS_RET ret = DRV_POS_OK;
    BYTE port622Idx = 0;
    BYTE portStartIdx = 0;
    BYTE portEndIdx = 0;
    BYTE portIdx = 0;

    DRV_POS_CHIPID_CHECK(chipId);
    if(DRV_POS_LEFT_PORT_GRP == portGrp)
    {
        port622Idx = 0;
        portStartIdx = 0;
        portEndIdx = 3;
    }
    else if(DRV_POS_RIGHT_PORT_GRP == portGrp)
    {
        port622Idx = 4;
        portStartIdx = 4;
        portEndIdx = 7;
    }
    else
    {
        DRV_POS_ERROR_RET(DRV_POS_PORT_ERR, "chip[%d] portGrp[%d] drv_atPos_pppModeSet155 failed, portGrp not match", chipId, portGrp);
    }

#if 0
    /*删除进程*/
    ret = drv_pos_ppp_thread_delete(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_ppp_thread_delete failed, return %d", chipId, ret);

    ret = drv_pos_sdh_thread_delete(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_sdh_thread_delete failed, return %u", chipId, ret);
#endif

    ret = drv_pos_save_stmMode(chipId, portGrp, DRV_POS_STM_RATE_STM4);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_save_stmMode failed, return %d", chipId, portIdx, ret);

    /*创建端口的pppLink业务，并绑定到stm中, 此处要注意，要先解绑定所有的hdlc后，再修改stm速率和模式*/
    for(portIdx = portEndIdx; (portIdx >= portStartIdx) && (portIdx <= portEndIdx); portIdx--)
    {
        if(portIdx == port622Idx)/*622端口，直接绑定*/
        {
            /*解绑定*/
            ret = drv_atPos_link_disconnect(chipId, portIdx);
            DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_atPos_link_disconnect failed, return %d", chipId, portIdx, ret);

            /*设置对应的622端口的默认SDH配置*/
            ret = drv_atPos_sdh_init_cfg(chipId, portIdx);
            DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_atPos_set_sdhModeRate failed, return %d", chipId, portIdx, ret);

            /*绑定*/
            ret = drv_atPos_link_connect(chipId, portIdx);
            DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_atPos_link_connect failed, return %d", chipId, portIdx, ret);
        }
        else/*除622端口外，直接调用函数删除ppplink的业务，并非使能激光器*/
        {
            ret = drv_atPos_ppp_delete(chipId, portIdx);
            DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_atPos_ppp_delete failed, return %d", chipId, portIdx, ret);

            ret = drv_pos_portTxLaserStateSet(chipId, portIdx, DRV_POS_PORT_TX_LASER_DISABLE);
            DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_portTxLaserStateSet failed, return %d", 
                chipId, portIdx, ret);
        }
    }
#if 0
    /*创建进程*/
    ret = drv_pos_ppp_thread_create(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_ppp_thread_create failed, return %d", chipId, ret);

    ret = drv_pos_sdh_thread_create(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_sdh_thread_create failed, return %u", chipId, ret);
#endif
    
    return DRV_POS_OK;
}

/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_pos_overhead.c
* 文件标识: 
* 其它说明: 驱动STM-1 POS 模块的STM-1接口的开销字节实现函数
* 当前版本: V1.0
* 作    者: 刘钰00130907.
* 完成日期: 2013-08-30
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/


#include "drv_pos_overhead.h"
#include "drv_pos_comm.h"
#include "AtSdhPath.h"

#include "bsp_cp3ban_common.h"


/* 全局变量定义 */
/* 监测SDH开销字节的使能标记,默认非使能.在初始化单板时,使能标记;在卸载单板时,非使能标记. */
static WORD32 g_drv_pos_sdh_oh_enable[DRV_POS_MAX_SUBCARD];
static WORD32 g_drv_pos_sdhSecOhEnable[DRV_POS_MAX_SUBCARD][DRV_POS_MAX_PORT]; 
static WORD32 g_drv_pos_sdhVc4OhEnable[DRV_POS_MAX_SUBCARD][DRV_POS_MAX_PORT];

/* SDH overhead线程的threadId */
static int g_drv_pos_sdh_oh_thread_id[DRV_POS_MAX_SUBCARD]; 

/* g_drv_pos_sdh_oh_thread保存SDH overhead线程的信息 */
static struct mod_thread g_drv_pos_sdh_oh_thread[DRV_POS_MAX_SUBCARD]; 

static DRV_POS_STM1_OVERHEAD g_drv_pos_stm1_oh[DRV_POS_MAX_SUBCARD][DRV_POS_MAX_PORT]; /* 存放STM-1帧的开销字节 */


/**************************************************************************
* 函数名称: drv_pos_stm1OhMemGet
* 功能描述: 获取保存STM-1帧的开销的内存
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 0~3;  portId: 1~8
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_stm1OhMemGet(BYTE chipId, BYTE portIdx, DRV_POS_STM1_OVERHEAD** ppOverhead)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(ppOverhead, DRV_POS_PT_ERR, "chip[%d] port[%d] ppOverhead is NULL", chipId, portIdx);

    *ppOverhead = &(g_drv_pos_stm1_oh[chipId][portIdx]);
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_stm1OverheadSave
* 功能描述: 保存STM-1帧的开销字节.
* 访问的表: 软件表g_drv_pos_stm1_oh.
* 修改的表: 软件表g_drv_pos_stm1_oh.
* 输入参数: chipId: 0~3;  portId: 1~8
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_stm1OverheadSave(BYTE chipId, BYTE portIdx, const DRV_POS_STM1_OVERHEAD *pOverhead)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pOverhead, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pOverhead is NULL", chipId, portIdx);
    
    memset(&(g_drv_pos_stm1_oh[chipId][portIdx]), 0, sizeof(DRV_POS_STM1_OVERHEAD));
    memcpy(&(g_drv_pos_stm1_oh[chipId][portIdx]), pOverhead, sizeof(DRV_POS_STM1_OVERHEAD));
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_traceMsgStrLenGet
* 功能描述: 通过J字节的跟踪标识方式来获取J字节的最大长度
* 访问的表: 无
* 修改的表: 无
* 输入参数: 无 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_traceMsgStrLenGet(WORD32 msgMode, WORD32 *length)
{    
    DRV_POS_POINTER_CHECK(length, DRV_POS_PT_ERR, "length is NULL");

    *length = 1;
    if (DRV_POS_TRACE_MSG_1BYTE == msgMode)
    {
        *length = DRV_POS_TRACE_MSG_LEN_1;
    }
    else if (DRV_POS_TRACE_MSG_16BYTE == msgMode)
    {
        *length = DRV_POS_TRACE_MSG_LEN_16;
    }
    else if (DRV_POS_TRACE_MSG_64BYTE == msgMode)
    {
        *length = DRV_POS_TRACE_MSG_LEN_64;
    }
    else
    {
        *length = 1;
        DRV_POS_ERROR_RET(DRV_POS_ERROR, "traceMsg select unsupported mode");
    }
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_txJ0Get
* 功能描述: 获取发送的J0的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: msgBuf:保存J0的值,需要64字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_txJ0Get(BYTE chipId, BYTE portIdx, BYTE *msgBuf)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    AtSdhLine atSdhLine = NULL;
    tAtSdhTti tti;
    eAtRet atRet = cAtOk;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(msgBuf, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] msgBuf is NULL", chipId, portIdx);

    memset(&tti, 0, sizeof(tAtSdhTti));
    memset(msgBuf, 0, cAtSdhChannelMaxTtiLength);

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d",
        chipId, portIdx, ret);

    /* Get transmitted J0 */
    atRet = AtSdhChannelTxTtiGet((AtSdhChannel)atSdhLine, &tti);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] j0 AtSdhChannelTxTtiGet failed, return %s, atSdhLine = %p",
        chipId, portIdx, AtRet2String(atRet), atSdhLine);
    
    memset(msgBuf, 0, DRV_POS_TRACE_MSG_MAX_LEN);
    strncpy((CHAR*)msgBuf, (CHAR*)tti.message, DRV_POS_TRACE_MSG_MAX_LEN);
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_txJ0Set
* 功能描述: 设置发送的J0的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_txJ0Set(BYTE chipId, BYTE portIdx, WORD32 traceMode, const BYTE *msgBuf)
{
    DRV_POS_RET rv = DRV_POS_OK;  /* 函数返回码 */
    WORD32 length = 1;
    AtSdhLine atSdhLine = NULL;
    BYTE txJ0Msg[DRV_POS_TRACE_MSG_MAX_LEN] = {0}; /* 实际需要设置的J0 */
    eAtSdhTtiMode ttiMode = 0;
    tAtSdhTti tti;
    eAtRet atRet = cAtOk;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(msgBuf, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] msgBuf is NULL");
    
    memset(&tti, 0, sizeof(tAtSdhTti));
    
    BSP_Print(BSP_DEBUG_ALL, "chipId[%d] portIdx[%d] tx J0 set traceMode = %d, msgBuf = %s\n", chipId, portIdx, traceMode, msgBuf);

    if (DRV_POS_TRACE_MSG_1BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode1Byte;
    }
    else if (DRV_POS_TRACE_MSG_16BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode16Byte;
    }
    else if (DRV_POS_TRACE_MSG_64BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode64Byte;
    }
    else
    {
        DRV_POS_ERROR_RET(DRV_POS_PARAM_ERR, "chip[%d] portIdx[%d] traceMode error, traceMode = %d", 
                chipId, portIdx, traceMode);
    }

    rv = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(rv, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d",
        chipId, portIdx, rv);
    
    /* 通过J0字节的跟踪标识方式来获取J0字节的最大长度 */
    rv = drv_pos_traceMsgStrLenGet(traceMode, &length); 
    DRV_POS_IF_ERROR_RET(rv, "chip[%d] portIdx[%d] drv_pos_traceMsgStrLenGet failed, return %d",
        chipId, portIdx, rv);
    
    memcpy(txJ0Msg, msgBuf, length);
    
    /* configure transmitted TTI */
    AtSdhTtiMake(ttiMode, txJ0Msg, strlen((char *)txJ0Msg), &tti);
    
    BSP_Print(BSP_DEBUG_ALL, "chipId[%d] portIdx[%d] AtSdhTtiMake mode = %d, message = %s, txJ0Msg = %s, strlen(txJ0Msg) = %d\n", 
        chipId, portIdx, tti.mode, tti.message, txJ0Msg, strlen((char*)txJ0Msg));
    atRet = AtSdhChannelTxTtiSet((AtSdhChannel)atSdhLine, &tti);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] AtSdhChannelTxTtiSet failed, return %s, atSdhLine = %p, tti mode = %d, tti msg = %s",
        chipId, portIdx, AtRet2String(atRet), atSdhLine, tti.mode, tti.message);
    
    return DRV_POS_OK;
}



/**************************************************************************
* 函数名称: drv_pos_expectedRxJ0Get
* 功能描述: 获取期望接收的J0的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: msgBuf:保存J0的值,需要64字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_expectedRxJ0Get(BYTE chipId, BYTE portIdx, BYTE *msgBuf)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    AtSdhLine atSdhLine = NULL;
    tAtSdhTti tti;
    eAtRet atRet = cAtOk;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(msgBuf, DRV_POS_PT_ERR, "chip[%d] port[%d] msgBuf is NULL", chipId, portIdx);
    
    memset(&tti, 0, sizeof(tAtSdhTti));

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine return %d",
        chipId, portIdx, ret);
    
    /* Get expected RX J0. */
    atRet = AtSdhChannelExpectedTtiGet((AtSdhChannel)atSdhLine, &tti);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] port[%d] AtSdhChannelExpectedTtiGet failed, return %s, atSdhLine = %p",
        chipId, portIdx, AtRet2String(atRet), atSdhLine);

    memset(msgBuf, 0, DRV_POS_TRACE_MSG_MAX_LEN);
    strncpy((CHAR*)msgBuf, (CHAR*)tti.message, DRV_POS_TRACE_MSG_MAX_LEN);
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_expectedRxJ0Set
* 功能描述: 设置期望接收的J0的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_expectedRxJ0Set(BYTE chipId, BYTE portIdx, WORD32 traceMode,const BYTE *msgBuf)
{
    DRV_POS_RET rv = DRV_POS_OK;  /* 函数返回码 */
    WORD32 length = 1;
    AtSdhLine atSdhLine = NULL;
    BYTE expectedRxJ0Buf[DRV_POS_TRACE_MSG_MAX_LEN] = {0}; /* 实际需要设置的J0 */
    eAtSdhTtiMode ttiMode = 0;
    tAtSdhTti tti;
    eAtRet atRet = cAtOk;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(msgBuf, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] msgBuf is NULL");

    BSP_Print(BSP_DEBUG_ALL, "chipId[%d] portIdx[%d] rx expect J0 set traceMode = %d, msgBuf = %s\n", 
        chipId, portIdx, traceMode, msgBuf);

    memset(&tti, 0, sizeof(tAtSdhTti));

    if (DRV_POS_TRACE_MSG_1BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode1Byte;
    }
    else if (DRV_POS_TRACE_MSG_16BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode16Byte;
    }
    else if (DRV_POS_TRACE_MSG_64BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode64Byte;
    }
    else
    {            
        DRV_POS_ERROR_RET(DRV_POS_PARAM_ERR, "chip[%d] portIdx[%d] traceMode error, traceMode = %d", 
                chipId, portIdx, traceMode);
    }

    rv = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(rv, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d",
        chipId, portIdx, rv);
    
    /* 通过J0字节的跟踪标识方式来获取J0字节的最大长度 */
    rv = drv_pos_traceMsgStrLenGet(traceMode, &length); 
    DRV_POS_IF_ERROR_RET(rv, "chip[%d] portIdx[%d] drv_pos_traceMsgStrLenGet failed, return %d",
        chipId, portIdx, rv);
    
    memcpy(expectedRxJ0Buf, msgBuf, length);
    
    /* configure expected RX TTI */
    AtSdhTtiMake(ttiMode, expectedRxJ0Buf, strlen((char *)expectedRxJ0Buf), &tti);
    atRet = AtSdhChannelExpectedTtiSet((AtSdhChannel)atSdhLine, &tti);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] AtSdhChannelExpectedTtiSet failed, return %s, atSdhLine = %p, tti mode = %d, tti msg = %s",
        chipId, portIdx, AtRet2String(atRet), atSdhLine, tti.mode, tti.message);

    /* enable RS-TIM monitoring */
    rv = AtSdhChannelTimMonitorEnable((AtSdhChannel)atSdhLine, cAtTrue);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] AtSdhChannelTimMonitorEnable failed, return %s, atSdhLine = %p",
        chipId, portIdx, AtRet2String(atRet), atSdhLine);
  
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_rxJ0Get
* 功能描述: 获取实际接收的J0的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: msgBuf:保存J0的值,需要64字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_rxJ0Get(BYTE chipId, BYTE portIdx, BYTE *msgBuf)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    AtSdhLine atSdhLine = NULL;
    tAtSdhTti tti;
    eAtRet atRet = cAtOk;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(msgBuf, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] msgBuf is NULL", chipId, portIdx);
    
    memset(&tti, 0, sizeof(tAtSdhTti));

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d",
        chipId, portIdx, ret);
    
    /* Get received J0 */
    atRet = AtSdhChannelRxTtiGet((AtSdhChannel)atSdhLine, &tti);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] AtSdhChannelRxTtiGet failed, return %s, atSdhLine = %p",
        chipId, portIdx, AtRet2String(atRet), atSdhLine);

    memset(msgBuf, 0, cAtSdhChannelMaxTtiLength);
    strncpy((CHAR*)msgBuf, (CHAR*)tti.message, cAtSdhChannelMaxTtiLength);
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_txS1Set
* 功能描述: 设置发送的S1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_txS1Set(BYTE chipId, BYTE portIdx, BYTE s1Value)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    AtSdhLine atSdhLine = NULL;
    eAtRet atRet = cAtOk;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d",
        chipId, portIdx, ret);
    
    /* Set transmitted S1 */
    atRet = AtSdhLineTxS1Set(atSdhLine, s1Value);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] AtSdhLineTxS1Set failed, return %s, atSdhLine = %p, s1Value = 0x%x", 
        chipId, portIdx, AtRet2String(atRet), atSdhLine, s1Value);
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_txS1Get
* 功能描述: 获取发送的S1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: pS1Value:保存S1的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_txS1Get(BYTE chipId, BYTE portIdx, BYTE *pS1Value)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    AtSdhLine atSdhLine = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d",
        chipId, portIdx, ret);

    /* 先清零再赋值 */
    *pS1Value = 0;
    *pS1Value = AtSdhLineTxS1Get((AtSdhLine)atSdhLine);  /* Get transmitted S1. */
    
    return ret;
}


/**************************************************************************
* 函数名称: drv_pos_rxS1Get
* 功能描述: 获取实际接收到的S1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: pS1Value:保存S1的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_rxS1Get(BYTE chipId, BYTE portIdx, BYTE *pS1Value)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    AtSdhLine atSdhLine = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pS1Value, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pS1Value is NULL", chipId, portIdx);

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d",
        chipId, portIdx, ret);
    
    /* 先清零再赋值 */
    *pS1Value = 0;
    *pS1Value = AtSdhLineRxS1Get(atSdhLine);  /* Get received S1 */
    
    return ret;
}


/**************************************************************************
* 函数名称: drv_pos_txK1Set
* 功能描述: 设置发送的K1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_txK1Set(BYTE chipId, BYTE portIdx, BYTE k1Value)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    AtSdhLine atSdhLine = NULL;
    eAtRet atRet = cAtOk;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    
    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d",
        chipId, portIdx, ret);
    
    /* Set transmitted K1 */
    atRet = AtSdhLineTxK1Set(atSdhLine, k1Value);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] AtSdhLineTxK1Set failed, return %s, atSdhLine = %p, k1Value = 0x%x", 
        chipId, portIdx, AtRet2String(atRet), atSdhLine, k1Value);
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_txK1Get
* 功能描述: 获取发送的K1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: pK1Value:保存K1的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_txK1Get(BYTE chipId, BYTE portIdx, BYTE *pK1Value)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    AtSdhLine atSdhLine = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pK1Value, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pK1Value is NULL", chipId, portIdx);

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d",
        chipId, portIdx, ret);

    /* 先清零再赋值 */
    *pK1Value = 0;
    *pK1Value = AtSdhLineTxK1Get((AtSdhLine)atSdhLine); /* Get transmitted K1. */
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_rxK1Get
* 功能描述: 获取实际接收到的K1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: pK1Value:保存K1的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_rxK1Get(BYTE chipId, BYTE portIdx, BYTE *pK1Value)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    AtSdhLine atSdhLine = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pK1Value, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pK1Value is NULL", chipId, portIdx);

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d",
        chipId, portIdx, ret);
    
    /* 先清零再赋值 */
    *pK1Value = 0;
    *pK1Value = AtSdhLineRxK1Get(atSdhLine);  /* Get received K1 */
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_txK2Set
* 功能描述: 设置发送的K2的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_txK2Set(BYTE chipId, BYTE portIdx, BYTE k2Value)
{
    DRV_POS_RET ret = DRV_POS_OK;
    eAtRet atRet = cAtOk;  /* 函数返回码 */
    AtSdhLine atSdhLine = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
     
    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d",
            chipId, portIdx, ret);
    
    /* Set transmitted K2 */
    atRet = AtSdhLineTxK2Set(atSdhLine, k2Value);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] AtSdhLineTxK2Set failed, return %s, atSdhLine = %p, k2Value = 0x%x",
        chipId, portIdx, AtRet2String(atRet), atSdhLine, k2Value);
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_txK2Get
* 功能描述: 获取发送的K2的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: pK2Value:保存K2的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_txK2Get(BYTE chipId, BYTE portIdx, BYTE *pK2Value)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtSdhLine atSdhLine = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pK2Value, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pK2Value is NULL", chipId, portIdx);

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d",
            chipId, portIdx, ret);

    /* 先清零再赋值 */
    *pK2Value = 0;
    *pK2Value = AtSdhLineTxK2Get((AtSdhLine)atSdhLine);  /* Get transmitted K2. */
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_rxK2Get
* 功能描述: 获取实际接收到的K2的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: pK2Value:保存K2的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_rxK2Get(BYTE chipId, BYTE portIdx, BYTE *pK2Value)
{
    AtSdhLine atSdhLine = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pK2Value, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pK2Value is NULL", chipId, portIdx);
    
    /* 先清零再赋值 */
    *pK2Value = 0;
    *pK2Value = AtSdhLineRxK2Get(atSdhLine);  /* Get received K2 */
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_sectionOhGet
* 功能描述: 获取section Overhead
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-1-23   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sectionOhGet(BYTE chipId, BYTE portIdx, DRV_POS_SECTION_OH *pSectionOh)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtModuleSdh atModuleSdh = NULL;
    eAtRet atRet = cAtOk;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pSectionOh, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pSectionOh is NULL", chipId, portIdx);


    /*获取sdh module ，以做信号量保护*/
    ret = drv_pos_get_atModuleSdh(chipId, &atModuleSdh);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_atModuleSdh failed, return %d", chipId, ret);

    atRet = AtModuleLock((AtModule)atModuleSdh);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtModuleLock failed, return %s",
        chipId, portIdx, AtRet2String(atRet));

    ret |= drv_pos_txJ0Get(chipId, portIdx, pSectionOh->txJ0MsgBuf);
    ret |= drv_pos_rxJ0Get(chipId, portIdx, pSectionOh->rxJ0MsgBuf);
    ret |= drv_pos_expectedRxJ0Get(chipId, portIdx, pSectionOh->expectedRxJ0MsgBuf);

    ret |= drv_pos_rxK1Get(chipId, portIdx, &(pSectionOh->rxK1Value));
    ret |= drv_pos_txK1Get(chipId, portIdx, &(pSectionOh->txK1Value));

    ret |= drv_pos_rxK2Get(chipId, portIdx, &(pSectionOh->rxK2Value));
    ret |= drv_pos_txK2Get(chipId, portIdx, &(pSectionOh->txK2Value));
    
    ret |= drv_pos_rxS1Get(chipId, portIdx, &(pSectionOh->rxS1Value));
    ret |= drv_pos_txS1Get(chipId, portIdx, &(pSectionOh->txS1Value));

    atRet = AtModuleUnLock((AtModule)atModuleSdh);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtModuleUnLock failed, return %s",
        chipId, portIdx, AtRet2String(atRet));

    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] section oh get failed, return %d", chipId, portIdx, ret);
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_txJ1Set
* 功能描述: 设置发送的J1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_txJ1Set(BYTE chipId, BYTE portIdx, WORD32 traceMode, const BYTE *msgBuf)
{
    DRV_POS_RET rv = DRV_POS_OK;  /* 函数返回码 */
    AtSdhVc atSdhVc4 = NULL;
    WORD32 length = 1;
    BYTE txJ1Msg[DRV_POS_TRACE_MSG_MAX_LEN] = {0}; /* 实际需要设置的J1 */
    eAtSdhTtiMode ttiMode = 0;
    tAtSdhTti tti;
    eAtRet atRet = cAtOk;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(msgBuf, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] msgBuf is NULL");

    BSP_Print(BSP_DEBUG_ALL, "chipId[%d] portIdx[%d] tx J1 set traceMode = %d, msgBuf = %s\n", chipId, portIdx, traceMode, msgBuf);

    memset(&tti, 0, sizeof(tAtSdhTti));

    if (DRV_POS_TRACE_MSG_1BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode1Byte;
    }
    else if (DRV_POS_TRACE_MSG_16BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode16Byte;
    }
    else if (DRV_POS_TRACE_MSG_64BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode64Byte;
    }
    else
    {
        DRV_POS_ERROR_RET(DRV_POS_PARAM_ERR, "chip[%d] portIdx[%d] traceMode error, traceMode = %d", 
            chipId, portIdx, traceMode);
    }
    rv = drv_pos_get_atSdhVC(chipId, portIdx, &atSdhVc4);
    DRV_POS_IF_ERROR_RET(rv, "chip[%d] portIdx[%d] drv_pos_get_atSdhVC failed, return %d",
        chipId, portIdx, rv);
    
    /* 通过J0字节的跟踪标识方式来获取J0字节的最大长度 */
    rv = drv_pos_traceMsgStrLenGet(traceMode, &length); 
    DRV_POS_IF_ERROR_RET(rv, "chip[%d] portIdx[%d] drv_pos_traceMsgStrLenGet failed, return %d",
        chipId, portIdx, rv);
    
    memcpy(txJ1Msg, msgBuf, length);
    
    AtSdhTtiMake(ttiMode, txJ1Msg, strlen((char *)txJ1Msg), &tti);
    atRet = AtSdhChannelTxTtiSet((AtSdhChannel)atSdhVc4, &tti);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] AtSdhChannelTxTtiSet failed, return %s, atSdhVc4 = %p, tti mode = %d, tti msg = %s",
        chipId, portIdx, AtRet2String(atRet), atSdhVc4, tti.mode, tti.message);
  
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_txJ1Get
* 功能描述: 获取发送的J1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: msgBuf:保存J1的值,需要64字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_txJ1Get(BYTE chipId, BYTE portIdx, BYTE au4Id, BYTE *msgBuf)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    BYTE aug1Index = 0;  /* ARRIVE芯片的AUG-1索引号,从0开始编号 */
    AtSdhVc atSdhVc4 = NULL;
    tAtSdhTti tti;
    eAtRet atRet = cAtOk;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_AU4_CHECK(au4Id);
    DRV_POS_POINTER_CHECK(msgBuf, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] input msgBuf is NULL", chipId, portIdx);
        
    memset(&tti, 0, sizeof(tAtSdhTti));
    aug1Index = au4Id - (BYTE)1;

    ret = drv_pos_get_atSdhVC(chipId, portIdx, &atSdhVc4);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhVC failed", chipId, portIdx);
    
    /* Get transmitted J1. */
    atRet = AtSdhChannelTxTtiGet((AtSdhChannel)atSdhVc4, &tti);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] AtSdhChannelTxTtiGet failed, return %s, atSdhVc4 = %p", 
        chipId, portIdx, AtRet2String(atRet), atSdhVc4);
    
    memset(msgBuf, 0, cAtSdhChannelMaxTtiLength);
    memcpy(msgBuf, tti.message, cAtSdhChannelMaxTtiLength);
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_expectedRxJ1Set
* 功能描述: 设置期望接收的J1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_expectedRxJ1Set(BYTE chipId, BYTE portIdx, WORD32 traceMode, const BYTE *msgBuf)
{
    DRV_POS_RET rv = DRV_POS_OK;  /* 函数返回码 */
    WORD32 length = 1;
    AtSdhVc atSdhVc4 = NULL;
    BYTE expectedRxJ1Buf[DRV_POS_TRACE_MSG_MAX_LEN] = {0}; /* 实际需要设置的J1 */
    eAtSdhTtiMode ttiMode = 0;
    tAtSdhTti tti;
    eAtRet atRet = cAtOk;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(msgBuf, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] msgBuf is NULL");

    BSP_Print(BSP_DEBUG_ALL, "chipId[%d] portIdx[%d] rx expect J1 set traceMode = %d, msgBuf = %s\n", chipId, portIdx, traceMode, msgBuf);

    memset(&tti, 0, sizeof(tAtSdhTti));

    if (DRV_POS_TRACE_MSG_1BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode1Byte;
    }
    else if (DRV_POS_TRACE_MSG_16BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode16Byte;
    }
    else if (DRV_POS_TRACE_MSG_64BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode64Byte;
    }
    else
    {
        DRV_POS_ERROR_RET(DRV_POS_PARAM_ERR, "chip[%d] portIdx[%d] traceMode error, traceMode = %d", 
            chipId, portIdx, traceMode);
    }

    rv = drv_pos_get_atSdhVC(chipId, portIdx, &atSdhVc4);
    DRV_POS_IF_ERROR_RET(rv, "chip[%d] portIdx[%d] drv_pos_get_atSdhVC failed, return %d",
        chipId, portIdx, rv);
    
    /* 通过J0字节的跟踪标识方式来获取J0字节的最大长度 */
    rv = drv_pos_traceMsgStrLenGet(traceMode, &length); 
    DRV_POS_IF_ERROR_RET(rv, "chip[%d] portIdx[%d] drv_pos_traceMsgStrLenGet failed, return %d",
        chipId, portIdx, rv);
    
    memcpy(expectedRxJ1Buf, msgBuf, length);
    
    /* configure expected RX TTI */
    AtSdhTtiMake(ttiMode, expectedRxJ1Buf, strlen((char *)expectedRxJ1Buf), &tti);
    atRet = AtSdhChannelExpectedTtiSet((AtSdhChannel)atSdhVc4, &tti);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] AtSdhChannelExpectedTtiSet failed, return %s, atSdhVc4 = %p, tti mode = %d, tti msg = %s",
        chipId, portIdx, AtRet2String(atRet), atSdhVc4, tti.mode, tti.message);

    /* enable VC4-TIM. */
    atRet = AtSdhChannelTimMonitorEnable((AtSdhChannel)atSdhVc4, cAtTrue);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] AtSdhChannelTimMonitorEnable failed, return %s, atSdhVc4 = %p",
        chipId, portIdx, AtRet2String(atRet), atSdhVc4);
  
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_expectedRxJ1Get
* 功能描述: 获取期望接收的J1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: msgBuf:保存J1的值,需要64字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_expectedRxJ1Get(BYTE chipId, BYTE portIdx, BYTE au4Id, BYTE *msgBuf)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    BYTE aug1Index = 0;  /* ARRIVE芯片的AUG-1索引号,从0开始编号 */
    AtSdhVc atSdhVc4 = NULL;
    tAtSdhTti tti;
    eAtRet atRet = cAtOk;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_AU4_CHECK(au4Id);
    DRV_POS_POINTER_CHECK(msgBuf, DRV_POS_PT_ERR, "chip[%d] port[%d] input msgBuf is NULL", chipId, portIdx);
    
    memset(&tti, 0, sizeof(tAtSdhTti));
    aug1Index = au4Id - (BYTE)1;

    ret = drv_pos_get_atSdhVC(chipId, portIdx, &atSdhVc4);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhVC failed, return %d",
        chipId, portIdx, ret);
        
    /* Get expected RX J1. */
    atRet = AtSdhChannelExpectedTtiGet((AtSdhChannel)atSdhVc4, &tti);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] AtSdhChannelExpectedTtiGet failed, return %s, atSdhVc4 = %p", 
        chipId, portIdx, AtRet2String(atRet), atSdhVc4);
    
    memset(msgBuf, 0, cAtSdhChannelMaxTtiLength);
    memcpy(msgBuf, tti.message, cAtSdhChannelMaxTtiLength);
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_rxJ1Get
* 功能描述: 获取实际接收到的J1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: msgBuf:保存J1的值,需要64字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_rxJ1Get(BYTE chipId, BYTE portIdx, BYTE au4Id, BYTE *msgBuf)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    BYTE aug1Index = 0;  /* ARRIVE芯片的AUG-1索引号,从0开始编号 */
    AtSdhVc atSdhVc4 = NULL;
    tAtSdhTti tti;
    eAtRet atRet = cAtOk;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_AU4_CHECK(au4Id);
    DRV_POS_POINTER_CHECK(msgBuf, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] input msgBuf is NULL", chipId, portIdx);
    
    memset(&tti, 0, sizeof(tAtSdhTti));
    aug1Index = au4Id - (BYTE)1;

    ret = drv_pos_get_atSdhVC(chipId, portIdx, &atSdhVc4);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhVC failed, return %d",
        chipId, portIdx, ret);
        
    /* Get received J1 */
    atRet = AtSdhChannelRxTtiGet((AtSdhChannel)atSdhVc4, &tti);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] AtSdhChannelRxTtiGet failed, return %s, atSdhVc4 %p", 
        chipId, portIdx, AtRet2String(atRet), atSdhVc4);
    
    memset(msgBuf, 0, cAtSdhChannelMaxTtiLength);
    strncpy((CHAR*)msgBuf, (CHAR*)tti.message, cAtSdhChannelMaxTtiLength);
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_txC2Set
* 功能描述: 设置发送的C2的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_txC2Set(BYTE chipId, BYTE portIdx, BYTE c2Value)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    AtSdhVc atSdhVc4 = NULL;
    eAtRet atRet = cAtOk;
    BYTE tmpC2Value = 0;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_get_atSdhVC(chipId, portIdx, &atSdhVc4);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhVC failed, return %d",
        chipId, portIdx, ret);
        
    /* Get transmitted C2 */
    tmpC2Value = AtSdhPathTxPslGet((AtSdhPath)atSdhVc4);
    
    /* 当需要设置的C2和从芯片中读取的C2不一致时,才写芯片,否则不写芯片 */
    if (c2Value != tmpC2Value)
    {
        atRet = AtSdhPathTxPslSet((AtSdhPath)atSdhVc4, c2Value);
        DRV_POS_IF_AT_ERROR_RET(atRet, 
            "chip[%d] portIdx[%d] AtSdhPathTxPslSet failed, return %s, atSdhVc4 = %p, c2Value = 0x%x", 
            chipId, portIdx, AtRet2String(atRet),atSdhVc4, c2Value);
    }
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_txC2Get
* 功能描述: 获取发送的C2的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: pC2Value:用来保存C2的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_txC2Get(BYTE chipId, BYTE portIdx, BYTE au4Id, BYTE *pC2Value)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    BYTE aug1Index = 0;  /* ARRIVE芯片的AUG-1索引号,从0开始编号 */
    AtSdhVc atSdhVc4 = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_AU4_CHECK(au4Id);
    DRV_POS_POINTER_CHECK(pC2Value, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] input pC2Value is NULL", chipId, portIdx);
    
    aug1Index = au4Id - (BYTE)1;
    

    ret = drv_pos_get_atSdhVC(chipId, portIdx, &atSdhVc4);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhVC failed, return %d",
        chipId, portIdx, ret);
   
    /* 先清零再赋值 */
    *pC2Value = 0;
    *pC2Value = AtSdhPathTxPslGet((AtSdhPath)atSdhVc4);  /* Get transmitted C2. */
    
    return ret;
}


/**************************************************************************
* 函数名称: drv_pos_expectedRxC2Set
* 功能描述: 设置期望接收的C2的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_expectedRxC2Set(BYTE chipId, BYTE portIdx, BYTE c2Value)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    BYTE aug1Index = 0;  /* ARRIVE芯片的AUG-1索引号,从0开始编号 */
    AtSdhVc atSdhVc4 = NULL;
    eAtRet atRet = cAtOk;
    BYTE tmpC2Value = 0;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    
    ret = drv_pos_get_atSdhVC(chipId, portIdx, &atSdhVc4);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhVC failed, return %d",
        chipId, portIdx, ret);
    
    /* Get expected RX C2 */
    tmpC2Value = AtSdhPathExpectedPslGet((AtSdhPath)atSdhVc4);
    
    /* 当需要设置的C2和从芯片中读取的C2不一致时,才写芯片,否则不写芯片 */
    if (c2Value != tmpC2Value)
    {
        atRet = AtSdhPathExpectedPslSet((AtSdhPath)atSdhVc4, c2Value);
        DRV_POS_IF_AT_ERROR_RET(atRet, 
            "chip[%d] portIdx[%d] AtSdhPathExpectedPslSet failed, return %s, atSdhVc4 = %p, c2Value = 0x%x", 
            chipId, portIdx, AtRet2String(atRet), atSdhVc4, c2Value);
    }
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_expectedRxC2Get
* 功能描述: 获取期望接收的C2的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: pC2Value:用来保存C2的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_expectedRxC2Get(BYTE chipId, BYTE portIdx, BYTE au4Id, BYTE *pC2Value)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    BYTE aug1Index = 0;  /* ARRIVE芯片的AUG-1索引号,从0开始编号 */
    AtSdhVc atSdhVc4 = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_AU4_CHECK(au4Id);
    DRV_POS_POINTER_CHECK(pC2Value, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] input pC2Value is NULL", chipId, portIdx);
    
    aug1Index = au4Id - (BYTE)1;
    
    ret = drv_pos_get_atSdhVC(chipId, portIdx, &atSdhVc4);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhVC failed, return %d",
        chipId, portIdx, ret);
    
    /* 先清零再赋值 */
    *pC2Value = 0;
    *pC2Value = AtSdhPathExpectedPslGet((AtSdhPath)atSdhVc4);  /* Get expected RX C2 */
    
    return ret;
}


/**************************************************************************
* 函数名称: drv_pos_rxC2Get
* 功能描述: 获取实际接收的C2的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: pC2Value:用来保存C2的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_rxC2Get(BYTE chipId, BYTE portIdx, BYTE au4Id, BYTE *pC2Value)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    AtSdhVc atSdhVc4 = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_AU4_CHECK(au4Id);
    DRV_POS_POINTER_CHECK(pC2Value, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] input pC2Value is NULL", chipId, portIdx);
    
    ret = drv_pos_get_atSdhVC(chipId, portIdx, &atSdhVc4);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhVC failed, return %d",
        chipId, portIdx, ret);

    /* 先清零再赋值 */
    *pC2Value = 0;
    *pC2Value = AtSdhPathRxPslGet((AtSdhPath)atSdhVc4); /* Get received C2. */
    
    return ret;
}

/**************************************************************************
* 函数名称: drv_pos_highPathOhGet
* 功能描述: 获取高阶 Overhead
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-1-23   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_highPathOhGet(BYTE chipId, BYTE portIdx, DRV_POS_VC4_OH *pHpOh)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtModuleSdh atModuleSdh = NULL;
    eAtRet atRet = cAtOk;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pHpOh, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pHpOh is NULL", chipId, portIdx);

    
    /*获取sdh module ，以做信号量保护*/
    ret = drv_pos_get_atModuleSdh(chipId, &atModuleSdh);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_atModuleSdh failed, return %d", chipId, ret);
    
    atRet = AtModuleLock((AtModule)atModuleSdh);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtModuleLock failed, return %s",
        chipId, portIdx, AtRet2String(atRet));

    ret |= drv_pos_txJ1Get(chipId, portIdx, 1, pHpOh->txJ1MsgBuf);
    ret |= drv_pos_rxJ1Get(chipId, portIdx, 1, pHpOh->rxJ1MsgBuf);
    ret |= drv_pos_expectedRxJ1Get(chipId, portIdx, 1, pHpOh->expectedRxJ1MsgBuf);

    ret |= drv_pos_txC2Get(chipId, portIdx, 1, &(pHpOh->txC2Value));
    ret |= drv_pos_rxC2Get(chipId, portIdx, 1, &(pHpOh->rxC2Value));
    ret |= drv_pos_expectedRxC2Get(chipId, portIdx, 1, &(pHpOh->expectedRxC2Value));
    
    atRet = AtModuleUnLock((AtModule)atModuleSdh);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtModuleUnLock failed, return %s",
        chipId, portIdx, AtRet2String(atRet));

    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] hightPath oh get failed, return %d", chipId, portIdx, ret);
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_txK1Query
* 功能描述: 查询发送的K1.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: *pValue:保存K1的值
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_txK1Query(BYTE chipId, BYTE portIdx, BYTE *pValue)
{
    DRV_POS_RET ret = DRV_POS_OK;
    DRV_POS_STM1_OVERHEAD *pOverhead = NULL;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pValue, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pValue is NULL", chipId, portIdx);
    
    ret = drv_pos_stm1OhMemGet(chipId, portIdx, &pOverhead);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_stm1OhMemGet failed", chipId, portIdx);

    *pValue = 0;  /* 先清零再赋值 */
    *pValue = pOverhead->sectionOh.txK1Value;
    
    return ret;
}


/**************************************************************************
* 函数名称: drv_pos_rxK1Query
* 功能描述: 查询接收的K1.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: *pValue:保存K1的值
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_rxK1Query(BYTE chipId, BYTE portIdx, BYTE *pValue)
{
    DRV_POS_RET ret = DRV_POS_OK;
    DRV_POS_STM1_OVERHEAD *pOverhead = NULL;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pValue, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pK1Value is NULL", chipId, portIdx);
    
    ret = drv_pos_stm1OhMemGet(chipId, portIdx, &pOverhead);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_stm1OhMemGet failed, return %d", chipId, portIdx, ret);

    *pValue = 0;  /* 先清零再赋值 */
    *pValue = pOverhead->sectionOh.rxK1Value;
    
    return ret;
}



/**************************************************************************
* 函数名称: drv_pos_txK2Query
* 功能描述: 查询发送的K2.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: *pValue:保存K2的值
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_txK2Query(BYTE chipId, BYTE portIdx, BYTE *pValue)
{
    DRV_POS_RET ret = DRV_POS_OK;
    DRV_POS_STM1_OVERHEAD *pOverhead = NULL;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pValue, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pValue is NULL", chipId, portIdx);
    
    ret = drv_pos_stm1OhMemGet(chipId, portIdx, &pOverhead);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_stm1OhMemGet failed, return %d", 
        chipId, portIdx, ret);

    *pValue = 0;  /* 先清零再赋值 */
    *pValue = pOverhead->sectionOh.txK2Value;
    
    return ret;
}


/**************************************************************************
* 函数名称: drv_pos_rxK2Query
* 功能描述: 查询接收的K2.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: *pValue:保存K2的值
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_rxK2Query(BYTE chipId, BYTE portIdx, BYTE *pValue)
{
    DRV_POS_RET ret = DRV_POS_OK;
    DRV_POS_STM1_OVERHEAD *pOverhead = NULL;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pValue, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pValue is NULL", chipId, portIdx);
    
    ret = drv_pos_stm1OhMemGet(chipId, portIdx, &pOverhead);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_stm1OhMemGet failed, return %d", 
        chipId, portIdx, ret);

    *pValue = 0;  /* 先清零再赋值 */
    *pValue = pOverhead->sectionOh.rxK2Value;
    
    return ret;
}


/**************************************************************************
* 函数名称: drv_pos_sdhOhFlagGet
* 功能描述: 获取监测SDH开销的标记.
* 访问的表: 软件表g_drv_pos_sdh_oh_enable.
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.           
* 输出参数: pFlag: 保存开销使能标记,DRV_POS_ENABLE使能,DRV_POS_DISABLE非使能.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-13   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhOhFlagGet(BYTE chipId, WORD32 *pFlag)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_POINTER_CHECK(pFlag, DRV_POS_PT_ERR, "chip[%d] pFlag is NULL", chipId);

    *pFlag = 0;
    *pFlag = g_drv_pos_sdh_oh_enable[chipId];
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhOhFlagSet
* 功能描述: 设置监测SDH开销的标记.
* 访问的表: 软件表g_drv_pos_sdh_oh_enable.
* 修改的表: 软件表g_drv_pos_sdh_oh_enable.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           flag: 开销使能标记,DRV_POS_ENABLE使能,DRV_POS_DISABLE非使能.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-13   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhOhFlagSet(BYTE chipId, WORD32 flag)
{
    DRV_POS_CHIPID_CHECK(chipId);
    
    g_drv_pos_sdh_oh_enable[chipId] = flag;
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhSecOhFlagGet
* 功能描述: 获取监测SDH段开销的标记.
* 访问的表: 软件表g_drv_pos_sdhSecOhEnable.
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.           
* 输出参数: pFlag: 保存开销使能标记,DRV_POS_ENABLE使能,DRV_POS_DISABLE非使能.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-13   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhSecOhFlagGet(BYTE chipId, BYTE portIdx, WORD32 *pFlag)
{    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pFlag, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pFlag is NULL", chipId, portIdx);
   
    *pFlag = 0;
    *pFlag = g_drv_pos_sdhSecOhEnable[chipId][portIdx];
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhSecOhFlagSet
* 功能描述: 设置监测SDH段开销的标记.
* 访问的表: 软件表g_drv_pos_sdhSecOhEnable.
* 修改的表: 软件表g_drv_pos_sdhSecOhEnable.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           flag: 开销使能标记,DRV_POS_ENABLE使能,DRV_POS_DISABLE非使能.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-13   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhSecOhFlagSet(BYTE chipId, BYTE portIdx, WORD32 flag)
{    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
   
    g_drv_pos_sdhSecOhEnable[chipId][portIdx] = flag;
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhVc4OhFlagGet
* 功能描述: 获取监测SDH高阶通道开销的标记.
* 访问的表: 软件表g_drv_pos_sdhVc4OhEnable.
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.           
* 输出参数: pFlag: 保存开销使能标记,DRV_POS_ENABLE使能,DRV_POS_DISABLE非使能.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-13   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhVc4OhFlagGet(BYTE chipId, BYTE portIdx, WORD32 *pFlag)
{    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pFlag, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pFlag is NULL", chipId, portIdx);
    
    *pFlag = 0;
    *pFlag = g_drv_pos_sdhVc4OhEnable[chipId][portIdx];
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhVc4OhFlagSet
* 功能描述: 设置监测SDH高阶通道开销的标记.
* 访问的表: 软件表g_drv_pos_sdhVc4OhEnable.
* 修改的表: 软件表g_drv_pos_sdhVc4OhEnable.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           flag: 开销使能标记,DRV_POS_ENABLE使能,DRV_POS_DISABLE非使能.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-13   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhVc4OhFlagSet(BYTE chipId, BYTE portIdx, WORD32 flag)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    
    g_drv_pos_sdhVc4OhEnable[chipId][portIdx] = flag;
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhOverheadMonitor
* 功能描述: 监测STM-1单板的SDH开销.
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pDwSubslotId: STM-1单板的子槽位号 
* 输出参数: 无
* 返 回 值: 
* 其它说明: SDH开销线程的入口函数
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-27   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhOverheadMonitor(const WORD32 *pDwSubslotId)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    BYTE subslotId = 0;
    BYTE chipId = 0;         /* ARRIVE芯片的芯片编号 */
    BYTE portIdx = 0;         /* STM-1单板的155M端口的编号 */
    BYTE portIndex = 0;
    DRV_POS_STM1_OVERHEAD stm1Oh;
    BYTE portNum = 0;
    BYTE portMap[DRV_POS_MAX_PORT] = {0};

    WORD32 rv = BSP_E_CP3BAN_OK;
    WORD32 dwBoardStatus = 0;  /* CP3BAN单板的状态 */

    DRV_POS_POINTER_CHECK(pDwSubslotId, DRV_POS_PT_ERR, "pDwSubslotId is NULL");
    subslotId = (BYTE)(*pDwSubslotId);
    chipId = subslotId - (BYTE)1;
    DRV_POS_CHIPID_CHECK(chipId);

    if(NULL == pDwSubslotId)
    {
        free((void*)pDwSubslotId);
        pDwSubslotId = NULL;
    }
    g_drv_pos_sdh_oh_thread_id[chipId] = __gettid();
    
    while (DRV_POS_TRUE)
    {
        rv = BSP_cp3banBoardStatusGet((WORD32)subslotId, &dwBoardStatus); /* 获取单板的状态 */
        if (BSP_E_CP3BAN_OK != rv)
        {
            DRV_POS_ERROR_RET(DRV_POS_ERROR, "chip[%d] BSP_cp3banBoardStatusGet failed, return %d", chipId, ret);
        }
        if (BSP_CP3BAN_BOARD_OFFLINE == dwBoardStatus)  /* 单板不在位,直接返回成功 */
        {
            DRV_POS_ERROR_RET(DRV_POS_ERROR, "chip[%d] board is offline, exit the overhead monitor", chipId, ret);
        }  
        if (DRV_POS_DISABLE != g_drv_pos_sdh_oh_enable[chipId])
        {
            /*获取端口映射*/
            ret = drv_pos_get_valid_port_map(chipId, portMap, &portNum);
            DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_valid_port_map failed return %d", chipId, ret);
    

            for (portIdx = 0; portIdx < portNum; portIdx++)
            {
                portIndex = portMap[portIdx];


                if (DRV_POS_DISABLE != g_drv_pos_sdhSecOhEnable[chipId][portIndex])
                {
                    ret = drv_pos_sectionOhGet(chipId, portIndex, &(stm1Oh.sectionOh));
                }

                if (DRV_POS_DISABLE != g_drv_pos_sdhVc4OhEnable[chipId][portIndex])
                {
                    ret = drv_pos_highPathOhGet(chipId, portIndex, &(stm1Oh.vc4Oh));
                }
                /* 不论获取是否成功，都存储*/
                ret = drv_pos_stm1OverheadSave(chipId, portIndex, &stm1Oh);  /* Save STM-1 overhead bytes. */
                if (DRV_POS_OK != ret)
                {
                    continue;
                }
                
                BSP_DelayMs(125);  /* delay 125ms. */
            }
        }
        BSP_DelayMs(1000);  /* delay 1000ms. */
    }
    
    return ret;
}


/**************************************************************************
* 函数名称: drv_pos_txJ0Query
* 功能描述: 查询发送的J0.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: msgBuf:保存J0的值,需要64字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_txJ0Query(BYTE chipId, BYTE portIdx, BYTE *msgBuf)
{
    DRV_POS_RET rv = DRV_POS_OK;  /* 函数返回码 */
    DRV_POS_STM1_OVERHEAD *pOverhead = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(msgBuf, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] msgBuf is NULL", chipId, portIdx);
    
    rv = drv_pos_stm1OhMemGet(chipId, portIdx, &pOverhead);
    DRV_POS_IF_ERROR_RET(rv, "chip[%d] portIdx[%d] drv_pos_stm1OhMemGet failed", chipId, portIdx);

    memset(msgBuf, 0, DRV_POS_TRACE_MSG_MAX_LEN);  /* 赋值之前先清零 */
    strncpy((CHAR*)msgBuf, (CHAR*)pOverhead->sectionOh.txJ0MsgBuf, DRV_POS_TRACE_MSG_MAX_LEN);
    
    return rv;
}


/**************************************************************************
* 函数名称: drv_pos_rxJ0Query
* 功能描述: 查询实际接收的J0.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: msgBuf:保存J0的值,需要64字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_rxJ0Query(BYTE chipId, BYTE portIdx, BYTE *msgBuf)
{
    DRV_POS_RET rv = DRV_POS_OK;  /* 函数返回码 */
    DRV_POS_STM1_OVERHEAD *pOverhead = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(msgBuf, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] msgBuf is NULL", chipId, portIdx);
    
    rv = drv_pos_stm1OhMemGet(chipId, portIdx, &pOverhead);
    DRV_POS_IF_ERROR_RET(rv, "chip[%d] portIdx[%d] drv_pos_stm1OhMemGet failed", chipId, portIdx);

    memset(msgBuf, 0, DRV_POS_TRACE_MSG_MAX_LEN);  /* 赋值之前先清零 */
    strncpy((CHAR*)msgBuf, (CHAR*)pOverhead->sectionOh.rxJ0MsgBuf, DRV_POS_TRACE_MSG_MAX_LEN);
    
    return rv;
}


/**************************************************************************
* 函数名称: drv_pos_expectedRxJ0Query
* 功能描述: 查询期望接收的J0.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: msgBuf:保存J0的值,需要64字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_expectedRxJ0Query(BYTE chipId, BYTE portIdx, BYTE *msgBuf)
{
    DRV_POS_RET rv = DRV_POS_OK;  /* 函数返回码 */
    DRV_POS_STM1_OVERHEAD *pOverhead = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(msgBuf, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] msgBuf is NULL", chipId, portIdx);
    
    rv = drv_pos_stm1OhMemGet(chipId, portIdx, &pOverhead);
    DRV_POS_IF_ERROR_RET(rv, "chip[%d] portIdx[%d] drv_pos_stm1OhMemGet failed", chipId, portIdx);

    memset(msgBuf, 0, DRV_POS_TRACE_MSG_MAX_LEN);  /* 赋值之前先清零 */
    strncpy((CHAR*)msgBuf, (CHAR*)pOverhead->sectionOh.expectedRxJ0MsgBuf, DRV_POS_TRACE_MSG_MAX_LEN);
    
    return rv;
}


/**************************************************************************
* 函数名称: drv_pos_txS1Query
* 功能描述: 查询发送的S1.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: *pValue:保存S1的值
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_txS1Query(BYTE chipId, BYTE portIdx, BYTE *pValue)
{
    DRV_POS_RET rv = DRV_POS_OK;  /* 函数返回码 */
    DRV_POS_STM1_OVERHEAD *pOverhead = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pValue, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pValue is NULL", chipId, portIdx);
    
    rv = drv_pos_stm1OhMemGet(chipId, portIdx, &pOverhead);
    DRV_POS_IF_ERROR_RET(rv, "chip[%d] portIdx[%d] drv_pos_stm1OhMemGet failed", chipId, portIdx);

    *pValue = 0;
    *pValue = pOverhead->sectionOh.txS1Value;
    
    return rv;
}


/**************************************************************************
* 函数名称: drv_pos_rxS1Query
* 功能描述: 查询接收的S1.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: *pValue:保存S1的值
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_rxS1Query(BYTE chipId, BYTE portIdx, BYTE *pValue)
{
    WORD32 rv = DRV_POS_OK;  /* 函数返回码 */
    DRV_POS_STM1_OVERHEAD *pOverhead = NULL;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pValue, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pValue is NULL", chipId, portIdx);
    
    rv = drv_pos_stm1OhMemGet(chipId, portIdx, &pOverhead);
    DRV_POS_IF_ERROR_RET(rv, "chip[%d] port[%d] drv_pos_stm1OhMemGet failed", chipId, portIdx);

    *pValue = 0;
    *pValue = pOverhead->sectionOh.rxS1Value;
    
    return rv;
}


/**************************************************************************
* 函数名称: drv_pos_txJ1Query
* 功能描述: 查询发送的J1.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: msgBuf:保存J1的值,需要64字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_txJ1Query(BYTE chipId, BYTE portIdx, BYTE *msgBuf)
{
    DRV_POS_RET rv = DRV_POS_OK;  /* 函数返回码 */
    DRV_POS_STM1_OVERHEAD *pOverhead = NULL;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(msgBuf, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] msgBuf is NULL", chipId, portIdx);
    
    rv = drv_pos_stm1OhMemGet(chipId, portIdx, &pOverhead);
    DRV_POS_IF_ERROR_RET(rv, "chip[%d] portIdx[%d] drv_pos_stm1OhMemGet failed", chipId, portIdx);

    memset(msgBuf, 0, DRV_POS_TRACE_MSG_MAX_LEN);  /* 赋值之前先清零 */
    strncpy((CHAR*)msgBuf, (CHAR*)pOverhead->vc4Oh.txJ1MsgBuf, DRV_POS_TRACE_MSG_MAX_LEN);
    
    return rv;
}


/**************************************************************************
* 函数名称: drv_pos_rxJ1Query
* 功能描述: 查询实际接收的J1.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: msgBuf:保存J1的值,需要64字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_rxJ1Query(BYTE chipId, BYTE portIdx, BYTE *msgBuf)
{
    DRV_POS_RET rv = DRV_POS_OK;  /* 函数返回码 */
    DRV_POS_STM1_OVERHEAD *pOverhead = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(msgBuf, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] msgBuf is NULL", chipId, portIdx);
    
    rv = drv_pos_stm1OhMemGet(chipId, portIdx, &pOverhead);
    DRV_POS_IF_ERROR_RET(rv, "chip[%d] portIdx[%d] drv_pos_stm1OhMemGet failed", chipId, portIdx);

    memset(msgBuf, 0, DRV_POS_TRACE_MSG_MAX_LEN);  /* 赋值之前先清零 */
    strncpy((CHAR*)msgBuf, (CHAR*)pOverhead->vc4Oh.rxJ1MsgBuf, DRV_POS_TRACE_MSG_MAX_LEN);
    
    return rv;
}


/**************************************************************************
* 函数名称: drv_pos_expectedRxJ1Query
* 功能描述: 查询期望接收的J1.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: msgBuf:保存J1的值,需要64字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_expectedRxJ1Query(BYTE chipId, BYTE portIdx, BYTE *msgBuf)
{
    DRV_POS_RET rv = DRV_POS_OK;  /* 函数返回码 */
    DRV_POS_STM1_OVERHEAD *pOverhead = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(msgBuf, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] msgBuf is NULL", chipId, portIdx);
    
    rv = drv_pos_stm1OhMemGet(chipId, portIdx, &pOverhead);
    DRV_POS_IF_ERROR_RET(rv, "chip[%d] portIdx[%d] drv_pos_stm1OhMemGet failed", chipId, portIdx);

    memset(msgBuf, 0, DRV_POS_TRACE_MSG_MAX_LEN);  /* 赋值之前先清零 */
    strncpy((CHAR*)msgBuf, (CHAR*)pOverhead->vc4Oh.expectedRxJ1MsgBuf, DRV_POS_TRACE_MSG_MAX_LEN);
    
    return rv;
}


/**************************************************************************
* 函数名称: drv_pos_txC2Query
* 功能描述: 查询发送的C2.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: *pValue:保存C2的值. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_txC2Query(BYTE chipId, BYTE portIdx, BYTE *pValue)
{
    DRV_POS_RET rv = DRV_POS_OK;  /* 函数返回码 */
    DRV_POS_STM1_OVERHEAD *pOverhead = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pValue, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pValue is NULL", chipId, portIdx);
    
    rv = drv_pos_stm1OhMemGet(chipId, portIdx, &pOverhead);
    DRV_POS_IF_ERROR_RET(rv, "chip[%d] portIdx[%d] drv_pos_stm1OhMemGet failed", chipId, portIdx);

    *pValue = 0;  /* 赋值之前先清零 */
    *pValue = pOverhead->vc4Oh.txC2Value;
    
    return rv;
}


/**************************************************************************
* 函数名称: drv_pos_rxC2Query
* 功能描述: 查询实际接收的C2.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: *pValue:保存C2的值. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_rxC2Query(BYTE chipId, BYTE portIdx, BYTE *pValue)
{
    DRV_POS_RET rv = DRV_POS_OK;  /* 函数返回码 */
    DRV_POS_STM1_OVERHEAD *pOverhead = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pValue, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pValue is NULL", chipId, portIdx);
    
    rv = drv_pos_stm1OhMemGet(chipId, portIdx, &pOverhead);
    DRV_POS_IF_ERROR_RET(rv, "chip[%d] portIdx[%d] drv_pos_stm1OhMemGet failed", chipId, portIdx);

    *pValue = 0;  /* 赋值之前先清零 */
    *pValue = pOverhead->vc4Oh.rxC2Value;
    
    return rv;
}


/**************************************************************************
* 函数名称: drv_pos_expectedRxC2Query
* 功能描述: 查询期望接收的C2.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: *pValue:保存C2的值. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_expectedRxC2Query(BYTE chipId, BYTE portIdx, BYTE *pValue)
{
    DRV_POS_RET rv = DRV_POS_OK;  /* 函数返回码 */
    DRV_POS_STM1_OVERHEAD *pOverhead = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pValue, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pValue is NULL", chipId, portIdx);
    
    rv = drv_pos_stm1OhMemGet(chipId, portIdx, &pOverhead);
    DRV_POS_IF_ERROR_RET(rv, "chip[%d] portIdx[%d] drv_pos_stm1OhMemGet failed", chipId, portIdx);

    *pValue = 0;  /* 赋值之前先清零 */
    *pValue = pOverhead->vc4Oh.expectedRxC2Value;
    
    return rv;
}

/**************************************************************************
* 函数名称: drv_pos_sdhOhThreadCreate
* 功能描述: 创建SDH开销的线程.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM-1单板的子槽位号 
* 输出参数: 无
* 返 回 值: 
* 其它说明: 该线程用来监测STM-1单板的SDH开销查询.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-12   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhOhThreadCreate(BYTE subslotId)
{
    DRV_POS_RET rv = DRV_POS_OK;
    WORD32 * pDwSubslotId = NULL;
    BYTE chipId = 0;
    
    chipId = subslotId - (BYTE)1;
    DRV_POS_CHIPID_CHECK(chipId);
    
    pDwSubslotId = malloc(sizeof(WORD32));
    DRV_POS_POINTER_CHECK(pDwSubslotId, DRV_POS_PT_ERR, "chip[%d] malloc failed", chipId);
    
    memset(pDwSubslotId, 0, sizeof(WORD32));
    *pDwSubslotId = (WORD32)subslotId;
    
    init_thread(&g_drv_pos_sdh_oh_thread[chipId], 0, 0, 80, (void *)drv_pos_sdhOverheadMonitor, ((void *)(pDwSubslotId)));
    rv = start_mod_thread(&g_drv_pos_sdh_oh_thread[chipId]);
    if (BSP_OK != rv)
    {
        if(NULL == pDwSubslotId)
        {
            free((void*)pDwSubslotId);
            pDwSubslotId = NULL;
        }
        DRV_POS_ERROR_RET(DRV_POS_ERROR, "chip[%d] sdhOh start_mod_thread failed ", chipId);
    }
    
    DRV_POS_SUCCESS_PRINT("chip[%d] drv_pos_sdhOhThreadCreate", chipId);
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhOhThreadDelete
* 功能描述: 删除SDH开销的线程.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM-1单板的子槽位号 
* 输出参数: 无
* 返 回 值: 
* 其它说明: 该线程用来监测STM-1单板的SDH开销查询.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-12   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhOhThreadDelete(BYTE subslotId)
{
    BYTE chipId = 0;
    
    chipId = subslotId - (BYTE)1;
    DRV_POS_CHIPID_CHECK(chipId);
    ROSNG_TRACE_DEBUG("g_drv_pos_sdh_oh_thread_id[%d] = %d START\n", chipId, g_drv_pos_sdh_oh_thread_id[chipId]);

    if(0 != g_drv_pos_sdh_oh_thread_id[chipId])
    {
        stop_mod_thread(&g_drv_pos_sdh_oh_thread[chipId]);
        memset(&g_drv_pos_sdh_oh_thread[chipId], 0, sizeof(g_drv_pos_sdh_oh_thread[chipId]));
        g_drv_pos_sdh_oh_thread_id[chipId] = 0;
    }
    
    ROSNG_TRACE_DEBUG("g_drv_pos_sdh_oh_thread_id[%d] = %d END\n", chipId, g_drv_pos_sdh_oh_thread_id[chipId]);
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhOhThreadIdGet
* 功能描述: 获取SDH开销线程的threadId.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3. 
* 输出参数: *pThreadId: 用来保存threadId.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-14   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhOhThreadIdGet(BYTE chipId, int *pThreadId)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_POINTER_CHECK(pThreadId, DRV_POS_PT_ERR, "chip[%d] pThreadId is NULL", chipId);
    
    *pThreadId = g_drv_pos_sdh_oh_thread_id[chipId];
    
    return DRV_POS_OK;
}



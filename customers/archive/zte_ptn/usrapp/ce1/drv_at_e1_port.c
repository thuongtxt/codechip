/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_at_e1_port.c
* 文件标识: 
* 其它说明: ce1tan 端口属性设置
* 当前版本: V1.0
* 作    者: 陈博10140635.
* 完成日期: 2013-05-15
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/


#include "AtDriver.h"
#include "AtDevice.h"
#include "AtModulePdh.h"
#include "AtModulePw.h"
#include "AtSerdesController.h"
#include "AtEthPort.h"
#include "AtChannel.h"
#include "AtPdhDe1.h"
#include "AtPdhChannel.h"
#include "AtModuleEth.h"
#include "AtModuleClock.h"
#include "AtClockExtractor.h"
#include "drv_at_e1_port.h"
#include "drv_at_e1_pwe3.h"

/*全局变量声明*/
AT_E1_PORT_STATS g_atE1PortStats[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_E1_INTF_NUM];



/*外部变量*/
extern DRV_AT_PW_PARA  g_atE1PwInfo [AT_MAX_SUBCARD_NUM_PER_SLOT][AT_MAX_PW_NUM_PER_SUBCARD] ;


/*外部函数声明*/
extern AtDevice DrvAtDevHdlGet(BYTE chipId);
extern WORD32 ZteLiuRead(BYTE subcardId,BYTE liuId,WORD32 addr,BYTE *value);
extern WORD32 ZteLiuWrite(BYTE subcardId,BYTE liuId,WORD32 addr,BYTE value);
extern WORD32 DrvAtPwClkModeTableUpdate(BYTE chipId,WORD32 e1LinkNo,BYTE newClkMode );
extern WORD32 DrvAtClkSrcPwIdPerE1Find(BYTE chipId,WORD32 e1LinkNo,WORD32*pwId);
extern WORD32 DrvAtPwObjGet(BYTE chipId,WORD32 pwId,AtPw*atPwObj);
/*******************************************************************************
* 函数名称:   DrvLiuIdCalc
* 功能描述:   获取LIU id和每片liu上的e1 Id
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvLiuIdCalc(WORD32 e1LinkNo,BYTE* liuId,WORD32*e1LinkPerLiu)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    
    CHECK_AT_E1LINK_ID(e1LinkNo);
    CHECK_AT_POINTER_NULL(liuId);
    CHECK_AT_POINTER_NULL(e1LinkPerLiu);
    
    if((e1LinkNo >=  0)&&(e1LinkNo < 8 ))
    {
        *liuId = 0;
        *e1LinkPerLiu = e1LinkNo;
    }
    else if((e1LinkNo >=  8)&&(e1LinkNo < 16 ))
    {
        *liuId = 1 ;
        *e1LinkPerLiu = e1LinkNo - 8;
    }
    else if((e1LinkNo >=  16)&&(e1LinkNo < 24 ))
    {
        *liuId = 2 ;
        *e1LinkPerLiu = e1LinkNo - 16;
    }
    return ret ;

}
/*******************************************************************************
* 函数名称:   DrvLiuTxInsertAisOp
* 功能描述:   liu Tx 强制插入ais
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvLiuTxInsertAisOp(BYTE subcardId,WORD32 e1LinkNo,BYTE option)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    WORD32 e1LinkPerLiu = 0;
    BYTE liuId = 0;
    BYTE valueGet = 0;
    BYTE valueSet = 0;
    
    CHECK_AT_SUBCARD_ID(subcardId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    
    DrvLiuIdCalc(e1LinkNo,&liuId,&e1LinkPerLiu);
        
    if(LIU_INSERT_ON == option)
    {
        ZteLiuRead(subcardId, liuId, LIU_TX_FORCE_AIS_REG,&valueGet);
        valueSet = valueGet|(((BYTE)1<<e1LinkPerLiu));
        ZteLiuWrite(subcardId,liuId,LIU_TX_FORCE_AIS_REG,valueSet);      /*发送方向插全"1"*/
    }
    else if(LIU_INSERT_OFF == option)
    {
        ZteLiuRead( subcardId, liuId, LIU_TX_FORCE_AIS_REG,&valueGet);
        valueSet = valueGet&(~((BYTE)1<<e1LinkPerLiu));
        ZteLiuWrite(subcardId,liuId,LIU_TX_FORCE_AIS_REG,valueSet);      /*屏蔽发送方向插全"1"*/  
    }
    
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvLiuLoopBackOp
* 功能描述:   liu loopback设置函数
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvLiuLoopBackOp(BYTE subcardId,WORD32 e1LinkNo,BYTE loopbackMode ,BYTE option)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    WORD32 e1LinkPerLiu = 0;
    BYTE chipId = subcardId - 1;
    BYTE liuId = 0;
    BYTE valueGet = 0;
    BYTE valueSet = 0;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    
    DRV_AT_E1_PRINT(DRV_AT_LIU_LOOP_SET,"[CE1TAN][LIU LOOP]:%s line %d,subcardId is %u,e1LinkNo is %u,loopMode is %u,option is %u\n",__FILE__, __LINE__,subcardId,e1LinkNo,loopbackMode,option);
    DrvLiuIdCalc(e1LinkNo,&liuId,&e1LinkPerLiu);

    if(CE1_ENABLE == option)
    {
        if(LIU_LOOP_LINE == loopbackMode)           /*loopback line环回*/
        {
            ZteLiuRead(subcardId, liuId, LIU_LOOP_LINE_REG,&valueGet);
            valueSet = valueGet |(((BYTE)1<<e1LinkPerLiu));
            ZteLiuWrite(subcardId,liuId,LIU_LOOP_LINE_REG,valueSet);  
            g_atE1PortStats[chipId][e1LinkNo].loopbackMode = LIU_LOOP_LINE ;
        }
        else if(LIU_LOOP_INTERNAL == loopbackMode)    /*loopback intenal 环回*/
        {    
            ZteLiuRead( subcardId, liuId, LIU_LOOP_INTERNAL_REG,&valueGet);
            valueSet = valueGet |(((BYTE)1<<e1LinkPerLiu));
            ZteLiuWrite(subcardId,liuId,LIU_LOOP_INTERNAL_REG,valueSet);   
            g_atE1PortStats[chipId][e1LinkNo].loopbackMode = LIU_LOOP_INTERNAL;
        }
    }
    if(CE1_DISABLE == option) /*取消两种环回*/
    {
        ZteLiuRead( subcardId, liuId, LIU_LOOP_LINE_REG,&valueGet);
        valueSet = valueGet&(~((BYTE)1<<e1LinkPerLiu));
        ZteLiuWrite(subcardId,liuId,LIU_LOOP_LINE_REG,valueSet);      
        
        ZteLiuRead( subcardId, liuId, LIU_LOOP_INTERNAL_REG,&valueGet);
        valueSet = valueGet &(~((BYTE)1<<e1LinkPerLiu));
        ZteLiuWrite(subcardId,liuId,LIU_LOOP_INTERNAL_REG,valueSet);                 
        g_atE1PortStats[chipId][e1LinkNo].loopbackMode = LIU_LOOP_NONE;
    }
          
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvLiuLosGet
* 功能描述:   liu alarm get
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvLiuLosGet(BYTE subcardId,WORD32 e1LinkNo,BYTE*liuLos)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    WORD32 e1LinkPerLiu = 0;
    BYTE liuId = 0;
    BYTE valueGet = 0;
    
    CHECK_AT_SUBCARD_ID(subcardId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    CHECK_AT_POINTER_NULL(liuLos);
    
    DrvLiuIdCalc(e1LinkNo,&liuId,&e1LinkPerLiu);
    ZteLiuRead( subcardId, liuId, LIU_RX_LOS_DETECT_REG,&valueGet);
    *liuLos = (0 == (valueGet&((BYTE)1<<e1LinkPerLiu)))? 0:1;
    
    return ret;    
}

/*******************************************************************************
* 函数名称:   DrvLiuLinecodeSet
* 功能描述:   liu lincode set
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvLiuLinecodeSet(BYTE subcardId,WORD32 e1LinkNo,WORD32 linecodeType)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 e1LinkPerLiu = 0;
    BYTE chipId = subcardId -1;
    BYTE liuId = 0;
    BYTE valueSet = 0;
    BYTE valueGet = 0;
    WORD32 LiuLcType = 0; /*liu 的linecode类型*/
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    DRV_AT_E1_PRINT(DRV_AT_LINECODE_SET,"[CE1TAN][LINECODE SET]:%s line %d,subcardId is %u,e1LinkNo is %u,linecodeType is %u\n",__FILE__, __LINE__,subcardId,e1LinkNo,linecodeType);
    DrvLiuIdCalc(e1LinkNo,&liuId,&e1LinkPerLiu);
    
    if(SDH_CODE_HDB3 == linecodeType)
    {
        ZteLiuWrite(subcardId,liuId,LIU_ADDR_PONITER_CTRL_REG,0xaa);/*切到第二页寄存器*/
        ZteLiuRead(subcardId, liuId, LIU_LINECODE_SET_REG,&valueGet);
        valueSet = valueGet&(~((BYTE)1<<e1LinkPerLiu));
        ZteLiuWrite(subcardId,liuId,LIU_LINECODE_SET_REG,valueSet);
        ZteLiuWrite(subcardId,liuId,LIU_ADDR_PONITER_CTRL_REG,0);/*切到第一页寄存器*/
        LiuLcType = LIU_LINECODE_HDB3 ;
    }
    else if(SDH_CODE_AMI == linecodeType)
    {
        ZteLiuWrite(subcardId,liuId,LIU_ADDR_PONITER_CTRL_REG,0xaa);/*切到第二页寄存器*/
        ZteLiuRead(subcardId, liuId, LIU_LINECODE_SET_REG,&valueGet);
        valueSet = valueGet|(((BYTE)1<<e1LinkPerLiu));
        ZteLiuWrite(subcardId,liuId,LIU_LINECODE_SET_REG,valueSet);
        ZteLiuWrite(subcardId,liuId,LIU_ADDR_PONITER_CTRL_REG,0);/*切到第一页寄存器*/
        LiuLcType = LIU_LINECODE_AMI;
    }
    else
    {
        ret = DRV_LIU_LINECODE_NOT_SUPPORT;
    }
    
    g_atE1PortStats[chipId][e1LinkNo].lineCode = LiuLcType ;
    return ret ;

}

/*******************************************************************************
* 函数名称:   DrvLiuPortControl
* 功能描述:   liu 端口控制
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvLiuPortControl(BYTE subcardId,WORD32 e1LinkNo,WORD32 action)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 e1LinkPerLiu = 0;
    BYTE chipId = subcardId -1;
    BYTE liuId = 0;
    BYTE valueSet = 0;
    BYTE valueGet = 0;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    
    DRV_AT_E1_PRINT(DRV_AT_PORT_CTRL,"[CE1TAN][LIU PORT CONTROL]:%s line %d,subcardId is %u,e1LinkNo is %u,action is %u\n",__FILE__, __LINE__,subcardId,e1LinkNo,action);
    DrvLiuIdCalc(e1LinkNo,&liuId,&e1LinkPerLiu);
    if(BSP_ON == action)/*打开端口*/
    {
        ZteLiuWrite(subcardId,liuId,LIU_ADDR_PONITER_CTRL_REG,0xaa);/*切到第二页寄存器*/
        ZteLiuRead(subcardId, liuId, LIU_TX_POWER_DOWN_REG,&valueGet);
        valueSet = valueGet&(~((BYTE)1<<e1LinkPerLiu));
        ZteLiuWrite(subcardId,liuId,LIU_TX_POWER_DOWN_REG,valueSet);/*打开发送*/

        ZteLiuRead(subcardId, liuId, LIU_RX_POWER_DOWN_REG,&valueGet);
        valueSet = valueGet&(~((BYTE)1<<e1LinkPerLiu));
        ZteLiuWrite(subcardId,liuId,LIU_RX_POWER_DOWN_REG,valueSet);/*打开接收*/    
        ZteLiuWrite(subcardId,liuId,LIU_ADDR_PONITER_CTRL_REG,0);/*切回第一页寄存器*/
        g_atE1PortStats[chipId][e1LinkNo].isShutDown = CE1_FALSE;
    
    }
    else if (BSP_OFF == action)/*关闭端口*/
    { 
        ZteLiuWrite(subcardId,liuId,LIU_ADDR_PONITER_CTRL_REG,0xaa);/*切到第二页寄存器*/
        ZteLiuRead(subcardId, liuId, LIU_TX_POWER_DOWN_REG,&valueGet);
        valueSet = valueGet|(((BYTE)1<<e1LinkPerLiu));
        ZteLiuWrite(subcardId,liuId,LIU_TX_POWER_DOWN_REG,valueSet);/*down 掉发送*/

        ZteLiuRead(subcardId, liuId, LIU_RX_POWER_DOWN_REG,&valueGet);
        valueSet = valueGet|(((BYTE)1<<e1LinkPerLiu));
        ZteLiuWrite(subcardId,liuId,LIU_RX_POWER_DOWN_REG,valueSet);/*down 掉接收*/
        ZteLiuWrite(subcardId,liuId,LIU_ADDR_PONITER_CTRL_REG,0);/*切回第一页寄存器*/
        g_atE1PortStats[chipId][e1LinkNo].isShutDown = CE1_TURE;
    
    }
       
    return ret ;
}


/*******************************************************************************
* 函数名称:   DrvLiuRecvClkSet
* 功能描述:   liu 端口接收时钟设置
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvLiuRecvClkSet(BYTE subcardId,WORD32 e1LinkNo,BYTE recvClkMode)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 e1LinkPerLiu = 0;
    BYTE chipId = subcardId -1;
    BYTE liuId = 0;
    BYTE valueSet = 0;
    BYTE valueGet = 0;

    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    
    DRV_AT_E1_PRINT(DRV_AT_PORT_CTRL,"[CE1TAN][LIU RECVCLK SET]:%s line %d,subcardId is %u,e1LinkNo is %u,recvClkMode is %u\n",__FILE__, __LINE__,subcardId,e1LinkNo,recvClkMode);
    DrvLiuIdCalc(e1LinkNo,&liuId,&e1LinkPerLiu);
    
    if(BSP_SDH_PORT_RECV_CLK_LINE  == recvClkMode)
    {
        ZteLiuWrite(subcardId,liuId,LIU_ADDR_PONITER_CTRL_REG,0xaa);/*切到第二页寄存器*/
        ZteLiuRead(subcardId, liuId, LIU_RECV_CLK_SET,&valueGet);
        valueSet = valueGet&(~((BYTE)1<<e1LinkPerLiu));
        ZteLiuWrite(subcardId,liuId,LIU_RECV_CLK_SET,valueSet);      /*从线路恢复时钟*/
        ZteLiuWrite(subcardId,liuId,LIU_ADDR_PONITER_CTRL_REG,0);/*切回第一页寄存器*/
        g_atE1PortStats[chipId][e1LinkNo].recvClkMode = LIU_RECV_CLK_LINE;
    }
    else if(BSP_SDH_PORT_RECV_CLK_SYS  == recvClkMode)
    {
        ZteLiuWrite(subcardId,liuId,LIU_ADDR_PONITER_CTRL_REG,0xaa); /*切到第二页寄存器*/
        ZteLiuRead(subcardId, liuId, LIU_RECV_CLK_SET,&valueGet);
        valueSet = valueGet|(((BYTE)1<<e1LinkPerLiu));
        ZteLiuWrite(subcardId,liuId,LIU_RECV_CLK_SET,valueSet);    /*用系统时钟*/
        ZteLiuWrite(subcardId,liuId,LIU_ADDR_PONITER_CTRL_REG,0);   /*切回第一页寄存器*/
        g_atE1PortStats[chipId][e1LinkNo].recvClkMode = LIU_RECV_CLK_SYS;
    }

    return ret;
}



/*******************************************************************************
* 函数名称:   DrvAtE1ObjGet
* 功能描述:   获取一个E1 object
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvAtE1ObjGet(BYTE chipId,WORD32  e1LinkId,AtPdhDe1 *atE1Obj)
{  
    WORD32 ret = DRV_AT_SUCCESS ;
    AtModulePdh atPdhModule = NULL;
    AtDevice atDevHdl = NULL ;

    CHECK_AT_POINTER_NULL(atE1Obj) ;
    atDevHdl = DrvAtDevHdlGet(chipId) ;
    CHECK_AT_POINTER_NULL(atDevHdl);
    
    atPdhModule = (AtModulePdh)AtDeviceModuleGet(atDevHdl, cAtModulePdh);
    if(NULL == atPdhModule)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PDH MODULE GET FAILED !!\n", __FILE__, __LINE__); 
        return DRV_AT_PDH_GET_MODULE_FAILED; 
    }
    *atE1Obj = AtModulePdhDe1Get(atPdhModule, e1LinkId);
    if(NULL == (*atE1Obj))
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, E1 OBJ GET FAILED !!CHIP ID IS %u ,E1 lINK NO :%u \n", __FILE__, __LINE__,chipId,e1LinkId); 
        return DRV_AT_PDH_GET_OBJ_FAILED; 
    }
    return ret;
}


/*******************************************************************************
* 函数名称:   DrvAtE1Nxds0ObjCreate
* 功能描述:   创建一个cesop E1  object
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvAtE1Nxds0ObjCreate(BYTE chipId,WORD32 e1LinkNo,WORD32 tsBitMap,AtPdhNxDS0 *atNxds0Obj)
{
    WORD32 ret = DRV_AT_SUCCESS;
    AtPdhDe1 atE1Obj = NULL;

    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    CHECK_AT_POINTER_NULL(atNxds0Obj) ;
    
    ret = DrvAtE1ObjGet( chipId, e1LinkNo,&atE1Obj) ;
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, E1 OBJECT  GET  FAILED ! !! \n", __FILE__, __LINE__); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atE1Obj);
    *atNxds0Obj = AtPdhDe1NxDs0Create(atE1Obj,tsBitMap);
    if(NULL == (*atNxds0Obj))
    {
       DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtPdhDe1NxDs0Create() failed.CHIP ID IS %u ,E1 lINK NO :%u \n", __FILE__, __LINE__,chipId,e1LinkNo); 
       return DRV_AT_PDH_CESOP_E1_CREATE_FAILED; 
    }
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtE1Nxds0ObjGet
* 功能描述:   获取一个cesop E1  object
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvAtE1Nxds0ObjGet(BYTE chipId,WORD32 e1LinkNo,WORD32 tsBitMap,AtPdhNxDS0 *atNxds0Obj)
{
    WORD32 ret = DRV_AT_SUCCESS;
    AtPdhDe1 atE1Obj = NULL;

    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    CHECK_AT_POINTER_NULL(atNxds0Obj) ;
    
    ret = DrvAtE1ObjGet( chipId, e1LinkNo,&atE1Obj) ;
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, E1 OBJECT  GET  FAILED ! !! \n", __FILE__, __LINE__); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atE1Obj) ;
    *atNxds0Obj = AtPdhDe1NxDs0Get(atE1Obj,tsBitMap);
    if(NULL == (*atNxds0Obj))
    {
       DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtPdhDe1NxDs0Get()failed . CHIP ID IS %u ,E1 lINK NO :%u TS BITMAP is %u \n", __FILE__, __LINE__,chipId,e1LinkNo,tsBitMap); 
       return DRV_AT_PDH_CESOP_E1_GET_FAILED; 
    }
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtE1Nxds0ObjDelete
* 功能描述:   删除一个cesop E1  object
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvAtE1Nxds0ObjDelete(BYTE chipId,WORD32 e1LinkNo,WORD32 tsBitMap)
{
    WORD32 ret = DRV_AT_SUCCESS;
    eAtRet atRet = cAtOk ;
    AtPdhDe1 atE1Obj = NULL;
    AtPdhNxDS0 atNxds0Obj = NULL ;

    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    
    ret = DrvAtE1ObjGet(chipId,e1LinkNo,&atE1Obj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, E1 OBJ GET FAILED !!\n", __FILE__, __LINE__); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atE1Obj);
    
    ret = DrvAtE1Nxds0ObjGet(chipId,e1LinkNo,tsBitMap,&atNxds0Obj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, E1 CESOP OBJECT  GET  FAILED ! !! \n", __FILE__, __LINE__); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atNxds0Obj);
    
    atRet = AtPdhDe1NxDs0Delete(atE1Obj,atNxds0Obj);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtPdhDe1NxDs0Delete() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_PDH_CESOP_E1_DELETE_FAILED;
    }    
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtE1DefaultCfg
* 功能描述:   E1 Framer的默认配置
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtE1DefaultCfg(BYTE chipId,WORD32 e1LinkId)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    eAtRet atRet = cAtOk;
    AtPdhDe1 atE1Obj = NULL ;
   
    /* Get object to configure */  
    ret = DrvAtE1ObjGet(chipId,e1LinkId,&atE1Obj);  
    if(DRV_AT_SUCCESS != ret)  
    {
         DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, E1 OBJ GET FAILED !!\n", __FILE__, __LINE__);  
         return ret; 
    }
    CHECK_AT_POINTER_NULL(atE1Obj);
    /* Set default configuration */
    atRet = AtPdhChannelFrameTypeSet((AtPdhChannel)atE1Obj, cAtPdhE1UnFrm);   /*UNFRAME*/
    if (cAtOk != atRet)
    {
         DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtPdhChannelFrameTypeSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
         return DRV_AT_PDH_FRAME_SET_ERROR; 
    }
    g_atE1PortStats[chipId][e1LinkId].frameMode = AT_E1_UNFRAME ;
    atRet = AtChannelTimingSet ((AtChannel)atE1Obj,cAtTimingModeSys,NULL) ;  /*SYS CLKMODE*/
    if (cAtOk != atRet)
    {
         DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtChannelTimingSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
         return DRV_AT_PDH_TIMING_SET_ERROR;
    }    
/*can not support*/
 #if 0
    atRet = AtPdhChannelLineCodeSet((AtChannel)atE1Obj, cAtPdhDe1LineCodeHdb3);  /*LINECODE HDB3*/
    if (cAtOk != atRet) 
    { 
         DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtPdhChannelLineCodeSet() ret=0x%x.\n", __FILE__, __LINE__, atRet);
         return DRV_AT_PDH_LINECODE_SET_ERROR;
    }      
  #endif  
    ROSNG_TRACE_DEBUG("E1 FRAMER CONFIGURE SUCCESS,  E1LINK ID IS %u .\n", e1LinkId);  
    return ret ;
}


/*******************************************************************************
* 函数名称:   DrvAtE1IntfInit
* 功能描述:   E1端口的初始化
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtE1IntfInit(BYTE chipId)
{  
    WORD32 ret = DRV_AT_SUCCESS ;  
    WORD32 e1LinkNo = 0;   
    for(e1LinkNo = AT_E1_START_ID ; e1LinkNo <= AT_E1_END_ID; e1LinkNo++)   /*芯片的e1 id 从0开始*/   
    { 
        ret = DrvAtE1DefaultCfg(chipId,e1LinkNo);
        if (DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, E1 INTF INIT FAILED !! RET=0x%x, E1LINK ID IS %u .\n", __FILE__, __LINE__, ret, e1LinkNo);
            return ret; 
        }     
    }
    ROSNG_TRACE_DEBUG("\nE1 INTF INIT SUCCESS !!  CHIP ID IS %u .\n", chipId);
    return ret ;
}


/*******************************************************************************
* 函数名称:   DrvAtE1FrameTypeSet
* 功能描述:   E1端口的帧格式设置
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtE1FrameTypeSet(BYTE chipId,WORD32 e1LinkNo,BYTE frameType)
{  
    WORD32 ret = DRV_AT_SUCCESS ;  
    eAtRet atRet = cAtOk;   
    AtPdhDe1 atE1Obj = NULL ;  
    WORD16 atFrameType = 0;

    CHECK_AT_CHIP_ID(chipId);  
    CHECK_AT_E1LINK_ID(e1LinkNo);

     DRV_AT_E1_PRINT(DRV_AT_E1_FRAME_SET,"[CE1TAN][E1 FRAME SET]:%s line %d,chipId is %u,e1LinkNo is %u,frameType is %u\n",__FILE__, __LINE__,chipId,e1LinkNo,frameType);
    /* Get object to configure */   
    ret = DrvAtE1ObjGet(chipId,e1LinkNo,&atE1Obj); 
    if(DRV_AT_SUCCESS != ret) 
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, E1 OBJ GET FAILED !!\n", __FILE__, __LINE__);   
        return ret;  
    } 
    CHECK_AT_POINTER_NULL(atE1Obj);
    /*set frame type : unframe ,frame basic and frame crc */
    if(AT_E1_UNFRAME == frameType )                /*unframe*/   
    { 
        atFrameType = cAtPdhE1UnFrm ;
    }  
    else if ((AT_E1_PCM30 == frameType) ||(AT_E1_PCM31 == frameType))    /*basic frame*/  
    {
        atFrameType = cAtPdhE1Frm ; 
    } 
    else   
    {
        atFrameType = cAtPdhE1MFCrc ;                   /*crc frame*/
    } 
    atRet = AtPdhChannelFrameTypeSet((AtPdhChannel)atE1Obj, atFrameType);    
    if (cAtOk !=atRet )  
    {  
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtPdhChannelFrameTypeSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));   
        return DRV_AT_PDH_FRAME_SET_ERROR;  
    }        
   
    /*is 16 bit enable ?  distinguish pcm30 and pcm31*/ 
    if((AT_E1_PCM30 == frameType) ||(AT_E1_PCM30CRC == frameType))  /*pcm30 & pcm30crc*/ 
    {   
        atRet = AtPdhDe1SignalingDs0MaskSet(atE1Obj,AT_E1_SIGNAL_MASK);
        if (cAtOk !=atRet)
        {
            DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtPdhDe1SignalingDs0MaskSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
            return DRV_AT_PDH_SIGNAL_MASK_SET_ERROR; 
        }    
        atRet = AtPdhDe1SignalingEnable(atE1Obj , AT_E1_SIGNAL_ENABLE);  
        if (cAtOk !=atRet)
        {
            DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtPdhDe1SignalingEnable() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
            return DRV_AT_PDH_SIGNAL_ENABLE_ERROR; 
        }    
    } 
    else if ((AT_E1_PCM31 == frameType) ||(AT_E1_PCM31CRC == frameType))   /*unframe & pcm31& pcm31crc*/ 
    { 
        atRet = AtPdhDe1SignalingIsEnabled(atE1Obj);
        if(cAtTrue == atRet)
        {
            atRet = AtPdhDe1SignalingEnable(atE1Obj , AT_E1_SIGNAL_DISABLE);  
            if (cAtOk != atRet)
            {
                DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtPdhDe1SignalingEnable() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
                return DRV_AT_PDH_SIGNAL_ENABLE_ERROR;
            }      
        }
    }
    g_atE1PortStats[chipId][e1LinkNo].frameMode = frameType;
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtE1FrameTypeGet
* 功能描述:   获取E1端口的帧格式
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtE1FrameTypeGet(BYTE chipId,WORD32 e1LinkNo,BYTE *frameType)
{
    WORD32 ret = DRV_AT_SUCCESS ;  
    eBool  isSignaling = cAtTrue ;
    AtPdhDe1 atE1Obj = NULL ;  
    WORD16 atFrameType = 0;

    CHECK_AT_CHIP_ID(chipId);  
    CHECK_AT_E1LINK_ID(e1LinkNo);
    CHECK_AT_POINTER_NULL(frameType);
    *frameType = AT_E1_UNKNOWN_FRAME ;
    /* Get object to configure */   
    ret = DrvAtE1ObjGet(chipId,e1LinkNo,&atE1Obj); 
    if(DRV_AT_SUCCESS != ret) 
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, E1 OBJ GET FAILED !!\n", __FILE__, __LINE__);   
        return ret;  
    } 
    CHECK_AT_POINTER_NULL(atE1Obj);
    /*get frame type : unframe ,frame basic and frame crc */
    atFrameType = AtPdhChannelFrameTypeGet((AtPdhChannel)atE1Obj);
    isSignaling = AtPdhDe1SignalingIsEnabled((AtPdhDe1)atE1Obj);
    
    if(cAtPdhE1UnFrm == atFrameType )                /*unframe*/   
    { 
        *frameType = AT_E1_UNFRAME ;    
    }  
    else if (cAtPdhE1Frm == atFrameType)                /*basic frame*/  
    {
        if(cAtTrue == isSignaling)
        {
            *frameType = AT_E1_PCM30; 
        }
        else
        {
            *frameType = AT_E1_PCM31; 
        }
    }   
    else if(cAtPdhE1MFCrc == atFrameType)               /*crc frame*/
    { 
        if(cAtTrue == isSignaling)
        {
            *frameType = AT_E1_PCM30CRC; 
        }
        else
        {
            *frameType = AT_E1_PCM31CRC; 
        }        
    }
    if(AT_E1_UNKNOWN_FRAME == *frameType)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, FrameType Get Failed,atFrameType = %u\n", __FILE__, __LINE__,atFrameType);   
        return DRV_AT_PDH_FRAME_GET_ERROR; 
    }
    return ret ;
}


/*******************************************************************************
* 函数名称:   DrvAtE1PwBindCheck
* 功能描述:   检查这条e1有没有绑定pw
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtE1BindPwIdGet(BYTE chipId,WORD32 e1LinkNo,WORD32* pwId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 i = 0;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    CHECK_AT_POINTER_NULL(pwId);
    *pwId = INVALID_PW_ID;
    
    for(i = 0 ; i< AT_MAX_PW_NUM_PER_SUBCARD ; i++)
    {
        if((g_atE1PwInfo[chipId][i].chipId == chipId)
        &&(g_atE1PwInfo[chipId][i].e1LinkNum == e1LinkNo)
        &&(g_atE1PwInfo[chipId][i].pwStatus == PW_USED))
        {
            *pwId = i;
            break ;            
        }  
    }  
    if(INVALID_PW_ID == (*pwId))
    {
       ret = DRV_AT_PDH_NOT_BIND_PW;
    }
    
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtE1ClkModeSet
* 功能描述:   E1的时钟模式设置
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtE1ClkModeSet(BYTE chipId,WORD32 e1LinkNo,BYTE clkMode,WORD32 srcE1LinkNo,WORD32 pwId)
{  
    WORD32 ret = DRV_AT_SUCCESS ; 
    eAtRet atRet = cAtOk ;
    AtPdhDe1 atE1Obj = NULL ;  
    AtPdhDe1 atClkSrcE1Obj = NULL ;  
    AtPw atPwObj = NULL;
    eAtTimingMode atClkMode = 0 ;

    CHECK_AT_CHIP_ID(chipId);  
    CHECK_AT_E1LINK_ID(e1LinkNo);
    CHECK_AT_PW_ID(pwId);
    DRV_AT_E1_PRINT(DRV_AT_E1_CLK_SET,"[CE1TAN][CLKMODE SET]:%s line %d,chipId is %u,e1LinkNo is %u,clkMode is %u,srcE1LinkNo is %u,pwId is %u\n",__FILE__,__LINE__,chipId,e1LinkNo,clkMode,srcE1LinkNo,pwId);
    /* Get object to configure */   
    ret = DrvAtE1ObjGet(chipId,e1LinkNo,&atE1Obj);   
    if(DRV_AT_SUCCESS != ret) 
    {  
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, E1 OBJ GET FAILED !!\n", __FILE__, __LINE__); 
        return ret;   
    }  

    /*select clk mode*/
    switch (clkMode)
    {
        case SYS_CLK_MODE :
            atClkMode = cAtTimingModeSys ; 
            break ;
        case ACR_CLK_MODE :
            atClkMode = cAtTimingModeAcr ;
            break ;
        case DCR_CLK_MODE :
            atClkMode = cAtTimingModeDcr ;
            break ;
        case SLAVE_CLK_MODE :
            atClkMode = cAtTimingModeSlave ;
            break ;
        case LOOP_CLK_MODE :
            atClkMode = cAtTimingModeLoop ;
            break ;
        default :
            atClkMode = cAtTimingModeUnknown ;
            break ;
    }

    if((SYS_CLK_MODE == clkMode)||(LOOP_CLK_MODE == clkMode))
    {
        atRet = AtChannelTimingSet ((AtChannel)atE1Obj,atClkMode,NULL) ;    
        if (cAtOk != atRet) 
        { 
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtChannelTimingSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
            return DRV_AT_PDH_TIMING_SET_ERROR; 
        }    
    }
    else if((ACR_CLK_MODE == clkMode)||(DCR_CLK_MODE == clkMode))
    {     
        ret = DrvAtPwObjGet(chipId,pwId,&atPwObj);                                /*get pw obj*/
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PW OBJECT  GET  FAILED ! !!CHIPID IS %u, PW ID IS %u \n", __FILE__, __LINE__,chipId,pwId); 
            return ret; 
        }
        CHECK_AT_POINTER_NULL(atPwObj);
        atRet = AtChannelTimingSet ((AtChannel)atE1Obj,atClkMode,(AtChannel)atPwObj) ;    
        if (cAtOk != atRet) 
        { 
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtChannelTimingSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
            return DRV_AT_PDH_TIMING_SET_ERROR; 
        }
    }
    else if(SLAVE_CLK_MODE == clkMode)
    {
        ret = DrvAtE1ObjGet(chipId,srcE1LinkNo,&atClkSrcE1Obj);                  
        if(DRV_AT_SUCCESS != ret) 
        { 
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, E1 OBJ GET FAILED !!\n", __FILE__, __LINE__); 
            return ret; 
        }     
        CHECK_AT_POINTER_NULL(atClkSrcE1Obj);
        atRet = AtChannelTimingSet ((AtChannel)atE1Obj,atClkMode,(AtChannel)atClkSrcE1Obj) ;    
        if (cAtOk != atRet) 
        { 
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtChannelTimingSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
            return DRV_AT_PDH_TIMING_SET_ERROR; 
        }    
    }
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtE1ClkModeGet
* 功能描述:  获取 E1的时钟模式设置
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtE1ClkModeGet(BYTE chipId,WORD32 e1LinkNo,BYTE *clkMode)
{
    WORD32 ret = DRV_AT_SUCCESS ; 
    eAtTimingMode atTimingMode = 0 ;
    AtPdhDe1 atE1Obj = NULL ;  

    CHECK_AT_CHIP_ID(chipId);  
    CHECK_AT_E1LINK_ID(e1LinkNo);
    CHECK_AT_POINTER_NULL(clkMode);
    *clkMode = UNKNOWN_CLK_MODE ;
    
    /* Get object to configure */   
    ret = DrvAtE1ObjGet(chipId,e1LinkNo,&atE1Obj);   
    if(DRV_AT_SUCCESS != ret) 
    {  
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, E1 OBJ GET FAILED !!\n", __FILE__, __LINE__); 
        return ret;   
    }
    atTimingMode = AtChannelTimingModeGet((AtChannel)atE1Obj);
    /*get clk mode*/
    switch (atTimingMode)
    {
        case cAtTimingModeSys :
            *clkMode = SYS_CLK_MODE ; 
            break ;
        case cAtTimingModeAcr :
            *clkMode = ACR_CLK_MODE ;
            break ;
        case cAtTimingModeDcr :
            *clkMode = DCR_CLK_MODE ;
            break ;
        case cAtTimingModeSlave :
            *clkMode = SLAVE_CLK_MODE ;
            break ;
        case cAtTimingModeLoop :
            *clkMode = LOOP_CLK_MODE ;
            break ;
        default :
            break ;
    }
    if(UNKNOWN_CLK_MODE == *clkMode)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, TimingMode Get Failed,atTimingMode = %u\n", __FILE__, __LINE__,atTimingMode);   
        return DRV_AT_PDH_TIMING_GET_ERROR; 
    }
    return ret;
}


/*******************************************************************************
* 函数名称:   DrvAtE1ClkModeGet
* 功能描述:   更改E1的时钟模式设置(暂不支持)
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtE1ClkModeUpdate(BYTE chipId,WORD32 e1LinkNo ,BYTE newClkMode)
{
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE oldClkMode = 0;
    WORD32 pwId = 0;
    WORD32 srcPwId = 0;
    
    ret = DrvAtE1BindPwIdGet(chipId,e1LinkNo,&pwId);   /*检查该E1有没有与pw 绑定*/
    if(DRV_AT_SUCCESS != ret) 
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, THIS E1[%u] NOT BIND WITH ANY PW !!\n", __FILE__, __LINE__,e1LinkNo);   
        return ret;  
    } 
    ret = DrvAtE1ClkModeGet(chipId,e1LinkNo,&oldClkMode);
    if(DRV_AT_SUCCESS != ret) 
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, DrvAtE1ClkModeGet() FAILED !!\n", __FILE__, __LINE__);   
        return ret;  
    } 
    DRV_AT_E1_PRINT(DRV_AT_E1_CLK_SET,"[CE1TAN][CLKMODE UPDATE]:%s line %d,chipId is %u,e1LinkNo is %u,Old clkMode is %u,New clkMode is %u",__FILE__, __LINE__,chipId,e1LinkNo,oldClkMode,newClkMode);
    if(oldClkMode == newClkMode)   /*如果新的时钟模式与之前的相同，返回成功*/
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d,This timingMode has already exsited!\n", __FILE__, __LINE__);  
        return DRV_AT_SUCCESS;
    }
    
    ret = DrvAtE1ClkModeSet(chipId,e1LinkNo,newClkMode,0,pwId);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, E1 CLK MODE  SET  FAILED ! !! \n", __FILE__, __LINE__); 
        return ret; 
    }
    /*更新表项*/
    if((SYS_CLK_MODE == oldClkMode)&&((ACR_CLK_MODE == newClkMode)||(DCR_CLK_MODE == newClkMode)))    /*sys -> acr/dcr*/
    {
        DrvAtPwClkModeInfoUpdate(chipId,e1LinkNo,newClkMode);
        g_atE1PwInfo[chipId][pwId].isClkSrc = CE1_TURE;
    }
    if(((ACR_CLK_MODE == oldClkMode)||(DCR_CLK_MODE == oldClkMode))&&(SYS_CLK_MODE == newClkMode))       /*acr/dcr -> sys*/
    {
        DrvAtPwClkModeInfoUpdate(chipId,e1LinkNo,newClkMode);
        if(SATOP_MODE == g_atE1PwInfo[chipId][pwId].servMode)        /*更新时钟模式不会更新业务类型，所以可从该条e1绑定的任意一条pw或取*/
        {
            g_atE1PwInfo[chipId][pwId].isClkSrc = CE1_FALSE;
        }
        else if(CESOP_MODE == g_atE1PwInfo[chipId][pwId].servMode)
        {
            ret = DrvAtNoneClkSrcPwIdFind(chipId,e1LinkNo,&srcPwId);
            if(DRV_AT_INVALID_PW_ID == ret)                                             
            {
                DRV_AT_E1_PRINT(DRV_AT_E1_CLK_SET,"[CE1TAN][CLKMODE UPDATE]: %s line %d, CAN NOT FIND ANY PW ID FOR CLK RECOVERY !!CHIP ID IS %u e1LinkNo IS %u\n", __FILE__, __LINE__,chipId, e1LinkNo);
                return DRV_AT_SUCCESS;
            }
            g_atE1PwInfo[chipId][srcPwId].isClkSrc = CE1_FALSE;       
        }
    }
    return ret ;
}



/*******************************************************************************
* 函数名称:   DrvAtE1ClkStateGet
* 功能描述:   获取 E1的时钟装态
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtE1ClkStateGet(BYTE chipId,WORD32 e1LinkNo,WORD32*clkState)
{
    WORD32 ret = DRV_AT_SUCCESS;
    AtPdhDe1 atE1Obj = NULL ; 
    eAtClockState atClkStatus = 0;
    
    CHECK_AT_CHIP_ID(chipId);  
    CHECK_AT_E1LINK_ID(e1LinkNo);
    CHECK_AT_POINTER_NULL(clkState);
    
    *clkState = CLK_UNKNOWN_STATUS;
    /* Get object to configure */   
    ret = DrvAtE1ObjGet(chipId,e1LinkNo,&atE1Obj);   
    if(DRV_AT_SUCCESS != ret) 
    {  
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, E1 OBJ GET FAILED !!\n", __FILE__, __LINE__); 
        return ret;   
    }
    atClkStatus = AtChannelClockStateGet((AtChannel)atE1Obj);
    
    if (cAtClockStateNotApplicable == atClkStatus)
    {
        *clkState = CLK_NOT_APPLICABLE;
    }
    else if (cAtClockStateInit == atClkStatus)
    {
        *clkState = CLK_INIT_STATUS;
    }
    else if (cAtClockStateHoldOver == atClkStatus)
    {
        *clkState = CLK_HOLDOVER_STATUS;
    }
    else if (cAtClockStateLearning == atClkStatus)
    {
        *clkState = CLK_LEARNING_STATUS;
    }
    else if (cAtClockStateLocked == atClkStatus)
    {
        *clkState = CLK_LOCKED_STATUS;
    }
    else
    {
        *clkState = CLK_UNKNOWN_STATUS;
    }
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtE1TimingSrcGet
* 功能描述:  获取E1的时钟源
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtE1TimingSrcGet(BYTE chipId,WORD32 e1LinkNo,BYTE*sourceType,WORD32* sourceId)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    BYTE clkMode = 0 ;
    AtPdhDe1 atE1Obj = NULL ;  
    AtChannel atChan = NULL ;
    
    CHECK_AT_CHIP_ID(chipId);  
    CHECK_AT_E1LINK_ID(e1LinkNo);
    CHECK_AT_POINTER_NULL(sourceId);
    CHECK_AT_POINTER_NULL(sourceType);
    
    /* Get object to configure */   
    ret = DrvAtE1ObjGet(chipId,e1LinkNo,&atE1Obj); 
    if(DRV_AT_SUCCESS != ret) 
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, E1 OBJ GET FAILED !!\n", __FILE__, __LINE__);   
        return ret;  
    } 
    CHECK_AT_POINTER_NULL(atE1Obj);
    
    ret = DrvAtE1ClkModeGet(chipId,e1LinkNo,&clkMode);
    if(DRV_AT_SUCCESS != ret) 
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, DrvAtE1ClkModeGet() FAILED !!\n", __FILE__, __LINE__);   
        return ret;  
    } 
    /*get  timingsrc  obj */
    atChan = AtChannelTimingSourceGet((AtChannel)atE1Obj);      
    if(SYS_CLK_MODE == clkMode)  /*if sys mode,timingsrc will be null */ 
    {
        if(NULL == atChan)
        {
            *sourceType = AT_NULL ;              
        }
        else
        {
            DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, TimingSrc Get Failed\n", __FILE__, __LINE__);   
            return DRV_AT_PDH_TIMING_SRC_GET_ERROR; 
        }    
    }
    else if((ACR_CLK_MODE == clkMode)||(DCR_CLK_MODE == clkMode))    /*if acr/dcr mode,timingsrc will be pw obj */  
    {
        *sourceId = AtChannelIdGet(atChan) ;
        *sourceType = AT_PW_OBJ ;
    }
    else if((SLAVE_CLK_MODE == clkMode)||(LOOP_CLK_MODE == clkMode))  /*if slave mode,timingsrc will be pdh obj */  
    {
        *sourceId = AtChannelIdGet(atChan) ;
        *sourceType = AT_PDH_OBJ ;
    }

    return ret ;
}


/*******************************************************************************
* 函数名称:   DrvAtE1ClkInfoGet
* 功能描述:   获取 主时钟E1的时钟信息
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtE1ClkInfoGet(BYTE chipId,WORD32 e1LinkNo,T_BSP_SDH_TRIB_TIMING_INFO *e1ClkInfo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 timingStatus = 0;
    WORD32 pwId = 0;
    WORD32 pwNum = 0;
    DRV_AT_PW_PARA pwInfo;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    CHECK_AT_POINTER_NULL(e1ClkInfo);
    memset(&pwInfo,0,sizeof(pwInfo));
    memset(e1ClkInfo,0,sizeof(T_BSP_SDH_TRIB_TIMING_INFO));
    
    ret = DrvAtClkSrcPwIdFind(chipId,e1LinkNo,&pwId); /*获取一条e1里作为时钟源的pwid，只针对时钟域内的主时钟e1*/
    if(DRV_AT_SUCCESS != ret) 
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, E1[%u] DO NOT HAVE CLKSRC PW !!\n", __FILE__, __LINE__,e1LinkNo);   
        return ret;  
    } 
    ret = DrvAtPwInfoGetFromPwId(chipId,pwId,&pwInfo);
    e1ClkInfo->dwClkDomainNo = pwInfo.clkDomainId;
    if(SATOP_MODE == pwInfo.servMode)
    {
        e1ClkInfo->dwPwNum = 1;
    }
    else if (CESOP_MODE == pwInfo.servMode)
    {
        ret = DrvAtCesopLinksNumGet(chipId,e1LinkNo,&pwNum);
        e1ClkInfo->dwPwNum = pwNum;
    }
    
    e1ClkInfo->dwMasterPwId = pwInfo.cip;
    if(ACR_CLK_MODE == pwInfo.clockMode)
    {
        e1ClkInfo->dwClkMode = BSP_SDH_TRIB_ACR_TIMING;
    }
    else if (DCR_CLK_MODE == pwInfo.clockMode)
    {
        e1ClkInfo->dwClkMode = BSP_SDH_TRIB_DCR_TIMING;   
    }
    else if(SYS_CLK_MODE  == pwInfo.clockMode)
    {
        e1ClkInfo->dwClkMode = BSP_SDH_TRIB_SYSTEM_TIMING;
    }
    else if(LOOP_CLK_MODE == pwInfo.clockMode)
    {
        e1ClkInfo->dwClkMode = BSP_SDH_TRIB_LOOP_TIMING;
    }
      
    ret = DrvAtE1ClkStateGet(chipId,e1LinkNo,&timingStatus);
    if (CLK_NOT_APPLICABLE == timingStatus)
    {
        e1ClkInfo->dwClkState  = BSP_TRIB_TIMING_UNLOCKED;
    }
    else if ((CLK_INIT_STATUS == timingStatus)
        ||(CLK_HOLDOVER_STATUS == timingStatus)
        ||(CLK_LEARNING_STATUS == timingStatus))
    {
        e1ClkInfo->dwClkState  = BSP_TRIB_TIMING_FOLLOWING;
    }
    else if (CLK_LOCKED_STATUS == timingStatus)
    {
        e1ClkInfo->dwClkState  = BSP_TRIB_TIMING_LOCKED;
    }   
    
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtE1ClkDomainNoGet
* 功能描述:   获取 E1的所在时钟域号
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtE1ClkDomainNoGet(T_BSP_SRV_FUNC_TDM_E1_CLK_INFOARG *e1ClkdomainInfo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE chipId = 0;
    WORD32 e1LinkNo = 0;
    WORD32 pwId = 0;
    WORD32 clkDomainNo = 0;
    
    
    CHECK_AT_POINTER_NULL(e1ClkdomainInfo);
    chipId = e1ClkdomainInfo->dwSubSlotId -1;
    e1LinkNo = e1ClkdomainInfo->dwStmId - 1;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    
    for(pwId = 0;pwId< AT_MAX_PW_NUM_PER_SUBCARD; pwId++)
    {
        if((e1LinkNo == g_atE1PwInfo[chipId][pwId].e1LinkNum)
        &&(PW_USED == g_atE1PwInfo[chipId][pwId].pwStatus))
        {
            clkDomainNo = g_atE1PwInfo[chipId][pwId].clkDomainId;
            break;
        }
    }
    e1ClkdomainInfo->dwDomainId = clkDomainNo;
    
    return ret;
}


/*******************************************************************************
* 函数名称:   DrvAtE1LineClkRecvSrcSet
* 功能描述:   设置某一个端口的线路时钟为fpga恢复源
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtE1LineClkRecvSrcSet(BYTE chipId,WORD32 e1LinkNo,BYTE isEnable)
{
    WORD32 ret = DRV_AT_SUCCESS;
    eAtRet atRet = cAtOk;
    AtDevice atDevHdl = NULL;
    AtModuleClock atclkModule = NULL;
    AtClockExtractor clkExtractor =  NULL;
    AtPdhDe1 atE1Obj = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    atDevHdl = DrvAtDevHdlGet(chipId);
    CHECK_AT_POINTER_NULL(atDevHdl);
    /* Get clock extractor and the line want to extract clock */
    atclkModule = (AtModuleClock)AtDeviceModuleGet(atDevHdl, cAtModuleClock);
    clkExtractor = AtModuleClockExtractorGet(atclkModule, 0);/*e1 card :extractorId = 0*/
    
    DRV_AT_E1_PRINT(DRV_AT_LINECLK_RECV,"[CE1TAN][LINECLK RECVOERY]:%s line %d,CHIP ID IS %u,E1LINKNO IS %u,IS ENABLE? :%u",__FILE__, __LINE__,chipId,e1LinkNo,isEnable);
    ret = DrvAtE1ObjGet(chipId,e1LinkNo,&atE1Obj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, E1 OBJ GET FAILED !!\n", __FILE__, __LINE__); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atE1Obj);
    /* Extract */
    if(CE1_TURE == isEnable)
    {
        atRet = AtClockExtractorPdhDe1LiuClockExtract(clkExtractor,atE1Obj);
        if(cAtOk != atRet)
        {
            DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtClockExtractorPdhDe1LiuClockExtract()  FAILED !!ret =%s \n", __FILE__, __LINE__,AtRet2String(atRet)); 
            return ret; 
        }
        atRet = AtClockExtractorEnable(clkExtractor, cAtTrue);
        if(cAtOk != atRet)
        {
            DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtClockExtractorEnable()  FAILED !!ret =%s \n", __FILE__, __LINE__,AtRet2String(atRet)); 
            return ret; 
        }
    }
    else if (CE1_FALSE == isEnable)
    {
        atRet = AtClockExtractorEnable(clkExtractor, cAtFalse);
        if(cAtOk != atRet)
        {
            DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtClockExtractorEnable()  FAILED !!ret =%s \n", __FILE__, __LINE__,AtRet2String(atRet)); 
            return ret; 
        }
    }
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtE1LineClkRecvSrcGet
* 功能描述:   获取 线路时钟恢复源
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtE1LineClkRecvSrcGet(BYTE chipId,WORD32* e1LinkNo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    AtDevice atDevHdl = NULL;
    AtModuleClock atclkModule = NULL;
    AtClockExtractor clkExtractor =  NULL;
    AtPdhDe1 atE1Obj = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(e1LinkNo);
    atDevHdl = DrvAtDevHdlGet(chipId);
    CHECK_AT_POINTER_NULL(atDevHdl);
    
    atclkModule = (AtModuleClock)AtDeviceModuleGet(atDevHdl, cAtModuleClock);
    if(NULL == atclkModule)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CLOCK MODULE GET FAILED !! \n", __FILE__, __LINE__); 
        return DRV_AT_CLK_MODULE_GET_FAILED; 
    }
    clkExtractor = AtModuleClockExtractorGet(atclkModule,0);
    if(NULL == clkExtractor)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CLOCK EXTRACTOR GET FAILED !! \n", __FILE__, __LINE__); 
        return DRV_AT_CLK_EXTRACTOR_GET_FAILED; 
    }  
    atE1Obj = AtClockExtractorPdhDe1Get(clkExtractor);
    if(NULL == atE1Obj)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CLOCK EXTRACTOR SOURCE IS NULL !! \n", __FILE__, __LINE__); 
        return DRV_AT_CLK_EXTRACTOR_SRC_GET_FAILED; 
    }
    *e1LinkNo = 0;    
    *e1LinkNo = AtChannelIdGet((AtChannel)atE1Obj);
    DRV_AT_E1_PRINT(DRV_AT_LINECLK_RECV,"[CE1TAN][LINECLK RECVOERY]:%s line %d,CHIP ID IS %u,LINECLK RECV SRC IS E1LINKNO %u",__FILE__, __LINE__,chipId,*e1LinkNo);
    
    return ret;
}



/*******************************************************************************
* 函数名称:   DrvAtE1LineClkLockedCheck
* 功能描述:   获取 线路时钟的锁定状态
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtE1LineClkLockedCheck(BYTE chipId,BYTE*isLocked)
{
    WORD32 ret = DRV_AT_SUCCESS;
    eBool atRet = cAtFalse;
    AtDevice atDevHdl = NULL;
    AtModuleClock atclkModule = NULL;
    AtClockExtractor clkExtractor =  NULL;

    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(isLocked);
    atDevHdl = DrvAtDevHdlGet(chipId);
    CHECK_AT_POINTER_NULL(atDevHdl);

    atclkModule = (AtModuleClock)AtDeviceModuleGet(atDevHdl, cAtModuleClock);
    if(NULL == atclkModule)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CLOCK MODULE GET FAILED !! \n", __FILE__, __LINE__); 
        return DRV_AT_CLK_MODULE_GET_FAILED; 
    }
    clkExtractor = AtModuleClockExtractorGet(atclkModule,0);
    if(NULL == clkExtractor)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CLOCK EXTRACTOR GET FAILED !! \n", __FILE__, __LINE__); 
        return DRV_AT_CLK_EXTRACTOR_GET_FAILED; 
    }  
    
    atRet = AtClockExtractorPdhDe1LiuClockIsExtracted(clkExtractor);
    if(cAtTrue == atRet)
    {
        *isLocked = CE1_TURE;
    }
    else if(cAtFalse == atRet)
    {
        *isLocked = CE1_FALSE;
    }
    
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtE1LoopBackSet
* 功能描述:   E1 通道环回设置
* 输入参数:   
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-17     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtE1LoopBackSet(BYTE chipId,WORD32 e1LinkNo,BYTE loopbackMode)
{ 
    WORD32 ret = DRV_AT_SUCCESS ;
    eBool isSupport = cAtTrue  ;
    eAtRet atRet = cAtOk ;
    AtChannel atE1ChanInfo = NULL ;  
    AtPdhDe1 atE1Obj = NULL ;
    
    CHECK_AT_CHIP_ID(chipId);  
    CHECK_AT_E1LINK_ID(e1LinkNo);
    
    /* Get object to configure */  
    ret = DrvAtE1ObjGet(chipId,e1LinkNo,&atE1Obj)   ;
    if(DRV_AT_SUCCESS != ret)
    {      
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, E1 OBJ GET FAILED !!\n", __FILE__, __LINE__); 
        return ret; 
    }  
    CHECK_AT_POINTER_NULL(atE1Obj);
    atE1ChanInfo = (AtChannel)atE1Obj;
    isSupport = AtChannelLoopbackIsSupported(atE1ChanInfo,loopbackMode) ;    /*判断loopback mode  是否支持*/  
    if(cAtTrue != isSupport)   
    { 
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, LOOPBACK MODE IS NOT SUPPORTED !!\n", __FILE__, __LINE__); 
        return DRV_AT_PDH_LOOPBACK_MODE_NOT_SUPPORT; 
    }
    atRet = AtChannelLoopbackSet(atE1ChanInfo,loopbackMode);          /* 0: No loopback 1: Local line loopback  2: Local payload loopback 3: Remote line loopback 4: Remote payload loopback */
    if (cAtOk!= atRet)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtChannelLoopbackSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_PDH_LOOPBACK_SET_ERROR ;
    }       
    
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtE1LoopBackGet
* 功能描述:   E1 通道环回设置
* 输入参数:   
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-17     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtE1LoopBackGet(BYTE chipId,WORD32 e1LinkNo,BYTE* loopbackMode)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    BYTE atLoopbackMode = 0;
    AtPdhDe1 atE1Obj = NULL ;
    
    CHECK_AT_CHIP_ID(chipId);  
    CHECK_AT_E1LINK_ID(e1LinkNo);
    CHECK_AT_POINTER_NULL(loopbackMode);
    * loopbackMode = AT_E1_UNKONWN_LOOPBACK ;
    /* Get object to configure */  
    ret = DrvAtE1ObjGet(chipId,e1LinkNo,&atE1Obj)   ;
    if(DRV_AT_SUCCESS != ret)
    {      
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, E1 OBJ GET FAILED !!\n", __FILE__, __LINE__); 
        return ret; 
    }  
    CHECK_AT_POINTER_NULL(atE1Obj);
    atLoopbackMode = AtChannelLoopbackGet((AtChannel)atE1Obj);
    switch(atLoopbackMode) 
    {
        case cAtPdhLoopbackModeRelease:                                  /*取消环回*/
            *loopbackMode = AT_E1_LOOPBACK_CANCEL;
            break ;
        case cAtPdhLoopbackModeLocalLine:                       /*端口内环，向后拼环回*/
            *loopbackMode = AT_E1_LINE_LOOPBACK_LOCAL;       
            break;
        case cAtPdhLoopbackModeRemoteLine:                               /*端口外环*/
            *loopbackMode = AT_E1_LINE_LOOPBACK_REMOTE;     
            break; 
        case cAtPdhLoopbackModeLocalPayload:                       /*framer 内环，向后拼环回*/
            *loopbackMode = AT_E1_FRAMER_LOOPBACK_LOCAL;      
            break;
        case cAtPdhLoopbackModeRemotePayload:                          /*framer 外环，向e1端口环回*/
            *loopbackMode = AT_E1_FRAMER_LOOPBACK_REMOTE;
            break;   
        default:
            break;
    } 
    if(AT_E1_UNKONWN_LOOPBACK == *loopbackMode)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, LoopbackMode Get Failed,atLoopbackMode = %u\n", __FILE__, __LINE__,atLoopbackMode);   
        return DRV_AT_PDH_LOOPBACK_GET_ERROR; 
    }
    return ret ;
 
}

/*******************************************************************************
* 函数名称:   DrvAtE1LoopBackOp
* 功能描述:   E1 环回设置
* 输入参数:   
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-17     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtE1LoopBackOp(BYTE chipId,WORD32 e1LinkNo,BYTE loopbackMode)
{ 
    WORD32 ret = DRV_AT_SUCCESS ;  
    BYTE atLoopbackMode = 0;
    
    switch(loopbackMode) 
    {
        case AT_E1_LOOPBACK_CANCEL:                                   /*取消环回*/
            atLoopbackMode = cAtPdhLoopbackModeRelease;
            break ;
        case AT_E1_LINE_LOOPBACK_LOCAL:                       /*端口内环，向后拼环回*/
            atLoopbackMode = cAtPdhLoopbackModeLocalLine;       
            break;
        case AT_E1_LINE_LOOPBACK_REMOTE:                               /*端口外环*/
            atLoopbackMode = cAtPdhLoopbackModeRemoteLine;     
            break; 
        case AT_E1_FRAMER_LOOPBACK_LOCAL:                       /*framer 内环，向e1端口环回*/
            atLoopbackMode = cAtPdhLoopbackModeLocalPayload;      
            break;
        case AT_E1_FRAMER_LOOPBACK_REMOTE:                              /*framer 外环，向后拼环回*/
            atLoopbackMode = cAtPdhLoopbackModeRemotePayload;
            break;   
        default:
            break;
    } 
    ret = DrvAtE1LoopBackSet(chipId,e1LinkNo,atLoopbackMode);
    if (DRV_AT_SUCCESS != ret)  
    {  
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, DrvAtE1LoopBackSet() ret=0x%x.\n", __FILE__, __LINE__, ret);   
        return ret; 
    }      
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtE1StausGet
* 功能描述:   查看E1状态
* 输入参数:   
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-17     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtE1StausGet(BYTE chipId,WORD32 e1LinkNo,BYTE*isEnable )
{
    WORD32 ret = DRV_AT_SUCCESS ;
    AtPdhDe1 atE1Obj = NULL ;
    
    CHECK_AT_CHIP_ID(chipId);  
    CHECK_AT_E1LINK_ID(e1LinkNo);
    CHECK_AT_POINTER_NULL(isEnable);
    
    /* Get object to configure */  
    ret = DrvAtE1ObjGet(chipId,e1LinkNo,&atE1Obj)   ;
    if(DRV_AT_SUCCESS != ret)
    {      
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, E1 OBJ GET FAILED !!\n", __FILE__, __LINE__); 
        return ret; 
    }  
    *isEnable = AtChannelIsEnabled((AtChannel)atE1Obj);
    
    return ret ;

}


/*******************************************************************************
* 函数名称:   DrvAtEthPortObjGet
* 功能描述:   根据ethport id 获取一个Eth Port Obj
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvAtEthPortObjGet(BYTE chipId,BYTE  ethPortId,AtEthPort*atEthPort)
{ 
    WORD32 ret = DRV_AT_SUCCESS ;   
    AtModuleEth ethModule = NULL ;   
    AtDevice atDevHdl = NULL ;
    
    CHECK_AT_POINTER_NULL(atEthPort); 
    atDevHdl = DrvAtDevHdlGet(chipId) ;
    CHECK_AT_POINTER_NULL(atDevHdl)  ;
    
    ethModule = (AtModuleEth)AtDeviceModuleGet(atDevHdl, cAtModuleEth);
    if(NULL == ethModule) 
    { 
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ETH MODULE GET FAILED  !!\n", __FILE__, __LINE__); 
        return DRV_AT_ETH_MOUDULE_GET_FAILED; 
    }   
    *atEthPort = AtModuleEthPortGet(ethModule, ethPortId) ;     
    if(NULL == (*atEthPort))
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ETH PORT GET FAILED  !!ETH PORT ID IS %u \n", __FILE__, __LINE__,ethPortId); 
        return DRV_AT_ETH_PORT_GET_FAILED;  
    }
    
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtEthSerdesCfg
* 功能描述:   以太端口硬件参数配置
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0     chenbo    create
********************************************************************************/ 
WORD32 DrvAtEthSerdesCfg(BYTE chipId, BYTE ethPortId, WORD32 phyParameterId, WORD32 value)
{
    WORD32 ret = DRV_AT_SUCCESS;
    eAtRet atRet = cAtOk;
    AtEthPort ethPortObj = NULL;      /* Ethernet port */
    AtSerdesController serdesCtrl = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_ETHPORT_ID(ethPortId);
    
    ret = DrvAtEthPortObjGet(chipId,ethPortId,&ethPortObj);
    if(DRV_AT_SUCCESS != ret)   
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ETH PORT GET FAILED  !!\n", __FILE__, __LINE__); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(ethPortObj);
    serdesCtrl = AtEthPortSerdesController(ethPortObj);
    if(NULL == serdesCtrl)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtEthPortSerdesController() FAILED  !!\n", __FILE__, __LINE__); 
        return DRV_AT_ETH_PORT_CRTLR_GET_FAILED; 
    }
    atRet = AtSerdesControllerPhysicalParamSet(serdesCtrl,phyParameterId,value);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtSerdesControllerPhysicalParamSet()failed ,ret=%s  !!\n", __FILE__, __LINE__,AtRet2String(atRet)); 
        return DRV_AT_ETH_PORT_CFG_FAILED; 
    }
    
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtEthSerdesCfgGet
* 功能描述:   以太端口硬件参数获取
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0     chenbo    create
********************************************************************************/ 
WORD32 DrvAtEthSerdesCfgGet(BYTE chipId, BYTE ethPortId, WORD32 phyParameterId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 value = 0;
    AtEthPort ethPortObj = NULL;      /* Ethernet port */
    AtSerdesController serdesCtrl = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_ETHPORT_ID(ethPortId);
    
    ret = DrvAtEthPortObjGet(chipId,ethPortId,&ethPortObj);
    if(DRV_AT_SUCCESS != ret)   
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ETH PORT GET FAILED  !!\n", __FILE__, __LINE__); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(ethPortObj);
    serdesCtrl = AtEthPortSerdesController(ethPortObj);
    if(NULL == serdesCtrl)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtEthPortSerdesController() FAILED  !!\n", __FILE__, __LINE__); 
        return DRV_AT_ETH_PORT_CRTLR_GET_FAILED; 
    }    
    value = AtSerdesControllerPhysicalParamGet(serdesCtrl,phyParameterId);
    ROSNG_TRACE_DEBUG("PhyParaId is %u,The value is %u\n",phyParameterId,value);
    
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtEthSerdesLoopBack
* 功能描述:   SERDES 环回
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0     chenbo    create
********************************************************************************/ 
WORD32 DrvAtEthSerdesLoopBack(BYTE chipId, BYTE ethPortId,WORD32  loopbackMode)
{
    WORD32 ret = DRV_AT_SUCCESS;
    eAtRet atRet = cAtOk;
    AtEthPort ethPortObj = NULL;      /* Ethernet port */
    AtSerdesController serdesCtrl = NULL;
    WORD32 atLoopBackMode = 0;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_ETHPORT_ID(ethPortId);     
    
    ret = DrvAtEthPortObjGet(chipId,ethPortId,&ethPortObj);
    if(DRV_AT_SUCCESS != ret)   
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ETH PORT GET FAILED  !!\n", __FILE__, __LINE__); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(ethPortObj);
    serdesCtrl = AtEthPortSerdesController(ethPortObj);
    if(NULL == serdesCtrl)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtEthPortSerdesController() FAILED  !!\n", __FILE__, __LINE__); 
        return DRV_AT_ETH_PORT_CRTLR_GET_FAILED; 
    }    
    CHECK_AT_POINTER_NULL(serdesCtrl);
    
    if(AT_SERDES_LOOPBACK_LOCAL == loopbackMode)
    {
        atLoopBackMode = cAtLoopbackModeLocal;
    }
    else if (AT_SERDES_LOOPBACK_REMOTE == loopbackMode)
    {
        atLoopBackMode = cAtLoopbackModeRemote;
    }
    else if(AT_SERDES_LOOPBACK_CANCEL == loopbackMode)
    {
        atLoopBackMode = cAtLoopbackModeRelease;
    }
    atRet = AtSerdesControllerLoopbackSet(serdesCtrl,atLoopBackMode);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtSerdesControllerLoopbackSet()failed ,ret=%s  !!\n", __FILE__, __LINE__,AtRet2String(atRet)); 
        return DRV_AT_ETH_SERDES_LOOP_FAILED; 
    }
       
    return ret;
}


/*******************************************************************************
* 函数名称:   DrvAtEthIntfInit
* 功能描述:   以太端口初始化
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvAtEthIntfInit(BYTE chipId,BYTE ethPortId)
{   
    WORD32 ret = DRV_AT_SUCCESS ;
    eAtRet atRet = cAtOk ;
    BYTE srcMac [6] = {0xCA, 0xFE, 0xCA, 0xFE, 0xCA, 0xFE} ;
    AtEthPort atEthPort = NULL;

    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_ETHPORT_ID(ethPortId);
    
    ret = DrvAtEthPortObjGet(chipId, ethPortId,&atEthPort) ;    /*get eth port*/
    if(DRV_AT_SUCCESS != ret)   
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ETH PORT GET FAILED  !!\n", __FILE__, __LINE__); 
        return ret; 
    }    
    CHECK_AT_POINTER_NULL(atEthPort);    
    /*set default cfg*/
    atRet = AtEthPortSourceMacAddressSet(atEthPort,srcMac) ;         /*smac set*/
     if (cAtOk!= atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtEthPortSourceMacAddressSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_ETH_SMAC_SET_ERROR;
    }    

    atRet = AtEthPortMacCheckingEnable(atEthPort,AT_ETH_MAC_CHECK_DISABLE);          /*mac checking disable*/
    if (cAtOk!= atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtEthPortSourceMacAddressSet() ret=0x%x.\n", __FILE__, __LINE__, atRet);
        return DRV_AT_ETH_MAC_CHECK_SET_ERROR;
    }    
    atRet = AtEthPortInterfaceSet(atEthPort,cAtEthPortInterfaceSgmii); /*eth port type :SGMII*/
    if (cAtOk!= atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtEthPortInterfaceSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_ETH_PORT_TYPE_SET_ERROR;
    }    
    atRet = AtEthPortSpeedSet(atEthPort,cAtEthPortSpeed1000M) ;   /*port speed :1000M */
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtEthPortSpeedSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_ETH_PORT_SPEED_SET_ERROR;
    }      
    atRet = AtEthPortAutoNegEnable(atEthPort, AT_ETH_AUTO_NEG_DISABLE); /*auto neg disable*/
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtEthPortAutoNegEnable() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_ETH_AUTO_NEG_SET_ERROR;
    }    
    atRet = AtEthPortTxIpgSet(atEthPort, AT_ETH_TX_IPG);                            /*Tx ipg set*/
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtEthPortTxIpgSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_ETH_TXIPG_SET_ERROR;
    }    
    atRet = AtEthPortRxIpgSet(atEthPort, AT_ETH_RX_IPG);                            /*Rx ipg set*/
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtEthPortRxIpgSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_ETH_RXIPG_SET_ERROR;
    }    
    ret = DrvAtEthSerdesCfg(chipId,ethPortId,cAtSerdesParamVod,50);
    if(DRV_AT_SUCCESS != ret)   
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ETH PORT CFG FAILED  !!\n", __FILE__, __LINE__); 
        return ret; 
    }    
    
    ROSNG_TRACE_DEBUG("\nETH INTF INIT SUCCESS !!  CHIP ID IS %u .\n", chipId);
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtEthSourceMacSet
* 功能描述:   以太端口smac 设置
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvAtEthSourceMacSet(BYTE chipId,BYTE ethPortId,BYTE* sMac)
{
    WORD32 ret = DRV_AT_SUCCESS;
    eAtRet atRet = cAtOk;
    AtEthPort atEthPort = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_ETHPORT_ID(ethPortId);
    CHECK_AT_POINTER_NULL(sMac);
    
    ret = DrvAtEthPortObjGet(chipId, ethPortId,&atEthPort) ;    /*get eth port*/
    if(DRV_AT_SUCCESS != ret)   
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ETH PORT GET FAILED  !!\n", __FILE__, __LINE__); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atEthPort);
    /*set default cfg*/
    atRet = AtEthPortSourceMacAddressSet(atEthPort,sMac) ;         /*smac set*/
    if (cAtOk!= atRet)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtEthPortSourceMacAddressSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_ETH_SMAC_SET_ERROR;
    }    
    
    return ret;
}


/*******************************************************************************
* 函数名称:   DrvAtEthLoopBackSet
* 功能描述:   以太端口环回设置
* 输入参数:   
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-17     v1.0        chenbo    create
********************************************************************************/
WORD32  DrvAtEthLoopBackSet(BYTE chipId,BYTE ethPortId,BYTE loopbackMode)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    eAtRet atRet = cAtOk ;
    eBool  isSupport = cAtTrue ;
    AtEthPort atEthPort = NULL;
    
    CHECK_AT_CHIP_ID(chipId); 
    CHECK_AT_ETHPORT_ID(ethPortId);
    
    ret = DrvAtEthPortObjGet(chipId, ethPortId, &atEthPort) ;
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ETH PORT GET FAILED  !!\n", __FILE__, __LINE__); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atEthPort);
    
    isSupport = AtChannelLoopbackIsSupported((AtChannel)atEthPort,loopbackMode) ;    /*判断loopback mode  是否支持*/
    if( cAtTrue != isSupport)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, LOOPBACK MODE IS NOT SUPPORTED !!\n", __FILE__, __LINE__); 
        return DRV_AT_ETH_LOOPBACK_MODE_NOT_SUPPORT; 
    }
    atRet = AtChannelLoopbackSet((AtChannel)atEthPort,loopbackMode);          /* 0: No loopback 1: Local loopback  2: Remote  loopback  */
    if (cAtOk!= atRet)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtChannelLoopbackSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_ETH_LOOPBACK_SET_ERROR ;
    } 
    
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtEthLoopBackGet
* 功能描述:   获取Eth 环回设置
* 输入参数:   
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-17     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtEthLoopBackGet(BYTE chipId,BYTE ethPortId,BYTE *loopbackMode)
{
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE atLoopbackMode = 0;
    AtEthPort atEthPort = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_ETHPORT_ID(ethPortId);
    CHECK_AT_POINTER_NULL(loopbackMode);
    *loopbackMode = AT_ETH_UNKONWN_LOOPBACK ;
    
    ret = DrvAtEthPortObjGet(chipId, ethPortId, &atEthPort) ;
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ETH PORT GET FAILED  !!\n", __FILE__, __LINE__); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atEthPort);
    
    atLoopbackMode = AtChannelLoopbackGet((AtChannel)atEthPort);
    switch(atLoopbackMode)
    {
        case cAtLoopbackModeRelease :    /*取消环回*/
            *loopbackMode = AT_ETH_LOOPBACK_CANCEL ;
            break ;
        case cAtLoopbackModeLocal:                                                /*内环，向e1端口环回*/
            *loopbackMode = AT_ETH_LOOPBACK_LOCAL ;
            break ;
        case cAtLoopbackModeRemote:        /*外环，向后拼环回*/
            *loopbackMode = AT_ETH_LOOPBACK_REMOTE ;
            break ;
        default :
            break ;
    }
    if(AT_ETH_UNKONWN_LOOPBACK == *loopbackMode)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, LoopbackMode Get Failed,atLoopbackMode = %u\n", __FILE__, __LINE__,atLoopbackMode);   
        return DRV_AT_ETH_LOOPBACK_GET_ERROR; 
    }
    
    return ret ;
}

    
/*******************************************************************************
* 函数名称:   DrvAtEthLoopBackOp
* 功能描述:   Eth 环回设置
* 输入参数:   
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-17     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtEthLoopBackOp(BYTE chipId,BYTE ethPortId,BYTE loopbackMode)
{
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE atLoopbackMode = 0;

    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_ETHPORT_ID(ethPortId);
    
    switch(loopbackMode)
    {
        case AT_ETH_LOOPBACK_CANCEL :    /*取消环回*/
            atLoopbackMode = cAtLoopbackModeRelease ;
            break ;
        case AT_ETH_LOOPBACK_LOCAL:    /*内环，向e1端口环回*/
            atLoopbackMode = cAtLoopbackModeLocal ;
            break ;
        case AT_ETH_LOOPBACK_REMOTE:        /*外环，向后拼环回*/
            atLoopbackMode = cAtLoopbackModeRemote ;
            break ;
        default :
            break ;
    }
    ret = DrvAtEthLoopBackSet(chipId,ethPortId,atLoopbackMode);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, DrvAtEthPortLoopBack() ret=0x%x.\n", __FILE__, __LINE__, ret);
        return ret;
    }    
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtEthStatusGet
* 功能描述:   获取以太端口状态
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-16     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvAtEthStatusGet(BYTE chipId,BYTE ethPortId,WORD32* status)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    WORD32 ethAlm = 0;
    AtEthPort atEthPort = NULL;

    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_ETHPORT_ID(ethPortId);
    CHECK_AT_POINTER_NULL(status);
    
    ret = DrvAtEthPortObjGet(chipId, ethPortId,&atEthPort);
    if(DRV_AT_SUCCESS != ret)   
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ETH PORT GET FAILED  !!\n", __FILE__, __LINE__); 
        return ret; 
    }    
    CHECK_AT_POINTER_NULL(atEthPort);
    
    ethAlm = AtChannelAlarmGet((AtChannel)atEthPort);
    if(cAtEthPortAlarmLinkUp == ethAlm)
    {
        *status = AT_ETH_PORT_UP ;
    }
    if(cAtEthPortAlarmLinkDown == ethAlm)
    {
        *status = AT_ETH_PORT_DOWN ;
    }
    
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtEthStaticGet
* 功能描述:   获取以太口报文统计
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-16     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvAtEthStaticGet(BYTE chipId,BYTE ethPortId,DRV_AT_ETH_COUNT* ethCouter)
{
    WORD32 ret = DRV_AT_SUCCESS;
    eAtRet atRet = cAtOk ;
    tAtEthPortCounters atEthCouter ;
    AtEthPort atEthPort = NULL;

    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_ETHPORT_ID(ethPortId);
    CHECK_AT_POINTER_NULL(ethCouter);
    
    memset(&atEthCouter, 0, sizeof(atEthCouter));
    memset(ethCouter, 0, sizeof(DRV_AT_ETH_COUNT));
    ret = DrvAtEthPortObjGet(chipId, ethPortId,&atEthPort) ;    /*get eth port*/
    if(DRV_AT_SUCCESS != ret)   
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ETH PORT GET FAILED  !!\n", __FILE__, __LINE__); 
        return ret; 
    }    
    CHECK_AT_POINTER_NULL(atEthPort);
    
    atRet = AtChannelAllCountersClear((AtChannel)atEthPort, &atEthCouter);
    if(cAtOk != atRet)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtChannelAllCountersGet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet)); 
        return atRet; 
    }
    ethCouter->txPackets = atEthCouter.txPackets ; 
    ethCouter->txBytes = atEthCouter.txBytes;
    ethCouter->txOamPackets = atEthCouter.txOamPackets ;
    ethCouter->txPeriodOamPackets = atEthCouter.txPeriodOamPackets ;
    ethCouter->txPwPackets = atEthCouter.txPwPackets ;
    ethCouter->txPacketsLen0_64 = atEthCouter.txPacketsLen0_64 ;
    ethCouter->txPacketsLen65_127 = atEthCouter.txPacketsLen65_127 ;
    ethCouter->txPacketsLen128_255 = atEthCouter.txPacketsLen128_255 ;
    ethCouter->txPacketsLen256_511 = atEthCouter.txPacketsLen256_511 ;
    ethCouter->txPacketsLen512_1024 = atEthCouter.txPacketsLen512_1024 ;
    ethCouter->txPacketsLen1025_1528 = atEthCouter.txPacketsLen1025_1528 ;
    ethCouter->txPacketsJumbo = atEthCouter.txPacketsJumbo ;
    ethCouter->txTopPackets = atEthCouter.txTopPackets ;
    ethCouter->rxPackets = atEthCouter.rxPackets ;
    ethCouter->rxBytes = atEthCouter.rxBytes ;
    ethCouter->rxErrEthHdrPackets = atEthCouter.rxErrEthHdrPackets ;
    ethCouter->rxErrBusPackets = atEthCouter.rxErrBusPackets ;
    ethCouter->rxErrFcsPackets = atEthCouter.rxErrFcsPackets ;
    ethCouter->rxOversizePackets = atEthCouter.rxOversizePackets ;
    ethCouter->rxUndersizePackets = atEthCouter.rxUndersizePackets ;
    ethCouter->rxPacketsLen0_64 = atEthCouter.rxPacketsLen0_64 ;
    ethCouter->rxPacketsLen65_127 = atEthCouter.rxPacketsLen65_127 ;
    ethCouter->rxPacketsLen128_255 = atEthCouter.rxPacketsLen128_255 ;
    ethCouter->rxPacketsLen256_511 = atEthCouter.rxPacketsLen256_511 ;
    ethCouter->rxPacketsLen512_1024 = atEthCouter.rxPacketsLen512_1024 ;
    ethCouter->rxPacketsLen1025_1528 = atEthCouter.rxPacketsLen1025_1528 ;
    ethCouter->rxPacketsJumbo = atEthCouter.rxPacketsJumbo ;
    ethCouter->rxPwUnsupportedPackets = atEthCouter.rxPwUnsupportedPackets ;
    ethCouter->rxErrPwLabelPackets = atEthCouter.rxErrPwLabelPackets ;
    ethCouter->rxDiscardedPackets = atEthCouter.rxDiscardedPackets ;
    ethCouter->rxPausePackets = atEthCouter.rxPausePackets ;
    ethCouter->rxErrPausePackets = atEthCouter.rxErrPausePackets ;
    ethCouter->rxBrdCastPackets = atEthCouter.rxBrdCastPackets ;
    ethCouter->rxMultCastPackets = atEthCouter.rxMultCastPackets ;
    ethCouter->rxArpPackets = atEthCouter.rxArpPackets ;
    ethCouter->rxOamPackets = atEthCouter.rxOamPackets ;
    ethCouter->rxEfmOamPackets = atEthCouter.rxEfmOamPackets ;
    ethCouter->rxErrEfmOamPackets = atEthCouter.rxErrEfmOamPackets ;
    ethCouter->rxEthOamType1Packets = atEthCouter.rxEthOamType1Packets ;
    ethCouter->rxEthOamType2Packets = atEthCouter.rxEthOamType2Packets ;
    ethCouter->rxMplsPackets = atEthCouter.rxMplsPackets ;
    ethCouter->rxErrMplsPackets = atEthCouter.rxErrMplsPackets ;
    ethCouter->rxMplsErrOuterLblPackets = atEthCouter.rxMplsErrOuterLblPackets ;
    ethCouter->rxMplsDataPackets = atEthCouter.rxMplsDataPackets ;
    ethCouter->rxErrPsnPackets = atEthCouter.rxErrPsnPackets ;
    ethCouter->rxPacketsSendToCpu = atEthCouter.rxPacketsSendToCpu ;
    ethCouter->rxPacketsSendToPw = atEthCouter.rxPacketsSendToPw ;


    return ret;
}


/*******************************************************************************
* 函数名称:   DrvAtEthPortSwitch
* 功能描述:   切换以太端口
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-16     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvAtEthPortSwitch(BYTE chipId,BYTE workingPortId,BYTE switchPortId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    eAtRet atRet = cAtOk;
    AtEthPort workingEthPort = NULL;
    AtEthPort switchEthPort = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_ETHPORT_ID(workingPortId);
    CHECK_AT_ETHPORT_ID(switchPortId);
    
    ret = DrvAtEthPortObjGet(chipId,workingPortId,&workingEthPort);
    if(DRV_AT_SUCCESS != ret)   
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ETH PORT GET FAILED  !!\n", __FILE__, __LINE__); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(workingEthPort);
    
    ret = DrvAtEthPortObjGet(chipId,switchPortId,&switchEthPort);
    if(DRV_AT_SUCCESS != ret)   
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ETH PORT GET FAILED  !!\n", __FILE__, __LINE__); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(switchEthPort);
    
    atRet = AtEthPortSwitch(workingEthPort,switchEthPort);
    if(cAtOk != atRet)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtEthPortSwitch() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet)); 
        return DRV_AT_ETH_PORT_SWITCH_FAILED; 
    }
    
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtEthPortSwitchRelease
* 功能描述:   切换以太端口
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-16     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvAtEthPortSwitchRelease(BYTE chipId,BYTE ethPortId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    eAtRet atRet = cAtOk;
    AtEthPort ethPortObj = NULL;
   
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_ETHPORT_ID(ethPortId); 
    
    ret = DrvAtEthPortObjGet(chipId,ethPortId,&ethPortObj);
    if(DRV_AT_SUCCESS != ret)   
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ETH PORT GET FAILED  !!\n", __FILE__, __LINE__); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(ethPortObj);
     
    atRet = AtEthPortSwitchRelease(ethPortObj);
    if(cAtOk != atRet)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtEthPortSwitchRelease() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet)); 
        return DRV_AT_ETH_PORT_RELEASE_FAILED; 
    }
    
    return ret;
}




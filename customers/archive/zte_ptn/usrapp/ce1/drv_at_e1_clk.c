/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_tha_e1_clk.c
* 文件标识: 
* 其它说明: 时钟模块
* 当前版本: V1.0
* 作    者: 陈博10140635.
* 完成日期: 2013-04-25
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/
#include "drv_at_e1_clk.h"
#include "drv_at_e1_port.h"
#include "drv_at_e1_pwe3.h"


/*全局变量*/
AT_CLK_DOMAIN_INFO g_ClkDomainInfo[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_MAX_DOMAIN_NUM_PER_SUBCARD] ;


/*外部函数*/
extern WORD32 DrvAtPwClkInfoUpdate(BYTE chipId,WORD32 e1LinkNo,BYTE newClkState,WORD32 newMasterE1LinkNo,BYTE isClkRsc);
extern DRV_AT_PW_PARA  g_atE1PwInfo [AT_MAX_SUBCARD_NUM_PER_SLOT][AT_MAX_PW_NUM_PER_SUBCARD] ;  



/*******************************************************************************
* 函数名称:   DrvAtClkDomainFreeIndexGet
* 功能描述:   找一个可用的时钟域index
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtClkDomainFreeIndexGet(BYTE chipId,WORD32 *index)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 i = 0;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(index);
    for(i = 0 ;i< AT_MAX_DOMAIN_NUM_PER_SUBCARD ; i++)      
    {
        if(CLK_DOMAIN_DISABLE == g_ClkDomainInfo[chipId][i].domainEnable)  
        {
            *index = i;
            break ;
        }
    }
    if(AT_MAX_DOMAIN_NUM_PER_SUBCARD == i)
    {
        return DRV_AT_CLK_DOMAIN_TABLE_FULL ;
    }
    else
    {
        return ret ;
    }
}


/*******************************************************************************
* 函数名称:   DrvAtClkDomainMemberFreeIndexGet
* 功能描述:   找一个可用的时钟域成员index
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtClkDomainMemberFreeIndexGet(BYTE chipId,WORD32 domainIndex,WORD32* memberIndex)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 i  = 0;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(memberIndex);
    *memberIndex = 0;
    
    if(CLK_DOMAIN_ENABLE != g_ClkDomainInfo[chipId][domainIndex].domainEnable)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CLKDOMAIN[%u] IS NOT ENABLE.\n", __FILE__, __LINE__, g_ClkDomainInfo[chipId][domainIndex].domainId);
        return DRV_AT_CLK_DOMAIN_NOT_ENABLE ;
    }
    
    for(i = 0 ;i < AT_MAX_E1_NUM_PER_DOMAIN;i++)
    {
        if(CE1_DISABLE == g_ClkDomainInfo[chipId][domainIndex].memberInfo[i].isEnable)
        {
            *memberIndex = i;
            break;
        }   
    }
    if(AT_MAX_E1_NUM_PER_DOMAIN == i)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CLKDOMAIN[%u] IS FULL.\n", __FILE__, __LINE__, g_ClkDomainInfo[chipId][domainIndex].domainId);
        return DRV_AT_CLK_DOMAIN_MEMBER_FULL;
    }
    else
    {
        return ret ;
    }

}

/*******************************************************************************
* 函数名称:   DrvAtClkDomainInfoCreate
* 功能描述:   时钟域信息创建
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtClkDomainInfoCreate(BYTE chipId,WORD32 clkDomainId,BYTE clkMode ,WORD32 masterE1LinkNo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 i = 0 ;
    WORD32 domainIndex = 0;
    WORD32 memberIndex = 0;

    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(masterE1LinkNo);
    
    for(i = 0 ;i< AT_MAX_DOMAIN_NUM_PER_SUBCARD ; i++)     /*is this clkdomainId existing?*/
    {
        if((clkDomainId == g_ClkDomainInfo[chipId][i].domainId)&&( CLK_DOMAIN_ENABLE == g_ClkDomainInfo[chipId][i].domainEnable)) /*如果检测到时钟域已经存在，则返回失败*/ 
        {
            DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, DOMAIN [%u] HAS ALREADY HAVE MASTER E1LinkNo [%u]\n", __FILE__, __LINE__,clkDomainId,g_ClkDomainInfo[chipId][i].masterE1LinkNo);
            return DRV_AT_CLK_DOMAIN_ALREADY_HAVE_MASTER;
        }
    }
    if(AT_MAX_DOMAIN_NUM_PER_SUBCARD == i)     /*not find the clkdomainId , create a new index*/
    {
        ret = DrvAtClkDomainFreeIndexGet(chipId,&domainIndex);    
        if (DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CAN NOT FIND A IDLE CLK DOMAIN INDEX ret=0x%x.\n", __FILE__, __LINE__, ret);
            return ret;
        }    
    }
    /*write the table*/
    g_ClkDomainInfo[chipId][domainIndex].domainId = clkDomainId;
    g_ClkDomainInfo[chipId][domainIndex].masterE1LinkNo = masterE1LinkNo ;
    g_ClkDomainInfo[chipId][domainIndex].clkMode = clkMode ;
    g_ClkDomainInfo[chipId][domainIndex].domainEnable = CLK_DOMAIN_ENABLE ;
    /*获取空闲的时钟域成员index*/
    ret = DrvAtClkDomainMemberFreeIndexGet(chipId,domainIndex,&memberIndex);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CAN NOT FIND A IDLE CLK DOMAIN MEMBER INDEX ret=0x%x.\n", __FILE__, __LINE__, ret);
        return ret;
    }    
    g_ClkDomainInfo[chipId][domainIndex].memberInfo[memberIndex].e1LinkNo = masterE1LinkNo;
    g_ClkDomainInfo[chipId][domainIndex].memberInfo[memberIndex].state = MASTER_STATUS;
    g_ClkDomainInfo[chipId][domainIndex].memberInfo[memberIndex].isEnable = CE1_ENABLE;
    

    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtClkDomainInfoCheck
* 功能描述:  时钟域信息检查，获取index
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期     版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtClkDomainInfoCheck(BYTE chipId,WORD32 clkDomainId,BYTE clkMode,WORD32*domainIndex)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 i = 0;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(domainIndex);
    *domainIndex = 0;
    
    for(i = 0 ;i< AT_MAX_DOMAIN_NUM_PER_SUBCARD ; i++)
    {
        if((clkDomainId == g_ClkDomainInfo[chipId][i].domainId)
        &&( CLK_DOMAIN_ENABLE == g_ClkDomainInfo[chipId][i].domainEnable)
        &&(clkMode == g_ClkDomainInfo[chipId][i].clkMode))  
        {
            *domainIndex = i;
            break ;
        }
    }
    if(AT_MAX_DOMAIN_NUM_PER_SUBCARD == i )
    {
        return DRV_AT_CLK_DOMAIN_INFO_WRONG;
    }
    else
    {
        return ret;
    }
}

/*******************************************************************************
* 函数名称:   DrvAtClkDomainInfoDelete
* 功能描述:   时钟域信息删除
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtClkDomainInfoDelete(BYTE chipId,WORD32 clkDomainId,BYTE clkMode )
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 domainIndex = 0;
    
    ret = DrvAtClkDomainInfoCheck(chipId,clkDomainId,clkMode,&domainIndex);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CAN NOT FIND CLK DOMAIN [%u] ret=0x%x.\n", __FILE__, __LINE__,clkDomainId, ret);
        return ret;
    }    
    memset(&g_ClkDomainInfo[chipId][domainIndex],0,sizeof(AT_CLK_DOMAIN_INFO));
     
    return ret ;
}


/*******************************************************************************
* 函数名称:   DrvAtClkDomainInfoUpdate
* 功能描述:   时钟域信息更新
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtClkDomainInfoUpdate(BYTE chipId,BYTE clkMode,WORD32 clkDomainId,WORD32 newMasterE1No)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 domainIndex = 0;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(newMasterE1No);
    
    /*检查时钟域号是否存在，若存在返回index*/
    ret = DrvAtClkDomainInfoCheck(chipId,clkDomainId,clkMode,&domainIndex);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CAN NOT FIND CLK DOMAIN [%u] ret=0x%x.\n", __FILE__, __LINE__,clkDomainId, ret);
        return ret;
    }    
    g_ClkDomainInfo[chipId][domainIndex].masterE1LinkNo = newMasterE1No;
    
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtClkDomainMemberInfoCreate
* 功能描述:   时钟域成员信息创建
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtClkDomainMemberInfoCreate(BYTE chipId,WORD32 clkDomainId,BYTE clkMode,WORD32 e1LinkNo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 domainIndex = 0;
    WORD32 memberIndex = 0;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    
    /*检查时钟域号是否存在，若存在返回index*/
    ret = DrvAtClkDomainInfoCheck(chipId,clkDomainId,clkMode,&domainIndex);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CAN NOT FIND CLK DOMAIN [%u] ret=0x%x.\n", __FILE__, __LINE__,clkDomainId, ret);
        return ret;
    }    
    /*获取空闲的时钟域成员index*/
    ret = DrvAtClkDomainMemberFreeIndexGet(chipId,domainIndex,&memberIndex);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CAN NOT FIND A IDLE CLK DOMAIN MEMBER INDEX ret=0x%x.\n", __FILE__, __LINE__, ret);
        return ret;
    }    
    g_ClkDomainInfo[chipId][domainIndex].memberInfo[memberIndex].e1LinkNo = e1LinkNo;
    g_ClkDomainInfo[chipId][domainIndex].memberInfo[memberIndex].state = SLAVE_STATUS;
    g_ClkDomainInfo[chipId][domainIndex].memberInfo[memberIndex].isEnable = CE1_ENABLE;
    
    return ret;
    
}

/*******************************************************************************
* 函数名称:   DrvAtClkDomainMemberInfoCheck
* 功能描述:   时钟域成员信息检查，获取index
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtClkDomainMemberInfoCheck(BYTE chipId,WORD32 domainIndex,WORD32 e1LinkNo,WORD32* memberIndex)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 i = 0;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    CHECK_AT_POINTER_NULL(memberIndex);
    
    *memberIndex = 0;
    for(i = 0;i < AT_MAX_E1_NUM_PER_DOMAIN;i++)
    {
        if((CE1_ENABLE == g_ClkDomainInfo[chipId][domainIndex].memberInfo[i].isEnable)
        &&(e1LinkNo == g_ClkDomainInfo[chipId][domainIndex].memberInfo[i].e1LinkNo))
        {
            *memberIndex = i;
            break;
        }
    }
    if(AT_MAX_E1_NUM_PER_DOMAIN == i)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CLKDOMAIN[%u] CAN NOT FIND  MEMBER E1[%u].\n", __FILE__, __LINE__, g_ClkDomainInfo[chipId][domainIndex].domainId,e1LinkNo);
        return DRV_AT_CLK_DOMAIN_MEMBER_NOT_FIND;
    }
    else
    {
        return ret;
    }
}


/*******************************************************************************
* 函数名称:   DrvAtClkDomainMemberInfoDelete
* 功能描述:   时钟域成员信息删除
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtClkDomainMemberInfoDelete(BYTE chipId,WORD32 clkDomainId,BYTE clkMode,WORD32 e1LinkNo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 domainIndex = 0;
    WORD32 memberIndex = 0;

    ret = DrvAtClkDomainInfoCheck(chipId,clkDomainId,clkMode,&domainIndex);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CAN NOT FIND CLK DOMAIN [%u] ret=0x%x.\n", __FILE__, __LINE__,clkDomainId, ret);
        return ret;
    }    
    
    ret = DrvAtClkDomainMemberInfoCheck(chipId,domainIndex,e1LinkNo,&memberIndex);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CAN NOT FIND CLK DOMAIN [%u] ret=0x%x.\n", __FILE__, __LINE__,clkDomainId, ret);
        return ret;
    }    
    memset(&g_ClkDomainInfo[chipId][domainIndex].memberInfo[memberIndex],0,sizeof(AT_CLK_DOMAIN_MEMBER));

    
    return ret;
}


/*******************************************************************************
* 函数名称:   DrvAtClkDomainMemberInfoUpdate
* 功能描述:   时钟域成员信息更新
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtClkDomainMemberInfoUpdate(BYTE chipId,WORD32 clkDomainId,BYTE clkMode,BYTE clkstate,WORD32 e1LinkNo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 domainIndex = 0;
    WORD32 memberIndex = 0;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);

    
    ret = DrvAtClkDomainInfoCheck(chipId,clkDomainId,clkMode,&domainIndex);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CAN NOT FIND CLK DOMAIN [%u] ret=0x%x.\n", __FILE__, __LINE__,clkDomainId, ret);
        return ret;
    }    
    
    ret = DrvAtClkDomainMemberInfoCheck(chipId,domainIndex,e1LinkNo,&memberIndex);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CAN NOT FIND CLK DOMAIN [%u] ret=0x%x.\n", __FILE__, __LINE__,clkDomainId, ret);
        return ret;
    }    
    /*更新表项*/
    g_ClkDomainInfo[chipId][domainIndex].memberInfo[memberIndex].state = clkstate;
    
    return ret;

}

/*******************************************************************************
* 函数名称:   DrvAtClkDomainMasterGet
* 功能描述:   时钟域主时钟获取
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtClkDomainMasterGet(BYTE chipId,WORD32 clkDomainId,BYTE clkMode ,WORD32 *masterE1LinkNo)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    WORD32 i = 0;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(masterE1LinkNo);
    
    for(i = 0 ;i< AT_MAX_DOMAIN_NUM_PER_SUBCARD ; i++)
    {
        if((clkDomainId == g_ClkDomainInfo[chipId][i].domainId)
        &&( CLK_DOMAIN_ENABLE == g_ClkDomainInfo[chipId][i].domainEnable)
        &&(clkMode == g_ClkDomainInfo[chipId][i].clkMode))  
        {
            *masterE1LinkNo = g_ClkDomainInfo[chipId][i].masterE1LinkNo ;
          break ;
        }
    }
    if(AT_MAX_DOMAIN_NUM_PER_SUBCARD == i)
    {
        return DRV_AT_CLK_SRC_LINK_GET_FAILED ;
    }
    else
    {
        return ret ;
    }
}


/*******************************************************************************
* 函数名称:   DrvAtClkDomainSlaveMemberGet
* 功能描述:   时钟域从成员获取，只获取查到的第一个
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期     版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtClkDomainSlaveMemberGet(BYTE chipId,WORD32 clkDomainId,BYTE clkMode ,WORD32 masterE1LinkNo,WORD32* e1LinkNo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 domainIndex = 0;
    WORD32 i = 0;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(e1LinkNo);
    CHECK_AT_E1LINK_ID(masterE1LinkNo);

    *e1LinkNo = 0;
    ret = DrvAtClkDomainInfoCheck(chipId,clkDomainId,clkMode,&domainIndex);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CAN NOT FIND CLK DOMAIN [%u] ret=0x%x.\n", __FILE__, __LINE__,clkDomainId, ret);
        return ret;
    }    
    for(i = 0;i < AT_MAX_E1_NUM_PER_DOMAIN;i++)
    {
        if((g_ClkDomainInfo[chipId][domainIndex].masterE1LinkNo == masterE1LinkNo)
        &&(CE1_ENABLE == g_ClkDomainInfo[chipId][domainIndex].memberInfo[i].isEnable)
        &&(SLAVE_STATUS == g_ClkDomainInfo[chipId][domainIndex].memberInfo[i].state))
        {
            *e1LinkNo = g_ClkDomainInfo[chipId][domainIndex].memberInfo[i].e1LinkNo;
            break;
        }
    }
    if(AT_MAX_E1_NUM_PER_DOMAIN == i)
    {
        //DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CLKDOMAIN[%u] CAN NOT FIND SLAVE MEMBER.\n", __FILE__, __LINE__, g_ClkDomainInfo[chipId][domainIndex].domainId);
        return DRV_AT_CLK_DOMAIN_NO_SLAVE_MEMBER;
    }
    else
    {
       return ret ;
    }
}

/*******************************************************************************
* 函数名称:   DrvAtClkDomainMemberAlmGet
* 功能描述:   时钟域成员告警状态获取
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期     版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtClkDomainMemberAlmGet(BYTE chipId,WORD32 e1LinkNo,WORD32*isAlmState)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32  pwId = 0;
    WORD32 isPwAlarm = 0;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(isAlmState);
    
    DRV_AT_E1_PRINT(DRV_AT_CLK_DOMAIN_ALM_GET,"[CE1TAN][CLK DOMAIN ALM GET]:%s line %d,chipid is %u ,e1LinkNo is %u\n",__FILE__, __LINE__,chipId,e1LinkNo);
    
    for(pwId = 0;pwId < AT_MAX_PW_NUM_PER_SUBCARD;pwId++)
    {
        if((g_atE1PwInfo[chipId][pwId].chipId == chipId)
        &&(g_atE1PwInfo[chipId][pwId].e1LinkNum == e1LinkNo)
        &&(g_atE1PwInfo[chipId][pwId].pwStatus == PW_USED))
        {
            isPwAlarm = DrvAtPwClkAlarmGetFromPwId(chipId,pwId);
            if(CE1_TURE == isPwAlarm)
            {
                *isAlmState = CE1_TURE;
                break;
            }
            else
            {
                *isAlmState = CE1_FALSE;
            }
        }                
    }
    
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtClkSrcSwitch
* 功能描述:   时钟域主时钟切换
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtClkSrcSwitch(BYTE chipId,BYTE clkMode,WORD32 clkDomainId,WORD32 oldMasterE1No, WORD32 newMasterE1No)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 currentMasterE1No = 0;
    WORD32 pwId = 0;
    WORD32 slaveE1LinkNo = 0;
    WORD32 i = 0;
    WORD32 domainIndex = 0;

    DRV_AT_E1_PRINT(DRV_AT_CLK_DOMAIN_SWITCH,"[CE1TAN][CLK DOMAIN SWITCH]:%s line %d,chipid is %u ,clkMode is %u,clkDomainId is %u,oldMastert is %u,newMaster is %u\n",__FILE__, __LINE__,chipId,clkMode,clkDomainId,oldMasterE1No,newMasterE1No);
    /*检查时钟模式是否是
ACR或DCR*/
    if((clkMode!= ACR_CLK_MODE)&&(clkMode!= DCR_CLK_MODE))
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CLKMODE[%u] IS NOT ACR OR DCR  ! !! CHIP ID IS %u ,CLK DOMIAN ID IS %u \n", __FILE__, __LINE__,clkMode,chipId,clkDomainId); 
        return DRV_AT_CLK_DOMAIN_INFO_WRONG; 
    }
    /*检查时钟域号是否存在，若存在返回index*/
    ret = DrvAtClkDomainInfoCheck(chipId,clkDomainId,clkMode,&domainIndex);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CAN NOT FIND CLK DOMAIN [%u] ret=0x%x.\n", __FILE__, __LINE__,clkDomainId, ret);
        return ret;
    }    
    /*获取时钟域内的master E1*/
    ret = DrvAtClkDomainMasterGet(chipId,clkDomainId,clkMode,&currentMasterE1No);     
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, MASTER E1LINKNO GET FAILED ! !! CHIP ID IS %u ,CLK DOMIAN ID IS %u \n", __FILE__, __LINE__,chipId,clkDomainId); 
        return ret; 
    }
    /*如果bsp的master和pm的不一致，返回错误*/
    if(currentMasterE1No != oldMasterE1No)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, MASTER E1lINKNO NOT THE SAME ! !! CHIP ID IS %u ,CLK DOMIAN ID IS %u,BSP MASTER IS E1[%u],PM MASTER IS E1[%u] \n", __FILE__, __LINE__,chipId,clkDomainId,currentMasterE1No,oldMasterE1No); 
        return DRV_AT_CLK_DOMAIN_INFO_WRONG; 
    }
    /*如果新分配的主时钟与原来的一致，不响应*/
    if(oldMasterE1No == newMasterE1No)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, NO NEED TO CHANGE MASTER ! !! CHIP ID IS %u ,CLK DOMIAN ID IS %u,OLD MASTER IS E1[%u],NEW MASTER IS E1[%u] \n", __FILE__, __LINE__,chipId,clkDomainId,oldMasterE1No,newMasterE1No); 
        return DRV_AT_SUCCESS; 
    }
    /*查找E1下绑定的pw，如果E1绑定多条pw，返回查到的第一条pwid*/
    ret = DrvAtE1BindPwIdGet(chipId,newMasterE1No,&pwId);
    if(DRV_AT_SUCCESS != ret) 
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, THIS E1[%u] NOT BIND WITH ANY PW !!\n", __FILE__, __LINE__,newMasterE1No);   
        return ret;  
    } 
    /*将新的主时钟E1设置成从pw恢复*/
    ret = DrvAtE1ClkModeSet(chipId,newMasterE1No,clkMode,0,pwId);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, E1[%u] CLK MODE  SET  FAILED ! !! \n", __FILE__, __LINE__,newMasterE1No); 
        return ret; 
    }
    
    /*把老主的从时钟成员设置成新主的从时钟*/
    for(i = 0;i < AT_MAX_E1_NUM_PER_DOMAIN;i++)
    {   
        if((CE1_ENABLE == g_ClkDomainInfo[chipId][domainIndex].memberInfo[i].isEnable)
        &&(SLAVE_STATUS == g_ClkDomainInfo[chipId][domainIndex].memberInfo[i].state))
        {
            slaveE1LinkNo = g_ClkDomainInfo[chipId][domainIndex].memberInfo[i].e1LinkNo;
            if(slaveE1LinkNo != newMasterE1No) /*这边要跳过新主*/
            {  
                ret = DrvAtE1ClkModeSet(chipId,slaveE1LinkNo,SLAVE_CLK_MODE,newMasterE1No,0);
                if(DRV_AT_SUCCESS != ret)
                {
                    DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, E1[%u] CLK MODE  SET  FAILED ! !! \n", __FILE__, __LINE__,slaveE1LinkNo); 
                    return ret; 
                }
                /*更新从时钟下pw的状态信息*/
                ret = DrvAtPwClkInfoUpdate(chipId,slaveE1LinkNo,SLAVE_STATUS,newMasterE1No,CE1_FALSE);
                if(DRV_AT_SUCCESS != ret)
                {
                    DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PW CLK INFO UPDATE FAILED ! !!E1linkNo is %u \n", __FILE__, __LINE__,slaveE1LinkNo); 
                    return ret; 
                }     
            }  
        }    
    }
    
    /*将老主时钟设置成新主时钟的从*/
    ret = DrvAtE1ClkModeSet(chipId,oldMasterE1No,SLAVE_CLK_MODE,newMasterE1No,0);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, E1[%u] CLK MODE  SET  FAILED ! !! \n", __FILE__, __LINE__,slaveE1LinkNo); 
        return ret; 
    }
    /*更新时钟域成员表项，将老主时钟设置成从*/
    ret = DrvAtClkDomainMemberInfoUpdate(chipId,clkDomainId,clkMode,SLAVE_STATUS,oldMasterE1No);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CLK DOMAIN[%u] MEMBER INFO UPDATE FAILED ! !! \n", __FILE__, __LINE__,clkDomainId); 
        return ret; 
    }
    
    /*更新旧主时钟下pw的时钟状态信息*/
    ret = DrvAtPwClkInfoUpdate(chipId,oldMasterE1No,SLAVE_STATUS,newMasterE1No,CE1_FALSE);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PW CLK INFO UPDATE FAILED ! !!E1linkNo is %u \n", __FILE__, __LINE__,slaveE1LinkNo); 
        return ret; 
    }  
    /*更新新主时钟下的pw 时钟状态信息,之所以在最后才更新新主时钟的表项，是因为只有时钟域内的从时钟
    全部设置完成后，新主才是真正意义上的时钟域内的主时钟*/
    
    /*更新时钟域成员表项，将新主的主时钟设置成自己*/
    ret = DrvAtClkDomainMemberInfoUpdate(chipId,clkDomainId,clkMode,MASTER_STATUS,newMasterE1No);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CLK DOMAIN[%u] MEMBER INFO UPDATE FAILED ! !! \n", __FILE__, __LINE__,clkDomainId); 
        return ret; 
    }
    ret = DrvAtPwClkInfoUpdate(chipId,newMasterE1No,MASTER_STATUS,newMasterE1No,CE1_FALSE);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PW CLK INFO UPDATE FAILED ! !!E1linkNo is %u \n", __FILE__, __LINE__,slaveE1LinkNo); 
        return ret; 
    } 
    g_atE1PwInfo[chipId][pwId].isClkSrc = CE1_TURE;/*将新主时钟的恢复原pw做标记*/
    
    /*更新时钟域表项，将其主时钟设置成新主*/
    ret = DrvAtClkDomainInfoUpdate(chipId,clkMode,clkDomainId,newMasterE1No);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CLK DOMAIN[%u]  INFO UPDATE FAILED ! !! \n", __FILE__, __LINE__,clkDomainId); 
        return ret; 
    }
    
    return ret;
}










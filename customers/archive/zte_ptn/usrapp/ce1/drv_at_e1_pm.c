/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_at_e1_pm.c
* 文件标识: 
* 其它说明: ce1tan ces 性能统计代码
* 当前版本: V1.0
* 作    者: 陈博10140635.
* 完成日期: 2013-05-15
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/
#include "AtDriver.h"
#include "AtDevice.h"
#include "AtModulePdh.h"
#include "AtPdhDe1.h"
#include "AtChannel.h"
#include "drv_at_e1_pm.h"
#include "drv_at_e1_port.h"



/*全局变量*/
PDH_E1_ALARM  g_atPdhE1Alarm[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_E1_INTF_NUM] ;
PDH_E1_ALARM g_atPdhE1HisAlm[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_E1_INTF_NUM] ;
PDH_E1_ERR_COUNT g_atPdhE1ErrCount[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_E1_INTF_NUM];
PDH_E1_ERR_COUNT g_atPdhE1ErrCountSum[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_E1_INTF_NUM];
PDH_E1_ERR_COUNT g_atPdhE1ErrCountToPm[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_E1_INTF_NUM];
WORD32 g_almthrcount[AT_MAX_SUBCARD_NUM_PER_SLOT]; 
WORD32 g_errthrcount[AT_MAX_SUBCARD_NUM_PER_SLOT];
WORD32 g_almthrcountDbg[AT_MAX_SUBCARD_NUM_PER_SLOT] ; 
WORD32 g_ErrthrcountDbg[AT_MAX_SUBCARD_NUM_PER_SLOT] ; 
struct mod_thread g_atE1AlmThrMod[AT_MAX_SUBCARD_NUM_PER_SLOT] ;
struct mod_thread g_atE1ErrThrMod[AT_MAX_SUBCARD_NUM_PER_SLOT] ;

/*外部全局变量*/
extern CE1TAN_THREAD_HANDLE g_threadHdl[AT_MAX_SUBCARD_NUM_PER_SLOT];
extern CE1TAN_THREAD_FLAG g_threadFlag[AT_MAX_SUBCARD_NUM_PER_SLOT];
extern CE1TAN_THREAD_ID g_threadId[AT_MAX_SUBCARD_NUM_PER_SLOT];

/*外部函数*/
extern WORD32 DrvAtE1ObjGet(BYTE chipId,WORD32 e1LinkId, AtPdhDe1* atE1Obj);
extern int __gettid (void);
extern WORD32 BSP_CE1TAN_BoardStatusGet(WORD32 dwSubslotId, BYTE *pdwIsOnline);

/*******************************************************************************
* 函数名称:   DrvAtE1AlarmGet
* 功能描述:   获取当前E1通道告警
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-16     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvAtE1AlarmGet(BYTE chipId,WORD32 e1LinkNo)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    BYTE portLos = 0;
    WORD32 alarmBitMap = 0;
    WORD32 hisAlmBitMap = 0;
    AtPdhDe1 atE1Obj = NULL ;
    
    CHECK_AT_CHIP_ID(chipId); 
    CHECK_AT_E1LINK_ID(e1LinkNo);

    /*Get liu los*/
    ret = DrvLiuLosGet(chipId+1,e1LinkNo,&portLos);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_E1_ALM_THREAD,"ERROR: %s line %d, LIU ALARM GET FAILED !!\n", __FILE__, __LINE__); 
        return ret; 
    }    
    /* Get object to configure */  
    ret = DrvAtE1ObjGet(chipId,e1LinkNo,&atE1Obj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_E1_ALM_THREAD,"ERROR: %s line %d, E1 OBJ GET FAILED !!\n", __FILE__, __LINE__); 
        return ret; 
    }
    alarmBitMap = AtChannelAlarmGet((AtChannel)atE1Obj) ;
    hisAlmBitMap = AtChannelAlarmInterruptClear((AtChannel)atE1Obj);
    /*读取当前e1 告警位图*/
    g_atPdhE1Alarm[chipId][e1LinkNo].port_los = portLos;
    g_atPdhE1Alarm[chipId][e1LinkNo].e1_los = (0 == (alarmBitMap & AtPdhE1AlarmLos) ) ? 0:1;   /*e1-los*/
    g_atPdhE1Alarm[chipId][e1LinkNo].e1_lof = (0 == (alarmBitMap & AtPdhE1AlarmLof))  ? 0:1;    /*e1-lof*/
    g_atPdhE1Alarm[chipId][e1LinkNo].e1_ais = (0 == (alarmBitMap & AtPdhE1AlarmAis) ) ? 0:1;    /*e1-ais*/
    g_atPdhE1Alarm[chipId][e1LinkNo].e1_rai = (0 == (alarmBitMap & AtPdhE1AlarmRai))  ? 0:1;    /*e1-rai*/
    g_atPdhE1Alarm[chipId][e1LinkNo].e1_lomf = (0 == (alarmBitMap & AtPdhE1AlarmLomf))  ? 0:1;    /*e1-lomf*/
    /*读取历史e1 告警位图*/
    g_atPdhE1HisAlm[chipId][e1LinkNo].e1_los = (0 == (hisAlmBitMap & AtPdhE1AlarmLos) ) ? 0:1;   /*e1-los*/
    g_atPdhE1HisAlm[chipId][e1LinkNo].e1_lof = (0 == (hisAlmBitMap & AtPdhE1AlarmLof))  ? 0:1;    /*e1-lof*/
    g_atPdhE1HisAlm[chipId][e1LinkNo].e1_ais = (0 == (hisAlmBitMap & AtPdhE1AlarmAis) ) ? 0:1;    /*e1-ais*/
    g_atPdhE1HisAlm[chipId][e1LinkNo].e1_rai = (0 == (hisAlmBitMap & AtPdhE1AlarmRai))  ? 0:1;    /*e1-rai*/

    return ret ;
    
}

/*******************************************************************************
* 函数名称:   DrvAt24E1AlarmGet
* 功能描述:   获取所有E1通道告警
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-16     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvAt24E1AlarmGet(BYTE chipId)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    WORD32 e1LinkNo = 0;
    for(e1LinkNo = 0;e1LinkNo < AT_E1_INTF_NUM ;e1LinkNo++)
    {
        ret = DrvAtE1AlarmGet(chipId,e1LinkNo);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, E1 ALARM GET FAILED !!CHIPID IS %u,E1LINKNO IS %u\n", __FILE__, __LINE__,chipId,e1LinkNo); 
            return ret; 
        }
    } 
    return ret ;
}
/*******************************************************************************
* 函数名称:   DrvAtE1TxAlarmInsert
* 功能描述:   TX 方向告警插入
* 输入参数:   insertFlag=1插入；insertFlag=0取消插入
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-16     v1.0        chenbo    create
********************************************************************************/
WORD32  DrvAtE1TxAlarmInsert(BYTE chipId,WORD32 e1LinkNo,WORD32 alarmType,BYTE insertFlag)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    eAtRet atRet = cAtOk ;
    AtPdhDe1 atE1Obj = NULL ;
    
    CHECK_AT_CHIP_ID(chipId); 
    CHECK_AT_E1LINK_ID(e1LinkNo);
    
    /* Get object to configure */  
    ret = DrvAtE1ObjGet(chipId,e1LinkNo,&atE1Obj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, E1 OBJ GET FAILED !!\n", __FILE__, __LINE__); 
        return ret; 
    }
    if((AtPdhE1AlarmAis == alarmType) ||(AtPdhE1AlarmRai == alarmType))     /*只支持插入ais和rai*/  
    { 
        if(INSERT_ON == insertFlag)
        {
            atRet = AtChannelTxAlarmForce((AtChannel)atE1Obj , alarmType) ;  /*插入告警*/
            if (cAtOk != atRet)
            {
                DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtChannelTxAlarmForce() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
                return DRV_AT_TX_ALM_INSERT_ON_ERROR ;
            }       
        }
    else if (INSERT_OFF == insertFlag)
    {
            atRet = AtChannelTxAlarmUnForce((AtChannel)atE1Obj , alarmType) ;   /*取消插入*/
            if (cAtOk != atRet)
            {
                DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtChannelTxAlarmForce() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
                return DRV_AT_TX_ALM_INSERT_OFF_ERROR ;
            }       
    }       
    }
    else
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"THE TYPE OF THE ALARM INSERTED IS NOT SUPPORTED !!CHIP ID IS %u , E1LINKNO IS %u ",chipId,e1LinkNo);
        return DRV_AT_PDH_ALARM_NOT_SUPPORT ;
    }
    return ret ;
}
      

/*******************************************************************************
* 函数名称:   DrvAtE1RxAlarmInsert
* 功能描述:   Rx 方向告警插入
* 输入参数:   insertFlag=1插入；insertFlag=0取消插入
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-16     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtE1RxAlarmInsert(BYTE chipId,WORD32 e1LinkNo,WORD32 alarmType,BYTE insertFlag)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    eAtRet atRet = cAtOk ;
    AtPdhDe1 atE1Obj = NULL ;
    
    CHECK_AT_CHIP_ID(chipId); 
    CHECK_AT_E1LINK_ID(e1LinkNo);
    
    /* Get object to configure */  
    ret = DrvAtE1ObjGet(chipId,e1LinkNo,&atE1Obj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, E1 OBJ GET FAILED !!\n", __FILE__, __LINE__); 
        return ret; 
    }
    if((AtPdhE1AlarmLof == alarmType) )     /*只支持插入lof*/  
    { 
        if(INSERT_ON == insertFlag)
        {
            atRet = AtChannelRxAlarmForce((AtChannel)atE1Obj , alarmType) ;
            if (cAtOk != atRet)
            {
                DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtChannelTxAlarmForce() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
                return DRV_AT_RX_ALM_INSERT_ON_ERROR ;
            }       
        }
    else if (INSERT_OFF == insertFlag)
    {
            atRet = AtChannelRxAlarmUnForce((AtChannel)atE1Obj , alarmType) ;
            if (cAtOk != atRet)
            {
                DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtChannelTxAlarmForce() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
                return DRV_AT_RX_ALM_INSERT_OFF_ERROR ;
            }       
    }       
    }
    else
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"THE TYPE OF THE ALARM INSERTED IS NOT SUPPORTED !!CHIP ID IS %u , E1LINKNO IS %u ",chipId,e1LinkNo);
        return DRV_AT_PDH_ALARM_NOT_SUPPORT ;
    }
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtE1ErrCountGet
* 功能描述:   获取当前E1误码计数
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-16     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvAtE1ErrCountGet(BYTE chipId,WORD32 e1LinkNo)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    AtPdhDe1 atE1Obj = NULL ;
    
    CHECK_AT_CHIP_ID(chipId); 
    CHECK_AT_E1LINK_ID(e1LinkNo);
    
    /* Get object to configure */  
    ret = DrvAtE1ObjGet(chipId,e1LinkNo,&atE1Obj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_E1_ERR_THREAD,"ERROR: %s line %d, E1 OBJ GET FAILED !!\n", __FILE__, __LINE__); 
        return ret; 
    }
    
    /*读取e1 误码(读清模式)*/
    g_atPdhE1ErrCount[chipId][e1LinkNo].bpv = AtChannelCounterClear((AtChannel)atE1Obj, cAtPdhDe1CounterBpvExz);   /*e1-los*/
    g_atPdhE1ErrCount[chipId][e1LinkNo].crc = AtChannelCounterClear((AtChannel)atE1Obj, cAtPdhDe1CounterCrc);    /*e1-lof*/
    g_atPdhE1ErrCount[chipId][e1LinkNo].rei = AtChannelCounterClear((AtChannel)atE1Obj, cAtPdhDe1CounterRei);    /*e1-ais*/
    g_atPdhE1ErrCount[chipId][e1LinkNo].fe = AtChannelCounterClear((AtChannel)atE1Obj, cAtPdhDe1CounterFe);    /*e1-rai*/

    /*累加e1误码计数*/
    g_atPdhE1ErrCountSum[chipId][e1LinkNo].bpv += g_atPdhE1ErrCount[chipId][e1LinkNo].bpv;
    g_atPdhE1ErrCountToPm[chipId][e1LinkNo].bpv += g_atPdhE1ErrCount[chipId][e1LinkNo].bpv;

    g_atPdhE1ErrCountSum[chipId][e1LinkNo].crc += g_atPdhE1ErrCount[chipId][e1LinkNo].crc;
    g_atPdhE1ErrCountToPm[chipId][e1LinkNo].crc += g_atPdhE1ErrCount[chipId][e1LinkNo].crc;

    g_atPdhE1ErrCountSum[chipId][e1LinkNo].rei += g_atPdhE1ErrCount[chipId][e1LinkNo].rei;
    g_atPdhE1ErrCountToPm[chipId][e1LinkNo].rei += g_atPdhE1ErrCount[chipId][e1LinkNo].rei;

    g_atPdhE1ErrCountSum[chipId][e1LinkNo].fe += g_atPdhE1ErrCount[chipId][e1LinkNo].fe;
    g_atPdhE1ErrCountToPm[chipId][e1LinkNo].fe += g_atPdhE1ErrCount[chipId][e1LinkNo].fe;
    
    return ret ;
    
}


/*******************************************************************************
* 函数名称:   DrvAt24E1ErrCountGet
* 功能描述:   获取所有E1误码计数
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-16     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvAt24E1ErrCountGet(BYTE chipId)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    WORD32 e1LinkNo = 0 ;
    
    for(e1LinkNo = 0;e1LinkNo < AT_E1_INTF_NUM;e1LinkNo++)
    {  
        ret = DrvAtE1ErrCountGet(chipId,e1LinkNo);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, E1 ERROR GET FAILED !!CHIPID IS %u,E1LINKNO IS %u\n", __FILE__, __LINE__,chipId,e1LinkNo); 
            return ret; 
        }
    }
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtE1ErrInsert
* 功能描述:   插入误码
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-16     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvAtE1ErrInsert(BYTE chipId,WORD32 e1LinkNo,BYTE insertFlag)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    eAtRet atRet = cAtOk ;
    AtPdhDe1 atE1Obj = NULL ;
    
    CHECK_AT_CHIP_ID(chipId); 
    CHECK_AT_E1LINK_ID(e1LinkNo);
    
    /* Get object to configure */  
    ret = DrvAtE1ObjGet(chipId,e1LinkNo,&atE1Obj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, E1 OBJ GET FAILED !!\n", __FILE__, __LINE__); 
        return ret; 
    }
    /*only support crc err*/
    if(INSERT_ON == insertFlag)
    {
        atRet = AtChannelTxErrorForce((AtChannel)atE1Obj,cAtPdhDe1CounterCrc);
        if (cAtOk != atRet)
        {
            DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtChannelTxErrorForce() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
            return DRV_AT_TX_ERR_INSERT_ON_ERROR ;
        }    
    }
    else if(INSERT_OFF == insertFlag)
    {
        atRet = AtChannelTxErrorUnForce((AtChannel)atE1Obj,cAtPdhDe1CounterCrc);
        if (cAtOk != atRet)
        {
            DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtChannelTxErrorUnForce() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
            return DRV_AT_TX_ERR_INSERT_OFF_ERROR ;
        }    
    }
    
    return ret ;

}


/*******************************************************************************
* 函数名称:   DrvAtE1AlmDetectScan
* 功能描述:   检测E1告警的监控线程
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-16     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvAtE1AlmDetectScan(const WORD32* slotId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 e1LinkNo = 0;
    WORD32 chipNo = 0;
    WORD32 subcardId = 0;
    BYTE isOnline = CE1_FALSE;
    
    CHECK_AT_POINTER_NULL(slotId);
    subcardId = *slotId ;
    if((subcardId > AT_MAX_SUBCARD_NUM_PER_SLOT)||(subcardId < AT_MIN_SUBCARD_NUM_PER_SLOT))
    {
        DRV_AT_E1_ERROR_LOG("E1 ALARM THREAD ,INVALID subcardId!!! addr is 0x%x,slotId is %d,subcardId is %d\n",slotId,*slotId,subcardId);
        if(NULL !=slotId)
        {
            free((WORD32*)slotId);
            slotId = NULL;
        }
        return DRV_AT_INVALID_SUBCARD_ID;
    }
    free((WORD32*)slotId);
    slotId = NULL;
    
    chipNo = subcardId - 1;
    g_threadId[chipNo].E1_ALARM_THR_ID = __gettid();
    DRV_AT_E1_COMMON_LOG("E1 ALARM THREAD ,CHIPID IS %u,__gettid is %u\n",chipNo,g_threadId[chipNo].E1_ALARM_THR_ID);
    
    while(1)
    {
        if(CE1_ENABLE == g_threadFlag[chipNo].E1_ALARM_THR_FLAG)
        {   
            for(e1LinkNo = 0;e1LinkNo < AT_E1_INTF_NUM ;e1LinkNo++)
            {
                BSP_DelayMs(25);
                ret = BSP_CE1TAN_BoardStatusGet((chipNo+1),&isOnline);
                if (DRV_AT_SUCCESS != ret)
                {
                    DRV_AT_E1_PRINT(DRV_AT_E1_ALM_THREAD,"ERROR: %s line %d, BSP_CE1TAN_BoardStatusGet() ret=0x%x.\n", __FILE__, __LINE__, ret);
                    return ret;    
                }
                if(CE1_FALSE == isOnline)
                {
                    DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, CE1TAN IS NOT ONLINE.SUBCARD ID IS %u\n", __FILE__, __LINE__, (chipNo+1));
                    return DRV_AT_SUCCESS;    
                }
                ret = DrvAtE1AlarmGet(chipNo,e1LinkNo);
                if(DRV_AT_SUCCESS != ret)
                {
                    DRV_AT_E1_PRINT(DRV_AT_E1_ALM_THREAD,"ERROR: %s line %d, E1 ALARM GET FAILED !!CHIPID IS %u,e1LinkNo is %u\n", __FILE__, __LINE__,chipNo,e1LinkNo); 
                    continue; 
                }
                g_almthrcount[chipNo]++;
            }
        }
        BSP_DelayMs(1000);  /* delay 1000ms. */
        g_almthrcountDbg[chipNo]++;
    }
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtE1ErrDetectScan
* 功能描述:   检测E1误码的监控线程
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-16     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvAtE1ErrDetectScan(const WORD32*slotId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 e1LinkNo = 0;
    WORD32 subcardId = 0;
    WORD32 chipNo = 0;
    BYTE isOnline = 0;

    CHECK_AT_POINTER_NULL(slotId);
    subcardId = *slotId ;
    if((subcardId > AT_MAX_SUBCARD_NUM_PER_SLOT)||(subcardId < AT_MIN_SUBCARD_NUM_PER_SLOT))
    {
        DRV_AT_E1_ERROR_LOG("E1 ERROR THREAD ,INVALID subcardId!!! addr is 0x%x,slotId is %d,subcardId is %d\n",slotId,*slotId,subcardId);
        if(NULL !=slotId)
        {
            free((WORD32*)slotId);
            slotId = NULL;
        }
        return DRV_AT_INVALID_SUBCARD_ID;
    }
    free((WORD32*)slotId);
    slotId = NULL;
    
    chipNo = subcardId - 1;
    g_threadId[chipNo].E1_ERROR_THR_ID = __gettid();
    DRV_AT_E1_COMMON_LOG("E1 ERROR THREAD,CHIPID IS %u, __gettid is %u\n",chipNo,g_threadId[chipNo].E1_ERROR_THR_ID);
    
    while(1)
    {
        if(CE1_ENABLE == g_threadFlag[chipNo].E1_ERROR_THR_FLAG)
        {
            for(e1LinkNo = 0;e1LinkNo < AT_E1_INTF_NUM ;e1LinkNo++)
            {
                BSP_DelayMs(25);
                ret = BSP_CE1TAN_BoardStatusGet((chipNo+1),&isOnline);
                if (DRV_AT_SUCCESS != ret)
                {
                    DRV_AT_E1_PRINT(DRV_AT_E1_ERR_THREAD,"ERROR: %s line %d, BSP_CE1TAN_BoardStatusGet() ret=0x%x.\n", __FILE__, __LINE__, ret);
                    return ret;    
                }
                if(CE1_FALSE == isOnline)
                {
                    DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, CE1TAN IS NOT ONLINE.SUBCARD ID IS %u\n", __FILE__, __LINE__,(chipNo+1));
                    return DRV_AT_SUCCESS;    
                }
                ret = DrvAtE1ErrCountGet(chipNo,e1LinkNo);
                if(DRV_AT_SUCCESS != ret)
                {
                    DRV_AT_E1_PRINT(DRV_AT_E1_ERR_THREAD,"ERROR: %s line %d, E1 ERR GET FAILED !!CHIPID IS %u,e1LinkNo is %u\n", __FILE__, __LINE__,chipNo,e1LinkNo); 
                    continue; 
                }
                g_errthrcount[chipNo]++;
            }
            
        }
        BSP_DelayMs(1000);  /* delay 1000ms. */
        g_ErrthrcountDbg[chipNo]++;
    }
    return ret;
}


/**************************************************************************
* 函数名称: DrvAtE1AlmMemGet
* 功能描述: 获取保存e1告警的内存
* 访问的表: 
* 修改的表: 无
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-04-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 DrvAtE1AlmMemGet(BYTE chipId, WORD32 e1LinkNo,PDH_E1_ALARM*alarm)
{
    WORD32 ret = DRV_AT_SUCCESS;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    CHECK_AT_POINTER_NULL(alarm);  
    
    memcpy(alarm, &(g_atPdhE1Alarm[chipId][e1LinkNo]), sizeof(PDH_E1_ALARM));
    
    return ret;
}

/**************************************************************************
* 函数名称: DrvAtE1ErrMemGet
* 功能描述: 获取保存e1误码的内存
* 访问的表: 
* 修改的表: 无
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-04-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 DrvAtE1ErrMemGet(BYTE chipId, WORD32 e1LinkNo,PDH_E1_ERR_COUNT*error,BYTE countMode)
{
    WORD32 ret = DRV_AT_SUCCESS;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    CHECK_AT_POINTER_NULL(error);
    
    if(MONITOR_REAL_TIME == countMode)               /*实时统计*/
    {
        memcpy(error, &(g_atPdhE1ErrCount[chipId][e1LinkNo]), sizeof(PDH_E1_ERR_COUNT));
    }
    else if(MONITOR_PERFORMANCE == countMode)   /*累计统计*/
    {
        memcpy(error, &(g_atPdhE1ErrCountSum[chipId][e1LinkNo]), sizeof(PDH_E1_ERR_COUNT));
    }
    else if(PDH_ERROR_STATIC_SEND_TO_PM == countMode) /*上报给产品管理*/
    {
        memcpy(error, &(g_atPdhE1ErrCountToPm[chipId][e1LinkNo]), sizeof(PDH_E1_ERR_COUNT));
    }
    
    return ret;
}

/**************************************************************************
* 函数名称: DrvAtE1AlmMemClear
* 功能描述: 获取保存SDH告警的内存
* 访问的表: 
* 修改的表: 无
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-04-26   V1.0    
**************************************************************************/
WORD32 DrvAtE1AlmMemClear(BYTE chipId, WORD32 e1LinkNo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    
    memset(&(g_atPdhE1Alarm[chipId][e1LinkNo]),0,sizeof(PDH_E1_ALARM));
    
    return ret;
}

/**************************************************************************
* 函数名称: DrvAtE1ErrMemClear
* 功能描述: 清空保存e1误码的内存
* 访问的表: 
* 修改的表: 无
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-04-26   V1.0      
**************************************************************************/
WORD32 DrvAtE1ErrMemClear(BYTE chipId, WORD32 e1LinkNo,BYTE countMode)
{
    WORD32 ret = DRV_AT_SUCCESS;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    
    memset(&(g_atPdhE1ErrCount[chipId][e1LinkNo]),0,sizeof(PDH_E1_ERR_COUNT));
    
    if(MONITOR_REAL_TIME == countMode)               /*实时统计*/
    {
        memset(&(g_atPdhE1ErrCount[chipId][e1LinkNo]),0,sizeof(PDH_E1_ERR_COUNT));
    }
    else if(MONITOR_PERFORMANCE == countMode)   /*累计统计*/
    {
        memset(&(g_atPdhE1ErrCountSum[chipId][e1LinkNo]),0,sizeof(PDH_E1_ERR_COUNT));
    }
    else if(PDH_ERROR_STATIC_SEND_TO_PM == countMode) /*上报给产品管理*/
    {
        memset(&(g_atPdhE1ErrCountToPm[chipId][e1LinkNo]),0,sizeof(PDH_E1_ERR_COUNT));
    }
    
    return ret;
}

/**************************************************************************
* 函数名称: DrvAtE1AlarmQuery
* 功能描述: 查询E1告警
* 访问的表: 
* 修改的表: 
* 输入参数: 
* 输出参数: 
* 返 回 值: 
* 其它说明: 本函数供产品管理调用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-26   V1.0     
**************************************************************************/
WORD32 DrvAtE1AlarmQuery(BYTE subcardId,WORD32 e1PortId, WORD32 *QueryResult)
{
    WORD32 ret = DRV_AT_SUCCESS;  /* 函数返回码 */
    BYTE e1LinkNo = e1PortId -1;
    BYTE chipId = subcardId -1;
    WORD32 tmpResult = 0;
    PDH_E1_ALARM e1Alm;
  
    CHECK_AT_POINTER_NULL(QueryResult);
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    memset(&e1Alm,0,sizeof(PDH_E1_ALARM));
    
    DRV_AT_E1_PRINT(DRV_AT_E1_ALM_QUERY,"[CE1TAN][E1 ALM QUERY]:%s line %d, subcardId is %u,e1PortId is %u\n",__FILE__, __LINE__,subcardId,e1PortId);
    DrvAtE1AlmMemGet(chipId,e1LinkNo,&e1Alm);
    *QueryResult = 0;   /* 赋值之前先清零 */
    tmpResult = tmpResult | (BSP_SDH_ALAM_E1_RED & (((WORD32)e1Alm.port_los)<< 0));
    tmpResult = tmpResult | (BSP_SDH_ALAM_E1_AIS & (((WORD32)e1Alm.e1_ais)<< 1));
    tmpResult = tmpResult | (BSP_SDH_ALAM_E1_LOF & (((WORD32)e1Alm.e1_lof)<< 2));
    tmpResult = tmpResult | (BSP_SDH_ALAM_E1_RAI & (((WORD32)e1Alm.e1_rai)<< 3));
    tmpResult = tmpResult | (BSP_SDH_ALAM_E1_SMLOF & (((WORD32)e1Alm.e1_lomf)<< 5));
    *QueryResult = tmpResult;
    
    DRV_AT_E1_PRINT(DRV_AT_E1_ALM_QUERY,"[CE1TAN][E1 ALM QUERY]:%s line %d, subcardId is %u,e1PortId is %u,alarmBitmap is 0x%x\n",__FILE__, __LINE__,subcardId,e1PortId,tmpResult);    
    return ret;
}

/**************************************************************************
* 函数名称: DrvAtE1ErrorQuery
* 功能描述: 查询E1告警
* 访问的表: 
* 修改的表: 
* 输入参数: 
* 输出参数: 
* 返 回 值: 
* 其它说明: 本函数供产品管理调用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-26   V1.0     
**************************************************************************/
WORD32 DrvAtE1ErrorQuery(BYTE subcardId,WORD32 e1PortId, T_BSP_SDH_ERR_E1*QueryResult)
{
    WORD32 ret = DRV_AT_SUCCESS;  /* 函数返回码 */
    BYTE e1LinkNo = e1PortId -1;
    BYTE chipId = subcardId -1;
    PDH_E1_ERR_COUNT e1Err;
    
    CHECK_AT_POINTER_NULL(QueryResult);
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    memset(&e1Err,0,sizeof(PDH_E1_ERR_COUNT));
    DRV_AT_E1_PRINT(DRV_AT_E1_ERR_QUERY,"[CE1TAN][E1 ERR QUERY]:%s line %d, subcardId is %u,e1PortId is %u",__FILE__, __LINE__,subcardId,e1PortId);
    DrvAtE1ErrMemGet(chipId,e1LinkNo,&e1Err,PDH_ERROR_STATIC_SEND_TO_PM);
    
    QueryResult->Ferr.dwFlag = 16;
    QueryResult->Ferr.dwLow = 0;
    QueryResult->Febe.dwFlag = 16;
    QueryResult->Febe.dwLow = e1Err.fe;
    QueryResult->LcvErr.dwFlag = 16;
    QueryResult->LcvErr.dwLow = 0;
    QueryResult->CrcErr.dwFlag = 16;
    QueryResult->CrcErr.dwLow = e1Err.crc;
    
    DRV_AT_E1_PRINT(DRV_AT_E1_ERR_QUERY,"[CE1TAN][E1 ERR QUERY]:%s line %d, subcardId is %u,e1PortId is %u,fe errCount is 0x%x,crc errCount is 0x%x",__FILE__, __LINE__,subcardId,e1PortId,e1Err.fe,e1Err.crc);
    return ret;
}    

/*******************************************************************************
* 函数名称:   DrvE1AlarmThreadLaunch
* 功能描述:   起一个查询E1告警的线程
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-16     v1.0        chenbo    create
********************************************************************************/ 

WORD32 DrvE1AlarmThreadLaunch(BYTE chipId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32* pSubcardId = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(&g_atE1AlmThrMod[chipId]);
    memset(&g_atE1AlmThrMod[chipId], 0, sizeof(struct mod_thread));
    
    pSubcardId = malloc(sizeof(WORD32));
    CHECK_AT_POINTER_NULL(pSubcardId);
    memset(pSubcardId,0,sizeof(WORD32));
    
    *pSubcardId = (WORD32)(chipId+1);

    init_thread(&g_atE1AlmThrMod[chipId], 0, 0, 80, DrvAtE1AlmDetectScan, pSubcardId);
    ret = start_mod_thread(&g_atE1AlmThrMod[chipId]);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, start_mod_thread() ret=0x%x.\n", __FILE__, __LINE__, ret);
        if(NULL != pSubcardId)
        {
            free(pSubcardId);
            pSubcardId = NULL;
        }
        return DRV_PDH_ALM_THR_LAUNCH_FAILED;    
    }
    
    g_threadHdl[chipId].E1_ALARM_THR_HANDLE = g_atE1AlmThrMod[chipId].pthrid;
    ROSNG_TRACE_DEBUG("\nE1 ALARM THREAD LAUNCH SUCCESS!\n");
    return ret;
}


/*******************************************************************************
* 函数名称:   DrvE1ErrorThreadLaunch
* 功能描述:   起一个查询E1误码的线程
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-16     v1.0        chenbo    create
********************************************************************************/ 

WORD32 DrvE1ErrorThreadLaunch(BYTE chipId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32* pSubcardId = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(&g_atE1ErrThrMod[chipId]);
    memset(&g_atE1ErrThrMod[chipId], 0, sizeof(struct mod_thread));
    
    pSubcardId = malloc(sizeof(WORD32));
    CHECK_AT_POINTER_NULL(pSubcardId );
    memset(pSubcardId,0,sizeof(WORD32));
    
    *pSubcardId = (WORD32)(chipId+1);
    init_thread(&g_atE1ErrThrMod[chipId], 0, 0, 80, DrvAtE1ErrDetectScan, pSubcardId);
    ret = start_mod_thread(&g_atE1ErrThrMod[chipId]);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, start_mod_thread() ret=0x%x.\n", __FILE__, __LINE__, ret);
        if(NULL !=pSubcardId)
        {
            free(pSubcardId);
            pSubcardId = NULL;
        }
        return DRV_PDH_ALM_THR_LAUNCH_FAILED;    
    }    
    
    g_threadHdl[chipId].E1_ERROR_THR_HANDLE = g_atE1ErrThrMod[chipId].pthrid;
    ROSNG_TRACE_DEBUG("\nE1 ERROR THREAD LAUNCH SUCCESS!\n");
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvE1AlarmThreadDelete
* 功能描述:   删除一个查询E1告警的线程
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-16     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvE1AlarmThreadDelete(BYTE chipId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(&g_atE1AlmThrMod[chipId]);

    stop_mod_thread(&g_atE1AlmThrMod[chipId]);    
    g_almthrcount[chipId] = 0;
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvE1ErrorThreadDelete
* 功能描述:   删除一个查询E1误码的线程
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-16     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvE1ErrorThreadDelete(BYTE chipId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(&g_atE1ErrThrMod[chipId]);
    
    stop_mod_thread(&g_atE1ErrThrMod[chipId]);
    g_errthrcount[chipId] = 0;
    return ret;
}



/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_at_e1_print.c
* 文件标识: 
* 其它说明: ce1tan 诊断函数 
* 当前版本: V1.0
* 作    者: 陈博10140635.
* 完成日期: 2013-04-25
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/
#include "AtDriver.h"
#include "AtDevice.h"
#include "AtModuleEth.h"
#include "AtModulePw.h"
#include "AtModuleEncap.h"
#include "AtModuleEth.h"
#include "AtModulePpp.h"
#include "AtEthPort.h"
#include "AtPw.h"
#include "AtPktAnalyzer.h"
#include "AtModulePktAnalyzer.h"
#include "AtHdlcChannel.h"
#include "AtHdlcBundle.h"
#include "drv_at_e1_init.h"
#include "drv_at_e1_port.h"
#include "drv_at_e1_pwe3.h"
#include "drv_at_e1_pm.h"
#include "drv_at_e1_access.h"
#include "drv_at_e1_clk.h"
#include "drv_at_e1_mlppp.h"


/*全局变量*/

/*以下字符串数组供打印使用，排列顺序与port.h 中的枚举顺序相同*/
char*g_servType[2] =
{
    "satop",
    "cesop",
}; 

char*g_frameType[6] =
{
    "unframe",
    "pcm30",
    "pcm31",
    "pcm30crc",
    "pcm31crc",
    "unknown",
}; 

char*g_clkMode[6] =
{
    "system",
    "acr",
    "dcr",
    "slave",
    "loop",
    "unknown",
}; 

char* g_clkStatus[3] = 
{
    "none",
    "master",
    "slave",
};

char*g_timingSrcType[3] =
{
    "none",
    "e1->",
    "pw->",
}; 

char*g_clkState[6] =
{
    "notApplicable", /**< Not applicable */
    "init",              /**< Init */
    "holdOver",      /**< Holdover state */
    "learning",       /**< Learn State */
    "locked",         /**< Locked State */
    "unknown",      /**< Unknown or not applicable state */
};
char*g_fpgaLoopMode[6] =
{  
    "none",
    "line loop in",
    "line loop out",
    "framer loop in",
    "framer loop out",
    "unknown",
}; 

char*g_liuLoopMode[3] = 
{
    "loop out",
    "loop in",
    "none",
};

char*g_isEnable[2] =
{
    "disable",
    "enable",
}; 

char*g_isUp[2] =
{
    "down",
    "up",
};

char*g_ethloop[3] =
{
    "none",
    "loop in",
    "loop out",    
};

char*g_isOk[2] =
{
    "ok",
    "failed",
};

char*g_boardType[10] = 
{
    "75 Ohm dz ces",
    "75 Ohm nbic ces",
    "75 Ohm dz mlppp",
    "75 Ohm nbic mlppp",   
    "120 Ohm dz ces",
    "120 Ohm nbic ces",
    "120 Ohm dz mlppp",
    "120 Ohm nbic mlppp",
    "unknow board type",
};

char*g_fcsMode[3] = 
{
    "no fcs",
    "fcs 16",    
    "fcs 32",   
};

char*g_seqMode[3] = 
{
    "short",  
    "long",   
    "unknown", 
};

char*g_fragSize[5] =  
{
    "no fragsize",
    "fragsize 64",       /**< 64-byte fragment */
    "fragsize 128",   /**< 128-byte fragment */
    "fragsize 256" ,  /**< 256-byte fragment */
    "fragsize 512",   /**< 512-byte fragment */
};

/*外部全局变量*/
extern PDH_E1_ALARM  g_atPdhE1Alarm[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_E1_INTF_NUM] ;
extern PDH_E1_ALARM g_atPdhE1HisAlm[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_E1_INTF_NUM] ;
extern PDH_E1_ERR_COUNT g_atPdhE1ErrCount[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_E1_INTF_NUM];
extern DRV_AT_BASE_ADDR g_ce1BaseAddr[AT_MAX_SUBCARD_NUM_PER_SLOT];
extern DRV_AT_PW_PARA g_atE1PwInfo[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_MAX_PW_NUM_PER_SUBCARD] ;
extern DRV_AT_PW_COUNT g_atE1PwCount[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_MAX_PW_NUM_PER_SUBCARD];
extern WORD32 g_almthrcount[AT_MAX_SUBCARD_NUM_PER_SLOT] ;
extern WORD32 g_errthrcount[AT_MAX_SUBCARD_NUM_PER_SLOT] ;
extern WORD32 g_pwThrcount[AT_MAX_SUBCARD_NUM_PER_SLOT] ;
extern WORD32 g_almthrcountDbg[AT_MAX_SUBCARD_NUM_PER_SLOT] ; 
extern WORD32 g_ErrthrcountDbg[AT_MAX_SUBCARD_NUM_PER_SLOT] ; 
extern WORD32  g_pwThrcountDbg[AT_MAX_SUBCARD_NUM_PER_SLOT] ;
extern CE1TAN_THREAD_HANDLE g_threadHdl[AT_MAX_SUBCARD_NUM_PER_SLOT];
extern CE1TAN_THREAD_FLAG g_threadFlag[AT_MAX_SUBCARD_NUM_PER_SLOT];
extern CE1TAN_THREAD_ID g_threadId[AT_MAX_SUBCARD_NUM_PER_SLOT];
extern AtDevice g_atDevHandle[AT_MAX_SUBCARD_NUM_PER_SLOT]  ;     /* at 芯片的芯片句柄 */
extern AtHal g_atDevHal[AT_MAX_SUBCARD_NUM_PER_SLOT] ;            /*at 芯片hal*/
extern WORD32 g_atProductCode[AT_MAX_SUBCARD_NUM_PER_SLOT];
extern AT_E1_PORT_STATS g_atE1PortStats[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_E1_INTF_NUM];
extern AT_CLK_DOMAIN_INFO g_ClkDomainInfo[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_MAX_DOMAIN_NUM_PER_SUBCARD] ;
extern DRV_AT_PPP_LINK_INFO g_atPppLinkInfo[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_MAX_PPP_NUM_PER_SUBCARD];
extern DRV_AT_MLPPP_BUNDLE_INFO g_atMlpppBundleInfo[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_MAX_MLPPP_BUNDLE_NUM_PER_SUBCARD];
    
/*外部函数*/
extern WORD32 DrvAtEthPortObjGet(BYTE chipId,BYTE ethPortId, AtEthPort* atEthPort);
extern int AtCliExecute(const char *cmdString);
extern void AtDeviceStatusClear(AtDevice self);
extern AtDevice DrvAtDevHdlGet(BYTE chipId);
extern WORD32 BSP_CE1TAN_BOARD_TYPE_GET(BYTE subcardId);
extern WORD32 BSP_CE1TAN_EPLD_Read(WORD16 subcardId, WORD32 dwAddr, WORD16 *pwData );
extern WORD32 BSP_IS_FPGA_DOWNLOAD_FINISH(BYTE subcardId);
extern WORD32 BSP_IS_CE1TAN_INIT_OK(BYTE subcardId);
extern WORD32 DrvAtPwObjGet(BYTE chipId,WORD32 pwId,AtPw*atPwObj);
extern WORD32 DrvAtEncapChanObjGet(BYTE chipId, WORD32 encapChanNo,AtEncapChannel* encapChanObj);
extern WORD32 DrvAtEthFlowObjGet(BYTE chipId, WORD32 flowId,AtEthFlow* ethFlowObj);
extern WORD32 DrvAtHdlcLinkObjGet(BYTE chipId, WORD32 encapChanNo, AtHdlcLink *pAtHdlcLink);
extern WORD32 DrvAtMlpppBundleGet(BYTE chipId,WORD32 bundleIndex,AtMpBundle* bundle);



/*******************************************************************************
* 函数名称:   atcli
* 功能描述:   在自己的shell下运行arrive的cli
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 atcmd(const char* cmdName)
{    
    return AtCliExecute(cmdName);
}


/*******************************************************************************
* 函数名称:   printset
* 功能描述:   打印开关
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 printset(WORD32 printType)
{
    g_at_print = printType ;
    
    return DRV_AT_SUCCESS ;
}


/*******************************************************************************
* 函数名称:   clearStatus
* 功能描述:   拷机前清空芯片状态
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 clearStatus(BYTE subcardId)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    
    CHECK_AT_SUBCARD_ID(subcardId);
    AtDeviceStatusClear(DrvAtDevHdlGet(subcardId-1));
    
    return ret;
}

/*******************************************************************************
* 函数名称:   show_prints
* 功能描述:   show打印开关
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
void show_print(void)
{
    ROSNG_TRACE_DEBUG("g_at_print = 0x%x\n",g_at_print);
    return  ;
}


/*******************************************************************************
* 函数名称:   show_sdk_ver
* 功能描述:   show sdk版本号
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 show_dev_ver(BYTE subcardId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    const CHAR *sdkVer = NULL ;
    const CHAR *fpgaVer = NULL;
    
    CHECK_AT_SUBCARD_ID(subcardId);
    CHECK_AT_POINTER_NULL(g_atDevHandle[subcardId-1]);
    
    fpgaVer = AtDeviceVersion(g_atDevHandle[subcardId-1]);
    sdkVer = AtDriverVersion(AtDriverSharedDriverGet()) ;
    
    ROSNG_TRACE_DEBUG("FPGA Version : %s\n",fpgaVer);
    ROSNG_TRACE_DEBUG("SDK Version : %s\n",sdkVer);
    return ret;
}


/*******************************************************************************
* 函数名称:   show_epld_date
* 功能描述:   show epld 编译日期
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 show_epld_date(BYTE subcardId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD16 data = 0;
    
    BSP_CE1TAN_EPLD_Read(subcardId,0x15c,&data);
    
    ROSNG_TRACE_DEBUG("EPLD Version(date) : 0x%x\n",data);
    return ret ;
}


/*******************************************************************************
* 函数名称:   show_dev_info
* 功能描述:   打印设备信息
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 show_dev_info(BYTE subcardId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    eAtRet atRet = cAtOk;
    BYTE chipId = subcardId -1;
    WORD16 value = 0;
    WORD16 boardId = 0;
    WORD16 pcbId = 0;
    WORD16 bomId = 0;
    BYTE sskeyChkRst = 0;
    WORD32 fpgaDwFlag = 0;
    WORD32 devInitFlag = 0;
    WORD32 boardType = 0;
           
    CHECK_AT_CHIP_ID(chipId);
    ret = BSP_CE1TAN_EPLD_Read(subcardId,0,&value);
    if(DRV_AT_SUCCESS != ret) 
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, BSP_CE1TAN_EPLD_Read() FAILED !!\n", __FILE__, __LINE__);   
        return ret;  
    } 
    boardId = value;
    value = 0;
    ret = BSP_CE1TAN_EPLD_Read(subcardId,2,&value);
    if(DRV_AT_SUCCESS != ret) 
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, BSP_CE1TAN_EPLD_Read() FAILED !!\n", __FILE__, __LINE__);   
        return ret;  
    } 
    pcbId = ((value) & 0x00f0) >> 4;
    bomId =  (value) & 0x000f;
    ROSNG_TRACE_DEBUG("\nBoard Name: CE1TAN\n");
    ROSNG_TRACE_DEBUG("BoardId : 0x%x\n",boardId);
    ROSNG_TRACE_DEBUG("PcbId : 0x%x\n",pcbId);
    ROSNG_TRACE_DEBUG("BomId : 0x%x\n",bomId);
    
    boardType = BSP_CE1TAN_BOARD_TYPE_GET(subcardId);
    ROSNG_TRACE_DEBUG("BoardType: %s\n",g_boardType[boardType]);
    
    fpgaDwFlag = (BSP_IS_FPGA_DOWNLOAD_FINISH(subcardId) == 1)?0:1;
    ROSNG_TRACE_DEBUG("DownLoadFpgaOk? : %s\n",g_isOk[fpgaDwFlag]);
    
    devInitFlag = (BSP_IS_CE1TAN_INIT_OK(subcardId) == 1)?0:1;
    ROSNG_TRACE_DEBUG("InitOk? : %s\n",g_isOk[devInitFlag]);
    
    show_epld_date(subcardId);
    show_dev_ver(subcardId);
    ROSNG_TRACE_DEBUG("AtDevice Driver : 0x%08x\n",AtDriverSharedDriverGet());
    ROSNG_TRACE_DEBUG("AtDevice Handle : 0x%08x\n",g_atDevHandle[chipId]);
    ROSNG_TRACE_DEBUG("AtDevice Hal : 0x%08x\n",g_atDevHal[chipId]);
    ROSNG_TRACE_DEBUG("AtDevice ProductCode : 0x%08x\n",g_atProductCode[chipId]);
    atRet = AtDeviceSSKeyCheck(g_atDevHandle[chipId]);
    sskeyChkRst = (atRet == cAtOk)?0:1;
    ROSNG_TRACE_DEBUG("AtSSkeyOk? : %s\n",g_isOk[sskeyChkRst]);
    return ret ;
}

/*******************************************************************************
* 函数名称:   show_base_addr
* 功能描述:   show 基地址
* 输入参数:   无
* 输出参数:   
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 show_base_addr(BYTE subcardId)
{
    BYTE chipId = subcardId -1;
    CHECK_AT_CHIP_ID(chipId) ;
    
    ROSNG_TRACE_DEBUG("\nEPLD BaseAddr is 0x%x\n",g_ce1BaseAddr[chipId].epldBaseAddr);
    ROSNG_TRACE_DEBUG("LIU BaseAddr is 0x%x\n",   g_ce1BaseAddr[chipId].liuBaseAddr);
    ROSNG_TRACE_DEBUG("FPGA BaseAddr is 0x%x\n",g_ce1BaseAddr[chipId].fpgaBaseAddr);
    
    return DRV_AT_SUCCESS;
}

/*******************************************************************************
* 函数名称:   show_thr_info
* 功能描述:   show 线程信息
* 输入参数:   无
* 输出参数:   
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/

WORD32 show_thr_info(BYTE subcardId)
{
    BYTE chipId = subcardId -1;
    CHECK_AT_CHIP_ID(chipId) ;

    ROSNG_TRACE_DEBUG("\nE1 Alarm Thread Flag is %u\n",g_threadFlag[chipId].E1_ALARM_THR_FLAG);
    ROSNG_TRACE_DEBUG("E1 Alarm Thread Handle is 0x%x\n",g_threadHdl[chipId].E1_ALARM_THR_HANDLE);
    ROSNG_TRACE_DEBUG("E1 Alarm Thread Id is %u\n",g_threadId[chipId].E1_ALARM_THR_ID);
    ROSNG_TRACE_DEBUG("E1 Alarm Thread Count is %u\n",g_almthrcount[chipId]);
    ROSNG_TRACE_DEBUG("E1 Alarm Thread Debug Count is %u\n",g_almthrcountDbg[chipId]);
    
    ROSNG_TRACE_DEBUG("\nE1 Error Thread Flag is %u\n",g_threadFlag[chipId].E1_ERROR_THR_FLAG);
    ROSNG_TRACE_DEBUG("E1 Error Thread Handle is 0x%x\n",g_threadHdl[chipId].E1_ERROR_THR_HANDLE);
    ROSNG_TRACE_DEBUG("E1 Error Thread Id is %u\n",g_threadId[chipId].E1_ERROR_THR_ID);
    ROSNG_TRACE_DEBUG("E1 Error Thread Count is %u\n",g_errthrcount[chipId]);
    ROSNG_TRACE_DEBUG("E1 Error Thread Debug Count is %u\n",g_ErrthrcountDbg[chipId]);
    
    ROSNG_TRACE_DEBUG("\nPW Counter Thread Flag is %u\n",g_threadFlag[chipId].PW_COUNT_THR_FLAG);
    ROSNG_TRACE_DEBUG("PW Counter Thread Handle is 0x%x\n",g_threadHdl[chipId].PW_COUNT_THR_HANDLE);
    ROSNG_TRACE_DEBUG("PW Counter Thread Id is %u\n",g_threadId[chipId].PW_COUNT_THR_ID);
    ROSNG_TRACE_DEBUG("PW Counter Thread Count is %u\n",g_pwThrcount[chipId]);
    ROSNG_TRACE_DEBUG("PW Counter Thread Debug Count is %u\n",g_pwThrcountDbg[chipId]);
    
    return DRV_AT_SUCCESS;    
}

/*******************************************************************************
* 函数名称:   show_e1_cfg
* 功能描述:   show e1 端口配置
* 输入参数:   无
* 输出参数:   
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 show_e1_cfg(BYTE subcardId,WORD32 e1PortId)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    BYTE chipId = 0;
    BYTE frameType = 0;
    BYTE clkMode = 0;
    BYTE srcType = 0;
    BYTE fpgaLoopMode = 0;
    BYTE liuLoopMode = 0;
    BYTE isEnable = 0 ;
    WORD32 e1LinkNo = 0;
    WORD32 srcId = 0;
    WORD32 clkState = 0;
    
    chipId = subcardId -1 ;
    e1LinkNo = e1PortId -1 ;
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    
    ret = DrvAtE1FrameTypeGet(chipId,e1LinkNo,&frameType);
    if(DRV_AT_SUCCESS != ret) 
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, DrvAtE1FrameTypeGet() FAILED !!\n", __FILE__, __LINE__);   
        return ret;  
    } 
    ret = DrvAtE1ClkModeGet(chipId,e1LinkNo,&clkMode);
    if(DRV_AT_SUCCESS != ret) 
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, DrvAtE1ClkModeGet() FAILED !!\n", __FILE__, __LINE__);   
        return ret;  
    } 
    ret = DrvAtE1TimingSrcGet(chipId,e1LinkNo,&srcType,&srcId);
    if(DRV_AT_SUCCESS != ret) 
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, DrvAtE1TimingSrcGet() FAILED !!\n", __FILE__, __LINE__);   
        return ret;  
    } 
    ret = DrvAtE1ClkStateGet(chipId,e1LinkNo,&clkState);
    if(DRV_AT_SUCCESS != ret) 
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, DrvAtE1ClkStateGet() FAILED !!\n", __FILE__, __LINE__);   
        return ret;  
    } 
    ret = DrvAtE1LoopBackGet(chipId,e1LinkNo,&fpgaLoopMode);
    if(DRV_AT_SUCCESS != ret) 
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, DrvAtE1LoopBackGet()  FAILED !!\n", __FILE__, __LINE__);   
        return ret;  
    } 
    ret = DrvAtE1StausGet(chipId,e1LinkNo,&isEnable);
    if(DRV_AT_SUCCESS != ret) 
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, DrvAtE1StausGet()  FAILED !!\n", __FILE__, __LINE__);   
        return ret;  
    } 
    ROSNG_TRACE_DEBUG(" \n ****** E1 Link Id: %u ******\n",e1LinkNo);
    ROSNG_TRACE_DEBUG(" - Cfg -\n");
    ROSNG_TRACE_DEBUG(" FrameType :  %s\n",g_frameType[frameType]);
    ROSNG_TRACE_DEBUG(" ClkMode :    %s\n",g_clkMode[clkMode]);
    if(AT_NULL == srcType)
    {
        ROSNG_TRACE_DEBUG(" ClkSource :  %s\n",g_timingSrcType[srcType]);  
    }
    else
    {
        ROSNG_TRACE_DEBUG(" ClkSource :  %s%u\n",g_timingSrcType[srcType],srcId);
        ROSNG_TRACE_DEBUG(" ClkState :   %s\n",g_clkState[clkState]);
    }
    liuLoopMode = g_atE1PortStats[chipId][e1LinkNo].loopbackMode;   
    ROSNG_TRACE_DEBUG(" Port LoopMode :  %s\n",g_liuLoopMode[liuLoopMode]);
    ROSNG_TRACE_DEBUG(" Fpga LoopMode :  %s\n",g_fpgaLoopMode[fpgaLoopMode]);
    ROSNG_TRACE_DEBUG(" ChanStatus : %s\n",g_isEnable[isEnable]);
    
    return ret ;
}

/*******************************************************************************
* 函数名称:   show_e1_alarm
* 功能描述:   打印当前E1通道告警
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-16     v1.0        chenbo    create
********************************************************************************/ 
WORD32 show_e1_alarm(BYTE subcardId,WORD32 e1PortId)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    BYTE chipId = 0; 
    WORD32 e1LinkNo = 0;
    
    CHECK_AT_SUBCARD_ID(subcardId); 
    CHECK_AT_E1PORT_ID(e1PortId);
    chipId = subcardId -1;
    e1LinkNo = e1PortId -1;


    ROSNG_TRACE_DEBUG(" - Alarm -\n");
    ROSNG_TRACE_DEBUG(" PORT-LOS: %u \n",g_atPdhE1Alarm[chipId][e1LinkNo].port_los);
    ROSNG_TRACE_DEBUG(" E1-LOS: %u \n", g_atPdhE1Alarm[chipId][e1LinkNo].e1_los);
    ROSNG_TRACE_DEBUG(" E1-LOF: %u \n", g_atPdhE1Alarm[chipId][e1LinkNo].e1_lof);
    ROSNG_TRACE_DEBUG(" E1-AIS: %u \n", g_atPdhE1Alarm[chipId][e1LinkNo].e1_ais);
    ROSNG_TRACE_DEBUG(" E1-RAI: %u \n", g_atPdhE1Alarm[chipId][e1LinkNo].e1_rai);
    ROSNG_TRACE_DEBUG(" E1-LOMF: %u \n", g_atPdhE1Alarm[chipId][e1LinkNo].e1_lomf);

    return ret ;
}



/*******************************************************************************
* 函数名称:   show_e1_err
* 功能描述:   打印当前误码计数
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-16     v1.0        chenbo    create
********************************************************************************/ 
WORD32 show_e1_err(BYTE subcardId,WORD32 e1PortId)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    BYTE chipId = 0;
    WORD32 e1LinkNo = 0;
    PDH_E1_ERR_COUNT e1Err;
    
    CHECK_AT_SUBCARD_ID(subcardId); 
    CHECK_AT_E1PORT_ID(e1PortId);
    chipId = subcardId -1;
    e1LinkNo = e1PortId -1;
    memset(&e1Err,0,sizeof(PDH_E1_ERR_COUNT));
    
    DrvAtE1ErrMemGet(chipId,e1LinkNo,&e1Err,MONITOR_PERFORMANCE);
    
    ROSNG_TRACE_DEBUG(" - Error -\n");
    ROSNG_TRACE_DEBUG(" BPV EXZ counter: %u\n", e1Err.bpv);
    ROSNG_TRACE_DEBUG(" CRC error counter: %u\n", e1Err.crc);
    ROSNG_TRACE_DEBUG(" FE counter: %u\n", e1Err.fe );
    ROSNG_TRACE_DEBUG(" REI counter: %u\n", e1Err.rei);

    DrvAtE1ErrMemClear(chipId,e1LinkNo,MONITOR_PERFORMANCE);
    
    return ret ;
}


/*******************************************************************************
* 函数名称:   show_e1_info
* 功能描述:  show e1 信息
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-16     v1.0        chenbo    create
********************************************************************************/ 
WORD32  show_e1_info(BYTE subcardId,WORD32 e1PortId)
{
    WORD32 ret = DRV_AT_SUCCESS ;

    CHECK_AT_SUBCARD_ID(subcardId); 
    CHECK_AT_E1PORT_ID(e1PortId);
    
    ret = show_e1_cfg(subcardId,e1PortId);
    ret|=show_e1_alarm(subcardId,e1PortId);
    ret|=show_e1_err(subcardId,e1PortId);
    
    return ret ;

}

    
/*******************************************************************************
* 函数名称:   show_cfg
* 功能描述:   打印通道配置

* 输入参数:   无
* 输出参数:   无

* 返 回 值:      无
* 其它说明:   其它说明

* 修改日期              版本号       修改人        修改内容

* ------------------------------------------------------------------------------ 
* 2013-05-16     v1.0        chenbo    create
********************************************************************************/
WORD32 show_cfg(BYTE subcardId)
{    
    WORD32 ret = DRV_AT_SUCCESS ;
    WORD32 e1PortId = 1;
    
    for(e1PortId = 1;e1PortId <= AT_E1_INTF_NUM ;e1PortId++)
    {
        show_e1_cfg(subcardId,e1PortId);
    }
    
    return ret ;
}

/*******************************************************************************
* 函数名称:   show_alarm
* 功能描述:   show 所有e1告警
* 输入参数:   无
* 输出参数:   
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 show_alarm(BYTE subcardId)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    WORD32 e1PortId = 1;
    
    for(e1PortId = 1;e1PortId <= AT_E1_INTF_NUM ;e1PortId++)
    {
        show_e1_alarm(subcardId,e1PortId);
    }

    return ret ;
}

/*******************************************************************************
* 函数名称:   show_err
* 功能描述:   show 所有e1 误码
* 输入参数:   无
* 输出参数:   
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 show_err(BYTE  subcardId)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    WORD32 e1PortId = 0;
    
    for(e1PortId = 1;e1PortId <= AT_E1_INTF_NUM ;e1PortId++)
    {
        show_e1_err(subcardId,e1PortId);
    }
    
    return ret ;
}

/*******************************************************************************
* 函数名称:   show_alle1_info
* 功能描述:   show 所有e1信息
* 输入参数:   无
* 输出参数:   
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 show_alle1_info(BYTE subcardId)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    WORD32 e1PortId = 1;

    for(e1PortId = 1;e1PortId <= AT_E1_INTF_NUM ;e1PortId++)
    {
        show_e1_info(subcardId,e1PortId);
    }
 
    return ret ;
}

/*******************************************************************************
* 函数名称:   show_pw_info
* 功能描述:   show pw 信息
* 输入参数:   无
* 输出参数:   
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 show_pw_info(BYTE subcardId, WORD32 pwId)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    BYTE chipId = subcardId - 1;
    BYTE isEnable = 0;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_PW_ID(pwId);
    
    ret = DrvAtPwStatusCheck(chipId,pwId);/*check pw status*/
    if(DRV_AT_SUCCESS != ret)
    {   
        //DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, THIS PW IS NOT EXISTED ! !! \n", __FILE__, __LINE__);  
        return ret; 
    }    
    
    ROSNG_TRACE_DEBUG(" \n*** PwId:%u ***\n",pwId);
    ROSNG_TRACE_DEBUG("cip:\t\t%u \n",g_atE1PwInfo[chipId][pwId].cip);
    ROSNG_TRACE_DEBUG("servType:\t%s \n",g_servType[g_atE1PwInfo[chipId][pwId].servMode]);
    ROSNG_TRACE_DEBUG("subcardId:\t%u \n",g_atE1PwInfo[chipId][pwId].chipId + 1);
    ROSNG_TRACE_DEBUG("e1PortId:\t%u \n",g_atE1PwInfo[chipId][pwId].e1LinkNum + 1);
    if(CESOP_MODE == g_atE1PwInfo[chipId][pwId].servMode)
    {
        ROSNG_TRACE_DEBUG("timeslotBitmap:\t0x%08x \n",g_atE1PwInfo[chipId][pwId].timeSlotBitMap);
        ROSNG_TRACE_DEBUG("tsNum:\t\t%u \n",g_atE1PwInfo[chipId][pwId].tsNum);
    }
    ROSNG_TRACE_DEBUG("payloadSize:\t%u \n",g_atE1PwInfo[chipId][pwId].payloadSize);
    ROSNG_TRACE_DEBUG("jitbuffSize:\t%u \n",g_atE1PwInfo[chipId][pwId].jitterBufferSize);
    ROSNG_TRACE_DEBUG("isClkSrc:\t%u\n",g_atE1PwInfo[chipId][pwId].isClkSrc);
    ret = DrvAtPwStausGet(chipId, pwId,&isEnable);
    if(DRV_AT_SUCCESS != ret)
    {   
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PW SATAUS  GET  FAILED ! !! \n", __FILE__, __LINE__);  
        return ret; 
    }
    ROSNG_TRACE_DEBUG("dMac:\t\t{%x.%x.%x.%x.%x.%x}\n",g_atE1PwInfo[chipId][pwId].destMac[0],
                                                                                     g_atE1PwInfo[chipId][pwId].destMac[1],
                                                                                     g_atE1PwInfo[chipId][pwId].destMac[2],
                                                                                     g_atE1PwInfo[chipId][pwId].destMac[3],
                                                                                     g_atE1PwInfo[chipId][pwId].destMac[4],
                                                                                     g_atE1PwInfo[chipId][pwId].destMac[5]
    );
    /*pri，cfi，vlanID从全局表获取*/
    ROSNG_TRACE_DEBUG("vlanTag1:\t{pri1,cfi1,vlanId1}={0x%x,0x%x,0x%x}\n",g_atE1PwInfo[chipId][pwId].vlan1Pri,g_atE1PwInfo[chipId][pwId].vlan1Cfi,g_atE1PwInfo[chipId][pwId].vlan1Info.zteVlan1Id);
    ROSNG_TRACE_DEBUG("vlanTag2:\t{pri2,cfi2,vlanId2}={0x%x,0x%x,0x%x}\n",g_atE1PwInfo[chipId][pwId].vlan2Pri,g_atE1PwInfo[chipId][pwId].vlan2Cfi,g_atE1PwInfo[chipId][pwId].vlan2Info.zteVlan2Id);
    /*vlanId的具体构成从sdk获取*/
    ROSNG_TRACE_DEBUG("vlanId1 detail:\t{cpu,encap,len}={0x%x,0x%x,0x%x}\n",g_atE1PwInfo[chipId][pwId].vlan1Info.cpu,g_atE1PwInfo[chipId][pwId].vlan1Info.encapType,g_atE1PwInfo[chipId][pwId].vlan1Info.pktLen);
    ROSNG_TRACE_DEBUG("vlanId2 detail:\t{portId,isMlppp,channId}={0x%x,0x%x,0x%x}\n",g_atE1PwInfo[chipId][pwId].vlan2Info.portNo,g_atE1PwInfo[chipId][pwId].vlan2Info.isMlppp,g_atE1PwInfo[chipId][pwId].vlan2Info.chanNo);
    ROSNG_TRACE_DEBUG("status:\t\t%s \n",g_isEnable[isEnable]);

    return ret ;
}


/*******************************************************************************
* 函数名称:   show_pw_count
* 功能描述:   show pw 计数
* 输入参数:   无
* 输出参数:   
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 show_pw_count(BYTE subcardId, WORD32 pwId ,BYTE countMode)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    BYTE chipId = 0;
    chipId = subcardId -1 ; 
    DRV_AT_PW_COUNT pwCount ;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_PW_ID(pwId);

    memset(&pwCount,0,sizeof(DRV_AT_PW_COUNT));
    
    ret = DrvAtPwStatusCheck(chipId,pwId);/*check pw status*/
    if(DRV_AT_SUCCESS != ret)
    {   
        //DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, THIS PW IS NOT EXISTED ! !! \n", __FILE__, __LINE__);  
        return DRV_AT_SUCCESS; 
    }
    
    ret = DrvAtPwCountMemGet(chipId,pwId,&pwCount,countMode);/*根据读取计数的模式选择相应的全局表*/
    
    ROSNG_TRACE_DEBUG(" \n**** PwId:%u ****\n",pwId);
    ROSNG_TRACE_DEBUG("TxPackets : %u\n", pwCount.txPackets);
    ROSNG_TRACE_DEBUG("TxPayloadBytes : %u\n", pwCount.txPayloadBytes);
    ROSNG_TRACE_DEBUG("RxPackets : %u\n", pwCount.rxPackets);
    ROSNG_TRACE_DEBUG("RxPayloadBytes : %u\n", pwCount.rxPayloadBytes);    
    ROSNG_TRACE_DEBUG("RxDiscardedPackets : %u\n", pwCount.rxDiscardedPackets);
    ROSNG_TRACE_DEBUG("RxMalformedPackets : %u\n", pwCount.rxMalformedPackets);
    ROSNG_TRACE_DEBUG("RxReorderedPackets : %u\n", pwCount.rxReorderedPackets);
    ROSNG_TRACE_DEBUG("RxLostPackets : %u\n", pwCount.rxLostPackets);
    ROSNG_TRACE_DEBUG("RxOutOfSeqDropPackets: %u\n", pwCount.rxOutOfSeqDropPackets);
    ROSNG_TRACE_DEBUG("RxOamPackets : %u\n", pwCount.rxOamPackets);

    ROSNG_TRACE_DEBUG("TxLbitPackets : %u\n", pwCount.txLbitPackets);
    ROSNG_TRACE_DEBUG("TxRbitPackets : %u\n", pwCount.txRbitPackets);
    ROSNG_TRACE_DEBUG("RxLbitPackets : %u\n", pwCount.rxLbitPackets);
    ROSNG_TRACE_DEBUG("RxRbitPackets : %u\n", pwCount.rxRbitPackets);
    ROSNG_TRACE_DEBUG("RxJitBufOverrun : %u\n", pwCount.rxJitBufOverrun);
    ROSNG_TRACE_DEBUG("RxJitBufUnderrun : %u\n", pwCount.rxJitBufUnderrun);
    ROSNG_TRACE_DEBUG("RxLops : %u\n", pwCount.rxLops);
    ROSNG_TRACE_DEBUG("RxPacketsSentToTdm : %u\n", pwCount.rxPacketsSentToTdm);

    if(MONITOR_PERFORMANCE == countMode)
    {
        ret = DrvAtPwCountMemClear(chipId,pwId,countMode);/*如果想读一段时间的计数，读之前需将全局表清0*/
    }
    return ret ;
}


/*******************************************************************************
* 函数名称:   show_pw_alarm
* 功能描述:   show pw 告警
* 输入参数:   无
* 输出参数:   
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 show_pw_alarm(BYTE chipId ,WORD32 pwId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    AtPw atPwObj = NULL;
    WORD32 alarmBitMap = 0;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_PW_ID(pwId);
    
    ret = DrvAtPwStatusCheck(chipId,pwId);/*check pw status,if failed ,return*/
    if(DRV_AT_SUCCESS != ret)
    {   
        //DRV_AT_E1_PRINT(DRV_AT_THREAD_PRINT,"ERROR: %s line %d, PW IS NOT EXISTED ! !! chipId is %u, pwId is %u\n", __FILE__, __LINE__,chipId,pwId);  
        return ret; 
    }   
    ret = DrvAtPwObjGet(chipId, pwId, &atPwObj);
    if(DRV_AT_SUCCESS != ret)
    {   
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PW OBJECT  GET  FAILED ! !! \n", __FILE__, __LINE__);  
        return ret; 
    }
    
    CHECK_AT_POINTER_NULL(atPwObj);
    /* Get PW alarm */
    alarmBitMap = AtChannelAlarmGet((AtChannel)atPwObj);    
    ROSNG_TRACE_DEBUG(" \n**** PwId:%u ****\n",pwId);
    ROSNG_TRACE_DEBUG("Los Of Packet: %u\n",(0 == (alarmBitMap & cAtPwAlarmTypeLops))?0:1);
    ROSNG_TRACE_DEBUG("JitterBuffer OverRun: %u\n",(0 == (alarmBitMap & cAtPwAlarmTypeJitterBufferOverrun))?0:1);
    ROSNG_TRACE_DEBUG("JitterBuffer UnderRun: %u\n",(0 == (alarmBitMap & cAtPwAlarmTypeJitterBufferOverrun))?0:1);
    ROSNG_TRACE_DEBUG("L Bit: %u\n",(0 == (alarmBitMap & cAtPwAlarmTypeLBit))?0:1);
    ROSNG_TRACE_DEBUG("R Bit: %u\n",(0 == (alarmBitMap & cAtPwAlarmTypeRBit))?0:1);
    ROSNG_TRACE_DEBUG("M Bit: %u\n",(0 == (alarmBitMap & cAtPwAlarmTypeMBit))?0:1);
    
    if(CESOP_MODE == g_atE1PwInfo[chipId][pwId].servMode)
    {
        ROSNG_TRACE_DEBUG("M Bit: %u\n",(0 == (alarmBitMap & cAtPwAlarmTypeMBit))?0:1);
    }
    else if(SATOP_MODE == g_atE1PwInfo[chipId][pwId].servMode)
    {
        ROSNG_TRACE_DEBUG("M Bit: 0\n");
    }

    return ret;
}


/*******************************************************************************
* 函数名称:   show_allpw_info
* 功能描述:   show 所有pw 信息
* 输入参数:   无
* 输出参数:   
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 show_allpw_info(BYTE subcardId)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    WORD32 pwId = 0;

    CHECK_AT_SUBCARD_ID(subcardId);
        
    for(pwId = 0; pwId < AT_MAX_PW_NUM_PER_SUBCARD ; pwId++)
    {
        ret = show_pw_info(subcardId,pwId);
        if(DRV_AT_SUCCESS != ret)
        {
            continue; 
        }
    }     

    return DRV_AT_SUCCESS ;
}

/*******************************************************************************
* 函数名称:   show_allpw_count
* 功能描述:   show 所有pw 计数
* 输入参数:   无
* 输出参数:   
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 show_allpw_count(BYTE subcardId,BYTE countMode)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    WORD32 pwId = 0;

    CHECK_AT_SUBCARD_ID(subcardId);
    
    for(pwId = 0; pwId < AT_MAX_PW_NUM_PER_SUBCARD ; pwId++)
    {
        ret = show_pw_count(subcardId,pwId,countMode);
        if(DRV_AT_SUCCESS != ret)
        {
            continue; 
        }
    }  
    
    return DRV_AT_SUCCESS ;
}


/*******************************************************************************
* 函数名称:   show_eth_info
* 功能描述:   show 以太端口信息
* 输入参数:   无
* 输出参数:   
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 show_eth_info(BYTE subcardId,BYTE ethPortId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 status = 0;
    BYTE loopbackMode = 0;
    BYTE chipId = subcardId -1;

    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_ETHPORT_ID(ethPortId);
    
    ret = DrvAtEthStatusGet(chipId,ethPortId,&status);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ETH PORT STATUS GET FAILED !!\n", __FILE__, __LINE__); 
        return ret;   
    }
    ret = DrvAtEthLoopBackGet(chipId,ethPortId,&loopbackMode);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ETH LOOPBACK MODE GET FAILED !!\n", __FILE__, __LINE__); 
        return ret;   
    }
    ROSNG_TRACE_DEBUG("\n**** EthPortId Info ****\n");
    ROSNG_TRACE_DEBUG("Link Status: %s\n",g_isUp[status]);
    ROSNG_TRACE_DEBUG("LoopBackMode: %s\n",g_ethloop[loopbackMode]);
    
    return ret;
}

/*******************************************************************************
* 函数名称:   show_eth_count
* 功能描述:   以太报文统计
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 show_eth_count(BYTE subcardId,BYTE ethPortId)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    BYTE chipId = 0;
    eAtRet atRet = cAtOk ;
    tAtEthPortCounters ethCouter ;
    AtEthPort atEthPort = NULL;

    chipId = subcardId -1 ; 
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_ETHPORT_ID(ethPortId);
    
    memset(&ethCouter, 0, sizeof(ethCouter));
    ret = DrvAtEthPortObjGet(chipId, ethPortId,&atEthPort) ;    /*get eth port*/
    if(DRV_AT_SUCCESS != ret)   
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ETH PORT GET FAILED  !!\n", __FILE__, __LINE__); 
        return ret; 
    }    
    CHECK_AT_POINTER_NULL(atEthPort);
    
    atRet = AtChannelAllCountersClear((AtChannel)atEthPort, &ethCouter);
    if(cAtOk != atRet)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtChannelAllCountersGet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet)); 
        return atRet; 
    }
    ROSNG_TRACE_DEBUG ("\nTDM----->PSN\n" );  
    ROSNG_TRACE_DEBUG ("txPackets : %d\n",ethCouter.txPackets ); 
    ROSNG_TRACE_DEBUG ("txBytes : %d\n",ethCouter.txBytes);
    ROSNG_TRACE_DEBUG ("txOamPackets : %d\n",ethCouter.txOamPackets );
    ROSNG_TRACE_DEBUG ("txPeriodOamPackets : %d\n",ethCouter.txPeriodOamPackets );
    ROSNG_TRACE_DEBUG ("txPwPackets : %d\n",ethCouter.txPwPackets );
    ROSNG_TRACE_DEBUG ("txPacketsLen0_64 : %d\n",ethCouter.txPacketsLen0_64 );
    ROSNG_TRACE_DEBUG ("txPacketsLen65_127 : %d\n",ethCouter.txPacketsLen65_127 );
    ROSNG_TRACE_DEBUG ("txPacketsLen128_255 : %d\n",ethCouter.txPacketsLen128_255 );
    ROSNG_TRACE_DEBUG ("txPacketsLen256_511 : %d\n",ethCouter.txPacketsLen256_511 );
    ROSNG_TRACE_DEBUG ("txPacketsLen512_1024 : %d\n",ethCouter.txPacketsLen512_1024 );
    ROSNG_TRACE_DEBUG ("txPacketsLen1025_1528 : %d\n",ethCouter.txPacketsLen1025_1528 );
    ROSNG_TRACE_DEBUG ("txPacketsJumbo : %d\n",ethCouter.txPacketsJumbo );
    ROSNG_TRACE_DEBUG ("txTopPackets : %d\n",ethCouter.txTopPackets );
    
    ROSNG_TRACE_DEBUG ("\nPSN----->TDM\n" ); 
    ROSNG_TRACE_DEBUG ("rxPackets : %d\n",ethCouter.rxPackets );
    ROSNG_TRACE_DEBUG ("rxBytes : %d\n",ethCouter.rxBytes );
    ROSNG_TRACE_DEBUG ("rxErrEthHdrPackets : %d\n",ethCouter.rxErrEthHdrPackets );
    ROSNG_TRACE_DEBUG ("rxErrBusPackets : %d\n",ethCouter.rxErrBusPackets );
    ROSNG_TRACE_DEBUG ("rxErrFcsPackets : %d\n",ethCouter.rxErrFcsPackets );
    ROSNG_TRACE_DEBUG ("rxOversizePackets : %d\n",ethCouter.rxOversizePackets );
    ROSNG_TRACE_DEBUG ("rxUndersizePackets : %d\n",ethCouter.rxUndersizePackets );
    ROSNG_TRACE_DEBUG ("rxPacketsLen0_64 : %d\n",ethCouter.rxPacketsLen0_64 );
    ROSNG_TRACE_DEBUG ("rxPacketsLen65_127 : %d\n",ethCouter.rxPacketsLen65_127 );
    ROSNG_TRACE_DEBUG ("rxPacketsLen128_255 : %d\n",ethCouter.rxPacketsLen128_255 );
    ROSNG_TRACE_DEBUG ("rxPacketsLen256_511 : %d\n",ethCouter.rxPacketsLen256_511 );
    ROSNG_TRACE_DEBUG ("rxPacketsLen512_1024 : %d\n",ethCouter.rxPacketsLen512_1024 );
    ROSNG_TRACE_DEBUG ("rxPacketsLen1025_1528 : %d\n",ethCouter.rxPacketsLen1025_1528 );
    ROSNG_TRACE_DEBUG ("rxPacketsJumbo : %d\n",ethCouter.rxPacketsJumbo );
    ROSNG_TRACE_DEBUG ("rxPwUnsupportedPackets : %d\n",ethCouter.rxPwUnsupportedPackets );
    ROSNG_TRACE_DEBUG ("rxErrPwLabelPackets : %d\n",ethCouter.rxErrPwLabelPackets );
    ROSNG_TRACE_DEBUG ("rxDiscardedPackets : %d\n",ethCouter.rxDiscardedPackets );
    ROSNG_TRACE_DEBUG ("rxPausePackets : %d\n",ethCouter.rxPausePackets );
    ROSNG_TRACE_DEBUG ("rxErrPausePackets : %d\n",ethCouter.rxErrPausePackets );
    ROSNG_TRACE_DEBUG ("rxBrdCastPackets : %d\n",ethCouter.rxBrdCastPackets );
    ROSNG_TRACE_DEBUG ("rxMultCastPackets : %d\n",ethCouter.rxMultCastPackets );
    ROSNG_TRACE_DEBUG ("rxArpPackets : %d\n",ethCouter.rxArpPackets );
    ROSNG_TRACE_DEBUG ("rxOamPackets : %d\n",ethCouter.rxOamPackets );
    ROSNG_TRACE_DEBUG ("rxEfmOamPackets : %d\n",ethCouter.rxEfmOamPackets );
    ROSNG_TRACE_DEBUG ("rxErrEfmOamPackets : %d\n",ethCouter.rxErrEfmOamPackets );
    ROSNG_TRACE_DEBUG ("rxEthOamType1Packets : %d\n",ethCouter.rxEthOamType1Packets );
    ROSNG_TRACE_DEBUG ("rxEthOamType2Packets : %d\n",ethCouter.rxEthOamType2Packets );
    ROSNG_TRACE_DEBUG ("rxMplsPackets : %d\n",ethCouter.rxMplsPackets );
    ROSNG_TRACE_DEBUG ("rxErrMplsPackets : %d\n",ethCouter.rxErrMplsPackets );
    ROSNG_TRACE_DEBUG ("rxMplsErrOuterLblPackets : %d\n",ethCouter.rxMplsErrOuterLblPackets );
    ROSNG_TRACE_DEBUG ("rxMplsDataPackets : %d\n",ethCouter.rxMplsDataPackets );
    ROSNG_TRACE_DEBUG ("rxErrPsnPackets : %d\n",ethCouter.rxErrPsnPackets );
    ROSNG_TRACE_DEBUG ("rxPacketsSendToCpu : %d\n",ethCouter.rxPacketsSendToCpu );
    ROSNG_TRACE_DEBUG ("rxPacketsSendToPw : %d\n",ethCouter.rxPacketsSendToPw );

    return ret;
}


/*******************************************************************************
* 函数名称:   show_eth_dump_pkt
* 功能描述:   以太抓包
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 show_eth_dump_pkt(BYTE subcardId,BYTE ethPortId ,BYTE direction)
{
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE chipId = subcardId - 1;
    AtModulePktAnalyzer pktAnalyzerModule = NULL;
    AtPktAnalyzer pktAnalyzerObj = NULL;
    AtEthPort atEthPort = NULL;
    
    CHECK_AT_CHIP_ID(chipId);    
    CHECK_AT_ETHPORT_ID(ethPortId);
    /* Get analyzer Module */
    pktAnalyzerModule = (AtModulePktAnalyzer)AtDeviceModuleGet(DrvAtDevHdlGet(chipId), cAtModulePktAnalyzer);
    if(NULL == pktAnalyzerModule)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PKT ANALYZER MODULE GET FAILED  !!\n", __FILE__, __LINE__); 
        return DRV_AT_PKT_ANALYZE_MODULE_GET_FAILED; 
    }
    ret = DrvAtEthPortObjGet(chipId,ethPortId,&atEthPort);
    if(DRV_AT_SUCCESS != ret)   
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ETH PORT GET FAILED  !!\n", __FILE__, __LINE__); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atEthPort);
    /*Get obj ,if null,then create,if not null ,no need to create*/
    pktAnalyzerObj = AtModulePacketAnalyzerCurrentAnalyzerGet(pktAnalyzerModule);
    if (NULL == pktAnalyzerObj)
    {
        pktAnalyzerObj = AtModulePacketAnalyzerEthPortPktAnalyzerCreate(pktAnalyzerModule, atEthPort);
        if(NULL == pktAnalyzerObj)
        {
            DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PKT ANALYZER OBJ CREATE FAILED  !!\n", __FILE__, __LINE__); 
            return DRV_AT_PKT_ANALYZE_OBJ_CREATE_FAILED; 
        }
    }
    if(PAKCTES_RX_DIR == direction)
    {
        AtPktAnalyzerAnalyzeRxPackets(pktAnalyzerObj);        /* Analyze packets at RX direction */
    }
    else if(PAKCTES_TX_DIR == direction)
    {
        AtPktAnalyzerAnalyzeTxPackets(pktAnalyzerObj);        /* Analyze packets at TX direction */
    }
    
    return ret ;
}


/*******************************************************************************
* 函数名称:   show_domain_info
* 功能描述:   显示时钟域信息
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 show_domain_info(BYTE subcardId,WORD32 clkDomainNo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE chipId = subcardId - 1;
    WORD32 domainIndex = 0;
    WORD32 memberIndex = 0;
    WORD32 i = 0;
    WORD32 j = 0;

    CHECK_AT_CHIP_ID(chipId);

        
    for(i = 0 ;i< AT_MAX_DOMAIN_NUM_PER_SUBCARD ; i++)
    {
        if((clkDomainNo == g_ClkDomainInfo[chipId][i].domainId)
        &&( CLK_DOMAIN_ENABLE == g_ClkDomainInfo[chipId][i].domainEnable))  
        {
            domainIndex = i;
            break ;
        }
    }
    if(AT_MAX_DOMAIN_NUM_PER_SUBCARD == i )
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CAN NOT FIND CLK DOMAIN [%u] .\n", __FILE__, __LINE__,clkDomainNo);
        return DRV_AT_CLK_DOMAIN_INFO_WRONG;
    }
    ROSNG_TRACE_DEBUG("\n***** ClkDomainNo: %u *****\n",clkDomainNo);
    ROSNG_TRACE_DEBUG("--- Domain Info ---\n");
    ROSNG_TRACE_DEBUG("ClockMode: %s\n",g_clkMode[g_ClkDomainInfo[chipId][domainIndex].clkMode]);
    ROSNG_TRACE_DEBUG("Master E1LinkNo: %u\n",g_ClkDomainInfo[chipId][domainIndex].masterE1LinkNo);
    ROSNG_TRACE_DEBUG("--- Domain Member Info --- \n");
    
    for(j = 0;j < AT_MAX_E1_NUM_PER_DOMAIN;j++)
    {
        if(CE1_ENABLE == g_ClkDomainInfo[chipId][domainIndex].memberInfo[j].isEnable)   
        {
            memberIndex = j;
            ROSNG_TRACE_DEBUG("E1LinkNo: %u",g_ClkDomainInfo[chipId][domainIndex].memberInfo[j].e1LinkNo);
            ROSNG_TRACE_DEBUG("/  ClkStatus: %s\n",g_clkStatus[g_ClkDomainInfo[chipId][domainIndex].memberInfo[j].state]);
        }
    }
       
    return ret;
}


/*******************************************************************************
* 函数名称:   show_ppp_info
* 功能描述:   显示ppp 信息
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 show_ppp_info(BYTE subcardId,WORD32 pppLinkNo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE chipId = subcardId - 1;
    CHECK_AT_CHIP_ID(chipId);
    
    ret = DrvAtPppLinkStatusCheck(chipId,pppLinkNo);
    if(DRV_AT_SUCCESS != ret)
    {   
        return ret; 
    }  
   
    ROSNG_TRACE_DEBUG(" \n****** PPP LINK:%u ******\n",pppLinkNo);  
    ROSNG_TRACE_DEBUG("e1LinkNo:%u\n",g_atPppLinkInfo[chipId][pppLinkNo].e1LinkNo);
    ROSNG_TRACE_DEBUG("encapchanNo:0x%x\n",g_atPppLinkInfo[chipId][pppLinkNo].encapChanNo);
    ROSNG_TRACE_DEBUG("flowId:0x%x\n",g_atPppLinkInfo[chipId][pppLinkNo].flowId);
    ROSNG_TRACE_DEBUG("fcsMode:%s\n",g_fcsMode[g_atPppLinkInfo[chipId][pppLinkNo].fcsMode]);
    ROSNG_TRACE_DEBUG("isScramble?:%s\n",g_isEnable[g_atPppLinkInfo[chipId][pppLinkNo].isScrambleEn]);
    ROSNG_TRACE_DEBUG("mlppp bundleIndex:0x%x\n",g_atPppLinkInfo[chipId][pppLinkNo].mlpppBundleIndex);
    ROSNG_TRACE_DEBUG("vlanTag1:\t{pri1,cfi1,vlanId1}={0x%x,0x%x,0x%x}\n",g_atPppLinkInfo[chipId][pppLinkNo].vlanTag1.pri,g_atPppLinkInfo[chipId][pppLinkNo].vlanTag1.cfi,0);
    ROSNG_TRACE_DEBUG("vlanTag2:\t{pri2,cfi2,vlanId2}={0x%x,0x%x,0x%x}\n",g_atPppLinkInfo[chipId][pppLinkNo].vlanTag2.pri,g_atPppLinkInfo[chipId][pppLinkNo].vlanTag2.cfi,0);
    /*vlanId的具体构成从sdk获取*/
    ROSNG_TRACE_DEBUG("vlanId1 detail:\t{cpu,encap,len}={0x%x,0x%x,0x%x}\n",g_atPppLinkInfo[chipId][pppLinkNo].vlanTag1.cpu,g_atPppLinkInfo[chipId][pppLinkNo].vlanTag1.encapType,g_atPppLinkInfo[chipId][pppLinkNo].vlanTag1.pktLen);
    ROSNG_TRACE_DEBUG("vlanId2 detail:\t{portId,isMlppp,channId}={0x%x,0x%x,0x%x}\n",g_atPppLinkInfo[chipId][pppLinkNo].vlanTag2.portNo,g_atPppLinkInfo[chipId][pppLinkNo].vlanTag2.isMlppp,g_atPppLinkInfo[chipId][pppLinkNo].vlanTag2.chanNo);
    return ret;
}


/*******************************************************************************
* 函数名称:   show_allppp_info
* 功能描述:   show 所有ppp 信息
* 输入参数:   无
* 输出参数:   
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 show_allppp_info(BYTE subcardId)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    WORD32 pppLinkNo = 0;

    CHECK_AT_SUBCARD_ID(subcardId);
        
    for(pppLinkNo = 0; pppLinkNo < AT_MAX_PPP_NUM_PER_SUBCARD ; pppLinkNo++)
    {
        ret = show_ppp_info(subcardId,pppLinkNo);
        if(DRV_AT_SUCCESS != ret)
        {
            continue; 
        }
    }     

    return DRV_AT_SUCCESS ;
}

/*******************************************************************************
* 函数名称:   show_mlppp_info
* 功能描述:   显示mlppp 信息
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 show_mlppp_info(BYTE subcardId,WORD32 bundleIndex)
{
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE chipId = subcardId - 1;
    WORD32 j = 0;
    WORD32 memberIndex = 0;
    CHECK_AT_CHIP_ID(chipId);
    
    ret = DrvAtMlpppStatusCheck(chipId,bundleIndex);
    if(DRV_AT_SUCCESS != ret)
    {   
        return ret; 
    }  
    ROSNG_TRACE_DEBUG(" \n****** MLPPP BUNDLE:%u ******\n",bundleIndex);  
    ROSNG_TRACE_DEBUG("bundleId (from pm):%u\n",g_atMlpppBundleInfo[chipId][bundleIndex].bundleId);
    ROSNG_TRACE_DEBUG("flowId:0x%x\n",g_atMlpppBundleInfo[chipId][bundleIndex].flowId);
    ROSNG_TRACE_DEBUG("mrru:%u\n",g_atMlpppBundleInfo[chipId][bundleIndex].mrru);
    ROSNG_TRACE_DEBUG("seqMode:%s\n",g_seqMode[g_atMlpppBundleInfo[chipId][bundleIndex].seqMode]);
    ROSNG_TRACE_DEBUG("fragSize:%s\n",g_fragSize[g_atMlpppBundleInfo[chipId][bundleIndex].fragsize]);
    ROSNG_TRACE_DEBUG("bundle Member Info:\n");
    for(j = 0;j < AT_MAX_PPP_LINK_NUM_PER_MLPPP_BUNDLE;j++)
    {
        if(PPP_LINK_USED == g_atMlpppBundleInfo[chipId][bundleIndex].linkMember[j].status)   
        {
            memberIndex = j;
            ROSNG_TRACE_DEBUG("--pppLinkNo:%u",g_atMlpppBundleInfo[chipId][bundleIndex].linkMember[j].pppLinkNo); 
            ROSNG_TRACE_DEBUG("  e1LinkNo:%u\n",g_atMlpppBundleInfo[chipId][bundleIndex].linkMember[j].e1LinkNo);       
        }
    }
    return ret;
}

/*******************************************************************************
* 函数名称:   show_allmlppp_info
* 功能描述:   显示所有ppp 信息
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 show_allmlppp_info(BYTE subcardId)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    WORD32 bundleIndex = 0;

    CHECK_AT_SUBCARD_ID(subcardId);
        
    for(bundleIndex = 0; bundleIndex < AT_MAX_MLPPP_BUNDLE_NUM_PER_SUBCARD ; bundleIndex++)
    {
        ret = show_mlppp_info(subcardId,bundleIndex);
        if(DRV_AT_SUCCESS != ret)
        {
            continue; 
        }
    }     

    return DRV_AT_SUCCESS ;
}


/*******************************************************************************
* 函数名称:   show_hdlc_count
* 功能描述:   hdlc 统计
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 show_ppp_count(BYTE subcardId,WORD32 pppLinkNo)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    BYTE chipId = 0;
    WORD32 encapChanNo = 0;
    WORD32 flowId = 0;
    eAtRet atRet = cAtOk ;
    AtHdlcChannel atHdlcChannel = NULL;
    AtEncapChannel atEncapChanObj = NULL;
    AtHdlcLink atHdlcLinkObj = NULL;
    AtEthFlow ethFlowObj = NULL;
    tAtPppLinkCounters pppLinkCounters;
    tAtHdlcChannelCounters hdlcCounters;
    tAtEthFlowCounters flowCounters;
    DRV_AT_PPP_LINK_INFO pppLinkInfo;

    chipId = subcardId -1 ; 
    CHECK_AT_CHIP_ID(chipId);
    
    memset(&hdlcCounters, 0, sizeof(hdlcCounters));
    memset(&pppLinkCounters, 0, sizeof(pppLinkCounters));
    memset(&flowCounters, 0, sizeof(flowCounters));
    memset(&pppLinkInfo, 0, sizeof(pppLinkInfo));

    /*检查该条ppp是否被创建*/
    ret = DrvAtPppLinkStatusCheck(chipId,pppLinkNo);
    if(DRV_AT_SUCCESS != ret)
    {   
        return ret; 
    }  
    
    /*获取ppp linkinfo*/
    ret = DrvAtPppInfoGetFromPppLinkNo(chipId,pppLinkNo,&pppLinkInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CAN NOT GET PPP INFO ,CHIP ID IS %u,PPP LINKNO IS %u !!\n", __FILE__, __LINE__,chipId,pppLinkNo);
        return ret;
    }
    encapChanNo = pppLinkInfo.encapChanNo;
    flowId = pppLinkInfo.flowId;
    /*获取encapchan*/
    ret = DrvAtEncapChanObjGet(chipId, encapChanNo, &atEncapChanObj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ENCAP CHANNEL OBJ GET FAILED ! !!CHIP ID IS %u ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
        return ret; 
    }  
    CHECK_AT_POINTER_NULL(atEncapChanObj);
    atHdlcChannel = (AtHdlcChannel)atEncapChanObj;
    /*获取hdlc link*/
    ret = DrvAtHdlcLinkObjGet(chipId,encapChanNo,&atHdlcLinkObj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, HDLC LINK OBJ GET FAILED ! !!CHIP ID IS %u ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atHdlcLinkObj);
    /*获取eth flow*/
    ret = DrvAtEthFlowObjGet(chipId, flowId, &ethFlowObj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ETH FLOW OBJ GET FAILED ! !!CHIP ID IS %u FLOW ID IS %u\n", __FILE__, __LINE__,chipId,flowId); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(ethFlowObj);
    atRet = AtChannelAllCountersClear((AtChannel)atHdlcChannel, &hdlcCounters);
    if(cAtOk != atRet)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtChannelAllCountersGet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet)); 
        return atRet; 
    }
    atRet = AtChannelAllCountersClear((AtChannel)atHdlcLinkObj, &pppLinkCounters);
    if(cAtOk != atRet)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtChannelAllCountersGet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet)); 
        return atRet; 
    }
    atRet = AtChannelAllCountersClear((AtChannel)ethFlowObj, &flowCounters);
    if(cAtOk != atRet)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtChannelAllCountersGet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet)); 
        return atRet; 
    }
    ROSNG_TRACE_DEBUG(" \n******* PppLinkNo:%u *******\n",pppLinkNo);
    ROSNG_TRACE_DEBUG ("- hdlc counter -\n" );  
    ROSNG_TRACE_DEBUG ("txPkt : %d\n",hdlcCounters.txPkt ); 
    ROSNG_TRACE_DEBUG ("txByte : %d\n",hdlcCounters.txByte);
    ROSNG_TRACE_DEBUG ("txGoodPkt : %d\n",hdlcCounters.txGoodPkt );
    ROSNG_TRACE_DEBUG ("txAbortPkt : %d\n",hdlcCounters.txAbortPkt );
    ROSNG_TRACE_DEBUG ("rxPkt : %d\n",hdlcCounters.rxPkt );
    ROSNG_TRACE_DEBUG ("rxByte : %d\n",hdlcCounters.rxByte );
    ROSNG_TRACE_DEBUG ("rxGoodPkt : %d\n",hdlcCounters.rxGoodPkt );
    ROSNG_TRACE_DEBUG ("rxAbortPkt : %d\n",hdlcCounters.rxAbortPkt );
    ROSNG_TRACE_DEBUG ("rxFcsErrPkt : %d\n",hdlcCounters.rxFcsErrPkt );
    ROSNG_TRACE_DEBUG ("rxAddrCtrlErrPkt : %d\n",hdlcCounters.rxAddrCtrlErrPkt );
    ROSNG_TRACE_DEBUG ("rxSapiErrPkt : %d\n",hdlcCounters.rxSapiErrPkt );
    ROSNG_TRACE_DEBUG ("rxErrPkt : %d\n",hdlcCounters.rxErrPkt );
    ROSNG_TRACE_DEBUG ("\n- ppp counter -\n" );  
    ROSNG_TRACE_DEBUG ("txByte : %d\n",pppLinkCounters.txByte ); 
    ROSNG_TRACE_DEBUG ("txPlain : %d\n",pppLinkCounters.txPlain);
    ROSNG_TRACE_DEBUG ("txFragment : %d\n",pppLinkCounters.txFragment );
    ROSNG_TRACE_DEBUG ("txOam : %d\n",pppLinkCounters.txOam );
    ROSNG_TRACE_DEBUG ("txGoodByte : %d\n",pppLinkCounters.txGoodByte );
    ROSNG_TRACE_DEBUG ("txGoodPkt : %d\n",pppLinkCounters.txGoodPkt );
    ROSNG_TRACE_DEBUG ("txDiscardPkt : %d\n",pppLinkCounters.txDiscardPkt );
    ROSNG_TRACE_DEBUG ("rxPlain : %d\n",pppLinkCounters.rxPlain );
    ROSNG_TRACE_DEBUG ("rxFragment : %d\n",pppLinkCounters.rxFragment );
    ROSNG_TRACE_DEBUG ("rxOam : %d\n",pppLinkCounters.rxOam );
    ROSNG_TRACE_DEBUG ("rxExceedMru : %d\n",pppLinkCounters.rxExceedMru );
    ROSNG_TRACE_DEBUG ("rxGoodByte : %d\n",pppLinkCounters.rxGoodByte );
    ROSNG_TRACE_DEBUG ("rxGoodPkt : %d\n",pppLinkCounters.rxGoodPkt );
    ROSNG_TRACE_DEBUG ("rxDiscardPkt : %d\n",pppLinkCounters.rxDiscardPkt );
    ROSNG_TRACE_DEBUG ("\n- eth flow counter -\n" );  
    ROSNG_TRACE_DEBUG ("txByte : %d\n",flowCounters.txByte );
    ROSNG_TRACE_DEBUG ("txPacket : %d\n",flowCounters.txPacket );
    ROSNG_TRACE_DEBUG ("txGoodByte : %d\n",flowCounters.txGoodByte );
    ROSNG_TRACE_DEBUG ("txFragment : %d\n",flowCounters.txFragment ); 
    ROSNG_TRACE_DEBUG ("txNullFragment : %d\n",flowCounters.txNullFragment);
    ROSNG_TRACE_DEBUG ("txDiscardPacket : %d\n",flowCounters.txDiscardPacket );
    ROSNG_TRACE_DEBUG ("txDiscardByte : %d\n",flowCounters.txDiscardByte );
    ROSNG_TRACE_DEBUG ("txGoodPkt : %d\n",flowCounters.txGoodPkt );
    ROSNG_TRACE_DEBUG ("txGoodFragment : %d\n",flowCounters.txGoodFragment );
    ROSNG_TRACE_DEBUG ("txDiscardFragment : %d\n",flowCounters.txDiscardFragment );
    ROSNG_TRACE_DEBUG ("windowViolation : %d\n",flowCounters.windowViolation );
    ROSNG_TRACE_DEBUG ("rxByte : %d\n",flowCounters.rxByte );
    ROSNG_TRACE_DEBUG ("rxPacket : %d\n",flowCounters.rxPacket );
    ROSNG_TRACE_DEBUG ("rxGoodByte : %d\n",flowCounters.rxGoodByte );
    ROSNG_TRACE_DEBUG ("rxGoodPkt : %d\n",flowCounters.rxGoodPkt );
    ROSNG_TRACE_DEBUG ("rxFragment : %d\n",flowCounters.rxFragment );
    ROSNG_TRACE_DEBUG ("rxNullFragment : %d\n",flowCounters.rxNullFragment );
    ROSNG_TRACE_DEBUG ("rxDiscardByte : %d\n",flowCounters.rxDiscardByte );
    ROSNG_TRACE_DEBUG ("rxDiscardPkt : %d\n",flowCounters.rxDiscardPkt );
    ROSNG_TRACE_DEBUG ("mrruExceed : %d\n",flowCounters.mrruExceed );


    return ret;
}

/*******************************************************************************
* 函数名称:   show_allppp_count
* 功能描述:   显示所有ppp 计数
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 show_allppp_count(BYTE subcardId)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    WORD32 pppLinkNo = 0;

    CHECK_AT_SUBCARD_ID(subcardId);
        
    for(pppLinkNo = 0; pppLinkNo < AT_MAX_PPP_NUM_PER_SUBCARD ; pppLinkNo++)
    {
        ret = show_ppp_count(subcardId,pppLinkNo);
        if(DRV_AT_SUCCESS != ret)
        {
            continue; 
        }
    }     

    return DRV_AT_SUCCESS ;
}
/*******************************************************************************
* 函数名称:   show_mlppp_count
* 功能描述:   mlppp 统计
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 show_mlppp_count(BYTE subcardId,WORD32 bundleIndex)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    BYTE chipId = 0;
    eAtRet atRet = cAtOk ;
    WORD32 flowId = 0;
    AtEthFlow ethFlowObj = NULL;
    tAtHdlcBundleCounters bundleCounters ;
    tAtEthFlowCounters flowCounters;
    AtMpBundle mpBundle = NULL;
    DRV_AT_MLPPP_BUNDLE_INFO mlpppInfo;

    chipId = subcardId -1 ; 
    CHECK_AT_CHIP_ID(chipId);

    memset(&bundleCounters, 0, sizeof(bundleCounters));
    memset(&flowCounters, 0, sizeof(flowCounters));
    memset(&mlpppInfo, 0, sizeof(mlpppInfo));
    
    ret = DrvAtMlpppStatusCheck(chipId,bundleIndex);
    if(DRV_AT_SUCCESS != ret)
    {   
        return ret; 
    }  
    /*获取ppp linkinfo*/
    ret = DrvAtMlpppInfoGetFromIndex(chipId,bundleIndex,&mlpppInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CAN NOT GET PPP INFO ,CHIP ID IS %u,BUNDLE ID  IS %u !!\n", __FILE__, __LINE__,chipId,bundleIndex);
        return ret;
    }
    flowId = mlpppInfo.flowId;
    ret = DrvAtMlpppBundleGet(chipId,bundleIndex,&mpBundle);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, MLPPP BUNDLE  GET FAILED ! !!CHIP ID IS %u BUNDLE ID IS %u\n", __FILE__, __LINE__,chipId,bundleIndex); 
        return ret; 
    }  
    CHECK_AT_POINTER_NULL(mpBundle);
    /*获取eth flow*/
    ret = DrvAtEthFlowObjGet(chipId, flowId, &ethFlowObj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ETH FLOW OBJ GET FAILED ! !!CHIP ID IS %u FLOW ID IS %u\n", __FILE__, __LINE__,chipId,flowId); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(ethFlowObj);
    atRet = AtChannelAllCountersClear((AtChannel)mpBundle, &bundleCounters);
    if(cAtOk != atRet)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtChannelAllCountersGet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet)); 
        return atRet; 
    }
    atRet = AtChannelAllCountersClear((AtChannel)ethFlowObj, &flowCounters);
    if(cAtOk != atRet)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtChannelAllCountersGet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet)); 
        return atRet; 
    }
    ROSNG_TRACE_DEBUG(" \n******* MlpppBundle index:%u *******\n",bundleIndex);
    ROSNG_TRACE_DEBUG ("- bundle counter -\n" );  
    ROSNG_TRACE_DEBUG ("txPkt : %d\n",bundleCounters.txPkt ); 
    ROSNG_TRACE_DEBUG ("txByte : %d\n",bundleCounters.txByte);
    ROSNG_TRACE_DEBUG ("txAbortPkt : %d\n",bundleCounters.txAbortPkt );
    ROSNG_TRACE_DEBUG ("txGoodByte : %d\n",bundleCounters.txGoodByte );
    ROSNG_TRACE_DEBUG ("txGoodPkt : %d\n",bundleCounters.txGoodPkt );
    ROSNG_TRACE_DEBUG ("txFragmentPkt : %d\n",bundleCounters.txFragmentPkt );
    ROSNG_TRACE_DEBUG ("txDiscardPkt : %d\n",bundleCounters.txDiscardPkt );
    ROSNG_TRACE_DEBUG ("rxPkt : %d\n",bundleCounters.rxPkt );
    ROSNG_TRACE_DEBUG ("rxByte : %d\n",bundleCounters.rxByte );
    ROSNG_TRACE_DEBUG ("rxAbortPkt : %d\n",bundleCounters.rxAbortPkt );
    ROSNG_TRACE_DEBUG ("rxDiscardFragment : %d\n",bundleCounters.rxDiscardFragment );
    ROSNG_TRACE_DEBUG ("rxFragmentPkt : %d\n",bundleCounters.rxFragmentPkt );
    ROSNG_TRACE_DEBUG ("rxDiscardPkt : %d\n",bundleCounters.rxDiscardPkt );
    ROSNG_TRACE_DEBUG ("rxGoodByte : %d\n",bundleCounters.rxGoodByte );
    ROSNG_TRACE_DEBUG ("rxGoodPkt : %d\n",bundleCounters.rxGoodPkt );
    ROSNG_TRACE_DEBUG ("rxQueueGoodPkt : %d\n",bundleCounters.rxQueueGoodPkt );
    ROSNG_TRACE_DEBUG ("rxQueueDiscardPkt : %d\n",bundleCounters.rxQueueDiscardPkt );
    ROSNG_TRACE_DEBUG ("rxReasembleFail : %d\n",bundleCounters.rxReasembleFail );
    ROSNG_TRACE_DEBUG ("\n- eth flow counter -\n" );  
    ROSNG_TRACE_DEBUG ("txByte : %d\n",flowCounters.txByte );
    ROSNG_TRACE_DEBUG ("txPacket : %d\n",flowCounters.txPacket );
    ROSNG_TRACE_DEBUG ("txGoodByte : %d\n",flowCounters.txGoodByte );
    ROSNG_TRACE_DEBUG ("txFragment : %d\n",flowCounters.txFragment ); 
    ROSNG_TRACE_DEBUG ("txNullFragment : %d\n",flowCounters.txNullFragment);
    ROSNG_TRACE_DEBUG ("txDiscardPacket : %d\n",flowCounters.txDiscardPacket );
    ROSNG_TRACE_DEBUG ("txDiscardByte : %d\n",flowCounters.txDiscardByte );
    ROSNG_TRACE_DEBUG ("txGoodPkt : %d\n",flowCounters.txGoodPkt );
    ROSNG_TRACE_DEBUG ("txGoodFragment : %d\n",flowCounters.txGoodFragment );
    ROSNG_TRACE_DEBUG ("txDiscardFragment : %d\n",flowCounters.txDiscardFragment );
    ROSNG_TRACE_DEBUG ("windowViolation : %d\n",flowCounters.windowViolation );
    ROSNG_TRACE_DEBUG ("rxByte : %d\n",flowCounters.rxByte );
    ROSNG_TRACE_DEBUG ("rxPacket : %d\n",flowCounters.rxPacket );
    ROSNG_TRACE_DEBUG ("rxGoodByte : %d\n",flowCounters.rxGoodByte );
    ROSNG_TRACE_DEBUG ("rxGoodPkt : %d\n",flowCounters.rxGoodPkt );
    ROSNG_TRACE_DEBUG ("rxFragment : %d\n",flowCounters.rxFragment );
    ROSNG_TRACE_DEBUG ("rxNullFragment : %d\n",flowCounters.rxNullFragment );
    ROSNG_TRACE_DEBUG ("rxDiscardByte : %d\n",flowCounters.rxDiscardByte );
    ROSNG_TRACE_DEBUG ("rxDiscardPkt : %d\n",flowCounters.rxDiscardPkt );
    ROSNG_TRACE_DEBUG ("mrruExceed : %d\n",flowCounters.mrruExceed );

    return ret;
}

/*******************************************************************************
* 函数名称:   show_allmlppp_info
* 功能描述:   显示所有mlppp 统计
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 show_allmlppp_count(BYTE subcardId)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    WORD32 bundleIndex = 0;

    CHECK_AT_SUBCARD_ID(subcardId);
        
    for(bundleIndex = 0; bundleIndex < AT_MAX_MLPPP_BUNDLE_NUM_PER_SUBCARD ; bundleIndex++)
    {
        ret = show_mlppp_count(subcardId,bundleIndex);
        if(DRV_AT_SUCCESS != ret)
        {
            continue; 
        }
    }     

    return DRV_AT_SUCCESS ;
}

/*命令列表*/
void ce1help(void)
{
    ROSNG_TRACE_DEBUG("\n-----------------  Help -----------------  \n");
    ROSNG_TRACE_DEBUG("\n1. cfglist :\tshow api list for configuring\n");
    ROSNG_TRACE_DEBUG("2. dbglist :\tshow api list for read&write\n");
    ROSNG_TRACE_DEBUG("3. paralist :\tshow para type \n");
    ROSNG_TRACE_DEBUG("4. showlist :\tshow api list for performance\n");
}
                            
void cfglist(void)
{
   ROSNG_TRACE_DEBUG("\n----------------------------  API For Configuring ----------------------------  \n");
   ROSNG_TRACE_DEBUG("\n1. Board Init :\t\t\tBSP_CE1TAN_Initailize(subcardId) \n");
   ROSNG_TRACE_DEBUG("2. LIU Init :\t\t\tBSP_CE1TAN_LIU_INIT(subcardId) \n");
   ROSNG_TRACE_DEBUG("3. Fpga Init :\t\t\tDrvAtDevInit(subcardId) \n");
   ROSNG_TRACE_DEBUG("4. Board Uninit :\t\tBSP_CE1TAN_Uninitailize(subcardId) \n");
   ROSNG_TRACE_DEBUG("5. Fpga Remove :\t\tDrvAtDevRemove(subcardId) \n");
   ROSNG_TRACE_DEBUG("6. DownLoad FPGA :\t\tCe1tan_Fpga_DownLoad(subcardId)  \n");
   ROSNG_TRACE_DEBUG("7. Print Set :\t\t\tprintset( printType) \n");
   ROSNG_TRACE_DEBUG("8. Liu Loopback :\t\tDrvLiuLoopBackOp(subcardId, e1LinkNo,loopbackMode,option) \n");
   ROSNG_TRACE_DEBUG("9. Eth Loopback :\t\tDrvAtEthLoopBackOp(chipId,ethportId,loopbackMode)\n");
   ROSNG_TRACE_DEBUG("9. Serdes Loopback :\t\tDrvAtEthSerdesLoopBack(chipId,ethportId,loopbackMode)\n");
   ROSNG_TRACE_DEBUG("10. E1 Loopback :\t\tDrvAtE1LoopBackOp(chipId, e1LinkNo, loopbackMode)\n");
   ROSNG_TRACE_DEBUG("11. Frame Mode :\t\tDrvAtE1FrameTypeSet(chipId, e1LinkNo, frameType)\n");
   ROSNG_TRACE_DEBUG("12. Clk Mode :\t\t\tDrvAtE1ClkModeSet(chipId, e1LinkNo, clkMode, srcE1LinkNo)\n");
   ROSNG_TRACE_DEBUG("13.Rx Force Alarm :\t\tDrvAtE1RxAlarmInsert(chipId, e1LinkNo, alarmType, insertFlag)\n");
   ROSNG_TRACE_DEBUG("14.Tx Force Alarm :\t\tDrvAtE1TxAlarmInsert(chipId, e1LinkNo, alarmType, insertFlag)\n");
   ROSNG_TRACE_DEBUG("15.PW ADD :\t\t\tDrvAtPwAddForTest(subcardId,e1PortId,servType,cip,clkMode,tsBitmap,tsNum,enc,jitterBufferSize)\n");
   ROSNG_TRACE_DEBUG("16.PW DEL :\t\t\tDrvAtPwDelForTest(subcardId, e1PortId, servType,clkMode, cip,tsBitmap)\n");
   ROSNG_TRACE_DEBUG("17.Set Thread Flag :\t\tDrvThreadFlagset(chipId,flagType,disable/enable)\n");
}


void dbglist(void)
{
   ROSNG_TRACE_DEBUG("\n----------------------------  Read&Write ----------------------------  \n");
   ROSNG_TRACE_DEBUG("\n1. Liu Read :\t\t\tZteLiuRegShow(subcardId, liuId, addr) \n");
   ROSNG_TRACE_DEBUG("2. Liu Write :\t\t\tZteLiuWrite(subcardId,liuId,addr,value) \n");
   ROSNG_TRACE_DEBUG("3. Epld Read :\t\t\tEpldRegShow(subcardId,offsetAddr) \n");
   ROSNG_TRACE_DEBUG("4. Epld Write :\t\t\tBsp_CE1TAN_EPLD_Write(subcardId,dwAddr,wData)\n");
   ROSNG_TRACE_DEBUG("5. 15K-2 FpgaRead :\t\tArriveFpga2ReadShow(subcardId,addr)\n");
   ROSNG_TRACE_DEBUG("6. 15K-2 FpgaWrite :\t\tArriveFpga2Write(subcardId,offsetAddr,value\n"); 
   ROSNG_TRACE_DEBUG("7. 15K-8 FpgaRead :\t\tArriveFpga8ReadShow(subcardId,addr)\n");
   ROSNG_TRACE_DEBUG("8. 15K-8 FpgaWrite :\t\tArriveFpga8Write(subcardId,offsetAddr,value)\n");
   ROSNG_TRACE_DEBUG("8. FpgaRead(Common) :\t\tArriveFpgaRegShow(subcardId,offsetAddr)\n");
   ROSNG_TRACE_DEBUG("8. FpgaWrite(Common) :\t\tArriveFpgaWrite(subcardId,offsetAddr,value)\n");
   ROSNG_TRACE_DEBUG("9. Ad9557 Read :\t\tAd9557RegShow(subcardId,addr)\n");
   ROSNG_TRACE_DEBUG("10.Ad9557 Write :\t\tBSP_CE1TANAd9557Write(dwSubslotId,wRegAddr,wData)\n");
}


void paralist(void)
{
    ROSNG_TRACE_DEBUG("\n---------------------------- Para  Type----------------------------  \n");
    ROSNG_TRACE_DEBUG("\n1. Ethernet LoopbackMode:\n   0 - LOOPBACK_CANCEL  \n   1 - LOOPBACK_LOCAL \n   2 - LOOPBACK_REMOTE\n");
    ROSNG_TRACE_DEBUG("\n2. Liu LoopbackMode:\n   0 - LOOPBACK LINE  \n   1 - LOOPBACK INTERNAL \n   2 - LOOPBACK NONE\n");        
    ROSNG_TRACE_DEBUG("3. Fpga E1 LoopbackMode:\n   0 - LOOPBACK_CANCEL \n   1 - LINE_LOOPBACK_LOCAL \n   2 - LINE_LOOPBACK_REMOTE \n   3 - FRAMER_LOOPBACK_LOCAL \n   4 - FRAMER_LOOPBACK_REMOTE\n");
    ROSNG_TRACE_DEBUG("4. Fpga E1 FrameMode:\n   0 - UNFRAME \n   1 - PCM30 \n   2 - PCM31 \n   3 - PCM30CRC \n   4 - PCM31CRC\n");
    ROSNG_TRACE_DEBUG("5. Timing Mode:\n   0 - SYS \n   1 - ACR \n   2 - DCR \n   3 - SLAVE\n   4 - LOOP\n");
    ROSNG_TRACE_DEBUG("6. Timing State:\n   0 - NONE\n   1 - MASTER \n   2 - SLAVE\n");
    ROSNG_TRACE_DEBUG("7. Force Rx Alarm:\n   2 - LOF\n");
    ROSNG_TRACE_DEBUG("8. Force Tx Alarm:\n   4 - AIS \n   8 - RAI\n");

}


void showlist(void)
{
    ROSNG_TRACE_DEBUG("\n---------------------------- show function----------------------------  \n");
    ROSNG_TRACE_DEBUG("\n--------base --------\n");
    ROSNG_TRACE_DEBUG("1. show_print(void)\n");
    ROSNG_TRACE_DEBUG("2. show_dev_ver(subcardId)\n");
    ROSNG_TRACE_DEBUG("3. show_epld_date(subcardId)\n");
    ROSNG_TRACE_DEBUG("4. show_base_addr(subcardId)\n");
    ROSNG_TRACE_DEBUG("5. show_dev_info(subcardId)\n");
    ROSNG_TRACE_DEBUG("6. show_thr_info(subcardId)\n");
    ROSNG_TRACE_DEBUG("\n--------For 1 E1 --------\n");
    ROSNG_TRACE_DEBUG("5. show_e1_cfg(subcardId,e1PortId)\n");
    ROSNG_TRACE_DEBUG("6. show_e1_alarm(subcardId,e1PortId)\n");
    ROSNG_TRACE_DEBUG("7. show_e1_err(subcardId, e1PortId)\n");
    ROSNG_TRACE_DEBUG("8. show_e1_info(subcardId, e1PortId) --- cfg, alarm,err\n");
    ROSNG_TRACE_DEBUG("\n--------For 24 E1 --------\n");
    ROSNG_TRACE_DEBUG("8. show_cfg(subcardId) \n");
    ROSNG_TRACE_DEBUG("9. show_alarm(subcardId) \n");
    ROSNG_TRACE_DEBUG("10.show_err(subcardId) \n");
    ROSNG_TRACE_DEBUG("11.show_alle1_info(subcardId) \n");
    ROSNG_TRACE_DEBUG("\n--------For 1 PW --------\n");
    ROSNG_TRACE_DEBUG("12.show_pw_count(subcardId, pwId,countMode)\n");
    ROSNG_TRACE_DEBUG("13.show_pw_info(subcardId, pwId)\n");
    ROSNG_TRACE_DEBUG("\n--------For all PW --------\n");
    ROSNG_TRACE_DEBUG("12.show_allpw_count(subcardId,countMode)\n");
    ROSNG_TRACE_DEBUG("13.show_allpw_info(subcardId)\n");
    ROSNG_TRACE_DEBUG("\n--------For eth --------\n");
    ROSNG_TRACE_DEBUG("14.show_eth_count(subcardId,ethportId)\n");
    ROSNG_TRACE_DEBUG("15.show_eth_info(subcardId,ethportId)\n");
    ROSNG_TRACE_DEBUG("16.show_eth_dump_pkt(subcardId,ethportId,dir)\n");
    ROSNG_TRACE_DEBUG("\n--------For clkdomain --------\n");
    ROSNG_TRACE_DEBUG("17.show_domain_info(subcardId,domainNo)\n");
    ROSNG_TRACE_DEBUG("\n--------arrive cli --------\n");
    ROSNG_TRACE_DEBUG("18.device select(subcardId)\n");
    ROSNG_TRACE_DEBUG("19.device sskey check(subcardId)\n"); 

}


void testlist(void)
{
   ROSNG_TRACE_DEBUG("\n----------------------------  CES ----------------------------  \n");
   ROSNG_TRACE_DEBUG("\n1. DrvAtPwAddForTest(subcardId, e1PortId,servType, cip,clkMode,tsBitmap, tsNum,enc, jitterBufferSize) \n");
   ROSNG_TRACE_DEBUG("2. DrvAtPwDelForTest(subcardId,e1PortId,servType,clkMode,cip,tsBitmap) \n");
   ROSNG_TRACE_DEBUG("3. DrvAtPwAddMaxTest(subcardId,enc,jitterbuff) \n");
   ROSNG_TRACE_DEBUG("4. DrvAtPwDelMaxTest(subcardId)\n");
   ROSNG_TRACE_DEBUG("5. DrvAtPwAddDelLoopTest(subcardId,enc,jitterbuff,loopTime,ifCheckTester)\n");
   ROSNG_TRACE_DEBUG("6. DrvAtPwAddOp(subcardId)\n"); 
   ROSNG_TRACE_DEBUG("7. DrvAtPwDelOp(subcardId)\n");
   ROSNG_TRACE_DEBUG("8. DrvAtPwAddClkDomainTest(subcardId,e1PortId,servType,cip,clkMode,clkState,domainNo,enc,jitterBufferSize)\n");
   ROSNG_TRACE_DEBUG("9. DrvAtPwDelClkDomainTest(subcardId,e1PortId,servType,clkMode,cip,tsBitmap)\n");
   ROSNG_TRACE_DEBUG("\n----------------------------  MLPPP ----------------------------  \n");
   ROSNG_TRACE_DEBUG("10.DrvAtPppCreateOp(subcardId,StartE1PortId,endE1PortId)\n");
   ROSNG_TRACE_DEBUG("11.DrvAtPppDeleteOp(subcardId,StartE1PortId,endE1PortId)\n");
   ROSNG_TRACE_DEBUG("12.DrvAtMlpppCreateOp(subcardId,bundleId,StartE1PortId,endE1PortId)\n");
   ROSNG_TRACE_DEBUG("13.DrvAtMlpppDeleteOp(subcardId,bundleId,StartE1PortId,endE1PortId)\n");
   ROSNG_TRACE_DEBUG("14.DrvAtPppAddDelLoopTest(subcardId,loopTime)\n");





}


/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_tdm_intf.c
* 文件标识: 
* 其它说明: 驱动STM TDM PWE3模块的接口部分的实现函数.
* 当前版本: V1.0
* 作    者: 谢伟生10112265.
* 完成日期: 2013-05-16
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/


#include "drv_tdm_intf.h"
#include "drv_tdm_pwe3.h"
#include "drv_tdm_sdh_alarm.h"
#include "drv_tdm_init.h"
#include "attypes.h"
#include "AtCommon.h"
#include "AtClasses.h"
#include "AtDevice.h"
#include "AtModule.h"
#include "AtModuleSdh.h"
#include "AtSdhLine.h"
#include "AtChannel.h"
#include "AtSdhChannel.h"
#include "AtModulePdh.h"
#include "AtPdhDe1.h"
#include "AtPdhChannel.h"
#include "AtModulePw.h"
#include "AtModuleEth.h"
#include "AtEthPort.h"
#include "AtSerdesController.h"
#include "AtModuleClock.h"
#include "AtClockExtractor.h"
#include "AtZtePtn.h"


/* 全局变量定义 */
/* 数组g_drv_tdm_sdh_intf_info用来保存STM接口的配置信息 */
static DRV_TDM_SDH_INTF_INFO* g_drv_tdm_sdh_intf_info[DRV_TDM_MAX_STM_CARD_NUM] = {NULL, NULL, NULL, NULL};

/* g_drv_tdm_clock_domain数组用来保存时钟域信息 */
static DRV_TDM_CLOCK_DOMAIN* g_drv_tdm_clock_domain[DRV_TDM_MAX_STM_CARD_NUM] = {NULL, NULL, NULL, NULL};

/* g_drv_tdm_e1ClkSwitchAlm用来保存E1链路的时钟切换告警 */
static DRV_TDM_E1_CLK_SWITCH_ALM* g_drv_tdm_e1ClkSwitchAlm[DRV_TDM_MAX_STM_CARD_NUM] = {NULL, NULL, NULL, NULL};

/* 使能E1链路的时钟切换告警的标记,默认非使能.在初始化单板时,使能标记;在卸载单板时,非使能标记.*/
static WORD32 g_drv_tdm_e1ClkSwtichAlmFlag[DRV_TDM_MAX_STM_CARD_NUM] = {0};

/* g_drv_tdm_e1ClkSwitchAlmThread用来保存E1链路的时钟切换告警线程的信息 */
static struct mod_thread g_drv_tdm_e1ClkSwitchAlmThread[DRV_TDM_MAX_STM_CARD_NUM] = {{0}};

/* E1链路的时钟切换告警线程的threadId */
static pthread_t g_drv_tdm_e1ClkSwitchAlmThreadId[DRV_TDM_MAX_STM_CARD_NUM] = {0};

/* E1链路的时钟切换告警线程的TID */
static int g_drv_tdm_e1ClkSwitchAlmTid[DRV_TDM_MAX_STM_CARD_NUM] = {0};


/**************************************************************************
* 函数名称: drv_tdm_sdhIntfMemAllocate
* 功能描述: 动态分配内存
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_sdhIntfMemAllocate(BYTE chipId)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    if (NULL == g_drv_tdm_sdh_intf_info[chipId])
    {
        g_drv_tdm_sdh_intf_info[chipId] = (DRV_TDM_SDH_INTF_INFO *)(malloc(DRV_TDM_MAX_STM_PORT_NUM * sizeof(DRV_TDM_SDH_INTF_INFO)));
        if (NULL == g_drv_tdm_sdh_intf_info[chipId])
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, allocate memory for g_drv_tdm_sdh_intf_info[%u] failed.\n", __FILE__, __LINE__, chipId);
            return DRV_TDM_MEM_ALLOC_FAIL;
        }
        memset(g_drv_tdm_sdh_intf_info[chipId], 0, (DRV_TDM_MAX_STM_PORT_NUM * sizeof(DRV_TDM_SDH_INTF_INFO)));
    }

    if (NULL == g_drv_tdm_e1ClkSwitchAlm[chipId])
    {
        g_drv_tdm_e1ClkSwitchAlm[chipId] = (DRV_TDM_E1_CLK_SWITCH_ALM *)(malloc(DRV_TDM_MAX_STM_PORT_NUM * sizeof(DRV_TDM_E1_CLK_SWITCH_ALM)));
        if (NULL == g_drv_tdm_e1ClkSwitchAlm[chipId])
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, allocate memory for g_drv_tdm_e1ClkSwitchAlm[%u] failed.\n", __FILE__, __LINE__, chipId);
            return DRV_TDM_MEM_ALLOC_FAIL;
        }
        memset(g_drv_tdm_e1ClkSwitchAlm[chipId], 0, (DRV_TDM_MAX_STM_PORT_NUM * sizeof(DRV_TDM_E1_CLK_SWITCH_ALM)));
    }

    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_sdhIntfMemFree
* 功能描述: 释放已经动态分配的内存
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_sdhIntfMemFree(BYTE chipId)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    if (NULL != g_drv_tdm_sdh_intf_info[chipId])
    {
        free(g_drv_tdm_sdh_intf_info[chipId]);
        g_drv_tdm_sdh_intf_info[chipId] = NULL; /* 防止成为野指针 */
    }

    if (NULL != g_drv_tdm_e1ClkSwitchAlm[chipId])
    {
        free(g_drv_tdm_e1ClkSwitchAlm[chipId]);
        g_drv_tdm_e1ClkSwitchAlm[chipId] = NULL;  /* 防止成为野指针 */
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_sdhIntfMemGet
* 功能描述: 获取保存SDH接口配置信息的内存
* 访问的表: 软件表g_drv_tdm_sdh_intf_info
* 修改的表: 无
* 输入参数: chipId: 0~3, portId: 1~8. aug1Id: 1~4.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-04-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_sdhIntfMemGet(BYTE chipId, 
                                      BYTE portId, 
                                      BYTE aug1Id, 
                                      DRV_TDM_SDH_INTF_INFO** ppIntf)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwStmIntfMode = 0;   /* STM端口模式 */
    BYTE stmIntfIndex = 0;      /* STM端口索引号,取值为0~7. */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(ppIntf);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_intf_info[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_INTF_CFG_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_stmIntfModeGet(chipId, portId, &dwStmIntfMode);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_INTF_CFG_MSG, "ERROR: %s line %d, drv_tdm_stmIntfModeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_STM1_INTF == dwStmIntfMode)  /* STM-1 interface mode */
    {
        stmIntfIndex = portId - (BYTE)1;
    }
    else if (DRV_TDM_STM4_INTF == dwStmIntfMode) /* STM-4 interface mode */
    {
        rv = drv_tdm_aug1IndexGet(portId, aug1Id, &stmIntfIndex);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_INTF_CFG_MSG, "ERROR: %s line %d, drv_tdm_aug1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_INTF_CFG_MSG, "ERROR: %s line %d, invalid stmIntfMode, stmIntfMode=%u.\n", __FILE__, __LINE__, dwStmIntfMode);
        return DRV_TDM_INVALID_STM_INTF_MODE;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    *ppIntf = &(g_drv_tdm_sdh_intf_info[chipId][stmIntfIndex]);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1ClkSwitchAlmSave
* 功能描述: 保存E1链路的时钟切换告警.
* 访问的表: 软件表g_drv_tdm_e1ClkSwitchAlm.
* 修改的表: 软件表g_drv_tdm_e1ClkSwitchAlm.
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
*           e1ClkState: E1链路的时钟切换状态.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-11-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1ClkSwitchAlmSave(BYTE chipId, 
                                             BYTE portId, 
                                             BYTE aug1Id, 
                                             BYTE e1LinkId, 
                                             BYTE e1ClkState)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwStmIntfMode = 0;   /* STM端口模式 */
    BYTE stmIntfIndex = 0;   /* STM端口的索引号,取值为0~7. */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);  
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_e1ClkSwitchAlm[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_stmIntfModeGet(chipId, portId, &dwStmIntfMode);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, drv_tdm_stmIntfModeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_STM1_INTF == dwStmIntfMode)  /* STM-1 interface mode */
    {
        stmIntfIndex = portId - (BYTE)1;
    }
    else if (DRV_TDM_STM4_INTF == dwStmIntfMode) /* STM-4 interface mode */
    {
        rv = drv_tdm_aug1IndexGet(portId, aug1Id, &stmIntfIndex);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, drv_tdm_aug1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, invalid stmIntfMode, stmIntfMode=%u.\n", __FILE__, __LINE__, dwStmIntfMode);
        return DRV_TDM_INVALID_STM_INTF_MODE;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    g_drv_tdm_e1ClkSwitchAlm[chipId][stmIntfIndex].e1ClkSwitchAlm[e1LinkId] = e1ClkState;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1ClkSwitchAlmQuery
* 功能描述: 查询E1链路的时钟切换告警.
* 访问的表: 软件表g_drv_tdm_e1ClkSwitchAlm.
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
* 输出参数: *pE1ClkState: 保存E1链路的时钟切换告警.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-11-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1ClkSwitchAlmQuery(BYTE chipId, 
                                               BYTE portId, 
                                               BYTE aug1Id, 
                                               BYTE e1LinkId, 
                                               BYTE *pE1ClkState)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwStmIntfMode = 0;   /* STM端口模式 */
    BYTE stmIntfIndex = 0;   /* STM端口的索引号,取值为0~7. */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);  /* 检查ARRIVE芯片的chipId的范围 */
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pE1ClkState);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_e1ClkSwitchAlm[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_stmIntfModeGet(chipId, portId, &dwStmIntfMode);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, drv_tdm_stmIntfModeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_STM1_INTF == dwStmIntfMode)  /* STM-1 interface mode */
    {
        stmIntfIndex = portId - (BYTE)1;
    }
    else if (DRV_TDM_STM4_INTF == dwStmIntfMode) /* STM-4 interface mode */
    {
        rv = drv_tdm_aug1IndexGet(portId, aug1Id, &stmIntfIndex);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, drv_tdm_aug1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, invalid stmIntfMode, stmIntfMode=%u.\n", __FILE__, __LINE__, dwStmIntfMode);
        return DRV_TDM_INVALID_STM_INTF_MODE;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    *pE1ClkState = 0;  /* 先清零再赋值 */
    *pE1ClkState = g_drv_tdm_e1ClkSwitchAlm[chipId][stmIntfIndex].e1ClkSwitchAlm[e1LinkId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_clkDomainMemAllocate
* 功能描述: 动态分配内存
* 访问的表: 软件表g_drv_tdm_clock_domain.
* 修改的表: 软件表g_drv_tdm_clock_domain.
* 输入参数: chipId: 0~3
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 该内存用来保存时钟域信息.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-23   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_clkDomainMemAllocate(BYTE chipId)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);

    if (NULL == g_drv_tdm_clock_domain[chipId])
    {
        g_drv_tdm_clock_domain[chipId] = (DRV_TDM_CLOCK_DOMAIN *)(malloc(DRV_TDM_MAX_CLK_DOMAIN_NUM * sizeof(DRV_TDM_CLOCK_DOMAIN)));
        if (NULL == g_drv_tdm_clock_domain[chipId])
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, allocate memory for g_drv_tdm_clock_domain[%u] failed.\n", __FILE__, __LINE__, chipId);
            return DRV_TDM_MEM_ALLOC_FAIL;
        }
        memset(g_drv_tdm_clock_domain[chipId], 0, (DRV_TDM_MAX_CLK_DOMAIN_NUM * sizeof(DRV_TDM_CLOCK_DOMAIN)));
    }
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_clkDomainMemFree
* 功能描述: 释放已经动态分配的内存
* 访问的表: 软件表g_drv_tdm_clock_domain.
* 修改的表: 软件表g_drv_tdm_clock_domain.
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 该内存用来保存时钟域信息.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-23   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_clkDomainMemFree(BYTE chipId)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    if (NULL != g_drv_tdm_clock_domain[chipId])
    {
        free(g_drv_tdm_clock_domain[chipId]);
        g_drv_tdm_clock_domain[chipId] = NULL;  /* 防止成为野指针 */
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_clkDomainMemGet
* 功能描述: 获取保存时钟域信息的内存
* 访问的表: 软件表g_drv_tdm_clock_domain
* 修改的表: 无
* 输入参数: chipId: 0~3, clkDomainId: 0~503.
* 输出参数: ppClkDomain 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-23   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_clkDomainMemGet(BYTE chipId, 
                                          WORD32 clkDomainId, 
                                          DRV_TDM_CLOCK_DOMAIN** ppClkDomain)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_CLOCK_DOMAIN_ID(clkDomainId);
    DRV_TDM_CHECK_POINTER_IS_NULL(ppClkDomain);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_clock_domain[chipId]);
    
    *ppClkDomain = &(g_drv_tdm_clock_domain[chipId][clkDomainId]);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1FramingModeSave
* 功能描述: 保存E1帧的成帧模式
* 访问的表: 软件表g_drv_tdm_sdh_intf_info
* 修改的表: 软件表g_drv_tdm_sdh_intf_info
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
*           framingMode: E1帧模式,具体参见定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1FramingModeSave(BYTE chipId, 
                                             BYTE portId, 
                                             BYTE aug1Id, 
                                             BYTE e1LinkId, 
                                             BYTE framingMode)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwStmIntfMode = 0; /* STM端口方式 */
    BYTE stmIntfIndex = 0;   /* STM接口的索引号,取值为0~7. */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_intf_info[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_stmIntfModeGet(chipId, portId, &dwStmIntfMode);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmIntfModeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_STM1_INTF == dwStmIntfMode)  /* STM-1 interface mode */
    {
        stmIntfIndex = portId - (BYTE)1;
    }
    else if (DRV_TDM_STM4_INTF == dwStmIntfMode) /* STM-4 interface mode */
    {
        rv = drv_tdm_aug1IndexGet(portId, aug1Id, &stmIntfIndex);
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_aug1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid stmIntfMode, stmIntfMode=%u.\n", __FILE__, __LINE__, dwStmIntfMode);
        return DRV_TDM_INVALID_STM_INTF_MODE;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    g_drv_tdm_sdh_intf_info[chipId][stmIntfIndex].e1FramingMode[e1LinkId] = framingMode;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1BindingStateSet
* 功能描述: 设置E1链路的绑定状态.
* 访问的表: 软件表g_drv_tdm_sdh_intf_info
* 修改的表: 软件表g_drv_tdm_sdh_intf_info
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
*           state: E1链路的绑定状态,参见DRV_TDM_E1_LINK_UNBINDING/DRV_TDM_E1_LINK_BINDING定义.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 设置E1链路是否绑定PW.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1BindingStateSet(BYTE chipId, 
                                          BYTE portId, 
                                          BYTE aug1Id, 
                                          BYTE e1LinkId, 
                                          BYTE state)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwStmIntfMode = 0; /* STM端口方式 */
    BYTE stmIntfIndex = 0;   /* STM接口的索引号,取值为0~7. */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_intf_info[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_stmIntfModeGet(chipId, portId, &dwStmIntfMode);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmIntfModeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_STM1_INTF == dwStmIntfMode)  /* STM-1 interface mode */
    {
        stmIntfIndex = portId - (BYTE)1;
    }
    else if (DRV_TDM_STM4_INTF == dwStmIntfMode) /* STM-4 interface mode */
    {
        rv = drv_tdm_aug1IndexGet(portId, aug1Id, &stmIntfIndex);
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_aug1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid stmIntfMode, stmIntfMode=%u.\n", __FILE__, __LINE__, dwStmIntfMode);
        return DRV_TDM_INVALID_STM_INTF_MODE;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    g_drv_tdm_sdh_intf_info[chipId][stmIntfIndex].e1BindingState[e1LinkId] = state;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1BindingStateGet
* 功能描述: 获取E1链路的绑定状态.
* 访问的表: 软件表g_drv_tdm_sdh_intf_info
* 修改的表: 无.
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
* 输出参数: *pState: 保存E1链路的绑定状态,参见DRV_TDM_E1_LINK_UNBINDING/DRV_TDM_E1_LINK_BINDING定义.
* 返 回 值: 
* 其它说明: 获取E1链路是否绑定了PW.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1BindingStateGet(BYTE chipId, 
                                           BYTE portId, 
                                           BYTE aug1Id, 
                                           BYTE e1LinkId, 
                                           BYTE *pState)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwStmIntfMode = 0; /* STM端口方式 */
    BYTE stmIntfIndex = 0;   /* STM接口的索引号,取值为0~7. */
    
    DRV_TDM_CHECK_CHIP_ID(chipId); 
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pState);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_intf_info[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_stmIntfModeGet(chipId, portId, &dwStmIntfMode);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmIntfModeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_STM1_INTF == dwStmIntfMode)  /* STM-1 interface mode */
    {
        stmIntfIndex = portId - (BYTE)1;
    }
    else if (DRV_TDM_STM4_INTF == dwStmIntfMode) /* STM-4 interface mode */
    {
        rv = drv_tdm_aug1IndexGet(portId, aug1Id, &stmIntfIndex);
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_aug1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid stmIntfMode, stmIntfMode=%u.\n", __FILE__, __LINE__, dwStmIntfMode);
        return DRV_TDM_INVALID_STM_INTF_MODE;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    *pState = 0;  /* 先清零再赋值 */
    *pState = g_drv_tdm_sdh_intf_info[chipId][stmIntfIndex].e1BindingState[e1LinkId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1FramingModeGet
* 功能描述: 获取E1帧的成帧模式
* 访问的表: 软件表g_drv_tdm_sdh_intf_info
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
* 输出参数: *pFrmMode: 保存E1帧的成帧模式
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1FramingModeGet(BYTE chipId, 
                                           BYTE portId, 
                                           BYTE aug1Id, 
                                           BYTE e1LinkId, 
                                           BYTE *pFrmMode)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwStmIntfMode = 0; /* STM端口方式 */
    BYTE stmIntfIndex = 0;   /* STM接口的索引号,取值为0~7. */
    
    DRV_TDM_CHECK_CHIP_ID(chipId); 
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pFrmMode);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_intf_info[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_stmIntfModeGet(chipId, portId, &dwStmIntfMode);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_stmIntfModeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_STM1_INTF == dwStmIntfMode)  /* STM-1 interface mode */
    {
        stmIntfIndex = portId - (BYTE)1;
    }
    else if (DRV_TDM_STM4_INTF == dwStmIntfMode) /* STM-4 interface mode */
    {
        rv = drv_tdm_aug1IndexGet(portId, aug1Id, &stmIntfIndex);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_aug1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, invalid stmIntfMode, stmIntfMode=%u.\n", __FILE__, __LINE__, dwStmIntfMode);
        return DRV_TDM_INVALID_STM_INTF_MODE;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    *pFrmMode = g_drv_tdm_sdh_intf_info[chipId][stmIntfIndex].e1FramingMode[e1LinkId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1FramingModeSet
* 功能描述: 设置E1的帧模式
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
*           frmMode: E1帧模式,具体参见定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1FramingModeSet(BYTE chipId, 
                                           BYTE portId, 
                                           BYTE aug1Id, 
                                           BYTE e1LinkId, 
                                           WORD32 frmMode)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 e1BindingPwNum = 0;   /* E1链路绑定的PW数量 */
    WORD16 atDevE1Frm = 0;   /* ARRIVE芯片的E1成帧方式 */
    BYTE e1FrmModeOld = 0;   /* 已经配置了的E1帧类型 */
    AtPdhDe1 pE1 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);  /* 检查ARRIVE芯片的chipId的范围 */
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
   
    switch (frmMode)
    {
        case DRV_TDM_UNFRAME_E1:
            atDevE1Frm = cAtPdhE1UnFrm;  /* E1 unframe mode */
            break;
        case DRV_TDM_PCM30_E1:
        case DRV_TDM_PCM31_E1:
            atDevE1Frm = cAtPdhE1Frm;  /* E1 basic frame, FAS/NFAS framing */
            break;
        case DRV_TDM_PCM30CRC_E1:
        case DRV_TDM_PCM31CRC_E1:
            atDevE1Frm = cAtPdhE1MFCrc;  /* E1 Multi-Frame with CRC4 */
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid frmMode=%u.\n", __FILE__, __LINE__, frmMode);
            return DRV_TDM_INVALID_ARGUMENT;  /* 直接返回失败 */
    }

    /* 获取E1链路绑定的PW的数量 */
    rv = drv_tdm_e1BindingPwNumGet(chipId, portId, aug1Id, e1LinkId, &e1BindingPwNum);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, drv_tdm_e1BindingPwNumGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
        return rv;
    }
    /* 获取已经配置了的E1帧类型 */
    rv = drv_tdm_e1FramingModeGet(chipId, portId, aug1Id, e1LinkId, &e1FrmModeOld);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u e1LinkId %u, drv_tdm_e1FramingModeGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, aug1Id, e1LinkId, rv);
        return rv;
    }
    /* 当E1已经绑定了PW且需要新配置的E1帧类型和已经配置好了的E1帧类型不一致时,禁止设置E1帧类型 */
    if ((e1BindingPwNum > 0) && (((BYTE)frmMode) != e1FrmModeOld))
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, E1 has been binded with pw, and the new e1 frame mode is unmatched with old e1 frame mode.\n", __FILE__, __LINE__, chipId, portId, e1LinkId);
        return DRV_TDM_E1_FRAME_MODE_FORBID;
    }
    
    rv = drv_tdm_atPdhE1PtrGet(chipId, portId, aug1Id, e1LinkId, &pE1);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u e1LinkId %u, drv_tdm_atPdhE1PtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, aug1Id, e1LinkId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pE1);
    
    rv = AtPdhChannelFrameTypeSet((AtPdhChannel)pE1, atDevE1Frm); /* Set frame type of E1 channel */
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u e1LinkId %u, AtPdhChannelFrameTypeSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, aug1Id, e1LinkId, rv);
        return DRV_TDM_SDK_API_FAIL;
    }

    if ((DRV_TDM_PCM30_E1 == frmMode) || (DRV_TDM_PCM30CRC_E1 == frmMode))
    {
        /* set signaling Ds0 bitmap of DS1/E1 */
        rv = AtPdhDe1SignalingDs0MaskSet(pE1, DRV_TDM_E1_SIGNALING_MASK);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u e1LinkId %u, AtPdhDe1SignalingDs0MaskSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, aug1Id, e1LinkId, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
        /* 对于PCM30 E1,timeslot16用来传送signaling信令. */
        rv = AtPdhDe1SignalingEnable(pE1, cAtTrue);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u e1LinkId %u, AtPdhDe1SignalingEnable() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, aug1Id, e1LinkId, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    else if ((DRV_TDM_PCM31_E1 == frmMode) || (DRV_TDM_PCM31CRC_E1 == frmMode)) 
    {
        /* set signaling Ds0 bitmap of DS1/E1 */
        rv = AtPdhDe1SignalingDs0MaskSet(pE1, DRV_TDM_E1_SIGNALING_MASK);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u e1LinkId %u, AtPdhDe1SignalingDs0MaskSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, aug1Id, e1LinkId, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
        /* 对于PCM31 E1,timeslot16用来传送数据. */
        rv = AtPdhDe1SignalingEnable(pE1, cAtFalse);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u e1LinkId %u, AtPdhDe1SignalingEnable() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, aug1Id, e1LinkId, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    else
    {
        ;  /* 什么都不执行 */
    }

    /* 保存E1帧的成帧模式 */
    rv = drv_tdm_e1FramingModeSave(chipId, portId, aug1Id, e1LinkId, (BYTE)frmMode);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u e1LinkId %u, drv_tdm_e1FramingModeSave() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, aug1Id, e1LinkId, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_masterE1LinkSave
* 功能描述: 保存主E1链路的时钟信息.
* 访问的表: 软件表g_drv_tdm_clock_domain
* 修改的表: 软件表g_drv_tdm_clock_domain
* 输入参数: chipId: 芯片编号,取值为0~3.
*           clkDomainId: clock domain id,取值为0~503.
*           clkDomainNo: clock domain NO.
*           *pE1Link: E1链路的信息.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-28   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_masterE1LinkSave(BYTE chipId, 
                                          WORD32 clkDomainId, 
                                          WORD32 clkDomainNo, 
                                          const DRV_TDM_E1_LINK *pE1Link)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE e1LinkId = 0;    /* VC4中的E1链路编号,取值为1~63. */
    WORD32 linkId = 0;  /* E1链路在单板上的编号,取值为1~504. */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_CLOCK_DOMAIN_ID(clkDomainId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pE1Link);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_clock_domain[chipId]);
    
    /* 获取VC4中的E1链路的编号. */
    rv = drv_tdm_e1LinkIdGet(pE1Link->tug3Id, pE1Link->tug2Id, pE1Link->tu12Id, &e1LinkId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_e1LinkIdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    /* 根据portId,aug1Id和VC4中的e1LinkId来计算单板上的E1链路编号. */
    rv = drv_tdm_linkIdGet(chipId, pE1Link->portId, pE1Link->au4Id, e1LinkId, &linkId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_linkIdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_LINK_ID(linkId);
    
    g_drv_tdm_clock_domain[chipId][clkDomainId].clkDomainNo = clkDomainNo;
    memcpy(&(g_drv_tdm_clock_domain[chipId][clkDomainId].e1Link[linkId]), pE1Link, sizeof(DRV_TDM_E1_LINK));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_slaveE1LinkSave
* 功能描述: 保存从E1链路的时钟信息.
* 访问的表: 软件表g_drv_tdm_clock_domain
* 修改的表: 软件表g_drv_tdm_clock_domain
* 输入参数: chipId: 芯片编号,取值为0~3.
*           clkDomainId: clock domain id,取值为0~503.
*           *pE1Link: E1链路的信息.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-28   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_slaveE1LinkSave(BYTE chipId, 
                                         WORD32 clkDomainId, 
                                         const DRV_TDM_E1_LINK *pE1Link)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE e1LinkId = 0;    /* VC4中的E1链路编号,取值为1~63. */
    WORD32 linkId = 0;  /* E1链路在单板上的编号,取值为1~504. */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_CLOCK_DOMAIN_ID(clkDomainId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pE1Link);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_clock_domain[chipId]);
    
    /* 获取VC4中的E1链路的编号. */
    rv = drv_tdm_e1LinkIdGet(pE1Link->tug3Id, pE1Link->tug2Id, pE1Link->tu12Id, &e1LinkId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_e1LinkIdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    /* 根据portId,aug1Id和VC4中的e1LinkId来计算单板上的E1链路编号. */
    rv = drv_tdm_linkIdGet(chipId, pE1Link->portId, pE1Link->au4Id, e1LinkId, &linkId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_linkIdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_LINK_ID(linkId);
    
    memcpy(&(g_drv_tdm_clock_domain[chipId][clkDomainId].e1Link[linkId]), pE1Link, sizeof(DRV_TDM_E1_LINK));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1LinkClockClear
* 功能描述: 清除E1链路的时钟信息.
* 访问的表: 软件表g_drv_tdm_clock_domain
* 修改的表: 软件表g_drv_tdm_clock_domain
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
*           clkDomainNo: clock domain NO.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-28   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1LinkClockClear(BYTE chipId, 
                                         BYTE portId, 
                                         BYTE aug1Id, 
                                         BYTE e1LinkId, 
                                         WORD32 clkDomainNo)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 linkId = 0;  /* E1链路在单板上的编号,取值为1~504. */
    WORD32 clkDomainId = 0;  /* 驱动内部的clock domain ID */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* 根据portId,aug1Id和VC4中的e1LinkId来计算单板上的E1链路编号. */
    rv = drv_tdm_linkIdGet(chipId, portId, aug1Id, e1LinkId, &linkId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_linkIdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_LINK_ID(linkId);
    
    rv = drv_tdm_clkDomainIdGet(chipId, clkDomainNo, &clkDomainId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_clkDomainIdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_CLOCK_DOMAIN_ID(clkDomainId);
    
    memset(&(g_drv_tdm_clock_domain[chipId][clkDomainId].e1Link[linkId]), 0, sizeof(DRV_TDM_E1_LINK));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_clkDomainClear
* 功能描述: 清除时钟域信息.
* 访问的表: 软件表g_drv_tdm_clock_domain
* 修改的表: 软件表g_drv_tdm_clock_domain
* 输入参数: chipId: 芯片编号,取值为0~3.
*           clkDomainNo: clock domain NO.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-28   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_clkDomainClear(BYTE chipId, WORD32 clkDomainNo)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 clkDomainId = 0;  /* 驱动内部的clock domain ID */
    DRV_TDM_E1_LINK masterE1Link;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    memset(&masterE1Link, 0, sizeof(DRV_TDM_E1_LINK));
    
    rv = drv_tdm_clkDomainIdGet(chipId, clkDomainNo, &clkDomainId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_clkDomainIdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_CLOCK_DOMAIN_ID(clkDomainId);
    
    rv = drv_tdm_masterE1LinkGet(chipId, clkDomainId, &masterE1Link);
    if (DRV_TDM_MASTER_E1_IS_NOT_EXISTED == rv) /* 不存在主E1链路 */
    {
        /* 当没有主E1链路时,清空整个时钟域软件表 */
        memset(&(g_drv_tdm_clock_domain[chipId][clkDomainId]), 0, sizeof(DRV_TDM_CLOCK_DOMAIN));
        return DRV_TDM_OK;
    }
    else if (DRV_TDM_OK == rv)  /* 存在主E1链路,不需要清除时钟域信息,直接返回成功 */
    {
        return DRV_TDM_OK;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u clkDomainNo %u, drv_tdm_masterE1LinkGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, clkDomainNo, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_clkDomainStateSet
* 功能描述: 设置时钟域的状态.
* 访问的表: 软件表g_drv_tdm_clock_domain
* 修改的表: 软件表g_drv_tdm_clock_domain
* 输入参数: chipId: 芯片编号,取值为0~3.
*           clkDomainId: 时钟域ID,取值为0~503.
*           state: 时钟域状态,具体参见DRV_TDM_CLK_DOMAIN_STATE定义.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-24   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_clkDomainStateSet(BYTE chipId, WORD32 clkDomainId, WORD32 state)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_CLOCK_DOMAIN_ID(clkDomainId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_clock_domain[chipId]);
    
    g_drv_tdm_clock_domain[chipId][clkDomainId].clkDomainState = state;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_clkDomainStateGet
* 功能描述: 获取时钟域的状态.
* 访问的表: 软件表g_drv_tdm_clock_domain
* 修改的表: 无.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           clkDomainId: 时钟域ID,取值为0~503.
* 输出参数: *pState: 保存时钟域状态,具体参见DRV_TDM_CLK_DOMAIN_STATE定义.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-24   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_clkDomainStateGet(BYTE chipId, WORD32 clkDomainId, WORD32 *pState)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_CLOCK_DOMAIN_ID(clkDomainId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pState);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_clock_domain[chipId]);
    
    *pState = 0;  /* 先清零再赋值 */
    *pState = g_drv_tdm_clock_domain[chipId][clkDomainId].clkDomainState;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_freeClkDomainIdGet
* 功能描述: 获取可用的时钟域ID.
* 访问的表: 软件表g_drv_tdm_clock_domain
* 修改的表: 无.
* 输入参数: chipId: 芯片编号,取值为0~3.
* 输出参数: *pClkDomainId: 保存可用的时钟域ID.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-24   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_freeClkDomainIdGet(BYTE chipId, WORD32 *pClkDomainId)
{
    WORD32 i = 0;    /* 循环变量 */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pClkDomainId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_clock_domain[chipId]);
    
    *pClkDomainId = DRV_TDM_MAX_CLK_DOMAIN_NUM;  /* 初始化情况下,赋值domain id为一个无效值 */
    for (i = DRV_TDM_MIN_CLK_DOMAIN_ID; i <= DRV_TDM_MAX_CLK_DOMAIN_ID; i++)
    {
        if (DRV_TDM_CLK_DOMAIN_FREE == g_drv_tdm_clock_domain[chipId][i].clkDomainState)
        {
            *pClkDomainId = i;
            break;  
        }
    }
    if (i > DRV_TDM_MAX_CLK_DOMAIN_ID)
    {
        return DRV_TDM_INVALID_CLK_DOMAIN_ID;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_clkDomainIdGet
* 功能描述: 获取时钟域ID.
* 访问的表: 软件表g_drv_tdm_clock_domain
* 修改的表: 无.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           clkDomainNo: clock domain NO.
* 输出参数: *pClkDomainId: 保存时钟域ID.
* 返 回 值: 
* 其它说明: 通过clock domain NO来获取驱动的clock domain id.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-24   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_clkDomainIdGet(BYTE chipId, 
                                       WORD32 clkDomainNo, 
                                       WORD32 *pClkDomainId)
{
    WORD32 i = 0;    /* 循环变量 */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pClkDomainId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_clock_domain[chipId]);
    
    *pClkDomainId = DRV_TDM_MAX_CLK_DOMAIN_NUM;  /* 初始化情况下,赋值domain id为一个无效值 */
    for (i = DRV_TDM_MIN_CLK_DOMAIN_ID; i <= DRV_TDM_MAX_CLK_DOMAIN_ID; i++)
    {
        if ((DRV_TDM_CLK_DOMAIN_FREE != g_drv_tdm_clock_domain[chipId][i].clkDomainState)
            && (clkDomainNo == g_drv_tdm_clock_domain[chipId][i].clkDomainNo))
        {
            *pClkDomainId = i;
            return DRV_TDM_OK;  /* 通过domain No找到了domain id后直接返回成功 */
        }
    }

    if (i > DRV_TDM_MAX_CLK_DOMAIN_ID)
    {
        return DRV_TDM_CLK_DOMAIN_IS_NOT_EXISTED;  /* 通过domain NO没有找到domain id,则直接返回domain不存在 */
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_clkDomainCheck
* 功能描述: 检查某个时钟域是否已经存在.
* 访问的表: 软件表g_drv_tdm_clock_domain
* 修改的表: 无.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           clkDomainNo: clock domain NO.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-28   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_clkDomainCheck(BYTE chipId, WORD32 clkDomainNo)
{
    WORD32 i = 0;    /* 循环变量 */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_clock_domain[chipId]);
    
    for (i = DRV_TDM_MIN_CLK_DOMAIN_ID; i <= DRV_TDM_MAX_CLK_DOMAIN_ID; i++)
    {
        if ((DRV_TDM_CLK_DOMAIN_FREE != g_drv_tdm_clock_domain[chipId][i].clkDomainState)
            && (clkDomainNo == g_drv_tdm_clock_domain[chipId][i].clkDomainNo))
        {
            return DRV_TDM_CLK_DOMAIN_IS_EXISTED;  /* 时钟域已经存在 */
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_masterE1LinkGet
* 功能描述: 获取时钟域的主E1链路.
* 访问的表: 软件表g_drv_tdm_clock_domain
* 修改的表: 无.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           clkDomainId: clock domain id,取值为0~503.
* 输出参数: *pMasterE1Link: 保存时钟域的主E1链路.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-24   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_masterE1LinkGet(BYTE chipId, 
                                         WORD32 clkDomainId, 
                                         DRV_TDM_E1_LINK *pMasterE1Link)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 i = 0;
    WORD32 clkDomainState = 0;  /* the state of clock domain */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_CLOCK_DOMAIN_ID(clkDomainId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pMasterE1Link);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_clock_domain[chipId]);

    rv = drv_tdm_clkDomainStateGet(chipId, clkDomainId, &clkDomainState);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u clkDomainId %u, drv_tdm_clkDomainStateGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, clkDomainId, rv);
        return rv;
    }
    if (DRV_TDM_CLK_DOMAIN_FREE == clkDomainState)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u clkDomainId %u, the state of clock domain is free.\n", __FILE__, __LINE__, chipId, clkDomainId);
        return DRV_TDM_CLK_DOMAIN_IS_NOT_EXISTED;
    }
    
    memset(pMasterE1Link, 0, sizeof(DRV_TDM_E1_LINK));
    for (i = DRV_TDM_LINK_ID_START; i <= DRV_TDM_LINK_ID_END; i++)
    {
        if ((DRV_TDM_E1_LINK_MASTER == g_drv_tdm_clock_domain[chipId][clkDomainId].e1Link[i].e1LinkClkState)
            && (DRV_TDM_E1_LINK_USED == g_drv_tdm_clock_domain[chipId][clkDomainId].e1Link[i].linkStatus))
        {
            memcpy(pMasterE1Link, &(g_drv_tdm_clock_domain[chipId][clkDomainId].e1Link[i]), sizeof(DRV_TDM_E1_LINK));
            break;
        }
    }
    if (i > DRV_TDM_LINK_ID_END)  /* 没有查找到主E1链路 */
    {
        return DRV_TDM_MASTER_E1_IS_NOT_EXISTED;
    }

    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_masterE1LinkSet
* 功能描述: 设置时钟域的主E1链路.
* 访问的表: 软件表g_drv_tdm_clock_domain
* 修改的表: 软件表g_drv_tdm_clock_domain
* 输入参数: chipId: 芯片编号,取值为0~3.
*           clkDomainId: clock domain id,取值为0~503.
*           portId: STM端口编号,取值为1~8.
*           au4Id: AU4编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-24   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_masterE1LinkSet(BYTE chipId, 
                                        WORD32 clkDomainId, 
                                        BYTE portId, 
                                        BYTE au4Id, 
                                        BYTE e1LinkId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE subslotId = 0;      /* 单板的子槽位号 */
    WORD16 pwId = DRV_TDM_PW_ID_INVALID;
    DRV_TDM_E1_LINK e1Link;   /* E1链路 */
    DRV_TDM_PW_CFG_INFO *pPwCfgInfo = NULL;
    AtDevice pDevice = NULL;
    AtPdhDe1 pE1 = NULL;    /* E1链路 */
    AtModulePw pPwModule = NULL;
    AtPw pPw = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_CLOCK_DOMAIN_ID(clkDomainId);
    DRV_TDM_CHECK_AUG1_ID(au4Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    subslotId = chipId + (BYTE)1;
    memset(&e1Link, 0, sizeof(DRV_TDM_E1_LINK));
    
    rv = drv_tdm_atPdhE1PtrGet(chipId, portId, au4Id, e1LinkId, &pE1);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, drv_tdm_atPdhE1PtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pE1);
    
    /* 对于SATOP PW,e1LinkId与pwId是一一对应的;对于CESOPSN PW,获取e1LinkId绑定的第一条PW. */
    rv = drv_tdm_e1BindingPwIdGet(subslotId, portId, au4Id, e1LinkId, &pwId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, drv_tdm_e1BindingPwIdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_PW_ID(pwId);
    
    /* 获取PW配置信息 */
    rv = drv_tdm_pwMemGet(chipId, pwId, &pPwCfgInfo);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, drv_tdm_pwMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCfgInfo);
    
    e1Link.chipId = chipId;
    e1Link.portId = pPwCfgInfo->acInfo.portId;
    e1Link.au4Id = pPwCfgInfo->acInfo.au4Id;
    e1Link.tug3Id = pPwCfgInfo->acInfo.tug3Id;
    e1Link.tug2Id = pPwCfgInfo->acInfo.tug2Id;
    e1Link.tu12Id = pPwCfgInfo->acInfo.tu12Id;
    e1Link.e1ClkMode = (BYTE)(pPwCfgInfo->acInfo.e1TimingMode);
    e1Link.e1LinkClkState = DRV_TDM_E1_LINK_MASTER;
    e1Link.linkStatus = DRV_TDM_E1_LINK_USED;
    
    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    /* Get PW Module */
    pPwModule = (AtModulePw)AtDeviceModuleGet(pDevice, cAtModulePw); 
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwModule);
    /* Get pseudowire object by its identifier */
    pPw = AtModulePwGetPw(pPwModule, pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPw);
    if (DRV_TDM_TIMING_MODE_ACR == e1Link.e1ClkMode)
    {
        rv = AtChannelTimingSet((AtChannel)pE1, cAtTimingModeAcr, (AtChannel)pPw);
    }
    else if (DRV_TDM_TIMING_MODE_DCR == e1Link.e1ClkMode)
    {
        rv = AtChannelTimingSet((AtChannel)pE1, cAtTimingModeDcr, (AtChannel)pPw);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, invalid timing mode, e1TimingMode=%u.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, e1Link.e1ClkMode);
        return DRV_TDM_INVALID_ARGUMENT;
    }
    if (cAtOk != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, AtChannelTimingSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    rv = drv_tdm_masterE1LinkSave(chipId, clkDomainId, pPwCfgInfo->acInfo.clkDomainNo, &e1Link);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u clkDomainNo %u, drv_tdm_freeClkDomainIdGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pPwCfgInfo->acInfo.clkDomainNo, rv);
        return rv;
    }
    rv = drv_tdm_clkDomainStateSet(chipId, clkDomainId, DRV_TDM_CLK_DOMAIN_USED);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u clkDomainNo %u, drv_tdm_clkDomainStateSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pPwCfgInfo->acInfo.clkDomainNo, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_slaveE1LinkSet
* 功能描述: 设置时钟域的从E1链路.
* 访问的表: 软件表g_drv_tdm_clock_domain
* 修改的表: 软件表g_drv_tdm_clock_domain
* 输入参数: chipId: 芯片编号,取值为0~3.
*           clkDomainId: clock domain id,取值为0~503.
*           portId: STM端口编号,取值为1~8.
*           au4Id: AU4编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-24   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_slaveE1LinkSet(BYTE chipId, 
                                       WORD32 clkDomainId, 
                                       BYTE portId, 
                                       BYTE au4Id, 
                                       BYTE e1LinkId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE subslotId = 0;      /* 单板的子槽位号 */
    BYTE masterE1LinkId = 0;  /* 主E1链路的link ID */
    WORD16 pwId = DRV_TDM_PW_ID_INVALID;
    DRV_TDM_E1_LINK e1Link;   /* E1链路 */
    DRV_TDM_E1_LINK masterE1Link; /* 主E1链路 */
    DRV_TDM_PW_CFG_INFO *pPwCfgInfo = NULL;
    AtPdhDe1 pE1 = NULL;    /* E1链路 */
    AtPdhDe1 pMasterE1 = NULL;   /* 主E1链路 */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_CLOCK_DOMAIN_ID(clkDomainId);
    DRV_TDM_CHECK_AUG1_ID(au4Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    subslotId = chipId + (BYTE)1;
    memset(&e1Link, 0, sizeof(DRV_TDM_E1_LINK));
    memset(&masterE1Link, 0, sizeof(DRV_TDM_E1_LINK));
    
    rv = drv_tdm_atPdhE1PtrGet(chipId, portId, au4Id, e1LinkId, &pE1);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, drv_tdm_atPdhE1PtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pE1);
    
    /* 对于SATOP PW,e1LinkId与pwId是一一对应的;对于CESOPSN PW,获取e1LinkId绑定的第一条PW. */
    rv = drv_tdm_e1BindingPwIdGet(subslotId, portId, au4Id, e1LinkId, &pwId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, drv_tdm_e1BindingPwIdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_PW_ID(pwId);
    
    /* 获取PW配置信息 */
    rv = drv_tdm_pwMemGet(chipId, pwId, &pPwCfgInfo);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, drv_tdm_pwMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCfgInfo);
    
    e1Link.chipId = chipId;
    e1Link.portId = pPwCfgInfo->acInfo.portId;
    e1Link.au4Id = pPwCfgInfo->acInfo.au4Id;
    e1Link.tug3Id = pPwCfgInfo->acInfo.tug3Id;
    e1Link.tug2Id = pPwCfgInfo->acInfo.tug2Id;
    e1Link.tu12Id = pPwCfgInfo->acInfo.tu12Id;
    e1Link.e1ClkMode = (BYTE)(pPwCfgInfo->acInfo.e1TimingMode);
    e1Link.e1LinkClkState = DRV_TDM_E1_LINK_SLAVE;
    e1Link.linkStatus = DRV_TDM_E1_LINK_USED;
    
    rv = drv_tdm_masterE1LinkGet(chipId, clkDomainId, &masterE1Link);  /* 获取主E1链路 */
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, drv_tdm_masterE1LinkGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (e1Link.e1ClkMode != masterE1Link.e1ClkMode)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u clkDomainNo %u portId %u tug3Id %u tug2Id %u tu12Id %u, e1 timing mode is unmatch, e1TimingMode=%u.\n", __FILE__, __LINE__, chipId, pPwCfgInfo->acInfo.clkDomainNo, e1Link.portId, e1Link.tug3Id, e1Link.tug2Id, e1Link.tu12Id, e1Link.e1ClkMode);
        return DRV_TDM_E1_TIMING_MODE_UNMATCH;
    }
    rv = drv_tdm_e1LinkIdGet(masterE1Link.tug3Id, 
                             masterE1Link.tug2Id, 
                             masterE1Link.tu12Id, 
                             &masterE1LinkId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, drv_tdm_e1LinkIdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_atPdhE1PtrGet(masterE1Link.chipId, 
                               masterE1Link.portId, 
                               masterE1Link.au4Id, 
                               masterE1LinkId, 
                               &pMasterE1);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, drv_tdm_atPdhE1PtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, masterE1Link.portId, masterE1LinkId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pMasterE1);
    rv = AtChannelTimingSet((AtChannel)pE1, cAtTimingModeSlave, (AtChannel)pMasterE1);
    if (cAtOk != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, AtChannelTimingSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    rv = drv_tdm_slaveE1LinkSave(chipId, clkDomainId, &e1Link);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u clkDomainNo %u, drv_tdm_slaveE1LinkSave() rv=0x%x.\n", __FILE__, __LINE__, chipId, pPwCfgInfo->acInfo.clkDomainNo, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1TimingModeSave
* 功能描述: 保存E1帧的时钟模式
* 访问的表: 软件表g_drv_tdm_sdh_intf_info
* 修改的表: 软件表g_drv_tdm_sdh_intf_info
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
*           timingMode: E1时钟模式,具体参见定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1TimingModeSave(BYTE chipId, 
                                           BYTE portId, 
                                           BYTE aug1Id, 
                                           BYTE e1LinkId, 
                                           BYTE timingMode)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwStmIntfMode = 0; /* STM端口方式 */
    BYTE stmIntfIndex = 0;   /* STM接口的索引号,取值为0~7. */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_intf_info[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_stmIntfModeGet(chipId, portId, &dwStmIntfMode);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmIntfModeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_STM1_INTF == dwStmIntfMode)  /* STM-1 interface mode */
    {
        stmIntfIndex = portId - (BYTE)1;
    }
    else if (DRV_TDM_STM4_INTF == dwStmIntfMode) /* STM-4 interface mode */
    {
        rv = drv_tdm_aug1IndexGet(portId, aug1Id, &stmIntfIndex);
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_aug1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid stmIntfMode, stmIntfMode=%u.\n", __FILE__, __LINE__, dwStmIntfMode);
        return DRV_TDM_INVALID_STM_INTF_MODE;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    g_drv_tdm_sdh_intf_info[chipId][stmIntfIndex].e1TimingMode[e1LinkId] = timingMode;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1TimingModeGet
* 功能描述: 获取E1链路的时钟模式
* 访问的表: 软件表g_drv_tdm_sdh_intf_info
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
* 输出参数: *pTimingMode: 保存E1的时钟模式.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1TimingModeGet(BYTE chipId, 
                                          BYTE portId, 
                                          BYTE aug1Id, 
                                          BYTE e1LinkId, 
                                          BYTE *pTimingMode)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwStmIntfMode = 0; /* STM端口方式 */
    BYTE stmIntfIndex = 0;   /* STM接口的索引号,取值为0~7. */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);  
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pTimingMode);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_intf_info[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_stmIntfModeGet(chipId, portId, &dwStmIntfMode);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, drv_tdm_stmIntfModeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_STM1_INTF == dwStmIntfMode)  /* STM-1 interface mode */
    {
        stmIntfIndex = portId - (BYTE)1;
    }
    else if (DRV_TDM_STM4_INTF == dwStmIntfMode) /* STM-4 interface mode */
    {
        rv = drv_tdm_aug1IndexGet(portId, aug1Id, &stmIntfIndex);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, drv_tdm_aug1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, invalid stmIntfMode, stmIntfMode=%u.\n", __FILE__, __LINE__, dwStmIntfMode);
        return DRV_TDM_INVALID_STM_INTF_MODE;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);

    *pTimingMode = 0; /* 先清零再赋值 */
    *pTimingMode = g_drv_tdm_sdh_intf_info[chipId][stmIntfIndex].e1TimingMode[e1LinkId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_acrDcrE1TimingSet
* 功能描述: 设置E1链路的自适应ACR/差分DCR时钟.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           au4Id: AU4编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
* 输出参数: 
* 返 回 值: 
* 其它说明: 该函数只能在创建/删除PW是被调用
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-28   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_acrDcrE1TimingSet(BYTE chipId, 
                                           BYTE portId, 
                                           BYTE au4Id, 
                                           BYTE e1LinkId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE subslotId = 0;      /* 单板的子槽位号 */
    BYTE e1BindingState = 0;  /* E1链路是否绑定了PW  */
    WORD16 pwId = DRV_TDM_PW_ID_INVALID;
    WORD32 clkDomainId = DRV_TDM_MAX_CLK_DOMAIN_NUM;  /* 初始时,赋值为无效值 */
    DRV_TDM_PW_CFG_INFO *pPwCfgInfo = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(au4Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    subslotId = chipId + (BYTE)1;

    /* 对于SATOP PW,e1LinkId与pwId是一一对应的;对于CESOPSN PW,获取e1LinkId绑定的第一条PW. */
    rv = drv_tdm_e1BindingPwIdGet(subslotId, portId, au4Id, e1LinkId, &pwId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_e1BindingPwIdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_PW_ID(pwId);
    
    /* 获取PW配置信息 */
    rv = drv_tdm_pwMemGet(chipId, pwId, &pPwCfgInfo);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_pwMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCfgInfo);

    /* 获取E1链路是否绑定了PW */
    rv = drv_tdm_e1BindingStateGet(chipId, portId, au4Id, e1LinkId, &e1BindingState);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_e1BindingStateGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    if (DRV_TDM_E1_LINK_MASTER == pPwCfgInfo->acInfo.e1LinkClkState)  /* master E1 link */
    {
        if (DRV_TDM_E1_LINK_UNBINDING == e1BindingState)  /* unbinding PW */
        {
            rv = drv_tdm_clkDomainCheck(chipId, pPwCfgInfo->acInfo.clkDomainNo);
            if (DRV_TDM_CLK_DOMAIN_IS_EXISTED == rv)  /* 主E1链路已经存在,一个时钟域只能有一个主E1链路 */
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u clkDomainNo %u, master E1 link is existed.\n", __FILE__, __LINE__, chipId, pPwCfgInfo->acInfo.clkDomainNo);
                return rv;
            }
            else if (DRV_TDM_OK == rv)  /* 主E1链路还不存在 */
            {
                ;  /* 函数返回成功,什么都不做,接着往下执行 */
            }
            else  /* 函数返回失败 */
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u clkDomainNo %u, drv_tdm_clkDomainCheck() rv=0x%x.\n", __FILE__, __LINE__, chipId, pPwCfgInfo->acInfo.clkDomainNo, rv);
                return rv;
            }
            rv = drv_tdm_freeClkDomainIdGet(chipId, &clkDomainId);
            if (DRV_TDM_OK != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_freeClkDomainIdGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
                return rv;
            }
            DRV_TDM_CHECK_CLOCK_DOMAIN_ID(clkDomainId);
        }
        else  /* binding pw */
        {
            /* 通过clock domain NO来获取驱动的clock domain id. */
            rv = drv_tdm_clkDomainIdGet(chipId, pPwCfgInfo->acInfo.clkDomainNo, &clkDomainId);
            if (DRV_TDM_OK != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u clkDomainNo %u, drv_tdm_clkDomainIdGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pPwCfgInfo->acInfo.clkDomainNo, rv);
                return rv;
            }
            DRV_TDM_CHECK_CLOCK_DOMAIN_ID(clkDomainId);
        }

        rv = drv_tdm_masterE1LinkSet(chipId, clkDomainId, portId, au4Id, e1LinkId);
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u clkDomainNo %u, drv_tdm_masterE1LinkSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pPwCfgInfo->acInfo.clkDomainNo, rv);
            return rv;
        }
    }
    else if (DRV_TDM_E1_LINK_SLAVE == pPwCfgInfo->acInfo.e1LinkClkState)  /* slave E1 link */
    {
        /* 通过clock domain NO来获取驱动的clock domain id. */
        rv = drv_tdm_clkDomainIdGet(chipId, pPwCfgInfo->acInfo.clkDomainNo, &clkDomainId);
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u clkDomainNo %u, drv_tdm_clkDomainIdGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pPwCfgInfo->acInfo.clkDomainNo, rv);
            return rv;
        }
        DRV_TDM_CHECK_CLOCK_DOMAIN_ID(clkDomainId);
        rv = drv_tdm_slaveE1LinkSet(chipId, clkDomainId, portId, au4Id, e1LinkId);
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u clkDomainNo %u portId %u au4Id %u e1LinkId %u, drv_tdm_slaveE1LinkSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pPwCfgInfo->acInfo.clkDomainNo, portId, au4Id, e1LinkId, rv);
            return rv;
        }
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u au4Id %u e1LinkId %u, invalid E1 link clock state, e1LinkClkState=%u.\n", __FILE__, __LINE__, chipId, portId, au4Id, e1LinkId, pPwCfgInfo->acInfo.e1LinkClkState);
        return DRV_TDM_INVALID_ARGUMENT;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_systemE1TimingSet
* 功能描述: 设置E1链路的系统system时钟.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-28   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_systemE1TimingSet(BYTE chipId, 
                                            BYTE portId, 
                                            BYTE aug1Id, 
                                            BYTE e1LinkId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtPdhDe1 pE1 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atPdhE1PtrGet(chipId, portId, aug1Id, e1LinkId, &pE1);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u e1LinkId %u, drv_tdm_atPdhE1PtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, aug1Id, e1LinkId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pE1);
    
    rv = AtChannelTimingSet((AtChannel)pE1, cAtTimingModeSys, NULL);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u e1LinkId %u, AtChannelTimingSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, aug1Id, e1LinkId, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_loopE1TimingSet
* 功能描述: 设置E1链路的LOOP时钟.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-28   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_loopE1TimingSet(BYTE chipId, 
                                        BYTE portId, 
                                        BYTE aug1Id, 
                                        BYTE e1LinkId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtPdhDe1 pE1 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atPdhE1PtrGet(chipId, portId, aug1Id, e1LinkId, &pE1);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, drv_tdm_atPdhE1PtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pE1);
    
    rv = AtChannelTimingSet((AtChannel)pE1, cAtTimingModeLoop, NULL);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, AtChannelTimingSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1TimingModeSet
* 功能描述: 设置E1链路的时钟模式.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
*           portId: STM端口编号,取值为1~8.
*           au4Id: AU4编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
*           timingMode: E1时钟模式,具体参见DRV_TDM_TIMING_MODE定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-01   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1TimingModeSet(BYTE subslotId, 
                                          BYTE portId, 
                                          BYTE au4Id, 
                                          BYTE e1LinkId, 
                                          WORD32 timingMode)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);  
    DRV_TDM_CHECK_AUG1_ID(au4Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    switch (timingMode)
    {
        case DRV_TDM_TIMING_MODE_SYS:  /* System timing mode */
            rv = drv_tdm_systemE1TimingSet(chipId, portId, au4Id, e1LinkId);
            if (DRV_TDM_OK != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u au4Id %u e1LinkId %u, drv_tdm_systemE1TimingSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, au4Id, e1LinkId, rv);
                return rv;
            }
            break;
        case DRV_TDM_TIMING_MODE_ACR:  /* ACR timing mode */
        case DRV_TDM_TIMING_MODE_DCR:  /* DCR timing mode */
            rv = drv_tdm_acrDcrE1TimingSet(chipId, portId, au4Id, e1LinkId);
            if (DRV_TDM_OK != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u au4Id %u e1LinkId %u, drv_tdm_acrDcrE1TimingSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, au4Id, e1LinkId, rv);
                return rv;
            }
            break;
        case DRV_TDM_TIMING_MODE_LOOP:  /* Loop timing mode */
            rv = drv_tdm_loopE1TimingSet(chipId, portId, au4Id, e1LinkId);
            if (DRV_TDM_OK != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u au4Id %u e1LinkId %u, drv_tdm_loopE1TimingSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, au4Id, e1LinkId, rv);
                return rv;
            }
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid timingMode=%u.\n", __FILE__, __LINE__, timingMode);
            return DRV_TDM_INVALID_ARGUMENT;
    }
    
    /* 保存E1帧的时钟模式 */
    rv = drv_tdm_e1TimingModeSave(chipId, portId, au4Id, e1LinkId, (BYTE)timingMode);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_e1TimingModeSave() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    /* 在设置E1链路的时钟模式时,需要更新PW软件表中的E1链路的时钟模式. */
    rv = drv_tdm_pwE1TimingModeSet(chipId, portId, au4Id, e1LinkId, (BYTE)timingMode);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_pwE1TimingModeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_masterE1TimingStatusGet
* 功能描述: 获取主E1链路的时钟的状态.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4. 
*           portId: STM端口的编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4. 
*           e1LinkId: VC4中的E1链路的编号,取值为1~63.
* 输出参数: *pStatus: 保存主E1链路的时钟状态.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-05   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_masterE1TimingStatusGet(BYTE subslotId, 
                                                   BYTE portId, 
                                                   BYTE aug1Id, 
                                                   BYTE e1LinkId, 
                                                   WORD32 *pStatus)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    eAtClockState atClkState = 0;
    BYTE chipId = 0;
    AtPdhDe1 pE1 = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId); 
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pStatus);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atPdhE1PtrGet(chipId, portId, aug1Id, e1LinkId, &pE1);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, drv_tdm_atPdhE1PtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pE1);

    /* Get clock state */
    atClkState = AtChannelClockStateGet((AtChannel)pE1);
    *pStatus = 0;  /* 赋值之前先清零 */
    if (cAtClockStateUnknown == atClkState)
    {
        *pStatus = DRV_TDM_TIMING_STATE_UNKNOWN;
    }
    else if (cAtClockStateNotApplicable == atClkState)
    {
        *pStatus = DRV_TDM_TIMING_STATE_NOT_APPLICABLE;
    }
    else if (cAtClockStateInit == atClkState)
    {
        *pStatus = DRV_TDM_TIMING_STATE_INIT;
    }
    else if (cAtClockStateHoldOver == atClkState)
    {
        *pStatus = DRV_TDM_TIMING_STATE_HOLDOVER;
    }
    else if (cAtClockStateLearning == atClkState)
    {
        *pStatus = DRV_TDM_TIMING_STATE_LEARNING;
    }
    else if (cAtClockStateLocked == atClkState)
    {
        *pStatus = DRV_TDM_TIMING_STATE_LOCKED;
    }
    else
    {
        *pStatus = DRV_TDM_TIMING_STATE_INVALID;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1LinkClkInfoGet
* 功能描述: 获取E1链路的时钟信息.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
*           portId: STM端口编号,取值为1~8.
*           au4Id: AU4编号,取值为1~4.. 
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
* 输出参数: *pE1ClkInfo: 保存E1链路的时钟信息. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-11-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1LinkClkInfoGet(BYTE subslotId, 
                                         BYTE portId, 
                                         BYTE au4Id, 
                                         BYTE e1LinkId, 
                                         DRV_TDM_E1_CLK_QRY_INFO *pE1ClkInfo)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;  /* ARRIVE芯片的芯片编号 */
    WORD16 pwId = DRV_TDM_PW_ID_INVALID;  /* 初始化时,赋值为无效值 */
    WORD32 e1BindingPwNum = 0;  /* E1链路绑定的PW的数量 */
    WORD32 clkStatus = 0;
    DRV_TDM_PW_CFG_INFO *pPwInfo = NULL;

    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);  
    DRV_TDM_CHECK_AUG1_ID(au4Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pE1ClkInfo);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* 对于SATOP PW,e1LinkId与pwId是一一对应的;对于CESOPSN PW,获取e1LinkId绑定的第一条PW. */
    rv = drv_tdm_e1BindingPwIdGet(subslotId, portId, au4Id, e1LinkId, &pwId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, drv_tdm_e1BindingPwIdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_PW_ID(pwId);

    rv = drv_tdm_pwMemGet(chipId, pwId, &pPwInfo);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, drv_tdm_pwMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwInfo);

    rv = drv_tdm_e1BindingPwNumGet(chipId, portId, au4Id, e1LinkId, &e1BindingPwNum);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, drv_tdm_e1BindingPwNumGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_masterE1TimingStatusGet(subslotId, portId, au4Id, e1LinkId, &clkStatus);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, drv_tdm_masterE1TimingStatusGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    memset(pE1ClkInfo, 0, sizeof(DRV_TDM_E1_CLK_QRY_INFO));
    pE1ClkInfo->clkDomainNo = pPwInfo->acInfo.clkDomainNo;
    pE1ClkInfo->masterChannelNO = pPwInfo->acInfo.channelNo;
    pE1ClkInfo->e1BindingPwNum = e1BindingPwNum;
    pE1ClkInfo->e1ClkMode = pPwInfo->acInfo.e1TimingMode;
    pE1ClkInfo->e1ClkState = clkStatus;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1ClkSwitchAlarmGet
* 功能描述: 获取E1链路的时钟切换告警.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
*           portId: STM端口编号,取值为1~8.
*           au4Id: AU4编号,取值为1~4. 
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
* 输出参数: *pAlarmState: 保存E1链路的时钟切换告警状态. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-11-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1ClkSwitchAlarmGet(BYTE subslotId, 
                                              BYTE portId, 
                                              BYTE au4Id, 
                                              BYTE e1LinkId, 
                                              BYTE *pAlarmState)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;  /* ARRIVE芯片的芯片编号 */
    WORD16 pwId = DRV_TDM_PW_ID_INVALID;  /* 初始化时,赋值为无效值 */
    BYTE e1ClkState = 0;  /* E1链路的时钟状态 */
    WORD32 masterE1ClkState = 0;  /* 主E1链路的时钟状态 */
    DRV_TDM_PW_CNT_INFO *pPwCnt = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);  
    DRV_TDM_CHECK_AUG1_ID(au4Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pAlarmState);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    /* 对于SATOP PW,e1LinkId与pwId是一一对应的;对于CESOPSN PW,获取e1LinkId绑定的第一条PW. */
    rv = drv_tdm_e1BindingPwIdGet(subslotId, portId, au4Id, e1LinkId, &pwId);
    if (DRV_TDM_E1_IS_NOT_BINDED == rv) /* 如果E1链路没有绑定PW,则直接返回成功. */
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "WARNING: %s line %d, subslotId %u portId %u au4Id %u e1LinkId %u, E1 link is not binded with PW.\n", __FILE__, __LINE__, subslotId, portId, au4Id, e1LinkId); 
        return DRV_TDM_OK; 
    }
    else if (DRV_TDM_OK == rv)  /* 如果返回成功,则什么都不做,接着往下执行 */
    {
        ;
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, drv_tdm_e1BindingPwIdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    /* 获取PW软件表中的E1链路的时钟状态. */
    rv = drv_tdm_pwE1ClkStateGet(chipId, pwId, &e1ClkState);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, drv_tdm_pwE1ClkStateGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_pwPktCntMemGet(chipId, pwId, &pPwCnt);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, drv_tdm_pwPktCntMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCnt);

    *pAlarmState = 0;  /* 先清零再赋值 */
    if (DRV_TDM_E1_LINK_MASTER == e1ClkState)
    {
        rv = drv_tdm_masterE1TimingStatusGet(subslotId, portId, au4Id, e1LinkId, &masterE1ClkState);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, drv_tdm_masterE1TimingStatusGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        if ((DRV_TDM_TIMING_STATE_NOT_APPLICABLE != masterE1ClkState) 
            && (pPwCnt->rxPkts > 0))
        {
            *pAlarmState = DRV_TDM_CLK_SWITCH_NONE_ALARM;
        }
        else
        {
            *pAlarmState = DRV_TDM_CLK_SWITCH_ALARM;
        }
    }
    else if (DRV_TDM_E1_LINK_SLAVE == e1ClkState)
    {
        if ((pPwCnt->rxPkts > 0) 
            && (0 == pPwCnt->rxDiscardedPkts)
            && (0 == pPwCnt->rxMalformedPkts) 
            && (0 == pPwCnt->rxLostPkts) 
            && (0 == pPwCnt->rxOutofSeqDropPkts)  
            && (0 == pPwCnt->rxJitBufOverrunPkts) 
            && (0 == pPwCnt->rxJitBufUnderrunPkts))
        {
            *pAlarmState = DRV_TDM_CLK_SWITCH_NONE_ALARM;
        }
        else
        {
            *pAlarmState = DRV_TDM_CLK_SWITCH_ALARM;
        }
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, invalid E1 link clock state, e1LinkClkState=0x%x.\n", __FILE__, __LINE__, e1ClkState);
        return DRV_TDM_INVALID_ARGUMENT;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1ClkSwitchAlmFlagSet
* 功能描述: 设置监测E1链路的时钟切换告警的标记.
* 访问的表: 软件表g_drv_tdm_e1ClkSwtichAlmFlag.
* 修改的表: 软件表g_drv_tdm_e1ClkSwtichAlmFlag.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           flag: 使能标记,DRV_TDM_ENABLE使能,DRV_TDM_DISABLE非使能.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-11-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1ClkSwitchAlmFlagSet(BYTE chipId, WORD32 flag)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    g_drv_tdm_e1ClkSwtichAlmFlag[chipId] = flag;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1ClkSwitchAlmFlagGet
* 功能描述: 获取监测E1链路的时钟切换告警的标记.
* 访问的表: 软件表g_drv_tdm_e1ClkSwtichAlmFlag.
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.           
* 输出参数: pFlag: 保存使能标记,DRV_TDM_ENABLE使能,DRV_TDM_DISABLE非使能.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-11-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1ClkSwitchAlmFlagGet(BYTE chipId, WORD32 *pFlag)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pFlag);
    
    *pFlag = g_drv_tdm_e1ClkSwtichAlmFlag[chipId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1ClkAlmThreadIdGet
* 功能描述: 获取E1链路的时钟切换告警线程的threadId.
* 访问的表: 软件表g_drv_tdm_e1ClkSwitchAlmThreadId.
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3. 
* 输出参数: *pThreadId: 用来保存threadId.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-11-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1ClkAlmThreadIdGet(BYTE chipId, pthread_t *pThreadId)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pThreadId);
    
    *pThreadId = g_drv_tdm_e1ClkSwitchAlmThreadId[chipId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1ClkSwitchAlmTidGet
* 功能描述: 获取E1链路的时钟切换告警线程的TID.
* 访问的表: 软件表g_drv_tdm_e1ClkSwitchAlmTid.
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3. 
* 输出参数: *pTid: 用来保存TID.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-11-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1ClkSwitchAlmTidGet(BYTE chipId, int *pTid)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pTid);
    
    *pTid = g_drv_tdm_e1ClkSwitchAlmTid[chipId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1ClkSwitchAlmMonitor
* 功能描述: 监测E1链路的时钟切换告警.
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pSubslotId: 保存STM单板的子槽位号 
* 输出参数: 无
* 返 回 值: 
* 其它说明: E1链路的时钟切换告警线程的入口函数
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-11-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1ClkSwitchAlmMonitor(VOID *pSubslotId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE subslotId = 0;
    WORD32 dwBoardStatus = 0;  /* CP3BAN单板的状态 */
    WORD32 initFlag = 0;  /* STM TDM PWE3模块的初始化标记 */
    BYTE chipId = 0;
    BYTE portId = 0;    /* STM端口编号 */
    BYTE maxPortId = 0; /* 最大的STM端口编号 */
    WORD32 dwStmIntfMode = 0;  /* STM端口模式 */
    BYTE aug1Id = 0;   /* AUG1编号 */
    BYTE maxAug1Id = 0;  /* 最大的AUG1编号 */
    BYTE e1LinkId = 0;   /* VC4中的E1链路的编号,取值为1~63. */
    BYTE timingMode = 0;
    WORD32 boardType = 0;
    BYTE alarmState = 0;
    BYTE tmpAlmState = 0;
    WORD32 i = 0;
    WORD32 *pdwSubslotId = NULL; 
    WORD32 e1ClkAlmTimes = 0;
    
    pdwSubslotId = (WORD32 *)pSubslotId;
    if (NULL == pdwSubslotId)
    { 
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, pdwSubslotId is NULL pointer.\n", __FILE__, __LINE__); 
        return DRV_TDM_NULL_POINTER; 
    }
    subslotId = (BYTE)(*pdwSubslotId);
    free(pdwSubslotId);
    pdwSubslotId = NULL;  /* 释放内存空间,以防成为野指针 */
    BSP_Print(BSP_DEBUG_ALL, "%s line %d, drv_tdm_e1ClkSwitchAlmMonitor(), subslotId=%u.\n", __FILE__, __LINE__, subslotId);
    if ((subslotId < DRV_TDM_SUBCARD_ID_START) || (subslotId > DRV_TDM_SUBCARD_ID_END))
    { 
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid subslotId, subslotId=%u.\n", __FILE__, __LINE__, subslotId); 
        return DRV_TDM_INVALID_SUBSLOT_ID; 
    }
    chipId = subslotId - (BYTE)1;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    g_drv_tdm_e1ClkSwitchAlmTid[chipId] = __gettid();  /* save TID */
    
    rv = BSP_cp3ban_boardTypeGet((WORD32)subslotId, &boardType);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_boardTypeGet() rv=0x%x\n", __FILE__, __LINE__, subslotId, rv);
        return rv;
    }
    if (BSP_CP3BAN_8PORT_BOARD == boardType)  /* 8端口的STM单板 */
    {
        maxPortId = DRV_TDM_STM_8PORT_ID_END;
    }
    else if (BSP_CP3BAN_4PORT_BOARD == boardType)  /* 4端口的STM单板 */
    {
        maxPortId = DRV_TDM_STM_4PORT_ID_END;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, invalid board type, boardType=%u.\n", __FILE__, __LINE__, subslotId, boardType);
        return DRV_TDM_INVALID_BOARD_TYPE;
    }

    while (DRV_TDM_TRUE)
    {
        rv = BSP_cp3banBoardStatusGet((WORD32)subslotId, &dwBoardStatus);  /* 获取单板的状态 */
        if (BSP_E_CP3BAN_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, subslotId %u, BSP_cp3banBoardStatusGet() rv=0x%x.\n", __FILE__, __LINE__, subslotId, rv);
            return rv;
        }
        if (BSP_CP3BAN_BOARD_OFFLINE == dwBoardStatus)  /* 单板不在位,直接返回成功. */
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "WARNING: %s line %d, subslotId %u, CP3BAN board is offline.\n", __FILE__, __LINE__, subslotId);
            return DRV_TDM_OK;
        }
        rv = drv_tdm_pwe3InitFlagGet(chipId, &initFlag);  /* 获取STM TDM PWE3模块的初始化标记 */
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3InitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
            return rv;
        }
        if (DRV_TDM_STM_BOARD_UNINSTALL != initFlag) /* STM TDM PWE3模块已经初始化 */
        {
            if (DRV_TDM_DISABLE != g_drv_tdm_e1ClkSwtichAlmFlag[chipId])
            {
                for (portId = DRV_TDM_STM_PORT_ID_START; portId <= maxPortId; portId++)
                {
                    rv = drv_tdm_stmIntfModeGet(chipId, portId, &dwStmIntfMode);
                    if (DRV_TDM_OK != rv)
                    {
                        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, drv_tdm_stmIntfModeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                        continue;
                    }
                    if (DRV_TDM_STM1_INTF == dwStmIntfMode)  /* STM-1 interface mode */
                    {
                        maxAug1Id = DRV_TDM_MIN_AUG1_ID;
                    }
                    else if (DRV_TDM_STM4_INTF == dwStmIntfMode) /* STM-4 interface mode */
                    {
                        maxAug1Id = DRV_TDM_MAX_AUG1_ID;
                    }
                    else
                    {
                        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "WARNING: %s line %d, chipId %u portId %u, invalid stmIntfMode, stmIntfMode=%u.\n", __FILE__, __LINE__, chipId, portId, dwStmIntfMode);
                        continue;
                    }
                    for (aug1Id = DRV_TDM_MIN_AUG1_ID; aug1Id <= maxAug1Id; aug1Id++)
                    {
                        for (e1LinkId = DRV_TDM_E1_LINK_ID_START; e1LinkId <= DRV_TDM_E1_LINK_ID_END; e1LinkId++)
                        {
                            e1ClkAlmTimes = 0;
                            rv = drv_tdm_e1TimingModeGet(chipId, portId, aug1Id, e1LinkId, &timingMode);
                            if (DRV_TDM_OK != rv)
                            {
                                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, drv_tdm_e1TimingModeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                                continue;
                            }
                            if ((DRV_TDM_TIMING_MODE_ACR == timingMode) 
                                || (DRV_TDM_TIMING_MODE_DCR == timingMode))
                            {
                                /* 连续查询10次告警,当有8次产生告警时,则需要进行时钟切换 */
                                for (i = 0; i < DRV_TDM_E1_CLK_SWITCH_ALM_QRY_TIMES; i++)
                                {
                                    rv = drv_tdm_e1ClkSwitchAlarmGet(subslotId, 
                                                                     portId, 
                                                                     aug1Id, 
                                                                     e1LinkId, 
                                                                     &alarmState);
                                    if (DRV_TDM_OK != rv)
                                    {
                                        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, drv_tdm_e1ClkSwitchAlarmGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                                        continue;
                                    }
                                    if (DRV_TDM_CLK_SWITCH_NONE_ALARM != alarmState)
                                    {
                                        e1ClkAlmTimes++;
                                    }
                                    BSP_DelayMs(10000);  /* delay 10s. */
                                }
                                if (e1ClkAlmTimes >= DRV_TDM_E1_CLK_SWITCH_THRESHOLD)
                                {
                                    tmpAlmState = DRV_TDM_CLK_SWITCH_ALARM;
                                }
                                else
                                {
                                    tmpAlmState = DRV_TDM_CLK_SWITCH_NONE_ALARM;
                                }
                                rv = drv_tdm_e1ClkSwitchAlmSave(chipId, 
                                                                portId, 
                                                                aug1Id, 
                                                                e1LinkId, 
                                                                tmpAlmState);
                                if (DRV_TDM_OK != rv)
                                {
                                    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_QRY, "ERROR: %s line %d, drv_tdm_e1ClkSwitchAlmSave() rv=0x%x.\n", __FILE__, __LINE__, rv);
                                    continue;
                                }
                            }
                        }
                    }
                }
            }
        }
        BSP_DelayMs(1000);  /* delay 1000ms. */
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1ClkAlmThreadCreate
* 功能描述: 创建监测E1链路的时钟切换告警的线程.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4. 
* 输出参数: 无
* 返 回 值: 
* 其它说明: 该线程用来监测E1链路的时钟切换告警.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-11-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1ClkAlmThreadCreate(BYTE subslotId)
{
    int rv = DRV_TDM_OK;
    BYTE chipId = 0;
    WORD32 *pdwSubslotId = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);  
    chipId = subslotId - (BYTE)1;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    pdwSubslotId = (WORD32 *)(malloc(sizeof(WORD32)));
    if (NULL == pdwSubslotId)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, allocate memory for pdwSubslotId failed.\n", __FILE__, __LINE__, subslotId);
        return DRV_TDM_MEM_ALLOC_FAIL;
    }
    memset(pdwSubslotId, 0, sizeof(WORD32));  /* 先清零再赋值 */
    *pdwSubslotId = (WORD32)subslotId;
    
    init_thread(&(g_drv_tdm_e1ClkSwitchAlmThread[chipId]), 0, 0, 20, drv_tdm_e1ClkSwitchAlmMonitor, (VOID *)pdwSubslotId);
    rv = start_mod_thread(&(g_drv_tdm_e1ClkSwitchAlmThread[chipId]));
    if (0 != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, start_mod_thread() rv=%d.\n", __FILE__, __LINE__, subslotId, rv);
        return DRV_TDM_CREATE_THREAD_FAIL;    
    }
    g_drv_tdm_e1ClkSwitchAlmThreadId[chipId] = g_drv_tdm_e1ClkSwitchAlmThread[chipId].pthrid;
    
    BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, subslotId %u, Create thread e1ClkSwitchAlarmThread successfully, threadId=0x%x.\n", __FILE__, __LINE__, subslotId, g_drv_tdm_e1ClkSwitchAlmThread[chipId].pthrid);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1ClkAlmThreadDelete
* 功能描述: 删除监测E1链路的时钟切换告警的线程.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号 
* 输出参数: 无
* 返 回 值: 
* 其它说明: 该线程用来监测E1链路的时钟切换告警.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-11-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1ClkAlmThreadDelete(BYTE subslotId)
{
    BYTE chipId = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);  
    chipId = subslotId - (BYTE)1;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    stop_mod_thread(&(g_drv_tdm_e1ClkSwitchAlmThread[chipId]));
    g_drv_tdm_e1ClkSwitchAlmThreadId[chipId] = g_drv_tdm_e1ClkSwitchAlmThread[chipId].pthrid;
    g_drv_tdm_e1ClkSwitchAlmTid[chipId] = 0;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1TimingRefUpdate
* 功能描述: 更新E1链路的时钟恢复.
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pClkSwitch: E1的时钟恢复信息.
* 输出参数:  
* 返 回 值: 
* 其它说明: 该函数用于ACR/DCR时钟模式的时钟切换.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-29   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1TimingRefUpdate(const DRV_TDM_CLOCK_SWITCH *pClkSwitch)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;         /* ARRIVE芯片的芯片编号 */
    BYTE e1LinkId = 0;
    BYTE drvOldMasterE1LinkId = 0;  /* 驱动软件表中的原老的主E1链路 */
    WORD32 clkDomainId = DRV_TDM_MAX_CLK_DOMAIN_NUM;  /* 初始化时,赋值为无效值 */
    WORD32 i = 0;
    DRV_TDM_E1_LINK drvOldE1Link;  /* 驱动软件表中保存的老的主E链路 */
    DRV_TDM_CLOCK_DOMAIN tmpClkDomain;  /* 临时变量 */
    DRV_TDM_CLOCK_DOMAIN *pClkDomain = NULL;
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pClkSwitch);
    DRV_TDM_CHECK_STM_SUBSLOT_ID(pClkSwitch->subslotId);
    chipId = pClkSwitch->subslotId - (BYTE)1;
    memset(&drvOldE1Link, 0 ,sizeof(DRV_TDM_E1_LINK));
    memset(&tmpClkDomain, 0, sizeof(DRV_TDM_CLOCK_DOMAIN));
    
    rv = drv_tdm_clkDomainIdGet(chipId, pClkSwitch->clkDomainNo, &clkDomainId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u clkDomainNo %u, drv_tdm_clkDomainIdGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pClkSwitch->clkDomainNo, rv);
        return rv;
    }
    DRV_TDM_CHECK_CLOCK_DOMAIN_ID(clkDomainId);

    rv = drv_tdm_clkDomainMemGet(chipId, clkDomainId, &pClkDomain);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u clkDomainNo %u, drv_tdm_clkDomainMemGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pClkSwitch->clkDomainNo, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pClkDomain);
    memcpy(&tmpClkDomain, pClkDomain, sizeof(DRV_TDM_CLOCK_DOMAIN));

    /* 获取驱动软件表中的原老的主E1链路 */
    rv = drv_tdm_masterE1LinkGet(chipId, clkDomainId, &drvOldE1Link);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u clkDomainNo %u, drv_tdm_masterE1LinkGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pClkSwitch->clkDomainNo, rv);
        return rv;
    }
    /* 如果产品管理带下来的老主E1链路与驱动保存的原老的主E1不一致的话,则直接返回失败 */
    if ((pClkSwitch->oldMasterE1Link.portId != drvOldE1Link.portId) 
        || (pClkSwitch->oldMasterE1Link.au4Id != drvOldE1Link.au4Id) 
        || (pClkSwitch->oldMasterE1Link.tug3Id != drvOldE1Link.tug3Id) 
        || (pClkSwitch->oldMasterE1Link.tug2Id != drvOldE1Link.tug2Id) 
        || (pClkSwitch->oldMasterE1Link.tu12Id != drvOldE1Link.tu12Id))
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u clkDomainNo %u portId %u tug3Id %u tug2Id %u tu12Id %u, invalid old master E1 link.\n", __FILE__, __LINE__, chipId, pClkSwitch->clkDomainNo, pClkSwitch->oldMasterE1Link.portId, pClkSwitch->oldMasterE1Link.tug3Id, pClkSwitch->oldMasterE1Link.tug2Id, pClkSwitch->oldMasterE1Link.tu12Id);
        return DRV_TDM_INVALID_MASTER_E1_LINK;
    }
    rv = drv_tdm_e1LinkIdGet(drvOldE1Link.tug3Id, 
                             drvOldE1Link.tug2Id, 
                             drvOldE1Link.tu12Id, 
                             &drvOldMasterE1LinkId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u clkDomainNo %u, drv_tdm_e1LinkIdGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pClkSwitch->clkDomainNo, rv);
        return rv;
    }
    /* 清除驱动时钟域软件表中的原老的主E1链路的时钟信息 */
    rv = drv_tdm_e1LinkClockClear(chipId, 
                                  drvOldE1Link.portId, 
                                  drvOldE1Link.au4Id,
                                  drvOldMasterE1LinkId, 
                                  pClkSwitch->clkDomainNo);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u clkDomainNo %u, drv_tdm_e1LinkClockClear() rv=0x%x.\n", __FILE__, __LINE__, chipId, pClkSwitch->clkDomainNo, rv);
        return rv;
    }
    rv = drv_tdm_clkDomainClear(chipId, pClkSwitch->clkDomainNo); /* 先清除驱动的软件表 */
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u clkDomainNo %u, drv_tdm_clkDomainClear() rv=0x%x.\n", __FILE__, __LINE__, chipId, pClkSwitch->clkDomainNo, rv);
        return rv;
    }

    /* 设置时钟域的新的主E1链路 */
    e1LinkId = 0;
    rv = drv_tdm_e1LinkIdGet(pClkSwitch->newMasterE1Link.tug3Id, 
                             pClkSwitch->newMasterE1Link.tug2Id, 
                             pClkSwitch->newMasterE1Link.tu12Id, 
                             &e1LinkId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u clkDomainNo %u, drv_tdm_e1LinkIdGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pClkSwitch->clkDomainNo, rv);
        return rv;
    }
    rv = drv_tdm_masterE1LinkSet(chipId, 
                                 clkDomainId, 
                                 pClkSwitch->newMasterE1Link.portId, 
                                 pClkSwitch->newMasterE1Link.au4Id, 
                                 e1LinkId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u clkDomainNo %u, drv_tdm_masterE1LinkSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pClkSwitch->clkDomainNo, rv);
        return rv;
    }
    /* 更新PW软件表中的E1链路的时钟状态 */
    rv = drv_tdm_pwE1ClkStateSet(chipId, 
                                 pClkSwitch->newMasterE1Link.portId, 
                                 pClkSwitch->newMasterE1Link.au4Id, 
                                 e1LinkId, 
                                 DRV_TDM_E1_LINK_MASTER);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, drv_tdm_pwE1ClkStateSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pClkSwitch->newMasterE1Link.portId, e1LinkId, rv);
        return rv;
    }
    
    /* 设置时钟域中的原老的主E1链路从新的主E1链路中恢复时钟 */
    rv = drv_tdm_slaveE1LinkSet(chipId, 
                                clkDomainId, 
                                drvOldE1Link.portId, 
                                drvOldE1Link.au4Id, 
                                drvOldMasterE1LinkId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u clkDomainNo %u portId %u au4Id %u e1LinkId %u, drv_tdm_slaveE1LinkSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pClkSwitch->clkDomainNo, drvOldE1Link.portId, drvOldE1Link.au4Id, drvOldMasterE1LinkId, rv);
        return rv;
    }
    /* 更新PW软件表中的E1链路的时钟状态 */
    rv = drv_tdm_pwE1ClkStateSet(chipId, 
                                 drvOldE1Link.portId, 
                                 drvOldE1Link.au4Id, 
                                 drvOldMasterE1LinkId, 
                                 DRV_TDM_E1_LINK_SLAVE);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, drv_tdm_pwE1ClkStateSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, drvOldE1Link.portId, drvOldMasterE1LinkId, rv);
        return rv;
    }
    
    /* 设置时钟域中的原从E1链路从新的主E1链路中恢复时钟 */
    for (i = DRV_TDM_LINK_ID_START; i <= DRV_TDM_LINK_ID_END; i++)
    {
        if ((DRV_TDM_E1_LINK_SLAVE == tmpClkDomain.e1Link[i].e1LinkClkState) 
            && (DRV_TDM_E1_LINK_USED == tmpClkDomain.e1Link[i].linkStatus))
        {
            if ((tmpClkDomain.e1Link[i].portId != pClkSwitch->newMasterE1Link.portId) 
                || (tmpClkDomain.e1Link[i].au4Id != pClkSwitch->newMasterE1Link.au4Id) 
                || (tmpClkDomain.e1Link[i].tug3Id != pClkSwitch->newMasterE1Link.tug3Id) 
                || (tmpClkDomain.e1Link[i].tug2Id != pClkSwitch->newMasterE1Link.tug2Id) 
                || (tmpClkDomain.e1Link[i].tu12Id != pClkSwitch->newMasterE1Link.tu12Id))
            {
                e1LinkId = 0;
                rv = drv_tdm_e1LinkIdGet(tmpClkDomain.e1Link[i].tug3Id, 
                                         tmpClkDomain.e1Link[i].tug2Id, 
                                         tmpClkDomain.e1Link[i].tu12Id, 
                                         &e1LinkId);
                if (DRV_TDM_OK != rv)
                {
                    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u clkDomainNo %u, drv_tdm_e1LinkIdGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pClkSwitch->clkDomainNo, rv);
                    return rv;
                }
                rv = drv_tdm_slaveE1LinkSet(chipId, 
                                            clkDomainId, 
                                            tmpClkDomain.e1Link[i].portId, 
                                            tmpClkDomain.e1Link[i].au4Id, 
                                            e1LinkId);
                if (DRV_TDM_OK != rv)
                {
                    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u clkDomainNo %u portId %u au4Id %u e1LinkId %u, drv_tdm_slaveE1LinkSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pClkSwitch->clkDomainNo, tmpClkDomain.e1Link[i].portId, tmpClkDomain.e1Link[i].au4Id, e1LinkId, rv);
                    return rv;
                }
                /* 更新PW软件表中的E1链路的时钟状态 */
                rv = drv_tdm_pwE1ClkStateSet(chipId, 
                                             tmpClkDomain.e1Link[i].portId, 
                                             tmpClkDomain.e1Link[i].au4Id, 
                                             e1LinkId, 
                                             DRV_TDM_E1_LINK_SLAVE);
                if (DRV_TDM_OK != rv)
                {
                    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_E1_TIMING_CFG, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, drv_tdm_pwE1ClkStateSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpClkDomain.e1Link[i].portId, e1LinkId, rv);
                    return rv;
                }
            }
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_ethIntfLbMdSet
* 功能描述: 设置ARRIVE芯片的Ethernet接口的环回方式
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           ethIntfIdx: ARRIVE芯片的Ethernet接口索引,取值为0~1.
*           lbMd: refer enumeration DRV_TDM_ETH_INTF_LOOPBACK_MODE.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_ethIntfLbMdSet(BYTE chipId, BYTE ethIntfIdx, WORD32 lbMd)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE atDevLpMd = 0;      /* ARRIVE芯片的Ethernet interface的环回方式 */
    AtDevice pDevice = NULL;     /* ARRIVE芯片的device指针 */
    AtModuleEth pEthModule = NULL;  /* Ethernet module */
    AtEthPort pEthPort = NULL;      /* Ethernet port */

    DRV_TDM_CHECK_CHIP_ID(chipId);  
    DRV_TDM_CHECK_ETH_INTF_INDEX(ethIntfIdx); /* 检查ARRIVE芯片的Ethernet接口的index范围 */

    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_INTF_CFG_MSG, "ERROR: %s line %d, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);

    /* Get Ethernet Module */
    pEthModule = (AtModuleEth)AtDeviceModuleGet(pDevice, cAtModuleEth);
    DRV_TDM_CHECK_POINTER_IS_NULL(pEthModule);
    
    /* Get Ethernet port */
    pEthPort = AtModuleEthPortGet(pEthModule, ethIntfIdx);
    DRV_TDM_CHECK_POINTER_IS_NULL(pEthPort);

    switch (lbMd)
    {
        case DRV_TDM_ETH_INTF_RELEASE:
            atDevLpMd = cAtLoopbackModeRelease;
            break;
        case DRV_TDM_ETH_INTF_INTERNAL:
            atDevLpMd = cAtLoopbackModeLocal;
            break;
        case DRV_TDM_ETH_INTF_EXTERNAL:
            atDevLpMd = cAtLoopbackModeRemote;
            break;
        default:
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_INTF_CFG_MSG, "ERROR: %s line %d, invalid lbMd=%u.\n", __FILE__, __LINE__, lbMd);
            return DRV_TDM_INVALID_ARGUMENT;  /* 直接返回 */
    }
    /* Set loopback for Ethernet interface */
    rv = AtChannelLoopbackSet((AtChannel)pEthPort, atDevLpMd);
    if (cAtOk != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_INTF_CFG_MSG, "ERROR: %s line %d, AtChannelLoopbackSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_sgmiiSerdesLbMdSet
* 功能描述: 设置ARRIVE芯片的Ethernet接口的Serdes环回方式
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           ethIntfIdx: ARRIVE芯片的Ethernet接口索引,取值为0~1.
*           lbMd: refer enumeration DRV_TDM_SGMII_SERDES_LB_MD.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-11-07   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_sgmiiSerdesLbMdSet(BYTE chipId, BYTE ethIntfIdx, WORD32 lbMd)
{
    WORD32 rv = DRV_TDM_OK;
    AtDevice pDevice = NULL;     /* ARRIVE芯片的device指针 */
    AtModuleEth pEthModule = NULL;  /* Ethernet module */
    AtEthPort pEthPort = NULL;      /* Ethernet port */
    AtSerdesController pSerdesCtrl = NULL;
    eAtLoopbackMode atLbMd = 0;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_ETH_INTF_INDEX(ethIntfIdx);
    
    switch (lbMd)
    {
        case DRV_TDM_SGMII_SERDES_RELEASE_LB:
            atLbMd = cAtLoopbackModeRelease;
            break;
        case DRV_TDM_SGMII_SERDES_INTERNAL_LB:
            atLbMd = cAtLoopbackModeLocal;
            break;
        case DRV_TDM_SGMII_SERDES_EXTERNAL_LB:
            atLbMd = cAtLoopbackModeRemote;
            break;
        default:
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_INTF_CFG_MSG, "ERROR: %s line %d, invalid lbMd=%u.\n", __FILE__, __LINE__, lbMd);
            return DRV_TDM_INVALID_ARGUMENT;  /* 直接返回 */
    }
    
    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_INTF_CFG_MSG, "ERROR: %s line %d, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    
    /* Get Ethernet Module */
    pEthModule = (AtModuleEth)AtDeviceModuleGet(pDevice, cAtModuleEth);
    DRV_TDM_CHECK_POINTER_IS_NULL(pEthModule);
    
    /* Get Ethernet port */
    pEthPort = AtModuleEthPortGet(pEthModule, ethIntfIdx);
    DRV_TDM_CHECK_POINTER_IS_NULL(pEthPort);
    
    pSerdesCtrl = AtEthPortSerdesController(pEthPort);  /* Get SERDES controller */
    DRV_TDM_CHECK_POINTER_IS_NULL(pSerdesCtrl);
    
    rv = AtSerdesControllerLoopbackSet(pSerdesCtrl, atLbMd);
    if (cAtOk != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_INTF_CFG_MSG, "ERROR: %s line %d, AtSerdesControllerLoopbackSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_sgmiiSerdesGet
* 功能描述: 获取Ethernet SGMII接口的SERDES配置信息.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: CP3BAN单板的子槽位号,取值为1~4.
*           ethIntfIndex: Ethernet SGMII接口的索引号,取值为0~1.
*           phyParameterId: 参数ID,具体参见DRV_TDM_SGMII_SERDES_PARAM定义.
* 输出参数: *pValue: 保存SERDES的值
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_sgmiiSerdesGet(BYTE subslotId, 
                                       BYTE ethIntfIndex, 
                                       WORD32 phyParameterId, 
                                       WORD32 *pValue)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 serdesValue = 0;
    BYTE chipId = 0;
    AtDevice pDevice = NULL;     /* ARRIVE芯片的device指针 */
    AtModuleEth pEthModule = NULL;  /* Ethernet module */
    AtEthPort pEthPort = NULL;      /* Ethernet port */
    AtSerdesController pSerdesCtrl = NULL;
    eAtSerdesParam param = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    DRV_TDM_CHECK_ETH_INTF_INDEX(ethIntfIndex);
    DRV_TDM_CHECK_POINTER_IS_NULL(pValue);
    chipId = subslotId - (BYTE)1;
    
    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_INTF_CFG_MSG, "ERROR: %s line %d, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    
    /* Get Ethernet Module */
    pEthModule = (AtModuleEth)AtDeviceModuleGet(pDevice, cAtModuleEth);
    DRV_TDM_CHECK_POINTER_IS_NULL(pEthModule);
    
    /* Get Ethernet port */
    pEthPort = AtModuleEthPortGet(pEthModule, ethIntfIndex);
    DRV_TDM_CHECK_POINTER_IS_NULL(pEthPort);

    switch (phyParameterId)
    {
        case DRV_TDM_SGMII_SERDES_VOD:
            param = cAtSerdesParamVod;
            break;
        case DRV_TDM_SGMII_SERDES_PRE_TAP:
            param = cAtSerdesParamPreEmphasisPreTap;
            break;
        case DRV_TDM_SGMII_SERDES_FIRST_TAP:
            param = cAtSerdesParamPreEmphasisFirstPostTap;
            break;
        case DRV_TDM_SGMII_SERDES_SECOND_TAP:
            param = cAtSerdesParamPreEmphasisSecondPostTap;
            break;
        default:
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_INTF_CFG_MSG, "ERROR: %s line %d, invalid phyParameterId = %u.\n", __FILE__, __LINE__, phyParameterId);
            return DRV_TDM_INVALID_ARGUMENT;  /* 直接返回 */
    }
    
    pSerdesCtrl = AtEthPortSerdesController(pEthPort);  /* Get SERDES controller */
    DRV_TDM_CHECK_POINTER_IS_NULL(pSerdesCtrl);
    /* Get configured value of physical parameter */
    serdesValue = AtSerdesControllerPhysicalParamGet(pSerdesCtrl, param);
    
    *pValue = 0;  /* 先清零再赋值 */
    *pValue = serdesValue;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_sgmiiSerdesSet
* 功能描述: 设置Ethernet SGMII接口的SERDES配置信息.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: CP3BAN单板的子槽位号,取值为1~4.
*           ethIntfIndex: Ethernet SGMII接口的索引号,取值为0~1.
*           phyParameterId: 参数ID,具体参见DRV_TDM_SGMII_SERDES_PARAM定义.
*           value: 需要设置的值.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_sgmiiSerdesSet(BYTE subslotId, 
                                       BYTE ethIntfIndex, 
                                       WORD32 phyParameterId, 
                                       WORD32 value)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;
    AtDevice pDevice = NULL;     /* ARRIVE芯片的device指针 */
    AtModuleEth pEthModule = NULL;  /* Ethernet module */
    AtEthPort pEthPort = NULL;      /* Ethernet port */
    AtSerdesController pSerdesCtrl = NULL;
    eAtSerdesParam param = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    DRV_TDM_CHECK_ETH_INTF_INDEX(ethIntfIndex);
    chipId = subslotId - (BYTE)1;
    
    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    
    /* Get Ethernet Module */
    pEthModule = (AtModuleEth)AtDeviceModuleGet(pDevice, cAtModuleEth);
    DRV_TDM_CHECK_POINTER_IS_NULL(pEthModule);
    
    /* Get Ethernet port */
    pEthPort = AtModuleEthPortGet(pEthModule, ethIntfIndex);
    DRV_TDM_CHECK_POINTER_IS_NULL(pEthPort);

    switch (phyParameterId)
    {
        case DRV_TDM_SGMII_SERDES_VOD:
            param = cAtSerdesParamVod;
            break;
        case DRV_TDM_SGMII_SERDES_PRE_TAP:
            param = cAtSerdesParamPreEmphasisPreTap;
            break;
        case DRV_TDM_SGMII_SERDES_FIRST_TAP:
            param = cAtSerdesParamPreEmphasisFirstPostTap;
            break;
        case DRV_TDM_SGMII_SERDES_SECOND_TAP:
            param = cAtSerdesParamPreEmphasisSecondPostTap;
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid phyParameterId = %u.\n", __FILE__, __LINE__, phyParameterId);
            return DRV_TDM_INVALID_ARGUMENT;  /* 直接返回 */
    }

    pSerdesCtrl = AtEthPortSerdesController(pEthPort);  /* Get SERDES controller */
    DRV_TDM_CHECK_POINTER_IS_NULL(pSerdesCtrl);
    rv = AtSerdesControllerPhysicalParamSet(pSerdesCtrl, param, value); /* Set physical parameter */
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSerdesControllerPhysicalParamSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_ERROR;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stmIntfLbMdSave
* 功能描述: 保存STM接口的环回方式.
* 访问的表: 软件表g_drv_tdm_sdh_intf_info
* 修改的表: 软件表g_drv_tdm_sdh_intf_info
* 输入参数:  
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stmIntfLbMdSave(BYTE chipId, BYTE portId, WORD32 loopbackMode)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE stmIntfIndex = 0;   /* STM接口的索引号,取值为0~7 */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);  
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_intf_info[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    stmIntfIndex = portId - (BYTE)1;
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    g_drv_tdm_sdh_intf_info[chipId][stmIntfIndex].stmIntfLbMd = loopbackMode;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stmIntfLbMdSet
* 功能描述: 设置STM接口的环回方式
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: STM端口的编号,取值为1~8.
*           lbMd: refer enumeration DRV_TDM_SDH_LOOPBACK_MODE
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stmIntfLbMdSet(BYTE chipId, BYTE portId, WORD32 lbMd)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    DRV_TDM_SDH_INTF_INFO *pIntf = NULL;
    BYTE atDevLpMd = 0;      /* ARRIVE芯片的STM接口的环回方式 */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);  
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_sdhIntfMemGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, &pIntf);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_sdhIntfMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pIntf);
    
    /* Get SDH line. */
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    switch (lbMd)
    {
        case DRV_SDH_LOOPBACK_RELEASE:
            atDevLpMd = cAtLoopbackModeRelease;
            break;
        case DRV_SDH_LOOPBACK_INTERNAL:
            atDevLpMd = cAtLoopbackModeLocal;
            break;
        case DRV_SDH_LOOPBACK_EXTERNAL:
            atDevLpMd = cAtLoopbackModeRemote;
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid lbMd=%u.\n", __FILE__, __LINE__, lbMd);
            return DRV_TDM_INVALID_ARGUMENT;  /* 直接返回 */
    }

    if (DRV_SDH_LOOPBACK_RELEASE == lbMd)  /* 取消环回 */
    {
        /* Set loopback for STM channel */
        rv = AtChannelLoopbackSet((AtChannel)pLine, cAtLoopbackModeRelease);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelLoopbackSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    else  /* 设置环回 */
    {
        /* 当STM端口使能时,才去设置芯片的STM接口环回 */
        if (DRV_TDM_STM_INTF_DISABLE != pIntf->stmIntfState) 
        {
            /* Set loopback for STM channel */
            rv = AtChannelLoopbackSet((AtChannel)pLine, atDevLpMd);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelLoopbackSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
        }
    }

    /* 保存STM接口的环回方式. */
    rv = drv_tdm_stmIntfLbMdSave(chipId, portId, lbMd);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_stmIntfLbMdSave() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stmIntfLbMdGet
* 功能描述: 获取STM接口的环回方式
* 访问的表: 软件表g_drv_tdm_sdh_intf_info.
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: STM端口的编号,取值为1~8.
* 输出参数: *pLbMode: 保存STM接口的环回方式.  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stmIntfLbMdGet(BYTE chipId, BYTE portId, WORD32 *pLbMode)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE stmIntfIndex = 0;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pLbMode);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_intf_info[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    stmIntfIndex = portId - (BYTE)1;
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    *pLbMode = 0;   /* 先清零再赋值 */
    *pLbMode = g_drv_tdm_sdh_intf_info[chipId][stmIntfIndex].stmIntfLbMd;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1LinkLbMdSave
* 功能描述: 保存E1链路的环回方式.
* 访问的表: 软件表g_drv_tdm_sdh_intf_info
* 修改的表: 软件表g_drv_tdm_sdh_intf_info
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
*           loopbackMode: E1链路环回方式,具体参见DRV_TDM_PDH_LOOPBACK_MODE定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1LinkLbMdSave(BYTE chipId, 
                                        BYTE portId, 
                                        BYTE aug1Id, 
                                        BYTE e1LinkId, 
                                        BYTE loopbackMode)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwStmIntfMode = 0; /* STM端口方式 */
    BYTE stmIntfIndex = 0;   /* STM端口的索引号,取值为0~7. */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_intf_info[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_stmIntfModeGet(chipId, portId, &dwStmIntfMode);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmIntfModeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_STM1_INTF == dwStmIntfMode)  /* STM-1 interface mode */
    {
        stmIntfIndex = portId - (BYTE)1;
    }
    else if (DRV_TDM_STM4_INTF == dwStmIntfMode) /* STM-4 interface mode */
    {
        rv = drv_tdm_aug1IndexGet(portId, aug1Id, &stmIntfIndex);
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_aug1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid stmIntfMode, stmIntfMode=%u.\n", __FILE__, __LINE__, dwStmIntfMode);
        return DRV_TDM_INVALID_STM_INTF_MODE;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    g_drv_tdm_sdh_intf_info[chipId][stmIntfIndex].e1LinkLbMd[e1LinkId] = loopbackMode;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1LinkLbMdGet
* 功能描述: 获取E1通道的环回方式
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
* 输出参数: *pLbMd: 保存环回方式.refer enumeration DRV_TDM_PDH_LOOPBACK_MODE 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1LinkLbMdGet(BYTE chipId, 
                                       BYTE portId, 
                                       BYTE aug1Id, 
                                       BYTE e1LinkId, 
                                       BYTE *pLbMd)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwStmIntfMode = 0; /* STM端口方式 */
    BYTE stmIntfIndex = 0;   /* STM接口的索引号,取值为0~7. */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pLbMd);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_intf_info[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_stmIntfModeGet(chipId, portId, &dwStmIntfMode);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_stmIntfModeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_STM1_INTF == dwStmIntfMode)  /* STM-1 interface mode */
    {
        stmIntfIndex = portId - (BYTE)1;
    }
    else if (DRV_TDM_STM4_INTF == dwStmIntfMode) /* STM-4 interface mode */
    {
        rv = drv_tdm_aug1IndexGet(portId, aug1Id, &stmIntfIndex);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_aug1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, invalid stmIntfMode, stmIntfMode=%u.\n", __FILE__, __LINE__, dwStmIntfMode);
        return DRV_TDM_INVALID_STM_INTF_MODE;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    *pLbMd = 0;  /* 赋值之前先清零 */
    *pLbMd = g_drv_tdm_sdh_intf_info[chipId][stmIntfIndex].e1LinkLbMd[e1LinkId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1LinkLbMdSet
* 功能描述: 设置E1通道的环回方式
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
*           lbMd: refer enumeration DRV_TDM_PDH_LOOPBACK_MODE
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1LinkLbMdSet(BYTE chipId, 
                                      BYTE portId, 
                                      BYTE aug1Id, 
                                      BYTE e1LinkId, 
                                      WORD32 lbMd)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE atDevLpMd = 0;      /* ARRIVE芯片的E1通道的环回方式 */
    AtPdhDe1 pE1 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atPdhE1PtrGet(chipId, portId, aug1Id, e1LinkId, &pE1);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, drv_tdm_atPdhE1PtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pE1);
    
    switch (lbMd)
    {
        case DRV_PDH_LOOPBACK_RELEASE:
            atDevLpMd = cAtPdhLoopbackModeRelease;
            break;
        case DRV_PDH_LOOPBACK_INTERNAL_PAYLOAD:
            atDevLpMd = cAtPdhLoopbackModeLocalPayload;
            break;
        case DRV_PDH_LOOPBACK_EXTERNAL_PAYLOAD:
            atDevLpMd = cAtPdhLoopbackModeRemotePayload;
            break;
        case DRV_PDH_LOOPBACK_INTERNAL_LINE:
            atDevLpMd = cAtPdhLoopbackModeLocalLine;
            break;
        case DRV_PDH_LOOPBACK_EXTERNAL_LINE:
            atDevLpMd = cAtPdhLoopbackModeRemoteLine;
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid lbMd=%u.\n", __FILE__, __LINE__, lbMd);
            return DRV_TDM_INVALID_ARGUMENT;  /* 直接返回 */
    }

    /* Set loopback for E1 channel */
    rv = AtChannelLoopbackSet((AtChannel)pE1, atDevLpMd);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u e1LinkId %u, AtChannelLoopbackSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, aug1Id, e1LinkId, rv);
        return DRV_TDM_SDK_API_FAIL;
    }

    /* 保存E1链路的环回方式. */
    rv = drv_tdm_e1LinkLbMdSave(chipId, portId, aug1Id, e1LinkId, (BYTE)lbMd);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u e1LinkId %u, drv_tdm_e1LinkLbMdSave() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, aug1Id, e1LinkId, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_ethIntfAlmGet
* 功能描述: 获取ARRVIE芯片的Ethernet接口的告警.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           ethIntfIndex: ARRIVE芯片的Ethernet接口索引,取值为0~1.
* 输出参数: *pAtDevEthIntfAlm: 用来保存Ethernet接口的告警. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-11   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_ethIntfAlmGet(BYTE chipId, 
                                    BYTE ethIntfIndex, 
                                    DRV_TDM_AT_DEV_ETH_INTF_ALM *pAtDevEthIntfAlm)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 atDevEthIntfAlm = 0;
    AtDevice pDevice = NULL;     /* ARRIVE芯片的device指针 */
    AtModuleEth pEthModule = NULL;  /* Ethernet module */
    AtEthPort pEthPort = NULL;      /* Ethernet port */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);  /* 检查ARRIVE芯片的chipId的范围 */
    DRV_TDM_CHECK_ETH_INTF_INDEX(ethIntfIndex); /* 检查ARRIVE芯片的Ethernet接口的index范围 */
    DRV_TDM_CHECK_POINTER_IS_NULL(pAtDevEthIntfAlm);
    
    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_INTF_CFG_MSG, "ERROR: %s line %d, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    
    /* Get Ethernet Module */
    pEthModule = (AtModuleEth)AtDeviceModuleGet(pDevice, cAtModuleEth);
    DRV_TDM_CHECK_POINTER_IS_NULL(pEthModule);
    
    /* Get Ethernet port */
    pEthPort = AtModuleEthPortGet(pEthModule, ethIntfIndex);
    DRV_TDM_CHECK_POINTER_IS_NULL(pEthPort);

    /* Get alarm of the ethernet interface. */
    atDevEthIntfAlm = AtChannelAlarmGet((AtChannel)pEthPort);
    if (cAtEthPortAlarmLinkUp == atDevEthIntfAlm)
    {
        pAtDevEthIntfAlm->ethIntfAlm = DRV_TDM_AT_ETH_INFF_LINK_UP;
    }
    else
    {
        pAtDevEthIntfAlm->ethIntfAlm = DRV_TDM_AT_ETH_INFF_LINK_DOWN;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txStmScrambleSave
* 功能描述: 保存发送方向的STM帧的扰码方式.
* 访问的表: 软件表g_drv_tdm_sdh_intf_info.
* 修改的表: 软件表g_drv_tdm_sdh_intf_info.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: STM端口的编号,取值为1~8.
*           scrambleEn: 具体参见DRV_TDM_STM_TX_SCRAMBLE_STATE定义
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-11   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txStmScrambleSave(BYTE chipId, BYTE portId, WORD32 scrambleEn)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE stmIntfIndex = 0;   /* STM接口的索引号,取值为0~7. */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_intf_info[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    stmIntfIndex = portId - (BYTE)1;
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    g_drv_tdm_sdh_intf_info[chipId][stmIntfIndex].txStmScrambleEn = scrambleEn;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txStmScrambleGet
* 功能描述: 获取发送方向的STM帧的扰码方式.
* 访问的表: 软件表g_drv_tdm_sdh_intf_info.
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: STM端口的编号,取值为1~8.
* 输出参数: *pScrambleEn: 具体参见DRV_TDM_STM_TX_SCRAMBLE_STATE定义.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-06   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txStmScrambleGet(BYTE chipId, BYTE portId, WORD32 *pScrambleEn)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE stmIntfIndex = 0;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pScrambleEn);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_intf_info[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    stmIntfIndex = portId - (BYTE)1;
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    *pScrambleEn = 0;   /* 先清零再赋值 */
    *pScrambleEn = g_drv_tdm_sdh_intf_info[chipId][stmIntfIndex].txStmScrambleEn;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txStmScrambleSet
* 功能描述: 设置发送方向的STM帧是否扰码.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
*           portId: STM端口的编号,取值为1~8.
*           scrambleEn: 具体参见DRV_TDM_STM_TX_SCRAMBLE_STATE定义
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-06   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txStmScrambleSet(BYTE subslotId, BYTE portId, WORD32 scrambleEn)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;
    eBool atDevScrambleEn = 0;
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);  
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* Get SDH line. */
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    if (DRV_TDM_STM_TX_SCRAMBLE_DISABLE == scrambleEn)
    {
        atDevScrambleEn = cAtFalse;
    }
    else if (DRV_TDM_STM_TX_SCRAMBLE_ENABLE == scrambleEn)
    {
        atDevScrambleEn = cAtTrue;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid scrambleEn=%u.\n", __FILE__, __LINE__, scrambleEn);
        return DRV_TDM_INVALID_ARGUMENT;  
    }
    
    /* Enable/disable scramble */
    rv = AtSdhLineScrambleEnable(pLine, atDevScrambleEn);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtSdhLineScrambleEnable() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return DRV_TDM_SDK_API_FAIL;
    }

    /* 保存软件表 */
    rv = drv_tdm_txStmScrambleSave(chipId, portId, scrambleEn);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_txStmScrambleSave() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stmIntfTxLaserStateSave
* 功能描述: 保存发送方向的STM接口的Laser状态.
* 访问的表: 软件表g_drv_tdm_sdh_intf_info.
* 修改的表: 软件表g_drv_tdm_sdh_intf_info.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: STM端口编号,取值为1~8.
*           state: 具体参见DRV_TDM_STM_INTF_TX_LASER_STATE定义.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-20   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stmIntfTxLaserStateSave(BYTE chipId, BYTE portId, WORD32 state)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE stmIntfIndex = 0;   /* STM接口的索引号,取值为0~7 */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_intf_info[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    stmIntfIndex = portId - (BYTE)1;
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    g_drv_tdm_sdh_intf_info[chipId][stmIntfIndex].txLaserState = state;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stmIntfTxLaserStateGet
* 功能描述: 获取发送方向的STM接口的Laser状态.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
*           portId: STM端口编号,取值为1~8.
* 输出参数: *pState: 保存激光器的状态,具体参见DRV_TDM_STM_INTF_TX_LASER_STATE定义.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-20   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stmIntfTxLaserStateGet(BYTE subslotId, BYTE portId, WORD32 *pState)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;
    BYTE stmIntfIndex = 0;   /* ARRIVE芯片的STM-1接口的索引,从0开始编号 */
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);  
    DRV_TDM_CHECK_POINTER_IS_NULL(pState);
    chipId = subslotId - (BYTE)1;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_intf_info[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    stmIntfIndex = portId - (BYTE)1;
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    *pState = 0;  /* 先清零再赋值 */
    *pState = g_drv_tdm_sdh_intf_info[chipId][stmIntfIndex].txLaserState;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stmIntfTxLaserStateSet
* 功能描述: 设置发送方向的STM接口的Laser状态.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
*           portId: STM端口编号,取值为1~8.
*           state: 具体参见DRV_TDM_STM_INTF_TX_LASER_STATE定义
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-20   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stmIntfTxLaserStateSet(BYTE subslotId, BYTE portId, WORD32 state)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;
    AtSdhLine pLine = NULL;
    AtSerdesController pSerdesController = NULL;
    eBool txLaserState = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);  
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* Get SDH line. */
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);

    pSerdesController = AtSdhLineSerdesController(pLine);
    DRV_TDM_CHECK_POINTER_IS_NULL(pSerdesController);

    if (DRV_TDM_INTF_TX_LASER_ENABLE == state)
    {
        txLaserState = cAtTrue;
    }
    else if (DRV_TDM_INTF_TX_LASER_DISABLE == state)
    {
        txLaserState = cAtFalse;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid state=%u\n.\n", __FILE__, __LINE__, state);
        return DRV_TDM_INVALID_ARGUMENT;
    }

    /* Enable/Disbale tx laser of STM interface */
    rv = AtSerdesControllerEnable(pSerdesController, txLaserState);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtSerdesControllerEnable() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return DRV_TDM_SDK_API_FAIL;
    }

    /* 保存软件表 */
    rv = drv_tdm_stmIntfTxLaserStateSave(chipId, portId, state);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_stmIntfTxLaserStateSave() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stmIntfStateSave
* 功能描述: 保存STM接口的状态.
* 访问的表: 软件表g_drv_tdm_sdh_intf_info.
* 修改的表: 软件表g_drv_tdm_sdh_intf_info.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: STM端口的编号,取值为1~8.
*           state: 具体参见DRV_TDM_STM_INTF_STATE定义.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-20   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stmIntfStateSave(BYTE chipId, BYTE portId, WORD32 state)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE stmIntfIndex = 0;   /* STM接口的索引号,取值为0~7. */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_intf_info[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    stmIntfIndex = portId - (BYTE)1;
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    g_drv_tdm_sdh_intf_info[chipId][stmIntfIndex].stmIntfState = state;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stmIntfStateGet
* 功能描述: 获取STM接口的状态.
* 访问的表: 软件表g_drv_tdm_sdh_intf_info.
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
*           portId: STM端口的编号,取值为1~8.
* 输出参数: *pState: 保存端口的状态,具体参见DRV_TDM_STM_INTF_STATE定义. 
* 返 回 值: 
* 其它说明: 该函数用来获取shutdown状态.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-20   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stmIntfStateGet(BYTE subslotId, BYTE portId, WORD32 *pState)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;
    BYTE stmIntfIndex = 0;   /* STM接口的索引号,取值为0~7. */
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);  
    DRV_TDM_CHECK_POINTER_IS_NULL(pState);
    chipId = subslotId - (BYTE)1;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_intf_info[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    stmIntfIndex = portId - (BYTE)1;
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    *pState = 0;  /* 先清零再赋值 */
    *pState = g_drv_tdm_sdh_intf_info[chipId][stmIntfIndex].stmIntfState;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stmIntfStateSet
* 功能描述: 设置STM接口的状态.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
*           portId: STM端口的编号,取值为1~8.
*           state: 具体参见DRV_TDM_STM_INTF_STATE定义
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 该函数用来进行shutdown操作.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-20   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stmIntfStateSet(BYTE subslotId, BYTE portId, WORD32 state)
{
    WORD32 rv = DRV_TDM_OK;
    DRV_TDM_SDH_INTF_INFO *pIntf = NULL;
    BYTE chipId = 0;
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);  
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_sdhIntfMemGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, &pIntf);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_sdhIntfMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pIntf);
   
    /* Get SDH line. */
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    if (DRV_TDM_STM_INTF_ENABLE == state)
    {
        /* Enable tx laser of STM interface */
        rv = drv_tdm_stmIntfTxLaserStateSet(subslotId, portId, DRV_TDM_INTF_TX_LASER_ENABLE);
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_stmIntfTxLaserStateSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
            return rv;
        }
        /* UnForce RX RS-LOS alarm */
        rv = AtChannelRxAlarmUnForce((AtChannel)pLine, cAtSdhLineAlarmLos);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelRxAlarmUnForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
            return DRV_TDM_SDK_API_FAIL;
        }

        /* 如果在端口上作了环回,需要把环回置位 */
        if (DRV_SDH_LOOPBACK_INTERNAL == pIntf->stmIntfLbMd)
        {
            /* Set loopback for STM channel */
            rv = AtChannelLoopbackSet((AtChannel)pLine, cAtLoopbackModeLocal);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelLoopbackSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
        }
        else if (DRV_SDH_LOOPBACK_EXTERNAL == pIntf->stmIntfLbMd)
        {
            /* Set loopback for STM channel */
            rv = AtChannelLoopbackSet((AtChannel)pLine, cAtLoopbackModeRemote);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelLoopbackSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
        }
        else
        {
            ;
        }

    }
    else if (DRV_TDM_STM_INTF_DISABLE == state)
    {
        /* 在端口disable时,如果端口有环回,需要将环回取消 */
        if ((DRV_SDH_LOOPBACK_INTERNAL == pIntf->stmIntfLbMd) 
            || (DRV_SDH_LOOPBACK_EXTERNAL == pIntf->stmIntfLbMd))
        {
            /* Set loopback for STM-1 channel */
            rv = AtChannelLoopbackSet((AtChannel)pLine, cAtLoopbackModeRelease);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelLoopbackSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
        }
        /* Disbale tx laser of STM interface */
        rv = drv_tdm_stmIntfTxLaserStateSet(subslotId, portId, DRV_TDM_INTF_TX_LASER_DISABLE);
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_stmIntfTxLaserStateSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
            return rv;
        }
        /* Force RX RS-LOS alarm */
        rv = AtChannelRxAlarmForce((AtChannel)pLine, cAtSdhLineAlarmLos);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelRxAlarmForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid state=%u\n.\n", __FILE__, __LINE__, state);
        return DRV_TDM_INVALID_ARGUMENT;
    }
    
    /* 保存软件表 */
    rv = drv_tdm_stmIntfStateSave(chipId, portId, state);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_stmIntfStateSave() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stmIntfTxClockModeSave
* 功能描述: 保存STM接口的TX时钟方式.
* 访问的表: 软件表g_drv_tdm_sdh_intf_info.
* 修改的表: 软件表g_drv_tdm_sdh_intf_info.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: STM端口的编号,取值为1~8.
*           txClkMode: 具体参见DRV_TDM_STM_INTF_TX_CLK_MODE定义.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-10   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stmIntfTxClockModeSave(BYTE chipId, 
                                                  BYTE portId, 
                                                  WORD32 txClkMode)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE stmIntfIndex = 0;   /* STM端口的索引号,取值为0~7. */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_intf_info[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    stmIntfIndex = portId - (BYTE)1;
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    g_drv_tdm_sdh_intf_info[chipId][stmIntfIndex].stmIntfTxClkMd = txClkMode;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stmIntfTxClockModeGet
* 功能描述: 获取STM接口的TX时钟方式.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
*           portId: STM端口的编号,取值为1~8.
* 输出参数: *pClkMode: 保存STM端口的TX时钟方式.具体参见DRV_TDM_STM_INTF_TX_CLK_MODE定义.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-10   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stmIntfTxClockModeGet(BYTE subslotId, 
                                                 BYTE portId, 
                                                 WORD32 *pClkMode)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;
    BYTE stmIntfIndex = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pClkMode);
    chipId = subslotId - (BYTE)1;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_intf_info[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    stmIntfIndex = portId - (BYTE)1;
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    *pClkMode = 0;   /* 先清零再赋值 */
    *pClkMode = g_drv_tdm_sdh_intf_info[chipId][stmIntfIndex].stmIntfTxClkMd;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stmIntfTxClockModeSet
* 功能描述: 设置STM接口的TX时钟方式.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
*           portId: STM端口的编号,取值为1~8.
*           clkMode: 具体参见DRV_TDM_STM_INTF_TX_CLK_MODE定义
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-10   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stmIntfTxClockModeSet(BYTE subslotId, BYTE portId, WORD32 clkMode)
{    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stmIntfRecoveredClkSet
* 功能描述: 设置STM接口的RX恢复时钟.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
*           portId: STM端口的编号,取值为1~8.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-10   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stmIntfRecoveredClkSet(BYTE subslotId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;
    BYTE extractorId = 0;
    AtDevice pDevice = NULL;
    AtSdhLine pLine = NULL;
    AtModuleClock pClockModule = NULL;
    AtClockExtractor pClockExtractor = NULL;

    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);  
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    if ((portId >= 1) && (portId <= 2))
    {
        extractorId = 0;
    }
    else if ((portId >= 3) && (portId <= 4))
    {
        extractorId = 1;
    }
    else if ((portId >= 5) && (portId <= 6))
    {
        extractorId = 2;
    }
    else
    {
        extractorId = 3;
    }

    /* 获取ARRIVE芯片的device指针 */
    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);

    /* Get SDH line. */
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);

    pClockModule = (AtModuleClock)AtDeviceModuleGet(pDevice, cAtModuleClock);
    DRV_TDM_CHECK_POINTER_IS_NULL(pClockModule);

    pClockExtractor = AtModuleClockExtractorGet(pClockModule, extractorId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pClockExtractor);

    /* Extract clock */
    rv = AtClockExtractorExtract(pClockExtractor, cAtTimingModeSdhLineRef, (AtChannel)pLine);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u extractorId %u, AtClockExtractorExtract() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, extractorId, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "SUCCESS: subslotId %u portId %u, set recovered clock of stm-1 port successfully.\n", subslotId, portId);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_fpgaPhyIntfStateGet
* 功能描述: 获取FPGA内部的物理接口的状态.
* 访问的表: 无
* 修改的表: 无
* 输入参数: ucSubslotId: 单板的子槽位号,取值为1~4.
* 输出参数: *pdwStatus: 保存FPGA内部的物理接口状态,具体参见DRV_TDM_FPGA_PHY_INTF_STATUS定义. 
* 返 回 值: 
* 其它说明: 包括FPGA内部的8个STM接口及两个Ethernet接口的状态.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-10   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_fpgaPhyIntfStateGet(BYTE ucSubslotId, WORD32 *pdwStatus)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    BYTE chipId = 0;
    AtDevice pDevice = NULL;
    eBool atPhyIntfState = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(ucSubslotId); 
    DRV_TDM_CHECK_POINTER_IS_NULL(pdwStatus);
    chipId = ucSubslotId - (BYTE)1;
    
    /* 获取ARRIVE芯片的device指针 */
    dwRetValue = drv_tdm_atDevicePtrGet(chipId, &pDevice);
    if (DRV_TDM_OK != dwRetValue)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, chipId %u, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwRetValue);
        return dwRetValue;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    
    *pdwStatus = 0;  /* 先清零再赋值 */
    atPhyIntfState = AtZtePtnInterfaceIsGood(pDevice);
    if (cAtTrue == atPhyIntfState)
    {
        *pdwStatus = DRV_TDM_FPGA_PHY_INTF_GOOD;
    }
    else if (cAtFalse == atPhyIntfState)
    {
        *pdwStatus = DRV_TDM_FPGA_PHY_INTF_BAD;
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, chipId %u, AtZtePtnInterfaceIsGood() rv=%u.\n", __FILE__, __LINE__, chipId, atPhyIntfState);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}



/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_tdm_init.c
* 文件标识: 
* 其它说明: 驱动STM TDM PWE3模块的初始化部分的实现函数
* 当前版本: V1.0
* 作    者: 谢伟生10112265.
* 完成日期: 2013-03-26
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/


#include "drv_tdm_init.h"
#include "drv_stm1_fpga.h"
#include "drv_tdm_sdh_alarm.h"
#include "drv_tdm_sdh_perf.h"
#include "drv_tdm_overhead.h"
#include "drv_tdm_pwe3.h"
#include "drv_tdm_intf.h"
#include "drv_pwe3_main.h"
#include "drv_stm4_init.h"
#include "attypes.h"
#include "AtCommon.h"
#include "AtClasses.h"
#include "AtDriver.h"
#include "AtDevice.h"
#include "AtDeviceInternal.h"
#include "AtOsal.h"
#include "AtHal.h"
#include "AtHalZtePtn.h"
#include "AtZtePtn.h"
#include "AtIpCore.h"
#include "AtModule.h"
#include "AtModuleSdh.h"
#include "AtChannel.h"
#include "AtSdhChannel.h"
#include "AtSdhLine.h"
#include "AtSdhPath.h"
#include "AtSdhAug.h"
#include "AtSdhVc.h"
#include "AtSdhTug.h"
#include "AtModulePdh.h"
#include "AtPdhChannel.h"
#include "AtPdhDe1.h"
#include "AtModuleEth.h"
#include "AtEthPort.h"
#include "AtSerdesController.h"
#include "AtModuleBer.h"
#include "AtBerController.h"
#include "AtModuleRam.h"
#include "AtRam.h"


/* 定义全局变量 */
static WORD32 g_drv_tdm_debug_flag = 0;  /* 调试打印开关 */

/* STM单板的STM端口模式,具体参见DRV_TDM_STM_INTF_MODE定义 */
static WORD32 g_drv_tdm_stm_intf_mode[DRV_TDM_MAX_STM_CARD_NUM][DRV_TDM_MAX_STM_PORT_NUM] = {{0}};

/* 保存ARRIVE driver的指针.对于ARRIVE方案,一个CPU对应一个Driver.*/
static AtDriver g_drv_tdm_atDevDriverPtr = NULL; 

/* 保存ARRIVE device的指针*/
static AtDevice g_drv_tdm_atDevicePtr[DRV_TDM_MAX_STM_CARD_NUM] = {NULL, NULL, NULL, NULL};

/* 保存ARRIVE HAL的指针 */
static AtHal g_drv_tdm_atDevHalPtr[DRV_TDM_MAX_STM_CARD_NUM] = {NULL, NULL, NULL, NULL};

/* 保存SDH Line的指针 */
static AtSdhLine g_drv_tdm_atSdhLinePtr[DRV_TDM_MAX_STM_CARD_NUM][DRV_TDM_MAX_STM_PORT_NUM] = {{NULL}};

/* 保存SDH AUG4的指针 */
static AtSdhAug g_drv_tdm_atSdhAug4Ptr[DRV_TDM_MAX_STM_CARD_NUM][DRV_TDM_MAX_STM_PORT_NUM] = {{NULL}};

/* 保存SDH AUG1的指针 */
static AtSdhAug g_drv_tdm_atSdhAug1Ptr[DRV_TDM_MAX_STM_CARD_NUM][DRV_TDM_MAX_STM_PORT_NUM] = {{NULL}};

/* 保存SDH AU4的指针 */
static AtSdhAu g_drv_tdm_atSdhAu4Ptr[DRV_TDM_MAX_STM_CARD_NUM][DRV_TDM_MAX_STM_PORT_NUM] = {{NULL}};

/* 保存SDH VC4的指针 */
static AtSdhVc g_drv_tdm_atSdhVc4Ptr[DRV_TDM_MAX_STM_CARD_NUM][DRV_TDM_MAX_STM_PORT_NUM] = {{NULL}};

/* 保存SDH TU12的指针 */
static AtSdhTu g_drv_tdm_atSdhTu12Ptr[DRV_TDM_MAX_STM_CARD_NUM][DRV_TDM_MAX_STM_PORT_NUM][DRV_TDM_E1_NUM_PER_STM1 + 1] = {{{NULL}}};

/* 保存SDH VC12的指针 */
static AtSdhVc g_drv_tdm_atSdhVc12Ptr[DRV_TDM_MAX_STM_CARD_NUM][DRV_TDM_MAX_STM_PORT_NUM][DRV_TDM_E1_NUM_PER_STM1 + 1] = {{{NULL}}};

/* 保存PDH E1的指针 */
static AtPdhDe1 g_drv_tdm_atPdhE1Ptr[DRV_TDM_MAX_STM_CARD_NUM][DRV_TDM_MAX_STM_PORT_NUM][DRV_TDM_E1_NUM_PER_STM1 + 1] = {{{NULL}}};

/* 保存Ethernet Port的指针 */
static AtEthPort g_drv_tdm_atEthPortPtr[DRV_TDM_MAX_STM_CARD_NUM][DRV_TDM_CHIP_ETH_INTF_NUM] = {{NULL}};

/* STM TDM PWE3模块的初始化成功标志,1表示初始化成功,0表示初始化失败*/
static WORD32 g_drv_tdm_pwe3_init_flag[DRV_TDM_MAX_STM_CARD_NUM] = {0};

/* STM单板的源MAC地址 */
static BYTE g_drv_tdm_boardSrcMacAddr[DRV_TDM_MAC_ADDR_SIZE] = {0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA}; 

/* FPGA芯片的product code */
static WORD32 g_drv_tdm_fpgaProductCode[DRV_TDM_MAX_STM_CARD_NUM] = {0};


/**************************************************************************
* 函数名称: DRV_TDM_DEBUG_TRACE
* 功能描述: STM-1 TDM PWE3模块的调试打印函数.
* 访问的表: 无
* 修改的表: 无
* 输入参数: flag: 打印标记参数; format: 格式化字符串.
* 输出参数: 无.
* 返 回 值: 无.
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
VOID DRV_TDM_DEBUG_TRACE(WORD32 flag, CHAR* format, ...)
{
    if ((0 != (g_drv_tdm_debug_flag & flag)) || (DRV_TDM_DBG_ALL_MSG == flag))
    {
        va_list args;
        va_start(args, format);
        vfprintf(stderr, format, args);
        va_end(args);
    }
    
    return;
}


/**************************************************************************
* 函数名称: drv_tdm_stmIntfModeGet
* 功能描述: 获取STM端口的模式.
* 访问的表: 无
* 修改的表: 无
* 输入参数: ucChipId: ARRIVE芯片编号,取值为0~3.
*           ucPortId: STM端口的编号,取值为1~8.
* 输出参数: *pdwIntfMode: 保存STM端口模式,具体参见DRV_TDM_STM_INTF_MODE定义.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stmIntfModeGet(BYTE ucChipId, BYTE ucPortId, WORD32 *pdwIntfMode)
{
    BYTE ucPortIdx = 0;
    
    DRV_TDM_CHECK_CHIP_ID(ucChipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pdwIntfMode);
    ucPortIdx = ucPortId - (BYTE)1;
    DRV_TDM_CHECK_STM_INDEX(ucPortIdx);
    
    *pdwIntfMode = g_drv_tdm_stm_intf_mode[ucChipId][ucPortIdx];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_maxStmPortIdGet
* 功能描述: 获取STM单板的最大STM端口号.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
* 输出参数: *pMaxStmPortId: 保存STM端口的最大编号,取值为4或者8.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-08-15   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_maxStmPortIdGet(BYTE subslotId, BYTE *pMaxStmPortId)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 boardType = 0;
    
    DRV_STM_CHECK_POINTER_IS_NULL(pMaxStmPortId);
    
    rv = BSP_cp3ban_boardTypeGet((WORD32)subslotId, &boardType);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_boardTypeGet() rv=0x%x\n", __FILE__, __LINE__, subslotId, rv);
        return rv;
    }
    if (BSP_CP3BAN_8PORT_BOARD == boardType)  /* 8端口的STM单板 */
    {
        *pMaxStmPortId = DRV_TDM_STM_8PORT_ID_END;
    }
    else if (BSP_CP3BAN_4PORT_BOARD == boardType)  /* 4端口的STM单板 */
    {
        *pMaxStmPortId = DRV_TDM_STM_4PORT_ID_END;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, invalid board type, boardType=%u.\n", __FILE__, __LINE__, subslotId, boardType);
        return DRV_TDM_INVALID_BOARD_TYPE;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_maxAug1IdGet
* 功能描述: 获取最大的AUG1编号.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: STM端口编号,取值为1~8.
* 输出参数: *pMaxAug1Id: 保存AUG1的最大编号,取值为1或者4.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-08-15   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_maxAug1IdGet(BYTE chipId, BYTE portId, BYTE *pMaxAug1Id)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwStmIntfMode = 0;
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pMaxAug1Id);
    
    rv = drv_tdm_stmIntfModeGet(chipId, portId, &dwStmIntfMode);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_stmIntfModeGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    if (DRV_TDM_STM1_INTF == dwStmIntfMode)  /* STM-1 interface mode */
    {
        *pMaxAug1Id = DRV_TDM_MIN_AUG1_ID;
    }
    else if (DRV_TDM_STM4_INTF == dwStmIntfMode) /* STM-4 interface mode */
    {
        *pMaxAug1Id = DRV_TDM_MAX_AUG1_ID;
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, chipId %u portId %u, invalid stmIntfMode, stmIntfMode=%u.\n", __FILE__, __LINE__, chipId, portId, dwStmIntfMode);
        return DRV_TDM_INVALID_STM_INTF_MODE;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stm1IndexGet
* 功能描述: 获取STM1索引号.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
* 输出参数: *pStmIntfIndex: 保存STM1索引号.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-08-15   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stm1IndexGet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *pStm1Index)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwStmIntfMode = 0;
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pStm1Index);
    
    rv = drv_tdm_stmIntfModeGet(chipId, portId, &dwStmIntfMode);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_stmIntfModeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_STM1_INTF == dwStmIntfMode)  /* STM-1 interface mode */
    {
        *pStm1Index = portId - (BYTE)1;
    }
    else if (DRV_TDM_STM4_INTF == dwStmIntfMode) /* STM-4 interface mode */
    {
        rv = drv_tdm_aug1IndexGet(portId, aug1Id, pStm1Index);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_aug1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, invalid stmIntfMode, stmIntfMode=%u.\n", __FILE__, __LINE__, dwStmIntfMode);
        return DRV_TDM_INVALID_STM_INTF_MODE;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stmPortIdCheck
* 功能描述: 根据CP3BAN单板的类型来检查port ID.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: ARRIVE芯片的编号,取值为0~3.
*           portId: STM端口的编号,取值为1~8.
* 输出参数: 
* 返 回 值: 无.
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stmPortIdCheck(BYTE chipId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwSubslotId = 0;  /* 单板的子槽位号 */
    WORD32 boardType = 0;    /* 单板类型 */
    
    dwSubslotId = (WORD32)chipId + 1;
    
    rv = BSP_cp3ban_boardTypeGet(dwSubslotId, &boardType);
    if (BSP_E_CP3BAN_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_boardTypeGet() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    
    if (BSP_CP3BAN_8PORT_BOARD == boardType)  /* 8端口的STM单板 */
    {
        DRV_TDM_CHECK_8PORT_ID(portId);
    }
    else if (BSP_CP3BAN_4PORT_BOARD == boardType)  /* 4端口的STM单板 */
    {
        DRV_TDM_CHECK_4PORT_ID(portId);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, subslotId %u, invalid board type, boardType=%u.\n", __FILE__, __LINE__, dwSubslotId, boardType);
        return DRV_TDM_INVALID_BOARD_TYPE;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stmIntfModeSet
* 功能描述: 设置STM端口的模式.
* 访问的表: 软件表g_drv_tdm_stm_intf_mode.
* 修改的表: 软件表g_drv_tdm_stm_intf_mode.
* 输入参数: ucChipId: ARRIVE芯片编号,取值为0~3.
*           ucPortId: STM端口的编号,取值为1~8.
*           dwIntfMode: STM端口模式,具体参见DRV_TDM_STM_INTF_MODE定义.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stmIntfModeSet(BYTE ucChipId, BYTE ucPortId, WORD32 dwIntfMode)
{
    BYTE ucPortIdx = 0;
    
    DRV_TDM_CHECK_CHIP_ID(ucChipId);
    ucPortIdx = ucPortId - (BYTE)1;
    DRV_TDM_CHECK_STM_INDEX(ucPortIdx);
    
    g_drv_tdm_stm_intf_mode[ucChipId][ucPortIdx] = dwIntfMode;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_debugFlagGet
* 功能描述: 获取STM-1 TDM PWE3模块的调试打印开关.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 无.
* 输出参数: 无.
* 返 回 值: 无.
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_debugFlagGet(VOID)
{
    return g_drv_tdm_debug_flag;
}


/**************************************************************************
* 函数名称: drv_tdm_debugFlagSet
* 功能描述: 设置STM-1 TDM PWE3模块的调试打印开关的值.
* 访问的表: 无.
* 修改的表: 无.
* 输入参数: flag: 调试控制开关的值.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-07   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_debugFlagSet(WORD32 flag)
{
    g_drv_tdm_debug_flag = flag;
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwe3InitFlagSet
* 功能描述: 设置STM-1 TDM PWE3模块的初始化标志.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号.
*           flag: 初始化标记.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwe3InitFlagSet(BYTE chipId, WORD32 flag)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    g_drv_tdm_pwe3_init_flag[chipId] = flag;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwe3InitFlagGet
* 功能描述: 获取STM-1 TDM PWE3模块的初始化标志.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号  
* 输出参数: pFlag: 保存STM-1单板初始化标记的值. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwe3InitFlagGet(BYTE chipId, WORD32 *pFlag)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pFlag);
    
    *pFlag = g_drv_tdm_pwe3_init_flag[chipId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_threadMonitorFlagSet
* 功能描述: 设置STM TDM PWE3模块的线程监控标记.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号.
*           flag: 任务监控标记.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-13   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_threadMonitorFlagSet(BYTE chipId, WORD32 flag)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    
    rv = drv_tdm_sdhBerFlagSet(chipId, flag);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_sdhBerFlagSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_e1ClkSwitchAlmFlagSet(chipId, flag);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_e1ClkSwitchAlmFlagSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_sdhAlarmFlagSet(chipId, flag);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_sdhAlarmFlagSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_sdhPerfFlagSet(chipId, flag);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_sdhPerfFlagSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_pwe3PerfAlmFlagSet(chipId, flag);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_pwe3PerfAlmFlagSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
 
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_memoryAllocate
* 功能描述: 动态分配内存
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: ARRIVE芯片的编号,取值为0~3. 
* 输出参数: 无 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_memoryAllocate(BYTE chipId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    
    rv = drv_tdm_pwMemAllocate(chipId);  /* 动态分配内存,该内存用来保存PW配置信息 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwMemAllocate() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_sdhIntfMemAllocate(chipId);  /* 动态分配内存,该内存用来保存SDH接口配置信息 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_sdhIntfMemAllocate() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_sdhPerfMemAllocate(chipId);  /* 动态分配内存,该内存用来保存SDH性能统计信息 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_sdhPerfMemAllocate() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_clkDomainMemAllocate(chipId);  /* 该内存用来保存时钟域信息. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_clkDomainMemAllocate() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_sdhAlarmMemAllocate(chipId);  /* 该内存用来保存SDH告警 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_sdhAlarmMemAllocate() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, chipId %u, Allocate memory successfully.\n", __FILE__, __LINE__, chipId);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_memoryFree
* 功能描述: 释放已经动态分配的内存
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: ARRIVE芯片的编号,取值为0~3. 
* 输出参数: 无 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_memoryFree(BYTE chipId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    
    rv = drv_tdm_pwMemFree(chipId);  /* 释放已经动态分配的内存,该内存用来保存PW配置信息 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_pwMemFree() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_sdhIntfMemFree(chipId);  /* 释放已经动态分配的内存,该内存用来保存SDH接口配置信息 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_sdhIntfMemFree() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_sdhPerfMemFree(chipId);  /* 释放已经动态分配的内存,该内存用来保存SDH性能统计信息 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_sdhPerfMemFree() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_clkDomainMemFree(chipId);  /* 释放已经动态分配的内存,该内存用来保存时钟域信息. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_clkDomainMemFree() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_sdhAlarmMemFree(chipId);  /* 释放已经动态分配的内存,该内存用来保存SDH告警. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_sdhAlarmMemFree() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, chipId %u, Free memory successfully.\n", __FILE__, __LINE__, chipId);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_tug3Tug2Tu12IdGet
* 功能描述: 获取VC4中的TUG3,TUG2,TU12编号.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 通过E1链路编号来获取TUG3,TUG2,TU12的编号
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_tug3Tug2Tu12IdGet(BYTE e1LinkId, 
                                            BYTE *tug3Id, 
                                            BYTE *tug2Id, 
                                            BYTE *tu12Id)
{    
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(tug3Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(tug2Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(tu12Id);

    *tug3Id = ((e1LinkId - 1) / DRV_TDM_E1_NUM_PER_TUG3) + 1;
    *tug2Id = (((e1LinkId - 1) % DRV_TDM_E1_NUM_PER_TUG3) / DRV_TDM_E1_NUM_PER_TUG2) + 1;
    *tu12Id = ((e1LinkId - 1) % DRV_TDM_E1_NUM_PER_TUG2) + 1;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1LinkIdGet
* 功能描述: 获取VC4中的E1链路的编号.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 通过TUG3,TUG2,TU12编号来获取VC4中的E1链路的编号
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1LinkIdGet(BYTE tug3Id, BYTE tug2Id, BYTE tu12Id, BYTE *e1LinkId)
{    
    DRV_TDM_CHECK_TUG3_ID(tug3Id);
    DRV_TDM_CHECK_TUG2_ID(tug2Id);
    DRV_TDM_CHECK_TU12_ID(tu12Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(e1LinkId);

    *e1LinkId = (tug3Id - 1) * DRV_TDM_E1_NUM_PER_TUG3 + (tug2Id - 1) * DRV_TDM_E1_NUM_PER_TUG2 + tu12Id;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_linkIdGet
* 功能描述: 获取STM单板上的E1链路的编号.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路的编号,取值为1~63.
* 输出参数: *pLinkId: 保存单板上的E1链路的编号,取值为1~504. 
* 返 回 值: 
* 其它说明: 根据portId,aug1Id和VC4中的e1LinkId来计算单板上的E1链路编号.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_linkIdGet(BYTE chipId, 
                              BYTE portId, 
                              BYTE aug1Id, 
                              BYTE e1LinkId, 
                              WORD32 *pLinkId)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE stmIntfIndex = 0;  /* STM接口的索引号,取值为0~7. */

    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pLinkId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_stm1IndexGet(chipId, portId, aug1Id, &stmIntfIndex);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_stm1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    *pLinkId = ((WORD32)stmIntfIndex) * DRV_TDM_E1_NUM_PER_STM1 + (WORD32)e1LinkId;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_aug1IndexGet
* 功能描述: 获取STM-4帧的AUG1索引号.
* 访问的表: 无
* 修改的表: 无
* 输入参数: ucPortId: STM-4端口的编号,取值为1或者5.
*           ucAug1Id: STM-4帧中的AUG1编号,取值为1~4.
* 输出参数: *pucAug1Idx: 保存单板上的AUG1索引号,取值为0~7. 
* 返 回 值: 
* 其它说明: 根据ucPortId和ucAug1Id来计算STM-4帧的AUG1索引号.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-01-14   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_aug1IndexGet(BYTE ucPortId, BYTE ucAug1Id, BYTE *pucAug1Idx)
{
    DRV_TDM_CHECK_AUG1_ID(ucAug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(pucAug1Idx);
    
    if (DRV_TDM_STM_PORT_ID_START == ucPortId)
    {
        *pucAug1Idx = ucAug1Id - (BYTE)1;
    }
    else if ((DRV_TDM_STM_PORT_ID_START + (DRV_TDM_STM_8PORT_ID_END / 2)) == ucPortId)
    {
        *pucAug1Idx = (DRV_TDM_STM_8PORT_ID_END / 2) + (ucAug1Id - 1);
    }
    else
    {
        return DRV_TDM_INVALID_PORT_ID;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1TimeslotNumGet
* 功能描述: 通过E1帧的时隙位图来获取E1帧的时隙数
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: E1帧的时隙数指的是传输数据的时隙,不包括传输信令的时隙.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1TimeslotNumGet(WORD32 timeslotBmp, BYTE *timeslotNum)
{
    WORD32 i = 0;  /* 循环变量 */
    BYTE tmpTsNum = 0;  /* 临时变量 */
    
    DRV_TDM_CHECK_POINTER_IS_NULL(timeslotNum);
    
    if (0 == timeslotBmp)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid e1 timeslotBitmap=0.\n", __FILE__, __LINE__);
        return DRV_TDM_ERROR;  
    }
    else if (DRV_TDM_UNFRAME_E1_BMP == timeslotBmp)  /* unframe E1 */
    {
        *timeslotNum = DRV_TDM_E1_MAX_TS_NUM;
    }
    else  /* frame E1 */
    {
        for (i = 0; i < DRV_TDM_E1_MAX_TS_NUM; i++)
        {
            if (0 != ((((WORD32)0x1) << i) & timeslotBmp))
            {
                tmpTsNum++;
            }
        }
        *timeslotNum = tmpTsNum;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atDevProductCodeGet
* 功能描述: 获取ARRIVE芯片的product code.
* 访问的表: 软件表g_drv_tdm_fpgaProductCode.
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3. 
* 输出参数: *pProductCode: 保存ARRIVE芯片的product code. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atDevProductCodeGet(BYTE chipId, WORD32 *pProductCode)
{ 
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pProductCode);    
    
    *pProductCode = g_drv_tdm_fpgaProductCode[chipId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_fpgaVersionNumberGet
* 功能描述: 获取FPGA芯片的Version Number.
* 访问的表: 无.
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4. 
* 输出参数: *pVerNumber: 保存Version Number. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-07-31   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_fpgaVersionNumberGet(BYTE subslotId, WORD32 *pVerNumber)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;
    AtDevice pDevice = NULL;
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pVerNumber);
    chipId = subslotId - (BYTE)1;
    
    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, chipId %u, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    
    *pVerNumber = AtDeviceVersionNumber(pDevice);  /* Get version number */
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atDevDriverPtrGet
* 功能描述: 获取ARRIVE芯片的driver指针
* 访问的表: 软件表g_drv_tdm_atDevDriverPtr
* 修改的表: 无
* 输入参数: 无 
* 输出参数: ppDriver为二级指针 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atDevDriverPtrGet(AtDriver *ppDriver)
{    
    DRV_TDM_CHECK_POINTER_IS_NULL(ppDriver);
    
    *ppDriver = g_drv_tdm_atDevDriverPtr;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atDevicePtrGet
* 功能描述: 获取ARRIVE芯片的device指针
* 访问的表: 软件表g_drv_tdm_atDevicePtr
* 修改的表: 无
* 输入参数: chipId: 芯片编号  
* 输出参数: ppDevice为二级指针 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atDevicePtrGet(BYTE chipId, AtDevice *ppDevice)
{    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(ppDevice);
    
    *ppDevice = g_drv_tdm_atDevicePtr[chipId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atDevHalGet
* 功能描述: 获取ARRIVE芯片的HAL指针
* 访问的表: 软件表g_drv_tdm_atDevHalPtr
* 修改的表: 无
* 输入参数: chipId: 芯片编号  
* 输出参数: ppHal: 为二级指针 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-24   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atDevHalGet(BYTE chipId, AtHal *ppHal)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(ppHal);
    
    *ppHal = g_drv_tdm_atDevHalPtr[chipId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atSdhLinePtrGet
* 功能描述: 获取ARRIVE芯片的SDH Line指针
* 访问的表: 软件表g_drv_tdm_atSdhLinePtr
* 修改的表: 无.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
* 输出参数: ppSdhLine: 为二级指针 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-24   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atSdhLinePtrGet(BYTE chipId, BYTE portId, AtSdhLine *ppSdhLine)
{
    BYTE stmIntfIndex = 0;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(ppSdhLine);
    stmIntfIndex = portId - (BYTE)1;
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    *ppSdhLine = g_drv_tdm_atSdhLinePtr[chipId][stmIntfIndex];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atSdhAug4PtrGet
* 功能描述: 获取ARRIVE芯片的SDH AUG4指针
* 访问的表: 软件表g_drv_tdm_atSdhAug4Ptr
* 修改的表: 无.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
* 输出参数: ppSdhAug4: 为二级指针 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-01-14   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atSdhAug4PtrGet(BYTE chipId, BYTE portId, AtSdhAug *ppSdhAug4)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE ucIntfIdx = 0;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(ppSdhAug4);
    rv = drv_tdm_stm4PortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_stm4PortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    ucIntfIdx = portId - (BYTE)1;
    DRV_TDM_CHECK_STM_INDEX(ucIntfIdx);
    
    *ppSdhAug4 = g_drv_tdm_atSdhAug4Ptr[chipId][ucIntfIdx];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atSdhAug1PtrGet
* 功能描述: 获取ARRIVE芯片的SDH AUG1指针
* 访问的表: 软件表g_drv_tdm_atSdhAug1Ptr
* 修改的表: 无.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
* 输出参数: ppSdhAug1: 为二级指针 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-24   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atSdhAug1PtrGet(BYTE chipId, 
                                         BYTE portId, 
                                         BYTE aug1Id, 
                                         AtSdhAug *ppSdhAug1)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE stmIntfIndex = 0;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(ppSdhAug1);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_stm1IndexGet(chipId, portId, aug1Id, &stmIntfIndex);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_stm1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    *ppSdhAug1 = g_drv_tdm_atSdhAug1Ptr[chipId][stmIntfIndex];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atSdhAu4PtrGet
* 功能描述: 获取ARRIVE芯片的SDH AU4指针
* 访问的表: 软件表g_drv_tdm_atSdhAu4Ptr
* 修改的表: 无.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
* 输出参数: ppSdhAu4: 为二级指针 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-24   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atSdhAu4PtrGet(BYTE chipId, 
                                        BYTE portId, 
                                        BYTE aug1Id, 
                                        AtSdhAu *ppSdhAu4)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE stmIntfIndex = 0;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(ppSdhAu4);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_stm1IndexGet(chipId, portId, aug1Id, &stmIntfIndex);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_stm1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    *ppSdhAu4 = g_drv_tdm_atSdhAu4Ptr[chipId][stmIntfIndex];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atSdhVc4PtrGet
* 功能描述: 获取ARRIVE芯片的SDH VC4指针
* 访问的表: 软件表g_drv_tdm_atSdhVc4Ptr
* 修改的表: 无.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
* 输出参数: ppSdhVc4: 为二级指针 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-24   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atSdhVc4PtrGet(BYTE chipId, 
                                       BYTE portId, 
                                       BYTE aug1Id, 
                                       AtSdhVc *ppSdhVc4)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE stmIntfIndex = 0;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(ppSdhVc4);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_stm1IndexGet(chipId, portId, aug1Id, &stmIntfIndex);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_stm1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    *ppSdhVc4 = g_drv_tdm_atSdhVc4Ptr[chipId][stmIntfIndex];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atSdhTu12PtrGet
* 功能描述: 获取ARRIVE芯片的SDH TU12指针
* 访问的表: 软件表g_drv_tdm_atSdhTu12Ptr
* 修改的表: 无.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
* 输出参数: ppSdhTu12: 为二级指针 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-24   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atSdhTu12PtrGet(BYTE chipId, 
                                         BYTE portId, 
                                         BYTE aug1Id, 
                                         BYTE e1LinkId, 
                                         AtSdhTu *ppSdhTu12)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE stmIntfIndex = 0;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(ppSdhTu12);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_stm1IndexGet(chipId, portId, aug1Id, &stmIntfIndex);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_stm1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    *ppSdhTu12 = g_drv_tdm_atSdhTu12Ptr[chipId][stmIntfIndex][e1LinkId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atSdhVc12PtrGet
* 功能描述: 获取ARRIVE芯片的SDH VC12指针
* 访问的表: 软件表g_drv_tdm_atSdhVc12Ptr
* 修改的表: 无.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
* 输出参数: ppSdhVc12: 为二级指针 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-24   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atSdhVc12PtrGet(BYTE chipId, 
                                         BYTE portId, 
                                         BYTE aug1Id, 
                                         BYTE e1LinkId, 
                                         AtSdhVc *ppSdhVc12)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE stmIntfIndex = 0;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(ppSdhVc12);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_stm1IndexGet(chipId, portId, aug1Id, &stmIntfIndex);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_stm1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    *ppSdhVc12 = g_drv_tdm_atSdhVc12Ptr[chipId][stmIntfIndex][e1LinkId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atPdhE1PtrGet
* 功能描述: 获取ARRIVE芯片的PDH E1指针
* 访问的表: 软件表g_drv_tdm_atPdhE1Ptr
* 修改的表: 无.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
* 输出参数: ppPdhE1: 为二级指针 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-24   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atPdhE1PtrGet(BYTE chipId, 
                                     BYTE portId, 
                                     BYTE aug1Id, 
                                     BYTE e1LinkId, 
                                     AtPdhDe1 *ppPdhE1)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE stmIntfIndex = 0;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(ppPdhE1);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_stm1IndexGet(chipId, portId, aug1Id, &stmIntfIndex);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, drv_tdm_stm1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    *ppPdhE1 = g_drv_tdm_atPdhE1Ptr[chipId][stmIntfIndex][e1LinkId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atDevEthPortPtrGet
* 功能描述: 获取ARRIVE芯片的Ethernet port指针
* 访问的表: 软件表g_drv_tdm_atEthPortPtr
* 修改的表: 软件表g_drv_tdm_atEthPortPtr
* 输入参数: chipId: 芯片编号,取值为0~3.
*           ethIntfIndex: Ethernet接口的索引,取值为0~1.
* 输出参数: ppEthPort: 为二级指针 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-24   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atDevEthPortPtrGet(BYTE chipId, BYTE ethIntfIndex, AtEthPort *ppEthPort)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_ETH_INTF_INDEX(ethIntfIndex);
    DRV_TDM_CHECK_POINTER_IS_NULL(ppEthPort);
    
    *ppEthPort = g_drv_tdm_atEthPortPtr[chipId][ethIntfIndex];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_ethIntfInit
* 功能描述: 初始化Ethernet接口
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           ethIntfIndex: Ethernet接口的索引号,取值为0~1.
* 输出参数:  
* 返 回 值: 
* 其它说明: L2VPN业务不需要配置IP地址. 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_ethIntfInit(BYTE chipId, BYTE ethIntfIndex)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 productCode = 0;
    AtDevice pDevice = NULL;
    AtModuleEth pEthModule = NULL;  /* Ethernet module */
    AtEthPort pEthPort = NULL;      /* Ethernet port */
    AtSerdesController pSerdesCtrl = NULL;
    eAtEthPortInterface intfType = 0;
    eAtEthPortSpeed intfSpeed = 0;
    eBool flowCtrlFlag = cAtFalse;  /* The flag to flow control. */
    BYTE txIpg = 0;
    BYTE rxIpg = 0;
    
    DRV_TDM_CHECK_ETH_INTF_INDEX(ethIntfIndex);

    rv = drv_tdm_atDevProductCodeGet(chipId, &productCode);  /* Get product code of FPGA. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atDevProductCodeGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    DRV_STM_CHECK_POINTER_IS_NULL(pDevice);

    pEthModule = (AtModuleEth)AtDeviceModuleGet(pDevice, cAtModuleEth);  /* Get Ethernet Module */
    DRV_STM_CHECK_POINTER_IS_NULL(pEthModule);
    pEthPort = AtModuleEthPortGet(pEthModule, ethIntfIndex);  /* Get Ethernet port */
    DRV_STM_CHECK_POINTER_IS_NULL(pEthPort);
    g_drv_tdm_atEthPortPtr[chipId][ethIntfIndex] = pEthPort; /* 保存Ethernet port指针 */

    if ((DRV_TDM_FPGA_CODE_1G_CES == productCode) 
        || (DRV_TDM_FPGA_CODE_1G_MLPPP == productCode))
    {
        intfType = cAtEthPortInterfaceSgmii;
        intfSpeed = cAtEthPortSpeed1000M;
        flowCtrlFlag = cAtFalse;
        txIpg = DRV_TDM_AT_DEV_TX_IPG_1G_CES;
        rxIpg = DRV_TDM_AT_DEV_RX_IPG_1G_CES;
    }
    else if ((DRV_TDM_FPGA_CODE_10G_CES == productCode) 
        || (DRV_TDM_FPGA_CODE_10G_MLPPP == productCode))
    {
        intfType = cAtEthPortInterfaceXGMii;
        intfSpeed = cAtEthPortSpeed10G;
        flowCtrlFlag = cAtTrue;
        txIpg = DRV_TDM_AT_DEV_TX_IPG_10G_CES;
        rxIpg = DRV_TDM_AT_DEV_RX_IPG_10G_CES;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, productCode 0x%x is invalid.\n", __FILE__, __LINE__, chipId, productCode);
        return DRV_TDM_AT_INVALID_PRODUCT_CODE;
    }
    
    rv = AtEthPortSourceMacAddressSet(pEthPort, g_drv_tdm_boardSrcMacAddr);  /* Set source MAC address */
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtEthPortSourceMacAddressSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    rv = AtEthPortMacCheckingEnable(pEthPort, cAtFalse); /* Disable MAC address check */
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtEthPortMacCheckingEnable() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    rv = AtEthPortInterfaceSet(pEthPort, intfType); /* 设置Ethernet接口的接口类型 */
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtEthPortInterfaceSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    rv = AtEthPortSpeedSet(pEthPort, intfSpeed); /* Set Speed for Ethernet port */
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtEthPortSpeedSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    if ((DRV_TDM_FPGA_CODE_10G_CES == productCode) 
        || (DRV_TDM_FPGA_CODE_10G_MLPPP == productCode))
    {
        rv = AtEthPortHiGigEnable(pEthPort, cAtTrue);  /* Enable HiGig */
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtEthPortHiGigEnable() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    rv = AtEthPortDuplexModeSet(pEthPort, cAtEthPortWorkingModeFullDuplex); /* Set working mode for Ethernet port */
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtEthPortDuplexModeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    rv = AtEthPortAutoNegEnable(pEthPort, cAtFalse); /* Disable Ethernet port auto-negotiation */
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtEthPortAutoNegEnable() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    rv = AtEthPortTxIpgSet(pEthPort, txIpg); /* Set number of Inter Packet Gaps at Tx direction */
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtEthPortTxIpgSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    rv = AtEthPortRxIpgSet(pEthPort, rxIpg); /* Set number of Inter Packet Gaps at Rx direction */
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtEthPortRxIpgSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    rv = AtEthPortFlowControlEnable(pEthPort, flowCtrlFlag); 
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtEthPortFlowControlEnable() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    rv = AtEthPortPauseFrameIntervalSet(pEthPort, 0); /* Set Pause Frame Interval */
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtEthPortPauseFrameIntervalSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    rv = AtEthPortPauseFrameExpireTimeSet(pEthPort, 0); /* Set Pause Frame expire time */
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtEthPortPauseFrameExpireTimeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    if ((DRV_TDM_FPGA_CODE_1G_MLPPP == productCode) 
        || (DRV_TDM_FPGA_CODE_10G_MLPPP == productCode))
    {
        rv = AtEthPortMaxPacketSizeSet(pEthPort, 10000);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtEthPortMaxPacketSizeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
    }

    pSerdesCtrl = AtEthPortSerdesController(pEthPort);  /* Get SERDES controller */
    DRV_STM_CHECK_POINTER_IS_NULL(pSerdesCtrl);
    rv = AtSerdesControllerPhysicalParamSet(pSerdesCtrl, cAtSerdesParamVod, 50); /* Set physical parameter */
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSerdesControllerPhysicalParamSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stm1MappingSet
* 功能描述: 设置STM-1帧的映射方式
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           ucIntfIdx: STM-1接口的索引号,取值为0~7.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stm1MappingSet(BYTE chipId, BYTE ucIntfIdx)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtDevice pDevice = NULL;
    AtModuleSdh pSdhModule = NULL;
    AtSdhLine pLine = NULL;
    AtSdhAug pAug1 = NULL;
    AtSdhAu pAu4 = NULL;
    AtSdhVc pVc4 = NULL;
    AtSdhTug pTug3 = NULL;
    AtSdhTug pTug2 = NULL;
    AtSdhTu pTu12 = NULL;
    AtSdhVc pVc12 = NULL;
    AtPdhDe1 pE1 = NULL;
    BYTE tug3Id = 0;  /* ARRIVE芯片的TUG3编号,芯片从0开始编号 */
    BYTE tug2Id = 0;  /* ARRIVE芯片的TUG2编号,芯片从0开始编号 */
    BYTE tu12Id = 0;  /* ARRIVE芯片的TU12编号,芯片从0开始编号 */
    BYTE tug3No = 0;  /* 从1开始取值 */
    BYTE tug2No = 0;  /* 从1开始取值 */
    BYTE tu12No = 0;  /* 从1开始取值 */
    BYTE e1LinkId = 0;

    DRV_TDM_CHECK_STM_INDEX(ucIntfIdx);
    
    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_STM_CHECK_POINTER_IS_NULL(pDevice);
    
    /* Get SDH module */
    pSdhModule = (AtModuleSdh)AtDeviceModuleGet(pDevice, cAtModuleSdh);
    DRV_STM_CHECK_POINTER_IS_NULL(pSdhModule);
    
    /* Get line object */
    pLine = AtModuleSdhLineGet(pSdhModule, ucIntfIdx);
    DRV_STM_CHECK_POINTER_IS_NULL(pLine);
    
    /* Map desired AUG-1 to VC-4 */
    pAug1 = AtSdhLineAug1Get(pLine, 0); /* Get AUG-1 */
    DRV_STM_CHECK_POINTER_IS_NULL(pAug1);
    g_drv_tdm_atSdhAug1Ptr[chipId][ucIntfIdx] = pAug1;
    rv = AtSdhChannelMapTypeSet((AtSdhChannel)pAug1, cAtSdhAugMapTypeAug1MapVc4);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelMapTypeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }

    /* Get AU-4 */
    pAu4 = AtSdhLineAu4Get(pLine, 0);
    DRV_STM_CHECK_POINTER_IS_NULL(pAu4);
    g_drv_tdm_atSdhAu4Ptr[chipId][ucIntfIdx] = pAu4;
    
    /* Map VC-4 to 3xTUG-3 */
    pVc4 = AtSdhLineVc4Get(pLine, 0); /* Get VC-4 */
    DRV_STM_CHECK_POINTER_IS_NULL(pVc4);
    g_drv_tdm_atSdhVc4Ptr[chipId][ucIntfIdx] = pVc4; /* save VC4 pointer. */
    rv = AtSdhChannelMapTypeSet((AtSdhChannel)pVc4, cAtSdhVcMapTypeVc4Map3xTug3s);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelMapTypeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    for (tug3Id = 0; tug3Id < DRV_TDM_MAX_TUG3_ID; tug3Id++)
    {
        tug3No = tug3Id + (BYTE)1;
        /* Map TUG-3 to 7xTUG-2 */
        pTug3 = AtSdhLineTug3Get(pLine, 0, tug3Id); /* Get TUG-3 */
        DRV_STM_CHECK_POINTER_IS_NULL(pTug3);
        rv = AtSdhChannelMapTypeSet((AtSdhChannel)pTug3, cAtSdhTugMapTypeTug3Map7xTug2s);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelMapTypeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
        
        for (tug2Id = 0; tug2Id < DRV_TDM_MAX_TUG2_ID; tug2Id++)
        {
            tug2No = tug2Id + (BYTE)1;
            /* Map TUG-2 to 3xTU-12 */
            pTug2 = AtSdhLineTug2Get(pLine, 0, tug3Id, tug2Id); /* Get TUG-2 */
            DRV_STM_CHECK_POINTER_IS_NULL(pTug2);
            rv = AtSdhChannelMapTypeSet((AtSdhChannel)pTug2, cAtSdhTugMapTypeTug2Map3xTu12s);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelMapTypeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            
            for (tu12Id = 0; tu12Id < DRV_TDM_MAX_TU12_ID; tu12Id++)
            {
                tu12No = tu12Id + (BYTE)1;
                rv = drv_tdm_e1LinkIdGet(tug3No, tug2No, tu12No, &e1LinkId);
                if (DRV_TDM_OK != rv)
                {
                    BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_e1LinkIdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                    return rv;
                }

                /* Get TU-12 */
                pTu12 = AtSdhLineTu1xGet(pLine, 0, tug3Id, tug2Id, tu12Id);
                DRV_STM_CHECK_POINTER_IS_NULL(pTu12);
                g_drv_tdm_atSdhTu12Ptr[chipId][ucIntfIdx][e1LinkId] = pTu12;
    
                /* Map TU-12 to VC-12 */
                pVc12 = AtSdhLineVc1xGet(pLine, 0, tug3Id, tug2Id, tu12Id); /* Get VC-12 */
                DRV_STM_CHECK_POINTER_IS_NULL(pVc12);
                g_drv_tdm_atSdhVc12Ptr[chipId][ucIntfIdx][e1LinkId] = pVc12; /* save SDH VC12. */
                rv = AtSdhChannelMapTypeSet((AtSdhChannel)pVc12, cAtSdhVcMapTypeVc1xMapDe1);
                if (cAtOk != rv)
                {
                    BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelMapTypeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                    return DRV_TDM_SDK_API_FAIL;
                }

                /* Get E1 and configure its framing type */
                pE1 = (AtPdhDe1)AtSdhChannelMapChannelGet((AtSdhChannel)pVc12); /* Get E1 */
                DRV_STM_CHECK_POINTER_IS_NULL(pE1);
                g_drv_tdm_atPdhE1Ptr[chipId][ucIntfIdx][e1LinkId] = pE1;  /* save PDH E1 */
                rv = AtPdhChannelFrameTypeSet((AtPdhChannel)pE1, cAtPdhE1UnFrm); /* 默认为UNFRAME E1 */
                if (cAtOk != rv)
                {
                    BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtPdhChannelFrameTypeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                    return DRV_TDM_SDK_API_FAIL;
                }

                /* Set timing mode of E1 channel */
                rv = AtChannelTimingSet((AtChannel)pE1, cAtTimingModeSys, NULL);
                if (cAtOk != rv)
                {
                    BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtChannelTimingSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                    return DRV_TDM_SDK_API_FAIL;
                }

                /* Enable E1 link. */
                rv = AtChannelEnable((AtChannel)pE1, cAtTrue);
                if (cAtOk != rv)
                {
                    BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtChannelEnable() rv=0x%x.\n", __FILE__, __LINE__, rv);
                    return DRV_TDM_SDK_API_FAIL;
                }
            }
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stm4MappingSet
* 功能描述: 设置STM-4帧的映射方式
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           ucIntfIdx: STM-4接口的索引号,取值为0或者4.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-01-14   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stm4MappingSet(BYTE chipId, BYTE ucIntfIdx)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtDevice pDevice = NULL;
    AtModuleSdh pSdhModule = NULL;
    AtSdhLine pLine = NULL;
    AtSdhAug pAug4 = NULL;
    AtSdhAug pAug1 = NULL;
    AtSdhAu pAu4 = NULL;
    AtSdhVc pVc4 = NULL;
    AtSdhTug pTug3 = NULL;
    AtSdhTug pTug2 = NULL;
    AtSdhTu pTu12 = NULL;
    AtSdhVc pVc12 = NULL;
    AtPdhDe1 pE1 = NULL;
    BYTE aug1Id = 0;  /* ARRIVE芯片的AUG-1编号,芯片从0开始编号 */
    BYTE tug3Id = 0;  /* ARRIVE芯片的TUG3编号,芯片从0开始编号 */
    BYTE tug2Id = 0;  /* ARRIVE芯片的TUG2编号,芯片从0开始编号 */
    BYTE tu12Id = 0;  /* ARRIVE芯片的TU12编号,芯片从0开始编号 */
    BYTE ucPortId = 0;  /* STM-4端口的编号,取值为1或者5 */
    BYTE aug1No = 0;  /* 从1开始取值 */
    BYTE tug3No = 0;  /* 从1开始取值 */
    BYTE tug2No = 0;  /* 从1开始取值 */
    BYTE tu12No = 0;  /* 从1开始取值 */
    BYTE e1LinkId = 0;
    BYTE ucAug1Idx = 0;  /* 驱动内部的AUG1索引,仅仅用来作软件表的索引,取值为0~7 */
    
    DRV_TDM_CHECK_STM_INDEX(ucIntfIdx);
    ucPortId = ucIntfIdx + (BYTE)1;

    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_STM_CHECK_POINTER_IS_NULL(pDevice);

    /* Get SDH module */
    pSdhModule = (AtModuleSdh)AtDeviceModuleGet(pDevice, cAtModuleSdh);
    DRV_STM_CHECK_POINTER_IS_NULL(pSdhModule);
    
    /* Get line object */
    pLine = AtModuleSdhLineGet(pSdhModule, ucIntfIdx);
    DRV_STM_CHECK_POINTER_IS_NULL(pLine);

    /* AUG-4 maps 4xAUG-1s */
    pAug4 = AtSdhLineAug4Get(pLine, 0);  /* Get AUG-4 */
    DRV_STM_CHECK_POINTER_IS_NULL(pAug4);
    g_drv_tdm_atSdhAug4Ptr[chipId][ucIntfIdx] = pAug4;
    rv = AtSdhChannelMapTypeSet((AtSdhChannel)pAug4, cAtSdhAugMapTypeAug4Map4xAug1s);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelMapTypeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }

    for (aug1Id = 0; aug1Id < DRV_TDM_MAX_AUG1_ID; aug1Id++)
    {
        aug1No = aug1Id + (BYTE)1;
        rv = drv_tdm_aug1IndexGet(ucPortId, aug1No, &ucAug1Idx);
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_aug1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        DRV_TDM_CHECK_AUG1_INDEX(ucAug1Idx);
        /* Map desired AUG-1 to VC-4 */
        pAug1 = AtSdhLineAug1Get(pLine, aug1Id); /* Get AUG-1 */
        DRV_STM_CHECK_POINTER_IS_NULL(pAug1);
        g_drv_tdm_atSdhAug1Ptr[chipId][ucAug1Idx] = pAug1;
        rv = AtSdhChannelMapTypeSet((AtSdhChannel)pAug1, cAtSdhAugMapTypeAug1MapVc4);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelMapTypeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
        
        /* Get AU-4 */
        pAu4 = AtSdhLineAu4Get(pLine, aug1Id);
        DRV_STM_CHECK_POINTER_IS_NULL(pAu4);
        g_drv_tdm_atSdhAu4Ptr[chipId][ucAug1Idx] = pAu4;
        
        /* Map VC-4 to 3xTUG-3 */
        pVc4 = AtSdhLineVc4Get(pLine, aug1Id); /* Get VC-4 */
        DRV_STM_CHECK_POINTER_IS_NULL(pVc4);
        g_drv_tdm_atSdhVc4Ptr[chipId][ucAug1Idx] = pVc4; /* save VC4 pointer. */
        rv = AtSdhChannelMapTypeSet((AtSdhChannel)pVc4, cAtSdhVcMapTypeVc4Map3xTug3s);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelMapTypeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
        
        for (tug3Id = 0; tug3Id < DRV_TDM_MAX_TUG3_ID; tug3Id++)
        {
            tug3No = tug3Id + (BYTE)1;
            /* Map TUG-3 to 7xTUG-2 */
            pTug3 = AtSdhLineTug3Get(pLine, aug1Id, tug3Id); /* Get TUG-3 */
            DRV_STM_CHECK_POINTER_IS_NULL(pTug3);
            rv = AtSdhChannelMapTypeSet((AtSdhChannel)pTug3, cAtSdhTugMapTypeTug3Map7xTug2s);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelMapTypeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            
            for (tug2Id = 0; tug2Id < DRV_TDM_MAX_TUG2_ID; tug2Id++)
            {
                tug2No = tug2Id + (BYTE)1;
                /* Map TUG-2 to 3xTU-12 */
                pTug2 = AtSdhLineTug2Get(pLine, aug1Id, tug3Id, tug2Id); /* Get TUG-2 */
                DRV_STM_CHECK_POINTER_IS_NULL(pTug2);
                rv = AtSdhChannelMapTypeSet((AtSdhChannel)pTug2, cAtSdhTugMapTypeTug2Map3xTu12s);
                if (cAtOk != rv)
                {
                    BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelMapTypeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                    return DRV_TDM_SDK_API_FAIL;
                }
                
                for (tu12Id = 0; tu12Id < DRV_TDM_MAX_TU12_ID; tu12Id++)
                {
                    tu12No = tu12Id + (BYTE)1;
                    rv = drv_tdm_e1LinkIdGet(tug3No, tug2No, tu12No, &e1LinkId);
                    if (DRV_TDM_OK != rv)
                    {
                        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_e1LinkIdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                        return rv;
                    }
                    
                    /* Get TU-12 */
                    pTu12 = AtSdhLineTu1xGet(pLine, aug1Id, tug3Id, tug2Id, tu12Id);
                    DRV_STM_CHECK_POINTER_IS_NULL(pTu12);
                    g_drv_tdm_atSdhTu12Ptr[chipId][ucAug1Idx][e1LinkId] = pTu12;
                    
                    /* Map TU-12 to VC-12 */
                    pVc12 = AtSdhLineVc1xGet(pLine, aug1Id, tug3Id, tug2Id, tu12Id); /* Get VC-12 */
                    DRV_STM_CHECK_POINTER_IS_NULL(pVc12);
                    g_drv_tdm_atSdhVc12Ptr[chipId][ucAug1Idx][e1LinkId] = pVc12; /* save SDH VC12. */
                    rv = AtSdhChannelMapTypeSet((AtSdhChannel)pVc12, cAtSdhVcMapTypeVc1xMapDe1);
                    if (cAtOk != rv)
                    {
                        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelMapTypeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                        return DRV_TDM_SDK_API_FAIL;
                    }
                    
                    /* Get E1 and configure its framing type */
                    pE1 = (AtPdhDe1)AtSdhChannelMapChannelGet((AtSdhChannel)pVc12); /* Get E1 */
                    DRV_STM_CHECK_POINTER_IS_NULL(pE1);
                    g_drv_tdm_atPdhE1Ptr[chipId][ucAug1Idx][e1LinkId] = pE1;  /* save PDH E1 */
                    rv = AtPdhChannelFrameTypeSet((AtPdhChannel)pE1, cAtPdhE1UnFrm); /* 默认为UNFRAME E1 */
                    if (cAtOk != rv)
                    {
                        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtPdhChannelFrameTypeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                        return DRV_TDM_SDK_API_FAIL;
                    }
                    
                    /* Set timing mode of E1 channel */
                    rv = AtChannelTimingSet((AtChannel)pE1, cAtTimingModeSys, NULL);
                    if (cAtOk != rv)
                    {
                        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtChannelTimingSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                        return DRV_TDM_SDK_API_FAIL;
                    }
                    
                    /* Enable E1 link. */
                    rv = AtChannelEnable((AtChannel)pE1, cAtTrue);
                    if (cAtOk != rv)
                    {
                        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtChannelEnable() rv=0x%x.\n", __FILE__, __LINE__, rv);
                        return DRV_TDM_SDK_API_FAIL;
                    }
                }
            }
        }
    }

    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atDevSectionOhInit
* 功能描述: 初始化STM接口的段开销
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           stmIntfIndex: STM接口的索引号,取值为0~7.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atDevSectionOhInit(BYTE chipId, BYTE stmIntfIndex)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE portId = 0;
    AtSdhLine pLine = NULL;
    BYTE j0Msg[DRV_TDM_TRACE_MSG_LEN_64] = {0};
    tAtSdhTti tti;

    portId = stmIntfIndex + (BYTE)1;
    memset(&tti, 0, sizeof(tAtSdhTti));
    
    /* Get SDH line. */
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_STM_CHECK_POINTER_IS_NULL(pLine);
    
    /* configure TTI, both transmitted and expected */
    AtSdhTtiMake(cAtSdhTtiMode16Byte, j0Msg, (DRV_TDM_TRACE_MSG_LEN_16 - 1), &tti);
    rv = AtSdhChannelTxTtiSet((AtSdhChannel)pLine, &tti);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelTxTtiSet() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    rv = AtSdhChannelExpectedTtiSet((AtSdhChannel)pLine, &tti);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelExpectedTtiSet() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    /* 默认时disable RS-TIM monitoring */
    rv = AtSdhChannelTimMonitorEnable((AtSdhChannel)pLine, cAtFalse);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelTimMonitorEnable() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    /* Disable AIS downstream and RDI to remote side. */
    rv = AtSdhChannelAlarmAffectingEnable((AtSdhChannel)pLine, cAtSdhLineAlarmTim, cAtFalse);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelAlarmAffectingEnable() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atDevHighPathOhInit
* 功能描述: 初始化STM接口的高阶通道开销
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           stmIntfIndex: STM接口的索引号,取值为0~7.
*           aug1Id: AUG1编号,取值为1~4.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atDevHighPathOhInit(BYTE chipId, BYTE stmIntfIndex, BYTE aug1Id)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE ucPortId = 0;  /* STM端口编号,取值为1~8. */
    AtSdhVc pVc4 = NULL;
    BYTE j1Msg[DRV_TDM_TRACE_MSG_LEN_64] = {0};
    tAtSdhTti tti;
    
    ucPortId = stmIntfIndex + (BYTE)1;
    memset(&tti, 0, sizeof(tAtSdhTti));
    
    rv = drv_tdm_atSdhVc4PtrGet(chipId, ucPortId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv; 
    }
    DRV_STM_CHECK_POINTER_IS_NULL(pVc4);
    
    /* configure J1, both transmitted and expected RX */
    AtSdhTtiMake(cAtSdhTtiMode16Byte, j1Msg, (DRV_TDM_TRACE_MSG_LEN_16 - 1), &tti);
    rv = AtSdhChannelTxTtiSet((AtSdhChannel)pVc4, &tti);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelTxTtiSet() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL; 
    }
    rv = AtSdhChannelExpectedTtiSet((AtSdhChannel)pVc4, &tti);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelExpectedTtiSet() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL; 
    }
    /* 默认时disable HP-TIM monitoring */
    rv = AtSdhChannelTimMonitorEnable((AtSdhChannel)pVc4, cAtFalse);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelTimMonitorEnable() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    /* Disable AIS downstream and RDI to remote side. */
    rv = AtSdhChannelAlarmAffectingEnable((AtSdhChannel)pVc4, cAtSdhPathAlarmTim, cAtFalse);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelAlarmAffectingEnable() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    
    /* 发送的C2默认设置为2 */
    rv = AtSdhPathTxPslSet((AtSdhPath)pVc4, DRV_TDM_DEFAULT_TX_C2);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhPathTxPslSet() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    /* 期望接收的C2默认设置为2 */
    rv = AtSdhPathExpectedPslSet((AtSdhPath)pVc4, DRV_TDM_DEFAULT_EXPECTED_RX_C2);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhPathExpectedPslSet() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    /*默认enable HP-PLM. */
    rv = AtSdhChannelAlarmAffectingEnable((AtSdhChannel)pVc4, cAtSdhPathAlarmPlm, cAtTrue);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelAlarmAffectingEnable() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    /*默认enable HP-UNEQ. */
    rv = AtSdhChannelAlarmAffectingEnable((AtSdhChannel)pVc4, cAtSdhPathAlarmUneq, cAtTrue);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelAlarmAffectingEnable() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    /* 默认Enable HP-RDI */
    rv = AtSdhPathERdiEnable((AtSdhPath)pVc4, cAtTrue);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhPathERdiEnable() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atDevLowPathOhInit
* 功能描述: 初始化STM接口的低阶通道开销
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           stmIntfIndex: STM接口的索引,取值为0~7.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atDevLowPathOhInit(BYTE chipId, 
                                             BYTE stmIntfIndex, 
                                             BYTE aug1Id, 
                                             BYTE e1LinkId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE ucPortId = 0;  /* STM端口编号,取值为1~8. */
    AtSdhVc pVc12 = NULL;
    BYTE j2Msg[DRV_TDM_TRACE_MSG_LEN_64] = {0};
    tAtSdhTti tti;
    
    ucPortId = stmIntfIndex + (BYTE)1;
    memset(&tti, 0, sizeof(tAtSdhTti));
    
    rv = drv_tdm_atSdhVc12PtrGet(chipId, ucPortId, aug1Id, e1LinkId, &pVc12);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=%s.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_STM_CHECK_POINTER_IS_NULL(pVc12);
    
    /* configure J2, both transmitted and expected RX */
    AtSdhTtiMake(cAtSdhTtiMode16Byte, j2Msg, (DRV_TDM_TRACE_MSG_LEN_16 - 1), &tti);
    rv = AtSdhChannelTxTtiSet((AtSdhChannel)pVc12, &tti);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelTxTtiSet() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    rv = AtSdhChannelExpectedTtiSet((AtSdhChannel)pVc12, &tti);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelExpectedTtiSet() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    /* 默认时disable LP-TIM monitoring */
    rv = AtSdhChannelTimMonitorEnable((AtSdhChannel)pVc12, cAtFalse);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelTimMonitorEnable() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    /* Disable AIS downstream and RDI to remote side. */
    rv = AtSdhChannelAlarmAffectingEnable((AtSdhChannel)pVc12, cAtSdhPathAlarmTim, cAtFalse);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelAlarmAffectingEnable() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }

    /* 发送的V5默认设置为2 */
    rv = AtSdhPathTxPslSet((AtSdhPath)pVc12, DRV_TDM_DEFAULT_TX_V5);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhPathTxPslSet() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    /* 期望接收的V5默认设置为2 */
    rv = AtSdhPathExpectedPslSet((AtSdhPath)pVc12, DRV_TDM_DEFAULT_EXPECTED_RX_V5);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhPathExpectedPslSet() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    /*默认enable LP-PLM. */
    rv = AtSdhChannelAlarmAffectingEnable((AtSdhChannel)pVc12, cAtSdhPathAlarmPlm, cAtTrue);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelAlarmAffectingEnable() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    /*默认enable LP-UNEQ. */
    rv = AtSdhChannelAlarmAffectingEnable((AtSdhChannel)pVc12, cAtSdhPathAlarmUneq, cAtTrue);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelAlarmAffectingEnable() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    /* 默认Enable LP-RDI */
    rv = AtSdhPathERdiEnable((AtSdhPath)pVc12, cAtTrue);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhPathERdiEnable() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stm1OverheadInit
* 功能描述: 初始化STM-1接口的开销overhead
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           stmIntfIndex: STM-1接口的索引号,取值为0~7.
* 输出参数:  
* 返 回 值: 
* 其它说明: 包括段开销,高阶通道开销,低阶通道开销
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stm1OverheadInit(BYTE chipId, BYTE stmIntfIndex)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE e1LinkId = 0;   /* E1链路的编号 */
    
    /* 初始化STM1接口的段开销 */
    rv = drv_tdm_atDevSectionOhInit(chipId, stmIntfIndex);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u stmIntfIndex %u, drv_tdm_atDevSectionOhInit() rv=0x%x.\n", __FILE__, __LINE__, chipId, stmIntfIndex, rv);
        return rv;
    }
    
    /* 初始化STM1接口的高阶通道开销 */
    rv = drv_tdm_atDevHighPathOhInit(chipId, stmIntfIndex, DRV_TDM_STM1_AUG1_ID);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u stm1IntfIndex %u, drv_tdm_atDevHighPathOhInit() rv=0x%x.\n", __FILE__, __LINE__, chipId, stmIntfIndex, rv);
        return rv;
    }
    
    for (e1LinkId = DRV_TDM_E1_LINK_ID_START; e1LinkId <= DRV_TDM_E1_LINK_ID_END; e1LinkId++)
    {
        /* 初始化STM1接口的低阶通道开销 */
        rv = drv_tdm_atDevLowPathOhInit(chipId, stmIntfIndex, DRV_TDM_STM1_AUG1_ID, e1LinkId);
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u stmIntfIndex %u e1LinkId %u, drv_tdm_atDevLowPathOhInit() rv=0x%x.\n", __FILE__, __LINE__, chipId, stmIntfIndex, e1LinkId, rv);
            return rv;
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stm4OverheadInit
* 功能描述: 初始化STM-4接口的开销overhead
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           stmIntfIndex: STM-4接口的索引号,取值为0或者4.
* 输出参数:  
* 返 回 值: 
* 其它说明: 包括段开销,高阶通道开销,低阶通道开销
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-01-15   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stm4OverheadInit(BYTE chipId, BYTE stmIntfIndex)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE aug1Id = 0;  /* AUG1编号 */
    BYTE e1LinkId = 0;   /* E1链路的编号 */
    
    /* 初始化STM-4接口的段开销 */
    rv = drv_tdm_atDevSectionOhInit(chipId, stmIntfIndex);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u stmIntfIndex %u, drv_tdm_atDevSectionOhInit() rv=0x%x.\n", __FILE__, __LINE__, chipId, stmIntfIndex, rv);
        return rv;
    }
    
    for (aug1Id = DRV_TDM_MIN_AUG1_ID; aug1Id <= DRV_TDM_MAX_AUG1_ID; aug1Id++)
    {
        /* 初始化STM-4接口的高阶通道开销 */
        rv = drv_tdm_atDevHighPathOhInit(chipId, stmIntfIndex, aug1Id);
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u stm1IntfIndex %u aug1Id %u, drv_tdm_atDevHighPathOhInit() rv=0x%x.\n", __FILE__, __LINE__, chipId, stmIntfIndex, aug1Id, rv);
            return rv;
        }
        for (e1LinkId = DRV_TDM_E1_LINK_ID_START; e1LinkId <= DRV_TDM_E1_LINK_ID_END; e1LinkId++)
        {
            /* 初始化STM-4接口的低阶通道开销 */
            rv = drv_tdm_atDevLowPathOhInit(chipId, stmIntfIndex, aug1Id, e1LinkId);
            if (DRV_TDM_OK != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u stmIntfIndex %u aug1Id %u e1LinkId %u, drv_tdm_atDevLowPathOhInit() rv=0x%x.\n", __FILE__, __LINE__, chipId, stmIntfIndex, aug1Id, e1LinkId, rv);
                return rv;
            }
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stm1BerInit
* 功能描述: 初始化STM-1接口的Bit Error Rate
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           stm1IntfIndex: STM-1接口的索引号,取值为0~7.
* 输出参数:  
* 返 回 值: 
* 其它说明: 包括RS,MS,VC4及VC12的BER
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stm1BerInit(BYTE chipId, BYTE stm1IntfIndex)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE portId = 0;    /* STM-1端口编号,取值为1~8 */
    BYTE e1LinkId = 0;      /* E1链路编号,取值为1~63 */
    AtSdhLine pLine = NULL;
    AtSdhAu pAu4 = NULL;
    AtSdhVc pVc4 = NULL;
    AtSdhTu pTu12 = NULL;
    AtSdhVc pVc12 = NULL;
    AtPdhDe1 pE1 = NULL;
    AtBerController pBerCtrl = NULL;
    
    portId = stm1IntfIndex + (BYTE)1;
    
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_STM_CHECK_POINTER_IS_NULL(pLine);
    
    pBerCtrl = NULL;
    /* Get BER Monitoring controller of RS layer */
    pBerCtrl = AtSdhLineRsBerControllerGet(pLine);
    DRV_STM_CHECK_POINTER_IS_NULL(pBerCtrl);
    rv = AtBerControllerEnable(pBerCtrl, cAtTrue);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerEnable() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    /* Set threshold to report RS BER-SD alarm */
    rv = AtBerControllerSdThresholdSet(pBerCtrl, cAtBerRate1E5);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerSdThresholdSet() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    /* Set threshold to report RS BER-SF alarm */
    rv = AtBerControllerSfThresholdSet(pBerCtrl, cAtBerRate1E3);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerSfThresholdSet() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }

    pBerCtrl = NULL;
    /* Get BER Monitoring controller of MS layer.*/
    pBerCtrl = AtSdhLineMsBerControllerGet(pLine);
    DRV_STM_CHECK_POINTER_IS_NULL(pBerCtrl);
    rv = AtBerControllerEnable(pBerCtrl, cAtTrue);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerEnable() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    /* Set threshold to report MS BER-SD alarm */
    rv = AtBerControllerSdThresholdSet(pBerCtrl, cAtBerRate1E5);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerSdThresholdSet() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    /* Set threshold to report MS BER-SF alarm */
    rv = AtBerControllerSfThresholdSet(pBerCtrl, cAtBerRate1E3);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerSfThresholdSet() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }

    /* 在初始化时需要清除一下SDH性能统计计数 */
    AtSdhChannelBlockErrorCounterClear((AtSdhChannel)pLine, cAtSdhLineCounterTypeB1);
    AtSdhChannelBlockErrorCounterClear((AtSdhChannel)pLine, cAtSdhLineCounterTypeB2);
    AtSdhChannelBlockErrorCounterClear((AtSdhChannel)pLine, cAtSdhLineCounterTypeRei);

    rv = drv_tdm_atSdhAu4PtrGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, &pAu4);  /* Get AU4 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhAu4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_STM_CHECK_POINTER_IS_NULL(pAu4);    
    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, &pVc4);  /* Get VC-4 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_STM_CHECK_POINTER_IS_NULL(pVc4);
    /* Get BER controller that monitors BER for high path channel.*/
    pBerCtrl = NULL;   /* 先清零再赋值 */
    pBerCtrl = AtSdhChannelBerControllerGet((AtSdhChannel)pVc4);
    DRV_STM_CHECK_POINTER_IS_NULL(pBerCtrl);
    rv = AtBerControllerEnable(pBerCtrl, cAtTrue);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerEnable() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    /* Set threshold to report VC4 BER-SD alarm */
    rv = AtBerControllerSdThresholdSet(pBerCtrl, cAtBerRate1E5);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerSdThresholdSet() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    /* Set threshold to report VC4 BER-SF alarm */
    rv = AtBerControllerSfThresholdSet(pBerCtrl, cAtBerRate1E3);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerSfThresholdSet() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }

    /* 在初始化时清除SDH性能统计计数 */
    AtSdhChannelBlockErrorCounterClear((AtSdhChannel)pVc4, cAtSdhPathCounterTypeBip);
    AtSdhChannelBlockErrorCounterClear((AtSdhChannel)pVc4, cAtSdhPathCounterTypeRei);
    AtChannelCounterClear((AtChannel)pAu4, cAtSdhPathCounterTypeRxPPJC);
    AtChannelCounterClear((AtChannel)pAu4, cAtSdhPathCounterTypeRxNPJC);
    AtChannelCounterClear((AtChannel)pAu4, cAtSdhPathCounterTypeTxPPJC);
    AtChannelCounterClear((AtChannel)pAu4, cAtSdhPathCounterTypeTxNPJC);
    
    for (e1LinkId = DRV_TDM_E1_LINK_ID_START; e1LinkId <= DRV_TDM_E1_LINK_ID_END; e1LinkId++)
    {
        pTu12 = NULL;  /* 先清零再赋值 */
        rv = drv_tdm_atSdhTu12PtrGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, &pTu12); /* Get TU12 */
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhTu12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        DRV_STM_CHECK_POINTER_IS_NULL(pTu12);
        pVc12 = NULL;  /* 先清零再赋值 */
        rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, &pVc12);  /* Get VC-12 */ 
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        DRV_STM_CHECK_POINTER_IS_NULL(pVc12);
        pE1 = NULL;  /* 先清零再赋值 */
        rv = drv_tdm_atPdhE1PtrGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, &pE1); /* Get E1 */
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atPdhE1PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        DRV_STM_CHECK_POINTER_IS_NULL(pE1);

        /* Get BER controller that monitors BER for low path channel.*/
        pBerCtrl = NULL;  /* 先清零再赋值 */
        pBerCtrl = AtSdhChannelBerControllerGet((AtSdhChannel)pVc12);
        DRV_STM_CHECK_POINTER_IS_NULL(pBerCtrl);
        rv = AtBerControllerEnable(pBerCtrl, cAtTrue);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerEnable() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
            return DRV_TDM_SDK_API_FAIL;
        }
        /* Set threshold to report VC12 BER-SD alarm */
        rv = AtBerControllerSdThresholdSet(pBerCtrl, cAtBerRate1E5);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerSdThresholdSet() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
            return DRV_TDM_SDK_API_FAIL;
        }
        /* Set threshold to report VC12 BER-SF alarm */
        rv = AtBerControllerSfThresholdSet(pBerCtrl, cAtBerRate1E3);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerSfThresholdSet() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
            return DRV_TDM_SDK_API_FAIL;
        }

        /* 在初始化时清除一下SDH性能统计计数 */
        AtSdhChannelBlockErrorCounterClear((AtSdhChannel)pVc12, cAtSdhPathCounterTypeBip);
        AtSdhChannelBlockErrorCounterClear((AtSdhChannel)pVc12, cAtSdhPathCounterTypeRei);
        AtChannelCounterClear((AtChannel)pTu12, cAtSdhPathCounterTypeRxPPJC);
        AtChannelCounterClear((AtChannel)pTu12, cAtSdhPathCounterTypeRxNPJC);
        AtChannelCounterClear((AtChannel)pTu12, cAtSdhPathCounterTypeTxPPJC);
        AtChannelCounterClear((AtChannel)pTu12, cAtSdhPathCounterTypeTxNPJC);
        AtChannelCounterClear((AtChannel)pE1, cAtPdhDe1CounterCrc);
        AtChannelCounterClear((AtChannel)pE1, cAtPdhDe1CounterFe);
        AtChannelCounterClear((AtChannel)pE1, cAtPdhDe1CounterRei);
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stm4BerInit
* 功能描述: 初始化STM-4接口的Bit Error Rate
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           stmIntfIndex: STM-4接口的索引号,取值为0或者4.
* 输出参数:  
* 返 回 值: 
* 其它说明: 包括RS,MS,VC4及VC12的BER
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-01-15   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stm4BerInit(BYTE chipId, BYTE stmIntfIndex)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE portId = 0;    /* STM-4端口编号,取值为1或者5 */
    BYTE aug1Id = 0;    /* AUG1编号,取值为1~4. */
    BYTE e1LinkId = 0;      /* E1链路编号,取值为1~63 */
    AtSdhLine pLine = NULL;
    AtSdhAu pAu4 = NULL;
    AtSdhVc pVc4 = NULL;
    AtSdhTu pTu12 = NULL;
    AtSdhVc pVc12 = NULL;
    AtPdhDe1 pE1 = NULL;
    AtBerController pBerCtrl = NULL;
    
    portId = stmIntfIndex + (BYTE)1;
    
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_STM_CHECK_POINTER_IS_NULL(pLine);
    
    pBerCtrl = NULL;
    /* Get BER Monitoring controller of RS layer */
    pBerCtrl = AtSdhLineRsBerControllerGet(pLine);
    DRV_STM_CHECK_POINTER_IS_NULL(pBerCtrl);
    rv = AtBerControllerEnable(pBerCtrl, cAtTrue);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerEnable() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    /* Set threshold to report RS BER-SD alarm */
    rv = AtBerControllerSdThresholdSet(pBerCtrl, cAtBerRate1E5);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerSdThresholdSet() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    /* Set threshold to report RS BER-SF alarm */
    rv = AtBerControllerSfThresholdSet(pBerCtrl, cAtBerRate1E3);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerSfThresholdSet() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }

    pBerCtrl = NULL;
    /* Get BER Monitoring controller of MS layer.*/
    pBerCtrl = AtSdhLineMsBerControllerGet(pLine);
    DRV_STM_CHECK_POINTER_IS_NULL(pBerCtrl);
    rv = AtBerControllerEnable(pBerCtrl, cAtTrue);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerEnable() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    /* Set threshold to report MS BER-SD alarm */
    rv = AtBerControllerSdThresholdSet(pBerCtrl, cAtBerRate1E5);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerSdThresholdSet() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    /* Set threshold to report MS BER-SF alarm */
    rv = AtBerControllerSfThresholdSet(pBerCtrl, cAtBerRate1E3);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerSfThresholdSet() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }

    /* 在初始化时需要清除一下SDH性能统计计数 */
    AtSdhChannelBlockErrorCounterClear((AtSdhChannel)pLine, cAtSdhLineCounterTypeB1);
    AtSdhChannelBlockErrorCounterClear((AtSdhChannel)pLine, cAtSdhLineCounterTypeB2);
    AtSdhChannelBlockErrorCounterClear((AtSdhChannel)pLine, cAtSdhLineCounterTypeRei);

    for (aug1Id = DRV_TDM_MIN_AUG1_ID; aug1Id <= DRV_TDM_MAX_AUG1_ID; aug1Id++)
    {
        pAu4 = NULL;  /* 先清零再赋值 */
        rv = drv_tdm_atSdhAu4PtrGet(chipId, portId, aug1Id, &pAu4);  /* Get AU4 */
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhAu4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        DRV_STM_CHECK_POINTER_IS_NULL(pAu4); 
        rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);  /* Get VC-4 */
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, aug1Id, rv);
            return rv;
        }
        DRV_STM_CHECK_POINTER_IS_NULL(pVc4);
        /* Get BER controller that monitors BER for high path channel.*/
        pBerCtrl = NULL;   /* 先清零再赋值 */
        pBerCtrl = AtSdhChannelBerControllerGet((AtSdhChannel)pVc4);
        DRV_STM_CHECK_POINTER_IS_NULL(pBerCtrl);
        rv = AtBerControllerEnable(pBerCtrl, cAtTrue);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u, AtBerControllerEnable() rv=%s.\n", __FILE__, __LINE__, chipId, portId, aug1Id, AtRet2String(rv));
            return DRV_TDM_SDK_API_FAIL;
        }
        /* Set threshold to report VC4 BER-SD alarm */
        rv = AtBerControllerSdThresholdSet(pBerCtrl, cAtBerRate1E5);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u, AtBerControllerSdThresholdSet() rv=%s.\n", __FILE__, __LINE__, chipId, portId, aug1Id, AtRet2String(rv));
            return DRV_TDM_SDK_API_FAIL;
        }
        /* Set threshold to report VC4 BER-SF alarm */
        rv = AtBerControllerSfThresholdSet(pBerCtrl, cAtBerRate1E3);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u, AtBerControllerSfThresholdSet() rv=%s.\n", __FILE__, __LINE__, chipId, portId, aug1Id, AtRet2String(rv));
            return DRV_TDM_SDK_API_FAIL;
        }

        /* 在初始化时清除SDH性能统计计数 */
        AtSdhChannelBlockErrorCounterClear((AtSdhChannel)pVc4, cAtSdhPathCounterTypeBip);
        AtSdhChannelBlockErrorCounterClear((AtSdhChannel)pVc4, cAtSdhPathCounterTypeRei);
        AtChannelCounterClear((AtChannel)pAu4, cAtSdhPathCounterTypeRxPPJC);
        AtChannelCounterClear((AtChannel)pAu4, cAtSdhPathCounterTypeRxNPJC);
        AtChannelCounterClear((AtChannel)pAu4, cAtSdhPathCounterTypeTxPPJC);
        AtChannelCounterClear((AtChannel)pAu4, cAtSdhPathCounterTypeTxNPJC);
        
        for (e1LinkId = DRV_TDM_E1_LINK_ID_START; e1LinkId <= DRV_TDM_E1_LINK_ID_END; e1LinkId++)
        {
            pTu12 = NULL;  /* 先清零再赋值 */
            rv = drv_tdm_atSdhTu12PtrGet(chipId, portId, aug1Id, e1LinkId, &pTu12); /* Get TU12 */
            if (DRV_TDM_OK != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhTu12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                return rv;
            }
            DRV_STM_CHECK_POINTER_IS_NULL(pTu12);
            pVc12 = NULL;  /* 先清零再赋值 */
            rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12);  /* Get VC-12 */
            if (DRV_TDM_OK != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u e1LinkId %u, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, aug1Id, e1LinkId, rv);
                return rv;
            }
            DRV_STM_CHECK_POINTER_IS_NULL(pVc12);
            pE1 = NULL;  /* 先清零再赋值 */
            rv = drv_tdm_atPdhE1PtrGet(chipId, portId, aug1Id, e1LinkId, &pE1); /* Get E1 */
            if (DRV_TDM_OK != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atPdhE1PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                return rv;
            }
            DRV_STM_CHECK_POINTER_IS_NULL(pE1);
            /* Get BER controller that monitors BER for low path channel.*/
            pBerCtrl = NULL;  /* 先清零再赋值 */
            pBerCtrl = AtSdhChannelBerControllerGet((AtSdhChannel)pVc12);
            DRV_STM_CHECK_POINTER_IS_NULL(pBerCtrl);
            rv = AtBerControllerEnable(pBerCtrl, cAtTrue);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u e1LinkId %u, AtBerControllerEnable() rv=%s.\n", __FILE__, __LINE__, chipId, portId, aug1Id, e1LinkId, AtRet2String(rv));
                return DRV_TDM_SDK_API_FAIL;
            }
            /* Set threshold to report VC12 BER-SD alarm */
            rv = AtBerControllerSdThresholdSet(pBerCtrl, cAtBerRate1E5);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u e1LinkId %u, AtBerControllerSdThresholdSet() rv=%s.\n", __FILE__, __LINE__, chipId, portId, aug1Id, e1LinkId, AtRet2String(rv));
                return DRV_TDM_SDK_API_FAIL;
            }
            /* Set threshold to report VC12 BER-SF alarm */
            rv = AtBerControllerSfThresholdSet(pBerCtrl, cAtBerRate1E3);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u e1LinkId %u, AtBerControllerSfThresholdSet() rv=%s.\n", __FILE__, __LINE__, chipId, portId, aug1Id, e1LinkId, AtRet2String(rv));
                return DRV_TDM_SDK_API_FAIL;
            }

            /* 在初始化时清除一下SDH性能统计计数 */
            AtSdhChannelBlockErrorCounterClear((AtSdhChannel)pVc12, cAtSdhPathCounterTypeBip);
            AtSdhChannelBlockErrorCounterClear((AtSdhChannel)pVc12, cAtSdhPathCounterTypeRei);
            AtChannelCounterClear((AtChannel)pTu12, cAtSdhPathCounterTypeRxPPJC);
            AtChannelCounterClear((AtChannel)pTu12, cAtSdhPathCounterTypeRxNPJC);
            AtChannelCounterClear((AtChannel)pTu12, cAtSdhPathCounterTypeTxPPJC);
            AtChannelCounterClear((AtChannel)pTu12, cAtSdhPathCounterTypeTxNPJC);
            AtChannelCounterClear((AtChannel)pE1, cAtPdhDe1CounterCrc);
            AtChannelCounterClear((AtChannel)pE1, cAtPdhDe1CounterFe);
            AtChannelCounterClear((AtChannel)pE1, cAtPdhDe1CounterRei);
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stm1IntfInit
* 功能描述: 初始化STM-1接口
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           stm1IntfIndex: STM-1接口的索引号,取值为0~7.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stm1IntfInit(BYTE chipId, BYTE stm1IntfIndex)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE ucPortId = 0;    /* STM-1端口编号,取值为1~8. */
    AtDevice pDevice = NULL;
    AtModuleSdh pSdhModule = NULL;
    AtSdhLine pLine = NULL;
    AtSerdesController pSerdesController = NULL;
    
    DRV_TDM_CHECK_STM_INDEX(stm1IntfIndex);
    ucPortId = stm1IntfIndex + (BYTE)1;
    
    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_STM_CHECK_POINTER_IS_NULL(pDevice);
    
    /* get SDH module */
    pSdhModule = (AtModuleSdh)AtDeviceModuleGet(pDevice, cAtModuleSdh);
    DRV_STM_CHECK_POINTER_IS_NULL(pSdhModule);
    
    /* Get line object and configure its rate to STM-1 */
    pLine = AtModuleSdhLineGet(pSdhModule, stm1IntfIndex);
    DRV_STM_CHECK_POINTER_IS_NULL(pLine);
    g_drv_tdm_atSdhLinePtr[chipId][stm1IntfIndex] = pLine; /* save SDH line */

    pSerdesController = AtSdhLineSerdesController(pLine);
    DRV_STM_CHECK_POINTER_IS_NULL(pSerdesController);
    
    /* Set line mode to SDH */
    rv = AtSdhLineModeSet(pLine, cAtSdhLineModeSdh);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhLineModeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    rv = AtSdhLineRateSet(pLine, cAtSdhLineRateStm1); /* set STM-1 frame rate */
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhLineRateSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    rv = drv_tdm_stmIntfModeSet(chipId, ucPortId, DRV_TDM_STM1_INTF);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmIntfModeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    /* 设置STM-1帧的映射方式 */
    rv = drv_tdm_stm1MappingSet(chipId, stm1IntfIndex);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u stm1IntfIndex %u, drv_tdm_stm1MappingSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, stm1IntfIndex, rv);
        return rv;
    }

    /* 初始化时Enable scramble on TX direction */
    rv = AtSdhLineScrambleEnable(pLine, cAtTrue);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhLineScrambleEnable() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }

    /* 初始化STM-1接口的开销overhead */
    rv = drv_tdm_stm1OverheadInit(chipId, stm1IntfIndex);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u stm1IntfIndex %u, drv_tdm_stm1OverheadInit() rv=0x%x.\n", __FILE__, __LINE__, chipId, stm1IntfIndex, rv);
        return rv;
    }

    /* 初始化STM-1接口的Bit Error Rate */
    rv = drv_tdm_stm1BerInit(chipId, stm1IntfIndex);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u stm1IntfIndex %u, drv_tdm_stm1BerInit() rv=0x%x.\n", __FILE__, __LINE__, chipId, stm1IntfIndex, rv);
        return rv;
    }

    /* 初始化时disable tx laser of STM-1 interface */
    rv = AtSerdesControllerEnable(pSerdesController, cAtFalse);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSerdesControllerEnable() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }

    /* 初始化时Force RX RS-LOS alarm */
    rv = AtChannelRxAlarmForce((AtChannel)pLine, cAtSdhLineAlarmLos);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelRxAlarmForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, ucPortId, rv);
        return DRV_TDM_SDK_API_FAIL;
    }

    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stm4IntfInit
* 功能描述: 初始化STM-4接口
* 访问的表: 无
* 修改的表: 无
* 输入参数: ucChipId: ARRIVE芯片编号,取值为0~3.
*           ucPortId: STM-4端口编号,取值为1或者5.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-01-14   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stm4IntfInit(BYTE ucChipId, BYTE ucPortId)
{
    WORD32 dwRetVal = DRV_TDM_OK;  /* 函数返回码 */
    BYTE ucIntfIdx = 0;    /* STM端口索引,取值为0或者4 */
    BYTE ucPortIdx = 0;    /* STM端口索引,取值为0~7 */
    BYTE ucE1LinkId = 0;
    AtDevice pDevice = NULL;
    AtModuleSdh pSdhModule = NULL;
    AtSdhLine pLine = NULL;
    AtSerdesController pSerdesController = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(ucChipId);  
    dwRetVal = drv_tdm_stm4PortIdCheck(ucChipId, ucPortId);
    if (DRV_TDM_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stm4PortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, dwRetVal);
        return dwRetVal;
    }
    ucIntfIdx = ucPortId - (BYTE)1;
    DRV_TDM_CHECK_STM_INDEX(ucIntfIdx);
    
    /* 在初始化STM-4接口之前,必须清空下面的软件表 */
    for (ucPortIdx = ucIntfIdx; ucPortIdx < (ucIntfIdx + 4); ucPortIdx++) 
    {
        g_drv_tdm_stm_intf_mode[ucChipId][ucPortIdx] = DRV_TDM_INVALID_STM_INTF;
        g_drv_tdm_atSdhLinePtr[ucChipId][ucPortIdx] = NULL;
        g_drv_tdm_atSdhAug1Ptr[ucChipId][ucPortIdx] = NULL;
        g_drv_tdm_atSdhAu4Ptr[ucChipId][ucPortIdx] = NULL;
        g_drv_tdm_atSdhVc4Ptr[ucChipId][ucPortIdx] = NULL;
        for (ucE1LinkId = DRV_TDM_E1_LINK_ID_START; ucE1LinkId <= DRV_TDM_E1_LINK_ID_END; ucE1LinkId++)
        {
            g_drv_tdm_atSdhTu12Ptr[ucChipId][ucPortIdx][ucE1LinkId] = NULL;
            g_drv_tdm_atSdhVc12Ptr[ucChipId][ucPortIdx][ucE1LinkId] = NULL;
            g_drv_tdm_atPdhE1Ptr[ucChipId][ucPortIdx][ucE1LinkId] = NULL;
        }
    }
    
    dwRetVal = drv_tdm_atDevicePtrGet(ucChipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, dwRetVal);
        return dwRetVal;
    }
    DRV_STM_CHECK_POINTER_IS_NULL(pDevice);
    
    /* get SDH module */
    pSdhModule = (AtModuleSdh)AtDeviceModuleGet(pDevice, cAtModuleSdh);
    DRV_STM_CHECK_POINTER_IS_NULL(pSdhModule);
    
    /* Get line object and configure its rate to STM-4 */
    pLine = AtModuleSdhLineGet(pSdhModule, ucIntfIdx);
    DRV_STM_CHECK_POINTER_IS_NULL(pLine);
    g_drv_tdm_atSdhLinePtr[ucChipId][ucIntfIdx] = pLine; /* save SDH line */
    
    pSerdesController = AtSdhLineSerdesController(pLine);
    DRV_STM_CHECK_POINTER_IS_NULL(pSerdesController);
    
    /* Set line mode to SDH */
    dwRetVal = AtSdhLineModeSet(pLine, cAtSdhLineModeSdh);
    if (cAtOk != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhLineModeSet() rv=0x%x.\n", __FILE__, __LINE__, dwRetVal);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    dwRetVal = AtSdhLineRateSet(pLine, cAtSdhLineRateStm4); /* set STM-4 frame rate */
    if (cAtOk != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhLineRateSet() rv=0x%x.\n", __FILE__, __LINE__, dwRetVal);
        return DRV_TDM_SDK_API_FAIL;
    }
    dwRetVal = drv_tdm_stmIntfModeSet(ucChipId, ucPortId, DRV_TDM_STM4_INTF);
    if (DRV_TDM_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmIntfModeSet() rv=0x%x.\n", __FILE__, __LINE__, dwRetVal);
        return dwRetVal;
    }
    
    /* 设置STM-4接口的STM-4帧的映射方式 */
    dwRetVal = drv_tdm_stm4MappingSet(ucChipId, ucIntfIdx);
    if (DRV_TDM_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u stmIntfIndex %u, drv_tdm_stm4MappingSet() rv=0x%x.\n", __FILE__, __LINE__, ucChipId, ucIntfIdx, dwRetVal);
        return dwRetVal;
    }
    
    /* 初始化时Enable scramble on TX direction */
    dwRetVal = AtSdhLineScrambleEnable(pLine, cAtTrue);
    if (cAtOk != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhLineScrambleEnable() rv=0x%x.\n", __FILE__, __LINE__, dwRetVal);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    /* 初始化STM-4接口的开销overhead */
    dwRetVal = drv_tdm_stm4OverheadInit(ucChipId, ucIntfIdx);
    if (DRV_TDM_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u stm4IntfIndex %u, drv_tdm_stm4OverheadInit() rv=0x%x.\n", __FILE__, __LINE__, ucChipId, ucIntfIdx, dwRetVal);
        return dwRetVal;
    }

    /* 初始化STM-4接口的Bit Error Rate */
    dwRetVal = drv_tdm_stm4BerInit(ucChipId, ucIntfIdx);
    if (DRV_TDM_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u stm4IntfIndex %u, drv_tdm_stm4BerInit() rv=0x%x.\n", __FILE__, __LINE__, ucChipId, ucIntfIdx, dwRetVal);
        return dwRetVal;
    }
    
    /* 初始化时disable tx laser of STM-4 interface */
    dwRetVal = AtSerdesControllerEnable(pSerdesController, cAtFalse);
    if (cAtOk != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSerdesControllerEnable() rv=0x%x.\n", __FILE__, __LINE__, dwRetVal);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_phyIntfInit
* 功能描述: 初始化FPGA芯片的物理接口
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
* 输出参数:  
* 返 回 值: 
* 其它说明: 包括初始化芯片的STM接口和Ethernet接口,在初始化FPGA时,按照STM-1来初始化STM接口.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_phyIntfInit(BYTE chipId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE subslotId = 0;  /* 单板的子槽位号 */
    BYTE portId = 0;    /* STM端口编号 */
    BYTE maxPortId = 0;  /* STM端口的最大编号 */
    BYTE stmIntfIndex = 0; /* ARRIVE芯片的STM接口的索引号. */
    BYTE ethIntfIndex = 0;  /* ARRIVE芯片的Ethernet接口的索引号. */

    subslotId = chipId + (BYTE)1;

    rv = drv_tdm_maxStmPortIdGet(subslotId, &maxPortId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, drv_tdm_maxStmPortIdGet() rv=0x%x\n", __FILE__, __LINE__, subslotId, rv);
        return rv;
    }
    
    /* 初始化ARRIVE芯片的STM-1接口 */
    for (portId = DRV_TDM_STM_PORT_ID_START; portId <= maxPortId; portId++)
    {
        stmIntfIndex = portId - (BYTE)1;;
        rv = drv_tdm_stm1IntfInit(chipId, stmIntfIndex);
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u stmIntfIndex %u, drv_tdm_stm1IntfInit() rv=0x%x.\n", __FILE__, __LINE__, chipId, stmIntfIndex, rv);
            return rv;
        }
        rv = drv_tdm_rxStmChannelStateSet(chipId, portId, DRV_TDM_SDH_CHANNEL_ENABLE);
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_rxStmChannelStateSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
            return rv;
        }
    }
    
    /* 初始化ARRIVE芯片的Ethernet接口 */
    for (ethIntfIndex = DRV_TDM_CHIP_ETH_INTF_ID_START; ethIntfIndex <= DRV_TDM_CHIP_ETH_INTF_ID_END; ethIntfIndex++)
    {
        rv = drv_tdm_ethIntfInit(chipId, ethIntfIndex);
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u ethIntfIndex %u, drv_tdm_ethIntfInit() rv=0x%x.\n", __FILE__, __LINE__, chipId, ethIntfIndex, rv);
            return rv;
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atDeviceInitialize
* 功能描述: Initialize FPGA.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3. 
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atDeviceInitialize(BYTE chipId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 systemType = 0;   /* ZXUPN15000的系统类型 */
    WORD32 dwSubslotId = 0;  /* 单板的子槽位号 */
    BYTE portId = 0;         /* STM端口的编号 */
    BYTE maxPortId = 0;      /* STM端口的最大编号 */
    BYTE maxNumberOfDevices = 0;  /* 线卡上的FPGA芯片的数量 */
    VOID* pFpgaBaseAddr = NULL;
    WORD32 fpgaBaseAddr = 0;     /* FPGA芯片的基地址 */
    WORD32 productCode = 0;
    BYTE coreId = 0;
    BYTE ipCoresNum = 0;
    BSP_CP3BAN_CHIP_BASE_ADDR *pMappingAddr = NULL;
    AtIpCore ipCore = NULL;
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);  /* 检查ARRIVE芯片的chipId的范围 */
    dwSubslotId = (WORD32)chipId + 1;

    systemType = BSP_cp3banSystemTypeGet();  /* 获取ZXUPN15000系统类型. */
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM == systemType)  /* 非15K-2系统 */
    {
        maxNumberOfDevices = DRV_TDM_MAX_CHIP_NUM_NONE_15K_2;
    }
    else  /* 15K-2系统 */
    {
        maxNumberOfDevices = DRV_TDM_MAX_CHIP_NUM_15K_2;
    }

    rv = drv_tdm_maxStmPortIdGet((BYTE)dwSubslotId, &maxPortId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, drv_tdm_maxStmPortIdGet() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }

    rv = drv_pwe3_initSemaphoreGet();  /* 获取信号量 */
    if (DRV_PWE3_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_pwe3_initSemaphoreGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    g_drv_tdm_atDevDriverPtr = AtDriverSharedDriverGet(); /* Get the singleton driver object */
    if (NULL == g_drv_tdm_atDevDriverPtr)  /* create ARRIVE driver */
    {
        g_drv_tdm_atDevDriverPtr = AtDriverCreate(maxNumberOfDevices, AtOsalLinux());
        if (NULL == g_drv_tdm_atDevDriverPtr)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId=%u, create driver unsuccessfully.\n", __FILE__, __LINE__, chipId);
            rv = drv_pwe3_initSemaphoreFree();  /* 释放信号量 */
            if (DRV_PWE3_OK != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_pwe3_initSemaphoreFree() rv=0x%x.\n", __FILE__, __LINE__, rv);
                return rv;
            }
            return DRV_TDM_SDK_API_FAIL;
        }
        BSP_Print(BSP_DEBUG_ALL, "%s line %d, chipId %u, pDriver = %p.\n", __FILE__, __LINE__, chipId, g_drv_tdm_atDevDriverPtr);
    }
    rv = drv_pwe3_initSemaphoreFree(); /* 释放信号量 */
    if (DRV_PWE3_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_pwe3_initSemaphoreFree() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    if (NULL == g_drv_tdm_atDevHalPtr[chipId])  /* Create HAL (Hardware Abstraction Layer). */
    {
        if (BSP_CP3BAN_NONE_15K_2_SYSTEM == systemType)  /* 非15K-2系统 */
        {
            g_drv_tdm_atDevHalPtr[chipId] = AtHalZtePtnEpldNew(dwSubslotId, drv_tdm_fpgaReg16Read, drv_tdm_fpgaReg16Write);
        }
        else  /* 15K-2系统 */
        {
            rv = BSP_cp3banMappingBaseAddrGet(dwSubslotId, &pMappingAddr);
            if (BSP_E_CP3BAN_OK != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u , BSP_cp3banMappingBaseAddrGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, rv);
                return rv;
            }
            DRV_STM_CHECK_POINTER_IS_NULL(pMappingAddr);
            pFpgaBaseAddr = pMappingAddr->fpgaBaseAddr;
            DRV_STM_CHECK_POINTER_IS_NULL(pFpgaBaseAddr);
            fpgaBaseAddr = (WORD32)pFpgaBaseAddr;
            BSP_Print(BSP_DEBUG_ALL, "%s line %d, pFpgaBaseAddr = %p, fpgaBaseAddr = 0x%x.\n", __FILE__, __LINE__, pFpgaBaseAddr, fpgaBaseAddr);
            g_drv_tdm_atDevHalPtr[chipId] = AtHalZtePtnNew(fpgaBaseAddr);
        }
        if (NULL == g_drv_tdm_atDevHalPtr[chipId])
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u , create HAL unsuccessfully.\n", __FILE__, __LINE__, chipId);
            return DRV_TDM_SDK_API_FAIL;
        }
        BSP_Print(BSP_DEBUG_ALL, "%s line %d, chipId %u, pHal = %p.\n", __FILE__, __LINE__, chipId, g_drv_tdm_atDevHalPtr[chipId]);
    }
    productCode = AtHalRead(g_drv_tdm_atDevHalPtr[chipId], 0x0);  /* Get product code. */
    BSP_Print(BSP_DEBUG_ALL, "%s line %d, chipId %u, productCode=0x%x.\n", __FILE__, __LINE__, chipId, productCode);
    /* Check if fpga product code is valid */
    if ((DRV_TDM_FPGA_CODE_1G_CES != productCode) 
        && (DRV_TDM_FPGA_CODE_10G_CES != productCode)
        && (DRV_TDM_FPGA_CODE_1G_MLPPP != productCode) 
        && (DRV_TDM_FPGA_CODE_10G_MLPPP != productCode))
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, productCode=0x%x, product code is invalid.\n", __FILE__, __LINE__, chipId, productCode);
        return DRV_TDM_AT_INVALID_PRODUCT_CODE;
    }
    g_drv_tdm_fpgaProductCode[chipId] = productCode;  /* save product code */
    
    if (NULL == g_drv_tdm_atDevicePtr[chipId]) /* create ARRIVE device */
    {
        g_drv_tdm_atDevicePtr[chipId] = AtDriverDeviceCreate(g_drv_tdm_atDevDriverPtr, productCode);
        if (NULL == g_drv_tdm_atDevicePtr[chipId])
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId=%u, create device unsuccessfully.\n", __FILE__, __LINE__, chipId);
            return DRV_TDM_SDK_API_FAIL;
        }
        BSP_Print(BSP_DEBUG_ALL, "%s line %d, chipId %u, pDevice = %p.\n", __FILE__, __LINE__, chipId, g_drv_tdm_atDevicePtr[chipId]);
    }

    /* Install Hardware Abstraction Layer */
    ipCoresNum = AtDeviceNumIpCoresGet(g_drv_tdm_atDevicePtr[chipId]); /* Get number of IP cores */
    for (coreId = 0; coreId < ipCoresNum; coreId++)
    {
        ipCore = AtDeviceIpCoreGet(g_drv_tdm_atDevicePtr[chipId], coreId); /* Get IP core object by its ID. */
        if (NULL == ipCore)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u coreId %u, AtDeviceIpCoreGet() unsuccessfully.\n", __FILE__, __LINE__, chipId, coreId);
            return DRV_TDM_SDK_API_FAIL;
        }
        rv = AtIpCoreHalSet(ipCore, g_drv_tdm_atDevHalPtr[chipId]); /* Install Hardware Abstraction Layer. */
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u coreId %u, AtIpCoreHalSet() rv=%s.\n", __FILE__, __LINE__, chipId, coreId, AtRet2String(rv));
            return DRV_TDM_SDK_API_FAIL;
        }
        BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, chipId %u coreId %u, Install HAL successfully.\n", __FILE__, __LINE__, chipId, coreId);
    }

    rv = AtDeviceInit(g_drv_tdm_atDevicePtr[chipId]); /* Initialize ARRIVE device */
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, AtDeviceInit() rv=%s.\n", __FILE__, __LINE__, chipId, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    
    rv = drv_tdm_phyIntfInit(chipId);  /* Initialize ARRIVE chip's physical interface. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_phyIntfInit() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    if ((DRV_TDM_FPGA_CODE_10G_CES == productCode) 
        || (DRV_TDM_FPGA_CODE_10G_MLPPP == productCode))
    {
        /* 对于HIGIG模式的STM单板,默认都只工作在HiGiG0口 */
        rv = drv_tdm_ethIntfApsSwitch((BYTE)dwSubslotId, 1, 0); 
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, drv_tdm_ethIntfApsSwitch() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, rv);
            return rv;
        }
    }

    rv = drv_tdm_interruptEnable((BYTE)dwSubslotId, DRV_TDM_STM_INTERRUPT_ENABLE); /* enable interrupt. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, drv_tdm_interruptEnable() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }

    /* 初始化时,disable interrupt mask. 当线卡初始化完成后,由产品管理来enable interrupt mask. */
    for (portId = DRV_TDM_STM_PORT_ID_START; portId <= maxPortId; portId++)
    {
        /* 清除中断的历史状态 */
        rv = drv_tdm_secAlmIntrClear((BYTE)dwSubslotId, portId);
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u portId %u, drv_tdm_secAlmIntrClear() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, portId, rv);
            return rv;
        }
        rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine); /* Get SDH line. */
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
            return rv;
        }
        DRV_STM_CHECK_POINTER_IS_NULL(pLine);
        /* Disable interrupt mask. */
        rv = AtChannelInterruptMaskSet((AtChannel)pLine, (WORD32)0xffffffff, 0);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelInterruptMaskSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
    }

    if ((DRV_TDM_FPGA_CODE_1G_CES == productCode) || (DRV_TDM_FPGA_CODE_10G_CES == productCode))
    {    /* For ACR/DCR clock, disable clock learn from L-bit packets. */
        rv = drv_tdm_clkLearnLbitPktsEnable(chipId, DRV_TDM_CLK_LEARN_LBIT_PKT_DISABLE);
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_clkLearnLbitPktsEnable() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
            return rv;
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atDevHalRegRead
* 功能描述: 读取ARRIVE芯片的某个寄存器的值.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           address: 寄存器的地址.         
* 输出参数: *pValue: 用来保存寄存器的值. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-06   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atDevHalRegRead(BYTE chipId, WORD32 address, WORD32 *pValue)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtHal pHal = NULL;
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pValue);
    
    rv = drv_tdm_atDevHalGet(chipId, &pHal);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ACCESS_FPGA, "ERROR: %s line %d, chipId %u, drv_tdm_atDevHalGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pHal);
    
    *pValue = AtHalRead(pHal, address);  /* Read a register. */
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atDevHalRegWrite
* 功能描述: 写ARRIVE芯片的某个寄存器的值.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           address: 寄存器的地址.  
*           value: 寄存器的值.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-06   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atDevHalRegWrite(BYTE chipId, WORD32 address, WORD32 value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtHal pHal = NULL;
        
    rv = drv_tdm_atDevHalGet(chipId, &pHal);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ACCESS_FPGA, "ERROR: %s line %d, chipId %u, drv_tdm_atDevHalGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pHal);
    
    AtHalWrite(pHal, address, value);  /* Write a register. */
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_threadStateSet
* 功能描述: 设置线程的状态.
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3.
*           state: 线程的状态,具体参见定义DRV_TDM_THREAD_STATE.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-07-28   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_threadStateSet(BYTE chipId, WORD32 state)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
        
    rv = drv_tdm_sdhBerThreadStateSet(chipId, state);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_sdhBerThreadStateSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_e1ClkAlmThreadStateSet(chipId, state);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_e1ClkAlmThreadStateSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_sdhAlmThreadStateSet(chipId, state);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_sdhAlmThreadStateSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_sdhPerfThreadStateSet(chipId, state);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_sdhPerfThreadStateSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_pwe3PerfAlmThreadStateSet(chipId, state);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_pwe3PerfAlmThreadStateSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_threadCreate
* 功能描述: 创建STM TDM PWE3模块所需要的线程.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-13   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_threadCreate(BYTE subslotId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
        
    rv = drv_tdm_sdhBerThreadCreate(subslotId);  /* create thread for monitoring sdh ber. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_sdhBerThreadCreate() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_e1ClkAlmThreadCreate(subslotId);  /* 创建监测E1链路的时钟切换告警的线程. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_e1ClkAlmThreadCreate() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_sdhAlmThreadCreate(subslotId);  /* 创建监测SDH告警的线程. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_sdhAlmThreadCreate() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_sdhPerfThreadCreate(subslotId);  /* 创建监测SDH性能统计的线程. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_sdhPerfThreadCreate() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_pwe3PerfAlmThreadCreate(subslotId);  /* 创建监测PWE3性能告警的线程. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_pwe3PerfAlmThreadCreate() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_threadDelete
* 功能描述: 删除在STM单板上所创建的线程.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-13   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_threadDelete(BYTE subslotId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
        
    rv = drv_tdm_sdhBerThreadDelete(subslotId);  /* delete thread for monitoring sdh ber. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_sdhBerThreadDelete() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_e1ClkAlmThreadDelete(subslotId);  /* 删除监测E1链路的时钟切换告警的线程 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, drv_tdm_e1ClkAlmThreadDelete() rv=0x%x.\n", __FILE__, __LINE__, subslotId, rv);
        return rv;
    }
    rv = drv_tdm_sdhAlmThreadDelete(subslotId);  /* 删除监测SDH告警的线程. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, drv_tdm_sdhAlmThreadDelete() rv=0x%x.\n", __FILE__, __LINE__, subslotId, rv);
        return rv;
    }
    rv = drv_tdm_sdhPerfThreadDelete(subslotId);  /* 删除监测SDH性能统计的线程. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, drv_tdm_sdhPerfThreadDelete() rv=0x%x.\n", __FILE__, __LINE__, subslotId, rv);
        return rv;
    }
    rv = drv_tdm_pwe3PerfAlmThreadDelete(subslotId);  /* 删除监测PWE3性能告警的线程. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, drv_tdm_pwe3PerfAlmThreadDelete() rv=0x%x.\n", __FILE__, __LINE__, subslotId, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_semCreate
* 功能描述: 创建信号量.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-13   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_semCreate(BYTE chipId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
        
    rv = drv_tdm_sdhBerSemCreate(chipId);  /* create semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_sdhBerSemCreate() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_e1ClkAlmSemCreate(chipId);  /* create semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_e1ClkAlmSemCreate() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_pwSemCreate(chipId);  /* create semaphore. */  
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemCreate() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_pwe3PerfAlmSemCreate(chipId);  /* create semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3PerfAlmSemCreate() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_sdhAlmSemCreate(chipId);  /* create semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_sdhAlmSemCreate() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_sdhPerfSemCreate(chipId);  /* create semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_sdhPerfSemCreate() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_stmIntfSemCreate(chipId);  /* create semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_stmIntfSemCreate() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, chipId %u, Create semaphore successfully.\n", __FILE__, __LINE__, chipId);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_semDestroy
* 功能描述: 销毁信号量.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-13   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_semDestroy(BYTE chipId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
        
    rv = drv_tdm_sdhBerSemDestroy(chipId);  /* destroy semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_sdhBerSemDestroy() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_e1ClkAlmSemDestroy(chipId);  /* destroy semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_e1ClkAlmSemDestroy() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_pwSemDestroy(chipId);  /* destroy semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemDestroy() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_pwe3PerfAlmSemDestroy(chipId);  /* destroy semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3PerfAlmSemDestroy() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_sdhAlmSemDestroy(chipId);  /* destroy semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_sdhAlmSemDestroy() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_sdhPerfSemDestroy(chipId);  /* destroy semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_sdhPerfSemDestroy() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_stmIntfSemDestroy(chipId);  /* destroy semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_stmIntfSemDestroy() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, chipId %u, Destroy semaphore successfully.\n", __FILE__, __LINE__, chipId);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_semaphoreDestroy
* 功能描述: 销毁信号量.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 无.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-11   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_semaphoreDestroy(VOID)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 systemType = 0;   /* ZXUPN15000类型 */
    
    systemType = BSP_cp3banSystemTypeGet();  /* 获取ZXUPN15000系统类型. */
    /* 对于非15K-2系统,不需要销毁信号量g_drv_pwe3_initSemaphoreId和g_drv_pwe3_cardCntSemaphoreId.
       如果销毁的话,在DZ或者GZ线卡上插有多块接口卡时,
       热插补操作会出问题,可能导致FPGA初始化时获取不到信号量 */
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM != systemType)  /* 15K-2系统 */
    {
        /* 销毁信号量 */
        rv = drv_pwe3_initSemaphoreDestroy();
        if (DRV_PWE3_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_pwe3_initSemaphoreDestroy() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        rv = drv_pwe3_cardCntSemaphoreDestroy();
        if (DRV_PWE3_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_pwe3_cardCntSemaphoreDestroy() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwe3Initialize
* 功能描述: 初始化STM TDM PWE3模块
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4. 
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 以单板的子槽位号为单位来初始化STM TDM PWE3模块.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwe3Initialize(BYTE subslotId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 initFlag = 0;     /* STM单板的初始化标记. */
    BYTE chipId = 0;   /* ARRIVE芯片的芯片编号 */
    
    chipId = subslotId - (BYTE)1;
    
    rv = drv_tdm_pwe3InitFlagGet(chipId, &initFlag);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3InitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    if (DRV_TDM_STM_BOARD_UNINSTALL != initFlag) /* 如果单板已经初始化成功了,则直接返回成功. */
    {
        BSP_Print(BSP_DEBUG_ALL, "WARNING: %s line %d, subslotId %u, STM board has been initialized.\n", __FILE__, __LINE__, subslotId);
        return DRV_TDM_OK;
    }

    rv = drv_tdm_threadStateSet(chipId, DRV_TDM_THREAD_STOP);  /* 初始化之前,先将线程设置为STOP状态. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_threadStateSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_threadMonitorFlagSet(chipId, DRV_TDM_ENABLE);  /* Enable monitor thread. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_threadMonitorFlagSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    /* 根据项目要求,目前需要disable掉sdhBer线程,以便降低CPU利用率.2014-09-04. */
    rv = drv_tdm_sdhBerFlagSet(chipId, DRV_TDM_DISABLE); 
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_sdhBerFlagSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_memoryAllocate(chipId);  /* allocate memory for STM board. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_memoryAllocate() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_semCreate(chipId);  /* Create semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_semCreate() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    
    rv = drv_tdm_atDeviceInitialize(chipId); /* Initialize FPGA. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atDeviceInitialize() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);    
        return rv;
    }
    BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, chipId %u, Initialize FPGA device successfully.\n", __FILE__, __LINE__, chipId);

    rv = drv_tdm_threadCreate(subslotId);  /* Create threads for STM board. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, drv_tdm_threadCreate() rv=0x%x.\n", __FILE__, __LINE__, subslotId, rv);
        return rv;
    }
    rv = drv_tdm_threadStateSet(chipId, DRV_TDM_THREAD_RUNNING);  /* 创建完线程后,将线程设置为RUNNING状态. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_threadStateSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }

    rv = drv_tdm_e1BindingPwNumClear(chipId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_e1BindingPwNumClear() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_stmIntfPwNumClear(chipId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmIntfPwNumClear() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_ethIntfPwNumClear(chipId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_ethIntfPwNumClear() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_ethIntfStateClear(chipId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_ethIntfStateClear() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_stmRecClkExtractStateClear(chipId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmRecClkExtractStateClear() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_pwe3_cardCntSemaphoreGet();  /* 获取信号量 */
    if (DRV_PWE3_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_pwe3_cardCntSemaphoreGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    drv_pwe3_subcardCntSet(DRV_TDM_SUBCARD_INCREASE);  /* 增加窄带子卡的数量 */
    rv = drv_pwe3_cardCntSemaphoreFree();  /* 释放信号量 */
    if (DRV_PWE3_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_pwe3_cardCntSemaphoreFree() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_pwe3InitFlagSet(chipId, DRV_TDM_STM_BOARD_INSTALLED);  /* STM TDM PWE3模块初始化成功 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_pwe3InitFlagSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
        
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atDeviceUnInitialize
* 功能描述: 去初始化FPGA芯片
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3. 
* 输出参数:  
* 返 回 值: 
* 其它说明: 本函数用来释放FPGA芯片的资源.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atDeviceUnInitialize(BYTE chipId)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 systemType = 0;   /* ZXUPN15000类型 */
    WORD32 dwSubslotId = 0;  /* 单板的子槽位号 */
    WORD32 boardType = 0;    /* 单板类型 */
    BYTE portId = 0;         /* STM端口的编号. */
    BYTE maxPortId = 0;      /* STM端口的最大编号 */
    BYTE ucStmIntfIdx = 0;  /* FPGA芯片的STM接口的索引,取值为0~7. */
    BYTE e1LinkId = 0;       /* E1链路的编号,取值为1~63. */
    BYTE ethIntfIndex = 0;  /* FPGA芯片的Ethernet接口的索引. */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);  /* 检查ARRIVE芯片的chipId的范围 */
    dwSubslotId = (WORD32)chipId + 1;

    systemType = BSP_cp3banSystemTypeGet();  /* 获取ZXUPN15000系统类型. */

    rv = BSP_cp3ban_boardTypeGet(dwSubslotId, &boardType);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_boardTypeGet() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    
    if (BSP_CP3BAN_8PORT_BOARD == boardType)  /* 8端口的STM单板 */
    {
        maxPortId = DRV_TDM_STM_8PORT_ID_END;
    }
    else if (BSP_CP3BAN_4PORT_BOARD == boardType)  /* 4端口的STM单板 */
    {
        maxPortId = DRV_TDM_STM_4PORT_ID_END;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, invalid board type, boardType=%u.\n", __FILE__, __LINE__, dwSubslotId, boardType);
        return DRV_TDM_INVALID_BOARD_TYPE;
    }

    /* Delete a device */
    if ((NULL != g_drv_tdm_atDevDriverPtr) && (NULL != g_drv_tdm_atDevicePtr[chipId]))
    {
        AtDriverDeviceDelete(g_drv_tdm_atDevDriverPtr, g_drv_tdm_atDevicePtr[chipId]);  
    }

    if (BSP_CP3BAN_NONE_15K_2_SYSTEM != systemType)   /* 15K-2系统 */
    {
        if (NULL != g_drv_tdm_atDevDriverPtr)
        {
            AtDriverDelete(g_drv_tdm_atDevDriverPtr);  /* Delete driver */
        }
        g_drv_tdm_atDevDriverPtr = NULL;   /* 释放掉驱动自己的资源 */
    }

    if (NULL != g_drv_tdm_atDevHalPtr[chipId])
    {
        AtHalDelete(g_drv_tdm_atDevHalPtr[chipId]);  /* delete HAL (Hardware Abstraction Layer) */
    }
     
    /* 在释放完ARRIVE芯片的资源后,应该释放掉驱动的资源 */
    g_drv_tdm_atDevicePtr[chipId] = NULL;
    g_drv_tdm_atDevHalPtr[chipId] = NULL;
    for (portId = DRV_TDM_STM_PORT_ID_START; portId <= maxPortId; portId++)
    {
        ucStmIntfIdx = portId - (BYTE)1;
        g_drv_tdm_stm_intf_mode[chipId][ucStmIntfIdx] = 0;
        g_drv_tdm_atSdhLinePtr[chipId][ucStmIntfIdx] = NULL;
        g_drv_tdm_atSdhAug4Ptr[chipId][ucStmIntfIdx] = NULL;
        g_drv_tdm_atSdhAug1Ptr[chipId][ucStmIntfIdx] = NULL;
        g_drv_tdm_atSdhAu4Ptr[chipId][ucStmIntfIdx] = NULL;
        g_drv_tdm_atSdhVc4Ptr[chipId][ucStmIntfIdx] = NULL;
        for (e1LinkId = DRV_TDM_E1_LINK_ID_START; e1LinkId <= DRV_TDM_E1_LINK_ID_END; e1LinkId++)
        {
            g_drv_tdm_atSdhTu12Ptr[chipId][ucStmIntfIdx][e1LinkId] = NULL;
            g_drv_tdm_atSdhVc12Ptr[chipId][ucStmIntfIdx][e1LinkId] = NULL;
            g_drv_tdm_atPdhE1Ptr[chipId][ucStmIntfIdx][e1LinkId] = NULL;
        }
        rv = drv_tdm_rxStmChannelStateSet(chipId, portId, DRV_TDM_SDH_CHANNEL_ENABLE);
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_rxStmChannelStateSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
            return rv;
        }
    }

    for (ethIntfIndex = DRV_TDM_CHIP_ETH_INTF_ID_START; ethIntfIndex <= DRV_TDM_CHIP_ETH_INTF_ID_END; ethIntfIndex++)
    {
        g_drv_tdm_atEthPortPtr[chipId][ethIntfIndex] = NULL;
    }

    g_drv_tdm_fpgaProductCode[chipId] = 0;  /* clear memory */
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwe3UnInitialize
* 功能描述: 卸载STM TDM PWE3模块.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.
* 输出参数: 无.  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-13   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwe3UnInitialize(BYTE subslotId)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;   /* ARRIVE芯片的芯片编号 */
    
    chipId = subslotId - (BYTE)1;
    
    rv = drv_tdm_threadMonitorFlagSet(chipId, DRV_TDM_DISABLE);  /* Disable monitor task. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_threadMonitorFlagSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_threadDelete(subslotId);  /* Delete thread */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, drv_tdm_threadDelete() rv=0x%x.\n", __FILE__, __LINE__, subslotId, rv);
        return rv;
    }
    rv = drv_tdm_semDestroy(chipId);  /* Destroy semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_semDestroy() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }

    rv = drv_tdm_atDeviceUnInitialize(chipId);  /* UnInitialize FPGA chip, and release resources of FPGA chip. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atDeviceUnInitialize() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, chipId %u, Uninitialize FPGA successfully.\n", __FILE__, __LINE__, chipId);

    rv = drv_tdm_memoryFree(chipId);  /* release memory resource. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_memoryFree() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_e1BindingPwNumClear(chipId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_e1BindingPwNumClear() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_stmIntfPwNumClear(chipId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmIntfPwNumClear() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_ethIntfPwNumClear(chipId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_ethIntfPwNumClear() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_ethIntfStateClear(chipId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_ethIntfStateClear() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_stmRecClkExtractStateClear(chipId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmRecClkExtractStateClear() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_pwe3_cardCntSemaphoreGet();  /* 获取信号量 */
    if (DRV_PWE3_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_pwe3_cardCntSemaphoreGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    drv_pwe3_subcardCntSet(DRV_TDM_SUBCARD_DECREASE);  /* 减少窄带子卡的数量 */
    rv = drv_pwe3_cardCntSemaphoreFree();  /* 释放信号量 */
    if (DRV_PWE3_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_pwe3_cardCntSemaphoreFree() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_semaphoreDestroy();  /* 销毁信号量 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_semaphoreDestroy() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_pwe3InitFlagSet(chipId, DRV_TDM_STM_BOARD_UNINSTALL);  /* STM单板卸载成功 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_pwe3InitFlagSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
        
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_fpgaRamDdrNumGet
* 功能描述: 获取FPGA芯片的RAM DDR的数量.
* 访问的表: 无
* 修改的表: 无
* 输入参数: ucChipId: FPGA芯片的芯片编号,取值为0~3.
* 输出参数: *pucRamDdrNum: 保存RAM DDR的数量.  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-12-02   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_fpgaRamDdrNumGet(BYTE ucChipId, BYTE *pucRamDdrNum)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    BYTE ucDdrNum = 0;     /* FPGA芯片上的RAM的数量 */
    AtDevice pDevice = NULL;
    AtModuleRam pRamModule = NULL;
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pucRamDdrNum);
    
    dwRetValue = drv_tdm_atDevicePtrGet(ucChipId, &pDevice);
    if (DRV_TDM_OK != dwRetValue)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, chipId %u, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, ucChipId, dwRetValue);
        return dwRetValue;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    
    pRamModule = (AtModuleRam)AtDeviceModuleGet(pDevice, cAtModuleRam);
    DRV_TDM_CHECK_POINTER_IS_NULL(pRamModule);
    
    ucDdrNum = AtModuleRamNumDdrGet(pRamModule);  /* Get number of DDRs */
    *pucRamDdrNum = ucDdrNum;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_fpgaRamCheck
* 功能描述: 检测FPGA芯片的RAM.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
*           durationMs: 测试的持续时间,以毫秒为单位.
* 输出参数: 无.  
* 返 回 值: 
* 其它说明: 在检测FPGA RAM后,必须重新初始化FPGA,否则会存在一些bug.因为在
            检测FPGA RAM时,芯片厂商会往FPGA里面写一些值,必须通过重新初始化
            来清除这些垃圾值.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-12-02   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_fpgaRamCheck(BYTE subslotId, WORD32 durationMs)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    BYTE ucChipId = 0;
    BYTE ucDdrNum = 0;     /* FPGA芯片上的RAM的数量 */
    BYTE ucDdrId = 0;      /* ddr ID */
    AtDevice pDevice = NULL;
    AtModuleRam pRamModule = NULL;
    AtRam pRam = NULL;

    ucChipId = subslotId - (BYTE)1;

    if (durationMs <= DRV_TDM_MILLISECONDS_IN_1_MINUTES)  /* 至少测试1分钟 */
    {
        durationMs = DRV_TDM_MILLISECONDS_IN_1_MINUTES;
    }

    dwRetValue = drv_tdm_atDevicePtrGet(ucChipId, &pDevice);
    if (DRV_TDM_OK != dwRetValue)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, ucChipId, dwRetValue);
        return dwRetValue;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    
    pRamModule = (AtModuleRam)AtDeviceModuleGet(pDevice, cAtModuleRam);
    DRV_TDM_CHECK_POINTER_IS_NULL(pRamModule);
    
    ucDdrNum = AtModuleRamNumDdrGet(pRamModule);  /* Get number of DDRs */
    for (ucDdrId = 0; ucDdrId < ucDdrNum; ucDdrId++)
    {
        pRam = AtModuleRamDdrGet(pRamModule, ucDdrId);  /* Get DDR */
        DRV_TDM_CHECK_POINTER_IS_NULL(pRam);
        
        dwRetValue = AtRamAddressBusTestWithDuration(pRam, durationMs);  /* Test address bus */
        if (cAtOk != dwRetValue)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, chipId %u ddrId %u, AtRamAddressBusTestWithDuration() rv=0x%x.\n", __FILE__, __LINE__, ucChipId, ucDdrId, dwRetValue);
        }
    
        dwRetValue = AtRamDataBusTestWithDuration(pRam, durationMs);  /* Test data bus */
        if (cAtOk != dwRetValue)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, chipId %u ddrId %u, AtRamDataBusTestWithDuration() rv=0x%x.\n", __FILE__, __LINE__, ucChipId, ucDdrId, dwRetValue);
        }

        dwRetValue = AtRamMemoryTestWithDuration(pRam, durationMs); /* Test memory */
        if (cAtOk != dwRetValue)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, chipId %u ddrId %u AtRamMemoryTestWithDuration() rv=0x%x.\n", __FILE__, __LINE__, ucChipId, ucDdrId, dwRetValue);
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atDeviceSSKeyCheck
* 功能描述: 检测FPGA芯片的SSKey.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           dwCheckTimes: 检测次数.
* 输出参数: 无.  
* 返 回 值: 
* 其它说明: 该函数仅仅用来测试FPGA的SSKey的稳定性,请不要调用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-06-14   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atDeviceSSKeyCheck(BYTE chipId, WORD32 dwCheckTimes)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 i = 0;
    AtDevice pDevice = NULL;
    
    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    
    if (dwCheckTimes < 1)
    {
        dwCheckTimes = 1;
    }
    for (i = 0; i < dwCheckTimes; i++)
    {
        rv = AtDeviceSSKeyCheck(pDevice);
        if (cAtOk != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, chipId %u, testingTimes = %u, AtDeviceSSKeyCheck() rv=0x%x.\n", __FILE__, __LINE__, chipId, i, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_fpgaRamTestedStateGet
* 功能描述: 获取FPGA芯片的RAM检测状态.
* 访问的表: 无
* 修改的表: 无
* 输入参数: ucSubslotId: STM单板的子槽位号,取值为1~4.
* 输出参数: *pdwRamTestState: 保存RAM的检测状态,具体参见DRV_TDM_FPGA_RAM_TEST_STATUS定义.  
* 返 回 值: 
* 其它说明: 检查FPGA芯片的所有RAM是否测试完成.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-13   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_fpgaRamTestedStateGet(BYTE ucSubslotId, WORD32 *pdwRamTestState)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    BYTE ucChipId = 0;
    AtDevice pDevice = NULL;
    eBool atRamTestState = 0;
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pdwRamTestState);
    ucChipId = ucSubslotId - (BYTE)1;
    
    /* 获取ARRIVE芯片的device指针 */
    dwRetValue = drv_tdm_atDevicePtrGet(ucChipId, &pDevice);
    if (DRV_TDM_OK != dwRetValue)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, chipId %u, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, ucChipId, dwRetValue);
        return dwRetValue;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    
    atRamTestState = AtZtePtnRamTested(pDevice);
    if (cAtTrue == atRamTestState)
    {
        *pdwRamTestState = DRV_TDM_FPGA_RAM_FINISHED;
    }
    else if (cAtFalse == atRamTestState)
    {
        *pdwRamTestState = DRV_TDM_FPGA_RAM_UNFINISHED;
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, chipId %u, AtZtePtnRamTested() rv=%u.\n", __FILE__, __LINE__, ucChipId, atRamTestState);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_fpgaRamStateGet
* 功能描述: 获取FPGA芯片的RAM状态.
* 访问的表: 无
* 修改的表: 无
* 输入参数: ucSubslotId: STM单板的子槽位号,取值为1~4.
* 输出参数: *pdwRamState: 保存RAM的状态,具体参见DRV_TDM_FPGA_RAM_STATUS定义.  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-13   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_fpgaRamStateGet(BYTE ucSubslotId, WORD32 *pdwRamState)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    BYTE ucChipId = 0;
    AtDevice pDevice = NULL;
    eBool atRamState = 0;
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pdwRamState);
    ucChipId = ucSubslotId - (BYTE)1;
    
    /* 获取ARRIVE芯片的device指针 */
    dwRetValue = drv_tdm_atDevicePtrGet(ucChipId, &pDevice);
    if (DRV_TDM_OK != dwRetValue)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, chipId %u, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, ucChipId, dwRetValue);
        return dwRetValue;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    
    atRamState = AtZtePtnRamIsGood(pDevice);
    if (cAtTrue == atRamState)
    {
        *pdwRamState = DRV_TDM_FPGA_RAM_GOOD;
    }
    else if (cAtFalse == atRamState)
    {
        *pdwRamState = DRV_TDM_FPGA_RAM_BAD;
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, chipId %u, AtZtePtnRamIsGood() rv=%u.\n", __FILE__, __LINE__, ucChipId, atRamState);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_fpgaLogicStateGet
* 功能描述: 获取FPGA芯片的FPGA逻辑状态.
* 访问的表: 无
* 修改的表: 无
* 输入参数: ucSubslotId: STM单板的子槽位号,取值为1~4.
* 输出参数: *pdwLogicState: 保存FPGA逻辑的状态,具体参见DRV_TDM_FPGA_LOGIC_STATUS定义.  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-13   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_fpgaLogicStateGet(BYTE ucSubslotId, WORD32 *pdwLogicState)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    BYTE ucChipId = 0;
    AtDevice pDevice = NULL;
    eBool atFpgaLogicState = 0;
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pdwLogicState);
    ucChipId = ucSubslotId - (BYTE)1;
    
    /* 获取ARRIVE芯片的device指针 */
    dwRetValue = drv_tdm_atDevicePtrGet(ucChipId, &pDevice);
    if (DRV_TDM_OK != dwRetValue)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, chipId %u, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, ucChipId, dwRetValue);
        return dwRetValue;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    
    atFpgaLogicState = AtZtePtnFpgaLogicIsGood(pDevice);
    if (cAtTrue == atFpgaLogicState)
    {
        *pdwLogicState = DRV_TDM_FPGA_LOGIC_GOOD;
    }
    else if (cAtFalse == atFpgaLogicState)
    {
        *pdwLogicState = DRV_TDM_FPGA_LOGIC_BAD;
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, chipId %u, AtZtePtnFpgaLogicIsGood() rv=%u.\n", __FILE__, __LINE__, ucChipId, atFpgaLogicState);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_fpgaClockStateGet
* 功能描述: 获取FPGA内部的时钟状态.
* 访问的表: 无
* 修改的表: 无
* 输入参数: ucSubslotId: 单板的子槽位号,取值为1~4.
* 输出参数: *pClkState: 保存FPGA内部的时钟状态. 
* 返 回 值: 
* 其它说明: 通过FPGA内部的PLL读取各时钟(TCXO 19M,125M,100M,155M)锁定状态
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-12-11   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_fpgaClockStateGet(BYTE ucSubslotId, T_DRV_TDM_FPGA_CLK_STATE *pClkState)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    BYTE chipId = 0;
    AtDevice pDevice = NULL;
    eAtZtePtnClockStatus atClkState = 0;
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pClkState);
    chipId = ucSubslotId - (BYTE)1;
    
    /* 获取ARRIVE芯片的device指针 */
    dwRetValue = drv_tdm_atDevicePtrGet(chipId, &pDevice);
    if (DRV_TDM_OK != dwRetValue)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, chipId %u, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwRetValue);
        return dwRetValue;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    
    memset(pClkState, 0, sizeof(T_DRV_TDM_FPGA_CLK_STATE));  /* 先清零再赋值 */
    atClkState = AtZtePtnClockCheck(pDevice);

    if (0 != (atClkState & cAtZtePtnClockStatusSystemClockFail))
    {
        pClkState->ucSysClkState = DRV_TDM_FPGA_CLK_BAD;
    }
    else
    {
        pClkState->ucSysClkState = DRV_TDM_FPGA_CLK_GOOD;
    }

    if (0 != (atClkState & cAtZtePtnClockStatusLocalBusClockFail))
    {
        pClkState->ucLocalBusClkState = DRV_TDM_FPGA_CLK_BAD;
    }
    else
    {
        pClkState->ucLocalBusClkState = DRV_TDM_FPGA_CLK_GOOD;
    }

    if (0 != (atClkState & cAtZtePtnClockStatusGeClockFail))
    {
        pClkState->ucGeClkState = DRV_TDM_FPGA_CLK_BAD;
    }
    else
    {
        pClkState->ucGeClkState = DRV_TDM_FPGA_CLK_GOOD;
    }

    if (0 != (atClkState & cAtZtePtnClockStatusOcnClock1Fail))
    {
        pClkState->ucOcnClk1State = DRV_TDM_FPGA_CLK_BAD;
    }
    else
    {
        pClkState->ucOcnClk1State = DRV_TDM_FPGA_CLK_GOOD;
    }

    if (0 != (atClkState & cAtZtePtnClockStatusOcnClock2Fail))
    {
        pClkState->ucOcnClk2State = DRV_TDM_FPGA_CLK_BAD;
    }
    else
    {
        pClkState->ucOcnClk2State = DRV_TDM_FPGA_CLK_GOOD;
    }

    if (0 != (atClkState & cAtZtePtnClockStatusDdr1ClockFail))
    {
        pClkState->ucDdr1ClkState = DRV_TDM_FPGA_CLK_BAD;
    }
    else
    {
        pClkState->ucDdr1ClkState = DRV_TDM_FPGA_CLK_GOOD;
    }

    if (0 != (atClkState & cAtZtePtnClockStatusDdr2ClockFail))
    {
        pClkState->ucDdr2ClkState = DRV_TDM_FPGA_CLK_BAD;
    }
    else
    {
        pClkState->ucDdr2ClkState = DRV_TDM_FPGA_CLK_GOOD;
    }

    if (0 != (atClkState & cAtZtePtnClockStatusDdr3ClockFail))
    {
        pClkState->ucDdr3ClkState = DRV_TDM_FPGA_CLK_BAD;
    }
    else
    {
        pClkState->ucDdr3ClkState = DRV_TDM_FPGA_CLK_GOOD;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_fpgaClockStateQuery
* 功能描述: 查询FPGA内部的所有时钟的状态.
* 访问的表: 无
* 修改的表: 无
* 输入参数: ucSubslotId: 单板的子槽位号,取值为1~4.
* 输出参数: *pdwClkState: 保存FPGA内部的时钟状态,0表示时钟锁住;1表示没有锁住. 
* 返 回 值: 
* 其它说明: 通过FPGA内部的PLL读取各时钟(TCXO 19M,125M,100M,155M)锁定状态
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-12-11   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_fpgaClockStateQuery(BYTE ucSubslotId, WORD32 *pdwClkState)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    T_DRV_TDM_FPGA_CLK_STATE tClkState;
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pdwClkState);
    memset(&tClkState, 0, sizeof(tClkState));
    
    dwRetValue = drv_tdm_fpgaClockStateGet(ucSubslotId, &tClkState);
    if (DRV_TDM_OK != dwRetValue)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, subslotId %u, drv_tdm_fpgaClockStateGet() rv=0x%x.\n", __FILE__, __LINE__, ucSubslotId, dwRetValue);
        return dwRetValue;
    }
    
    if ((DRV_TDM_FPGA_CLK_GOOD == tClkState.ucSysClkState) 
        && (DRV_TDM_FPGA_CLK_GOOD == tClkState.ucLocalBusClkState) 
        && (DRV_TDM_FPGA_CLK_GOOD == tClkState.ucGeClkState) 
        && (DRV_TDM_FPGA_CLK_GOOD == tClkState.ucOcnClk1State)
        && (DRV_TDM_FPGA_CLK_GOOD == tClkState.ucOcnClk2State) 
        && (DRV_TDM_FPGA_CLK_GOOD == tClkState.ucDdr1ClkState) 
        && (DRV_TDM_FPGA_CLK_GOOD == tClkState.ucDdr2ClkState) 
        && (DRV_TDM_FPGA_CLK_GOOD == tClkState.ucDdr3ClkState))
    {
        *pdwClkState = 0;
    }
    else
    {
        *pdwClkState = 1;
    }
    
    return DRV_TDM_OK;
}



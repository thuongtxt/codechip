/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_tdm_sdh_alarm.c
* 文件标识: 
* 其它说明: 驱动STM TDM PWE3模块的STM接口的SDH告警实现函数
* 当前版本: V1.0
* 作    者: 谢伟生10112265.
* 完成日期: 2013-03-26
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/


#include "drv_tdm_sdh_alarm.h"
#include "drv_tdm_overhead.h"
#include "drv_tdm_init.h"
#include "bsp_sdh_common.h"
#include "AtCommon.h"
#include "AtDevice.h"
#include "AtIpCore.h"
#include "AtClasses.h"
#include "AtModule.h"
#include "AtModuleSdh.h"
#include "AtChannel.h"
#include "AtSdhLine.h"
#include "AtSdhPath.h"
#include "AtModulePdh.h"
#include "AtPdhDe1.h"


/**************************************************************************
* 函数名称: drv_tdm_sectionAlarmGet
* 功能描述: 获取STM帧的段告警
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: 端口编号,取值为1~8.
* 输出参数: pSectionAlm: 保存段告警
* 返 回 值: 
* 其它说明: 包括再生段告警和复用段告警
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_sectionAlarmGet(BYTE chipId, 
                                        BYTE portId, 
                                        DRV_TDM_SECTION_ALARM *pSectionAlm)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 alarmStatus = 0;  /* Section alarm status */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pSectionAlm);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_ALM_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    /* Get SDH line. */
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_ALM_QRY, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);

    /* get current alarm status of section */
    alarmStatus = AtChannelAlarmGet((AtChannel)pLine);

    memset(pSectionAlm, 0, sizeof(DRV_TDM_SECTION_ALARM)); /* 先清空内存,在赋值 */
    if (0 != (alarmStatus & cAtSdhLineAlarmLos)) /* RS-LOS */
    {
        pSectionAlm->rsLos = 1;
    }
    else
    {
        pSectionAlm->rsLos = 0;
    }
    
    if (0 != (alarmStatus & cAtSdhLineAlarmOof)) /* RS-OOF */
    {
        pSectionAlm->rsOof = 1;
    }
    else
    {
        pSectionAlm->rsOof = 0;
    }
    
    if (0 != (alarmStatus & cAtSdhLineAlarmLof))  /* RS-LOF */
    {
        pSectionAlm->rsLof = 1;
    }
    else
    {
        pSectionAlm->rsLof = 0;
    }
    
    if (0 != (alarmStatus & cAtSdhLineAlarmTim))  /* RS-TIM */
    {
        pSectionAlm->rsTim = 1;
    }
    else
    {
        pSectionAlm->rsTim = 0;
    }
    
    if (0 != (alarmStatus & cAtSdhLineAlarmAis))  /* MS-AIS */
    {
        pSectionAlm->msAis = 1;
    }
    else
    {
        pSectionAlm->msAis = 0;
    }
    
    if (0 != (alarmStatus & cAtSdhLineAlarmRdi))  /* MS-RDI */
    {
        pSectionAlm->msRdi = 1;
    }
    else
    {
        pSectionAlm->msRdi = 0;
    }
    
    if (0 != (alarmStatus & cAtSdhLineAlarmBerSd))  /* MS-SD */
    {
        pSectionAlm->msSd = 1;
    }
    else
    {
        pSectionAlm->msSd = 0;
    }
    
    if (0 != (alarmStatus & cAtSdhLineAlarmBerSf))  /* MS-SF */
    {
        pSectionAlm->msSf = 1;
    }
    else
    {
        pSectionAlm->msSf = 0;
    }

    if (0 != (alarmStatus & cAtSdhLineAlarmRsBerSd))  /* RS-SD */
    {
        pSectionAlm->rsSd = 1;
    }
    else
    {
        pSectionAlm->rsSd = 0;
    }

    if (0 != (alarmStatus & cAtSdhLineAlarmRsBerSf))  /* RS-SF */
    {
        pSectionAlm->rsSf= 1;
    }
    else
    {
        pSectionAlm->rsSf = 0;
    }

    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_highPathAlarmGet
* 功能描述: 获取STM帧的高阶通道告警
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
* 输出参数: *pVc4Alm: 保存VC4告警.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_highPathAlarmGet(BYTE chipId, 
                                          BYTE portId, 
                                          BYTE aug1Id, 
                                          DRV_TDM_VC4_ALARM *pVc4Alm)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 alarmStatus = 0;  /* VC4 alarm status */
    AtSdhAu pAu4 = NULL;
    AtSdhVc pVc4 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4Alm);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_ALM_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    memset(pVc4Alm, 0, sizeof(DRV_TDM_VC4_ALARM)); /* 先清空再赋值 */
    
    rv = drv_tdm_atSdhAu4PtrGet(chipId, portId, aug1Id, &pAu4);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_ALM_QRY, "ERROR: %s line %d, drv_tdm_atSdhAu4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pAu4);
    alarmStatus = 0;  /* 先清零再赋值 */
    alarmStatus = AtChannelAlarmGet((AtChannel)pAu4);
    if (0 != (alarmStatus & cAtSdhPathAlarmLop)) /* AU4 loss of pointer */
    {
        pVc4Alm->au4Lop = 1;
    }
    else
    {
        pVc4Alm->au4Lop = 0;
    }
    if (0 != (alarmStatus & cAtSdhPathAlarmAis)) /* AU4 alarm indication signal */
    {
        pVc4Alm->au4Ais = 1;
    }
    else
    {
        pVc4Alm->au4Ais = 0;
    }
    if (0 != (alarmStatus & cAtSdhPathAlarmLom)) /* VC4 Loss of Multi-Frame */
    {
        pVc4Alm->vc4Lomf = 1;
    }
    else
    {
        pVc4Alm->vc4Lomf = 0;
    }
    
    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_ALM_QRY, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4); 
    alarmStatus = 0;  /* 先清零再赋值 */
    alarmStatus = AtChannelAlarmGet((AtChannel)pVc4);
    if (0 != (alarmStatus & cAtSdhPathAlarmUneq))  /* VC4 unequipped */
    {
        pVc4Alm->vc4Uneq = 1;
    }
    else
    { 
        pVc4Alm->vc4Uneq = 0;
    }
    if (0 != (alarmStatus & cAtSdhPathAlarmPlm)) /* VC4 payload label mismatch  */
    {
        pVc4Alm->vc4Plm = 1;
    }
    else
    {
        pVc4Alm->vc4Plm = 0;
    }
    if (0 != (alarmStatus & cAtSdhPathAlarmTim)) /* VC4 trace identifier mismatch  */
    {
        pVc4Alm->vc4Tim = 1;
    }
    else
    { 
        pVc4Alm->vc4Tim = 0;
    }
    if (0 != (alarmStatus & cAtSdhPathAlarmRdi)) /* VC4 remote defect indication */
    {
        pVc4Alm->vc4Rdi = 1;
    }
    else
    {
        pVc4Alm->vc4Rdi = 0;
    }
    if (0 != (alarmStatus & cAtSdhPathAlarmErdiS)) /* VC4 ERDI-S */
    {
        pVc4Alm->vc4ERdiS= 1;
    }
    else
    {
        pVc4Alm->vc4ERdiS = 0;
    }
    if (0 != (alarmStatus & cAtSdhPathAlarmErdiP)) /* VC4 ERDI-P */
    {
        pVc4Alm->vc4ERdiP = 1;
    }
    else
    {
        pVc4Alm->vc4ERdiP = 0;
    }
    if (0 != (alarmStatus & cAtSdhPathAlarmErdiC)) /* VC4 ERDI-C */
    {
        pVc4Alm->vc4ERdiC = 1;
    }
    else
    {
        pVc4Alm->vc4ERdiC = 0;
    }
    if (0 != (alarmStatus & cAtSdhPathAlarmBerSd)) /* VC4-SD */
    {
        pVc4Alm->vc4Sd = 1;
    }
    else
    {
        pVc4Alm->vc4Sd = 0;
    }
    if (0 != (alarmStatus & cAtSdhPathAlarmBerSf))  /* VC4-SF */
    {
        pVc4Alm->vc4Sf = 1;
    }
    else
    {
        pVc4Alm->vc4Sf = 0;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_lowPathAlarmGet
* 功能描述: 获取STM帧的低阶通道告警
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
* 输出参数: *pVc12Alm: 保存VC12告警.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_lowPathAlarmGet(BYTE chipId, 
                                         BYTE portId, 
                                         BYTE aug1Id, 
                                         BYTE e1LinkId, 
                                         DRV_TDM_VC12_ALARM *pVc12Alm)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 alarmStatus = 0;  /* VC12 alarm status */
    AtSdhTu pTu12 = NULL;
    AtSdhVc pVc12 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12Alm); 
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_ALM_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    memset(pVc12Alm, 0, sizeof(DRV_TDM_VC12_ALARM)); /* 先清空再赋值 */

    rv = drv_tdm_atSdhTu12PtrGet(chipId, portId, aug1Id, e1LinkId, &pTu12);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_ALM_QRY, "ERROR: %s line %d, drv_tdm_atSdhTu12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pTu12);
    alarmStatus = 0;  /* 先清零再赋值 */
    alarmStatus = AtChannelAlarmGet((AtChannel)pTu12);
    if (0 != (alarmStatus & cAtSdhPathAlarmLop)) /* TU12 loss of pointer */
    {
        pVc12Alm->tu12Lop = 1;
    }
    else
    {
        pVc12Alm->tu12Lop = 0;
    }
    if (0 != (alarmStatus & cAtSdhPathAlarmAis)) /* TU12 alarm indication signal */
    {
        pVc12Alm->tu12Ais = 1;
    }
    else
    {
        pVc12Alm->tu12Ais = 0;
    }
    
    rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_ALM_QRY, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);
    alarmStatus = 0;  /* 先清零再赋值 */
    alarmStatus = AtChannelAlarmGet((AtChannel)pVc12);
    if (0 != (alarmStatus & cAtSdhPathAlarmUneq)) /* VC12 unequipped */
    {
        pVc12Alm->vc12Uneq = 1;
    }
    else
    {
        pVc12Alm->vc12Uneq = 0;
    }
    if (0 != (alarmStatus & cAtSdhPathAlarmPlm)) /* VC12 payload label mismatch  */
    {
        pVc12Alm->vc12Plm = 1;
    }
    else
    {
        pVc12Alm->vc12Plm = 0;
    }
    if (0 != (alarmStatus & cAtSdhPathAlarmTim)) /* VC12 trace identifier mismatch  */
    {
        pVc12Alm->vc12Tim = 1;
    }
    else
    {
        pVc12Alm->vc12Tim = 0;
    }
    if (0 != (alarmStatus & cAtSdhPathAlarmRdi)) /* VC12 remote defect indication */
    {
        pVc12Alm->vc12Rdi = 1;
    }
    else
    {
        pVc12Alm->vc12Rdi = 0;
    }
    if (0 != (alarmStatus & cAtSdhPathAlarmErdiS)) /* VC12 ERDI-S */
    {
        pVc12Alm->vc12ERdiS = 1;
    }
    else
    {
        pVc12Alm->vc12ERdiS = 0;
    }
    if (0 != (alarmStatus & cAtSdhPathAlarmErdiP)) /* VC12 ERDI-P */
    {
        pVc12Alm->vc12ERdiP = 1;
    }
    else
    {
        pVc12Alm->vc12ERdiP = 0;
    }
    if (0 != (alarmStatus & cAtSdhPathAlarmErdiC)) /* VC12 ERDI-C */
    {
        pVc12Alm->vc12ERdiC = 1;
    }
    else
    {
        pVc12Alm->vc12ERdiC = 0;
    }
    if (0 != (alarmStatus & cAtSdhPathAlarmBerSd)) /* VC12 signal degrade */
    {
        pVc12Alm->vc12Sd = 1;
    }
    else
    {
        pVc12Alm->vc12Sd = 0;
    }
    if (0 != (alarmStatus & cAtSdhPathAlarmBerSf)) /* VC12 signal fail */
    {
        pVc12Alm->vc12Sf = 1;
    }
    else
    {
        pVc12Alm->vc12Sf = 0;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1AlarmGet
* 功能描述: 获取STM帧的E1告警
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-04-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1AlarmGet(BYTE chipId, 
                                  BYTE portId, 
                                  BYTE aug1Id, 
                                  BYTE e1LinkId, 
                                  DRV_TDM_E1_ALARM *pE1Alm)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 alarmStatus = 0;  /* E1 alarm status */
    AtPdhDe1 pE1 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);  
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pE1Alm);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_ALM_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_atPdhE1PtrGet(chipId, portId, aug1Id, e1LinkId, &pE1);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_ALM_QRY, "ERROR: %s line %d, drv_tdm_atPdhE1PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pE1);
    
    /* get current alarm status of E1 */
    alarmStatus = AtChannelAlarmGet((AtChannel)pE1);

    memset(pE1Alm, 0, sizeof(DRV_TDM_E1_ALARM)); /* 先清空再赋值 */
    if (0 != (alarmStatus & cAtPdhDe1AlarmLof)) /* E1 loss of frame */
    {
        pE1Alm->e1Lof = 1;
    }
    else
    {
        pE1Alm->e1Lof = 0;
    }
    
    if (0 != (alarmStatus & cAtPdhDe1AlarmLomf)) /* E1 loss of multi-frame */
    {
        pE1Alm->e1Lomf = 1;
    }
    else
    {
        pE1Alm->e1Lomf = 0;
    }
    
    if (0 != (alarmStatus & cAtPdhDe1AlarmAis)) /* E1 alarm indication signal */
    {
        pE1Alm->e1Ais = 1;
    }
    else
    {
        pE1Alm->e1Ais = 0;
    }
    
    if (0 != (alarmStatus & cAtPdhDe1AlarmRai)) /* E1 remote alarm indication */
    {
        pE1Alm->e1Rai = 1;
    }
    else
    {
        pE1Alm->e1Rai = 0;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_sectionAlarmQuery
* 功能描述: 查询STM帧的段告警
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口的编号,取值为1~8.
* 输出参数: *pAlmResult: 保存段告警
* 返 回 值: 
* 其它说明: 本函数供产品管理调用.包括再生段告警和复用段告警
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_sectionAlarmQuery(BYTE chipId, BYTE portId, WORD32 *pAlmResult)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 tmpResult = 0;
    DRV_TDM_SECTION_ALARM secAlarm;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pAlmResult);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_ALM_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    memset(&secAlarm, 0, sizeof(DRV_TDM_SECTION_ALARM));
    rv = drv_tdm_sectionAlarmGet(chipId, portId, &secAlarm);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_ALM_QRY, "ERROR: %s line %d, drv_tdm_sectionAlarmGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    *pAlmResult = 0;   /* 赋值之前先清零 */
    tmpResult = tmpResult | (BSP_SDH_ALM_SECT_STM & (((WORD32)(secAlarm.rsTim)) << 0));
    tmpResult = tmpResult | (BSP_SDH_ALM_SECT_SD & (((WORD32)(secAlarm.msSd)) << 3));
    tmpResult = tmpResult | (BSP_SDH_ALM_SECT_AIS & (((WORD32)(secAlarm.msAis)) << 4));
    tmpResult = tmpResult | (BSP_SDH_ALM_SECT_RDI & (((WORD32)(secAlarm.msRdi)) << 5));
    tmpResult = tmpResult | (BSP_SDH_ALM_SECT_LOF & (((WORD32)(secAlarm.rsLof)) << 6));
    tmpResult = tmpResult | (BSP_SDH_ALM_SECT_LOS & (((WORD32)(secAlarm.rsLos)) << 8));
    tmpResult = tmpResult | (BSP_SDH_ALM_SECT_SF & (((WORD32)(secAlarm.msSf)) << 9));
    tmpResult = tmpResult | (BSP_SDH_ALM_SECT_RSSD & (((WORD32)(secAlarm.rsSd)) << 10));
    tmpResult = tmpResult | (BSP_SDH_ALM_SECT_RSSF & (((WORD32)(secAlarm.rsSf)) << 11));
    tmpResult = tmpResult | (BSP_SDH_ALM_SECT_OOF & (((WORD32)(secAlarm.rsOof)) << 12));
    *pAlmResult = tmpResult;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_highPathAlarmQuery
* 功能描述: 查询STM帧的高阶通道的告警
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~8.
* 输出参数: *pAlmResult: 保存高阶通道告警
* 返 回 值: 
* 其它说明: 本函数供产品管理调用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_highPathAlarmQuery(BYTE chipId, 
                                             BYTE portId, 
                                             BYTE aug1Id, 
                                             WORD32 *pAlmResult)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 tmpResult = 0;
    BYTE hpRdiAlm = 0;
    DRV_TDM_VC4_ALARM vc4Alarm;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(pAlmResult);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_ALM_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    memset(&vc4Alarm, 0, sizeof(DRV_TDM_VC4_ALARM));
    rv = drv_tdm_highPathAlarmGet(chipId, portId, aug1Id, &vc4Alarm);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_ALM_QRY, "ERROR: %s line %d, drv_tdm_highPathAlarmGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    if ((0 == vc4Alarm.vc4Rdi) 
        && (0 == vc4Alarm.vc4ERdiS) 
        && (0 == vc4Alarm.vc4ERdiP) 
        && (0 == vc4Alarm.vc4ERdiC))
    {
        hpRdiAlm = 0;
    }
    else
    {
        hpRdiAlm = 1;
    }
    
    *pAlmResult = 0;   /* 赋值之前先清零 */
    tmpResult = tmpResult | (BSP_SDH_ALM_HP_PTM & (((WORD32)(vc4Alarm.vc4Tim)) << 1));
    tmpResult = tmpResult | (BSP_SDH_ALM_HP_PSLM & (((WORD32)(vc4Alarm.vc4Plm)) << 3));
    tmpResult = tmpResult | (BSP_SDH_ALM_HP_UNEQP & (((WORD32)(vc4Alarm.vc4Uneq)) << 4));
    tmpResult = tmpResult | (BSP_SDH_ALM_HP_LOP & (((WORD32)(vc4Alarm.au4Lop)) << 5));
    tmpResult = tmpResult | (BSP_SDH_ALM_HP_PAIS & (((WORD32)(vc4Alarm.au4Ais)) << 6));
    tmpResult = tmpResult | (BSP_SDH_ALM_HP_PRDI & (((WORD32)hpRdiAlm) << 7));
    tmpResult = tmpResult | (BSP_SDH_ALM_HP_SD & (((WORD32)(vc4Alarm.vc4Sd)) << 8));
    tmpResult = tmpResult | (BSP_SDH_ALM_HP_SF & (((WORD32)(vc4Alarm.vc4Sf)) << 9));
    tmpResult = tmpResult | (BSP_SDH_ALM_HP_LOMF & (((WORD32)(vc4Alarm.vc4Lomf)) << 10));
    *pAlmResult = tmpResult;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_lowPathAlarmQuery
* 功能描述: 查询STM帧的低阶通道的告警
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
* 输出参数: *pAlmResult: 保存高阶通道告警
* 返 回 值: 
* 其它说明: 本函数供产品管理调用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_lowPathAlarmQuery(BYTE chipId, 
                                            BYTE portId, 
                                            BYTE aug1Id, 
                                            BYTE e1LinkId, 
                                            WORD32 *pAlmResult)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE lpRdi = 0;
    WORD32 tmpResult = 0;
    DRV_TDM_VC12_ALARM vc12Alarm;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pAlmResult);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_ALM_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    memset(&vc12Alarm, 0, sizeof(DRV_TDM_VC12_ALARM));
    rv = drv_tdm_lowPathAlarmGet(chipId, portId, aug1Id, e1LinkId, &vc12Alarm);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_ALM_QRY, "ERROR: %s line %d, drv_tdm_lowPathAlarmGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    if ((0 == vc12Alarm.vc12Rdi) 
        && (0 == vc12Alarm.vc12ERdiS) 
        && (0 == vc12Alarm.vc12ERdiP) 
        && (0 == vc12Alarm.vc12ERdiC))
    {
        lpRdi = 0;
    }
    else
    {
        lpRdi = 1;
    }
    
    *pAlmResult = 0;   /* 赋值之前先清零 */
    tmpResult = tmpResult | (BSP_SDH_ALM_LP_SD & (((WORD32)(vc12Alarm.vc12Sd)) << 0));
    tmpResult = tmpResult | (BSP_SDH_ALM_LP_SF & (((WORD32)(vc12Alarm.vc12Sf)) << 1));
    tmpResult = tmpResult | (BSP_SDH_ALM_LP_TIM & (((WORD32)(vc12Alarm.vc12Tim)) << 3));
    tmpResult = tmpResult | (BSP_SDH_ALM_LP_UNEQ & (((WORD32)(vc12Alarm.vc12Uneq)) << 5));
    tmpResult = tmpResult | (BSP_SDH_ALM_LP_PSLM & (((WORD32)(vc12Alarm.vc12Plm)) << 6));
    tmpResult = tmpResult | (BSP_SDH_ALM_LP_RDI & (((WORD32)lpRdi) << 9));
    tmpResult = tmpResult | (BSP_SDH_ALM_LP_LOP & (((WORD32)(vc12Alarm.tu12Lop)) << 10));
    tmpResult = tmpResult | (BSP_SDH_ALM_LP_AIS & (((WORD32)(vc12Alarm.tu12Ais)) << 11));
    *pAlmResult = tmpResult;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1AlarmQuery
* 功能描述: 查询E1告警
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
* 输出参数: *pAlmResult: 保存高阶通道告警
* 返 回 值: 
* 其它说明: 本函数供产品管理调用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1AlarmQuery(BYTE chipId, 
                                     BYTE portId, 
                                     BYTE aug1Id, 
                                     BYTE e1LinkId, 
                                     WORD32 *pAlmResult)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 tmpResult = 0;
    DRV_TDM_E1_ALARM e1Alarm;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pAlmResult);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_ALM_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    memset(&e1Alarm, 0, sizeof(DRV_TDM_E1_ALARM));
    rv = drv_tdm_e1AlarmGet(chipId, portId, aug1Id, e1LinkId, &e1Alarm);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_ALM_QRY, "ERROR: %s line %d, drv_tdm_e1AlarmGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    *pAlmResult = 0;   /* 赋值之前先清零 */
    tmpResult = tmpResult | (BSP_SDH_ALAM_E1_AIS & (((WORD32)(e1Alarm.e1Ais)) << 1));
    tmpResult = tmpResult | (BSP_SDH_ALAM_E1_LOF & (((WORD32)(e1Alarm.e1Lof)) << 2));
    tmpResult = tmpResult | (BSP_SDH_ALAM_E1_RAI & (((WORD32)(e1Alarm.e1Rai)) << 3));
    tmpResult = tmpResult | (BSP_SDH_ALAM_E1_SMLOF & (((WORD32)(e1Alarm.e1Lomf)) << 5));
    *pAlmResult = tmpResult;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_rsLosForce
* 功能描述: 强制插入RS-LOS告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           direction: 告警插入方向,具体参见DRV_TDM_TRANSFER_DIRECTION定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_rsLosForce(BYTE chipId, BYTE portId, WORD32 direction)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* Get SDH line. */
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);

    switch (direction)
    {
        case DRV_TDM_TX_DIRECTION:
            /* force RS-LOS to TX direction */
            rv = AtChannelTxAlarmForce((AtChannel)pLine, cAtSdhLineAlarmLos);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelTxAlarmForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        case DRV_TDM_RX_DIRECTION:
            /* Force RS-LOS to RX direction */
            rv = AtChannelRxAlarmForce((AtChannel)pLine, cAtSdhLineAlarmLos);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelRxAlarmForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, direciton=%u.\n", __FILE__, __LINE__, direction);
            return DRV_TDM_INVALID_ARGUMENT;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_rsLosUnForce
* 功能描述: 取消强制插入RS-LOS告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           direction: 告警插入方向,具体参见DRV_TDM_TRANSFER_DIRECTION定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_rsLosUnForce(BYTE chipId, BYTE portId, WORD32 direction)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* Get SDH line. */
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);

    switch (direction)
    {
        case DRV_TDM_TX_DIRECTION:
            /* UnForce RS-LOS to TX direction */
            rv = AtChannelTxAlarmUnForce((AtChannel)pLine, cAtSdhLineAlarmLos);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelTxAlarmUnForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        case DRV_TDM_RX_DIRECTION:
            /* UnForce RS-LOS to RX direction */
            rv = AtChannelRxAlarmUnForce((AtChannel)pLine, cAtSdhLineAlarmLos);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelRxAlarmUnForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, direciton=%u.\n", __FILE__, __LINE__, direction);
            return DRV_TDM_INVALID_ARGUMENT;
    }

    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_rsLofForce
* 功能描述: 强制插入RS-LOF告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           direction: 告警插入方向,具体参见DRV_TDM_TRANSFER_DIRECTION定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_rsLofForce(BYTE chipId, BYTE portId, WORD32 direction)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* Get SDH line. */
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    switch (direction)
    {
        case DRV_TDM_TX_DIRECTION:
            /* force RS-LOF to TX direction */
            rv = AtChannelTxAlarmForce((AtChannel)pLine, cAtSdhLineAlarmLof);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelTxAlarmForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        case DRV_TDM_RX_DIRECTION:
            /* Force RS-LOF to RX direction */
            rv = AtChannelRxAlarmForce((AtChannel)pLine, cAtSdhLineAlarmLof);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelRxAlarmForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, direciton=%u.\n", __FILE__, __LINE__, direction);
            return DRV_TDM_INVALID_ARGUMENT;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_rsLofUnForce
* 功能描述: 取消强制插入RS-LOF告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           direction: 告警插入方向,具体参见DRV_TDM_TRANSFER_DIRECTION定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_rsLofUnForce(BYTE chipId, BYTE portId, WORD32 direction)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* Get SDH line. */
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    switch (direction)
    {
        case DRV_TDM_TX_DIRECTION:
            /* UnForce RS-LOF to TX direction */
            rv = AtChannelTxAlarmUnForce((AtChannel)pLine, cAtSdhLineAlarmLof);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelTxAlarmUnForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        case DRV_TDM_RX_DIRECTION:
            /* UnForce RS-LOF to RX direction */
            rv = AtChannelRxAlarmUnForce((AtChannel)pLine, cAtSdhLineAlarmLof);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelRxAlarmUnForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, direciton=%u.\n", __FILE__, __LINE__, direction);
            return DRV_TDM_INVALID_ARGUMENT;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_msAisForce
* 功能描述: 强制插入MS-AIS告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           direction: 告警插入方向,具体参见DRV_TDM_TRANSFER_DIRECTION定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_msAisForce(BYTE chipId, BYTE portId, WORD32 direction)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* Get SDH line. */
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    switch (direction)
    {
        case DRV_TDM_TX_DIRECTION:  /* TX direction*/
            /* force MS-AIS to TX direction */
            rv = AtChannelTxAlarmForce((AtChannel)pLine, cAtSdhLineAlarmAis);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelTxAlarmForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        case DRV_TDM_RX_DIRECTION:  /* RX direction*/
            /* force MS-AIS to RX direction */
            rv = AtChannelRxAlarmForce((AtChannel)pLine, cAtSdhLineAlarmAis);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelRxAlarmForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, direciton=%u.\n", __FILE__, __LINE__, direction);
            return DRV_TDM_INVALID_ARGUMENT;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_msAisUnForce
* 功能描述: 取消强制插入MS-AIS告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           direction: 告警插入方向,具体参见DRV_TDM_TRANSFER_DIRECTION定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_msAisUnForce(BYTE chipId, BYTE portId, WORD32 direction)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* Get SDH line. */
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    switch (direction)
    {
        case DRV_TDM_TX_DIRECTION:  /* TX direction*/
            /* UnForce MS-AIS to TX direction */
            rv = AtChannelTxAlarmUnForce((AtChannel)pLine, cAtSdhLineAlarmAis);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelTxAlarmUnForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        case DRV_TDM_RX_DIRECTION:  /* RX direction*/
            /* UnForce MS-AIS to RX direction */
            rv = AtChannelRxAlarmUnForce((AtChannel)pLine, cAtSdhLineAlarmAis);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelRxAlarmUnForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, direciton=%u.\n", __FILE__, __LINE__, direction);
            return DRV_TDM_INVALID_ARGUMENT;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_au4AisForce
* 功能描述: 强制插入AU4-AIS告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           direction: 告警插入方向,具体参见DRV_TDM_TRANSFER_DIRECTION定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_au4AisForce(BYTE chipId, BYTE portId, BYTE aug1Id, WORD32 direction)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhAu pAu4 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhAu4PtrGet(chipId, portId, aug1Id, &pAu4);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_atSdhAu4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pAu4);
    
    switch (direction)
    {
        case DRV_TDM_TX_DIRECTION:  /* TX direction*/
            /* force AU4-AIS to TX direction */
            rv = AtChannelTxAlarmForce((AtChannel)pAu4, cAtSdhPathAlarmAis);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelTxAlarmForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        case DRV_TDM_RX_DIRECTION:  /* RX direction*/
            /* force AU4-AIS to RX direction */
            rv = AtChannelRxAlarmForce((AtChannel)pAu4, cAtSdhPathAlarmAis);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelRxAlarmForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, direciton=%u.\n", __FILE__, __LINE__, direction);
            return DRV_TDM_INVALID_ARGUMENT;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_au4AisUnForce
* 功能描述: 取消强制插入AU4-AIS告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           direction: 告警插入方向,具体参见DRV_TDM_TRANSFER_DIRECTION定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_au4AisUnForce(BYTE chipId, BYTE portId, BYTE aug1Id, WORD32 direction)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhAu pAu4 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhAu4PtrGet(chipId, portId, aug1Id, &pAu4);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_atSdhAu4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pAu4);
    
    switch (direction)
    {
        case DRV_TDM_TX_DIRECTION:  /* TX direction*/
            /* UnForce AU4-AIS to TX direction */
            rv = AtChannelTxAlarmUnForce((AtChannel)pAu4, cAtSdhPathAlarmAis);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelTxAlarmUnForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        case DRV_TDM_RX_DIRECTION:  /* RX direction*/
            /* UnForce AU4-AIS to RX direction */
            rv = AtChannelRxAlarmUnForce((AtChannel)pAu4, cAtSdhPathAlarmAis);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelRxAlarmUnForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, direciton=%u.\n", __FILE__, __LINE__, direction);
            return DRV_TDM_INVALID_ARGUMENT;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_au4LopForce
* 功能描述: 强制插入AU4-LOP告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           direction: 告警插入方向,具体参见DRV_TDM_TRANSFER_DIRECTION定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_au4LopForce(BYTE chipId, BYTE portId, BYTE aug1Id, WORD32 direction)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhAu pAu4 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhAu4PtrGet(chipId, portId, aug1Id, &pAu4);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_atSdhAu4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pAu4);
    
    switch (direction)
    {
        case DRV_TDM_TX_DIRECTION:  /* TX direction*/
            /* force AU4-LOP to TX direction */
            rv = AtChannelTxAlarmForce((AtChannel)pAu4, cAtSdhPathAlarmLop);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelTxAlarmForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        case DRV_TDM_RX_DIRECTION:  /* RX direction*/
            /* force AU4-LOP to RX direction */
            rv = AtChannelRxAlarmForce((AtChannel)pAu4, cAtSdhPathAlarmLop);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelRxAlarmForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, direciton=%u.\n", __FILE__, __LINE__, direction);
            return DRV_TDM_INVALID_ARGUMENT;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_au4LopUnForce
* 功能描述: 取消强制插入AU4-LOP告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           direction: 告警插入方向,具体参见DRV_TDM_TRANSFER_DIRECTION定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_au4LopUnForce(BYTE chipId, BYTE portId, BYTE aug1Id, WORD32 direction)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhAu pAu4 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhAu4PtrGet(chipId, portId, aug1Id, &pAu4);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_atSdhAu4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pAu4);
    
    switch (direction)
    {
        case DRV_TDM_TX_DIRECTION:  /* TX direction*/
            /* UnForce AU4-LOP to TX direction */
            rv = AtChannelTxAlarmUnForce((AtChannel)pAu4, cAtSdhPathAlarmLop);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelTxAlarmUnForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        case DRV_TDM_RX_DIRECTION:  /* RX direction*/
            /* UnForce AU4-LOP to RX direction */
            rv = AtChannelRxAlarmUnForce((AtChannel)pAu4, cAtSdhPathAlarmLop);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelRxAlarmUnForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, direciton=%u.\n", __FILE__, __LINE__, direction);
            return DRV_TDM_INVALID_ARGUMENT;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_vc4UneqForce
* 功能描述: 强制插入VC4-UNEQ告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           direction: 告警插入方向,具体参见DRV_TDM_TRANSFER_DIRECTION定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_vc4UneqForce(BYTE chipId, BYTE portId, BYTE aug1Id, WORD32 direction)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc4 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    
    switch (direction)
    {
        case DRV_TDM_TX_DIRECTION:  /* TX direction*/
            /* force VC4-UNEQ to TX direction */
            rv = AtChannelTxAlarmForce((AtChannel)pVc4, cAtSdhPathAlarmUneq);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelTxAlarmForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        case DRV_TDM_RX_DIRECTION:  /* RX direction*/
            /* force VC4-UNEQ to RX direction */
            rv = AtChannelRxAlarmForce((AtChannel)pVc4, cAtSdhPathAlarmUneq);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelRxAlarmForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, direciton=%u.\n", __FILE__, __LINE__, direction);
            return DRV_TDM_INVALID_ARGUMENT;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_vc4UneqUnForce
* 功能描述: 取消强制插入VC4-UNEQ告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           direction: 告警插入方向,具体参见DRV_TDM_TRANSFER_DIRECTION定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_vc4UneqUnForce(BYTE chipId, BYTE portId, BYTE aug1Id, WORD32 direction)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc4 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    
    switch (direction)
    {
        case DRV_TDM_TX_DIRECTION:  /* TX direction*/
            /* UnForce VC4-UNEQ to TX direction */
            rv = AtChannelTxAlarmUnForce((AtChannel)pVc4, cAtSdhPathAlarmUneq);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelTxAlarmUnForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        case DRV_TDM_RX_DIRECTION:  /* RX direction*/
            /* UnForce VC4-UNEQ to RX direction */
            rv = AtChannelRxAlarmUnForce((AtChannel)pVc4, cAtSdhPathAlarmUneq);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelRxAlarmUnForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, direciton=%u.\n", __FILE__, __LINE__, direction);
            return DRV_TDM_INVALID_ARGUMENT;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_tu12AisForce
* 功能描述: 强制插入TU12-AIS告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路的编号,取值为1~63.
*           direction: 告警插入方向,具体参见DRV_TDM_TRANSFER_DIRECTION定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_tu12AisForce(BYTE chipId, 
                                    BYTE portId, 
                                    BYTE aug1Id, 
                                    BYTE e1LinkId, 
                                    WORD32 direction)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhTu pTu12 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_atSdhTu12PtrGet(chipId, portId, aug1Id, e1LinkId, &pTu12);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, drv_tdm_atSdhTu12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pTu12);
    
    switch (direction)
    {
        case DRV_TDM_TX_DIRECTION:  /* TX direction*/
            /* force TU12-AIS to TX direction */
            rv = AtChannelTxAlarmForce((AtChannel)pTu12, cAtSdhPathAlarmAis);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, AtChannelTxAlarmForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        case DRV_TDM_RX_DIRECTION:  /* RX direction*/
            /* force TU12-AIS to RX direction */
            rv = AtChannelRxAlarmForce((AtChannel)pTu12, cAtSdhPathAlarmAis);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, AtChannelRxAlarmForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, direciton=%u.\n", __FILE__, __LINE__, direction);
            return DRV_TDM_INVALID_ARGUMENT;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_tu12AisUnForce
* 功能描述: 取消强制插入TU12-AIS告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路的编号,取值为1~63.
*           direction: 告警插入方向,具体参见DRV_TDM_TRANSFER_DIRECTION定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_tu12AisUnForce(BYTE chipId, 
                                        BYTE portId, 
                                        BYTE aug1Id, 
                                        BYTE e1LinkId, 
                                        WORD32 direction)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhTu pTu12 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_atSdhTu12PtrGet(chipId, portId, aug1Id, e1LinkId, &pTu12);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, drv_tdm_atSdhTu12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pTu12);
    
    switch (direction)
    {
        case DRV_TDM_TX_DIRECTION:  /* TX direction*/
            /* UnForce TU12-AIS to TX direction */
            rv = AtChannelTxAlarmUnForce((AtChannel)pTu12, cAtSdhPathAlarmAis);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, AtChannelTxAlarmUnForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        case DRV_TDM_RX_DIRECTION:  /* RX direction*/
            /* UnForce TU12-AIS to RX direction */
            rv = AtChannelRxAlarmUnForce((AtChannel)pTu12, cAtSdhPathAlarmAis);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, AtChannelRxAlarmUnForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, direciton=%u.\n", __FILE__, __LINE__, direction);
            return DRV_TDM_INVALID_ARGUMENT;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_tu12LopForce
* 功能描述: 强制插入TU12-LOP告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路的编号,取值为1~63.
*           direction: 告警插入方向,具体参见DRV_TDM_TRANSFER_DIRECTION定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_tu12LopForce(BYTE chipId, 
                                    BYTE portId, 
                                    BYTE aug1Id, 
                                    BYTE e1LinkId, 
                                    WORD32 direction)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhTu pTu12 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhTu12PtrGet(chipId, portId, aug1Id, e1LinkId, &pTu12);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, drv_tdm_atSdhTu12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pTu12);
    
    switch (direction)
    {
        case DRV_TDM_TX_DIRECTION:  /* TX direction*/
            /* force TU12-LOP to TX direction */
            rv = AtChannelTxAlarmForce((AtChannel)pTu12, cAtSdhPathAlarmLop);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, AtChannelTxAlarmForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        case DRV_TDM_RX_DIRECTION:  /* RX direction*/
            /* force TU12-LOP to RX direction */
            rv = AtChannelRxAlarmForce((AtChannel)pTu12, cAtSdhPathAlarmLop);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, AtChannelRxAlarmForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, direciton=%u.\n", __FILE__, __LINE__, direction);
            return DRV_TDM_INVALID_ARGUMENT;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_tu12LopUnForce
* 功能描述: 取消强制插入TU12-LOP告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路的编号,取值为1~63.
*           direction: 告警插入方向,具体参见DRV_TDM_TRANSFER_DIRECTION定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_tu12LopUnForce(BYTE chipId, 
                                        BYTE portId, 
                                        BYTE aug1Id, 
                                        BYTE e1LinkId, 
                                        WORD32 direction)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhTu pTu12 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhTu12PtrGet(chipId, portId, aug1Id, e1LinkId, &pTu12);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, drv_tdm_atSdhTu12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pTu12);
    
    switch (direction)
    {
        case DRV_TDM_TX_DIRECTION:  /* TX direction*/
            /* UnForce TU12-LOP to TX direction */
            rv = AtChannelTxAlarmUnForce((AtChannel)pTu12, cAtSdhPathAlarmLop);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, AtChannelTxAlarmUnForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        case DRV_TDM_RX_DIRECTION:  /* RX direction*/
            /* UnForce TU12-LOP to RX direction */
            rv = AtChannelRxAlarmUnForce((AtChannel)pTu12, cAtSdhPathAlarmLop);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, AtChannelRxAlarmUnForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, direciton=%u.\n", __FILE__, __LINE__, direction);
            return DRV_TDM_INVALID_ARGUMENT;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_tu12LomfForce
* 功能描述: 强制插入TU12-LOMF告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           direction: 告警插入方向,具体参见DRV_TDM_TRANSFER_DIRECTION定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_tu12LomfForce(BYTE chipId, 
                                      BYTE portId, 
                                      BYTE aug1Id, 
                                      WORD32 direction)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhAu pAu4 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhAu4PtrGet(chipId, portId, aug1Id, &pAu4);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhAu4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pAu4);
    
    switch (direction)
    {
        case DRV_TDM_TX_DIRECTION:  /* TX direction*/
            /* force TU12-LOMF to TX direction */
            rv = AtChannelTxAlarmForce((AtChannel)pAu4, cAtSdhPathAlarmLom);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u, AtChannelTxAlarmForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, aug1Id, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        case DRV_TDM_RX_DIRECTION:  /* RX direction*/
            /* force TU12-LOMF to RX direction */
            rv = AtChannelRxAlarmForce((AtChannel)pAu4, cAtSdhPathAlarmLom);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u, AtChannelRxAlarmForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, aug1Id, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, direciton=%u.\n", __FILE__, __LINE__, direction);
            return DRV_TDM_INVALID_ARGUMENT;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_tu12LomfUnForce
* 功能描述: 取消强制插入TU12-LOMF告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           direction: 告警插入方向,具体参见DRV_TDM_TRANSFER_DIRECTION定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_tu12LomfUnForce(BYTE chipId, 
                                         BYTE portId, 
                                         BYTE aug1Id, 
                                         WORD32 direction)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhAu pAu4 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhAu4PtrGet(chipId, portId, aug1Id, &pAu4);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhAu4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pAu4);
    
    switch (direction)
    {
        case DRV_TDM_TX_DIRECTION:  /* TX direction*/
            /* UnForce TU12-LOMF to TX direction */
            rv = AtChannelTxAlarmUnForce((AtChannel)pAu4, cAtSdhPathAlarmLom);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u, AtChannelTxAlarmUnForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, aug1Id, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        case DRV_TDM_RX_DIRECTION:  /* RX direction*/
            /* UnForce TU12-LOMF to RX direction */
            rv = AtChannelRxAlarmUnForce((AtChannel)pAu4, cAtSdhPathAlarmLom);
            if (cAtOk != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u, AtChannelRxAlarmUnForce() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, aug1Id, rv);
                return DRV_TDM_SDK_API_FAIL;
            }
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, direciton=%u.\n", __FILE__, __LINE__, direction);
            return DRV_TDM_INVALID_ARGUMENT;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_secAlmAffectingEnable
* 功能描述: 当有段层告警时,是否允许下插AIS及回告RDI.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           alarmType: 段层告警类型,具体参见DRV_TDM_SECTION_ALARM_TYPE定义.
*           enableFlag: 使能标记,具体参见DRV_TDM_BOOL定义.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-31   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_secAlmAffectingEnable(BYTE chipId, 
                                                 BYTE portId, 
                                                 WORD32 alarmType, 
                                                 WORD32 enableFlag)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 atAlarmType = 0;
    eBool atEnableFlag = cAtFalse;
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* Get SDH line. */
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);

    switch (alarmType)
    {
        case DRV_TDM_SDH_RS_LOS:
            atAlarmType = cAtSdhLineAlarmLos;
            break;
        case DRV_TDM_SDH_RS_OOF:
            atAlarmType = cAtSdhLineAlarmOof;
            break;
        case DRV_TDM_SDH_RS_LOF:
            atAlarmType = cAtSdhLineAlarmLof;
            break;
        case DRV_TDM_SDH_RS_TIM:
            atAlarmType = cAtSdhLineAlarmTim;
            break;
        case DRV_TDM_SDH_RS_SD:
            atAlarmType = cAtSdhLineAlarmRsBerSd;
            break;
        case DRV_TDM_SDH_RS_SF:
            atAlarmType = cAtSdhLineAlarmRsBerSf;
            break;
        case DRV_TDM_SDH_MS_AIS:
            atAlarmType = cAtSdhLineAlarmAis;
            break;
        case DRV_TDM_SDH_MS_RDI:
            atAlarmType = cAtSdhLineAlarmRdi;
            break;
        case DRV_TDM_SDH_MS_SD:
            atAlarmType = cAtSdhLineAlarmBerSd;
            break;
        case DRV_TDM_SDH_MS_SF:
            atAlarmType = cAtSdhLineAlarmBerSf;
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, invalid alarm type, alarmType=0x%x.\n", __FILE__, __LINE__, chipId, portId, alarmType);
            return DRV_TDM_INVALID_ARGUMENT;
    }

    if (DRV_TDM_FALSE == enableFlag)
    {
        atEnableFlag = cAtFalse;
    }
    else
    {
        atEnableFlag = cAtTrue;
    }
    
    /* Enable/disable AIS downstream and RDI to remote side */
    rv = AtSdhChannelAlarmAffectingEnable((AtSdhChannel)pLine, atAlarmType, atEnableFlag);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelAlarmAffectingEnable() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_hpAlmAffectingEnable
* 功能描述: 当有高阶通道告警时,是否允许下插AIS及回告RDI.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           alarmType: 高阶通道告警类型,具体参见DRV_TDM_HP_ALARM_TYPE定义.
*           enableFlag: 使能标记,具体参见DRV_TDM_BOOL定义.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-31   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_hpAlmAffectingEnable(BYTE chipId, 
                                               BYTE portId, 
                                               BYTE aug1Id, 
                                               WORD32 alarmType, 
                                               WORD32 enableFlag)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 atAlarmType = 0;
    eBool atEnableFlag = cAtFalse;
    AtSdhVc pVc4 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    
    switch (alarmType)
    {
        case DRV_TDM_SDH_AU4_AIS:
            atAlarmType = cAtSdhPathAlarmAis;
            break;
        case DRV_TDM_SDH_AU4_LOP:
            atAlarmType = cAtSdhPathAlarmLop;
            break;
        case DRV_TDM_SDH_VC4_TIM:
            atAlarmType = cAtSdhPathAlarmTim;
            break;
        case DRV_TDM_SDH_VC4_UNEQ:
            atAlarmType = cAtSdhPathAlarmUneq;
            break;
        case DRV_TDM_SDH_VC4_PLM:
            atAlarmType = cAtSdhPathAlarmPlm;
            break;
        case DRV_TDM_SDH_VC4_RDI:
            atAlarmType = cAtSdhPathAlarmRdi;
            break;
        case DRV_TDM_SDH_VC4_SD:
            atAlarmType = cAtSdhPathAlarmBerSd;
            break;
        case DRV_TDM_SDH_VC4_SF:
            atAlarmType = cAtSdhPathAlarmBerSf;
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, invalid alarm type, alarmType=0x%x.\n", __FILE__, __LINE__, chipId, portId, alarmType);
            return DRV_TDM_INVALID_ARGUMENT;
    }
    
    if (DRV_TDM_FALSE == enableFlag)
    {
        atEnableFlag = cAtFalse;
    }
    else
    {
        atEnableFlag = cAtTrue;
    }
    
    /* Enable/disable AIS downstream and RDI to remote side. */
    rv = AtSdhChannelAlarmAffectingEnable((AtSdhChannel)pVc4, atAlarmType, atEnableFlag);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelAlarmAffectingEnable() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_lpAlmAffectingEnable
* 功能描述: 当有低阶通道告警时,是否允许下插AIS及回告RDI.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
*           alarmType: 低阶通道告警类型,具体参见DRV_TDM_LP_ALARM_TYPE定义.
*           enableFlag: 使能标记,具体参见DRV_TDM_BOOL定义.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-31   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_lpAlmAffectingEnable(BYTE chipId, 
                                               BYTE portId, 
                                               BYTE aug1Id, 
                                               BYTE e1LinkId, 
                                               WORD32 alarmType, 
                                               WORD32 enableFlag)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 atAlarmType = 0;
    eBool atEnableFlag = cAtFalse;
    AtSdhVc pVc12 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);
    
    switch (alarmType)
    {
        case DRV_TDM_SDH_TU12_AIS:
            atAlarmType = cAtSdhPathAlarmAis;
            break;
        case DRV_TDM_SDH_TU12_LOP:
            atAlarmType = cAtSdhPathAlarmLop;
            break;
        case DRV_TDM_SDH_TU12_LOMF:
            atAlarmType = cAtSdhPathAlarmLom;
            break;
        case DRV_TDM_SDH_VC12_TIM:
            atAlarmType = cAtSdhPathAlarmTim;
            break;
        case DRV_TDM_SDH_VC12_UNEQ:
            atAlarmType = cAtSdhPathAlarmUneq;
            break;
        case DRV_TDM_SDH_VC12_PLM:
            atAlarmType = cAtSdhPathAlarmPlm;
            break;
        case DRV_TDM_SDH_VC12_RDI:
            atAlarmType = cAtSdhPathAlarmRdi;
            break;
        case DRV_TDM_SDH_VC12_SD:
            atAlarmType = cAtSdhPathAlarmBerSd;
            break;
        case DRV_TDM_SDH_VC12_SF:
            atAlarmType = cAtSdhPathAlarmBerSf;
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, invalid alarm type, alarmType=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, alarmType);
            return DRV_TDM_INVALID_ARGUMENT;
    }
    
    if (DRV_TDM_FALSE == enableFlag)
    {
        atEnableFlag = cAtFalse;
    }
    else
    {
        atEnableFlag = cAtTrue;
    }
    
    /* Enable/disable AIS downstream and RDI to remote side. */
    rv = AtSdhChannelAlarmAffectingEnable((AtSdhChannel)pVc12, atAlarmType, atEnableFlag);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelAlarmAffectingEnable() rv=%s.\n", __FILE__, __LINE__, AtRet2String(rv));
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_interruptEnable
* 功能描述: 设置中断是否使能.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4. 
*           enableFlag: 中断使能标记,具体参见DRV_TDM_STM_INTERRUPT_STATE定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-13   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_interruptEnable(BYTE subslotId, WORD32 enableFlag)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;
    BYTE ipCoresNum = 0;
    BYTE coreId = 0;
    eBool intrEnable = cAtFalse;
    AtDevice pDevice = NULL;
    AtIpCore pIpCore = NULL;
    AtModuleSdh pSdhModule = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;

    if (DRV_TDM_STM_INTERRUPT_DISABLE == enableFlag)
    {
        intrEnable = cAtFalse;
    }
    else if (DRV_TDM_STM_INTERRUPT_ENABLE == enableFlag)
    {
        intrEnable = cAtTrue;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, invalid parameter, enableFlag=%u.\n", __FILE__, __LINE__, chipId, enableFlag);
        return DRV_TDM_INVALID_ARGUMENT;
    }
    
    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);

    ipCoresNum = AtDeviceNumIpCoresGet(pDevice); /* Get number of IP cores */
    for (coreId = 0; coreId < ipCoresNum; coreId++)
    {
        pIpCore = AtDeviceIpCoreGet(pDevice, coreId); /* Get IP core object by its ID. */
        if (NULL == pIpCore)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u coreId %u, AtDeviceIpCoreGet() unsuccessfully.\n", __FILE__, __LINE__, chipId, coreId);
            return DRV_TDM_SDK_API_FAIL;
        }

        /* Enable/disable interrupt on this core */
        rv = AtIpCoreInterruptEnable(pIpCore, intrEnable);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u coreId %u, AtIpCoreInterruptEnable() rv=0x%x.\n", __FILE__, __LINE__, chipId, coreId, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
    }
     
    pSdhModule = (AtModuleSdh)AtDeviceModuleGet(pDevice, cAtModuleSdh); /* get SDH module */
    DRV_TDM_CHECK_POINTER_IS_NULL(pSdhModule);

    /* Enable/disable interrupt on sdh module */
    rv = AtModuleInterruptEnable((AtModule)pSdhModule, intrEnable);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, AtModuleInterruptEnable() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_secAlmIntrStatusGet
* 功能描述: 获取STM断层告警的中断状态.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4. 
*           portId: STM端口的编号,取值为1~8.
* 输出参数: *pIntrStatus: 保存中断状态.
* 返 回 值: 
* 其它说明: 该函数用来获取中断状态,同时会关闭中断.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-13   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_secAlmIntrStatusGet(BYTE subslotId, BYTE portId, WORD32 *pIntrStatus)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;
    WORD32 atIntrStatus = 0;   /* ARRIVE芯片的中断状态 */
    WORD32 tmpIntrStatus = 0;  /* 临时变量 */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pIntrStatus);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_INTR_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    /* Get SDH line. */
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_INTR_MSG, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);

    /* Get alarm interrupt change status. FPGA will not generate new interrupt alarm inside. */
    atIntrStatus = AtChannelAlarmInterruptGet((AtChannel)pLine);
    
    if (0 != (atIntrStatus & cAtSdhLineAlarmLos))
    {
        tmpIntrStatus = tmpIntrStatus | DRV_TDM_RSLOS_INTR_STATUS_BIT;
    }
    else
    {
        tmpIntrStatus = tmpIntrStatus & (~DRV_TDM_RSLOS_INTR_STATUS_BIT);
    }

    if (0 != (atIntrStatus & cAtSdhLineAlarmOof))
    {
        tmpIntrStatus = tmpIntrStatus | DRV_TDM_RSOOF_INTR_STATUS_BIT;
    }
    else
    {
        tmpIntrStatus = tmpIntrStatus & (~DRV_TDM_RSOOF_INTR_STATUS_BIT);
    }

    if (0 != (atIntrStatus & cAtSdhLineAlarmLof))
    {
        tmpIntrStatus = tmpIntrStatus | DRV_TDM_RSLOF_INTR_STATUS_BIT;
    }
    else
    {
        tmpIntrStatus = tmpIntrStatus & (~DRV_TDM_RSLOF_INTR_STATUS_BIT);
    }

    if (0 != (atIntrStatus & cAtSdhLineAlarmTim))
    {
        tmpIntrStatus = tmpIntrStatus | DRV_TDM_RSTIM_INTR_STATUS_BIT;
    }
    else
    {
        tmpIntrStatus = tmpIntrStatus & (~DRV_TDM_RSTIM_INTR_STATUS_BIT);
    }

    if (0 != (atIntrStatus & cAtSdhLineAlarmAis))
    {
        tmpIntrStatus = tmpIntrStatus | DRV_TDM_MSAIS_INTR_STATUS_BIT;
    }
    else
    {
        tmpIntrStatus = tmpIntrStatus & (~DRV_TDM_MSAIS_INTR_STATUS_BIT);
    }

    if (0 != (atIntrStatus & cAtSdhLineAlarmRdi))
    {
        tmpIntrStatus = tmpIntrStatus | DRV_TDM_MSRDI_INTR_STATUS_BIT;
    }
    else
    {
        tmpIntrStatus = tmpIntrStatus & (~DRV_TDM_MSRDI_INTR_STATUS_BIT);
    }

    if (0 != (atIntrStatus & cAtSdhLineAlarmBerSd))   /* MS-SD*/
    {
        tmpIntrStatus = tmpIntrStatus | DRV_TDM_MSSD_INTR_STATUS_BIT;
    }
    else
    {
        tmpIntrStatus = tmpIntrStatus & (~DRV_TDM_MSSD_INTR_STATUS_BIT);
    }

    if (0 != (atIntrStatus & cAtSdhLineAlarmBerSf))  /* MS-SF */
    {
        tmpIntrStatus = tmpIntrStatus | DRV_TDM_MSSF_INTR_STATUS_BIT;
    }
    else
    {
        tmpIntrStatus = tmpIntrStatus & (~DRV_TDM_MSSF_INTR_STATUS_BIT);
    }

    if (0 != (atIntrStatus & cAtSdhLineAlarmKByteChange))
    {
        tmpIntrStatus = tmpIntrStatus | DRV_TDM_KBYTE_INTR_STATUS_BIT;
    }
    else
    {
        tmpIntrStatus = tmpIntrStatus & (~DRV_TDM_KBYTE_INTR_STATUS_BIT);
    }

    if (0 != (atIntrStatus & cAtSdhLineAlarmKByteFail))
    {
        tmpIntrStatus = tmpIntrStatus | DRV_TDM_K_FAIL_INTR_STATUS_BIT;
    }
    else
    {
        tmpIntrStatus = tmpIntrStatus & (~DRV_TDM_K_FAIL_INTR_STATUS_BIT);
    }

    if (0 != (atIntrStatus & cAtSdhLineAlarmS1Change))
    {
        tmpIntrStatus = tmpIntrStatus | DRV_TDM_S1BYTE_INTR_STATUS_BIT;
    }
    else
    {
        tmpIntrStatus = tmpIntrStatus & (~DRV_TDM_S1BYTE_INTR_STATUS_BIT);
    }

    if (0 != (atIntrStatus & cAtSdhLineAlarmRsBerSd))  /* RS-SD */
    {
        tmpIntrStatus = tmpIntrStatus | DRV_TDM_RSSD_INTR_STATUS_BIT;
    }
    else
    {
        tmpIntrStatus = tmpIntrStatus & (~DRV_TDM_RSSD_INTR_STATUS_BIT);
    }

    if (0 != (atIntrStatus & cAtSdhLineAlarmRsBerSf))  /* RS-SF */
    {
        tmpIntrStatus = tmpIntrStatus | DRV_TDM_RSSF_INTR_STATUS_BIT;
    }
    else
    {
        tmpIntrStatus = tmpIntrStatus & (~DRV_TDM_RSSF_INTR_STATUS_BIT);
    }

    *pIntrStatus = 0;  /* 先清零再赋值 */
    *pIntrStatus = tmpIntrStatus;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_secAlmIntrClear
* 功能描述: 清除STM断层告警的中断.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4. 
*           portId: STM端口的编号,取值为1~8.
* 输出参数: 
* 返 回 值: 
* 其它说明: 清除中断的历史状态,同时会重新打开中断.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-13   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_secAlmIntrClear(BYTE subslotId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;
    WORD32 intrStatus = 0;  /* 历史告警的中断状态 */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_INTR_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* Get SDH line. */
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_INTR_MSG, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);

    /* FPGA will clear interrupt alarm and generate new interrupt alarm */
    intrStatus = AtChannelAlarmInterruptClear((AtChannel)pLine);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_secAlmIntrMaskGet
* 功能描述: 获取STM断层告警的中断屏蔽.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4. 
*           portId: STM端口的编号,取值为1~8.
* 输出参数: *pIntrMask: 保存中断屏蔽.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-13   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_secAlmIntrMaskGet(BYTE subslotId, BYTE portId, WORD32 *pIntrMask)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;
    WORD32 atIntrMask = 0;
    WORD32 tmpIntrMask = 0;
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pIntrMask);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_INTR_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* Get SDH line. */
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_INTR_MSG, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);

    /* Get interrupt mask */
    atIntrMask = AtChannelInterruptMaskGet((AtChannel)pLine);
    
    if (0 != (atIntrMask & cAtSdhLineAlarmLos))
    {
        tmpIntrMask = tmpIntrMask | DRV_TDM_RSLOS_INTR_MASK_BIT;
    }
    else
    {
        tmpIntrMask = tmpIntrMask & (~DRV_TDM_RSLOS_INTR_MASK_BIT);
    }
    
    if (0 != (atIntrMask & cAtSdhLineAlarmOof))
    {
        tmpIntrMask = tmpIntrMask | DRV_TDM_RSOOF_INTR_MASK_BIT;
    }
    else
    {
        tmpIntrMask = tmpIntrMask & (~DRV_TDM_RSOOF_INTR_MASK_BIT);
    }
    
    if (0 != (atIntrMask & cAtSdhLineAlarmLof))
    {
        tmpIntrMask = tmpIntrMask | DRV_TDM_RSLOF_INTR_MASK_BIT;
    }
    else
    {
        tmpIntrMask = tmpIntrMask & (~DRV_TDM_RSLOF_INTR_MASK_BIT);
    }
    
    if (0 != (atIntrMask & cAtSdhLineAlarmTim))
    {
        tmpIntrMask = tmpIntrMask | DRV_TDM_RSTIM_INTR_MASK_BIT;
    }
    else
    {
        tmpIntrMask = tmpIntrMask & (~DRV_TDM_RSTIM_INTR_MASK_BIT);
    }
    
    if (0 != (atIntrMask & cAtSdhLineAlarmAis))
    {
        tmpIntrMask = tmpIntrMask | DRV_TDM_MSAIS_INTR_MASK_BIT;
    }
    else
    {
        tmpIntrMask = tmpIntrMask & (~DRV_TDM_MSAIS_INTR_MASK_BIT);
    }
    
    if (0 != (atIntrMask & cAtSdhLineAlarmRdi))
    {
        tmpIntrMask = tmpIntrMask | DRV_TDM_MSRDI_INTR_MASK_BIT;
    }
    else
    {
        tmpIntrMask = tmpIntrMask & (~DRV_TDM_MSRDI_INTR_MASK_BIT);
    }
    
    if (0 != (atIntrMask & cAtSdhLineAlarmBerSd))  /* MS-SD */
    {
        tmpIntrMask = tmpIntrMask | DRV_TDM_MSSD_INTR_MASK_BIT;
    }
    else
    {
        tmpIntrMask = tmpIntrMask & (~DRV_TDM_MSSD_INTR_MASK_BIT);
    }
    
    if (0 != (atIntrMask & cAtSdhLineAlarmBerSf))  /* MS-SF */
    {
        tmpIntrMask = tmpIntrMask | DRV_TDM_MSSF_INTR_MASK_BIT;
    }
    else
    {
        tmpIntrMask = tmpIntrMask & (~DRV_TDM_MSSF_INTR_MASK_BIT);
    }
    
    if (0 != (atIntrMask & cAtSdhLineAlarmKByteChange))
    {
        tmpIntrMask = tmpIntrMask | DRV_TDM_KBYTE_INTR_MASK_BIT;
    }
    else
    {
        tmpIntrMask = tmpIntrMask & (~DRV_TDM_KBYTE_INTR_MASK_BIT);
    }
    
    if (0 != (atIntrMask & cAtSdhLineAlarmKByteFail))
    {
        tmpIntrMask = tmpIntrMask | DRV_TDM_K_FAIL_INTR_MASK_BIT;
    }
    else
    {
        tmpIntrMask = tmpIntrMask & (~DRV_TDM_K_FAIL_INTR_MASK_BIT);
    }
    
    if (0 != (atIntrMask & cAtSdhLineAlarmS1Change))
    {
        tmpIntrMask = tmpIntrMask | DRV_TDM_S1BYTE_INTR_MASK_BIT;
    }
    else
    {
        tmpIntrMask = tmpIntrMask & (~DRV_TDM_S1BYTE_INTR_MASK_BIT);
    }

    if (0 != (atIntrMask & cAtSdhLineAlarmRsBerSd))  /* RS-SD */
    {
        tmpIntrMask = tmpIntrMask | DRV_TDM_RSSD_INTR_MASK_BIT;
    }
    else
    {
        tmpIntrMask = tmpIntrMask & (~DRV_TDM_RSSD_INTR_MASK_BIT);
    }

    if (0 != (atIntrMask & cAtSdhLineAlarmRsBerSf))  /* RS-SF */
    {
        tmpIntrMask = tmpIntrMask | DRV_TDM_RSSF_INTR_MASK_BIT;
    }
    else
    {
        tmpIntrMask = tmpIntrMask & (~DRV_TDM_RSSF_INTR_MASK_BIT);
    }
    
    *pIntrMask = 0;   /* 先清零再赋值 */
    *pIntrMask = tmpIntrMask;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_secAlmIntrMaskSet
* 功能描述: 设置STM断层告警的中断屏蔽.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4. 
*           portId: STM端口的编号,取值为1~8.
*           maskBmp: 中断屏蔽的位图.
*           maskEnBmp: 中断屏蔽的使能位图.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-13   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_secAlmIntrMaskSet(BYTE subslotId, 
                                           BYTE portId, 
                                           WORD32 maskBmp, 
                                           WORD32 maskEnBmp)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;
    WORD32 atDefectMask = 0;
    WORD32 atEnableMask = 0;
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_INTR_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* Get SDH line. */
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_INTR_MSG, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);

    if (0 != (maskBmp & DRV_TDM_RSLOS_INTR_MASK_BIT))
    {
        atDefectMask = atDefectMask | cAtSdhLineAlarmLos;
    }
    else
    {
        atDefectMask = atDefectMask & (~cAtSdhLineAlarmLos);
    }

    if (0 != (maskBmp & DRV_TDM_RSLOF_INTR_MASK_BIT))
    {
        atDefectMask = atDefectMask | cAtSdhLineAlarmLof;
    }
    else
    {
        atDefectMask = atDefectMask & (~cAtSdhLineAlarmLof);
    }

    if (0 != (maskBmp & DRV_TDM_RSOOF_INTR_MASK_BIT))
    {
        atDefectMask = atDefectMask | cAtSdhLineAlarmOof;
    }
    else
    {
        atDefectMask = atDefectMask & (~cAtSdhLineAlarmOof);
    }

    if (0 != (maskBmp & DRV_TDM_RSTIM_INTR_MASK_BIT))
    {
        atDefectMask = atDefectMask | cAtSdhLineAlarmTim;
    }
    else
    {
        atDefectMask = atDefectMask & (~cAtSdhLineAlarmTim);
    }

    if (0 != (maskBmp & DRV_TDM_MSAIS_INTR_MASK_BIT))
    {
        atDefectMask = atDefectMask | cAtSdhLineAlarmAis;
    }
    else
    {
        atDefectMask = atDefectMask & (~cAtSdhLineAlarmAis);
    }

    if (0 != (maskBmp & DRV_TDM_MSRDI_INTR_MASK_BIT))
    {
        atDefectMask = atDefectMask | cAtSdhLineAlarmRdi;
    }
    else
    {
        atDefectMask = atDefectMask & (~cAtSdhLineAlarmRdi); 
    }

    if (0 != (maskBmp & DRV_TDM_MSSD_INTR_MASK_BIT))  /* MS-SD */
    {
        atDefectMask = atDefectMask | cAtSdhLineAlarmBerSd;
        
    }
    else
    {
        atDefectMask = atDefectMask & (~cAtSdhLineAlarmBerSd);
    }

    if (0 != (maskBmp & DRV_TDM_MSSF_INTR_MASK_BIT))  /* MS-SF */
    {
        atDefectMask = atDefectMask | cAtSdhLineAlarmBerSf;
    }
    else
    {
        atDefectMask = atDefectMask & (~cAtSdhLineAlarmBerSf);
    }

    if (0 != (maskBmp & DRV_TDM_KBYTE_INTR_MASK_BIT))
    {
        atDefectMask = atDefectMask | cAtSdhLineAlarmKByteChange;
    }
    else
    {
        atDefectMask = atDefectMask & (~cAtSdhLineAlarmKByteChange);
    }

    if (0 != (maskBmp & DRV_TDM_K_FAIL_INTR_MASK_BIT))
    {
        atDefectMask = atDefectMask | cAtSdhLineAlarmKByteFail;
    }
    else
    {
        atDefectMask = atDefectMask & (~cAtSdhLineAlarmKByteFail);
    }

    if (0 != (maskBmp & DRV_TDM_S1BYTE_INTR_MASK_BIT))
    {
        atDefectMask = atDefectMask | cAtSdhLineAlarmS1Change;
    }
    else
    {
        atDefectMask = atDefectMask & (~cAtSdhLineAlarmS1Change);
    }

    if (0 != (maskBmp & DRV_TDM_RSSD_INTR_MASK_BIT))  /* RS-SD */
    {
        atDefectMask = atDefectMask | cAtSdhLineAlarmRsBerSd;
    }
    else
    {
        atDefectMask = atDefectMask & (~cAtSdhLineAlarmRsBerSd);
    }

    if (0 != (maskBmp & DRV_TDM_RSSF_INTR_MASK_BIT))  /* RS-SF */
    {
        atDefectMask = atDefectMask | cAtSdhLineAlarmRsBerSf;
    }
    else
    {
        atDefectMask = atDefectMask & (~cAtSdhLineAlarmRsBerSf);
    }

    if (0 != (maskEnBmp & DRV_TDM_RSLOS_INTR_MASK_STATE))
    {
        atEnableMask = atEnableMask | cAtSdhLineAlarmLos;
    }
    else
    {
        atEnableMask = atEnableMask & (~cAtSdhLineAlarmLos);
    }

    if (0 != (maskEnBmp & DRV_TDM_RSLOF_INTR_MASK_STATE))
    {
        atEnableMask = atEnableMask | cAtSdhLineAlarmLof;
    }
    else
    {
        atEnableMask = atEnableMask & (~cAtSdhLineAlarmLof);
    }

    if (0 != (maskEnBmp & DRV_TDM_RSOOF_INTR_MASK_STATE))
    {
        atEnableMask = atEnableMask | cAtSdhLineAlarmOof;
    }
    else
    {
        atEnableMask = atEnableMask & (~cAtSdhLineAlarmOof);
    }

    if (0 != (maskEnBmp & DRV_TDM_RSTIM_INTR_MASK_STATE))
    {
        atEnableMask = atEnableMask | cAtSdhLineAlarmTim;
    }
    else
    {
        atEnableMask = atEnableMask & (~cAtSdhLineAlarmTim);
    }

    if (0 != (maskEnBmp & DRV_TDM_MSAIS_INTR_MASK_STATE))
    {
        atEnableMask = atEnableMask | cAtSdhLineAlarmAis;
    }
    else
    {
        atEnableMask = atEnableMask & (~cAtSdhLineAlarmAis);
    }

    if (0 != (maskEnBmp & DRV_TDM_MSRDI_INTR_MASK_STATE))
    {
        atEnableMask = atEnableMask | cAtSdhLineAlarmRdi;
    }
    else
    {
        atEnableMask = atEnableMask & (~cAtSdhLineAlarmRdi);
    }

    if (0 != (maskEnBmp & DRV_TDM_MSSD_INTR_MASK_STATE))  /* MS-SD */
    {
        atEnableMask = atEnableMask | cAtSdhLineAlarmBerSd;
    }
    else
    {
        atEnableMask = atEnableMask & (~cAtSdhLineAlarmBerSd);
    }

    if (0 != (maskEnBmp & DRV_TDM_MSSF_INTR_MASK_STATE))  /* MS-SF */
    {
        atEnableMask = atEnableMask | cAtSdhLineAlarmBerSf;
    }
    else
    {
        atEnableMask = atEnableMask & (~cAtSdhLineAlarmBerSf);
    }

    if (0 != (maskEnBmp & DRV_TDM_KBYTE_INTR_MASK_STATE))
    {
        atEnableMask = atEnableMask | cAtSdhLineAlarmKByteChange;
    }
    else
    {
        atEnableMask = atEnableMask & (~cAtSdhLineAlarmKByteChange);
    }

    if (0 != (maskEnBmp & DRV_TDM_K_FAIL_INTR_MASK_STATE))
    {
        atEnableMask = atEnableMask | cAtSdhLineAlarmKByteFail;
    }
    else
    {
        atEnableMask = atEnableMask & (~cAtSdhLineAlarmKByteFail);
    }

    if (0 != (maskEnBmp & DRV_TDM_S1BYTE_INTR_MASK_STATE))
    {
        atEnableMask = atEnableMask | cAtSdhLineAlarmS1Change;
    }
    else
    {
        atEnableMask = atEnableMask & (~cAtSdhLineAlarmS1Change);
    }

    if (0 != (maskEnBmp & DRV_TDM_RSSD_INTR_MASK_STATE))  /* RS-SD */
    {
        atEnableMask = atEnableMask | cAtSdhLineAlarmRsBerSd;
    }
    else
    {
        atEnableMask = atEnableMask & (~cAtSdhLineAlarmRsBerSd);
    }

    if (0 != (maskEnBmp & DRV_TDM_RSSF_INTR_MASK_STATE))  /* RS-SF */
    {
        atEnableMask = atEnableMask | cAtSdhLineAlarmRsBerSf;
    }
    else
    {
        atEnableMask = atEnableMask & (~cAtSdhLineAlarmRsBerSf);
    }

    rv = AtChannelInterruptMaskSet((AtChannel)pLine, atDefectMask, atEnableMask);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u, AtChannelInterruptMaskSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}



/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_stm4_init.c
* 文件标识: 
* 其它说明: 驱动STM-4 TDM PWE3模块的初始化部分的实现函数
* 当前版本: V1.0
* 作    者: 谢伟生10112265.
* 完成日期: 2014-01-13
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/


#include "drv_tdm_init.h"
#include "drv_stm4_init.h"


/**************************************************************************
* 函数名称: drv_tdm_stm4PortIdCheck
* 功能描述: 根据CP3BAN单板的类型来检查STM-4 port ID.
* 访问的表: 无
* 修改的表: 无
* 输入参数: ucChipId: ARRIVE芯片的编号,取值为0~3.
*           ucPortId: STM-4端口的编号,取值为1或者5.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-01-14   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stm4PortIdCheck(BYTE ucChipId, BYTE ucPortId)
{
    WORD32 dwRetVal = DRV_TDM_OK;
    WORD32 dwSubslotId = 0;  /* 单板的子槽位号 */
    WORD32 dwBoardType = 0;    /* 单板类型 */
    
    DRV_TDM_CHECK_CHIP_ID(ucChipId);
    dwSubslotId = (WORD32)ucChipId + 1;
    
    dwRetVal = BSP_cp3ban_boardTypeGet(dwSubslotId, &dwBoardType);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_boardTypeGet() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        return dwRetVal;
    }
    
    if (BSP_CP3BAN_8PORT_BOARD == dwBoardType)  /* 8端口的STM单板 */
    {
        if ((DRV_TDM_STM_PORT_ID_START != ucPortId) 
            && ((DRV_TDM_STM_PORT_ID_START + (DRV_TDM_STM_8PORT_ID_END / 2)) != ucPortId))
        {
            return DRV_TDM_INVALID_PORT_ID;
        }
    }
    else if (BSP_CP3BAN_4PORT_BOARD == dwBoardType)  /* 4端口的STM单板 */
    {
        if (DRV_TDM_STM_PORT_ID_START != ucPortId)
        {
            return DRV_TDM_INVALID_PORT_ID;
        }
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_COMMON, "ERROR: %s line %d, subslotId %u, invalid board type, boardType=%u.\n", __FILE__, __LINE__, dwSubslotId, dwBoardType);
        return DRV_TDM_INVALID_BOARD_TYPE;
    }
    
    return DRV_TDM_OK;
}



/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_tdm_overhead.c
* 文件标识: 
* 其它说明: 驱动STM TDM PWE3模块的STM接口的开销字节实现函数
* 当前版本: V1.0
* 作    者: 谢伟生10112265.
* 完成日期: 2013-03-26
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/


#include "drv_tdm_overhead.h"
#include "drv_tdm_init.h"
#include "AtCommon.h"
#include "AtClasses.h"
#include "AtDevice.h"
#include "AtModule.h"
#include "AtModuleSdh.h"
#include "AtSdhChannel.h"
#include "AtSdhLine.h"
#include "AtSdhPath.h"


/**************************************************************************
* 函数名称: drv_tdm_traceMsgStrLenGet
* 功能描述: 通过J字节的跟踪标识方式来获取J字节的最大长度
* 访问的表: 无
* 修改的表: 无
* 输入参数: 无 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_traceMsgStrLenGet(WORD32 msgMode, WORD32 *length)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    
    DRV_TDM_CHECK_POINTER_IS_NULL(length);

    *length = 1;
    if (DRV_TDM_TRACE_MSG_1BYTE == msgMode)
    {
        *length = DRV_TDM_TRACE_MSG_LEN_1;
    }
    else if (DRV_TDM_TRACE_MSG_16BYTE == msgMode)
    {
        *length = DRV_TDM_TRACE_MSG_LEN_16;
    }
    else if (DRV_TDM_TRACE_MSG_64BYTE == msgMode)
    {
        *length = DRV_TDM_TRACE_MSG_LEN_64;
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_CFG_MSG, "ERROR: %s line %d, msgMode=%u.\n", __FILE__, __LINE__, msgMode);
        *length = 1;
        rv = DRV_TDM_INVALID_ARGUMENT;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txJ0Set
* 功能描述: 设置发送的J0的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: msgBuf:存放J0字节,需要64字节的内存空间.
* 输出参数:  
* 返 回 值: 
* 其它说明: msgBuf中不包含CRC字节,SDK会根据J0字节来计算CRC的.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txJ0Set(BYTE chipId, BYTE portId, WORD32 traceMode, const BYTE *msgBuf)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 length = 1;
    AtSdhLine pLine = NULL;
    BYTE txJ0Msg[DRV_TDM_TRACE_MSG_LEN_64] = {0}; /* 实际需要设置的J0 */
    eAtSdhTtiMode ttiMode = 0;
    tAtSdhTti tti;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(msgBuf);  /* 检查指针是否为NULL指针 */
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    if (DRV_TDM_TRACE_MSG_1BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode1Byte;
    }
    else if (DRV_TDM_TRACE_MSG_16BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode16Byte;
    }
    else if (DRV_TDM_TRACE_MSG_64BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode64Byte;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid trace mode, traceMode=%u.\n", __FILE__, __LINE__, traceMode);
        return DRV_TDM_INVALID_ARGUMENT;
    }
    
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    /* 通过J0字节的跟踪标识方式来获取J0字节的最大长度 */
    rv = drv_tdm_traceMsgStrLenGet(traceMode, &length); 
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_traceMsgStrLenGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    /* configure transmitted TTI */
    memset(&tti, 0, sizeof(tAtSdhTti));
    memcpy(txJ0Msg, msgBuf, length);
    AtSdhTtiMake(ttiMode, txJ0Msg, strlen((char *)txJ0Msg), &tti);
    rv = AtSdhChannelTxTtiSet((AtSdhChannel)pLine, &tti);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelTxTtiSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txJ0Get
* 功能描述: 获取发送的J0的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: msgBuf:保存J0的值,需要65字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txJ0Get(BYTE chipId, BYTE portId, BYTE *msgBuf)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhLine pLine = NULL;
    tAtSdhTti tti;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(msgBuf);  /* 检查指针是否为NULL指针 */
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);

    /* Get transmitted J0. */
    memset(&tti, 0, sizeof(tAtSdhTti));
    rv = AtSdhChannelTxTtiGet((AtSdhChannel)pLine, &tti);
    if (cAtOk != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, AtSdhChannelTxTtiGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    memset(msgBuf, 0, DRV_TDM_TRACE_MSG_MAX_LEN);
    msgBuf[0] = tti.crc;
    memcpy(&(msgBuf[1]), tti.message, (DRV_TDM_TRACE_MSG_MAX_LEN - 1));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_expectedRxJ0Set
* 功能描述: 设置期望接收的J0的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: msgBuf: 存放J0的值,需要64字节的内存空间.
* 输出参数:  
* 返 回 值: 
* 其它说明: msgBuf中不包含CRC字节,SDK会根据J0字节来计算CRC的.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_expectedRxJ0Set(BYTE chipId, BYTE portId, WORD32 traceMode, const BYTE *msgBuf)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 length = 1;
    AtSdhLine pLine = NULL;
    BYTE expectedRxJ0Buf[DRV_TDM_TRACE_MSG_LEN_64] = {0}; /* 实际需要设置的J0 */
    eAtSdhTtiMode ttiMode = 0;
    tAtSdhTti tti;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(msgBuf);  /* 检查指针是否为NULL指针 */
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    if (DRV_TDM_TRACE_MSG_1BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode1Byte;
    }
    else if (DRV_TDM_TRACE_MSG_16BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode16Byte;
    }
    else if (DRV_TDM_TRACE_MSG_64BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode64Byte;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid trace mode, traceMode=%u.\n", __FILE__, __LINE__, traceMode);
        return DRV_TDM_INVALID_ARGUMENT;
    }

    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    /* 通过J0字节的跟踪标识方式来获取J0字节的最大长度 */
    rv = drv_tdm_traceMsgStrLenGet(traceMode, &length); 
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_traceMsgStrLenGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    /* configure expected RX TTI */
    memset(&tti, 0, sizeof(tAtSdhTti));
    memcpy(expectedRxJ0Buf, msgBuf, length);
    AtSdhTtiMake(ttiMode, expectedRxJ0Buf, strlen((char *)expectedRxJ0Buf), &tti);
    rv = AtSdhChannelExpectedTtiSet((AtSdhChannel)pLine, &tti);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelExpectedTtiSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    /* disable RS-TIM monitoring */
    rv = AtSdhChannelTimMonitorEnable((AtSdhChannel)pLine, cAtFalse);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelTimMonitorEnable() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    /* Disable AIS downstream and RDI to remote side. */
    rv = AtSdhChannelAlarmAffectingEnable((AtSdhChannel)pLine, cAtSdhLineAlarmTim, cAtFalse);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelAlarmAffectingEnable() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }

    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_expectedRxJ0Get
* 功能描述: 获取期望接收的J0的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: msgBuf:保存J0的值,需要65字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_expectedRxJ0Get(BYTE chipId, BYTE portId, BYTE *msgBuf)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhLine pLine = NULL;
    tAtSdhTti tti;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(msgBuf);  /* 检查指针是否为NULL指针 */
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);

    /* Get expected RX J0. */
    memset(&tti, 0, sizeof(tAtSdhTti));
    rv = AtSdhChannelExpectedTtiGet((AtSdhChannel)pLine, &tti);
    if (cAtOk != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, AtSdhChannelExpectedTtiGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    memset(msgBuf, 0, DRV_TDM_TRACE_MSG_MAX_LEN);
    msgBuf[0] = tti.crc;
    memcpy(&(msgBuf[1]), tti.message, (DRV_TDM_TRACE_MSG_MAX_LEN - 1));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_rxJ0Get
* 功能描述: 获取实际接收的J0的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: msgBuf:保存J0的值,需要65字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_rxJ0Get(BYTE chipId, BYTE portId, BYTE *msgBuf)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhLine pLine = NULL;
    tAtSdhTti tti;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(msgBuf);  /* 检查指针是否为NULL指针 */
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    /* Get received J0 */
    memset(&tti, 0, sizeof(tAtSdhTti));
    rv = AtSdhChannelRxTtiGet((AtSdhChannel)pLine, &tti);
    if (cAtOk != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, AtSdhChannelRxTtiGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }

    memset(msgBuf, 0, DRV_TDM_TRACE_MSG_MAX_LEN);  /* 先清零再赋值 */
    msgBuf[0] = tti.crc;
    memcpy(&(msgBuf[1]), tti.message, (DRV_TDM_TRACE_MSG_MAX_LEN - 1));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txJ1Set
* 功能描述: 设置发送的J1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           traceMode: 具体参见DRV_TDM_TRACE_MSG_MODE定义.
*           msgBuf: 存放J1字节,需要64字节的内存空间.
* 输出参数:  
* 返 回 值: 
* 其它说明: msgBuf中不包含CRC字节,SDK会根据J1字节来计算CRC的.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txJ1Set(BYTE chipId, 
                       BYTE portId, 
                       BYTE aug1Id, 
                       WORD32 traceMode, 
                       const BYTE *msgBuf)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc4 = NULL;
    WORD32 length = 1;
    BYTE txJ1Msg[DRV_TDM_TRACE_MSG_LEN_64] = {0}; /* 实际需要设置的J1 */
    eAtSdhTtiMode ttiMode = 0;
    tAtSdhTti tti;
    
    DRV_TDM_CHECK_CHIP_ID(chipId); 
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(msgBuf);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    if (DRV_TDM_TRACE_MSG_1BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode1Byte;
    }
    else if (DRV_TDM_TRACE_MSG_16BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode16Byte;
    }
    else if (DRV_TDM_TRACE_MSG_64BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode64Byte;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid trace mode, traceMode=%u.\n", __FILE__, __LINE__, traceMode);
        return DRV_TDM_INVALID_ARGUMENT;
    }

    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    
    /* 通过J1字节的跟踪标识方式来获取J1字节的最大长度 */
    rv = drv_tdm_traceMsgStrLenGet(traceMode, &length); 
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_traceMsgStrLenGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    memset(&tti, 0, sizeof(tAtSdhTti));
    memcpy(txJ1Msg, msgBuf, length);
    AtSdhTtiMake(ttiMode, txJ1Msg, strlen((char *)txJ1Msg), &tti);
    rv = AtSdhChannelTxTtiSet((AtSdhChannel)pVc4, &tti);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelTxTtiSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txJ1Get
* 功能描述: 获取发送的J1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
* 输出参数: msgBuf:保存J1的值,需要65字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txJ1Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *msgBuf)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc4 = NULL;
    tAtSdhTti tti;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(msgBuf);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    
    /* Get transmitted J1. */
    memset(&tti, 0, sizeof(tAtSdhTti));
    rv = AtSdhChannelTxTtiGet((AtSdhChannel)pVc4, &tti);
    if (cAtOk != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, AtSdhChannelTxTtiGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    memset(msgBuf, 0, DRV_TDM_TRACE_MSG_MAX_LEN);
    msgBuf[0] = tti.crc;
    memcpy(&(msgBuf[1]), tti.message, (DRV_TDM_TRACE_MSG_MAX_LEN - 1));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_expectedRxJ1Set
* 功能描述: 设置期望接收的J1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           traceMode: 具体参见DRV_TDM_TRACE_MSG_MODE定义.
*           msgBuf: 存放J1的值,需要64字节的内存空间.
* 输出参数:  
* 返 回 值: 
* 其它说明: msgBuf中不包含CRC字节,SDK会根据J1字节来计算CRC的.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_expectedRxJ1Set(BYTE chipId, 
                               BYTE portId, 
                               BYTE aug1Id, 
                               WORD32 traceMode, 
                               const BYTE *msgBuf)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 length = 1;
    AtSdhVc pVc4 = NULL;
    BYTE expectedRxJ1Buf[DRV_TDM_TRACE_MSG_LEN_64] = {0}; /* 实际需要设置的J1 */
    eAtSdhTtiMode ttiMode = 0;
    tAtSdhTti tti;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(msgBuf);  /* 检查指针是否为NULL指针 */
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    if (DRV_TDM_TRACE_MSG_1BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode1Byte;
    }
    else if (DRV_TDM_TRACE_MSG_16BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode16Byte;
    }
    else if (DRV_TDM_TRACE_MSG_64BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode64Byte;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid trace mode, traceMode=%u.\n", __FILE__, __LINE__, traceMode);
        return DRV_TDM_INVALID_ARGUMENT;
    }

    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    
    /* 通过J1字节的跟踪标识方式来获取J1字节的最大长度 */
    rv = drv_tdm_traceMsgStrLenGet(traceMode, &length); 
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_traceMsgStrLenGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    /* configure expected RX TTI */
    memset(&tti, 0, sizeof(tAtSdhTti));
    memcpy(expectedRxJ1Buf, msgBuf, length);
    AtSdhTtiMake(ttiMode, expectedRxJ1Buf, strlen((char *)expectedRxJ1Buf), &tti);
    rv = AtSdhChannelExpectedTtiSet((AtSdhChannel)pVc4, &tti);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelExpectedTtiSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    /* disable HP-TIM monitoring */
    rv = AtSdhChannelTimMonitorEnable((AtSdhChannel)pVc4, cAtFalse);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelTimMonitorEnable() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    /* Disable AIS downstream and RDI to remote side. */
    rv = AtSdhChannelAlarmAffectingEnable((AtSdhChannel)pVc4, cAtSdhPathAlarmTim, cAtFalse);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelAlarmAffectingEnable() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }

    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_expectedRxJ1Get
* 功能描述: 获取期望接收的J1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
* 输出参数: msgBuf:保存J1的值,需要65字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_expectedRxJ1Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *msgBuf)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc4 = NULL;
    tAtSdhTti tti;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(msgBuf);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    
    /* Get expected RX J1. */
    memset(&tti, 0, sizeof(tAtSdhTti));
    rv = AtSdhChannelExpectedTtiGet((AtSdhChannel)pVc4, &tti);
    if (cAtOk != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, AtSdhChannelExpectedTtiGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    memset(msgBuf, 0, DRV_TDM_TRACE_MSG_MAX_LEN);
    msgBuf[0] = tti.crc;
    memcpy(&(msgBuf[1]), tti.message, (DRV_TDM_TRACE_MSG_MAX_LEN - 1));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_rxJ1Get
* 功能描述: 获取实际接收到的J1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
* 输出参数: msgBuf:保存J1的值,需要65字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_rxJ1Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *msgBuf)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc4 = NULL;
    tAtSdhTti tti;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(msgBuf);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    
    /* Get received J1 */
    memset(&tti, 0, sizeof(tAtSdhTti));
    rv = AtSdhChannelRxTtiGet((AtSdhChannel)pVc4, &tti);
    if (cAtOk != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, AtSdhChannelRxTtiGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    memset(msgBuf, 0, DRV_TDM_TRACE_MSG_MAX_LEN);
    msgBuf[0] = tti.crc;
    memcpy(&(msgBuf[1]), tti.message, (DRV_TDM_TRACE_MSG_MAX_LEN - 1));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txJ2Set
* 功能描述: 设置发送的J2的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
*           traceMode: 具体参见DRV_TDM_TRACE_MSG_MODE定义.
*           msgBuf: 存放J2的值,需要64字节的内存空间.
* 输出参数:  
* 返 回 值: 
* 其它说明: msgBuf中不包含CRC字节,SDK会根据J2字节来计算CRC的.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txJ2Set(BYTE chipId, 
                       BYTE portId, 
                       BYTE aug1Id, 
                       BYTE e1LinkId, 
                       WORD32 traceMode, 
                       const BYTE *msgBuf)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc12 = NULL;
    WORD32 length = 1;
    BYTE txJ2Msg[DRV_TDM_TRACE_MSG_LEN_64] = {0}; /* 实际需要设置的J1 */
    eAtSdhTtiMode ttiMode = 0;
    tAtSdhTti tti;
    
    DRV_TDM_CHECK_CHIP_ID(chipId); 
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(msgBuf);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    if (DRV_TDM_TRACE_MSG_1BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode1Byte;
    }
    else if (DRV_TDM_TRACE_MSG_16BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode16Byte;
    }
    else if (DRV_TDM_TRACE_MSG_64BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode64Byte;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid trace mode, traceMode=%u.\n", __FILE__, __LINE__, traceMode);
        return DRV_TDM_INVALID_ARGUMENT;
    }

    rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);
    
    /* 通过J2字节的跟踪标识方式来获取J2字节的最大长度 */
    rv = drv_tdm_traceMsgStrLenGet(traceMode, &length); 
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_traceMsgStrLenGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    memset(&tti, 0, sizeof(tAtSdhTti));
    memcpy(txJ2Msg, msgBuf, length);
    AtSdhTtiMake(ttiMode, txJ2Msg, strlen((char *)txJ2Msg), &tti);
    rv = AtSdhChannelTxTtiSet((AtSdhChannel)pVc12, &tti);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelTxTtiSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txJ2Get
* 功能描述: 获取发送的J2的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
* 输出参数: msgBuf:保存J2的值,需要65字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txJ2Get(BYTE chipId, 
                       BYTE portId, 
                       BYTE aug1Id, 
                       BYTE e1LinkId, 
                       BYTE *msgBuf)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc12 = NULL;
    tAtSdhTti tti;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(msgBuf);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);

    /* Get transmitted J2. */
    memset(&tti, 0, sizeof(tAtSdhTti));
    rv = AtSdhChannelTxTtiGet((AtSdhChannel)pVc12, &tti);
    if (cAtOk != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, AtSdhChannelTxTtiGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    memset(msgBuf, 0, DRV_TDM_TRACE_MSG_MAX_LEN);
    msgBuf[0] = tti.crc;
    memcpy(&(msgBuf[1]), tti.message, (DRV_TDM_TRACE_MSG_MAX_LEN - 1));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_expectedRxJ2Set
* 功能描述: 设置期望接收的J2的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
*           traceMode: 具体参见DRV_TDM_TRACE_MSG_MODE定义.
*           msgBuf: 存放J2的值,需要64字节的内存空间.
* 输出参数:  
* 返 回 值: 
* 其它说明: msgBuf中不包含CRC字节,SDK会根据J2字节来计算CRC的.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_expectedRxJ2Set(BYTE chipId, 
                               BYTE portId, 
                               BYTE aug1Id, 
                               BYTE e1LinkId, 
                               WORD32 traceMode, 
                               const BYTE *msgBuf)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc12 = NULL;
    WORD32 length = 1;
    BYTE expectedRxJ2Msg[DRV_TDM_TRACE_MSG_LEN_64] = {0}; /* 实际需要设置的J2 */
    eAtSdhTtiMode ttiMode = 0;
    tAtSdhTti tti;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(msgBuf);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    if (DRV_TDM_TRACE_MSG_1BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode1Byte;
    }
    else if (DRV_TDM_TRACE_MSG_16BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode16Byte;
    }
    else if (DRV_TDM_TRACE_MSG_64BYTE == traceMode)
    {
        ttiMode = cAtSdhTtiMode64Byte;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid trace mode, traceMode=%u.\n", __FILE__, __LINE__, traceMode);
        return DRV_TDM_INVALID_ARGUMENT;
    }

    rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);
    
    /* 通过J2字节的跟踪标识方式来获取J2字节的最大长度 */
    rv = drv_tdm_traceMsgStrLenGet(traceMode, &length); 
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_traceMsgStrLenGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    memset(&tti, 0, sizeof(tAtSdhTti));
    memcpy(expectedRxJ2Msg, msgBuf, length);
    AtSdhTtiMake(ttiMode, expectedRxJ2Msg, strlen((char *)expectedRxJ2Msg), &tti);
    rv = AtSdhChannelExpectedTtiSet((AtSdhChannel)pVc12, &tti);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelTxTtiSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    /* disable LP-TIM monitoring */
    rv = AtSdhChannelTimMonitorEnable((AtSdhChannel)pVc12, cAtFalse);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelTimMonitorEnable() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    /* Disable AIS downstream and RDI to remote side. */
    rv = AtSdhChannelAlarmAffectingEnable((AtSdhChannel)pVc12, cAtSdhPathAlarmTim, cAtFalse);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhChannelAlarmAffectingEnable() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }

    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_expectedRxJ2Get
* 功能描述: 获取期望接收的J2的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
* 输出参数: msgBuf:保存J2的值,需要65字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_expectedRxJ2Get(BYTE chipId, 
                               BYTE portId, 
                               BYTE aug1Id, 
                               BYTE e1LinkId, 
                               BYTE *msgBuf)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc12 = NULL;
    tAtSdhTti tti;
    
    DRV_TDM_CHECK_CHIP_ID(chipId); 
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(msgBuf);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);
    
    /* Get expected RX J2 */
    memset(&tti, 0, sizeof(tAtSdhTti));
    rv = AtSdhChannelExpectedTtiGet((AtSdhChannel)pVc12, &tti);
    if (cAtOk != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, AtSdhChannelExpectedTtiGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    memset(msgBuf, 0, DRV_TDM_TRACE_MSG_MAX_LEN);
    msgBuf[0] = tti.crc;
    memcpy(&(msgBuf[1]), tti.message, (DRV_TDM_TRACE_MSG_MAX_LEN - 1));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_rxJ2Get
* 功能描述: 获取实际接收到的J2的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
*           msgBuf:保存J2的值,需要65字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_rxJ2Get(BYTE chipId, 
                       BYTE portId, 
                       BYTE aug1Id, 
                       BYTE e1LinkId, 
                       BYTE *msgBuf)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc12 = NULL;
    tAtSdhTti tti;
    
    DRV_TDM_CHECK_CHIP_ID(chipId); 
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(msgBuf);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);
    
    /* Get received J2 */
    memset(&tti, 0, sizeof(tAtSdhTti));
    rv = AtSdhChannelRxTtiGet((AtSdhChannel)pVc12, &tti);
    if (cAtOk != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, AtSdhChannelRxTtiGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    memset(msgBuf, 0, DRV_TDM_TRACE_MSG_MAX_LEN);
    msgBuf[0] = tti.crc;
    memcpy(&(msgBuf[1]), tti.message, (DRV_TDM_TRACE_MSG_MAX_LEN - 1));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txC2Set
* 功能描述: 设置发送的C2的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           c2Value: C2 value.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txC2Set(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE c2Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc4 = NULL;
    BYTE tmpC2Value = 0;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    
    /* Get transmitted C2 */
    tmpC2Value = AtSdhPathTxPslGet((AtSdhPath)pVc4);
    
    /* 当需要设置的C2和从芯片中读取的C2不一致时,才写芯片,否则不写芯片 */
    if (c2Value != tmpC2Value)
    {
        rv = AtSdhPathTxPslSet((AtSdhPath)pVc4, c2Value);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhPathTxPslSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txC2Get
* 功能描述: 获取发送的C2的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
* 输出参数: pC2Value:用来保存C2的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txC2Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *pC2Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc4 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(pC2Value);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    
    *pC2Value = 0;  /* 先清零再赋值 */
    *pC2Value = AtSdhPathTxPslGet((AtSdhPath)pVc4);  /* Get transmitted C2. */
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_expectedRxC2Set
* 功能描述: 设置期望接收的C2的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           c2Value: C2 value.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_expectedRxC2Set(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE c2Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc4 = NULL;
    BYTE tmpC2Value = 0;
    
    DRV_TDM_CHECK_CHIP_ID(chipId); 
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    
    /* Get expected RX C2 */
    tmpC2Value = AtSdhPathExpectedPslGet((AtSdhPath)pVc4);
    
    /* 当需要设置的C2和从芯片中读取的C2不一致时,才写芯片,否则不写芯片 */
    if (c2Value != tmpC2Value)
    {
        rv = AtSdhPathExpectedPslSet((AtSdhPath)pVc4, c2Value);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhPathExpectedPslSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_expectedRxC2Get
* 功能描述: 获取期望接收的C2的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
* 输出参数: pC2Value:用来保存C2的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_expectedRxC2Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *pC2Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc4 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId); 
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(pC2Value);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    
    /* 先清零再赋值 */
    *pC2Value = 0;
    *pC2Value = AtSdhPathExpectedPslGet((AtSdhPath)pVc4);  /* Get expected RX C2 */
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_rxC2Get
* 功能描述: 获取实际接收的C2的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
* 输出参数: pC2Value:用来保存C2的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_rxC2Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *pC2Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc4 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(pC2Value);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);

    /* 先清零再赋值 */
    *pC2Value = 0;
    *pC2Value = AtSdhPathRxPslGet((AtSdhPath)pVc4); /* Get received C2. */
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_lpTxPslSet
* 功能描述: 设置VC12通道的发送的path signal label的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
*           pslValue: path signal label value.
* 输出参数:  
* 返 回 值: 
* 其它说明: 根据G.707标准,即为V5字节的bit5~bit7.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_lpTxPslSet(BYTE chipId, 
                                 BYTE portId, 
                                 BYTE aug1Id, 
                                 BYTE e1LinkId, 
                                 BYTE pslValue)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc12 = NULL;
    BYTE tmpPslValue = 0;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);
    
    tmpPslValue = AtSdhPathTxPslGet((AtSdhPath)pVc12);
    
    /* 当实际需要设置的path signal label和从芯片中读取的path signal label不一致时,才需要写芯片 */
    if (pslValue != tmpPslValue)
    {
        rv = AtSdhPathTxPslSet((AtSdhPath)pVc12, pslValue);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhPathTxPslSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_lpTxPslGet
* 功能描述: 获取VC12通道的发送的path signal label的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
* 输出参数: pPslValue:保存path signal label的值,需要1字节的内存空间  
* 返 回 值: 
* 其它说明: 根据G.707标准,即为V5字节的bit5~bit7.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_lpTxPslGet(BYTE chipId, 
                                 BYTE portId, 
                                 BYTE aug1Id, 
                                 BYTE e1LinkId, 
                                 BYTE *pPslValue)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc12 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPslValue);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);
    
    *pPslValue = 0;  /* 先赋值再清零 */
    *pPslValue = AtSdhPathTxPslGet((AtSdhPath)pVc12);  
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_lpExpectedRxPslSet
* 功能描述: 设置VC12通道的期望接收的path signal label的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
*           pslValue: path signal label value.
* 输出参数:  
* 返 回 值: 
* 其它说明: 根据G.707标准,即为V5字节的bit5~bit7.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_lpExpectedRxPslSet(BYTE chipId, 
                                            BYTE portId, 
                                            BYTE aug1Id, 
                                            BYTE e1LinkId, 
                                            BYTE pslValue)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc12 = NULL;
    BYTE tmpPslValue = 0;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
  
    rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);
    
    tmpPslValue = AtSdhPathExpectedPslGet((AtSdhPath)pVc12);
    
    /* 当实际需要设置的path signal label和从芯片中读取的path signal label不一致时,才需要写芯片 */
    if (pslValue != tmpPslValue)
    {
        rv = AtSdhPathExpectedPslSet((AtSdhPath)pVc12, pslValue);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhPathExpectedPslSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_lpExpectedRxPslGet
* 功能描述: 获取VC12通道的期望接收的path signal label的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
* 输出参数: pPslValue:保存path signal label的值,需要1字节的内存空间  
* 返 回 值: 
* 其它说明: 根据G.707标准,即为V5字节的bit5~bit7.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_lpExpectedRxPslGet(BYTE chipId, 
                                             BYTE portId, 
                                             BYTE aug1Id, 
                                             BYTE e1LinkId, 
                                             BYTE *pPslValue)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc12 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPslValue);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);

    *pPslValue = 0;  /* 先清零再赋值 */
    *pPslValue = AtSdhPathExpectedPslGet((AtSdhPath)pVc12);   
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_lpRxPslGet
* 功能描述: 获取VC12通道的实际接收到的path signal label的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
* 输出参数: pPslValue:保存path signal label的值,需要1字节的内存空间  
* 返 回 值: 
* 其它说明: 根据G.707标准,即为V5字节的bit5~bit7.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_lpRxPslGet(BYTE chipId, 
                                 BYTE portId, 
                                 BYTE aug1Id, 
                                 BYTE e1LinkId, 
                                 BYTE *pPslValue)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc12 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId); 
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPslValue);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);
    
    *pPslValue = 0;  /* 先清零再赋值 */
    *pPslValue = AtSdhPathRxPslGet((AtSdhPath)pVc12);  
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txS1Set
* 功能描述: 设置发送的S1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           s1Value: S1 value.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txS1Set(BYTE chipId, BYTE portId, BYTE s1Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);

    /* Set transmitted S1 */
    rv = AtSdhLineTxS1Set(pLine, s1Value);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhLineTxS1Set() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txS1Get
* 功能描述: 获取发送的S1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
* 输出参数: pS1Value:保存S1的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txS1Get(BYTE chipId, BYTE portId, BYTE *pS1Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pS1Value);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);

    /* 先清零再赋值 */
    *pS1Value = 0;
    *pS1Value = AtSdhLineTxS1Get((AtSdhLine)pLine);  /* Get transmitted S1. */
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_rxS1Get
* 功能描述: 获取实际接收到的S1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
* 输出参数: pS1Value:保存S1的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_rxS1Get(BYTE chipId, BYTE portId, BYTE *pS1Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pS1Value);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    /* 先清零再赋值 */
    *pS1Value = 0;
    *pS1Value = AtSdhLineRxS1Get(pLine);  /* Get received S1 */
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txK1Set
* 功能描述: 设置发送的K1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           k1Value: K1 value.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txK1Set(BYTE chipId, BYTE portId, BYTE k1Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    /* Set transmitted K1 */
    rv = AtSdhLineTxK1Set(pLine, k1Value);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhLineTxK1Set() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txK1Get
* 功能描述: 获取发送的K1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
* 输出参数: pK1Value:保存K1的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txK1Get(BYTE chipId, BYTE portId, BYTE *pK1Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pK1Value);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);

    /* 先清零再赋值 */
    *pK1Value = 0;
    *pK1Value = AtSdhLineTxK1Get((AtSdhLine)pLine); /* Get transmitted K1. */
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_rxK1Get
* 功能描述: 获取实际接收到的K1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
* 输出参数: pK1Value:保存K1的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_rxK1Get(BYTE chipId, BYTE portId, BYTE *pK1Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pK1Value);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    /* 先清零再赋值 */
    *pK1Value = 0;
    *pK1Value = AtSdhLineRxK1Get(pLine);  /* Get received K1 */
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txK2Set
* 功能描述: 设置发送的K2的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           k2Value: K2 valuel.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txK2Set(BYTE chipId, BYTE portId, BYTE k2Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    /* Set transmitted K2 */
    rv = AtSdhLineTxK2Set(pLine, k2Value);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtSdhLineTxK2Set() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txK2Get
* 功能描述: 获取发送的K2的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
* 输出参数: pK2Value:保存K2的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txK2Get(BYTE chipId, BYTE portId, BYTE *pK2Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pK2Value);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);

    /* 先清零再赋值 */
    *pK2Value = 0;
    *pK2Value = AtSdhLineTxK2Get((AtSdhLine)pLine);  /* Get transmitted K2. */
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_rxK2Get
* 功能描述: 获取实际接收到的K2的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
* 输出参数: pK2Value:保存K2的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_rxK2Get(BYTE chipId, BYTE portId, BYTE *pK2Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pK2Value);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    /* 先清零再赋值 */
    *pK2Value = 0;
    *pK2Value = AtSdhLineRxK2Get(pLine);  /* Get received K2 */
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txM1Get
* 功能描述: 获取发送的M1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
* 输出参数: *pM1Value: 保存M1的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txM1Get(BYTE chipId, BYTE portId, BYTE *pM1Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pM1Value);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    *pM1Value = 0;  /* 先清零再赋值 */
    *pM1Value = AtSdhChannelTxOverheadByteGet((AtSdhChannel)pLine, cAtSdhLineOverheadByteM1); 
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_rxM1Get
* 功能描述: 获取接收的M1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
* 输出参数: *pM1Value: 保存M1的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_rxM1Get(BYTE chipId, BYTE portId, BYTE *pM1Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pM1Value);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    *pM1Value = 0;  /* 先清零再赋值 */
    *pM1Value = AtSdhChannelRxOverheadByteGet((AtSdhChannel)pLine, cAtSdhLineOverheadByteM1); 
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txG1Get
* 功能描述: 获取发送的G1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
* 输出参数: *pG1Value: 用来保存G1的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-25   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txG1Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *pG1Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc4 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(pG1Value);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    
    *pG1Value = 0;  /* 先清零再赋值 */
    *pG1Value = AtSdhChannelTxOverheadByteGet((AtSdhChannel)pVc4, cAtSdhPathOverheadByteG1);  
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_rxG1Get
* 功能描述: 获取接收的G1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
* 输出参数: *pG1Value: 用来保存G1的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-25   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_rxG1Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *pG1Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc4 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(pG1Value);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    
    *pG1Value = 0;  /* 先清零再赋值 */
    *pG1Value = AtSdhChannelRxOverheadByteGet((AtSdhChannel)pVc4, cAtSdhPathOverheadByteG1);  
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txH4Get
* 功能描述: 获取发送的H4的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
* 输出参数: *pH4Value: 用来保存H4的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-25   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txH4Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *pH4Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc4 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(pH4Value);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    
    *pH4Value = 0;  /* 先清零再赋值 */
    *pH4Value = AtSdhChannelTxOverheadByteGet((AtSdhChannel)pVc4, cAtSdhPathOverheadByteH4);  
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_rxH4Get
* 功能描述: 获取接收的H4的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
* 输出参数: *pH4Value: 用来保存H4的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-25   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_rxH4Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *pH4Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc4 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(pH4Value);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    
    *pH4Value = 0;  /* 先清零再赋值 */
    *pH4Value = AtSdhChannelRxOverheadByteGet((AtSdhChannel)pVc4, cAtSdhPathOverheadByteH4);  
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txK3Get
* 功能描述: 获取发送的K3的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
* 输出参数: *pK3Value: 用来保存K3的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-25   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txK3Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *pK3Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc4 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(pK3Value);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    
    *pK3Value = 0;  /* 先清零再赋值 */
    *pK3Value = AtSdhChannelTxOverheadByteGet((AtSdhChannel)pVc4, cAtSdhPathOverheadByteK3);  
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_rxK3Get
* 功能描述: 获取接收的K3的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
* 输出参数: *pK3Value: 用来保存K3的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-25   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_rxK3Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *pK3Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc4 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(pK3Value);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    
    *pK3Value = 0;  /* 先清零再赋值 */
    *pK3Value = AtSdhChannelRxOverheadByteGet((AtSdhChannel)pVc4, cAtSdhPathOverheadByteK3);  
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txN1Get
* 功能描述: 获取发送的N1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
* 输出参数: *pN1Value: 用来保存N1的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-25   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txN1Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *pN1Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc4 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(pN1Value);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    
    *pN1Value = 0;  /* 先清零再赋值 */
    *pN1Value = AtSdhChannelTxOverheadByteGet((AtSdhChannel)pVc4, cAtSdhPathOverheadByteN1);  
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_rxN1Get
* 功能描述: 获取接收的N1的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
* 输出参数: *pN1Value: 用来保存N1的值,需要1字节的内存空间 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-25   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_rxN1Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *pN1Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc4 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(pN1Value);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    
    *pN1Value = 0;  /* 先清零再赋值 */
    *pN1Value = AtSdhChannelRxOverheadByteGet((AtSdhChannel)pVc4, cAtSdhPathOverheadByteN1);  
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txV5Get
* 功能描述: 获取发送的V5的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
* 输出参数: *pV5Value: 保存V5的值,需要1字节的内存空间  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-25   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txV5Get(BYTE chipId, 
                             BYTE portId, 
                             BYTE aug1Id, 
                             BYTE e1LinkId, 
                             BYTE *pV5Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc12 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pV5Value);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);
    
    *pV5Value = 0;  /* 先赋值再清零 */
    *pV5Value = AtSdhChannelTxOverheadByteGet((AtSdhChannel)pVc12, cAtSdhPathOverheadByteV5);  
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_rxV5Get
* 功能描述: 获取接收的V5的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
* 输出参数: *pV5Value: 保存V5的值,需要1字节的内存空间  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-25   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_rxV5Get(BYTE chipId, 
                             BYTE portId, 
                             BYTE aug1Id, 
                             BYTE e1LinkId, 
                             BYTE *pV5Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc12 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pV5Value);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);
    
    *pV5Value = 0;  /* 先赋值再清零 */
    *pV5Value = AtSdhChannelRxOverheadByteGet((AtSdhChannel)pVc12, cAtSdhPathOverheadByteV5);  
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txN2Get
* 功能描述: 获取发送的N2的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
* 输出参数: *pN2Value: 保存N2的值,需要1字节的内存空间  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-25   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txN2Get(BYTE chipId, 
                             BYTE portId, 
                             BYTE aug1Id, 
                             BYTE e1LinkId, 
                             BYTE *pN2Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc12 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pN2Value);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);
    
    *pN2Value = 0;  /* 先赋值再清零 */
    *pN2Value = AtSdhChannelTxOverheadByteGet((AtSdhChannel)pVc12, cAtSdhPathOverheadByteN2);  
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_rxN2Get
* 功能描述: 获取接收的N2的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
* 输出参数: *pN2Value: 保存N2的值,需要1字节的内存空间  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-25   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_rxN2Get(BYTE chipId, 
                             BYTE portId, 
                             BYTE aug1Id, 
                             BYTE e1LinkId, 
                             BYTE *pN2Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc12 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pN2Value);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);
    
    *pN2Value = 0;  /* 先赋值再清零 */
    *pN2Value = AtSdhChannelRxOverheadByteGet((AtSdhChannel)pVc12, cAtSdhPathOverheadByteN2);  
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txK4Get
* 功能描述: 获取发送的K4的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
* 输出参数: *pK4Value: 保存K4的值,需要1字节的内存空间  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-25   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txK4Get(BYTE chipId, 
                             BYTE portId, 
                             BYTE aug1Id, 
                             BYTE e1LinkId, 
                             BYTE *pK4Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc12 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pK4Value);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);
    
    *pK4Value = 0;  /* 先赋值再清零 */
    *pK4Value = AtSdhChannelTxOverheadByteGet((AtSdhChannel)pVc12, cAtSdhPathOverheadByteK4);  
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_rxK4Get
* 功能描述: 获取接收的K4的值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
* 输出参数: *pK4Value: 保存K4的值,需要1字节的内存空间  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-25   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_rxK4Get(BYTE chipId, 
                             BYTE portId, 
                             BYTE aug1Id, 
                             BYTE e1LinkId, 
                             BYTE *pK4Value)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc12 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pK4Value);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_OH_QUERY_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);
    
    *pK4Value = 0;  /* 先赋值再清零 */
    *pK4Value = AtSdhChannelRxOverheadByteGet((AtSdhChannel)pVc12, cAtSdhPathOverheadByteK4);  
    
    return DRV_TDM_OK;
}



/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_tdm_debug.c
* 文件标识: 
* 其它说明: 驱动STM TDM PWE3模块的常见调试函数和故障诊断函数的实现
* 当前版本: V1.0
* 作    者: 谢伟生10112265.
* 完成日期: 2013-05-16
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/


#include "drv_tdm_init.h"
#include "drv_tdm_sdh_alarm.h"
#include "drv_tdm_sdh_perf.h"
#include "drv_tdm_overhead.h"
#include "drv_tdm_pwe3.h"
#include "drv_stm4_init.h"
#include "drv_tdm_intf.h"
#include "AtCommon.h"
#include "AtOsal.h"
#include "AtClasses.h"
#include "AtDevice.h"
#include "AtHal.h"
#include "AtModuleSdh.h"
#include "AtModulePdh.h"
#include "AtSdhLine.h"
#include "AtChannel.h"
#include "AtSdhChannel.h"
#include "AtModuleEth.h"
#include "AtChannel.h"
#include "AtEthPort.h"
#include "AtCliModule.h"
#include "AtComponent.h"
#include "AtCli.h"


/* 全局变量定义 */
static char* g_drv_tdm_e1FramingModeStr[DRV_TDM_FRAME_MAX_SIZE] = {"UNFRAME", "PCM30", "PCM30CRC", "PCM31", "PCM31CRC"};
static char* g_drv_tdm_e1TimingModeStr[DRV_TDM_TIMING_MAX_SIZE] = {"System", "ACR", "DCR", "Loop", "Slave", "UNKNOWN"};
static char* g_drv_tdm_e1ClkStateStr[DRV_TDM_E1_LINK_STATE_MAX] = {"SLAVE", "MASTER"};
static char* g_drv_tdm_e1LinkStatusStr[DRV_TDM_E1_LINK_STATUS_MAX] = {"FREE", "USED"};
static char* g_drv_tdm_clkDomainStateStr[DRV_TDM_CLK_DOMAIN_MAX] = {"FREE", "USED"};
static char* g_drv_tdm_sdhLbMdStr[DRV_SDH_LOOPBACK_MAX_SIZE] = {"RELEASE", "INTERNAL", "EXTERNAL"};
static char* g_drv_tdm_txScrambleStr[DRV_TDM_STM1_SCRAMBLE_MAX_SIZE] = {"ENABLE", "DISABLE"};
static char* g_drv_tdm_txLaserStr[DRV_TDM_INTF_TX_LASER_MAX_SIZE] = {"DISABLE", "ENABLE"};
static char* g_drv_tdm_stm1IntfStateStr[DRV_TDM_STM_INTF_MAX_SIZE] = {"DISABLE", "ENABLE"};
static char* g_drv_tdm_stm1IntfTxClkMdStr[DRV_TDM_STM_INTF_CLK_MAX_SIZE] = {"INTERNAL", "RECOVERED"};
static char* g_drv_tdm_e1LinkLbMdStr[DRV_PDH_LOOPBACK_MAX_SIZE] = {"RELEASE", "INTERNAL PAYLOAD", "EXTERNAL PAYLOAD", "INTERNAL LINE", "EXTERNAL LINE"};
static char* g_drv_tdm_e1TimingStatusStr[DRV_TDM_TIMING_STATE_MAX_SIZE] = {"Unknown", "Not Applicable", "Init", "Holdover", "Learning", "Locked", "Invalid"};
static char* g_drv_tdm_e1ClkSwitchAlmStr[DRV_TDM_CLK_SWITCH_ALM_MAX] = {"NONE ALARM", "ALARM"};
static char* g_drv_tdm_berRateStr[DRV_TDM_BER_RATE_MAX_SIZE] = {"Unknown", "1e-3", "1e-4", "1e-5", "1e-6", "1e-7", "1e-8", "1e-9", "1e-10"};
static char* g_drv_tdm_pwAttributeStr[DRV_TDM_PW_ATTRIBUTE_MAX] = {"SLAVE", "MASTER"};
static char* g_drv_tdm_e1BindingStateStr[DRV_TDM_E1_LINK_BINDING_MAX] = {"UNBINDING", "BINDING"};
static char* g_drv_tdm_ethIntfFlagStr[DRV_TDM_ETH_INTF_FLAG_MAX] = {"NONE SWITCH", "SWITCH"};
static char* g_drv_tdm_fpgaClkStateStr[DRV_TDM_FPGA_CLK_MAX] = {"GOOD", "BAD"};
static char* g_drv_tdm_fpgaRamTestedStateStr[DRV_TDM_FPGA_RAM_TEST_MAX] = {"UNFINISHED", "FINISHED"};
static char* g_drv_tdm_fpgaRamStateStr[DRV_TDM_FPGA_RAM_MAX] = {"GOOD", "BAD"};
static char* g_drv_tdm_fpgaLogicStateStr[DRV_TDM_FPGA_LOGIC_MAX] = {"GOOD", "BAD"};
static char* g_drv_tdm_fpgaPhyIntfStateStr[DRV_TDM_FPGA_PHY_INTF_MAX] = {"GOOD", "BAD"};


/**************************************************************************
* 函数名称: drv_tdm_debugFlagShow
* 功能描述: 显示STM TDM PWE3模块的调试打印开关的值.
* 访问的表: 无.
* 修改的表: 无.
* 输入参数: 无.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-07   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_debugFlagShow(VOID)
{
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_debug_flag = 0x%x.\n", drv_tdm_debugFlagGet());
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwe3InitFlagShow
* 功能描述: 显示STM TDM PWE3模块的初始化标记.
* 访问的表: 无.
* 修改的表: 无.
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-14   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwe3InitFlagShow(BYTE subslotId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;
    WORD32 initFlag = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    
    rv = drv_tdm_pwe3InitFlagGet(chipId, &initFlag);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_pwe3InitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_pwe3_init_flag[%u] = %u.\n", chipId, initFlag);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_threadIdShow
* 功能描述: 显示STM单板上的相关threadId.
* 访问的表: 无.
* 修改的表: 无.
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-14   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_threadIdShow(BYTE subslotId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    pthread_t threadId = 0;
    BYTE chipId = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;

    threadId = 0;
    rv = drv_tdm_sdhBerThreadIdGet(chipId, &threadId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_sdhBerThreadIdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_sdh_ber_thread_id[%u]=0x%x.\n", chipId, threadId);

    threadId = 0;
    rv = drv_tdm_e1ClkAlmThreadIdGet(chipId, &threadId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_e1ClkAlmThreadIdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_e1ClkSwitchAlmThreadId[%u]=0x%x.\n", chipId, threadId);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_tIdShow
* 功能描述: 显示STM单板上的相关TID.
* 访问的表: 无.
* 修改的表: 无.
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-14   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_tIdShow(BYTE subslotId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    int tId = 0;
    BYTE chipId = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;

    tId = 0;
    rv = drv_tdm_sdhBerTidGet(chipId, &tId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_sdhBerTidGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_sdh_ber_tid[%u]=%d.\n", chipId, tId);

    tId = 0;
    rv = drv_tdm_e1ClkSwitchAlmTidGet(chipId, &tId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_e1ClkSwitchAlmTidGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_e1ClkSwitchAlmTid[%u]=%d.\n", chipId, tId);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_threadMonitorFlagShow
* 功能描述: 显示STM单板上的监测线程的标记.
* 访问的表: 无.
* 修改的表: 无.
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-01   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_threadMonitorFlagShow(BYTE subslotId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 flag = 0;
    BYTE chipId = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;

    flag = 0;
    rv = drv_tdm_sdhBerFlagGet(chipId, &flag);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_sdhBerFlagGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_sdh_ber_enable[%u]=0x%x.\n", chipId, flag);

    flag = 0;
    rv = drv_tdm_e1ClkSwitchAlmFlagGet(chipId, &flag);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_e1ClkSwitchAlmFlagGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_e1ClkSwtichAlmFlag[%u]=0x%x.\n", chipId, flag);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stmIntfModeShow
* 功能描述: 显示STM接口的模式.
* 访问的表: 无.
* 修改的表: 无
* 输入参数: ucSubslotId: STM单板的子槽位号,取值为1~4.
*           ucPortId: STM端口编号,取值为1~8.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-01-14   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stmIntfModeShow(BYTE ucSubslotId, BYTE ucPortId)
{
    WORD32 dwRetVal = DRV_TDM_OK;  /* 函数返回码 */
    BYTE ucChipId = 0;
    WORD32 dwIntfMode = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(ucSubslotId);
    ucChipId = ucSubslotId - (BYTE)1;
    dwRetVal = drv_tdm_stmPortIdCheck(ucChipId, ucPortId);
    if (DRV_TDM_OK != dwRetVal)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, dwRetVal);
        return dwRetVal;
    }
    
    dwRetVal = drv_tdm_stmIntfModeGet(ucChipId, ucPortId, &dwIntfMode);
    if (DRV_TDM_OK != dwRetVal)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stmIntfModeGet() rv=0x%x.\n", __FILE__, __LINE__, dwRetVal);
        return dwRetVal;
    }
    
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "\n[subslotId %u portId %u]'s STM interface mode: ", ucSubslotId, ucPortId);
    if (DRV_TDM_STM1_INTF == dwIntfMode)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "STM-1\n");
    }
    else if (DRV_TDM_STM4_INTF == dwIntfMode)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "STM-4\n");
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "INVALID\n");
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atDevDriverPtrShow
* 功能描述: 显示ARRIVE芯片的driver指针
* 访问的表: 无.
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-14   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atDevDriverPtrShow(BYTE subslotId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;
    AtDriver pDiver = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    
    rv = drv_tdm_atDevDriverPtrGet(&pDiver);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atDevDriverPtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDiver);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "chipId = %u, pDiver = %p.\n", chipId, pDiver);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atDevicePtrShow
* 功能描述: 显示ARRIVE芯片的device指针
* 访问的表: 无.
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.  
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-14   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atDevicePtrShow(BYTE subslotId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;
    AtDevice pDevice = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    
    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "chipId=%u, pDevice=%p.\n", chipId, pDevice);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atDevHalShow
* 功能描述: 显示ARRIVE芯片的HAL指针
* 访问的表: 无.
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.   
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-24   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atDevHalShow(BYTE subslotId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;
    AtHal pHal = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    
    rv = drv_tdm_atDevHalGet(chipId, &pHal);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atDevHalGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pHal);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "chipId=%u, pHal=%p.\n", chipId, pHal);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stm1SdhPtrShow
* 功能描述: 显示STM-1接口的SDH pointer.
* 访问的表: 无.
* 修改的表: 无
* 输入参数: subslotId: 接口卡的子槽位号,取值为1~4.  
*           portId: STM-1端口的编号,取值为1~8.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-24   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stm1SdhPtrShow(BYTE subslotId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;  /* 芯片编号,取值为0~3. */
    BYTE stmIntfIdx = 0;  /* STM接口的索引号,取值为0~7. */
    BYTE e1LinkId = 0;  /* VC4中的E1链路的编号,取值为1~63. */
    AtSdhLine pLine = NULL;
    AtSdhAug pAug1 = NULL;
    AtSdhAu pAu4 = NULL;
    AtSdhVc pVc4 = NULL;
    AtSdhTu pTu12 = NULL;
    AtSdhVc pVc12 = NULL;
    AtPdhDe1 pE1 = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    stmIntfIdx = portId - (BYTE)1;
    
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_atSdhLinePtr[%u][%u] = %p.\n", chipId, stmIntfIdx, pLine);

    rv = drv_tdm_atSdhAug1PtrGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, &pAug1);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atSdhAug1PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pAug1);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_atSdhAug1Ptr[%u][%u] = %p.\n", chipId, stmIntfIdx, pAug1);

    rv = drv_tdm_atSdhAu4PtrGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, &pAu4);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atSdhAu4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pAu4);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_atSdhAu4Ptr[%u][%u] = %p.\n", chipId, stmIntfIdx, pAu4);
    
    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_atSdhVc4Ptr[%u][%u] = %p.\n", chipId, stmIntfIdx, pVc4);

    for (e1LinkId = DRV_TDM_E1_LINK_ID_START; e1LinkId <= DRV_TDM_E1_LINK_ID_END; e1LinkId++)
    {
        pTu12 = NULL;
        rv = drv_tdm_atSdhTu12PtrGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, &pTu12);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atSdhTu12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        DRV_TDM_CHECK_POINTER_IS_NULL(pTu12);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_atSdhTu12Ptr[%u][%u][%u] = %p.\n", chipId, stmIntfIdx, e1LinkId, pTu12);
        
        pVc12 = NULL; 
        rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, &pVc12);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_atSdhVc12Ptr[%u][%u][%u] = %p.\n", chipId, stmIntfIdx, e1LinkId, pVc12);

        pE1 = NULL;
        rv = drv_tdm_atPdhE1PtrGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, &pE1);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atPdhE1PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        DRV_TDM_CHECK_POINTER_IS_NULL(pE1);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_atPdhE1Ptr[%u][%u][%u] = %p.\n", chipId, stmIntfIdx, e1LinkId, pE1);
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stm4SdhPtrShow
* 功能描述: 显示STM-4接口的SDH pointer.
* 访问的表: 无.
* 修改的表: 无
* 输入参数: subslotId: 接口卡的子槽位号,取值为1~4.  
*           portId: STM-4端口的编号,取值为1或者5.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-01-28   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stm4SdhPtrShow(BYTE subslotId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;  /* 芯片编号,取值为0~3. */
    BYTE stmIntfIdx = 0;  /* STM接口的索引号,取值为0~7. */
    BYTE aug1Id = 0;      /* AUG1编号,取值为1~4. */
    BYTE e1LinkId = 0;  /* VC4中的E1链路的编号,取值为1~63. */
    AtSdhLine pLine = NULL;
    AtSdhAug pAug4 = NULL;
    AtSdhAug pAug1 = NULL;
    AtSdhAu pAu4 = NULL;
    AtSdhVc pVc4 = NULL;
    AtSdhTu pTu12 = NULL;
    AtSdhVc pVc12 = NULL;
    AtPdhDe1 pE1 = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stm4PortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stm4PortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_atSdhLinePtr[%u][%u] = %p.\n", chipId, (portId - 1), pLine);

    rv = drv_tdm_atSdhAug4PtrGet(chipId, portId, &pAug4);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atSdhAug4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pAug4);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_atSdhAug4Ptr[%u][%u] = %p.\n", chipId, (portId - 1), pAug4);

    for (aug1Id = DRV_TDM_MIN_AUG1_ID; aug1Id <= DRV_TDM_MAX_AUG1_ID; aug1Id++)
    {
        rv = drv_tdm_aug1IndexGet(portId, aug1Id, &stmIntfIdx);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_aug1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }

        rv = drv_tdm_atSdhAug1PtrGet(chipId, portId, aug1Id, &pAug1);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atSdhAug1PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        DRV_TDM_CHECK_POINTER_IS_NULL(pAug1);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_atSdhAug1Ptr[%u][%u] = %p.\n", chipId, stmIntfIdx, pAug1);
        
        rv = drv_tdm_atSdhAu4PtrGet(chipId, portId, aug1Id, &pAu4);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atSdhAu4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        DRV_TDM_CHECK_POINTER_IS_NULL(pAu4);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_atSdhAu4Ptr[%u][%u] = %p.\n", chipId, stmIntfIdx, pAu4);
        
        rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_atSdhVc4Ptr[%u][%u] = %p.\n", chipId, stmIntfIdx, pVc4);
        
        for (e1LinkId = DRV_TDM_E1_LINK_ID_START; e1LinkId <= DRV_TDM_E1_LINK_ID_END; e1LinkId++)
        {
            pTu12 = NULL;
            rv = drv_tdm_atSdhTu12PtrGet(chipId, portId, aug1Id, e1LinkId, &pTu12);
            if (DRV_TDM_OK != rv)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atSdhTu12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                return rv;
            }
            DRV_TDM_CHECK_POINTER_IS_NULL(pTu12);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_atSdhTu12Ptr[%u][%u][%u] = %p.\n", chipId, stmIntfIdx, e1LinkId, pTu12);
            
            pVc12 = NULL; 
            rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12);
            if (DRV_TDM_OK != rv)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                return rv;
            }
            DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_atSdhVc12Ptr[%u][%u][%u] = %p.\n", chipId, stmIntfIdx, e1LinkId, pVc12);
            
            pE1 = NULL;
            rv = drv_tdm_atPdhE1PtrGet(chipId, portId, aug1Id, e1LinkId, &pE1);
            if (DRV_TDM_OK != rv)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atPdhE1PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                return rv;
            }
            DRV_TDM_CHECK_POINTER_IS_NULL(pE1);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_atPdhE1Ptr[%u][%u][%u] = %p.\n", chipId, stmIntfIdx, e1LinkId, pE1);
        }
    }

    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atDevHalRegShow
* 功能描述: 显示ARRIVE芯片的寄存器的值.
* 访问的表: 无.
* 修改的表: 无
* 输入参数: subslotId: 接口卡的子槽位号,取值为1~4.
*           address: ARRIVE芯片的寄存器的地址.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-27   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atDevHalRegShow(BYTE subslotId, WORD32 address)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0; 
    WORD32 regValue = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    
    rv = drv_tdm_atDevHalRegRead(chipId, address, &regValue);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atDevHalRegRead() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "chipId %u, register address 0x%x, register value 0x%x.\n", chipId, address, regValue);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_dbg_atDevRegNTimesRead
* 功能描述: 测试连续N次读取ARRIVE芯片的寄存器.
* 访问的表: 无.
* 修改的表: 无.
* 输入参数: subslotId: 接口卡的子槽位号,取值为1~4.
*           address: 寄存器的地址.
*           times: 读取芯片寄存器的次数.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-06   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_dbg_atDevRegNTimesRead(BYTE subslotId, WORD32 address, WORD32 times)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0; 
    WORD32 regValue = 0;  /* 寄存器的值 */
    WORD32 i = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    
    if (0 == times)
    {
        times = 1;
    }
    
    for (i = 1; i <= times; i++)
    {
        rv = drv_tdm_atDevHalRegRead(chipId, address, &regValue);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atDevHalRegRead() rv=0x%x.\n", __FILE__, __LINE__, rv);
            continue;
        }
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u times %u, register address 0x%x, regValue = 0x%x.\n", subslotId, i, address, regValue);
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_dbg_atDevRegNTimesWrite
* 功能描述: 测试连续N次写入ARRIVE芯片的寄存器.
* 访问的表: 无.
* 修改的表: 无.
* 输入参数: subslotId: 接口卡的子槽位号,取值为1~4.
*           address: 寄存器的地址.
*           testValue: 需要写入寄存器的值.
*           times: 写入芯片寄存器的次数.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-06   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_dbg_atDevRegNTimesWrite(BYTE subslotId, WORD32 address, WORD32 testValue, WORD32 times)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0; 
    WORD32 regValue = 0;  /* 寄存器的值 */
    WORD32 i = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    
    if (0 == times)
    {
        times = 1;
    }
    
    for (i = 1; i <= times; i++)
    {
        rv = drv_tdm_atDevHalRegWrite(chipId, address, testValue);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atDevHalRegWrite() rv=0x%x.\n", __FILE__, __LINE__, rv);
            continue;
        }
        rv = drv_tdm_atDevHalRegRead(chipId, address, &regValue);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atDevHalRegRead() rv=0x%x.\n", __FILE__, __LINE__, rv);
            continue;
        }
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u times %u, register address 0x%x, regValue = 0x%x.\n", subslotId, i, address, regValue);
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stm1AlarmShow
* 功能描述: 显示STM-1帧的SDH告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
*           portId: STM-1端口编号,取值为1~8.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 包括RS/MS/AU4/VC4/TU12/VC12/E1级别的告警.直接读取芯片来获取告警.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-27   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stm1AlarmShow(BYTE subslotId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;         /* ARRIVE芯片的芯片编号 */
    BYTE e1LinkId = 0;
    BYTE tug3Id = 0;
    BYTE tug2Id = 0;
    BYTE tu12Id = 0;
    DRV_TDM_SECTION_ALARM sectionAlm;
    DRV_TDM_VC4_ALARM vc4Alm;
    DRV_TDM_VC12_ALARM vc12Alm;
    DRV_TDM_E1_ALARM e1Alm;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    memset(&sectionAlm, 0, sizeof(DRV_TDM_SECTION_ALARM));
    rv = drv_tdm_sectionAlarmGet(chipId, portId, &sectionAlm);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_sectionAlarmGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    memset(&vc4Alm, 0, sizeof(DRV_TDM_VC4_ALARM));
    rv = drv_tdm_highPathAlarmGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, &vc4Alm);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_highPathAlarmGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "\n[subslotId %u portId %u]'s SDH alarm info: \n", subslotId, portId);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "RS-LOS    : %u\n", sectionAlm.rsLos);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "RS-LOF    : %u\n", sectionAlm.rsLof);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "RS-OOF    : %u\n", sectionAlm.rsOof);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "RS-TIM    : %u\n", sectionAlm.rsTim);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "RS-SD     : %u\n", sectionAlm.rsSd);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "RS-SF     : %u\n", sectionAlm.rsSf);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "MS-AIS    : %u\n", sectionAlm.msAis);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "MS-RDI    : %u\n", sectionAlm.msRdi);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "MS-SD     : %u\n", sectionAlm.msSd);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "MS-SF     : %u\n", sectionAlm.msSf);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "AU4-LOP   : %u\n", vc4Alm.au4Lop);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "AU4-AIS   : %u\n", vc4Alm.au4Ais);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VC4-LOMF  : %u\n", vc4Alm.vc4Lomf);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VC4-UNEQ  : %u\n", vc4Alm.vc4Uneq);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VC4-PLM   : %u\n", vc4Alm.vc4Plm);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VC4-TIM   : %u\n", vc4Alm.vc4Tim);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VC4-RDI   : %u\n", vc4Alm.vc4Rdi);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VC4-ERDIS : %u\n", vc4Alm.vc4ERdiS);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VC4-ERDIP : %u\n", vc4Alm.vc4ERdiP);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VC4-ERDIC : %u\n", vc4Alm.vc4ERdiC);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VC4-SD    : %u\n", vc4Alm.vc4Sd);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VC4-SF    : %u\n", vc4Alm.vc4Sf);
    for (e1LinkId = DRV_TDM_E1_LINK_ID_START; e1LinkId <= DRV_TDM_E1_LINK_ID_END; e1LinkId++)
    {
        rv = drv_tdm_tug3Tug2Tu12IdGet(e1LinkId, &tug3Id, &tug2Id, &tu12Id);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_tug3Tug2Tu12IdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }

        memset(&vc12Alm, 0, sizeof(DRV_TDM_VC12_ALARM));
        rv = drv_tdm_lowPathAlarmGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, &vc12Alm);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_lowPathAlarmGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }

        memset(&e1Alm, 0, sizeof(DRV_TDM_E1_ALARM));
        rv = drv_tdm_e1AlarmGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, &e1Alm);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_e1AlarmGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] TU12-LOP   : %u\n", tug3Id, tug2Id, tu12Id, vc12Alm.tu12Lop);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] TU12-AIS   : %u\n", tug3Id, tug2Id, tu12Id, vc12Alm.tu12Ais);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] VC12-UNEQ  : %u\n", tug3Id, tug2Id, tu12Id, vc12Alm.vc12Uneq);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] VC12-PLM   : %u\n", tug3Id, tug2Id, tu12Id, vc12Alm.vc12Plm);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] VC12-TIM   : %u\n", tug3Id, tug2Id, tu12Id, vc12Alm.vc12Tim);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] VC12-RDI   : %u\n", tug3Id, tug2Id, tu12Id, vc12Alm.vc12Rdi);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] VC12-ERDIS : %u\n", tug3Id, tug2Id, tu12Id, vc12Alm.vc12ERdiS);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] VC12-ERDIP : %u\n", tug3Id, tug2Id, tu12Id, vc12Alm.vc12ERdiP);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] VC12-ERDIC : %u\n", tug3Id, tug2Id, tu12Id, vc12Alm.vc12ERdiC);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] VC12-SD    : %u\n", tug3Id, tug2Id, tu12Id, vc12Alm.vc12Sd);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] VC12-SF    : %u\n", tug3Id, tug2Id, tu12Id, vc12Alm.vc12Sf);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] E1-LOF     : %u\n", tug3Id, tug2Id, tu12Id, e1Alm.e1Lof);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] E1-LOMF    : %u\n", tug3Id, tug2Id, tu12Id, e1Alm.e1Lomf);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] E1-AIS     : %u\n", tug3Id, tug2Id, tu12Id, e1Alm.e1Ais);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] E1-RAI     : %u\n", tug3Id, tug2Id, tu12Id, e1Alm.e1Rai);
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stm4AlarmShow
* 功能描述: 显示STM-4帧的SDH告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
*           portId: STM-4端口编号,取值为1或者5.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 包括RS/MS/AU4/VC4/TU12/VC12/E1级别的告警.直接读取芯片来获取告警.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-01-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stm4AlarmShow(BYTE subslotId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;         /* ARRIVE芯片的芯片编号 */
    BYTE aug1Id = 0;
    BYTE e1LinkId = 0;
    BYTE tug3Id = 0;
    BYTE tug2Id = 0;
    BYTE tu12Id = 0;
    DRV_TDM_SECTION_ALARM sectionAlm;
    DRV_TDM_VC4_ALARM vc4Alm;
    DRV_TDM_VC12_ALARM vc12Alm;
    DRV_TDM_E1_ALARM e1Alm;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stm4PortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stm4PortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    memset(&sectionAlm, 0, sizeof(DRV_TDM_SECTION_ALARM));
    rv = drv_tdm_sectionAlarmGet(chipId, portId, &sectionAlm);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_sectionAlarmGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "\n[subslotId %u portId %u]'s SDH alarm info: \n", subslotId, portId);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "RS-LOS    : %u\n", sectionAlm.rsLos);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "RS-LOF    : %u\n", sectionAlm.rsLof);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "RS-OOF    : %u\n", sectionAlm.rsOof);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "RS-TIM    : %u\n", sectionAlm.rsTim);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "RS-SD     : %u\n", sectionAlm.rsSd);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "RS-SF     : %u\n", sectionAlm.rsSf);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "MS-AIS    : %u\n", sectionAlm.msAis);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "MS-RDI    : %u\n", sectionAlm.msRdi);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "MS-SD     : %u\n", sectionAlm.msSd);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "MS-SF     : %u\n", sectionAlm.msSf);

    for (aug1Id = DRV_TDM_MIN_AUG1_ID; aug1Id <= DRV_TDM_MAX_AUG1_ID; aug1Id++)
    {
        memset(&vc4Alm, 0, sizeof(DRV_TDM_VC4_ALARM));
        rv = drv_tdm_highPathAlarmGet(chipId, portId, aug1Id, &vc4Alm);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_highPathAlarmGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, AU4-LOP   : %u\n", aug1Id, vc4Alm.au4Lop);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, AU4-AIS   : %u\n", aug1Id, vc4Alm.au4Ais);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, VC4-LOMF  : %u\n", aug1Id, vc4Alm.vc4Lomf);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, VC4-UNEQ  : %u\n", aug1Id, vc4Alm.vc4Uneq);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, VC4-PLM   : %u\n", aug1Id, vc4Alm.vc4Plm);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, VC4-TIM   : %u\n", aug1Id, vc4Alm.vc4Tim);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, VC4-RDI   : %u\n", aug1Id, vc4Alm.vc4Rdi);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, VC4-ERDIS : %u\n", aug1Id, vc4Alm.vc4ERdiS);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, VC4-ERDIP : %u\n", aug1Id, vc4Alm.vc4ERdiP);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, VC4-ERDIC : %u\n", aug1Id, vc4Alm.vc4ERdiC);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, VC4-SD    : %u\n", aug1Id, vc4Alm.vc4Sd);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, VC4-SF    : %u\n", aug1Id, vc4Alm.vc4Sf);

        for (e1LinkId = DRV_TDM_E1_LINK_ID_START; e1LinkId <= DRV_TDM_E1_LINK_ID_END; e1LinkId++)
        {
            rv = drv_tdm_tug3Tug2Tu12IdGet(e1LinkId, &tug3Id, &tug2Id, &tu12Id);
            if (DRV_TDM_OK != rv)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_tug3Tug2Tu12IdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                return rv;
            }
            
            memset(&vc12Alm, 0, sizeof(DRV_TDM_VC12_ALARM));
            rv = drv_tdm_lowPathAlarmGet(chipId, portId, aug1Id, e1LinkId, &vc12Alm);
            if (DRV_TDM_OK != rv)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_lowPathAlarmGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                return rv;
            }
            
            memset(&e1Alm, 0, sizeof(DRV_TDM_E1_ALARM));
            rv = drv_tdm_e1AlarmGet(chipId, portId, aug1Id, e1LinkId, &e1Alm);
            if (DRV_TDM_OK != rv)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_e1AlarmGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                return rv;
            }
            
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] TU12-LOP   : %u\n", aug1Id, tug3Id, tug2Id, tu12Id, vc12Alm.tu12Lop);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] TU12-AIS   : %u\n", aug1Id, tug3Id, tug2Id, tu12Id, vc12Alm.tu12Ais);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] VC12-UNEQ  : %u\n", aug1Id, tug3Id, tug2Id, tu12Id, vc12Alm.vc12Uneq);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] VC12-PLM   : %u\n", aug1Id, tug3Id, tug2Id, tu12Id, vc12Alm.vc12Plm);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] VC12-TIM   : %u\n", aug1Id, tug3Id, tug2Id, tu12Id, vc12Alm.vc12Tim);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] VC12-RDI   : %u\n", aug1Id, tug3Id, tug2Id, tu12Id, vc12Alm.vc12Rdi);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] VC12-ERDIS : %u\n", aug1Id, tug3Id, tug2Id, tu12Id, vc12Alm.vc12ERdiS);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] VC12-ERDIP : %u\n", aug1Id, tug3Id, tug2Id, tu12Id, vc12Alm.vc12ERdiP);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] VC12-ERDIC : %u\n", aug1Id, tug3Id, tug2Id, tu12Id, vc12Alm.vc12ERdiC);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] VC12-SD    : %u\n", aug1Id, tug3Id, tug2Id, tu12Id, vc12Alm.vc12Sd);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] VC12-SF    : %u\n", aug1Id, tug3Id, tug2Id, tu12Id, vc12Alm.vc12Sf);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] E1-LOF     : %u\n", aug1Id, tug3Id, tug2Id, tu12Id, e1Alm.e1Lof);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] E1-LOMF    : %u\n", aug1Id, tug3Id, tug2Id, tu12Id, e1Alm.e1Lomf);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] E1-AIS     : %u\n", aug1Id, tug3Id, tug2Id, tu12Id, e1Alm.e1Ais);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] E1-RAI     : %u\n", aug1Id, tug3Id, tug2Id, tu12Id, e1Alm.e1Rai);
        }
    }
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stm1AlmQryShow
* 功能描述: 显示STM-1告警的查询结果.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
*           portId: STM-1端口编号,取值为1~8.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 显示给产品管理的STM-1告警查询结果.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-29   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stm1AlmQryShow(BYTE subslotId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0; 
    BYTE e1LinkId = 0;
    BYTE tug3Id = 0;
    BYTE tug2Id = 0;
    BYTE tu12Id = 0;
    WORD32 qryResult = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "\n[subslotId %u portId %u]'s STM-1 alarm query result: \n", subslotId, portId);
    qryResult = 0;
    rv = drv_tdm_sectionAlarmQuery(chipId, portId, &qryResult);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_sectionAlarmQuery() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "Section alarm query result: 0x%x\n", qryResult);
    
    qryResult = 0;
    rv = drv_tdm_highPathAlarmQuery(chipId, portId, DRV_TDM_STM1_AUG1_ID, &qryResult);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_highPathAlarmQuery() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VC4 path alarm query result: 0x%x\n", qryResult);
    
    for (e1LinkId = DRV_TDM_E1_LINK_ID_START; e1LinkId <= DRV_TDM_E1_LINK_ID_END; e1LinkId++)
    {
        rv = drv_tdm_tug3Tug2Tu12IdGet(e1LinkId, &tug3Id, &tug2Id, &tu12Id);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_tug3Tug2Tu12IdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            continue;
        }
        
        qryResult = 0;
        rv = drv_tdm_lowPathAlarmQuery(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, &qryResult);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_lowPathAlarmQuery() rv=0x%x.\n", __FILE__, __LINE__, rv);
            continue;
        }
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "[%u-%u-%u] VC12 path alarm query result: 0x%x\n", tug3Id, tug2Id, tu12Id, qryResult);
        
        qryResult = 0;
        rv = drv_tdm_e1AlarmQuery(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, &qryResult);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_e1AlarmQuery() rv=0x%x.\n", __FILE__, __LINE__, rv);
            continue;
        }
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "[%u-%u-%u] E1 alarm query result: 0x%x\n", tug3Id, tug2Id, tu12Id, qryResult);
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stm4AlmQryShow
* 功能描述: 显示STM-4告警的查询结果.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
*           portId: STM-4端口编号,取值为1或者5.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 显示给产品管理的STM-4告警查询结果.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-01-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stm4AlmQryShow(BYTE subslotId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0; 
    BYTE aug1Id = 0;
    BYTE e1LinkId = 0;
    BYTE tug3Id = 0;
    BYTE tug2Id = 0;
    BYTE tu12Id = 0;
    WORD32 qryResult = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stm4PortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stm4PortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "\n[subslotId %u portId %u]'s STM-1 alarm query result: \n", subslotId, portId);
    qryResult = 0;
    rv = drv_tdm_sectionAlarmQuery(chipId, portId, &qryResult);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_sectionAlarmQuery() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "Section alarm query result: 0x%x\n", qryResult);

    for (aug1Id = DRV_TDM_MIN_AUG1_ID; aug1Id <= DRV_TDM_MAX_AUG1_ID; aug1Id++)
    {
        qryResult = 0;
        rv = drv_tdm_highPathAlarmQuery(chipId, portId, aug1Id, &qryResult);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_highPathAlarmQuery() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, VC4 path alarm query result: 0x%x\n", aug1Id, qryResult);
        
        for (e1LinkId = DRV_TDM_E1_LINK_ID_START; e1LinkId <= DRV_TDM_E1_LINK_ID_END; e1LinkId++)
        {
            rv = drv_tdm_tug3Tug2Tu12IdGet(e1LinkId, &tug3Id, &tug2Id, &tu12Id);
            if (DRV_TDM_OK != rv)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_tug3Tug2Tu12IdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                continue;
            }
            
            qryResult = 0;
            rv = drv_tdm_lowPathAlarmQuery(chipId, portId, aug1Id, e1LinkId, &qryResult);
            if (DRV_TDM_OK != rv)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_lowPathAlarmQuery() rv=0x%x.\n", __FILE__, __LINE__, rv);
                continue;
            }
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, [%u-%u-%u] VC12 path alarm query result: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, qryResult);
            
            qryResult = 0;
            rv = drv_tdm_e1AlarmQuery(chipId, portId, aug1Id, e1LinkId, &qryResult);
            if (DRV_TDM_OK != rv)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_e1AlarmQuery() rv=0x%x.\n", __FILE__, __LINE__, rv);
                continue;
            }
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, [%u-%u-%u] E1 alarm query result: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, qryResult);
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atStm1PerfShow
* 功能描述: 显示STM-1帧的SDH性能统计计数(一次性值).
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.
*           portId: STM-1端口的端口号,取值为1~8.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 直接读取芯片来显示统计计数.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-27   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atStm1PerfShow(BYTE subslotId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;         /* ARRIVE芯片的芯片编号 */
    BYTE e1LinkId = 0;
    BYTE tug3Id = 0;
    BYTE tug2Id = 0;
    BYTE tu12Id = 0;
    DRV_TDM_SECTION_PERF secPerfCnt;
    DRV_TDM_VC4_PERF hpPerfCnt;
    DRV_TDM_VC12_PERF lpPerfCnt;
    DRV_TDM_E1_PERF e1PerfCnt;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    memset(&secPerfCnt, 0, sizeof(DRV_TDM_SECTION_PERF));
    rv = drv_tdm_secPerfCntGet(chipId, portId, &secPerfCnt);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_secPerfCntGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "\n[subslotId %u portId %u]'s STM-1 performance information: \n", subslotId, portId);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "b1ErrCounters: 0x%x\n", secPerfCnt.b1Cnts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "b2ErrCounters: 0x%x\n", secPerfCnt.b2Cnts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "msReiCounters: 0x%x\n", secPerfCnt.msReiCnts);

    memset(&hpPerfCnt, 0, sizeof(DRV_TDM_VC4_PERF));
    rv = drv_tdm_hpPerfCntGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, &hpPerfCnt);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_hpPerfCntGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "b3ErrCounters: 0x%x\n", hpPerfCnt.b3Cnts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "hpReiCounters: 0x%x\n", hpPerfCnt.hpReiCnts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxAu4PositivePointerJustificationCounters: 0x%x\n", hpPerfCnt.rxAu4Ppjc);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxAu4NegativePointerJustificationCounters: 0x%x\n", hpPerfCnt.rxAu4Npjc);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txAu4PositivePointerJustificationCounters: 0x%x\n", hpPerfCnt.txAu4Ppjc);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txAu4NegativePointerJustificationCounters: 0x%x\n", hpPerfCnt.txAu4Npjc);

    for (e1LinkId = DRV_TDM_E1_LINK_ID_START; e1LinkId <= DRV_TDM_E1_LINK_ID_END; e1LinkId++)
    {
        rv = drv_tdm_tug3Tug2Tu12IdGet(e1LinkId, &tug3Id, &tug2Id, &tu12Id);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_tug3Tug2Tu12IdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            continue;
        }

        memset(&lpPerfCnt, 0, sizeof(DRV_TDM_VC12_PERF));
        rv = drv_tdm_lpPerfCntGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, &lpPerfCnt);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_lpPerfCntGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            continue;
        }
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] V5ErrCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, lpPerfCnt.bipCnts);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] lpReiCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, lpPerfCnt.lpReiCnts);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] rxTu12PositivePointerJustificationCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, lpPerfCnt.rxTu12Ppjc);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] rxTu12NegativePointerJustificationCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, lpPerfCnt.rxTu12Npjc);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] txTu12PositivePointerJustificationCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, lpPerfCnt.txTu12Ppjc);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] txTu12NegativePointerJustificationCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, lpPerfCnt.txTu12Npjc);

        memset(&e1PerfCnt, 0, sizeof(DRV_TDM_E1_PERF));
        rv = drv_tdm_e1PerfCntGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, &e1PerfCnt);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_e1PerfCntGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            continue;
        }
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] e1CrcErrorCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, e1PerfCnt.e1CrcErrCnts);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] e1FrmBitErrorCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, e1PerfCnt.e1FrmBitErrCnts);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] e1ReiCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, e1PerfCnt.e1ReiCnts);
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atStm4PerfShow
* 功能描述: 显示STM-4帧的SDH性能统计计数(一次性值).
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.
*           portId: STM-4端口的端口号,取值为1或者5.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 直接读取芯片来显示统计计数.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-27   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atStm4PerfShow(BYTE subslotId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;         /* ARRIVE芯片的芯片编号 */
    BYTE aug1Id = 0;
    BYTE e1LinkId = 0;
    BYTE tug3Id = 0;
    BYTE tug2Id = 0;
    BYTE tu12Id = 0;
    DRV_TDM_SECTION_PERF secPerfCnt;
    DRV_TDM_VC4_PERF hpPerfCnt;
    DRV_TDM_VC12_PERF lpPerfCnt;
    DRV_TDM_E1_PERF e1PerfCnt;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stm4PortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stm4PortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    memset(&secPerfCnt, 0, sizeof(DRV_TDM_SECTION_PERF));
    rv = drv_tdm_secPerfCntGet(chipId, portId, &secPerfCnt);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_secPerfCntGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "\n[subslotId %u portId %u]'s STM-4 performance information: \n", subslotId, portId);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "b1ErrCounters: 0x%x\n", secPerfCnt.b1Cnts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "b2ErrCounters: 0x%x\n", secPerfCnt.b2Cnts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "msReiCounters: 0x%x\n", secPerfCnt.msReiCnts);

    for (aug1Id = DRV_TDM_MIN_AUG1_ID; aug1Id <= DRV_TDM_MAX_AUG1_ID; aug1Id++)
    {
        memset(&hpPerfCnt, 0, sizeof(DRV_TDM_VC4_PERF));
        rv = drv_tdm_hpPerfCntGet(chipId, portId, aug1Id, &hpPerfCnt);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_hpPerfCntGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, b3ErrCounters: 0x%x\n", aug1Id, hpPerfCnt.b3Cnts);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, hpReiCounters: 0x%x\n", aug1Id, hpPerfCnt.hpReiCnts);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, rxAu4PositivePointerJustificationCounters: 0x%x\n", aug1Id, hpPerfCnt.rxAu4Ppjc);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, rxAu4NegativePointerJustificationCounters: 0x%x\n", aug1Id, hpPerfCnt.rxAu4Npjc);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, txAu4PositivePointerJustificationCounters: 0x%x\n", aug1Id, hpPerfCnt.txAu4Ppjc);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, txAu4NegativePointerJustificationCounters: 0x%x\n", aug1Id, hpPerfCnt.txAu4Npjc);
        
        for (e1LinkId = DRV_TDM_E1_LINK_ID_START; e1LinkId <= DRV_TDM_E1_LINK_ID_END; e1LinkId++)
        {
            rv = drv_tdm_tug3Tug2Tu12IdGet(e1LinkId, &tug3Id, &tug2Id, &tu12Id);
            if (DRV_TDM_OK != rv)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_tug3Tug2Tu12IdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                continue;
            }
            
            memset(&lpPerfCnt, 0, sizeof(DRV_TDM_VC12_PERF));
            rv = drv_tdm_lpPerfCntGet(chipId, portId, aug1Id, e1LinkId, &lpPerfCnt);
            if (DRV_TDM_OK != rv)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_lpPerfCntGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                continue;
            }
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] V5ErrCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, lpPerfCnt.bipCnts);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] lpReiCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, lpPerfCnt.lpReiCnts);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] rxTu12PositivePointerJustificationCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, lpPerfCnt.rxTu12Ppjc);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] rxTu12NegativePointerJustificationCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, lpPerfCnt.rxTu12Npjc);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] txTu12PositivePointerJustificationCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, lpPerfCnt.txTu12Ppjc);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] txTu12NegativePointerJustificationCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, lpPerfCnt.txTu12Npjc);

            memset(&e1PerfCnt, 0, sizeof(DRV_TDM_E1_PERF));
            rv = drv_tdm_e1PerfCntGet(chipId, portId, aug1Id, e1LinkId, &e1PerfCnt);
            if (DRV_TDM_OK != rv)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_e1PerfCntGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                continue;
            }
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] e1CrcErrCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, e1PerfCnt.e1CrcErrCnts);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] e1FrmBitErrCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, e1PerfCnt.e1FrmBitErrCnts);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] e1ReiCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, e1PerfCnt.e1ReiCnts);
        }
    }

    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stm1PerfShow
* 功能描述: 显示STM-1帧的SDH性能统计计数(一次性值).
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.
*           portId: STM-1端口的端口号,取值为1~8.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 读取驱动的软件表来显示统计计数.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-27   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stm1PerfShow(BYTE subslotId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;         /* ARRIVE芯片的芯片编号 */
    BYTE e1LinkId = 0;
    BYTE tug3Id = 0;
    BYTE tug2Id = 0;
    BYTE tu12Id = 0;
    DRV_TDM_SDH_PERF *pPerfCnt = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_sdhPerfMemGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, &pPerfCnt);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_sdhPerfMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pPerfCnt);
    
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "\n[subslotId %u portId %u]'s STM-1 performance information: \n", subslotId, portId);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "b1ErrCounters: 0x%x\n", pPerfCnt->sectionPerf.b1Cnts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "b2ErrCounters: 0x%x\n", pPerfCnt->sectionPerf.b2Cnts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "msReiCounters: 0x%x\n", pPerfCnt->sectionPerf.msReiCnts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "b3ErrCounters: 0x%x\n", pPerfCnt->vc4Perf.b3Cnts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "hpReiCounters: 0x%x\n", pPerfCnt->vc4Perf.hpReiCnts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxAu4PositivePointerJustificationCounters: 0x%x\n", pPerfCnt->vc4Perf.rxAu4Ppjc);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxAu4NegativePointerJustificationCounters: 0x%x\n", pPerfCnt->vc4Perf.rxAu4Npjc);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txAu4PositivePointerJustificationCounters: 0x%x\n", pPerfCnt->vc4Perf.txAu4Ppjc);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txAu4NegativePointerJustificationCounters: 0x%x\n", pPerfCnt->vc4Perf.txAu4Npjc);
    
    for (e1LinkId = DRV_TDM_E1_LINK_ID_START; e1LinkId <= DRV_TDM_E1_LINK_ID_END; e1LinkId++)
    {
        rv = drv_tdm_tug3Tug2Tu12IdGet(e1LinkId, &tug3Id, &tug2Id, &tu12Id);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_tug3Tug2Tu12IdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            continue;
        }
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] V5ErrCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, pPerfCnt->vc12Perf[e1LinkId].bipCnts);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] lpReiCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, pPerfCnt->vc12Perf[e1LinkId].lpReiCnts);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] rxTu12PositivePointerJustificationCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, pPerfCnt->vc12Perf[e1LinkId].rxTu12Ppjc);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] rxTu12NegativePointerJustificationCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, pPerfCnt->vc12Perf[e1LinkId].rxTu12Npjc);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] txTu12PositivePointerJustificationCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, pPerfCnt->vc12Perf[e1LinkId].txTu12Ppjc);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] txTu12NegativePointerJustificationCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, pPerfCnt->vc12Perf[e1LinkId].txTu12Npjc);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] e1CrcErrCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, pPerfCnt->e1Perf[e1LinkId].e1CrcErrCnts);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] e1FrmBitErrCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, pPerfCnt->e1Perf[e1LinkId].e1FrmBitErrCnts);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] e1ReiCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, pPerfCnt->e1Perf[e1LinkId].e1ReiCnts);
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stm4PerfShow
* 功能描述: 显示STM-4帧的SDH性能统计计数(一次性值).
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.
*           portId: STM-4端口的端口号,取值为1或者5.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 读取驱动的软件表来显示统计计数.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-01-29   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stm4PerfShow(BYTE subslotId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;         /* ARRIVE芯片的芯片编号 */
    BYTE aug1Id = 0;
    BYTE e1LinkId = 0;
    BYTE tug3Id = 0;
    BYTE tug2Id = 0;
    BYTE tu12Id = 0;
    DRV_TDM_SDH_PERF *pPerfCnt = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stm4PortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stm4PortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_sdhPerfMemGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, &pPerfCnt);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_sdhPerfMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pPerfCnt);
    
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "\n[subslotId %u portId %u]'s STM-4 performance information: \n", subslotId, portId);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "b1ErrCounters: 0x%x\n", pPerfCnt->sectionPerf.b1Cnts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "b2ErrCounters: 0x%x\n", pPerfCnt->sectionPerf.b2Cnts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "msReiCounters: 0x%x\n", pPerfCnt->sectionPerf.msReiCnts);

    for (aug1Id = DRV_TDM_MIN_AUG1_ID; aug1Id <= DRV_TDM_MAX_AUG1_ID; aug1Id++)
    {
        pPerfCnt = NULL;
        rv = drv_tdm_sdhPerfMemGet(chipId, portId, aug1Id, &pPerfCnt);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_sdhPerfMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        DRV_TDM_CHECK_POINTER_IS_NULL(pPerfCnt);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, b3ErrCounters: 0x%x\n", aug1Id, pPerfCnt->vc4Perf.b3Cnts);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, hpReiCounters: 0x%x\n", aug1Id, pPerfCnt->vc4Perf.hpReiCnts);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, rxAu4PositivePointerJustificationCounters: 0x%x\n", aug1Id, pPerfCnt->vc4Perf.rxAu4Ppjc);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, rxAu4NegativePointerJustificationCounters: 0x%x\n", aug1Id, pPerfCnt->vc4Perf.rxAu4Npjc);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, txAu4PositivePointerJustificationCounters: 0x%x\n", aug1Id, pPerfCnt->vc4Perf.txAu4Ppjc);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, txAu4NegativePointerJustificationCounters: 0x%x\n", aug1Id, pPerfCnt->vc4Perf.txAu4Npjc);
        
        for (e1LinkId = DRV_TDM_E1_LINK_ID_START; e1LinkId <= DRV_TDM_E1_LINK_ID_END; e1LinkId++)
        {
            rv = drv_tdm_tug3Tug2Tu12IdGet(e1LinkId, &tug3Id, &tug2Id, &tu12Id);
            if (DRV_TDM_OK != rv)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_tug3Tug2Tu12IdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                continue;
            }
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] V5ErrCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, pPerfCnt->vc12Perf[e1LinkId].bipCnts);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] lpReiCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, pPerfCnt->vc12Perf[e1LinkId].lpReiCnts);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] rxTu12PositivePointerJustificationCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, pPerfCnt->vc12Perf[e1LinkId].rxTu12Ppjc);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] rxTu12NegativePointerJustificationCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, pPerfCnt->vc12Perf[e1LinkId].rxTu12Npjc);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] txTu12PositivePointerJustificationCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, pPerfCnt->vc12Perf[e1LinkId].txTu12Ppjc);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] txTu12NegativePointerJustificationCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, pPerfCnt->vc12Perf[e1LinkId].txTu12Npjc);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] e1CrcErrCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, pPerfCnt->e1Perf[e1LinkId].e1CrcErrCnts);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] e1FrmBitErrCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, pPerfCnt->e1Perf[e1LinkId].e1FrmBitErrCnts);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] e1ReiCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, pPerfCnt->e1Perf[e1LinkId].e1ReiCnts);
        }
    }

    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stm1TotalPerfShow
* 功能描述: 显示STM-1帧的SDH性能统计计数(累加值).
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.
*           portId: STM-1端口的端口号,取值为1~8.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 读取驱动的软件表来显示统计计数.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-27   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stm1TotalPerfShow(BYTE subslotId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;         /* ARRIVE芯片的芯片编号 */
    BYTE e1LinkId = 0;
    BYTE tug3Id = 0;
    BYTE tug2Id = 0;
    BYTE tu12Id = 0;
    DRV_TDM_SDH_PERF *pPerfCnt = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_sdhTotalPerfMemGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, &pPerfCnt);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_sdhTotalPerfMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pPerfCnt);
    
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "\n[subslotId %u portId %u]'s STM-1 total performance information: \n", subslotId, portId);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "b1ErrCounters: 0x%x\n", pPerfCnt->sectionPerf.b1Cnts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "b2ErrCounters: 0x%x\n", pPerfCnt->sectionPerf.b2Cnts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "msReiCounters: 0x%x\n", pPerfCnt->sectionPerf.msReiCnts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "b3ErrCounters: 0x%x\n", pPerfCnt->vc4Perf.b3Cnts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "hpReiCounters: 0x%x\n", pPerfCnt->vc4Perf.hpReiCnts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxAu4PositivePointerJustificationCounters: 0x%x\n", pPerfCnt->vc4Perf.rxAu4Ppjc);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxAu4NegativePointerJustificationCounters: 0x%x\n", pPerfCnt->vc4Perf.rxAu4Npjc);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txAu4PositivePointerJustificationCounters: 0x%x\n", pPerfCnt->vc4Perf.txAu4Ppjc);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txAu4NegativePointerJustificationCounters: 0x%x\n", pPerfCnt->vc4Perf.txAu4Npjc);
    
    for (e1LinkId = DRV_TDM_E1_LINK_ID_START; e1LinkId <= DRV_TDM_E1_LINK_ID_END; e1LinkId++)
    {
        rv = drv_tdm_tug3Tug2Tu12IdGet(e1LinkId, &tug3Id, &tug2Id, &tu12Id);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_tug3Tug2Tu12IdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            continue;
        }
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] V5ErrCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, pPerfCnt->vc12Perf[e1LinkId].bipCnts);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] lpReiCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, pPerfCnt->vc12Perf[e1LinkId].lpReiCnts);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] rxTu12PositivePointerJustificationCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, pPerfCnt->vc12Perf[e1LinkId].rxTu12Ppjc);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] rxTu12NegativePointerJustificationCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, pPerfCnt->vc12Perf[e1LinkId].rxTu12Npjc);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] txTu12PositivePointerJustificationCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, pPerfCnt->vc12Perf[e1LinkId].txTu12Ppjc);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] txTu12NegativePointerJustificationCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, pPerfCnt->vc12Perf[e1LinkId].txTu12Npjc);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] e1CrcErrCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, pPerfCnt->e1Perf[e1LinkId].e1CrcErrCnts);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] e1FrmBitErrCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, pPerfCnt->e1Perf[e1LinkId].e1FrmBitErrCnts);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] e1ReiCounters: 0x%x\n", tug3Id, tug2Id, tu12Id, pPerfCnt->e1Perf[e1LinkId].e1ReiCnts);
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stm4TotalPerfShow
* 功能描述: 显示STM-4帧的SDH性能统计计数(累加值).
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.
*           portId: STM-4端口的端口号,取值为1或者5.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 读取驱动的软件表来显示统计计数.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-27   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stm4TotalPerfShow(BYTE subslotId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;         /* ARRIVE芯片的芯片编号 */
    BYTE aug1Id = 0;
    BYTE e1LinkId = 0;
    BYTE tug3Id = 0;
    BYTE tug2Id = 0;
    BYTE tu12Id = 0;
    DRV_TDM_SDH_PERF *pPerfCnt = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stm4PortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stm4PortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_sdhTotalPerfMemGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, &pPerfCnt);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_sdhTotalPerfMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pPerfCnt);
    
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "\n[subslotId %u portId %u]'s STM-4 total performance information: \n", subslotId, portId);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "b1ErrCounters: 0x%x\n", pPerfCnt->sectionPerf.b1Cnts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "b2ErrCounters: 0x%x\n", pPerfCnt->sectionPerf.b2Cnts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "msReiCounters: 0x%x\n", pPerfCnt->sectionPerf.msReiCnts);

    for (aug1Id = DRV_TDM_MIN_AUG1_ID; aug1Id <= DRV_TDM_MAX_AUG1_ID; aug1Id++)
    {
        pPerfCnt = NULL;
        rv = drv_tdm_sdhTotalPerfMemGet(chipId, portId, aug1Id, &pPerfCnt);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_sdhTotalPerfMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            continue;
        }
        DRV_TDM_CHECK_POINTER_IS_NULL(pPerfCnt);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, b3ErrCounters: 0x%x\n", aug1Id, pPerfCnt->vc4Perf.b3Cnts);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, hpReiCounters: 0x%x\n", aug1Id, pPerfCnt->vc4Perf.hpReiCnts);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, rxAu4PositivePointerJustificationCounters: 0x%x\n", aug1Id, pPerfCnt->vc4Perf.rxAu4Ppjc);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, rxAu4NegativePointerJustificationCounters: 0x%x\n", aug1Id, pPerfCnt->vc4Perf.rxAu4Npjc);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, txAu4PositivePointerJustificationCounters: 0x%x\n", aug1Id, pPerfCnt->vc4Perf.txAu4Ppjc);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, txAu4NegativePointerJustificationCounters: 0x%x\n", aug1Id, pPerfCnt->vc4Perf.txAu4Npjc);
        
        for (e1LinkId = DRV_TDM_E1_LINK_ID_START; e1LinkId <= DRV_TDM_E1_LINK_ID_END; e1LinkId++)
        {
            rv = drv_tdm_tug3Tug2Tu12IdGet(e1LinkId, &tug3Id, &tug2Id, &tu12Id);
            if (DRV_TDM_OK != rv)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_tug3Tug2Tu12IdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                continue;
            }
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] V5ErrCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, pPerfCnt->vc12Perf[e1LinkId].bipCnts);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] lpReiCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, pPerfCnt->vc12Perf[e1LinkId].lpReiCnts);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] rxTu12PositivePointerJustificationCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, pPerfCnt->vc12Perf[e1LinkId].rxTu12Ppjc);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] rxTu12NegativePointerJustificationCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, pPerfCnt->vc12Perf[e1LinkId].rxTu12Npjc);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] txTu12PositivePointerJustificationCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, pPerfCnt->vc12Perf[e1LinkId].txTu12Ppjc);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] txTu12NegativePointerJustificationCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, pPerfCnt->vc12Perf[e1LinkId].txTu12Npjc);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] e1CrcErrCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, pPerfCnt->e1Perf[e1LinkId].e1CrcErrCnts);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] e1FrmBitErrCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, pPerfCnt->e1Perf[e1LinkId].e1FrmBitErrCnts);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] e1ReiCounters: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, pPerfCnt->e1Perf[e1LinkId].e1ReiCnts);
        }
    }

    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stm1BerShow
* 功能描述: 显示STM-1的BER信息.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.
*           portId: STM-1端口的端口号,取值为1~8.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 包括显示SD/SF门限值及当前的BER速率.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-27   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stm1BerShow(BYTE subslotId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;         /* ARRIVE芯片的芯片编号 */
    BYTE e1LinkId = 0;
    BYTE tug3Id = 0;
    BYTE tug2Id = 0;
    BYTE tu12Id = 0;
    WORD32 sdThreshold = 0;  /* SD门限值 */
    WORD32 sfThreshold = 0;  /* SF门限值 */
    WORD32 berRate = 0;      /* BER rate */
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "\n[subslotId %u portId %u]'s STM-1 BER information:\n", subslotId, portId);
    
    sdThreshold = 0;
    sfThreshold = 0;
    berRate = 0;
    rv = drv_tdm_rsSdSfThresholdGet(chipId, portId, DRV_TDM_SD, &sdThreshold);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rsSdSfThresholdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_rsSdSfThresholdGet(chipId, portId, DRV_TDM_SF, &sfThreshold);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rsSdSfThresholdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_rsCurrentBerRateGet(chipId, portId, &berRate);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rsCurrentBerRateGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if ((sdThreshold < DRV_TDM_BER_RATE_MAX_SIZE) 
        && (sfThreshold < DRV_TDM_BER_RATE_MAX_SIZE) 
        && (berRate < DRV_TDM_BER_RATE_MAX_SIZE))
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "RS-SD threshold: %s\n", g_drv_tdm_berRateStr[sdThreshold]);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "RS-SF threshold: %s\n", g_drv_tdm_berRateStr[sfThreshold]);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "RS current ber rate: %s\n", g_drv_tdm_berRateStr[berRate]);
    }
    
    sdThreshold = 0;
    sfThreshold = 0;
    berRate = 0;
    rv = drv_tdm_msSdSfThresholdGet(chipId, portId, DRV_TDM_SD, &sdThreshold);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_msSdSfThresholdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_msSdSfThresholdGet(chipId, portId, DRV_TDM_SF, &sfThreshold);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_msSdSfThresholdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_msCurrentBerRateGet(chipId, portId, &berRate);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_msCurrentBerRateGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if ((sdThreshold < DRV_TDM_BER_RATE_MAX_SIZE) 
        && (sfThreshold < DRV_TDM_BER_RATE_MAX_SIZE) 
        && (berRate < DRV_TDM_BER_RATE_MAX_SIZE))
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "MS-SD threshold: %s\n", g_drv_tdm_berRateStr[sdThreshold]);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "MS-SF threshold: %s\n", g_drv_tdm_berRateStr[sfThreshold]);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "MS current ber rate: %s\n", g_drv_tdm_berRateStr[berRate]);
    }

    sdThreshold = 0;
    sfThreshold = 0;
    berRate = 0;
    rv = drv_tdm_vc4SdSfThresholdGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, DRV_TDM_SD, &sdThreshold);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_vc4SdSfThresholdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_vc4SdSfThresholdGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, DRV_TDM_SF, &sfThreshold);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_vc4SdSfThresholdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_vc4CurrentBerRateGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, &berRate);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_vc4CurrentBerRateGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if ((sdThreshold < DRV_TDM_BER_RATE_MAX_SIZE) 
        && (sfThreshold < DRV_TDM_BER_RATE_MAX_SIZE) 
        && (berRate < DRV_TDM_BER_RATE_MAX_SIZE))
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "HP-SD threshold: %s\n", g_drv_tdm_berRateStr[sdThreshold]);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "HP-SF threshold: %s\n", g_drv_tdm_berRateStr[sfThreshold]);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "HP current ber rate: %s\n", g_drv_tdm_berRateStr[berRate]);
    }

    for (e1LinkId = DRV_TDM_E1_LINK_ID_START; e1LinkId <= DRV_TDM_E1_LINK_ID_END; e1LinkId++)
    {
        rv = drv_tdm_tug3Tug2Tu12IdGet(e1LinkId, &tug3Id, &tug2Id, &tu12Id);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_tug3Tug2Tu12IdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            continue;
        }
        
        sdThreshold = 0;
        sfThreshold = 0;
        berRate = 0;
        rv = drv_tdm_vc12SdSfThresholdGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, DRV_TDM_SD, &sdThreshold);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, drv_tdm_vc12SdSfThresholdGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
            continue;
        }
        rv = drv_tdm_vc12SdSfThresholdGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, DRV_TDM_SF, &sfThreshold);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, drv_tdm_vc12SdSfThresholdGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
            continue;
        }
        rv = drv_tdm_vc12CurrentBerRateGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, &berRate);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, drv_tdm_vc12CurrentBerRateGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
            continue;
        }
        if ((sdThreshold < DRV_TDM_BER_RATE_MAX_SIZE) 
            && (sfThreshold < DRV_TDM_BER_RATE_MAX_SIZE) 
            && (berRate < DRV_TDM_BER_RATE_MAX_SIZE))
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] LP-SD threshold: %s\n", tug3Id, tug2Id, tu12Id, g_drv_tdm_berRateStr[sdThreshold]);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] LP-SF threshold: %s\n", tug3Id, tug2Id, tu12Id, g_drv_tdm_berRateStr[sfThreshold]);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "E1[%u-%u-%u] LP current ber rate: %s\n", tug3Id, tug2Id, tu12Id, g_drv_tdm_berRateStr[berRate]);
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stm4BerShow
* 功能描述: 显示STM-4的BER信息.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.
*           portId: STM-4端口的端口号,取值为1或者5.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 包括显示SD/SF门限值及当前的BER速率.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-27   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stm4BerShow(BYTE subslotId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;         /* ARRIVE芯片的芯片编号 */
    BYTE aug1Id = 0;   
    BYTE e1LinkId = 0;
    BYTE tug3Id = 0;
    BYTE tug2Id = 0;
    BYTE tu12Id = 0;
    WORD32 sdThreshold = 0;  /* SD门限值 */
    WORD32 sfThreshold = 0;  /* SF门限值 */
    WORD32 berRate = 0;      /* BER rate */
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stm4PortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stm4PortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "\n[subslotId %u portId %u]'s STM-4 BER information:\n", subslotId, portId);
    
    sdThreshold = 0;
    sfThreshold = 0;
    berRate = 0;
    rv = drv_tdm_rsSdSfThresholdGet(chipId, portId, DRV_TDM_SD, &sdThreshold);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rsSdSfThresholdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_rsSdSfThresholdGet(chipId, portId, DRV_TDM_SF, &sfThreshold);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rsSdSfThresholdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_rsCurrentBerRateGet(chipId, portId, &berRate);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rsCurrentBerRateGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if ((sdThreshold < DRV_TDM_BER_RATE_MAX_SIZE) 
        && (sfThreshold < DRV_TDM_BER_RATE_MAX_SIZE) 
        && (berRate < DRV_TDM_BER_RATE_MAX_SIZE))
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "RS-SD threshold: %s\n", g_drv_tdm_berRateStr[sdThreshold]);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "RS-SF threshold: %s\n", g_drv_tdm_berRateStr[sfThreshold]);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "RS current ber rate: %s\n", g_drv_tdm_berRateStr[berRate]);
    }
    
    sdThreshold = 0;
    sfThreshold = 0;
    berRate = 0;
    rv = drv_tdm_msSdSfThresholdGet(chipId, portId, DRV_TDM_SD, &sdThreshold);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_msSdSfThresholdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_msSdSfThresholdGet(chipId, portId, DRV_TDM_SF, &sfThreshold);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_msSdSfThresholdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_msCurrentBerRateGet(chipId, portId, &berRate);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_msCurrentBerRateGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if ((sdThreshold < DRV_TDM_BER_RATE_MAX_SIZE) 
        && (sfThreshold < DRV_TDM_BER_RATE_MAX_SIZE) 
        && (berRate < DRV_TDM_BER_RATE_MAX_SIZE))
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "MS-SD threshold: %s\n", g_drv_tdm_berRateStr[sdThreshold]);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "MS-SF threshold: %s\n", g_drv_tdm_berRateStr[sfThreshold]);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "MS current ber rate: %s\n", g_drv_tdm_berRateStr[berRate]);
    }

    for (aug1Id = DRV_TDM_MIN_AUG1_ID; aug1Id <= DRV_TDM_MAX_AUG1_ID; aug1Id++)
    {
        sdThreshold = 0;
        sfThreshold = 0;
        berRate = 0;
        rv = drv_tdm_vc4SdSfThresholdGet(chipId, portId, aug1Id, DRV_TDM_SD, &sdThreshold);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_vc4SdSfThresholdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        rv = drv_tdm_vc4SdSfThresholdGet(chipId, portId, aug1Id, DRV_TDM_SF, &sfThreshold);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_vc4SdSfThresholdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        rv = drv_tdm_vc4CurrentBerRateGet(chipId, portId, aug1Id, &berRate);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_vc4CurrentBerRateGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        if ((sdThreshold < DRV_TDM_BER_RATE_MAX_SIZE) 
            && (sfThreshold < DRV_TDM_BER_RATE_MAX_SIZE) 
            && (berRate < DRV_TDM_BER_RATE_MAX_SIZE))
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, HP-SD threshold: %s\n", aug1Id, g_drv_tdm_berRateStr[sdThreshold]);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, HP-SF threshold: %s\n", aug1Id, g_drv_tdm_berRateStr[sfThreshold]);
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, HP current ber rate: %s\n", aug1Id, g_drv_tdm_berRateStr[berRate]);
        }
        
        for (e1LinkId = DRV_TDM_E1_LINK_ID_START; e1LinkId <= DRV_TDM_E1_LINK_ID_END; e1LinkId++)
        {
            rv = drv_tdm_tug3Tug2Tu12IdGet(e1LinkId, &tug3Id, &tug2Id, &tu12Id);
            if (DRV_TDM_OK != rv)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_tug3Tug2Tu12IdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                continue;
            }
            
            sdThreshold = 0;
            sfThreshold = 0;
            berRate = 0;
            rv = drv_tdm_vc12SdSfThresholdGet(chipId, portId, aug1Id, e1LinkId, DRV_TDM_SD, &sdThreshold);
            if (DRV_TDM_OK != rv)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, drv_tdm_vc12SdSfThresholdGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
                continue;
            }
            rv = drv_tdm_vc12SdSfThresholdGet(chipId, portId, aug1Id, e1LinkId, DRV_TDM_SF, &sfThreshold);
            if (DRV_TDM_OK != rv)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, drv_tdm_vc12SdSfThresholdGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
                continue;
            }
            rv = drv_tdm_vc12CurrentBerRateGet(chipId, portId, aug1Id, e1LinkId, &berRate);
            if (DRV_TDM_OK != rv)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, drv_tdm_vc12CurrentBerRateGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
                continue;
            }
            if ((sdThreshold < DRV_TDM_BER_RATE_MAX_SIZE) 
                && (sfThreshold < DRV_TDM_BER_RATE_MAX_SIZE) 
                && (berRate < DRV_TDM_BER_RATE_MAX_SIZE))
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] LP-SD threshold: %s\n", aug1Id, tug3Id, tug2Id, tu12Id, g_drv_tdm_berRateStr[sdThreshold]);
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] LP-SF threshold: %s\n", aug1Id, tug3Id, tug2Id, tu12Id, g_drv_tdm_berRateStr[sfThreshold]);
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, E1[%u-%u-%u] LP current ber rate: %s\n", aug1Id, tug3Id, tug2Id, tu12Id, g_drv_tdm_berRateStr[berRate]);
            }
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stm1OhShow
* 功能描述: 显示STM-1帧的SDH开销字节.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.
*           portId: STM-1端口的端口号,取值为1~8.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 直接读取芯片来显示开销字节.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-01   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stm1OhShow(BYTE subslotId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;         /* ARRIVE芯片的芯片编号 */
    BYTE e1LinkId = 0;
    BYTE tug3Id = 0;
    BYTE tug2Id = 0;
    BYTE tu12Id = 0;
    BYTE txJ0MsgBuf[DRV_TDM_TRACE_MSG_MAX_LEN] = {0};
    BYTE rxJ0MsgBuf[DRV_TDM_TRACE_MSG_MAX_LEN] = {0};
    BYTE expectedRxJ0MsgBuf[DRV_TDM_TRACE_MSG_MAX_LEN] = {0};
    BYTE txK1Value = 0;
    BYTE rxK1Value = 0;
    BYTE txK2Value = 0;
    BYTE rxK2Value = 0;
    BYTE txS1Value = 0;
    BYTE rxS1Value = 0;
    BYTE txM1Value = 0;
    BYTE rxM1Value = 0;
    BYTE txJ1MsgBuf[DRV_TDM_TRACE_MSG_MAX_LEN] = {0};
    BYTE rxJ1MsgBuf[DRV_TDM_TRACE_MSG_MAX_LEN] = {0};
    BYTE expectedRxJ1MsgBuf[DRV_TDM_TRACE_MSG_MAX_LEN] = {0};
    BYTE txC2Value = 0;
    BYTE rxC2Value = 0;
    BYTE expectedRxC2Value = 0;
    BYTE txG1Value = 0;
    BYTE rxG1Value = 0;
    BYTE txH4Value = 0;
    BYTE rxH4Value = 0;
    BYTE txK3Value = 0;
    BYTE rxK3Value = 0;
    BYTE txN1Value = 0;
    BYTE rxN1Value = 0;
    BYTE txJ2MsgBuf[DRV_TDM_TRACE_MSG_MAX_LEN] = {0};
    BYTE rxJ2MsgBuf[DRV_TDM_TRACE_MSG_MAX_LEN] = {0};
    BYTE expectedRxJ2MsgBuf[DRV_TDM_TRACE_MSG_MAX_LEN] = {0};
    BYTE txLpPslValue = 0;
    BYTE rxLpPslValue = 0;
    BYTE expectedRxLpPslValue = 0;
    BYTE txV5Value = 0;
    BYTE rxV5Value = 0;
    BYTE txN2Value = 0;
    BYTE rxN2Value = 0;
    BYTE txK4Value = 0;
    BYTE rxK4Value = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_txJ0Get(chipId, portId, txJ0MsgBuf);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_txJ0Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_rxJ0Get(chipId, portId, rxJ0MsgBuf);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rxJ0Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_expectedRxJ0Get(chipId, portId, expectedRxJ0MsgBuf);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_expectedRxJ0Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_txK1Get(chipId, portId, &txK1Value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_txK1Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_rxK1Get(chipId, portId, &rxK1Value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rxK1Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_txK2Get(chipId, portId, &txK2Value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_txK2Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_rxK2Get(chipId, portId, &rxK2Value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rxK2Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_txS1Get(chipId, portId, &txS1Value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_txS1Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_rxS1Get(chipId, portId, &rxS1Value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rxS1Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_txM1Get(chipId, portId, &txM1Value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_txM1Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_rxM1Get(chipId, portId, &rxM1Value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rxM1Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_txJ1Get(chipId, portId, DRV_TDM_STM1_AUG1_ID, txJ1MsgBuf);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_txJ1Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_rxJ1Get(chipId, portId, DRV_TDM_STM1_AUG1_ID, rxJ1MsgBuf);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rxJ1Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_expectedRxJ1Get(chipId, portId, DRV_TDM_STM1_AUG1_ID, expectedRxJ1MsgBuf);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_expectedRxJ1Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_txC2Get(chipId, portId, DRV_TDM_STM1_AUG1_ID, &txC2Value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_txC2Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_rxC2Get(chipId, portId, DRV_TDM_STM1_AUG1_ID, &rxC2Value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rxC2Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_expectedRxC2Get(chipId, portId, DRV_TDM_STM1_AUG1_ID, &expectedRxC2Value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_expectedRxC2Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_txG1Get(chipId, portId, DRV_TDM_STM1_AUG1_ID, &txG1Value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_txG1Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_rxG1Get(chipId, portId, DRV_TDM_STM1_AUG1_ID, &rxG1Value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rxG1Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_txH4Get(chipId, portId, DRV_TDM_STM1_AUG1_ID, &txH4Value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_txH4Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_rxH4Get(chipId, portId, DRV_TDM_STM1_AUG1_ID, &rxH4Value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rxH4Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_txK3Get(chipId, portId, DRV_TDM_STM1_AUG1_ID, &txK3Value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_txK3Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_rxK3Get(chipId, portId, DRV_TDM_STM1_AUG1_ID, &rxK3Value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rxK3Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_txN1Get(chipId, portId, DRV_TDM_STM1_AUG1_ID, &txN1Value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_txN1Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_rxN1Get(chipId, portId, DRV_TDM_STM1_AUG1_ID, &rxN1Value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rxN1Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "[subslotId %u portId %u]'s SDH overhead information: \n", subslotId, portId);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "tx J0: %s, crc: 0x%x\n", &(txJ0MsgBuf[1]), txJ0MsgBuf[0]);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rx J0: %s, crc: 0x%x\n", &(rxJ0MsgBuf[1]), rxJ0MsgBuf[0]);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "expected rx J0: %s, crc: 0x%x\n", &(expectedRxJ0MsgBuf[1]), expectedRxJ0MsgBuf[0]);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "tx K1: 0x%x\n", txK1Value);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rx K1: 0x%x\n", rxK1Value);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "tx K2: 0x%x\n", txK2Value);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rx K2: 0x%x\n", rxK2Value);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "tx S1: 0x%x\n", txS1Value);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rx S1: 0x%x\n", rxS1Value);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "tx M1: 0x%x\n", txM1Value);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rx M1: 0x%x\n", rxM1Value);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "tx J1: %s, crc: 0x%x\n", &(txJ1MsgBuf[1]), txJ1MsgBuf[0]);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rx J1: %s, crc: 0x%x\n", &(rxJ1MsgBuf[1]), rxJ1MsgBuf[0]);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "expected rx J1: %s, crc: 0x%x\n", &(expectedRxJ1MsgBuf[1]), expectedRxJ1MsgBuf[0]);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "tx C2: 0x%x\n", txC2Value);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rx C2: 0x%x\n", rxC2Value);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "expected rx C2: 0x%x\n", expectedRxC2Value);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "tx G1: 0x%x\n", txG1Value);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rx G1: 0x%x\n", rxG1Value);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "tx H4: 0x%x\n", txH4Value);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rx H4: 0x%x\n", rxH4Value);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "tx K3: 0x%x\n", txK3Value);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rx K3: 0x%x\n", rxK3Value);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "tx N1: 0x%x\n", txN1Value);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rx N1: 0x%x\n", rxN1Value);
    for (e1LinkId = DRV_TDM_E1_LINK_ID_START; e1LinkId <= DRV_TDM_E1_LINK_ID_END; e1LinkId++)
    {
        rv = drv_tdm_tug3Tug2Tu12IdGet(e1LinkId, &tug3Id, &tug2Id, &tu12Id);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_tug3Tug2Tu12IdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }

        rv = drv_tdm_txJ2Get(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, txJ2MsgBuf);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_txJ2Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }

        rv = drv_tdm_rxJ2Get(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, rxJ2MsgBuf);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rxJ2Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }

        rv = drv_tdm_expectedRxJ2Get(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, expectedRxJ2MsgBuf);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_expectedRxJ2Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }

        rv = drv_tdm_lpTxPslGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, &txLpPslValue);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_lpTxPslGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }

        rv = drv_tdm_lpRxPslGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, &rxLpPslValue);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_lpRxPslGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }

        rv = drv_tdm_lpExpectedRxPslGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, &expectedRxLpPslValue);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_lpExpectedRxPslGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }

        rv = drv_tdm_txV5Get(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, &txV5Value);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_txV5Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        
        rv = drv_tdm_rxV5Get(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, &rxV5Value);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rxV5Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }

        rv = drv_tdm_txN2Get(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, &txN2Value);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_txN2Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        
        rv = drv_tdm_rxN2Get(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, &rxN2Value);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rxN2Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }

        rv = drv_tdm_txK4Get(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, &txK4Value);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_txK4Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        rv = drv_tdm_rxK4Get(chipId, portId, DRV_TDM_STM1_AUG1_ID, e1LinkId, &rxK4Value);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rxK4Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }

        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VC12[%u-%u-%u] tx J2: %s, crc: 0x%x\n", tug3Id, tug2Id, tu12Id, &(txJ2MsgBuf[1]), txJ2MsgBuf[0]);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VC12[%u-%u-%u] rx J2: %s, crc: 0x%x\n", tug3Id, tug2Id, tu12Id, &(rxJ2MsgBuf[1]), rxJ2MsgBuf[0]);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VC12[%u-%u-%u] expected rx J2: %s, crc: 0x%x\n", tug3Id, tug2Id, tu12Id, &(expectedRxJ2MsgBuf[1]), expectedRxJ2MsgBuf[0]);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VC12[%u-%u-%u] tx lpPsl: 0x%x\n", tug3Id, tug2Id, tu12Id, txLpPslValue);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VC12[%u-%u-%u] rx lpPsl: 0x%x\n", tug3Id, tug2Id, tu12Id, rxLpPslValue);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VC12[%u-%u-%u] expected rx lpPsl: 0x%x\n", tug3Id, tug2Id, tu12Id, expectedRxLpPslValue);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VC12[%u-%u-%u] tx V5: 0x%x\n", tug3Id, tug2Id, tu12Id, txV5Value);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VC12[%u-%u-%u] rx V5: 0x%x\n", tug3Id, tug2Id, tu12Id, rxV5Value);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VC12[%u-%u-%u] tx N2: 0x%x\n", tug3Id, tug2Id, tu12Id, txN2Value);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VC12[%u-%u-%u] rx N2: 0x%x\n", tug3Id, tug2Id, tu12Id, rxN2Value);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VC12[%u-%u-%u] tx K4: 0x%x\n", tug3Id, tug2Id, tu12Id, txK4Value);
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VC12[%u-%u-%u] rx K4: 0x%x\n", tug3Id, tug2Id, tu12Id, rxK4Value);
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stm4OhShow
* 功能描述: 显示STM-4帧的SDH开销字节.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.
*           portId: STM-4端口的端口号,取值为1或者5.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 直接读取芯片来显示开销字节.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-01-06   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stm4OhShow(BYTE subslotId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;         /* ARRIVE芯片的芯片编号 */
    BYTE aug1Id = 0;
    BYTE e1LinkId = 0;
    BYTE tug3Id = 0;
    BYTE tug2Id = 0;
    BYTE tu12Id = 0;
    BYTE msgBuf[DRV_TDM_TRACE_MSG_MAX_LEN] = {0};
    BYTE value = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stm4PortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stm4PortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "[subslotId %u portId %u]'s SDH overhead information: \n", subslotId, portId);
    rv = drv_tdm_txJ0Get(chipId, portId, msgBuf);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_txJ0Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "tx J0: %s, crc: 0x%x\n", &(msgBuf[1]), msgBuf[0]);
    
    rv = drv_tdm_rxJ0Get(chipId, portId, msgBuf);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rxJ0Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rx J0: %s, crc: 0x%x\n", &(msgBuf[1]), msgBuf[0]);

    rv = drv_tdm_expectedRxJ0Get(chipId, portId, msgBuf);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_expectedRxJ0Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "expected rx J0: %s, crc: 0x%x\n", &(msgBuf[1]), msgBuf[0]);

    rv = drv_tdm_txK1Get(chipId, portId, &value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_txK1Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "tx K1: 0x%x\n", value);

    rv = drv_tdm_rxK1Get(chipId, portId, &value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rxK1Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rx K1: 0x%x\n", value);

    rv = drv_tdm_txK2Get(chipId, portId, &value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_txK2Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "tx K2: 0x%x\n", value);

    rv = drv_tdm_rxK2Get(chipId, portId, &value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rxK2Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rx K2: 0x%x\n", value);

    rv = drv_tdm_txS1Get(chipId, portId, &value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_txS1Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "tx S1: 0x%x\n", value);

    rv = drv_tdm_rxS1Get(chipId, portId, &value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rxS1Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rx S1: 0x%x\n", value);

    for (aug1Id = DRV_TDM_MIN_AUG1_ID; aug1Id <= DRV_TDM_MAX_AUG1_ID; aug1Id++)
    {
        rv = drv_tdm_txJ1Get(chipId, portId, aug1Id, msgBuf);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_txJ1Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, tx J1: %s, crc: 0x%x\n", aug1Id, &(msgBuf[1]), msgBuf[0]);
        
        rv = drv_tdm_rxJ1Get(chipId, portId, aug1Id, msgBuf);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rxJ1Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, rx J1: %s, crc: 0x%x\n", aug1Id, &(msgBuf[1]), msgBuf[0]);
        
        rv = drv_tdm_expectedRxJ1Get(chipId, portId, aug1Id, msgBuf);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_expectedRxJ1Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, expected rx J1: %s, crc: 0x%x\n", aug1Id, &(msgBuf[1]), msgBuf[0]);
        
        rv = drv_tdm_txC2Get(chipId, portId, aug1Id, &value);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_txC2Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, tx C2: 0x%x\n", aug1Id, value);
        
        rv = drv_tdm_rxC2Get(chipId, portId, aug1Id, &value);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rxC2Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, rx C2: 0x%x\n", aug1Id, value);
        
        rv = drv_tdm_expectedRxC2Get(chipId, portId, aug1Id, &value);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_expectedRxC2Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, expected rx C2: 0x%x\n", aug1Id, value);

        for (e1LinkId = DRV_TDM_E1_LINK_ID_START; e1LinkId <= DRV_TDM_E1_LINK_ID_END; e1LinkId++)
        {
            rv = drv_tdm_tug3Tug2Tu12IdGet(e1LinkId, &tug3Id, &tug2Id, &tu12Id);
            if (DRV_TDM_OK != rv)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_tug3Tug2Tu12IdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                return rv;
            }
            
            rv = drv_tdm_txJ2Get(chipId, portId, aug1Id, e1LinkId, msgBuf);
            if (DRV_TDM_OK != rv)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_txJ2Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
                return rv;
            }
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, VC12[%u-%u-%u] tx J2: %s, crc: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, &(msgBuf[1]), msgBuf[0]);
            
            rv = drv_tdm_rxJ2Get(chipId, portId, aug1Id, e1LinkId, msgBuf);
            if (DRV_TDM_OK != rv)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_rxJ2Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
                return rv;
            }
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, VC12[%u-%u-%u] rx J2: %s, crc: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, &(msgBuf[1]), msgBuf[0]);
            
            rv = drv_tdm_expectedRxJ2Get(chipId, portId, aug1Id, e1LinkId, msgBuf);
            if (DRV_TDM_OK != rv)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_expectedRxJ2Get() rv=0x%x.\n", __FILE__, __LINE__, rv);
                return rv;
            }
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, VC12[%u-%u-%u] expected rx J2: %s, crc: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, &(msgBuf[1]), msgBuf[0]);
            
            rv = drv_tdm_lpTxPslGet(chipId, portId, aug1Id, e1LinkId, &value);
            if (DRV_TDM_OK != rv)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_lpTxPslGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                return rv;
            }
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, VC12[%u-%u-%u] tx lpPsl: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, value);
            
            rv = drv_tdm_lpRxPslGet(chipId, portId, aug1Id, e1LinkId, &value);
            if (DRV_TDM_OK != rv)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_lpRxPslGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                return rv;
            }
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, VC12[%u-%u-%u] rx lpPsl: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, value);
            
            rv = drv_tdm_lpExpectedRxPslGet(chipId, portId, aug1Id, e1LinkId, &value);
            if (DRV_TDM_OK != rv)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_lpExpectedRxPslGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
                return rv;
            }
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "aug1Id %u, VC12[%u-%u-%u] expected rx lpPsl: 0x%x\n", aug1Id, tug3Id, tug2Id, tu12Id, value);
        }
    }

    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atPwPktCntShow
* 功能描述: 显示PW报文统计计数
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号.
*           pwId: 驱动的PW ID,取值为0~1023.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 直接读取芯片来获取PW报文统计计数
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-27   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atPwPktCntShow(BYTE subslotId, WORD16 pwId)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;
    BYTE pwState = 0;
    DRV_TDM_PW_CNT_INFO pwCnt;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    DRV_TDM_CHECK_PW_ID(pwId);
    chipId = subslotId - (BYTE)1;
    
    rv = drv_tdm_pwStateGet(chipId, pwId, &pwState);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_pwStateGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_PW_FREE == pwState)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, chipId %u pwId %u, PW is free.\n", __FILE__, __LINE__, chipId, pwId);
        return DRV_TDM_INVALID_PW_STATE;
    }
    
    /* Get pw counters */
    memset(&pwCnt, 0, sizeof(DRV_TDM_PW_CNT_INFO));
    rv = drv_tdm_pwCntGet(chipId, pwId, &pwCnt);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_pwCntGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "\n[subslotId %u pwId %u]'s PW counters:\n", subslotId, pwId);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txPackets: 0x%x\n", pwCnt.txPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txPayloadBytes: 0x%x\n", pwCnt.txPayloadBytes);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxPackets: 0x%x\n", pwCnt.rxPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxPayloadBytes: 0x%x\n", pwCnt.rxPayloadBytes);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxDiscardedPackets: 0x%x\n", pwCnt.rxDiscardedPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxMalformedPackets: 0x%x\n", pwCnt.rxMalformedPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxReorderedPackets: 0x%x\n", pwCnt.rxReorderedPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxLostPackets: 0x%x\n", pwCnt.rxLostPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxOutofSeqenceDropPackets: 0x%x\n", pwCnt.rxOutofSeqDropPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxOamPackets: 0x%x\n", pwCnt.rxOamPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txLbitPackets: 0x%x\n", pwCnt.txLbitPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txRbitPackets: 0x%x\n", pwCnt.txRbitPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxLbitPackets: 0x%x\n", pwCnt.rxLbitPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxRbitPackets: 0x%x\n", pwCnt.rxRbitPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxJitterBufferOverrunPackets: 0x%x\n", pwCnt.rxJitBufOverrunPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxJitterBufferUnderrunPackets: 0x%x\n", pwCnt.rxJitBufUnderrunPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxLops: 0x%x\n", pwCnt.rxLops);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxPacketsSentToTdm: 0x%x\n", pwCnt.rxPktsSentToTdm);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txMbitPackets: 0x%x\n", pwCnt.txMbitPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxMbitPackets: 0x%x\n", pwCnt.rxMbitPkts);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atAcPwPktCntShow
* 功能描述: 显示PW报文统计计数
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
*           timeslotBmp: E1链路的时隙位图,取值为0x0~0xffffffff.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 直接读取芯片来获取PW报文统计计数
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-27   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atAcPwPktCntShow(BYTE subslotId, 
                                             BYTE portId, 
                                             BYTE aug1Id, 
                                             BYTE e1LinkId, 
                                             WORD32 timeslotBmp)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;
    WORD16 pwId = 0;
    DRV_TDM_PW_CNT_INFO pwCnt;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId); 
    chipId = subslotId - (BYTE)1;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_acBindingPwIdGet(subslotId, portId, aug1Id, e1LinkId, timeslotBmp, &pwId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_acBindingPwIdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_PW_ID(pwId);
    
    /* Get pw counters */
    memset(&pwCnt, 0, sizeof(DRV_TDM_PW_CNT_INFO));
    rv = drv_tdm_pwCntGet(chipId, pwId, &pwCnt);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_pwCntGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "\n[subslotId %u pwId %u]'s PW counters:\n", subslotId, pwId);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txPackets: 0x%x\n", pwCnt.txPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txPayloadBytes: 0x%x\n", pwCnt.txPayloadBytes);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxPackets: 0x%x\n", pwCnt.rxPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxPayloadBytes: 0x%x\n", pwCnt.rxPayloadBytes);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxDiscardedPackets: 0x%x\n", pwCnt.rxDiscardedPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxMalformedPackets: 0x%x\n", pwCnt.rxMalformedPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxReorderedPackets: 0x%x\n", pwCnt.rxReorderedPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxLostPackets: 0x%x\n", pwCnt.rxLostPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxOutofSeqenceDropPackets: 0x%x\n", pwCnt.rxOutofSeqDropPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxOamPackets: 0x%x\n", pwCnt.rxOamPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txLbitPackets: 0x%x\n", pwCnt.txLbitPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txRbitPackets: 0x%x\n", pwCnt.txRbitPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxLbitPackets: 0x%x\n", pwCnt.rxLbitPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxRbitPackets: 0x%x\n", pwCnt.rxRbitPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxJitterBufferOverrunPackets: 0x%x\n", pwCnt.rxJitBufOverrunPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxJitterBufferUnderrunPackets: 0x%x\n", pwCnt.rxJitBufUnderrunPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxLops: 0x%x\n", pwCnt.rxLops);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxPacketsSentToTdm: 0x%x\n", pwCnt.rxPktsSentToTdm);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txMbitPackets: 0x%x\n", pwCnt.txMbitPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxMbitPackets: 0x%x\n", pwCnt.rxMbitPkts);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwPktCntShow
* 功能描述: 显示PW报文统计计数
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM-1单板的子槽位号,取值为1~4.
*           pwId: 驱动的PW ID,取值为0~1023.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 读取驱动的软件表g_drv_tdm_pw_pkt_cnt来获取PW报文统计计数
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-27   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwPktCntShow(BYTE subslotId, WORD16 pwId)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;
    BYTE pwState = 0;
    DRV_TDM_PW_CNT_INFO *pPwCnt = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    DRV_TDM_CHECK_PW_ID(pwId);
    chipId = subslotId - (BYTE)1;
    
    rv = drv_tdm_pwStateGet(chipId, pwId, &pwState);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_pwStateGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_PW_FREE == pwState)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, chipId %u pwId %u, PW is free.\n", __FILE__, __LINE__, chipId, pwId);
        return DRV_TDM_INVALID_PW_STATE;
    }
    
    rv = drv_tdm_pwPktCntMemGet(chipId, pwId, &pPwCnt);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_pwPktCntMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCnt);
    
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "\n[subslotId %u pwId %u]'s PW counters:\n", subslotId, pwId);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txPackets: 0x%x\n", pPwCnt->txPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txPayloadBytes: 0x%x\n", pPwCnt->txPayloadBytes);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxPackets: 0x%x\n", pPwCnt->rxPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxPayloadBytes: 0x%x\n", pPwCnt->rxPayloadBytes);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxDiscardedPackets: 0x%x\n", pPwCnt->rxDiscardedPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxMalformedPackets: 0x%x\n", pPwCnt->rxMalformedPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxReorderedPackets: 0x%x\n", pPwCnt->rxReorderedPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxLostPackets: 0x%x\n", pPwCnt->rxLostPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxOutofSeqenceDropPackets: 0x%x\n", pPwCnt->rxOutofSeqDropPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxOamPackets: 0x%x\n", pPwCnt->rxOamPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txLbitPackets: 0x%x\n", pPwCnt->txLbitPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txRbitPackets: 0x%x\n", pPwCnt->txRbitPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxLbitPackets: 0x%x\n", pPwCnt->rxLbitPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxRbitPackets: 0x%x\n", pPwCnt->rxRbitPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxJitterBufferOverrunPackets: 0x%x\n", pPwCnt->rxJitBufOverrunPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxJitterBufferUnderrunPackets: 0x%x\n", pPwCnt->rxJitBufUnderrunPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxLops: 0x%x\n", pPwCnt->rxLops);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxPacketsSentToTdm: 0x%x\n", pPwCnt->rxPktsSentToTdm);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txMbitPackets: 0x%x\n", pPwCnt->txMbitPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxMbitPackets: 0x%x\n", pPwCnt->rxMbitPkts);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwTotalPktCntShow
* 功能描述: 显示PW的累积报文统计计数
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM-1单板的子槽位号,取值为1~4.
*           pwId: 驱动的PW ID,取值为0~1023.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 读取驱动的软件表g_drv_tdm_pw_total_pkt_cnt来获取PW报文统计计数
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-27   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwTotalPktCntShow(BYTE subslotId, WORD16 pwId)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;
    BYTE pwState = 0;
    DRV_TDM_PW_CNT_INFO *pPwCnt = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    DRV_TDM_CHECK_PW_ID(pwId);
    chipId = subslotId - (BYTE)1;
    
    rv = drv_tdm_pwStateGet(chipId, pwId, &pwState);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_pwStateGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_PW_FREE == pwState)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, chipId %u pwId %u, PW is free.\n", __FILE__, __LINE__, chipId, pwId);
        return DRV_TDM_INVALID_PW_STATE;
    }
    
    rv = drv_tdm_pwTotalPktCntMemGet(chipId, pwId, &pPwCnt);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_pwTotalPktCntMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCnt);
    
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "\n[subslotId %u pwId %u]'s PW total packet counters:\n", subslotId, pwId);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txPackets: 0x%x\n", pPwCnt->txPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txPayloadBytes: 0x%x\n", pPwCnt->txPayloadBytes);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxPackets: 0x%x\n", pPwCnt->rxPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxPayloadBytes: 0x%x\n", pPwCnt->rxPayloadBytes);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxDiscardedPackets: 0x%x\n", pPwCnt->rxDiscardedPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxMalformedPackets: 0x%x\n", pPwCnt->rxMalformedPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxReorderedPackets: 0x%x\n", pPwCnt->rxReorderedPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxLostPackets: 0x%x\n", pPwCnt->rxLostPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxOutofSeqenceDropPackets: 0x%x\n", pPwCnt->rxOutofSeqDropPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxOamPackets: 0x%x\n", pPwCnt->rxOamPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txLbitPackets: 0x%x\n", pPwCnt->txLbitPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txRbitPackets: 0x%x\n", pPwCnt->txRbitPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxLbitPackets: 0x%x\n", pPwCnt->rxLbitPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxRbitPackets: 0x%x\n", pPwCnt->rxRbitPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxJitterBufferOverrunPackets: 0x%x\n", pPwCnt->rxJitBufOverrunPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxJitterBufferUnderrunPackets: 0x%x\n", pPwCnt->rxJitBufUnderrunPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxLops: 0x%x\n", pPwCnt->rxLops);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxPacketsSentToTdm: 0x%x\n", pPwCnt->rxPktsSentToTdm);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txMbitPackets: 0x%x\n", pPwCnt->txMbitPkts);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxMbitPackets: 0x%x\n", pPwCnt->rxMbitPkts);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwCntDebugFlagShow
* 功能描述: 显示PWE3报文统计的调试开关的值.
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3.
*           pwId: PWID,取值为0~1023.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwCntDebugFlagShow(BYTE chipId, WORD16 pwId)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 flag = 0;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    
    rv = drv_tdm_pwCntDebugFlagGet(chipId, pwId, &flag);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_pwCntDebugFlagGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_pwCntDebug[%u][%u] = %u.\n", chipId, pwId, flag);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_ethIntfPktCntShow
* 功能描述: 显示Ethernet接口的报文统计计数
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM-1单板的子槽位号.
*           ethIntfIndex: Ethernet接口索引,取值为0~1.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 直接读取芯片来获取Ethernet接口的报文统计计数
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-27   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_ethIntfPktCntShow(BYTE subslotId, BYTE ethIntfIndex)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;
    AtDevice pDevice = NULL;     /* ARRIVE芯片的device指针 */
    AtModuleEth pEthModule = NULL;  /* Ethernet module */
    AtEthPort pEthPort = NULL;      /* Ethernet port */
    tAtEthPortCounters ethIntfPktCnt;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    DRV_TDM_CHECK_ETH_INTF_INDEX(ethIntfIndex);
    chipId = subslotId - (BYTE)1;
    
    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);

    /* Get Ethernet Module */
    pEthModule = (AtModuleEth)AtDeviceModuleGet(pDevice, cAtModuleEth);
    DRV_TDM_CHECK_POINTER_IS_NULL(pEthModule);
    
    /* Get Ethernet port */
    pEthPort = AtModuleEthPortGet(pEthModule, ethIntfIndex);
    DRV_TDM_CHECK_POINTER_IS_NULL(pEthPort);
    
    memset(&ethIntfPktCnt, 0, sizeof(tAtEthPortCounters));
    /* 获取Ethernet接口的报文统计,读清的. */
    rv = AtChannelAllCountersClear((AtChannel)pEthPort, &ethIntfPktCnt); 
    if (cAtOk != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, AtChannelAllCountersClear() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "\n[subslotId %u ethIntfIndex %u]'s packet counters:\n", subslotId, ethIntfIndex);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txPackets: 0x%x\n", ethIntfPktCnt.txPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txBytes: 0x%x\n", ethIntfPktCnt.txBytes);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txOamPackets: 0x%x\n", ethIntfPktCnt.txOamPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txPeriodOamPackets: 0x%x\n", ethIntfPktCnt.txPeriodOamPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txPwPackets: 0x%x\n", ethIntfPktCnt.txPwPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txPacketsLen0_64: 0x%x\n", ethIntfPktCnt.txPacketsLen0_64);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txPacketsLen65_127: 0x%x\n", ethIntfPktCnt.txPacketsLen65_127);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txPacketsLen128_255: 0x%x\n", ethIntfPktCnt.txPacketsLen128_255);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txPacketsLen256_511: 0x%x\n", ethIntfPktCnt.txPacketsLen256_511);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txPacketsLen512_1024: 0x%x\n", ethIntfPktCnt.txPacketsLen512_1024);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txPacketsLen1025_1528: 0x%x\n", ethIntfPktCnt.txPacketsLen1025_1528);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txPacketsJumbo: 0x%x\n", ethIntfPktCnt.txPacketsJumbo);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "txTopPackets: 0x%x\n", ethIntfPktCnt.txTopPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxPackets: 0x%x\n", ethIntfPktCnt.rxPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxBytes: 0x%x\n", ethIntfPktCnt.rxBytes);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxErrEthHdrPackets: 0x%x\n", ethIntfPktCnt.rxErrEthHdrPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxErrBusPackets: 0x%x\n", ethIntfPktCnt.rxErrBusPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxErrFcsPackets: 0x%x\n", ethIntfPktCnt.rxErrFcsPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxOversizePackets: 0x%x\n", ethIntfPktCnt.rxOversizePackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxUndersizePackets: 0x%x\n", ethIntfPktCnt.rxUndersizePackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxPacketsLen0_64: 0x%x\n", ethIntfPktCnt.rxPacketsLen0_64);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxPacketsLen65_127: 0x%x\n", ethIntfPktCnt.rxPacketsLen65_127);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxPacketsLen128_255: 0x%x\n", ethIntfPktCnt.rxPacketsLen128_255);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxPacketsLen256_511: 0x%x\n", ethIntfPktCnt.rxPacketsLen256_511);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxPacketsLen512_1024: 0x%x\n", ethIntfPktCnt.rxPacketsLen512_1024);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxPacketsLen1025_1528: 0x%x\n", ethIntfPktCnt.rxPacketsLen1025_1528);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxPacketsJumbo: 0x%x\n", ethIntfPktCnt.rxPacketsJumbo);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxPwUnsupportedPackets: 0x%x\n", ethIntfPktCnt.rxPwUnsupportedPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxErrPwLabelPackets: 0x%x\n", ethIntfPktCnt.rxErrPwLabelPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxDiscardedPackets: 0x%x\n", ethIntfPktCnt.rxDiscardedPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxPausePackets: 0x%x\n", ethIntfPktCnt.rxPausePackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxErrPausePackets: 0x%x\n", ethIntfPktCnt.rxErrPausePackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxBrdCastPackets: 0x%x\n", ethIntfPktCnt.rxBrdCastPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxMultCastPackets: 0x%x\n", ethIntfPktCnt.rxMultCastPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxArpPackets: 0x%x\n", ethIntfPktCnt.rxArpPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxOamPackets: 0x%x\n", ethIntfPktCnt.rxOamPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxEfmOamPackets: 0x%x\n", ethIntfPktCnt.rxEfmOamPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxErrEfmOamPackets: 0x%x\n", ethIntfPktCnt.rxErrEfmOamPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxEthOamType1Packets: 0x%x\n", ethIntfPktCnt.rxEthOamType1Packets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxEthOamType2Packets: 0x%x\n",ethIntfPktCnt.rxEthOamType2Packets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxIpv4Packets: 0x%x\n", ethIntfPktCnt.rxIpv4Packets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxErrIpv4Packets: 0x%x\n", ethIntfPktCnt.rxErrIpv4Packets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxIcmpIpv4Packets: 0x%x\n", ethIntfPktCnt.rxIcmpIpv4Packets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxIpv6Packets: 0x%x\n", ethIntfPktCnt.rxIpv6Packets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxErrIpv6Packets: 0x%x\n", ethIntfPktCnt.rxErrIpv6Packets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxIcmpIpv6Packets: 0x%x\n", ethIntfPktCnt.rxIcmpIpv6Packets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxMefPackets: 0x%x\n", ethIntfPktCnt.rxMefPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxErrMefPackets: 0x%x\n", ethIntfPktCnt.rxErrMefPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxMplsPackets: 0x%x\n", ethIntfPktCnt.rxMplsPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxErrMplsPackets: 0x%x\n", ethIntfPktCnt.rxErrMplsPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxMplsErrOuterLblPackets: 0x%x\n", ethIntfPktCnt.rxMplsErrOuterLblPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxMplsDataPackets: 0x%x\n", ethIntfPktCnt.rxMplsDataPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxLdpIpv4Packets: 0x%x\n", ethIntfPktCnt.rxLdpIpv4Packets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxLdpIpv6Packets: 0x%x\n", ethIntfPktCnt.rxLdpIpv6Packets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxMplsIpv4Packets: 0x%x\n", ethIntfPktCnt.rxMplsIpv4Packets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxMplsIpv6Packets: 0x%x\n", ethIntfPktCnt.rxMplsIpv6Packets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxL2tpv3Packets: 0x%x\n", ethIntfPktCnt.rxL2tpv3Packets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxErrL2tpv3Packets: 0x%x\n", ethIntfPktCnt.rxErrL2tpv3Packets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxL2tpv3Ipv4Packets: 0x%x\n", ethIntfPktCnt.rxL2tpv3Ipv4Packets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxL2tpv3Ipv6Packets: 0x%x\n", ethIntfPktCnt.rxL2tpv3Ipv6Packets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxUdpPackets: 0x%x\n", ethIntfPktCnt.rxUdpPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxErrUdpPackets: 0x%x\n", ethIntfPktCnt.rxErrUdpPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxUdpIpv4Packets: 0x%x\n", ethIntfPktCnt.rxUdpIpv4Packets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxUdpIpv6Packets: 0x%x\n", ethIntfPktCnt.rxUdpIpv6Packets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxErrPsnPackets: 0x%x\n", ethIntfPktCnt.rxErrPsnPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxPacketsSendToCpu: 0x%x\n", ethIntfPktCnt.rxPacketsSendToCpu);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxPacketsSendToPw: 0x%x\n", ethIntfPktCnt.rxPacketsSendToPw);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxTopPackets: 0x%x\n", ethIntfPktCnt.rxTopPackets);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rxPacketDaMis: 0x%x\n", ethIntfPktCnt.rxPacketDaMis);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_ethIntfAlmShow
* 功能描述: 显示Ethernet接口的告警.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
*           ethIntfIndex: ARRIVE芯片的Ethernet接口索引,取值为0~1.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-11   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_ethIntfAlmShow(BYTE subslotId, BYTE ethIntfIndex)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;
    DRV_TDM_AT_DEV_ETH_INTF_ALM ethIntfAlm;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    DRV_TDM_CHECK_ETH_INTF_INDEX(ethIntfIndex);
    chipId = subslotId - (BYTE)1;

    memset(&ethIntfAlm, 0, sizeof(DRV_TDM_AT_DEV_ETH_INTF_ALM));
    rv = drv_tdm_ethIntfAlmGet(chipId, ethIntfIndex, &ethIntfAlm);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_ethIntfAlmGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    if (DRV_TDM_AT_ETH_INFF_LINK_DOWN == ethIntfAlm.ethIntfAlm)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u ethIntfIndex %u, the status of ethernet port: DOWN.\n", subslotId, ethIntfIndex);
    }
    else if (DRV_TDM_AT_ETH_INFF_LINK_UP == ethIntfAlm.ethIntfAlm)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u ethIntfIndex %u, the status of ethernet port: UP.\n", subslotId, ethIntfIndex);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u ethIntfIndex %u, the status of ethernet port: UNKNOWN.\n", subslotId, ethIntfIndex);
    }
    
    return rv;
}


/**************************************************************************
* 函数名称: drv_tdm_atPwAlarmShow
* 功能描述: 显示PW告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
*           pwId: 驱动的PW ID,取值为0~1023.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 直接读取芯片来获取PW告警
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-27   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atPwAlarmShow(BYTE subslotId, WORD16 pwId)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;
    BYTE pwState = 0;
    DRV_TDM_PW_ALM_INFO pwAlarm;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    DRV_TDM_CHECK_PW_ID(pwId);
    chipId = subslotId - (BYTE)1;
    
    rv = drv_tdm_pwStateGet(chipId, pwId, &pwState);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_pwStateGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_PW_FREE == pwState)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, (chipId %u pwId %u) is free.\n", __FILE__, __LINE__, chipId, pwId);
        return DRV_TDM_INVALID_PW_STATE;
    }
    
    memset(&pwAlarm, 0, sizeof(DRV_TDM_PW_ALM_INFO));
    rv = drv_tdm_pwAlarmGet(chipId, pwId, &pwAlarm);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_pwAlarmGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "[subslotId %u pwId %u]'s PW alarm:\n", subslotId, pwId);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "lopsAlm: %u\n", pwAlarm.lopsAlm);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "jitterBufOverrunAlm: %u\n", pwAlarm.jitterBufOverrunAlm);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "jitterBufUnderrunAlm: %u\n", pwAlarm.jitterBufUnderrunAlm);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "lBitAlm: %u\n", pwAlarm.lBitAlm);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rBitAlm: %u\n", pwAlarm.rBitAlm);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "mBitAlm: %u\n", pwAlarm.mBitAlm);
    
    return rv;
}


/**************************************************************************
* 函数名称: drv_tdm_pwAlarmShow
* 功能描述: 显示PW告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
*           pwId: 驱动的PW ID,取值为0~1023.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 读取驱动的软件表g_drv_tdm_pw_alarm来获取PW告警
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-27   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwAlarmShow(BYTE subslotId, WORD16 pwId)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;
    BYTE pwState = 0;
    DRV_TDM_PW_ALM_INFO *pPwAlarm = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    DRV_TDM_CHECK_PW_ID(pwId);
    chipId = subslotId - (BYTE)1;
    
    rv = drv_tdm_pwStateGet(chipId, pwId, &pwState);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_pwStateGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_PW_FREE == pwState)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, (chipId %u pwId %u) is free.\n", __FILE__, __LINE__, chipId, pwId);
        return DRV_TDM_INVALID_PW_STATE;
    }
    
    rv = drv_tdm_pwAlarmMemGet(chipId, pwId, &pPwAlarm);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_pwAlarmMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwAlarm);
    
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "\n[subslotId %u pwId %u]'s PW alarm:\n", subslotId, pwId);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "lopsAlm: %u\n", pPwAlarm->lopsAlm);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "jitterBufOverrunAlm: %u\n", pPwAlarm->jitterBufOverrunAlm);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "jitterBufUnderrunAlm: %u\n", pPwAlarm->jitterBufUnderrunAlm);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "lBitAlm: %u\n", pPwAlarm->lBitAlm);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rBitAlm: %u\n", pPwAlarm->rBitAlm);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "mBitAlm: %u\n", pPwAlarm->mBitAlm);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwAlmDebugFlagShow
* 功能描述: 显示PWE3告警的调试开关的值.
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3.
*           pwId: PWID,取值为0~1023.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwAlmDebugFlagShow(BYTE chipId, WORD16 pwId)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 flag = 0;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    
    rv = drv_tdm_pwAlmDebugFlagGet(chipId, pwId, &flag);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_pwAlmDebugFlagGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_pwAlmDebug[%u][%u] = %u.\n", chipId, pwId, flag);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwInfoShow
* 功能描述: 显示PW的配置信息.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 接口卡的子槽位号,取值为1~4.
*           portId: STM端口的编号,取值为1~8.
*           au4Id: AU4编号,取值为1~4.
*           e1LinkId: VC4中的E1编号,取值为1~63.
*           tsBitmap: E1时隙位图.取值为0~0xffffffff.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 通过读取驱动的软件表来显示PW的配置信息.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-03   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwInfoShow(BYTE subslotId, 
                                  BYTE portId, 
                                  BYTE au4Id, 
                                  BYTE e1LinkId, 
                                  WORD32 tsBitmap)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;
    WORD16 pwId = 0;
    DRV_TDM_PW_CFG_INFO *pPwCfgInfo = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    DRV_TDM_CHECK_AUG1_ID(au4Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_acBindingPwIdGet(subslotId, portId, au4Id, e1LinkId, tsBitmap, &pwId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_acBindingPwIdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_PW_ID(pwId);
    
    rv = drv_tdm_pwMemGet(chipId, pwId, &pPwCfgInfo);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_pwMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCfgInfo);
    
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "PW configuration information:\n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId: %u\n", pPwCfgInfo->acInfo.subslotId);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "chipId: %u\n", pPwCfgInfo->acInfo.chipId);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "portId: %u\n", pPwCfgInfo->acInfo.portId);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "au4Id: %u\n", pPwCfgInfo->acInfo.au4Id);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "tug3Id: %u\n", pPwCfgInfo->acInfo.tug3Id);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "tug2Id: %u\n", pPwCfgInfo->acInfo.tug2Id);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "tu12Id: %u\n", pPwCfgInfo->acInfo.tu12Id);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "e1LinkId: %u\n", pPwCfgInfo->acInfo.e1LinkId);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "e1TimeslotNumber: %u\n", pPwCfgInfo->acInfo.e1TsNum);
    if (pPwCfgInfo->acInfo.e1FramingMode < DRV_TDM_FRAME_MAX_SIZE)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "e1FramingMode: %s\n", g_drv_tdm_e1FramingModeStr[pPwCfgInfo->acInfo.e1FramingMode]);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "e1FramingMode: INVALID\n");
    }
    if (pPwCfgInfo->acInfo.e1TimingMode < DRV_TDM_TIMING_MAX_SIZE)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "e1TimingMode: %s\n", g_drv_tdm_e1TimingModeStr[pPwCfgInfo->acInfo.e1TimingMode]);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "e1TimingMode: INVALID\n");
    }
    if ((DRV_TDM_TIMING_MODE_ACR == pPwCfgInfo->acInfo.e1TimingMode) 
        || (DRV_TDM_TIMING_MODE_DCR == pPwCfgInfo->acInfo.e1TimingMode))
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "clockDomainNo: %u\n", pPwCfgInfo->acInfo.clkDomainNo);
        if (pPwCfgInfo->acInfo.e1LinkClkState < DRV_TDM_E1_LINK_STATE_MAX)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "e1LinkClockStatus: %s\n", g_drv_tdm_e1ClkStateStr[pPwCfgInfo->acInfo.e1LinkClkState]);
        }
        else
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "e1LinkClockStatus: INVALID\n");
        }
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "timeslotBitmap: 0x%x\n", pPwCfgInfo->acInfo.timeslotBmp);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "channelNo: 0x%x\n", pPwCfgInfo->acInfo.channelNo);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "pwId: %u\n", pwId);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "pAtPw: %p\n", pPwCfgInfo->pAtPw);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VLAN1_vlan1Id: 0x%x\n", pPwCfgInfo->vlan1Tag.vlan1Id);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VLAN1_priority: %u\n", pPwCfgInfo->vlan1Tag.priority);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VLAN1_cfi: %u\n", pPwCfgInfo->vlan1Tag.cfi);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VLAN2_vlan2Id: 0x%x\n", pPwCfgInfo->vlan2Tag.vlan2Id);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VLAN2_priority: %u\n", pPwCfgInfo->vlan2Tag.priority);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "VLAN2_cfi: %u\n", pPwCfgInfo->vlan2Tag.cfi);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "pktNumInBuf: %u\n", pPwCfgInfo->pktNumInBuf);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "jitterBufferSize: 0x%x\n", pPwCfgInfo->jitterBufSize);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "jitterDelay: 0x%x\n", pPwCfgInfo->jitterDelay);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "encapsulationNumber: %u\n", pPwCfgInfo->encapNum);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "payloadSize: 0x%x\n", pPwCfgInfo->pldSize);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "dstMacAddress: %02X.%02X.%02X.%02X.%02X.%02X\n", 
                        pPwCfgInfo->dstMacAddr[0],  
                        pPwCfgInfo->dstMacAddr[1],  
                        pPwCfgInfo->dstMacAddr[2],  
                        pPwCfgInfo->dstMacAddr[3],   
                        pPwCfgInfo->dstMacAddr[4],  
                        pPwCfgInfo->dstMacAddr[5]);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ethIntfIndex: %u\n", pPwCfgInfo->ethIntfIndex);
    if (pPwCfgInfo->ethIntfSwitchFlag < DRV_TDM_ETH_INTF_FLAG_MAX)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ethIntfSwitchFlag: %s\n", g_drv_tdm_ethIntfFlagStr[pPwCfgInfo->ethIntfSwitchFlag]);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ethIntfSwitchFlag: INVALID\n");
    }
    if (DRV_TDM_RTP_DISABLE == pPwCfgInfo->rtpEnable)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rtpEnable: DISABLE\n");
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rtpEnable: ENABLE\n");
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "rtpSsrc: 0x%x\n", pPwCfgInfo->rtpCfg.ssrc);
    }
    if (DRV_TDM_PW_CW_DISABLE == pPwCfgInfo->cwEnable)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "controlWord: DISABLE\n");
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "controlWord: ENABLE\n");
    }
    if (DRV_TDM_PW_CW_SEQ_DISABLE == pPwCfgInfo->seqEnable)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "cwSequenceNO: DISABLE\n");
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "cwSequenceNO: ENABLE\n");
    }
    if (DRV_TDM_SATOP_CHANNEL == pPwCfgInfo->channelMode)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "pwType: SAToP PW\n");
    }
    else if (DRV_TDM_CESOP_BASIC_CHANNEL == pPwCfgInfo->channelMode)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "pwType: CESoPSN Basic PW\n");
    }
    else if (DRV_TDM_CESOP_CAS_CHANNEL == pPwCfgInfo->channelMode)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "pwType: CESoPSN CAS PW\n");
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "pwType: Unknown PW\n");
    }
    if ((DRV_TDM_CESOP_BASIC_CHANNEL == pPwCfgInfo->channelMode) 
        || (DRV_TDM_CESOP_CAS_CHANNEL == pPwCfgInfo->channelMode))
    {
        if (pPwCfgInfo->pwAttribute < DRV_TDM_PW_ATTRIBUTE_MAX)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "pwAttribute: %s\n", g_drv_tdm_pwAttributeStr[pPwCfgInfo->pwAttribute]);
        }
        else
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "pwAttribute: INVALID\n");
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atPwJitBufInfoShow
* 功能描述: 显示FPGA芯片的PW jitter buffer size 和 jitter buffer delay.
* 访问的表: 无
* 修改的表: 无
* 输入参数: ucChipId: 芯片编号,取值为0~3.
*           wPwId: PW ID,取值为0~1023.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 直接显示FPGA芯片的值.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atPwJitBufInfoShow(BYTE ucChipId, WORD16 wPwId)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    WORD32 dwJitBufSize = 0;
    WORD32 dwJitBufDelay = 0;

    DRV_TDM_CHECK_CHIP_ID(ucChipId);
    DRV_TDM_CHECK_PW_ID(wPwId);
    
    dwRetValue = drv_tdm_atPwJitBufSizeGet(ucChipId, wPwId, &dwJitBufSize);
    if (DRV_TDM_OK != dwRetValue)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atPwJitBufSizeGet() rv=0x%x.\n", __FILE__, __LINE__, dwRetValue);
        return dwRetValue;
    }
    dwRetValue = drv_tdm_atPwJitBufDelayGet(ucChipId, wPwId, &dwJitBufDelay);
    if (DRV_TDM_OK != dwRetValue)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atPwJitBufDelayGet() rv=0x%x.\n", __FILE__, __LINE__, dwRetValue);
        return dwRetValue;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "%s line %d, chipId %u pwId %u, jitterBufferSize=%u, jitterBufferDelay=%u.\n", __FILE__, __LINE__, ucChipId, wPwId, dwJitBufSize, dwJitBufDelay);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1BindingPwNumShow
* 功能描述: 显示E1链路绑定的PW的数量.
* 访问的表: 
* 修改的表: 
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           tug3Id: TUG3编号,取值为1~3.
*           tug2Id: TUG2编号,取值为1~7.
*           tu12Id: TU12编号,取值为1~3.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-30   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1BindingPwNumShow(BYTE subslotId, 
                                                 BYTE portId, 
                                                 BYTE aug1Id, 
                                                 BYTE tug3Id, 
                                                 BYTE tug2Id, 
                                                 BYTE tu12Id)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;
    BYTE e1LinkId = 0;
    WORD32 linkId = 0;
    WORD32 pwNum = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_e1LinkIdGet(tug3Id, tug2Id, tu12Id, &e1LinkId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_e1LinkIdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_linkIdGet(chipId, portId, aug1Id, e1LinkId, &linkId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_linkIdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_e1BindingPwNumGet(chipId, portId, aug1Id, e1LinkId, &pwNum);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_e1BindingPwNumGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_e1BindingPwNum[%u][%u] = %u.\n", chipId, linkId, pwNum);

    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stmIntfPwNumShow
* 功能描述: 显示STM接口绑定的PW的数量.
* 访问的表: 
* 修改的表: 
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
*           portId: STM端口编号,取值为1~8.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-01-24   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stmIntfPwNumShow(BYTE subslotId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;
    WORD32 dwPwNum = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_stmIntfPwNumGet(chipId, portId, &dwPwNum);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stmIntfPwNumGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_stmIntfPwNum[%u][%u] = %u.\n", chipId, portId, dwPwNum);
    
    return DRV_TDM_OK; 
}


/**************************************************************************
* 函数名称: drv_tdm_ethIntfPwNumShow
* 功能描述: 显示Ethernet接口绑定的PW的数量.
* 访问的表: 
* 修改的表: 
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
*           ethIntfId: Ethernet接口编号,取值为0~1.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-30   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_ethIntfPwNumShow(BYTE subslotId, BYTE ethIntfId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;
    WORD32 pwNum = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    DRV_TDM_CHECK_ETH_INTF_INDEX(ethIntfId);
    chipId = subslotId - (BYTE)1;
    
    rv = drv_tdm_ethIntfPwNumGet(chipId, ethIntfId, &pwNum);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_ethIntfPwNumGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "g_drv_tdm_ethIntfPwNum[%u][%u] = %u.\n", chipId, ethIntfId, pwNum);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1TimingModeShow
* 功能描述: 显示E1通道的时钟模式.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: 无
* 返 回 值: 
* 其它说明: 直接读取芯片来显示E1的时钟模式.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-18   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1TimingModeShow(BYTE subslotId, BYTE portId, BYTE e1LinkId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;         /* ARRIVE芯片的芯片编号 */
    AtPdhDe1 pE1 = NULL;
    WORD32 e1TimingMode = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PARAM_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atPdhE1PtrGet(chipId, portId, 1, e1LinkId, &pE1);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atPdhE1PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pE1);
    
    e1TimingMode = AtChannelTimingModeGet((AtChannel)pE1);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u portId %u e1LinkId %u, E1 timing mode: ", subslotId, portId, e1LinkId);
    if (cAtTimingModeUnknown == e1TimingMode)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "unknown timing mode.\n");
    }
    else if (cAtTimingModeSys == e1TimingMode)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "system timing mode.\n");
    }
    else if (cAtTimingModeLoop == e1TimingMode)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "loop timing mode.\n");
    }
    else if (cAtTimingModeSdhLineRef == e1TimingMode)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "sdh line reference timing mode.\n");
    }
    else if (cAtTimingModeExt1Ref == e1TimingMode)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "line EXT #1 timing mode.\n");
    }
    else if (cAtTimingModeExt2Ref == e1TimingMode)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "line EXT #2 timing mode.\n");
    }
    else if (cAtTimingModeAcr == e1TimingMode)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ACR timing mode.\n");
    }
    else if (cAtTimingModeDcr == e1TimingMode)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "DCR timing mode.\n");
    }
    else if (cAtTimingModeSlave == e1TimingMode)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "slave timing mode.\n");
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "invalid value, e1TimingMode = %u.\n", e1TimingMode);
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_masterE1TimingStatusShow
* 功能描述: 显示主E1链路的时钟状态.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-18   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_masterE1TimingStatusShow(BYTE subslotId, BYTE portId, BYTE e1LinkId)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 clkState = 0;
    BYTE chipId = 0;
    BYTE tug3Id = 0;
    BYTE tug2Id = 0;
    BYTE tu12Id = 0;

    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_masterE1TimingStatusGet(subslotId, portId, 1, e1LinkId, &clkState);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_masterE1TimingStatusGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (clkState >= DRV_TDM_TIMING_STATE_MAX_SIZE)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, invalid clock state, clkState=0x%x\n", __FILE__, __LINE__, clkState);
        return DRV_TDM_ERROR;
    }
    rv = drv_tdm_tug3Tug2Tu12IdGet(e1LinkId, &tug3Id, &tug2Id, &tu12Id);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_tug3Tug2Tu12IdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "[subslotId %u portId %u tug3Id %u tug2Id %u tu12Id %u]'s timing status: %s\n", 
                        subslotId, portId, tug3Id, tug2Id, tu12Id, g_drv_tdm_e1TimingStatusStr[clkState]);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1ClkSwitchAlmShow
* 功能描述: 显示E1链路的时钟切换告警.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
*           portId: 端口编号,取值为1~8.
*           e1LinkId: E1链路编号,取值为1~63.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-11-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1ClkSwitchAlmShow(BYTE subslotId, BYTE portId, BYTE e1LinkId)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE e1ClkAlmState = 0;
    BYTE chipId = 0;
    BYTE tug3Id = 0;
    BYTE tug2Id = 0;
    BYTE tu12Id = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_e1ClkSwitchAlmQuery(chipId, portId, 1, e1LinkId, &e1ClkAlmState);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_e1ClkSwitchAlmQuery() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (e1ClkAlmState >= DRV_TDM_CLK_SWITCH_ALM_MAX)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, invalid e1 clock switch alarm, e1ClkSwitchAlarm=0x%x\n", __FILE__, __LINE__, e1ClkAlmState);
        return DRV_TDM_ERROR;
    }
    
    rv = drv_tdm_tug3Tug2Tu12IdGet(e1LinkId, &tug3Id, &tug2Id, &tu12Id);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_tug3Tug2Tu12IdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "[subslotId %u portId %u tug3Id %u tug2Id %u tu12Id %u]'s clock switch alarm status: %s\n", 
                        subslotId, portId, tug3Id, tug2Id, tu12Id, g_drv_tdm_e1ClkSwitchAlmStr[e1ClkAlmState]);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_clockDomainShow
* 功能描述: 显示时钟域信息.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: CP3BAN单板的子槽位号,取值为1~4.
*           clkDomainNo: clock domain NO,从1开始取值.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-29   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_clockDomainShow(BYTE subslotId, WORD32 clkDomainNo)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 i = 0;
    BYTE chipId = 0;
    WORD32 clkDomainId = DRV_TDM_MAX_CLK_DOMAIN_NUM;  /* 初始化时,赋值为无效值 */
    DRV_TDM_CLOCK_DOMAIN *pClkDomain = NULL;
    DRV_TDM_E1_LINK masterE1Link;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    memset(&masterE1Link, 0, sizeof(DRV_TDM_E1_LINK));
    
    rv = drv_tdm_clkDomainIdGet(chipId, clkDomainNo, &clkDomainId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_clkDomainIdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_CLOCK_DOMAIN_ID(clkDomainId);
    
    rv = drv_tdm_clkDomainMemGet(chipId, clkDomainId, &pClkDomain);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_clkDomainMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pClkDomain);
    
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "[subslotId %u clockDomainNo %u]'s clock domain information:\n", subslotId, clkDomainNo);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "clockDomainNo: %u\n", pClkDomain->clkDomainNo);
    if (pClkDomain->clkDomainState < DRV_TDM_CLK_DOMAIN_MAX)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "clockDomainState: %s\n", g_drv_tdm_clkDomainStateStr[pClkDomain->clkDomainState]);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "clockDomainState: INVALID\n");
    }
    rv = drv_tdm_masterE1LinkGet(chipId, clkDomainId, &masterE1Link);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_masterE1LinkGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "Master E1 link's information:\n");
    if ((masterE1Link.e1ClkMode < DRV_TDM_TIMING_MAX_SIZE) 
        && (masterE1Link.e1LinkClkState < DRV_TDM_E1_LINK_STATE_MAX) 
        && (masterE1Link.linkStatus < DRV_TDM_E1_LINK_STATUS_MAX))
    {    
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "    chipId %u portId %u au4Id %u tug3Id %u tug2Id %u tu12Id %u, e1ClkMode %s, e1LinkClkState %s, e1LinkStatus %s.\n", 
                            masterE1Link.chipId, 
                            masterE1Link.portId, 
                            masterE1Link.au4Id, 
                            masterE1Link.tug3Id, 
                            masterE1Link.tug2Id, 
                            masterE1Link.tu12Id, 
                            g_drv_tdm_e1TimingModeStr[masterE1Link.e1ClkMode], 
                            g_drv_tdm_e1ClkStateStr[masterE1Link.e1LinkClkState], 
                            g_drv_tdm_e1LinkStatusStr[masterE1Link.linkStatus]);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "INVALID\n");
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "Slave E1 link's information:\n");
    for (i = DRV_TDM_LINK_ID_START; i <= DRV_TDM_LINK_ID_END; i++)
    {
        if ((DRV_TDM_E1_LINK_SLAVE == pClkDomain->e1Link[i].e1LinkClkState) 
            && (DRV_TDM_E1_LINK_USED == pClkDomain->e1Link[i].linkStatus))
        {
            if ((pClkDomain->e1Link[i].e1ClkMode < DRV_TDM_TIMING_MAX_SIZE) 
                && (pClkDomain->e1Link[i].e1LinkClkState < DRV_TDM_E1_LINK_STATE_MAX) 
                && (pClkDomain->e1Link[i].linkStatus < DRV_TDM_E1_LINK_STATUS_MAX))
            {    
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "    chipId %u portId %u au4Id %u tug3Id %u tug2Id %u tu12Id %u, e1ClkMode %s, e1LinkClkState %s, e1LinkStatus %s.\n", 
                                    pClkDomain->e1Link[i].chipId, 
                                    pClkDomain->e1Link[i].portId, 
                                    pClkDomain->e1Link[i].au4Id, 
                                    pClkDomain->e1Link[i].tug3Id, 
                                    pClkDomain->e1Link[i].tug2Id, 
                                    pClkDomain->e1Link[i].tu12Id, 
                                    g_drv_tdm_e1TimingModeStr[pClkDomain->e1Link[i].e1ClkMode], 
                                    g_drv_tdm_e1ClkStateStr[pClkDomain->e1Link[i].e1LinkClkState], 
                                    g_drv_tdm_e1LinkStatusStr[pClkDomain->e1Link[i].linkStatus]);
            }
            else
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "INVALID\n");
            }
        }
        else
        {
            continue;
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stm1IntfCfgShow
* 功能描述: 显示STM-1接口的配置信息.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.
*           portId: STM-1端口的编号,取值为1~8.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-18   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stm1IntfCfgShow(BYTE subslotId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;
    BYTE e1LinkId = 0;
    BYTE tug3Id = 0;
    BYTE tug2Id = 0;
    BYTE tu12Id = 0;
    WORD32 stmLoopbackMode = 0;
    WORD32 scramble = 0;
    WORD32 laserState = 0;
    WORD32 stmIntfState = 0;
    WORD32 stmIntfTxClkMode = 0;
    BYTE e1FramingMode = 0;
    BYTE e1TimingMode = 0;
    BYTE e1LoopbackMode = 0;
    BYTE e1LinkBindingState = 0; /* E1链路是否绑定了PW  */
    DRV_TDM_SDH_INTF_INFO *pIntf = NULL;

    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_sdhIntfMemGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, &pIntf);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_sdhIntfMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pIntf);
    
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "\n[subslotId %u portId %u]'s STM-1 interface configuration information: \n", subslotId, portId);
    stmLoopbackMode = pIntf->stmIntfLbMd;
    if (stmLoopbackMode < DRV_SDH_LOOPBACK_MAX_SIZE)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "STM-1 interface loopback mode: %s\n", 
                            g_drv_tdm_sdhLbMdStr[stmLoopbackMode]);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "STM-1 interface loopback mode: %s\n", 
                            "INVALID");
    }
    scramble = pIntf->txStmScrambleEn;
    if (scramble < DRV_TDM_STM1_SCRAMBLE_MAX_SIZE)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "STM-1 TX scramble: %s\n", 
                            g_drv_tdm_txScrambleStr[scramble]);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "STM-1 TX scramble: %s\n", "INVALID");
    }
    laserState = pIntf->txLaserState;
    if (laserState < DRV_TDM_INTF_TX_LASER_MAX_SIZE)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "STM-1 TX Laser state: %s\n", 
                            g_drv_tdm_txLaserStr[laserState]);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "STM-1 TX scramble: %s\n", "INVALID");
    }
    stmIntfState = pIntf->stmIntfState;
    if (stmIntfState < DRV_TDM_STM_INTF_MAX_SIZE)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "STM-1 interface state: %s\n", 
                            g_drv_tdm_stm1IntfStateStr[stmIntfState]);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "STM-1 interface state: %s\n", "INVALID");
    }
    stmIntfTxClkMode = pIntf->stmIntfTxClkMd;
    if (stmIntfTxClkMode < DRV_TDM_STM_INTF_CLK_MAX_SIZE)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "STM-1 interface tx clock mode: %s\n", 
                            g_drv_tdm_stm1IntfTxClkMdStr[stmIntfTxClkMode]);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "STM-1 interface tx clock mode: %s\n", "INVALID");
    }
    for (e1LinkId = DRV_TDM_E1_LINK_ID_START; e1LinkId <= DRV_TDM_E1_LINK_ID_END; e1LinkId++)
    {
        rv = drv_tdm_tug3Tug2Tu12IdGet(e1LinkId, &tug3Id, &tug2Id, &tu12Id);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_tug3Tug2Tu12IdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            continue;
        }
        e1FramingMode = pIntf->e1FramingMode[e1LinkId];
        if (e1FramingMode < DRV_TDM_FRAME_MAX_SIZE)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "[%u-%u-%u] E1 framing mode: %s\n", 
                                tug3Id, tug2Id, tu12Id, g_drv_tdm_e1FramingModeStr[e1FramingMode]);
        }
        else
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "[%u-%u-%u] E1 framing mode: %s\n", 
                                tug3Id, tug2Id, tu12Id, "INVALID");
        }
        e1TimingMode = pIntf->e1TimingMode[e1LinkId];
        if (e1TimingMode < DRV_TDM_TIMING_MAX_SIZE)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "[%u-%u-%u] E1 timing mode: %s\n", 
                                tug3Id, tug2Id, tu12Id, g_drv_tdm_e1TimingModeStr[e1TimingMode]);
        }
        else
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "[%u-%u-%u] E1 timing mode: %s\n", 
                                tug3Id, tug2Id, tu12Id, "INVALID");
        }
        e1LoopbackMode = pIntf->e1LinkLbMd[e1LinkId];
        if (e1LoopbackMode < DRV_PDH_LOOPBACK_MAX_SIZE)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "[%u-%u-%u] E1 loopback mode: %s\n", 
                                tug3Id, tug2Id, tu12Id, g_drv_tdm_e1LinkLbMdStr[e1LoopbackMode]);
        }
        else
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "[%u-%u-%u] E1 loopback mode: %s\n", 
                                tug3Id, tug2Id, tu12Id, "INVALID");
        }
        e1LinkBindingState = pIntf->e1BindingState[e1LinkId];
        if (e1LinkBindingState < DRV_TDM_E1_LINK_BINDING_MAX)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "[%u-%u-%u] E1 binding state: %s\n", 
                                tug3Id, tug2Id, tu12Id, g_drv_tdm_e1BindingStateStr[e1LinkBindingState]);
        }
        else
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "[%u-%u-%u] E1 binding state: %s\n", 
                                tug3Id, tug2Id, tu12Id, "INVALID");
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atStmIntfLbMdShow
* 功能描述: 显示ARRIVE芯片的STM接口的环回方式.
* 访问的表: 无
* 修改的表: 无
* 输入参数: ucSubslotId: CP3BAN单板的子槽位号,取值为1~4.
*           ucPortId: STM端口的编号,取值为1~8.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 直接从芯片中读取端口的环回方式.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atStmIntfLbMdShow(BYTE ucSubslotId, BYTE ucPortId)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE ucChipId = 0;
    AtSdhLine pLine = NULL;
    BYTE atDevLbMd = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(ucSubslotId);
    ucChipId = ucSubslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(ucChipId, ucPortId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    /* Get SDH line. */
    rv = drv_tdm_atSdhLinePtrGet(ucChipId, ucPortId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, chipId %u portId %u, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, ucChipId, ucPortId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);

    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "\n[subslotId %u portId %u]'s loopback mode: ", ucSubslotId, ucPortId);
    atDevLbMd = AtChannelLoopbackGet((AtChannel)pLine);
    if (cAtLoopbackModeRelease == atDevLbMd)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "RELEASE\n");
    }
    else if (cAtLoopbackModeLocal == atDevLbMd)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "INTERNAL\n");
    }
    else if (cAtLoopbackModeRemote == atDevLbMd)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "EXTERNAL\n");
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "INVALID\n");
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_sgmiiSerdesShow
* 功能描述: 显示SGMII接口的SERDES配置信息.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: CP3BAN单板的子槽位号,取值为1~4.
*           ethIntfIndex: SGMII接口的索引号,取值为0~1.
*           phyParameterId: 参数ID,具体参见DRV_TDM_SGMII_SERDES_PARAM定义.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_sgmiiSerdesShow(BYTE subslotId, BYTE ethIntfIndex, WORD32 phyParameterId)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 value = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    DRV_TDM_CHECK_ETH_INTF_INDEX(ethIntfIndex);
    
    rv = drv_tdm_sgmiiSerdesGet(subslotId, ethIntfIndex, phyParameterId, &value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_sgmiiSerdesGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u ethIntfIndex %u phyParameterId %u, value = 0x%x.\n", subslotId, ethIntfIndex, phyParameterId, value);
    
    return rv;
}


/**************************************************************************
* 函数名称: drv_tdm_secAlmIntrInfoShow
* 功能描述: 显示STM帧的断层告警的中断信息.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: CP3BAN单板的子槽位号,取值为1~4.
*           portId: STM端口的编号,取值为1~8.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-13   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_secAlmIntrInfoShow(BYTE subslotId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;
    WORD32 intrStatus = 0;
    WORD32 intrMask = 0;

    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    /* 获取STM断层告警的中断状态. */
    rv = drv_tdm_secAlmIntrStatusGet(subslotId, portId, &intrStatus);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_secAlmIntrStatusGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u portId %u, sectionAlarmInterruptStatus=0x%x.\n", subslotId, portId, intrStatus);

    /* 获取STM断层告警的中断屏蔽. */
    rv = drv_tdm_secAlmIntrMaskGet(subslotId, portId, &intrMask);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_secAlmIntrMaskGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u portId %u, sectionAlarmInterruptMask=0x%x.\n", subslotId, portId, intrMask);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_fpgaClockStateShow
* 功能描述: 显示FPGA内部的时钟状态.
* 访问的表: 无
* 修改的表: 无
* 输入参数: ucSubslotId: 单板的子槽位号,取值为1~4.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 通过FPGA内部的PLL读取各时钟(TCXO 19M,125M,100M,155M)锁定状态
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-10   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_fpgaClockStateShow(BYTE ucSubslotId)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    T_DRV_TDM_FPGA_CLK_STATE tClkStatus;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(ucSubslotId);
    memset(&tClkStatus, 0, sizeof(tClkStatus));
    
    dwRetValue = drv_tdm_fpgaClockStateGet(ucSubslotId, &tClkStatus);
    if (DRV_TDM_OK != dwRetValue)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, subslotId %u, drv_tdm_fpgaClockStateGet() rv=0x%x.\n", __FILE__, __LINE__, ucSubslotId, dwRetValue);
        return dwRetValue;
    }
    if (tClkStatus.ucSysClkState < DRV_TDM_FPGA_CLK_MAX)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, sysClockStatus: %s\n", ucSubslotId, g_drv_tdm_fpgaClkStateStr[tClkStatus.ucSysClkState]);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, sysClockStatus: INVALID\n", ucSubslotId);
    }

    if (tClkStatus.ucLocalBusClkState < DRV_TDM_FPGA_CLK_MAX)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, localBusClockStatus: %s\n", ucSubslotId, g_drv_tdm_fpgaClkStateStr[tClkStatus.ucLocalBusClkState]);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, localBusClockStatus: INVALID\n", ucSubslotId);
    }

    if (tClkStatus.ucGeClkState < DRV_TDM_FPGA_CLK_MAX)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, geClockStatus: %s\n", ucSubslotId, g_drv_tdm_fpgaClkStateStr[tClkStatus.ucGeClkState]);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, geClockStatus: INVALID\n", ucSubslotId);
    }

    if (tClkStatus.ucOcnClk1State < DRV_TDM_FPGA_CLK_MAX)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, ocnClock1Status: %s\n", ucSubslotId, g_drv_tdm_fpgaClkStateStr[tClkStatus.ucOcnClk1State]);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, ocnClock1Status: INVALID\n", ucSubslotId);
    }

    if (tClkStatus.ucOcnClk2State < DRV_TDM_FPGA_CLK_MAX)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, ocnClock2Status: %s\n", ucSubslotId, g_drv_tdm_fpgaClkStateStr[tClkStatus.ucOcnClk2State]);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, ocnClock2Status: INVALID\n", ucSubslotId);
    }

    if (tClkStatus.ucDdr1ClkState < DRV_TDM_FPGA_CLK_MAX)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, ddr1ClockStatus: %s\n", ucSubslotId, g_drv_tdm_fpgaClkStateStr[tClkStatus.ucDdr1ClkState]);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, ddr1ClockStatus: INVALID\n", ucSubslotId);
    }

    if (tClkStatus.ucDdr2ClkState < DRV_TDM_FPGA_CLK_MAX)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, ddr2ClockStatus: %s\n", ucSubslotId, g_drv_tdm_fpgaClkStateStr[tClkStatus.ucDdr2ClkState]);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, ddr2ClockStatus: INVALID\n", ucSubslotId);
    }

    if (tClkStatus.ucDdr3ClkState < DRV_TDM_FPGA_CLK_MAX)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, ddr3ClockStatus: %s\n", ucSubslotId, g_drv_tdm_fpgaClkStateStr[tClkStatus.ucDdr3ClkState]);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, ddr3ClockStatus: INVALID\n", ucSubslotId);
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_fpgaRamTestedStateShow
* 功能描述: 显示FPGA芯片的RAM是否测试完成.
* 访问的表: 无
* 修改的表: 无
* 输入参数: ucSubslotId: 单板的子槽位号,取值为1~4.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 检查FPGA芯片的所有RAM是否测试完成.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-10   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_fpgaRamTestedStateShow(BYTE ucSubslotId)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    WORD32 dwFpgaRamState = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(ucSubslotId);
    
    dwRetValue = drv_tdm_fpgaRamTestedStateGet(ucSubslotId, &dwFpgaRamState);
    if (DRV_TDM_OK != dwRetValue)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, subslotId %u, drv_tdm_fpgaRamTestedStateGet() rv=0x%x.\n", __FILE__, __LINE__, ucSubslotId, dwRetValue);
        return dwRetValue;
    }
    if (dwFpgaRamState < DRV_TDM_FPGA_RAM_TEST_MAX)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, fpgaRamTestedStatus: %s\n", ucSubslotId, g_drv_tdm_fpgaRamTestedStateStr[dwFpgaRamState]);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, fpgaRamTestedStatus: INVALID\n", ucSubslotId);
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_fpgaRamStateShow
* 功能描述: 显示FPGA芯片的RAM状态.
* 访问的表: 无
* 修改的表: 无
* 输入参数: ucSubslotId: STM-1单板的子槽位号,取值为1~4.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-13   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_fpgaRamStateShow(BYTE ucSubslotId)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    WORD32 dwFpgaRamState = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(ucSubslotId);
    
    dwRetValue = drv_tdm_fpgaRamStateGet(ucSubslotId, &dwFpgaRamState);
    if (DRV_TDM_OK != dwRetValue)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, subslotId %u, drv_tdm_fpgaRamStateGet() rv=0x%x.\n", __FILE__, __LINE__, ucSubslotId, dwRetValue);
        return dwRetValue;
    }
    if (dwFpgaRamState < DRV_TDM_FPGA_RAM_MAX)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, fpgaRamStatus: %s\n", ucSubslotId, g_drv_tdm_fpgaRamStateStr[dwFpgaRamState]);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, fpgaRamStatus: INVALID\n", ucSubslotId);
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_fpgaLogicStateShow
* 功能描述: 显示FPGA芯片的FPGA逻辑状态.
* 访问的表: 无
* 修改的表: 无
* 输入参数: ucSubslotId: STM-1单板的子槽位号,取值为1~4.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-13   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_fpgaLogicStateShow(BYTE ucSubslotId)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    WORD32 dwFagaLogicState = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(ucSubslotId);
    
    dwRetValue = drv_tdm_fpgaLogicStateGet(ucSubslotId, &dwFagaLogicState);
    if (DRV_TDM_OK != dwRetValue)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, subslotId %u, drv_tdm_fpgaLogicStateGet() rv=0x%x.\n", __FILE__, __LINE__, ucSubslotId, dwRetValue);
        return dwRetValue;
    }
    if (dwFagaLogicState < DRV_TDM_FPGA_LOGIC_MAX)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, fpgaLogicStatus: %s\n", ucSubslotId, g_drv_tdm_fpgaLogicStateStr[dwFagaLogicState]);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, fpgaLogicStatus: INVALID\n", ucSubslotId);
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_fpgaPhyIntfStateShow
* 功能描述: 显示FPGA内部的物理接口的状态.
* 访问的表: 无
* 修改的表: 无
* 输入参数: ucSubslotId: 单板的子槽位号,取值为1~4.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 包括FPGA内部的8个STM-1接口及两个Ethernet接口的状态.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-10   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_fpgaPhyIntfStateShow(BYTE ucSubslotId)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    WORD32 dwStatus = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(ucSubslotId);
    
    dwRetValue = drv_tdm_fpgaPhyIntfStateGet(ucSubslotId, &dwStatus);
    if (DRV_TDM_OK != dwRetValue)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, subslotId %u, drv_tdm_fpgaPhyIntfStateGet() rv=0x%x.\n", __FILE__, __LINE__, ucSubslotId, dwRetValue);
        return dwRetValue;
    }
    if (dwStatus < DRV_TDM_FPGA_PHY_INTF_MAX)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, fpgaPhyIntfStatus: %s\n", ucSubslotId, g_drv_tdm_fpgaPhyIntfStateStr[dwStatus]);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, fpgaPhyIntfStatus: INVALID\n", ucSubslotId);
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_fpgaRamDdrNumShow
* 功能描述: 显示FPGA芯片的RAM DDR的数量.
* 访问的表: 无
* 修改的表: 无
* 输入参数: ucSubslotId: 单板的子槽位号,取值为1~4.
* 输出参数: 无.  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-12-02   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_fpgaRamDdrNumShow(BYTE ucSubslotId)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    BYTE ucChipId = 0;
    BYTE ucRamDdrNum = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(ucSubslotId);
    ucChipId = ucSubslotId - (BYTE)1;
    
    dwRetValue = drv_tdm_fpgaRamDdrNumGet(ucChipId, &ucRamDdrNum);
    if (DRV_TDM_OK != dwRetValue)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, subslotId %u, drv_tdm_fpgaRamDdrNumGet() rv=0x%x.\n", __FILE__, __LINE__, ucSubslotId, dwRetValue);
        return dwRetValue;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, fpgaRamDdrNum %u.\n", ucSubslotId, ucRamDdrNum);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_dbg_atDeviceSet
* 功能描述: 选择FPGA芯片.
* 访问的表: 无
* 修改的表: 无
* 输入参数: ucSubslotId: 单板的子槽位号,取值为1~4.
* 输出参数: 无.  
* 返 回 值: 
* 其它说明: 该函数的作用与CLI命令atcli "device select"的作用一样.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-12-02   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_dbg_atDeviceSet(BYTE ucSubslotId)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    BYTE ucChipId = 0;
    AtDevice pDevice = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(ucSubslotId);
    ucChipId = ucSubslotId - (BYTE)1;
    
    dwRetValue = drv_tdm_atDevicePtrGet(ucChipId, &pDevice);
    if (DRV_TDM_OK != dwRetValue)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, dwRetValue);
        return dwRetValue;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);

    if (NULL == CliDeviceSet(pDevice))
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, CliDeviceSet() return null pointer.\n", __FILE__, __LINE__);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_debugHelp
* 功能描述: STM TDM PWE3模块的调试帮助命令.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 无.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-10   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_debugHelp(VOID)
{
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "\nThe commands to debug STM TDM PWE3 module as followings:\n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_debugFlagSet(WORD32 flag) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_debugFlagShow(VOID) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_pwe3InitFlagShow(BYTE subslotId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_fpgaReg16Show(BYTE subslotId, WORD16 offsetAddr) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_threadIdShow(BYTE subslotId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_tIdShow(BYTE subslotId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_threadMonitorFlagShow(BYTE subslotId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_stmIntfModeShow(BYTE ucSubslotId, BYTE ucPortId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_threadMonitorFlagSet(BYTE chipId, WORD32 flag) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_atDevDriverPtrShow(BYTE subslotId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_atDevicePtrShow(BYTE subslotId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_atDevHalShow(BYTE subslotId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_stm1SdhPtrShow(BYTE subslotId, BYTE portId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_stm4SdhPtrShow(BYTE subslotId, BYTE portId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_atDevHalRegShow(BYTE subslotId, WORD32 address) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_stm1AlarmShow(BYTE subslotId, BYTE portId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_stm4AlarmShow(BYTE subslotId, BYTE portId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_stm1AlmQryShow(BYTE subslotId, BYTE portId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_stm4AlmQryShow(BYTE subslotId, BYTE portId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_atStm1PerfShow(BYTE subslotId, BYTE portId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_atStm4PerfShow(BYTE subslotId, BYTE portId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_stm1PerfShow(BYTE subslotId, BYTE portId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_stm4PerfShow(BYTE subslotId, BYTE portId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_stm1TotalPerfShow(BYTE subslotId, BYTE portId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_stm4TotalPerfShow(BYTE subslotId, BYTE portId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_stm1BerShow(BYTE subslotId, BYTE portId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_stm4BerShow(BYTE subslotId, BYTE portId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_stm1OhShow(BYTE subslotId, BYTE portId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_stm4OhShow(BYTE subslotId, BYTE portId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_atPwPktCntShow(BYTE subslotId, WORD16 pwId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_atAcPwPktCntShow(BYTE subslotId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, WORD32 timeslotBmp) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_pwPktCntShow(BYTE subslotId, WORD16 pwId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_pwTotalPktCntShow(BYTE subslotId, WORD16 pwId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_pwCntDebugFlagSet(BYTE chipId, WORD16 pwId, WORD32 flag) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_pwCntDebugFlagShow(BYTE chipId, WORD16 pwId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_ethIntfPktCntShow(BYTE subslotId, BYTE ethIntfIndex) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_ethIntfAlmShow(BYTE subslotId, BYTE ethIntfIndex) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_atPwAlarmShow(BYTE subslotId, BYTE pwId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_pwAlarmShow(BYTE subslotId, WORD16 pwId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_pwAlmDebugFlagSet(BYTE chipId, WORD16 pwId, WORD32 flag) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_pwAlmDebugFlagShow(BYTE chipId, WORD16 pwId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_pwInfoShow(BYTE subslotId, BYTE portId, BYTE au4Id, BYTE e1LinkId, WORD32 tsBitmap) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_atPwJitBufInfoShow(BYTE ucChipId, WORD16 wPwId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_e1BindingPwNumShow(BYTE subslotId, BYTE portId, BYTE aug1Id, BYTE tug3Id, BYTE tug2Id, BYTE tu12Id) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_stmIntfPwNumShow(BYTE subslotId, BYTE portId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_ethIntfPwNumShow(BYTE subslotId, BYTE ethIntfId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_ethIntfLbMdSet(BYTE chipId, BYTE ethIntfIdx, WORD32 lbMd) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_sgmiiSerdesLbMdSet(BYTE chipId, BYTE ethIntfIdx, WORD32 lbMd) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_stmIntfLbMdSet(BYTE chipId, BYTE portId, WORD32 lbMd) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_e1LinkLbMdSet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, WORD32 lbMd) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_e1TimingModeShow(BYTE subslotId, BYTE portId, BYTE e1LinkId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_stm1IntfCfgShow(BYTE subslotId, BYTE portId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_atStmIntfLbMdShow(BYTE ucSubslotId, BYTE ucPortId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_masterE1TimingStatusShow(BYTE subslotId, BYTE portId, BYTE e1LinkId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_e1ClkSwitchAlmShow(BYTE subslotId, BYTE portId, BYTE e1LinkId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_clockDomainShow(BYTE subslotId, WORD32 clkDomainNo) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_sgmiiSerdesShow(BYTE subslotId, BYTE ethIntfIndex, WORD32 phyParameterId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_secAlmIntrInfoShow(BYTE subslotId, BYTE portId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_fpgaClockStateShow(BYTE ucSubslotId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_fpgaRamTestedStateShow(BYTE ucSubslotId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_fpgaRamStateShow(BYTE ucSubslotId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_fpgaLogicStateShow(BYTE ucSubslotId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_fpgaPhyIntfStateShow(BYTE ucSubslotId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_fpgaRamDdrNumShow(BYTE ucSubslotId) \n");
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "WORD32 drv_tdm_dbg_atDeviceSet(BYTE ucSubslotId) \n");
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: atcli
* 功能描述: 执行ARRIVE的CLI平台
* 访问的表: 无
* 修改的表: 无
* 输入参数: cmdName: ARRIVE的调试命令.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-01   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 atcli(const char* cmdName)
{
    DRV_TDM_CHECK_POINTER_IS_NULL(cmdName);
    
    return AtCliExecute(cmdName);
}

void debugMeasureReadEpldFunc(WORD32 dwSubslotId, WORD16 wAddr)
{
    WORD16 regValue = 0;
    tAtOsalCurTime     startTime, stopTime;
    WORD32 totalTime = 0;
    
    memset(&startTime, 0, sizeof(tAtOsalCurTime));
    memset(&stopTime, 0, sizeof(tAtOsalCurTime));
    
    AtOsalCurTimeGet(&startTime);
    BSP_cp3banEpld16Read(dwSubslotId, wAddr, &regValue);
    AtOsalCurTimeGet(&stopTime);
    
    /* Calculate time in microsecond */
    totalTime = (stopTime.sec - startTime.sec) * 1000000 +  (stopTime.usec- startTime.usec);
    printf("Total time to perform BSP_cp3banEpld16Read = %ul\n", totalTime);
}


WORD32 testLinkBerLib(void)
{
    AtComponenentLinkTest();
    return DRV_TDM_OK;
}


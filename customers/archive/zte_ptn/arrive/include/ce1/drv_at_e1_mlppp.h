/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_at_e1_mlppp.h
* 文件标识: 
* 其它说明: ce1tan mlppp 业务代码
* 当前版本: V1.0
* 作    者: 陈博10140635.
* 完成日期: 2013-12-16
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/

#ifndef _DRV_AT_E1_MLPPP_H_
#define _DRV_AT_E1_MLPPP_H_

#include "drv_at_e1_common.h"



#define INVALID_PPP_LINKNO        0xffffffff
#define INVALID_ENCAP_CHANNO  0xffffffff
#define INVALID_FLOW_ID            0xffffffff
#define INVALID_MLPPP_BUNDLE_INDEX 0xffffffff

#define PPP_LINK_IDLE 0
#define PPP_LINK_USED 1

#define MLPPP_BUNDLE_IDLE  0
#define MLPPP_BUNDLE_USED 1

#define PCM30_TS_BITMAP   0xfffefffe
#define PCM31_TS_BITMAP   0xfffffffe


#define  AT_IPV4_PID              0x0021
#define  AT_IPV6_PID              0x0057
#define  AT_MPLSU_PID           0x0281
#define  AT_MPLSM_PID           0x0283
#define  AT_ISIS_PID               0x0023
#define  AT_BCP_PID               0x0031
#define  AT_PPP_PID                0xc000
#define  AT_MLPPP_PID            0x003d

#define  AT_IPV4_ETHTYPE          0x0800
#define  AT_IPV6_ETHTYPE          0x86dd
#define  AT_MPLSU_ETHTYPE       0x8847
#define  AT_MPLSM_ETHTYPE       0x8848

#define  AT_PPP_ROUTE_ENCAPTYP    1
#define  AT_MLPPP_ROUTE_ENCAPTYP    14

#define  AT_PRI_DEFAULT       5
#define  AT_CFI_DEFAULT       0

typedef struct
{
    BYTE pri;
    BYTE cfi;
    WORD32 zteVlan1Id;
    BYTE cpu;
    BYTE encapType;
    BYTE pktLen;
    BYTE resv[3];
}DRV_AT_PPP_VLAN_TAG1_INFO;


typedef struct
{
    BYTE pri;
    BYTE cfi;
    BYTE resv[2];
    WORD32 zteVlan2Id;
    BYTE portNo;
    BYTE isMlppp;
    WORD16 chanNo;
}DRV_AT_PPP_VLAN_TAG2_INFO;

typedef struct
{
    WORD32 cip;
    WORD32 pppLinkNo;
    WORD32 e1LinkNo;
    WORD32 encapChanNo;
    WORD32 flowId;
    WORD32 fcsMode;
    WORD32 mlpppBundleIndex;
    WORD32 mtu;
    WORD32 tdi;
    DRV_AT_PPP_VLAN_TAG1_INFO vlanTag1;
    DRV_AT_PPP_VLAN_TAG2_INFO vlanTag2;
    BYTE chipId;
    BYTE isScrambleEn;
    BYTE status;
    BYTE resv;

}DRV_AT_PPP_LINK_INFO;


typedef struct
{
    WORD32 bundleId;
    WORD32 flowId;
    WORD32 mrru;
    WORD32 seqMode;
    WORD32 fragsize;
    DRV_AT_PPP_VLAN_TAG1_INFO vlanTag1;
    DRV_AT_PPP_VLAN_TAG2_INFO vlanTag2;
    DRV_AT_PPP_LINK_INFO linkMember[AT_MAX_PPP_LINK_NUM_PER_MLPPP_BUNDLE];/*一个mlppp组包最多含32个成员，数组下标表示e1号*/
    BYTE chipId;
    BYTE status;
    BYTE resv[2];

}DRV_AT_MLPPP_BUNDLE_INFO;



typedef enum
{
    AT_HDLC_NOFCS,
    AT_HDLC_FCS16 ,    
    AT_HDLC_FCS32 ,   
}AT_HDLC_FCS_MODE;


typedef enum 
{
    AT_PPP_PHASE_UNKNOWN,       /**< Unknown phase */
    AT_PPP_PHASE_DEAD,             /**< Dead phase. No data packet nor control packet flows */
    AT_PPP_PHASE_ESTABLISH,     /**< Go to establish phase. Only LCP control packets flow. No data packet */
    AT_PPP_PHASE_AUTHEN,         /**< Go to Authenticate phase. Only LCP, authentication
                                              protocol and link quality monitoring packets are allowed */
    AT_PPP_PHASE_ENTER_NET,   /**< Go to Network phase, only LCP/NCP control packets flow. No data packet */
    AT_PPP_PHASE_ACTIVE_NET , /**< Still in Network phase and all of packets can flow */
}AT_PPP_PHASE_MODE;


typedef enum 
{
    AT_SEQ_MODE_SHORT,  /**< Short sequence */
    AT_SEQ_MODE_LONG,   /**< Long sequence */
    AT_SEQ_MODE_UNKNOWN /**< Unknown sequence mode */
}AT_MLPPP_SEQ_MODE;


typedef enum 
{
    AT_FRAG_SIZE_NONE,    /**< No fragment */
    AT_FRAG_SIZE_64,       /**< 64-byte fragment */
    AT_FRAG_SIZE_128 ,   /**< 128-byte fragment */
    AT_FRAG_SIZE_256 ,  /**< 256-byte fragment */
    AT_FRAG_SIZE_512   /**< 512-byte fragment */
}AT_FRAG_SIZE_MODE;


WORD32 DrvAtPppLinkCreate(DRV_AT_PPP_LINK_INFO* pppLinkInfo);
WORD32 DrvAtPppLinkDelete(DRV_AT_PPP_LINK_INFO* pppLinkInfo);
WORD32 DrvAtMlpppBundleCreate(DRV_AT_MLPPP_BUNDLE_INFO* mlpppInfo);
WORD32 DrvAtMlpppBundleDelete(BYTE chipId,WORD32 bundleId);
WORD32 DrvAtPppInfoGetFromE1(BYTE chipId,WORD32 e1LinkNo,DRV_AT_PPP_LINK_INFO * PppLinkInfo);
WORD32 DrvAtPppServiceCreate(T_BSP_SRV_FUNC_PPP_PW_BIND_ARG* pmPppInfo);
WORD32 DrvAtPppServiceDelete(T_BSP_SRV_FUNC_PW_UNBIND_ARG* pmPppdelInfo);
WORD32 DrvAtPppServiceModify(T_BSP_SRV_FUNC_PPP_PW_MODIFY_ARG* pppModifyInfo);
WORD32 DrvAtMlpppServiceCreate(T_BSP_SRV_FUNC_MPPP_PW_BIND_ARG* mlpppInfo);
WORD32 DrvAtMlpppServiceDelete(T_BSP_SRV_FUNC_PW_UNBIND_ARG* mlpppDelInfo);
WORD32 DrvAtMlpppServiceModify(T_BSP_SRV_FUNC_MPPP_PW_BIND_ARG* mlpppModifyInfo);
WORD32 DrvAtMlpppServiceAddLink(T_BSP_SRV_FUNC_MPPP_BUNDLE_LINK_ADD* mlpppAddLink);
WORD32 DrvAtMlpppServiceDelLink(T_BSP_SRV_FUNC_MPPP_BUNDLE_LINK_DELETE* mlpppDelLink);
WORD32 DrvAtPidSet(BYTE chipId);
WORD32 DrvAtPppLinkStatusCheck(BYTE chipId ,WORD32 pppLinkNo);
WORD32 DrvAtPppInfoGetFromPppLinkNo(BYTE chipId,WORD32 pppLinkNo,DRV_AT_PPP_LINK_INFO * PppLinkInfo);
WORD32 DrvAtMlpppInfoGetFromIndex(BYTE chipId,WORD32 bundleIndex,DRV_AT_MLPPP_BUNDLE_INFO* mlpppInfo);
WORD32 DrvAtMlpppStatusCheck(BYTE chipId ,WORD32 bundleIndex);
WORD32 DrvAtMlpppBundleAddLink(BYTE chipId,WORD32 bundleId,WORD32 e1LinkNo);
WORD32 DrvAtMlpppBundleRemoveLink(BYTE chipId,WORD32 bundleId,WORD32 e1LinkNo);

#endif


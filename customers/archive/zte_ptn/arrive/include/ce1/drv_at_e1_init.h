/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_tha_e1_init.h
* 文件标识: 
* 其它说明: 
* 当前版本: V1.0
* 作    者: 陈博10140635.
* 完成日期: 2013-04-25
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/

#ifndef _DRV_AT_E1_INIT_H_
#define _DRV_AT_E1_INIT_H_


#include "drv_at_e1_common.h"


/*卡类型*/
#define  CE1TAN  1   /*CE1 子卡*/
#define  CP3BAN  2   /*STM-1子卡*/


/*基地址*/
typedef struct
{
    WORD32 epldBaseAddr ;
    WORD32 liuBaseAddr ;
    WORD32 fpgaBaseAddr ;
}DRV_AT_BASE_ADDR;


/*address offset*/
#define  AT_BASE_ADDRESS_OFFSET_PER_CHIP       (WORD32)0xD0000000  /*每块子卡的基地址偏移量*/
/*chip id =0*/
#define  AT_CHIP_ID0_CORE_ID0_BASE_ADDRESS   (WORD32)0x080d1000      /*CHIP  0 的fpga基地址()*/
//##define  AT_CHIP_ID0_CORE_ID0_BASE_ADDRESS   (WORD32)0xE0000000      /*CHIP  0 的fpga基地址()*/
#define  AT_CHIP_ID0_CORE_ID1_BASE_ADDRESS   (WORD32)0xE1000000      /*CORE ID 1暂时用不到*/    
/*chip id =1*/
#define  AT_CHIP_ID1_CORE_ID0_BASE_ADDRESS   AT_CHIP_ID0_CORE_ID0_BASE_ADDRESS + AT_BASE_ADDRESS_OFFSET_PER_CHIP  /*CHIP  1 的基地址*/
#define  AT_CHIP_ID1_CORE_ID1_BASE_ADDRESS   (WORD32)0xD0000000
/*chip id =2*/
#define  AT_CHIP_ID2_CORE_ID0_BASE_ADDRESS   AT_CHIP_ID1_CORE_ID0_BASE_ADDRESS + AT_BASE_ADDRESS_OFFSET_PER_CHIP  /*CHIP  2 的基地址*/
#define  AT_CHIP_ID2_CORE_ID1_BASE_ADDRESS   (WORD32)0xD0000000
/*chip id =3*/
#define  AT_CHIP_ID3_CORE_ID0_BASE_ADDRESS   AT_CHIP_ID2_CORE_ID0_BASE_ADDRESS + AT_BASE_ADDRESS_OFFSET_PER_CHIP  /*CHIP  3 的基地址*/
#define  AT_CHIP_ID3_CORE_ID1_BASE_ADDRESS   (WORD32)0xD0000000
/*invalid addr*/
#define  AT_CORE_INVALID_BASE_ADDRESS  (WORD32)0xffffffff   


typedef enum {
    CE1TAN_75_OHM_DZ_CES_BOARD,
    CE1TAN_75_OHM_NBIC_CES_BOARD,
    CE1TAN_75_OHM_DZ_MLPPP_BOARD,
    CE1TAN_75_OHM_NBIC_MLPPP_BOARD,
    
    CE1TAN_120_OHM_DZ_CES_BOARD,
    CE1TAN_120_OHM_NBIC_CES_BOARD,
    CE1TAN_120_OHM_DZ_MLPPP_BOARD,
    CE1TAN_120_OHM_NBIC_MLPPP_BOARD,
    CE1TAN_UNKNOWN_BOARD_TYPE,
}CE1TAN_BOARD_TYPE;


WORD32 DrvAtDevInit(BYTE subcardId);
WORD32 DrvAtDevRemove(BYTE chipId);
WORD32 DrvAtDevCreate(BYTE chipId);
WORD32 DrvAtInstallHalForCore(BYTE chipId,BYTE coreId);
WORD32 DrvAtCe1tanBaseAddrGet(BYTE chipId,DRV_AT_BASE_ADDR* baseAddr);
WORD32 DrvThreadFlagset(BYTE chipId,BYTE flagType,WORD32 flag);


#endif


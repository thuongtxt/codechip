/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_pwe3_main.h
* 文件标识: 
* 其它说明: 驱动PWE3模块的公共头文件.
* 当前版本: V1.0
* 作    者: 谢伟生10112265.
* 完成日期: 2013-08-16
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/


#ifndef _DRV_PWE3_MAIN_H_
#define _DRV_PWE3_MAIN_H_

#include "Pub_TypeDef.h"

typedef enum
{
    DRV_PWE3_OK = 0,
    DRV_PWE3_ERROR = 0x3001, 
    DRV_PWE3_CREATE_SEM_FAIL = 0x3002, 
    DRV_PWE3_DESTROY_SEM_FAIL = 0x3003, 
    DRV_PWE3_GET_SEM_FAIL = 0x3004, 
    DRV_PWE3_FREE_SEM_FAIL = 0x3005, 
} DRV_PWE3_RETURN_CODE;  /* 函数的返回码 */

typedef enum
{
    DRV_TDM_SUBCARD_DECREASE = 0,
    DRV_TDM_SUBCARD_INCREASE = 1,
} DRV_TDM_SUBCARD_OP_TYPE;


/* 函数原型声明 */
extern WORD32 drv_pwe3_initSemaphoreIdGet(VOID);
extern WORD32 drv_pwe3_initSemaphoreCreate(VOID);
extern WORD32 drv_pwe3_initSemaphoreDestroy(VOID);
extern WORD32 drv_pwe3_initSemaphoreGet(VOID);
extern WORD32 drv_pwe3_initSemaphoreFree(VOID);
extern WORD32 drv_pwe3_cardCntSemaphoreIdGet(VOID);
extern WORD32 drv_pwe3_cardCntSemaphoreCreate(VOID);
extern WORD32 drv_pwe3_cardCntSemaphoreDestroy(VOID);
extern WORD32 drv_pwe3_cardCntSemaphoreGet(VOID);
extern WORD32 drv_pwe3_cardCntSemaphoreFree(VOID);
extern VOID drv_pwe3_subcardCntSet(WORD32 opType);
extern WORD32 drv_pwe3_subcardCntGet(VOID);

#endif



/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Pub_TypeDef.h
 * 
 * Created Date: May 29, 2014
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _PUB_TYPEDEF_H_
#define _PUB_TYPEDEF_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/
#define BSP_E_CP3BAN_OK 0
#define BSP_CP3BAN_NONE_15K_2_SYSTEM 2
#define BSP_DEBUG_ALL 0
#define BSP_CP3BAN_8PORT_BOARD 8
#define BSP_CP3BAN_4PORT_BOARD 4
#define BSP_CP3BAN_MIN_SUBSLOT_ID 0
#define BSP_CP3BAN_MAX_SUBSLOT_ID 0
#define BSP_CP3BAN_TRUE 0
#define BSP_CP3BAN_THREAD_STOP 0
#define BSP_CP3BAN_BOARD_ONLINE 0
#define BSP_CP3BAN_BOARD_ONLINE 0
#define BSP_CP3BAN_BOARD_UNINSTALL 0
#define BSP_CP3BAN_DISABLE 0

#define BSP_CP3BAN_MAX_BOARD_NUM 4
#define BSP_CP3BAN_MAX3997_NUM 4
#define BSP_CP3BAN_MAX_STM_PORT_NUM 8
#define BSP_CP3BAN_MAX_STM1_8PORT_ID 8
#define BSP_CP3BAN_MAX_STM1_4PORT_ID 4

#define MUTEX_STYLE 0
#define BSP_CP3BAN_SEM_NONE_EXISTED 0

#define BSP_CP3BAN_DBG_FPGA_ACCESS_MSG 0
#define BSP_CP3BAN_DBG_OPTIC_MSG 0
#define BSP_CP3BAN_DBG_COMMON_MSG 0

#define BSP_CP3BAN_FPGA_LOAD_UNFINISHED 0
#define BSP_E_CP3BAN_FPGA_NOT_LOAD 0
#define BSP_E_CP3BAN_FPGA_NOT_READY 0
#define BSP_CP3BAN_SEM_EXISTED 0
#define BSP_E_CP3BAN_CREATE_SEM_FAIL 0
#define BSP_E_CP3BAN_INVALID_BOARD_TYPE 0
#define BSP_E_CP3BAN_ALLOC_MEM_FAIL 0
#define BSP_OK 0
#define BSP_E_CP3BAN_GET_SEM_FAIL 0
#define BSP_E_CP3BAN_SEM_NONE_EXISTED 0
#define BSP_E_CP3BAN_FREE_SEM_FAIL 0
#define BSP_E_CP3BAN_DESTROY_SEM_FAIL 0
#define BSP_E_CP3BAN_NULL_POINTER 0
#define BSP_CP3BAN_DBG_CPLD_ACCESS_MSG 0

#define BSP_CP3BAN_FPGA_STASUS_CHECK_NUM 200

#define BSP_CP3BAN_FPGA_STATUS_REG 0
#define BSP_CP3BAN_FPGA_ADDR_REG 0
#define BSP_CP3BAN_FPGA_CTRL_REG 0
#define BSP_CP3BAN_FPGA_DATA_READ_REG 0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef uint32 WORD32;
typedef uint32 WORD16;
typedef uint8  BYTE;
typedef char   CHAR;

typedef void BSP_CP3BAN_CHIP_BASE_ADDR;
typedef struct T_BSP_MAX3997CFG_t T_BSP_MAX3997CFG;
typedef struct T_BSP_CP3BAN_AD9557_CFG_t T_BSP_CP3BAN_AD9557_CFG;
typedef void T_BSP_OPT_INFO;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#endif /* _PUB_TYPEDEF_H_ */


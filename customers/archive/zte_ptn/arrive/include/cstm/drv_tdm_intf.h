/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_tdm_intf.h
* 文件标识: 
* 其它说明: 驱动STM TDM PWE3模块的接口部分的头文件
* 当前版本: V1.0
* 作    者: 谢伟生10112265.
* 完成日期: 2013-05-16
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/

#ifndef _DRV_TDM_INTF_H_
#define _DRV_TDM_INTF_H_

#include "drv_tdm_init.h"

#define DRV_TDM_E1_FRAME_TIMES                 ((WORD32)125)   /* 传输一帧E1所需要的时间为125us */
#define DRV_TDM_E1_SIGNALING_MASK              ((WORD32)0XFFFEFFFE)
#define DRV_TDM_MAX_CLK_DOMAIN_NUM             ((WORD32)504)   /* 最多支持504个时钟域 */
#define DRV_TDM_MAX_E1_NUM_PER_CLK_DOMAIN      ((WORD32)504)   /* 每个时钟域最多容纳504条E1链路 */
#define DRV_TDM_MIN_CLK_DOMAIN_ID              ((WORD32)0)     /* 驱动最小的domain ID */
#define DRV_TDM_MAX_CLK_DOMAIN_ID              ((WORD32)503)   /* 驱动最大的domain ID */
#define DRV_TDM_E1_CLK_SWITCH_ALM_QRY_TIMES    ((WORD32)10)    /* 查询E1链路的时钟切换告警的次数 */
#define DRV_TDM_E1_CLK_SWITCH_THRESHOLD        ((WORD32)8)     /* 时钟切换的门限值 */

typedef enum
{
    DRV_TDM_UNFRAME_E1 = 0,      /* Unframe E1 */
    DRV_TDM_PCM30_E1 = 1,        /* PCM30 E1 */
    DRV_TDM_PCM30CRC_E1 = 2,     /* PCM30CRC E1 */
    DRV_TDM_PCM31_E1 = 3,        /* PCM31 E1 */
    DRV_TDM_PCM31CRC_E1 = 4,     /* PCM31CRC E1 */
    DRV_TDM_FRAME_MAX_SIZE = 5,
} DRV_TDM_E1_FRAME_MODE;  /* E1帧的成帧模式 */

typedef enum
{
    DRV_TDM_TIMING_MODE_SYS = 0,        /* System timing mode */
    DRV_TDM_TIMING_MODE_ACR = 1,        /* ACR timing mode */
    DRV_TDM_TIMING_MODE_DCR = 2,        /* DCR timing mode */
    DRV_TDM_TIMING_MODE_LOOP = 3,       /* Loop timing mode */
    DRV_TDM_TIMING_MODE_SLAVE = 4,       /* Slave timing mode */
    DRV_TDM_TIMING_MODE_UNKNOWN = 5,    /* Unknown timing mode */
    DRV_TDM_TIMING_MAX_SIZE = 6,
} DRV_TDM_TIMING_MODE;   /* Timing mode */

typedef enum
{
    DRV_PDH_LOOPBACK_RELEASE = 0,        /* Release loopback */
    DRV_PDH_LOOPBACK_INTERNAL_PAYLOAD = 1,   /* internal loopback except E1 framer */
    DRV_PDH_LOOPBACK_EXTERNAL_PAYLOAD = 2,  /* external loopback include E1 framer */
    DRV_PDH_LOOPBACK_INTERNAL_LINE = 3,      /* internal loopback include E1 framer */
    DRV_PDH_LOOPBACK_EXTERNAL_LINE = 4,      /* external loopback except E1 framer */
    DRV_PDH_LOOPBACK_MAX_SIZE = 5,
} DRV_TDM_PDH_LOOPBACK_MODE;   /* PDH Looback Mode */

typedef enum
{
    DRV_SDH_LOOPBACK_RELEASE = 0,     /* Release loopback */
    DRV_SDH_LOOPBACK_INTERNAL = 1,    /* SDH loopback in */
    DRV_SDH_LOOPBACK_EXTERNAL = 2,    /* SDH loopback out */
    DRV_SDH_LOOPBACK_MAX_SIZE = 3,
} DRV_TDM_SDH_LOOPBACK_MODE;   /* SDH Looback Mode */

typedef enum
{
    DRV_TDM_PW_LOOPBACK_RELEASE = 0,
    DRV_TDM_PW_LOOPBACK_INTERNAL = 1,
    DRV_TDM_PW_LOOPBACK_EXTERNAL = 2,
} DRV_TDM_PW_LOOPBACK_MODE;   /* PW Looback Mode */


typedef enum
{
    DRV_TDM_ETH_INTF_RELEASE = 0,   /* release loopback */
    DRV_TDM_ETH_INTF_INTERNAL = 1,  /* 向TDM侧环回 */
    DRV_TDM_ETH_INTF_EXTERNAL = 2,  /* 向PSN侧环回 */
} DRV_TDM_ETH_INTF_LOOPBACK_MODE;  /* Ethernet interface loopback mode. */

typedef enum
{
    DRV_TDM_SGMII_SERDES_RELEASE_LB = 0,  /*Release loopback */
    DRV_TDM_SGMII_SERDES_INTERNAL_LB = 1,   /* 向TDM侧环回 */
    DRV_TDM_SGMII_SERDES_EXTERNAL_LB = 2,   /* 向PSN侧环回 */
} DRV_TDM_SGMII_SERDES_LB_MD;   /* SGMII Serdes loopback mode */

typedef enum
{
    DRV_TDM_AT_ETH_INFF_LINK_DOWN = 0,   /* Ethernet interface link down. */
    DRV_TDM_AT_ETH_INFF_LINK_UP = 1,     /* Ethernet interface link up. */
} DRV_TDM_AT_ETH_INFF_LINK_STATUS;   /* Ethernet interface link status. */

typedef enum
{
    DRV_TDM_AT_STM1_INTF_DOWN = 0,    /* STM-1 interface down */ 
    DRV_TDM_AT_STM1_INTF_UP = 1,      /* STM-1 interface up */
} DRV_TDM_AT_STM1_INTF_STATUS;    /* STM-1 interface status */

typedef enum
{
    DRV_TDM_TIMING_STATE_UNKNOWN = 0,       /* Unknown or not applicable state */
    DRV_TDM_TIMING_STATE_NOT_APPLICABLE = 1,    /* Not applicable */
    DRV_TDM_TIMING_STATE_INIT = 2,          /* Initializing state */
    DRV_TDM_TIMING_STATE_HOLDOVER = 3,      /* Holdover state */
    DRV_TDM_TIMING_STATE_LEARNING = 4,      /* Learning State */
    DRV_TDM_TIMING_STATE_LOCKED = 5,        /* Locked State */
    DRV_TDM_TIMING_STATE_INVALID = 6,
    DRV_TDM_TIMING_STATE_MAX_SIZE = 7,
} DRV_TDM_E1_TIMING_STATE;

typedef enum
{
    DRV_TDM_STM_TX_SCRAMBLE_ENABLE = 0,  /* Enable scramble on tx direction. */
    DRV_TDM_STM_TX_SCRAMBLE_DISABLE = 1,   /* Disable scramble on tx direction. */
    DRV_TDM_STM1_SCRAMBLE_MAX_SIZE = 2,
} DRV_TDM_STM_TX_SCRAMBLE_STATE;

typedef enum
{
    DRV_TDM_INTF_TX_LASER_DISABLE = 0,
    DRV_TDM_INTF_TX_LASER_ENABLE = 1,
    DRV_TDM_INTF_TX_LASER_MAX_SIZE = 2,
} DRV_TDM_STM_INTF_TX_LASER_STATE;

typedef enum
{
    DRV_TDM_STM_INTF_DISABLE = 0,     /* disable STM interface. */
    DRV_TDM_STM_INTF_ENABLE = 1,      /* enable STM interface. */
    DRV_TDM_STM_INTF_MAX_SIZE = 2,
} DRV_TDM_STM_INTF_STATE;

typedef enum
{
    DRV_TDM_STM_INTF_INTERNAL_CLK = 0,    /* 单板的本地时钟 */
    DRV_TDM_STM_INTF_RECOVERED_CLK = 1,   /* 从RX端口恢复的时钟 */
    DRV_TDM_STM_INTF_CLK_MAX_SIZE = 2, 
} DRV_TDM_STM_INTF_TX_CLK_MODE;   /* STM-1接口的TX时钟方式 */

typedef enum
{
    DRV_TDM_SGMII_SERDES_VOD = 0,
    DRV_TDM_SGMII_SERDES_PRE_TAP = 1,
    DRV_TDM_SGMII_SERDES_FIRST_TAP = 2,
    DRV_TDM_SGMII_SERDES_SECOND_TAP = 3,
} DRV_TDM_SGMII_SERDES_PARAM;

typedef enum
{
    DRV_TDM_CLK_DOMAIN_FREE = 0,    /* clock domain is free */
    DRV_TDM_CLK_DOMAIN_USED = 1,    /* clock domain is used */
    DRV_TDM_CLK_DOMAIN_MAX = 2, 
} DRV_TDM_CLK_DOMAIN_STATE;    /* the state of clock domain */

typedef enum
{
    DRV_TDM_ETH_INTF_NONE_SWITCH = 0,  /* None switch */
    DRV_TDM_ETH_INTF_SWITCH = 1,       /* switch */
    DRV_TDM_ETH_INTF_FLAG_MAX = 2,      
} DRV_TDM_ETH_INTF_SWITCH_FLAG;   /* GE接口的切换标记 */ 

typedef enum
{
    DRV_TDM_FPGA_PHY_INTF_GOOD = 0,    /* FPGA physical interface is good. */
    DRV_TDM_FPGA_PHY_INTF_BAD = 1,     /* FPGA physical interface is bad. */
    DRV_TDM_FPGA_PHY_INTF_MAX = 2, 
} DRV_TDM_FPGA_PHY_INTF_STATUS;  /* The status of FPGA physical interface */

#define DRV_TDM_CLK_SWITCH_NONE_ALARM    ((BYTE)0)
#define DRV_TDM_CLK_SWITCH_ALARM         ((BYTE)1)
#define DRV_TDM_CLK_SWITCH_ALM_MAX       ((BYTE)2)

#define DRV_TDM_E1_LINK_SLAVE         ((BYTE)0)   /* slave E1 link */
#define DRV_TDM_E1_LINK_MASTER        ((BYTE)1)   /* master E1 link */
#define DRV_TDM_E1_LINK_STATE_MAX     ((BYTE)2)

#define DRV_TDM_E1_LINK_FREE          ((BYTE)0)    /* E1 link is free */    
#define DRV_TDM_E1_LINK_USED          ((BYTE)1)    /* E1 link is used */
#define DRV_TDM_E1_LINK_STATUS_MAX    ((BYTE)2)

#define DRV_TDM_E1_LINK_UNBINDING          ((BYTE)0)
#define DRV_TDM_E1_LINK_BINDING            ((BYTE)1)
#define DRV_TDM_E1_LINK_BINDING_MAX        ((BYTE)2)

/* check clock domain ID */
#define DRV_TDM_CHECK_CLOCK_DOMAIN_ID(clkDomainId) \
    do \
    { \
        if ((clkDomainId) > DRV_TDM_MAX_CLK_DOMAIN_ID) \
        { \
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PARAM_MSG, "ERROR: %s line %d, invalid clkDomainId=%u.\n", __FILE__, __LINE__, (clkDomainId)); \
            return DRV_TDM_INVALID_CLK_DOMAIN_ID; \
        } \
    } while(0) \

typedef struct
{
    WORD32 ethIntfAlm;
} DRV_TDM_AT_DEV_ETH_INTF_ALM;   /* Ethernet interface alarm. */

typedef struct
{
    WORD32 stmIntfLbMd;                 /* the loopback mode of STM interface. */
    WORD32 txStmScrambleEn;             /* Enable/Disable scramble on TX direction. */
    WORD32 txLaserState;                 /* Enable/Disable tx laser */
    WORD32 stmIntfState;                /* Enable/Disable STM interface */
    WORD32 stmIntfTxClkMd;              /* STM interface TX clock mode */
    BYTE e1FramingMode[DRV_TDM_E1_NUM_PER_STM1 + 1];   /* E1 framing mode */
    BYTE e1TimingMode[DRV_TDM_E1_NUM_PER_STM1 + 1];    /* E1 timing mode */
    BYTE e1LinkLbMd[DRV_TDM_E1_NUM_PER_STM1 + 1];      /* the loopback mode of E1 interface. */
    BYTE e1BindingState[DRV_TDM_E1_NUM_PER_STM1 + 1]; /* the binding state of E1 link. */
} DRV_TDM_SDH_INTF_INFO;

typedef struct
{
    BYTE chipId;           /* 芯片编号 */
    BYTE portId;           /* STM端口编号 */
    BYTE au4Id;            /* AU4编号 */
    BYTE tug3Id;           /* TUG3编号 */
    BYTE tug2Id;           /* TUG2编号 */
    BYTE tu12Id;           /* TU12编号 */
    BYTE e1ClkMode;        /* 时钟模式 */
    BYTE e1LinkClkState;   /* E1链路的时钟状态,只适应于ACR/DCR时钟方式 */
    BYTE linkStatus;       /* 时钟域中E1链路的状态 */
    BYTE reserved[3];      /* 保留字节,用于字节对齐 */
} DRV_TDM_E1_LINK;   /* E1链路 */

typedef struct
{
    BYTE portId;           /* STM端口编号 */
    BYTE au4Id;            /* AU4编号 */
    BYTE tug3Id;           /* TUG3编号 */
    BYTE tug2Id;           /* TUG2编号 */
    BYTE tu12Id;           /* TU12编号 */
    BYTE reserved[3];    /* 保留字段,用于字节对齐 */
} DRV_TDM_PDH_E1_LINK;

typedef struct
{
    DRV_TDM_E1_LINK e1Link[DRV_TDM_MAX_E1_NUM_PER_CLK_DOMAIN + 1]; /* 每个时钟域最多容纳504条E1链路 */
    WORD32 clkDomainNo;       /* the NO of clock domain */
    WORD32 clkDomainState;    /* the status of clock domain */
} DRV_TDM_CLOCK_DOMAIN;    /* clock domain */

typedef struct
{
    BYTE subslotId;                        /* 单板的子槽位号,从1开始取值 */
    BYTE reserved[3];                      /* 保留字段,用于字节对齐 */
    WORD32 clkDomainNo;                    /* the NO of clock domain */
    DRV_TDM_PDH_E1_LINK newMasterE1Link;    /* new maste E1 link */
    DRV_TDM_PDH_E1_LINK oldMasterE1Link;   /* old maste E1 link */
} DRV_TDM_CLOCK_SWITCH;  /* 该结构体用来进行时钟切换 */

typedef struct
{
    BYTE e1ClkSwitchAlm[DRV_TDM_E1_NUM_PER_STM1 + 1];
} DRV_TDM_E1_CLK_SWITCH_ALM;   /* E1链路的时钟切换告警 */

typedef struct
{
    WORD32 clkDomainNo;         /* 时钟域domain NO */
    WORD32 masterChannelNO;     /* E1绑定的主PW的channel NO */
    WORD32 e1BindingPwNum;      /* E1绑定的PW数量 */
    WORD32 e1ClkState;          /* 时钟锁定状态 */
    WORD32 e1ClkMode;           /* 时钟模式 */
} DRV_TDM_E1_CLK_QRY_INFO;  /* E1链路的时钟信息查询结构体 */

/* 函数原型声明 */
extern WORD32 drv_tdm_sdhIntfMemAllocate(BYTE chipId);
extern WORD32 drv_tdm_sdhIntfMemFree(BYTE chipId);
extern WORD32 drv_tdm_sdhIntfMemGet(BYTE chipId, BYTE portId, BYTE aug1Id, DRV_TDM_SDH_INTF_INFO** ppIntf);
extern WORD32 drv_tdm_clkDomainMemAllocate(BYTE chipId);
extern WORD32 drv_tdm_clkDomainMemFree(BYTE chipId);
extern WORD32 drv_tdm_clkDomainMemGet(BYTE chipId, WORD32 clkDomainId, DRV_TDM_CLOCK_DOMAIN** ppClkDomain);
extern WORD32 drv_tdm_e1ClkSwitchAlmQuery(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, BYTE *pE1ClkState);
extern WORD32 drv_tdm_e1FramingModeGet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, BYTE *pFrmMode);
extern WORD32 drv_tdm_e1FramingModeSet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, WORD32 frmMode);
extern WORD32 drv_tdm_e1BindingStateSet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, BYTE state);
extern WORD32 drv_tdm_e1BindingStateGet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, BYTE *pState);
extern WORD32 drv_tdm_clkDomainStateGet(BYTE chipId, WORD32 clkDomainId, WORD32 *pState);
extern WORD32 drv_tdm_clkDomainIdGet(BYTE chipId, WORD32 clkDomainNo, WORD32 *pClkDomainId);
extern WORD32 drv_tdm_masterE1LinkGet(BYTE chipId, WORD32 clkDomainId, DRV_TDM_E1_LINK *pMasterE1Link);
extern WORD32 drv_tdm_e1LinkClockClear(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, WORD32 clkDomainNo);
extern WORD32 drv_tdm_clkDomainClear(BYTE chipId, WORD32 clkDomainNo);
extern WORD32 drv_tdm_e1TimingModeSave(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, BYTE timingMode);
extern WORD32 drv_tdm_systemE1TimingSet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId);
extern WORD32 drv_tdm_e1TimingModeSet(BYTE subslotId, BYTE portId, BYTE au4Id, BYTE e1LinkId, WORD32 timingMode);
extern WORD32 drv_tdm_e1TimingModeGet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, BYTE *pTimingMode);
extern WORD32 drv_tdm_e1ClkSwitchAlmFlagSet(BYTE chipId, WORD32 flag);
extern WORD32 drv_tdm_e1ClkSwitchAlmFlagGet(BYTE chipId, WORD32 *pFlag);
extern WORD32 drv_tdm_e1ClkAlmThreadIdGet(BYTE chipId, pthread_t *pThreadId);
extern WORD32 drv_tdm_e1ClkSwitchAlmTidGet(BYTE chipId, int *pTid);
extern WORD32 drv_tdm_e1ClkAlmThreadCreate(BYTE subslotId);
extern WORD32 drv_tdm_e1ClkAlmThreadDelete(BYTE subslotId);
extern WORD32 drv_tdm_e1LinkClkInfoGet(BYTE subslotId, BYTE portId, BYTE au4Id, BYTE e1LinkId, DRV_TDM_E1_CLK_QRY_INFO *pE1ClkInfo);
extern WORD32 drv_tdm_masterE1TimingStatusGet(BYTE subslotId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, WORD32 *pStatus);
extern WORD32 drv_tdm_e1TimingRefUpdate(const DRV_TDM_CLOCK_SWITCH *pClkSwitch);
extern WORD32 drv_tdm_stmIntfLbMdSet(BYTE chipId, BYTE portId, WORD32 lbMd);
extern WORD32 drv_tdm_stmIntfLbMdGet(BYTE chipId, BYTE portId, WORD32 *pLbMode);
extern WORD32 drv_tdm_e1LinkLbMdGet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, BYTE *pLbMd);
extern WORD32 drv_tdm_e1LinkLbMdSet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, WORD32 lbMd);
extern WORD32 drv_tdm_ethIntfAlmGet(BYTE chipId, BYTE ethIntfIndex, DRV_TDM_AT_DEV_ETH_INTF_ALM *pAtDevEthIntfAlm);
extern WORD32 drv_tdm_sgmiiSerdesGet(BYTE subslotId, BYTE ethIntfIndex, WORD32 phyParameterId, WORD32 *pValue);
extern WORD32 drv_tdm_sgmiiSerdesSet(BYTE subslotId, BYTE ethIntfIndex, WORD32 phyParameterId, WORD32 value);
extern WORD32 drv_tdm_txStmScrambleGet(BYTE chipId, BYTE portId, WORD32 *pScrambleEn);
extern WORD32 drv_tdm_txStmScrambleSet(BYTE subslotId, BYTE portId, WORD32 scrambleEn);
extern WORD32 drv_tdm_stmIntfTxLaserStateGet(BYTE subslotId, BYTE portId, WORD32 *pState);
extern WORD32 drv_tdm_stmIntfTxLaserStateSet(BYTE subslotId, BYTE portId, WORD32 state);
extern WORD32 drv_tdm_stmIntfStateGet(BYTE subslotId, BYTE portId, WORD32 *pState);
extern WORD32 drv_tdm_stmIntfStateSet(BYTE subslotId, BYTE portId, WORD32 state);
extern WORD32 drv_tdm_stmIntfTxClockModeGet(BYTE subslotId, BYTE portId, WORD32 *pClkMode);
extern WORD32 drv_tdm_stmIntfTxClockModeSet(BYTE subslotId, BYTE portId, WORD32 clkMode);
extern WORD32 drv_tdm_stmIntfRecoveredClkSet(BYTE subslotId, BYTE portId);
extern WORD32 drv_tdm_fpgaPhyIntfStateGet(BYTE ucSubslotId, WORD32 *pdwStatus);


#endif



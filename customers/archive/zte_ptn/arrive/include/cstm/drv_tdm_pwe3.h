/***********************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_tdm_pwe3.h
* 文件标识: 
* 其它说明: 驱动STM TDM PWE3模块的业务实现模块的头文件
* 当前版本: V1.0
* 作    者: 谢伟生10112265.
* 完成日期: 2013-03-26.
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
************************************************************/

#ifndef _DRV_TDM_PWE3_H_
#define _DRV_TDM_PWE3_H_

#include "bsp_srv_pwe3.h"
#include "drv_tdm_init.h"
#include "AtModulePw.h"

/* 常量定义 */

typedef enum
{
    DRV_TDM_PW_NUM_DECREMENT = 0,
    DRV_TDM_PW_NUM_INCREMENT = 1, 
} DRV_TDM_PW_NUM_OPERATE;

#define DRV_TDM_SATOP_CHANNEL           ((BYTE)0)   /* SATOP channel */
#define DRV_TDM_CESOP_BASIC_CHANNEL     ((BYTE)1)   /* CESOP Basic channel */
#define DRV_TDM_CESOP_CAS_CHANNEL       ((BYTE)2)   /* CESOP CAS channel */
#define DRV_TDM_UNKNOWN_CHANNEL         ((BYTE)3)   /* Unknown channel */

#define DRV_TDM_CESOP_CAS_DISABLE   ((BYTE)0)  /* disable CAS */
#define DRV_TDM_CESOP_CAS_ENABLE    ((BYTE)1)  /* enable CAS */

#define DRV_TDM_SLAVE_PW             ((BYTE)0)  /* Slave pw */
#define DRV_TDM_MASTER_PW            ((BYTE)1)  /* Maste pw */
#define DRV_TDM_PW_ATTRIBUTE_MAX     ((BYTE)2)

#define DRV_TDM_PW_CW_DISABLE    ((BYTE)0)   /* control word disable */
#define DRV_TDM_PW_CW_ENABLE     ((BYTE)1)   /* control word enable */

#define DRV_TDM_PW_CW_SEQ_DISABLE   ((BYTE)0)  /* sequence number disable */
#define DRV_TDM_PW_CW_SEQ_ENABLE    ((BYTE)1)  /* sequence number enable */

#define DRV_TDM_RTP_DISABLE     ((BYTE)0)  /* disable RTP */
#define DRV_TDM_RTP_ENABLE      ((BYTE)1)  /* enable RTP */

#define DRV_TDM_PW_FREE         ((BYTE)0)  /* PW is free */
#define DRV_TDM_PW_USED         ((BYTE)1)  /* PW is used */

#define DRV_TDM_ENCAP_TYPE_TDM_PACKET    ((BYTE)0x0A)   /* TDM报文类型 */

#define DRV_TDM_E1_RATE                  ((WORD32)256)  /* E1 rate, in packets/ms */
#define DRV_TDM_DS0_RATE                 ((WORD32)8)    /* DS0 rate, in packets/ms */

#define DRV_TDM_MAX_PW_NUM_PER_STM1_CARD    (1024)  /* 每块STM-1单板支持的最大PW数量 */
#define DRV_TDM_PW_ID_INVALID  DRV_TDM_MAX_PW_NUM_PER_STM1_CARD     /* 无效的PWID */
#define DRV_TDM_PW_ID_MIN      ((WORD16)0)     /* 最小的PWID */
#define DRV_TDM_PW_ID_MAX      ((WORD16)1023)   /* 最大的PWID */
#define AT_DEV_LOFS_SET_THRESHOLD      ((BYTE)30)  /* ARRIVE's LOFS set threshold */
#define AT_DEV_LOFS_CLEAR_THRESHOLD    ((BYTE)30)  /* ARRIVE's LOFS clear threshold */
#define AT_DEV_ETHERNET_PRIORITY       ((BYTE)1)  /* ARRIVE's priority on direction to Ethernet*/
#define DRV_TDM_PW_CW_MBIT_VALUE0    ((BYTE)0x0) /* the Mbit's value of PW control word */
#define DRV_TDM_PW_CW_MBIT_VALUE2    ((BYTE)0x2) /* the Mbit's value of PW control word */

#define DRV_TDM_PW_CNT_TASK_NAME_SIZE    40  /* PW counter任务的任务名大小 */
#define DRV_TDM_PW_CNT_TASK_PRIORITY     ((WORD16)75)  /* PW counter进程的优先级 */
#define DRV_TDM_PW_CNT_TASK_STACK_SIZE   ((WORD32)(200*1024))  /* PW counter进程的stack大小 */

#define DRV_TDM_PW_ALM_TASK_NAME_SIZE    40  /* PW alarm任务的任务名大小 */
#define DRV_TDM_PW_ALM_TASK_PRIORITY     ((WORD16)75)  /* PW alarm进程的优先级 */
#define DRV_TDM_PW_ALM_TASK_STACK_SIZE   ((WORD32)(200*1024))  /* PW alarm进程的stack大小 */

#define DRV_TDM_PW_PAYLOAD_SIZE_MIN      ((WORD16)8)
#define DRV_TDM_PW_PAYLOAD_SIZE_MAX      ((WORD16)1500)

#define DRV_TDM_MIN_PKT_NUM_IN_BUF       ((WORD32)3)
#define DRV_TDM_MAX_PKT_NUM_IN_BUF       ((WORD32)2048)
#define DRV_TDM_MAX_JITTER_BUF_SIZE      ((WORD32)512000)  /* max jitter buffer size */

#define DRV_TDM_PW_VLAN_PRIORITY_MIN      ((BYTE)0)
#define DRV_TDM_PW_VLAN_PRIORITY_MAX      ((BYTE)7)

#define DRV_TDM_CES_ENCAP_TYPE      ((BYTE)0xa)
#define DRV_TDM_1000US_PER_1MS      ((WORD32)1000)

/* Ethernet接口最多能承载的PW的数量 */
#define DRV_TDM_ETH_INTF_MAX_PW_NUM    ((WORD32)400)

/* 检查PW的编号 */
#define DRV_TDM_CHECK_PW_ID(pwId) \
    do \
    { \
        if ((pwId) >= DRV_TDM_MAX_PW_NUM_PER_STM1_CARD) \
        { \
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PARAM_MSG, "ERROR: %s line %d, invalid pwId=%u.\n", __FILE__, __LINE__, (pwId)); \
            return DRV_TDM_INVALID_PW_ID; \
        } \
    } while(0) \

#define DRV_TDM_CHECK_PW_PAYLOAD_SIZE(payloadSize) \
    do \
    { \
        if (((payloadSize) < DRV_TDM_PW_PAYLOAD_SIZE_MIN) || ((payloadSize) > DRV_TDM_PW_PAYLOAD_SIZE_MAX)) \
        { \
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PARAM_MSG, "ERROR: %s line %d, invalid payloadSize=%u.\n", __FILE__, __LINE__, (payloadSize)); \
            return DRV_TDM_INVALID_PW_PAYLOAD_SIZE; \
        } \
    } while(0) \

/* check vlan priority of PW */
#define DRV_TDM_CHECK_PW_VLAN_PRIORITY(priority) \
    do \
    { \
        if ((priority) > DRV_TDM_PW_VLAN_PRIORITY_MAX) \
        { \
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PARAM_MSG, "ERROR: %s line %d, invalid priority=%u.\n", __FILE__, __LINE__, (priority)); \
            return DRV_TDM_INVALID_PW_VLAN_PRI; \
        } \
    } while(0) \
 
/* 数据结构定义 */

typedef struct
{
    BYTE subslotId;      /* 子卡的子槽位号 */
    BYTE chipId;         /* 芯片编号,从0开始取值 */
    BYTE portId;         /* STM端口的编号 */
    BYTE au4Id;          /* AU4编号 */
    BYTE tug3Id;         /* TUG3编号 */
    BYTE tug2Id;         /* TUG2编号 */
    BYTE tu12Id;         /* TU12编号 */
    BYTE e1LinkId;       /* VC4中的E1链路编号,取值为1~63 */
    BYTE e1TsNum;        /* E1帧的时隙数 */
    BYTE e1FramingMode;    /* E1帧类型 */
    BYTE e1LinkClkState;  /* the clock state of E1 link */
    BYTE reserved[1];      /* 保留字节,用于内存对齐 */
    WORD32 e1TimingMode;   /* E1的时钟模式 */
    WORD32 clkDomainNo;    /* the NO of clock domain,接口卡唯一 */
    WORD32 timeslotBmp;    /* timeslot bitmap */
    WORD32 channelNo;      /* channelNo, 接口卡唯一 */
} DRV_TDM_PW_AC_INFO;    /* PW attachment circuit information */

typedef struct
{
    WORD16 vlan1Id;
    BYTE priority;         
    BYTE cfi;              
} DRV_TDM_ETH_VLAN1_TAG;   /* VLAN1 tag, 即S-VLAN. */

typedef struct
{
    WORD16 vlan2Id;
    BYTE  priority;         
    BYTE  cfi;              
} DRV_TDM_ETH_VLAN2_TAG;      /* VLAN2 tag, 即C-VLAN. */

typedef struct
{
    WORD32 ssrc;    /* SSRC field */
} DRV_TDM_RTP_CFG_INFO;    /* RTP configuration information */

typedef struct
{
    AtPw pAtPw;                   /* ARRIVE芯片的PW指针 */
    DRV_TDM_PW_AC_INFO acInfo;    /* PW attachment circuit information */
    DRV_TDM_ETH_VLAN1_TAG vlan1Tag;  /* vlan1 tag, that is S-VLAN. */
    DRV_TDM_ETH_VLAN2_TAG vlan2Tag;  /* vlan2 tag, that is C-VLAN. */
    DRV_TDM_RTP_CFG_INFO rtpCfg;    /* RTP configuration information */
    WORD32 pktNumInBuf;         /* packet number in jitter buffer */
    WORD32 jitterBufSize; /* jitter buffer size in microseconds, in multiple of 125us */
    WORD32 jitterDelay;   /* number of Packets in jitter buffer */
    WORD16 pldSize;    /* 报文的净荷大小,不包括4字节的控制字. payload size. */
    WORD16 encapNum;      /* 封包级联数 */
    BYTE dstMacAddr[DRV_TDM_MAC_ADDR_SIZE];  /* destination MAC address */
    BYTE ethIntfIndex;    /* ARRIVE芯片的Ethernet接口的索引,取值为0~1. */
    BYTE rtpEnable;       /* 是否支持RTP */
    BYTE cwEnable;        /* control word是否使能 */
    BYTE seqEnable;       /* control word sequence number是否使能 */
    BYTE channelMode;    /* channel mode */
    BYTE pwAttribute;    /* 对于CESoPSN PW才有意义,成帧E1链路下的PW属性,即为MASTER或SLAVE */
    BYTE pwState;    /* pw state */
    BYTE ethIntfSwitchFlag;  /* the flag of ethernet interface switching */
    BYTE reserved[2];  /* 保留字节,用于字节对齐 */
} DRV_TDM_PW_CFG_INFO;  /* PW configuration information */

typedef struct
{
    WORD32 txPkts;            /* number of transmitted packets */
    WORD32 txPayloadBytes;   /* number of transmitted bytes of PW payload, including PW 
                                Control Word, RTP but not include PSN header */
    WORD32 rxPkts;            /* number of received packets */
    WORD32 rxPayloadBytes;    /* number of received bytes of PW payload, including PW 
                                Control Word, RTP but not include PSN header */
    WORD32 rxDiscardedPkts;   /* The number of discarded packets, 
                                 which are packet types configured to be discarded. Same with OutOfSeqDropedPackets.*/
    WORD32 rxMalformedPkts;   /* The number of Rx Malformed packets  */
    WORD32 rxReorderedPkts;  /* The number of re-ordered packets */
    WORD32 rxLostPkts;       /* The number of Lost packets */
    WORD32 rxOutofSeqDropPkts;  /* The number of received packets are out of sequence, 
                                   but can not be re-ordered. */
    WORD32 rxOamPkts;   /* The number of Rx PW OAM packets such VCCV packets */
    WORD32 txLbitPkts;        /* number of transmitted L-Bit packets */
    WORD32 txRbitPkts;        /* number of transmitted R-bit packets */
    WORD32 rxLbitPkts;        /* number of received L-bit packets */
    WORD32 rxRbitPkts;        /* number of received R-bit packets */
    WORD32 rxJitBufOverrunPkts; /* The number of jitter buffer overrun */
    WORD32 rxJitBufUnderrunPkts; /* The number of jitter buffer underrun */
    WORD32 rxLops;    /* number of transitions from normal to lost of packet 
                         synchronization condition */
    WORD32 rxPktsSentToTdm; /* The number of packets send to SONET/SDH/TDM side, 
                               includes data packets, replacing packets of L/R 
                               bits packets, and replacing packets of LOST packets. */
    WORD32 txMbitPkts;        /* number of transmitted M-bit packets */
    WORD32 rxMbitPkts;        /* number of received M-bit packets */   
} DRV_TDM_PW_CNT_INFO;    /* PW packet statistics structure */

typedef struct
{
    BYTE lopsAlm;               /* Loss of Packet Alarm in RX direction */
    BYTE jitterBufOverrunAlm;   /* Jitter buffer overrun alarm in RX direction*/
    BYTE jitterBufUnderrunAlm;  /* Jitter buffer underrun alarm in RX direction */
    BYTE lBitAlm;               /* L bit alarm in RX direction */
    BYTE rBitAlm;               /* R bit alarm in RX direction */
    BYTE mBitAlm;               /* M bit alarm in RX direction */
    BYTE reserved[2];           /* 保留字节,以便字节对齐 */
} DRV_TDM_PW_ALM_INFO;  /* PW alarm structure */


/* 函数原型声明 */
extern WORD32 drv_tdm_pwMemAllocate(BYTE chipId);
extern WORD32 drv_tdm_pwMemFree(BYTE chipId);
extern WORD32 drv_tdm_pwCntDebugFlagSet(BYTE chipId, WORD16 pwId, WORD32 flag);
extern WORD32 drv_tdm_pwCntDebugFlagGet(BYTE chipId, WORD16 pwId, WORD32 *pFlag);
extern WORD32 drv_tdm_pwAlmDebugFlagSet(BYTE chipId, WORD16 pwId, WORD32 flag);
extern WORD32 drv_tdm_pwAlmDebugFlagGet(BYTE chipId, WORD16 pwId, WORD32 *pFlag);
extern WORD32 drv_tdm_pwMemGet(BYTE chipId, WORD16 pwId, DRV_TDM_PW_CFG_INFO **ppPwInfo);
extern WORD32 drv_tdm_pwPktCntMemGet(BYTE chipId, WORD16 pwId, DRV_TDM_PW_CNT_INFO **ppPktCnt);
extern WORD32 drv_tdm_pwTotalPktCntMemGet(BYTE chipId, WORD16 pwId, DRV_TDM_PW_CNT_INFO **ppPktCnt);
extern WORD32 drv_tdm_pwAlarmMemGet(BYTE chipId, WORD16 pwId, DRV_TDM_PW_ALM_INFO **ppPwAlm);
extern WORD32 drv_tdm_ethIntfPwNumGet(BYTE chipId, BYTE ethIntfId, WORD32 *pPwNum);
extern WORD32 drv_tdm_ethIntfPwNumSet(BYTE chipId, BYTE ethIntfId, WORD32 operateType);
extern WORD32 drv_tdm_ethIntfPwNumClear(BYTE chipId);
extern WORD32 drv_tdm_e1BindingPwIdGet(BYTE subslotId, BYTE portId, BYTE au4Id, BYTE e1LinkId, WORD16 *pPwId);
extern WORD32 drv_tdm_acBindingPwIdGet(BYTE subslotId, 
                                       BYTE portId, 
                                       BYTE au4Id, 
                                       BYTE e1LinkId, 
                                       WORD32 timeslotBmp, 
                                       WORD16 *pPwId);
extern WORD32 drv_tdm_pwAttributeSet(BYTE chipId, WORD16 pwId, BYTE attribute);
extern WORD32 drv_tdm_pwAttributeGet(BYTE chipId, WORD16 pwId, BYTE *pAttribute);
extern WORD32 drv_tdm_pwStateGet(BYTE chipId, WORD16 pwId, BYTE *state);
extern WORD32 drv_tdm_pwE1ClkStateGet(BYTE chipId, WORD16 pwId, BYTE *pClkState);
extern WORD32 drv_tdm_pwE1ClkStateSet(BYTE chipId, BYTE portId, BYTE au4Id, BYTE e1LinkId, BYTE e1ClkState);
extern WORD32 drv_tdm_pwE1TimingModeSet(BYTE chipId, BYTE portId, BYTE au4Id, BYTE e1LinkId, BYTE e1TimingMode);
extern WORD32 drv_tdm_pwCreate(const T_BSP_SRV_FUNC_PW_BIND_ARG *pPwArg);
extern WORD32 drv_tdm_pwRemove(const T_BSP_SRV_FUNC_PW_UNBIND_ARG *pUnbindPwArg);
extern WORD32 drv_tdm_e1BindingPwNumGet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, WORD32 *pPwNum);
extern WORD32 drv_tdm_e1BindingPwNumSet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, WORD32 operateType);
extern WORD32 drv_tdm_e1BindingPwNumClear(BYTE chipId);
extern WORD32 drv_tdm_stmIntfPwNumGet(BYTE chipId, BYTE portId, WORD32 *pdwPwNum);
extern WORD32 drv_tdm_stmIntfPwNumSet(BYTE chipId, BYTE portId, WORD32 operateType);
extern WORD32 drv_tdm_stmIntfPwNumClear(BYTE chipId);
extern WORD32 drv_tdm_pwJitterBufSizeUpdate(const T_BSP_SRV_FUNC_PW_BIND_ARG *pPwArg);
extern WORD32 drv_tdm_pwPldSizeUpdate(const T_BSP_SRV_FUNC_PW_BIND_ARG *pPwArg);
extern WORD32 drv_tdm_pwVlan1PriUpdate(const T_BSP_SRV_FUNC_PW_BIND_ARG *pPwArg);
extern WORD32 drv_tdm_atPwJitBufSizeGet(BYTE ucChipId, WORD16 wPwId, WORD32 *pdwJitBufSize);
extern WORD32 drv_tdm_atPwJitBufDelayGet(BYTE ucChipId, WORD16 wPwId, WORD32 *pdwJitBufDelay);
extern WORD32 drv_tdm_pwUpdate(BYTE chipId, BYTE ethIntfId);
extern WORD32 drv_tdm_pwRestore(BYTE chipId, BYTE ethIntfId);
extern WORD32 drv_tdm_pwCntGet(BYTE chipId, WORD16 pwId, DRV_TDM_PW_CNT_INFO *pPwCntInfo);
extern WORD32 drv_tdm_pwAlarmGet(BYTE chipId, WORD16 pwId, DRV_TDM_PW_ALM_INFO *pPwAlm);
extern WORD32 drv_tdm_pwCntQuery(T_BSP_SRV_FUNC_CHIP_CESSTATIC_ARG *pPwe3Cnt);
extern WORD32 drv_tdm_pwAlarmQuery(T_BSP_SRV_FUNC_CHIP_CESALARM_ARG *pPwe3Alarm);

#endif


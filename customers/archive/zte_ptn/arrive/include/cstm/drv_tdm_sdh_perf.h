/***********************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_tdm_sdh_perf.h
* 文件标识: 
* 其它说明: 驱动STM TDM PWE3模块的STM接口的SDH性能统计处理头文件
* 当前版本: V1.0
* 作    者: 谢伟生10112265.
* 完成日期: 2013-03-26.
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
************************************************************/

#ifndef _DRV_TDM_SDH_PERF_H_
#define _DRV_TDM_SDH_PERF_H_

#include "drv_tdm_init.h"

typedef enum
{
    DRV_TDM_BER_RATE_UNKNOWN = 0, /* Unknown bit error rate */
    DRV_TDM_BER_RATE_1E3 = 1,     /* Bit error rate is 10-3 */
    DRV_TDM_BER_RATE_1E4 = 2,     /* Bit error rate is 10-4 */
    DRV_TDM_BER_RATE_1E5 = 3,     /* Bit error rate is 10-5 */
    DRV_TDM_BER_RATE_1E6 = 4,     /* Bit error rate is 10-6 */
    DRV_TDM_BER_RATE_1E7 = 5,     /* Bit error rate is 10-7 */
    DRV_TDM_BER_RATE_1E8 = 6,     /* Bit error rate is 10-8 */
    DRV_TDM_BER_RATE_1E9 = 7,     /* Bit error rate is 10-9 */
    DRV_TDM_BER_RATE_1E10 = 8,    /* Bit error rate is 10-10 */
    DRV_TDM_BER_RATE_MAX_SIZE = 9, 
} DRV_TDM_BER_RATE;    /* Bit error rate for generating SD/SF alarm */

typedef enum
{
    DRV_TDM_SD = 0,        /* SD */
    DRV_TDM_SF = 1,        /* SF */
} DRV_TDM_SD_SF_TYPE;  /* SDSF类型 */

typedef struct
{
    WORD32 b1Cnts;     /* B1 performance staticstis counters */
    WORD32 b2Cnts;     /* B2 performance staticstis counters */
    WORD32 msReiCnts;    /* B2 REI performance staticstis counters */
} DRV_TDM_SECTION_PERF;  /* Section performance staticstis counters */

typedef struct
{
    WORD32 b3Cnts;    /* B3 performance staticstis counters */
    WORD32 hpReiCnts; /* B3 REI performance staticstis counters */
    WORD32 rxAu4Ppjc; /* Detected AU4 Positive Pointer Justification Count */
    WORD32 rxAu4Npjc; /* Detected AU4 Negative Pointer Justification Count */
    WORD32 txAu4Ppjc; /* Generated AU4 Positive Pointer Justification Count */
    WORD32 txAu4Npjc; /* Generated AU4 Negative Pointer Justification Count */
} DRV_TDM_VC4_PERF;  /* High path performance staticstis counters */

typedef struct
{
    WORD32 bipCnts;    /* V5 performance staticstis counters */
    WORD32 lpReiCnts; /* V5 REI performance staticstis counters */
    WORD32 rxTu12Ppjc; /* Detected TU12 Positive Pointer Justification Count */
    WORD32 rxTu12Npjc; /* Detected TU12 Negative Pointer Justification Count */
    WORD32 txTu12Ppjc; /* Generated TU12 Positive Pointer Justification Count */
    WORD32 txTu12Npjc; /* Generated TU12 Negative Pointer Justification Count */
} DRV_TDM_VC12_PERF;  /* Low path performance staticstis counters */

typedef struct
{
    WORD32 e1CrcErrCnts;     /* CRC error counter.只有PCM30CRC和PCM31CRC才会有这个特性.根据G.704标准,第0时隙的bit1来表示CRC错误 */
    WORD32 e1FrmBitErrCnts;  /* Framing Bit Error counter */
    WORD32 e1ReiCnts;        /* REI counter */
} DRV_TDM_E1_PERF;  /* E1 link performance staticstis counters. */

typedef struct
{
    DRV_TDM_SECTION_PERF sectionPerf;   /* Section performance staticstis counters */
    DRV_TDM_VC4_PERF vc4Perf;   /* High path performance staticstis counters */
    DRV_TDM_VC12_PERF vc12Perf[DRV_TDM_E1_NUM_PER_STM1 + 1]; /* Low path performance staticstis counters */
    DRV_TDM_E1_PERF e1Perf[DRV_TDM_E1_NUM_PER_STM1 + 1];  /* E1 link performance staticstis counters. */
} DRV_TDM_SDH_PERF;


/* 函数原型声明 */
extern WORD32 drv_tdm_sdhPerfMemAllocate(BYTE chipId);
extern WORD32 drv_tdm_sdhPerfMemFree(BYTE chipId);
extern WORD32 drv_tdm_sdhPerfMemGet(BYTE chipId, BYTE portId, BYTE aug1Id, DRV_TDM_SDH_PERF** ppPerf);
extern WORD32 drv_tdm_sdhTotalPerfMemGet(BYTE chipId, BYTE portId, BYTE aug1Id, DRV_TDM_SDH_PERF** ppPerf);
extern WORD32 drv_tdm_secPerfCntGet(BYTE chipId, BYTE portId, DRV_TDM_SECTION_PERF *pSecPerf);
extern WORD32 drv_tdm_secPerfCntQuery(BYTE chipId, BYTE portId, DRV_TDM_SECTION_PERF *pSecPerf);
extern WORD32 drv_tdm_hpPerfCntGet(BYTE chipId, BYTE portId, BYTE aug1Id, DRV_TDM_VC4_PERF *pVc4Perf);
extern WORD32 drv_tdm_hpPerfCntQuery(BYTE chipId, BYTE portId, BYTE aug1Id, DRV_TDM_VC4_PERF *pVc4Perf);
extern WORD32 drv_tdm_lpPerfCntGet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, DRV_TDM_VC12_PERF *pVc12Perf);
extern WORD32 drv_tdm_lpPerfCntQuery(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, DRV_TDM_VC12_PERF *pVc12Perf);
extern WORD32 drv_tdm_e1PerfCntGet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, DRV_TDM_E1_PERF *pE1Perf);
extern WORD32 drv_tdm_e1PerfCntQuery(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, DRV_TDM_E1_PERF *pE1Perf);
extern WORD32 drv_tdm_txB1ErrForce(BYTE chipId, BYTE portId);
extern WORD32 drv_tdm_txB1ErrUnForce(BYTE chipId, BYTE portId);
extern WORD32 drv_tdm_txB2ErrForce(BYTE chipId, BYTE portId);
extern WORD32 drv_tdm_txB2ErrUnForce(BYTE chipId, BYTE portId);
extern WORD32 drv_tdm_txB3ErrForce(BYTE chipId, BYTE portId, BYTE aug1Id);
extern WORD32 drv_tdm_txB3ErrUnForce(BYTE chipId, BYTE portId, BYTE aug1Id);
extern WORD32 drv_tdm_txLpBipvErrForce(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId);
extern WORD32 drv_tdm_txLpBipvErrUnForce(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId);
extern WORD32 drv_tdm_rsSdSfThresholdSet(BYTE chipId, BYTE portId, WORD32 sdsfType, WORD32 threshold);
extern WORD32 drv_tdm_rsSdSfThresholdGet(BYTE chipId, BYTE portId, WORD32 sdsfType, WORD32 *pThreshold);
extern WORD32 drv_tdm_rsCurrentBerRateGet(BYTE chipId, BYTE portId, WORD32 *pBerRate);
extern WORD32 drv_tdm_msSdSfThresholdSet(BYTE chipId, BYTE portId, WORD32 sdsfType, WORD32 threshold);
extern WORD32 drv_tdm_msSdSfThresholdGet(BYTE chipId, BYTE portId, WORD32 sdsfType, WORD32 *pThreshold);
extern WORD32 drv_tdm_msCurrentBerRateGet(BYTE chipId, BYTE portId, WORD32 *pBerRate);
extern WORD32 drv_tdm_vc4SdSfThresholdSet(BYTE chipId, BYTE portId, BYTE aug1Id, WORD32 sdsfType, WORD32 threshold);
extern WORD32 drv_tdm_vc4SdSfThresholdGet(BYTE chipId, BYTE portId, BYTE aug1Id, WORD32 sdsfType, WORD32 *pThreshold);
extern WORD32 drv_tdm_vc4CurrentBerRateGet(BYTE chipId, BYTE portId, BYTE aug1Id, WORD32 *pBerRate);
extern WORD32 drv_tdm_vc12SdSfThresholdSet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, WORD32 sdsfType, WORD32 threshold);
extern WORD32 drv_tdm_vc12SdSfThresholdGet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, WORD32 sdsfType, WORD32 *pThreshold);
extern WORD32 drv_tdm_vc12CurrentBerRateGet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, WORD32 *pBerRate);
extern WORD32 drv_tdm_sdhBerFlagSet(BYTE chipId, WORD32 flag);
extern WORD32 drv_tdm_sdhBerFlagGet(BYTE chipId, WORD32 *pFlag);
extern WORD32 drv_tdm_sdhBerThreadCreate(BYTE subslotId);
extern WORD32 drv_tdm_sdhBerThreadDelete(BYTE subslotId);
extern WORD32 drv_tdm_sdhBerThreadIdGet(BYTE chipId, pthread_t *pThreadId);
extern WORD32 drv_tdm_sdhBerTidGet(BYTE chipId, int *pTid);

#endif


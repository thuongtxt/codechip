/***********************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_tdm_init.h
* 文件标识: 
* 其它说明: 驱动STM TDM PWE3模块的初始化头文件
* 当前版本: V1.0
* 作    者: 谢伟生10112265.
* 完成日期: 2013-03-26.
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
************************************************************/

#ifndef _DRV_TDM_INIT_H_
#define _DRV_TDM_INIT_H_

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "bsp.h"
#include "bsp_mml.h"
#include "bsp_oal_api.h"
#include "Pub_TypeDef.h"
#include "bsp_cp3ban_ctrl.h"
#include "bsp_cp3ban_common.h"
#include "eipmod.h"
#include "services.h"
#include "modthread.h"
#include "AtClasses.h"
#include "AtHal.h"
#include "AtModuleSdh.h"
#include "AtModulePdh.h"
#include "AtModuleEth.h"


typedef enum
{
    DRV_TDM_OK = 0,
    DRV_TDM_ERROR = 0x2001,
    DRV_TDM_INVALID_CHIP_ID = 0x2002,
    DRV_TDM_INVALID_ARGUMENT = 0x2003,
    DRV_TDM_NULL_POINTER = 0x2004,
    DRV_TDM_INVALID_PORT_ID = 0x2005,
    DRV_TDM_INVALID_CHIP_STM1_INTF = 0x2006,
    DRV_TDM_INVALID_CHIP_ETH_INTF = 0x2007,
    DRV_TDM_INVALID_SUBSLOT_ID = 0x2008,
    DRV_TDM_INVALID_E1_LINK_ID = 0x2009,
    DRV_TDM_INVALID_AU4_ID = 0x200a,
    DRV_TDM_INVALID_TUG3_ID = 0x200b,
    DRV_TDM_INVALID_TUG2_ID = 0x200c,
    DRV_TDM_INVALID_TU12_ID = 0x200d,
    DRV_TDM_INVALID_PW_ID = 0x200e,
    DRV_TDM_OVERLAP_PW_CFG = 0x200f,  /* 重叠的PW配置 */
    DRV_TDM_OVERLAP_VLANID_CFG = 0x2010,  /* 重叠的VLANID */
    DRV_TDM_PW_EXISTED = 0x2011,      /* PW已经存在 */
    DRV_TDM_PW_IS_NOT_EXISTED = 0x2012,  /* PW不存在 */
    DRV_TDM_MEM_ALLOC_FAIL = 0x2013,
    DRV_TDM_MEM_FREE_FAIL = 0x2014,
    DRV_TDM_STM_BOARD_UNINIT = 0x2015,   /* 单板没有初始化 */
    DRV_TDM_INVALID_PW_PAYLOAD_SIZE = 0x2016, 
    DRV_TDM_SDK_API_FAIL = 0x2017, 
    DRV_TDM_CREATE_THREAD_FAIL = 0x2018, 
    DRV_TDM_INVALID_LINK_ID = 0x2019, 
    DRV_TDM_E1_PW_TYPE_UNMATCH = 0x201a, /* E1类型和PW类型的不一致 */
    DRV_TDM_INVALID_PW_TYPE = 0x201b, 
    DRV_TDM_AT_INVALID_PRODUCT_CODE = 0x201c, /* invalid product code of FPGA. */
    DRV_TDM_E1_TIMESLOT_INVALID = 0x201d,     /* invalid E1 timeslot bitmap */
    DRV_TDM_E1_FRAME_MODE_FORBID = 0x201e, 
    DRV_TDM_INVALID_CLK_DOMAIN_ID = 0x201f, /* invalid clock domain id */
    DRV_TDM_CLK_DOMAIN_IS_NOT_EXISTED = 0x2020, /* clok domain is not existed. */
    DRV_TDM_MASTER_E1_IS_NOT_EXISTED = 0x2021, 
    DRV_TDM_CLK_DOMAIN_IS_EXISTED = 0x2022, /* clok domain is existed. */
    DRV_TDM_MASTER_E1_IS_EXISTED = 0x2023,  /* master E1 link is existed */
    DRV_TDM_E1_TIMING_MODE_UNMATCH = 0x2024, /* e1 timing mode unmatch in one clock domain */
    DRV_TDM_INVALID_MASTER_E1_LINK = 0x2025, 
    DRV_TDM_INVALID_PW_JIT_BUF_SIZE = 0x2026, /* invalid pw jitter buffer size */
    DRV_TDM_E1_IS_NOT_BINDED = 0x2027,       /* E1 link is not binding with PW */
    DRV_TDM_AC_IS_NOT_BINDED = 0x2028,       /* Attachment circuit is not binding with PW */
    DRV_TDM_INVALID_PW_VLAN_PRI = 0x2029, /* Invalid vlan priority */
    DRV_TDM_FPGA_RAM_ADDR_BUS_FAIL = 0x202a,  /* RAM address bus fail of FPGA */
    DRV_TDM_FPGA_RAM_DATA_BUS_FAIL = 0x202b,  /* RAM data bus fail of FPGA */
    DRV_TDM_FPGA_RAM_MEMORY_FAIL = 0x202c,    /* RAM memory fail of FPGA. */
    DRV_TDM_ERR_VLAN2_PORT_ID = 0x202d, /* Invalid port id in vlan2 tag */
    DRV_TDM_ERR_VLAN1_ENCAP_TYPE = 0x202e, /* invalid encapsulation in vlan1 tag */
    DRV_TDM_ERR_CPU_PKT_INDICATOR = 0x202f,  /* invalid cpu pkt indicator in vlan1 tag. */
    DRV_TDM_ERR_MLPPP_IMA_FLAG = 0x2030,  /* invalid mlppp ima flag in vlan2 tag. */
    DRV_TDM_ERR_PKT_LENGTH = 0x2031, 
    DRV_TDM_INVALID_PW_STATE = 0x2032, /* invalid pw state. */
    DRV_TDM_INVALID_BOARD_TYPE = 0x2033,  /* invalid board type. */
    DRV_TDM_INVALID_AUG1_ID = 0x2034, /* invalid AUG-1 ID */
    DRV_TDM_INVALID_AUG1_INDEX = 0x2035, /* invalid AUG-1 index */
    DRV_TDM_INVALID_STM_INTF_INDEX = 0x2036,  /* invalid STM interface index */
    DRV_TDM_INVALID_STM_INTF_MODE = 0x2037,   /* invalid STM interface mode */
} DRV_TDM_RETURN_CODE;  /* 函数的返回码 */

typedef enum
{
    DRV_TDM_FALSE = 0,    /* 逻辑假 */
    DRV_TDM_TRUE = 1,     /* 逻辑真 */
} DRV_TDM_BOOL;

typedef enum
{
    DRV_TDM_INVALID_STM_INTF = 0,   /* invalid STM interface */
    DRV_TDM_STM1_INTF = 1,          /* STM-1 interface */
    DRV_TDM_STM4_INTF = 2,          /* STM-4 interface */
} DRV_TDM_STM_INTF_MODE;  /* STM interface mode*/

#define DRV_TDM_DISABLE        ((WORD32)0)
#define DRV_TDM_ENABLE         ((WORD32)1)

#define DRV_TDM_LOW_32BITS_IN_WORD64    ((WORD32)32)  /* 64位整数中的低32位 */
#define DRV_TDM_LOW_16BITS_IN_WORD64    ((WORD32)16)  /* 64位整数中的低16位 */

#define DRV_CP3BAN_BOARD_ID   ((WORD16)0x8a10)  /* CP3BAN单板的BOARD ID */

#define DRV_TDM_STM1_CARD_OFFLINE     ((BYTE)0)    /* STM-1单板离线 */
#define DRV_TDM_STM1_CARD_ONLINE      ((BYTE)1)    /* STM-1单板在线 */

#define DRV_TDM_STM_BOARD_UNINSTALL  ((WORD32)0)  /* STM单板还未初始化 */
#define DRV_TDM_STM_BOARD_INSTALLED  ((WORD32)1)  /* STM单板初始化成功 */

#define DRV_TDM_MAX_BYTE          ((BYTE)0xFF)            /* 最大的8位无符号整数 */
#define DRV_TDM_MAX_WORD16        ((WORD16)0xFFFF)          /* 最大的16位无符号整数 */
#define DRV_TDM_MAX_WORD32        ((WORD32)0xFFFFFFFF)      /* 最大的32位无符号整数 */

#define DRV_TDM_MAC_ADDR_SIZE    ((WORD32)6)           /* MAC地址大小 */

/* TDM PWE3模块的调试信息级别定义宏 */
#define DRV_TDM_DBG_PARAM_MSG         ((WORD32)0x00000001)  /* 调试上层参数 */
#define DRV_TDM_DBG_PW_CFG_MSG        ((WORD32)0x00000002)  /* 调试PW配置 */
#define DRV_TDM_DBG_OH_CFG_MSG        ((WORD32)0x00000004)  /* 调试SDH开销设置 */
#define DRV_TDM_DBG_OH_QUERY_MSG      ((WORD32)0x00000008)  /* 调试SDH开销查询 */
#define DRV_TDM_DBG_SDH_ALM_QRY       ((WORD32)0x00000010)  /* 调试SDH告警查询 */
#define DRV_TDM_DBG_SDH_ALM_CFG       ((WORD32)0x00000020)  /* 调试SDH告警设置 */
#define DRV_TDM_DBG_SDH_PERF_QRY      ((WORD32)0x00000040)  /* 调试SDH性能统计查询 */
#define DRV_TDM_DBG_SDH_PERF_CFG      ((WORD32)0x00000080)  /* 调试SDH性能插入 */
#define DRV_TDM_INTF_LOOPBACK_DIAG    ((WORD32)0x00000100)  /* 调试接口环回操作 */
#define DRV_TDM_PKT_CNT_DIAG          ((WORD32)0x00000200)  /* 调试报文统计操作 */
#define DRV_TDM_DBG_SDH_ERR_QRY       ((WORD32)0x00000400)  /* 调试SDH误码查询 */
#define DRV_TDM_DBG_SDH_ERR_CFG       ((WORD32)0x00000800)  /* 调试SDH误码插入 */
#define DRV_TDM_DBG_PW_CNT_MSG        ((WORD32)0x00001000)  /* 调试PW报文统计 */
#define DRV_TDM_DBG_PW_ALM_MSG        ((WORD32)0x00002000)  /* 调试PW告警 */
#define DRV_TDM_DBG_PW_CW_INS         ((WORD32)0x00004000)  /* 调试PW控制字插入 */
#define DRV_TDM_DBG_INTF_CFG_MSG      ((WORD32)0x00008000)  /* 调试接口  */
#define DRV_TDM_DBG_ACCESS_CPLD       ((WORD32)0x00010000)  /* 调试访问CPLD */
#define DRV_TDM_DBG_ACCESS_FPGA       ((WORD32)0x00020000)  /* 调试访问FPGA */
#define DRV_TDM_DBG_BOARD_HOTSWAP     ((WORD32)0x00040000)  /* 调试STM-1单板的热插拔处理 */
#define DRV_TDM_DBG_MEMORY_CFG        ((WORD32)0x00080000)  /* 调试内存 */
#define DRV_TDM_DBG_COMMON            ((WORD32)0x00100000)  /* common debug */
#define DRV_TDM_DBG_E1_TIMING_CFG     ((WORD32)0x00200000)  /* 调试E1的时钟配置  */
#define DRV_TDM_DBG_AT_REG_ACCESS     ((WORD32)0x00400000)  /* 调试访问ARRIVE芯片的寄存器. */
#define DRV_TDM_DBG_E1_TIMING_QRY     ((WORD32)0x00800000)  /* 调试查询E1时钟的状态 */
#define DRV_TDM_DBG_INTR_MSG          ((WORD32)0x01000000)  /* 调试中断 */
#define DRV_TDM_DBG_ALL_MSG           ((WORD32)0xFFFFFFFF)

#define DRV_TDM_MAX_STM_CARD_NUM         ((BYTE)4)   /* 每块线卡的最大STM子卡数 */
#define DRV_TDM_MAX_STM_PORT_NUM         ((BYTE)8)   /* 每块子卡的最大STM端口数 */
#define DRV_TDM_MIN_STM_INTF_INDEX       ((BYTE)0)  /* 最小的STM接口索引 */
#define DRV_TDM_MAX_STM_INTF_INDEX       ((BYTE)7)  /* 最大的STM接口索引 */
#define DRV_TDM_MAX_CHIP_NUM_NONE_15K_2  ((BYTE)4)  /* 对于非15K-2系统,每块线卡上的ARRIVE芯片数量 */
#define DRV_TDM_MAX_CHIP_NUM_15K_2       ((BYTE)1)  /* 对于15K-2系统,每块线卡上的ARRIVE芯片数量 */
#define DRV_TDM_SUBCARD_ID_START    ((BYTE)1)   /* STM单板的开始编号 */
#define DRV_TDM_SUBCARD_ID_END      ((BYTE)4)   /* STM单板的结尾编号 */
#define DRV_TDM_STM_PORT_ID_START   ((BYTE)1)   /* STM单板的STM端口的开始编号 */
#define DRV_TDM_STM_8PORT_ID_END    ((BYTE)8)   /* 8端口STM单板的STM端口的结束编号 */
#define DRV_TDM_STM_4PORT_ID_END    ((BYTE)4)   /* 4端口STM单板的STM端口的结束编号 */
#define DRV_TDM_CHIP_ETH_INTF_NUM        ((BYTE)2)  /* ARRIVE芯片的Eth接口的数量 */
#define DRV_TDM_CHIP_ETH_INTF_ID_START   ((BYTE)0) /* ARRIVE芯片的Eth接口的开始编号 */
#define DRV_TDM_CHIP_ETH_INTF_ID_END     ((BYTE)1) /* ARRIVE芯片的Eth接口的结束编号 */
#define DRV_TDM_E1_NUM_PER_TUG3          ((BYTE)21)
#define DRV_TDM_E1_NUM_PER_TUG2          ((BYTE)3)
#define DRV_TDM_E1_NUM_PER_STM1     ((WORD32)63)  /* STM-1帧承载的E1链路的数量 */
#define DRV_TDM_E1_LINK_ID_START    ((BYTE)1)   /* E1链路的开始编号 */
#define DRV_TDM_E1_LINK_ID_END      ((BYTE)63)  /* E1链路的结束编号 */
#define DRV_TDM_E1_NUM_PER_BOARD    ((WORD32)504)  /* 每块STM单板所承载的E1链路的数量 */
#define DRV_TDM_LINK_ID_START       ((WORD32)1)    /* 开始编号 */
#define DRV_TDM_LINK_ID_END         ((WORD32)504)  /* 结束编号 */
#define DRV_TDM_STM1_AUG1_ID        ((BYTE)1)      /* STM-1帧的AUG1编号 */
#define DRV_TDM_MIN_AUG1_ID         ((BYTE)1)      /* 最小的AUG1编号 */
#define DRV_TDM_MAX_AUG1_ID         ((BYTE)4)      /* 最大的AUG1编号 */
#define DRV_TDM_MIN_AUG1_INDEX      ((BYTE)0)      /* 最小的AUG1索引 */
#define DRV_TDM_MAX_AUG1_INDEX      ((BYTE)7)      /* 最大的AUG1索引 */
#define DRV_TDM_MIN_TUG3_ID             ((BYTE)1)  /* 最小的TUG3编号 */
#define DRV_TDM_MAX_TUG3_ID             ((BYTE)3)  /* 最大的TUG3编号 */
#define DRV_TDM_MIN_TUG2_ID             ((BYTE)1)  /* 最小的TUG2编号 */
#define DRV_TDM_MAX_TUG2_ID             ((BYTE)7)  /* 最大的TUG2编号 */
#define DRV_TDM_MIN_TU12_ID             ((BYTE)1)  /* 最小的TU12编号 */
#define DRV_TDM_MAX_TU12_ID             ((BYTE)3)  /* 最大的TU12编号 */
#define DRV_TDM_DEFAULT_TX_C2                  ((BYTE)2)  /* 默认发送的C2的值 */
#define DRV_TDM_DEFAULT_EXPECTED_RX_C2         ((BYTE)2)  /* 默认期望接收的C2的值 */
#define DRV_TDM_DEFAULT_TX_V5                  ((BYTE)2)  /* 默认发送的V5的值 */
#define DRV_TDM_DEFAULT_EXPECTED_RX_V5         ((BYTE)2)  /* 默认期望接收的V5的值 */

#define DRV_TDM_AT_DEV_TX_IPG   ((BYTE)12)  /* Number of TX Inter Packet Gap Clock for ARRIVE */
#define DRV_TDM_AT_DEV_RX_IPG   ((BYTE)4)   /* Number of RX Inter Packet Gap Clock for ARRIVE */

#define DRV_TDM_E1_MAX_TS_NUM     ((BYTE)32)  /* E1帧的最大时隙数 */
#define DRV_TDM_UNFRAME_E1_BMP    ((WORD32)0xFFFFFFFF)  /* unframe E1的时隙位图 */
#define DRV_TDM_E1_TIMESLOT_0     ((WORD32)0x00000001)  /* E1帧的第0时隙 */
#define DRV_TDM_E1_TIMESLOT_16    ((WORD32)0x00010000)  /* E1帧的第16时隙 */

#define DRV_TDM_FPGA_RAM_DATA_BUS_ADDR   ((WORD32)0x2345)

#define DRV_TDM_SDH_BER_PERIODIC   ((WORD32)100)

/* The clock status of FPGA. */
#define DRV_TDM_FPGA_CLK_GOOD      ((BYTE)0)   /* Clock is good. */
#define DRV_TDM_FPGA_CLK_BAD       ((BYTE)1)   /* Clock is bad. */
#define DRV_TDM_FPGA_CLK_MAX       ((BYTE)2)

#define DRV_TDM_FPGA_CODE_STM        ((WORD32)0x60031031)     /* FPGA product code for STM */
#define DRV_TDM_FPGA_CODE_MLPPP      ((WORD32)0x60031035)     /* FPGA product code for MLPPP */

typedef enum
{
    DRV_TDM_FPGA_RAM_UNFINISHED = 0,     /* ram test is unfinished. */
    DRV_TDM_FPGA_RAM_FINISHED = 1,       /* ram test is finished. */
    DRV_TDM_FPGA_RAM_TEST_MAX = 2,
} DRV_TDM_FPGA_RAM_TEST_STATUS;   /* The ram test status of FPGA. */

typedef enum
{
    DRV_TDM_FPGA_RAM_GOOD = 0,     /* ram is good. */
    DRV_TDM_FPGA_RAM_BAD = 1,      /* ram is bad. */
    DRV_TDM_FPGA_RAM_MAX = 2,
} DRV_TDM_FPGA_RAM_STATUS;   /* The ram status of FPGA. */

typedef enum
{
    DRV_TDM_FPGA_LOGIC_GOOD = 0,     /* FPGA Logic is good. */
    DRV_TDM_FPGA_LOGIC_BAD = 1,      /* FPGA Logic is bad. */
    DRV_TDM_FPGA_LOGIC_MAX = 2, 
} DRV_TDM_FPGA_LOGIC_STATUS;  /* The logic status of FPGA. */

extern VOID DRV_TDM_DEBUG_TRACE(WORD32 flag, CHAR* format, ...);  /* 应该放在宏定义之前,防止编译失败 */

/* 检查ARRIVE芯片的chipId的范围 */
#define DRV_TDM_CHECK_CHIP_ID(chipId) \
    do \
    { \
        if ((chipId) >= DRV_TDM_MAX_STM_CARD_NUM) \
        { \
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PARAM_MSG, "ERROR: %s line %d, invalid chipId=%u.\n", __FILE__, __LINE__, (chipId)); \
            return DRV_TDM_INVALID_CHIP_ID; \
        } \
    } while(0) \

/* 检查STM单板的子卡ID范围 */
#define DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId) \
    do \
    { \
        if (((subslotId) < DRV_TDM_SUBCARD_ID_START) || ((subslotId) > DRV_TDM_SUBCARD_ID_END)) \
        { \
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PARAM_MSG, "ERROR: %s line %d, invalid subslotId=%u.\n", __FILE__, __LINE__, (subslotId)); \
            return DRV_TDM_INVALID_SUBSLOT_ID; \
        } \
    } while(0) \

/* 检查8端口STM单板的155M端口的ID范围 */
#define DRV_TDM_CHECK_8PORT_ID(portId) \
    do \
    { \
        if (((portId) < DRV_TDM_STM_PORT_ID_START) || ((portId) > DRV_TDM_STM_8PORT_ID_END)) \
        { \
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PARAM_MSG, "ERROR: %s line %d, invalid portId=%u.\n", __FILE__, __LINE__, (portId)); \
            return DRV_TDM_INVALID_PORT_ID; \
        } \
    } while(0) \

/* 检查4端口STM单板的155M端口的ID范围 */
#define DRV_TDM_CHECK_4PORT_ID(portId) \
    do \
    { \
        if (((portId) < DRV_TDM_STM_PORT_ID_START) || ((portId) > DRV_TDM_STM_4PORT_ID_END)) \
        { \
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PARAM_MSG, "ERROR: %s line %d, invalid portId=%u.\n", __FILE__, __LINE__, (portId)); \
            return DRV_TDM_INVALID_PORT_ID; \
        } \
    } while(0) \

/* 检查STM接口的索引 */
#define DRV_TDM_CHECK_STM_INDEX(ucIntfIdx) \
    do \
    { \
        if ((ucIntfIdx) > DRV_TDM_MAX_STM_INTF_INDEX) \
        { \
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PARAM_MSG, "ERROR: %s line %d, invalid stmIntfIndex=%u.\n", __FILE__, __LINE__, (ucIntfIdx)); \
            return DRV_TDM_INVALID_STM_INTF_INDEX; \
        } \
    } while(0) \

/* 检查ARRIVE芯片的Ethernet接口的index范围 */
#define DRV_TDM_CHECK_ETH_INTF_INDEX(ethIntfIndex) \
    do \
    { \
        if ((ethIntfIndex) >= DRV_TDM_CHIP_ETH_INTF_NUM) \
        { \
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PARAM_MSG, "ERROR: %s line %d, invalid ethIntfIndex=%u.\n", __FILE__, __LINE__, (ethIntfIndex)); \
            return DRV_TDM_INVALID_CHIP_ETH_INTF; \
        } \
    } while(0) \

/* 检查VC4中的E1链路的编号 */
#define DRV_TDM_CHECK_E1_LINK_ID(e1LinkId) \
    do \
    { \
        if (((e1LinkId) < DRV_TDM_E1_LINK_ID_START) || ((e1LinkId) > DRV_TDM_E1_LINK_ID_END)) \
        { \
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PARAM_MSG, "ERROR: %s line %d, invalid e1LinkId=%u.\n", __FILE__, __LINE__, (e1LinkId)); \
            return DRV_TDM_INVALID_E1_LINK_ID; \
        } \
    } while(0) \

/* 检查STM单板上的E1链路的编号 */
#define DRV_TDM_CHECK_LINK_ID(linkId) \
    do \
    { \
        if (((linkId) < DRV_TDM_LINK_ID_START) || ((linkId) > DRV_TDM_LINK_ID_END)) \
        { \
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PARAM_MSG, "ERROR: %s line %d, invalid linkId=%u.\n", __FILE__, __LINE__, (linkId)); \
            return DRV_TDM_INVALID_LINK_ID; \
        } \
    } while(0) \


#define DRV_TDM_CHECK_AUG1_ID(aug1Id) \
    do \
    { \
        if (((aug1Id) < DRV_TDM_MIN_AUG1_ID) || ((aug1Id) > DRV_TDM_MAX_AUG1_ID)) \
        { \
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PARAM_MSG, "ERROR: %s line %d, invalid aug1Id=%u.\n", __FILE__, __LINE__, (aug1Id)); \
            return DRV_TDM_INVALID_AUG1_ID; \
        } \
    } while(0) \

#define DRV_TDM_CHECK_AUG1_INDEX(aug1Index) \
    do \
    { \
        if ((aug1Index) > DRV_TDM_MAX_AUG1_INDEX) \
        { \
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PARAM_MSG, "ERROR: %s line %d, invalid aug1Index=%u.\n", __FILE__, __LINE__, (aug1Index)); \
            return DRV_TDM_INVALID_AUG1_INDEX; \
        } \
    } while(0) \


/* 检查TUG3的编号 */
#define DRV_TDM_CHECK_TUG3_ID(tug3Id) \
    do \
    { \
        if (((tug3Id) < DRV_TDM_MIN_TUG3_ID) || ((tug3Id) > DRV_TDM_MAX_TUG3_ID)) \
        { \
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PARAM_MSG, "ERROR: %s line %d, invalid tug3Id=%u.\n", __FILE__, __LINE__, (tug3Id)); \
            return DRV_TDM_INVALID_TUG3_ID; \
        } \
    } while(0) \

/* 检查TUG2的编号 */
#define DRV_TDM_CHECK_TUG2_ID(tug2Id) \
    do \
    { \
        if (((tug2Id) < DRV_TDM_MIN_TUG2_ID) || ((tug2Id) > DRV_TDM_MAX_TUG2_ID)) \
        { \
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PARAM_MSG, "ERROR: %s line %d, invalid tug2Id=%u.\n", __FILE__, __LINE__, (tug2Id)); \
            return DRV_TDM_INVALID_TUG2_ID; \
        } \
    } while(0) \

/* 检查TU12的编号 */
#define DRV_TDM_CHECK_TU12_ID(tu12Id) \
    do \
    { \
        if (((tu12Id) < DRV_TDM_MIN_TU12_ID) || ((tu12Id) > DRV_TDM_MAX_TU12_ID)) \
        { \
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PARAM_MSG, "ERROR: %s line %d, invalid tu12Id=%u.\n", __FILE__, __LINE__, (tu12Id)); \
            return DRV_TDM_INVALID_TU12_ID; \
        } \
    } while(0) \
    
/* 检查指针是否为NULL指针 */
#define DRV_TDM_CHECK_POINTER_IS_NULL(ptr) \
    do \
    { \
        if (NULL == (ptr)) \
        { \
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PARAM_MSG, "ERROR: %s line %d, NULL pointer.\n", __FILE__, __LINE__); \
            return DRV_TDM_NULL_POINTER; \
        } \
    } while(0) \

typedef struct
{
    BYTE ucSysClkState;
    BYTE ucLocalBusClkState;
    BYTE ucGeClkState;
    BYTE ucOcnClk1State;
    BYTE ucOcnClk2State;
    BYTE ucDdr1ClkState;
    BYTE ucDdr2ClkState;
    BYTE ucDdr3ClkState;
} T_DRV_TDM_FPGA_CLK_STATE;  /* The clock status of FPGA. */


/* 函数原型声明 */
extern WORD32 drv_tdm_stmPortIdCheck(BYTE chipId, BYTE portId);
extern WORD32 drv_tdm_stmIntfModeSet(BYTE ucChipId, BYTE ucPortId, WORD32 dwIntfMode);
extern WORD32 drv_tdm_stmIntfModeGet(BYTE ucChipId, BYTE ucPortId, WORD32 *pdwIntfMode);
extern WORD32 drv_tdm_debugFlagGet(VOID);
extern WORD32 drv_tdm_tug3Tug2Tu12IdGet(BYTE e1LinkId, BYTE *tug3Id, BYTE *tug2Id, BYTE *tu12Id);
extern WORD32 drv_tdm_e1LinkIdGet(BYTE tug3Id, BYTE tug2Id, BYTE tu12Id, BYTE *e1LinkId);
extern WORD32 drv_tdm_linkIdGet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, WORD32 *pLinkId);
extern WORD32 drv_tdm_aug1IndexGet(BYTE ucPortId, BYTE ucAug1Id, BYTE *pucAug1Idx);
extern WORD32 drv_tdm_e1TimeslotNumGet(WORD32 timeslotBmp, BYTE *timeslotNum);
extern WORD32 drv_tdm_atDevProductCodeGet(BYTE subslotId, WORD32 *pProductCode);
extern WORD32 drv_tdm_atDevDriverPtrGet(AtDriver *ppDriver);
extern WORD32 drv_tdm_atDevicePtrGet(BYTE chipId, AtDevice *ppDevice);
extern WORD32 drv_tdm_atDevHalGet(BYTE chipId, AtHal *ppHal);
extern WORD32 drv_tdm_atSdhLinePtrGet(BYTE chipId, BYTE portId, AtSdhLine *ppSdhLine);
extern WORD32 drv_tdm_atSdhAug4PtrGet(BYTE chipId, BYTE portId, AtSdhAug *ppSdhAug4);
extern WORD32 drv_tdm_atSdhAug1PtrGet(BYTE chipId, BYTE portId, BYTE aug1Id, AtSdhAug *ppSdhAug1);
extern WORD32 drv_tdm_atSdhAu4PtrGet(BYTE chipId, BYTE portId, BYTE aug1Id, AtSdhAu *ppSdhAu4);
extern WORD32 drv_tdm_atSdhVc4PtrGet(BYTE chipId, BYTE portId, BYTE aug1Id, AtSdhVc *ppSdhVc4);
extern WORD32 drv_tdm_atSdhTu12PtrGet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, AtSdhTu *ppSdhTu12);
extern WORD32 drv_tdm_atSdhVc12PtrGet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, AtSdhVc *ppSdhVc12);
extern WORD32 drv_tdm_atPdhE1PtrGet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, AtPdhDe1 *ppPdhE1);
extern WORD32 drv_tdm_atDevEthPortPtrGet(BYTE chipId, BYTE ethIntfIndex, AtEthPort *ppEthPort);
extern WORD32 drv_tdm_atDevHalRegRead(BYTE chipId, WORD32 address, WORD32 *pValue);
extern WORD32 drv_tdm_atDevHalRegWrite(BYTE chipId, WORD32 address, WORD32 value);
extern WORD32 drv_tdm_pwe3InitFlagGet(BYTE chipId, WORD32 *pFlag);
extern WORD32 drv_tdm_pwe3Initialize(BYTE subslotId);
extern WORD32 drv_tdm_pwe3UnInitialize(BYTE subslotId);
extern WORD32 drv_tdm_fpgaRamDdrNumGet(BYTE ucChipId, BYTE *pucRamDdrNum);
extern WORD32 drv_tdm_fpgaRamCheck(BYTE ucChipId);
extern WORD32 drv_tdm_fpgaRamTestedStateGet(BYTE ucSubslotId, WORD32 *pdwRamTestState);
extern WORD32 drv_tdm_fpgaRamStateGet(BYTE ucSubslotId, WORD32 *pdwRamState);
extern WORD32 drv_tdm_fpgaLogicStateGet(BYTE ucSubslotId, WORD32 *pdwLogicState);
extern WORD32 drv_tdm_fpgaClockStateGet(BYTE ucSubslotId, T_DRV_TDM_FPGA_CLK_STATE *pClkState);
extern WORD32 drv_tdm_fpgaClockStateQuery(BYTE ucSubslotId, WORD32 *pdwClkState);

    
#endif


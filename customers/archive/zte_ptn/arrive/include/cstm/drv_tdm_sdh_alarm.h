/***********************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_tdm_sdh_alarm.h
* 文件标识: 
* 其它说明: 驱动STM TDM PWE3模块的STM接口的SDH告警处理头文件
* 当前版本: V1.0
* 作    者: 谢伟生10112265.
* 完成日期: 2013-03-26.
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
************************************************************/

#ifndef _DRV_TDM_SDH_ALARM_H_
#define _DRV_TDM_SDH_ALARM_H_

#include "drv_tdm_init.h"


typedef enum
{
    DRV_TDM_SDH_RS_LOS = 0x00000001,       
    DRV_TDM_SDH_RS_OOF = 0x00000002,       
    DRV_TDM_SDH_RS_LOF = 0x00000004,       
    DRV_TDM_SDH_RS_TIM = 0x00000008,       
    DRV_TDM_SDH_RS_SD  = 0x00000010, 
    DRV_TDM_SDH_RS_SF  = 0x00000020,   
    DRV_TDM_SDH_MS_AIS = 0x00000040,       
    DRV_TDM_SDH_MS_RDI = 0x00000080,       
    DRV_TDM_SDH_MS_SD  = 0x00000100,       
    DRV_TDM_SDH_MS_SF  = 0x00000200,       
} DRV_TDM_SECTION_ALARM_TYPE;

typedef enum
{
    DRV_TDM_SDH_AU4_AIS  = 0x00000001,  
    DRV_TDM_SDH_AU4_LOP  = 0x00000002,  
    DRV_TDM_SDH_VC4_TIM  = 0x00000004,  
    DRV_TDM_SDH_VC4_UNEQ = 0x00000008,  
    DRV_TDM_SDH_VC4_PLM  = 0x00000010,  
    DRV_TDM_SDH_VC4_RDI  = 0x00000020,  
    DRV_TDM_SDH_VC4_SD   = 0x00000040,  
    DRV_TDM_SDH_VC4_SF   = 0x00000080, 
} DRV_TDM_HP_ALARM_TYPE;

typedef enum
{
    DRV_TDM_SDH_TU12_AIS  = 0x00000001,  
    DRV_TDM_SDH_TU12_LOP  = 0x00000002,  
    DRV_TDM_SDH_TU12_LOMF = 0x00000004,
    DRV_TDM_SDH_VC12_TIM  = 0x00000008,  
    DRV_TDM_SDH_VC12_UNEQ = 0x00000010,  
    DRV_TDM_SDH_VC12_PLM  = 0x00000020,  
    DRV_TDM_SDH_VC12_RDI  = 0x00000040,  
    DRV_TDM_SDH_VC12_SD   = 0x00000080,  
    DRV_TDM_SDH_VC12_SF   = 0x00000100, 
} DRV_TDM_LP_ALARM_TYPE;

/* 中断状态的位图 */
#define DRV_TDM_RSLOS_INTR_STATUS_BIT     ((WORD32)0x00000001)
#define DRV_TDM_RSLOF_INTR_STATUS_BIT     ((WORD32)0x00000002)
#define DRV_TDM_RSOOF_INTR_STATUS_BIT     ((WORD32)0x00000004)
#define DRV_TDM_RSTIM_INTR_STATUS_BIT     ((WORD32)0x00000008)
#define DRV_TDM_MSAIS_INTR_STATUS_BIT     ((WORD32)0x00000010)
#define DRV_TDM_MSRDI_INTR_STATUS_BIT     ((WORD32)0x00000020)
#define DRV_TDM_MSSD_INTR_STATUS_BIT      ((WORD32)0x00000040)
#define DRV_TDM_MSSF_INTR_STATUS_BIT      ((WORD32)0x00000080)
#define DRV_TDM_KBYTE_INTR_STATUS_BIT     ((WORD32)0x00000100) /* K-byte change event. Stable K-byte is changed. */
#define DRV_TDM_K_FAIL_INTR_STATUS_BIT    ((WORD32)0x00000200) /* K-byte fail. There is no stable K-byte */
#define DRV_TDM_S1BYTE_INTR_STATUS_BIT    ((WORD32)0x00000400)
#define DRV_TDM_RSSD_INTR_STATUS_BIT      ((WORD32)0x00000800)
#define DRV_TDM_RSSF_INTR_STATUS_BIT      ((WORD32)0x00001000)

/* 中断屏蔽的位图 */
#define DRV_TDM_RSLOS_INTR_MASK_BIT       DRV_TDM_RSLOS_INTR_STATUS_BIT
#define DRV_TDM_RSLOF_INTR_MASK_BIT       DRV_TDM_RSLOF_INTR_STATUS_BIT
#define DRV_TDM_RSOOF_INTR_MASK_BIT       DRV_TDM_RSOOF_INTR_STATUS_BIT
#define DRV_TDM_RSTIM_INTR_MASK_BIT       DRV_TDM_RSTIM_INTR_STATUS_BIT
#define DRV_TDM_MSAIS_INTR_MASK_BIT       DRV_TDM_MSAIS_INTR_STATUS_BIT
#define DRV_TDM_MSRDI_INTR_MASK_BIT       DRV_TDM_MSRDI_INTR_STATUS_BIT
#define DRV_TDM_MSSD_INTR_MASK_BIT        DRV_TDM_MSSD_INTR_STATUS_BIT
#define DRV_TDM_MSSF_INTR_MASK_BIT        DRV_TDM_MSSF_INTR_STATUS_BIT
#define DRV_TDM_KBYTE_INTR_MASK_BIT       DRV_TDM_KBYTE_INTR_STATUS_BIT
#define DRV_TDM_K_FAIL_INTR_MASK_BIT      DRV_TDM_K_FAIL_INTR_STATUS_BIT
#define DRV_TDM_S1BYTE_INTR_MASK_BIT      DRV_TDM_S1BYTE_INTR_STATUS_BIT
#define DRV_TDM_RSSD_INTR_MASK_BIT        DRV_TDM_RSSD_INTR_STATUS_BIT
#define DRV_TDM_RSSF_INTR_MASK_BIT        DRV_TDM_RSSF_INTR_STATUS_BIT
#define DRV_TDM_ALL_INTR_MASK_BIT         ((WORD32)0xffffffff)

/* 中断屏蔽的状态标记 */
#define DRV_TDM_RSLOS_INTR_MASK_STATE     DRV_TDM_RSLOS_INTR_MASK_BIT
#define DRV_TDM_RSLOF_INTR_MASK_STATE     DRV_TDM_RSLOF_INTR_MASK_BIT
#define DRV_TDM_RSOOF_INTR_MASK_STATE     DRV_TDM_RSOOF_INTR_MASK_BIT
#define DRV_TDM_RSTIM_INTR_MASK_STATE     DRV_TDM_RSTIM_INTR_MASK_BIT
#define DRV_TDM_MSAIS_INTR_MASK_STATE     DRV_TDM_MSAIS_INTR_MASK_BIT
#define DRV_TDM_MSRDI_INTR_MASK_STATE     DRV_TDM_MSRDI_INTR_MASK_BIT
#define DRV_TDM_MSSD_INTR_MASK_STATE      DRV_TDM_MSSD_INTR_MASK_BIT
#define DRV_TDM_MSSF_INTR_MASK_STATE      DRV_TDM_MSSF_INTR_MASK_BIT
#define DRV_TDM_KBYTE_INTR_MASK_STATE     DRV_TDM_KBYTE_INTR_MASK_BIT
#define DRV_TDM_K_FAIL_INTR_MASK_STATE    DRV_TDM_K_FAIL_INTR_MASK_BIT
#define DRV_TDM_S1BYTE_INTR_MASK_STATE    DRV_TDM_S1BYTE_INTR_MASK_BIT
#define DRV_TDM_RSSD_INTR_MASK_STATE      DRV_TDM_RSSD_INTR_MASK_BIT
#define DRV_TDM_RSSF_INTR_MASK_STATE      DRV_TDM_RSSF_INTR_MASK_BIT
#define DRV_TDM_ALL_INTR_MASK_DISABLE     ((WORD32)0)
#define DRV_TDM_ALL_INTR_MASK_ENABLE      ((WORD32)0xffffffff)

typedef enum
{
    DRV_TDM_STM_INTERRUPT_DISABLE = 0,   /* disable interrupt */
    DRV_TDM_STM_INTERRUPT_ENABLE = 1,    /* enable interrupt */
} DRV_TDM_STM_INTERRUPT_STATE;

typedef struct
{
    BYTE rsOof;  /* RS out of frame */
    BYTE rsLof;  /* RS loss of frame */
    BYTE rsLos;  /* RS loss of signal */
    BYTE rsTim;  /* RS trace identifier mismatch */
    BYTE rsSd;   /* RS signal degrade */
    BYTE rsSf;   /* RS signal fail */
    BYTE msAis;  /* MS alarm indication signal */
    BYTE msRdi;  /* RS remote defect indication */
    BYTE msSd;   /* MS signal degrade */
    BYTE msSf;   /* MS signal fail */
    BYTE reserved[2];  /* 保留字节,用于内存对齐 */
} DRV_TDM_SECTION_ALARM;  /* STM帧的段告警*/

typedef struct
{
    BYTE au4Lop;       /* AU4 loss of pointer */
    BYTE au4Ais;       /* AU4 alarm indication signal */
    BYTE vc4Lomf;      /* VC4 Loss of MultiFrame */
    BYTE vc4Uneq;      /* VC4 unequipped */
    BYTE vc4Plm;       /* VC4 payload label mismatch  */
    BYTE vc4Tim;       /* VC4 trace identifier mismatch  */
    BYTE vc4Rdi;       /* VC4 remote defect indication */
    BYTE vc4Sd;        /* VC4 signal degrade */
    BYTE vc4Sf;        /* VC4 signal fail */
    BYTE vc4ERdiS;     /* VC4 ERDI-S, E-RDI Server defect, AU4-AIS/AU4-LOP告警回告ERDI-S  */
    BYTE vc4ERdiP;     /* VC4 ERDI-P, E-RDI Payload defect, VC4-PLM告警回告ERDI-P */
    BYTE vc4ERdiC;     /* VC4 ERDI-C, E-RDI connectivity defect, VC4-TIM/VC4-UNEQ回告ERDI-C */
} DRV_TDM_VC4_ALARM;  /* STM帧的高阶通道告警,根据G.707标准来定义E-RDI */

typedef struct 
{
    BYTE tu12Lop;      /* TU12 loss of pointer */
    BYTE tu12Ais;      /* TU12 alarm indication signal */
    BYTE vc12Uneq;     /* VC12 unequipped */
    BYTE vc12Plm;      /* VC12 payload label mismatch  */
    BYTE vc12Tim;      /* VC12 trace identifier mismatch  */
    BYTE vc12Rdi;      /* VC12 remote defect indication */
    BYTE vc12Sd;       /* VC12 signal degrade */
    BYTE vc12Sf;       /* VC12 signal fail */
    BYTE vc12ERdiS;    /* VC12 ERDI-S, E-RDI Server defect, TU12-AIS/TU12-LOP告警回告ERDI-S */
    BYTE vc12ERdiP;    /* VC12 ERDI-P, E-RDI Payload defect, VC12-PLM告警回告ERDI-P */
    BYTE vc12ERdiC;    /* VC12 ERDI-C, E-RDI connectivity defect, VC12-TIM/VC12-UNEQ回告ERDI-C */
    BYTE reserved[1];  /* 保留字节,用来进行字节对齐 */
} DRV_TDM_VC12_ALARM;  /* STM帧的低阶通道告警,根据G.707标准来定义E-RDI */

typedef struct
{
    BYTE e1Lof;          /* E1 loss of frame */
    BYTE e1Lomf;         /* E1 loss of multi-frame */
    BYTE e1Ais;          /* E1 alarm indication signal */
    BYTE e1Rai;          /* E1 remote alarm indication */
} DRV_TDM_E1_ALARM;  /* STM帧的E1告警*/


/* 函数原型声明 */
extern WORD32 drv_tdm_sectionAlarmGet(BYTE chipId, BYTE portId, DRV_TDM_SECTION_ALARM *pSectionAlm);
extern WORD32 drv_tdm_highPathAlarmGet(BYTE chipId, BYTE portId, BYTE aug1Id, DRV_TDM_VC4_ALARM *pVc4Alm);
extern WORD32 drv_tdm_lowPathAlarmGet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, DRV_TDM_VC12_ALARM *pVc12Alm);
extern WORD32 drv_tdm_e1AlarmGet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, DRV_TDM_E1_ALARM *pE1Alm);
extern WORD32 drv_tdm_sectionAlarmQuery(BYTE chipId, BYTE portId, WORD32 *pAlmResult);
extern WORD32 drv_tdm_highPathAlarmQuery(BYTE chipId, BYTE portId, BYTE aug1Id, WORD32 *pAlmResult);
extern WORD32 drv_tdm_lowPathAlarmQuery(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, WORD32 *pAlmResult);
extern WORD32 drv_tdm_e1AlarmQuery(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, WORD32 *pAlmResult);
extern WORD32 drv_tdm_msAisForce(BYTE chipId, BYTE portId, WORD32 direction);
extern WORD32 drv_tdm_msAisUnForce(BYTE chipId, BYTE portId, WORD32 direction);
extern WORD32 drv_tdm_au4AisForce(BYTE chipId, BYTE portId, BYTE aug1Id, WORD32 direction);
extern WORD32 drv_tdm_au4AisUnForce(BYTE chipId, BYTE portId, BYTE aug1Id, WORD32 direction);
extern WORD32 drv_tdm_au4LopForce(BYTE chipId, BYTE portId, BYTE aug1Id, WORD32 direction);
extern WORD32 drv_tdm_au4LopUnForce(BYTE chipId, BYTE portId, BYTE aug1Id, WORD32 direction);
extern WORD32 drv_tdm_vc4UneqForce(BYTE chipId, BYTE portId, BYTE aug1Id, WORD32 direction);
extern WORD32 drv_tdm_vc4UneqUnForce(BYTE chipId, BYTE portId, BYTE aug1Id, WORD32 direction);
extern WORD32 drv_tdm_tu12AisForce(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, WORD32 direction);
extern WORD32 drv_tdm_tu12AisUnForce(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, WORD32 direction);
extern WORD32 drv_tdm_tu12LopForce(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, WORD32 direction);
extern WORD32 drv_tdm_tu12LopUnForce(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, WORD32 direction);
extern WORD32 drv_tdm_tu12LomfForce(BYTE chipId, BYTE portId, BYTE aug1Id, WORD32 direction);
extern WORD32 drv_tdm_tu12LomfUnForce(BYTE chipId, BYTE portId, BYTE aug1Id, WORD32 direction);
extern WORD32 drv_tdm_secAlmAffectingEnable(BYTE chipId, BYTE portId, WORD32 alarmType, WORD32 enableFlag);
extern WORD32 drv_tdm_hpAlmAffectingEnable(BYTE chipId, BYTE portId, BYTE aug1Id, WORD32 alarmType, WORD32 enableFlag);
extern WORD32 drv_tdm_lpAlmAffectingEnable(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, WORD32 alarmType, WORD32 enableFlag);
extern WORD32 drv_tdm_interruptEnable(BYTE subslotId, WORD32 enableFlag);
extern WORD32 drv_tdm_secAlmIntrStatusGet(BYTE subslotId, BYTE portId, WORD32 *pIntrStatus);
extern WORD32 drv_tdm_secAlmIntrClear(BYTE subslotId, BYTE portId);
extern WORD32 drv_tdm_secAlmIntrMaskGet(BYTE subslotId, BYTE portId, WORD32 *pIntrMask);
extern WORD32 drv_tdm_secAlmIntrMaskSet(BYTE subslotId, BYTE portId, WORD32 maskBmp, WORD32 maskEnBmp);

#endif



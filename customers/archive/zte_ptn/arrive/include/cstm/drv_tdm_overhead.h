/***********************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_tdm_overhead.h
* 文件标识: 
* 其它说明: 驱动STM TDM PWE3模块的STM接口的开销字节处理头文件
* 当前版本: V1.0
* 作    者: 谢伟生10112265.
* 完成日期: 2013-03-26.
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
************************************************************/

#ifndef _DRV_TDM_OVERHEAD_H_
#define _DRV_TDM_OVERHEAD_H_

#include "drv_tdm_init.h"

#define DRV_TDM_SDH_OH_TASK_NAME_SIZE    40  /* SDH overhead任务的任务名大小 */
#define DRV_TDM_SDH_OH_TASK_PRIORITY     (WORD16)(75)  /* SDH overhead进程的优先级 */
#define DRV_TDM_SDH_OH_TASK_STACK_SIZE   (WORD32)(200*1024)  /* SDH overhead进程的stack大小 */

typedef enum
{
    DRV_TDM_TX_DIRECTION = 0,
    DRV_TDM_RX_DIRECTION = 1,
} DRV_TDM_TRANSFER_DIRECTION;  /* STM-1帧的传输方向 */

/* The following enumeration defines constants indicating OCN trace message modes */
typedef enum
{
    DRV_TDM_TRACE_MSG_1BYTE  = 0,  /* Message 1 byte */
    DRV_TDM_TRACE_MSG_16BYTE = 1,  /* Message 16 byte */
    DRV_TDM_TRACE_MSG_64BYTE = 2,  /* Message 64 byte */
} DRV_TDM_TRACE_MSG_MODE;

/* The following enumeration defines constants indicating OCN trace message length */
typedef enum
{
    DRV_TDM_TRACE_MSG_LEN_1  = 1,    /* Message 1 bytes */
    DRV_TDM_TRACE_MSG_LEN_16 = 16,   /* Message 16 bytes */
    DRV_TDM_TRACE_MSG_LEN_64 = 64,   /* Message 64 bytes */
} DRV_TDM_TRACE_MSG_LEN;

#define DRV_TDM_TRACE_MSG_MAX_LEN  (DRV_TDM_TRACE_MSG_LEN_64 + 1)  /* J字节的最大长度为65字节(包括1字节的CRC) */


/* 函数原型声明 */
extern WORD32 drv_tdm_txJ0Set(BYTE chipId, BYTE portId, WORD32 traceMode, const BYTE *msgBuf);
extern WORD32 drv_tdm_txJ0Get(BYTE chipId, BYTE portId, BYTE *msgBuf);
extern WORD32 drv_tdm_expectedRxJ0Set(BYTE chipId, BYTE portId, WORD32 traceMode, const BYTE *msgBuf);
extern WORD32 drv_tdm_expectedRxJ0Get(BYTE chipId, BYTE portId, BYTE *msgBuf);
extern WORD32 drv_tdm_rxJ0Get(BYTE chipId, BYTE portId, BYTE *msgBuf);
extern WORD32 drv_tdm_txJ1Set(BYTE chipId, BYTE portId, BYTE aug1Id, WORD32 traceMode, const BYTE *msgBuf);
extern WORD32 drv_tdm_txJ1Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *msgBuf);
extern WORD32 drv_tdm_expectedRxJ1Set(BYTE chipId, BYTE portId, BYTE aug1Id, WORD32 traceMode, const BYTE *msgBuf);
extern WORD32 drv_tdm_expectedRxJ1Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *msgBuf);
extern WORD32 drv_tdm_rxJ1Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *msgBuf);
extern WORD32 drv_tdm_txJ2Set(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, WORD32 traceMode, const BYTE *msgBuf);
extern WORD32 drv_tdm_txJ2Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, BYTE *msgBuf);
extern WORD32 drv_tdm_expectedRxJ2Set(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, WORD32 traceMode, const BYTE *msgBuf);
extern WORD32 drv_tdm_expectedRxJ2Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, BYTE *msgBuf);
extern WORD32 drv_tdm_rxJ2Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, BYTE *msgBuf);
extern WORD32 drv_tdm_txC2Set(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE c2Value);
extern WORD32 drv_tdm_txC2Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *pC2Value);
extern WORD32 drv_tdm_expectedRxC2Set(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE c2Value);
extern WORD32 drv_tdm_expectedRxC2Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *pC2Value);
extern WORD32 drv_tdm_rxC2Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *pC2Value);
extern WORD32 drv_tdm_lpTxPslSet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, BYTE v5Value);
extern WORD32 drv_tdm_lpTxPslGet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, BYTE *pV5Value);
extern WORD32 drv_tdm_lpExpectedRxPslSet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, BYTE v5Value);
extern WORD32 drv_tdm_lpExpectedRxPslGet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, BYTE *pV5Value);
extern WORD32 drv_tdm_lpRxPslGet(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, BYTE *pV5Value);
extern WORD32 drv_tdm_txS1Set(BYTE chipId, BYTE portId, BYTE s1Value);
extern WORD32 drv_tdm_txS1Get(BYTE chipId, BYTE portId, BYTE *pS1Value);
extern WORD32 drv_tdm_rxS1Get(BYTE chipId, BYTE portId, BYTE *pS1Value);
extern WORD32 drv_tdm_txK1Set(BYTE chipId, BYTE portId, BYTE k1Value);
extern WORD32 drv_tdm_txK1Get(BYTE chipId, BYTE portId, BYTE *pK1Value);
extern WORD32 drv_tdm_rxK1Get(BYTE chipId, BYTE portId, BYTE *pK1Value);
extern WORD32 drv_tdm_txK2Set(BYTE chipId, BYTE portId, BYTE k2Value);
extern WORD32 drv_tdm_txK2Get(BYTE chipId, BYTE portId, BYTE *pK2Value);
extern WORD32 drv_tdm_rxK2Get(BYTE chipId, BYTE portId, BYTE *pK2Value);
extern WORD32 drv_tdm_txM1Get(BYTE chipId, BYTE portId, BYTE *pM1Value);
extern WORD32 drv_tdm_rxM1Get(BYTE chipId, BYTE portId, BYTE *pM1Value);
extern WORD32 drv_tdm_txG1Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *pG1Value);
extern WORD32 drv_tdm_rxG1Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *pG1Value);
extern WORD32 drv_tdm_txH4Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *pH4Value);
extern WORD32 drv_tdm_rxH4Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *pH4Value);
extern WORD32 drv_tdm_txK3Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *pK3Value);
extern WORD32 drv_tdm_rxK3Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *pK3Value);
extern WORD32 drv_tdm_txN1Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *pN1Value);
extern WORD32 drv_tdm_rxN1Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE *pN1Value);
extern WORD32 drv_tdm_txV5Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, BYTE *pV5Value);
extern WORD32 drv_tdm_rxV5Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, BYTE *pV5Value);
extern WORD32 drv_tdm_txN2Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, BYTE *pN2Value);
extern WORD32 drv_tdm_rxN2Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, BYTE *pN2Value);
extern WORD32 drv_tdm_txK4Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, BYTE *pK4Value);
extern WORD32 drv_tdm_rxK4Get(BYTE chipId, BYTE portId, BYTE aug1Id, BYTE e1LinkId, BYTE *pK4Value);

#endif



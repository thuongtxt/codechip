/***********************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_pos_sdh_perf.h
* 文件标识: 
* 其它说明: 驱动STM-1 POS PWE3模块的STM-1接口的SDH性能统计处理头文件
* 当前版本: V1.0
* 作    者: 刘钰00130907.
* 完成日期: 2013-03-26.
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
************************************************************/

#ifndef _DRV_POS_SDH_PERF_H_
#define _DRV_POS_SDH_PERF_H_

#include "drv_pos_comm.h"

#define DRV_POS_SDH_PERF_TASK_NAME_SIZE    ((WORD32)40)  /* SDH performance任务的任务名大小 */
#define DRV_POS_SDH_PERF_TASK_PRIORITY     ((WORD16)75)  /* SDH performance进程的优先级 */
#define DRV_POS_SDH_PERF_TASK_STACK_SIZE   ((WORD32)(200*1024))  /* SDH performance进程的stack大小 */

#define DRV_POS_SDH_BER_TASK_NAME_SIZE    ((WORD32)40)  /* SDH BER任务的任务名大小 */
#define DRV_POS_SDH_BER_TASK_PRIORITY     ((WORD16)75)  /* SDH BER进程的优先级 */
#define DRV_POS_SDH_BER_TASK_STACK_SIZE   ((WORD32)(200*1024))  /* SDH BER进程的stack大小 */

#define DRV_POS_SDH_BER_PERIODIC   ((WORD32)100)


typedef enum
{
    DRV_POS_BER_RATE_UNKNOWN = cAtBerRateUnknown, /* Unknown bit error rate */
    DRV_POS_BER_RATE_1E3 = cAtBerRate1E3,     /* Bit error rate is 10-3 */
    DRV_POS_BER_RATE_1E4 = cAtBerRate1E4,     /* Bit error rate is 10-4 */
    DRV_POS_BER_RATE_1E5 = cAtBerRate1E5,     /* Bit error rate is 10-5 */
    DRV_POS_BER_RATE_1E6 = cAtBerRate1E6,     /* Bit error rate is 10-6 */
    DRV_POS_BER_RATE_1E7 = cAtBerRate1E7,     /* Bit error rate is 10-7 */
    DRV_POS_BER_RATE_1E8 = cAtBerRate1E8,     /* Bit error rate is 10-8 */
    DRV_POS_BER_RATE_1E9 = cAtBerRate1E9,     /* Bit error rate is 10-9 */
    DRV_POS_BER_RATE_1E10 = cAtBerRate1E10    /* Bit error rate is 10-10*/
} DRV_POS_BER_RATE;    /* Bit error rate for generating SD/SF alarm */

typedef enum
{
    DRV_POS_SD = 0,        /* SD */
    DRV_POS_SF = 1,        /* SF */
} DRV_POS_SD_SF_TYPE;  /* SDSF类型 */

typedef struct
{
    WORD32 b1Cnts;     /* B1 performance staticstis counters */
    WORD32 b2Cnts;     /* B2 performance staticstis counters */
    WORD32 msReiCnts;    /* B2 REI performance staticstis counters */
} DRV_POS_SECTION_PERF;  /* Section performance staticstis counters */

typedef struct
{
    WORD32 b3Cnts;    /* B3 performance staticstis counters */
    WORD32 hpReiCnts; /* B3 REI performance staticstis counters */
    WORD32 rxAu4Ppjc; /* Detected AU4 Positive Pointer Justification Count */
    WORD32 rxAu4Npjc; /* Detected AU4 Negative Pointer Justification Count */
    WORD32 txAu4Ppjx; /* Generated AU4 Positive Pointer Justification Count */
    WORD32 txAu4Npjc; /* Generated AU4 Negative Pointer Justification Count */
} DRV_POS_VC4_PERF;  /* High path performance staticstis counters */


typedef struct
{
    DRV_POS_SECTION_PERF sectionPerf;   /* Section performance staticstis counters */
    DRV_POS_VC4_PERF vc4Perf;   /* High path performance staticstis counters */
} DRV_POS_SDH_PERF;


/* 函数原型声明 */
DRV_POS_RET drv_pos_sdhPerfMemGet(BYTE chipId, BYTE portIdx, DRV_POS_SDH_PERF** ppPerf);
/*DRV_POS_RET drv_pos_sdhPerfClear(BYTE chipId, BYTE portIdx);
DRV_POS_RET drv_pos_sectionPerfSave(BYTE chipId, BYTE portIdx, const DRV_POS_SECTION_PERF *pSectionPerf);
DRV_POS_RET drv_pos_highPathPerfSave(BYTE chipId, BYTE portIdx, const DRV_POS_VC4_PERF *pVc4Perf);
DRV_POS_RET drv_pos_sectionPerfGet(BYTE chipId, BYTE portIdx, DRV_POS_SECTION_PERF *pSectionPerf);
DRV_POS_RET drv_pos_highPathPerfGet(BYTE chipId, BYTE portIdx, DRV_POS_VC4_PERF *pVc4Perf);*/
DRV_POS_RET drv_pos_sectionPerfQuery(BYTE chipId, BYTE portIdx, DRV_POS_SECTION_PERF *pSecPerf);
DRV_POS_RET drv_pos_highPathPerfQuery(BYTE chipId, BYTE portIdx, DRV_POS_VC4_PERF *pVc4Perf);
DRV_POS_RET drv_pos_sdhPerfFlagGet(BYTE chipId, WORD32 *pFlag);
DRV_POS_RET drv_pos_sdhPerfFlagSet(BYTE chipId, WORD32 flag);
DRV_POS_RET drv_pos_sdhSecPerfFlagGet(BYTE chipId, BYTE portIdx, WORD32 *pFlag);
DRV_POS_RET drv_pos_sdhSecPerfFlagSet(BYTE chipId, BYTE portIdx, WORD32 flag);
DRV_POS_RET drv_pos_sdhVc4PerfFlagGet(BYTE chipId, BYTE portIdx, WORD32 *pFlag);
DRV_POS_RET drv_pos_sdhVc4PerfFlagSet(BYTE chipId, BYTE portIdx, WORD32 flag);
/*DRV_POS_RET drv_pos_sdhPerfMonitor(const WORD32 *pDwSubslotId);*/
DRV_POS_RET drv_pos_sdhPerfThreadCreate(BYTE subslotId);
DRV_POS_RET drv_pos_sdhPerfThreadDelete(BYTE subslotId);
DRV_POS_RET drv_pos_sdhPerfThreadIdGet(BYTE chipId, int *pThreadId);

/*DRV_POS_RET drv_pos_txB1ErrForce(BYTE chipId, BYTE portIdx);
DRV_POS_RET drv_pos_txB1ErrUnForce(BYTE chipId, BYTE portIdx);
DRV_POS_RET drv_pos_txB2ErrForce(BYTE chipId, BYTE portIdx);
DRV_POS_RET drv_pos_txB2ErrUnForce(BYTE chipId, BYTE portIdx);
DRV_POS_RET drv_pos_txB3ErrForce(BYTE chipId, BYTE portIdx);
DRV_POS_RET drv_pos_txB3ErrUnForce(BYTE chipId, BYTE portIdx, BYTE au4Id);*/

DRV_POS_RET drv_pos_rsSdSfThresholdSet(BYTE chipId, BYTE portIdx, WORD32 sdsfType, DRV_POS_BER_RATE threshold);
DRV_POS_RET drv_pos_rsSdSfThresholdGet(BYTE chipId, BYTE portIdx, WORD32 sdsfType, DRV_POS_BER_RATE *pThreshold);
DRV_POS_RET drv_pos_msSdSfThresholdSet(BYTE chipId, BYTE portIdx, WORD32 sdsfType, WORD32 threshold);
DRV_POS_RET drv_pos_msSdSfThresholdGet(BYTE chipId, BYTE portIdx, WORD32 sdsfType, DRV_POS_BER_RATE *pThreshold);
DRV_POS_RET drv_pos_vc4SdSfThresholdSet(BYTE chipId, BYTE portIdx, WORD32 sdsfType, WORD32 threshold);
DRV_POS_RET drv_pos_vc4SdSfThresholdGet(BYTE chipId, BYTE portIdx, WORD32 sdsfType, DRV_POS_BER_RATE *pThreshold);
DRV_POS_RET drv_pos_sdhBerFlagSet(BYTE chipId, WORD32 flag);
DRV_POS_RET drv_pos_sdhBerFlagGet(BYTE chipId, WORD32 *pFlag);
/*DRV_POS_RET drv_pos_sdhBerMonitor(const WORD32 *pDwSubslotId);*/
DRV_POS_RET drv_pos_sdhBerThreadCreate(BYTE subslotId);
DRV_POS_RET drv_pos_sdhBerThreadDelete(BYTE subslotId);
DRV_POS_RET drv_pos_sdhBerThreadIdGet(BYTE chipId, int *pThreadId);

#endif


/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_pos.h
* 文件标识: 
* 其它说明: 15k pos模块初始化头文件
* 当前版本: V1.0
* 作    者: 刘钰00130907
* 完成日期: 2013-08-16
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/
#ifndef _DRV_POS_H_
#define _DRV_POS_H_
#include "drv_pos_comm.h"


DRV_POS_RET drv_pos_init(BYTE chipId);
DRV_POS_RET drv_pos_init_no_link(BYTE chipId);
DRV_POS_RET drv_pos_uninit(BYTE chipId);


#endif

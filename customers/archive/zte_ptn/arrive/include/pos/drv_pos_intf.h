/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_pos_intf.h
* 文件标识: 
* 其它说明: 驱动STM-1 POS 模块的接口部分的头文件
* 当前版本: V1.0
* 作    者: 刘钰00130907.
* 完成日期: 2013-05-16
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/

#ifndef _DRV_POS_INTF_H_
#define _DRV_POS_INTF_H_

#include "drv_pos_comm.h"
#include "AtEthPort.h"

typedef enum
{
    DRV_POS_ETH_PORT_UP = cAtEthPortAlarmLinkUp,     /* Ethernet interface link up. */
    DRV_POS_ETH_PORT_DOWN = cAtEthPortAlarmLinkDown  /* Ethernet interface link down. */
} DRV_POS_ETH_PORT_STATUS;       /* Ethernet interface link status. */

typedef enum
{
    DRV_POS_SGMII_SERDES_VOD = cAtSerdesParamVod,
    DRV_POS_SGMII_SERDES_PRE_TAP = cAtSerdesParamPreEmphasisPreTap,
    DRV_POS_SGMII_SERDES_FIRST_TAP = cAtSerdesParamPreEmphasisFirstPostTap,
    DRV_POS_SGMII_SERDES_SECOND_TAP = cAtSerdesParamPreEmphasisSecondPostTap
} DRV_POS_SGMII_SERDES_PARAM;

typedef enum
{
    DRV_POS_LOOPBACK_RELEASE = cAtLoopbackModeRelease,     /* 非环回 */
    DRV_POS_LOOPBACK_INTERNAL = cAtLoopbackModeLocal,      /* 内环 */
    DRV_POS_LOOPBACK_EXTERNAL = cAtLoopbackModeRemote,      /* 外环 */
    DRV_POS_LOOPBACK_MAX_SIZE
} DRV_POS_LOOPBACK_MODE;/*POS 环回状态，包括以太和SDH状态*/

typedef enum
{
    DRV_POS_PORT_SCRAMBLE_ENABLE = 0,    /* Enable scramble on tx direction. */
    DRV_POS_PORT_SCRAMBLE_DISABLE = 1,   /* Disable scramble on tx direction. */
    DRV_POS_PORT_SCRAMBLE_MAX_SIZE = 2
} DRV_POS_PORT_SCRAMBLE_STATE;        /* pos 端口扰码状态，SDH侧，hdlc上*/

typedef enum
{
    DRV_POS_PORT_TX_LASER_ENABLE = 0,
    DRV_POS_PORT_TX_LASER_DISABLE = 1,
    DRV_POS_PORT_TX_LASER_MAX_SIZE = 2
} DRV_POS_PORT_TX_LASER_STATE;        /* pos 端口激光器状态，SDH侧*/
 
typedef enum
{
    DRV_POS_PORT_NO_SHUTDOWN = 0,
    DRV_POS_PORT_SHUTDOWN = 1,
    DRV_POS_PORT_SHUTDOWN_STATE_MAX_SIZE = 2
} DRV_POS_PORT_SHUTDOWN_STATE;        /* pos 端口SHUTDOWN状态，SDH侧*/

typedef enum
{
    DRV_POS_PORT_NOFCS = 0,
    DRV_POS_PORT_FCS16 = 1,
    DRV_POS_PORT_FCS32 = 2 
}DRV_POS_PORT_FCS_MODE;              /* pos 端口SHUTDOWN状态，窄带侧，hdlc上*/

typedef struct
{
    DRV_POS_LOOPBACK_MODE loopbackMode;                 /* 端口环回模式 */
    DRV_POS_PORT_SCRAMBLE_STATE scrambleState;          /* 端口扰码状态 */
    DRV_POS_PORT_TX_LASER_STATE txLaserState;           /* 端口激光器发送状态 */
    DRV_POS_PORT_SHUTDOWN_STATE shutdownState;          /* 端口shutdown状态 */
    DRV_POS_PORT_FCS_MODE fcsMode;                      /* 设置的fcs模式*/
} DRV_POS_SDH_PORT_INFO;
 
/* 函数原型声明 */
DRV_POS_RET drv_pos_ethPortAlarmGet(BYTE chipId, BYTE ethIdx, DRV_POS_ETH_PORT_STATUS *pEthPortAlarm);
DRV_POS_RET drv_pos_sdhPortInfoMemGet(BYTE chipId, BYTE portIdx, DRV_POS_SDH_PORT_INFO** ppPortInfo);
DRV_POS_RET drv_pos_portLoopbackGet(BYTE chipId, BYTE portIdx, DRV_POS_LOOPBACK_MODE *pLoopbackMode);
DRV_POS_RET drv_pos_portLoopbackSet(BYTE chipId, BYTE portIdx, DRV_POS_LOOPBACK_MODE loopbackMode);
DRV_POS_RET drv_pos_portScrambleGet(BYTE chipId, BYTE portIdx, DRV_POS_PORT_SCRAMBLE_STATE *pScrambleEn);
DRV_POS_RET drv_pos_portScrambleSet(BYTE chipId, BYTE portIdx, DRV_POS_PORT_SCRAMBLE_STATE scrambleEn);
DRV_POS_RET drv_pos_portTxLaserStateGet(BYTE chipId, BYTE portIdx, DRV_POS_PORT_TX_LASER_STATE *pState);
DRV_POS_RET drv_pos_portTxLaserStateSet(BYTE chipId, BYTE portIdx, DRV_POS_PORT_TX_LASER_STATE state);
DRV_POS_RET drv_pos_portShutdownStateGet(BYTE chipId, BYTE portIdx, DRV_POS_PORT_SHUTDOWN_STATE *pState);
DRV_POS_RET drv_pos_portShutdownStateSet(BYTE chipId, BYTE portIdx, DRV_POS_PORT_SHUTDOWN_STATE state);
DRV_POS_RET drv_pos_portFcsModeGet(BYTE chipId, BYTE portIdx, DRV_POS_PORT_FCS_MODE *pFcsMode);
DRV_POS_RET drv_pos_portFcsModeSet(BYTE chipId, BYTE portIdx, DRV_POS_PORT_FCS_MODE fcsMode);

DRV_POS_RET drv_pos_ppp_mode_set(BYTE chipId, DRV_POS_PORT_GROUP portGrp, DRV_POS_PPP_MODE posPppMode);

#endif


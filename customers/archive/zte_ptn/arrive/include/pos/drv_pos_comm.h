/*************************************************************************
* ��Ȩ����(C)2013, ����ͨѶ�ɷ����޹�˾.
*
* �ļ�����: drv_pos_comm.h
* �ļ���ʶ: 
* ����˵��: 15k posģ�鹫������ͷ�ļ�������posģ���trace��¼��debug��
* ��ǰ�汾: V1.0
* ��    ��: ����00130907
* �������: 2013-08-15
*
* �޸ļ�¼: 
*    �޸�����: 
*    �汾��: V1.0
*    �޸���: 
*    �޸�����: create
**************************************************************************/

#ifndef _DRV_POS_COMM_H_
#define _DRV_POS_COMM_H_

#include "AtClasses.h"
#include "AtHal.h"
#include "AtSdhLine.h"
#include "stdio.h"
#include"../../../../../../common/Pub_TypeDef.h"
#include "drv_stm1_fpga.h"
#include "AtModuleEth.h"
#include "AtSdhChannel.h"
#include "AtDevice.h"
#include "bsp_oal_api.h"
   
#define ROSNG_TRACE_DEBUG printf

#define DRV_POS_MAX_4PORT             (BYTE)4
#define DRV_POS_MAX_8PORT             (BYTE)8
#define DRV_POS_MAX_PORT              DRV_POS_MAX_8PORT                  /*MAX port number for one subcard*/

#define DRV_POS_MAX_PORT_GROUP_NUM     (BYTE)2                 /*���ĸ��˿���һ���飬 ���ĸ��˿�һ����*/

#define DRV_POS_MAX_SUBCARD           4

#define DRV_POS_AT_DEVICE_NUM_NBIC    1                     /*15k-2ϵͳ NBIC�߿���һ��driver��һ��device*/
#define DRV_POS_AT_DEVICE_NUM_HPFUDZ  DRV_POS_MAX_SUBCARD   /*��15k-2ϵͳ HPFUDZ�߿���һ��driver��4��device*/

#define DRV_POS_ETHERNET_PORT_NUM     2
#define DRV_POS_MAC_SIZE              6

#define DRV_POS_AT_TX_IPG             ((BYTE)12)  /* Number of TX Inter Packet Gap Clock for ARRIVE */
#define DRV_POS_AT_RX_IPG             ((BYTE)4)   /* Number of RX Inter Packet Gap Clock for ARRIVE */

#define DRV_POS_DEFAULT_TX_C2         0x16

#define DRV_POS_TRUE                  1
#define DRV_POS_FALSE                 0

#define DRV_POS_ENABLE                1
#define DRV_POS_DISABLE               0

#define DRV_POS_AU4                   (BYTE)1

#define DRV_POS_TX_DIRECTION          0
#define DRV_POS_RX_DIRECTION          1  

typedef enum
{
    DRV_POS_LEFT_PORT_GRP,      /*���һ��*/
    DRV_POS_RIGHT_PORT_GRP,     /*�ұ�һ��*/
    DRV_POS_PORT_GRP_MAX
}DRV_POS_PORT_GROUP;

typedef enum
{
    DRV_POS_STM_RATE_INVALID,
    DRV_POS_STM_RATE_STM1,
    DRV_POS_STM_RATE_STM4
}DRV_POS_STM_RATE_MODE;

typedef enum
{
    DRV_POS_PPP_155,
    DRV_POS_PPP_622,
}DRV_POS_PPP_MODE;

typedef enum
{
    DRV_POS_INIT_START,
    DRV_POS_INIT_VAR_READY,
    DRV_POS_INIT_AT_READY,
    DRV_POS_INIT_INFCONFIG_READY,
    DRV_POS_INIT_PPP_CREATE_READY,
    DRV_POS_INIT_END
}DRV_POS_INIT_STATE;

typedef enum
{
    DRV_POS_OK,                                 /* 0 -- success */
    DRV_POS_AT_ERR,                             /* 1 -- at core op error */
    DRV_POS_PARAM_ERR,                          /* 2 -- param check error */
    DRV_POS_PT_ERR,                             /* 3 -- pointer check error */

    DRV_POS_CHIPID_ERR,                         /* 4 -- chipId check error */
    DRV_POS_ETHERPORT_ERR,                      /* 5 -- ethernet idx check error*/
    DRV_POS_PORT_ERR,                           /* 6 -- SDH idx check error*/
    
    DRV_POS_BASEADD_ERR,                        /* 7 -- baseAdd check error*/
    DRV_POS_BASEADD_NOT_INIT,                   /* 8 */
    
    DRV_POS_DRIVER_ALREADY_CREATE,              /* 9 */
    DRV_POS_DRIVER_NOT_CREATE,                  /* 10 */
    DRV_POS_DRIVER_DELETE_ERROR,                /* 11 */

    DRV_POS_DEVICE_MAX_NUM_ERROR,               /* 12 */
    DRV_POS_DEVICE_ALREADY_CREATE,              /* 13 */
    DRV_POS_DEVICE_NOT_CREATE,                  /* 14 */
    DRV_POS_DEVICE_DELETE_ERROR,                /* 15 */

    DRV_POS_HAL_ALREADY_CREATE,                 /* 16 */
    DRV_POS_HAL_NOT_CREATE,                     /* 17 */
    DRV_POS_HAL_DELETE_ERROR,                   /* 18 */

    DRV_POS_IPCORE_NUM_ERR,                     /* 19 */

    DRV_POS_ETH_FLOW_ALREADY_CREATE,            /* 20 */
    DRV_POS_ETH_FLOW_NOT_CREATE,                /* 21 */
    DRV_POS_ETH_FLOW_DELETE_ERROR,              /* 22 */
    
    DRV_POS_HDLC_CHANNEL_ALREADY_CREATE,        /* 23 */
    DRV_POS_HDLC_CHANNEL_NOT_CREATE,            /* 24 */
    DRV_POS_HDLC_CHANNEL_DELETE_ERROR,          /* 25 */

    DRV_POS_THREAD_ERR,                         /* 26 */
    
    DRV_POS_STM_MODE_ERR,                       /* 27 */
    
    DRV_POS_ERROR                               /* 28 -- other error*/ 
}DRV_POS_RET;
typedef struct
{
    WORD32   sysType;        /*ϵͳ����*/
    WORD32   *baseAdd;
    AtDriver atDriver;
    AtDevice atDevice;
    AtHal    atHal;
    WORD32   stmMode[DRV_POS_MAX_PORT_GROUP_NUM];
}DRV_POS_INFO;

typedef struct
{
    AtHdlcChannel atHdlcChannel;
    AtEthFlow     atEthFlow;
}DRV_POS_LINK_INFO;

typedef struct
{
    BYTE pri            :    3;     /* ���ȼ�priority */
    BYTE cfi            :    1;     /* cfi ��׼�ֶ�*/
    BYTE pkt_indicator  :    1;     /* ���з���ҵ��ת����ʶ��Ϊ����CPU����*/
    BYTE encap_type     :    5;     /* ��ʾ�ñ��ı�ʾ����*/
    BYTE pkt_len        :    6;     /* �����ʾ���ĳ��ȴ���60�ֽڣ���0*/
}DRV_POS_VLAN1;

typedef struct
{
    BYTE pri            :    3;     /* ���ȼ�priority*/
    BYTE cfi            :    1;     /* cfi ��׼�ֶ�*/
    BYTE port_no        :    3;     /* STM-1�˿ڱ�ţ�24CE1ͳһ���Ϊ0����˿ڱ��Ϊ0 */
    BYTE is_aggregate   :    1;     /* channelNo*/
    BYTE channel_num;
}DRV_POS_VLAN2;

typedef enum
{
    DRV_POS_PERF_DEBUG_OFF,
    DRV_POS_SDH_SEC_PERF_DBG,
    DRV_POS_SDH_HIG_PERF_DBG,
    DRV_POS_PPP_PERF_DBG
}DRV_POS_PERF_DEBUG;

#define DRV_POS_PRINT(err_format, args...) \
do{\
    ROSNG_TRACE_DEBUG(err_format, ##args);\
    ROSNG_TRACE_DEBUG("\n");\
}while(0)

#define DRV_POS_SUCCESS_PRINT(err_format, args...) \
do{\
    BSP_Print(BSP_DEBUG_ALL, "\n");\
    BSP_Print(BSP_DEBUG_ALL, "[DRV POS MODULE SUCCESS]: ");\
    BSP_Print(BSP_DEBUG_ALL, err_format, ##args);\
    BSP_Print(BSP_DEBUG_ALL, "\n");\
}while(0)


#define DRV_POS_ERROR_PRINT(err_format, args...) \
do{\
    if(g_pos_err)\
    {\
        ROSNG_TRACE_DEBUG("\n");\
        ROSNG_TRACE_DEBUG("[DRV POS MODULE ERROR]: function %s, line %d\n", __FUNCTION__, __LINE__);\
        ROSNG_TRACE_DEBUG("[DRV POS MODULE ERROR DETAIL]:");\
        ROSNG_TRACE_DEBUG(err_format, ##args);\
        ROSNG_TRACE_DEBUG("\n");\
    }\
}while(0)

#define DRV_POS_ERROR_LOG(err_format, args...) \
do{\
    BSP_Print(BSP_DEBUG_ALL, "\n");\
    BSP_Print(BSP_DEBUG_ALL, "[DRV POS MODULE ERROR]: function %s, line %d\n", __FUNCTION__, __LINE__);\
    BSP_Print(BSP_DEBUG_ALL, "[DRV POS MODULE ERROR DETAIL]:");\
    BSP_Print(BSP_DEBUG_ALL, err_format, ##args);\
    BSP_Print(BSP_DEBUG_ALL, "\n");\
}while(0)

#define DRV_POS_ERROR_RET(ret, err_format, args...) \
do{\
    DRV_POS_ERROR_LOG(err_format, ##args);\
    return (ret);\
}while(0)

#define DRV_POS_IF_ERROR_RET(ret, err_format, args...) \
do{\
    if(DRV_POS_OK != (ret))\
    {\
        DRV_POS_ERROR_RET(ret, err_format, ##args);\
    }\
}while(0)

#define DRV_POS_IF_AT_ERROR_RET(atRet, err_format, args...)\
do{\
    if(cAtOk != (atRet))\
    {\
        DRV_POS_ERROR_RET(DRV_POS_AT_ERR, err_format, ##args);\
    }\
}while(0)

#define DRV_POS_IF_AT_ERROR_LOG(atRet, err_format, args...)\
do{\
    if(cAtOk != (atRet))\
    {\
        DRV_POS_ERROR_LOG(err_format, ##args);\
    }\
}while(0)


#define DRV_POS_POINTER_CHECK(pt, ret, err_format, args...)\
do{\
    if(NULL == (pt))\
    {\
        DRV_POS_ERROR_RET(ret, err_format, ##args);\
    }\
}while(0)

#define DRV_POS_CHIPID_CHECK(chipId) \
do{\
    if((chipId) >= DRV_POS_MAX_SUBCARD) \
    { \
        DRV_POS_ERROR_RET(DRV_POS_CHIPID_ERR, "chipId is out of range, chipId = %d", chipId); \
    } \
}while(0)

#define DRV_POS_ETHPORTIDX_CHECK(ethIdx) \
do{\
    if(!(((ethIdx) < DRV_POS_ETHERNET_PORT_NUM) && ((ethIdx) >= 0)))\
    {\
        DRV_POS_ERROR_RET(DRV_POS_ETHERPORT_ERR, "ethIdx is out of range, ethIdx = %d", ethIdx);\
    }\
}while(0)

#define DRV_POS_PORTIDX_CHECK(chipId, portIdx) \
do{\
    if(DRV_POS_OK != drv_pos_portIdx_isLegal(chipId, portIdx))\
    {\
        return DRV_POS_PORT_ERR;\
    }\
}while(0)
#define DRV_POS_AU4_CHECK(au4) \
do{\
    if((au4) != DRV_POS_AU4)\
    {\
        DRV_POS_ERROR_RET(DRV_POS_PORT_ERR, "au4 = %d", au4);\
    }\
}while(0)

    
extern WORD32 g_pos_perf_debug;
extern WORD32 g_pos_err;
extern BYTE g_pos_suc;
extern BYTE g_drv_pos_boardMac[DRV_POS_MAC_SIZE];
extern BYTE g_drv_pos_ePortMac[DRV_POS_MAC_SIZE];
extern int __gettid(void);

DRV_POS_RET drv_pos_init_state_clear(BYTE chipId);
DRV_POS_RET drv_pos_global_info_clear(BYTE chipId);
DRV_POS_RET drv_pos_link_info_clear(BYTE chipId);
DRV_POS_RET drv_pos_get_init_state(BYTE chipId, DRV_POS_INIT_STATE * pInitState);
DRV_POS_RET drv_pos_save_init_state(BYTE chipId, DRV_POS_INIT_STATE initState);
DRV_POS_RET drv_pos_get_BaseAddress(BYTE chipId, WORD32** ppBaseAdd);
DRV_POS_RET drv_pos_save_BaseAddress(BYTE chipId, WORD32 * pBaseAdd);
DRV_POS_RET drv_pos_get_systemType(BYTE chipId, WORD32* pSystemType);
DRV_POS_RET drv_pos_save_systemType(BYTE chipId, WORD32 systemType);
DRV_POS_RET drv_pos_get_stmMode(BYTE chipId, DRV_POS_PORT_GROUP portGrp, DRV_POS_STM_RATE_MODE* pStmMode);
DRV_POS_RET drv_pos_save_stmMode(BYTE chipId, DRV_POS_PORT_GROUP portGrp, DRV_POS_STM_RATE_MODE stmMode);
WORD32 drv_pos_get_atMaxDeviceNum(BYTE chipId);
DRV_POS_RET drv_pos_get_valid_port_map(BYTE chipId, BYTE *pPortMap, BYTE *pPortNum);
DRV_POS_RET drv_pos_get_atDriver(BYTE chipId, AtDriver *pAtDriver);
DRV_POS_RET drv_pos_save_atDriver(BYTE chipId, AtDriver atDriver);
DRV_POS_RET drv_pos_get_atDevice(BYTE chipId, AtDevice *pAtDevice);
DRV_POS_RET drv_pos_save_atDevice(BYTE chipId, AtDevice atDevice);
DRV_POS_RET drv_pos_get_atHal(BYTE chipId, AtHal *pAtHal);
DRV_POS_RET drv_pos_save_atHal(BYTE chipId, AtHal atHal);
DRV_POS_RET drv_pos_get_atEthFlow(BYTE chipId, BYTE portIdx, AtEthFlow *pAtEthFlow);
DRV_POS_RET drv_pos_save_atEthFlow(BYTE chipId, BYTE portIdx, AtEthFlow atEthFlow);
DRV_POS_RET drv_pos_get_atHdlcChannel(BYTE chipId, BYTE portIdx, AtHdlcChannel *pAtHdlcChannel);
DRV_POS_RET drv_pos_save_atHdlcChannel(BYTE chipId, BYTE portIdx, AtHdlcChannel atHdlcChannel);
DRV_POS_RET drv_pos_get_atModuleSdh(BYTE chipId, AtModuleSdh *pAtModuleSdh);
DRV_POS_RET drv_pos_get_atModuleBer(BYTE chipId, AtModuleBer *pAtModuleBer);
DRV_POS_RET drv_pos_get_atSdhLine(BYTE chipId, BYTE portIdx, AtSdhLine * pAtSdhLine);
DRV_POS_RET drv_pos_get_atSerdesController(BYTE chipId, BYTE portIdx, AtSerdesController* pAtSerdesCtrler);
DRV_POS_RET drv_pos_get_atSdhVC(BYTE chipId, BYTE portIdx, AtSdhVc * pAtSdhVc4);
DRV_POS_RET drv_pos_get_atSdhAu(BYTE chipId, BYTE portIdx, AtSdhAu * pAtSdhAug);
DRV_POS_RET drv_pos_get_atModuleEth(BYTE chipId, AtModuleEth *pAtModuleEth);
DRV_POS_RET drv_pos_get_atEthPort(BYTE chipId, BYTE ethIdx, AtEthPort * pAtEthPort);
DRV_POS_RET drv_pos_get_atModuleEncap(BYTE chipId, AtModuleEncap *pAtModuleEncap);
DRV_POS_RET drv_pos_get_atHdlcLink(BYTE chipId, BYTE portIdx, AtHdlcLink *pAtHdlcLink);
DRV_POS_RET drv_pos_portIdx_isLegal(BYTE chipId, BYTE portIdx);

#endif


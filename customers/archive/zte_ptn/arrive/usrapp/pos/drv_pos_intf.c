/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_pos_intf.c
* 文件标识: 
* 其它说明: 驱动 POS 模块的接口端口源代码.
* 当前版本: V1.0
* 作    者: 刘钰 00130907.
* 完成日期: 2013-08-30
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/
#include "drv_pos_intf.h"
#include "drv_pos_sdh_alarm.h"
#include "drv_pos.h"

#include "AtHdlcChannel.h"

/* 全局变量定义 */
/* 数组g_drv_pos_sdh_port_info用来保存pos端口配置信息 */
static DRV_POS_SDH_PORT_INFO g_drv_pos_sdh_port_info[DRV_POS_MAX_SUBCARD][DRV_POS_MAX_PORT];


/**************************************************************************
* 函数名称: drv_pos_ethPortLoopbackModeSet
* 功能描述: 设置以太口的环回方式
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           ethIdx: ARRIVE芯片的Ethernet接口索引,取值为0~1.
*           loopbackMode: 类型 DRV_POS_LOOPBACK_MODE
*                         取值 DRV_POS_LOOPBACK_RELEASE  0
*                              DRV_POS_LOOPBACK_INTERNAL 1
*                              DRV_POS_LOOPBACK_EXTERNAL 2
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰 00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_ethPortLoopbackModeSet(BYTE chipId, BYTE ethIdx, DRV_POS_LOOPBACK_MODE loopbackMode)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    AtEthPort atEthPort = NULL;      /* Ethernet port */
    eAtRet atRet = cAtOk;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_ETHPORTIDX_CHECK(ethIdx);

    ret = drv_pos_get_atEthPort(chipId, ethIdx, &atEthPort);
    DRV_POS_POINTER_CHECK(atEthPort, ret, "chip[%d] ethIdx[%d] drv_pos_get_atEthPort failed", 
        chipId, ethIdx);

    if((DRV_POS_LOOPBACK_RELEASE != loopbackMode) && (DRV_POS_LOOPBACK_INTERNAL != loopbackMode) 
        && (DRV_POS_LOOPBACK_EXTERNAL != loopbackMode))
    {
        DRV_POS_ERROR_RET(DRV_POS_PARAM_ERR, "chip[%d] ethIdx[%d] loopback mode parament is invalid", 
            chipId, ethIdx);
    }

    /* Set loopback for Ethernet interface */
    atRet = AtChannelLoopbackSet((AtChannel)atEthPort, (BYTE)loopbackMode);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] ethIdx[%d] AtChannelLoopbackSet failed, return %s, atEthPort = %p, lbMode = %d",
        chipId, ethIdx, AtRet2String(atRet), atEthPort, loopbackMode);
    
    return ret;
}


/**************************************************************************
* 函数名称: drv_pos_sgmiiSerdesGet
* 功能描述: 获取SGMII接口的SERDES配置信息.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 子槽位号,取值为0~3.
*           ethIdx: SGMII接口的索引号,取值为0~1.
*           phyParameterId: 参数ID,具体参见DRV_POS_SGMII_SERDES_PARAM定义.
* 输出参数: *pValue: 保存SERDES的值
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰 00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sgmiiSerdesGet(BYTE chipId, BYTE ethIdx, DRV_POS_SGMII_SERDES_PARAM phyParameterId, WORD32 *pValue)
{
    DRV_POS_RET ret = DRV_POS_OK;
    WORD32 serdesValue = 0;
   
    AtEthPort atEthPort = NULL;      /* Ethernet port */
    AtSerdesController atSerdesController = NULL;
    eAtSerdesParam param = 0;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_ETHPORTIDX_CHECK(ethIdx);
    DRV_POS_POINTER_CHECK(pValue, DRV_POS_PT_ERR, "chip[%d] ethIdx[%d] input pValue is NULL", chipId, ethIdx);

    if((DRV_POS_SGMII_SERDES_VOD != phyParameterId) && (DRV_POS_SGMII_SERDES_PRE_TAP != phyParameterId)
        && (DRV_POS_SGMII_SERDES_FIRST_TAP != phyParameterId) && (DRV_POS_SGMII_SERDES_SECOND_TAP != phyParameterId))
    {
        DRV_POS_ERROR_RET(DRV_POS_PARAM_ERR, "chip[%d] ethIdx[%d] phyParameterId = %d, is invalid", 
            chipId, ethIdx, phyParameterId);
    }

    ret = drv_pos_get_atEthPort(chipId, ethIdx, &atEthPort);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] ethIdx[%d] drv_pos_get_atEthPort failed, return %d", chipId, ethIdx, ret);
    
    atSerdesController = AtEthPortSerdesController(atEthPort);  /* Get SERDES controller */
    DRV_POS_POINTER_CHECK(atSerdesController, DRV_POS_AT_ERR, 
        "chipId[%d] ethIdx[%d] AtEthPortSerdesController failed, atEthPort = %p",
        chipId, ethIdx, atEthPort);
    param = phyParameterId;
    /* Get configured value of physical parameter */
    serdesValue = AtSerdesControllerPhysicalParamGet(atSerdesController, param);
    
    *pValue = 0;  /* 先清零再赋值 */
    *pValue = serdesValue;
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_sgmiiSerdesSet
* 功能描述: 设置SGMII接口的SERDES配置信息.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: CP3BAN单板的子槽位号,取值为0~3.
*           ethIdx: SGMII接口的索引号,取值为0~1.
*           phyParameterId: 参数ID,具体参见DRV_POS_SGMII_SERDES_PARAM定义.
*           value: 需要设置的值.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰 00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sgmiiSerdesSet(BYTE chipId, BYTE ethIdx, DRV_POS_SGMII_SERDES_PARAM phyParameterId, WORD32 value)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtEthPort atEthPort = NULL;      /* Ethernet port */
    AtSerdesController atSerdesController = NULL;
    eAtSerdesParam param = 0;
    eAtRet atRet = cAtOk;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_ETHPORTIDX_CHECK(ethIdx);
    
    if((DRV_POS_SGMII_SERDES_VOD != phyParameterId) && (DRV_POS_SGMII_SERDES_PRE_TAP != phyParameterId)
        && (DRV_POS_SGMII_SERDES_FIRST_TAP != phyParameterId) && (DRV_POS_SGMII_SERDES_SECOND_TAP != phyParameterId))
    {
        DRV_POS_ERROR_RET(DRV_POS_PARAM_ERR, "chip[%d] ethIdx[%d] phyParameterId = %d, is invalid", 
            chipId, ethIdx, phyParameterId);
    }

    ret = drv_pos_get_atEthPort(chipId, ethIdx, &atEthPort);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] ethIdx[%d] drv_pos_get_atEthPort failed, return %d", chipId, ethIdx, ret);
    
    
    atSerdesController = AtEthPortSerdesController(atEthPort);  /* Get SERDES controller */
    DRV_POS_POINTER_CHECK(atSerdesController, DRV_POS_AT_ERR, 
        "chipId[%d] ethIdx[%d] AtEthPortSerdesController failed, atEthPort = %p",
        chipId, ethIdx, atEthPort);

    param = phyParameterId;
    
    atRet = AtSerdesControllerPhysicalParamSet(atSerdesController, param, value); /* Set physical parameter */
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] ethIdx[%d] AtSerdesControllerPhysicalParamSet failed, return %s, atSerdesController = %p, param = %d, value = %d",
        chipId, ethIdx, AtRet2String(atRet), atSerdesController, param, value);
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_ethPortAlarmGet
* 功能描述: 获取ARRVIE芯片的Ethernet接口的告警.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           ethIdx: ARRIVE芯片的Ethernet接口索引,取值为0~1.
* 输出参数: *pEthPortAlarm: 用来保存Ethernet接口的告警. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰 00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_ethPortAlarmGet(BYTE chipId, BYTE ethIdx, DRV_POS_ETH_PORT_STATUS *pEthPortAlarm)
{
    DRV_POS_RET ret = DRV_POS_OK;
    eAtEthPortAlarmType atEthPortAlm = 0;
    AtEthPort atEthPort = NULL;      /* Ethernet port */

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_ETHPORTIDX_CHECK(ethIdx);
    DRV_POS_POINTER_CHECK(pEthPortAlarm, DRV_POS_PT_ERR, "chip[%d] ethIdx[%d] pEthPortAlarm is NULL", chipId, ethIdx);

    ret = drv_pos_get_atEthPort(chipId, ethIdx, &atEthPort);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] ethIdx[%d] drv_pos_get_atEthPort failed", chipId, ethIdx);

    /* Get alarm of the ethernet interface. */
    atEthPortAlm = AtChannelAlarmGet((AtChannel)atEthPort);

    *pEthPortAlarm = (DRV_POS_ETH_PORT_STATUS)atEthPortAlm;/*宏定义，内容相同*/
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_sdhIntfMemGet
* 功能描述: 获取保存SDH端口配置信息的内存
* 访问的表: 软件表g_drv_pos_sdh_port_info
* 修改的表: 无
* 输入参数: chipId: 0~3, 
*           portIdx: 0~7.
* 输出参数: ppPortInfo,指向DRV_POS_SDH_PORT_INFO的指针的指针
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰 00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhPortInfoMemGet(BYTE chipId, BYTE portIdx, DRV_POS_SDH_PORT_INFO** ppPortInfo)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(ppPortInfo, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] ppIntf is NULL", chipId, portIdx);

    *ppPortInfo = &(g_drv_pos_sdh_port_info[chipId][portIdx]);
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_portLoopbackSave
* 功能描述: 保存端口环回方式.
* 访问的表: 软件表g_drv_pos_sdh_port_info
* 修改的表: 软件表g_drv_pos_sdh_port_info
* 输入参数: chipId: 0~3, 
*           portIdx: 0~7.
*           loopbackMode: 类型 DRV_POS_LOOPBACK_MODE
*                         取值 DRV_POS_LOOPBACK_RELEASE  0
*                              DRV_POS_LOOPBACK_INTERNAL 1
*                              DRV_POS_LOOPBACK_EXTERNAL 2
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰 00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_portLoopbackSave(BYTE chipId, BYTE portIdx, DRV_POS_LOOPBACK_MODE loopbackMode)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    
    g_drv_pos_sdh_port_info[chipId][portIdx].loopbackMode = loopbackMode;
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_portLoopbackGet
* 功能描述: 获取端口的环回方式
* 访问的表: 软件表g_drv_pos_sdh_port_info.
* 修改的表: 软件表g_drv_pos_sdh_port_info.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口的编号,取值为0~7.
* 输出参数: *pLoopbackMode: 保存SDH端口的环回方式.  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰 00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_portLoopbackGet(BYTE chipId, BYTE portIdx, DRV_POS_LOOPBACK_MODE *pLoopbackMode)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pLoopbackMode, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] input pLbMode is NULL", chipId, portIdx);
    
    *pLoopbackMode = 0;   /* 先清零再赋值 */
    *pLoopbackMode = g_drv_pos_sdh_port_info[chipId][portIdx].loopbackMode;
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_portLoopbackSet
* 功能描述: 设置SDH端口的环回方式
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口的编号,取值为0~7.
*           loopbackMode: 类型 DRV_POS_SDH_LOOPBACK_MODE
*                         取值 DRV_POS_SDH_LOOPBACK_RELEASE  0
*                              DRV_POS_SDH_LOOPBACK_INTERNAL 1
*                              DRV_POS_SDH_LOOPBACK_EXTERNAL 2
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰 00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_portLoopbackSet(BYTE chipId, BYTE portIdx, DRV_POS_LOOPBACK_MODE loopbackMode)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    BYTE atDevLpMd = 0;      
    AtSdhLine atSdhLine = NULL;
    eAtRet atRet = cAtOk;
    WORD32 intfState = DRV_POS_PORT_NO_SHUTDOWN;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    if((DRV_POS_LOOPBACK_RELEASE != loopbackMode) && (DRV_POS_LOOPBACK_INTERNAL!= loopbackMode) 
        && (DRV_POS_LOOPBACK_EXTERNAL != loopbackMode))
    {
        DRV_POS_ERROR_RET(DRV_POS_PARAM_ERR, "chip[%d] ethIdx[%d] lbMd = %d, is invalid", 
            chipId, portIdx, loopbackMode);
    }

    ret = drv_pos_portShutdownStateGet(chipId, portIdx, &intfState);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_portShutdownStateGet failed, return %d",
        chipId, portIdx, ret);

    if(DRV_POS_PORT_NO_SHUTDOWN == intfState)/*如果当前状态是no shutdown状态，则loopback是什么就给芯片下什么*/
    {
    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
        DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d",
            chipId, portIdx, ret);

        atDevLpMd = loopbackMode;

        atRet = AtChannelLoopbackSet((AtChannel)atSdhLine, atDevLpMd);
        DRV_POS_IF_AT_ERROR_RET(atRet, 
            "chip[%d] portIdx[%d] AtChannelLoopbackSet failed, return %s, atSdhLine = %p, atDevLpMd = %d",
            chipId, portIdx, AtRet2String(atRet), atSdhLine, atDevLpMd);        
    }
    else /*如果当前端口是shutdown状态，则只记录loopback操作，不进行实际芯片操作*/
    {
        ;
    }
    
    /* 保存端口环回方式. */
    ret = drv_pos_portLoopbackSave(chipId, portIdx, (BYTE)loopbackMode);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_portLoopbackSave failed, lbMd = %d",
        chipId, portIdx, loopbackMode);
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_portScrambleSave
* 功能描述: 保存POS的hdlc扰码方式.
* 访问的表: 软件表g_drv_pos_sdh_port_info.
* 修改的表: 软件表g_drv_pos_sdh_port_info.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portIdx: SDH端口的编号,取值为0~7.
*           scrambleEn: 类型 DRV_POS_PORT_SCRAMBLE_STATE
*                       取值 DRV_POS_PORT_SCRAMBLE_ENABLE  0
*                            DRV_POS_PORT_SCRAMBLE_DISABLE 1
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰 00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_portScrambleSave(BYTE chipId, BYTE portIdx, DRV_POS_PORT_SCRAMBLE_STATE scrambleEn)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    
    g_drv_pos_sdh_port_info[chipId][portIdx].scrambleState = scrambleEn;
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_portScrambleGet
* 功能描述: 获取SDH端口hdlc的扰码方式.
* 访问的表: 软件表g_drv_pos_sdh_port_info.
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portIdx: SDH端口的编号,取值为0~7.
* 输出参数: *pScrambleEn: 保存扰码方式.具体参见DRV_POS_PORT_SCRAMBLE_STATE定义 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰 00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_portScrambleGet(BYTE chipId, BYTE portIdx, DRV_POS_PORT_SCRAMBLE_STATE *pScrambleEn)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pScrambleEn, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pointer pScrambleEn is NULL", 
        chipId, portIdx);

    *pScrambleEn = 0;   /* 先清零再赋值 */
    *pScrambleEn = g_drv_pos_sdh_port_info[chipId][portIdx].scrambleState;
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_txStm1ScrambleSet
* 功能描述: 设置发送方向的STM-1帧是否扰码.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portIdx: SDH端口的编号,取值为0~7.
*           scrambleEn: 类型 DRV_POS_PORT_SCRAMBLE_STATE
*                       取值 DRV_POS_PORT_SCRAMBLE_ENABLE  0
*                            DRV_POS_PORT_SCRAMBLE_DISABLE 1
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰 00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_portScrambleSet(BYTE chipId, BYTE portIdx, DRV_POS_PORT_SCRAMBLE_STATE scrambleEn)
{
    DRV_POS_RET ret = DRV_POS_OK;
    eBool atDevScrambleEn = 0;
    AtHdlcChannel atHdlcChannel = NULL;
    eAtRet atRet = cAtOk;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_get_atHdlcChannel(chipId, portIdx, &atHdlcChannel);
    if(DRV_POS_HDLC_CHANNEL_NOT_CREATE == ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atHdlcChannel failed, return %d, ",
            chipId, portIdx, ret);
    }
    
    if (DRV_POS_PORT_SCRAMBLE_DISABLE == scrambleEn)
    {
        atDevScrambleEn = cAtFalse;
    }
    else if (DRV_POS_PORT_SCRAMBLE_ENABLE == scrambleEn)
    {
        atDevScrambleEn = cAtTrue;
    }
    else
    {
        /*参数输入错误时，配置成默认模式*/
        atDevScrambleEn = cAtTrue;
    }
    
    /* Enable/disable scramble */
    atRet = AtHdlcChannelScrambleEnable(atHdlcChannel, atDevScrambleEn);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] AtHdlcChannelScrambleEnable failed, return %s, atDevScrambleEn = %d",
        chipId, portIdx, AtRet2String(atRet), atDevScrambleEn);

    ret = drv_pos_portScrambleSave(chipId, portIdx, scrambleEn);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_portScrambleSave failed, return %d, scrambelEn = %d",
            chipId, portIdx, ret, scrambleEn);
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_portTxLaserStateSave
* 功能描述: 保存发送方向的STM接口的Laser状态.
* 访问的表: 软件表g_drv_pos_sdh_port_info.
* 修改的表: 软件表g_drv_pos_sdh_port_info.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portIdx: SDH端口的编号,取值为0~7.
*           state: 具体参见DRV_POS_STM_INTF_TX_LASER_STATE定义.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-17   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_portTxLaserStateSave(BYTE chipId, BYTE portIdx, DRV_POS_PORT_TX_LASER_STATE state)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    g_drv_pos_sdh_port_info[chipId][portIdx].txLaserState = state;
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_portTxLaserStateGet
* 功能描述: 获取发送方向的STM-1接口的Laser状态.
* 访问的表: g_drv_pos_sdh_port_info
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portIdx: SDH端口的编号,取值为0~7.
* 输出参数: *pState: 保存激光器的状态,具体参见DRV_POS_PORT_TX_LASER_STATE定义.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-15   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_portTxLaserStateGet(BYTE chipId, BYTE portIdx, DRV_POS_PORT_TX_LASER_STATE *pState)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pState, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pointer pState is NULL");
    
    *pState = 0;  /* 先清零再赋值 */
    *pState = g_drv_pos_sdh_port_info[chipId][portIdx].txLaserState;
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_stm1IntfTxLaserStateSet
* 功能描述: 设置发送方向的STM-1接口的Laser状态.
* 访问的表: g_drv_pos_sdh_port_info
* 修改的表: g_drv_pos_sdh_port_info
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portIdx: SDH端口的编号,取值为0~7.
*           state: 具体参见DRV_POS_PORT_TX_LASER_STATE定义
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-17   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_portTxLaserStateSet(BYTE chipId, BYTE portIdx, DRV_POS_PORT_TX_LASER_STATE state)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtSerdesController atSerdesController = NULL;
    eBool txLaserState = 0;
    eAtRet atRet = cAtOk;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_get_atSerdesController(chipId, portIdx, &atSerdesController);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] get serdesController failed", chipId, portIdx);

    if (DRV_POS_PORT_TX_LASER_ENABLE == state)
    {
        txLaserState = cAtTrue;
    }
    else if (DRV_POS_PORT_TX_LASER_DISABLE == state)
    {
        txLaserState = cAtFalse;
    }
    else
    {
        DRV_POS_ERROR_RET(DRV_POS_PARAM_ERR, "chip[%d] portIdx[%d] drv_pos_portTxLaserStateSet failed, state is %d", 
            chipId, portIdx, state);
    }

    /* Enable/Disbale tx laser of STM interface */
    atRet = AtSerdesControllerEnable(atSerdesController, txLaserState);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] AtSerdesControllerEnable failed, return %s, atSerdesController = %p, txLaserState = %d",
        chipId, portIdx, AtRet2String(atRet), atSerdesController, txLaserState);

    /* 保存软件表 */
    ret = drv_pos_portTxLaserStateSave(chipId, portIdx, state);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_portTxLaserStateSave failed, return %d, state = %d",
        chipId, portIdx, ret, state);
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_portShutdownStateSave
* 功能描述: 保存端口shutdown的状态.
* 访问的表: 软件表g_drv_pos_sdh_port_info.
* 修改的表: 软件表g_drv_pos_sdh_port_info.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portIdx: SDH端口的编号,取值为0~7.
*           state: 具体参见DRV_POS_PORT_SHUTDOWN_STATE定义.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-17   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_portShutdownStateSave(BYTE chipId, BYTE portIdx, DRV_POS_PORT_SHUTDOWN_STATE state)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    g_drv_pos_sdh_port_info[chipId][portIdx].shutdownState = state;
    
    return DRV_POS_OK;
}



/**************************************************************************
* 函数名称: drv_pos_portShutdownStateGet
* 功能描述: 获取端口shutdown状态.
* 访问的表: 软件表g_drv_pos_sdh_port_info.
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portIdx: SDH端口的编号,取值为0~7.
* 输出参数: *pState: 保存端口shutdown状态,具体参见DRV_POS_PORT_SHUTDOWN_STATE定义. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-15   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_portShutdownStateGet(BYTE chipId, BYTE portIdx, DRV_POS_PORT_SHUTDOWN_STATE *pState)
{    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pState, DRV_POS_PT_ERR, "chip[%d] portIdx[%d]  pointer pState is NULL");

    *pState = 0;  /* 先清零再赋值 */
    *pState = g_drv_pos_sdh_port_info[chipId][portIdx].shutdownState;
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_portShutdownStateSet
* 功能描述: 设置端口shutdown状态.
* 访问的表: g_drv_pos_sdh_port_info
* 修改的表: g_drv_pos_sdh_port_info
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portIdx: SDH端口的编号,取值为0~7.
*           state: 具体参见DRV_POS_PORT_SHUTDOWN_STATE定义
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-17   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_portShutdownStateSet(BYTE chipId, BYTE portIdx, DRV_POS_PORT_SHUTDOWN_STATE state)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtSdhLine atSdhLine = NULL;
    eAtRet atRet = cAtOk;
    WORD32 loopbackMode = DRV_POS_LOOPBACK_RELEASE;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    /* Enable/Disbale tx laser of STM interface */
    ret = drv_pos_portTxLaserStateSet(chipId, portIdx, state);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_portTxLaserStateSet failed, return %d, state = %d", 
        chipId, portIdx, ret, state);
        
    if (DRV_POS_PORT_NO_SHUTDOWN == state)
    {
        ret = drv_pos_rxRsLosUnForce(chipId, portIdx);
        DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_rxRsLosForce failed, return %d",
            chipId, portIdx, ret);
    }
    else if (DRV_POS_PORT_SHUTDOWN == state)
    {
        ret = drv_pos_rxRsLosForce(chipId, portIdx);
        DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_rxRsLosForce failed, return %d",
            chipId, portIdx, ret);
    }
    else
    {
        DRV_POS_ERROR_RET(DRV_POS_PARAM_ERR, "chip[%d] portIdx[%d] drv_pos_portShutdownStateSet failed, state is %d", 
            chipId, portIdx, state);
    }
    /* 需要先保存软件表，放在后面会导致noshutdown的时候loopback不起作用 */
    ret = drv_pos_portShutdownStateSave(chipId, portIdx, state);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_portShutdownStateSave failed, return %d, state = %d",
        chipId, portIdx, ret, state);

    ret = drv_pos_portLoopbackGet(chipId, portIdx, &loopbackMode);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_portLoopbackGet failed, return %d", chipId, portIdx, ret);
    
    if(DRV_POS_LOOPBACK_RELEASE != loopbackMode)/*如果当前状态是环回状态，则根据要设置的端口状态来设置环回状态*/
    {
        if(DRV_POS_PORT_NO_SHUTDOWN == state)/*如果是要将端口no shut dowm，则要先恢复端口的环回状态，避免配置丢失*/
        {
            ret = drv_pos_portLoopbackSet(chipId, portIdx, loopbackMode);
            DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_portLoopbackSet failed, return %d",
                chipId, portIdx, ret);
}
        else/*否则如果是要将端口shutdowm，则只关闭端口的芯片环回状态，不更改软件表项，避免配置丢失*/
        {            
            ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
            DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] get SdhLine failed", chipId, portIdx);

            atRet = AtChannelLoopbackSet(atSdhLine, cAtLoopbackModeRelease);
            DRV_POS_IF_AT_ERROR_RET(atRet, 
                "chip[%d] portIdx[%d] AtChannelLoopbackSet cAtLoopbackModeRelease failed, return %s, atSdhLine = %p",
                chipId, portIdx, AtRet2String(atRet), atSdhLine);
        }   
    }

    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_portFcsModeSave
* 功能描述: 保存POS端口fcs模式.
* 访问的表: 软件表g_drv_pos_sdh_port_info.
* 修改的表: 软件表g_drv_pos_sdh_port_info.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portIdx: SDH端口的编号,取值为0~7.
*           fcsMode: 具体参见DRV_POS_PORT_FCS_MODE定义.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-29   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_portFcsModeSave(BYTE chipId, BYTE portIdx, DRV_POS_PORT_FCS_MODE fcsMode)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    g_drv_pos_sdh_port_info[chipId][portIdx].fcsMode = fcsMode;
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_portFcsModeGet
* 功能描述: 获取POS端口fcs模式
* 访问的表: 软件表g_drv_pos_sdh_port_info.
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portIdx: SDH端口的编号,取值为0~7.
* 输出参数: *pFcsMode: 保存端口的状态,具体参见DRV_POS_PORT_FCS_MODE定义. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-29   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_portFcsModeGet(BYTE chipId, BYTE portIdx, DRV_POS_PORT_FCS_MODE *pFcsMode)
{    
    DRV_POS_RET ret = DRV_POS_OK;
    AtHdlcChannel atHdlcChannel = NULL;
    eAtHdlcFcsMode atFcsMode = cAtHdlcFcsModeNoFcs;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pFcsMode, DRV_POS_PT_ERR, "chip[%d] portIdx[%d]  pointer pFcsMode is NULL");

    /*获取hdlc channel*/
    ret = drv_pos_get_atHdlcChannel(chipId, portIdx, &atHdlcChannel);
    if(DRV_POS_HDLC_CHANNEL_NOT_CREATE == ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] portIdx[%d] get hdlcChannel failed", chipId, portIdx);
    }
    
    atFcsMode = AtHdlcChannelFcsModeGet(atHdlcChannel);
    switch(atFcsMode)
    {
        case cAtHdlcFcsModeNoFcs:
            pFcsMode = DRV_POS_PORT_NOFCS;
            break;
        case cAtHdlcFcsModeFcs16:
            pFcsMode = DRV_POS_PORT_FCS16;
            break;
        case cAtHdlcFcsModeFcs32:
            pFcsMode = DRV_POS_PORT_FCS32;
            break;
        default:
            DRV_POS_ERROR_RET(ret, "chip[%d] portIdx[%d] AtHdlcChannelFcsModeGet failed, get fcsMode = %d",
                chipId, portIdx, atFcsMode);
            break;

    }
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_portFcsStateSet
* 功能描述: 设置STM-1接口的状态.
* 访问的表: g_drv_pos_sdh_port_info
* 修改的表: g_drv_pos_sdh_port_info
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portIdx: SDH端口的编号,取值为0~7.
*           fcsMode: 具体参见DRV_POS_PORT_FCS_MODE定义
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-29   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_portFcsModeSet(BYTE chipId, BYTE portIdx, DRV_POS_PORT_FCS_MODE fcsMode)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtHdlcChannel atHdlcChannel = NULL;
    eAtRet atRet = cAtOk;
    eAtHdlcFcsMode atFcsMode = cAtHdlcFcsModeNoFcs;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    /*获取hdlc channel*/
    ret = drv_pos_get_atHdlcChannel(chipId, portIdx, &atHdlcChannel);
    if(DRV_POS_HDLC_CHANNEL_NOT_CREATE == ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] portIdx[%d] get hdlcChannel failed", chipId, portIdx);
    }
    
    if(DRV_POS_PORT_FCS16 == fcsMode)
    {
        atFcsMode = cAtHdlcFcsModeFcs16;
    }
    else if(DRV_POS_PORT_FCS32 == fcsMode)
    {
        atFcsMode = cAtHdlcFcsModeFcs32;
    }
    else if(DRV_POS_PORT_NOFCS == fcsMode)
    {
        atFcsMode = cAtHdlcFcsModeNoFcs;
    }
    else
    {
        ;
    }
    
    atRet = AtHdlcChannelFcsModeSet(atHdlcChannel, atFcsMode);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] AtSerdesControllerEnable failed, return %s, atHdlcChannel = %p, atFcsMode = %d",
        chipId, portIdx, AtRet2String(atRet), atHdlcChannel, atFcsMode);

    /* 保存软件表 */
    ret = drv_pos_portFcsModeSave(chipId, portIdx, fcsMode);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_portFcsModeSave failed, return %d, fcsMode = %d",
        chipId, portIdx, ret, fcsMode);
    
    return DRV_POS_OK;
}
/**************************************************************************
* 函数名称: drv_pos_ppp_mode_set
* 功能描述: 左右端口组POS模式切换
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portGrp: 端口组，左端口组或右端口组，详见DRV_POS_PORT_GROUP定义
*           posPppMode: 具体参见DRV_POS_PPP_MODE定义
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 该函数用来进行POS模式切换(目前支持155、622)
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-12-9   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_ppp_mode_set(BYTE chipId, DRV_POS_PORT_GROUP portGrp, DRV_POS_PPP_MODE posPppMode)
{
    DRV_POS_RET ret = DRV_POS_OK;
    DRV_POS_STM_RATE_MODE stmModeCur = DRV_POS_STM_RATE_INVALID;
    DRV_POS_STM_RATE_MODE stmModeSet = DRV_POS_STM_RATE_INVALID;

    DRV_POS_CHIPID_CHECK(chipId);
    /*用posPppMode映射到stmMode中*/
    if(DRV_POS_PPP_155 == posPppMode)
    {
        stmModeSet = DRV_POS_STM_RATE_STM1;
    }
    else if(DRV_POS_PPP_622 == posPppMode)
    {
        stmModeSet = DRV_POS_STM_RATE_STM4;
    }
    
    /*获取当前stm模式*/
    ret = drv_pos_get_stmMode(chipId, portGrp, &stmModeCur);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portGrp[%d] drv_pos_get_stmMode failed", chipId, portGrp);
    /*如果当前模式和要设置的模式一致，则记录并返回成功*/
    if(stmModeCur == stmModeSet)
    {
        DRV_POS_ERROR_RET(DRV_POS_OK, "stmModeCur = %d, stmModeSet = %d, log but not return error", stmModeCur, stmModeSet);
    }

    switch(posPppMode)
    {
        case DRV_POS_PPP_155: /*从622切换到155*/
            ret = drv_atPos_pppModeSet155(chipId, portGrp);
            DRV_POS_IF_ERROR_RET(ret, "chip[%d] portGrp[%d] drv_atPos_pppModeSet155 failed, return %d", chipId, portGrp, ret);

            break;
        case DRV_POS_PPP_622: /*从155切换到622*/
            ret = drv_atPos_pppModeSet622(chipId, portGrp);
            DRV_POS_IF_ERROR_RET(ret, "chip[%d] portGrp[%d] drv_atPos_pppModeSet622 failed, return %d", chipId, portGrp, ret);
            break;
    }
    
    return DRV_POS_OK;
}



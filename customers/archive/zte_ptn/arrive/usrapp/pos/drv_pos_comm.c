/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_pos_comm.c
* 文件标识: 
* 其它说明: 15k pos模块公共代码，包括pos模块的trace记录、debug等
* 当前版本: V1.0
* 作    者: 刘钰00130907
* 完成日期: 2013-08-15
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/
#include "drv_pos_comm.h"
#include "drv_pos_diag.h"
#include "AtModuleEth.h"
#include "AtEthPort.h"
#include "AtSdhChannel.h"
#include "AtSdhLine.h"
#include "AtSdhPath.h"
#include "AtSdhAug.h"
#include "AtSdhVc.h"
#include "AtHdlcChannel.h"

static DRV_POS_INIT_STATE g_drv_pos_init_state[DRV_POS_MAX_SUBCARD];
static DRV_POS_INFO g_drv_pos_share_info[DRV_POS_MAX_SUBCARD];
static DRV_POS_LINK_INFO g_drv_pos_link_info[DRV_POS_MAX_SUBCARD][DRV_POS_MAX_PORT];

BYTE g_drv_pos_boardMac[DRV_POS_MAC_SIZE] = {0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA};
BYTE g_drv_pos_ePortMac[DRV_POS_MAC_SIZE] = {0xD0, 0xDA, 0xD0, 0xDA, 0xD0, 0xDA};
WORD32 g_pos_err;
BYTE g_pos_suc;
WORD32 g_pos_perf_debug;;


/*********************************************************************
* 函数名称：drv_pos_init_state_clear
* 功能描述：清空保存pos初始化状态的全局变量表
*           
* 访问的表：pos初始化状态的全局变量表 
*           DRV_POS_INIT_STATE g_drv_pos_init_state[DRV_POS_MAX_SUBCARD];
* 修改的表：
* 输入参数: chipId:   芯片序列号，范围0~3
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
*          
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/10/22            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_init_state_clear(BYTE chipId)
{
    /*check chipid*/
    DRV_POS_CHIPID_CHECK(chipId);

    memset(&g_drv_pos_init_state[chipId], 0, sizeof(g_drv_pos_init_state[chipId]));
    
    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_pos_global_info_clear
* 功能描述：清空保存pos全局信息的全局变量表
*           
* 访问的表：保存POS全局信息的全局变量表
*           DRV_POS_INFO g_drv_pos_share_info[DRV_POS_MAX_SUBCARD]
* 修改的表：
* 输入参数: chipId:   芯片序列号，范围0~3
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
*          
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/9/26            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_global_info_clear(BYTE chipId)
{
    /*check chipid*/
    DRV_POS_CHIPID_CHECK(chipId);

    memset(&g_drv_pos_share_info[chipId], 0, sizeof(g_drv_pos_share_info[chipId]));
    
    return DRV_POS_OK;
}
/*********************************************************************
* 函数名称：drv_pos_link_info_clear
* 功能描述：清空保存pos链路信息的全局变量表
*           
* 访问的表：保存POS全局信息的全局变量表
*           DRV_POS_LINK_INFO g_drv_pos_link_info[DRV_POS_MAX_SUBCARD][DRV_POS_MAX_PORT]
* 修改的表：
* 输入参数: chipId:   芯片序列号，范围0~3
* 输出参数：
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
*          
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/9/26            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_link_info_clear(BYTE chipId)
{
    /*check chipid*/
    DRV_POS_CHIPID_CHECK(chipId);

    memset(g_drv_pos_link_info[chipId], 0, sizeof(g_drv_pos_link_info[chipId]));
    
    return DRV_POS_OK;
}
/*********************************************************************
* 函数名称：drv_pos_get_init_state
* 功能描述：获取POS初始化状态
*           
* 访问的表：pos初始化状态的全局变量表 
*           DRV_POS_INIT_STATE g_drv_pos_init_state[DRV_POS_MAX_SUBCARD];
* 修改的表：
* 输入参数: chipId:   芯片序列号，范围0~3
* 输出参数：pInitState: 初始化状态指针
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/10/22            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_get_init_state(BYTE chipId, DRV_POS_INIT_STATE* pInitState)
{
    /*check chipid*/
    DRV_POS_CHIPID_CHECK(chipId);
    /*check input pointer*/
    DRV_POS_POINTER_CHECK(pInitState, DRV_POS_PT_ERR, "chip[%d] pointer pInitState is NULL", chipId);

    *pInitState = g_drv_pos_init_state[chipId];
    
    return DRV_POS_OK;
}
/*********************************************************************
* 函数名称：drv_pos_save_init_state
* 功能描述：存储POS初始化状态
*           
* 访问的表：pos初始化状态的全局变量表 
*           DRV_POS_INIT_STATE g_drv_pos_init_state[DRV_POS_MAX_SUBCARD];
* 修改的表：pos初始化状态的全局变量表 
*           DRV_POS_INIT_STATE g_drv_pos_init_state[DRV_POS_MAX_SUBCARD];
* 输入参数: chipId:   芯片序列号，范围0~3
*           initState: 要存储的pos卡初始化状态
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/10/22            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_save_init_state(BYTE chipId, DRV_POS_INIT_STATE initState)
{
    /*check chipid*/
    DRV_POS_CHIPID_CHECK(chipId);

    g_drv_pos_init_state[chipId] = initState;

    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_pos_get_BaseAddress
* 功能描述：获取POS卡的基地址
*           
* 访问的表：保存POS全局信息的全局变量表
*           DRV_POS_INFO g_drv_pos_share_info[DRV_POS_MAX_SUBCARD]
* 修改的表：
* 输入参数: chipId:   芯片序列号，范围0~3
* 输出参数：ppBaseAdd: 基地址的指针
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_get_BaseAddress(BYTE chipId, WORD32** ppBaseAdd)
{
    /*check chipid*/
    DRV_POS_CHIPID_CHECK(chipId);
    /*check input pointer*/
    DRV_POS_POINTER_CHECK(ppBaseAdd, DRV_POS_PT_ERR, "chip[%d] pointer baseAdd is NULL", chipId);

    *ppBaseAdd = g_drv_pos_share_info[chipId].baseAdd;
    
    return DRV_POS_OK;
}
/*********************************************************************
* 函数名称：drv_pos_save_BaseAddress
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/9/26            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_save_BaseAddress(BYTE chipId, WORD32* pBaseAdd)
{
    /*check chipid*/
    DRV_POS_CHIPID_CHECK(chipId);
    /*不需要检查指针，不论指针是否等于都存入静态全局变量中*/

    g_drv_pos_share_info[chipId].baseAdd = pBaseAdd;
    
    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_pos_get_systemType
* 功能描述：获取POS卡的基地址
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_get_systemType(BYTE chipId, WORD32* pSystemType)
{
    /*check chipid*/
    DRV_POS_CHIPID_CHECK(chipId);
    /*check input pointer*/
    DRV_POS_POINTER_CHECK(pSystemType, DRV_POS_PT_ERR, "chip[%d] pointer pSystemType is NULL", chipId);

    *pSystemType = g_drv_pos_share_info[chipId].sysType;
    
    return DRV_POS_OK;
}
/*********************************************************************
* 函数名称：drv_pos_save_systemType
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/9/26            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_save_systemType(BYTE chipId, WORD32 systemType)
{
    /*check chipid*/
    DRV_POS_CHIPID_CHECK(chipId);

    g_drv_pos_share_info[chipId].sysType = systemType;
    
    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_pos_get_stmMode
* 功能描述：获取POS卡STM模式
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/11/15            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_get_stmMode(BYTE chipId, DRV_POS_PORT_GROUP portGrp, DRV_POS_STM_RATE_MODE* pStmMode)
{
    /*check chipid*/
    DRV_POS_CHIPID_CHECK(chipId);
    /*check input pointer*/
    DRV_POS_POINTER_CHECK(pStmMode, DRV_POS_PT_ERR, "chip[%d] pointer pSystemType is NULL", chipId);

    if(portGrp >= DRV_POS_PORT_GRP_MAX)
    {
        DRV_POS_ERROR_RET(DRV_POS_PORT_ERR, "chip[%d] drv_pos_get_stmMode failed, portGrp is not left or right");
    }

    *pStmMode = g_drv_pos_share_info[chipId].stmMode[portGrp];
    
    return DRV_POS_OK;
}
/*********************************************************************
* 函数名称：drv_pos_save_stmMode
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/11/15            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_save_stmMode(BYTE chipId, DRV_POS_PORT_GROUP portGrp, DRV_POS_STM_RATE_MODE stmMode)
{
    /*check chipid*/
    DRV_POS_CHIPID_CHECK(chipId);

    if(portGrp >= DRV_POS_PORT_GRP_MAX)
    {
        DRV_POS_ERROR_RET(DRV_POS_PORT_ERR, "chip[%d] drv_pos_save_stmMode failed, portGrp is not left or right");
    }

    g_drv_pos_share_info[chipId].stmMode[portGrp] = stmMode;
    
    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_pos_get_atDeviceNum
* 功能描述：获取POS卡的基地址
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
WORD32 drv_pos_get_atMaxDeviceNum(BYTE chipId)
{
    DRV_POS_RET ret = DRV_POS_OK;
    WORD32 systemType = 0;
    /*获取系统类型，从而决定创建device的个数*/
    ret = drv_pos_get_systemType(chipId, &systemType);
    if(DRV_POS_OK != ret)
    {
        /*不使用DRV_POS_ERROR_RET是因为此函数返回的是deviceNumber，当然是用它，返回DRV_POS_OK也行，不过可能会误导读代码的人*/
        DRV_POS_ERROR_LOG("chip[%d] get systemType failed, return is %d", chipId, ret);
        return 0;
    }

    if(0 == systemType)/* 非15K-2系统 */
    {
        return DRV_POS_AT_DEVICE_NUM_HPFUDZ;
    }
    else/*15k-2系统*/
    {
        return DRV_POS_AT_DEVICE_NUM_NBIC;
    }

}

/*********************************************************************
* 函数名称：drv_pos_get_max_port
* 功能描述：根据board类型，获取最大端口数
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/11/12            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_get_max_port(BYTE chipId, BYTE *pMaxPortNum)
{
    DRV_POS_RET ret = DRV_POS_OK;
    WORD32 boardType = 0;

    /*check chipid*/
    DRV_POS_CHIPID_CHECK(chipId);

    ret = BSP_cp3ban_boardTypeGet(chipId+1, &boardType);
    if (BSP_E_CP3BAN_OK != (WORD32)ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] BSP_cp3ban_boardTypeGet failed, return %d", chipId, ret);
    }
    if (BSP_CP3BAN_8PORT_BOARD == boardType)
    {
        *pMaxPortNum = DRV_POS_MAX_8PORT;
    }
    else if (BSP_CP3BAN_4PORT_BOARD == boardType)
    {
        *pMaxPortNum = DRV_POS_MAX_4PORT;
    }
    else
    {
        DRV_POS_ERROR_RET(DRV_POS_PORT_ERR, "chip[%d] BSP_cp3ban_boardTypeGet boardType is wrong, boardType = %d", 
            chipId, boardType);
    }
    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_pos_get_valid_port_map
* 功能描述：获取端口表，和有效端口个数
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/11/12            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_get_valid_port_map(BYTE chipId, BYTE *pPortMap, BYTE *pPortNum)
{
    DRV_POS_RET ret = DRV_POS_OK;
    BYTE maxPortNum = 0;
    DRV_POS_STM_RATE_MODE stmRateMode = DRV_POS_STM_RATE_INVALID;
    WORD32 i = 0;
    WORD32 mapIdx = 0;/*pPortMap的index，从0开始*/
    WORD32 portGrpStart = 0;/*一组端口映射开始的实际端口序列号*/
    WORD32 portGrpNum = 0; /*一组端口的数量*/

    /*check chipid*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_POINTER_CHECK(pPortMap, ret, "chip[%d] pointer pPortMap is NULL", chipId);
    DRV_POS_POINTER_CHECK(pPortNum, ret, "chip[%d] pointer pPortNum is NULL", chipId);

    *pPortNum = 0;
    memset(pPortMap, 0, sizeof(BYTE)*DRV_POS_MAX_PORT);

    /*获取左边四口的STM模式，并映射到pPortMap中*/
    ret = drv_pos_get_stmMode(chipId, DRV_POS_LEFT_PORT_GRP, &stmRateMode);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_stmMode failed, return %d", chipId, ret);

    switch(stmRateMode)
    {
        case DRV_POS_STM_RATE_STM1:
            portGrpNum = 4;
            break;
        case DRV_POS_STM_RATE_STM4:
            portGrpNum = 1;
            break;
        default:
            DRV_POS_ERROR_RET(DRV_POS_STM_MODE_ERR, "chip[%d] stmRate = %d", chipId, stmRateMode);
            break;
    }
    portGrpStart = 0;
    *pPortNum += portGrpNum;
    for(i = 0; i < portGrpNum; i++,mapIdx++)
    {
        pPortMap[mapIdx] = portGrpStart+i;
    }

    /*获取不考虑STM模式时，实际板最大端口数， 4口板还是8口板*/
    ret = drv_pos_get_max_port(chipId, &maxPortNum);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_max_port failed, return %d", chipId, ret);

    /*如果硬件是八口卡，则需要判断后面四口，否则不需要映射后面四口*/
    if(DRV_POS_MAX_8PORT == maxPortNum)
    {
        /*获取右边四口的STM模式，并映射到pPortMap中*/
        ret = drv_pos_get_stmMode(chipId, DRV_POS_RIGHT_PORT_GRP, &stmRateMode);
        DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_stmMode failed, portGrp = %d, return %d", 
            chipId, DRV_POS_RIGHT_PORT_GRP, ret);
        switch(stmRateMode)
        {
            case DRV_POS_STM_RATE_STM1:
                portGrpNum = 4;
                break;
            case DRV_POS_STM_RATE_STM4:
                portGrpNum = 1;
                break;
            default:
                DRV_POS_ERROR_RET(DRV_POS_STM_MODE_ERR, "chip[%d] stmRate = %d", chipId, stmRateMode);
                break;
        }
        portGrpStart = 4;
        *pPortNum += portGrpNum;
        for(i = 0; i < portGrpNum; i++,mapIdx++)
        {
            pPortMap[mapIdx] = portGrpStart+i;
        }

    }

    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_pos_portIdx_isLegal
* 功能描述：判断portIdx是否合法
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/11/13            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_portIdx_isLegal(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    BYTE maxPort = 0;

        /*check chipid*/
    DRV_POS_CHIPID_CHECK(chipId);

    ret = drv_pos_get_max_port(chipId, &maxPort);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_max_port failed, return %d", chipId, ret);

    if(portIdx >= maxPort)
    {
        DRV_POS_ERROR_RET(DRV_POS_PORT_ERR, "chip[%d] portIdx port is not legal, maxPortNum is %d", 
            chipId, portIdx, maxPort);
    }

    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_pos_get_atDriver
* 功能描述：获取AT的driver
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码 
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/28            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_get_atDriver(BYTE chipId, AtDriver *pAtDriver)
{
    /*check chipid and pointer*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_POINTER_CHECK(pAtDriver, DRV_POS_PT_ERR, "chip[%d] pointer atDriver is NULL", chipId);

    *pAtDriver = g_drv_pos_share_info[chipId].atDriver;

    if(NULL == *pAtDriver)
    {
        return DRV_POS_DRIVER_NOT_CREATE;
    }
    else
    {
        return DRV_POS_DRIVER_ALREADY_CREATE;
    }
}

/*********************************************************************
* 函数名称：drv_pos_save_atDriver
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/9/26            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_save_atDriver(BYTE chipId, AtDriver atDriver)
{
    /*check chipid*/
    DRV_POS_CHIPID_CHECK(chipId);

    g_drv_pos_share_info[chipId].atDriver = atDriver;
    
    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_pos_get_atDevice
* 功能描述：获取AT的driver
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_get_atDevice(BYTE chipId, AtDevice *pAtDevice)
{
    /*check chipid and port*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_POINTER_CHECK(pAtDevice, DRV_POS_PT_ERR, "chip[%d] pointer atDevice is NULL", chipId);

    *pAtDevice = g_drv_pos_share_info[chipId].atDevice;

    if(NULL == *pAtDevice)
    {
        return DRV_POS_DEVICE_NOT_CREATE;
    }
    else
    {
        return DRV_POS_DEVICE_ALREADY_CREATE;
    }

}

/*********************************************************************
* 函数名称：drv_pos_save_atDevice
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/9/26            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_save_atDevice(BYTE chipId, AtDevice atDevice)
{
    /*check chipid*/
    DRV_POS_CHIPID_CHECK(chipId);

    g_drv_pos_share_info[chipId].atDevice = atDevice;
    
    return DRV_POS_OK;
}


/*********************************************************************
* 函数名称：drv_pos_get_atHal
* 功能描述：获取AT的Hal
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码 
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_get_atHal(BYTE chipId, AtHal *pAtHal)
{
    /*check chipid and port*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_POINTER_CHECK(pAtHal, DRV_POS_PT_ERR, "chip[%d] pointer atHal is NULL", chipId);

    *pAtHal = g_drv_pos_share_info[chipId].atHal;

    if(NULL == *pAtHal)
    {
        return DRV_POS_HAL_NOT_CREATE;
    }
    else
    {
        return DRV_POS_HAL_ALREADY_CREATE;
    }
}

/*********************************************************************
* 函数名称：drv_pos_save_atHal
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/9/26            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_save_atHal(BYTE chipId, AtHal atHal)
{
    /*check chipid*/
    DRV_POS_CHIPID_CHECK(chipId);

    g_drv_pos_share_info[chipId].atHal = atHal;
    
    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_pos_get_atEthFlow
* 功能描述：获取单链路的EncapChannel
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_get_atEthFlow(BYTE chipId, BYTE portIdx, AtEthFlow *pAtEthFlow)
{
    /*check chipid and port*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pAtEthFlow, DRV_POS_PT_ERR, "chip[%d] pointer atEthFlow is NULL", chipId);

    *pAtEthFlow = g_drv_pos_link_info[chipId][portIdx].atEthFlow;
    if(NULL == *pAtEthFlow)
    {
        return DRV_POS_ETH_FLOW_NOT_CREATE;
    }
    else
    {
        return DRV_POS_ETH_FLOW_ALREADY_CREATE;
    }
}

/*********************************************************************
* 函数名称：drv_pos_save_atEthFlow
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_save_atEthFlow(BYTE chipId, BYTE portIdx, AtEthFlow atEthFlow)
{
    DRV_POS_RET ret = DRV_POS_OK;
    /*check chipid and port*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    g_drv_pos_link_info[chipId][portIdx].atEthFlow = atEthFlow;

    return ret;
}

/*********************************************************************
* 函数名称：drv_pos_get_atHdlcChannel
* 功能描述：获取单链路的EncapChannel
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_get_atHdlcChannel(BYTE chipId, BYTE portIdx, AtHdlcChannel *pAtHdlcChannel)
{
    /*check chipid and port*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pAtHdlcChannel, DRV_POS_PT_ERR, "chip[%d] pointer atEncapChannel is NULL", chipId);


    *pAtHdlcChannel = g_drv_pos_link_info[chipId][portIdx].atHdlcChannel;
    if(NULL == *pAtHdlcChannel)
    {
        return DRV_POS_HDLC_CHANNEL_NOT_CREATE;
    }
    else
    {
        return DRV_POS_HDLC_CHANNEL_ALREADY_CREATE;    
    }
    
}
/*********************************************************************
* 函数名称：drv_pos_save_atHdlcChannel
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_save_atHdlcChannel(BYTE chipId, BYTE portIdx, AtHdlcChannel atHdlcChannel)
{
    DRV_POS_RET ret = DRV_POS_OK;
    /*check chipid and port*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    g_drv_pos_link_info[chipId][portIdx].atHdlcChannel = atHdlcChannel;

    return ret;
}

/*********************************************************************
* 函数名称：drv_pos_get_atModuleSdh
* 功能描述：获取AtModuleSdh
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/28            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_get_atModuleSdh(BYTE chipId, AtModuleSdh *pAtModuleSdh)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtDevice atDev = NULL;
    /*check chipid and port*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_POINTER_CHECK(pAtModuleSdh, DRV_POS_PT_ERR, "chip[%d] pointer atModuleSdh is NULL", chipId);

    ret = drv_pos_get_atDevice(chipId, &atDev);
    if(DRV_POS_DEVICE_NOT_CREATE == ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] at driver not been created, drv_pos_get_atDriver return %d", chipId, ret);
    }

    /*Get Sdh module*/
    *pAtModuleSdh = (AtModuleSdh)AtDeviceModuleGet(atDev, cAtModuleSdh);
    DRV_POS_POINTER_CHECK(*pAtModuleSdh, DRV_POS_AT_ERR, "chip[%d] AtDeviceModuleGet failed, atDevice = %p", 
        chipId, atDev);

    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_pos_get_atModuleBer
* 功能描述：获取AtModuleBer
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/20            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_get_atModuleBer(BYTE chipId, AtModuleBer *pAtModuleBer)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtDevice atDev = NULL;
    /*check chipid and port*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_POINTER_CHECK(pAtModuleBer, DRV_POS_PT_ERR, "chip[%d] pointer atModuleSdh is NULL", chipId);

    ret = drv_pos_get_atDevice(chipId, &atDev);
    if(DRV_POS_DEVICE_NOT_CREATE == ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] at driver not been created, drv_pos_get_atDriver return %d", chipId, ret);
    }

    /*Get Sdh module*/
    *pAtModuleBer = (AtModuleBer)AtDeviceModuleGet(atDev, cAtModuleBer);
    DRV_POS_POINTER_CHECK(*pAtModuleBer, DRV_POS_AT_ERR, "chip[%d] AtDeviceModuleGet failed, atDevice = %p", 
        chipId, atDev);

    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_pos_get_atSdhLine
* 功能描述：获取单链路的SdhLine
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_get_atSdhLine(BYTE chipId, BYTE portIdx, AtSdhLine * pAtSdhLine)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtModuleSdh atModuleSdh = NULL;
    /*check chipid and port*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pAtSdhLine, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pointer atSdhLine is NULL", 
        chipId, portIdx);

    ret = drv_pos_get_atModuleSdh(chipId, &atModuleSdh);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_atModuleSdh failed, return %d", chipId, ret);
    
    /*Get SDH line */
    *pAtSdhLine = AtModuleSdhLineGet(atModuleSdh, portIdx);
    DRV_POS_POINTER_CHECK(*pAtSdhLine, DRV_POS_AT_ERR, "chip[%d] portIdx[%d] AtModuleSdhLineGet failed", 
        chipId, portIdx);

    return ret;
}

/*********************************************************************
* 函数名称：drv_pos_get_atSerdesController
* 功能描述：获取单链路的SdhLine
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_get_atSerdesController(BYTE chipId, BYTE portIdx, AtSerdesController* pAtSerdesCtrler)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtSdhLine atSdhLine = NULL;
    /*check chipid and port*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pAtSerdesCtrler, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pointer atSerdesCtrler is NULL", 
        chipId, portIdx);

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_atSdhLine failed, return %d", chipId, ret);
    
    /*Get SerdesController */
    *pAtSerdesCtrler = AtSdhLineSerdesController(atSdhLine);
    DRV_POS_POINTER_CHECK(*pAtSerdesCtrler, DRV_POS_AT_ERR, "chip[%d] portIdx[%d] AtSdhLineSerdesController failed", 
        chipId, portIdx);

    return ret;
}

/*********************************************************************
* 函数名称：drv_pos_get_atSdhVC4
* 功能描述：获取单链路的SdhLine
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_get_atSdhVC(BYTE chipId, BYTE portIdx, AtSdhVc * pAtSdhVc)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtSdhLine atSdhLine = NULL;
    DRV_POS_STM_RATE_MODE stmRateMode = DRV_POS_STM_RATE_INVALID;
    DRV_POS_PORT_GROUP portGrp = DRV_POS_LEFT_PORT_GRP;
    
    /*check chipid and port*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pAtSdhVc, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pointer pAtSdhVc is NULL",
        chipId, portIdx);

    /*Get Sdh Line*/
    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d", chipId, portIdx, ret);

    portGrp = (portIdx < 4)? DRV_POS_LEFT_PORT_GRP : DRV_POS_RIGHT_PORT_GRP;

    ret = drv_pos_get_stmMode(chipId, portGrp, &stmRateMode);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_stmMode failed, portGrp = %d, return %d", 
        chipId, portIdx, portGrp, ret);

    switch(stmRateMode)
    {
        case DRV_POS_STM_RATE_STM1:
            /*Get Sdh Vc4*/
            *pAtSdhVc = AtSdhLineVc4Get(atSdhLine, DRV_POS_AU4-(BYTE)1);
            DRV_POS_POINTER_CHECK(*pAtSdhVc, DRV_POS_AT_ERR, "chip[%d] port[%d] AtSdhLineVc4Get failed, sdh line = %p", 
                chipId, portIdx, atSdhLine);
            break;
        case DRV_POS_STM_RATE_STM4:
            /*Get Sdh Vc4*/
            *pAtSdhVc = AtSdhLineVc4_4cGet(atSdhLine, DRV_POS_AU4-(BYTE)1);
            DRV_POS_POINTER_CHECK(*pAtSdhVc, DRV_POS_AT_ERR, "chip[%d] port[%d] AtSdhLineVc4Get failed, sdh line = %p", 
                chipId, portIdx, atSdhLine);
            break;
        default:
            DRV_POS_ERROR_RET(ret, "chip[%d] stmRateMode is wrong, stmRateMode = %d", stmRateMode);
    }
    return ret;
}

/*********************************************************************
* 函数名称：drv_pos_get_atSdhAug
* 功能描述：获取单链路映射的Aug
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/11/18            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_get_atSdhAu(BYTE chipId, BYTE portIdx, AtSdhAu * pAtSdhAug)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtSdhLine atSdhLine = NULL;
    DRV_POS_STM_RATE_MODE stmRateMode = DRV_POS_STM_RATE_INVALID;
    DRV_POS_PORT_GROUP portGrp = DRV_POS_LEFT_PORT_GRP;
    
    /*check chipid and port*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pAtSdhAug, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pointer pAtSdhVc is NULL",
        chipId, portIdx);

    /*Get Sdh Line*/
    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d", chipId, portIdx, ret);

    portGrp = (portIdx < 4)? DRV_POS_LEFT_PORT_GRP : DRV_POS_RIGHT_PORT_GRP;

    ret = drv_pos_get_stmMode(chipId, portGrp, &stmRateMode);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] port[%d] drv_pos_get_stmMode failed, portGrp = %d, return %d", 
        chipId, portIdx, portGrp, ret);

    switch(stmRateMode)
    {
        case DRV_POS_STM_RATE_STM1:
            /*Get Sdh aug1*/
            *pAtSdhAug = AtSdhLineAu4Get(atSdhLine, DRV_POS_AU4-(BYTE)1);
            DRV_POS_POINTER_CHECK(*pAtSdhAug, DRV_POS_AT_ERR, "chip[%d] port[%d] AtSdhLineAug1Get failed, sdh line = %p", 
                chipId, portIdx, atSdhLine);
            break;
        case DRV_POS_STM_RATE_STM4:
            /*Get Sdh aug4_4c*/
            *pAtSdhAug = AtSdhLineAu4_4cGet(atSdhLine, DRV_POS_AU4-(BYTE)1);
            DRV_POS_POINTER_CHECK(*pAtSdhAug, DRV_POS_AT_ERR, "chip[%d] port[%d] AtSdhLineAu4_4cGet failed, sdh line = %p", 
                chipId, portIdx, atSdhLine);
            break;
        default:
            DRV_POS_ERROR_RET(ret, "chip[%d] stmRateMode is wrong, stmRateMode = %d", stmRateMode);
    }
    return ret;
}

/*********************************************************************
* 函数名称：drv_pos_get_atModuleEth
* 功能描述：获取AtModuleEth
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/28            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_get_atModuleEth(BYTE chipId, AtModuleEth *pAtModuleEth)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtDevice atDev = NULL;
    /*check chipid and port*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_POINTER_CHECK(pAtModuleEth, DRV_POS_PT_ERR, "chip[%d] pointer atModuleEth is NULL", chipId);

    ret = drv_pos_get_atDevice(chipId, &atDev);
    if(DRV_POS_DRIVER_NOT_CREATE == ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] at driver not been created, drv_pos_get_atDriver return %d", chipId, ret);
    }

    /*Get Sdh module*/
    *pAtModuleEth = (AtModuleEth)AtDeviceModuleGet(atDev, cAtModuleEth);
    DRV_POS_POINTER_CHECK(*pAtModuleEth, DRV_POS_AT_ERR, "chip[%d] AtDeviceModuleGet failed, atDevice = %p", 
        chipId, atDev);

    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_pos_get_at
* 功能描述：获取单链路的SdhLine
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_get_atEthPort(BYTE chipId, BYTE ethIdx, AtEthPort * pAtEthPort)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtModuleEth atModuleEth = NULL;
     
    /*check chipid and port*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_ETHPORTIDX_CHECK(ethIdx);
    DRV_POS_POINTER_CHECK(pAtEthPort, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] pointer atEthPort is NULL",
        chipId, ethIdx);

    /*Get ethernet module*/
    ret = drv_pos_get_atModuleEth(chipId, &atModuleEth);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_atModuleEth failed, return %d", chipId, ret);
        
    /* Get Ethernet port */
    *pAtEthPort = AtModuleEthPortGet(atModuleEth, ethIdx);
    DRV_POS_POINTER_CHECK(*pAtEthPort, DRV_POS_AT_ERR, "chip[%d] ethPortIdx[%d] AtModuleEthPortGet failed", chipId, ethIdx);
    
    return ret;
}

/*********************************************************************
* 函数名称：drv_pos_get_atModuleEncap
* 功能描述：获取AtModuleEncap
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/28            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_get_atModuleEncap(BYTE chipId, AtModuleEncap *pAtModuleEncap)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtDevice atDev = NULL;
    /*check chipid and port*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_POINTER_CHECK(pAtModuleEncap, DRV_POS_PT_ERR, "chip[%d] pointer atModuleEncap is NULL", chipId);

    ret = drv_pos_get_atDevice(chipId, &atDev);
    if(DRV_POS_DRIVER_NOT_CREATE == ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] at driver not been created, drv_pos_get_atDriver return %d", chipId, ret);
    }

    /*Get Sdh module*/
    *pAtModuleEncap = (AtModuleEncap)AtDeviceModuleGet(atDev, cAtModuleEncap);
    DRV_POS_POINTER_CHECK(*pAtModuleEncap, DRV_POS_AT_ERR, "chip[%d] AtDeviceModuleGet failed, atDevice = %p", chipId, atDev);

    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_pos_get_atHdlcChannel
* 功能描述：获取单链路的EncapChannel
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_get_atHdlcLink(BYTE chipId, BYTE portIdx, AtHdlcLink *pAtHdlcLink)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtHdlcChannel atHdlcChannel = NULL;

    /*check chipid and port*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pAtHdlcLink, DRV_POS_PT_ERR, "chip[%d] pointer atHdlcLink is NULL", chipId);

    ret = drv_pos_get_atHdlcChannel(chipId, portIdx, &atHdlcChannel);
    if(DRV_POS_HDLC_CHANNEL_NOT_CREATE == ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] portIdx[%d] atHdlcChannel not been create, drv_pos_get_atHdlcChannel return %d",
            chipId, portIdx, ret);
    }

    *pAtHdlcLink = (AtHdlcLink)AtHdlcChannelHdlcLinkGet(atHdlcChannel);
    DRV_POS_POINTER_CHECK(*pAtHdlcLink, DRV_POS_AT_ERR, 
        "chip[%d] AtHdlcChannelHdlcLinkGet failed, AtHdlcChannel = %p", 
        chipId, atHdlcChannel);

    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_pos_get_atModulePpp
* 功能描述：获取AtModulePpp
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/10/21            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_get_atModulePpp(BYTE chipId, AtModuleEncap *pAtModulePpp)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtDevice atDev = NULL;
    
    /*check chipid and port*/
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_POINTER_CHECK(pAtModulePpp, DRV_POS_PT_ERR, "chip[%d] pointer pAtModulePpp is NULL", chipId);

    ret = drv_pos_get_atDevice(chipId, &atDev);
    if(DRV_POS_DRIVER_NOT_CREATE == ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] at driver not been created, drv_pos_get_atDriver return %d", chipId, ret);
    }

    /*Get Ppp module*/
    *pAtModulePpp = (AtModuleEncap)AtDeviceModuleGet(atDev, cAtModulePpp);
    DRV_POS_POINTER_CHECK(*pAtModulePpp, DRV_POS_AT_ERR, "chip[%d] AtDeviceModuleGet failed, atDevice = %p", chipId, atDev);

    return DRV_POS_OK;
}


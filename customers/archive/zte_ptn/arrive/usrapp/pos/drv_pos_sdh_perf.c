/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_pos_sdh_perf.c
* 文件标识: 
* 其它说明: 驱动STM-1 POS PWE3模块的STM-1接口的SDH性能统计实现函数
* 当前版本: V1.0
* 作    者: 刘钰00130907.
* 完成日期: 2013-03-26
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/


#include "drv_pos_sdh_perf.h"

#include "AtSdhPath.h"

#include "bsp_cp3ban_common.h"


/* 全局变量定义 */
/* 使能SDH性能统计标记,默认非使能.在初始化单板时,使能标记;在卸载单板时,非使能标记.*/
static WORD32 g_drv_pos_sdh_perf_enable[DRV_POS_MAX_SUBCARD] = {0};
static WORD32 g_drv_pos_sdhSecPerfEnable[DRV_POS_MAX_SUBCARD][DRV_POS_MAX_PORT] = {{0}};
static WORD32 g_drv_pos_sdhVc4PerfEnable[DRV_POS_MAX_SUBCARD][DRV_POS_MAX_PORT] = {{0}};

/* SDH performance线程的threadId */
static int g_drv_pos_sdh_perf_thread_id[DRV_POS_MAX_SUBCARD];

/* g_drv_pos_sdh_perf_thread用来保存SDH performance线程的信息 */
static struct mod_thread g_drv_pos_sdh_perf_thread[DRV_POS_MAX_SUBCARD]; 

/* 全局数组g_drv_pos_sdh_perf用来保存STM-1帧的SDH性能统计计数 */
static DRV_POS_SDH_PERF g_drv_pos_sdh_perf[DRV_POS_MAX_SUBCARD][DRV_POS_MAX_PORT];

/* 使能SDH BER标记,默认非使能.在初始化单板时,使能标记;在卸载单板时,非使能标记.*/
static WORD32 g_drv_pos_sdh_ber_enable[DRV_POS_MAX_SUBCARD] = {0};  

/* g_drv_pos_sdh_ber_thread用来保存SDH BER线程的信息 */
static struct mod_thread g_drv_pos_sdh_ber_thread[DRV_POS_MAX_SUBCARD];

/* SDH BER线程的threadId */
static int g_drv_pos_sdh_ber_thread_id[DRV_POS_MAX_SUBCARD] = {0}; 

/**************************************************************************
* 函数名称: drv_pos_sdhPerfMemGet
* 功能描述: 获取保存SDH性能统计的内存
* 访问的表: 软件表g_drv_pos_sdh_perf
* 修改的表: 无
* 输入参数: chipId: 0~3
* 输出参数: ppPerf 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhPerfMemGet(BYTE chipId, BYTE portIdx, DRV_POS_SDH_PERF** ppPerf)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(ppPerf, DRV_POS_PT_ERR, 
        "chip[%d] portIdx[%d] input ppPerf is NULL", chipId, portIdx);
   
    *ppPerf = &(g_drv_pos_sdh_perf[chipId][portIdx]);
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhPerfClear
* 功能描述: 清空保存SDH性能统计的内存
* 访问的表: 软件表g_drv_pos_sdh_perf
* 修改的表: 软件表g_drv_pos_sdh_perf
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhPerfClear(BYTE chipId, BYTE portIdx)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    
    memset(&(g_drv_pos_sdh_perf[chipId][portIdx]), 0, sizeof(DRV_POS_SDH_PERF));
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sectionPerfSave
* 功能描述: 保存STM-1帧的段的性能统计
* 访问的表: 软件表g_drv_pos_sdh_perf
* 修改的表: 软件表g_drv_pos_sdh_perf
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sectionPerfSave(BYTE chipId, BYTE portIdx, const DRV_POS_SECTION_PERF *pSectionPerf)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pSectionPerf, DRV_POS_PT_ERR, 
        "chip[%d] port[%d] input pSectionPerf is NULL", chipId, portIdx);
    
    memset(&(g_drv_pos_sdh_perf[chipId][portIdx].sectionPerf), 0, sizeof(DRV_POS_SECTION_PERF));
    memcpy(&(g_drv_pos_sdh_perf[chipId][portIdx].sectionPerf), pSectionPerf, sizeof(DRV_POS_SECTION_PERF));

    if(DRV_POS_SDH_SEC_PERF_DBG == g_pos_perf_debug)
    {
        ROSNG_TRACE_DEBUG("**** chip[%d] portIdx[%d] section perf save ****\n", chipId, portIdx);
        ROSNG_TRACE_DEBUG("sectionPerf.b1Cnts = %d\n", g_drv_pos_sdh_perf[chipId][portIdx].sectionPerf.b1Cnts);
        ROSNG_TRACE_DEBUG("sectionPerf.b2Cnts = %d\n", g_drv_pos_sdh_perf[chipId][portIdx].sectionPerf.b2Cnts);
        ROSNG_TRACE_DEBUG("sectionPerf.msReiCnts = %d\n", g_drv_pos_sdh_perf[chipId][portIdx].sectionPerf.msReiCnts);
    }
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_highPathPerfSave
* 功能描述: 保存STM-1帧的高阶通道的性能统计
* 访问的表: 软件表g_drv_pos_sdh_perf
* 修改的表: 软件表g_drv_pos_sdh_perf
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_highPathPerfSave(BYTE chipId, BYTE portIdx, const DRV_POS_VC4_PERF *pVc4Perf)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pVc4Perf, DRV_POS_PT_ERR, 
        "chip[%d] portIdx[%d] input pVc4Perf is NULL", chipId, portIdx);
    
    memset(&(g_drv_pos_sdh_perf[chipId][portIdx].vc4Perf), 0, sizeof(DRV_POS_VC4_PERF));
    memcpy(&(g_drv_pos_sdh_perf[chipId][portIdx].vc4Perf), pVc4Perf, sizeof(DRV_POS_VC4_PERF));

    if(DRV_POS_SDH_HIG_PERF_DBG == g_pos_perf_debug)
    {      
        ROSNG_TRACE_DEBUG("**** chip[%d] portIdx[%d] HP perf save ****\n", chipId, portIdx);
        ROSNG_TRACE_DEBUG("vc4Perf.b3Cnts = %d\n", g_drv_pos_sdh_perf[chipId][portIdx].vc4Perf.b3Cnts);
        ROSNG_TRACE_DEBUG("vc4Perf.hpReiCnts = %d\n", g_drv_pos_sdh_perf[chipId][portIdx].vc4Perf.hpReiCnts);
        ROSNG_TRACE_DEBUG("vc4Perf.rxAu4Ppjc = %d\n", g_drv_pos_sdh_perf[chipId][portIdx].vc4Perf.rxAu4Ppjc);
        ROSNG_TRACE_DEBUG("vc4Perf.rxAu4Npjc = %d\n", g_drv_pos_sdh_perf[chipId][portIdx].vc4Perf.rxAu4Npjc);
        ROSNG_TRACE_DEBUG("vc4Perf.txAu4Ppjx = %d\n", g_drv_pos_sdh_perf[chipId][portIdx].vc4Perf.txAu4Ppjx);
        ROSNG_TRACE_DEBUG("vc4Perf.txAu4Npjc = %d\n", g_drv_pos_sdh_perf[chipId][portIdx].vc4Perf.txAu4Npjc);
    }
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sectionPerfGet
* 功能描述: 获取STM-1帧的段的性能统计
* 访问的表: 
* 修改的表: 
* 输入参数:  
* 输出参数: pSectionPerf: 保存段的性能统计
* 返 回 值: 
* 其它说明: 包括再生段告警和复用段的性能统计
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sectionPerfGet(BYTE chipId, BYTE portIdx, DRV_POS_SECTION_PERF *pSectionPerf)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    AtSdhLine atSdhLine = NULL;
    AtModuleSdh atModuleSdh = NULL;
    eAtRet atRet = cAtOk;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pSectionPerf, DRV_POS_PT_ERR, 
        "chip[%d] portIdx[%d] input pSectionPerf is NULL", chipId, portIdx);
    

    /*获取sdh module ，以做信号量保护*/
    ret = drv_pos_get_atModuleSdh(chipId, &atModuleSdh);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_atModuleSdh failed, return %d", chipId, ret);

    atRet = AtModuleLock((AtModule)atModuleSdh);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtModuleLock failed, return %s",
        chipId, portIdx, AtRet2String(atRet));

    /* Get line object */
    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    if(DRV_POS_OK != ret)
    {
        atRet = AtModuleUnLock((AtModule)atModuleSdh);
        DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtModuleUnLock failed, return %s",
            chipId, portIdx, AtRet2String(atRet));
       
        DRV_POS_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed");
    }
    
    memset(pSectionPerf, 0, sizeof(DRV_POS_SECTION_PERF)); /* 先清空再赋值 */
    /* Get B1 performance counter */
    pSectionPerf->b1Cnts += AtChannelCounterClear((AtChannel)atSdhLine, cAtSdhLineCounterTypeB1);
    /* Get B2 performance counter */
    pSectionPerf->b2Cnts += AtChannelCounterClear((AtChannel)atSdhLine, cAtSdhLineCounterTypeB2);
    /* Get MS REI performance counter */
    pSectionPerf->msReiCnts += AtChannelCounterClear((AtChannel)atSdhLine, cAtSdhLineCounterTypeRei);


    atRet = AtModuleUnLock((AtModule)atModuleSdh);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtModuleUnLock failed, return %s",
        chipId, portIdx, AtRet2String(atRet));

    if(DRV_POS_SDH_SEC_PERF_DBG == g_pos_perf_debug)
    {
        ROSNG_TRACE_DEBUG("**** chip[%d] portIdx[%d] section perf get ****\n", chipId, portIdx);
        ROSNG_TRACE_DEBUG("sectionPerf.b1Cnts = %d\n", pSectionPerf->b1Cnts);
        ROSNG_TRACE_DEBUG("sectionPerf.b2Cnts = %d\n", pSectionPerf->b2Cnts);
        ROSNG_TRACE_DEBUG("sectionPerf.msReiCnts = %d\n", pSectionPerf->msReiCnts);
    }
    
    return ret;
}


/**************************************************************************
* 函数名称: drv_pos_highPathPerfGet
* 功能描述: 获取STM-1帧的高阶通道的性能统计
* 访问的表: 
* 修改的表: 
* 输入参数:  
* 输出参数: pVc4Perf: 保存高阶通道的性能统计
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_highPathPerfGet(BYTE chipId, 
                               BYTE portIdx, 
                               DRV_POS_VC4_PERF *pVc4Perf)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    AtSdhVc atSdhVc4 = NULL;
    AtModuleSdh atModuleSdh = NULL;
    eAtRet atRet = cAtOk;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pVc4Perf, DRV_POS_PT_ERR, 
        "chip[%d] portIdx[%d] input pVc4Perf is NULL", chipId, portIdx);


    /*获取sdh module ，以做信号量保护*/
    ret = drv_pos_get_atModuleSdh(chipId, &atModuleSdh);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_atModuleSdh failed, return %d", chipId, ret);

    atRet = AtModuleLock((AtModule)atModuleSdh);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtModuleLock failed, return %s",
        chipId, portIdx, AtRet2String(atRet));

    ret = drv_pos_get_atSdhVC(chipId, portIdx, &atSdhVc4);
    if(DRV_POS_OK != ret)
    {
        
        atRet = AtModuleUnLock((AtModule)atModuleSdh);
        DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtModuleUnLock failed, return %s",
            chipId, portIdx, AtRet2String(atRet));
   
        DRV_POS_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhVC failed");
    }
    
    memset(pVc4Perf, 0, sizeof(DRV_POS_VC4_PERF));  /* 先清零再赋值 */
    /* Get B3 performance counter */
    pVc4Perf->b3Cnts += AtChannelCounterClear((AtChannel)atSdhVc4, cAtSdhPathCounterTypeBip);
    /* Get HP-REI performance counter */
    pVc4Perf->hpReiCnts += AtChannelCounterClear((AtChannel)atSdhVc4, cAtSdhPathCounterTypeRei);
    /* Get AU4 RX positive pointer justification count */
    pVc4Perf->rxAu4Ppjc += AtChannelCounterClear((AtChannel)atSdhVc4, cAtSdhPathCounterTypeRxPPJC);
    /* Get AU4 RX negative pointer justification count */
    pVc4Perf->rxAu4Npjc += AtChannelCounterClear((AtChannel)atSdhVc4, cAtSdhPathCounterTypeRxNPJC);
    /* Get AU4 TX positive pointer justification count */
    pVc4Perf->txAu4Ppjx += AtChannelCounterClear((AtChannel)atSdhVc4, cAtSdhPathCounterTypeTxPPJC);
    /* Get AU4 TX negative pointer justification count */
    pVc4Perf->txAu4Npjc += AtChannelCounterClear((AtChannel)atSdhVc4, cAtSdhPathCounterTypeTxNPJC);


    atRet = AtModuleUnLock((AtModule)atModuleSdh);
    DRV_POS_IF_AT_ERROR_RET(atRet, "chip[%d] portIdx[%d] AtModuleUnLock failed, return %s",
        chipId, portIdx, AtRet2String(atRet));

    if(DRV_POS_SDH_HIG_PERF_DBG == g_pos_perf_debug)
    {      
        ROSNG_TRACE_DEBUG("**** chip[%d] portIdx[%d] HP perf get ****\n", chipId, portIdx);
        ROSNG_TRACE_DEBUG("vc4Perf.b3Cnts = %d\n", pVc4Perf->b3Cnts);
        ROSNG_TRACE_DEBUG("vc4Perf.hpReiCnts = %d\n", pVc4Perf->hpReiCnts);
        ROSNG_TRACE_DEBUG("vc4Perf.rxAu4Ppjc = %d\n", pVc4Perf->rxAu4Ppjc);
        ROSNG_TRACE_DEBUG("vc4Perf.rxAu4Npjc = %d\n", pVc4Perf->rxAu4Npjc);
        ROSNG_TRACE_DEBUG("vc4Perf.txAu4Ppjx = %d\n", pVc4Perf->txAu4Ppjx);
        ROSNG_TRACE_DEBUG("vc4Perf.txAu4Npjc = %d\n", pVc4Perf->txAu4Npjc);
    }
    
    return ret;
}


/**************************************************************************
* 函数名称: drv_pos_sectionPerfQuery
* 功能描述: 查询段层的性能统计
* 访问的表: 无.
* 修改的表: 无.
* 输入参数: chipId: 0~3
*           portId: 1~8.
* 输出参数: *pSecPerf: 保存查询结果. 
* 返 回 值: 
* 其它说明: 该函数供产品管理调用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sectionPerfQuery(BYTE chipId, BYTE portIdx, DRV_POS_SECTION_PERF *pSecPerf)
{
    DRV_POS_RET ret = DRV_POS_OK;
    DRV_POS_SDH_PERF *pSdhPerf = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pSecPerf, DRV_POS_PT_ERR, 
        "chip[%d] portIdx[%d] input pSecPerf is NULL", chipId, portIdx);
    
    ret = drv_pos_sdhPerfMemGet(chipId, portIdx, &pSdhPerf);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_sdhPerfMemGet failed, return %d",
        chipId, portIdx, ret);
    
    memset(pSecPerf, 0, sizeof(DRV_POS_SECTION_PERF));  /* 先清零再赋值 */
    memcpy(pSecPerf, &(pSdhPerf->sectionPerf), sizeof(DRV_POS_SECTION_PERF));

    /*然后将本地存储的perf值清零*/
    memset(&(pSdhPerf->sectionPerf), 0, sizeof(DRV_POS_SECTION_PERF));
    
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_highPathPerfQuery
* 功能描述: 查询STM-1帧的高阶通道的性能统计
* 访问的表: 无.
* 修改的表: 无.
* 输入参数: chipId: 0~3.
*           portId: 1~8.
* 输出参数: *pVc4Perf: 保存查询结果. 
* 返 回 值: 
* 其它说明: 该函数供产品管理调用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_highPathPerfQuery(BYTE chipId, BYTE portIdx, DRV_POS_VC4_PERF *pVc4Perf)
{
    DRV_POS_RET ret = DRV_POS_OK;
    DRV_POS_SDH_PERF *pSdhPerf = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pVc4Perf, DRV_POS_PT_ERR, 
        "chip[%d] portIdx[%d] input pVc4Perf is NULL", chipId, portIdx);
    
    ret = drv_pos_sdhPerfMemGet(chipId, portIdx, &pSdhPerf);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_sdhPerfMemGet failed", chipId, portIdx);
    
    memset(pVc4Perf, 0, sizeof(DRV_POS_VC4_PERF));  /* 先清零再赋值 */
    memcpy(pVc4Perf, &(pSdhPerf->vc4Perf), sizeof(DRV_POS_VC4_PERF));

    /*然后将本地存储的perf值清零*/
    memset(&(pSdhPerf->vc4Perf), 0, sizeof(DRV_POS_VC4_PERF));
    
    return ret;
}


/**************************************************************************
* 函数名称: drv_pos_sdhPerfFlagGet
* 功能描述: 获取监测SDH性能统计的标记.
* 访问的表: 软件表g_drv_pos_sdh_perf_enable.
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.           
* 输出参数: pFlag: 保存性能统计使能标记,DRV_POS_ENABLE使能,DRV_POS_DISABLE非使能.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhPerfFlagGet(BYTE chipId, WORD32 *pFlag)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_POINTER_CHECK(pFlag, DRV_POS_PT_ERR, "chip[%d] input pFlag is NULL", chipId);

    *pFlag = 0;
    *pFlag = g_drv_pos_sdh_perf_enable[chipId];
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhPerfFlagSet
* 功能描述: 设置监测SDH性能统计的标记.
* 访问的表: 软件表g_drv_pos_sdh_perf_enable.
* 修改的表: 软件表g_drv_pos_sdh_perf_enable.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           flag: 性能统计使能标记,DRV_POS_ENABLE使能,DRV_POS_DISABLE非使能.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhPerfFlagSet(BYTE chipId, WORD32 flag)
{
    DRV_POS_CHIPID_CHECK(chipId);
    
    g_drv_pos_sdh_perf_enable[chipId] = flag;
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhSecPerfFlagGet
* 功能描述: 获取监测SDH的段性能统计的标记.
* 访问的表: 软件表g_drv_pos_sdhSecPerfEnable.
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.           
* 输出参数: pFlag: 保存性能统计使能标记,DRV_POS_ENABLE使能,DRV_POS_DISABLE非使能.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhSecPerfFlagGet(BYTE chipId, BYTE portIdx, WORD32 *pFlag)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pFlag, DRV_POS_PT_ERR, 
        "chip[%d] portIdx[%d] input pFlag is NULL", chipId, portIdx);
    
    *pFlag = 0;
    *pFlag = g_drv_pos_sdhSecPerfEnable[chipId][portIdx];
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhSecPerfFlagSet
* 功能描述: 设置监测SDH的段性能统计的标记.
* 访问的表: 软件表g_drv_pos_sdhSecPerfEnable.
* 修改的表: 软件表g_drv_pos_sdhSecPerfEnable.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           flag: 性能统计使能标记,DRV_POS_ENABLE使能,DRV_POS_DISABLE非使能.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhSecPerfFlagSet(BYTE chipId, BYTE portIdx, WORD32 flag)
{    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    
    g_drv_pos_sdhSecPerfEnable[chipId][portIdx] = flag;
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhVc4PerfFlagGet
* 功能描述: 获取监测SDH的高阶通道性能统计的标记.
* 访问的表: 软件表g_drv_pos_sdhVc4PerfEnable.
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.           
* 输出参数: pFlag: 保存性能统计使能标记,DRV_POS_ENABLE使能,DRV_POS_DISABLE非使能.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhVc4PerfFlagGet(BYTE chipId, BYTE portIdx, WORD32 *pFlag)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pFlag, DRV_POS_PT_ERR, 
        "chip[%d] portIdx[%d] input pFlag is NULL", chipId, portIdx);
    
    *pFlag = 0;
    *pFlag = g_drv_pos_sdhVc4PerfEnable[chipId][portIdx];
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhVc4PerfFlagSet
* 功能描述: 设置监测SDH的高阶通道性能统计的标记.
* 访问的表: 软件表g_drv_pos_sdhVc4PerfEnable.
* 修改的表: 软件表g_drv_pos_sdhVc4PerfEnable.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           flag: 性能统计使能标记,DRV_POS_ENABLE使能,DRV_POS_DISABLE非使能.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhVc4PerfFlagSet(BYTE chipId, BYTE portIdx, WORD32 flag)
{    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    
    g_drv_pos_sdhVc4PerfEnable[chipId][portIdx] = flag;
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhPerfMonitor
* 功能描述: 监测STM-1单板的SDH性能统计
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pDwSubslotId: STM-1单板的子槽位号 
* 输出参数: 无
* 返 回 值: 
* 其它说明: SDH性能统计线程的入口函数
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhPerfMonitor(const WORD32 *pDwSubslotId)
{
    DRV_POS_RET ret = DRV_POS_OK;  /* 函数返回码 */
    BYTE subslotId = 0;
    BYTE chipId = 0;         /* ARRIVE芯片的芯片编号 */
    BYTE portId = 0;         /* STM-1单板的155M端口的编号 */
    BYTE portIndex = 0;
    DRV_POS_SECTION_PERF sectionPerf;
    DRV_POS_VC4_PERF vc4Perf;
    BYTE portNum = 0;
    BYTE portMap[DRV_POS_MAX_PORT] = {0};

    WORD32 rv = BSP_E_CP3BAN_OK;
    WORD32 dwBoardStatus = 0;  /* CP3BAN单板的状态 */

    DRV_POS_POINTER_CHECK(pDwSubslotId, DRV_POS_PT_ERR, "sdhPerfMonitor input pDwSubslotId is NULL");

    subslotId = (BYTE)(*pDwSubslotId);
    chipId = subslotId - (BYTE)1;
    DRV_POS_CHIPID_CHECK(chipId);

    if(NULL == pDwSubslotId)
    {
        free((void*)pDwSubslotId);
        pDwSubslotId = NULL;
    }

    g_drv_pos_sdh_perf_thread_id[chipId] = __gettid();

    while (DRV_POS_TRUE)
    {
        rv = BSP_cp3banBoardStatusGet((WORD32)subslotId, &dwBoardStatus); /* 获取单板的状态 */
        if (BSP_E_CP3BAN_OK != rv)
        {
            g_drv_pos_sdh_perf_thread_id[chipId] = __gettid();
            DRV_POS_ERROR_RET(DRV_POS_ERROR, "chip[%d] BSP_cp3banBoardStatusGet failed, return %d", chipId, ret);
        }
        if (BSP_CP3BAN_BOARD_OFFLINE == dwBoardStatus)  /* 单板不在位,直接返回成功 */
        {
            g_drv_pos_sdh_perf_thread_id[chipId] = __gettid();
            DRV_POS_ERROR_RET(DRV_POS_ERROR, "chip[%d] board is offline, exit the overhead monitor", chipId, ret);
        }

        if (DRV_POS_DISABLE != g_drv_pos_sdh_perf_enable[chipId])
        {
    /*获取最大端口号*/
    ret = drv_pos_get_valid_port_map(chipId, portMap, &portNum);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_valid_port_map failed return %d", chipId, ret);
    
            for (portId = 0; portId < portNum; portId++)
            {
                portIndex = portMap[portId]; 
                if (DRV_POS_DISABLE != g_drv_pos_sdhSecPerfEnable[chipId][portIndex])
                {
                    /* Query section performance */
                    ret = drv_pos_sectionPerfGet(chipId, portIndex, &sectionPerf);
                    if (DRV_POS_OK != ret)
                    {
                        continue;
                    }
                    ret = drv_pos_sectionPerfSave(chipId, portIndex, &sectionPerf);
                    if (DRV_POS_OK != ret)
                    {
                        continue;
                    }
                }

                if (DRV_POS_DISABLE != g_drv_pos_sdhVc4PerfEnable[chipId][portIndex])
                {
                    /* Query high path performance */
                    ret = drv_pos_highPathPerfGet(chipId, portIndex, &vc4Perf);
                    if (DRV_POS_OK != ret)
                    {
                        continue;
                    }
                    
                    ret = drv_pos_highPathPerfSave(chipId, portIndex, &vc4Perf);
                    if (DRV_POS_OK != ret)
                    {
                        continue;
                    }
                }                
                BSP_DelayMs(125);  /* delay 125ms. */
            }
        }
        BSP_DelayMs(1000);  /* delay 1000ms. */
    }

    g_drv_pos_sdh_perf_thread_id[chipId] = __gettid();
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhPerfThreadCreate
* 功能描述: 创建SDH性能统计的线程.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM-1单板的子槽位号 
* 输出参数: 无
* 返 回 值: 
* 其它说明: 该线程用来监测STM-1单板的SDH性能统计.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhPerfThreadCreate(BYTE subslotId)
{
    DRV_POS_RET ret = DRV_POS_OK;
    WORD32 *pDwSubslotId = NULL;
    BYTE chipId = 0;

    chipId = subslotId - (BYTE)1;
    DRV_POS_CHIPID_CHECK(chipId);
    
    pDwSubslotId = malloc(sizeof(WORD32));
    DRV_POS_POINTER_CHECK(pDwSubslotId, DRV_POS_PT_ERR, "chip[%d] malloc failed", chipId);
    
    memset(pDwSubslotId, 0, sizeof(WORD32));
    *pDwSubslotId = subslotId;
    
    init_thread(&g_drv_pos_sdh_perf_thread[chipId], 0, 0, 80, (void *)drv_pos_sdhPerfMonitor, ((void *)(pDwSubslotId)));
    ret = start_mod_thread(&g_drv_pos_sdh_perf_thread[chipId]);
    if (BSP_OK != ret)
    {
        if(NULL == pDwSubslotId)
        {
            free((void*)pDwSubslotId);
            pDwSubslotId = NULL;
        }
        DRV_POS_ERROR_RET(DRV_POS_THREAD_ERR, "chip[%d] sdhPerfMonitor start_mod_thread failed", chipId);
    }
    DRV_POS_SUCCESS_PRINT("chip[%d] drv_pos_sdhPerfThreadCreate", chipId);
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhPerfThreadDelete
* 功能描述: 删除SDH性能统计的线程.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM-1单板的子槽位号 
* 输出参数: 无
* 返 回 值: 
* 其它说明: 该线程用来监测STM-1单板的SDH性能统计.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhPerfThreadDelete(BYTE subslotId)
{
    BYTE chipId = 0;
    
    chipId = subslotId - (BYTE)1;
    DRV_POS_CHIPID_CHECK(chipId);

    if(0 != g_drv_pos_sdh_perf_thread_id[chipId])
    {
        stop_mod_thread(&g_drv_pos_sdh_perf_thread[chipId]);
        memset(&g_drv_pos_sdh_perf_thread[chipId], 0, sizeof(g_drv_pos_sdh_perf_thread[chipId]));
        g_drv_pos_sdh_perf_thread_id[chipId] = 0;
    }
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhPerfThreadIdGet
* 功能描述: 获取SDH性能统计线程的threadId.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3. 
* 输出参数: *pThreadId: 用来保存threadId.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhPerfThreadIdGet(BYTE chipId, int *pThreadId)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_POINTER_CHECK(pThreadId, DRV_POS_PT_ERR, "chip[%d] input pThreadId is NULL", chipId);
    
    *pThreadId = g_drv_pos_sdh_perf_thread_id[chipId];
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_txB1ErrForce
* 功能描述: 在TX方向强制插入B1误码
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 0~3. portId: 1~8.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_txB1ErrForce(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    eAtRet atRet = cAtOk;
    AtSdhLine atSdhLine = NULL;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d",
        chipId, portIdx, ret);

    /* Force B1 error on STM-1 channel at TX direction. */
    atRet = AtChannelTxErrorForce((AtChannel)atSdhLine, cAtSdhLineCounterTypeB1);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] AtChannelTxErrorForce failed, return %s, atSdhLine %p, cAtSdhLineCounterTypeB1 = %d",
        chipId, portIdx, AtRet2String(atRet), atSdhLine, cAtSdhLineCounterTypeB1);
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_txB1ErrUnForce
* 功能描述: 在TX方向取消强制插入B1误码
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 0~3. portId: 1~8.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_txB1ErrUnForce(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    eAtRet atRet = cAtOk;
    AtSdhLine atSdhLine = NULL;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d",
        chipId, portIdx, ret);

        /* UnForce B1 error on STM-1 channel at TX direction. */
    atRet = AtChannelTxErrorUnForce((AtChannel)atSdhLine, cAtSdhLineCounterTypeB1);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] AtChannelTxErrorUnForce failed, return %s, atSdhLine = %p, cAtSdhLineCounterTypeB1 = %d", 
        chipId, portIdx, AtRet2String(atRet), atSdhLine, cAtSdhLineCounterTypeB1);
  
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_txB2ErrForce
* 功能描述: 在TX方向强制插入B2误码
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 0~3. portId: 1~8.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_txB2ErrForce(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    eAtRet atRet = cAtOk;
    AtSdhLine atSdhLine = NULL;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d",
        chipId, portIdx, ret);
    
    /* Force B2 error on STM-1 channel at TX direction. */
    atRet = AtChannelTxErrorForce((AtChannel)atSdhLine, cAtSdhLineCounterTypeB2);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] AtChannelTxErrorForce failed, return %s, atSdhLine = %p, cAtSdhLineCounterTypeB2 = %d", 
        chipId, portIdx, AtRet2String(atRet), atSdhLine, cAtSdhLineCounterTypeB2);
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_txB2ErrUnForce
* 功能描述: 在TX方向取消强制插入B2误码
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 0~3. portId: 1~8.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_txB2ErrUnForce(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    eAtRet atRet = cAtOk;
    AtSdhLine atSdhLine = NULL;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d",
        chipId, portIdx, ret);
    
    /* Force B2 error on STM-1 channel at TX direction. */
    atRet = AtChannelTxErrorUnForce((AtChannel)atSdhLine, cAtSdhLineCounterTypeB2);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] AtChannelTxErrorUnForce failed, return %s, atSdhLine = %p, cAtSdhLineCounterTypeB2 = %d", 
        chipId, portIdx, AtRet2String(atRet), atSdhLine, cAtSdhLineCounterTypeB2);
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_txB3ErrForce
* 功能描述: 在TX方向强制插入B3误码
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 0~3. portId: 1~8. au4Id: 1.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_txB3ErrForce(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    eAtRet atRet = cAtOk;
    AtSdhVc atSdhVC4 = NULL;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_get_atSdhVC(chipId, portIdx, &atSdhVC4);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhVC failed, return %d",
        chipId, portIdx, ret);

    /* Force B3 error on VC4 channel at TX direction. */
    atRet = AtChannelTxErrorForce((AtChannel)atSdhVC4, cAtSdhPathCounterTypeBip);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] AtChannelTxErrorForce failed, return %s, atSdhVC4 = %p, cAtSdhPathCounterTypeBip = %d", 
        chipId, portIdx, AtRet2String(atRet), atSdhVC4, cAtSdhPathCounterTypeBip);
  
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_txB3ErrUnForce
* 功能描述: 在TX方向取消强制插入B3误码
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 0~3. portId: 1~8. au4Id: 1.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_txB3ErrUnForce(BYTE chipId, BYTE portIdx, BYTE au4Id)
{
    DRV_POS_RET ret = DRV_POS_OK;
    eAtRet atRet = cAtOk;
    AtSdhVc atSdhVC4 = NULL;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_get_atSdhVC(chipId, portIdx, &atSdhVC4);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhVC failed, return %d",
        chipId, portIdx, ret);

    /* Force B3 error on VC4 channel at TX direction. */
    atRet = AtChannelTxErrorUnForce((AtChannel)atSdhVC4, cAtSdhPathCounterTypeBip);
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] AtChannelTxErrorUnForce failed, return %s, atSdhVC4 = %p, cAtSdhPathCounterTypeBip = %d", 
        chipId, portIdx, AtRet2String(atRet), atSdhVC4, cAtSdhPathCounterTypeBip);
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_rsSdSfThresholdSet
* 功能描述: 设置rS段的SD/SF门限值
* 访问的表: 无
* 修改的表: 无
* 输入参数: sdsfType: 0表示设置SD的门限值,1表示设置SF的门限值  
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_rsSdSfThresholdSet(BYTE chipId, 
                                              BYTE portIdx, 
                                              WORD32 sdsfType, 
                                              DRV_POS_BER_RATE threshold)
{
    DRV_POS_RET ret = DRV_POS_OK;
    eAtRet atRet = cAtOk;
    AtSdhLine atSdhLine = NULL;
    AtBerController pBerCtrl = NULL;
    eAtBerRate atDevThrsh = 0;  /* ARRIVE芯片的SD/SF门限值 */

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, 
        "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d", chipId, portIdx, ret);
         
    /* Get BER controller that monitors BER for RS channel */   
    pBerCtrl = AtSdhLineRsBerControllerGet(atSdhLine);
    DRV_POS_POINTER_CHECK(pBerCtrl, DRV_POS_PT_ERR, 
        "chip[%d] portIdx[%d] AtSdhLineRsBerControllerGet failed", chipId, portIdx);

    atDevThrsh = (eAtBerRate)threshold;
        
    if (DRV_POS_SD == sdsfType)  /* SD Type */
    {
        /* Set threshold to report BER-SD alarm */
        atRet = AtBerControllerSdThresholdSet(pBerCtrl, atDevThrsh);
    }
    else if (DRV_POS_SF == sdsfType)  /* SF Type */
    {
        /* Set threshold to report BER-SF alarm */
        atRet = AtBerControllerSfThresholdSet(pBerCtrl, atDevThrsh);
    }
    else
    {
        DRV_POS_ERROR_RET(DRV_POS_PARAM_ERR, 
            "chip[%d] portIdx[%d] threshold set , not sd and not sf", chipId, portIdx);
    }
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] SD/SF threshold Set %u failed, return %s, pBerCtrl = %p, sdsfType = %d",
        chipId, portIdx, atDevThrsh, AtRet2String(atRet), pBerCtrl, sdsfType);
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_rsSdSfThresholdGet
* 功能描述: 获取RS段的SD/SF门限值
* 访问的表: 无
* 修改的表: 无
* 输入参数: sdsfType: 0表示设置SD的门限值,1表示设置SF的门限值  
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-11-8   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_rsSdSfThresholdGet(BYTE chipId, 
                                      BYTE portIdx, 
                                      WORD32 sdsfType, 
                                      DRV_POS_BER_RATE *pThreshold)
{
    DRV_POS_RET ret = DRV_POS_OK;
    eAtRet atRet = cAtOk;
    AtSdhLine atSdhLine = NULL;
    AtBerController pBerCtrl = NULL;
    eAtBerRate atDevThrsh = 0;  /* ARRIVE芯片的SD/SF门限值 */

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pThreshold, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] param pThreshold = NULL");

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, 
        "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d", chipId, portIdx, ret);
         
    /* Get BER controller that monitors BER for RS channel */   
    pBerCtrl = AtSdhLineRsBerControllerGet(atSdhLine);
    DRV_POS_POINTER_CHECK(pBerCtrl, DRV_POS_PT_ERR, 
        "chip[%d] portIdx[%d] AtSdhLineRsBerControllerGet failed", chipId, portIdx);
        
    if (DRV_POS_SD == sdsfType)  /* SD Type */
    {
        /* Get threshold to report BER-SD rate */
        atDevThrsh = AtBerControllerSdThresholdGet(pBerCtrl);
    }
    else if (DRV_POS_SF == sdsfType)  /* SF Type */
    {
        /* Set threshold to report BER-SF alarm */
        atDevThrsh = AtBerControllerSfThresholdGet(pBerCtrl);
    }
    else
    {
        DRV_POS_ERROR_RET(DRV_POS_PARAM_ERR, 
            "chip[%d] portIdx[%d] threshold get , not sd and not sf", chipId, portIdx);
    }
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] SD/SF threshold Get %u failed, return %s, pBerCtrl = %p, sdsfType = %d",
        chipId, portIdx, atDevThrsh, AtRet2String(atRet), pBerCtrl, sdsfType);
    *pThreshold = atDevThrsh;
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_msSdSfThresholdSet
* 功能描述: 设置MS段的SD/SF门限值
* 访问的表: 无
* 修改的表: 无
* 输入参数: sdsfType: 0表示设置SD的门限值,1表示设置SF的门限值  
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_msSdSfThresholdSet(BYTE chipId, 
                                              BYTE portIdx, 
                                              WORD32 sdsfType, 
                                              WORD32 threshold)
{
    DRV_POS_RET ret = DRV_POS_OK;
    eAtRet atRet = cAtOk;
    AtSdhLine atSdhLine = NULL;
    AtBerController pBerCtrl = NULL;
    eAtBerRate atDevThrsh = 0;  /* ARRIVE芯片的SD/SF门限值 */

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, 
        "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d", chipId, portIdx, ret);
         
    /* Get BER controller that monitors BER for MS channel */
    pBerCtrl = AtSdhLineMsBerControllerGet(atSdhLine);
    DRV_POS_POINTER_CHECK(pBerCtrl, DRV_POS_PT_ERR, 
        "chip[%d] portIdx[%d] AtSdhLineMsBerControllerGet failed", chipId, portIdx);

    atDevThrsh = (eAtBerRate)threshold;
    
    if (DRV_POS_SD == sdsfType)  /* SD Type */
    {
        /* Set threshold to report BER-SD alarm */
        atRet = AtBerControllerSdThresholdSet(pBerCtrl, atDevThrsh);
    }
    else if (DRV_POS_SF == sdsfType)  /* SF Type */
    {
        /* Set threshold to report BER-SF alarm */
        atRet = AtBerControllerSfThresholdSet(pBerCtrl, atDevThrsh);
    }
    else
    {
        DRV_POS_ERROR_RET(DRV_POS_PARAM_ERR, 
            "chip[%d] portIdx[%d] threshold set , not sd and not sf", chipId, portIdx);
    }
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] SD/SF threshold Set %u failed, return %s, pBerCtrl = %p, sdsfType = %d",
        chipId, portIdx, atDevThrsh, AtRet2String(atRet), pBerCtrl, sdsfType);

    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_msSdSfThresholdGet
* 功能描述: 获取MS段的SD/SF门限值
* 访问的表: 无
* 修改的表: 无
* 输入参数: sdsfType: 0表示设置SD的门限值,1表示设置SF的门限值  
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-11-8   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_msSdSfThresholdGet(BYTE chipId, 
                                      BYTE portIdx, 
                                      WORD32 sdsfType, 
                                      DRV_POS_BER_RATE *pThreshold)
{
    DRV_POS_RET ret = DRV_POS_OK;
    eAtRet atRet = cAtOk;
    AtSdhLine atSdhLine = NULL;
    AtBerController pBerCtrl = NULL;
    eAtBerRate atDevThrsh = 0;  /* ARRIVE芯片的SD/SF门限值 */

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pThreshold, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] param pThreshold = NULL");

    ret = drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    DRV_POS_IF_ERROR_RET(ret, 
        "chip[%d] portIdx[%d] drv_pos_get_atSdhLine failed, return %d", chipId, portIdx, ret);
         
    /* Get BER controller that monitors BER for RS channel */   
    pBerCtrl = AtSdhLineMsBerControllerGet(atSdhLine);
    DRV_POS_POINTER_CHECK(pBerCtrl, DRV_POS_PT_ERR, 
        "chip[%d] portIdx[%d] AtSdhLineMsBerControllerGet failed", chipId, portIdx);
        
    if (DRV_POS_SD == sdsfType)  /* SD Type */
    {
        /* Get threshold to report BER-SD rate */
        atDevThrsh = AtBerControllerSdThresholdGet(pBerCtrl);
    }
    else if (DRV_POS_SF == sdsfType)  /* SF Type */
    {
        /* Set threshold to report BER-SF alarm */
        atDevThrsh = AtBerControllerSfThresholdGet(pBerCtrl);
    }
    else
    {
        DRV_POS_ERROR_RET(DRV_POS_PARAM_ERR, 
            "chip[%d] portIdx[%d] threshold get , not sd and not sf", chipId, portIdx);
    }
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] SD/SF threshold Get %u failed, return %s, pBerCtrl = %p, sdsfType = %d",
        chipId, portIdx, atDevThrsh, AtRet2String(atRet), pBerCtrl, sdsfType);
    *pThreshold = atDevThrsh;
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_vc4SdSfThresholdSet
* 功能描述: 设置VC4通道的SD/SF门限值
* 访问的表: 无
* 修改的表: 无
* 输入参数: sdsfType: 0表示设置SD的门限值,1表示设置SF的门限值  
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_vc4SdSfThresholdSet(BYTE chipId, 
                                               BYTE portIdx, 
                                               WORD32 sdsfType, 
                                               WORD32 threshold)
{
    AtSdhVc atSdhVc4 = NULL;
    eAtRet atRet = cAtOk;
    AtBerController pBerCtrl = NULL;
    eAtBerRate atDevThrsh = 0;  /* ARRIVE芯片的SD/SF门限值 */
    DRV_POS_RET ret = DRV_POS_OK;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_get_atSdhVC(chipId, portIdx, &atSdhVc4);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atSdhVC failed, return %d",
        chipId, portIdx, ret);

    /* Get BER controller that monitors BER for MS channel */
    pBerCtrl = AtSdhChannelBerControllerGet((AtSdhChannel)atSdhVc4);
    DRV_POS_POINTER_CHECK(pBerCtrl, DRV_POS_AT_ERR, 
        "chip[%d] portIdx[%d] AtSdhChannelBerControllerGet failed", chipId, portIdx);
    
    atDevThrsh = (eAtBerRate)threshold;
    
    if (DRV_POS_SD == sdsfType)  /* SD Type */
    {
        /* Set threshold to report BER-SD alarm */
        atRet = AtBerControllerSdThresholdSet(pBerCtrl, atDevThrsh);
    }
    else if (DRV_POS_SF == sdsfType)  /* SF Type */
    {
        /* Set threshold to report BER-SF alarm */
        atRet = AtBerControllerSfThresholdSet(pBerCtrl, atDevThrsh);
    }
    else
    {
        DRV_POS_ERROR_RET(DRV_POS_ERROR, "chip[%d] portIdx[%d] threshold set, not sd and not sf", 
            chipId, portIdx);
    }
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] VC4 SD/SF threshold Set %u failed, return %s, pBerCtrl = %p, sdsfType = %d",
        chipId, portIdx, atDevThrsh, AtRet2String(atRet), pBerCtrl, sdsfType);

    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_vc4SdSfThresholdGet
* 功能描述: 获取高阶的SD/SF门限值
* 访问的表: 无
* 修改的表: 无
* 输入参数: sdsfType: 0表示设置SD的门限值,1表示设置SF的门限值  
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-11-8   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_vc4SdSfThresholdGet(BYTE chipId, 
                                      BYTE portIdx, 
                                      WORD32 sdsfType, 
                                      DRV_POS_BER_RATE *pThreshold)
{
    DRV_POS_RET ret = DRV_POS_OK;
    eAtRet atRet = cAtOk;
    AtSdhVc atSdhVc4 = NULL;
    AtBerController pBerCtrl = NULL;
    eAtBerRate atDevThrsh = 0;  /* ARRIVE芯片的SD/SF门限值 */

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
    DRV_POS_POINTER_CHECK(pThreshold, DRV_POS_PT_ERR, "chip[%d] portIdx[%d] param pThreshold = NULL");

    ret = drv_pos_get_atSdhVC(chipId, portIdx, &atSdhVc4);
    DRV_POS_IF_ERROR_RET(ret, 
        "chip[%d] portIdx[%d] drv_pos_get_atSdhVC failed, return %d", chipId, portIdx, ret);
         
    /* Get BER controller that monitors BER for RS channel */   
    pBerCtrl = AtSdhChannelBerControllerGet((AtSdhChannel)atSdhVc4);
    DRV_POS_POINTER_CHECK(pBerCtrl, DRV_POS_PT_ERR, 
        "chip[%d] portIdx[%d] AtSdhChannelBerControllerGet failed", chipId, portIdx);
        
    if (DRV_POS_SD == sdsfType)  /* SD Type */
    {
        /* Get threshold to report BER-SD rate */
        atDevThrsh = AtBerControllerSdThresholdGet(pBerCtrl);
    }
    else if (DRV_POS_SF == sdsfType)  /* SF Type */
    {
        /* Set threshold to report BER-SF alarm */
        atDevThrsh = AtBerControllerSfThresholdGet(pBerCtrl);
    }
    else
    {
        DRV_POS_ERROR_RET(DRV_POS_PARAM_ERR, 
            "chip[%d] portIdx[%d] threshold get , not sd and not sf", chipId, portIdx);
    }
    DRV_POS_IF_AT_ERROR_RET(atRet, 
        "chip[%d] portIdx[%d] SD/SF threshold Get %u failed, return %s, pBerCtrl = %p, sdsfType = %d",
        chipId, portIdx, atDevThrsh, AtRet2String(atRet), pBerCtrl, sdsfType);
    *pThreshold = atDevThrsh;
    
    return DRV_POS_OK;
}

/**************************************************************************
* 函数名称: drv_pos_sdhBerFlagSet
* 功能描述: 设置监测SDH BER的标记.
* 访问的表: 软件表g_drv_pos_sdh_ber_enable.
* 修改的表: 软件表g_drv_pos_sdh_ber_enable.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           flag: BER使能标记,DRV_POS_ENABLE使能,DRV_POS_DISABLE非使能.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhBerFlagSet(BYTE chipId, WORD32 flag)
{
    DRV_POS_CHIPID_CHECK(chipId);
    
    g_drv_pos_sdh_ber_enable[chipId] = flag;
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhBerFlagGet
* 功能描述: 获取监测SDH BER的标记.
* 访问的表: 软件表g_drv_pos_sdh_ber_enable.
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.           
* 输出参数: pFlag: 保存BER使能标记,DRV_POS_ENABLE使能,DRV_POS_DISABLE非使能.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhBerFlagGet(BYTE chipId, WORD32 *pFlag)
{  
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_POINTER_CHECK(pFlag, DRV_POS_PT_ERR, "chip[%d] pFlag is NULL", chipId);
    
    *pFlag = g_drv_pos_sdh_ber_enable[chipId];
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhBerMonitor
* 功能描述: 监测STM-1单板的SDH BER (Bit Error Rate).
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pDwSubslotId: STM-1单板的子槽位号 
* 输出参数: 无
* 返 回 值: 
* 其它说明: SDH BER 线程的入口函数
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhBerMonitor(const WORD32 *pDwSubslotId)
{
    DRV_POS_RET ret = DRV_POS_OK;
    BYTE subslotId = 0;
    BYTE chipId = 0;         /* ARRIVE芯片的芯片编号 */
    AtModuleBer atBerModule = NULL;

    WORD32 timeMs = 0;
    static const WORD32 maxTime = 100;/* Mili second */ 

    WORD32 rv = BSP_E_CP3BAN_OK;
    WORD32 dwBoardStatus = 0;  /* CP3BAN单板的状态 */

    DRV_POS_POINTER_CHECK(pDwSubslotId, DRV_POS_PT_ERR, "input pDwSubslotId is NULL");

    subslotId = (BYTE)(*pDwSubslotId);
    chipId = subslotId - (BYTE)1;
    DRV_POS_CHIPID_CHECK(chipId);
  
  if(NULL == pDwSubslotId)
    {
        free((void*)pDwSubslotId);
        pDwSubslotId = NULL;
    }
  
    g_drv_pos_sdh_ber_thread_id[chipId] = __gettid();

    ret = drv_pos_get_atModuleBer(chipId, &atBerModule);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_atModuleBer failed, return %d", chipId, ret);
    
    while (DRV_POS_TRUE)
    {
        rv = BSP_cp3banBoardStatusGet((WORD32)subslotId, &dwBoardStatus); /* 获取单板的状态 */
        if (BSP_E_CP3BAN_OK != rv)
        {
            g_drv_pos_sdh_ber_thread_id[chipId] = __gettid();
            DRV_POS_ERROR_RET(DRV_POS_ERROR, "chip[%d] BSP_cp3banBoardStatusGet failed, return %d", chipId, ret);
        }
        if (BSP_CP3BAN_BOARD_OFFLINE == dwBoardStatus)  /* 单板不在位,直接返回成功 */
        {
            g_drv_pos_sdh_ber_thread_id[chipId] = __gettid();
            DRV_POS_ERROR_RET(DRV_POS_ERROR, "chip[%d] board is offline, exit the ber monitor", chipId, ret);
        }

        if (DRV_POS_DISABLE != g_drv_pos_sdh_ber_enable[chipId])
        {
            timeMs = AtModuleBerPeriodicProcess(atBerModule, maxTime);
            if (timeMs < maxTime)
               BSP_DelayMs(maxTime - timeMs);  /* delay 100ms. */
        }

    }
    g_drv_pos_sdh_ber_thread_id[chipId] = __gettid();

    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhBerThreadCreate
* 功能描述: 创建SDH BER监测的线程.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM-1单板的子槽位号 
* 输出参数: 无
* 返 回 值: 
* 其它说明: 该线程用来监测STM-1单板的SDH BER (bit error rate).
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhBerThreadCreate(BYTE subslotId)
{
    DRV_POS_RET ret = DRV_POS_OK;
    WORD32 *pDwSubslotId = NULL;
    BYTE chipId = 0;
    
    chipId = subslotId - (BYTE)1;
    DRV_POS_CHIPID_CHECK(chipId);
    
    pDwSubslotId = malloc(sizeof(WORD32));
    DRV_POS_POINTER_CHECK(pDwSubslotId, DRV_POS_PT_ERR, "chip[%d] malloc failed", chipId);
    
    memset(pDwSubslotId, 0, sizeof(WORD32));
    *pDwSubslotId = subslotId;
    
    init_thread(&g_drv_pos_sdh_ber_thread[chipId], 0, 0, 80, (void *)drv_pos_sdhBerMonitor, ((void *)pDwSubslotId));
    ret = start_mod_thread(&g_drv_pos_sdh_ber_thread[chipId]);
    if (BSP_OK != ret)
    {
        if(NULL == pDwSubslotId)
        {
            free((void*)pDwSubslotId);
            pDwSubslotId = NULL;
        }
        DRV_POS_ERROR_RET(DRV_POS_THREAD_ERR, "chip[%d] sdhBerMonitor start_mod_thread failed, return %d",
            chipId, ret);
    }
    
    DRV_POS_SUCCESS_PRINT("chip[%d] drv_pos_sdhBerThreadCreate", chipId);
    
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhBerThreadDelete
* 功能描述: 删除SDH BER监测的线程.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM-1单板的子槽位号 
* 输出参数: 无
* 返 回 值: 
* 其它说明: 该线程用来监测STM-1单板的SDH BER (bit error rate).
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhBerThreadDelete(BYTE subslotId)
{
    BYTE chipId = 0;

    chipId = subslotId - (BYTE)1;
    DRV_POS_CHIPID_CHECK(chipId);

    if(0 != g_drv_pos_sdh_ber_thread_id[chipId])
    {   
        stop_mod_thread(&g_drv_pos_sdh_ber_thread[chipId]);
        memset(&g_drv_pos_sdh_ber_thread[chipId], 0, sizeof(&g_drv_pos_sdh_ber_thread[chipId]));
        g_drv_pos_sdh_ber_thread_id[chipId] = 0;
    }
    return DRV_POS_OK;
}


/**************************************************************************
* 函数名称: drv_pos_sdhBerThreadIdGet
* 功能描述: 获取SDH BER线程的threadId.
* 访问的表: 软件表g_drv_pos_sdh_ber_thread_id.
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3. 
* 输出参数: *pThreadId: 用来保存threadId.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-30   V1.0  刘钰00130907     create    
**************************************************************************/
DRV_POS_RET drv_pos_sdhBerThreadIdGet(BYTE chipId, int *pThreadId)
{
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_POINTER_CHECK(pThreadId, DRV_POS_PT_ERR, "chip[%d] input pThreadId is NULL", chipId);
    
    *pThreadId = g_drv_pos_sdh_ber_thread_id[chipId];
    
    return DRV_POS_OK;
}


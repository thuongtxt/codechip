/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_pos_init.c
* 文件标识: 
* 其它说明: 15k pos模块初始化源代码
* 当前版本: V1.0
* 作    者: 刘钰00130907
* 完成日期: 2013-08-15
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/

#include "drv_pos.h"
#include "drv_pos_sdh_alarm.h"
#include "drv_pos_sdh_perf.h"
#include "drv_pos_overhead.h"
#include "drv_pos_ppp_perf_alarm.h"
#include "drv_pos_diag.h"

#include "drv_pwe3_main.h"

#include "drv_pos_init.h"

extern WORD32 BSP_cp3banMappingBaseAddrGet(WORD32 dwSubslotId, 
                                        BSP_CP3BAN_CHIP_BASE_ADDR **ppMappingAddr);
extern WORD32 BSP_cp3banSystemTypeGet(VOID);
extern WORD32 drv_tdm_semaphoreDestroy(VOID);


/*********************************************************************
* 函数名称：drv_pos_global_info_uninit
* 功能描述：
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
*          DRV_POS_RET返回码 
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/

DRV_POS_RET drv_pos_global_info_uninit(BYTE chipId)
{
    DRV_POS_RET ret = DRV_POS_OK;
    /*global variable reset*/
    ret = drv_pos_global_info_clear(chipId);
    ret = drv_pos_link_info_clear(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_global_info_clear failed, return %d", chipId, ret);

    ret = drv_pos_save_init_state(chipId, DRV_POS_INIT_START);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_save_init_state failed, return %d", chipId, ret);

    return ret;
}

/*********************************************************************
* 函数名称：drv_pos_global_info_init
* 功能描述：
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
*          DRV_POS_RET返回码 
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/

DRV_POS_RET drv_pos_global_info_init(BYTE chipId)
{
    DRV_POS_RET ret = DRV_POS_OK;
    WORD32 * pFpgaBaseAddr = NULL;
    WORD32 sysType = 0;
    WORD32 bspRet = 0;
    BSP_CP3BAN_CHIP_BASE_ADDR * pMappingAddr = NULL;

    /*global variable reset*/
    DRV_POS_IF_ERROR_RET(drv_pos_init_state_clear(chipId), "chip[%d] drv_pos_init_state_clear failed",chipId);
    DRV_POS_IF_ERROR_RET(drv_pos_global_info_clear(chipId), "chip[%d] drv_pos_global_info_clear failed",chipId);
    DRV_POS_IF_ERROR_RET(drv_pos_link_info_clear(chipId), "chip[%d] drv_pos_link_info_clear failed",chipId);
   
    bspRet = BSP_cp3banMappingBaseAddrGet(chipId+1, &pMappingAddr);
    if (BSP_E_CP3BAN_OK != bspRet)
    {
        DRV_POS_ERROR_RET(DRV_POS_BASEADD_ERR, "chip[%d] BSP_cp3banMappingBaseAddrGet failed, return %d", chipId, bspRet);
    }
    pFpgaBaseAddr = pMappingAddr->fpgaBaseAddr;/*若是非15k-2系统，baseAddress为0*/

    ret = drv_pos_save_BaseAddress(chipId, pFpgaBaseAddr);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_save_BaseAddress failed, return %d", chipId, ret);

    /*get systemType*/
    sysType = BSP_cp3banSystemTypeGet(); /* 获取ZXUPN15000系统类型. */
    ret = drv_pos_save_systemType(chipId, sysType);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_save_systemType failed, return %d", chipId, ret);

    /*默认按照STM1起*/
    ret = drv_pos_save_stmMode(chipId, DRV_POS_LEFT_PORT_GRP, DRV_POS_STM_RATE_STM1);
    DRV_POS_IF_ERROR_RET(ret, "subslotId[%d] left port drv_pos_save_stmMode failed, return %d", chipId, ret);
        
    ret |= drv_pos_save_stmMode(chipId, DRV_POS_RIGHT_PORT_GRP, DRV_POS_STM_RATE_STM1);
    DRV_POS_IF_ERROR_RET(ret, "subslotId[%d] left port drv_pos_save_stmMode failed, return %d", chipId, ret);
    
    ret = drv_pos_save_init_state(chipId, DRV_POS_INIT_VAR_READY);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_save_init_state failed, return %d", chipId, ret);

    DRV_POS_SUCCESS_PRINT("CHIP[%d] SUCCESS to initial global information, baseAddress is %p, sysType is %d",
        chipId, pFpgaBaseAddr, sysType);

    return ret;
}
/*********************************************************************
* 函数名称：drv_pos_at_DrvDev_uninit
* 功能描述：
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
*          DRV_POS_RET返回码 
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_at_DrvDev_uninit(BYTE chipId)
{
    DRV_POS_RET ret = DRV_POS_OK;
    DRV_POS_CHIPID_CHECK(chipId);

    /*delete Arrive device*/
    ret = drv_atPos_device_delete(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] AT pos device delete failed", chipId);

    /*delete Arrive driver*/
    ret = drv_atPos_driver_delete(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] AT pos driver delete failed", chipId);

    /*delete Arrive Hal*/
    ret = drv_atPos_hal_delete(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_atPos_hal_delete", chipId);
  
    ret = drv_pos_save_init_state(chipId, DRV_POS_INIT_VAR_READY);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_save_init_state failed, return %d", chipId, ret);

    return ret;
}

/*********************************************************************
* 函数名称：drv_pos_at_DrvDev_init
* 功能描述：
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
*          DRV_POS_RET返回码 
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_at_DrvDev_init(BYTE chipId)
{
    DRV_POS_RET ret = DRV_POS_OK;

    /*chipId 检查*/
    DRV_POS_CHIPID_CHECK(chipId);

    /*获取公共模块的初始化信号量，保护多子卡driver同时创建*/
    ret = drv_pwe3_initSemaphoreGet();
    if(DRV_PWE3_OK != (WORD32)ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] drv_pwe3_initSemaphoreGet failed, return %d", chipId, ret);
    }

    /*create Arrive driver*/
    ret = drv_atPos_driver_create(chipId);

    /*不论是否成功，都先释放信号量*/
    ret = drv_pwe3_initSemaphoreFree();
    if(DRV_PWE3_OK != (WORD32)ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] drv_pwe3_initSemaphoreFree failed, return %d", chipId);
    }
    /*然后判断是否成功*/
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_atPos_driver_create FAIL", chipId);
    DRV_POS_SUCCESS_PRINT("CHIP[%d] drv_atPos_driver_create PASS", chipId);

    /*create Arrive Hal*/
    ret = drv_atPos_hal_create(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_atPos_hal_create FAIL", chipId);
    DRV_POS_SUCCESS_PRINT("CHIP[%d] drv_atPos_hal_create PASS", chipId);

    /*create Arrive device*/
    ret = drv_atPos_device_create(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_atPos_device_create FAIL", chipId);
    DRV_POS_SUCCESS_PRINT("CHIP[%d] drv_atPos_device_create PASS", chipId);

    /*install hal for IP cores of device and initial AT device*/
    ret = drv_atPos_device_setup(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_atPos_device_setup FAIL", chipId);
    DRV_POS_SUCCESS_PRINT("CHIP[%d] drv_atPos_device_setup PASS", chipId);

    /*initial Device*/
    ret = drv_atPos_device_initial(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_atPos_device_initial FAIL", chipId);    

    /*使能SDH模块的中断*/
    ret = drv_atPos_sdhModuleInterruptEnable(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_atPos_sdhModuleInterruptEnable failed, return %d", chipId, ret);

    ret = drv_pos_save_init_state(chipId, DRV_POS_INIT_AT_READY);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_save_init_state failed, return %d", chipId, ret);

    DRV_POS_SUCCESS_PRINT("CHIP[%d] drv_atPos_device_initial PASS", chipId);
    
    return DRV_POS_OK;
}
/*********************************************************************
* 函数名称：drv_pos_at_interface_init
* 功能描述：at，芯片接口初始化配置
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
*          DRV_POS_RET返回码 
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/19            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_at_interface_init(BYTE chipId)
{
    DRV_POS_RET ret = DRV_POS_OK;
    BYTE portIdx = 0;
    BYTE ethPortIdx = 0;
    BYTE portNum = 0;
    BYTE portMap[DRV_POS_MAX_PORT] = {0};

    DRV_POS_CHIPID_CHECK(chipId);

    /*获取端口映射*/
    ret = drv_pos_get_valid_port_map(chipId, portMap, &portNum);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_valid_port_map failed return %d", chipId, ret);

    /*sdh接口初始化 */
    for(portIdx = 0; portIdx < portNum; portIdx++)
    {
        ret = drv_atPos_sdh_init_cfg(chipId, portMap[portIdx]);
        DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] sdh init cfg failed, return %d", chipId, portMap[portIdx], ret);
        DRV_POS_PRINT("#### CHIP[%d] PORT[%d] sdh init config SUCCESS", chipId, portMap[portIdx]);

    }
    for(ethPortIdx = 0; ethPortIdx < DRV_POS_ETHERNET_PORT_NUM; ethPortIdx++)
    {
        ret = drv_atPos_enet_init_cfg(chipId, ethPortIdx);
        DRV_POS_IF_ERROR_RET(ret, "chip[%d] ethPortIdx[%d] eth cfg failed, return %d", chipId, ethPortIdx, ret);
        DRV_POS_PRINT("#### CHIP[%d] PORT[%d] enet sdh init config SUCCESS", chipId, ethPortIdx);
    }
    
    ret = drv_pos_save_init_state(chipId, DRV_POS_INIT_INFCONFIG_READY);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_save_init_state failed, return %d", chipId, ret);
    
    return ret;

}

/*********************************************************************
* 函数名称：drv_pos_ppp_link_uninit
* 功能描述：pos卡的pppLink反初始化
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
*          DRV_POS_RET返回码 
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/11/13            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_ppp_link_uninit(BYTE chipId)
{
    DRV_POS_RET ret = DRV_POS_OK;
    BYTE portIdx = 0;
    BYTE portNum = 0;
    BYTE portMap[DRV_POS_MAX_PORT] = {0};

    DRV_POS_CHIPID_CHECK(chipId);

    /*获取端口映射*/
    ret = drv_pos_get_valid_port_map(chipId, portMap, &portNum);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_valid_port_map failed return %d", chipId, ret);

    /*删除ppp所有链路*/
    for(portIdx = 0; portIdx < portNum; portIdx++)
    {
        ret = drv_atPos_ppp_delete(chipId, portMap[portIdx]);
        DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_atPos_ppp_delete, return %d", 
            chipId, portMap[portIdx], ret);
    }
    ret = drv_pos_save_init_state(chipId, DRV_POS_INIT_AT_READY);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_save_init_state failed, return %d", chipId, ret);

    return ret;

}

/*********************************************************************
* 函数名称：drv_pos_ppp_link_init
* 功能描述：pos卡的pppLink初始化
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
*          DRV_POS_RET返回码 
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/11/13            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_ppp_link_init(BYTE chipId)
{
    DRV_POS_RET ret = DRV_POS_OK;
    BYTE portIdx = 0;
    BYTE portNum = 0;
    BYTE portMap[DRV_POS_MAX_PORT] = {0};

    DRV_POS_CHIPID_CHECK(chipId);

    /*获取端口映射*/
    ret = drv_pos_get_valid_port_map(chipId, portMap, &portNum);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_valid_port_map failed return %d", chipId, ret);

    /*对ppp链路建链*/
    for(portIdx = 0; portIdx < portNum; portIdx++)
    {
        ret = drv_atPos_ppp_create(chipId, portMap[portIdx]);
        DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_atPos_ppp_create, portIdx = %d, return %d", chipId, portMap[portIdx], ret);
    }
    /*设置pid表*/
    ret = drv_atPos_pppPidSet(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_atPos_pppPidSet failed, return %d", ret);
    
    return ret;

}
/*********************************************************************
* 函数名称：drv_pos_thread_flag_set
* 功能描述：pos卡的线程标记设置
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
*          DRV_POS_RET返回码 
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/11/13            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_thread_flag_set(BYTE chipId)
{
    DRV_POS_RET ret = DRV_POS_OK;
    BYTE portIdx = 0;
    BYTE portNum = 0;
    BYTE portMap[DRV_POS_MAX_PORT] = {0};

    DRV_POS_CHIPID_CHECK(chipId);

    /*获取端口映射*/
    ret = drv_pos_get_valid_port_map(chipId, portMap, &portNum);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_valid_port_map failed return %d", chipId, ret);

    /*ret |= drv_pos_stm1IntfFlagSet(chipId, DRV_POS_ENABLE);*/
    ret |= drv_pos_sdhAlmFlagSet(chipId, DRV_POS_ENABLE);
    ret |= drv_pos_sdhPerfFlagSet(chipId, DRV_POS_ENABLE);
    ret |= drv_pos_sdhBerFlagSet(chipId, DRV_POS_ENABLE);
    ret |= drv_pos_sdhOhFlagSet(chipId, DRV_POS_ENABLE);
    for(portIdx = 0; portIdx < portNum; portIdx++)
    {
        ret |= drv_pos_sdhSecAlmFlagSet(chipId, portMap[portIdx], DRV_POS_ENABLE);
        ret |= drv_pos_sdhVc4AlmFlagSet(chipId, portMap[portIdx], DRV_POS_ENABLE);
        ret |= drv_pos_sdhSecPerfFlagSet(chipId, portMap[portIdx], DRV_POS_ENABLE);
        ret |= drv_pos_sdhVc4PerfFlagSet(chipId, portMap[portIdx], DRV_POS_ENABLE);
        ret |= drv_pos_sdhSecOhFlagSet(chipId, portMap[portIdx], DRV_POS_ENABLE);
        ret |= drv_pos_sdhVc4OhFlagSet(chipId, portMap[portIdx], DRV_POS_ENABLE);
        ret |= drv_pos_pppPerfFlagSet(chipId, portMap[portIdx], DRV_POS_ENABLE);
    }
    return ret;
}
/*********************************************************************
* 函数名称：drv_pos_sdh_thread_create
* 功能描述：创建和SDH相关的线程
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
*          DRV_POS_RET返回码 
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/19            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_sdh_thread_create(BYTE chipId)
{
    DRV_POS_RET ret = DRV_POS_OK;
    DRV_POS_CHIPID_CHECK(chipId);

#if 0
    ret = drv_pos_stm1IntfThreadCreate(chipId+1);
    if(ret != DRV_POS_OK)
    {
        /*只记录，不返回失败*/
        DRV_POS_ERROR_LOG("chip[%d] drv_pos_stm1IntfThreadCreate failed", chipId);
    }
#endif

    ret = drv_pos_sdhAlmThreadCreate(chipId+1);
    if(ret != DRV_POS_OK)
    {
        /*只记录，不返回失败*/
        DRV_POS_ERROR_LOG("chip[%d] drv_pos_sdhAlmThreadCreate failed", chipId);
    }
    
    ret = drv_pos_sdhPerfThreadCreate(chipId+1);
    if(ret != DRV_POS_OK)
    {
        /*只记录，不返回失败*/
        DRV_POS_ERROR_LOG("chip[%d] drv_pos_sdhPerfThreadCreate failed", chipId);
    }
    
    ret = drv_pos_sdhBerThreadCreate(chipId+1);
    if(ret != DRV_POS_OK)
    {
        /*只记录，不返回失败*/
        DRV_POS_ERROR_LOG("chip[%d] drv_pos_sdhBerThreadCreate failed", chipId);
    }
    
    ret = drv_pos_sdhOhThreadCreate(chipId+1);
    if(ret != DRV_POS_OK)
    {
        /*只记录，不返回失败*/
        DRV_POS_ERROR_LOG("chip[%d] drv_pos_sdhOhThreadCreate failed", chipId);
    }
    
    return DRV_POS_OK;

}
/*********************************************************************
* 函数名称：drv_pos_sdh_thread_delete
* 功能描述：删除和SDH相关的线程
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
*          DRV_POS_RET返回码 
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/9/9            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_sdh_thread_delete(BYTE chipId)
{
    DRV_POS_RET ret = DRV_POS_OK;
    DRV_POS_CHIPID_CHECK(chipId);

#if 0
    ret = drv_pos_stm1IntfThreadDelete(chipId+1);
    if(ret != DRV_POS_OK)
    {
        /*只记录，不返回失败*/
        DRV_POS_ERROR_LOG("chip[%d] drv_pos_stm1IntfThreadDelete failed", chipId);
    }
#endif

    ret = drv_pos_sdhAlmThreadDelete(chipId+1);
    if(ret != DRV_POS_OK)
    {
        /*只记录，不返回失败*/
        DRV_POS_ERROR_LOG("chip[%d] drv_pos_sdhAlmThreadDelete failed", chipId);
    }

    ret = drv_pos_sdhPerfThreadDelete(chipId+1);
    if(ret != DRV_POS_OK)
    {
        /*只记录，不返回失败*/
        DRV_POS_ERROR_LOG("chip[%d] drv_pos_sdhPerfThreadDelete failed", chipId);
    }

    ret = drv_pos_sdhBerThreadDelete(chipId+1);
    if(ret != DRV_POS_OK)
    {
        /*只记录，不返回失败*/
        DRV_POS_ERROR_LOG("chip[%d] drv_pos_sdhBerThreadDelete failed", chipId);
    }

    ret = drv_pos_sdhOhThreadDelete(chipId+1);
    if(ret != DRV_POS_OK)
    {
        /*只记录，不返回失败*/
        DRV_POS_ERROR_LOG("chip[%d] drv_pos_sdhOhThreadDelete failed", chipId);
    }

    return DRV_POS_OK;

}

/*********************************************************************
* 函数名称：drv_pos_ppp_thread_create
* 功能描述：创建和PPP相关的线程
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
*          DRV_POS_RET返回码 
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/19            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_ppp_thread_create(BYTE chipId)
{
    DRV_POS_RET ret = DRV_POS_OK;
    DRV_POS_CHIPID_CHECK(chipId);

#if 0
    ret = drv_pos_pppAlmThreadCreate(chipId+1);
    if(ret != DRV_POS_OK)
    {
        /*只记录，不返回失败*/
        DRV_POS_ERROR_LOG("chip[%d] drv_pos_pppAlmThreadCreate failed", chipId);
    }
#endif
    ret = drv_pos_pppPerfThreadCreate(chipId+1);
    if(ret != DRV_POS_OK)
    {
        /*只记录，不返回失败*/
        DRV_POS_ERROR_LOG("chip[%d] drv_pos_pppPerfThreadCreate failed", chipId);
    }

    return DRV_POS_OK;

}

/*********************************************************************
* 函数名称：drv_pos_ppp_thread_delete
* 功能描述：删除和PPP相关的线程
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
*          DRV_POS_RET返回码 
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/19            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_ppp_thread_delete(BYTE chipId)
{
    DRV_POS_RET ret = DRV_POS_OK;
    DRV_POS_CHIPID_CHECK(chipId);

#if 0
    ret = drv_pos_pppAlmThreadDelete(chipId+1);
    if(ret != DRV_POS_OK)
    {
        /*只记录，不返回失败*/
        DRV_POS_ERROR_LOG("chip[%d] drv_pos_pppAlmThreadDelete failed", chipId);
    }
#endif
    ret = drv_pos_pppPerfThreadDelete(chipId+1);
    if(ret != DRV_POS_OK)
    {
        /*只记录，不返回失败*/
        DRV_POS_ERROR_LOG("chip[%d] drv_pos_pppPerfThreadDelete failed", chipId);
    }

    return DRV_POS_OK;

}

/*********************************************************************
* 函数名称：drv_pos_uninit
* 功能描述：
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
*          DRV_POS_RET返回码 
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_uninit(BYTE chipId)
{
    DRV_POS_RET ret = DRV_POS_OK;

    DRV_POS_CHIPID_CHECK(chipId);

    ret = drv_pos_ppp_thread_delete(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_ppp_thread_delete failed, return %d", chipId, ret);

    ret = drv_pos_sdh_thread_delete(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_sdh_thread_delete failed, return %u", chipId, ret);

    ret = drv_pos_ppp_link_uninit(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_ppp_link_uninit failed, return %d", chipId, ret);
    
    /*删除arrive driver and device ect.*/
    ret = drv_pos_at_DrvDev_uninit(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_at_DrvDev_uninit fail, return %d", chipId, ret);
    
    ret = drv_pos_global_info_uninit(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_at_DrvDev_uninit fail return %d", chipId, ret);

    ret = drv_pwe3_cardCntSemaphoreGet();  /* 获取信号量 */
    if (DRV_PWE3_OK != (WORD32)ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] drv_pwe3_cardCntSemaphoreGet failed, return %d", chipId, ret);
    }
    drv_pwe3_subcardCntSet(DRV_TDM_SUBCARD_DECREASE);  /* 减少窄带子卡的数量 */
    ret = drv_pwe3_cardCntSemaphoreFree();  /* 释放信号量 */
    if (DRV_PWE3_OK != (WORD32)ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] drv_pwe3_cardCntSemaphoreFree failed, return %d", chipId, ret);
    }

    /* 销毁信号量 */
    ret = drv_tdm_semaphoreDestroy();
    if (DRV_TDM_OK != (WORD32)ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] drv_tdm_semaphoreDestroy failed, return %d", chipId, ret);
    }

    return ret;
}

/*********************************************************************
* 函数名称：drv_pos_init
* 功能描述：
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
*          DRV_POS_RET返回码 
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_init(BYTE chipId)
{
    DRV_POS_RET ret = DRV_POS_OK;
    DRV_POS_INIT_STATE initState = DRV_POS_INIT_START;

    DRV_POS_CHIPID_CHECK(chipId);

    drv_pos_debug_all_on();

    /*检查之前是否被初始化成功过*/
    ret = drv_pos_get_init_state(chipId, &initState);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_init_state failed, return %d", chipId, ret);
    if(DRV_POS_INIT_START != initState)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] init state is %d, not for init", chipId, initState);
    }
    
    ret = drv_pos_global_info_init(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_global_info_init failed return %u", chipId, ret);

    ret = drv_pos_at_DrvDev_init(chipId);
    if(DRV_POS_OK != ret)
    {
        /*做回滚，当arrive芯片driver和device等没有初始化成功时，要将残留的初始化过的资源释放*/
        drv_pos_uninit(chipId);
        DRV_POS_ERROR_RET(ret, "chip[%d] drv_pos_at_DrvDev_init failed, return %u", chipId, ret);        
    }
    
    ret = drv_pos_at_interface_init(chipId);
    if(DRV_POS_OK != ret)
    {
        /*做回滚，将残留的初始化过的资源释放*/
        drv_pos_uninit(chipId);
        DRV_POS_ERROR_RET(ret, "chip[%d] drv_pos_at_interface_init failed, return %u", chipId, ret);  
    }

    ret = drv_pos_ppp_link_init(chipId);
    if(DRV_POS_OK != ret)
    {
        /*做回滚，将残留的初始化过的资源释放*/
        drv_pos_uninit(chipId);
        DRV_POS_ERROR_RET(ret, "chip[%d] drv_pos_ppp_link_init failed, return %u", chipId, ret);  
    }
    
    ret = drv_pos_save_init_state(chipId, DRV_POS_INIT_PPP_CREATE_READY);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_save_init_state failed, return %d", chipId, ret);

    /*创建监控进程*/
    ret = drv_pos_sdh_thread_create(chipId);
    if(DRV_POS_OK != ret)/*如果是线程创建不成功，则仍然让业务跑，记录，但不返回失败*/
    {
        DRV_POS_ERROR_LOG("chip[%d] drv_pos_sdh_thread_create failed, return %u", chipId, ret);
    }

    ret = drv_pos_ppp_thread_create(chipId);
    if(DRV_POS_OK != ret)/*如果是线程创建不成功，则仍然让业务跑，记录，但不返回失败*/
    {
        DRV_POS_ERROR_LOG("chip[%d] drv_pos_ppp_thread_create failed, return %u", chipId, ret);
    }

    /*设置监控进程标记*/
    ret = drv_pos_thread_flag_set(chipId);
    if(DRV_POS_OK != ret)/*如果是线程标记设置不成功，也仍然让业务跑，记录，但不返回失败*/
    {
        DRV_POS_ERROR_LOG("chip[%d] drv_pos_ppp_thread_create failed, return %u", chipId, ret);
    }

    /*用公用的函数，初始化时增加子卡数量*/
    ret = drv_pwe3_cardCntSemaphoreGet();  /* 获取信号量 */
    if (DRV_PWE3_OK != (WORD32)ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] drv_pwe3_cardCntSemaphoreGet failed, return %d", chipId, ret);
    }
    drv_pwe3_subcardCntSet(DRV_TDM_SUBCARD_INCREASE);  /* 增加窄带子卡的数量 */
    ret = drv_pwe3_cardCntSemaphoreFree();  /* 释放信号量 */
    if (DRV_PWE3_OK != (WORD32)ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] drv_pwe3_cardCntSemaphoreFree failed, return %d", chipId, ret);
    }

    ret = drv_pos_save_init_state(chipId, DRV_POS_INIT_END);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_save_init_state failed, return %d", chipId, ret);

    return ret;
}


/*********************************************************************
* 函数名称：drv_pos_init_no_link
* 功能描述：
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
*          DRV_POS_RET返回码 
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/16            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET drv_pos_init_no_link(BYTE chipId)
{
    DRV_POS_RET ret = DRV_POS_OK;

    DRV_POS_CHIPID_CHECK(chipId);

    drv_pos_debug_all_on();

    /*check whether been initialized before*/
    
    ret = drv_pos_global_info_init(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_global_info_init failed return %u", chipId, ret);

    ret = drv_pos_at_DrvDev_init(chipId);
    if(DRV_POS_OK != ret)
    {
        /*做回滚，当arrive芯片driver和device等没有初始化成功时，要将残留的初始化过的资源释放*/
        drv_pos_at_DrvDev_uninit(chipId);
        DRV_POS_ERROR_RET(ret, "chip[%d] drv_pos_at_DrvDev_init failed, return %u", chipId, ret);        
    }
    
    ret = drv_pos_at_interface_init(chipId);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_at_interface_init failed, return %u", chipId, ret);

    return ret;
}


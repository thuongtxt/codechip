/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_pos_diag.c
* 文件标识: 
* 其它说明: 15k pos模块诊断代码源文件
* 当前版本: V1.0
* 作    者: 刘钰00130907
* 完成日期: 2013-08-15
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/
#include "drv_pos_diag.h"
#include "drv_pos_comm.h"
#include "drv_pos_intf.h"
#include "drv_pos_sdh_alarm.h"
#include "drv_pos_sdh_perf.h"
#include "drv_pos_overhead.h"
#include "drv_pos_ppp_perf_alarm.h"

#include "AtHdlcChannel.h"
#include "AtPppLink.h"


/*global variable*/

static char * g_pos_init_state[] = {
    "DRV_POS_INIT_START",
    "DRV_POS_INIT_VAR_READY",
    "DRV_POS_INIT_AT_READY",
    "DRV_POS_INIT_INFCONFIG_READY",
    "DRV_POS_INIT_PPP_CREATE_READY",
    "DRV_POS_INIT_END"
};

static char * g_pos_system_type[] = {
    "none -2 system",
    "-2 system"
};

static char * g_pos_loopback_mode[] = {
    "release",
    "internal",
    "external"
};

static char * g_pos_enable_state[] = {
    "enable",
    "disable"
};
static char * g_pos_fcs_mode[] = {
    "nofcs",
    "fcs16",
    "fcs32"
};

static char *g_pos_alarm_state[] = {
    "  No ",
    "Alarm"
};

static char *g_pos_stm_mode[] = {
    "DRV_POS_STM_RATE_INVALID",
    "DRV_POS_STM_RATE_STM1",
    "DRV_POS_STM_RATE_STM4"
};
/*global extern */
extern const char *AtDriverVersion(AtDriver self);

/*function*/
/*********************************************************************
* 函数名称：drv_pos_err_on
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/30            V1.0               liuyu        创建
*********************************************************************/
void drv_pos_err_on(void)
{
    g_pos_err = 1;
}
/*********************************************************************
* 函数名称：drv_pos_err_on
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/30            V1.0               liuyu        创建
*********************************************************************/
void drv_pos_err_off(void)
{
    g_pos_err = 0;
}
/*********************************************************************
* 函数名称：drv_pos_suc_on
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/30            V1.0               liuyu        创建
*********************************************************************/
void drv_pos_suc_on(void)
{
    g_pos_suc = 1;
}
/*********************************************************************
* 函数名称：drv_pos_suc_off
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/30            V1.0               liuyu        创建
*********************************************************************/
void drv_pos_suc_off(void)
{
    g_pos_suc = 0;
}
/*********************************************************************
* 函数名称：drv_pos_debug_all_on
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/30            V1.0               liuyu        创建
*********************************************************************/
void drv_pos_debug_all_on(void)
{
    drv_pos_err_on();
    drv_pos_suc_on();
    
}
/*********************************************************************
* 函数名称：drv_pos_set_perf_debug
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/11/19            V1.0               liuyu        创建
*********************************************************************/
void drv_pos_set_perf_debug(DRV_POS_PERF_DEBUG debugFlag)
{
    g_pos_perf_debug = debugFlag;
}

/*********************************************************************
* 函数名称：show_pos_init_state
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/10/22            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET show_pos_init_state(BYTE chipId)
{
    DRV_POS_RET ret = DRV_POS_OK;
    WORD32 initState = DRV_POS_INIT_START;

    DRV_POS_CHIPID_CHECK(chipId);

    ret = drv_pos_get_init_state(chipId, &initState);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_init_state failed, return %d", chipId, ret);

    ROSNG_TRACE_DEBUG("chip[%d] init state : %s\n", chipId, g_pos_init_state[initState]);
    
    return DRV_POS_OK;
}
/*********************************************************************
* 函数名称：show_pos_global_info
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/30            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET show_pos_global_info(BYTE chipId)
{
    DRV_POS_RET ret = DRV_POS_OK;
    WORD32   sysType = 0;        /*系统类型*/
    WORD32   *pBaseAdd = NULL;
    AtDriver atDriver = NULL;
    AtDevice atDevice = NULL;
    AtHal    atHal = NULL;
    DRV_POS_STM_RATE_MODE leftStmMode = DRV_POS_STM_RATE_INVALID;
    DRV_POS_STM_RATE_MODE rightStmMode = DRV_POS_STM_RATE_INVALID;

    DRV_POS_CHIPID_CHECK(chipId);

    ret = drv_pos_get_atDriver(chipId, &atDriver);
    ret |= drv_pos_get_atDevice(chipId, &atDevice);
    ret |= drv_pos_get_atHal(chipId, &atHal);
    ret |= drv_pos_get_BaseAddress(chipId, &pBaseAdd);
    ret |= drv_pos_get_systemType(chipId, &sysType);
    ret |= drv_pos_get_stmMode(chipId, DRV_POS_LEFT_PORT_GRP, &leftStmMode);
    ret |= drv_pos_get_stmMode(chipId, DRV_POS_RIGHT_PORT_GRP, &rightStmMode);
    
    ROSNG_TRACE_DEBUG("******chip[%d] pos global info *********\n", chipId);
    ROSNG_TRACE_DEBUG("fpgaBaseAddress :  %p\n", pBaseAdd);
    ROSNG_TRACE_DEBUG("system type     :  %s\n", g_pos_system_type[sysType]);
    ROSNG_TRACE_DEBUG("atDriver        :  %p\n", atDriver);
    ROSNG_TRACE_DEBUG("atDevice        :  %p\n", atDevice);
    ROSNG_TRACE_DEBUG("atHal           :  %p\n", atHal);
    ROSNG_TRACE_DEBUG("leftStmMode     :  %s\n", g_pos_stm_mode[leftStmMode]);
    ROSNG_TRACE_DEBUG("rightStmMode    :  %s\n", g_pos_stm_mode[rightStmMode]);

    if(NULL != atDriver)
    {
        ROSNG_TRACE_DEBUG("SDK version     :  %s\n", AtDriverVersion(atDriver));
    }
    
    return DRV_POS_OK;
}
/*********************************************************************
* 函数名称：show_pos_link_info
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/30            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET show_pos_link_info(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtEthFlow atEthFlow = NULL;
    AtHdlcChannel atHdlcChannel = NULL;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_get_atEthFlow(chipId, portIdx, &atEthFlow);
    ret |= drv_pos_get_atHdlcChannel(chipId, portIdx, &atHdlcChannel);
    
    ROSNG_TRACE_DEBUG("****** pos link info *********\n");
    ROSNG_TRACE_DEBUG("chipId[%d] portIdx[%d]\n", chipId, portIdx);
    ROSNG_TRACE_DEBUG("atEthFlow     :  %p\n", atEthFlow);
    ROSNG_TRACE_DEBUG("atHdlcChannel :  %p\n", atHdlcChannel);
    
    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：show_pos_thread_id
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/30            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET show_pos_thread_id(void)
{
    BYTE chipId = 0;
    int threadId = 0;

    ROSNG_TRACE_DEBUG("****** pos thread id *********\n");
    ROSNG_TRACE_DEBUG("               sdhAlm    sdhPerf    sdhBer    sdhOh    pppPerf\n");
    for(chipId = 0; chipId < DRV_POS_MAX_SUBCARD; chipId++)
    {
#if 0
        threadId = 0;
        drv_pos_stm1IntfThreadIdGet(chipId, &threadId);
        ROSNG_TRACE_DEBUG("threadId[%d] : %8d    ", chipId, threadId);
#endif
        threadId = 0;
        drv_pos_sdhAlmThreadIdGet(chipId, &threadId);
        ROSNG_TRACE_DEBUG("threadId[%d] :  %6d    ",chipId, threadId);

        threadId = 0;
        drv_pos_sdhPerfThreadIdGet(chipId, &threadId);
        ROSNG_TRACE_DEBUG("%7d    ", threadId);

        threadId = 0;
        drv_pos_sdhBerThreadIdGet(chipId, &threadId);
        ROSNG_TRACE_DEBUG("%6d    ", threadId);

        threadId = 0;
        drv_pos_sdhOhThreadIdGet(chipId, &threadId);
        ROSNG_TRACE_DEBUG("%5d    ", threadId);

        threadId = 0;
        drv_pos_pppPerfThreadIdGet(chipId, &threadId);
        ROSNG_TRACE_DEBUG("%7d    \n", threadId);
        
    }

    return DRV_POS_OK;
}
/*********************************************************************
* 函数名称：show_pos_link_info
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/30            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET show_pos_thread_flag(BYTE chipId)
{
    DRV_POS_RET ret = DRV_POS_OK;
    BYTE portIdx = 0;
    WORD32 flag = 0;
    BYTE portNum = 0;
    BYTE portMap[DRV_POS_MAX_PORT] = {0};

    DRV_POS_CHIPID_CHECK(chipId);

    /*获取端口映射*/
    ret = drv_pos_get_valid_port_map(chipId, portMap, &portNum);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_valid_port_map failed return %d", chipId, ret);

    ROSNG_TRACE_DEBUG("****** pos thread flag *********\n", chipId);
    ROSNG_TRACE_DEBUG("          chip[%d]", chipId);
    
    for(portIdx = 0; portIdx < portNum; portIdx++)
    {
        ROSNG_TRACE_DEBUG("  p[%d]", portMap[portIdx]);
    }
    ROSNG_TRACE_DEBUG("\n");
#if 0    
    /* 打印stm接口Flag */
    flag = 0;
    drv_pos_stm1IntfFlagGet(chipId, &flag);
    ROSNG_TRACE_DEBUG("stm1Intf  %7u\n", flag);
    ROSNG_TRACE_DEBUG("-------------------------------------------------------------------\n");
#endif
    /* 打印sdhAlmFlag */
    flag = 0;
    drv_pos_sdhAlmFlagGet(chipId, &flag);
    ROSNG_TRACE_DEBUG("sdhAlm    %7u\n", flag);
    ROSNG_TRACE_DEBUG("-------------------------------------------------------------------\n");

    /* 打印sdhSecAlmFlag */
    ROSNG_TRACE_DEBUG("  SecAlm         ");
    for(portIdx = 0; portIdx < portNum; portIdx++)
    {
        flag = 0;
        drv_pos_sdhSecAlmFlagGet(chipId, portMap[portIdx], &flag);
        ROSNG_TRACE_DEBUG("  %4u", flag);
    }
    ROSNG_TRACE_DEBUG("\n");
    ROSNG_TRACE_DEBUG("-------------------------------------------------------------------\n");

    /* 打印sdhVC4AlmFlag */
    ROSNG_TRACE_DEBUG("  Vc4Alm         ");
    for(portIdx = 0; portIdx < portNum; portIdx++)
    {
        flag = 0;
        drv_pos_sdhVc4AlmFlagGet(chipId, portMap[portIdx], &flag);
        ROSNG_TRACE_DEBUG("  %4u", flag);
    }
    ROSNG_TRACE_DEBUG("\n");
    ROSNG_TRACE_DEBUG("-------------------------------------------------------------------\n");
    
    /* 打印sdhPerfFlag */
    flag = 0;
    drv_pos_sdhPerfFlagGet(chipId, &flag);
    ROSNG_TRACE_DEBUG("sdhPerf   %7u\n", flag);
    ROSNG_TRACE_DEBUG("-------------------------------------------------------------------\n");

    /* 打印sdhSecPerfFlag */
    ROSNG_TRACE_DEBUG("  SecPerf        ");
    for(portIdx = 0; portIdx < portNum; portIdx++)
    {
        flag = 0;
        drv_pos_sdhSecPerfFlagGet(chipId, portMap[portIdx], &flag);
        ROSNG_TRACE_DEBUG("  %4u", flag);
    }
    ROSNG_TRACE_DEBUG("\n");
    ROSNG_TRACE_DEBUG("-------------------------------------------------------------------\n");

    /* 打印sdhVc4PerfFlag */
    ROSNG_TRACE_DEBUG("  Vc4Perf        ");
    for(portIdx = 0; portIdx < portNum; portIdx++)
    {
        flag = 0;
        drv_pos_sdhVc4PerfFlagGet(chipId, portMap[portIdx], &flag);
        ROSNG_TRACE_DEBUG("  %4u", flag);
    }
    ROSNG_TRACE_DEBUG("\n");
    ROSNG_TRACE_DEBUG("-------------------------------------------------------------------\n");

    /* 打印sdhBerFlag */
    flag = 0;
    drv_pos_sdhBerFlagGet(chipId, &flag);
    ROSNG_TRACE_DEBUG("sdhBer    %7u\n", flag);
    ROSNG_TRACE_DEBUG("-------------------------------------------------------------------\n");

    /* 打印sdhOhFlag */
    flag = 0;
    drv_pos_sdhOhFlagGet(chipId, &flag);
    ROSNG_TRACE_DEBUG("sdhOh     %7u\n", flag);
    ROSNG_TRACE_DEBUG("-------------------------------------------------------------------\n");

    /* 打印sdhVc4PerfFlag */
    ROSNG_TRACE_DEBUG("  SecOh          ");
    for(portIdx = 0; portIdx < portNum; portIdx++)
    {
        flag = 0;
        drv_pos_sdhSecOhFlagGet(chipId, portMap[portIdx], &flag);
        ROSNG_TRACE_DEBUG("  %4u", flag);
    }
    ROSNG_TRACE_DEBUG("\n");
    ROSNG_TRACE_DEBUG("-------------------------------------------------------------------\n");
    
    /* 打印sdhVc4PerfFlag */
    ROSNG_TRACE_DEBUG("  Vc4Oh          ");
    for(portIdx = 0; portIdx < portNum; portIdx++)
    {
        flag = 0;
        drv_pos_sdhVc4OhFlagGet(chipId, portMap[portIdx], &flag);
        ROSNG_TRACE_DEBUG("  %4u", flag);
    }
    ROSNG_TRACE_DEBUG("\n");
    ROSNG_TRACE_DEBUG("-------------------------------------------------------------------\n");

    /* 打印posPppPerfFlag */
    ROSNG_TRACE_DEBUG("  pppPerf        ");
    for(portIdx = 0; portIdx < portNum; portIdx++)
    {
        flag = 0;
        drv_pos_pppPerfFlagGet(chipId, portMap[portIdx], &flag);
        ROSNG_TRACE_DEBUG("  %4u", flag);
    }
    ROSNG_TRACE_DEBUG("\n");
    ROSNG_TRACE_DEBUG("-------------------------------------------------------------------\n");
    

   return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：show_sdh_interface_status
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/9/22            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET show_sdh_interface_status(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    DRV_POS_SDH_PORT_INFO * pPortInfo = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_sdhPortInfoMemGet(chipId, portIdx, &pPortInfo);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] interface status get failed, return %d", chipId, portIdx, ret);

    ROSNG_TRACE_DEBUG("****** sdh interface status *********\n");
    ROSNG_TRACE_DEBUG("chipId[%d] portIdx[%d]\n", chipId, portIdx);
    ROSNG_TRACE_DEBUG("LoopbackMode : %s\n", g_pos_loopback_mode[pPortInfo->loopbackMode]);
    ROSNG_TRACE_DEBUG("ScrambleState : %s\n", g_pos_enable_state[pPortInfo->scrambleState]);
    ROSNG_TRACE_DEBUG("TxLaserState : %s\n", g_pos_enable_state[pPortInfo->txLaserState]);
    ROSNG_TRACE_DEBUG("ShutdownState: %s\n", g_pos_enable_state[pPortInfo->shutdownState]);
    ROSNG_TRACE_DEBUG("fcsMode      : %s\n", g_pos_fcs_mode[pPortInfo->fcsMode]);
    
    return DRV_POS_OK;
}
/*********************************************************************
* 函数名称：show_sdh_alarm_table
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/9/22            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET show_sdh_alarm_table(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    DRV_POS_SDH_ALARM * pSdhAlm = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_sdhAlmMemGet(chipId, portIdx, &pSdhAlm);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] SDH alarm get failed", chipId, portIdx);

    ROSNG_TRACE_DEBUG("+-------------------------------- SDH ALARM STATUS ---------------------------|\n");
    ROSNG_TRACE_DEBUG("| chipId[%d] portIdx[%d]\n", chipId, portIdx);
    ROSNG_TRACE_DEBUG("|-----------------------------------------------------------------------------|\n");

    ROSNG_TRACE_DEBUG("|  rsLos  |  rsLof  |  rsOof  |  rsTim  |  rsSd  |  rsSf  |  msAis  |  msRdi  |  msSd  |  msSf  |\n");
    ROSNG_TRACE_DEBUG("|  %-5s  |  %-5s  |  %-5s  |  %-5s  |  %-5s |  %-5s |  %-5s  |  %-5s  |  %-5s |  %-5s |\n", 
        g_pos_alarm_state[pSdhAlm->sectionAlm.rsLos],
        g_pos_alarm_state[pSdhAlm->sectionAlm.rsLof],
        g_pos_alarm_state[pSdhAlm->sectionAlm.rsOof],
        g_pos_alarm_state[pSdhAlm->sectionAlm.rsTim],
        g_pos_alarm_state[pSdhAlm->sectionAlm.rsSd],
        g_pos_alarm_state[pSdhAlm->sectionAlm.rsSf],
        g_pos_alarm_state[pSdhAlm->sectionAlm.msAis],
        g_pos_alarm_state[pSdhAlm->sectionAlm.msRdi],
        g_pos_alarm_state[pSdhAlm->sectionAlm.msSd],
        g_pos_alarm_state[pSdhAlm->sectionAlm.msSf]);
    ROSNG_TRACE_DEBUG("|--------------------------------------------------------------------------------------|\n");
    ROSNG_TRACE_DEBUG("|  au4Lop  |  vc4Ais  |  vc4Uneq  |  vc4Plm  |  vc4Tim  |  vc4Rdi  |  vc4Sd  |  vc4Sf  |\n");
    ROSNG_TRACE_DEBUG("|  %-6s  |  %-6s  |  %-7s  |  %-6s  |  %-6s  |  %-6s  |  %-5s  |  %-5s  \n", 
        g_pos_alarm_state[pSdhAlm->vc4Alm.au4Lop],
        g_pos_alarm_state[pSdhAlm->vc4Alm.vc4Ais],
        g_pos_alarm_state[pSdhAlm->vc4Alm.vc4Uneq],
        g_pos_alarm_state[pSdhAlm->vc4Alm.vc4Plm],
        g_pos_alarm_state[pSdhAlm->vc4Alm.vc4Tim],
        g_pos_alarm_state[pSdhAlm->vc4Alm.vc4Rdi],
        g_pos_alarm_state[pSdhAlm->vc4Alm.vc4Sd],
        g_pos_alarm_state[pSdhAlm->vc4Alm.vc4Sf]);


    
    return DRV_POS_OK;
}
/*********************************************************************
* 函数名称：show_sdh_perf_table
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/9/22            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET show_sdh_perf_table(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    DRV_POS_SDH_PERF * pSdhPerf = NULL;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_sdhPerfMemGet(chipId, portIdx, &pSdhPerf);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] SDH perf get failed", chipId, portIdx);

    ROSNG_TRACE_DEBUG("-----POS SDH PERF --------|\n");
    ROSNG_TRACE_DEBUG(" chipId[%d] portIdx[%d]\n", chipId, portIdx);

    ROSNG_TRACE_DEBUG("b1 counts    :  %u\n", pSdhPerf->sectionPerf.b1Cnts);
    ROSNG_TRACE_DEBUG("b2 counts    :  %u\n", pSdhPerf->sectionPerf.b2Cnts);
    ROSNG_TRACE_DEBUG("msRei counts :  %u\n", pSdhPerf->sectionPerf.msReiCnts);
    ROSNG_TRACE_DEBUG("b3 counts    :  %u\n", pSdhPerf->vc4Perf.b3Cnts);
    ROSNG_TRACE_DEBUG("hpRei counts :  %u\n", pSdhPerf->vc4Perf.hpReiCnts);
    ROSNG_TRACE_DEBUG("rxAu4Ppjc    :  %u\n", pSdhPerf->vc4Perf.rxAu4Ppjc);
    ROSNG_TRACE_DEBUG("rxAu4Npjc    :  %u\n", pSdhPerf->vc4Perf.rxAu4Npjc);
    ROSNG_TRACE_DEBUG("txAu4Ppjx    :  %u\n", pSdhPerf->vc4Perf.txAu4Ppjx);
    ROSNG_TRACE_DEBUG("txAu4Npjc    :  %u\n", pSdhPerf->vc4Perf.txAu4Npjc);

    return DRV_POS_OK;
}
/*********************************************************************
* 函数名称：show_sdh_overhead_table
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/9/22            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET show_sdh_overhead_table(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    DRV_POS_STM1_OVERHEAD * pOverHead = NULL;
        
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_stm1OhMemGet(chipId, portIdx, &pOverHead);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] SDH OverHead get failed", chipId, portIdx);
    
    ROSNG_TRACE_DEBUG("-----POS SDH OVERHEAD --------\n");
    ROSNG_TRACE_DEBUG(" chipId[%d] portIdx[%d]\n", chipId, portIdx);

    ROSNG_TRACE_DEBUG("tx J0 msg   :  %s\n", pOverHead->sectionOh.txJ0MsgBuf);
    ROSNG_TRACE_DEBUG("rx J0 msg   :  %s\n", pOverHead->sectionOh.rxJ0MsgBuf);
    ROSNG_TRACE_DEBUG("exp J0 msg  :  %s\n", pOverHead->sectionOh.expectedRxJ0MsgBuf);
    ROSNG_TRACE_DEBUG("tx K1       :  0x%x\n", pOverHead->sectionOh.txK1Value);
    ROSNG_TRACE_DEBUG("rx K1       :  0x%x\n", pOverHead->sectionOh.rxK1Value);
    ROSNG_TRACE_DEBUG("tx K2       :  0x%x\n", pOverHead->sectionOh.txK2Value);
    ROSNG_TRACE_DEBUG("rx K2       :  0x%x\n", pOverHead->sectionOh.rxK2Value);
    ROSNG_TRACE_DEBUG("tx S1       :  0x%x\n", pOverHead->sectionOh.txS1Value);
    ROSNG_TRACE_DEBUG("rx S1       :  0x%x\n", pOverHead->sectionOh.rxS1Value); 

    ROSNG_TRACE_DEBUG("tx J1 msg   :  %s\n", pOverHead->vc4Oh.txJ1MsgBuf);
    ROSNG_TRACE_DEBUG("rx J1 msg   :  %s\n", pOverHead->vc4Oh.rxJ1MsgBuf);
    ROSNG_TRACE_DEBUG("exp J1 msg  :  %s\n", pOverHead->vc4Oh.expectedRxJ1MsgBuf);
    ROSNG_TRACE_DEBUG("tx C2 val   :  0x%x\n", pOverHead->vc4Oh.txC2Value);
    ROSNG_TRACE_DEBUG("rx C2 val   :  0x%x\n", pOverHead->vc4Oh.rxC2Value);
    ROSNG_TRACE_DEBUG("exp C2 val  :  0x%x\n", pOverHead->vc4Oh.expectedRxC2Value);

    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：show_ppp_perf_table
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/9/22            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET show_ppp_perf_table(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    DRV_POS_PPP_PERF *pPppPerf = NULL;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_pppPerfMemGet(chipId, portIdx, &pPppPerf);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] POS PPP PERF get failed", chipId, portIdx);

    ROSNG_TRACE_DEBUG("-----POS PPP PERF --------|\n");
    ROSNG_TRACE_DEBUG("chipId[%d] portIdx[%d]\n", chipId, portIdx);

    ROSNG_TRACE_DEBUG("tx_pkt = %d\n", pPppPerf->tx_pkt);
    ROSNG_TRACE_DEBUG("tx_byte = %d\n", pPppPerf->tx_byte);
    ROSNG_TRACE_DEBUG("tx_goodPkt %d\n", pPppPerf->tx_goodPkt);
    ROSNG_TRACE_DEBUG("tx_abortPkt = %d\n", pPppPerf->tx_abortPkt);
    
    ROSNG_TRACE_DEBUG("rx_pkt = %d\n", pPppPerf->rx_pkt);
    ROSNG_TRACE_DEBUG("rx_byte = %d\n", pPppPerf->rx_byte);
    ROSNG_TRACE_DEBUG("rx_goodPkt = %d\n", pPppPerf->rx_goodPkt);
    ROSNG_TRACE_DEBUG("rx_abortPkt = %d\n", pPppPerf->rx_abortPkt);
    ROSNG_TRACE_DEBUG("rx_fcsErrPkt = %d\n", pPppPerf->rx_fcsErrPkt);
    ROSNG_TRACE_DEBUG("rx_addrCtrlErrPkt = %d\n", pPppPerf->rx_addrCtrlErrPkt);
    ROSNG_TRACE_DEBUG("rx_sapiErrPkt = %d\n", pPppPerf->rx_sapiErrPkt);
    ROSNG_TRACE_DEBUG("rx_errPkt = %d\n", pPppPerf->rx_errPkt);

    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：show_ppp_port_mapping
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/9            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET show_ppp_port_mapping(BYTE chipId)
{
    DRV_POS_RET ret = DRV_POS_OK;
    BYTE portNum = 0;
    BYTE portMap[DRV_POS_MAX_PORT] = {0};
    WORD32 i = 0;
    
    DRV_POS_CHIPID_CHECK(chipId);

    ret = drv_pos_get_valid_port_map(chipId, &portMap[0], &portNum);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] drv_pos_get_valid_port_map failed", chipId);

    ROSNG_TRACE_DEBUG("| ----- POS PORT MAPPING -----\n");
    ROSNG_TRACE_DEBUG("| mapIdx  : ");
    for(i = 0; i < portNum; i++)
    {
        ROSNG_TRACE_DEBUG("%d | ",i);
    }
    ROSNG_TRACE_DEBUG("\n");
    ROSNG_TRACE_DEBUG("| portIdx : ");
    for(i = 0; i < portNum; i++)
    {
        ROSNG_TRACE_DEBUG("%d | ",portMap[i]);
    }
    ROSNG_TRACE_DEBUG("\n");
    
    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：show_ppp_alarm_intr_mask
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/12            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET show_ppp_alarm_intr_mask(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    WORD32 defectMask = 0;
    WORD32 enableMask = 0;
    
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    ret = drv_pos_alarmIntrDefectMaskGet(chipId, portIdx, &defectMask);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_alarmIntrDefectMaskGet failed, return %d", chipId, portIdx, ret);

    ROSNG_TRACE_DEBUG("\n");
    ROSNG_TRACE_DEBUG("        LOS     LOF     AIS     RS-SD     RS-SF     MS-SD     MS-SF     K-CHANGE\n");
    ROSNG_TRACE_DEBUG("dMask :  ");
    ROSNG_TRACE_DEBUG("%s       ", (defectMask & cAtSdhLineAlarmLos)?"*": "-");
    ROSNG_TRACE_DEBUG("%s       ", (defectMask & cAtSdhLineAlarmLof)?"*": "-");
    ROSNG_TRACE_DEBUG("%s        ", (defectMask & cAtSdhLineAlarmAis)?"*": "-");
    ROSNG_TRACE_DEBUG("%s         ", (defectMask & cAtSdhLineAlarmRsBerSd)?"*": "-");
    ROSNG_TRACE_DEBUG("%s         ", (defectMask & cAtSdhLineAlarmRsBerSf)?"*": "-");
    ROSNG_TRACE_DEBUG("%s         ", (defectMask & cAtSdhLineAlarmBerSd)?"*": "-");
    ROSNG_TRACE_DEBUG("%s          ", (defectMask & cAtSdhLineAlarmBerSf)?"*": "-");
    ROSNG_TRACE_DEBUG("%s\n", (defectMask & cAtSdhLineAlarmKByteChange)?"*": "-");

    ret = drv_pos_alarmIntrStateGet(chipId, portIdx, &enableMask);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_alarmIntrStateGet failed, return %d", chipId, portIdx, ret);

    ROSNG_TRACE_DEBUG("eMask :  ");
    ROSNG_TRACE_DEBUG("%s       ", (enableMask & cAtSdhLineAlarmLos)?"*": "-");
    ROSNG_TRACE_DEBUG("%s       ", (enableMask & cAtSdhLineAlarmLof)?"*": "-");
    ROSNG_TRACE_DEBUG("%s        ", (enableMask & cAtSdhLineAlarmAis)?"*": "-");
    ROSNG_TRACE_DEBUG("%s         ", (enableMask & cAtSdhLineAlarmRsBerSd)?"*": "-");
    ROSNG_TRACE_DEBUG("%s         ", (enableMask & cAtSdhLineAlarmRsBerSf)?"*": "-");
    ROSNG_TRACE_DEBUG("%s         ", (enableMask & cAtSdhLineAlarmBerSd)?"*": "-");
    ROSNG_TRACE_DEBUG("%s          ", (enableMask & cAtSdhLineAlarmBerSf)?"*": "-");
    ROSNG_TRACE_DEBUG("%s\n", (enableMask & cAtSdhLineAlarmKByteChange)?"*": "-");
    
    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：show_pos_eth_port_status
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/31            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET show_pos_eth_port_status(BYTE chipId)
{
    DRV_POS_RET ret = DRV_POS_OK;
    BYTE ethPortIdx = 0;
    DRV_POS_ETH_PORT_STATUS ethPortAlm = DRV_POS_ETH_PORT_DOWN;

    DRV_POS_CHIPID_CHECK(chipId);

    ROSNG_TRACE_DEBUG("\n");
    ROSNG_TRACE_DEBUG("chip [%d]\n", chipId);
    
    ret = drv_pos_ethPortAlarmGet(chipId, ethPortIdx, &ethPortAlm);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] ethPortIdx[%d] drv_pos_ethPortAlarmGet failed, return %d", chipId, ethPortIdx, ret);

    ROSNG_TRACE_DEBUG("ethPortIdx[%d]    :    %s\n",ethPortIdx, (DRV_POS_ETH_PORT_DOWN == ethPortAlm) ? "DOWN" : "UP");

    ethPortIdx = 1;
    ethPortAlm = DRV_POS_ETH_PORT_DOWN;
    ret = drv_pos_ethPortAlarmGet(chipId, ethPortIdx, &ethPortAlm);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] ethPortIdx[%d] drv_pos_ethPortAlarmGet failed, return %d", chipId, ethPortIdx, ret);
    ROSNG_TRACE_DEBUG("ethPortIdx[%d]    :    %s\n", ethPortIdx, (DRV_POS_ETH_PORT_DOWN == ethPortAlm) ? "DOWN" : "UP");

    return DRV_POS_OK;
}
/*********************************************************************
* 函数名称：show_pos_ppp_counts
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/30            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET show_pos_ppp_counts(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtHdlcLink atHdlcLink = NULL;
    tAtPppLinkCounters pppLinkCounters;
    eAtRet atRet = cAtOk;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    memset(&pppLinkCounters, 0, sizeof(pppLinkCounters));

    ret = drv_pos_get_atHdlcLink(chipId, portIdx, &atHdlcLink);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] portIdx[%d] drv_pos_get_atHdlcLink failed, return %d", chipId, portIdx, ret);

    atRet = AtChannelAllCountersClear((AtChannel)atHdlcLink, &pppLinkCounters);
    if(atRet != cAtOk)
    {
        DRV_POS_ERROR_PRINT("chip[%d] portIdx[%d] AtChannelAllCountersGet failed, return %s, atHdlcLink = %p", 
            chipId, portIdx, AtRet2String(atRet), atHdlcLink);
        return DRV_POS_AT_ERR;
    }

    ROSNG_TRACE_DEBUG("***************** ppp link counts *******************\n");
    ROSNG_TRACE_DEBUG("| chipId[%d]   portIdx[%d] atHdlcLink[%p]\n", chipId, portIdx, atHdlcLink);
    ROSNG_TRACE_DEBUG("|    txByte  |   txPlain  | txFragment |    txOam   | txGoodByte |  txGoodPkt | txDiscardPkt |\n");
    ROSNG_TRACE_DEBUG("| %-10u | %-10u | %-10u | %-10u | %-10u | %-10u | %-12u |\n", 
        pppLinkCounters.txByte, pppLinkCounters.txPlain, pppLinkCounters.txFragment, pppLinkCounters.txOam,
        pppLinkCounters.txGoodByte, pppLinkCounters.txGoodPkt, pppLinkCounters.txDiscardPkt);
    
    ROSNG_TRACE_DEBUG("|-------------------------------------------------------------------------------------------|\n");
    ROSNG_TRACE_DEBUG("|   rxPlain  | rxFragment |    rxOam   | rxExceedMru | rxGoodByte |  rxGoodPkt |rxDiscardPkt|\n");
    ROSNG_TRACE_DEBUG("| %-10u | %-10u | %-10u | %-11u | %-10u | %-10u | %-10u |\n",
        pppLinkCounters.rxPlain, pppLinkCounters.rxFragment, pppLinkCounters.rxOam, pppLinkCounters.rxExceedMru, 
        pppLinkCounters.rxGoodByte, pppLinkCounters.rxGoodPkt, pppLinkCounters.rxDiscardPkt);
    return ret;
}
/*********************************************************************
* 函数名称：show_pos_ethport_counts
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/9/22            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET show_pos_eth_flow_counts(BYTE chipId, BYTE portIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtEthFlow atEthFlow = NULL;
    tAtEthFlowCounters ethFlowCounters;
    eAtRet atRet = cAtOk;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    memset(&ethFlowCounters, 0, sizeof(ethFlowCounters));

    ret = drv_pos_get_atEthFlow(chipId, portIdx, &atEthFlow);
    if(DRV_POS_ETH_FLOW_NOT_CREATE == ret)
    {
        DRV_POS_ERROR_RET(ret, "chip[%d] portIdx[%d] atEthFlow not been create, drv_pos_get_atEthFlow return %d",
            chipId, portIdx, ret);
    }

    atRet = AtChannelAllCountersClear((AtChannel)atEthFlow , &ethFlowCounters);
    if(atRet != cAtOk)
    {
        DRV_POS_ERROR_PRINT("chip[%d] ethIdx[%d] AtChannelAllCountersGet failed, return %s, atEthFlow = %p", 
            chipId, portIdx, AtRet2String(atRet), atEthFlow);
        return DRV_POS_AT_ERR;
    }

    ROSNG_TRACE_DEBUG("***************** eth Port counts *******************\n");
    ROSNG_TRACE_DEBUG("chipId[%d] portIdx[%d] atEthFlow[%p]\n", chipId, portIdx, atEthFlow);
    
    return ret;
}

/*********************************************************************
* 函数名称：show_pos_ethport_counts
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/9/22            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET show_pos_eth_port_counts(BYTE chipId, BYTE ethIdx)
{
    DRV_POS_RET ret = DRV_POS_OK;
    AtEthPort atEthPort = NULL;
    tAtEthPortCounters ethPortCounters;
    eAtRet atRet = cAtOk;

    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_ETHPORTIDX_CHECK(ethIdx);

    memset(&ethPortCounters, 0, sizeof(ethPortCounters));

    ret = drv_pos_get_atEthPort(chipId, ethIdx, &atEthPort);
    DRV_POS_IF_ERROR_RET(ret, "chip[%d] ethIdx[%d] drv_pos_get_atEthPort failed, return %d", chipId, ethIdx, ret);

    atRet = AtChannelAllCountersClear((AtChannel)atEthPort , &ethPortCounters);
    if(atRet != cAtOk)
    {
        DRV_POS_ERROR_PRINT("chip[%d] ethIdx[%d] AtChannelAllCountersGet failed, return %s, atEthPort = %p", 
            chipId, ethIdx, AtRet2String(atRet), atEthPort);
        return DRV_POS_AT_ERR;
    }

    ROSNG_TRACE_DEBUG("***************** eth Port counts *******************\n");
    ROSNG_TRACE_DEBUG("chipId[%d] ethIdx[%d] atEthPort[%p]\n", chipId, ethIdx, atEthPort);
    ROSNG_TRACE_DEBUG("------------------ TX -------------------\n");
    ROSNG_TRACE_DEBUG("txPackets              :  %u\n", ethPortCounters.txPackets);
    ROSNG_TRACE_DEBUG("txBytes                :  %u\n", ethPortCounters.txBytes);
    ROSNG_TRACE_DEBUG("txOamPackets           :  %u\n", ethPortCounters.txOamPackets);
    ROSNG_TRACE_DEBUG("txPeriodOamPackets     :  %u\n", ethPortCounters.txPeriodOamPackets);
    ROSNG_TRACE_DEBUG("txPwPackets            :  %u\n", ethPortCounters.txPwPackets);
    ROSNG_TRACE_DEBUG("txPacketsLen0_64       :  %u\n", ethPortCounters.txPacketsLen0_64);
    ROSNG_TRACE_DEBUG("txPacketsLen65_127     :  %u\n", ethPortCounters.txPacketsLen65_127);
    ROSNG_TRACE_DEBUG("txPacketsLen128_255    :  %u\n", ethPortCounters.txPacketsLen128_255);
    ROSNG_TRACE_DEBUG("txPacketsLen256_511    :  %u\n", ethPortCounters.txPacketsLen256_511);
    ROSNG_TRACE_DEBUG("txPacketsLen512_1024   :  %u\n", ethPortCounters.txPacketsLen512_1024);
    ROSNG_TRACE_DEBUG("txPacketsLen1025_1528  :  %u\n", ethPortCounters.txPacketsLen1025_1528);
    ROSNG_TRACE_DEBUG("txPacketsJumbo         :  %u\n", ethPortCounters.txPacketsJumbo);
    ROSNG_TRACE_DEBUG("txTopPackets           :  %u\n", ethPortCounters.txTopPackets);
    ROSNG_TRACE_DEBUG("------------------ RX -------------------\n");
    ROSNG_TRACE_DEBUG("MAC layer                             |  Uper layer\n");
    ROSNG_TRACE_DEBUG("rxPackets              :  %10u  | rxIpv4Packets            :  %10u\n", ethPortCounters.rxPackets, ethPortCounters.rxIpv4Packets);            
    ROSNG_TRACE_DEBUG("rxBytes                :  %10u  | rxErrIpv4Packets         :  %10u\n", ethPortCounters.rxBytes, ethPortCounters.rxErrIpv4Packets);              
    ROSNG_TRACE_DEBUG("rxErrEthHdrPackets     :  %10u  | rxIcmpIpv4Packets        :  %10u\n", ethPortCounters.rxErrEthHdrPackets, ethPortCounters.rxIcmpIpv4Packets);   
    ROSNG_TRACE_DEBUG("rxErrBusPackets        :  %10u  | rxIpv6Packets            :  %10u\n", ethPortCounters.rxErrBusPackets, ethPortCounters.rxIpv6Packets);
    ROSNG_TRACE_DEBUG("rxErrFcsPackets        :  %10u  | rxErrIpv6Packets         :  %10u\n", ethPortCounters.rxErrFcsPackets, ethPortCounters.rxErrIpv6Packets);
    ROSNG_TRACE_DEBUG("rxOversizePackets      :  %10u  | rxIcmpIpv6Packets        :  %10u\n", ethPortCounters.rxOversizePackets, ethPortCounters.rxIcmpIpv6Packets);
    ROSNG_TRACE_DEBUG("rxUndersizePackets     :  %10u  | rxMefPackets             :  %10u\n", ethPortCounters.rxUndersizePackets,ethPortCounters.rxMefPackets);
    ROSNG_TRACE_DEBUG("rxPacketsLen0_64       :  %10u  | rxErrMefPackets          :  %10u\n", ethPortCounters.rxPacketsLen0_64, ethPortCounters.rxErrMefPackets);
    ROSNG_TRACE_DEBUG("rxPacketsLen65_127     :  %10u  | rxMplsPackets            :  %10u\n", ethPortCounters.rxPacketsLen65_127, ethPortCounters.rxMplsPackets);
    ROSNG_TRACE_DEBUG("rxPacketsLen128_255    :  %10u  | rxErrMplsPackets         :  %10u\n", ethPortCounters.rxPacketsLen128_255, ethPortCounters.rxErrMplsPackets);
    ROSNG_TRACE_DEBUG("rxPacketsLen256_511    :  %10u  | rxMplsErrOuterLblPackets :  %10u\n", ethPortCounters.rxPacketsLen256_511, ethPortCounters.rxMplsErrOuterLblPackets);
    ROSNG_TRACE_DEBUG("rxPacketsLen512_1024   :  %10u  | rxMplsDataPackets        :  %10u\n", ethPortCounters.rxPacketsLen512_1024, ethPortCounters.rxMplsDataPackets);
    ROSNG_TRACE_DEBUG("rxPacketsLen1025_1528  :  %10u  | rxLdpIpv4Packets         :  %10u\n", ethPortCounters.rxPacketsLen1025_1528, ethPortCounters.rxLdpIpv4Packets);
    ROSNG_TRACE_DEBUG("rxPacketsJumbo         :  %10u  | rxLdpIpv6Packets         :  %10u\n", ethPortCounters.rxPacketsJumbo, ethPortCounters.rxLdpIpv6Packets);
    ROSNG_TRACE_DEBUG("rxPwUnsupportedPackets :  %10u  | rxMplsIpv4Packets        :  %10u\n", ethPortCounters.rxPwUnsupportedPackets, ethPortCounters.rxMplsIpv4Packets);
    ROSNG_TRACE_DEBUG("rxErrPwLabelPackets    :  %10u  | rxMplsIpv6Packets        :  %10u\n", ethPortCounters.rxErrPwLabelPackets, ethPortCounters.rxMplsIpv6Packets);
    ROSNG_TRACE_DEBUG("rxDiscardedPackets     :  %10u  | rxL2tpv3Packets          :  %10u\n", ethPortCounters.rxDiscardedPackets, ethPortCounters.rxL2tpv3Packets);
    ROSNG_TRACE_DEBUG("rxPausePackets         :  %10u  | rxErrL2tpv3Packets       :  %10u\n", ethPortCounters.rxPausePackets, ethPortCounters.rxErrL2tpv3Packets);
    ROSNG_TRACE_DEBUG("rxErrPausePackets      :  %10u  | rxL2tpv3Ipv4Packets      :  %10u\n", ethPortCounters.rxErrPausePackets, ethPortCounters.rxL2tpv3Ipv4Packets);
    ROSNG_TRACE_DEBUG("rxBrdCastPackets       :  %10u  | rxL2tpv3Ipv6Packets      :  %10u\n", ethPortCounters.rxMultCastPackets, ethPortCounters.rxL2tpv3Ipv6Packets);
    ROSNG_TRACE_DEBUG("rxArpPackets           :  %10u  | rxUdpPackets             :  %10u\n", ethPortCounters.rxArpPackets, ethPortCounters.rxUdpPackets);
    ROSNG_TRACE_DEBUG("rxOamPackets           :  %10u  | rxErrUdpPackets          :  %10u\n", ethPortCounters.rxOamPackets, ethPortCounters.rxErrUdpPackets);
    ROSNG_TRACE_DEBUG("rxEfmOamPackets        :  %10u  | rxUdpIpv4Packets         :  %10u\n", ethPortCounters.rxEfmOamPackets, ethPortCounters.rxUdpIpv4Packets);
    ROSNG_TRACE_DEBUG("rxErrEfmOamPackets     :  %10u  | rxUdpIpv6Packets         :  %10u\n", ethPortCounters.rxErrEfmOamPackets, ethPortCounters.rxUdpIpv6Packets);
    ROSNG_TRACE_DEBUG("rxEthOamType1Packets   :  %10u  | rxErrPsnPackets          :  %10u\n", ethPortCounters.rxEthOamType1Packets, ethPortCounters.rxErrPsnPackets);
    ROSNG_TRACE_DEBUG("rxEthOamType2Packets   :  %10u  | rxPacketsSendToCpu       :  %10u\n", ethPortCounters.rxEthOamType2Packets, ethPortCounters.rxPacketsSendToCpu);
    ROSNG_TRACE_DEBUG("                                      | rxPacketsSendToPw        :  %10u\n", ethPortCounters.rxPacketsSendToPw);
    ROSNG_TRACE_DEBUG("                                      | rxTopPackets             :  %10u\n", ethPortCounters.rxTopPackets);
    ROSNG_TRACE_DEBUG("                                      | rxPacketDaMis            :  %10u\n", ethPortCounters.rxPacketDaMis);
    
    
    return ret;
}

/*********************************************************************
* 函数名称：drv_pos_help
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/30            V1.0               liuyu        创建
*********************************************************************/
void drv_pos_help(void)
{
    ROSNG_TRACE_DEBUG("*************************** DRV POS HELP ***************************\n");
    ROSNG_TRACE_DEBUG("+------------------------------------------------------------------+\n");
    ROSNG_TRACE_DEBUG("|                      POS-PPP SWITCH                              |\n");
    ROSNG_TRACE_DEBUG("+------------------------------------------------------------------+\n");
    ROSNG_TRACE_DEBUG("| drv_pos_err_on/off                                               |\n");
    ROSNG_TRACE_DEBUG("| drv_pos_suc_on/off                                               |\n");
    ROSNG_TRACE_DEBUG("| drv_pos_set_perf_debug                                           |\n");    
    ROSNG_TRACE_DEBUG("|                                0  DRV_POS_PERF_DEBUG_OFF         |\n");
    ROSNG_TRACE_DEBUG("|                                1  DRV_POS_SDH_SEC_PERF_DBG       |\n");
    ROSNG_TRACE_DEBUG("|                                2  DRV_POS_SDH_HIG_PERF_DBG       |\n");
    ROSNG_TRACE_DEBUG("|                                3  DRV_POS_PPP_PERF_DBG           |\n");
    ROSNG_TRACE_DEBUG("+------------------------------------------------------------------|\n");
    ROSNG_TRACE_DEBUG("|                   POS_PPP_SOFT_TABLE                             |\n");
    ROSNG_TRACE_DEBUG("+------------------------------------------------------------------+\n");
    ROSNG_TRACE_DEBUG("| show_pos_init_state(chipId)                                      |\n");
    ROSNG_TRACE_DEBUG("| show_pos_global_info(chipId)                                     |\n");
    ROSNG_TRACE_DEBUG("| show_pos_link_info(chipId, portIdx)                              |\n");
    ROSNG_TRACE_DEBUG("| show_pos_thread_id()                                             |\n");
    ROSNG_TRACE_DEBUG("| show_pos_thread_flag(chipId)                                     |\n");
    ROSNG_TRACE_DEBUG("| show_sdh_interface_status(chipId, portIdx)                       |\n");
    ROSNG_TRACE_DEBUG("| show_sdh_alarm_table(chipId, portIdx)                            |\n");
    ROSNG_TRACE_DEBUG("| show_sdh_perf_table(chipId, portIdx)                             |\n");
    ROSNG_TRACE_DEBUG("| show_sdh_overhead_table(chipId, portIdx)                         |\n");
    ROSNG_TRACE_DEBUG("| show_ppp_perf_table(chipId, portIdx)                             |\n");
    ROSNG_TRACE_DEBUG("| show_ppp_port_mapping(chipId)                                    |\n");
    ROSNG_TRACE_DEBUG("| show_ppp_alarm_intr_mask(chipId, portIdx)                        |\n");
    ROSNG_TRACE_DEBUG("| show_pos_eth_port_status(chipId, portIdx)                        |\n");
    ROSNG_TRACE_DEBUG("+------------------------------------------------------------------|\n");
    ROSNG_TRACE_DEBUG("|                     POS_PPP_PKT_COUNT                            |\n");
    ROSNG_TRACE_DEBUG("|                dir: 1---rx, 2---tx, 3---rxtx                     |\n");
    ROSNG_TRACE_DEBUG("+------------------------------------------------------------------+\n");
    ROSNG_TRACE_DEBUG("| show_pos_ppp_counts(chipId, portIdx)                             |\n");
    ROSNG_TRACE_DEBUG("| show_pos_eth_flow_counts(chipId, portIdx)       no               |\n");
    ROSNG_TRACE_DEBUG("| show_pos_eth_port_counts(chipId, ethIdx)                         |\n");
    ROSNG_TRACE_DEBUG("+------------------------------------------------------------------+\n");
}

/*********************************************************************
* 函数名称：show_at_sdh_value
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/11/20            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET show_at_sdh_value(BYTE chipId, BYTE portIdx)
{
    AtModuleSdh atModuleSdh = NULL;
    AtSdhLine atSdhLine = NULL;
    AtSerdesController atSerdesCtrler = NULL;
    AtSdhVc atSdhVc = NULL;
    AtSdhAu atSdhAu = NULL;
        
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);

    drv_pos_get_atModuleSdh(chipId, &atModuleSdh);
    drv_pos_get_atSdhLine(chipId, portIdx, &atSdhLine);
    drv_pos_get_atSerdesController( chipId, portIdx, &atSerdesCtrler);
    drv_pos_get_atSdhVC(chipId, portIdx, &atSdhVc);
    drv_pos_get_atSdhAu(chipId, portIdx, &atSdhAu);
    
    ROSNG_TRACE_DEBUG("+---------------------------------+\n");
    ROSNG_TRACE_DEBUG("|                AT SDH           |\n");
    ROSNG_TRACE_DEBUG("+---------------------------------+\n");
    ROSNG_TRACE_DEBUG("| atModuleSdh = %p        |\n", atModuleSdh);
    ROSNG_TRACE_DEBUG("| atSdhLine = %p          |\n", atSdhLine);
    ROSNG_TRACE_DEBUG("| atSerdesController = %p |\n", atSerdesCtrler);
    ROSNG_TRACE_DEBUG("| atSdhVc = %p            |\n", atSdhVc);
    ROSNG_TRACE_DEBUG("| atSdhAu = %p            |\n", atSdhAu);
    ROSNG_TRACE_DEBUG("+---------------------------------|\n");

    return DRV_POS_OK;

}
/*********************************************************************
* 函数名称：show_at_eth_value
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/11/22            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET show_at_eth_value(BYTE chipId, BYTE ethIdx)
{
    AtModuleEth atModuleEth = NULL;
    AtEthPort atEthPort = NULL;
        
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_ETHPORTIDX_CHECK(ethIdx);
    
    drv_pos_get_atModuleEth(chipId, &atModuleEth);
    drv_pos_get_atEthPort(chipId, ethIdx, &atEthPort);
    
    ROSNG_TRACE_DEBUG("+---------------------------------+\n");
    ROSNG_TRACE_DEBUG("|                AT ETH           |\n");
    ROSNG_TRACE_DEBUG("+---------------------------------+\n");
    ROSNG_TRACE_DEBUG("| atModuleEth = %p        |\n", atModuleEth);
    ROSNG_TRACE_DEBUG("| atEthPort = %p          |\n", atEthPort);
    ROSNG_TRACE_DEBUG("+---------------------------------|\n");

    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：show_at_encap_value
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/11/22            V1.0               liuyu        创建
*********************************************************************/
DRV_POS_RET show_at_encap_value(BYTE chipId, BYTE portIdx)
{
    AtModuleEncap atModuleEncap = NULL;
    AtHdlcLink atHdlcLink = NULL;
         
    DRV_POS_CHIPID_CHECK(chipId);
    DRV_POS_PORTIDX_CHECK(chipId, portIdx);
   
    drv_pos_get_atModuleEncap(chipId, &atModuleEncap);
    drv_pos_get_atHdlcLink(chipId, portIdx, &atHdlcLink);
    
    ROSNG_TRACE_DEBUG("+---------------------------------+\n");
    ROSNG_TRACE_DEBUG("|              AT ENCAP           |\n");
    ROSNG_TRACE_DEBUG("+---------------------------------+\n");
    ROSNG_TRACE_DEBUG("| atModuleEncap = %p       |\n", atModuleEncap);
    ROSNG_TRACE_DEBUG("| atHdlcLink = %p          |\n", atHdlcLink);
    ROSNG_TRACE_DEBUG("+---------------------------------|\n");

    return DRV_POS_OK;
}

/*********************************************************************
* 函数名称：drv_at_pos_help
* 功能描述：
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/8/30            V1.0               liuyu        创建
*********************************************************************/
void drv_at_pos_help(void)
{
    ROSNG_TRACE_DEBUG("************************** DRV AT POS HELP *************************\n");
    ROSNG_TRACE_DEBUG("+------------------------------------------------------------------+\n");
    ROSNG_TRACE_DEBUG("|                       AT POS LOOPBACK                            |\n");
    ROSNG_TRACE_DEBUG("+------------------------------------------------------------------+\n");
    ROSNG_TRACE_DEBUG("| atcli \"sdh line loopback remote 1\"                             |\n");
    ROSNG_TRACE_DEBUG("| atcli \"sdh line loopback local 1\"                              |\n");
    ROSNG_TRACE_DEBUG("| atcli \"sdh line loopback release 1\"                            |\n");
    ROSNG_TRACE_DEBUG("| atcli \"wr 4000b 0\"                     release                 |\n");
    ROSNG_TRACE_DEBUG("| atcli \"wr 4000b 1\"                     remote                  |\n");
    ROSNG_TRACE_DEBUG("| atcli \"wr 4000b 2\"                     local                   |\n");
    ROSNG_TRACE_DEBUG("| atcli \"wr 104000b 0\"                   release                 |\n");
    ROSNG_TRACE_DEBUG("| atcli \"wr 104000b 1\"                   remote                  |\n");
    ROSNG_TRACE_DEBUG("| atcli \"wr 104000b 2\"                   local                   |\n");
    ROSNG_TRACE_DEBUG("| atcli \"eth port loopback 1 remote\"                             |\n");
    ROSNG_TRACE_DEBUG("| atcli \"eth port loopback 1 local\"                              |\n");
    ROSNG_TRACE_DEBUG("| atcli \"eth port loopback 1 release\"                            |\n");
    ROSNG_TRACE_DEBUG("+------------------------------------------------------------------|\n");
    ROSNG_TRACE_DEBUG("|                       AT POS DUMP PKT                            |\n");
    ROSNG_TRACE_DEBUG("+------------------------------------------------------------------+\n");                                            
    ROSNG_TRACE_DEBUG("| atcli \"wr 40f000 0x15\"      FROM DECAP TO MPIG                 |\n");
    ROSNG_TRACE_DEBUG("| atcli \"wr 40f000 0x55\"      MPIG TO CLA                        |\n");
    ROSNG_TRACE_DEBUG("| atcli \"wr 40f000 0x14\"      MPIG TO GE                         |\n");
    ROSNG_TRACE_DEBUG("| atcli \"wr 40f000 0x00\"      FROM TX GE (INCLUDE PREAMBLE+FCS)  |\n");
    ROSNG_TRACE_DEBUG("| atcli \"wr 40f000 0x02\"      FROM TX GE (ONLY DA+SA+...)        |\n");
    ROSNG_TRACE_DEBUG("| atcli \"wr 40f000 0x01\"      FROM RX GE (INCLUDE PREAMBLE+FCS)  |\n");
    ROSNG_TRACE_DEBUG("| atcli \"wr 40f000 0x03\"      FROM RX GE (ONLY DA+SA+...)        |\n");
    ROSNG_TRACE_DEBUG("| atcli \"wr 40f000 0x12\"      FROM RX GE TO CLA                  |\n");
    ROSNG_TRACE_DEBUG("| atcli \"wr 40f000 0x13\"      FROM CLA TO MPEG                   |\n");
    ROSNG_TRACE_DEBUG("| atcli \"wr 40f000 0x10\"      FROM MPEG TO ENC                   |\n");
    ROSNG_TRACE_DEBUG("| atcli \"dump 40f400 40f432\"  dump the pkt                       |\n");
    ROSNG_TRACE_DEBUG("+------------------------------------------------------------------|\n");
    ROSNG_TRACE_DEBUG("| atcli \"pktanalyzer create eport.<portId>\"                      |\n");
    ROSNG_TRACE_DEBUG("| atcli \"pktanalyzer create flow.<flowId>\"                       |\n");
    ROSNG_TRACE_DEBUG("| atcli \"pktanalyzer create ppp.<linkId>\"                        |\n");
    ROSNG_TRACE_DEBUG("| atcli \"pktanalyzer analyze rx\"                                 |\n");
    ROSNG_TRACE_DEBUG("| atcli \"pktanalyzer analyze tx\"                                 |\n");
    ROSNG_TRACE_DEBUG("+------------------------------------------------------------------+\n");
}

/*********************************************************************
* 函数名称：drv_pos_op_help
* 功能描述：pos操作帮助，用于诊断，调试
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:  
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2014/1/13            V1.0               liuyu        创建
*********************************************************************/
void drv_pos_op_help(void)
{
    ROSNG_TRACE_DEBUG("************************** DRV POS OP HELP *************************\n");
    ROSNG_TRACE_DEBUG("+------------------------------------------------------------------+\n");
    ROSNG_TRACE_DEBUG("|                         POS PORT OP                              |\n");
    ROSNG_TRACE_DEBUG("+------------------------------------------------------------------+\n");
    ROSNG_TRACE_DEBUG("| drv_pos_ethPortLoopbackModeSet(chipId, ethIdx, loopbackMode)     |\n");
    ROSNG_TRACE_DEBUG("|      0--release, 1--local, 2--remote|\n");
    ROSNG_TRACE_DEBUG("| drv_pos_portLoopbackSet(chipId, portIdx, loopbackMode)           |\n");
    ROSNG_TRACE_DEBUG("| drv_pos_portScrambleSet(chipId, portIdx, scrambleEn)             |\n");
    ROSNG_TRACE_DEBUG("| drv_pos_portTxLaserStateSet(chipId, portIdx, state)              |\n");
    ROSNG_TRACE_DEBUG("| drv_pos_portShutdownStateSet(chipId, portIdx, state)             |\n");
    ROSNG_TRACE_DEBUG("| drv_pos_portFcsModeSet(chipId, portIdx, fcsMode)                |\n");
    ROSNG_TRACE_DEBUG("| drv_pos_ppp_mode_set(chipId, portGrp, posPppMode)                |\n");
    ROSNG_TRACE_DEBUG("|                             |\n");
    ROSNG_TRACE_DEBUG("|                             |\n");
    ROSNG_TRACE_DEBUG("|                             |\n");
    ROSNG_TRACE_DEBUG("|                             |\n");
}


/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_at_e1_pwe3.c
* 文件标识: 
* 其它说明: ce1tan ces 业务代码
* 当前版本: V1.0
* 作    者: 陈博10140635.
* 完成日期: 2013-05-14
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/
#include "AtDriver.h"
#include "AtDevice.h"
#include "AtModulePdh.h"
#include "AtModulePw.h"
#include "AtPwCESoP.h"
#include "AtPwCounters.h"
#include "AtModuleEth.h"
#include "AtPdhDe1.h"
#include "AtPw.h"
#include "AtZtePtn.h"
#include "AtPktAnalyzer.h"
#include "AtModulePktAnalyzer.h"
#include "AtEthPort.h"
#include "drv_at_e1_pwe3.h"
#include "drv_at_e1_port.h"
#include "drv_at_e1_clk.h"
#include "bsp_hdlc_common.h"

/*全局变量*/
DRV_AT_PW_PARA  g_atE1PwInfo [AT_MAX_SUBCARD_NUM_PER_SLOT] [AT_MAX_PW_NUM_PER_SUBCARD] ;         /*驱动pw 信息软件表，基于子卡*/
DRV_AT_PW_COUNT g_atE1PwCount[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_MAX_PW_NUM_PER_SUBCARD];        /*pw 报文计数*/
DRV_AT_PW_COUNT g_atE1PwCountSum[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_MAX_PW_NUM_PER_SUBCARD];  /*pw 报文累加计数*/
DRV_AT_PW_COUNT g_atE1PwCountToPm[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_MAX_PW_NUM_PER_SUBCARD]; /*bsp上报给产品管理的ces统计*/
WORD16  g_atE1PwTotalNum[AT_MAX_SUBCARD_NUM_PER_SLOT];                 /*子卡上存在的pw数量统计*/
WORD32  g_atCesopLinksNum[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_E1_INTF_NUM];/*cesop模式下一条e1里的通道数*/
WORD32  g_pwThrcount[AT_MAX_SUBCARD_NUM_PER_SLOT] ;
WORD32  g_pwThrcountDbg[AT_MAX_SUBCARD_NUM_PER_SLOT] ;
WORD32  g_pwAlmThrcount[AT_MAX_SUBCARD_NUM_PER_SLOT] ;
WORD32  g_pwAlmThrcountDbg[AT_MAX_SUBCARD_NUM_PER_SLOT] ;
WORD32  g_isPwClkAlarm[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_MAX_PW_NUM_PER_SUBCARD];
struct mod_thread  g_pwCountThrMod[AT_MAX_SUBCARD_NUM_PER_SLOT];

/*外部全局变量*/
extern AtDevice DrvAtDevHdlGet(BYTE chipId);
extern CE1TAN_THREAD_FLAG g_threadFlag[AT_MAX_SUBCARD_NUM_PER_SLOT];
extern CE1TAN_THREAD_HANDLE g_threadHdl[AT_MAX_SUBCARD_NUM_PER_SLOT];
extern CE1TAN_THREAD_ID g_threadId[AT_MAX_SUBCARD_NUM_PER_SLOT];

/*外部函数声明*/
extern WORD32 DrvAtEthPortObjGet(BYTE chipId,BYTE ethPortId, AtEthPort* atEthPort);
extern WORD32 DrvAtE1ObjGet(BYTE chipId,WORD32 e1LinkId, AtPdhDe1* atE1Obj);
extern WORD32 DrvAtE1Nxds0ObjCreate(BYTE chipId,WORD32 e1LinkNo,WORD32 tsBitMap,AtPdhNxDS0 *atNxds0Obj);
extern WORD32 DrvAtE1Nxds0ObjGet(BYTE chipId,WORD32 e1LinkNo,WORD32 tsBitMap,AtPdhNxDS0 *atNxds0Obj);
extern WORD32 DrvAtE1ClkModeSet(BYTE chipId,WORD32 e1LinkNo,BYTE clkMode,WORD32 srcE1LinkNo,WORD32 pwId);
extern  int __gettid (void);
extern WORD32 BSP_CE1TAN_BoardStatusGet(WORD32 dwSubslotId, BYTE *pdwIsOnline);

/*******************************************************************************
* 函数名称:   DrvAtSavePwCfg
* 功能描述:   保存pw配置
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14    v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtSavePwCfg (BYTE chipId, WORD32 pwId, DRV_AT_PW_PARA *pwInfo)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    DRV_AT_PW_PARA * glbPwInfo = NULL ;
    
    CHECK_AT_POINTER_NULL(pwInfo);    
    glbPwInfo = &(g_atE1PwInfo[chipId][pwId]); /*参数在业务函数入口处已做过检查*/
    CHECK_AT_POINTER_NULL(glbPwInfo);
    memcpy(glbPwInfo, pwInfo, sizeof(DRV_AT_PW_PARA));
    
    return ret ;
}


/*******************************************************************************
* 函数名称:   DrvAtIdlePwIdGet
* 功能描述:   获取空闲可用的pwid
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtIdlePwIdGet(BYTE  chipId,WORD32 *pwId)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    WORD32 i = 0;
    
    CHECK_AT_POINTER_NULL(pwId);
    *pwId = INVALID_PW_ID; /*lint !e578*/  /* default set to invalid PW id value */
    
    for(i = 0; i < AT_MAX_PW_NUM_PER_SUBCARD ; i++)
    {
        if(PW_IDLE == g_atE1PwInfo[chipId][i].pwStatus)/*找到了一条可用的信道*/
        {
            *pwId = i;
            break;
        }
    }
    if(INVALID_PW_ID == (*pwId))
    {
        return DRV_AT_INVALID_PW_ID ;
    }
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtPwStatusCheck
* 功能描述:   检查pw是否被创建
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32     DrvAtPwStatusCheck(BYTE chipId ,WORD32 pwId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    
    if(PW_USED != g_atE1PwInfo[chipId][pwId].pwStatus)    /*表明还没被创建*/
    {
        ret = DRV_AT_PW_NOT_CREATE ;
    }
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtPwIdGetFromCip
* 功能描述:   找到cip 对应的pwid
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwIdGetFromCip(BYTE chipId,WORD32 cip)
{
    WORD32 i = 0;
    WORD32 pwId = INVALID_PW_ID; 
    
    for(i = 0 ; i< AT_MAX_PW_NUM_PER_SUBCARD ; i++)
    {
        if((g_atE1PwInfo[chipId][i].cip == cip)&&(PW_USED == g_atE1PwInfo[chipId][i].pwStatus))
        {
            pwId = i ;
            break ;
        }
    }  
    return pwId ;
}
    

 /*******************************************************************************
* 函数名称:   DrvAtPwInfoGetFromCip
* 功能描述:   找到cip 对应的pw信息
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwInfoGetFromCip(BYTE chipId,WORD32 cip,DRV_AT_PW_PARA* drvPwInfo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 i = 0;
    WORD32 pwId = INVALID_PW_ID; 
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(drvPwInfo);
    
    for(i = 0 ; i< AT_MAX_PW_NUM_PER_SUBCARD ; i++)
    {
        if((g_atE1PwInfo[chipId][i].cip == cip)&&(PW_USED == g_atE1PwInfo[chipId][i].pwStatus))
        {
            pwId = i ;
            break ;
        }
    }  
    if(pwId != INVALID_PW_ID)  
    {
        memcpy(drvPwInfo,&g_atE1PwInfo[chipId][pwId],sizeof(DRV_AT_PW_PARA));
    }
    else /*如果这条cip没有创建，返回失败*/
    {
        ret = DRV_AT_PW_INFO_GET_FAILED;
    }
    return ret ;
}


 /*******************************************************************************
* 函数名称:   DrvAtPwInfoGetFromPwId
* 功能描述:   通过pwid索引pw信息
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwInfoGetFromPwId(BYTE chipId,WORD32 pwId,DRV_AT_PW_PARA* drvPwInfo)
{
    WORD32 ret = DRV_AT_SUCCESS;
      
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_PW_ID(pwId);
    CHECK_AT_POINTER_NULL(drvPwInfo);
    
    memcpy(drvPwInfo,&g_atE1PwInfo[chipId][pwId],sizeof(DRV_AT_PW_PARA));
    
    return ret ;

}

 /*******************************************************************************
* 函数名称:   DrvAtPwClkAlarmGetFromPwId
* 功能描述:   通过pwid索引pw时钟域告警
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwClkAlarmGetFromPwId(BYTE chipId,WORD32 pwId)
{
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_PW_ID(pwId);
    
    return g_isPwClkAlarm[chipId][pwId];
}
 
 /*******************************************************************************
* 函数名称:   DrvAtPwInfoRelease
* 功能描述:   释放pw的全局表
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwInfoRelease (BYTE chipId ,WORD32 pwId )
{
    WORD32 ret = DRV_AT_SUCCESS;
    
    memset((&g_atE1PwInfo[chipId][pwId]),0,sizeof(DRV_AT_PW_PARA)) ; 
    memset(&g_atE1PwCount[chipId][pwId],0,sizeof(DRV_AT_PW_COUNT));
    memset(&g_atE1PwCountSum[chipId][pwId],0,sizeof(DRV_AT_PW_COUNT));

    return ret ;
}
 
/*******************************************************************************
* 函数名称:   DrvAtPwCircuitBindCheck
* 功能描述:   对pw绑定的电路进行检查看是否已被别的业务绑定
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwCircuitBindCheck(BYTE chipId ,WORD32 e1LinkNo,BYTE servType,WORD32 tsBitmap)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 i = 0;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    
    for(i = 0 ; i< AT_MAX_PW_NUM_PER_SUBCARD ; i++)
    {
        if((g_atE1PwInfo[chipId][i].chipId == chipId)
        &&(g_atE1PwInfo[chipId][i].e1LinkNum == e1LinkNo)
        &&(g_atE1PwInfo[chipId][i].pwStatus == PW_USED))
        {
            if(SATOP_MODE == servType)
            {
                ret = DRV_AT_PW_CIRCUIT_ALREADY_BIND ;
                break ;
            }
            else if (CESOP_MODE == servType)
            {
                if((g_atE1PwInfo[chipId][i].timeSlotBitMap == tsBitmap))
                {
                    ret = DRV_AT_PW_CIRCUIT_ALREADY_BIND;
                    break;
                }           
            }
        }  
    }    
    return ret;
}

 /*******************************************************************************
* 函数名称:   DrvAtCesopLinksNumGet
* 功能描述:   获取cesop模式下一条e1里的通道数
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtCesopLinksNumGet(BYTE chipId, WORD32 e1LinkNo,WORD32* cesopPwNum)
{
    WORD32 ret = DRV_AT_SUCCESS;
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    CHECK_AT_POINTER_NULL(cesopPwNum);
    
    *cesopPwNum = g_atCesopLinksNum[chipId][e1LinkNo];
    return ret;
}

 /*******************************************************************************
* 函数名称:   DrvAtNoneClkSrcPwIdFind
* 功能描述:   获取一个E1里可以做时钟源的pw
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtNoneClkSrcPwIdFind(BYTE chipId,WORD32 e1LinkNo,WORD32*pwId)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    WORD32 i = 0;
    
    CHECK_AT_POINTER_NULL(pwId);
    *pwId = INVALID_PW_ID; /*lint !e578*/  /* default set to invalid PW id value */
    
    for(i = 0;i< AT_MAX_PW_NUM_PER_SUBCARD; i++)
    {
        if((e1LinkNo == g_atE1PwInfo[chipId][i].e1LinkNum)
        &&(CE1_FALSE == g_atE1PwInfo[chipId][i].isClkSrc)
        &&(PW_USED == g_atE1PwInfo[chipId][i].pwStatus))
        {
            *pwId = i;
            break;
        }
    }
    if(INVALID_PW_ID == (*pwId))
    {
        return DRV_AT_INVALID_PW_ID ;
    }
    return ret ;
}

 /*******************************************************************************
* 函数名称:   DrvAtClkSrcPwIdFind
* 功能描述:   获取一个E1里时钟源的pw
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtClkSrcPwIdFind(BYTE chipId,WORD32 e1LinkNo,WORD32*pwId)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    WORD32 i = 0;
    
    CHECK_AT_POINTER_NULL(pwId);
    *pwId = INVALID_PW_ID; /*lint !e578*/  /* default set to invalid PW id value */
    
    for(i = 0;i< AT_MAX_PW_NUM_PER_SUBCARD; i++)
    {
        if((e1LinkNo == g_atE1PwInfo[chipId][i].e1LinkNum)
        &&(CE1_TURE == g_atE1PwInfo[chipId][i].isClkSrc)
        &&(PW_USED == g_atE1PwInfo[chipId][i].pwStatus))
        {
            *pwId = i;
            break;
        }
    }
    if(INVALID_PW_ID == (*pwId))
    {
        return DRV_AT_INVALID_PW_ID ;
    }
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtPwClkInfoUpdate
* 功能描述:   pw表项中的时钟信息更新
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwClkInfoUpdate(BYTE chipId,WORD32 e1LinkNo,BYTE newClkState,WORD32 newMasterE1LinkNo,BYTE isClkRsc)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 i = 0;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    CHECK_AT_E1LINK_ID(newMasterE1LinkNo);
        
    /*找出e1下绑定的pw，然后更新时钟主从状态*/
    for(i = 0;i< AT_MAX_PW_NUM_PER_SUBCARD; i++)
    {
        if((e1LinkNo == g_atE1PwInfo[chipId][i].e1LinkNum)
        &&(PW_USED == g_atE1PwInfo[chipId][i].pwStatus)
        &&((ACR_CLK_MODE == g_atE1PwInfo[chipId][i].clockMode)||(DCR_CLK_MODE == g_atE1PwInfo[chipId][i].clockMode)))
        {
            g_atE1PwInfo[chipId][i].clkState = newClkState;
            g_atE1PwInfo[chipId][i].masterE1LinkNo = newMasterE1LinkNo;    
            g_atE1PwInfo[chipId][i].isClkSrc = isClkRsc;
        }
    }
     
    return ret;
}


/*******************************************************************************
* 函数名称:   DrvAtPwObjGet
* 功能描述:   获取pw obj
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwObjGet(BYTE chipId,WORD32 pwId,AtPw*atPwObj)
{
    WORD32 ret = DRV_AT_SUCCESS;
    AtDevice atDevHdl =NULL ;
    AtModulePw atPwModule = NULL ;
   
    atDevHdl = DrvAtDevHdlGet(chipId) ;             /*get the devhdl*/
    CHECK_AT_POINTER_NULL(atDevHdl);  
    atPwModule = (AtModulePw)AtDeviceModuleGet(atDevHdl, cAtModulePw);      /*get pw module */
    if(NULL == atPwModule)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PW MODULE GET  FAILED ! !!CHIP ID IS  %u \n", __FILE__, __LINE__,chipId); 
        return DRV_AT_PW_MODULE_GET_FAILED; 
    }
    *atPwObj = AtModulePwGetPw(atPwModule,pwId);
    if(NULL == (*atPwObj))
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PW OBJ GET  FAILED ! !!CHIP ID IS  %u PW ID IS %u \n", __FILE__, __LINE__,chipId,pwId); 
        return DRV_AT_PW_OBJ_GET_FAILED; 
    }
    return ret ;

}

/*******************************************************************************
* 函数名称:   DrvAtPwClkSrcSwitch
* 功能描述:   Cesop业务pw 时钟源切换
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwClkSrcSwitch(BYTE chipId,WORD32 e1LinkNo,BYTE clkMode)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    WORD32 pwId = 0;

    /*select another cesop pw to be the CLKSRC*/  
    ret = DrvAtNoneClkSrcPwIdFind(chipId,e1LinkNo,&pwId);
    if(DRV_AT_INVALID_PW_ID == ret)                                             /*if this is the last pw in cesop e1 ,delete it ,no need to switch*/
    {
        DRV_AT_E1_ERROR_LOG("[CE1TAN][CESOP CLK SWITCH]: %s line %d, CAN NOT FIND ANY PW ID FOR CLK RECOVERY !!CHIP ID IS % u e1LinkNo IS %u\n", __FILE__, __LINE__,chipId, e1LinkNo);
        return DRV_AT_SUCCESS;
    }
    DRV_AT_E1_PRINT(DRV_AT_PW_CFG,"[CE1TAN][CESOP CLK SWITCH]: %s line %d,The timingsrc of E1 %u is pw %u\n",__FILE__, __LINE__,e1LinkNo,pwId);
    
    ret = DrvAtE1ClkModeSet(chipId,e1LinkNo,clkMode,0,pwId);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, E1 CLK MODE  SET  FAILED ! !! \n", __FILE__, __LINE__); 
        return ret; 
    }
    g_atE1PwInfo[chipId][pwId].isClkSrc = CE1_TURE ;
    
    return ret; 
}

/*******************************************************************************
* 函数名称:   DrvAtPwClkModeInfoUpdate
* 功能描述:   pw的时钟模式表项更新
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwClkModeInfoUpdate(BYTE chipId,WORD32 e1LinkNo,BYTE newClkMode )
{
    WORD32 ret = DRV_AT_SUCCESS ;
    WORD32 i = 0;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    
    for(i = 0 ; i< AT_MAX_PW_NUM_PER_SUBCARD ; i++)
    {
        if((g_atE1PwInfo[chipId][i].chipId == chipId)
        &&(g_atE1PwInfo[chipId][i].e1LinkNum == e1LinkNo)
        &&(g_atE1PwInfo[chipId][i].pwStatus == PW_USED))
        {
            g_atE1PwInfo[chipId][i].clockMode = newClkMode;            
        }  
    }
   
    return ret ;
}


/*******************************************************************************
* 函数名称:   DrvAtPwPldSizeInfoUpdate
* 功能描述:   更新驱动软件表的payloadsize
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwPldSizeInfoUpdate(BYTE chipId,WORD32 pwId,BYTE frameCount,WORD16 payloadSize)
{
    WORD32 ret = DRV_AT_SUCCESS;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_PW_ID(pwId);
    
    g_atE1PwInfo[chipId][pwId].frameNum = frameCount;
    g_atE1PwInfo[chipId][pwId].payloadSize= payloadSize;
    
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtPwJitBuffInfoUpdate
* 功能描述:   更新驱动软件表的jitterbuffersize
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwJitBuffInfoUpdate(BYTE chipId,WORD32 pwId,WORD32 jitbufSize)
{
    WORD32 ret = DRV_AT_SUCCESS;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_PW_ID(pwId);
    
    g_atE1PwInfo[chipId][pwId].jitterBufferSize = jitbufSize;
 
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtPwEthLayerInfoUpdate
* 功能描述:   更新驱动软件表的EthLayer
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwEthLayerInfoUpdate(BYTE chipId,WORD32 pwId, BYTE* dMac,tAtEthVlanTag* vlanTag1,tAtEthVlanTag* vlanTag2)
{
    WORD32 ret = DRV_AT_SUCCESS;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_PW_ID(pwId);
    CHECK_AT_POINTER_NULL(dMac);
    CHECK_AT_POINTER_NULL(vlanTag1);
    CHECK_AT_POINTER_NULL(vlanTag2);

    memset(g_atE1PwInfo[chipId][pwId].destMac, 0, sizeof(g_atE1PwInfo[chipId][pwId].destMac));
    memcpy(g_atE1PwInfo[chipId][pwId].destMac,dMac,sizeof(g_atE1PwInfo[chipId][pwId].destMac));
    
    g_atE1PwInfo[chipId][pwId].vlan1Pri = vlanTag1->priority;
    g_atE1PwInfo[chipId][pwId].vlan1Cfi = vlanTag1->cfi;
    g_atE1PwInfo[chipId][pwId].vlan2Pri = vlanTag2->priority;
    g_atE1PwInfo[chipId][pwId].vlan2Cfi = vlanTag2->cfi;
    g_atE1PwInfo[chipId][pwId].vlan1Info.zteVlan1Id = vlanTag1->vlanId;
    g_atE1PwInfo[chipId][pwId].vlan1Info.pktLen = (BYTE)((vlanTag1->vlanId) & VLAN_TAG1_BIT0_5);
    g_atE1PwInfo[chipId][pwId].vlan1Info.encapType = (BYTE)(((vlanTag1->vlanId) & VLAN_TAG1_BIT6_10)>>6);
    g_atE1PwInfo[chipId][pwId].vlan1Info.cpu = (BYTE)(((vlanTag1->vlanId) & VLAN_TAG1_BIT11)>>11);
    g_atE1PwInfo[chipId][pwId].vlan2Info.zteVlan2Id = vlanTag2->vlanId;
    g_atE1PwInfo[chipId][pwId].vlan2Info.chanNo = (WORD16)((vlanTag2->vlanId) & VLAN_TAG2_BIT0_7);
    g_atE1PwInfo[chipId][pwId].vlan2Info.isMlppp = (BYTE)(((vlanTag2->vlanId) & VLAN_TAG2_BIT8)>>8);
    g_atE1PwInfo[chipId][pwId].vlan2Info.portNo = (BYTE)(((vlanTag2->vlanId) & VLAN_TAG2_BIT9_11)>>9);
    
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtPwPldSizeModify
* 功能描述:   PW payload参数更新 
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwPldSizeModify(BYTE chipId,WORD32 pwId,WORD16 newPldSize)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 atRet = 0;
    AtPw atPwObj = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_PW_ID(pwId);
    
    ret = DrvAtPwStatusCheck(chipId,pwId);/*check pw status*/
    if(DRV_AT_SUCCESS != ret)
    {   
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, THIS PW IS NOT EXISTED ! !! \n", __FILE__, __LINE__);  
        return ret; 
    }
    ret = DrvAtPwObjGet(chipId,pwId,&atPwObj);/*get pw obj*/
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PW OBJECT GET FAILED ! !! \n", __FILE__, __LINE__); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atPwObj);
    atRet = AtPwPayloadSizeSet (atPwObj,newPldSize);  /*set new payloadsize*/                         
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtPwPayloadSizeSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_PW_PAYLOAD_SET_FAILED;
    }       
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtPwjitBufSizeModify
* 功能描述:   PW jiterbuffer 更新
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwjitBufSizeModify(BYTE chipId,WORD32 pwId,WORD32 newjitBufSize)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 atRet = 0;
    WORD32 oldJitBufSize = 0;
    AtPw atPwObj = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_PW_ID(pwId);
    
    ret = DrvAtPwStatusCheck(chipId,pwId);/*check pw status*/
    if(DRV_AT_SUCCESS != ret)
    {   
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, THIS PW IS NOT EXISTED ! !! \n", __FILE__, __LINE__);  
        return ret; 
    }
    ret = DrvAtPwObjGet(chipId,pwId,&atPwObj);/*get pw obj*/
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PW OBJECT  GET  FAILED ! !! \n", __FILE__, __LINE__); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atPwObj);
    
    oldJitBufSize = AtPwJitterBufferSizeGet(atPwObj);/*get old jitterbuffersize*/
    if(newjitBufSize >= oldJitBufSize)
    {
        atRet = AtPwJitterBufferSizeSet (atPwObj,newjitBufSize); /*jitterBuffSize*/
        if (cAtOk != atRet)
        {
           DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtPwJitterBufferSizeSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
           return DRV_AT_PW_JITBUFF_SIZE_SET_FAILED;
        }    

        atRet = AtPwJitterBufferDelaySet(atPwObj,newjitBufSize/2); /*jitterBuffDelay*/
        if (cAtOk != atRet)
        {
           DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtPwJitterBufferDelaySet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
           return DRV_AT_PW_JITBUFF_DELAY_SET_FAILED;
        }    
    }
    else
    {
        atRet = AtPwJitterBufferDelaySet(atPwObj,newjitBufSize/2); /*jitterBuffDelay*/
        if (cAtOk != atRet)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtPwJitterBufferDelaySet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
            return DRV_AT_PW_JITBUFF_DELAY_SET_FAILED;
        }
        atRet = AtPwJitterBufferSizeSet (atPwObj,newjitBufSize); /*jitterBuffSize*/
        if (cAtOk != atRet)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtPwJitterBufferSizeSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
            return DRV_AT_PW_JITBUFF_SIZE_SET_FAILED;
        }    
    }
        
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtPwEthLayerModify
* 功能描述:   PW 动态修改以太头,仅供调试使用
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwEthLayerModify(BYTE chipId,WORD32 pwId,char* dMac,BYTE pri1,BYTE cfi1,WORD16 vlanId1,BYTE pri2,BYTE cfi2,WORD16 vlanId2)
{
    WORD32 ret = DRV_AT_SUCCESS;
    eAtRet atRet = cAtOk ;
    AtPw atPwObj = NULL;
    WORD32 i = 0;
    BYTE destMac[6] = {0};
    char macTemp[3] = {0};
    char *endPos = "\0";
    tAtEthVlanTag vlanTag1;
    tAtEthVlanTag vlanTag2;
    
    memset(&vlanTag1,0,sizeof(tAtEthVlanTag));
    memset(&vlanTag2,0,sizeof(tAtEthVlanTag));

    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_PW_ID(pwId);
    
    ret = DrvAtPwStatusCheck(chipId,pwId);/*check pw status*/
    if(DRV_AT_SUCCESS != ret)
    {   
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, THIS PW IS NOT EXISTED ! !! \n", __FILE__, __LINE__);  
        return ret; 
    }
    ret = DrvAtPwObjGet(chipId,pwId,&atPwObj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PW OBJECT  GET  FAILED ! !! \n", __FILE__, __LINE__); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atPwObj);

    /*将字符串转化为BYTE*/
    for(i = 0; i < 6; i++)
    {
        memcpy(macTemp,dMac+5+i*3, 2);  /*去掉前面的5个字符"dmac:"*/
        destMac[i] = (BYTE)strtol(macTemp,&endPos,16);
        //printf("0x%x\n", destMac[i]);  
    }
    /*生成两层VLAN*/
    vlanTag1.priority = pri1;
    vlanTag1.cfi = cfi1;
    vlanTag1.vlanId = vlanId1;
    vlanTag2.priority = pri2;
    vlanTag2.cfi = cfi2;
    vlanTag2.vlanId = vlanId2;
    
    atRet = AtPwEthHeaderSet(atPwObj,destMac,&vlanTag2,&vlanTag1);/*设置vlan tag */
    if (cAtOk != atRet)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtPwEthHeaderSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_PW_ETH_VLAN_SET_FAILED;
    }    
    atRet = AtPwEthExpectedSVlanSet(atPwObj,&vlanTag1);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtPwEthExpectedSVlanSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_PW_ETH_VLAN_SET_FAILED;
    }    
    atRet = AtPwEthExpectedCVlanSet(atPwObj,&vlanTag2);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtPwEthExpectedCVlanSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_PW_ETH_VLAN_SET_FAILED;
    }    
    DrvAtPwEthLayerInfoUpdate(chipId,pwId,destMac,&vlanTag1,&vlanTag2);
    return ret ;

}

/*******************************************************************************
* 函数名称:   DrvAtPwPldSizeCheck
* 功能描述:   检查payloadSize大小是否合适
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwPldSizeCheck(WORD32 pldSize)
{
    WORD32 ret = DRV_AT_SUCCESS;

    if((pldSize < MIN_PAYLOAD_SIZE)||(pldSize > MAX_PAYLOAD_SIZE))
    {
       DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"PayloadSize %u not support!!\n",pldSize);
       ret = DRV_AT_PW_PAYLOAD_NOT_SUPPORT;
    }
      
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtPwJitBufSizeCheck
* 功能描述:   检查jitbuffer大小是否合适
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwJitBufSizeCheck(BYTE servType,WORD32 timeslotNum,WORD32 payloadSize,WORD32 JitBufSize)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 e1BytePerMs = 0;              /* bytes/ms */
    WORD32 ds0BytePerMs = 0;             /* bytes/ms */
    WORD32 pktNumPerUs = 0;              /* pkt num/us */
    WORD32 minJitBuffSize = 0;
    WORD32 maxJitBuffSize = 0;
    WORD32 recommendJitBufSize = 0;
       
    if (SATOP_MODE == servType)
    {
        e1BytePerMs = E1_SIZE_IN_BYTE * E1_FRAME_NUM_PER_MS; /*e1 speed = 256B/ms*/
        pktNumPerUs = payloadSize * 1000 / e1BytePerMs ;            /* in microsecond */
        minJitBuffSize = 2 * MIN_PKT_NUM_IN_BUF * pktNumPerUs;
        maxJitBuffSize = 2 * MAX_PKT_NUM_IN_BUF * pktNumPerUs; 
        recommendJitBufSize = 2 * 16 * pktNumPerUs;                    /*arrive recommended jitterBufferSize */
    }
    else if (CESOP_MODE == servType)
    {
        ds0BytePerMs = NXDS0_SIZE_IN_BYTE * NXDS0_FRAME_NUM_PER_MS; /*ts speed = 8B/ms*/
        pktNumPerUs = payloadSize * 1000 /(ds0BytePerMs * timeslotNum) ; /* in microsecond */
        minJitBuffSize = 2 * MIN_PKT_NUM_IN_BUF * pktNumPerUs;
        maxJitBuffSize = 2 * MAX_PKT_NUM_IN_BUF * pktNumPerUs;  
        recommendJitBufSize = 2 * 8 * pktNumPerUs;                                    /*arrive recommended jitterBufferSize */
    }
    if((JitBufSize > maxJitBuffSize)||(JitBufSize < minJitBuffSize))
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"JitterBufferSize %u not support!!\n",JitBufSize);
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"Recommend JitterBufferSize %u !!\n",recommendJitBufSize);
        ret = DRV_AT_PW_JITBUFF_NOT_SUPPORT;
    }
    
    return ret;
}


/*******************************************************************************
* 函数名称:   DrvAtPwRtpSet
* 功能描述:   PW RTP 配置
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwRtpSet(BYTE chipId,WORD32 pwId,BYTE isRtpEnable)
{
    WORD32 ret = DRV_AT_SUCCESS;
    eAtRet atRet = cAtOk;
    AtPw atPwObj =     NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_PW_ID(pwId);
    ret = DrvAtPwStatusCheck(chipId,pwId);/*check pw status*/
    if(DRV_AT_SUCCESS != ret)
    {   
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, THIS PW IS NOT EXISTED ! !! \n", __FILE__, __LINE__);  
        return ret; 
    }
    ret = DrvAtPwObjGet(chipId,pwId,&atPwObj);     /*get pw obj*/
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PW OBJECT  GET  FAILED ! !! \n", __FILE__, __LINE__); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atPwObj);
    atRet = AtPwRtpEnable(atPwObj,isRtpEnable) ;   /*rtp disable*/
    if (cAtOk != atRet)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtPwRtpEnable() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_PW_RTP_SET_FAILED;
    }    
    
    return ret ;
}


/*******************************************************************************
* 函数名称:   DrvAtPwControlwordSet
* 功能描述:   PW 控制字设置
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwControlwordSet(BYTE chipId,WORD32 pwId,BYTE type,BYTE isEnable)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    eAtRet atRet = cAtOk ;
    AtPw atPwObj = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_PW_ID(pwId);
    
    ret = DrvAtPwObjGet(chipId,pwId,&atPwObj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PW OBJECT  GET  FAILED ! !! \n", __FILE__, __LINE__); 
        return ret; 
    }
    switch (type)
    {
        case TX_LBIT:
            atRet = AtPwCwAutoTxLBitEnable(atPwObj,isEnable);
            break;
        case RX_LBIT:
            atRet = AtPwCwAutoRxLBitEnable(atPwObj,isEnable);
            break;
        case MBIT:
            atRet = AtPwCESoPCwAutoMBitEnable((AtPwCESoP)atPwObj,isEnable);
            break;
        case RBIT:
            atRet = AtPwCwAutoRBitEnable(atPwObj,isEnable);
            break;
        default :
            break;
    }
    if (cAtOk != atRet)
    {
       DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ControlWord set failed, ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
       return DRV_AT_PW_CW_SET_FAILED;
    }    
    
    return ret ;

}

/*******************************************************************************
* 函数名称:   DrvAtPwControlwordGet
* 功能描述:   PW 控制字状态获取
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwControlwordGet(BYTE chipId,WORD32 pwId,BYTE    type,BYTE*isEnable)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    *isEnable = 0 ;
    AtPw atPwObj = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_PW_ID(pwId);
    CHECK_AT_POINTER_NULL(isEnable);
    
    ret = DrvAtPwObjGet(chipId,pwId,&atPwObj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PW OBJECT  GET  FAILED ! !! \n", __FILE__, __LINE__); 
        return ret; 
    }
    switch (type)
    {
        case TX_LBIT:
            *isEnable = AtPwCwAutoTxLBitIsEnabled(atPwObj);
            break;
        case RX_LBIT:
            *isEnable = AtPwCwAutoRxLBitIsEnabled(atPwObj);
            break;
        case MBIT:
            *isEnable = AtPwCESoPCwAutoMBitIsEnabled((AtPwCESoP)atPwObj);
            break;
        case RBIT:
            *isEnable = AtPwCwAutoRBitIsEnabled(atPwObj);
            break;
        default :
            break;
    }
       
    return ret ;
}


/*******************************************************************************
* 函数名称:   DrvAtPwZteVlanTag1Make
* 功能描述:   vlantag1 设置
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwZteVlanTag1Make(BYTE priority,BYTE cfi,BYTE cpuPktIndicator,BYTE encapType,BYTE packetLength,tAtZtePtnTag1*zteTag1)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    CHECK_AT_POINTER_NULL(zteTag1);
    
    zteTag1->priority = priority;
    zteTag1->cfi = cfi;
    zteTag1->cpuPktIndicator = cpuPktIndicator;
    zteTag1->encapType = encapType;
    zteTag1->packetLength = packetLength;
    
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtPwZteVlanTag2Make
* 功能描述:   vlantag2 设置
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwZteVlanTag2Make(BYTE priority,BYTE cfi,BYTE portId,BYTE isMlpppIma,WORD16 zteChannelId,tAtZtePtnTag2*zteTag2)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    CHECK_AT_POINTER_NULL(zteTag2);
    
    zteTag2->priority= priority ;
    zteTag2->cfi = cfi;
    zteTag2->stmPortId = portId;
    zteTag2->isMlpppIma = isMlpppIma;
    zteTag2->zteChannelId = zteChannelId;
    
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtPwVlanTag1Get
* 功能描述:   vlantag1 获取
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwVlanTag1Get(BYTE chipId,WORD32 pwId,tAtZtePtnTag1*atvlanTag1)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    AtPw atPwObj = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_PW_ID(pwId);
    CHECK_AT_POINTER_NULL(atvlanTag1);
    
    ret = DrvAtPwObjGet(chipId,pwId,&atPwObj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PW OBJECT GET FAILED ! !! \n", __FILE__, __LINE__); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atPwObj);
    atvlanTag1 = AtZtePtnPwTag1Get(atPwObj,atvlanTag1);
    if(NULL == atvlanTag1)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PW VLANTAG 1 GET FAILED ! !!CHIP ID IS  %u, PW ID IS %u\n", __FILE__, __LINE__,chipId,pwId); 
        return DRV_AT_PW_ETH_VLAN_GET_FAILED; 
    }
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtPwVlanTag2Get
* 功能描述:   vlantag2 获取
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwVlanTag2Get(BYTE chipId,WORD32 pwId,tAtZtePtnTag2*atvlanTag2)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    AtPw atPwObj = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_PW_ID(pwId);
    CHECK_AT_POINTER_NULL(atvlanTag2);
    
    ret = DrvAtPwObjGet(chipId,pwId,&atPwObj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PW OBJECT GET FAILED ! !! \n", __FILE__, __LINE__); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atPwObj);
    atvlanTag2 = AtZtePtnPwTag2Get(atPwObj,atvlanTag2);
    if(NULL == atvlanTag2)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PW VLANTAG 2 GET FAILED ! !!CHIP ID IS  %u, PW ID IS %u\n", __FILE__, __LINE__,chipId,pwId); 
        return DRV_AT_PW_ETH_VLAN_GET_FAILED; 
    }
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtPwStaticCfg
* 功能描述:   pw 通用配置
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwStaticCfg(AtPw atPwObj)
{
    WORD32 ret = DRV_AT_SUCCESS;
    eAtRet atRet = cAtOk ;
    CHECK_AT_POINTER_NULL(atPwObj);
    
    atRet = AtPwReorderingEnable (atPwObj ,REORDER_ENABLE) ;                         /*reorder enable*/
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtPwReoderingEnable() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_PW_REORDER_SET_FAILED;
    }    
    atRet = AtPwSuppressEnable (atPwObj,SUPPRESS_DISABLE) ;                          /*suppress disable*/
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtPwSuppressEnable() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_PW_SUPPRESS_SET_FAILED;
    }    
    /*RTP header*/
    atRet = AtPwRtpEnable(atPwObj,RTP_DISABLE) ;                                             /*rtp disable*/
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtPwRtpEnable() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_PW_RTP_SET_FAILED;
    }    
    atRet = AtPwCwSequenceModeSet(atPwObj,cAtPwCwSequenceModeWrapZero);    
    if (cAtOk != atRet)
    {   
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtPwCwSequenceModeSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_PW_SEQ_MODE_SET_FAILED;
    }    
    atRet = AtPwCwLengthModeSet(atPwObj,cAtPwCwLengthModePayload) ;
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtPwCwLengthModeSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_PW_LEN_MODE_SET_FAILED;
    }    
    atRet = AtPwCwPktReplaceModeSet(atPwObj,cAtPwPktReplaceModeAis );
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtPwCwPktReplaceModeSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_PW_PKT_REP_MODE_SET_FAILED;
    }    
    return ret ;

}
 
/*******************************************************************************
* 函数名称:   DrvAtPwVariableCfg
* 功能描述:   PW 动态配置
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
 WORD32 DrvAtPwVariableCfg(AtPw atPwObj,DRV_AT_PW_PARA *atPwInfo)
 {
    WORD32 ret = DRV_AT_SUCCESS ;
    eAtRet atRet = cAtOk;
    BYTE chipId = 0 ;
    BYTE servType = 0;
    BYTE clkMode = 0;
    BYTE lopOccurThres = 0;
    BYTE lopClearThres = 0;
    WORD16 payloadSize = 0;    
    WORD32 jitterBuffSize = 0;
    WORD32 jitterBuffDelay = 0 ;
    WORD32 e1LinkNo = 0;
    WORD32 enc = 0 ;
    WORD32 timeslotNum = 0;
    
    CHECK_AT_POINTER_NULL(atPwObj); 
    CHECK_AT_POINTER_NULL(atPwInfo); 
    
    chipId = atPwInfo->chipId;
    e1LinkNo = atPwInfo->e1LinkNum;
    servType = atPwInfo->servMode ;
    clkMode = atPwInfo->clockMode;
    payloadSize = atPwInfo->payloadSize;
    timeslotNum = atPwInfo->tsNum;
    enc = atPwInfo->frameNum ;
    jitterBuffSize = atPwInfo->jitterBufferSize ;
    jitterBuffDelay = jitterBuffSize/2 ;
    lopOccurThres = 20;  /*4~31*/
    lopClearThres = 15; 
    
    atRet = AtPwLopsSetThresholdSet(atPwObj,lopOccurThres) ;                     /*lop occur threshold*/
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtPwLopsSetThresholdSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_PW_LOP_OCURR_THRES_SET_FAILED;
    }    
    atRet = AtPwLopsClearThresholdSet(atPwObj,lopClearThres) ;                   /*lop clear threshold*/
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtPwLopsClearThresholdSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_PW_LOP_CLEAR_THRES_SET_FAILED;
    }    
    ret = DrvAtPwPldSizeCheck(payloadSize);                                       /*check payloadsize if is support*/
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PAYLOADSIZE %u IS NOT SUITABLE FOR PAYLOAD %u!!\n", __FILE__, __LINE__,jitterBuffSize,payloadSize);
        return ret;
    }
    ret = DrvAtPwJitBufSizeCheck(servType,timeslotNum,payloadSize,jitterBuffSize);/*check jittbufsize if is suitable for payload*/
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, JITTERBUFSIZE %u IS NOT SUITABLE FOR PAYLOAD %u!!\n", __FILE__, __LINE__,jitterBuffSize,payloadSize);
        return ret;
    }     
    DRV_AT_E1_PRINT(DRV_AT_PW_CFG,"[CE1TAN][JITBUFF SET]: %s line %d, JITTERBUFSIZE IS %u \n", __FILE__, __LINE__,jitterBuffSize);
    atRet = AtPwJitterBufferSizeSet (atPwObj,jitterBuffSize);                     /*set jitterBuffSize*/
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtPwJitterBufferSizeSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_PW_JITBUFF_SIZE_SET_FAILED;
    }     
    atRet = AtPwJitterBufferDelaySet(atPwObj,jitterBuffDelay);                    /*set jitterBuffDelay*/
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtPwJitterBufferDelaySet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_PW_JITBUFF_DELAY_SET_FAILED;
    }
    if(SATOP_MODE == servType)
    {
        atRet = AtPwPayloadSizeSet (atPwObj,payloadSize);                                  /*set payloadsize*/
        if (cAtOk != atRet)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtPwPayloadSizeSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
            return DRV_AT_PW_PAYLOAD_SET_FAILED;
        }    
    }
    else if (CESOP_MODE == servType)
    {   
        atRet = AtPwPayloadSizeSet (atPwObj,enc);                                  /*in cesop mode ,just need to set the frameNum*/
        if (cAtOk != atRet)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtPwPayloadSizeSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
            return DRV_AT_PW_PAYLOAD_SET_FAILED;
        }    
    }    
    
#if 0
    atRet = AtPwPrioritySet(atPwObj,priority) ;                                                     /*pw priority*/
    if (cAtOk != atRet)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtPwPrioritySet() ret=0x%x.\n", __FILE__, __LINE__, atRet);
        return DRV_AT_PW_PRI_SET_FAILED;
    }    
#endif
    return ret ;
 }

/*******************************************************************************
* 函数名称:   DrvAtPwEthLayerSet
* 功能描述:   PW 以太层配置
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwEthLayerSet(AtPw atPwObj,DRV_AT_PW_PARA *atPwInfo)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    eAtRet atRet =cAtOk ;
    BYTE ethPortId = 0;
    BYTE chipId = 0;
    WORD32 vlan1Pri = 0;
    WORD32 vlan2Pri = 0;
    WORD32 vlan1cfi = 0;
    WORD32 vlan2cfi = 0;
    WORD32 vlan1Id = 0;
    WORD32 vlan2Id = 0;
    BYTE desMac[6] = {0};
    AtEthPort atEthPort = NULL ;
    tAtZtePtnTag1 zteTag1;
    tAtZtePtnTag2 zteTag2;
    memset(&zteTag1, 0, sizeof(zteTag1));
    memset(&zteTag2, 0, sizeof(zteTag2));
    
    CHECK_AT_POINTER_NULL(atPwObj);  
    CHECK_AT_POINTER_NULL(atPwInfo);
    
    chipId = atPwInfo->chipId ;    
    memcpy(&desMac,atPwInfo->destMac,6);
    vlan1Pri = atPwInfo->vlan1Pri;
    vlan2Pri = atPwInfo->vlan2Pri;
    vlan1cfi = atPwInfo->vlan1Cfi;
    vlan2cfi = atPwInfo->vlan2Cfi;
    vlan1Id = atPwInfo->vlan1Info.zteVlan1Id;
    vlan2Id = atPwInfo->vlan2Info.zteVlan2Id;
    
    
    ret = DrvAtEthPortObjGet(chipId,ethPortId,&atEthPort) ;
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ETH PORT GET FAILED  !!\n", __FILE__, __LINE__);
        return ret;
    }     
    atRet = AtPwEthPortSet(atPwObj,atEthPort); /* Use the first Ethernet port */
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtPwEthPortSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_PW_ETH_PORT_SET_FAILED;
    }
        
    /*get detail format ,store in "atPwInfo" table*/
    atPwInfo->vlan1Info.pktLen = (BYTE)(vlan1Id & VLAN_TAG1_BIT0_5);
    atPwInfo->vlan1Info.encapType = (BYTE)((vlan1Id & VLAN_TAG1_BIT6_10)>>6);
    atPwInfo->vlan1Info.cpu = (BYTE)((vlan1Id & VLAN_TAG1_BIT11)>>11);
             
    atPwInfo->vlan2Info.chanNo = (WORD16)(vlan2Id & VLAN_TAG2_BIT0_7);
    atPwInfo->vlan2Info.isMlppp = (BYTE)((vlan2Id & VLAN_TAG2_BIT8)>>8);
    atPwInfo->vlan2Info.portNo = (BYTE)((vlan2Id & VLAN_TAG2_BIT9_11)>>9);

    
    ret = DrvAtPwZteVlanTag1Make(vlan1Pri,vlan1cfi,atPwInfo->vlan1Info.cpu,atPwInfo->vlan1Info.encapType,atPwInfo->vlan1Info.pktLen,&zteTag1);
    ret |= DrvAtPwZteVlanTag2Make(vlan2Pri,vlan2cfi,atPwInfo->vlan2Info.portNo,atPwInfo->vlan2Info.isMlppp,atPwInfo->vlan2Info.chanNo,&zteTag2);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, VLAN TAG SET FAILED  ret = %u\n", __FILE__, __LINE__, ret);
        return ret;
    }     
    atRet = AtZtePwPtnHeaderSet(atPwObj,desMac,&zteTag1,&zteTag2);/*设置vlan tag */
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtZtePwPtnHeaderSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_PW_ETH_VLAN_SET_FAILED;
    }    
    atRet = AtZtePtnPwExpectedTag1Set(atPwObj,&zteTag1);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtZtePtnPwExpectedTag1Set() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_PW_ETH_VLAN_SET_FAILED;
    }    
    atRet = AtZtePtnPwExpectedTag2Set(atPwObj,&zteTag2);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtZtePtnPwExpectedTag2Set() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_PW_ETH_VLAN_SET_FAILED;
    }    
    return ret ;

}

/*******************************************************************************
* 函数名称:   DrvAtPwCircuitBind
* 功能描述:   PW 与circuit 绑定
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期     版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwCircuitBind(AtPw atPwObj,DRV_AT_PW_PARA *atPwInfo)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    eAtRet atRet =cAtOk ;
    BYTE servType = 0;
    BYTE chipId = 0;
    WORD32 e1LinkNo = 0;
    WORD32 tsBitMap = 0;
    AtPdhDe1 atE1Obj = NULL;
    AtPdhNxDS0 atNxds0Obj = NULL ;
    
    servType = atPwInfo->servMode ;
    chipId = atPwInfo->chipId ;
    e1LinkNo = atPwInfo->e1LinkNum ;
    tsBitMap = atPwInfo->timeSlotBitMap ;
    
    CHECK_AT_POINTER_NULL(atPwObj);  
    CHECK_AT_POINTER_NULL(atPwInfo);  
      
    if(CESOP_MODE == servType)
    {
        ret = DrvAtE1Nxds0ObjGet(chipId, e1LinkNo,tsBitMap, &atNxds0Obj);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, E1 CESOP OBJECT  GET  FAILED ! !! \n", __FILE__, __LINE__); 
            return ret; 
        }
        CHECK_AT_POINTER_NULL(atNxds0Obj);
        atRet = AtPwCircuitBind(atPwObj, (AtChannel)atNxds0Obj);
        if (cAtOk != atRet)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtPwCircuitBind() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
            return DRV_AT_PW_CIRCUIT_BIND_FAILED;
        }    
    }
    else if(SATOP_MODE == servType)
    {
        ret = DrvAtE1ObjGet( chipId, e1LinkNo,&atE1Obj) ;     
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, E1 OBJECT  GET  FAILED ! !! \n", __FILE__, __LINE__); 
            return ret; 
        }
        CHECK_AT_POINTER_NULL(atE1Obj);
        atRet = AtPwCircuitBind(atPwObj, (AtChannel)atE1Obj);
        if (cAtOk != atRet)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtPwCircuitBind() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
            return DRV_AT_PW_CIRCUIT_BIND_FAILED;
        }
    }   
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtPwActive
* 功能描述:   PW 激活
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwActive(AtPw atPwObj)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    eAtRet atRet = cAtOk ;
    CHECK_AT_POINTER_NULL(atPwObj);  
    
    atRet = AtChannelEnable((AtChannel)atPwObj, CHANNEL_ENABLE);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtChannelEnable() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_PW_ACTIVE_FAILED;
    }    
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtPwStausGet
* 功能描述:   查看Pw状态
* 输入参数:   
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-17     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwStausGet(BYTE chipId,WORD32 pwId,BYTE*isEnable )
{
    WORD32 ret = DRV_AT_SUCCESS ;
    AtPw atPwObj = NULL ;
    
    CHECK_AT_CHIP_ID(chipId);  
    CHECK_AT_PW_ID(pwId);
    CHECK_AT_POINTER_NULL(isEnable);
    
    /* Get object to configure */  
    ret = DrvAtPwObjGet(chipId,pwId,&atPwObj)   ;
    if(DRV_AT_SUCCESS != ret)
    {      
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, E1 OBJ GET FAILED !!\n", __FILE__, __LINE__); 
        return ret; 
    }  
    CHECK_AT_POINTER_NULL(atPwObj);
    *isEnable = AtChannelIsEnabled((AtChannel)atPwObj);
    
    return ret ;

}
/*******************************************************************************
* 函数名称:   DrvAtPwAllCfg
* 功能描述:   PW 属性配置总函数
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwAllCfg(AtPw atPwObj,DRV_AT_PW_PARA *atPwInfo)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    
    CHECK_AT_POINTER_NULL(atPwObj);  
    CHECK_AT_POINTER_NULL(atPwInfo);  

    /* Bind Pw With Circuit */
    ret = DrvAtPwCircuitBind(atPwObj,atPwInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, DrvAtPwCircuitBind() ret=0x%x.\n", __FILE__, __LINE__, ret);
        return ret;
    }     
    /*common cfg */
    ret = DrvAtPwStaticCfg(atPwObj);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, DrvAtPwStaticCfg() ret=0x%x.\n", __FILE__, __LINE__, ret);
        return ret;
    }    
    /*variable cfg (payloadsize, jitterbuffer...)*/
    ret = DrvAtPwVariableCfg(atPwObj,atPwInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, DrvAtPwVariableCfg() ret=0x%x.\n", __FILE__, __LINE__, ret);
        return ret;
    }    
    /* Configure Ethernet layer */
    ret = DrvAtPwEthLayerSet(atPwObj,atPwInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, DrvAtPwEthLayerSet() ret=0x%x.\n", __FILE__, __LINE__, ret);
        return ret;
    }    

    /* Activate this Pseudowire */
    ret = DrvAtPwActive(atPwObj);
    if (DRV_AT_SUCCESS != ret)
    {
       DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, DrvAtPwActive() ret=0x%x.\n", __FILE__, __LINE__, ret);
       return ret;
    }    
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtPwAdd
* 功能描述:   添加一条pw
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwAdd(DRV_AT_PW_PARA *atPwInfo)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    eAtRet atRet = cAtOk ;
    BYTE chipId = 0;
    BYTE servType = 0 ;
    BYTE clkMode = 0;
    BYTE atClkMode = 0;
    BYTE clkState = 0;
    WORD32 pwId = 0;
    WORD32 e1LinkNo = 0;
    WORD32 masterE1LinkNo = 0;/*时钟域中的主时钟*/
    WORD32 cesopLinks = 0;
    WORD32 tsBitMap = 0;
    WORD32 clkDomainId = 0;
    AtDevice atDevHdl = NULL;
    AtModulePw atPwModule = NULL ;
    AtPw atPwObj = NULL ;
    AtPdhNxDS0 atNxds0Obj = NULL ;

    CHECK_AT_POINTER_NULL(atPwInfo);   

    chipId = atPwInfo->chipId ;
    e1LinkNo = atPwInfo->e1LinkNum ;
    servType = atPwInfo->servMode ;
    clkMode = atPwInfo->clockMode ;
    clkState = atPwInfo->clkState ;
    clkDomainId = atPwInfo->clkDomainId;
    tsBitMap = atPwInfo->timeSlotBitMap ;
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    
    /***1 . 检查参数***/
    
    pwId = DrvAtPwIdGetFromCip(chipId,atPwInfo->cip);                    /*check if cip is existed*/
    if(INVALID_PW_ID != pwId)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, The Cip is existed !!CHIP ID IS % u CIP IS %u\n", __FILE__, __LINE__,chipId, atPwInfo->cip);
        return DRV_AT_PW_ALREADY_EXISTED;
    }
    pwId = 0;
    ret = DrvAtPwCircuitBindCheck(chipId,e1LinkNo,servType,tsBitMap); /*check if ciucuit is already binded with an existed pw*/
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, THIS CIRCUIT ALREADY BIND TO ONE PW!!CHIP ID IS %u \n", __FILE__, __LINE__,chipId);
        return ret;
    }
    ret = DrvAtIdlePwIdGet(chipId,&pwId);    /*get a idle pw id*/
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, CAN NOT GET A IDLE PW ID !!CHIP ID IS % u \n", __FILE__, __LINE__,chipId);
        return ret;
    }
    DRV_AT_E1_PRINT(DRV_AT_PW_CFG,"[CE1TAN][PW ADD]:%s line %d , cip is %u,pwId is %u\n",__FILE__,__LINE__,atPwInfo->cip,pwId);
    CHECK_AT_PW_ID(pwId);
    atPwInfo->pwId = pwId ;                       /*store pw id*/
    atDevHdl = DrvAtDevHdlGet(chipId) ;      /*get the devhdl*/
    CHECK_AT_POINTER_NULL(atDevHdl);  
    
    /*****2 .Create An Pw*****/    
    
    atPwModule = (AtModulePw)AtDeviceModuleGet(atDevHdl, cAtModulePw);      /*get pw module */
    if(NULL == atPwModule)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PW MODULE GET  FAILED ! !!CHIP ID IS  %u \n", __FILE__, __LINE__,chipId); 
        return DRV_AT_PW_MODULE_GET_FAILED; 
    }
    if(SATOP_MODE == servType)                           
    {
        atPwObj = (AtPw)AtModulePwSAToPCreate(atPwModule, (WORD16)pwId);   /*create satop pw*/
        if(NULL == atPwObj)
        {   /*如果之前的没删掉是添加不下去的，这个时候先删掉原来的，再添加*/
            ret = DrvAtPwObjGet(chipId,pwId,&atPwObj);                                /*get pw obj*/
            if(DRV_AT_SUCCESS != ret)
            {
                DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PW OBJECT GET  FAILED ! !!CHIP ID IS %u,PW ID IS %u \n", __FILE__, __LINE__,chipId,pwId); 
                return ret; 
            }
            if(NULL != atPwObj)
            {
                atRet = AtPwCircuitUnbind(atPwObj);
                if (cAtOk != atRet)
                {
                    DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtPwCircuitUnbind() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
                    return DRV_AT_PW_CIRCUIT_UNBIND_FAILED ;
                }    
                atRet = AtModulePwDeletePw(atPwModule,(WORD16)pwId);
                if (cAtOk != atRet)
                {
                    DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtModulePwDeletePw() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
                    return DRV_AT_PW_DELETE_FAILED ;
                }
                atPwObj = (AtPw)AtModulePwSAToPCreate(atPwModule, (WORD16)pwId);   /*create satop pw*/
                if(NULL == atPwObj)
                {
                    DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, SATOP PW CREATE FAILED ! !!CHIP ID IS %u,E1 LinkNo is %u , PW ID IS %u \n", __FILE__, __LINE__, chipId,e1LinkNo,pwId); 
                    return DRV_AT_PW_SATOP_ADD_FAILED; 
                }
            }
        }
    }
    else if(CESOP_MODE == servType)
    {      
        ret = DrvAtE1Nxds0ObjCreate(chipId,e1LinkNo,tsBitMap,&atNxds0Obj);   /*create a cesop e1 channel obj based on e1 obj*/
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, CESOP E1 LINK CREATE FAILED ! !!E1 LINKNO IS %u ,TS BITMAP IS 0x%x \n", __FILE__, __LINE__,e1LinkNo,tsBitMap); 
            return ret; 
        }
        atPwObj = (AtPw)AtModulePwCESoPCreate(atPwModule, (WORD16)pwId,cAtPwCESoPModeBasic);   /*create cesop pw*/
        if(NULL == atPwObj)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, CESOP PW CREATE FAILED ! !!CHIP ID IS %u,E1 LinkNo is %u , PW ID IS %u \n", __FILE__, __LINE__, chipId,e1LinkNo,pwId); 
            return DRV_AT_PW_CESOP_ADD_FAILED; 
        }
    }

    /***** 3 .Set PW Para *****/
    
    ret = DrvAtPwAllCfg(atPwObj,atPwInfo);
    if (DRV_AT_SUCCESS != ret)                                            /*if cfg failed ,need to unbind pw object and delete it!!*/
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PW CONFIGURE FAILED!!!CHIP ID IS %u,E1LinkNo is %u , PW ID IS %u \n", __FILE__, __LINE__, chipId,e1LinkNo,pwId);
        atRet = AtPwCircuitUnbind(atPwObj);
        if (cAtOk != atRet)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtPwCircuitUnbind() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
            return DRV_AT_PW_CIRCUIT_UNBIND_FAILED ;
        }
        if(CESOP_MODE == servType)
        {
            ret = DrvAtE1Nxds0ObjDelete(chipId,e1LinkNo,tsBitMap);
            if(DRV_AT_SUCCESS != ret)
            {
                DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, CESOP OBJ DELETE  FAILED ! !! CHIP ID IS  %u,E1 LINKNO IS %u\n", __FILE__, __LINE__,chipId , e1LinkNo); 
                return ret; 
            } 
        }
        atRet = AtModulePwDeletePw(atPwModule,(WORD16)pwId);      
        if (cAtOk!= atRet)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtModulePwDeletePw() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
            return DRV_AT_PW_DELETE_FAILED ;
        }    
        return ret;
    }    
    
    /***4 .将产品管理的时钟模式转化为写入底层的时钟模式atclkMode,写入芯片***/
    
    ret = DrvAtCesopLinksNumGet(chipId,e1LinkNo,&cesopLinks);   /*get the num of cesop links in e1*/        
    if(CESOP_MODE == servType)
    {   
        if(0 == cesopLinks)    /*如果是cesopE1的第一条pw，设置它为时钟源*/
        {  
            if((ACR_CLK_MODE == clkMode)||(DCR_CLK_MODE == clkMode))
            {
                if( MASTER_STATUS == clkState )
                {
                    atClkMode =  clkMode;
                    atPwInfo->isClkSrc = CE1_TURE;                 /*标注此条pw是e1的时钟恢复源*/
                }
                else if(SLAVE_STATUS == clkState)
                {
                    atClkMode = SLAVE_CLK_MODE;
                    ret = DrvAtClkDomainMasterGet(chipId,clkDomainId,clkMode,&masterE1LinkNo);     /*get the master e1 of clkdomain*/
                    if(DRV_AT_SUCCESS != ret)
                    {
                        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, SOURCE E1lINKNO GET  FAILED ! !! CHIP ID IS %u ,CLK DOMIAN ID IS %u \n", __FILE__, __LINE__,chipId,clkDomainId); 
                        return ret; 
                    }
                }
                else
                {
                    DRV_AT_E1_ERROR_LOG("ERROR: %s line %d,INVALID CLKSTATE %u",__FILE__, __LINE__,clkState);
                    return DRV_AT_INVALID_OPTION;
                }
            }
            else if(SYS_CLK_MODE == clkMode)
            {
                atClkMode = SYS_CLK_MODE;
            }
            ret = DrvAtE1ClkModeSet(chipId,e1LinkNo,atClkMode,masterE1LinkNo,pwId);    /*set e1 clkmode*/
            if(DRV_AT_SUCCESS != ret)
            {
                DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, E1 CLK MODE  SET  FAILED ! !! CHIP ID IS %u,E1 LINKNO IS %u ,ATCLKMODE IS %u\n", __FILE__, __LINE__,chipId,e1LinkNo,atClkMode); 
                return ret; 
            }      
        }
        g_atCesopLinksNum[chipId][e1LinkNo]++ ;  
    }
    else if (SATOP_MODE == servType)
    {
        if((ACR_CLK_MODE == clkMode)||(DCR_CLK_MODE == clkMode))
        {
            if( MASTER_STATUS == clkState )
            {
                atClkMode =  clkMode;
                atPwInfo->isClkSrc = CE1_TURE;                 /*标注此条pw是e1的时钟恢复源*/
            }
            else if(SLAVE_STATUS == clkState)
            {
                atClkMode = SLAVE_CLK_MODE;
                ret = DrvAtClkDomainMasterGet(chipId,clkDomainId,clkMode,&masterE1LinkNo);     /*get the master e1 of clkdomain*/
                if(DRV_AT_SUCCESS != ret)
                {
                    DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, SOURCE E1lINKNO GET  FAILED ! !! CHIP ID IS %u ,CLK DOMIAN ID IS %u \n", __FILE__, __LINE__,chipId,clkDomainId); 
                    return ret; 
                }
            }
            else
            {
                DRV_AT_E1_ERROR_LOG("ERROR: %s line %d,INVALID CLKSTATE %u",__FILE__, __LINE__,clkState);
                return DRV_AT_INVALID_OPTION;
            }
        }
        else if(SYS_CLK_MODE == clkMode)
        {
            atClkMode = SYS_CLK_MODE;
        }
        ret = DrvAtE1ClkModeSet(chipId,e1LinkNo,atClkMode,masterE1LinkNo,pwId);    /*set e1 clkmode*/
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, E1 CLK MODE  SET  FAILED ! !! CHIP ID IS %u,E1 LINKNO IS %u ,ATCLKMODE IS %u\n", __FILE__, __LINE__,chipId,e1LinkNo,atClkMode); 
            return ret; 
        }      
    }
    
    /****5 .注册时钟域信息****/
    
    if((ACR_CLK_MODE == clkMode) ||(DCR_CLK_MODE == clkMode))    /*ACR or DCR*/
    {
        if( MASTER_STATUS == clkState )
        {
            masterE1LinkNo = e1LinkNo;   /*当前e1就是时钟域的主链路*/
            if((SATOP_MODE == servType)||((CESOP_MODE == servType)&&(1 == cesopLinks)))      /*cesop只注册一次*/
            {
                ret = DrvAtClkDomainInfoCreate(chipId,clkDomainId,clkMode,masterE1LinkNo);      /*创建时钟域信息*/
                if(DRV_AT_SUCCESS != ret)
                {
                    DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, CLK DOMAIN INFO SET FAILED  ! !! CHIP ID IS %u ,CLK DOMIAN ID IS %u \n", __FILE__, __LINE__,chipId,clkDomainId); 
                    return ret; 
                }
            }
        }
        else if(SLAVE_STATUS == clkState)
        {
            ret = DrvAtClkDomainMasterGet(chipId,clkDomainId,clkMode,&masterE1LinkNo);     /*get the master e1 of clkdomain*/
            if(DRV_AT_SUCCESS != ret)
            {
                DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, SOURCE E1lINKNO GET  FAILED ! !! CHIP ID IS %u ,CLK DOMIAN ID IS %u \n", __FILE__, __LINE__,chipId,clkDomainId); 
                return ret; 
            }
            if((SATOP_MODE == servType)||((CESOP_MODE == servType)&&(1 == cesopLinks)))      /*cesop只注册一次*/
            {
                ret = DrvAtClkDomainMemberInfoCreate(chipId,clkDomainId,clkMode,e1LinkNo);      /*新增时钟域成员信息*/
                if(DRV_AT_SUCCESS != ret)
                {
                    DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, CLK DOMAIN MEMBER INFO SET FAILED  ! !! CHIP ID IS %u ,CLK DOMIAN ID IS %u \n", __FILE__, __LINE__,chipId,clkDomainId); 
                    return ret; 
                }
            }
        }
    }
    else     /*SYS MODE*/
    {
        masterE1LinkNo = INVALID_E1LINK_NO;  /*如果是系统时钟，则恢复源不是E1链路，设为无效值*/
    }
    
    /*****Save The Result *****/
    g_atE1PwTotalNum[chipId] ++ ;             /*子卡上pw统计加1*/
    atPwInfo->pwStatus = PW_USED ;          /*pw 添加成功后，即可将其使能*/
    atPwInfo->masterE1LinkNo = masterE1LinkNo;    /*记录时钟域的主时钟链路*/
    ret = DrvAtSavePwCfg(chipId,pwId,atPwInfo);  /*保存驱动表项*/
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, DrvAtSavePwCfg() ret=0x%x.\n", __FILE__, __LINE__, ret);
        return ret;
    }
    return ret ;
}


/*******************************************************************************
* 函数名称:   DrvAtPwDelete
* 功能描述:   删除一条pw
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwDelete(DRV_AT_PW_PARA *atPwInfo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    eAtRet  atRet = cAtOk ;
    BYTE chipId = 0;
    BYTE servType = 0;
    BYTE clkMode = 0;
    BYTE clkstate = 0;
    BYTE isClkSrc = 0;
    WORD32 pwId = 0 ;
    WORD32 cip = 0 ;
    WORD32 e1LinkNo = 0 ;
    WORD32 tsBitmap = 0;
    WORD32 clkdomainId = 0;
    WORD32 slaveE1LinkNo = 0;
    AtDevice atDevHdl = NULL;
    AtModulePw atPwModule = NULL ;
    AtPw atPwObj = NULL;
    
    CHECK_AT_POINTER_NULL(atPwInfo) ;
    
    chipId = atPwInfo->chipId ;
    pwId = atPwInfo->pwId;
    cip = atPwInfo->cip ;
    e1LinkNo = atPwInfo->e1LinkNum ;
    servType = atPwInfo->servMode ;
    clkMode = atPwInfo->clockMode ;
    clkstate = atPwInfo->clkState;
    isClkSrc = atPwInfo->isClkSrc;
    clkdomainId = atPwInfo->clkDomainId;
    tsBitmap = atPwInfo->timeSlotBitMap;
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    
    ret = DrvAtPwStatusCheck(chipId,pwId);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, THIS PW IS NOT EXISTED ! !! \n", __FILE__, __LINE__);  
        return ret; 
    }    
    /*判断这条pw对应的E1是否是时钟域的主，以及是否时钟域内是否还有从成员
    如果时钟域内有从成员，必须先切换主时钟，不能直接删除*/
    if((MASTER_STATUS == clkstate)&&((ACR_CLK_MODE == clkMode)||(DCR_CLK_MODE == clkMode)))
    {
        ret = DrvAtClkDomainSlaveMemberGet(chipId,clkdomainId,clkMode,e1LinkNo,&slaveE1LinkNo);
        if(DRV_AT_SUCCESS == ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, CLK DOMAIN[%u] HAVE SLAVE E1,NEED TO SWITCH FIRST ! !! \n", __FILE__, __LINE__,clkdomainId);  
            return DRV_AT_CLK_DOMAIN_MASTER_CAN_NOT_DEL; 
        }    
    }
    /*get pw obj*/
    atDevHdl = DrvAtDevHdlGet(chipId) ;                      
    CHECK_AT_POINTER_NULL(atDevHdl);  
    atPwModule = (AtModulePw)AtDeviceModuleGet(atDevHdl, cAtModulePw); 
    if(NULL == atPwModule)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PW MODULE GET  FAILED ! !!CHIP ID IS  %u \n", __FILE__, __LINE__,chipId); 
        return DRV_AT_PW_MODULE_GET_FAILED; 
    }
    ret = DrvAtPwObjGet(chipId,pwId,&atPwObj);
    if(DRV_AT_SUCCESS != ret)
    {
       DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PW OBJECT GET FAILED ! !! CHIP ID IS %u,PW ID IS %u \n", __FILE__, __LINE__,chipId,pwId); 
       return ret; 
    }
    CHECK_AT_POINTER_NULL(atPwObj);
    /*Unbind Circiut*/
    atRet = AtPwCircuitUnbind(atPwObj);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtPwCircuitUnbind() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_PW_CIRCUIT_UNBIND_FAILED ;
    }    
    /*if cesop mode , need to delete ndxs0 obj */
    if(CESOP_MODE == servType)
    {
        ret = DrvAtE1Nxds0ObjDelete(chipId,e1LinkNo,tsBitmap);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, CESOP OBJ DELETE  FAILED ! !! CHIP ID IS  %u,E1 LINKNO IS %u\n", __FILE__, __LINE__,chipId , e1LinkNo); 
            return ret; 
        } 
    }
    /*Delete The pw Object ,if the pw is timingsrc,arrive will switch e1 to sys inside fpga ,then we switch to another pw*/
    atRet = AtModulePwDeletePw(atPwModule,(WORD16)pwId);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtModulePwDeletePw() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_PW_DELETE_FAILED ;
    }    
    /*decrease cesoplink counter*/
    if(CESOP_MODE == servType)
    {
        g_atCesopLinksNum[chipId][e1LinkNo]--;
    }        
    /*if the pw is clk source in cesop ,need to switch the clksrc */
    if(((ACR_CLK_MODE == clkMode)||(DCR_CLK_MODE == clkMode))&&(CESOP_MODE == servType))
    {
        if((MASTER_STATUS == clkstate)&&(CE1_TURE == isClkSrc))   /*如果该pw是cesop的时钟恢复源*/
        {
            ret = DrvAtPwClkSrcSwitch(chipId,e1LinkNo,clkMode);
            if(DRV_AT_SUCCESS != ret)
            {
                DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PW CLK SWITCH FAILED ! !! CHIP ID IS %u, E1 LINKNO IS %u\n", __FILE__, __LINE__,chipId,e1LinkNo); 
                return ret; 
            } 
        }
    }

    /*清空时钟域成员信息，如果时钟域内已无成员，清空时钟域信息*/
    if((ACR_CLK_MODE == clkMode)||(DCR_CLK_MODE == clkMode))
    {
        if(MASTER_STATUS == clkstate)
        {   
            if(((CESOP_MODE == servType)&&(0 == g_atCesopLinksNum[chipId][e1LinkNo]))||(SATOP_MODE == servType))
            {       
                ret = DrvAtE1ClkModeSet(chipId,e1LinkNo,SYS_CLK_MODE,0,0);/*如果将e1下的pw全部删掉后需将e1恢复成默认的系统时钟*/
                if(DRV_AT_SUCCESS != ret)
                {
                    DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, E1[%u] CLK MODE  SET  FAILED ! !!,CHIP ID IS %u \n", __FILE__, __LINE__,e1LinkNo,chipId); 
                    return ret; 
                }
                ret = DrvAtClkDomainMemberInfoDelete(chipId,clkdomainId,clkMode,e1LinkNo);
                if(DRV_AT_SUCCESS != ret)
                {
                    DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, CLK DOMAIN[%u] DELETE MEMBER E1[%u] INFO FAILED ! !! \n", __FILE__, __LINE__,clkdomainId,e1LinkNo);  
                    return ret; 
                }    
                ret = DrvAtClkDomainInfoDelete(chipId,clkdomainId,clkMode);
                if(DRV_AT_SUCCESS != ret)
                {
                    DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, CLK DOMAIN[%u] DELETE  INFO FAILED ! !! \n", __FILE__, __LINE__,clkdomainId);  
                    return ret; 
                }            
            }        
        }      
        else if(SLAVE_STATUS == clkstate)
        {        
            if(((CESOP_MODE == servType)&&(0 == g_atCesopLinksNum[chipId][e1LinkNo]))||(SATOP_MODE == servType))
            {
                ret = DrvAtE1ClkModeSet(chipId,e1LinkNo,SYS_CLK_MODE,0,0);/*如果将e1下的pw全部删掉后需将e1恢复成默认的系统时钟*/
                if(DRV_AT_SUCCESS != ret)
                {
                    DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, E1[%u] CLK MODE  SET  FAILED ! !!CHIP ID IS %u \n", __FILE__, __LINE__,e1LinkNo,chipId); 
                    return ret; 
                }
                ret = DrvAtClkDomainMemberInfoDelete(chipId,clkdomainId,clkMode,e1LinkNo);
                if(DRV_AT_SUCCESS != ret)
                {
                    DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, CLK DOMAIN[%u] DELETE MEMBER E1[%u] INFO FAILED ! !! \n", __FILE__, __LINE__,clkdomainId,e1LinkNo);  
                    return ret; 
                }
            }
        }   
    }
    /*release pwid  resource*/
    g_atE1PwTotalNum[chipId] -- ;  /*子卡上pw数量减1*/
    DrvAtPwInfoRelease(chipId,pwId);  
    return ret ;
}


/**************************************************************************
* 函数名称: DrvAtPwCountMemGet
* 功能描述: 获取保存pw统计的内存
* 访问的表: 
* 修改的表: 无
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
 
**************************************************************************/
WORD32 DrvAtPwCountMemGet(BYTE chipId, WORD32 pwId,DRV_AT_PW_COUNT*pwCount,BYTE countMode)
{
    WORD32 ret = DRV_AT_SUCCESS;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_PW_ID(pwId);
    CHECK_AT_POINTER_NULL(pwCount);  

    if(MONITOR_REAL_TIME == countMode)               /*实时统计*/
    {
        memcpy(pwCount, &(g_atE1PwCount[chipId][pwId]), sizeof(DRV_AT_PW_COUNT));
    }
    else if(MONITOR_PERFORMANCE == countMode)   /*累计统计*/
    {
        memcpy(pwCount, &(g_atE1PwCountSum[chipId][pwId]), sizeof(DRV_AT_PW_COUNT));
    }
    else if(CES_STATIC_SEND_TO_PM == countMode) /*上报给产品管理*/
    {
        memcpy(pwCount, &(g_atE1PwCountToPm[chipId][pwId]), sizeof(DRV_AT_PW_COUNT));
    }
    return ret;
}


/**************************************************************************
* 函数名称: DrvAtPwCountMemClear
* 功能描述: 清空保存pw计数的内存
* 访问的表: 
* 修改的表: 无
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-04-26   V1.0      
**************************************************************************/
WORD32 DrvAtPwCountMemClear(BYTE chipId, WORD32 pwId,BYTE countMode)
{
    WORD32 ret = DRV_AT_SUCCESS;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_PW_ID(pwId);

    if(MONITOR_REAL_TIME == countMode)
    {
        memset(&(g_atE1PwCount[chipId][pwId]),0,sizeof(DRV_AT_PW_COUNT));
    }
    else if(MONITOR_PERFORMANCE == countMode)
    {
        memset(&(g_atE1PwCountSum[chipId][pwId]),0,sizeof(DRV_AT_PW_COUNT));
    }
    else if(CES_STATIC_SEND_TO_PM == countMode)
    {
        memset(&(g_atE1PwCountToPm[chipId][pwId]),0,sizeof(DRV_AT_PW_COUNT));
    }
    
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtPwCountCheck
* 功能描述:   pw计数检查
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwCountCheck(WORD32* count)
{
    WORD32 ret = DRV_AT_SUCCESS;

    CHECK_AT_POINTER_NULL(count);
    
    if(*count >= (WORD32)0xffffffff)
    {
        *count = 0;
    }
    
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtPwCountGet
* 功能描述:   获取pw 统计
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwCountGet(BYTE chipId,WORD32 pwId)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    eAtRet atRet = cAtOk ;
    AtDevice atDevHdl =NULL ;
    AtModule atPwModule = NULL ;
    AtPwCounters atPwCounter = NULL ; /* Variable to hold Pseudowire counter object */
    AtPwTdmCounters atTdmPwCounter = NULL;
    AtPw atPwObj = NULL ;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_PW_ID(pwId);
    
    ret = DrvAtPwStatusCheck(chipId,pwId);/*check pw status,if failed ,return*/
    if(DRV_AT_SUCCESS != ret)
    {   
        DRV_AT_E1_PRINT(DRV_AT_PW_COUNT_THREAD,"ERROR: %s line %d, PW IS NOT EXISTED ! !! chipId is %u, pwId is %u\n", __FILE__, __LINE__,chipId,pwId);  
        return ret; 
    } 
    atDevHdl = DrvAtDevHdlGet(chipId) ;             /*get the devhdl*/
    CHECK_AT_POINTER_NULL(atDevHdl);  
    atPwModule = AtDeviceModuleGet(atDevHdl, cAtModulePw);      /*get pw module */
    if(NULL == atPwModule)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PW MODULE GET  FAILED ! !!CHIP ID IS  %u \n", __FILE__, __LINE__,chipId); 
        return DRV_AT_PW_MODULE_GET_FAILED; 
    }
    atRet = AtModuleLock(atPwModule);/*获取信号量，防止同时操作pwObj这个地址*/
    if(cAtOk != atRet)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtModuleLock() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet)); 
        return atRet; 
    }    
    ret = DrvAtPwObjGet(chipId, pwId, &atPwObj);
    if(DRV_AT_SUCCESS != ret)
    {   
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PW OBJECT GET FAILED ! !! \n", __FILE__, __LINE__);  
        atRet = AtModuleUnLock(atPwModule);/*释放信号量*/
        if(cAtOk != atRet)
        {
            DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtModuleUnLock() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet)); 
            return atRet; 
        }
        return ret; 
    }
    
    CHECK_AT_POINTER_NULL(atPwObj);
    atRet = AtChannelAllCountersClear((AtChannel)atPwObj, &atPwCounter);
    if(cAtOk != atRet)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtChannelAllCountersGet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet)); 
        return atRet; 
    }
    atRet = AtModuleUnLock(atPwModule);/*释放信号量*/
    if(cAtOk != atRet)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtModuleUnLock() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet)); 
        return atRet; 
    }
    memset(&g_atE1PwCount[chipId][pwId],0,sizeof(DRV_AT_PW_COUNT));
    /*scan the pw counters in realtime*/
    g_atE1PwCount[chipId][pwId].txPackets = AtPwCountersTxPacketsGet(atPwCounter);
    g_atE1PwCount[chipId][pwId].txPayloadBytes = AtPwCountersTxPayloadBytesGet(atPwCounter);
    g_atE1PwCount[chipId][pwId].rxPackets = AtPwCountersRxPacketsGet(atPwCounter);    
    g_atE1PwCount[chipId][pwId].rxPayloadBytes = AtPwCountersRxPayloadBytesGet(atPwCounter);    
    g_atE1PwCount[chipId][pwId].rxDiscardedPackets = AtPwCountersRxDiscardedPacketsGet(atPwCounter);
    g_atE1PwCount[chipId][pwId].rxMalformedPackets = AtPwCountersRxMalformedPacketsGet(atPwCounter);
    g_atE1PwCount[chipId][pwId].rxReorderedPackets = AtPwCountersRxReorderedPacketsGet(atPwCounter);
    g_atE1PwCount[chipId][pwId].rxLostPackets = AtPwCountersRxLostPacketsGet(atPwCounter);
    g_atE1PwCount[chipId][pwId].rxOutOfSeqDropPackets = AtPwCountersRxOutOfSeqDropPacketsGet(atPwCounter);
    g_atE1PwCount[chipId][pwId].rxOamPackets = AtPwCountersRxOamPacketsGet(atPwCounter);
    
    atTdmPwCounter = (AtPwTdmCounters)atPwCounter;
    g_atE1PwCount[chipId][pwId].txLbitPackets = AtPwTdmCountersTxLbitPacketsGet(atTdmPwCounter);
    g_atE1PwCount[chipId][pwId].txRbitPackets = AtPwTdmCountersTxRbitPacketsGet(atTdmPwCounter);
    g_atE1PwCount[chipId][pwId].rxLbitPackets = AtPwTdmCountersRxLbitPacketsGet(atTdmPwCounter);
    g_atE1PwCount[chipId][pwId].rxRbitPackets = AtPwTdmCountersRxRbitPacketsGet(atTdmPwCounter);
    g_atE1PwCount[chipId][pwId].rxJitBufOverrun = AtPwTdmCountersRxJitBufOverrunGet(atTdmPwCounter);
    g_atE1PwCount[chipId][pwId].rxJitBufUnderrun = AtPwTdmCountersRxJitBufUnderrunGet(atTdmPwCounter);
    g_atE1PwCount[chipId][pwId].rxLops = AtPwTdmCountersRxLopsGet(atTdmPwCounter);
    g_atE1PwCount[chipId][pwId].rxPacketsSentToTdm = AtPwTdmCountersRxPacketsSentToTdmGet(atTdmPwCounter);

    /*sum count everytime to show perfomence*/
    g_atE1PwCountSum[chipId][pwId].txPackets += g_atE1PwCount[chipId][pwId].txPackets ;
    g_atE1PwCountToPm[chipId][pwId].txPackets += g_atE1PwCount[chipId][pwId].txPackets ; 

    g_atE1PwCountSum[chipId][pwId].txPayloadBytes += g_atE1PwCount[chipId][pwId].txPayloadBytes ;
    g_atE1PwCountToPm[chipId][pwId].txPayloadBytes += g_atE1PwCount[chipId][pwId].txPayloadBytes ;
   
    g_atE1PwCountSum[chipId][pwId].rxPackets += g_atE1PwCount[chipId][pwId].rxPackets ;
    g_atE1PwCountToPm[chipId][pwId].rxPackets += g_atE1PwCount[chipId][pwId].rxPackets ;
    
    g_atE1PwCountSum[chipId][pwId].rxPayloadBytes += g_atE1PwCount[chipId][pwId].rxPayloadBytes ;
    g_atE1PwCountToPm[chipId][pwId].rxPayloadBytes += g_atE1PwCount[chipId][pwId].rxPayloadBytes ;
   
    g_atE1PwCountSum[chipId][pwId].rxDiscardedPackets += g_atE1PwCount[chipId][pwId].rxDiscardedPackets ;
    g_atE1PwCountToPm[chipId][pwId].rxDiscardedPackets += g_atE1PwCount[chipId][pwId].rxDiscardedPackets ;
   
    g_atE1PwCountSum[chipId][pwId].rxMalformedPackets += g_atE1PwCount[chipId][pwId].rxMalformedPackets ;
    g_atE1PwCountToPm[chipId][pwId].rxMalformedPackets += g_atE1PwCount[chipId][pwId].rxMalformedPackets ;
    
    g_atE1PwCountSum[chipId][pwId].rxReorderedPackets += g_atE1PwCount[chipId][pwId].rxReorderedPackets ;
    g_atE1PwCountToPm[chipId][pwId].rxReorderedPackets += g_atE1PwCount[chipId][pwId].rxReorderedPackets ;
   
    g_atE1PwCountSum[chipId][pwId].rxLostPackets += g_atE1PwCount[chipId][pwId].rxLostPackets ;
    g_atE1PwCountToPm[chipId][pwId].rxLostPackets += g_atE1PwCount[chipId][pwId].rxLostPackets ;
    
    g_atE1PwCountSum[chipId][pwId].rxOutOfSeqDropPackets += g_atE1PwCount[chipId][pwId].rxOutOfSeqDropPackets ;
    g_atE1PwCountToPm[chipId][pwId].rxOutOfSeqDropPackets += g_atE1PwCount[chipId][pwId].rxOutOfSeqDropPackets ;
    
    g_atE1PwCountSum[chipId][pwId].rxOamPackets += g_atE1PwCount[chipId][pwId].rxOamPackets ;
    g_atE1PwCountToPm[chipId][pwId].rxOamPackets += g_atE1PwCount[chipId][pwId].rxOamPackets ;
   
    g_atE1PwCountSum[chipId][pwId].txLbitPackets += g_atE1PwCount[chipId][pwId].txLbitPackets ;
    g_atE1PwCountToPm[chipId][pwId].txLbitPackets += g_atE1PwCount[chipId][pwId].txLbitPackets ;
   
    g_atE1PwCountSum[chipId][pwId].txRbitPackets += g_atE1PwCount[chipId][pwId].txRbitPackets ;
    g_atE1PwCountToPm[chipId][pwId].txRbitPackets += g_atE1PwCount[chipId][pwId].txRbitPackets ;
  
    g_atE1PwCountSum[chipId][pwId].rxLbitPackets += g_atE1PwCount[chipId][pwId].rxLbitPackets ;
    g_atE1PwCountToPm[chipId][pwId].rxLbitPackets += g_atE1PwCount[chipId][pwId].rxLbitPackets ;
    
    g_atE1PwCountSum[chipId][pwId].rxRbitPackets += g_atE1PwCount[chipId][pwId].rxRbitPackets ;
    g_atE1PwCountToPm[chipId][pwId].rxRbitPackets += g_atE1PwCount[chipId][pwId].rxRbitPackets ;
  
    g_atE1PwCountSum[chipId][pwId].rxJitBufOverrun += g_atE1PwCount[chipId][pwId].rxJitBufOverrun ;
    g_atE1PwCountToPm[chipId][pwId].rxJitBufOverrun += g_atE1PwCount[chipId][pwId].rxJitBufOverrun ;
    
    g_atE1PwCountSum[chipId][pwId].rxJitBufUnderrun += g_atE1PwCount[chipId][pwId].rxJitBufUnderrun ;
    g_atE1PwCountToPm[chipId][pwId].rxJitBufUnderrun += g_atE1PwCount[chipId][pwId].rxJitBufUnderrun ;
    
    g_atE1PwCountSum[chipId][pwId].rxLops += g_atE1PwCount[chipId][pwId].rxLops ;
    g_atE1PwCountToPm[chipId][pwId].rxLops += g_atE1PwCount[chipId][pwId].rxLops ;
    
    g_atE1PwCountSum[chipId][pwId].rxPacketsSentToTdm += g_atE1PwCount[chipId][pwId].rxPacketsSentToTdm ;
    g_atE1PwCountToPm[chipId][pwId].rxPacketsSentToTdm += g_atE1PwCount[chipId][pwId].rxPacketsSentToTdm ;

    
    return ret ;
}


/*******************************************************************************
* 函数名称:   DrvAtPwCountScan
* 功能描述:   PW计数扫描
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwCountScan(const WORD32* slotId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 chipNo = 0;
    WORD32 subcardId = 0;
    BYTE isOnline = 0;
    WORD32 pwId = 0;
    WORD32 i = 0;
    WORD32 alarmCount = 0;
    DRV_AT_PW_COUNT pwCount;
    
    CHECK_AT_POINTER_NULL(slotId);
    subcardId = *slotId ;
    if((subcardId > AT_MAX_SUBCARD_NUM_PER_SLOT)||(subcardId < AT_MIN_SUBCARD_NUM_PER_SLOT))
    {
        DRV_AT_E1_ERROR_LOG("INVALID subcardId!!!addr is 0x%x,slotId is %d,subcardId is %d\n",slotId,*slotId,subcardId);
        if(NULL !=slotId)
        {
            free((WORD32*)slotId);
            slotId = NULL;
        }
        return DRV_AT_INVALID_SUBCARD_ID;
    }

    free((WORD32*)slotId);
    slotId = NULL;
    
    chipNo = subcardId - 1;
    memset(&pwCount,0,sizeof(DRV_AT_PW_COUNT));
    g_threadId[chipNo].PW_COUNT_THR_ID = __gettid();
    DRV_AT_E1_COMMON_LOG("PW COUNT THREAD, CHIPID IS %u,__gettid is %u\n",chipNo,g_threadId[chipNo].PW_COUNT_THR_ID);
    
    while(1)
    {
        if(CE1_ENABLE == g_threadFlag[chipNo].PW_COUNT_THR_FLAG)
        {
            for(pwId = 0; pwId < AT_MAX_PW_NUM_PER_SUBCARD ; pwId++)
            {
                alarmCount = 0;
                for(i = 0;i < 8 ;i++)   /*对时钟域内的每一条pw读8次，如果大于等于六次不收包或者有丢包则记录这条pw有告警*/
                {   
                    ret = BSP_CE1TAN_BoardStatusGet((chipNo+1),&isOnline);
                    if (DRV_AT_SUCCESS != ret)
                    {
                        DRV_AT_E1_PRINT(DRV_AT_PW_COUNT_THREAD,"ERROR: %s line %d, BSP_CE1TAN_BoardStatusGet() ret=0x%x.\n", __FILE__, __LINE__, ret);
                        return ret;    
                    }
                    if(CE1_FALSE == isOnline)
                    {
                        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, CE1TAN IS NOT ONLINE.SUBCARD ID IS %u\n", __FILE__, __LINE__,(chipNo+1));
                        return DRV_AT_SUCCESS;    
                    }
                    ret = DrvAtPwCountGet(chipNo,pwId);
                    if(DRV_AT_SUCCESS != ret)
                    {
                        DRV_AT_E1_PRINT(DRV_AT_PW_COUNT_THREAD,"ERROR: %s line %d, PW COUNT GET FAILED !!CHIPID IS %u,PW ID is %u\n", __FILE__, __LINE__,chipNo,pwId); 
                        break; 
                    }
                    DrvAtPwCountMemGet(chipNo,pwId,&pwCount,MONITOR_REAL_TIME);/*获取CES统计*/
                    if((ACR_CLK_MODE == g_atE1PwInfo[chipNo][pwId].clockMode)||(DCR_CLK_MODE == g_atE1PwInfo[chipNo][pwId].clockMode))
                    {
                        if(MASTER_STATUS == g_atE1PwInfo[chipNo][pwId].clkState)
                        {
                            if(0 == pwCount.rxPackets)
                            {
                                alarmCount ++;
                            }
                        }
                        else if(SLAVE_STATUS == g_atE1PwInfo[chipNo][pwId].clkState)
                        {
                            if((0 == pwCount.rxPackets)||(0 != pwCount.rxJitBufOverrun)||(0 != pwCount.rxJitBufUnderrun)||(0 != pwCount.rxDiscardedPackets)||(0 != pwCount.rxLostPackets))
                            {
                                alarmCount ++;
                            }
                        }
                    }
                    g_pwThrcount[chipNo]++;
                    BSP_DelayMs(30);
                }
               if(alarmCount >5)
               {
                   g_isPwClkAlarm[chipNo][pwId] = CE1_TURE;
               }
               else
               {
                   g_isPwClkAlarm[chipNo][pwId] = CE1_FALSE;
               }    
            }
        }             
    
        BSP_DelayMs(500);  /* delay 500ms. */
        g_pwThrcountDbg[chipNo]++;
    }
        
    return ret ;
}


/*******************************************************************************
* 函数名称:   DrvPwCountThreadLaunch
* 功能描述:   起一个pw计数的线程
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvPwCountThreadLaunch(BYTE chipId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32* pSubcardId = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(&g_pwCountThrMod[chipId]);

    pSubcardId = malloc(sizeof(WORD32));
    CHECK_AT_POINTER_NULL(pSubcardId);
    memset(pSubcardId,0,sizeof(WORD32));

    *pSubcardId = (WORD32)(chipId+1);
    
    memset(&g_pwCountThrMod[chipId], 0, sizeof(struct mod_thread));
    init_thread(&g_pwCountThrMod[chipId], 0, 0, 80, DrvAtPwCountScan, pSubcardId);
    ret = start_mod_thread(&g_pwCountThrMod[chipId]);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, start_mod_thread() ret=0x%x.\n", __FILE__, __LINE__, ret);
        if(NULL !=pSubcardId)
        {
            free(pSubcardId);
            pSubcardId = NULL;
        }
        return DRV_PW_COUNT_THR_LAUNCH_FAILED;    
    }
    
    g_threadHdl[chipId].PW_COUNT_THR_HANDLE = g_pwCountThrMod[chipId].pthrid;
    ROSNG_TRACE_DEBUG("\nPW COUNT THREAD LAUNCH SUCCESS!\n");
    
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvPwCountThreadDelete
* 功能描述:   删除一个查询pw统计的线程
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-16     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvPwCountThreadDelete(BYTE chipId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(&g_pwCountThrMod[chipId]);
    
    stop_mod_thread(&g_pwCountThrMod[chipId]);
    g_pwThrcount[chipId] = 0;
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtPwPmInfoPrint
* 功能描述:   打印产品管理的pw参数
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwPmInfoPrint(T_BSP_SRV_FUNC_PW_BIND_ARG * pmPwAddInfo)
{
    WORD32 ret = DRV_AT_SUCCESS;

    CHECK_AT_POINTER_NULL(pmPwAddInfo);
    
    DRV_AT_E1_PRINT(DRV_AT_PW_CFG,"[CE1TAN][PW INFO]:%s line %d,subcardId is %u\n",__FILE__, __LINE__,pmPwAddInfo->dwSubSlotId);
    DRV_AT_E1_PRINT(DRV_AT_PW_CFG,"[CE1TAN][PW INFO]:%s line %d,portId is %u\n",__FILE__, __LINE__,pmPwAddInfo->dwPortId);
    DRV_AT_E1_PRINT(DRV_AT_PW_CFG,"[CE1TAN][PW INFO]:%s line %d,timeslotNum is %u\n",__FILE__, __LINE__,pmPwAddInfo->ptPwInfo->dwTimeslotNum);
    DRV_AT_E1_PRINT(DRV_AT_PW_CFG,"[CE1TAN][PW INFO]:%s line %d,timeslotBitmap is 0x%08x\n",__FILE__, __LINE__,pmPwAddInfo->dwTimeslotBmp);
    DRV_AT_E1_PRINT(DRV_AT_PW_CFG,"[CE1TAN][PW INFO]:%s line %d,PwId is %u\n",__FILE__, __LINE__,pmPwAddInfo->dwPWid);
    DRV_AT_E1_PRINT(DRV_AT_PW_CFG,"[CE1TAN][PW INFO]:%s line %d,dMac:{%x.%x.%x.%x.%x.%x}\n",__FILE__, __LINE__,pmPwAddInfo->dstMacAddr[0],  
                                                                                                                                                                             pmPwAddInfo->dstMacAddr[1],
                                                                                                                                                                             pmPwAddInfo->dstMacAddr[2],
                                                                                                                                                                             pmPwAddInfo->dstMacAddr[3],
                                                                                                                                                                             pmPwAddInfo->dstMacAddr[4],
                                                                                                                                                                             pmPwAddInfo->dstMacAddr[5]
    );
    DRV_AT_E1_PRINT(DRV_AT_PW_CFG,"[CE1TAN][PW INFO]:%s line %d,frame count is %u\n",__FILE__, __LINE__,pmPwAddInfo->ptPwInfo->dwFrameCnt);
    DRV_AT_E1_PRINT(DRV_AT_PW_CFG,"[CE1TAN][PW INFO]:%s line %d,service Type is %u\n",__FILE__, __LINE__,pmPwAddInfo->ptPwInfo->eFrameType);
    DRV_AT_E1_PRINT(DRV_AT_PW_CFG,"[CE1TAN][PW INFO]:%s line %d,payloadSize is %u\n",__FILE__, __LINE__,pmPwAddInfo->ptPwInfo->dwPayloadSize);
    DRV_AT_E1_PRINT(DRV_AT_PW_CFG,"[CE1TAN][PW INFO]:%s line %d,jitterBufferSize is %u\n",__FILE__, __LINE__,pmPwAddInfo->ptPwInfo->dwJitterSize);
    DRV_AT_E1_PRINT(DRV_AT_PW_CFG,"[CE1TAN][PW INFO]:%s line %d,clockMode is %u\n",__FILE__, __LINE__,pmPwAddInfo->ptPwInfo->clock_mode);
    DRV_AT_E1_PRINT(DRV_AT_PW_CFG,"[CE1TAN][PW INFO]:%s line %d,clockDomainNo is %u\n",__FILE__, __LINE__,pmPwAddInfo->ptPwInfo->dwClkDomainId);
    DRV_AT_E1_PRINT(DRV_AT_PW_CFG,"[CE1TAN][PW INFO]:%s line %d,clockState is %u\n",__FILE__, __LINE__,pmPwAddInfo->ptPwInfo->dwE1ClkState);    
    DRV_AT_E1_PRINT(DRV_AT_PW_CFG,"[CE1TAN][PW INFO]:%s line %d,vlan1 pri is 0x%x\n",__FILE__, __LINE__,pmPwAddInfo->ptPwInfo->wVlan1Pri);
    DRV_AT_E1_PRINT(DRV_AT_PW_CFG,"[CE1TAN][PW INFO]:%s line %d,vlan2 pri is 0x%x\n",__FILE__, __LINE__,pmPwAddInfo->ptPwInfo->wVlan2Pri);
    DRV_AT_E1_PRINT(DRV_AT_PW_CFG,"[CE1TAN][PW INFO]:%s line %d,vlan1Id is 0x%x\n",__FILE__, __LINE__,pmPwAddInfo->ptPwInfo->wZteVlan1Id);
    DRV_AT_E1_PRINT(DRV_AT_PW_CFG,"[CE1TAN][PW INFO]:%s line %d,vlan2Id is 0x%x\n",__FILE__, __LINE__,pmPwAddInfo->ptPwInfo->wZteVlan2Id);
    
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtPwPmInfoConvert
* 功能描述:   将产品管理下来的参数转换成驱动的参数
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwPmInfoConvert(T_BSP_SRV_FUNC_PW_BIND_ARG * pmPwAddInfo,DRV_AT_PW_PARA *drvPwAddInfo)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    
    CHECK_AT_POINTER_NULL(pmPwAddInfo);
    CHECK_AT_POINTER_NULL(pmPwAddInfo->ptPwInfo);
    CHECK_AT_POINTER_NULL(drvPwAddInfo);

    DrvAtPwPmInfoPrint(pmPwAddInfo);/*将产品管理带下的参数打印出来，调试使用*/
    
    drvPwAddInfo->chipId = pmPwAddInfo->dwSubSlotId - 1;
    drvPwAddInfo->e1LinkNum = pmPwAddInfo->dwPortId -1;
    drvPwAddInfo->cip = pmPwAddInfo->dwPWid; /*产品管理的pwid就是bsp的cip*/
    drvPwAddInfo->timeSlotBitMap = pmPwAddInfo->dwTimeslotBmp;
    memcpy(drvPwAddInfo->destMac,pmPwAddInfo->dstMacAddr,sizeof(pmPwAddInfo->dstMacAddr));
    drvPwAddInfo->tsNum = pmPwAddInfo->ptPwInfo->dwTimeslotNum ;
    drvPwAddInfo->frameNum = pmPwAddInfo->ptPwInfo->dwFrameCnt ;
    drvPwAddInfo->payloadSize = pmPwAddInfo->ptPwInfo->dwPayloadSize ;
    #if 0
    /*写入芯片的buffersize = 2*实际buffer大小*/
    /*实际buffer大小= 一个包的速率* 包的个数*/
    /*包的速率 = 包的净荷大小/每一帧的速率
    /*每一帧的速率= 时隙数* 一个时隙的传输速率= 时隙*8B/ms */
    /*包的个数= pm传下来的参数*/
    #endif
    drvPwAddInfo->jitterBufferSize = 2 *1000 * ((drvPwAddInfo->payloadSize) /(8 * drvPwAddInfo->tsNum) ) *(pmPwAddInfo->ptPwInfo->dwJitterSize); /* in microsecond */
    drvPwAddInfo->vlan1Pri = pmPwAddInfo->ptPwInfo->wVlan1Pri;
    drvPwAddInfo->vlan2Pri = pmPwAddInfo->ptPwInfo->wVlan2Pri;
    drvPwAddInfo->vlan1Cfi = 0;
    drvPwAddInfo->vlan1Cfi = 0;
    drvPwAddInfo->vlan1Info.zteVlan1Id = pmPwAddInfo->ptPwInfo->wZteVlan1Id;
    drvPwAddInfo->vlan2Info.zteVlan2Id = pmPwAddInfo->ptPwInfo->wZteVlan2Id;
    
    if(BSP_WP_SATOP == pmPwAddInfo->ptPwInfo->eFrameType)
    {
        drvPwAddInfo->servMode = SATOP_MODE;
    }
    else if(BSP_WP_CESOP_NO_CAS == pmPwAddInfo->ptPwInfo->eFrameType)
    {
        drvPwAddInfo->servMode = CESOP_MODE;
    }
    else
    {
    
    }
    /*时钟模式*/
    if(T_BSP_SYSTEM_TIMING == pmPwAddInfo->ptPwInfo->clock_mode)
    {
        drvPwAddInfo->clockMode = SYS_CLK_MODE;
    }
    else if(T_BSP_ACR_TIMING == pmPwAddInfo->ptPwInfo->clock_mode)
    {
        drvPwAddInfo->clockMode = ACR_CLK_MODE;
    }
    else if(T_BSP_DCR_TIMING == pmPwAddInfo->ptPwInfo->clock_mode)
    {
        drvPwAddInfo->clockMode = DCR_CLK_MODE;
    }
    /*时钟域*/
    drvPwAddInfo->clkDomainId = pmPwAddInfo->ptPwInfo->dwClkDomainId;
    if(T_BSP_E1_CLOCK_SLAVE == pmPwAddInfo->ptPwInfo->dwE1ClkState)
    {
        drvPwAddInfo->clkState = SLAVE_STATUS;
    }
    else if(T_BSP_E1_CLOCK_MASTER == pmPwAddInfo->ptPwInfo->dwE1ClkState)
    {
        drvPwAddInfo->clkState = MASTER_STATUS ;
    }
    return ret ;
    
}


/*******************************************************************************
* 函数名称:   DrvAtPwe3ServiceCreate
* 功能描述:   添加业务
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwe3ServiceCreate(T_BSP_SRV_FUNC_PW_BIND_ARG * pmPwAddInfo)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    
    CHECK_AT_POINTER_NULL(pmPwAddInfo);
    DRV_AT_PW_PARA drvPwAddInfo;
    
    memset(&drvPwAddInfo,0,sizeof(DRV_AT_PW_PARA));
    
    ret = DrvAtPwPmInfoConvert(pmPwAddInfo,&drvPwAddInfo);/*convert pm pw info-->drv pw info*/
    if(DRV_AT_SUCCESS != ret)
    {   
        BSP_Print(BSP_DEBUG_ALL,"[CE1TAN][PW ADD]:ERROR: %s line %d, PW INFO CONVERT FAILED ! !! CIP IS %u\n", __FILE__, __LINE__,pmPwAddInfo->dwPWid);  
        return ret; 
    }
    ret = DrvAtPwAdd(&drvPwAddInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        BSP_Print(BSP_DEBUG_ALL,"[CE1TAN][PW ADD]:ERROR: %s line %d,PW CREATE FAILED !!CHIP ID IS %u, CIP IS %u, RET IS %u\n", __FILE__, __LINE__,drvPwAddInfo.chipId,drvPwAddInfo.cip,ret);
        return ret;
    }    
    
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtPwe3ServiceDelete
* 功能描述:   删除业务
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwe3ServiceDelete(T_BSP_SRV_FUNC_PW_UNBIND_ARG *pmPwDelInfo)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    BYTE chipId = 0;
    WORD32 cip = 0; 
    DRV_AT_PW_PARA atPwInfo ;
    
    memset(&atPwInfo,0,sizeof(DRV_AT_PW_PARA));
    CHECK_AT_POINTER_NULL(pmPwDelInfo);
 #if 0   
    ret = DrvAtPwPmInfoConvert(pmPwDelInfo,&drvPwAddInfo);    /*convert pm pw info-->drv pw info*/
    if(DRV_AT_SUCCESS != ret)
    {   
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PW INFO CONVERT FAILED ! !! CIP IS %u\n", __FILE__, __LINE__,pmPwDelInfo->dwCip);  
        return ret; 
    }
#endif
    chipId = pmPwDelInfo->dwSubslotId -1;
    cip = pmPwDelInfo->dwPWid;
    
    ret = DrvAtPwInfoGetFromCip(chipId,cip,&atPwInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        BSP_Print(BSP_DEBUG_ALL,"ERROR: %s line %d, CAN NOT GET PW INFO ,CIP ID IS %u !!\n", __FILE__, __LINE__,cip);
        return DRV_AT_PW_INFO_GET_FAILED;
    }
    ret = DrvAtPwDelete(&atPwInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        BSP_Print(BSP_DEBUG_ALL,"ERROR: %s line %d,PW DELETE FAILED !!CHIP ID IS %u, CIP IS %u ,RET IS %u\n", __FILE__, __LINE__,chipId,cip,ret);
        return ret;
    }    
      
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtPwe3ServPldSizeUpdate
* 功能描述:   更新payloadsize
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwe3ServPldSizeUpdate(T_BSP_SRV_FUNC_PW_BIND_ARG *pmPwInfo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE chipId = 0;
    BYTE servMode = 0;
    WORD32 pwId = 0 ;
    WORD32 cip = 0;
    WORD32 enc = 0;
    WORD16 pldSize = 0;
    WORD16 newPldSize = 0;
    DRV_AT_PW_PARA drvPwAddInfo;
    
    CHECK_AT_POINTER_NULL(pmPwInfo);  
    memset(&drvPwAddInfo,0,sizeof(DRV_AT_PW_PARA));
      
    ret = DrvAtPwPmInfoConvert(pmPwInfo,&drvPwAddInfo);    /*convert pm pw info-->drv pw info*/
    if(DRV_AT_SUCCESS != ret)
    {   
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PW INFO CONVERT FAILED ! !! CIP IS %u\n", __FILE__, __LINE__,pmPwInfo->dwCip);  
        return ret; 
    }
    chipId = drvPwAddInfo.chipId;
    cip = drvPwAddInfo.cip;
    servMode = drvPwAddInfo.servMode;
    enc = drvPwAddInfo.frameNum;
    pldSize = drvPwAddInfo.payloadSize;
    
    pwId = DrvAtPwIdGetFromCip(chipId,cip);
    if(INVALID_PW_ID == pwId)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, The Cip is not existed !!CHIP ID IS %u CIP IS %u\n", __FILE__, __LINE__,chipId, cip);
        return DRV_AT_PW_NOT_CREATE;
    }

    /*judge the serv type ,if satop ,choose the payloadsize,otherwise ,choose the enc*/
    if(SATOP_MODE == servMode)
    {
        newPldSize = pldSize;
    }
    else if (CESOP_MODE == servMode)
    {
        newPldSize = enc;
    }
    ret = DrvAtPwPldSizeModify(chipId,pwId,newPldSize);
    if(DRV_AT_SUCCESS != ret)
    {   
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PW PAYLOAD SIZE UPDATE FAILED ! !! CHIP ID IS %u,PW ID IS %u\n", __FILE__, __LINE__,chipId,pwId);  
        return ret; 
    }
    /*merge driver table*/
    DrvAtPwPldSizeInfoUpdate(chipId,pwId,enc,pldSize); 
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtPwe3ServJitBufSizeUpdate
* 功能描述:   更新jiterBuffer
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwe3ServJitBufSizeUpdate(T_BSP_SRV_FUNC_PW_BIND_ARG *pmPwInfo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE chipId = 0;
    WORD32 pwId = 0 ;
    WORD32 cip = 0;
    WORD32 jitterBufSize = 0;
    DRV_AT_PW_PARA drvPwAddInfo;
    
    CHECK_AT_POINTER_NULL(pmPwInfo);  
    memset(&drvPwAddInfo,0,sizeof(DRV_AT_PW_PARA));
      
    ret = DrvAtPwPmInfoConvert(pmPwInfo,&drvPwAddInfo);    /*convert pm pw info-->drv pw info*/
    if(DRV_AT_SUCCESS != ret)
    {   
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PW INFO CONVERT FAILED !!! CIP IS %u\n", __FILE__, __LINE__,pmPwInfo->dwCip);  
        return ret; 
    }
    chipId = drvPwAddInfo.chipId;
    cip = drvPwAddInfo.cip;
    jitterBufSize = drvPwAddInfo.jitterBufferSize;
    
    pwId = DrvAtPwIdGetFromCip(chipId,cip);
    if(INVALID_PW_ID == pwId)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, The Cip is not existed !!CHIP ID IS %u CIP IS %u\n", __FILE__, __LINE__,chipId, cip);
        return DRV_AT_PW_NOT_CREATE;
    } 
    ret = DrvAtPwjitBufSizeModify(chipId,pwId,jitterBufSize);
    if(DRV_AT_SUCCESS != ret)
    {   
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PW PAYLOAD SIZE UPDATE FAILED ! !! CHIP ID IS %u,PW ID IS %u\n", __FILE__, __LINE__,chipId,pwId);  
        return ret; 
    }
    /*merge driver table*/
    DrvAtPwJitBuffInfoUpdate(chipId,pwId,jitterBufSize);
    
    return ret;

}


/*******************************************************************************
* 函数名称:   DrvAtPwe3ServVlan1PriUpdate
* 功能描述:   更新vlan1 pri
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwe3ServVlan1PriUpdate(T_BSP_SRV_FUNC_PW_BIND_ARG *pmPwInfo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE chipId = 0;
    WORD32 pwId = 0 ;
    WORD32 cip = 0;
    DRV_AT_PW_PARA drvPwAddInfo;
    AtPw atPwObj = NULL;
       
    CHECK_AT_POINTER_NULL(pmPwInfo);  
    memset(&drvPwAddInfo,0,sizeof(DRV_AT_PW_PARA));
       
    ret = DrvAtPwPmInfoConvert(pmPwInfo,&drvPwAddInfo);    /*convert pm pw info-->drv pw info*/
    if(DRV_AT_SUCCESS != ret)
    {   
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PW INFO CONVERT FAILED !!! CIP IS %u\n", __FILE__, __LINE__,pmPwInfo->dwCip);  
        return ret; 
    }
    chipId = drvPwAddInfo.chipId;
    cip = drvPwAddInfo.cip;
    
    pwId = DrvAtPwIdGetFromCip(chipId,cip);
    if(INVALID_PW_ID == pwId)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, The Cip is not existed !!CHIP ID IS %u CIP IS %u\n", __FILE__, __LINE__,chipId, cip);
        return DRV_AT_PW_NOT_CREATE;
    } 
    /*get pw obj*/
    ret = DrvAtPwObjGet(chipId,pwId,&atPwObj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PW OBJECT  GET  FAILED ! !! \n", __FILE__, __LINE__); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atPwObj);
    /* Configure Ethernet layer again*/
    ret = DrvAtPwEthLayerSet(atPwObj,&drvPwAddInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, DrvAtPwEthLayerSet() ret=0x%x.\n", __FILE__, __LINE__, ret);
        return ret;
    }   
    /*更新驱动表项*/
    ret = DrvAtSavePwCfg(chipId,pwId,&drvPwAddInfo);  
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, DrvAtSavePwCfg() ret=0x%x.\n", __FILE__, __LINE__, ret);
        return ret;
    }
    return ret;
}


/*******************************************************************************
* 函数名称:   DrvAtPwe3ServCesStatic
* 功能描述:   pw ces统计接口
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwe3ServCesStatic(T_BSP_SRV_FUNC_CHIP_CESSTATIC_ARG *cesStaticInfo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE chipId = 0;
    WORD32 cip = 0;
    WORD32 pwId = 0;
    T_BSP_TDM_PW_STATISTICS* cesStatic = NULL;
    DRV_AT_PW_COUNT pwCount;
    
    CHECK_AT_POINTER_NULL(cesStaticInfo);
    CHECK_AT_POINTER_NULL(cesStaticInfo->pStatic);
    
    memset(&pwCount,0,sizeof(DRV_AT_PW_COUNT));
    chipId = cesStaticInfo->dwSubSlotId - 1;
    cip = cesStaticInfo->dwPWId;
    CHECK_AT_CHIP_ID(chipId);
    cesStatic = (T_BSP_TDM_PW_STATISTICS *)(cesStaticInfo->pStatic);
    
    pwId = DrvAtPwIdGetFromCip(chipId,cip);
    if(INVALID_PW_ID == pwId)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, The Cip is not existed !!CHIP ID IS %u CIP IS %u\n", __FILE__, __LINE__,chipId, cip);

        memset(cesStatic,0,sizeof(T_BSP_TDM_PW_STATISTICS));/*先清0再赋值*/
        cesStatic->NearRecvPkt.dwFlag = 32;
        cesStatic->NearSendPkt.dwFlag = 32; 
        cesStatic->NearSendByt.dwFlag = 32;
        cesStatic->IdlePkt.dwFlag = 0;   /*暂不支持*/
        cesStatic->NearLosPkt.dwFlag = 16;
        cesStatic->CsfPkt.dwFlag = 16;
        cesStatic->SNErrPkt.dwFlag = 16;
        cesStatic->MalformPkt.dwFlag = 16;
        cesStatic->BufOverCont.dwFlag = 16;
        cesStatic->FarLosPkt.dwFlag = 16;
        cesStatic->RdiPkt.dwFlag = 16;
        cesStatic->RxTDMByt.dwFlag = 32;
        cesStatic->RxTDMPkt.dwFlag = 32;
        cesStatic->RxFwdPkt.dwFlag = 32;
        cesStatic->RxOosync.dwFlag = 0;   /*暂不支持*/
        cesStatic->RxDrop.dwFlag = 0;   /*暂不支持*/

        return DRV_AT_PW_NOT_CREATE;
    }
    DRV_AT_E1_PRINT(DRV_AT_CES_STATIC_QUERY,"[CE1TAN][CES STATIC GET]:%s line %d,CIP(PWID FOR PM )IS %u,PWId(FOR BSP)is %u\n",__FILE__, __LINE__,cip,pwId);
    ret = DrvAtPwCountMemGet(chipId,pwId,&pwCount,CES_STATIC_SEND_TO_PM); /*获取上报给产品管理的CES 统计*/
    
    memset(cesStatic,0,sizeof(T_BSP_TDM_PW_STATISTICS)); /*先清0再赋值*/
    cesStatic->NearRecvPkt.dwFlag = 32;
    cesStatic->NearRecvPkt.dwLow = pwCount.rxPackets +  pwCount.rxMalformedPackets;  /*psn发过来的总报文数*/

    cesStatic->NearSendPkt.dwFlag = 32;
    cesStatic->NearSendPkt.dwLow = pwCount.rxPackets;
    
    cesStatic->NearSendByt.dwFlag = 32;
    cesStatic->NearSendByt.dwLow = pwCount.rxPayloadBytes;
    
    cesStatic->IdlePkt.dwFlag = 0;   /*暂不支持*/
    cesStatic->IdlePkt.dwLow = 0.;
    
    cesStatic->NearLosPkt.dwFlag = 16;
    cesStatic->NearLosPkt.dwLow = pwCount.rxLostPackets;

    cesStatic->CsfPkt.dwFlag = 16;
    cesStatic->CsfPkt.dwLow = pwCount.rxLbitPackets;

    cesStatic->SNErrPkt.dwFlag = 16;
    cesStatic->SNErrPkt.dwLow = pwCount.rxOutOfSeqDropPackets;

    cesStatic->MalformPkt.dwFlag = 16;
    cesStatic->MalformPkt.dwLow = pwCount.rxMalformedPackets;

    cesStatic->BufOverCont.dwFlag = 16;
    cesStatic->BufOverCont.dwLow = pwCount.rxJitBufOverrun;

    cesStatic->FarLosPkt.dwFlag = 16;
    cesStatic->FarLosPkt.dwLow = pwCount.rxLbitPackets;

    cesStatic->RdiPkt.dwFlag = 16;
    cesStatic->RdiPkt.dwLow = pwCount.rxRbitPackets;

    cesStatic->RxTDMByt.dwFlag = 32;
    cesStatic->RxTDMByt.dwLow = pwCount.txPayloadBytes;

    cesStatic->RxTDMPkt.dwFlag = 32;
    cesStatic->RxTDMPkt.dwLow = pwCount.txPackets;

    cesStatic->RxFwdPkt.dwFlag = 32;
    cesStatic->RxFwdPkt.dwLow = pwCount.txPackets;

    cesStatic->RxOosync.dwFlag = 0;   /*暂不支持*/
    cesStatic->RxOosync.dwLow = 0;

    cesStatic->RxDrop.dwFlag = 0;   /*暂不支持*/
    cesStatic->RxDrop.dwLow = 0;


    
    return ret;
}



/*******************************************************************************
* 函数名称:   DrvAtPwe3ServCesAlarm
* 功能描述:   pw ces告警接口
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwe3ServCesAlarm(T_BSP_SRV_FUNC_CHIP_CESALARM_ARG* cesAlarmInfo)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    BYTE chipId = 0;
    WORD32 cip = 0;
    WORD32 pwId = 0;
    T_BSP_SRVPWE3_CesAlarm cesAlarm;
    DRV_AT_PW_COUNT pwCount;
    
    CHECK_AT_POINTER_NULL(cesAlarmInfo);
    CHECK_AT_POINTER_NULL(cesAlarmInfo->pAlarm);
    memset(&cesAlarm,0,sizeof(T_BSP_SRVPWE3_CesAlarm));
    memset(&pwCount,0,sizeof(DRV_AT_PW_COUNT));
    
    chipId = cesAlarmInfo->dwSubSlotId - 1;
    cip = cesAlarmInfo->dwPWId;
    CHECK_AT_CHIP_ID(chipId);
    
    pwId = DrvAtPwIdGetFromCip(chipId,cip);
    if(INVALID_PW_ID == pwId)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, The Cip is not existed !!CHIP ID IS %u CIP IS %u\n", __FILE__, __LINE__,chipId, cip);
        return DRV_AT_PW_NOT_CREATE;
    }
    DRV_AT_E1_PRINT(DRV_AT_CES_ALARM_QUERY,"[CE1TAN][CES ALARM GET]:%s line %d,CIP(PWID FOR PM )IS %u,PWId(FOR BSP)is %u\n",__FILE__, __LINE__,cip,pwId);
    ret = DrvAtPwCountMemGet(chipId,pwId,&pwCount,MONITOR_REAL_TIME);
    
    cesAlarm.bCSF = (0 == pwCount.rxLbitPackets)?0:1;
    cesAlarm.bOverRun = (0 == pwCount.rxJitBufOverrun)?0:1;
    cesAlarm.bRDI = (0 == pwCount.rxRbitPackets)?0:1;
    cesAlarm.bPktLos = ((0 == pwCount.rxLostPackets)&&( 0 == pwCount.rxLops))?0:1;
    cesAlarm.bSN = (0 == pwCount.rxOutOfSeqDropPackets)?0:1;
    
    return ret ;
}




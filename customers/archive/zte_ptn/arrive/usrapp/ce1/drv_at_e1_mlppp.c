/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_at_e1_mlppp.c
* 文件标识: 
* 其它说明: ce1tan mlppp 业务代码
* 当前版本: V1.0
* 作    者: 陈博10140635.
* 完成日期: 2013-12-16
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/
#include "AtDriver.h"
#include "AtDevice.h"
#include "AtCommon.h"
#include "AtModulePdh.h"
#include "AtModuleEncap.h"
#include "AtModuleEth.h"
#include "AtModulePpp.h"
#include "AtEncapChannel.h"
#include "AtHdlcChannel.h"
#include "AtHdlcLink.h"
#include "AtHdlcBundle.h"
#include "AtPppLink.h"
#include "AtMpBundle.h"
#include "AtEthFlow.h"
#include "AtZtePtn.h"
#include "drv_at_e1_mlppp.h"

/*全局变量*/
DRV_AT_PPP_LINK_INFO g_atPppLinkInfo[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_MAX_PPP_NUM_PER_SUBCARD];
DRV_AT_MLPPP_BUNDLE_INFO g_atMlpppBundleInfo[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_MAX_MLPPP_BUNDLE_NUM_PER_SUBCARD];
WORD32  g_atBundleLinksNum[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_MAX_MLPPP_BUNDLE_NUM_PER_SUBCARD];

/*外部变量*/


/*外部函数*/
extern AtDevice DrvAtDevHdlGet(BYTE chipId);
extern WORD32 DrvAtE1ObjGet(BYTE chipId,WORD32 e1LinkId, AtPdhDe1* atE1Obj);

/*******************************************************************************
* 函数名称:   DrvAtIdlePppLinkNoGet
* 功能描述:   获取空闲可用的PppLink
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
********************************************************************************/
WORD32 DrvAtIdlePppLinkNoGet(BYTE chipId,WORD32 *pppLinkNo)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    WORD32 i = 0;
    
    CHECK_AT_POINTER_NULL(pppLinkNo);
    *pppLinkNo = INVALID_PPP_LINKNO; /*lint !e578*/  /* default set to invalid encapChanNo value */
    
    for(i = 0; i < AT_MAX_PPP_NUM_PER_SUBCARD ; i++)
    {
        if(PPP_LINK_IDLE == g_atPppLinkInfo[chipId][i].status)/*找到了一条可用的信道*/
        {
            *pppLinkNo = i;
            break;
        }
    }
    if(INVALID_PPP_LINKNO == (*pppLinkNo))
    {
        return DRV_AT_INVALID_PPP_LINK_NO ;
    }
    
    return ret;
}


/*******************************************************************************
* 函数名称:   DrvAtIdleMlpppBundleIndexGet
* 功能描述:   获取空闲可用的mlppp index
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
********************************************************************************/
WORD32 DrvAtIdleMlpppBundleIndexGet(BYTE chipId,WORD32 *bundleIndex)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    WORD32 i = 0;
    
    CHECK_AT_POINTER_NULL(bundleIndex);
    *bundleIndex = INVALID_MLPPP_BUNDLE_INDEX; /*lint !e578*/  /* default set to invalid encapChanNo value */
    
    for(i = 0; i < AT_MAX_MLPPP_BUNDLE_NUM_PER_SUBCARD ; i++)
    {
        if(MLPPP_BUNDLE_IDLE == g_atMlpppBundleInfo[chipId][i].status)/*找到了一条可用的信道*/
        {
            *bundleIndex = i;
            break;
        }
    }
    if(INVALID_MLPPP_BUNDLE_INDEX == (*bundleIndex))
    {
        return DRV_AT_INVALID_MLPPP_BUNDLE_INDEX ;
    }
    
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtPppLinkStatusCheck
* 功能描述:   检查PppLink是否被创建
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
********************************************************************************/
WORD32 DrvAtPppLinkStatusCheck(BYTE chipId ,WORD32 pppLinkNo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_PPPLINK_NO(pppLinkNo);
    
    if(PPP_LINK_USED != g_atPppLinkInfo[chipId][pppLinkNo].status)    /*表明还没被创建*/
    {
        ret = DRV_AT_PPP_LINK_NOT_CREATE ;
    }
    
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtMlpppStatusCheck
* 功能描述:   检查mlppp是否被创建
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
********************************************************************************/
WORD32 DrvAtMlpppStatusCheck(BYTE chipId ,WORD32 bundleIndex)
{
    WORD32 ret = DRV_AT_SUCCESS;
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_MLPPP_BUDNDLE_INDEX(bundleIndex);
    
    if(MLPPP_BUNDLE_USED != g_atMlpppBundleInfo[chipId][bundleIndex].status)    /*表明还没被创建*/
    {
        ret = DRV_AT_MLPPP_BUNDLE_NOT_CREATE ;
    }
    
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtPppLinkAddtoBundleCheck
* 功能描述:   检查PppLink是否已经加入mlppp组
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
********************************************************************************/
WORD32 DrvAtPppLinkAddtoBundleCheck(BYTE chipId,WORD32 bundleIndex,WORD32 e1LinkNo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    CHECK_AT_MLPPP_BUDNDLE_INDEX(bundleIndex);
        
    if(PPP_LINK_USED != g_atMlpppBundleInfo[chipId][bundleIndex].linkMember[e1LinkNo].status)    
    {
        ret = DRV_AT_PPP_LINK_NOT_CREATE ;
    }
    
    return ret ; 

}

/*******************************************************************************
* 函数名称:   DrvAtPppLinkNoGetFromCip
* 功能描述:   找到cip 对应的ppp linkno
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPppLinkNoGetFromCip(BYTE chipId,WORD32 cip)
{
    WORD32 i = 0;
    WORD32 pppLinkNo = INVALID_PPP_LINKNO; 
    
    for(i = 0 ; i< AT_MAX_PPP_NUM_PER_SUBCARD; i++)
    {
        if((g_atPppLinkInfo[chipId][i].cip == cip)&&(PPP_LINK_USED == g_atPppLinkInfo[chipId][i].status))
        {
            pppLinkNo = i ;
            break ;
        }
    }  
    return pppLinkNo ;
}

/*******************************************************************************
* 函数名称:   DrvAtPppLinkNoGetFromE1
* 功能描述:   找到e1(ndxs0) 对应的pppLinkNo
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
********************************************************************************/
WORD32 DrvAtPppLinkNoGetFromE1(BYTE chipId,WORD32 e1LinkNo,WORD32* pppLinkNo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 i = 0;
        
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(pppLinkNo);
    *pppLinkNo = INVALID_PPP_LINKNO; 
    
    for(i = 0 ; i< AT_MAX_PPP_NUM_PER_SUBCARD ; i++)
    {
        if((g_atPppLinkInfo[chipId][i].e1LinkNo == e1LinkNo)&&(PPP_LINK_USED == g_atPppLinkInfo[chipId][i].status))
        {
            *pppLinkNo = i ;
            break ;
        }
    }  
    
    return ret ;
}
    
/*******************************************************************************
* 函数名称:   DrvAtEncapChanNoGetFromE1
* 功能描述:   找到e1(ndxs0) 对应的EncapChanNo
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
********************************************************************************/
WORD32 DrvAtEncapChanNoGetFromE1(BYTE chipId,WORD32 e1LinkNo,WORD32* encapChanNo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 i = 0;
        
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(encapChanNo);
    *encapChanNo = INVALID_ENCAP_CHANNO; 
    
    for(i = 0 ; i< AT_MAX_PPP_NUM_PER_SUBCARD ; i++)
    {
        if((g_atPppLinkInfo[chipId][i].e1LinkNo == e1LinkNo)&&(PPP_LINK_USED == g_atPppLinkInfo[chipId][i].status))
        {
            *encapChanNo = g_atPppLinkInfo[chipId][i].encapChanNo;
            break ;
        }
    }  
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtFlowIdGetFromE1
* 功能描述:   找到e1(ndxs0) 对应的flowId
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
********************************************************************************/
WORD32 DrvAtFlowIdGetFromE1( BYTE chipId,WORD32 e1LinkNo,WORD32* flowId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 i = 0;
        
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    CHECK_AT_POINTER_NULL(flowId);
    *flowId = INVALID_FLOW_ID; 
    
    for(i = 0 ; i< AT_MAX_PPP_NUM_PER_SUBCARD ; i++)
    {
        if((g_atPppLinkInfo[chipId][i].e1LinkNo == e1LinkNo)&&(PPP_LINK_USED == g_atPppLinkInfo[chipId][i].status))
        {
            *flowId = g_atPppLinkInfo[chipId][i].flowId;
            break ;
        }
    }  
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtMlpppIndexGetFromBundleId
* 功能描述:   找到bundleId 对应的index
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
********************************************************************************/
WORD32 DrvAtMlpppIndexGetFromBundleId( BYTE chipId,WORD32 bundleId,WORD32* bundleIndex)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 i = 0;
        
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(bundleIndex);
    *bundleIndex = INVALID_MLPPP_BUNDLE_INDEX; 
    
    for(i = 0 ; i< AT_MAX_MLPPP_BUNDLE_NUM_PER_SUBCARD ; i++)
    {
        if((g_atMlpppBundleInfo[chipId][i].bundleId == bundleId)&&(MLPPP_BUNDLE_USED == g_atMlpppBundleInfo[chipId][i].status))
        {
            *bundleIndex = i ;
            break ;
        }
    }  
    
    return ret ;
}

 /*******************************************************************************
* 函数名称:   DrvAtPppInfoGetFromPppLinkNo
* 功能描述:   找到pppLinkNo 对应的pPP信息
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPppInfoGetFromPppLinkNo(BYTE chipId,WORD32 pppLinkNo,DRV_AT_PPP_LINK_INFO * PppLinkInfo)
{
    WORD32 ret = DRV_AT_SUCCESS;
  
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_PPPLINK_NO(pppLinkNo);
    CHECK_AT_POINTER_NULL(PppLinkInfo);
    
    memcpy(PppLinkInfo,&g_atPppLinkInfo[chipId][pppLinkNo],sizeof(DRV_AT_PPP_LINK_INFO)); 
    
    return ret ;
}
 
 /*******************************************************************************
* 函数名称:   DrvAtPppInfoGetFromE1
* 功能描述:   找到E1 对应的pPP信息
* 输入参数:   无
* 输出参数:   无
* 返 回 值:   无
* 其它说明:   其它说明
* 修改日期              版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPppInfoGetFromE1(BYTE chipId,WORD32 e1LinkNo,DRV_AT_PPP_LINK_INFO * PppLinkInfo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 i = 0;
    WORD32 pppLinkNo = INVALID_PPP_LINKNO; 
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    CHECK_AT_POINTER_NULL(PppLinkInfo);
    
    for(i = 0 ; i< AT_MAX_PPP_NUM_PER_SUBCARD ; i++)
    {
        if((g_atPppLinkInfo[chipId][i].e1LinkNo== e1LinkNo)&&(PPP_LINK_USED == g_atPppLinkInfo[chipId][i].status))
        {
            pppLinkNo = i ;
            break ;
        }
    }  
    if(pppLinkNo != INVALID_PPP_LINKNO)  
    {
        memcpy(PppLinkInfo,&g_atPppLinkInfo[chipId][pppLinkNo],sizeof(DRV_AT_PPP_LINK_INFO));
    }
    else /*如果这条PPP没有创建，返回失败*/
    {
        ret = DRV_AT_PPP_INFO_GET_FAILED;
    }
    return ret ;
}


 /*******************************************************************************
* 函数名称:   DrvAtPppInfoGetFromCip
* 功能描述:   找到cip 对应的ppp信息
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期     版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPppInfoGetFromCip(BYTE chipId,WORD32 cip,DRV_AT_PPP_LINK_INFO* drvPppInfo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 i = 0;
    WORD32 pppLinkNo = INVALID_PPP_LINKNO; 
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(drvPppInfo);
    
    for(i = 0 ; i< AT_MAX_PPP_NUM_PER_SUBCARD; i++)
    {
        if((g_atPppLinkInfo[chipId][i].cip == cip)&&(PPP_LINK_USED == g_atPppLinkInfo[chipId][i].status))
        {
            pppLinkNo = i ;
            break ;
        }
    }  
    if(pppLinkNo != INVALID_PPP_LINKNO)  
    {
        memcpy(drvPppInfo,&g_atPppLinkInfo[chipId][pppLinkNo],sizeof(DRV_AT_PPP_LINK_INFO));
    }
    else /*如果这条cip没有创建，返回失败*/
    {
        ret = DRV_AT_PPP_INFO_GET_FAILED;
    }
    
    return ret ;
}

  /*******************************************************************************
* 函数名称:   DrvAtMlpppInfoGetFromIndex
* 功能描述:   找到bundleIndex 对应的mlppp信息
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtMlpppInfoGetFromIndex(BYTE chipId,WORD32 bundleIndex,DRV_AT_MLPPP_BUNDLE_INFO* mlpppInfo)
{
    WORD32 ret = DRV_AT_SUCCESS;
  
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_MLPPP_BUDNDLE_INDEX(bundleIndex);
    CHECK_AT_POINTER_NULL(mlpppInfo);
        
    memcpy(mlpppInfo,&g_atMlpppBundleInfo[chipId][bundleIndex],sizeof(DRV_AT_MLPPP_BUNDLE_INFO)); 
    
    return ret ;
}
  
  /*******************************************************************************
* 函数名称:   DrvAtMlpppBundleLinksNumGet
* 功能描述:   获取mlppp中的link数
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtMlpppBundleLinksNumGet(BYTE chipId, WORD32 bundleIndex,WORD32* pppLinkNum)
{
    WORD32 ret = DRV_AT_SUCCESS;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_MLPPP_BUDNDLE_INDEX(bundleIndex);
    CHECK_AT_POINTER_NULL(pppLinkNum);

    *pppLinkNum = g_atBundleLinksNum[chipId][bundleIndex];
    
    return ret;
}
  
/*********************************************************************
* 函数名称：DrvAtEncapChanObjGet
* 功能描述：获取单链路的EncapChannel
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtEncapChanObjGet(BYTE chipId, WORD32 encapChanNo,AtEncapChannel* encapChanObj)
{
    WORD32 ret = DRV_AT_SUCCESS;
    AtDevice atDevHdl = NULL;
    AtModuleEncap atEncapModule = NULL;

    /*check chipid and port*/
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(encapChanObj);
    atDevHdl = DrvAtDevHdlGet(chipId);
    CHECK_AT_POINTER_NULL(atDevHdl);
    
    atEncapModule = (AtModuleEncap)AtDeviceModuleGet(atDevHdl, cAtModuleEncap);      /*get encap module */
    if(NULL == atEncapModule)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ENCAP MODULE GET FAILED ! !!CHIP ID IS %u \n", __FILE__, __LINE__,chipId); 
        return DRV_AT_ENCAP_MODULE_GET_FAILED; 
    }

    *encapChanObj = AtModuleEncapChannelGet(atEncapModule,encapChanNo);
    if(NULL == (*encapChanObj))
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ENCAP CHAN OBJ GET  FAILED ! !!CHIP ID IS  %u ENCAP CHANNO IS %u \n", __FILE__, __LINE__,chipId,encapChanNo); 
        return DRV_AT_ENCAP_CHAN_OBJ_GET_FAILED; 
    }

    return ret;
}


/*********************************************************************
* 函数名称：DrvAtEthFlowObjGet
* 功能描述：获取单链路的flow object
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值: 返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtEthFlowObjGet(BYTE chipId, WORD32 flowId,AtEthFlow* ethFlowObj)
{
    WORD32 ret = DRV_AT_SUCCESS;
    AtDevice atDevHdl = NULL;
    AtModuleEth atEthModule = NULL;

    /*check chipid and port*/
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(ethFlowObj);

    atDevHdl = DrvAtDevHdlGet(chipId);
    CHECK_AT_POINTER_NULL(atDevHdl);
    atEthModule = (AtModuleEth)AtDeviceModuleGet(atDevHdl, cAtModuleEth);      /*get encap module */
    if(NULL == atEthModule)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ETH MODULE GET FAILED ! !!CHIP ID IS %u \n", __FILE__, __LINE__,chipId); 
        return DRV_AT_ETH_MOUDULE_GET_FAILED; 
    }

    *ethFlowObj = AtModuleEthFlowGet(atEthModule,flowId);
    if(NULL == (*ethFlowObj))
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ETH FLOW OBJ GET  FAILED ! !!CHIP ID IS  %u FLOW ID IS %u \n", __FILE__, __LINE__,chipId,flowId); 
        return DRV_AT_ETH_FLOW_GET_FAILD; 
    }

    return ret;
}

/*********************************************************************
* 函数名称：DrvAtMlpppBundleGet
* 功能描述：获取一个mlppp bundle
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtMlpppBundleGet(BYTE chipId,WORD32 bundleIndex,AtMpBundle* bundle)
{
    WORD32 ret = DRV_AT_SUCCESS;
    AtModulePpp pppModule = NULL;
    AtDevice atDevHdl = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_MLPPP_BUDNDLE_INDEX(bundleIndex);
    CHECK_AT_POINTER_NULL(bundle);
    
    atDevHdl = DrvAtDevHdlGet(chipId);
    CHECK_AT_POINTER_NULL(atDevHdl);
    pppModule = (AtModulePpp)AtDeviceModuleGet(atDevHdl, cAtModulePpp);
    if(NULL == pppModule)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, PPP MODULE GET FAILED ! !!CHIP ID IS %u \n", __FILE__, __LINE__,chipId); 
        return DRV_AT_PPP_MODULE_GET_FAILED; 
    }
    *bundle = AtModulePppMpBundleGet(pppModule,bundleIndex);
    if(NULL == (*bundle))
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, MLPPP BUNDLE GET  FAILED ! !!CHIP ID IS  %u BUNDLE ID IS %u \n", __FILE__, __LINE__,chipId,bundleIndex); 
        return DRV_AT_MLPPP_BUNDLE_GET_FAILED; 
    }
    
    return ret;
}


/*********************************************************************
* 函数名称：DrvAtHdlcLinkObjGet
* 功能描述：获取单链路的hdlc link
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:       返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtHdlcLinkObjGet(BYTE chipId, WORD32 encapChanNo, AtHdlcLink *pAtHdlcLink)
{
    WORD32 ret = DRV_AT_SUCCESS;
    AtEncapChannel atEncapChanObj = NULL;

    /*check chipid and port*/
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(pAtHdlcLink);

    ret = DrvAtEncapChanObjGet(chipId, encapChanNo, &atEncapChanObj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ENCAP CHANNEL OBJ GET FAILED ! !!CHIP ID IS %u ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atEncapChanObj);
    *pAtHdlcLink = AtHdlcChannelHdlcLinkGet((AtHdlcChannel)atEncapChanObj);
    if(NULL == (*pAtHdlcLink))
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, HDLC LINK OBJ GET FAILED ! !!CHIP ID IS  %u ENCAP CHANNO IS %u \n", __FILE__, __LINE__,chipId,encapChanNo); 
        return DRV_AT_HDLC_LINK_GET_FAILED; 
    }

    return ret;
}


/*********************************************************************
* 函数名称：DrvAtEthOamFlowObjGet
* 功能描述：获取OAM FLOW
*           
* 访问的表：
* 修改的表：
* 输入参数:  
* 输出参数： 
* 返 回 值:       返回函数是否执行成功，成功返回DRV_POS_OK，错误则返回错误码
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtEthOamFlowObjGet(BYTE chipId,WORD32 encapChanNo,AtEthFlow* ethOamFlow)
{
    WORD32 ret = DRV_AT_SUCCESS;
    eAtRet atRet = cAtOk;
    AtHdlcLink atHdlcLinkObj = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(ethOamFlow);
    
    ret = DrvAtHdlcLinkObjGet(chipId,encapChanNo,&atHdlcLinkObj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, HDLC LINK OBJ GET FAILED ! !!CHIP ID IS %u ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atHdlcLinkObj);
    atRet = AtHdlcLinkOamPacketModeSet(atHdlcLinkObj,cAtHdlcLinkOamModeToPsn);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, AtHdlcLinkOamPacketModeSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_HDLC_LINK_OAM_MODE_SET_FAILED;
    } 
    
    *ethOamFlow = AtHdlcLinkOamFlowGet(atHdlcLinkObj);
    if(NULL == (*ethOamFlow))
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, ETH OAM FLOW OBJ GET FAILED ! !!CHIP ID IS  %u ENCAP CHANNO IS %u \n", __FILE__, __LINE__,chipId,encapChanNo); 
        return DRV_AT_HDLC_LINK_ETH_FLOW_OAM_GET_FAILED; 
    }
    
    return ret;
}

/**************************************************************************
* 函数名称: DrvAtHdlcFcsModeSet
* 功能描述: 设置fcs
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.

* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 该函数用来进行shutdown操作.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建 
**************************************************************************/
WORD32 DrvAtHdlcFcsModeSet(BYTE chipId, WORD32 encapChanNo, WORD32 fcsMode)
{
    WORD32 ret = DRV_AT_SUCCESS;
    AtEncapChannel atEncapChanObj = NULL;
    eAtRet atRet = cAtOk;
    eAtHdlcFcsMode atFcsMode = cAtHdlcFcsModeNoFcs;
    
    CHECK_AT_CHIP_ID(chipId);

    /*获取hdlc channel*/
    ret = DrvAtEncapChanObjGet(chipId, encapChanNo, &atEncapChanObj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP CHANNEL OBJ GET FAILED ! !!CHIP ID IS %u ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atEncapChanObj);
    
    
    if(AT_HDLC_FCS16 == fcsMode)
    {
        atFcsMode = cAtHdlcFcsModeFcs16;
    }
    else if(AT_HDLC_FCS32 == fcsMode)
    {
        atFcsMode = cAtHdlcFcsModeFcs32;
    }
    else if(AT_HDLC_NOFCS == fcsMode)
    {
        atFcsMode = cAtHdlcFcsModeNoFcs;
    }
    else
    {
        return DRV_AT_INVALID_OPTION ;;
    }
    
    atRet = AtHdlcChannelFcsModeSet((AtHdlcChannel)atEncapChanObj, atFcsMode);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtHdlcChannelFcsModeSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_HDLC_CHAN_FCS_SET_FAILED;
    } 
    
    return ret;
}


/**************************************************************************
* 函数名称: DrvAtHdlcTxScrambleSet
* 功能描述: 设置发送方向的hdlc帧是否扰码.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建 
**************************************************************************/
WORD32 DrvAtHdlcTxScrambleSet(BYTE chipId, WORD32 encapChanNo, BYTE isEnable)
{
    WORD32 ret = DRV_AT_SUCCESS;
    eBool atIsEnable = cAtFalse;
    AtEncapChannel atEncapChanObj = NULL;
    eAtRet atRet = cAtOk;

    CHECK_AT_CHIP_ID(chipId);
    
    /*获取hdlc channel*/
    ret = DrvAtEncapChanObjGet(chipId, encapChanNo, &atEncapChanObj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP CHANNEL OBJ GET FAILED ! !!CHIP ID IS %u ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atEncapChanObj);
    
    if (CE1_FALSE == isEnable)
    {
        atIsEnable = cAtFalse;
    }
    else if (CE1_TURE == isEnable)
    {
        atIsEnable = cAtTrue;
    }
  
    /* Enable/disable scramble */
    atRet = AtHdlcChannelScrambleEnable((AtHdlcChannel)atEncapChanObj, atIsEnable);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtHdlcChannelScrambleEnable() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_HDLC_CHAN_SCRAMBLE_SET_FAILED;
    } 
    
    return ret;
}

/**************************************************************************
* 函数名称: DrvAtVlanTagSet
* 功能描述: 设置vlan tag
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: 单板的子槽位号,取值为1~4.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建 
**************************************************************************/
WORD32 DrvAtVlanTagSet(BYTE chipId,  DRV_AT_PPP_VLAN_TAG1_INFO* vlanTag1,DRV_AT_PPP_VLAN_TAG2_INFO* vlanTag2,tAtZtePtnTag1* atVlan1Tag, tAtZtePtnTag2* atVlan2Tag)
{
    WORD32 ret = DRV_AT_SUCCESS;
    
    /*check chipid*/
    CHECK_AT_CHIP_ID(chipId);

    /*check input pointer*/
    CHECK_AT_POINTER_NULL(vlanTag1);
    CHECK_AT_POINTER_NULL(vlanTag2);
    CHECK_AT_POINTER_NULL(atVlan1Tag);
    CHECK_AT_POINTER_NULL(atVlan2Tag); 

    memset(atVlan1Tag, 0, sizeof(tAtZtePtnTag1));
    memset(atVlan2Tag, 0, sizeof(tAtZtePtnTag2));

    /* TODO: This following code just for test current FPGA, will be removed later */
    atVlan1Tag->cfi = vlanTag1->cfi;
    atVlan1Tag->cpuPktIndicator = vlanTag1->cpu;
    atVlan1Tag->encapType = vlanTag1->encapType;
    atVlan1Tag->priority = vlanTag1->pri;
    atVlan1Tag->packetLength = vlanTag1->pktLen;

    atVlan2Tag->cfi = vlanTag2->cfi;
    atVlan2Tag->isMlpppIma = vlanTag2->isMlppp;
    atVlan2Tag->priority = vlanTag2->pri;
    atVlan2Tag->stmPortId = vlanTag2->portNo;
    atVlan2Tag->zteChannelId = vlanTag2->chanNo;

    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtSavePppCfg
* 功能描述:   保存ppp配置
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期     版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14    v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtSavePppCfg (BYTE chipId, WORD32 pppLinkNo, DRV_AT_PPP_LINK_INFO *pppInfo)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    DRV_AT_PPP_LINK_INFO * glbPppInfo = NULL ;

    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_PPPLINK_NO(pppLinkNo);
    CHECK_AT_POINTER_NULL(pppInfo);   
    
    glbPppInfo = &(g_atPppLinkInfo[chipId][pppLinkNo]); /*参数在业务函数入口处已做过检查*/
    CHECK_AT_POINTER_NULL(glbPppInfo);
    memcpy(glbPppInfo, pppInfo, sizeof(DRV_AT_PPP_LINK_INFO));
    
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtSaveMlpppCfg
* 功能描述:   保存mlppp配置
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期     版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14    v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtSaveMlpppCfg (BYTE chipId, WORD32 bundleIndex, DRV_AT_MLPPP_BUNDLE_INFO* mlpppInfo)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    DRV_AT_MLPPP_BUNDLE_INFO * glbMlpppInfo = NULL ;

    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_MLPPP_BUDNDLE_INDEX(bundleIndex);
    CHECK_AT_POINTER_NULL(mlpppInfo);    
    
    glbMlpppInfo = &(g_atMlpppBundleInfo[chipId][bundleIndex]); /*参数在业务函数入口处已做过检查*/
    CHECK_AT_POINTER_NULL(glbMlpppInfo);
    memcpy(glbMlpppInfo, mlpppInfo, sizeof(DRV_AT_MLPPP_BUNDLE_INFO));
    
    return ret ;
}


/*********************************************************************
* 函数名称：DrvAtEncapChanCreate
* 功能描述：pos板打通一整条ppplink
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3        
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期      版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtEncapChanCreate(BYTE chipId, WORD32* encapChanNo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    AtDevice atDevHdl = NULL;
    AtModuleEncap atEncapModule = NULL;
    AtHdlcChannel atHdlcChannel = NULL;

    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(encapChanNo);
    *encapChanNo = 0;
    atDevHdl = DrvAtDevHdlGet(chipId);
    CHECK_AT_POINTER_NULL(atDevHdl);
    
     /*get encap module */
    atEncapModule = (AtModuleEncap)AtDeviceModuleGet(atDevHdl, cAtModuleEncap);     
    if(NULL == atEncapModule)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP MODULE GET FAILED ! !!CHIP ID IS %u \n", __FILE__, __LINE__,chipId); 
        return DRV_AT_ENCAP_MODULE_GET_FAILED; 
    }
    /*get a free encap chanNo*/
    *encapChanNo = AtModuleEncapFreeChannelGet(atEncapModule);    

    /*创建hdlc通道*/
    atHdlcChannel = AtModuleEncapHdlcPppChannelCreate(atEncapModule, *encapChanNo);
    if(NULL == atHdlcChannel)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP CHANNO CREATE FAILED ! !!CHIP ID IS %u ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,*encapChanNo); 
        return DRV_AT_ENCAP_CHAN_OBJ_CREATE_FAILED; 
    }

    return ret;
}



/*********************************************************************
* 函数名称：DrvAtEncapChanDelete
* 功能描述：pos板删除一整条ppplink
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtEncapChanDelete(BYTE chipId, WORD32 encapChanNo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    AtDevice atDevHdl = NULL;
    AtModuleEncap atEncapModule = NULL;
    eAtRet atRet = cAtOk;

    CHECK_AT_CHIP_ID(chipId);
    
    atDevHdl = DrvAtDevHdlGet(chipId);
    CHECK_AT_POINTER_NULL(atDevHdl);

    atEncapModule = (AtModuleEncap)AtDeviceModuleGet(atDevHdl, cAtModuleEncap);      /*get encap module */
    if(NULL == atEncapModule)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP MODULE GET FAILED ! !!CHIP ID IS %u \n", __FILE__, __LINE__,chipId); 
        return DRV_AT_ENCAP_MODULE_GET_FAILED; 
    }

    atRet = AtModuleEncapChannelDelete(atEncapModule, encapChanNo);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtModuleEncapChannelDelete() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_ENCAP_CHAN_OBJ_DELETE_FAILED;
    } 

    return ret;
}


/*********************************************************************
* 函数名称：DrvAtEncapChanPhyBind
* 功能描述：将encapChan 和phy绑定
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtEncapChanPhyBind(BYTE chipId,WORD32 encapChanNo,WORD32 e1LinkNo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 atRet = cAtOk;
    AtEncapChannel atEncapChanObj = NULL;
    AtPdhDe1 atE1Obj = NULL ; 
        
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    
    ret = DrvAtEncapChanObjGet(chipId, encapChanNo, &atEncapChanObj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP CHANNEL OBJ GET FAILED ! !!CHIP ID IS %u ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atEncapChanObj);
    
    ret = DrvAtE1ObjGet(chipId,e1LinkNo,&atE1Obj); 
    if(DRV_AT_SUCCESS != ret) 
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, E1 OBJ GET FAILED !!\n", __FILE__, __LINE__);   
        return ret;  
    } 
    CHECK_AT_POINTER_NULL(atE1Obj);
    
    atRet = AtEncapChannelPhyBind(atEncapChanObj,(AtChannel)atE1Obj);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtEncapChannelPhyBind() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_ENCAP_CHAN_PHY_BIND_FAILED;
    } 
    
    return ret;
}

/*********************************************************************
* 函数名称：DrvAtEncapChanPhyUnbind
* 功能描述：将encapChan 和phy解绑定
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtEncapChanPhyUnbind(BYTE chipId,WORD32 encapChanNo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 atRet = cAtOk;
    AtEncapChannel atEncapChanObj = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    
    ret = DrvAtEncapChanObjGet(chipId, encapChanNo, &atEncapChanObj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP CHANNEL OBJ GET FAILED ! !!CHIP ID IS %u ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atEncapChanObj);
    
    atRet = AtEncapChannelPhyBind(atEncapChanObj,NULL);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtEncapChannelPhyBind() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_ENCAP_CHAN_PHY_UNBIND_FAILED;
    } 
    
    return ret;
}


/*********************************************************************
* 函数名称：DrvAtEncapChanCfg
* 功能描述：配置encapChan 
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtEncapChanCfg(BYTE chipId,WORD32 encapChanNo)
{
    WORD32 ret = DRV_AT_SUCCESS;

    /*设置fcs模式*/
    ret = DrvAtHdlcFcsModeSet(chipId,encapChanNo,AT_HDLC_FCS16);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, HDLC FCS MODE SET FAILED ! !!CHIP ID IS %u ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
        return ret; 
    }
    /*设置扰码使能*/
    ret = DrvAtHdlcTxScrambleSet(chipId,encapChanNo,CE1_DISABLE);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, HDLC TX SCRAMBLE SET FAILED ! !!CHIP ID IS %u ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
        return ret; 
    }
    
    return ret;
}

/*********************************************************************
* 函数名称：DrvAtHdlcLinkTrafficEnable
* 功能描述：将ppp link traffic enable
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtHdlcLinkTrafficEnable(BYTE chipId,WORD32 encapChanNo,BYTE isEnable)
{
    WORD32 ret = DRV_AT_SUCCESS;
    AtHdlcLink atHdlcLinkObj = NULL;
    eBool atIsEnable = cAtFalse;
    eAtRet atRet = cAtOk;
    
    ret = DrvAtHdlcLinkObjGet(chipId,encapChanNo,&atHdlcLinkObj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, HDLC LINK OBJ GET FAILED ! !!CHIP ID IS %u ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atHdlcLinkObj);
    if(CE1_TURE == isEnable)
    {
        atIsEnable = cAtTrue;
    }
    else if(CE1_FALSE == isEnable)
    {
        atIsEnable = cAtFalse;
    }
    else
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, INVALID OPTION %u! !!\n",__FILE__, __LINE__,isEnable);
        return DRV_AT_INVALID_OPTION;
    }
    
    atRet = AtHdlcLinkTxTrafficEnable(atHdlcLinkObj,atIsEnable);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtHdlcLinkTxTrafficEnable() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_HDLC_LINK_TRAFFIC_ENABLE_FAILED;
    } 
    atRet = AtHdlcLinkRxTrafficEnable(atHdlcLinkObj,atIsEnable);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtHdlcLinkRxTrafficEnable() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_HDLC_LINK_TRAFFIC_ENABLE_FAILED;
    } 
    
    return ret;
}


/*********************************************************************
* 函数名称：DrvAtEthFlowCreate
* 功能描述：pos板打通一整条ppplink
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtEthFlowCreate(BYTE chipId, WORD32* flowId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    AtModuleEth atEthModule = NULL;
    AtEthFlow atEthFlow = NULL;
    AtDevice atDevHdl = NULL;

    /*check input parameter*/
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(flowId);
    *flowId = 0;
    atDevHdl = DrvAtDevHdlGet(chipId);
    CHECK_AT_POINTER_NULL(atDevHdl);
    atEthModule = (AtModuleEth)AtDeviceModuleGet(atDevHdl, cAtModuleEth);      /*get encap module */
    if(NULL == atEthModule)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ETH MODULE GET FAILED ! !!CHIP ID IS %u \n", __FILE__, __LINE__,chipId); 
        return DRV_AT_ETH_MOUDULE_GET_FAILED; 
    }

    *flowId = AtModuleEthFreeFlowIdGet(atEthModule);
    atEthFlow = AtModuleEthEopFlowCreate(atEthModule, *flowId);
    if(NULL == atEthFlow)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ETH FLOW CREATE FAILED ! !!CHIP ID IS %u FLOW ID IS %u\n", __FILE__, __LINE__,chipId,*flowId); 
        return DRV_AT_ETH_FLOW_CREATE_FAILED; 
    }

    return ret;
}


/*********************************************************************
* 函数名称：DrvAtEthFlowDelete
* 功能描述：删除eth flow 
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtEthFlowDelete(BYTE chipId, WORD32 flowId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    AtDevice atDevHdl = NULL;
    AtModuleEth atEthModule = NULL;
    eAtRet atRet = cAtOk;

    CHECK_AT_CHIP_ID(chipId);
    
    atDevHdl = DrvAtDevHdlGet(chipId);
    CHECK_AT_POINTER_NULL(atDevHdl);
    atEthModule = (AtModuleEth)AtDeviceModuleGet(atDevHdl, cAtModuleEth);      /*get encap module */
    if(NULL == atEthModule)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ETH MODULE GET FAILED ! !!CHIP ID IS %u \n", __FILE__, __LINE__,chipId); 
        return DRV_AT_ETH_MOUDULE_GET_FAILED; 
    }

    atRet = AtModuleEthFlowDelete(atEthModule, flowId);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtModuleEthFlowDelete() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_ETH_FLOW_DELETE_FAILD;
    } 

    return ret;
}

/*********************************************************************
* 函数名称：DrvAtEthFlowVlanSet
* 功能描述：设置以太流的vlan和其他特性
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtEthFlowVlanSet(BYTE chipId, WORD32 flowId,DRV_AT_PPP_VLAN_TAG1_INFO* vlanTag1,DRV_AT_PPP_VLAN_TAG2_INFO* vlanTag2)
{
    WORD32 ret = DRV_AT_SUCCESS;
    AtEthFlow atEthFlow = NULL;
    tAtZtePtnTag1 atVlanTag1;
    tAtZtePtnTag2 atVlanTag2;
    eAtRet atRet = cAtOk;
    BYTE ethPortId = 0;
    BYTE dMac[6] = {0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA};
    
    /*check input parameter*/
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(vlanTag1);
    CHECK_AT_POINTER_NULL(vlanTag2);
    memset(&atVlanTag1, 0, sizeof(atVlanTag1));
    memset(&atVlanTag2, 0, sizeof(atVlanTag2));

    /*获取eth Flow*/
    ret = DrvAtEthFlowObjGet(chipId, flowId, &atEthFlow);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ETH FLOW OBJ GET FAILED ! !!CHIP ID IS %u FLOW ID IS %u\n", __FILE__, __LINE__,chipId,flowId); 
        return ret; 
    }

    /*设置FlowEgress方向属性*/
    atRet = AtEthFlowEgressDestMacSet(atEthFlow, dMac);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtEthFlowEgressDestMacSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_ETH_DMAC_SET_ERROR;
    } 

    /*生成tAtEthVlanDesc*/
    ret = DrvAtVlanTagSet(chipId, vlanTag1,vlanTag2,&atVlanTag1, &atVlanTag2);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, VLAN TAG SET FAILED ! !!CHIP ID IS %u \n", __FILE__, __LINE__,chipId); 
        return ret; 
    }

    /*vlan ingress add*/
    atRet = AtZteEthFlowExpectedHeaderSet(atEthFlow, ethPortId, &atVlanTag1, &atVlanTag2);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtZteEthFlowExpectedHeaderSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_ETH_FLOW_EXP_HEADER_SET_FAILD;
    } 
       
    /*vlan egress add*/
    atRet = AtZteEthFlowTxHeaderSet(atEthFlow, ethPortId, &atVlanTag1, &atVlanTag2);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtZteEthFlowTxHeaderSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_ETH_FLOW_TX_HEADER_FAILD;
    } 


    return ret;
}

/*********************************************************************
* 函数名称：DrvAtEthFlowVlanSet
* 功能描述：设置以太流的vlan和其他特性
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtEthOamFlowVlanSet(BYTE chipId, WORD32 encapChanNo,DRV_AT_PPP_VLAN_TAG1_INFO* vlanTag1,DRV_AT_PPP_VLAN_TAG2_INFO* vlanTag2)
{
    WORD32 ret = DRV_AT_SUCCESS;
    AtEthFlow atEthOamFlow = NULL;
    tAtZtePtnTag1 atVlanTag1;
    tAtZtePtnTag2 atVlanTag2;
    eAtRet atRet = cAtOk;
    BYTE ethPortId = 0;
    BYTE dMac[6] = {0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA};
    
    /*check input parameter*/
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(vlanTag1);
    CHECK_AT_POINTER_NULL(vlanTag2);
    memset(&atVlanTag1, 0, sizeof(atVlanTag1));
    memset(&atVlanTag2, 0, sizeof(atVlanTag2));

    /*获取eth Flow*/
    ret = DrvAtEthOamFlowObjGet(chipId,encapChanNo,&atEthOamFlow);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ETH OAM FLOW OBJ GET FAILED ! !!CHIP ID IS %u ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atEthOamFlow);

    /*设置FlowEgress方向属性*/
    atRet = AtEthFlowEgressDestMacSet(atEthOamFlow, dMac);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtEthFlowEgressDestMacSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_ETH_DMAC_SET_ERROR;
    } 

    /*生成tAtEthVlanDesc*/
    ret = DrvAtVlanTagSet(chipId, vlanTag1,vlanTag2,&atVlanTag1, &atVlanTag2);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, VLAN TAG SET FAILED ! !!CHIP ID IS %u \n", __FILE__, __LINE__,chipId); 
        return ret; 
    }
       
    /*vlan egress add*/
    atRet = AtZteEthFlowTxHeaderSet(atEthOamFlow, ethPortId, &atVlanTag1, &atVlanTag2);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtZteEthFlowTxHeaderSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_ETH_FLOW_TX_HEADER_FAILD;
    } 

    return ret;
}


/*********************************************************************
* 函数名称：DrvAtHdlcLinkEthFlowBind
* 功能描述：hdlc link与eth flow 绑定 
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtHdlcLinkEthFlowBind(BYTE chipId,WORD32 encapChanNo,WORD32 flowId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    eAtRet atRet = cAtOk;
    AtHdlcLink atHdlcLinkObj = NULL;
    AtEthFlow atEthFlow = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    ret = DrvAtHdlcLinkObjGet(chipId,encapChanNo,&atHdlcLinkObj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, HDLC LINK OBJ GET FAILED ! !!CHIP ID IS %u ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atHdlcLinkObj);
    /*获取eth Flow*/
    ret = DrvAtEthFlowObjGet(chipId, flowId, &atEthFlow);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ETH FLOW OBJ GET FAILED ! !!CHIP ID IS %u FLOW ID IS %u\n", __FILE__, __LINE__,chipId,flowId); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atEthFlow);
    atRet = AtHdlcLinkFlowBind(atHdlcLinkObj,atEthFlow);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtHdlcLinkFlowBind() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_HDLC_LINK_ETH_FLOW_BIND_FAILED;
    } 

    return ret ;
}

/*********************************************************************
* 函数名称：DrvAtHdlcLinkEthFlowUnbind
* 功能描述：hdlc link 与eth flow解绑
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtHdlcLinkEthFlowUnbind(BYTE chipId,WORD32 encapChanNo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    eAtRet atRet = cAtOk;
    AtHdlcLink atHdlcLinkObj = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    ret = DrvAtHdlcLinkObjGet(chipId,encapChanNo,&atHdlcLinkObj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, HDLC LINK OBJ GET FAILED ! !!CHIP ID IS %u ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atHdlcLinkObj);
    atRet = AtHdlcLinkFlowBind(atHdlcLinkObj, NULL);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtHdlcLinkFlowBind() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_HDLC_LINK_ETH_FLOW_BIND_FAILED;
    } 
    
    return ret;
}


/*********************************************************************
* 函数名称：DrvAtPppLinkPhaseSet
* 功能描述：配置ppp的phase
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtPppLinkPhaseSet(BYTE chipId,WORD32 encapChanNo,WORD32 phaseMode)
{
    WORD32 ret = DRV_AT_SUCCESS;
    AtHdlcLink atHdlcLinkObj = NULL;
    eAtRet atRet = cAtOk;
    WORD32 atPhaseMode = 0;
    
    ret = DrvAtHdlcLinkObjGet(chipId,encapChanNo,&atHdlcLinkObj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, HDLC LINK OBJ GET FAILED ! !!CHIP ID IS %u ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atHdlcLinkObj);

    switch(phaseMode)
    {
        case AT_PPP_PHASE_UNKNOWN:
            atPhaseMode = cAtPppLinkPhaseUnknown; 
            break;
        case AT_PPP_PHASE_DEAD:
            atPhaseMode = cAtPppLinkPhaseDead;  
            break;
        case AT_PPP_PHASE_ESTABLISH:
            atPhaseMode = cAtPppLinkPhaseEstablish; 
            break;
        case AT_PPP_PHASE_AUTHEN:
            atPhaseMode = cAtPppLinkPhaseAuthenticate;
            break;
        case AT_PPP_PHASE_ENTER_NET:
            atPhaseMode = cAtPppLinkPhaseEnterNetwork;
            break;
        case AT_PPP_PHASE_ACTIVE_NET:
            atPhaseMode = cAtPppLinkPhaseNetworkActive;
            break;
        default:
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, INVALID PHASE MODE %u!!\n", __FILE__, __LINE__, phaseMode);
            return DRV_AT_INVALID_OPTION;
    }

    atRet = AtPppLinkPhaseSet((AtPppLink)atHdlcLinkObj,atPhaseMode);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtPppLinkPhaseSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_PPP_LINK_PHASE_SET_FAILED;
    } 
    
    return ret;
}

/*********************************************************************
* 函数名称：DrvAtPppLinkCreate
* 功能描述：创建一个ppp link
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtPppLinkCreate(DRV_AT_PPP_LINK_INFO* pppLinkInfo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE chipId = 0;
    WORD32 e1LinkNo = 0;
    WORD32 encapChanNo = 0;
    WORD32 flowId = 0;
    WORD32 pppLinkNo = 0;
    DRV_AT_PPP_VLAN_TAG1_INFO vlanTag1;
    DRV_AT_PPP_VLAN_TAG2_INFO vlanTag2;

    CHECK_AT_POINTER_NULL(pppLinkInfo);
    
    chipId = pppLinkInfo->chipId;
    e1LinkNo = pppLinkInfo->e1LinkNo;
    memset(&vlanTag1,0,sizeof(vlanTag1));
    memset(&vlanTag2,0,sizeof(vlanTag2));
    memcpy(&vlanTag1,&(pppLinkInfo->vlanTag1),sizeof(vlanTag1));
    memcpy(&vlanTag2,&(pppLinkInfo->vlanTag2),sizeof(vlanTag2));

    /*检查cip是否已被创建*/
    pppLinkNo = DrvAtPppLinkNoGetFromCip(chipId,pppLinkInfo->cip);                    
    if(INVALID_PPP_LINKNO != pppLinkNo)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, The Cip is existed !!CHIP ID IS % u CIP IS %u\n", __FILE__, __LINE__,chipId, pppLinkInfo->cip);
        return DRV_AT_PPP_LINK_ALREADY_EXISTED;
    }
    pppLinkNo = 0;
    
    /*根据e1号查看该条e1有无创建过ppp*/
    ret = DrvAtPppLinkNoGetFromE1(chipId,e1LinkNo,&pppLinkNo);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, DrvAtPppLinkNoGetFromE1() FAILED ! !!CHIP ID IS %u ,E1 LINKNO IS %u\n", __FILE__, __LINE__,chipId,e1LinkNo); 
        return ret; 
    }
    /*如果已经创建过，返回失败*/
    if(INVALID_PPP_LINKNO != pppLinkNo)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, THIS E1 HAS ALREADY BIND A PPP LINK ! !!CHIP ID IS %u ,E1 LINKNO IS %u, PPP LINKNO IS %u \n", __FILE__, __LINE__,chipId,e1LinkNo,pppLinkNo); 
        return DRV_AT_PPP_LINK_ALREADY_EXISTED; 
    }
    /*获取一个空闲的ppplinkNo*/
    ret = DrvAtIdlePppLinkNoGet(chipId,&pppLinkNo);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, IDLE PPP LINKNO GET FAILED ! !!CHIP ID IS %u ,PPP LINKNO IS %u\n", __FILE__, __LINE__,chipId,pppLinkNo); 
        return ret; 
    }    
    DRV_AT_E1_PRINT(DRV_AT_PPP_CFG,"[CE1TAN][PPP ADD]:%s line %d , cip is %u, ppp linkNo is %u\n",__FILE__,__LINE__,pppLinkInfo->cip,pppLinkNo);
    /*创建encap chan*/
    ret = DrvAtEncapChanCreate(chipId,&encapChanNo);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP CHAN CREATE FAILED ! !!CHIP ID IS %u ,ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
        return ret; 
    }
    /*配置encap chan*/
    ret = DrvAtEncapChanCfg(chipId,encapChanNo);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP CHAN CFG FAILED ! !!CHIP ID IS %u ,ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
        /*回退，将encapchan删除*/
        ret = DrvAtEncapChanDelete(chipId,encapChanNo);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP CHAN DELETE FAILED ! !!CHIP ID IS %u ,ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
            return ret; 
        }
        return ret; 
    }
    /*将encap chan和e1绑定*/
    ret = DrvAtEncapChanPhyBind(chipId,encapChanNo,e1LinkNo);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP CHAN BIND PHY FAILED ! !!CHIP ID IS %u ,ENCAP CHANNO IS %u,E1 LINKNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo,e1LinkNo); 
        /*回退，将encapchan删除*/
        ret = DrvAtEncapChanDelete(chipId,encapChanNo);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP CHAN DELETE FAILED ! !!CHIP ID IS %u ,ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
            return ret; 
        }   
        return ret; 
    }
    /*创建oam flow，并配置vlan*/
    ret = DrvAtEthOamFlowVlanSet(chipId,encapChanNo,&vlanTag1,&vlanTag2);    
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ETH OAM FLOW VLAN SET FAILED ! !!CHIP ID IS %u ,ENCAP CHANNO IS IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
        /*回退，与phy解绑*/
        ret = DrvAtEncapChanPhyUnbind(chipId,encapChanNo);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP CHAN UNBIND PHY FAILED ! !!CHIP ID IS %u ,ENCAP CHANNO IS %u \n", __FILE__, __LINE__,chipId,encapChanNo); 
            return ret; 
        }
        /*回退，将encapchan删除*/
        ret = DrvAtEncapChanDelete(chipId,encapChanNo);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP CHAN DELETE FAILED ! !!CHIP ID IS %u ,ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
            return ret; 
        }  
        return ret; 
    }
    /*创建一个eth flow*/
    ret = DrvAtEthFlowCreate(chipId,&flowId);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ETH FLOW CREATE FAILED ! !!CHIP ID IS %u ,FLOW ID IS %u\n", __FILE__, __LINE__,chipId,flowId); 
        return ret; 
    }
    /*给eth flow 设置vlan*/
    ret = DrvAtEthFlowVlanSet(chipId,flowId,&vlanTag1,&vlanTag2);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ETH FLOW VLAN SET FAILED ! !!CHIP ID IS %u ,FLOW ID IS IS %u\n", __FILE__, __LINE__,chipId,flowId);
        /*回退，与phy解绑*/
        ret = DrvAtEncapChanPhyUnbind(chipId,encapChanNo);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP CHAN UNBIND PHY FAILED ! !!CHIP ID IS %u ,ENCAP CHANNO IS %u \n", __FILE__, __LINE__,chipId,encapChanNo); 
            return ret; 
        }
        /*回退，将encapchan删除*/
        ret = DrvAtEncapChanDelete(chipId,encapChanNo);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP CHAN DELETE FAILED ! !!CHIP ID IS %u ,ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
            return ret; 
        } 
        /*回退，将flow删除*/
        ret = DrvAtEthFlowDelete(chipId,flowId);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ETH FLOW DELETE FAILED ! !!CHIP ID IS %u ,FLOW ID IS %u\n", __FILE__, __LINE__,chipId,flowId); 
            return ret; 
        }
        return ret; 
    }
    /*将hdlc link和eth flow 绑定*/
    ret = DrvAtHdlcLinkEthFlowBind(chipId,encapChanNo,flowId);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, HDLC LINK BIND ETH FLOW FAILED ! !!CHIP ID IS %u, ENCAP CHANNO IS %u,FLOW ID IS %u\n", __FILE__, __LINE__,chipId,encapChanNo,flowId); 
        /*回退，与phy解绑*/
        ret = DrvAtEncapChanPhyUnbind(chipId,encapChanNo);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP CHAN UNBIND PHY FAILED ! !!CHIP ID IS %u ,ENCAP CHANNO IS %u \n", __FILE__, __LINE__,chipId,encapChanNo); 
            return ret; 
        }
        /*回退，将encapchan删除*/
        ret = DrvAtEncapChanDelete(chipId,encapChanNo);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP CHAN DELETE FAILED ! !!CHIP ID IS %u ,ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
            return ret; 
        } 
        /*回退，将flow删除*/
        ret = DrvAtEthFlowDelete(chipId,flowId);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ETH FLOW DELETE FAILED ! !!CHIP ID IS %u ,FLOW ID IS %u\n", __FILE__, __LINE__,chipId,flowId); 
            return ret; 
        }
        return ret; 
    }
    /*将hdlc link 流量使能*/
    ret = DrvAtHdlcLinkTrafficEnable(chipId,encapChanNo,CE1_TURE);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, HDLC LINK TRFFIC ENABLE FAILED ! !!CHIP ID IS %u ,ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo);
        /*回退，与phy解绑*/
        ret = DrvAtEncapChanPhyUnbind(chipId,encapChanNo);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP CHAN UNBIND PHY FAILED ! !!CHIP ID IS %u ,ENCAP CHANNO IS %u \n", __FILE__, __LINE__,chipId,encapChanNo); 
            return ret; 
        }
        /*回退，将encapchan删除*/
        ret = DrvAtEncapChanDelete(chipId,encapChanNo);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP CHAN DELETE FAILED ! !!CHIP ID IS %u ,ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
            return ret; 
        } 
        /*回退，与flow解绑*/
        ret = DrvAtHdlcLinkEthFlowUnbind(chipId,encapChanNo);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, HDLC LINK BIND ETH FLOW FAILED ! !!CHIP ID IS %u, ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
            return ret; 
        }
        /*回退，将flow删除*/
        ret = DrvAtEthFlowDelete(chipId,flowId);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ETH FLOW DELETE FAILED ! !!CHIP ID IS %u ,FLOW ID IS %u\n", __FILE__, __LINE__,chipId,flowId); 
            return ret; 
        }
        return ret; 
    }
    /*设置ppp link phase*/
    ret = DrvAtPppLinkPhaseSet(chipId,encapChanNo,AT_PPP_PHASE_ACTIVE_NET);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PPP LINK PHASE SET FAILED ! !!CHIP ID IS %u ,ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
        /*回退，与phy解绑*/
        ret = DrvAtEncapChanPhyUnbind(chipId,encapChanNo);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP CHAN UNBIND PHY FAILED ! !!CHIP ID IS %u ,ENCAP CHANNO IS %u \n", __FILE__, __LINE__,chipId,encapChanNo); 
            return ret; 
        }
        /*回退，将encapchan删除*/
        ret = DrvAtEncapChanDelete(chipId,encapChanNo);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP CHAN DELETE FAILED ! !!CHIP ID IS %u ,ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
            return ret; 
        } 
        /*回退，与flow解绑*/
        ret = DrvAtHdlcLinkEthFlowUnbind(chipId,encapChanNo);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, HDLC LINK BIND ETH FLOW FAILED ! !!CHIP ID IS %u, ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
            return ret; 
        }
        /*回退，将flow删除*/
        ret = DrvAtEthFlowDelete(chipId,flowId);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ETH FLOW DELETE FAILED ! !!CHIP ID IS %u ,FLOW ID IS %u\n", __FILE__, __LINE__,chipId,flowId); 
            return ret; 
        }        
        return ret; 
    }
    /*创建流程结束后，将link 信息保存到临时表项*/
    pppLinkInfo->encapChanNo = encapChanNo;
    pppLinkInfo->flowId = flowId;
    pppLinkInfo->pppLinkNo = pppLinkNo;
    pppLinkInfo->mlpppBundleIndex = INVALID_MLPPP_BUNDLE_INDEX;
    pppLinkInfo->status = PPP_LINK_USED;
    
    /*保存ppp link 信息到全局软件表*/
    ret = DrvAtSavePppCfg(chipId,pppLinkNo,pppLinkInfo);  
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, DrvAtSavePppCfg() ret=0x%x.\n", __FILE__, __LINE__, ret);
        return ret;
    }
    
    return ret;
}

/*********************************************************************
* 函数名称：DrvAtPppLinkDelete
* 功能描述：删除ppp link
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtPppLinkDelete(DRV_AT_PPP_LINK_INFO* pppLinkInfo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE chipId = 0;
    WORD32 encapChanNo = 0;
    WORD32 flowId = 0;
    WORD32 pppLinkNo = 0;
    
    CHECK_AT_POINTER_NULL(pppLinkInfo);
    chipId = pppLinkInfo->chipId;
    encapChanNo = pppLinkInfo->encapChanNo;
    flowId = pppLinkInfo->flowId;
    pppLinkNo = pppLinkInfo->pppLinkNo;
    
    /*将hdlc link 流量disable*/
    ret = DrvAtHdlcLinkTrafficEnable(chipId,encapChanNo,CE1_FALSE);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, HDLC LINK TRFFIC ENABLE FAILED ! !!CHIP ID IS %u ,ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
        return ret; 
    }
    /*设置ppplink状态为dead*/
    ret = DrvAtPppLinkPhaseSet(chipId,encapChanNo,AT_PPP_PHASE_DEAD);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PPP LINK PHASE SET FAILED ! !!CHIP ID IS %u ,ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
        return ret; 
    }
    /*与phy解绑*/
    ret = DrvAtEncapChanPhyUnbind(chipId,encapChanNo);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP CHAN UNBIND PHY FAILED ! !!CHIP ID IS %u ,ENCAP CHANNO IS %u \n", __FILE__, __LINE__,chipId,encapChanNo); 
        return ret; 
    }
    /*与flow解绑*/
    ret = DrvAtHdlcLinkEthFlowUnbind(chipId,encapChanNo);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, HDLC LINK BIND ETH FLOW FAILED ! !!CHIP ID IS %u, ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
        return ret; 
    }
    /*将encapchan删除*/
    ret = DrvAtEncapChanDelete(chipId,encapChanNo);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP CHAN DELETE FAILED ! !!CHIP ID IS %u ,ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
        return ret; 
    }
    /*将flow删除*/
    ret = DrvAtEthFlowDelete(chipId,flowId);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ETH FLOW DELETE FAILED ! !!CHIP ID IS %u ,FLOW ID IS %u\n", __FILE__, __LINE__,chipId,flowId); 
        return ret; 
    }
    /*将全局表清空*/
    memset(&g_atPppLinkInfo[chipId][pppLinkNo],0,sizeof(DRV_AT_PPP_LINK_INFO));
    
    return ret;
}

/*********************************************************************
* 函数名称：DrvAtPppLinkAdd
* 功能描述：添加一个ppp link
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtPppLinkAdd(BYTE chipId,WORD32 encapChanNo,WORD32 bundleIndex)
{
    WORD32 ret = DRV_AT_SUCCESS;
    eAtRet atRet = cAtOk;
    AtMpBundle mpBundle = NULL;
    AtHdlcLink atHdlcLinkObj = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_MLPPP_BUDNDLE_INDEX(bundleIndex);
    
    ret = DrvAtMlpppBundleGet(chipId,bundleIndex,&mpBundle);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, MLPPP BUNDLE  GET FAILED ! !!CHIP ID IS %u BUNDLE ID IS %u\n", __FILE__, __LINE__,chipId,bundleIndex); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(mpBundle);
    ret = DrvAtHdlcLinkObjGet(chipId,encapChanNo,&atHdlcLinkObj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, HDLC LINK OBJ GET FAILED ! !!CHIP ID IS %u ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atHdlcLinkObj);
    atRet = AtHdlcBundleLinkAdd((AtHdlcBundle)mpBundle,atHdlcLinkObj);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtHdlcBundleLinkAdd() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_MLPPP_BUNDLE_ADD_LINK_FAILED;
    } 
    
    return ret;
}


/*********************************************************************
* 函数名称：DrvAtPppLinkRemove
* 功能描述：移除一个ppp link
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtPppLinkRemove(BYTE chipId,WORD32 encapChanNo,WORD32 bundleIndex)
{
    WORD32 ret = DRV_AT_SUCCESS;
    eAtRet atRet = cAtOk;
    AtMpBundle mpBundle = NULL;
    AtHdlcLink atHdlcLinkObj = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_MLPPP_BUDNDLE_INDEX(bundleIndex);
    
    ret = DrvAtMlpppBundleGet(chipId,bundleIndex,&mpBundle);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, MLPPP BUNDLE  GET FAILED ! !!CHIP ID IS %u BUNDLE ID IS %u\n", __FILE__, __LINE__,chipId,bundleIndex); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(mpBundle);
    ret = DrvAtHdlcLinkObjGet(chipId,encapChanNo,&atHdlcLinkObj);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, HDLC LINK OBJ GET FAILED ! !!CHIP ID IS %u ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atHdlcLinkObj);
    atRet = AtHdlcBundleLinkRemove((AtHdlcBundle)mpBundle,atHdlcLinkObj);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtHdlcBundleLinkRemove() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_MLPPP_BUNDLE_REMOVE_LINK_FAILED;
    } 
    
    return ret;
}

/*********************************************************************
* 函数名称：DrvAtMlpppBundleMrruSet
* 功能描述：配置mlppp mrru
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtMlpppBundleMrruSet(BYTE chipId,WORD32 bundleIndex,WORD32 mrru)
{
    WORD32 ret = DRV_AT_SUCCESS;
    eAtRet atRet = cAtOk;
    AtMpBundle mpBundle = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_MLPPP_BUDNDLE_INDEX(bundleIndex);
    
    ret = DrvAtMlpppBundleGet(chipId,bundleIndex,&mpBundle);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, MLPPP BUNDLE  GET FAILED ! !!CHIP ID IS %u BUNDLE ID IS %u\n", __FILE__, __LINE__,chipId,bundleIndex); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(mpBundle);
    
    atRet = AtMpBundleMrruSet(mpBundle,mrru);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtMpBundleMrruSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_MLPPP_BUNDLE_MRRU_SET_FAILED;
    } 
    
    return ret;
}

/*********************************************************************
* 函数名称：DrvAtMlpppSeqModeSet
* 功能描述：配置mlppp序列号模式
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtMlpppSeqModeSet(BYTE chipId,WORD32 bundleIndex,WORD32 seqMode)
{
    WORD32 ret = DRV_AT_SUCCESS;
    eAtRet atRet = cAtOk;
    AtMpBundle mpBundle = NULL;
    WORD32 atSeqMode = 0;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_MLPPP_BUDNDLE_INDEX(bundleIndex);

    ret = DrvAtMlpppBundleGet(chipId,bundleIndex,&mpBundle);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, MLPPP BUNDLE  GET FAILED ! !!CHIP ID IS %u BUNDLE ID IS %u\n", __FILE__, __LINE__,chipId,bundleIndex); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(mpBundle);

    if(AT_SEQ_MODE_UNKNOWN == seqMode)
    {
        atSeqMode = cAtMpSequenceModeUnknown;
    }
    else if(AT_SEQ_MODE_SHORT == seqMode)
    {
        atSeqMode = cAtMpSequenceModeShort;
    }
    else if(AT_SEQ_MODE_LONG == seqMode)
    {
        atSeqMode = cAtMpSequenceModeLong;
    }
    else
    {
       return DRV_AT_INVALID_OPTION ;
    }
    atRet = AtMpBundleSequenceModeSet(mpBundle,atSeqMode);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtMpBundleSequenceModeSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_MLPPP_BUNDLE_SEQMODE_SET_FAILED;
    } 
    
    return ret;
}

/*********************************************************************
* 函数名称：DrvAtMlpppBundleFragSizeSet
* 功能描述：配置mlppp分片模式
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtMlpppBundleFragSizeSet(BYTE chipId,WORD32 bundleIndex,WORD32 fragSizeMode)
{
    WORD32 ret = DRV_AT_SUCCESS;
    eAtRet atRet = cAtOk;
    WORD32 atFragSize = 0;
    AtMpBundle mpBundle = NULL;  
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_MLPPP_BUDNDLE_INDEX(bundleIndex);
    
    ret = DrvAtMlpppBundleGet(chipId,bundleIndex,&mpBundle);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, MLPPP BUNDLE GET FAILED ! !!CHIP ID IS %u BUNDLE ID IS %u\n", __FILE__, __LINE__,chipId,bundleIndex); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(mpBundle);
    
    if(AT_FRAG_SIZE_NONE == fragSizeMode)
    {
        atFragSize = cAtNoFragment;
    }
    else if(AT_FRAG_SIZE_64 == fragSizeMode)
    {
        atFragSize = cAtFragmentSize64;
    }
    else if(AT_FRAG_SIZE_128 == fragSizeMode)
    {
        atFragSize = cAtFragmentSize128;
    }
    else if(AT_FRAG_SIZE_256 == fragSizeMode)
    {
        atFragSize = cAtFragmentSize256;
    }
    else if(AT_FRAG_SIZE_512 == fragSizeMode)
    {
        atFragSize = cAtFragmentSize512;
    }
    else
    {
       return DRV_AT_INVALID_OPTION ;
    }

    atRet = AtHdlcBundleFragmentSizeSet((AtHdlcBundle)mpBundle,atFragSize);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtHdlcBundleFragmentSizeSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_MLPPP_BUNDLE_FRAG_SIZE_SET_FAILED;
    } 
    
    return ret;
}

/*********************************************************************
* 函数名称：DrvAtMlpppBundleCreate
* 功能描述：创建一个mlppp bundle
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtMlpppBundleCreate(DRV_AT_MLPPP_BUNDLE_INFO* mlpppInfo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE chipId = 0;
    WORD32 bundleId = 0;
    WORD32 seqMode = 0;
    WORD32 mrru = 0;
    WORD32 fragSize = 0;
    WORD32 bundleIndex = 0;
    eAtRet atRet = cAtOk;
    AtModulePpp pppModule = NULL;
    AtDevice atDevHdl = NULL;
    AtMpBundle bundle = NULL;

    CHECK_AT_POINTER_NULL(mlpppInfo);
    chipId = mlpppInfo->chipId;
    bundleId = mlpppInfo->bundleId;
    seqMode = mlpppInfo->seqMode;
    mrru = mlpppInfo->mrru;
    fragSize = mlpppInfo->fragsize;
        
    CHECK_AT_CHIP_ID(chipId);
    atDevHdl = DrvAtDevHdlGet(chipId);
    CHECK_AT_POINTER_NULL(atDevHdl);

    /*查找bundleId对应的bundle index*/
    ret = DrvAtMlpppIndexGetFromBundleId(chipId,bundleId,&bundleIndex);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, DrvAtMlpppIndexGetFromBundleId() FAILED ! !!CHIP ID IS %u ,MLPPP BUNDLE ID IS %u\n", __FILE__, __LINE__,chipId,bundleId); 
        return ret; 
    }
    /*如果已经创建过，返回失败*/
    if(INVALID_MLPPP_BUNDLE_INDEX != bundleIndex)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, MLPPP BUNDLE HAS AREADY EXISTED  ! !!CHIP ID IS %u ,MLPPP BUNDLE IS %u \n", __FILE__, __LINE__,chipId,bundleId); 
        return DRV_AT_MLPPP_BUNDLE_ALREADY_EXISTED; 
    }
    /*查找一个空闲的bundle index*/
    ret = DrvAtIdleMlpppBundleIndexGet(chipId,&bundleIndex);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, IDLE MLPPP BUNDLE INDEX GET FAILED ! !!CHIP ID IS %u ,MLPPP BUNDLE IS %u\n", __FILE__, __LINE__,chipId,bundleId); 
        return ret; 
    }  
    /*创建一个mlppp组*/
    pppModule = (AtModulePpp)AtDeviceModuleGet(atDevHdl, cAtModulePpp);
    if(NULL == pppModule)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PPP MODULE GET FAILED ! !!CHIP ID IS %u \n", __FILE__, __LINE__,chipId); 
        return DRV_AT_PPP_MODULE_GET_FAILED; 
    }
    bundle = AtModulePppMpBundleCreate(pppModule, bundleIndex); /* Use the first bundle */
    if(NULL == bundle)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, MLPPP BUNDLE CREATE FAILED ! !!CHIP ID IS %u BUNDLE ID IS %u\n", __FILE__, __LINE__,chipId,bundleIndex); 
        return DRV_AT_MLPPP_BUNDLE_CREATE_FAILED; 
    }
    /*配置mlppp组*/
    ret = DrvAtMlpppBundleMrruSet(chipId,bundleIndex,mrru);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, MLPPP BUNDLE MRRU SET FAILED ! !!CHIP ID IS %u ,MLPPP BUNDLE IS %u\n", __FILE__, __LINE__,chipId,bundleId); 
        /*回退，删除bundle*/
        ret = DrvAtMlpppBundleDelete(chipId,bundleId);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d,  MLPPP BUNDLE DELETE FAILED ! !!CHIP ID IS %u ,MLPPP BUNDLE IS %u\n", __FILE__, __LINE__,chipId,bundleId); 
            return ret; 
        }
        return ret; 
    }  
    ret = DrvAtMlpppSeqModeSet(chipId,bundleIndex,seqMode);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, MLPPP BUNDLE SEQ MODE SET FAILED ! !!CHIP ID IS %u ,MLPPP BUNDLE IS %u\n", __FILE__, __LINE__,chipId,bundleId); 
        /*回退，删除bundle*/
        ret = DrvAtMlpppBundleDelete(chipId,bundleId);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d,  MLPPP BUNDLE DELETE FAILED ! !!CHIP ID IS %u ,MLPPP BUNDLE IS %u\n", __FILE__, __LINE__,chipId,bundleId); 
            return ret; 
        }
        return ret; 
    }  
    ret = DrvAtMlpppBundleFragSizeSet(chipId,bundleIndex,fragSize);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, MLPPP BUNDLE SEQ MODE SET FAILED ! !!CHIP ID IS %u ,MLPPP BUNDLE IS %u\n", __FILE__, __LINE__,chipId,bundleId); 
        /*回退，删除bundle*/
        ret = DrvAtMlpppBundleDelete(chipId,bundleId);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d,  MLPPP BUNDLE DELETE FAILED ! !!CHIP ID IS %u ,MLPPP BUNDLE IS %u\n", __FILE__, __LINE__,chipId,bundleId); 
            return ret; 
        }
        return ret; 
    }  
    /*enable mlppp组*/
    atRet = AtChannelEnable((AtChannel)bundle, CE1_ENABLE);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtChannelEnable() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        /*回退，删除bundle*/
        ret = DrvAtMlpppBundleDelete(chipId,bundleId);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d,  MLPPP BUNDLE DELETE FAILED ! !!CHIP ID IS %u ,MLPPP BUNDLE IS %u\n", __FILE__, __LINE__,chipId,bundleId); 
            return ret; 
        }
        return DRV_AT_MLPPP_BUNDLE_ACTIVE_FAILED;
    }  
    
    /*保存mlppp临时软件表*/
    mlpppInfo->flowId = INVALID_FLOW_ID;
    mlpppInfo->status = MLPPP_BUNDLE_USED;
      
    /*保存mlppp 信息到全局软件表*/
    ret = DrvAtSaveMlpppCfg(chipId,bundleIndex,mlpppInfo);  
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, DrvAtSaveMlpppCfg() ret=0x%x.\n", __FILE__, __LINE__, ret);
        return ret;
    }
    
    return ret;
}


/*********************************************************************
* 函数名称：DrvAtMlpppBundleDelete
* 功能描述：删除一个mlppp bundle
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtMlpppBundleDelete(BYTE chipId,WORD32 bundleId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 bundleIndex = 0;
    eAtRet atRet = cAtOk;
    AtModulePpp pppModule = NULL;
    AtDevice atDevHdl = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    atDevHdl = DrvAtDevHdlGet(chipId);
    CHECK_AT_POINTER_NULL(atDevHdl);

    /*查找bundleId对应的bundle index*/
    ret = DrvAtMlpppIndexGetFromBundleId(chipId,bundleId,&bundleIndex);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, DrvAtMlpppIndexGetFromBundleId() FAILED ! !!CHIP ID IS %u ,MLPPP BUNDLE ID IS %u\n", __FILE__, __LINE__,chipId,bundleId); 
        return ret; 
    }
    /*如果找不到mlppp组返回失败*/
    if(INVALID_MLPPP_BUNDLE_INDEX == bundleIndex)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, MLPPP BUNDLE IS NOT EXSITED ! !!CHIP ID IS %u ,MLPPP BUNDLE ID IS %u \n", __FILE__, __LINE__,chipId,bundleId); 
        return DRV_AT_MLPPP_BUNDLE_NOT_CREATE; 
    }
    pppModule = (AtModulePpp)AtDeviceModuleGet(atDevHdl, cAtModulePpp);
    if(NULL == pppModule)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PPP MODULE GET FAILED ! !!CHIP ID IS %u \n", __FILE__, __LINE__,chipId); 
        return DRV_AT_PPP_MODULE_GET_FAILED; 
    }
    atRet = AtModulePppMpBundleDelete(pppModule,bundleIndex);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtModulePppMpBundleDelete() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_MLPPP_BUNDLE_DELETE_FAILED;
    } 
    memset(&g_atMlpppBundleInfo[chipId][bundleIndex],0,sizeof(DRV_AT_MLPPP_BUNDLE_INFO));
    
    return ret;   
}

/*********************************************************************
* 函数名称：DrvAtMlpppBundleEthFlowBind
* 功能描述：将mlppp bundle和flow绑定
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtMlpppBundleEthFlowBind(BYTE chipId,WORD32 bundleIndex,WORD32 flowId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    eAtRet atRet = cAtOk;
    AtMpBundle mpBundle = NULL;
    AtEthFlow atEthFlow = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_MLPPP_BUDNDLE_INDEX(bundleIndex);
    
    /*获取MLPPP bundle*/
    ret = DrvAtMlpppBundleGet(chipId,bundleIndex,&mpBundle);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, MLPPP BUNDLE  GET FAILED ! !!CHIP ID IS %u BUNDLE ID IS %u\n", __FILE__, __LINE__,chipId,bundleIndex); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(mpBundle);
    /*获取eth Flow*/
    ret = DrvAtEthFlowObjGet(chipId, flowId, &atEthFlow);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ETH FLOW OBJ GET FAILED ! !!CHIP ID IS %u FLOW ID IS %u\n", __FILE__, __LINE__,chipId,flowId); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atEthFlow);
    atRet = AtMpBundleFlowAdd(mpBundle,atEthFlow,0);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtMpBundleFlowAdd() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_MLPPP_BUNDLE_ETH_FLOW_BIND_FAILED;
    } 
    
    return ret;
}


/*********************************************************************
* 函数名称：DrvAtMlpppBundleEthFlowUnbind
* 功能描述：将mlppp bundle和flow解绑定
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtMlpppBundleEthFlowUnbind(BYTE chipId,WORD32 bundleIndex,WORD32 flowId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    eAtRet atRet = cAtOk;
    AtMpBundle mpBundle = NULL;
    AtEthFlow atEthFlow = NULL;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_MLPPP_BUDNDLE_INDEX(bundleIndex);
    /*获取MLPPP bundle*/
    ret = DrvAtMlpppBundleGet(chipId,bundleIndex,&mpBundle);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, MLPPP BUNDLE  GET FAILED ! !!CHIP ID IS %u BUNDLE ID IS %u\n", __FILE__, __LINE__,chipId,bundleIndex); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(mpBundle);
    /*获取eth Flow*/
    ret = DrvAtEthFlowObjGet(chipId, flowId, &atEthFlow);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ETH FLOW OBJ GET FAILED ! !!CHIP ID IS %u FLOW ID IS %u\n", __FILE__, __LINE__,chipId,flowId); 
        return ret; 
    }
    CHECK_AT_POINTER_NULL(atEthFlow);
    atRet = AtMpBundleFlowRemove(mpBundle,atEthFlow);
    if (cAtOk != atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtMpBundleFlowRemove() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
        return DRV_AT_MLPPP_BUNDLE_ETH_FLOW_UNBIND_FAILED;
    } 
    
    return ret;
}

/*********************************************************************
* 函数名称：DrvAtMlpppBundleAddLink
* 功能描述：将一个ppp加入到mlppp组里
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtMlpppBundleAddLink(BYTE chipId,WORD32 bundleId,WORD32 e1LinkNo)
{
    WORD32  ret = DRV_AT_SUCCESS;
    WORD32 bundleIndex = 0;
    WORD32 pppLinkNo = 0;
    WORD32 flowId = 0;
    WORD32 encapChanNo = 0;
    WORD32 linksNum = 0;
    DRV_AT_PPP_LINK_INFO pppLinkInfo;
    DRV_AT_MLPPP_BUNDLE_INFO mlpppInfo;
    DRV_AT_PPP_VLAN_TAG1_INFO vlanTag1;
    DRV_AT_PPP_VLAN_TAG2_INFO vlanTag2;
  
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);

    memset(&pppLinkInfo,0,sizeof(pppLinkInfo));
    memset(&mlpppInfo,0,sizeof(mlpppInfo));
    memset(&vlanTag1,0,sizeof(vlanTag1));
    memset(&vlanTag2,0,sizeof(vlanTag2));
    
    /*查找bundleId对应的bundle index*/
    ret = DrvAtMlpppIndexGetFromBundleId(chipId,bundleId,&bundleIndex);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, DrvAtMlpppIndexGetFromBundleId() FAILED ! !!CHIP ID IS %u ,MLPPP BUNDLE ID IS %u\n", __FILE__, __LINE__,chipId,bundleId); 
        return ret; 
    }
    /*如果找不到mlppp组返回失败*/
    if(INVALID_MLPPP_BUNDLE_INDEX == bundleIndex)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, MLPPP BUNDLE IS NOT EXSITED ! !!CHIP ID IS %u ,MLPPP BUNDLE ID IS %u \n", __FILE__, __LINE__,chipId,bundleId); 
        return DRV_AT_MLPPP_BUNDLE_NOT_CREATE; 
    }
    /*通过e1获取encapChanno*/
    ret = DrvAtEncapChanNoGetFromE1(chipId,e1LinkNo,&encapChanNo);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, DrvAtEncapChanNoGetFromE1() FAILED ! !!CHIP ID IS %u E1 LINKNO IS %u\n", __FILE__, __LINE__,chipId,e1LinkNo); 
        return ret; 
    }
    /*如果找不到encapchan 返回失败*/
    if(INVALID_ENCAP_CHANNO == encapChanNo)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP CHAN IS NOT EXSITED ! !!CHIP ID IS %u ,E1LINK NO IS %u \n", __FILE__, __LINE__,chipId,e1LinkNo); 
        return DRV_AT_ENCAP_CHAN_NOT_CREATED; 
    }
    /*检查这条e1是否已经被加入这个组,如果已加入则返回失败*/
    ret = DrvAtPppLinkAddtoBundleCheck(chipId,bundleIndex,e1LinkNo);
    if(DRV_AT_SUCCESS == ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PPP LINK HAS ALREADY ADD TO BUNDLE ! !!CHIP ID IS %u E1 LINKNO IS %u,BUNDLE INDEX IS %u\n", __FILE__, __LINE__,chipId,e1LinkNo,bundleIndex); 
        return DRV_AT_PPP_LINK_ALREADY_ADD_TO_BUNDLE; 
    }
    /*将ppp link加入mlppp组*/
    ret = DrvAtPppLinkAdd(chipId,encapChanNo,bundleIndex);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PPP LINK ADD FAILED ! !!CHIP ID IS %u ENCAP CHANNO IS %u,BUNDLE INDEX IS %u\n", __FILE__, __LINE__,chipId,encapChanNo,bundleIndex); 
        return ret; 
    }
    g_atBundleLinksNum[chipId][bundleIndex]++;
    
    DrvAtMlpppBundleLinksNumGet(chipId,bundleIndex,&linksNum);
    /*如果是mlppp组中的第一条link则创建flow*/
    if(1 == linksNum)
    {
        ret = DrvAtEthFlowCreate(chipId,&flowId);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ETH FLOW OBJ CREATE FAILED ! !!CHIP ID IS %u FLOW ID IS %u\n", __FILE__, __LINE__,chipId,flowId); 
            return ret; 
        }
        /*获取mlppp组的vlanTag*/
        ret = DrvAtMlpppInfoGetFromIndex(chipId,bundleIndex,&mlpppInfo);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, DrvAtMlpppInfoGetFromIndex() FAILED ! !!CHIP ID IS %u BUNDLE INDEX IS %u\n", __FILE__, __LINE__,chipId,bundleIndex); 
            return ret; 
        }
        memcpy(&vlanTag1,&mlpppInfo.vlanTag1,sizeof(vlanTag1));
        memcpy(&vlanTag2,&mlpppInfo.vlanTag2,sizeof(vlanTag2));
        /*给eth flow 设置vlan*/
        ret = DrvAtEthFlowVlanSet(chipId,flowId,&vlanTag1,&vlanTag2);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ETH FLOW VLAN SET FAILED ! !!CHIP ID IS %u ,FLOW ID IS IS %u\n", __FILE__, __LINE__,chipId,flowId); 
            return ret; 
        }
        /*将flow与mlppp组绑定*/
        ret = DrvAtMlpppBundleEthFlowBind(chipId,bundleIndex,flowId);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, MLPPP BUNDLE ETH FLOW BIND FAILED ! !!CHIP ID IS %u,BUNDLE ID IS %u, FLOW ID IS %u\n", __FILE__, __LINE__,chipId,bundleIndex,flowId); 
            return ret; 
        }
        g_atMlpppBundleInfo[chipId][bundleIndex].flowId = flowId;
    }

    /*通过e1号获取ppp软件表*/
    ret = DrvAtPppInfoGetFromE1(chipId,e1LinkNo,&pppLinkInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, CAN NOT GET PPP INFO ,CHIP ID IS %u,E1 LINKNO IS %u !!\n", __FILE__, __LINE__,chipId,e1LinkNo);
        return ret;
    }
    /*更新表项*/
    pppLinkInfo.mlpppBundleIndex = bundleIndex;
    pppLinkNo = pppLinkInfo.pppLinkNo;
    DrvAtSavePppCfg(chipId,pppLinkNo,&pppLinkInfo);
    memcpy(&g_atMlpppBundleInfo[chipId][bundleIndex].linkMember[e1LinkNo],&pppLinkInfo,sizeof(DRV_AT_PPP_LINK_INFO));
    
    return ret;
}


/*********************************************************************
* 函数名称：DrvAtMlpppBundleRemoveLink
* 功能描述：删除一个mlppp成员
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtMlpppBundleRemoveLink(BYTE chipId,WORD32 bundleId,WORD32 e1LinkNo)
{
    WORD32  ret = DRV_AT_SUCCESS;
    WORD32 encapChanNo = 0;
    WORD32 bundleIndex = 0;
    WORD32 linksNum = 0;
    WORD32 flowId = 0;
    WORD32 pppLinkNo = 0;
    DRV_AT_PPP_LINK_INFO pppLinkInfo;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    memset(&pppLinkInfo,0,sizeof(DRV_AT_PPP_LINK_INFO));
    
    /*查找bundleId对应的bundle index*/
    ret = DrvAtMlpppIndexGetFromBundleId(chipId,bundleId,&bundleIndex);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, DrvAtMlpppIndexGetFromBundleId() FAILED ! !!CHIP ID IS %u ,MLPPP BUNDLE ID IS %u\n", __FILE__, __LINE__,chipId,bundleId); 
        return ret; 
    }
    /*如果找不到mlppp组返回失败*/
    if(INVALID_MLPPP_BUNDLE_INDEX == bundleIndex)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, MLPPP BUNDLE IS NOT EXSITED ! !!CHIP ID IS %u ,MLPPP BUNDLE ID IS %u \n", __FILE__, __LINE__,chipId,bundleId); 
        return DRV_AT_MLPPP_BUNDLE_NOT_CREATE; 
    }
    /*通过e1获取encap channo*/
    ret = DrvAtEncapChanNoGetFromE1(chipId,e1LinkNo,&encapChanNo);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, DrvAtEncapChanNoGetFromE1() FAILED ! !!CHIP ID IS %u E1 LINKNO IS %u\n", __FILE__, __LINE__,chipId,e1LinkNo); 
        return ret; 
    }
    /*如果找不到encapchan 返回失败*/
    if(INVALID_ENCAP_CHANNO == encapChanNo)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ENCAP CHAN IS NOT EXSITED ! !!CHIP ID IS %u ,E1LINK NO IS %u \n", __FILE__, __LINE__,chipId,e1LinkNo); 
        return DRV_AT_ENCAP_CHAN_NOT_CREATED; 
    }
    /*检查这条e1是否已经被移除这个组,如果已移除则返回失败*/
    ret = DrvAtPppLinkAddtoBundleCheck(chipId,bundleIndex,e1LinkNo);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PPP LINK HAS ALREADY REMOVE TO BUNDLE ! !!CHIP ID IS %u E1 LINKNO IS %u,BUNDLE INDEX IS %u\n", __FILE__, __LINE__,chipId,e1LinkNo,bundleIndex); 
        return DRV_AT_PPP_LINK_ALREADY_REMOVE_TO_BUNDLE; 
    }
    /*移除一个ppp link*/
    ret = DrvAtPppLinkRemove(chipId,encapChanNo,bundleIndex);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PPP LINK REMOVE FAILED ! !!CHIP ID IS %u ENCAP CHANNO IS %u,BUNDLE ID IS %u\n", __FILE__, __LINE__,chipId,encapChanNo,bundleIndex); 
        return ret; 
    }
    g_atBundleLinksNum[chipId][bundleIndex]--;
    /*获取mlppp组内的link数，如果是最后一条就将flow解绑并删除*/
    DrvAtMlpppBundleLinksNumGet(chipId,bundleIndex,&linksNum);
    if(0 == linksNum)
    {
        flowId = g_atMlpppBundleInfo[chipId][bundleIndex].flowId;
        ret = DrvAtMlpppBundleEthFlowUnbind(chipId,bundleIndex,flowId);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, MLPPP BUNDLE ETH FLOW BIND FAILED ! !!CHIP ID IS %u,BUNDLE ID IS %u, FLOW ID IS %u\n", __FILE__, __LINE__,chipId,bundleIndex,flowId); 
            return ret; 
        }
        ret = DrvAtEthFlowDelete(chipId,flowId);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, ETH FLOW OBJ CREATE FAILED ! !!CHIP ID IS %u FLOW ID IS %u\n", __FILE__, __LINE__,chipId,flowId); 
            return ret; 
        }
        /*更新软件表，清空flowId和vlan*/
        g_atMlpppBundleInfo[chipId][bundleIndex].flowId = INVALID_FLOW_ID;
        memset(&g_atMlpppBundleInfo[chipId][bundleIndex].vlanTag1,0,sizeof(DRV_AT_PPP_VLAN_TAG1_INFO));
        memset(&g_atMlpppBundleInfo[chipId][bundleIndex].vlanTag2,0,sizeof(DRV_AT_PPP_VLAN_TAG2_INFO));
    }
    /*通过e1号获取ppp软件表*/
    ret = DrvAtPppInfoGetFromE1(chipId,e1LinkNo,&pppLinkInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, CAN NOT GET PPP INFO ,CHIP ID IS %u,E1 LINKNO IS %u !!\n", __FILE__, __LINE__,chipId,e1LinkNo);
        return ret;
    }
    /*清空bundle组中link的信息*/
    pppLinkInfo.mlpppBundleIndex = INVALID_MLPPP_BUNDLE_INDEX;
    pppLinkNo = pppLinkInfo.pppLinkNo;
    DrvAtSavePppCfg(chipId,pppLinkNo,&pppLinkInfo);
    memset(&g_atMlpppBundleInfo[chipId][bundleIndex].linkMember[e1LinkNo],0,sizeof(DRV_AT_PPP_LINK_INFO));
    
    return ret;
}


/*********************************************************************
* 函数名称：DrvAtPppInfoPrint
* 功能描述：将产品管理的参数打印
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtPppInfoPrint(T_BSP_SRV_FUNC_PPP_PW_BIND_ARG * pmPppAddInfo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    
    CHECK_AT_POINTER_NULL(pmPppAddInfo);
    
    DRV_AT_E1_PRINT(DRV_AT_PPP_CFG,"[CE1TAN][PPP INFO]:%s line %d,subcardId is %u\n",__FILE__, __LINE__,pmPppAddInfo->dwSubSlotId);
    DRV_AT_E1_PRINT(DRV_AT_PPP_CFG,"[CE1TAN][PPP INFO]:%s line %d,portId is %u\n",__FILE__, __LINE__,pmPppAddInfo->dwPortId);
    DRV_AT_E1_PRINT(DRV_AT_PPP_CFG,"[CE1TAN][PPP INFO]:%s line %d,PwId is %u\n",__FILE__, __LINE__,pmPppAddInfo->dwPWid);
    DRV_AT_E1_PRINT(DRV_AT_PPP_CFG,"[CE1TAN][PPP INFO]:%s line %d,dwMaxSdu is %u\n",__FILE__, __LINE__,pmPppAddInfo->dwMaxSdu);
    DRV_AT_E1_PRINT(DRV_AT_PPP_CFG,"[CE1TAN][PPP INFO]:%s line %d,dwTdi is %u\n",__FILE__, __LINE__,pmPppAddInfo->dwTdi);
 
    
    return ret;
}

/*********************************************************************
* 函数名称：DrvAtMlppInfoPrint
* 功能描述：将产品管理的参数打印
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtMlppInfoPrint(T_BSP_SRV_FUNC_MPPP_PW_BIND_ARG * pmMlpppInfo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    
    CHECK_AT_POINTER_NULL(pmMlpppInfo);
    
    DRV_AT_E1_PRINT(DRV_AT_MLPPP_CFG,"[CE1TAN][MLPPP INFO]:%s line %d,subcardId is %u\n",__FILE__, __LINE__,pmMlpppInfo->dwSubSlotId);
    DRV_AT_E1_PRINT(DRV_AT_MLPPP_CFG,"[CE1TAN][MLPPP INFO]:%s line %d,dwBundleId is %u\n",__FILE__, __LINE__,pmMlpppInfo->dwBundleId);
    DRV_AT_E1_PRINT(DRV_AT_MLPPP_CFG,"[CE1TAN][MLPPP INFO]:%s line %d,dwMrru is %u\n",__FILE__, __LINE__,pmMlpppInfo->dwMrru);
    DRV_AT_E1_PRINT(DRV_AT_MLPPP_CFG,"[CE1TAN][MLPPP INFO]:%s line %d,dwSnMode is %u\n",__FILE__, __LINE__,pmMlpppInfo->dwSnMode);
    DRV_AT_E1_PRINT(DRV_AT_MLPPP_CFG,"[CE1TAN][MLPPP INFO]:%s line %d,dwFragFlag is %u\n",__FILE__, __LINE__,pmMlpppInfo->dwFragFlag);
    DRV_AT_E1_PRINT(DRV_AT_MLPPP_CFG,"[CE1TAN][MLPPP INFO]:%s line %d,dwFragSize is %u\n",__FILE__, __LINE__,pmMlpppInfo->dwFragSize);
 
    
    return ret;
}

/*********************************************************************
* 函数名称：DrvAtPppPmInfoConvert
* 功能描述：将产品管理的参数转化为bsp的参数
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtPppPmInfoConvert(T_BSP_SRV_FUNC_PPP_PW_BIND_ARG * pmPppAddInfo,DRV_AT_PPP_LINK_INFO *drvPppAddInfo)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    
    CHECK_AT_POINTER_NULL(pmPppAddInfo);
    CHECK_AT_POINTER_NULL(drvPppAddInfo);

    DrvAtPppInfoPrint(pmPppAddInfo);/*将产品管理带下的参数打印出来，调试使用*/
    
    drvPppAddInfo->chipId = pmPppAddInfo->dwSubSlotId - 1;
    drvPppAddInfo->e1LinkNo = pmPppAddInfo->dwPortId -1;
    drvPppAddInfo->cip = pmPppAddInfo->dwPWid;              /*产品管理的pwid就是bsp的cip,子卡内唯一*/
    drvPppAddInfo->tdi = pmPppAddInfo->dwTdi;
    drvPppAddInfo->mtu = pmPppAddInfo->dwMaxSdu;
    drvPppAddInfo->fcsMode = AT_HDLC_FCS16;
    drvPppAddInfo->isScrambleEn = CE1_DISABLE;
    
    drvPppAddInfo->vlanTag1.pri= 5;
    drvPppAddInfo->vlanTag2.pri = 5;
    drvPppAddInfo->vlanTag1.cfi = 0;
    drvPppAddInfo->vlanTag2.cfi = 0;
    
    drvPppAddInfo->vlanTag1.pktLen = 0;
    drvPppAddInfo->vlanTag1.encapType = 0;
    drvPppAddInfo->vlanTag1.cpu = 0;
    
    drvPppAddInfo->vlanTag2.chanNo = (WORD16)(pmPppAddInfo->dwPWid);
    drvPppAddInfo->vlanTag2.isMlppp = 0;
    drvPppAddInfo->vlanTag2.portNo = 0;
    
    return ret;
}


/*********************************************************************
* 函数名称：DrvAtPppServiceCreate
* 功能描述：创建ppp接口
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtPppServiceCreate(T_BSP_SRV_FUNC_PPP_PW_BIND_ARG* pmPppInfo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    DRV_AT_PPP_LINK_INFO drvPppInfo;
    
    CHECK_AT_POINTER_NULL(pmPppInfo);
    
    memset(&drvPppInfo,0,sizeof(DRV_AT_PPP_LINK_INFO));
    
    ret = DrvAtPppPmInfoConvert(pmPppInfo,&drvPppInfo);     /*convert pm ppp info-->drv ppp info*/
    if(DRV_AT_SUCCESS != ret)
    {   
        BSP_Print(BSP_DEBUG_ALL,"[CE1TAN][PPP ADD]:ERROR: %s line %d, PPP INFO CONVERT FAILED ! !! PW ID IS %u\n", __FILE__, __LINE__,pmPppInfo->dwPWid);  
        return ret; 
    }
    ret = DrvAtPppLinkCreate(&drvPppInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        BSP_Print(BSP_DEBUG_ALL,"[CE1TAN][PPP ADD]:ERROR: %s line %d,PPP CREATE FAILED !!CHIP ID IS %u, CIP IS %u, RET IS %u\n", __FILE__, __LINE__,drvPppInfo.chipId,drvPppInfo.cip,ret);
        return ret;
    }    
    
    return ret;

}

/*********************************************************************
* 函数名称：DrvAtPppServiceDelete
* 功能描述：删除ppp接口
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtPppServiceDelete(T_BSP_SRV_FUNC_PW_UNBIND_ARG* pmPppdelInfo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE chipId = 0;
    WORD32 cip = 0;
    DRV_AT_PPP_LINK_INFO drvPppInfo ;
    
    CHECK_AT_POINTER_NULL(pmPppdelInfo);
    
    chipId = pmPppdelInfo->dwSubslotId -1;
    cip = pmPppdelInfo->dwPWid;
    memset(&drvPppInfo,0,sizeof(DRV_AT_PPP_LINK_INFO));
    
    ret = DrvAtPppInfoGetFromCip(chipId,cip,&drvPppInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        BSP_Print(BSP_DEBUG_ALL,"[CE1TAN][PPP DEL]ERROR: %s line %d, CAN NOT GET PPP INFO ,CIP IS %u !!\n", __FILE__, __LINE__,cip);
        return ret;
    }
    ret = DrvAtPppLinkDelete(&drvPppInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        BSP_Print(BSP_DEBUG_ALL,"[CE1TAN][PPP DEL]ERROR: %s line %d,PPP DELETE FAILED !!CHIP ID IS %u, CIP IS %u ,RET IS %u\n", __FILE__, __LINE__,chipId,cip,ret);
        return ret;
    }    
    
    return ret;
}

/*********************************************************************
* 函数名称：DrvAtPppServiceModify
* 功能描述：修改ppp配置接口
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtPppServiceModify(T_BSP_SRV_FUNC_PPP_PW_MODIFY_ARG* pppModifyInfo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE chipId = 0;
    WORD32 cip = 0;
    WORD32 encapChanNo = 0;
    WORD32 pppLinkNo = 0;
    WORD32 newCrc = 0;
    WORD32 newMtu = 0;
    WORD32 atCrc = 0;
    DRV_AT_PPP_LINK_INFO drvPppInfo ;
    
    CHECK_AT_POINTER_NULL(pppModifyInfo);
    memset(&drvPppInfo,0,sizeof(DRV_AT_PPP_LINK_INFO));
    
    chipId = pppModifyInfo->dwSubSlotId - 1;
    cip = pppModifyInfo->dwPWid;
    
    ret = DrvAtPppInfoGetFromCip(chipId,cip,&drvPppInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        BSP_Print(BSP_DEBUG_ALL,"ERROR: %s line %d, CAN NOT GET PPP INFO ,CIP IS %u !!\n", __FILE__, __LINE__,cip);
        return ret;
    }
    encapChanNo = drvPppInfo.encapChanNo;
    pppLinkNo = drvPppInfo.pppLinkNo;
    
    if(0 == pppModifyInfo->dwModifyType)
    {
        newMtu = pppModifyInfo->dwNewMaxSdu;
        /*arrive只支持mtu 9600*/

        drvPppInfo.mtu= newMtu; 
    }
    else if(1 == pppModifyInfo->dwModifyType)
    {
        newCrc = pppModifyInfo->dwNewCrcType;
        atCrc = (1== newCrc)?AT_HDLC_FCS32 : AT_HDLC_FCS16;
        ret = DrvAtHdlcFcsModeSet(chipId,encapChanNo,atCrc);
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, HDLC FCS MODE SET FAILED ! !!CHIP ID IS %u ENCAP CHANNO IS %u\n", __FILE__, __LINE__,chipId,encapChanNo); 
            return ret; 
        }
        drvPppInfo.fcsMode = atCrc; 
    }
    
    /*更新全局软件表*/
    ret = DrvAtSavePppCfg(chipId,pppLinkNo,&drvPppInfo);  
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, DrvAtSavePppCfg() ret=0x%x.\n", __FILE__, __LINE__, ret);
        return ret;
    }
    
    return ret;
}

/*********************************************************************
* 函数名称：DrvAtMlpppServiceCreate
* 功能描述：创建mlppp组接口
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtMlpppServiceCreate(T_BSP_SRV_FUNC_MPPP_PW_BIND_ARG* mlpppInfo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    DRV_AT_MLPPP_BUNDLE_INFO drvMlpppInfo ;
    
    CHECK_AT_POINTER_NULL(mlpppInfo);
    memset(&drvMlpppInfo,0,sizeof(DRV_AT_MLPPP_BUNDLE_INFO));
    
    DrvAtMlppInfoPrint(mlpppInfo);
    drvMlpppInfo.chipId = mlpppInfo->dwSubSlotId - 1;
    drvMlpppInfo.bundleId = mlpppInfo->dwBundleId;
    drvMlpppInfo.mrru = mlpppInfo->dwMrru;
    drvMlpppInfo.seqMode = (1 == mlpppInfo->dwSnMode)?AT_SEQ_MODE_LONG: AT_SEQ_MODE_SHORT;
    
    if(0 == mlpppInfo->dwFragFlag)
    {
        drvMlpppInfo.fragsize = AT_FRAG_SIZE_NONE;
    }
    else if(512 == mlpppInfo->dwFragSize)
    {
        drvMlpppInfo.fragsize = AT_FRAG_SIZE_512;
    }
    else if(256 == mlpppInfo->dwFragSize)
    {
        drvMlpppInfo.fragsize = AT_FRAG_SIZE_256;
    }
    else if(128 == mlpppInfo->dwFragSize)    
    {
        drvMlpppInfo.fragsize = AT_FRAG_SIZE_128;
    }
    else if(64 == mlpppInfo->dwFragSize)    
    {
        drvMlpppInfo.fragsize = AT_FRAG_SIZE_64;
    }
    else
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, INVALID FRAGSIZE %u.\n", __FILE__, __LINE__, mlpppInfo->dwFragSize);
        return DRV_AT_INVALID_OPTION;
    }
    drvMlpppInfo.vlanTag1.pri= 5;
    drvMlpppInfo.vlanTag2.pri = 5;
    drvMlpppInfo.vlanTag1.cfi = 0;
    drvMlpppInfo.vlanTag2.cfi = 0;
    
    drvMlpppInfo.vlanTag1.pktLen = 0;
    drvMlpppInfo.vlanTag1.encapType = 0;
    drvMlpppInfo.vlanTag1.cpu = 0;
    
    drvMlpppInfo.vlanTag2.chanNo = (WORD16)(mlpppInfo->dwBundleId);
    drvMlpppInfo.vlanTag2.isMlppp = 1;
    drvMlpppInfo.vlanTag2.portNo = 0;
    
    ret = DrvAtMlpppBundleCreate(&drvMlpppInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        BSP_Print(BSP_DEBUG_ALL,"[CE1TAN][MLPPP CREATE]:ERROR: %s line %d,MLPPP CREATE FAILED !!CHIP ID IS %u, BUNDLE ID IS %u, RET IS %u\n", __FILE__, __LINE__,drvMlpppInfo.chipId,drvMlpppInfo.bundleId,ret);
        return ret;
    }  
   
    return ret;
}

/*********************************************************************
* 函数名称：DrvAtMlpppServiceDelete
* 功能描述：删除mlppp接口
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtMlpppServiceDelete(T_BSP_SRV_FUNC_PW_UNBIND_ARG* mlpppDelInfo)
{
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE chipId = 0;
    WORD32 bundleId = 0;
    CHECK_AT_POINTER_NULL(mlpppDelInfo);
    chipId = mlpppDelInfo->dwSubslotId - 1;
    bundleId = mlpppDelInfo->dwPWid;
    
    ret = DrvAtMlpppBundleDelete(chipId,bundleId);
    if (DRV_AT_SUCCESS != ret)
    {
        BSP_Print(BSP_DEBUG_ALL,"[CE1TAN][MLPPP DELETE]:ERROR: %s line %d,MLPPP DELETE FAILED !!CHIP ID IS %u, BUNDLE ID IS %u, RET IS %u\n", __FILE__, __LINE__,chipId,bundleId,ret);
        return ret;
    }  
    
    return ret;
}

/*********************************************************************
* 函数名称：DrvAtMlpppServiceModify
* 功能描述：修改mlppp配置接口，预留，暂不需要
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtMlpppServiceModify(T_BSP_SRV_FUNC_MPPP_PW_BIND_ARG* mlpppModifyInfo)
{
WORD32 ret = DRV_AT_SUCCESS;


return ret;
}

/*********************************************************************
* 函数名称：DrvAtMlpppServiceAddLink
* 功能描述：添加一个mlppp成员
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtMlpppServiceAddLink(T_BSP_SRV_FUNC_MPPP_BUNDLE_LINK_ADD* mlpppAddLink)
{
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE chipId = 0;
    WORD32 bundleId = 0;
    WORD32 linkCip = 0;
    WORD32 e1LinkNo = 0;
    DRV_AT_PPP_LINK_INFO pppLinkInfo ;
    
    CHECK_AT_POINTER_NULL(mlpppAddLink);
    
    chipId = mlpppAddLink->dwSubSlotId - 1;
    bundleId = mlpppAddLink->dwBundlePwId;
    linkCip = mlpppAddLink->dwLinkPwId;
    memset(&pppLinkInfo,0,sizeof(DRV_AT_PPP_LINK_INFO));
    
    ret = DrvAtPppInfoGetFromCip(chipId,linkCip,&pppLinkInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        BSP_Print(BSP_DEBUG_ALL,"ERROR: %s line %d, CAN NOT GET PPP INFO ,CIP IS %u !!\n", __FILE__, __LINE__,linkCip);
        return ret;
    }
    e1LinkNo = pppLinkInfo.e1LinkNo;
    ret = DrvAtMlpppBundleAddLink(chipId,bundleId,e1LinkNo);
    if (DRV_AT_SUCCESS != ret)
    {
        BSP_Print(BSP_DEBUG_ALL,"[CE1TAN][MLPPP ADD LINK]:ERROR: %s line %d,MLPPP ADD LINK FAILED !!CHIP ID IS %u, BUNDLE ID IS %u, E1 LINKNO IS %u,RET IS %u\n", __FILE__, __LINE__,chipId,bundleId,e1LinkNo,ret);
        return ret;
    }  

    return ret;
}

/*********************************************************************
* 函数名称：DrvAtMlpppServiceDelLink
* 功能描述：删除一个mlppp成员
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
*           
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期     版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtMlpppServiceDelLink(T_BSP_SRV_FUNC_MPPP_BUNDLE_LINK_DELETE* mlpppDelLink)
{
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE chipId = 0;
    WORD32 bundleId = 0;
    WORD32 linkCip = 0;
    WORD32 e1LinkNo = 0;
    DRV_AT_PPP_LINK_INFO pppLinkInfo ;
    CHECK_AT_POINTER_NULL(mlpppDelLink);

    chipId = mlpppDelLink->dwSubSlotId - 1;
    bundleId = mlpppDelLink->dwBundlePwId;
    linkCip = mlpppDelLink->dwLinkPwId;
    memset(&pppLinkInfo,0,sizeof(DRV_AT_PPP_LINK_INFO));
    
    ret = DrvAtPppInfoGetFromCip(chipId,linkCip,&pppLinkInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        BSP_Print(BSP_DEBUG_ALL,"ERROR: %s line %d, CAN NOT GET PPP INFO ,CIP IS %u !!\n", __FILE__, __LINE__,linkCip);
        return ret;
    }
    e1LinkNo = pppLinkInfo.e1LinkNo;
    ret = DrvAtMlpppBundleRemoveLink(chipId,bundleId,e1LinkNo);
    if (DRV_AT_SUCCESS != ret)
    {
        BSP_Print(BSP_DEBUG_ALL,"[CE1TAN][MLPPP DEL LINK]:ERROR: %s line %d,MLPPP DEL LINK FAILED !!CHIP ID IS %u, BUNDLE ID IS %u, E1 LINKNO IS %u,RET IS %u\n", __FILE__, __LINE__,chipId,bundleId,e1LinkNo,ret);
        return ret;
    }
    
    return ret;
}

/*********************************************************************
* 函数名称：DrvAtPidSet
* 功能描述：设置arrive内部的PID表
* 访问的表：
* 修改的表：
* 输入参数：chipId:   芯片序列号，范围0~3
* 输出参数： 
* 返 回 值: 
* 其它说明：
* 修改日期              版本号             修改人        修改内容
* --------------------------------------------------------------------
* 2013/12/16            V1.0               chenbo       创建
*********************************************************************/
WORD32 DrvAtPidSet(BYTE chipId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    AtDevice atDevHdl = NULL;
    eAtRet atRet = cAtOk;
    WORD32 entryId[] = {0,1,2,3,4,15};
    WORD16 pid[] = {AT_IPV4_PID, AT_IPV6_PID, AT_MPLSU_PID, AT_MPLSM_PID, AT_ISIS_PID, AT_PPP_PID};
    WORD16 ethType[] = {AT_IPV4_ETHTYPE, AT_IPV6_ETHTYPE, AT_MPLSU_ETHTYPE, AT_MPLSM_ETHTYPE, 0,0};
    BYTE priority = AT_PRI_DEFAULT;
    BYTE cfi = AT_CFI_DEFAULT;
    BYTE i = 0;
      
    /*check chipId and port number*/
    CHECK_AT_CHIP_ID(chipId);
    
    atDevHdl = DrvAtDevHdlGet(chipId);
    CHECK_AT_POINTER_NULL(atDevHdl);
    
    for(i = 0; i < sizeof(entryId)/sizeof(WORD32); i++)
    {
        atRet = AtZtePtnPppLookupEntrySet(atDevHdl, entryId[i], pid[i], ethType[i], AT_PPP_ROUTE_ENCAPTYP);
        if (cAtOk != atRet)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtZtePtnPppLookupEntrySet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
            return DRV_AT_PPP_PID_SET_FAILED;
        }     
        atRet= AtZtePtnMlpppLookupEntryEncapTypeSet(atDevHdl,entryId[i],AT_MLPPP_ROUTE_ENCAPTYP);
        if (cAtOk != atRet)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtZtePtnMlpppLookupEntryEncapTypeSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
            return DRV_AT_MLPPP_PID_SET_FAILED;
        }    
        atRet = AtZtePtnPppLookupEntryCfiSet(atDevHdl, entryId[i], cfi);
        if (cAtOk != atRet)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtZtePtnPppLookupEntryCfiSet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
            return DRV_AT_PPP_PID_SET_FAILED;
        }       
        atRet = AtZtePtnPppLookupEntryPrioritySet(atDevHdl, entryId[i], priority);
        if (cAtOk != atRet)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtZtePtnPppLookupEntryPrioritySet() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
            return DRV_AT_PPP_PID_SET_FAILED;
        }        
        atRet = AtZtePtnPppLookupEntryEnable(atDevHdl, entryId[i], cAtTrue);
        if (cAtOk != atRet)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtZtePtnPppLookupEntryEnable() ret=%s.\n", __FILE__, __LINE__, AtRet2String(atRet));
            return DRV_AT_PPP_PID_SET_FAILED;
        } 
    
    }
       
    return ret;
}




/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_at_e1_test.c
* 文件标识: 
* 其它说明: ce1tan 测试函数代码
* 当前版本: V1.0
* 作    者: 陈博10140635.
* 完成日期: 2013-12-16
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/
#include "drv_at_e1_common.h"
#include "drv_at_e1_port.h"
#include "drv_at_e1_mlppp.h"
#include "drv_at_e1_pwe3.h"





/*###################### CES 调试函数 ########################*/

/*******************************************************************************
* 函数名称:   DrvAtPwAddForTest
* 功能描述:   添加pw测试函数,目前只适用于系统时钟
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwAddForTest(BYTE subcardId,WORD32 e1PortId,BYTE servType,WORD32 cip,BYTE clkMode,WORD32 tsBitmap, WORD32 tsNum,WORD32 enc,WORD32 jitterBufferSize)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    BYTE dMac[6] = {1,1,1,1,1,1};
    DRV_AT_PW_PARA atPwInfo ;
    memset(&atPwInfo, 0, sizeof(DRV_AT_PW_PARA));

    CHECK_AT_SUBCARD_ID(subcardId);
    CHECK_AT_E1PORT_ID(e1PortId);
    
    atPwInfo.chipId = subcardId -1 ;                /*subcard id :[1,4]*/ 
    atPwInfo.e1LinkNum = e1PortId -1 ;            /*e1 port :[1,24]*/
    atPwInfo.cip = cip ;                                   /*根据cip来索引对应的pw*/
    atPwInfo.servMode = servType ;
    atPwInfo.timeSlotBitMap = tsBitmap;
    atPwInfo.clockMode = clkMode ;
    atPwInfo.clkState = 0;
    atPwInfo.clkDomainId = 0;
    atPwInfo.masterE1LinkNo = 0 ;
    atPwInfo.jitterBufferSize = jitterBufferSize ;
    atPwInfo.frameNum = enc ;
    atPwInfo.tsNum = tsNum;
    atPwInfo.vlan1Pri = 0;
    atPwInfo.vlan1Cfi = 0;
    atPwInfo.vlan1Info.zteVlan1Id = 640; 
    atPwInfo.vlan2Pri = 0;
    atPwInfo.vlan2Cfi = 0;
    atPwInfo.vlan2Info.zteVlan2Id = cip;
    memcpy(atPwInfo.destMac,dMac,sizeof(atPwInfo.destMac));

    if(SATOP_MODE== servType)
    {
        atPwInfo.payloadSize = enc *32 ;
    }
    else
    {
        atPwInfo.payloadSize = enc *tsNum ;
    }

    ret = DrvAtPwAdd(&atPwInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, DrvAtPwAdd() ret=0x%x.\n", __FILE__, __LINE__, ret);
        return ret ;
    }    
    return ret ;
}


/*******************************************************************************
* 函数名称:   DrvAtPwDelForTest
* 功能描述:   删除pw测试函数
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwDelForTest(BYTE subcardId,WORD32 e1PortId,BYTE servType,BYTE clkMode,WORD32 cip,WORD32 tsBitmap)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    DRV_AT_PW_PARA atPwInfo ;
    memset(&atPwInfo, 0, sizeof(DRV_AT_PW_PARA));

    CHECK_AT_SUBCARD_ID(subcardId);
    
    ret = DrvAtPwInfoGetFromCip(subcardId-1,cip,&atPwInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, DrvAtPwInfoGetFromCip() ret=0x%x.\n", __FILE__, __LINE__, ret);
        return ret ;
    }    
    ret = DrvAtPwDelete(&atPwInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, DrvAtPwDelete() ret=0x%x.\n", __FILE__, __LINE__, ret);
        return ret ;
    }    
    return ret ;
}


/*******************************************************************************
* 函数名称:   DrvAtPwAddMaxTest
* 功能描述:   测试128条pw，cesop ，每条E1创建5条cesop业务，最后8条取前8路e1，没路建一条cesop业务
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwAddMaxTest(BYTE subcardId,WORD32 enc,WORD32 jitterbuff)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 e1PortId = 0;
    WORD32 cip = 0;
    
    CHECK_AT_SUBCARD_ID(subcardId);
    
    for(e1PortId = 1;e1PortId<=24;e1PortId++)
    {
        DrvAtE1FrameTypeSet(subcardId-1,e1PortId-1,AT_E1_PCM31);
        cip = e1PortId*5;
        ret = DrvAtPwAddForTest(subcardId,e1PortId,1,cip,1,0xf0000000,4,enc,jitterbuff);
        ret |= DrvAtPwAddForTest(subcardId,e1PortId,1,cip+1,1,0x0f000000,4,enc,jitterbuff);
        ret |= DrvAtPwAddForTest(subcardId,e1PortId,1,cip+2,1,0x00f00000,4,enc,jitterbuff);
        ret |= DrvAtPwAddForTest(subcardId,e1PortId,1,cip+3,1,0x000f0000,4,enc,jitterbuff);
        ret |= DrvAtPwAddForTest(subcardId,e1PortId,1,cip+4,1,0x0000f000,4,enc,jitterbuff);
        if(DRV_AT_SUCCESS != ret)
        {
            return DRV_AT_PW_CESOP_ADD_FAILED;
        }
    }
    for(e1PortId = 1;e1PortId<=8;e1PortId++)
    {
        cip = 130+e1PortId;
        ret = DrvAtPwAddForTest(subcardId,e1PortId,1,cip,1,0x00000f00,4,enc,jitterbuff);   
        if(DRV_AT_SUCCESS != ret)
        {
            return DRV_AT_PW_CESOP_ADD_FAILED;
        }
    }
    return ret;
}


/*******************************************************************************
* 函数名称:   DrvAtPwDelMaxTest
* 功能描述:   删除128条pw，cesop ，每条E1创建5条cesop业务，最后8条取前8路e1，没路建一条cesop业务
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwDelMaxTest(BYTE subcardId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 e1PortId = 0;
    WORD32 cip = 0;
    
    CHECK_AT_SUBCARD_ID(subcardId);
    
    for(e1PortId = 1;e1PortId<=24;e1PortId++)
    {
        cip = e1PortId*5;
        ret = DrvAtPwDelForTest(subcardId,e1PortId,1,1,cip,0xf0000000);
        ret |= DrvAtPwDelForTest(subcardId,e1PortId,1,1,cip+1,0x0f000000);
        ret |= DrvAtPwDelForTest(subcardId,e1PortId,1,1,cip+2,0x00f00000);
        ret |= DrvAtPwDelForTest(subcardId,e1PortId,1,1,cip+3,0x000f0000);
        ret |= DrvAtPwDelForTest(subcardId,e1PortId,1,1,cip+4,0x0000f000);
        if(DRV_AT_SUCCESS != ret)
        {
            return  DRV_AT_PW_DELETE_FAILED;
        }
    }
    for(e1PortId = 1;e1PortId<=8;e1PortId++)
    {
        cip = 130+e1PortId;
        ret = DrvAtPwDelForTest(subcardId,e1PortId,1,1,cip,0x00000f00);   
        if(DRV_AT_SUCCESS != ret)
        {
            return  DRV_AT_PW_DELETE_FAILED;
        }
    }
    for(e1PortId = 1;e1PortId<=24;e1PortId++)
    {
        DrvAtE1FrameTypeSet(subcardId-1,e1PortId-1,AT_E1_UNFRAME);
    }
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtPwAddDelLoopTest
* 功能描述:   添加删除ces业务测试，可选择监控模式和拷机模式
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwAddDelLoopTest(BYTE subcardId,WORD32 enc,WORD32 jitterbuff,WORD32 loopTime,BYTE ifCheckTester)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 e1PortId = 0;
    WORD32 cip = 0;
    WORD32 i = 0;
    
    while(i<loopTime)
    {
        for(e1PortId = 1;e1PortId<=24 ;e1PortId++)
        {
            cip = e1PortId;
            ret = DrvAtPwAddForTest(subcardId,e1PortId,0,cip,SYS_CLK_MODE,0,0,enc,jitterbuff);
            if(DRV_AT_SUCCESS != ret)
            {
                ROSNG_TRACE_DEBUG("\nAdd Pw Failed:subcardId:%u,e1PortId:%u,cip:%u\n",subcardId,e1PortId,cip);
                return DRV_AT_PW_CESOP_ADD_FAILED;
            }
            else
            {
                ROSNG_TRACE_DEBUG("Add Pw Ok:subcardId:%u,e1PortId:%u,cip:%u\n",subcardId,e1PortId,cip);
            }
        }
        if(CE1_TURE == ifCheckTester)
        {
            ROSNG_TRACE_DEBUG("\n!!!Check pw status on Tester now,u have 10s,%u times remaining ...\n\n",(loopTime - i));
            BSP_DelayMs(10000);
        }
        else
        {
            BSP_DelayMs(500);
        }
        
        for(e1PortId = 1;e1PortId<=24 ;e1PortId++)
        {
            cip = e1PortId;
            ret = DrvAtPwDelForTest(subcardId,e1PortId,0,SYS_CLK_MODE,cip,0);
            if(DRV_AT_SUCCESS != ret)
            {
                ROSNG_TRACE_DEBUG("\nDel Pw Failed:subcardId:%u,e1PortId:%u,cip:%u\n",subcardId,e1PortId,cip);
                return DRV_AT_PW_CESOP_ADD_FAILED;
            }
            else
            {
                ROSNG_TRACE_DEBUG("Del Pw Ok:subcardId:%u,e1PortId:%u,cip:%u\n",subcardId,e1PortId,cip);
            }
        }
        BSP_DelayMs(500);
        i++;
    }
    for(e1PortId = 1;e1PortId<=24 ;e1PortId++)
    {
        cip = e1PortId;
        ret = DrvAtPwAddForTest(subcardId,e1PortId,0,cip,SYS_CLK_MODE,0,0,enc,jitterbuff);
        if(DRV_AT_SUCCESS != ret)
        {
            ROSNG_TRACE_DEBUG("\nAdd Pw Failed:subcardId:%u,e1PortId:%u,cip:%u\n",subcardId,e1PortId,cip);
            return DRV_AT_PW_CESOP_ADD_FAILED;
        }
        else
        {
            ROSNG_TRACE_DEBUG("Add Pw Ok:subcardId:%u,e1PortId:%u,cip:%u\n",subcardId,e1PortId,cip);
        }
    }
    return ret ;
}


/*******************************************************************************
* 函数名称:   DrvAtPwAddOp
* 功能描述:   添加24条satop业务
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwAddOp(BYTE subcardId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 e1PortId = 0;
    WORD32 cip = 0;
    
    for(e1PortId = 1;e1PortId<=24 ;e1PortId++)
    {
        cip = e1PortId;
        ret = DrvAtPwAddForTest(subcardId,e1PortId,0,cip,SYS_CLK_MODE,0,0,8,32000);
        if(DRV_AT_SUCCESS != ret)
        {
            ROSNG_TRACE_DEBUG("\nAdd Pw Failed:subcardId:%u,e1PortId:%u,cip:%u\n",subcardId,e1PortId,cip);
            return DRV_AT_PW_CESOP_ADD_FAILED;
        }
        else
        {
            ROSNG_TRACE_DEBUG("Add Pw Ok:subcardId:%u,e1PortId:%u,cip:%u\n",subcardId,e1PortId,cip);
        }
    }
    
    return ret;

}

/*******************************************************************************
* 函数名称:   DrvAtPwDelOp
* 功能描述:   删除24条satop业务
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwDelOp(BYTE subcardId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 e1PortId = 0;
    WORD32 cip = 0;
      
    for(e1PortId = 1;e1PortId<=24 ;e1PortId++)
    {
        cip = e1PortId;
        ret = DrvAtPwDelForTest(subcardId,e1PortId,0,SYS_CLK_MODE,cip,0);
        if(DRV_AT_SUCCESS != ret)
        {
            ROSNG_TRACE_DEBUG("\nDel Pw Failed:subcardId:%u,e1PortId:%u,cip:%u\n",subcardId,e1PortId,cip);
            return DRV_AT_PW_CESOP_ADD_FAILED;
        }
        else
        {
            ROSNG_TRACE_DEBUG("Del Pw Ok:subcardId:%u,e1PortId:%u,cip:%u\n",subcardId,e1PortId,cip);
        }
    }
    
    return ret;
}


/*******************************************************************************
* 函数名称:   DrvAtPwAddClkDomainTest
* 功能描述:   时钟域功能测试，添加一条ACR/DCR 业务
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwAddClkDomainTest(BYTE subcardId,WORD32 e1PortId,BYTE servType,WORD32 cip,BYTE clkMode,BYTE clkState,WORD32 domainNo,WORD32 enc,WORD32 jitterBufferSize)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    BYTE dMac[6] = {1,1,1,1,1,1};
    DRV_AT_PW_PARA atPwInfo ;
    memset(&atPwInfo, 0, sizeof(DRV_AT_PW_PARA));

    CHECK_AT_SUBCARD_ID(subcardId);
    CHECK_AT_E1PORT_ID(e1PortId);
    
    atPwInfo.chipId = subcardId -1 ;                /*subcard id :[1,4]*/ 
    atPwInfo.e1LinkNum = e1PortId -1 ;            /*e1 port :[1,24]*/
    atPwInfo.cip = cip ;                                   /*根据cip来索引对应的pw*/
    atPwInfo.servMode = servType ;
    atPwInfo.clockMode = clkMode ;
    atPwInfo.clkState = clkState;
    atPwInfo.clkDomainId = domainNo;
    atPwInfo.masterE1LinkNo = 0 ;
    atPwInfo.jitterBufferSize = jitterBufferSize ;
    atPwInfo.frameNum = enc ;
    atPwInfo.vlan1Pri = 0;
    atPwInfo.vlan1Cfi = 0;
    atPwInfo.vlan1Info.zteVlan1Id = 640; 
    atPwInfo.vlan2Pri = 0;
    atPwInfo.vlan2Cfi = 0;
    atPwInfo.vlan2Info.zteVlan2Id = cip;
    memcpy(atPwInfo.destMac,dMac,sizeof(atPwInfo.destMac));

    if(SATOP_MODE== servType)
    {
        atPwInfo.payloadSize = enc *32 ;
    }

    ret = DrvAtPwAdd(&atPwInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, DrvAtPwAdd() ret=0x%x.\n", __FILE__, __LINE__, ret);
        return ret ;
    }    
    return ret ;
}

/*******************************************************************************
* 函数名称:   DrvAtPwAddDelLoopTest
* 功能描述:   时钟域功能测试，删除一条ACR/DCR 业务
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPwDelClkDomainTest(BYTE subcardId,WORD32 e1PortId,BYTE servType,BYTE clkMode,WORD32 cip,WORD32 tsBitmap)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    DRV_AT_PW_PARA atPwInfo ;
    memset(&atPwInfo, 0, sizeof(DRV_AT_PW_PARA));

    CHECK_AT_SUBCARD_ID(subcardId);
    
    ret = DrvAtPwInfoGetFromCip(subcardId-1,cip,&atPwInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, DrvAtPwInfoGetFromCip() ret=0x%x.\n", __FILE__, __LINE__, ret);
        return ret ;
    }    
    ret = DrvAtPwDelete(&atPwInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, DrvAtPwDelete() ret=0x%x.\n", __FILE__, __LINE__, ret);
        return ret ;
    }    
    
    return ret ;
    
}




/*###################### MLPPP 调试函数 ########################*/




/*******************************************************************************
* 函数名称:   DrvAtPppCreate
* 功能描述:   创建一条ppp link
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPppCreate(BYTE chipId,WORD32 e1LinkNo,WORD32 cip)
{
    WORD32 ret = DRV_AT_SUCCESS;
    DRV_AT_PPP_LINK_INFO pppLinkInfo;

    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    
    memset(&pppLinkInfo,0,sizeof(DRV_AT_PPP_LINK_INFO));
    
    pppLinkInfo.chipId = chipId;
    pppLinkInfo.e1LinkNo = e1LinkNo;
    pppLinkInfo.cip = cip;
    pppLinkInfo.fcsMode = AT_HDLC_FCS16;
    pppLinkInfo.isScrambleEn = CE1_DISABLE;
    pppLinkInfo.vlanTag1.cfi = 0;
    pppLinkInfo.vlanTag1.cpu = 0;
    pppLinkInfo.vlanTag1.encapType = 0;
    pppLinkInfo.vlanTag1.pri = 5;
    pppLinkInfo.vlanTag1.pktLen = 0;

    pppLinkInfo.vlanTag2.cfi = 0;
    pppLinkInfo.vlanTag2.isMlppp = 0;
    pppLinkInfo.vlanTag2.pri = 5;
    pppLinkInfo.vlanTag2.portNo = 0;
    pppLinkInfo.vlanTag2.chanNo = (WORD16)cip;
    
    ret = DrvAtPppLinkCreate(&pppLinkInfo);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, DrvAtPppLinkCreate() failed !!!CHIP ID IS %u ,E1 LINKNO IS %u\n", __FILE__, __LINE__,chipId,e1LinkNo); 
        return ret; 
    }
    DRV_AT_E1_PRINT(DRV_AT_ALL_PRINT,"ERROR: %s line %d, PPP CREATE OK !!!CHIP ID IS %u ,E1 LINKNO IS %u,CIP IS %u\n", __FILE__, __LINE__,chipId,e1LinkNo,cip); 
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtPppDel
* 功能描述:   删除一条 ppp link
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPppDel(BYTE chipId,WORD32 e1LinkNo)
{
    WORD32 ret = DRV_AT_SUCCESS;  
    DRV_AT_PPP_LINK_INFO pppLinkInfo;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_E1LINK_ID(e1LinkNo);
    memset(&pppLinkInfo,0,sizeof(DRV_AT_PPP_LINK_INFO));
    
    ret = DrvAtPppInfoGetFromE1(chipId,e1LinkNo,&pppLinkInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, CAN NOT GET PPP INFO ,CHIP ID IS %u,E1 LINKNO IS %u !!\n", __FILE__, __LINE__,chipId,e1LinkNo);
        return ret;
    }
    ret = DrvAtPppLinkDelete(&pppLinkInfo);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, DrvAtPppLinkDelete() FAILED ! !!CHIP ID IS %u ,E1 LINKNO IS %u\n", __FILE__, __LINE__,chipId,e1LinkNo); 
        return ret; 
    }
    DRV_AT_E1_PRINT(DRV_AT_ALL_PRINT,"ERROR: %s line %d, PPP DELETE OK !!!CHIP ID IS %u ,E1 LINKNO IS %u\n", __FILE__, __LINE__,chipId,e1LinkNo); 
    return ret;

}


/*******************************************************************************
* 函数名称:   DrvAtMlpppCreate
* 功能描述:   创建一个mlppp组
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtMlpppCreate(BYTE chipId,WORD32 bundleId,WORD32 mrru,WORD32 seqMode)
{
    WORD32 ret = DRV_AT_SUCCESS;
    
    CHECK_AT_CHIP_ID(chipId);
    
    DRV_AT_MLPPP_BUNDLE_INFO mlpppInfo;    
    memset(&mlpppInfo,0,sizeof(mlpppInfo));
    
    mlpppInfo.chipId = chipId;
    mlpppInfo.bundleId = bundleId;
    mlpppInfo.mrru = mrru;
    mlpppInfo.seqMode = seqMode;
        
    mlpppInfo.vlanTag1.pri= 5;
    mlpppInfo.vlanTag2.pri = 5;
    mlpppInfo.vlanTag1.cfi = 0;
    mlpppInfo.vlanTag2.cfi = 0;
    
    mlpppInfo.vlanTag1.pktLen = 0;
    mlpppInfo.vlanTag1.encapType = 0;
    mlpppInfo.vlanTag1.cpu = 0;
    
    mlpppInfo.vlanTag2.chanNo = (WORD16)(bundleId);
    mlpppInfo.vlanTag2.isMlppp = 1;
    mlpppInfo.vlanTag2.portNo = 0;
    
    ret = DrvAtMlpppBundleCreate(&mlpppInfo);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d,MLPPP CREATE FAILED !!CHIP ID IS %u, BUNDLE ID IS %u, RET IS %u\n", __FILE__, __LINE__,chipId,bundleId,ret);
        return ret;
    } 
    DRV_AT_E1_PRINT(DRV_AT_ALL_PRINT,"ERROR: %s line %d, MLPPP CREATE OK !!!CHIP ID IS %u ,BUNDLE ID  IS %u\n", __FILE__, __LINE__,chipId,bundleId);
    return ret;
}



/*******************************************************************************
* 函数名称:   DrvAtPppCreateOp
* 功能描述:   创建n条ppp link
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPppCreateOp(BYTE subcardId,WORD32 StartE1PortId,WORD32 endE1PortId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE chipId = 0;
    WORD32 e1LinkNo = 0;
    WORD32 StartE1LinkNo = StartE1PortId - 1;
    WORD32 endE1LnkNo = endE1PortId - 1;
    WORD32 cip = 0;
    
    CHECK_AT_SUBCARD_ID(subcardId);
    CHECK_AT_E1LINK_ID(StartE1LinkNo);
    CHECK_AT_E1LINK_ID(endE1LnkNo);
    
    chipId = subcardId - 1;
    for(e1LinkNo = StartE1LinkNo;e1LinkNo<= endE1LnkNo;e1LinkNo++)
    { 
        cip = e1LinkNo + 1;
        ret = DrvAtPppCreate(chipId,e1LinkNo,cip);   
        if(DRV_AT_SUCCESS != ret)
        {
            return ret;
        }
    }
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtPppDeleteOp
* 功能描述:   删除n条ppp link
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPppDeleteOp(BYTE subcardId,WORD32 StartE1PortId,WORD32 endE1PortId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE chipId = 0;
    WORD32 e1LinkNo = 0;
    WORD32 StartE1LinkNo = StartE1PortId - 1;
    WORD32 endE1LnkNo = endE1PortId - 1;
    
    CHECK_AT_SUBCARD_ID(subcardId);
    CHECK_AT_E1LINK_ID(StartE1LinkNo);
    CHECK_AT_E1LINK_ID(endE1LnkNo);
    
    chipId = subcardId - 1;
    for(e1LinkNo = StartE1LinkNo;e1LinkNo<= endE1LnkNo;e1LinkNo++)
    { 
        ret = DrvAtPppDel(chipId,e1LinkNo);   
        if(DRV_AT_SUCCESS != ret)
        {
            return ret;
        }
    }
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtMlpppCreateOp
* 功能描述:   mlppp测试，创建一个mlppp组，添加n条ppplink到一个mlppp组内
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtMlpppCreateOp(BYTE subcardId,WORD32 bundleId,WORD32 StartE1PortId,WORD32 endE1PortId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE chipId = 0;
    WORD32 e1LinkNo = 0;
    WORD32 StartE1LinkNo = StartE1PortId - 1;
    WORD32 endE1LnkNo = endE1PortId - 1;
    
    CHECK_AT_SUBCARD_ID(subcardId);
    CHECK_AT_E1LINK_ID(StartE1LinkNo);
    CHECK_AT_E1LINK_ID(endE1LnkNo);
    
    chipId = subcardId - 1;
    
    ret = DrvAtPppCreateOp(subcardId,StartE1PortId,endE1PortId);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d,DrvAtPppCreateOp() FAILED !!RET IS %u\n", __FILE__, __LINE__,ret);
        return ret;
    }
    ret = DrvAtMlpppCreate(chipId,bundleId,1500,AT_SEQ_MODE_LONG);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d,DrvAtMlpppCreate() FAILED !!RET IS %u\n", __FILE__, __LINE__,ret);
        return ret;
    }
    for(e1LinkNo = StartE1LinkNo;e1LinkNo<= endE1LnkNo;e1LinkNo++)
    { 
        ret = DrvAtMlpppBundleAddLink(chipId,bundleId,e1LinkNo);   
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d,DrvAtMlpppBundleAddLink() FAILED !!BUNDLE %u, E1 LINK %u,RET IS %u\n", __FILE__, __LINE__,bundleId,e1LinkNo,ret);
            return ret;
        }
    }   
    
    return ret;
}

/*******************************************************************************
* 函数名称:   DrvAtMlpppDeleteOp
* 功能描述:   将ppp 移除出mlppp组，并删除mlppp组
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtMlpppDeleteOp(BYTE subcardId,WORD32 bundleId,WORD32 StartE1PortId,WORD32 endE1PortId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE chipId = 0;
    WORD32 e1LinkNo = 0;
    WORD32 StartE1LinkNo = StartE1PortId - 1;
    WORD32 endE1LnkNo = endE1PortId - 1;
    
    CHECK_AT_SUBCARD_ID(subcardId);
    CHECK_AT_E1LINK_ID(StartE1LinkNo);
    CHECK_AT_E1LINK_ID(endE1LnkNo);
    
    for(e1LinkNo = StartE1LinkNo;e1LinkNo<= endE1LnkNo;e1LinkNo++)
    { 
        ret = DrvAtMlpppBundleRemoveLink(chipId,bundleId,e1LinkNo);   
        if(DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d,DrvAtMlpppBundleRemoveLink() FAILED !!BUNDLE %u, E1 LINK %u,RET IS %u\n", __FILE__, __LINE__,bundleId,e1LinkNo,ret);
            return ret;
        }
    }  
    
    ret = DrvAtPppDeleteOp(subcardId,StartE1PortId,endE1PortId);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d,DrvAtPppDeleteOp() FAILED !!RET IS %u\n", __FILE__, __LINE__,ret);
        return ret;
    }
    ret = DrvAtMlpppBundleDelete(chipId,bundleId);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d,DrvAtMlpppBundleDelete() FAILED !!RET IS %u\n", __FILE__, __LINE__,ret);
        return ret;
    }
    DRV_AT_E1_PRINT(DRV_AT_ALL_PRINT,"ERROR: %s line %d, MLPPP DELETE OK !!!CHIP ID IS %u ,BUNDLE ID  IS %u\n", __FILE__, __LINE__,chipId,bundleId);

    return ret;

}

/*******************************************************************************
* 函数名称:   DrvAtPppAddDelLoopTest
* 功能描述:   添加删除ppp业务测试
* 输入参数:   无
* 输出参数:   无
* 返 回 值:      无
* 其它说明:   其它说明
* 修改日期    版本号       修改人        修改内容
* ------------------------------------------------------------------------------
 * 2013-05-14     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtPppAddDelLoopTest(BYTE subcardId,WORD32 loopTime)
{
    WORD32 ret = DRV_AT_SUCCESS;
    WORD32 e1LinkNo = 0;
    WORD32 cip = 0;
    WORD32 i = 0;
    BYTE chipId = 0;
    chipId = subcardId - 1;
    
    CHECK_AT_CHIP_ID(chipId);
    
    while(i<loopTime)
    {
        for(e1LinkNo = 0;e1LinkNo<24 ;e1LinkNo++)
        {
            cip = e1LinkNo + 1;
            ret = DrvAtPppCreate(chipId,e1LinkNo,cip);   
            if(DRV_AT_SUCCESS != ret)
            {
                DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d,DrvAtPppCreate() FAILED !!E1LINKNO IS %u,RET IS %u\n", __FILE__, __LINE__,e1LinkNo,ret);
                return ret;
            }
        }
        
        for(e1LinkNo = 0;e1LinkNo<24 ;e1LinkNo++)
        {
            ret = DrvAtPppDel(chipId,e1LinkNo);   
            if(DRV_AT_SUCCESS != ret)
            {
                DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d,DrvAtPppDel() FAILED !!E1LINKNO IS %u,RET IS %u\n", __FILE__, __LINE__,e1LinkNo,ret);
                return ret;
            }
        }
        BSP_DelayMs(500);
        i++;
    }
    
    for(e1LinkNo = 0;e1LinkNo<24 ;e1LinkNo++)
    {
        cip = e1LinkNo + 1;
        ret = DrvAtPppCreate(chipId,e1LinkNo,cip);   
        if(DRV_AT_SUCCESS != ret)
        {
            return ret;
        }
    }
    
    return ret;
}



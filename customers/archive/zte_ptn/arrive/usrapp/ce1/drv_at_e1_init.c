/*************************************************************************
* ��Ȩ����(C)2013, ����ͨѶ�ɷ����޹�˾.
*
* �ļ�����: drv_at_e1_init.c
* �ļ���ʶ: 
* ����˵��: ce1tan ��ʼ������
* ��ǰ�汾: V1.0
* ��    ��: �²�10140635.
* �������: 2013-04-25
*
* �޸ļ�¼: 
*    �޸�����: 
*    �汾��: V1.0
*    �޸���: 
*    �޸�����: create
**************************************************************************/
#include "AtDriver.h"
#include "AtDevice.h"
#include "AtModulePdh.h"
#include "AtModuleEth.h"
#include "AtHalZtePtn.h"
#include "AtList.h"
#include "drv_at_e1_init.h"
#include "drv_at_e1_port.h"
#include "drv_at_e1_pwe3.h"
#include "drv_at_e1_clk.h"
#include "drv_at_e1_pm.h"
#include "drv_at_e1_access.h"
#include "drv_at_e1_mlppp.h"
#include "drv_pwe3_main.h"
#include "bsp_oal_api.h"
#include "AtTextUI.h"




/*��̬����*/
static BYTE   s_isSimulation = cAtFalse;             /*cAtTrue:simulation mode   cAtFalse: board mode*/
static WORD32 s_simulateProductCode = 0x60031021;    /* Support product: 60031021 (E1 SAToP, CESoP) and 60031031 (STM SAToP, CESoP) */

/*ȫ�ֱ���*/
WORD32 g_at_print = 0;  /* ���Դ�ӡ���� */
WORD32 g_atProductCode[AT_MAX_SUBCARD_NUM_PER_SLOT];
AtDevice g_atDevHandle[AT_MAX_SUBCARD_NUM_PER_SLOT] = {NULL,NULL,NULL,NULL};      /*at оƬ��оƬ��� */
AtHal g_atDevHal[AT_MAX_SUBCARD_NUM_PER_SLOT] = {NULL,NULL,NULL,NULL};            /*at оƬhal*/
WORD32 g_atDevInitFlag[AT_MAX_SUBCARD_NUM_PER_SLOT] ; 
DRV_AT_BASE_ADDR g_ce1BaseAddr[AT_MAX_SUBCARD_NUM_PER_SLOT] ;
CE1TAN_THREAD_HANDLE g_threadHdl[AT_MAX_SUBCARD_NUM_PER_SLOT];
CE1TAN_THREAD_ID g_threadId[AT_MAX_SUBCARD_NUM_PER_SLOT];
CE1TAN_THREAD_FLAG g_threadFlag[AT_MAX_SUBCARD_NUM_PER_SLOT];

/*�ⲿȫ�ֱ���*/
extern tCmdConf cmdConf[];
extern int cmdConfCount ;
extern struct mod_thread g_atE1AlmThrMod[AT_MAX_SUBCARD_NUM_PER_SLOT];
extern struct mod_thread g_atE1ErrThrMod[AT_MAX_SUBCARD_NUM_PER_SLOT];
extern struct mod_thread g_pwCountThrMod[AT_MAX_SUBCARD_NUM_PER_SLOT];
extern struct mod_thread g_pwAlarmThrMod[AT_MAX_SUBCARD_NUM_PER_SLOT];
extern DRV_AT_PW_PARA  g_atE1PwInfo [AT_MAX_SUBCARD_NUM_PER_SLOT][AT_MAX_PW_NUM_PER_SUBCARD] ;  /*����pw ��Ϣ����������ӿ�*/
extern DRV_AT_PW_COUNT g_atE1PwCount[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_MAX_PW_NUM_PER_SUBCARD];    /*pw ���ļ���*/
extern DRV_AT_PW_COUNT g_atE1PwCountSum[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_MAX_PW_NUM_PER_SUBCARD]; /*pw �����ۼӼ���*/
extern DRV_AT_PW_COUNT g_atE1PwCountToPm[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_MAX_PW_NUM_PER_SUBCARD]; 
extern WORD16 g_atE1PwTotalNum[AT_MAX_SUBCARD_NUM_PER_SLOT];                                    /*�ӿ��ϴ��ڵ�pw����ͳ��*/
extern WORD32 g_atCesopLinksNum[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_E1_INTF_NUM];                    /*cesopģʽ��һ��e1���ͨ����*/
extern AT_E1_PORT_STATS g_atE1PortStats[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_E1_INTF_NUM];
extern PDH_E1_ALARM  g_atPdhE1Alarm[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_E1_INTF_NUM] ;
extern PDH_E1_ALARM g_atPdhE1HisAlm[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_E1_INTF_NUM] ;
extern PDH_E1_ERR_COUNT g_atPdhE1ErrCount[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_E1_INTF_NUM];
extern PDH_E1_ERR_COUNT g_atPdhE1ErrCountSum[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_E1_INTF_NUM];
extern PDH_E1_ERR_COUNT g_atPdhE1ErrCountToPm[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_E1_INTF_NUM];
extern AT_CLK_DOMAIN_INFO g_ClkDomainInfo[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_MAX_DOMAIN_NUM_PER_SUBCARD] ;
extern WORD32  g_isPwClkAlarm[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_MAX_PW_NUM_PER_SUBCARD];
extern DRV_AT_PPP_LINK_INFO g_atPppLinkInfo[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_MAX_PPP_NUM_PER_SUBCARD];
extern DRV_AT_MLPPP_BUNDLE_INFO g_atMlpppBundleInfo[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_MAX_MLPPP_BUNDLE_NUM_PER_SUBCARD];
extern WORD32  g_atBundleLinksNum[AT_MAX_SUBCARD_NUM_PER_SLOT][AT_MAX_MLPPP_BUNDLE_NUM_PER_SUBCARD];

/*�ⲿ����*/
extern WORD32 BSP_Is_MainCard_Hpfudz(void);
extern WORD32 BSP_IS_FPGA_DOWNLOAD_FINISH(BYTE chipId);
extern WORD32 BSP_CE1TAN_BOARD_TYPE_GET(BYTE subcardId);
extern WORD32 DrvE1AlarmThreadLaunch(BYTE chipId);
extern WORD32 DrvE1ErrorThreadLaunch(BYTE chipId);
extern WORD32 DrvPwCountThreadLaunch(BYTE chipId);
extern WORD32 DrvPwCountThreadDelete(BYTE chipId);
extern WORD32 DrvE1ErrorThreadDelete(BYTE chipId);
extern WORD32 DrvE1AlarmThreadDelete(BYTE chipId);

/*******************************************************************************
* ��������:   DrvAtDevHdlGet
* ��������:   ��ȡоƬ���
* �������:   ��
* �������:   ��
* �� �� ֵ:   ��
* ����˵��:   ����˵��
* �޸�����              �汾��       �޸���        �޸�����
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
AtDevice DrvAtDevHdlGet(BYTE chipId)
{
    CHECK_AT_CHIP_ID(chipId);
    
    return g_atDevHandle[chipId];
}

/*******************************************************************************
* ��������:   DrvAtCe1tanBaseAddrGet
* ��������:   ��ȡ�ӿ��Ļ���ַ
* �������:   ��
* �������:   ��
* �� �� ֵ:      ��
* ����˵��:   ����˵��
* �޸�����    �汾��       �޸���        �޸�����
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtCe1tanBaseAddrGet(BYTE chipId,DRV_AT_BASE_ADDR* baseAddr)
{
    WORD32 ret = DRV_AT_SUCCESS;
    
    CHECK_AT_CHIP_ID(chipId);
    CHECK_AT_POINTER_NULL(baseAddr);

    baseAddr->epldBaseAddr  = g_ce1BaseAddr[chipId].epldBaseAddr;
    baseAddr->fpgaBaseAddr = g_ce1BaseAddr[chipId].fpgaBaseAddr;
    baseAddr->liuBaseAddr = g_ce1BaseAddr[chipId].liuBaseAddr;
    
    return ret;
}


/*******************************************************************************
* ��������:   DrvAtFpgaAddrGet
* ��������:   ��ȡCE1 �ӿ��Ļ���ַ
* �������:   ��
* �������:   ��
* �� �� ֵ:   ��
* ����˵��:   ����˵��
* �޸�����              �汾��       �޸���        �޸�����
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtFpgaAddrGet(BYTE chipId,BYTE coreId,WORD32 *ce1Addr)
{  
    WORD32 ret = DRV_AT_SUCCESS ;
    CHECK_AT_POINTER_NULL(ce1Addr);
       
    switch(chipId)
    {
        case 0 :    
            if(0 == coreId)
            {
                *ce1Addr =  g_ce1BaseAddr[chipId].fpgaBaseAddr ;
            }
            else if (1 == coreId)
            {
                *ce1Addr =  AT_CHIP_ID0_CORE_ID1_BASE_ADDRESS;
            }
         break ;
        case 1 :
            if(0 == coreId)
            {
                *ce1Addr = g_ce1BaseAddr[chipId].fpgaBaseAddr  ;
            }
            else if (1 == coreId)
            {
                *ce1Addr = AT_CHIP_ID1_CORE_ID1_BASE_ADDRESS;
            }
          break ;
        case 2:
            if(0 == coreId)
            {
                *ce1Addr = g_ce1BaseAddr[chipId].fpgaBaseAddr  ;
            }
            else if (1 == coreId)
            {
                *ce1Addr = AT_CHIP_ID2_CORE_ID1_BASE_ADDRESS;
            }
         break ;
        case 3:
            if(0 == coreId)
            {
                *ce1Addr = g_ce1BaseAddr[chipId].fpgaBaseAddr  ;
            }
            else if (1 == coreId)
            {
                *ce1Addr = AT_CHIP_ID3_CORE_ID1_BASE_ADDRESS;
            }
          break ;
        default:
            *ce1Addr = AT_CORE_INVALID_BASE_ADDRESS ;
            break ;
    }
    if( AT_CORE_INVALID_BASE_ADDRESS == (*ce1Addr))
    {
        ret = DRV_AT_BASE_ADDR_GET_ERROR ;
    }
    return ret ;
}


/*******************************************************************************
* ��������:   DrvAtBaseAddrGet
* ��������:   ��ȡ����ַ
* �������:   ��
* �������:   ��
* �� �� ֵ:   ��
* ����˵��:   ����˵��
* �޸�����              �汾��       �޸���        �޸�����
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtBaseAddrGet(BYTE cardType,BYTE chipId,BYTE coreId,WORD32 *baseAddr)
{  
    WORD32 ret = DRV_AT_BASE_ADDR_GET_ERROR;
    CHECK_AT_POINTER_NULL(baseAddr);
    
    if (s_isSimulation)   /*if in sim mode ,no need to get base addr*/
    {
        return DRV_AT_SUCCESS;
    }
    switch (cardType)
    {
        case CE1TAN :  /*24 E1*/
            ret = DrvAtFpgaAddrGet(chipId,coreId,baseAddr);
            break ;    
        case CP3BAN:   
            
        default:
            break ;       
    }
    return ret;
}


/*******************************************************************************
* ��������:   DrvAtCreateHal
* ��������:   �½�һ��HAL 
* �������:   ��
* �������:   ��
* �� �� ֵ:   ��
* ����˵��:   ����˵��
* �޸�����              �汾��       �޸���        �޸�����
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
static AtHal DrvAtCreateHal(BYTE subcardId, WORD32 baseAddress)
{
    if (s_isSimulation)                    /*in sim mode*/           
    {
        return AtHalSimGlobalHoldNew(cAtHalSimDefaultMemorySizeInMbyte);
    }
    if(CE1_FALSE == BSP_Is_MainCard_Hpfudz())
    {
        return AtHalZtePtnNew(baseAddress);/*15k-2,access directly*/
    }
    else if(CE1_TURE == BSP_Is_MainCard_Hpfudz())
    {
        return AtHalZtePtnEpldNew((WORD32)subcardId, ArriveFpga8Read, ArriveFpga8Write);/*15k-8,access indirectly*/
    }
    else
    {
        return NULL;
    }
}

/*******************************************************************************
* ��������:   DrvAtProductCodeGet
* ��������:   ��ȡproduct code
* �������:   ��
* �������:   ��
* �� �� ֵ:   ��
* ����˵��:   ����˵��
* �޸�����              �汾��       �޸���        �޸�����
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtProductCodeGet(BYTE chipId)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    WORD32 productCode = 0;
    WORD32 baseAddr = 0;
    BYTE subcardId = 0;
    AtHal atDevHal = NULL;
    
     /*in sim mode*/
    if (s_isSimulation)    
    {
        return s_simulateProductCode;
    }
    /*in board mode*/
    ret = DrvAtBaseAddrGet(CE1TAN, chipId, 0,&baseAddr);    
    if (DRV_AT_BASE_ADDR_GET_ERROR == ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d,  THE BASE ADDRESS IS INVALID  !!CHIP ID IS %u \n", __FILE__, __LINE__,chipId); 
        return ret;
    }
    subcardId = chipId +1;
    atDevHal = DrvAtCreateHal(subcardId,baseAddr);
    if(NULL == atDevHal)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, HAL CREATE FAILED  !!CHIP ID IS %u \n", __FILE__, __LINE__,chipId); 
        return DRV_AT_HAL_CREATE_FAILED; 
    }
    productCode = AtHalRead(atDevHal, 0);
    g_atProductCode[chipId] = productCode;
    AtHalDelete(atDevHal);  
    if (!AtDriverProductCodeIsValid(AtDriverSharedDriverGet(), productCode))  /**/
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtDriverProductCodeIsValid FAILED  !!\n", __FILE__, __LINE__);
    }
    return productCode;
}


/*******************************************************************************
* ��������:   DrvAtInstallHalForCore
* ��������:  ��װHAL
* �������:   ��
* �������:   ��
* �� �� ֵ:   ��
* ����˵��:   ����˵��
* �޸�����              �汾��       �޸���        �޸�����
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtInstallHalForCore(BYTE chipId,BYTE coreId)
{  
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE subcardId = 0;
    eAtRet atRet = cAtOk;                                         /*sdk�ķ���ֵ����*/
    WORD32 baseAddr = 0;
    AtIpCore ipCore = NULL ; 
    
    ret = DrvAtBaseAddrGet(CE1TAN,chipId,coreId,&baseAddr) ;
    if (DRV_AT_BASE_ADDR_GET_ERROR == ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d,  THE BASE ADDRESS IS INVALID  !!CHIP ID IS %u \n", __FILE__, __LINE__,chipId); 
        return ret;
    }
    subcardId = chipId +1;
    if(NULL == g_atDevHal[chipId])
    {
        g_atDevHal[chipId] = DrvAtCreateHal(subcardId,baseAddr);
        if(NULL == g_atDevHal[chipId])
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, HAL CREATE FAILED  !!CHIP ID IS %u \n", __FILE__, __LINE__,chipId); 
            return DRV_AT_HAL_CREATE_FAILED; 
        }
    }
    ipCore = AtDeviceIpCoreGet(g_atDevHandle[chipId], coreId) ;
    if(NULL == ipCore)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, IP CORE GET FAILED !!\n", __FILE__, __LINE__); 
        return DRV_AT_CORE_GET_FAILED; 
    }
    atRet = AtIpCoreHalSet(ipCore, g_atDevHal[chipId]);
    if (cAtOk != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AtIpCoreHalSet() ret =%s. CHIP ID : %u  ,CORE ID :%u .\n", __FILE__, __LINE__, AtRet2String(atRet), chipId, coreId);
        return DRV_AT_HAL_SET_FAILED;
    }
    return ret ;
}


/*******************************************************************************
* ��������:   DrvAtDevCreate
* ��������:   ����оƬ����
* �������:   ��
* �������:   ��
* �� �� ֵ:   ��
* ����˵��:   ����˵��
* �޸�����              �汾��       �޸���        �޸�����
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvAtDevCreate(BYTE chipId)
{
    WORD32 ret = DRV_AT_SUCCESS;  /* ���������� */
    WORD32 productCode = 0;
    eAtRet atRet = cAtOk ;
    BYTE coreId = 0;
    BYTE coreNum = 0 ;
    BYTE subcardNumPerLc = 0;        /*ÿ���߿����ӿ���*/
    AtDriver atDevDrvr = NULL ;
    
    /* Create Driver  */
    subcardNumPerLc = (BSP_Is_MainCard_Hpfudz() == CE1_TURE)?4:1;
    /* ��ȡ�ź��� */
    ret = drv_pwe3_initSemaphoreGet();
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, drv_pwe3_initSemaphoreGet() ret=0x%x.\n", __FILE__, __LINE__, ret);
        return ret;
    }
    atDevDrvr = AtDriverSharedDriverGet();
    if(NULL == atDevDrvr)
    { 
        atDevDrvr = AtDriverCreate(subcardNumPerLc, AtOsalLinux());
        if(NULL == atDevDrvr)
        {
            /* �ͷ��ź��� */
            ret = drv_pwe3_initSemaphoreFree();
            if (DRV_AT_SUCCESS != ret)
            {
                DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, drv_pwe3_initSemaphoreFree() ret=0x%x.\n", __FILE__, __LINE__, ret);
                return ret;
            }
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AT DRVIVER CREATE FAILED  !!\n", __FILE__, __LINE__); 
            return DRV_AT_DEV_CREATE_ERROR; 
        }
        else
        {
            ROSNG_TRACE_DEBUG("\nAT DRIVER CREATE SUCCESS !!THE CHIP ID IS %u .\n", chipId); 
        }
    }
    /* �ͷ��ź��� */
    ret = drv_pwe3_initSemaphoreFree();
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, drv_pwe3_initSemaphoreFree() rv=0x%x.\n", __FILE__, __LINE__, ret);
        return ret;
    }
    /* Create device, HAL will depend on each device */
    if(NULL == g_atDevHandle[chipId])
    {
        productCode = DrvAtProductCodeGet(chipId);
        g_atDevHandle[chipId] = AtDriverDeviceCreate(atDevDrvr, productCode);    
        if(NULL == g_atDevHandle[chipId])
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AT DEVICE HANDLE GET FAILED  !!\n", __FILE__, __LINE__); 
            return DRV_AT_DEV_HANDLE_GET_FAILED; 
        }
    }
    
    /* Install HAL for two IP cores */
    coreNum = AtDeviceNumIpCoresGet(g_atDevHandle[chipId]);
    for (coreId = 0; coreId < coreNum ; coreId++)
    {
        ret = DrvAtInstallHalForCore(chipId,coreId);
        if (DRV_AT_SUCCESS != ret)
        {
           DRV_AT_E1_ERROR_LOG("ERROR: %s line %d,   AT DEVICE HAL INSTALL FAILED !!CHIP ID : %u  ,CORE ID :%u .\n", __FILE__, __LINE__, chipId, coreId);
           return ret;
        }        
        else
        {
            ROSNG_TRACE_DEBUG("\nAT DEVICE HAL INSTALL SUCCESS !!THE CHIP ID IS %u  THE CORE ID IS %u .\n", chipId, coreId); 
        }
    } 
    atRet = AtDeviceInit(g_atDevHandle[chipId]);  /*init device*/
    if (cAtOk!= atRet)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d,   AtDeviceInit() failed !ret = %s \n", __FILE__, __LINE__,AtRet2String(atRet));
        return ret;
    }        
    return ret;
}

/*******************************************************************************
* ��������:   DrvAtDevCleanUp
* ��������:   ж��оƬ����
* �������:   ��
* �������:   ��
* �� �� ֵ:   ��
* ����˵��:   ����˵��
* �޸�����              �汾��       �޸���        �޸�����
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvAtDevCleanUp(BYTE chipId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE coreId = 0;
    eAtRet atRet = cAtOk;
    AtList atHals = NULL;
    AtDevice atDevHdl = NULL;
    AtDriver atDriver = NULL;
    AtHal atHal = NULL;
       
    CHECK_AT_CHIP_ID(chipId);
    atDriver = AtDriverSharedDriverGet();
    atDevHdl = g_atDevHandle[chipId];
    CHECK_AT_POINTER_NULL(atDriver);
    CHECK_AT_POINTER_NULL(atDevHdl);
    
    /* All HALs that are installed so far */
    atHals = AtListCreate(0);
    if( NULL == atHals)
    {
        DRV_AT_E1_ERROR_LOG("AT HAL LIST CREATE FAILED!! CHIP ID IS %u\n",chipId);
        return DRV_AT_HAL_LIST_CREATE_FAILED;
    }
    for (coreId = 0; coreId < AtDeviceNumIpCoresGet(atDevHdl); coreId++)
    {
        atHal = AtIpCoreHalGet(AtDeviceIpCoreGet(atDevHdl, coreId));
        if(NULL == atHal)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, HAL GET FAILED  !!CHIP ID IS %u \n", __FILE__, __LINE__,chipId); 
            return DRV_AT_HAL_GET_FAILED; 
        }
        if (AtListContainsObject(atHals, (AtObject)atHal)!= cAtTrue)
        {
            atRet = AtListObjectAdd(atHals, (AtObject)atHal);
            if(cAtOk != atRet)
            {
                DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, HAL LIST ADD FAILED  !!CHIP ID IS %u ret=%s.\n", __FILE__, __LINE__,chipId,AtRet2String(atRet)); 
                return DRV_AT_HAL_LIST_ADD_FAILED;   
            }
        }
    }
    AtDriverDeviceDelete(atDriver, atDevHdl);    
    /* Delete driver then delete all saved HALs */
    if(1 == drv_pwe3_subcardCntGet())
    {
        AtDriverDelete(atDriver);
    }
    while (AtListLengthGet(atHals) > 0)
    {
        AtHalDelete((AtHal)AtListObjectRemoveAtIndex(atHals, 0));
        AtObjectDelete((AtObject)atHals);
    }
    /*release resource*/
    g_atDevHandle[chipId] = NULL ;
    g_atDevHal[chipId] = NULL;
    g_atProductCode[chipId] = NULL;
    
    return ret;
}

/*******************************************************************************
* ��������:   DrvThreadFlagset
* ��������:   �����̱߳�־λ
* �������:   flag:0��ʾdisable��1��ʾenable
* �������:   ��
* �� �� ֵ:   ��
* ����˵��:   ����˵��
* �޸�����              �汾��       �޸���        �޸�����
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvThreadFlagset(BYTE chipId,BYTE flagType,WORD32 flag)
{
    WORD32 ret = DRV_AT_SUCCESS;
    
    switch(flagType)
    {
        case E1_ALARM_THREAD:
            g_threadFlag[chipId].E1_ALARM_THR_FLAG = flag ;
            break;
            
        case E1_ERROR_THREAD:
            g_threadFlag[chipId].E1_ERROR_THR_FLAG = flag ;
            break;
            
        case PW_COUNT_THREAD:
            g_threadFlag[chipId].PW_COUNT_THR_FLAG = flag ;
            break;
            
        case ALL_THREAD:
            g_threadFlag[chipId].E1_ALARM_THR_FLAG = flag ;
            g_threadFlag[chipId].E1_ERROR_THR_FLAG = flag ;
            g_threadFlag[chipId].PW_COUNT_THR_FLAG = flag ;
            break ;            
    default:
        break;
    }

    return ret ;

}

/*******************************************************************************
* ��������:   DrvThreadLaunch
* ��������:   ���߳�
* �������:   ��
* �������:   ��
* �� �� ֵ:   ��
* ����˵��:   ����˵��
* �޸�����              �汾��       �޸���        �޸�����
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvThreadLaunch(BYTE chipId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    
    CHECK_AT_CHIP_ID(chipId);
    
    ret = DrvE1AlarmThreadLaunch(chipId);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PDH ALARM THREAD LAUNCH FAILED !!CHIP ID : %u  .\n", __FILE__, __LINE__, chipId);
        return ret;     
    } 
    BSP_DelayMs(50);
    ret = DrvE1ErrorThreadLaunch(chipId);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PDH ERR THREAD LAUNCH FAILED !!CHIP ID : %u  .\n", __FILE__, __LINE__, chipId);
        return ret;
    }
    BSP_DelayMs(50);
    ret = DrvPwCountThreadLaunch(chipId);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PW COUNT THREAD LAUNCH FAILED !!CHIP ID : %u  .\n", __FILE__, __LINE__, chipId);
        return ret;
    }
    return ret ;
}

/*******************************************************************************
* ��������:   DrvThreadDelete
* ��������:   ɾ���߳�
* �������:   ��
* �������:   ��
* �� �� ֵ:   ��
* ����˵��:   ����˵��
* �޸�����              �汾��       �޸���        �޸�����
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvThreadDelete(BYTE chipId)
{
    WORD32 ret = DRV_AT_SUCCESS;
    
    CHECK_AT_CHIP_ID(chipId);
    
    ret = DrvE1AlarmThreadDelete(chipId);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PDH ALARM THREAD Delete FAILED !!CHIP ID : %u  .\n", __FILE__, __LINE__, chipId);
        return ret;     
    } 
    ret = DrvE1ErrorThreadDelete(chipId);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PDH ERR THREAD Delete FAILED !!CHIP ID : %u  .\n", __FILE__, __LINE__, chipId);
        return ret;
    }
    ret = DrvPwCountThreadDelete(chipId);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PW COUNT THREAD Delete FAILED !!CHIP ID : %u  .\n", __FILE__, __LINE__, chipId);
        return ret;
    }
    
    return ret ;
}

/*******************************************************************************
* ��������:   DrvAtMemClear
* ��������:   ����ڴ�
* �������:   ��
* �������:   ��
* �� �� ֵ:   ��
* ����˵��:   ����˵��
* �޸�����              �汾��       �޸���        �޸�����
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvAtMemClear(BYTE chipId)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    
    memset(&g_ce1BaseAddr[chipId],0,sizeof(DRV_AT_BASE_ADDR));
    memset(&g_threadHdl[chipId],0,sizeof(CE1TAN_THREAD_HANDLE));
    memset(&g_threadId[chipId],0,sizeof(CE1TAN_THREAD_ID));
    memset(&g_threadFlag[chipId],0,sizeof(CE1TAN_THREAD_FLAG));
    memset(&g_atE1AlmThrMod[chipId],0,sizeof(struct mod_thread));
    memset(&g_atE1ErrThrMod[chipId],0,sizeof(struct mod_thread));    
    memset(&g_pwCountThrMod[chipId],0,sizeof(struct mod_thread));
    memset(g_atPdhE1Alarm[chipId],0,AT_E1_INTF_NUM * sizeof(PDH_E1_ALARM));
    memset(g_atPdhE1HisAlm[chipId],0,AT_E1_INTF_NUM * sizeof(PDH_E1_ALARM));
    memset(g_atPdhE1ErrCount[chipId],0,AT_E1_INTF_NUM * sizeof(PDH_E1_ERR_COUNT));
    memset(g_atPdhE1ErrCountSum[chipId],0,AT_E1_INTF_NUM * sizeof(PDH_E1_ERR_COUNT));
    memset(g_atPdhE1ErrCountToPm[chipId],0,AT_E1_INTF_NUM * sizeof(PDH_E1_ERR_COUNT));
    memset(g_atE1PortStats[chipId],0,AT_E1_INTF_NUM * sizeof(AT_E1_PORT_STATS));
    memset(g_atE1PwInfo[chipId],0,AT_MAX_PW_NUM_PER_SUBCARD * sizeof(DRV_AT_PW_PARA));
    memset(g_atE1PwCount[chipId],0,AT_MAX_PW_NUM_PER_SUBCARD * sizeof(DRV_AT_PW_COUNT));
    memset(g_atE1PwCountSum[chipId],0,AT_MAX_PW_NUM_PER_SUBCARD * sizeof(DRV_AT_PW_COUNT));
    memset(g_atE1PwCountToPm[chipId],0,AT_MAX_PW_NUM_PER_SUBCARD * sizeof(DRV_AT_PW_COUNT));
    memset(g_atCesopLinksNum[chipId],0,AT_E1_INTF_NUM * sizeof(WORD32));
    memset(g_isPwClkAlarm[chipId],0,AT_MAX_PW_NUM_PER_SUBCARD*sizeof(WORD32));
    memset(g_ClkDomainInfo[chipId],0,AT_MAX_DOMAIN_NUM_PER_SUBCARD * sizeof(AT_CLK_DOMAIN_INFO));
    memset(g_atPppLinkInfo[chipId],0,AT_MAX_PPP_NUM_PER_SUBCARD * sizeof(DRV_AT_PPP_LINK_INFO));
    memset(g_atMlpppBundleInfo[chipId],0,AT_MAX_MLPPP_BUNDLE_NUM_PER_SUBCARD * sizeof(DRV_AT_MLPPP_BUNDLE_INFO));
    memset(g_atBundleLinksNum[chipId],0,AT_MAX_MLPPP_BUNDLE_NUM_PER_SUBCARD * sizeof(WORD32));
    g_atE1PwTotalNum[chipId] = 0;
        
    return ret;
}


/*******************************************************************************
* ��������:   DrvAtDevRemove
* ��������:   �Ƴ��豸
* �������:   ��
* �������:   ��
* �� �� ֵ:   ��
* ����˵��:   ����˵��
* �޸�����              �汾��       �޸���        �޸�����
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/ 
WORD32 DrvAtDevRemove(BYTE subcardId)
{ 
    WORD32 ret = DRV_AT_SUCCESS;
    BYTE chipId = subcardId -1 ;
    
    CHECK_AT_CHIP_ID(chipId);
    
    if(CE1_DISABLE == g_atDevInitFlag[chipId])
    { 
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, DEVICE HAS ALREADY REMOVE !! CHIP ID IS %u .\n", __FILE__, __LINE__, chipId);
        return ret ;    
    }
    /*set thread falg disable*/
    DrvThreadFlagset(chipId,ALL_THREAD,CE1_DISABLE);
    BSP_DelayMs(50);
    /*delete at device*/
    ret = DrvAtDevCleanUp(chipId);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, DELETE CHIP FAILED !!CHIP ID : %u  .\n", __FILE__, __LINE__, chipId);
        return ret;
    } 
    /*delete thread*/
    ret = DrvThreadDelete(chipId);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, DELETE THREAD FAILED !!CHIP ID : %u  .\n", __FILE__, __LINE__, chipId);
        return ret;
    }
    /*clear memory*/
    ret = DrvAtMemClear(chipId);
    if(DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, MEMORY FREE FAILED !!CHIP ID : %u  .\n", __FILE__, __LINE__, chipId);
        return ret;
    }
    ret = drv_pwe3_cardCntSemaphoreGet();  /* ��ȡ�ź��� */
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG( "ERROR: %s line %d, drv_pwe3_cardCntSemaphoreGet() ret=0x%x.\n", __FILE__, __LINE__, ret);
        return ret;
    }
    drv_pwe3_subcardCntSet(DRV_TDM_SUBCARD_DECREASE);  /* ����խ���ӿ������� */
    ret = drv_pwe3_cardCntSemaphoreFree();  /* �ͷ��ź��� */
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG( "ERROR: %s line %d, drv_pwe3_cardCntSemaphoreFree() ret=0x%x.\n", __FILE__, __LINE__, ret);
        return ret;
    }
    /* ��û�нӿڿ�ʱ,��Ҫ�����ź���. */
    if (0 == drv_pwe3_subcardCntGet())  
    {
         ret = drv_pwe3_initSemaphoreDestroy();
         if (DRV_AT_SUCCESS != ret)
         {
             DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, drv_pwe3_initSemaphoreDestroy() ret=0x%x.\n", __FILE__, __LINE__, ret);
             return ret;
         }
         ret = drv_pwe3_cardCntSemaphoreDestroy();
         if (DRV_AT_SUCCESS != ret)
         {
             DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, drv_pwe3_cardCntSemaphoreDestroy() ret=0x%x.\n", __FILE__, __LINE__, ret);
             return ret;
         }
    }
    g_atDevInitFlag[chipId] = CE1_DISABLE;      /*��ʼ����־λ����*/
    return ret ;
}


/*******************************************************************************
* ��������:   DrvAtDevInit
* ��������:   arrive FPGA ��ʼ������
* �������:   ��
* �������:   ��
* �� �� ֵ:   ��
* ����˵��:   ����˵��
* �޸�����              �汾��       �޸���        �޸�����
* ------------------------------------------------------------------------------
 * 2013-04-25     v1.0        chenbo    create
********************************************************************************/
WORD32 DrvAtDevInit(BYTE subcardId)
{
    WORD32 ret = DRV_AT_SUCCESS ;
    BYTE chipId = subcardId - 1;
    WORD32 boardType = 0;
    
    CHECK_AT_CHIP_ID(chipId);    

    if(CE1_ENABLE == g_atDevInitFlag[chipId]) /*check if device has already init?*/
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, DEVICE HAS ALREADY INIT !! CHIP ID IS %u .\n", __FILE__, __LINE__, chipId);
        return ret ;    
    }
    if(CE1_FALSE == BSP_IS_FPGA_DOWNLOAD_FINISH(subcardId))/*check if fpga image has already download?*/
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PLEASE DOWNLOAD FPGA FIRST !! CHIP ID IS %u .\n", __FILE__, __LINE__, chipId);
        return DRV_AT_FPGA_NOT_DOWNLOAD;
    }
    ret = DrvAtDevCreate(chipId); /*create driver and device */
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AT DEVICE INIT  FAILED !! RET= 0x%x, CHIP ID IS %u .\n", __FILE__, __LINE__, ret, chipId);
        return ret;
    }    
    ret = DrvAtE1IntfInit(chipId);/*init e1 interface */
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AT DEVICE E1 INTF INIT FAILED !! RET= 0x%x, CHIP ID IS %u .\n", __FILE__, __LINE__, ret, chipId);
        return ret;
    }    
    ret = DrvAtEthIntfInit(chipId,0);/*init eth interface*/ 
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AT DEVICE ETH INTF INIT FAILED !! RET= 0x%x, CHIP ID IS %u .\n", __FILE__, __LINE__, ret, chipId);
        return ret;
    }
    /*�����nbic��Ҫ��ʼ��2����̫��*/
    if(CE1_FALSE == BSP_Is_MainCard_Hpfudz())
    {
        ret = DrvAtEthIntfInit(chipId,1);/*init eth interface*/ 
        if (DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, AT DEVICE ETH INTF INIT FAILED !! RET= 0x%x, CHIP ID IS %u .\n", __FILE__, __LINE__, ret, chipId);
            return ret;
        }
    }
    /*��ȡ�����ͣ������mlpppҵ��������pid*/
    boardType = BSP_CE1TAN_BOARD_TYPE_GET(subcardId);
    if((CE1TAN_75_OHM_DZ_MLPPP_BOARD== boardType)
    ||(CE1TAN_75_OHM_NBIC_MLPPP_BOARD == boardType)
    ||(CE1TAN_120_OHM_DZ_MLPPP_BOARD == boardType)
    ||(CE1TAN_120_OHM_NBIC_MLPPP_BOARD == boardType))
    {
        ret = DrvAtPidSet(chipId);
        if (DRV_AT_SUCCESS != ret)
        {
            DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, PID SET FAILED !! RET= 0x%x, CHIP ID IS %u .\n", __FILE__, __LINE__, ret, chipId);
            return ret;
        }
    }
    DrvThreadFlagset(chipId,ALL_THREAD,CE1_ENABLE);/*set thread flag enable*/    
    ret = DrvThreadLaunch(chipId);   /*launch thread*/
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, THREAD LAUNCH FAILED !! RET= 0x%x, CHIP ID IS %u .\n", __FILE__, __LINE__, ret, chipId);
        return ret;
    }
    ret= drv_pwe3_cardCntSemaphoreGet();  /*��ȡ�ź���*/ 
    if (DRV_PWE3_OK != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, drv_pwe3_cardCntSemaphoreGet() ret=0x%x.\n", __FILE__, __LINE__, ret);
        return ret;
    }
    drv_pwe3_subcardCntSet(DRV_TDM_SUBCARD_INCREASE);   /*����խ���ӿ�������*/
    ret = drv_pwe3_cardCntSemaphoreFree();   /*�ͷ��ź���*/ 
    if (DRV_PWE3_OK != ret)
    {
        DRV_AT_E1_ERROR_LOG("ERROR: %s line %d, drv_pwe3_cardCntSemaphoreFree() ret=0x%x.\n", __FILE__, __LINE__, ret);
        return ret;
    }
    g_atDevInitFlag[chipId] = CE1_ENABLE;    /*24 E1 card init success !*/
    ROSNG_TRACE_DEBUG("\nAT DEVICE INIT SUCCESS !!!!  CHIP ID IS %u .\n", chipId);
    
    return ret ;
}



/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_tdm_sdh_perf.c
* 文件标识: 
* 其它说明: 驱动STM TDM PWE3模块的STM接口的SDH性能统计实现函数
* 当前版本: V1.0
* 作    者: 谢伟生10112265.
* 完成日期: 2013-03-26
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/


#include "drv_tdm_sdh_perf.h"
#include "drv_tdm_init.h"
#include "AtClasses.h"
#include "AtDevice.h"
#include "AtModule.h"
#include "AtModuleSdh.h"
#include "AtSdhLine.h"
#include "AtSdhPath.h"
#include "AtChannel.h"
#include "AtModuleBer.h"
#include "AtBerController.h"
#include "AtCommon.h"
#include "AtPdhDe1.h"


/* 全局变量定义 */
/* 全局数组g_drv_tdm_sdh_perf用来保存STM帧的SDH性能统计计数,保存的是一次性值 */
static DRV_TDM_SDH_PERF* g_drv_tdm_sdh_perf[DRV_TDM_MAX_STM_CARD_NUM] = {NULL, NULL, NULL, NULL};

/* 全局数组g_drv_tdm_sdh_total_perf用来保存STM帧的SDH性能统计计数,保存的是累加值 */
static DRV_TDM_SDH_PERF* g_drv_tdm_sdh_total_perf[DRV_TDM_MAX_STM_CARD_NUM] = {NULL, NULL, NULL, NULL};

/* 使能SDH BER标记,默认非使能.在初始化单板时,使能标记;在卸载单板时,非使能标记.*/
static WORD32 g_drv_tdm_sdh_ber_enable[DRV_TDM_MAX_STM_CARD_NUM] = {0};  

/* g_drv_tdm_sdh_ber_thread用来保存SDH BER线程的信息 */
static struct mod_thread g_drv_tdm_sdh_ber_thread[DRV_TDM_MAX_STM_CARD_NUM] = {{0}};

/* SDH BER线程的threadId */
static pthread_t g_drv_tdm_sdh_ber_thread_id[DRV_TDM_MAX_STM_CARD_NUM] = {0}; 

/* SDH BER线程的TID */
static int g_drv_tdm_sdh_ber_tid[DRV_TDM_MAX_STM_CARD_NUM] = {0};


/**************************************************************************
* 函数名称: drv_tdm_sdhPerfMemAllocate
* 功能描述: 动态分配内存,该内存用来保存SDH性能统计信息
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_sdhPerfMemAllocate(BYTE chipId)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    if (NULL == g_drv_tdm_sdh_perf[chipId])
    {
        g_drv_tdm_sdh_perf[chipId] = (DRV_TDM_SDH_PERF *)(malloc(DRV_TDM_MAX_STM_PORT_NUM * sizeof(DRV_TDM_SDH_PERF)));
        if (NULL == g_drv_tdm_sdh_perf[chipId])
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, allocate memory for g_drv_tdm_sdh_perf[%u] failed.\n", __FILE__, __LINE__, chipId);
            return DRV_TDM_MEM_ALLOC_FAIL;
        }
        memset(g_drv_tdm_sdh_perf[chipId], 0, (DRV_TDM_MAX_STM_PORT_NUM * sizeof(DRV_TDM_SDH_PERF)));
    }

    if (NULL == g_drv_tdm_sdh_total_perf[chipId])
    {
        g_drv_tdm_sdh_total_perf[chipId] = (DRV_TDM_SDH_PERF *)(malloc(DRV_TDM_MAX_STM_PORT_NUM * sizeof(DRV_TDM_SDH_PERF)));
        if (NULL == g_drv_tdm_sdh_total_perf[chipId])
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, allocate memory for g_drv_tdm_sdh_total_perf[%u] failed.\n", __FILE__, __LINE__, chipId);
            return DRV_TDM_MEM_ALLOC_FAIL;
        }
        memset(g_drv_tdm_sdh_total_perf[chipId], 0, (DRV_TDM_MAX_STM_PORT_NUM * sizeof(DRV_TDM_SDH_PERF)));
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_sdhPerfMemFree
* 功能描述: 释放已经动态分配的内存,该内存用来保存SDH性能统计信息
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_sdhPerfMemFree(BYTE chipId)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    if (NULL != g_drv_tdm_sdh_perf[chipId])
    {
        free(g_drv_tdm_sdh_perf[chipId]);
        g_drv_tdm_sdh_perf[chipId] = NULL; /* 防止成为野指针 */
    }

    if (NULL != g_drv_tdm_sdh_total_perf[chipId])
    {
        free(g_drv_tdm_sdh_total_perf[chipId]);
        g_drv_tdm_sdh_total_perf[chipId] = NULL; /* 防止成为野指针 */
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_sdhPerfMemGet
* 功能描述: 获取保存SDH性能统计(一次性值)的内存
* 访问的表: 软件表g_drv_tdm_sdh_perf
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
* 输出参数: ppPerf 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_sdhPerfMemGet(BYTE chipId, 
                                       BYTE portId, 
                                       BYTE aug1Id, 
                                       DRV_TDM_SDH_PERF** ppPerf)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwStmIntfMode = 0;   /* STM端口模式 */
    BYTE stmIntfIndex = 0;      /* STM端口索引号,取值为0~7. */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(ppPerf);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_perf[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_stmIntfModeGet(chipId, portId, &dwStmIntfMode);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmIntfModeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_STM1_INTF == dwStmIntfMode)  /* STM-1 interface mode */
    {
        stmIntfIndex = portId - (BYTE)1;
    }
    else if (DRV_TDM_STM4_INTF == dwStmIntfMode) /* STM-4 interface mode */
    {
        rv = drv_tdm_aug1IndexGet(portId, aug1Id, &stmIntfIndex);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_aug1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, invalid stmIntfMode, stmIntfMode=%u.\n", __FILE__, __LINE__, dwStmIntfMode);
        return DRV_TDM_INVALID_STM_INTF_MODE;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);

    *ppPerf = &(g_drv_tdm_sdh_perf[chipId][stmIntfIndex]);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_sdhTotalPerfMemGet
* 功能描述: 获取保存SDH性能统计(累加值)的内存
* 访问的表: 软件表g_drv_tdm_sdh_total_perf
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
* 输出参数: ppPerf 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_sdhTotalPerfMemGet(BYTE chipId, 
                                             BYTE portId, 
                                             BYTE aug1Id, 
                                             DRV_TDM_SDH_PERF** ppPerf)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwStmIntfMode = 0;   /* STM端口模式 */
    BYTE stmIntfIndex = 0;      /* STM端口索引号,取值为0~7. */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(ppPerf);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_total_perf[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_stmIntfModeGet(chipId, portId, &dwStmIntfMode);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmIntfModeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_STM1_INTF == dwStmIntfMode)  /* STM-1 interface mode */
    {
        stmIntfIndex = portId - (BYTE)1;
    }
    else if (DRV_TDM_STM4_INTF == dwStmIntfMode) /* STM-4 interface mode */
    {
        rv = drv_tdm_aug1IndexGet(portId, aug1Id, &stmIntfIndex);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_aug1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, invalid stmIntfMode, stmIntfMode=%u.\n", __FILE__, __LINE__, dwStmIntfMode);
        return DRV_TDM_INVALID_STM_INTF_MODE;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);

    *ppPerf = &(g_drv_tdm_sdh_total_perf[chipId][stmIntfIndex]);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_secPerfCntSave
* 功能描述: 保存STM帧的段层性能统计计数(一次性值).
* 访问的表: 软件表g_drv_tdm_sdh_perf.
* 修改的表: 软件表g_drv_tdm_sdh_perf.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: STM端口编号,取值为1~8.
*           *pSecPerf: 保存段层性格统计计数.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_secPerfCntSave(BYTE chipId, 
                                       BYTE portId, 
                                       const DRV_TDM_SECTION_PERF *pSecPerf)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE stmIntfIndex = 0;      /* STM端口索引号,取值为0~7 */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pSecPerf);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_perf[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    stmIntfIndex = portId - (BYTE)1;
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    memset(&(g_drv_tdm_sdh_perf[chipId][stmIntfIndex].sectionPerf), 0, sizeof(DRV_TDM_SECTION_PERF));  /* 先清零再赋值 */
    memcpy(&(g_drv_tdm_sdh_perf[chipId][stmIntfIndex].sectionPerf), pSecPerf, sizeof(DRV_TDM_SECTION_PERF));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_secPerfTotalCntSave
* 功能描述: 保存STM帧的段层性能统计计数(累加值).
* 访问的表: 软件表g_drv_tdm_sdh_total_perf.
* 修改的表: 软件表g_drv_tdm_sdh_total_perf.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: STM端口编号,取值为1~8.
*           *pSecPerf: 保存段层性格统计计数.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_secPerfTotalCntSave(BYTE chipId, 
                                             BYTE portId, 
                                             const DRV_TDM_SECTION_PERF *pSecPerf)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE stmIntfIndex = 0;      /* STM端口索引号,取值为0~7 */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pSecPerf);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_total_perf[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    stmIntfIndex = portId - (BYTE)1;
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    g_drv_tdm_sdh_total_perf[chipId][stmIntfIndex].sectionPerf.b1Cnts += pSecPerf->b1Cnts;
    g_drv_tdm_sdh_total_perf[chipId][stmIntfIndex].sectionPerf.b2Cnts += pSecPerf->b2Cnts;
    g_drv_tdm_sdh_total_perf[chipId][stmIntfIndex].sectionPerf.msReiCnts += pSecPerf->msReiCnts;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_secPerfCntGet
* 功能描述: 获取STM帧的段层性能统计计数(一次性值).
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: STM端口编号,取值为1~8.
* 输出参数: *pSecPerf: 保存段层的性能统计计数.
* 返 回 值: 
* 其它说明: 读清的,从芯片中读取.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_secPerfCntGet(BYTE chipId, 
                                     BYTE portId, 
                                     DRV_TDM_SECTION_PERF *pSecPerf)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pSecPerf);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    memset(pSecPerf, 0, sizeof(DRV_TDM_SECTION_PERF)); /* 先清空再赋值 */
    pSecPerf->b1Cnts = AtSdhChannelBlockErrorCounterClear((AtSdhChannel)pLine, cAtSdhLineCounterTypeB1);
    pSecPerf->b2Cnts = AtSdhChannelBlockErrorCounterClear((AtSdhChannel)pLine, cAtSdhLineCounterTypeB2);
    pSecPerf->msReiCnts = AtSdhChannelBlockErrorCounterClear((AtSdhChannel)pLine, cAtSdhLineCounterTypeRei);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_secPerfCntQuery
* 功能描述: 查询STM帧的段层性能统计计数(累加值).
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
* 输出参数: *pSecPerf: 保存段层性能统计计数.
* 返 回 值: 
* 其它说明: 本函数供产品管理调用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_secPerfCntQuery(BYTE chipId, 
                                        BYTE portId, 
                                        DRV_TDM_SECTION_PERF *pSecPerf)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    DRV_TDM_SECTION_PERF secPerfCnt;
    DRV_TDM_SDH_PERF *pSdhPerf = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pSecPerf);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    memset(&secPerfCnt, 0, sizeof(DRV_TDM_SECTION_PERF));
    
    rv = drv_tdm_secPerfCntGet(chipId, portId, &secPerfCnt);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_secPerfCntGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* 保存STM帧的段层性能统计计数(一次性值). */
    rv = drv_tdm_secPerfCntSave(chipId, portId, &secPerfCnt);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_secPerfCntSave() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    /* 保存STM帧的段层性能统计计数(累加值). */
    rv = drv_tdm_secPerfTotalCntSave(chipId, portId, &secPerfCnt);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_secPerfTotalCntSave() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    /* 获取保存SDH性能统计(累加值)的内存 */
    rv = drv_tdm_sdhTotalPerfMemGet(chipId, portId, DRV_TDM_STM1_AUG1_ID, &pSdhPerf);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_sdhTotalPerfMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pSdhPerf);
    
    memset(pSecPerf, 0, sizeof(DRV_TDM_SECTION_PERF));  /* 先清零再赋值 */
    memcpy(pSecPerf, &(pSdhPerf->sectionPerf), sizeof(DRV_TDM_SECTION_PERF));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_hpPerfCntSave
* 功能描述: 保存STM帧的高阶通道性能统计计数(一次性值).
* 访问的表: 软件表g_drv_tdm_sdh_perf.
* 修改的表: 软件表g_drv_tdm_sdh_perf.
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           *pVc4Perf: 保存高阶通道的性能统计计数.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_hpPerfCntSave(BYTE chipId, 
                                      BYTE portId, 
                                      BYTE aug1Id, 
                                      const DRV_TDM_VC4_PERF *pVc4Perf)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwStmIntfMode = 0;    /* STM端口模式 */
    BYTE stmIntfIndex = 0;      /* STM端口索引号,取值为0~7 */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4Perf);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_perf[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_stmIntfModeGet(chipId, portId, &dwStmIntfMode);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmIntfModeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_STM1_INTF == dwStmIntfMode)  /* STM-1 interface mode */
    {
        stmIntfIndex = portId - (BYTE)1;
    }
    else if (DRV_TDM_STM4_INTF == dwStmIntfMode) /* STM-4 interface mode */
    {
        rv = drv_tdm_aug1IndexGet(portId, aug1Id, &stmIntfIndex);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_aug1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, invalid stmIntfMode, stmIntfMode=%u.\n", __FILE__, __LINE__, dwStmIntfMode);
        return DRV_TDM_INVALID_STM_INTF_MODE;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    memset(&(g_drv_tdm_sdh_perf[chipId][stmIntfIndex].vc4Perf), 0, sizeof(DRV_TDM_VC4_PERF));  /* 先清零再赋值 */
    memcpy(&(g_drv_tdm_sdh_perf[chipId][stmIntfIndex].vc4Perf), pVc4Perf, sizeof(DRV_TDM_VC4_PERF));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_hpPerfTotalCntSave
* 功能描述: 保存STM帧的高阶通道的性能统计计数(累加值).
* 访问的表: 软件表g_drv_tdm_sdh_total_perf.
* 修改的表: 软件表g_drv_tdm_sdh_total_perf.
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           *pVc4Perf: 保存高阶通道的性能统计计数.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_hpPerfTotalCntSave(BYTE chipId, 
                                            BYTE portId, 
                                            BYTE aug1Id, 
                                            const DRV_TDM_VC4_PERF *pVc4Perf)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwStmIntfMode = 0;    /* STM端口模式 */
    BYTE stmIntfIndex = 0;      /* STM端口索引,取值为0~7 */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4Perf);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_total_perf[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_stmIntfModeGet(chipId, portId, &dwStmIntfMode);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmIntfModeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_STM1_INTF == dwStmIntfMode)  /* STM-1 interface mode */
    {
        stmIntfIndex = portId - (BYTE)1;
    }
    else if (DRV_TDM_STM4_INTF == dwStmIntfMode) /* STM-4 interface mode */
    {
        rv = drv_tdm_aug1IndexGet(portId, aug1Id, &stmIntfIndex);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_aug1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, invalid stmIntfMode, stmIntfMode=%u.\n", __FILE__, __LINE__, dwStmIntfMode);
        return DRV_TDM_INVALID_STM_INTF_MODE;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    g_drv_tdm_sdh_total_perf[chipId][stmIntfIndex].vc4Perf.b3Cnts += pVc4Perf->b3Cnts;
    g_drv_tdm_sdh_total_perf[chipId][stmIntfIndex].vc4Perf.hpReiCnts += pVc4Perf->hpReiCnts; 
    g_drv_tdm_sdh_total_perf[chipId][stmIntfIndex].vc4Perf.rxAu4Ppjc += pVc4Perf->rxAu4Ppjc;
    g_drv_tdm_sdh_total_perf[chipId][stmIntfIndex].vc4Perf.rxAu4Npjc += pVc4Perf->rxAu4Npjc;
    g_drv_tdm_sdh_total_perf[chipId][stmIntfIndex].vc4Perf.txAu4Ppjc += pVc4Perf->txAu4Ppjc;
    g_drv_tdm_sdh_total_perf[chipId][stmIntfIndex].vc4Perf.txAu4Npjc += pVc4Perf->txAu4Npjc;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_hpPerfCntGet
* 功能描述: 获取STM帧的高阶通道性能统计计数(一次性值).
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
* 输出参数: *pVc4Perf: 保存高阶通道性能统计计数.
* 返 回 值: 
* 其它说明: 读清的,直接从芯片中读取.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_hpPerfCntGet(BYTE chipId, 
                                    BYTE portId, 
                                    BYTE aug1Id, 
                                    DRV_TDM_VC4_PERF *pVc4Perf)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhAu pAu4 = NULL;
    AtSdhVc pVc4 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4Perf);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_atSdhAu4PtrGet(chipId, portId, aug1Id, &pAu4);  /* Get AU4 */
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_atSdhAu4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pAu4);
    
    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);  /* Get VC4 */
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    
    memset(pVc4Perf, 0, sizeof(DRV_TDM_VC4_PERF));  /* 先清零再赋值 */
    pVc4Perf->b3Cnts = AtSdhChannelBlockErrorCounterClear((AtSdhChannel)pVc4, cAtSdhPathCounterTypeBip);
    pVc4Perf->hpReiCnts = AtSdhChannelBlockErrorCounterClear((AtSdhChannel)pVc4, cAtSdhPathCounterTypeRei);
    pVc4Perf->rxAu4Ppjc = AtChannelCounterClear((AtChannel)pAu4, cAtSdhPathCounterTypeRxPPJC);
    pVc4Perf->rxAu4Npjc = AtChannelCounterClear((AtChannel)pAu4, cAtSdhPathCounterTypeRxNPJC);
    pVc4Perf->txAu4Ppjc = AtChannelCounterClear((AtChannel)pAu4, cAtSdhPathCounterTypeTxPPJC);
    pVc4Perf->txAu4Npjc = AtChannelCounterClear((AtChannel)pAu4, cAtSdhPathCounterTypeTxNPJC);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_hpPerfCntQuery
* 功能描述: 查询STM帧的高阶通道性能统计计数(累加值).
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
* 输出参数: *pVc4Perf: 保存高阶通道的性能统计计数.
* 返 回 值: 
* 其它说明: 该函数供产品管理调用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_hpPerfCntQuery(BYTE chipId, 
                                       BYTE portId, 
                                       BYTE aug1Id, 
                                       DRV_TDM_VC4_PERF *pVc4Perf)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    DRV_TDM_VC4_PERF hpPerfCnt;
    DRV_TDM_SDH_PERF *pSdhPerf = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4Perf);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    memset(&hpPerfCnt, 0, sizeof(DRV_TDM_VC4_PERF));
    
    rv = drv_tdm_hpPerfCntGet(chipId, portId, aug1Id, &hpPerfCnt);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_hpPerfCntGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* 保存STM帧的高阶通道性能统计计数(一次性值). */
    rv = drv_tdm_hpPerfCntSave(chipId, portId, aug1Id, &hpPerfCnt);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_hpPerfCntSave() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* 保存STM帧的高阶通道统计计数(累加值). */
    rv = drv_tdm_hpPerfTotalCntSave(chipId, portId, aug1Id, &hpPerfCnt);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_hpPerfTotalCntSave() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* 获取保存SDH性能统计(累加值)的内存 */
    rv = drv_tdm_sdhTotalPerfMemGet(chipId, portId, aug1Id, &pSdhPerf);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_sdhTotalPerfMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pSdhPerf);
    
    memset(pVc4Perf, 0, sizeof(DRV_TDM_VC4_PERF));  /* 先清零再赋值 */
    memcpy(pVc4Perf, &(pSdhPerf->vc4Perf), sizeof(DRV_TDM_VC4_PERF));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_lpPerfCntSave
* 功能描述: 保存STM帧的低阶通道性能统计计数(一次性值).
* 访问的表: 软件表g_drv_tdm_sdh_perf.
* 修改的表: 软件表g_drv_tdm_sdh_perf.
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
*           *pVc12Perf: 保存低阶通道的性能统计计数.  
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_lpPerfCntSave(BYTE chipId, 
                                     BYTE portId, 
                                     BYTE aug1Id, 
                                     BYTE e1LinkId, 
                                     const DRV_TDM_VC12_PERF *pVc12Perf)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwStmIntfMode = 0;    /* STM端口模式 */
    BYTE stmIntfIndex = 0;      /* STM端口索引号,取值为0~7 */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12Perf);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_perf[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_stmIntfModeGet(chipId, portId, &dwStmIntfMode);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmIntfModeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_STM1_INTF == dwStmIntfMode)  /* STM-1 interface mode */
    {
        stmIntfIndex = portId - (BYTE)1;
    }
    else if (DRV_TDM_STM4_INTF == dwStmIntfMode) /* STM-4 interface mode */
    {
        rv = drv_tdm_aug1IndexGet(portId, aug1Id, &stmIntfIndex);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_aug1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, invalid stmIntfMode, stmIntfMode=%u.\n", __FILE__, __LINE__, dwStmIntfMode);
        return DRV_TDM_INVALID_STM_INTF_MODE;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    memset(&(g_drv_tdm_sdh_perf[chipId][stmIntfIndex].vc12Perf[e1LinkId]), 0, sizeof(DRV_TDM_VC12_PERF));  /* 先清零再赋值 */
    memcpy(&(g_drv_tdm_sdh_perf[chipId][stmIntfIndex].vc12Perf[e1LinkId]), pVc12Perf, sizeof(DRV_TDM_VC12_PERF));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_lpPerfTotalCntSave
* 功能描述: 保存STM帧的低阶通道性能统计计数(累加值).
* 访问的表: 软件表g_drv_tdm_sdh_total_perf.
* 修改的表: 软件表g_drv_tdm_sdh_total_perf.
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
*           *pVc12Perf: 保存低阶通道的性能统计计数.   
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_lpPerfTotalCntSave(BYTE chipId, 
                                           BYTE portId, 
                                           BYTE aug1Id, 
                                           BYTE e1LinkId, 
                                           const DRV_TDM_VC12_PERF *pVc12Perf)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwStmIntfMode = 0;    /* STM端口模式 */
    BYTE stmIntfIndex = 0;      /* STM端口索引号,取值为0~7 */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12Perf);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_total_perf[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_stmIntfModeGet(chipId, portId, &dwStmIntfMode);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmIntfModeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_STM1_INTF == dwStmIntfMode)  /* STM-1 interface mode */
    {
        stmIntfIndex = portId - (BYTE)1;
    }
    else if (DRV_TDM_STM4_INTF == dwStmIntfMode) /* STM-4 interface mode */
    {
        rv = drv_tdm_aug1IndexGet(portId, aug1Id, &stmIntfIndex);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_aug1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, invalid stmIntfMode, stmIntfMode=%u.\n", __FILE__, __LINE__, dwStmIntfMode);
        return DRV_TDM_INVALID_STM_INTF_MODE;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    g_drv_tdm_sdh_total_perf[chipId][stmIntfIndex].vc12Perf[e1LinkId].bipCnts += pVc12Perf->bipCnts;
    g_drv_tdm_sdh_total_perf[chipId][stmIntfIndex].vc12Perf[e1LinkId].lpReiCnts += pVc12Perf->lpReiCnts;
    g_drv_tdm_sdh_total_perf[chipId][stmIntfIndex].vc12Perf[e1LinkId].rxTu12Ppjc += pVc12Perf->rxTu12Ppjc;
    g_drv_tdm_sdh_total_perf[chipId][stmIntfIndex].vc12Perf[e1LinkId].rxTu12Npjc += pVc12Perf->rxTu12Npjc;
    g_drv_tdm_sdh_total_perf[chipId][stmIntfIndex].vc12Perf[e1LinkId].txTu12Ppjc += pVc12Perf->txTu12Ppjc;
    g_drv_tdm_sdh_total_perf[chipId][stmIntfIndex].vc12Perf[e1LinkId].txTu12Npjc += pVc12Perf->txTu12Npjc;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_lpPerfCntGet
* 功能描述: 获取STM帧的低阶通道性能统计计数(一次性值).
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63. 
* 输出参数: *pVc12Perf: 保存低阶通道的性能统计计数.
* 返 回 值: 
* 其它说明: 读清的,直接从芯片中读取.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_lpPerfCntGet(BYTE chipId, 
                                   BYTE portId, 
                                   BYTE aug1Id, 
                                   BYTE e1LinkId, 
                                   DRV_TDM_VC12_PERF *pVc12Perf)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhTu pTu12 = NULL;
    AtSdhVc pVc12 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12Perf);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_atSdhTu12PtrGet(chipId, portId, aug1Id, e1LinkId, &pTu12); /* Get TU12 */
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_atSdhTu12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pTu12);

    rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12); /* Get VC12 */
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);
    
    memset(pVc12Perf, 0, sizeof(DRV_TDM_VC12_PERF)); /* 先清零再赋值 */
    pVc12Perf->bipCnts = AtSdhChannelBlockErrorCounterClear((AtSdhChannel)pVc12, cAtSdhPathCounterTypeBip);
    pVc12Perf->lpReiCnts = AtSdhChannelBlockErrorCounterClear((AtSdhChannel)pVc12, cAtSdhPathCounterTypeRei);
    pVc12Perf->rxTu12Ppjc = AtChannelCounterClear((AtChannel)pTu12, cAtSdhPathCounterTypeRxPPJC);
    pVc12Perf->rxTu12Npjc = AtChannelCounterClear((AtChannel)pTu12, cAtSdhPathCounterTypeRxNPJC);
    pVc12Perf->txTu12Ppjc = AtChannelCounterClear((AtChannel)pTu12, cAtSdhPathCounterTypeTxPPJC);
    pVc12Perf->txTu12Npjc = AtChannelCounterClear((AtChannel)pTu12, cAtSdhPathCounterTypeTxNPJC);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_lpPerfCntQuery
* 功能描述: 查询STM帧的低阶通道性能统计计数(累加值).
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.   
* 输出参数: *pVc12Perf: 保存低阶通道的性能统计计数.
* 返 回 值: 
* 其它说明: 该函数供产品管理调用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_lpPerfCntQuery(BYTE chipId, 
                                      BYTE portId, 
                                      BYTE aug1Id, 
                                      BYTE e1LinkId, 
                                      DRV_TDM_VC12_PERF *pVc12Perf)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    DRV_TDM_VC12_PERF lpPerfCnt;
    DRV_TDM_SDH_PERF *pSdhPerf = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12Perf);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    memset(&lpPerfCnt, 0, sizeof(DRV_TDM_VC12_PERF));
    
    rv = drv_tdm_lpPerfCntGet(chipId, portId, aug1Id, e1LinkId, &lpPerfCnt);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_lpPerfCntGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* 保存STM帧的低阶通道性能统计计数(一次性值). */
    rv = drv_tdm_lpPerfCntSave(chipId, portId, aug1Id, e1LinkId, &lpPerfCnt);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_lpPerfCntSave() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* 保存STM帧的低阶通道性能统计计数(累加值). */
    rv = drv_tdm_lpPerfTotalCntSave(chipId, portId, aug1Id, e1LinkId, &lpPerfCnt);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_lpPerfTotalCntSave() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* 获取保存SDH性能统计(累加值)的内存 */
    rv = drv_tdm_sdhTotalPerfMemGet(chipId, portId, aug1Id, &pSdhPerf);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_sdhTotalPerfMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pSdhPerf);
    
    memset(pVc12Perf, 0, sizeof(DRV_TDM_VC12_PERF));  /* 先清零再赋值 */
    memcpy(pVc12Perf, &(pSdhPerf->vc12Perf[e1LinkId]), sizeof(DRV_TDM_VC12_PERF));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1PerfCntSave
* 功能描述: 保存STM帧的E1通道性能统计计数(一次性值).
* 访问的表: 软件表g_drv_tdm_sdh_perf.
* 修改的表: 软件表g_drv_tdm_sdh_perf.
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
*           *pE1Perf: 保存E1通道的性能统计计数.  
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1PerfCntSave(BYTE chipId, 
                                     BYTE portId, 
                                     BYTE aug1Id, 
                                     BYTE e1LinkId, 
                                     const DRV_TDM_E1_PERF *pE1Perf)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwStmIntfMode = 0;    /* STM端口模式 */
    BYTE stmIntfIndex = 0;      /* STM端口索引号,取值为0~7 */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pE1Perf);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_perf[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_stmIntfModeGet(chipId, portId, &dwStmIntfMode);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmIntfModeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_STM1_INTF == dwStmIntfMode)  /* STM-1 interface mode */
    {
        stmIntfIndex = portId - (BYTE)1;
    }
    else if (DRV_TDM_STM4_INTF == dwStmIntfMode) /* STM-4 interface mode */
    {
        rv = drv_tdm_aug1IndexGet(portId, aug1Id, &stmIntfIndex);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_aug1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, invalid stmIntfMode, stmIntfMode=%u.\n", __FILE__, __LINE__, dwStmIntfMode);
        return DRV_TDM_INVALID_STM_INTF_MODE;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    memset(&(g_drv_tdm_sdh_perf[chipId][stmIntfIndex].e1Perf[e1LinkId]), 0, sizeof(DRV_TDM_E1_PERF));  /* 先清零再赋值 */
    memcpy(&(g_drv_tdm_sdh_perf[chipId][stmIntfIndex].e1Perf[e1LinkId]), pE1Perf, sizeof(DRV_TDM_E1_PERF));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1PerfTotalCntSave
* 功能描述: 保存STM帧的E1通道性能统计计数(累加值).
* 访问的表: 软件表g_drv_tdm_sdh_total_perf.
* 修改的表: 软件表g_drv_tdm_sdh_total_perf.
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
*           *pE1Perf: 保存E1通道的性能统计计数.  
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1PerfTotalCntSave(BYTE chipId, 
                                            BYTE portId, 
                                            BYTE aug1Id, 
                                            BYTE e1LinkId, 
                                            const DRV_TDM_E1_PERF *pE1Perf)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwStmIntfMode = 0;    /* STM端口模式 */
    BYTE stmIntfIndex = 0;      /* STM端口索引号,取值为0~7 */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pE1Perf);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_sdh_total_perf[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_stmIntfModeGet(chipId, portId, &dwStmIntfMode);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmIntfModeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_STM1_INTF == dwStmIntfMode)  /* STM-1 interface mode */
    {
        stmIntfIndex = portId - (BYTE)1;
    }
    else if (DRV_TDM_STM4_INTF == dwStmIntfMode) /* STM-4 interface mode */
    {
        rv = drv_tdm_aug1IndexGet(portId, aug1Id, &stmIntfIndex);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_aug1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, invalid stmIntfMode, stmIntfMode=%u.\n", __FILE__, __LINE__, dwStmIntfMode);
        return DRV_TDM_INVALID_STM_INTF_MODE;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);

    g_drv_tdm_sdh_total_perf[chipId][stmIntfIndex].e1Perf[e1LinkId].e1CrcErrCnts += pE1Perf->e1CrcErrCnts;
    g_drv_tdm_sdh_total_perf[chipId][stmIntfIndex].e1Perf[e1LinkId].e1FrmBitErrCnts += pE1Perf->e1FrmBitErrCnts;
    g_drv_tdm_sdh_total_perf[chipId][stmIntfIndex].e1Perf[e1LinkId].e1ReiCnts += pE1Perf->e1ReiCnts;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1PerfCntGet
* 功能描述: 获取STM帧的E1通道性能统计计数(一次性值).
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63. 
* 输出参数: *pE1Perf: 保存E1通道的性能统计计数.
* 返 回 值: 
* 其它说明: 读清的,直接从芯片中读取.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1PerfCntGet(BYTE chipId, 
                                    BYTE portId, 
                                    BYTE aug1Id, 
                                    BYTE e1LinkId, 
                                    DRV_TDM_E1_PERF *pE1Perf)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtPdhDe1 pE1 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pE1Perf);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_atPdhE1PtrGet(chipId, portId, aug1Id, e1LinkId, &pE1); /* Get E1 */
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_atPdhE1PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pE1);
    
    memset(pE1Perf, 0, sizeof(DRV_TDM_E1_PERF)); /* 先清零再赋值 */
    pE1Perf->e1CrcErrCnts = AtChannelCounterClear((AtChannel)pE1, cAtPdhDe1CounterCrc);
    pE1Perf->e1FrmBitErrCnts = AtChannelCounterClear((AtChannel)pE1, cAtPdhDe1CounterFe);
    pE1Perf->e1ReiCnts = AtChannelCounterClear((AtChannel)pE1, cAtPdhDe1CounterRei);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1PerfCntQuery
* 功能描述: 查询STM帧的E1通道性能统计计数(累加值).
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3. 
*           portId: STM端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.   
* 输出参数: *pE1Perf: 保存E1通道的性能统计计数.
* 返 回 值: 
* 其它说明: 该函数供产品管理调用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1PerfCntQuery(BYTE chipId, 
                                       BYTE portId, 
                                       BYTE aug1Id, 
                                       BYTE e1LinkId, 
                                       DRV_TDM_E1_PERF *pE1Perf)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    DRV_TDM_E1_PERF e1PerfCnt;
    DRV_TDM_SDH_PERF *pSdhPerf = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pE1Perf);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    memset(&e1PerfCnt, 0, sizeof(DRV_TDM_E1_PERF));
    
    rv = drv_tdm_e1PerfCntGet(chipId, portId, aug1Id, e1LinkId, &e1PerfCnt);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_e1PerfCntGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* 保存STM帧的E1通道性能统计计数(一次性值). */
    rv = drv_tdm_e1PerfCntSave(chipId, portId, aug1Id, e1LinkId, &e1PerfCnt);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_e1PerfCntSave() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* 保存STM帧的E1通道性能统计计数(累加值). */
    rv = drv_tdm_e1PerfTotalCntSave(chipId, portId, aug1Id, e1LinkId, &e1PerfCnt);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_e1PerfTotalCntSave() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* 获取保存SDH性能统计(累加值)的内存 */
    rv = drv_tdm_sdhTotalPerfMemGet(chipId, portId, aug1Id, &pSdhPerf);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_sdhTotalPerfMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pSdhPerf);
    
    memset(pE1Perf, 0, sizeof(DRV_TDM_E1_PERF));  /* 先清零再赋值 */
    memcpy(pE1Perf, &(pSdhPerf->e1Perf[e1LinkId]), sizeof(DRV_TDM_E1_PERF));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txB1ErrForce
* 功能描述: 在TX方向强制插入B1误码
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 0~3. portId: 1~8.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txB1ErrForce(BYTE chipId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
   
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);

    /* Force B1 error on STM channel at TX direction. */
    rv = AtChannelTxErrorForce((AtChannel)pLine, cAtSdhLineCounterTypeB1);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtChannelTxErrorForce() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txB1ErrUnForce
* 功能描述: 在TX方向取消强制插入B1误码
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 0~3. portId: 1~8.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txB1ErrUnForce(BYTE chipId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
   
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    /* UnForce B1 error on STM channel at TX direction. */
    rv = AtChannelTxErrorUnForce((AtChannel)pLine, cAtSdhLineCounterTypeB1);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtChannelTxErrorUnForce() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txB2ErrForce
* 功能描述: 在TX方向强制插入B2误码
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 0~3. portId: 1~8.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txB2ErrForce(BYTE chipId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    /* Force B2 error on STM channel at TX direction. */
    rv = AtChannelTxErrorForce((AtChannel)pLine, cAtSdhLineCounterTypeB2);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtChannelTxErrorForce() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txB2ErrUnForce
* 功能描述: 在TX方向取消强制插入B2误码
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 0~3. portId: 1~8.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txB2ErrUnForce(BYTE chipId, BYTE portId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhLine pLine = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    /* UnForce B2 error on STM channel at TX direction. */
    rv = AtChannelTxErrorUnForce((AtChannel)pLine, cAtSdhLineCounterTypeB2);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtChannelTxErrorUnForce() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txB3ErrForce
* 功能描述: 在TX方向强制插入B3误码
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 0~3. portId: 1~8. aug1Id: 1~4.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txB3ErrForce(BYTE chipId, BYTE portId, BYTE aug1Id)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc4 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* Get VC4 */
    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);

    /* Force B3 error on VC4 channel at TX direction. */
    rv = AtChannelTxErrorForce((AtChannel)pVc4, cAtSdhPathCounterTypeBip);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtChannelTxErrorForce() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txB3ErrUnForce
* 功能描述: 在TX方向取消强制插入B3误码
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 0~3. portId: 1~8. aug1Id: 1~4.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txB3ErrUnForce(BYTE chipId, BYTE portId, BYTE aug1Id)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc4 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* Get VC4 */
    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    
    /* UnForce B3 error on VC4 channel at TX direction. */
    rv = AtChannelTxErrorUnForce((AtChannel)pVc4, cAtSdhPathCounterTypeBip);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtChannelTxErrorUnForce() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txLpBipvErrForce
* 功能描述: 在TX方向强制插入VC12通道的BIP-V误码
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 0~3. portId: 1~8. aug1Id: 1~4. e1LinkId: 1~63.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txLpBipvErrForce(BYTE chipId, 
                                         BYTE portId, 
                                         BYTE aug1Id, 
                                         BYTE e1LinkId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc12 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* Get VC12 */
    rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);

    /* Force V5 error on VC12 channel at TX direction. */
    rv = AtChannelTxErrorForce((AtChannel)pVc12, cAtSdhPathCounterTypeBip);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtChannelTxErrorForce() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_txLpBipvErrUnForce
* 功能描述: 在TX方向取消强制插入VC12通道的BIP-V误码
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 0~3. portId: 1~8. aug1Id: 1~4. e1LinkId: 1~63.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_txLpBipvErrUnForce(BYTE chipId, 
                                             BYTE portId, 
                                             BYTE aug1Id, 
                                             BYTE e1LinkId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtSdhVc pVc12 = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* Get VC12 */
    rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);
    
    /* UnForce V5 error on VC12 channel at TX direction. */
    rv = AtChannelTxErrorUnForce((AtChannel)pVc12, cAtSdhPathCounterTypeBip);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtChannelTxErrorUnForce() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_rsSdSfThresholdSet
* 功能描述: 设置RS段的SD/SF门限值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           sdsfType: 0表示设置SD的门限值,1表示设置SF的门限值.
*           threshold: SD/SF门限值,具体参见DRV_TDM_BER_RATE定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_rsSdSfThresholdSet(BYTE chipId, 
                                            BYTE portId, 
                                            WORD32 sdsfType, 
                                            WORD32 threshold)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    eAtBerRate atDevThrsh = 0;  /* ARRIVE芯片的SD/SF门限值 */
    AtSdhLine pLine = NULL;
    AtBerController pBerCtrl = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    /* Get BER Monitoring controller of RS layer */
    pBerCtrl = AtSdhLineRsBerControllerGet(pLine);
    DRV_TDM_CHECK_POINTER_IS_NULL(pBerCtrl);
    
    switch (threshold)
    {
        case DRV_TDM_BER_RATE_UNKNOWN:
            atDevThrsh = cAtBerRateUnknown;
            break;
        case DRV_TDM_BER_RATE_1E3:
            atDevThrsh = cAtBerRate1E3;
            break;
        case DRV_TDM_BER_RATE_1E4:
            atDevThrsh = cAtBerRate1E4;
            break;
        case DRV_TDM_BER_RATE_1E5:
            atDevThrsh = cAtBerRate1E5;
            break;
        case DRV_TDM_BER_RATE_1E6:
            atDevThrsh = cAtBerRate1E6;
            break;
        case DRV_TDM_BER_RATE_1E7:
            atDevThrsh = cAtBerRate1E7;
            break;
        case DRV_TDM_BER_RATE_1E8:
            atDevThrsh = cAtBerRate1E8;
            break;
        case DRV_TDM_BER_RATE_1E9:
            atDevThrsh = cAtBerRate1E9;
            break;
        case DRV_TDM_BER_RATE_1E10:
            atDevThrsh = cAtBerRate1E10;
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid threshold=%u.\n", __FILE__, __LINE__, threshold);
            return DRV_TDM_INVALID_ARGUMENT; /* 直接返回,不要break */
    }
    
    if (DRV_TDM_SD == sdsfType)  /* SD Type */
    {
        /* Set threshold to report BER-SD alarm */
        rv = AtBerControllerSdThresholdSet(pBerCtrl, atDevThrsh);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerSdThresholdSet() rv=0x%x.\n", __FILE__, __LINE__, rv);  
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    else if (DRV_TDM_SF == sdsfType)  /* SF Type */
    {
        /* Set threshold to report BER-SF alarm */
        rv = AtBerControllerSfThresholdSet(pBerCtrl, atDevThrsh);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerSfThresholdSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid sdsfType=%u.\n", __FILE__, __LINE__, sdsfType);
        return DRV_TDM_INVALID_ARGUMENT; /* 直接返回 */
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_rsSdSfThresholdGet
* 功能描述: 获取RS段的SD/SF门限值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           sdsfType: 0表示获取SD的门限值,1表示获取SF的门限值. 
* 输出参数: *pThreshold: 保存门限值.
* 返 回 值: 
* 其它说明: 直接从芯片中读取.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_rsSdSfThresholdGet(BYTE chipId, 
                                            BYTE portId, 
                                            WORD32 sdsfType, 
                                            WORD32 *pThreshold)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    eAtBerRate atDevThrsh = 0;  /* ARRIVE芯片的SD/SF门限值 */
    AtSdhLine pLine = NULL;
    AtBerController pBerCtrl = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pThreshold);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    /* Get BER Monitoring controller of RS layer */
    pBerCtrl = AtSdhLineRsBerControllerGet(pLine);
    DRV_TDM_CHECK_POINTER_IS_NULL(pBerCtrl);
    
    *pThreshold = 0;   /* 先清零再赋值 */
    if (DRV_TDM_SD == sdsfType)
    {
        /* Get threshold used to report BER-SD defect */
        atDevThrsh = AtBerControllerSdThresholdGet(pBerCtrl);
    }
    else if (DRV_TDM_SF == sdsfType)
    {
        /* Get threshold used to report BER-SF defect */
        atDevThrsh = AtBerControllerSfThresholdGet(pBerCtrl);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, chipId %u portId %u, invalid threshold type, thresholdType=%u.\n", __FILE__, __LINE__, chipId, portId, sdsfType);  
        return DRV_TDM_INVALID_ARGUMENT;
    }
    if (cAtBerRate1E3 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E3;
    }
    else if (cAtBerRate1E4 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E4;
    }
    else if (cAtBerRate1E5 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E5;
    }
    else if (cAtBerRate1E6 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E6;
    }
    else if (cAtBerRate1E7 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E7;
    }
    else if (cAtBerRate1E8 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E8;
    }
    else if (cAtBerRate1E9 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E9;
    }
    else if (cAtBerRate1E10 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E10;
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, chipId %u portId %u, invalid threshold, threshold=%u.\n", __FILE__, __LINE__, chipId, portId, atDevThrsh);  
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_rsCurrentBerRateGet
* 功能描述: 获取RS段的当前BER速率.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
* 输出参数: *pBerRate: 保存当前的BER Rate.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_rsCurrentBerRateGet(BYTE chipId, BYTE portId, WORD32 *pBerRate)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    eAtBerRate atBerRate = 0;  /* ARRIVE芯片的BER rate */
    AtSdhLine pLine = NULL;
    AtBerController pBerCtrl = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pBerRate);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    /* Get BER Monitoring controller of RS layer */
    pBerCtrl = AtSdhLineRsBerControllerGet(pLine);
    DRV_TDM_CHECK_POINTER_IS_NULL(pBerCtrl);
    
    *pBerRate = 0;  /* 先清零再赋值 */
    atBerRate = AtBerControllerCurBerGet(pBerCtrl); /* Get current monitored BER rate */
    if (cAtBerRate1E3 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E3;
    }
    else if (cAtBerRate1E4 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E4;
    }
    else if (cAtBerRate1E5 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E5;
    }
    else if (cAtBerRate1E6 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E6;
    }
    else if (cAtBerRate1E7 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E7;
    }
    else if (cAtBerRate1E8 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E8;
    }
    else if (cAtBerRate1E9 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E9;
    }
    else if (cAtBerRate1E10 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E10;
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, chipId %u portId %u, invalid ber rate, berRate=%u.\n", __FILE__, __LINE__, chipId, portId, atBerRate);  
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_msSdSfThresholdSet
* 功能描述: 设置MS段的SD/SF门限值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           sdsfType: 0表示设置SD的门限值,1表示设置SF的门限值.
*           threshold: SD/SF门限值,具体参见DRV_TDM_BER_RATE定义. 
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_msSdSfThresholdSet(BYTE chipId, 
                                             BYTE portId, 
                                             WORD32 sdsfType, 
                                             WORD32 threshold)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    eAtBerRate atDevThrsh = 0;  /* ARRIVE芯片的SD/SF门限值 */
    AtSdhLine pLine = NULL;
    AtBerController pBerCtrl = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    /* Get BER Monitoring controller of MS layer */
    pBerCtrl = AtSdhLineMsBerControllerGet(pLine);
    DRV_TDM_CHECK_POINTER_IS_NULL(pBerCtrl);
    
    switch (threshold)
    {
        case DRV_TDM_BER_RATE_UNKNOWN:
            atDevThrsh = cAtBerRateUnknown;
            break;
        case DRV_TDM_BER_RATE_1E3:
            atDevThrsh = cAtBerRate1E3;
            break;
        case DRV_TDM_BER_RATE_1E4:
            atDevThrsh = cAtBerRate1E4;
            break;
        case DRV_TDM_BER_RATE_1E5:
            atDevThrsh = cAtBerRate1E5;
            break;
        case DRV_TDM_BER_RATE_1E6:
            atDevThrsh = cAtBerRate1E6;
            break;
        case DRV_TDM_BER_RATE_1E7:
            atDevThrsh = cAtBerRate1E7;
            break;
        case DRV_TDM_BER_RATE_1E8:
            atDevThrsh = cAtBerRate1E8;
            break;
        case DRV_TDM_BER_RATE_1E9:
            atDevThrsh = cAtBerRate1E9;
            break;
        case DRV_TDM_BER_RATE_1E10:
            atDevThrsh = cAtBerRate1E10;
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid threshold=%u.\n", __FILE__, __LINE__, threshold);
            return DRV_TDM_INVALID_ARGUMENT; /* 直接返回,不要break */
    }
    
    if (DRV_TDM_SD == sdsfType)  /* SD Type */
    {
        /* Set threshold to report BER-SD alarm */
        rv = AtBerControllerSdThresholdSet(pBerCtrl, atDevThrsh);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerSdThresholdSet() rv=0x%x.\n", __FILE__, __LINE__, rv);  
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    else if (DRV_TDM_SF == sdsfType)  /* SF Type */
    {
        /* Set threshold to report BER-SF alarm */
        rv = AtBerControllerSfThresholdSet(pBerCtrl, atDevThrsh);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerSfThresholdSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid sdsfType=%u.\n", __FILE__, __LINE__, sdsfType);
        return DRV_TDM_INVALID_ARGUMENT; /* 直接返回 */
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_msSdSfThresholdGet
* 功能描述: 获取MS段的SD/SF门限值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           sdsfType: 0表示获取SD的门限值,1表示获取SF的门限值. 
* 输出参数: *pThreshold: 保存门限值.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_msSdSfThresholdGet(BYTE chipId, 
                                              BYTE portId, 
                                              WORD32 sdsfType, 
                                              WORD32 *pThreshold)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    eAtBerRate atDevThrsh = 0;  /* ARRIVE芯片的SD/SF门限值 */
    AtSdhLine pLine = NULL;
    AtBerController pBerCtrl = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pThreshold);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    /* Get BER Monitoring controller of MS layer */
    pBerCtrl = AtSdhLineMsBerControllerGet(pLine);
    DRV_TDM_CHECK_POINTER_IS_NULL(pBerCtrl);
    
    *pThreshold = 0;  /* 先清零再赋值 */
    if (DRV_TDM_SD == sdsfType)
    {
        /* Get threshold used to report BER-SD defect */
        atDevThrsh = AtBerControllerSdThresholdGet(pBerCtrl);
    }
    else if (DRV_TDM_SF == sdsfType)
    {
        /* Get threshold used to report BER-SF defect */
        atDevThrsh = AtBerControllerSfThresholdGet(pBerCtrl);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, chipId %u portId %u, invalid threshold type, thresholdType=%u.\n", __FILE__, __LINE__, chipId, portId, sdsfType);  
        return DRV_TDM_INVALID_ARGUMENT;
    }
    if (cAtBerRate1E3 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E3;
    }
    else if (cAtBerRate1E4 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E4;
    }
    else if (cAtBerRate1E5 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E5;
    }
    else if (cAtBerRate1E6 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E6;
    }
    else if (cAtBerRate1E7 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E7;
    }
    else if (cAtBerRate1E8 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E8;
    }
    else if (cAtBerRate1E9 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E9;
    }
    else if (cAtBerRate1E10 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E10;
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, chipId %u portId %u, invalid threshold, threshold=%u.\n", __FILE__, __LINE__, chipId, portId, atDevThrsh);  
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_msCurrentBerRateGet
* 功能描述: 获取MS段的当前BER速率.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
* 输出参数: *pBerRate: 保存当前的BER Rate.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_msCurrentBerRateGet(BYTE chipId, 
                                               BYTE portId, 
                                               WORD32 *pBerRate)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    eAtBerRate atBerRate = 0;  /* ARRIVE芯片的BER rate */
    AtSdhLine pLine = NULL;
    AtBerController pBerCtrl = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pBerRate);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atSdhLinePtrGet(chipId, portId, &pLine);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_atSdhLinePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pLine);
    
    /* Get BER Monitoring controller of MS layer */
    pBerCtrl = AtSdhLineMsBerControllerGet(pLine);
    DRV_TDM_CHECK_POINTER_IS_NULL(pBerCtrl);
    
    *pBerRate = 0;  /* 先清零再赋值 */
    atBerRate = AtBerControllerCurBerGet(pBerCtrl); /* Get current monitored BER rate */
    if (cAtBerRate1E3 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E3;
    }
    else if (cAtBerRate1E4 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E4;
    }
    else if (cAtBerRate1E5 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E5;
    }
    else if (cAtBerRate1E6 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E6;
    }
    else if (cAtBerRate1E7 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E7;
    }
    else if (cAtBerRate1E8 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E8;
    }
    else if (cAtBerRate1E9 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E9;
    }
    else if (cAtBerRate1E10 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E10;
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, chipId %u portId %u, invalid ber rate, berRate=%u.\n", __FILE__, __LINE__, chipId, portId, atBerRate);  
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_vc4SdSfThresholdSet
* 功能描述: 设置VC4通道的SD/SF门限值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           sdsfType: 0表示设置SD的门限值,1表示设置SF的门限值.
*           threshold: SD/SF门限值,具体参见DRV_TDM_BER_RATE定义.   
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_vc4SdSfThresholdSet(BYTE chipId, 
                                              BYTE portId, 
                                              BYTE aug1Id, 
                                              WORD32 sdsfType, 
                                              WORD32 threshold)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    eAtBerRate atDevThrsh = 0;  /* ARRIVE芯片的SD/SF门限值 */
    AtSdhVc pVc4 = NULL;
    AtBerController pBerCtrl = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* Get VC4 */
    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    
    /* Get BER controller that monitors BER for VC-4 channel */
    pBerCtrl = AtSdhChannelBerControllerGet((AtSdhChannel)pVc4);
    DRV_TDM_CHECK_POINTER_IS_NULL(pBerCtrl);
    
    switch (threshold)
    {
        case DRV_TDM_BER_RATE_UNKNOWN:
            atDevThrsh = cAtBerRateUnknown;
            break;
        case DRV_TDM_BER_RATE_1E3:
            atDevThrsh = cAtBerRate1E3;
            break;
        case DRV_TDM_BER_RATE_1E4:
            atDevThrsh = cAtBerRate1E4;
            break;
        case DRV_TDM_BER_RATE_1E5:
            atDevThrsh = cAtBerRate1E5;
            break;
        case DRV_TDM_BER_RATE_1E6:
            atDevThrsh = cAtBerRate1E6;
            break;
        case DRV_TDM_BER_RATE_1E7:
            atDevThrsh = cAtBerRate1E7;
            break;
        case DRV_TDM_BER_RATE_1E8:
            atDevThrsh = cAtBerRate1E8;
            break;
        case DRV_TDM_BER_RATE_1E9:
            atDevThrsh = cAtBerRate1E9;
            break;
        case DRV_TDM_BER_RATE_1E10:
            atDevThrsh = cAtBerRate1E10;
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid threshold=%u.\n", __FILE__, __LINE__, threshold);
            return DRV_TDM_INVALID_ARGUMENT; /* 直接返回,不要break */
    }
    
    if (DRV_TDM_SD == sdsfType)  /* SD Type */
    {
        /* Set threshold to report BER-SD alarm */
        rv = AtBerControllerSdThresholdSet(pBerCtrl, atDevThrsh);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerSdThresholdSet() rv=0x%x.\n", __FILE__, __LINE__, rv);  
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    else if (DRV_TDM_SF == sdsfType)  /* SF Type */
    {
        /* Set threshold to report BER-SF alarm */
        rv = AtBerControllerSfThresholdSet(pBerCtrl, atDevThrsh);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerSfThresholdSet() rv=0x%x.\n", __FILE__, __LINE__, rv); 
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid sdsfType=%u.\n", __FILE__, __LINE__, sdsfType);
        return DRV_TDM_INVALID_ARGUMENT; /* 直接返回 */
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_vc4SdSfThresholdGet
* 功能描述: 获取VC4通道的SD/SF门限值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           sdsfType: 0表示获取SD的门限值,1表示获取SF的门限值  
* 输出参数: *pThreshold: 保存门限值.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_vc4SdSfThresholdGet(BYTE chipId, 
                                              BYTE portId, 
                                              BYTE aug1Id, 
                                              WORD32 sdsfType, 
                                              WORD32 *pThreshold)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    eAtBerRate atDevThrsh = 0;  /* ARRIVE芯片的SD/SF门限值 */
    AtSdhVc pVc4 = NULL;
    AtBerController pBerCtrl = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(pThreshold);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* Get VC4 */
    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    
    /* Get BER controller that monitors BER for VC-4 channel */
    pBerCtrl = AtSdhChannelBerControllerGet((AtSdhChannel)pVc4);
    DRV_TDM_CHECK_POINTER_IS_NULL(pBerCtrl);
    
    *pThreshold = 0;  /* 先清零再赋值 */
    if (DRV_TDM_SD == sdsfType)
    {
        /* Get threshold used to report BER-SD defect */
        atDevThrsh = AtBerControllerSdThresholdGet(pBerCtrl);
    }
    else if (DRV_TDM_SF == sdsfType)
    {
        /* Get threshold used to report BER-SF defect */
        atDevThrsh = AtBerControllerSfThresholdGet(pBerCtrl);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, chipId %u portId %u, invalid threshold type, thresholdType=%u.\n", __FILE__, __LINE__, chipId, portId, sdsfType);  
        return DRV_TDM_INVALID_ARGUMENT;
    }
    if (cAtBerRate1E3 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E3;
    }
    else if (cAtBerRate1E4 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E4;
    }
    else if (cAtBerRate1E5 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E5;
    }
    else if (cAtBerRate1E6 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E6;
    }
    else if (cAtBerRate1E7 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E7;
    }
    else if (cAtBerRate1E8 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E8;
    }
    else if (cAtBerRate1E9 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E9;
    }
    else if (cAtBerRate1E10 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E10;
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, chipId %u portId %u, invalid threshold, threshold=%u.\n", __FILE__, __LINE__, chipId, portId, atDevThrsh);  
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_vc4CurrentBerRateGet
* 功能描述: 获取VC4通道的当前BER速率.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
* 输出参数: *pBerRate: 保存当前的BER Rate.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_vc4CurrentBerRateGet(BYTE chipId, 
                                                BYTE portId, 
                                                BYTE aug1Id, 
                                                WORD32 *pBerRate)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    eAtBerRate atBerRate = 0;  /* ARRIVE芯片的BER rate */
    AtSdhVc pVc4 = NULL;
    AtBerController pBerCtrl = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_POINTER_IS_NULL(pBerRate);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* Get VC4 */
    rv = drv_tdm_atSdhVc4PtrGet(chipId, portId, aug1Id, &pVc4);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_atSdhVc4PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc4);
    
    /* Get BER controller that monitors BER for VC-4 channel */
    pBerCtrl = AtSdhChannelBerControllerGet((AtSdhChannel)pVc4);
    DRV_TDM_CHECK_POINTER_IS_NULL(pBerCtrl);
    
    *pBerRate = 0;  /* 先清零再赋值 */
    atBerRate = AtBerControllerCurBerGet(pBerCtrl); /* Get current monitored BER rate */
    if (cAtBerRate1E3 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E3;
    }
    else if (cAtBerRate1E4 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E4;
    }
    else if (cAtBerRate1E5 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E5;
    }
    else if (cAtBerRate1E6 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E6;
    }
    else if (cAtBerRate1E7 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E7;
    }
    else if (cAtBerRate1E8 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E8;
    }
    else if (cAtBerRate1E9 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E9;
    }
    else if (cAtBerRate1E10 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E10;
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, chipId %u portId %u, invalid ber rate, berRate=%u.\n", __FILE__, __LINE__, chipId, portId, atBerRate);  
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_vc12SdSfThresholdSet
* 功能描述: 设置VC12通道的SD/SF门限值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为 1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
*           sdsfType: 0表示设置SD的门限值,1表示设置SF的门限值.
*           threshold: SD/SF门限值,具体参见DRV_TDM_BER_RATE定义.
* 输出参数: 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_vc12SdSfThresholdSet(BYTE chipId, 
                                                BYTE portId, 
                                                BYTE aug1Id, 
                                                BYTE e1LinkId, 
                                                WORD32 sdsfType, 
                                                WORD32 threshold)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    eAtBerRate atDevThrsh = 0;  /* ARRIVE芯片的SD/SF门限值 */
    AtSdhVc pVc12 = NULL;
    AtBerController pBerCtrl = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* Get VC12 */
    rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);
    
    /* Get BER controller that monitors BER for VC12 channel */
    pBerCtrl = AtSdhChannelBerControllerGet((AtSdhChannel)pVc12);
    DRV_TDM_CHECK_POINTER_IS_NULL(pBerCtrl);
    
    switch (threshold)
    {
        case DRV_TDM_BER_RATE_UNKNOWN:   /* Unknown bit error rate */
            atDevThrsh = cAtBerRateUnknown;
            break;
        case DRV_TDM_BER_RATE_1E3:       /* Bit error rate is 10-3 */
            atDevThrsh = cAtBerRate1E3;
            break;
        case DRV_TDM_BER_RATE_1E4:       /* Bit error rate is 10-4 */
            atDevThrsh = cAtBerRate1E4;
            break;
        case DRV_TDM_BER_RATE_1E5:       /* Bit error rate is 10-5 */
            atDevThrsh = cAtBerRate1E5;
            break;
        case DRV_TDM_BER_RATE_1E6:       /* Bit error rate is 10-6 */
            atDevThrsh = cAtBerRate1E6;
            break;
        case DRV_TDM_BER_RATE_1E7:       /* Bit error rate is 10-7 */
            atDevThrsh = cAtBerRate1E7;
            break;
        case DRV_TDM_BER_RATE_1E8:       /* Bit error rate is 10-8 */
            atDevThrsh = cAtBerRate1E8;
            break;
        case DRV_TDM_BER_RATE_1E9:       /* Bit error rate is 10-9 */
            atDevThrsh = cAtBerRate1E9;
            break;
        case DRV_TDM_BER_RATE_1E10:
            atDevThrsh = cAtBerRate1E10;
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid threshold=%u.\n", __FILE__, __LINE__, threshold);
            return DRV_TDM_INVALID_ARGUMENT; /* 直接返回,不要break */
    }
    
    if (DRV_TDM_SD == sdsfType)  /* SD Type */
    {
        /* Set threshold to report BER-SD alarm */
        rv = AtBerControllerSdThresholdSet(pBerCtrl, atDevThrsh);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerSdThresholdSet() rv=0x%x.\n", __FILE__, __LINE__, rv); 
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    else if (DRV_TDM_SF == sdsfType)  /* SF Type */
    {
        /* Set threshold to report BER-SF alarm */
        rv = AtBerControllerSfThresholdSet(pBerCtrl, atDevThrsh);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtBerControllerSfThresholdSet() rv=0x%x.\n", __FILE__, __LINE__, rv); 
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid sdsfType=%u.\n", __FILE__, __LINE__, sdsfType);
        return DRV_TDM_INVALID_ARGUMENT; /* 直接返回 */
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_vc12SdSfThresholdGet
* 功能描述: 获取VC12通道的SD/SF门限值
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为 1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
*           sdsfType: 0表示获取SD的门限值,1表示获取SF的门限值.
* 输出参数: *pThreshold: 保存门限值.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_vc12SdSfThresholdGet(BYTE chipId, 
                                                BYTE portId, 
                                                BYTE aug1Id, 
                                                BYTE e1LinkId, 
                                                WORD32 sdsfType, 
                                                WORD32 *pThreshold)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    eAtBerRate atDevThrsh = 0;  /* ARRIVE芯片的SD/SF门限值 */
    AtSdhVc pVc12 = NULL;
    AtBerController pBerCtrl = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pThreshold);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* Get VC12 */
    rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);
    
    /* Get BER controller that monitors BER for VC12 channel */
    pBerCtrl = AtSdhChannelBerControllerGet((AtSdhChannel)pVc12);
    DRV_TDM_CHECK_POINTER_IS_NULL(pBerCtrl);
    
    *pThreshold = 0;  /* 先清零再赋值 */
    if (DRV_TDM_SD == sdsfType)  /* SD Type */
    {
        /* Get threshold used to report BER-SD defect */
        atDevThrsh = AtBerControllerSdThresholdGet(pBerCtrl);
    }
    else if (DRV_TDM_SF == sdsfType)  /* SF Type */
    {
        /* Get threshold used to report BER-SF defect */
        atDevThrsh = AtBerControllerSfThresholdGet(pBerCtrl);
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, invalid threshold type, thresholdType=%u.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, sdsfType);  
        return DRV_TDM_INVALID_ARGUMENT;
    }
    if (cAtBerRate1E3 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E3;
    }
    else if (cAtBerRate1E4 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E4;
    }
    else if (cAtBerRate1E5 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E5;
    }
    else if (cAtBerRate1E6 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E6;
    }
    else if (cAtBerRate1E7 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E7;
    }
    else if (cAtBerRate1E8 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E8;
    }
    else if (cAtBerRate1E9 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E9;
    }
    else if (cAtBerRate1E10 == atDevThrsh)
    {
        *pThreshold = DRV_TDM_BER_RATE_1E10;
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, invalid threshold, threshold=%u.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, atDevThrsh);  
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_vc12CurrentBerRateGet
* 功能描述: 获取VC12通道的当前BER速率.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: E1链路编号,取值为1~63.
* 输出参数: *pBerRate: 保存当前的BER Rate.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-17   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_vc12CurrentBerRateGet(BYTE chipId, 
                                                 BYTE portId, 
                                                 BYTE aug1Id, 
                                                 BYTE e1LinkId, 
                                                 WORD32 *pBerRate)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    eAtBerRate atBerRate = 0;  /* ARRIVE芯片的BER rate */
    AtSdhVc pVc12 = NULL;
    AtBerController pBerCtrl = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pBerRate);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    /* Get VC12 */
    rv = drv_tdm_atSdhVc12PtrGet(chipId, portId, aug1Id, e1LinkId, &pVc12);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, drv_tdm_atSdhVc12PtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pVc12);
    
    /* Get BER controller that monitors BER for VC12 channel */
    pBerCtrl = AtSdhChannelBerControllerGet((AtSdhChannel)pVc12);
    DRV_TDM_CHECK_POINTER_IS_NULL(pBerCtrl);
    
    *pBerRate = 0;  /* 先清零再赋值 */
    atBerRate = AtBerControllerCurBerGet(pBerCtrl); /* Get current monitored BER rate */
    if (cAtBerRate1E3 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E3;
    }
    else if (cAtBerRate1E4 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E4;
    }
    else if (cAtBerRate1E5 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E5;
    }
    else if (cAtBerRate1E6 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E6;
    }
    else if (cAtBerRate1E7 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E7;
    }
    else if (cAtBerRate1E8 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E8;
    }
    else if (cAtBerRate1E9 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E9;
    }
    else if (cAtBerRate1E10 == atBerRate)
    {
        *pBerRate = DRV_TDM_BER_RATE_1E10;
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, chipId %u portId %u e1LinkId %u, invalid ber rate, berRate=%u.\n", __FILE__, __LINE__, chipId, portId, e1LinkId, atBerRate);  
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_sdhBerFlagSet
* 功能描述: 设置监测SDH BER的标记.
* 访问的表: 软件表g_drv_tdm_sdh_ber_enable.
* 修改的表: 软件表g_drv_tdm_sdh_ber_enable.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           flag: BER使能标记,DRV_TDM_ENABLE使能,DRV_TDM_DISABLE非使能.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_sdhBerFlagSet(BYTE chipId, WORD32 flag)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    g_drv_tdm_sdh_ber_enable[chipId] = flag;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_sdhBerFlagGet
* 功能描述: 获取监测SDH BER的标记.
* 访问的表: 软件表g_drv_tdm_sdh_ber_enable.
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.           
* 输出参数: pFlag: 保存BER使能标记,DRV_TDM_ENABLE使能,DRV_TDM_DISABLE非使能.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_sdhBerFlagGet(BYTE chipId, WORD32 *pFlag)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pFlag);
    
    *pFlag = g_drv_tdm_sdh_ber_enable[chipId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_sdhBerMonitor
* 功能描述: 监测STM单板的SDH BER (Bit Error Rate).
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pSubslotId: 保存STM单板的子槽位号 
* 输出参数: 无
* 返 回 值: 
* 其它说明: SDH BER 线程的入口函数
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_sdhBerMonitor(VOID *pSubslotId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 initFlag = 0;    /* STM TDM PWE3模块的初始化标记 */
    WORD32 dwBoardStatus = 0;  /* CP3BAN单板的状态 */
    WORD32 *pdwSubslotId = NULL; 
    BYTE subslotId = 0;
    BYTE chipId = 0;         /* ARRIVE芯片的芯片编号 */
    AtDevice pDevice = NULL;
    AtModuleBer pBerModule = NULL;
    WORD32 dwTimeMs = 0;
    
    pdwSubslotId = (WORD32 *)pSubslotId;
    if (NULL == pdwSubslotId)
    { 
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, pdwSubslotId is NULL pointer.\n", __FILE__, __LINE__); 
        return DRV_TDM_NULL_POINTER; 
    }
    subslotId = (BYTE)(*pdwSubslotId);
    free(pdwSubslotId);
    pdwSubslotId = NULL;  /* 释放内存空间,以防成为野指针 */
    BSP_Print(BSP_DEBUG_ALL, "%s line %d, drv_tdm_sdhBerMonitor(), subslotId=%u.\n", __FILE__, __LINE__, subslotId);
    if ((subslotId < DRV_TDM_SUBCARD_ID_START) || (subslotId > DRV_TDM_SUBCARD_ID_END)) 
    { 
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid subslotId, subslotId=%u.\n", __FILE__, __LINE__, subslotId); 
        return DRV_TDM_INVALID_SUBSLOT_ID; 
    }
    chipId = subslotId - (BYTE)1;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    g_drv_tdm_sdh_ber_tid[chipId] = __gettid();  /* save TID */
    
    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    
    /* get BER module */
    pBerModule = (AtModuleBer)AtDeviceModuleGet(pDevice, cAtModuleBer);
    DRV_TDM_CHECK_POINTER_IS_NULL(pBerModule);
    
    while (DRV_TDM_TRUE)
    {
        rv = BSP_cp3banBoardStatusGet((WORD32)subslotId, &dwBoardStatus); /* 获取单板的状态 */
        if (BSP_E_CP3BAN_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, subslotId %u, BSP_cp3banBoardStatusGet() rv=0x%x.\n", __FILE__, __LINE__, subslotId, rv);
            return rv;
        }
        if (BSP_CP3BAN_BOARD_OFFLINE == dwBoardStatus)  /* 单板不在位,直接返回成功 */
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "WARNING: %s line %d, subslotId %u, CP3BAN board is offline.\n", __FILE__, __LINE__, subslotId);
            return DRV_TDM_OK;
        }
        rv = drv_tdm_pwe3InitFlagGet(chipId, &initFlag); /* 获取STM TDM PWE3模块的初始化标记 */
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_SDH_PERF_QRY, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3InitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
            return rv;
        }
        if (DRV_TDM_STM_BOARD_UNINSTALL != initFlag) /* STM TDM PWE3模块已经初始化 */
        {
            if (DRV_TDM_DISABLE != g_drv_tdm_sdh_ber_enable[chipId])
            {
                dwTimeMs = AtModuleBerPeriodicProcess(pBerModule, DRV_TDM_SDH_BER_PERIODIC);
                if (dwTimeMs < DRV_TDM_SDH_BER_PERIODIC)
                {
                    BSP_DelayMs(DRV_TDM_SDH_BER_PERIODIC - dwTimeMs);   
                }
            }
            else
            {
                BSP_DelayMs(1000);
            }
        }
        else
        {
            BSP_DelayMs(1000);
        }
    }

    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_sdhBerThreadCreate
* 功能描述: 创建SDH BER监测的线程.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4. 
* 输出参数: 无
* 返 回 值: 
* 其它说明: 该线程用来监测STM单板的SDH BER (bit error rate).
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_sdhBerThreadCreate(BYTE subslotId)
{
    int rv = DRV_TDM_OK;
    BYTE chipId = 0;
    WORD32 *pdwSubslotId = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);  
    chipId = subslotId - (BYTE)1;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    pdwSubslotId = (WORD32 *)(malloc(sizeof(WORD32)));
    if (NULL == pdwSubslotId)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, allocate memory for pdwSubslotId failed.\n", __FILE__, __LINE__, subslotId);
        return DRV_TDM_MEM_ALLOC_FAIL;
    }
    memset(pdwSubslotId, 0, sizeof(WORD32));
    *pdwSubslotId = (WORD32)subslotId;
    
    init_thread(&(g_drv_tdm_sdh_ber_thread[chipId]), 0, 0, 20, drv_tdm_sdhBerMonitor, (VOID *)pdwSubslotId);
    rv = start_mod_thread(&(g_drv_tdm_sdh_ber_thread[chipId]));
    if (0 != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, start_mod_thread() rv=%d.\n", __FILE__, __LINE__, subslotId, rv);
        return DRV_TDM_CREATE_THREAD_FAIL;    
    }
    g_drv_tdm_sdh_ber_thread_id[chipId] = g_drv_tdm_sdh_ber_thread[chipId].pthrid;
    
    BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, subslotId %u, Create thread sdhBerThread successfully, threadId=0x%x.\n", __FILE__, __LINE__, subslotId, g_drv_tdm_sdh_ber_thread[chipId].pthrid);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_sdhBerThreadDelete
* 功能描述: 删除SDH BER监测的线程.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号 
* 输出参数: 无
* 返 回 值: 
* 其它说明: 该线程用来监测STM单板的SDH BER (bit error rate).
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_sdhBerThreadDelete(BYTE subslotId)
{
    BYTE chipId = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);  
    chipId = subslotId - (BYTE)1;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    stop_mod_thread(&(g_drv_tdm_sdh_ber_thread[chipId]));
    g_drv_tdm_sdh_ber_thread_id[chipId] = g_drv_tdm_sdh_ber_thread[chipId].pthrid;
    g_drv_tdm_sdh_ber_tid[chipId] = 0;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_sdhBerThreadIdGet
* 功能描述: 获取SDH BER线程的threadId.
* 访问的表: 软件表g_drv_tdm_sdh_ber_thread_id.
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3. 
* 输出参数: *pThreadId: 用来保存threadId.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_sdhBerThreadIdGet(BYTE chipId, pthread_t *pThreadId)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pThreadId);
    
    *pThreadId = g_drv_tdm_sdh_ber_thread_id[chipId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_sdhBerTidGet
* 功能描述: 获取SDH BER线程的TID.
* 访问的表: 软件表g_drv_tdm_sdh_ber_tid.
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3. 
* 输出参数: *pTid: 用来保存TID.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_sdhBerTidGet(BYTE chipId, int *pTid)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pTid);
    
    *pTid = g_drv_tdm_sdh_ber_tid[chipId];
    
    return DRV_TDM_OK;
}



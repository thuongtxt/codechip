/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_tdm_test.c
* 文件标识: 
* 其它说明: 驱动STM TDM PWE3模块的单元测试函数的实现
* 当前版本: V1.0
* 作    者: 谢伟生10112265.
* 完成日期: 2013-05-16
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/


#include "bsp_srv_pwe3.h"
#include "bsp_cp3ban_ctrl.h"
#include "drv_tdm_init.h"
#include "drv_stm1_fpga.h"
#include "drv_tdm_intf.h"
#include "drv_tdm_pwe3.h"


/**************************************************************************
* 函数名称: drv_tdm_dbg_fpgaReg16Read
* 功能描述: 读取STM单板的FPGA芯片的16位寄存器.
* 访问的表: 无.
* 修改的表: 无.
* 输入参数: subslotId: STM单板的子槽位号.
*           offsetAddr: FPGA寄存器的偏移地址.
* 输出参数: pValue: 用来保存FPGA寄存器的值. 
* 返 回 值: 
* 其它说明: 该函数只能在非15K-2系统中使用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_dbg_fpgaReg16Read(WORD32 subslotId, 
                                             WORD16 offsetAddr, 
                                             WORD16 *pValue)
{
    WORD32 rv = DRV_TDM_OK;
    WORD16 regAddr = 0;
    WORD16 regValue = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(((BYTE)subslotId));
    DRV_TDM_CHECK_POINTER_IS_NULL(pValue);
    
    *pValue = 0;   /* 先清零再保存值 */
    regAddr = (offsetAddr << 1); /* 通过EPLD间接访问FPGA,FPGA的偏移地址需要乘以2. */
    rv = drv_tdm_fpgaReg16Read(subslotId, regAddr, &regValue);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ACCESS_FPGA, "ERROR: %s line %d, drv_tdm_fpgaReg16Read() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    *pValue = regValue;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_dbg_fpgaReg16Write
* 功能描述: 写STM单板的FPGA芯片的16位寄存器.
* 访问的表: 无.
* 修改的表: 无.
* 输入参数: subslotId: STM单板的子槽位号.
*           offsetAddr: FPGA寄存器的偏移地址.
*           value: 需要写入FPGA寄存器的值.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 该函数只能在非15K-2系统中使用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_dbg_fpgaReg16Write(WORD32 subslotId, 
                                             WORD16 offsetAddr, 
                                             WORD16 value)
{
    WORD32 rv = DRV_TDM_OK;
    WORD16 regAddr = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(((BYTE)subslotId));
    
    regAddr = (offsetAddr << 1); /* 通过EPLD间接访问FPGA,FPGA的偏移地址需要乘以2. */
    rv = drv_tdm_fpgaReg16Write(subslotId, regAddr, value);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ACCESS_FPGA, "ERROR: %s line %d, drv_tdm_fpgaReg16Write() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_dbg_fpgaNTimesRead
* 功能描述: 测试连续N次读取FPGA寄存器.
* 访问的表: 无.
* 修改的表: 无.
* 输入参数: subslotId: STM单板的子槽位号.
*           offsetAddr: FPGA寄存器的偏移地址.
*           times: 读取FPGA寄存器的次数.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 该函数只能在非15K-2系统中使用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_dbg_fpgaNTimesRead(WORD32 subslotId, 
                                              WORD16 offsetAddr, 
                                              WORD32 times)
{
    WORD32 rv = DRV_TDM_OK;
    WORD16 regValue = 0;  /* FPGA寄存器的值 */
    WORD32 i = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(((BYTE)subslotId));
    if (0 == times)
    {
        times = 1;
    }
    
    for (i = 1; i <= times; i++)
    {
        rv = drv_tdm_dbg_fpgaReg16Read(subslotId, offsetAddr, &regValue);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_dbg_fpgaReg16Read() rv=0x%x.\n", __FILE__, __LINE__, rv);
            continue;
        }
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u times %u, FPGA offset address 0x%x, regValue = 0x%x.\n", subslotId, i, offsetAddr, regValue);
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_dbg_fpgaNTimesWrite
* 功能描述: 测试连续N次写FPGA寄存器.
* 访问的表: 无.
* 修改的表: 无.
* 输入参数: subslotId: STM单板的子槽位号.
*           offsetAddr: FPGA寄存器的偏移地址.
*           testValue: 需要写入FPGA寄存器的值.
*           times: 测试FPGA寄存器的次数.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 该函数只能在非15K-2系统中使用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_dbg_fpgaNTimesWrite(WORD32 subslotId, 
                                               WORD16 offsetAddr, 
                                               WORD16 testValue, 
                                               WORD32 times)
{
    WORD32 rv = DRV_TDM_OK;
    WORD16 regValue = 0;  /* 读取出来的FPGA寄存器的值 */
    WORD32 i = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(((BYTE)subslotId));
    if (0 == times)
    {
        times = 1;
    }
    
    for (i = 1; i <= times; i++)
    {
        rv = drv_tdm_dbg_fpgaReg16Write(subslotId, offsetAddr, testValue);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_dbg_fpgaReg16Write() rv=0x%x.\n", __FILE__, __LINE__, rv);
            continue;
        }
        rv = drv_tdm_dbg_fpgaReg16Read(subslotId, offsetAddr, &regValue);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_dbg_fpgaReg16Read() rv=0x%x.\n", __FILE__, __LINE__, rv);
            continue;
        }
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u times %u, FPGA offset address 0x%x, regValue = 0x%x.\n", subslotId, i, offsetAddr, regValue);
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_fpgaReg16Show
* 功能描述: 显示STM单板的FPGA芯片的16位寄存器的值.
* 访问的表: 无.
* 修改的表: 无.
* 输入参数: subslotId: STM单板的子槽位号.
*           offsetAddr: FPGA寄存器的偏移地址.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 该函数只能在非15K-2系统中使用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_fpgaReg16Show(BYTE subslotId, WORD16 offsetAddr)
{
    WORD32 rv = DRV_TDM_OK;
    WORD16 regValue = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    
    rv = drv_tdm_dbg_fpgaReg16Read(((WORD32)subslotId), offsetAddr, &regValue);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_dbg_fpgaReg16Read() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "subslotId %u, FPGA offset address 0x%x, regValue = 0x%x.\n", subslotId, offsetAddr, regValue);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_dbg_pwAdd
* 功能描述: 添加PW
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.
*           portId: STM端口的编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路的编号,取值为1~63.
*           e1TimingMode: E1业务的时钟模式,参见定义T_BSP_SDH_TRIB_TIMING_MODE.
*           clkDomainNo: clock domain NO.从1开始取值.
*           e1ClkState: E1 clock status.具体参见T_BSP_E1_CLOCK_STATUS定义.
*           timeslotBmp: E1时隙位图.
*           encapSize: 封包级联数.
* 输出参数:  
* 返 回 值: 
* 其它说明: 该函数仅仅用来调试.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-27   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_dbg_pwAdd(WORD32 subslotId, 
                                  WORD32 portId, 
                                  WORD32 aug1Id, 
                                  WORD32 e1LinkId, 
                                  WORD32 e1TimingMode, 
                                  WORD32 clkDomainNo, 
                                  WORD32 e1ClkState, 
                                  WORD32 timeslotBmp, 
                                  WORD32 encapSize)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 pldSize = 0;
    WORD32 pwTye = 0;
    WORD32 channelNo = 0;
    WORD16 tmpValue = 0;
    WORD16 vlan1Id = 0;
    WORD16 vlan2Id = 0;
    BYTE chipId = 0;  /* 芯片编号,取值为0~3. */
    BYTE e1TsNum;   /* E1帧的时隙数 */
    BYTE stmIntfIndex = 0;  /* STM端口的索引号,取值为0~7. */
    WORD32 dwStmIntfMode = 0;  /* STM端口模式 */
    T_BSP_SRV_FUNC_PW_BIND_ARG pwArg;

    chipId = (BYTE)(subslotId - 1);
    memset(&pwArg, 0, sizeof(T_BSP_SRV_FUNC_PW_BIND_ARG));
    pwArg.ptPwInfo = (T_BSP_PW_INFO *)malloc(sizeof(T_BSP_PW_INFO));
    if (NULL == pwArg.ptPwInfo)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, allocate memory for pwArg.ptPwInfo failed.\n", __FILE__, __LINE__);
        return DRV_TDM_MEM_ALLOC_FAIL;
    }
    memset(pwArg.ptPwInfo, 0, sizeof(T_BSP_PW_INFO));
    
    rv = drv_tdm_e1TimeslotNumGet(timeslotBmp, &e1TsNum);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_e1TimeslotNumGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        free(pwArg.ptPwInfo);
        pwArg.ptPwInfo = NULL;
        return rv;
    }
    pldSize = encapSize * ((WORD32)e1TsNum);

    if (DRV_TDM_UNFRAME_E1_BMP == timeslotBmp)
    {
        pwTye = BSP_WP_SATOP;
    }
    else
    {
        pwTye = BSP_WP_CESOP_NO_CAS;
    }

    rv = drv_tdm_stmIntfModeGet(chipId, (BYTE)portId, &dwStmIntfMode);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_stmIntfModeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_STM1_INTF == dwStmIntfMode)  /* STM-1 interface mode */
    {
        stmIntfIndex = (BYTE)(portId - 1);
    }
    else if (DRV_TDM_STM4_INTF == dwStmIntfMode) /* STM-4 interface mode */
    {
        rv = drv_tdm_aug1IndexGet((BYTE)portId, (BYTE)aug1Id, &stmIntfIndex);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_aug1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, invalid stmIntfMode, stmIntfMode=%u.\n", __FILE__, __LINE__, dwStmIntfMode);
        return DRV_TDM_INVALID_STM_INTF_MODE;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);

    channelNo = (((WORD32)stmIntfIndex) * 256) + ((WORD32)e1LinkId);
    
    pwArg.dwSubSlotId = subslotId;
    pwArg.dwPortId = portId;
    pwArg.dwAu4Id = aug1Id;
    pwArg.dwTug3Id = ((e1LinkId - 1) / 21) + 1;
    pwArg.dwTug2Id = (((e1LinkId - 1) % 21) / 3) + 1;
    pwArg.dwTu12Id = ((e1LinkId - 1) %3 ) + 1;
    pwArg.dwTimeslotBmp = timeslotBmp;
    pwArg.dwPWid = channelNo;

    pwArg.ptPwInfo->eFrameType = pwTye;
    pwArg.ptPwInfo->dwFrameCnt = encapSize;
    pwArg.ptPwInfo->dwRtpHeaderMode = T_BSP_RTP_DISABLE;
    pwArg.ptPwInfo->clock_mode = e1TimingMode;
    pwArg.ptPwInfo->dwClkDomainId = clkDomainNo;
    pwArg.ptPwInfo->dwE1ClkState = e1ClkState;
    pwArg.ptPwInfo->dwCwEnable = T_BSP_CW_ENABLE;
    pwArg.ptPwInfo->dwSeqEnable = T_BSP_SEQ_ENABLE;
    pwArg.ptPwInfo->dwJitterSize = 80;
    
    if (pldSize > ((WORD32)36))
    {
        vlan1Id = vlan1Id & ((WORD16)0xffc0);
    }
    else
    {
        vlan1Id = (vlan1Id & ((WORD16)0xffc0)) | (((WORD16)(pldSize + 24)) & ((WORD16)0x003f));
    }
    tmpValue = (WORD16)0x0a;
    vlan1Id = (vlan1Id & ((WORD16)0xf83f)) | ((tmpValue << 6) & ((WORD16)0x07c0));
    tmpValue = 0;
    vlan1Id = (vlan1Id & ((WORD16)0xf7ff)) | ((tmpValue << 11) & ((WORD16)0x0800));
    pwArg.ptPwInfo->wZteVlan1Id = vlan1Id;
    
    tmpValue = e1LinkId;
    vlan2Id = (vlan2Id & ((WORD16)0xff00)) | (tmpValue & ((WORD16)0x00ff));
    vlan2Id = vlan2Id & ((WORD16)0xfeff);
    tmpValue = (WORD16)stmIntfIndex;
    vlan2Id = (vlan2Id & ((WORD16)0xf1ff)) | ((tmpValue << 9) & ((WORD16)0x0e00));
    pwArg.ptPwInfo->wZteVlan2Id = vlan2Id;
    
    pwArg.ptPwInfo->wVlan1Pri = 5;
    pwArg.ptPwInfo->wVlan2Pri = 5;
    
    rv = drv_tdm_pwCreate(&pwArg);
    if (DRV_TDM_OK != rv)
    {
        free(pwArg.ptPwInfo);
        pwArg.ptPwInfo = NULL;
        return rv;  
    }

    free(pwArg.ptPwInfo);
    pwArg.ptPwInfo = NULL;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_dbg_nPwsAdd
* 功能描述: 添加N条PW
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.
*           portId: STM端口的编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1TimingMode: E1业务的时钟模式,参见定义T_BSP_TIMING_MODE.
*           timeslotBmp: E1时隙位图.
*           clkDoaminNo: 时钟域编号.
*           e1ClkState: E1链路的时钟状态.
*           encapSize: 封包级联数.
* 输出参数:  
* 返 回 值: 
* 其它说明: 该函数仅仅用来调试,一次性添加63条PW.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-27   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_dbg_nPwsAdd(WORD32 subslotId, 
                                      WORD32 portId, 
                                      WORD32 aug1Id, 
                                      WORD32 e1TimingMode, 
                                      WORD32 timeslotBmp, 
                                      WORD32 clkDoaminNo, 
                                      WORD32 e1ClkState, 
                                      WORD32 encapSize)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 e1LinkId = 0;     /* VC4中的E1链路的编号,取值为1~63. */
    
    for (e1LinkId = 1; e1LinkId <= 63; e1LinkId++)
    {
        rv = drv_tdm_dbg_pwAdd(subslotId, 
                               portId, 
                               aug1Id, 
                               e1LinkId, 
                               e1TimingMode, 
                               clkDoaminNo, 
                               e1ClkState, 
                               timeslotBmp, 
                               encapSize);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, subslotId %u, drv_tdm_dbg_pwAdd() rv=0x%x\n", __FILE__, __LINE__, subslotId, rv);
            return rv;
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_dbg_pwDelete
* 功能描述: 删除PW
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.
*           channelNo: channel NO.
* 输出参数:  
* 返 回 值: 
* 其它说明: 该函数仅仅用来调试.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-27   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_dbg_pwDelete(WORD32 subslotId, WORD32 channelNo)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    T_BSP_SRV_FUNC_PW_UNBIND_ARG unbindPwArg;
    
    memset(&unbindPwArg, 0, sizeof(T_BSP_SRV_FUNC_PW_UNBIND_ARG));
    unbindPwArg.dwSubslotId = subslotId;
    unbindPwArg.dwPWid = channelNo;
    
    rv = drv_tdm_pwRemove(&unbindPwArg);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, subslotId %u channelNo 0x%x, drv_tdm_pwRemove() rv=0x%x.\n", __FILE__, __LINE__, subslotId, channelNo, rv);
        return rv;  
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_dbg_nPwsDelete
* 功能描述: 在某个STM端口删除N条PW
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.
*           startChannelNo: start channel NO.
*           pwCnts: PW数量.
* 输出参数:  
* 返 回 值: 
* 其它说明: 该函数仅仅用来调试.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-27   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_dbg_nPwsDelete(WORD32 subslotId, 
                                        WORD32 startChannelNo, 
                                        WORD32 pwCnts)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 channelNo = 0;
    WORD32 i = 0;
    
    for (i = 0; i < pwCnts; i++)
    {
        channelNo = startChannelNo + i;
        rv = drv_tdm_dbg_pwDelete(subslotId, channelNo);
        if (DRV_TDM_OK != rv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, subslotId %u, drv_tdm_dbg_pwDelete() rv=0x%x\n", __FILE__, __LINE__, subslotId, rv);
            return rv;
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_dbg_nPwsAddDelete
* 功能描述: 在CP3BAN单板上不停地添加删除TDM CES业务.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: STM单板的子槽位号,取值为1~4.
* 输出参数:  
* 返 回 值: 
* 其它说明: 该函数仅仅用来进行压力测试.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-12-12   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_dbg_nPwsAddDelete(WORD32 dwSubslotId)
{
    WORD32 dwRetValue = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 dwBoardType = 0;
    WORD32 dwPortId = 0;
    WORD32 dwMaxPortId = 0;
    WORD32 dwChannelNo = 0;
    WORD32 dwTestTimes = 0;  /* 测试次数 */
    
    dwRetValue = BSP_cp3ban_boardTypeGet(dwSubslotId, &dwBoardType);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_boardTypeGet() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return dwRetValue;
    }
    if (BSP_CP3BAN_8PORT_BOARD == dwBoardType)  /* 8端口的STM-1单板 */
    {
        dwMaxPortId = 8;
    }
    else if (BSP_CP3BAN_4PORT_BOARD == dwBoardType)  /* 4端口的STM-1单板 */
    {
        dwMaxPortId = 4;
    }
    else
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, subslotId %u, invalid board type, boardType=%u.\n", __FILE__, __LINE__, dwSubslotId, dwBoardType);
        return DRV_TDM_INVALID_BOARD_TYPE;
    }
    
    while (DRV_TDM_TRUE)
    {
        for (dwPortId = 1; dwPortId <= dwMaxPortId; dwPortId++)
        {
            dwRetValue = drv_tdm_dbg_nPwsAdd(dwSubslotId, dwPortId, 1, 0, 0xffffffff, 0, 0, 8);
            if (DRV_TDM_OK != dwRetValue)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, subslotId %u, drv_tdm_dbg_nPwsAdd() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "%s line %d, subslotId %u, testTimes = 0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTestTimes);
                return dwRetValue;
            }

            dwChannelNo = (dwPortId - 1) * 256 + 1;
            dwRetValue = drv_tdm_dbg_nPwsDelete(dwSubslotId, dwChannelNo, 63);
            if (DRV_TDM_OK != dwRetValue)
            {
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, subslotId %u, drv_tdm_dbg_nPwsDelete() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
                DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "%s line %d, subslotId %u, testTimes = 0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTestTimes);
                return dwRetValue;
            }
        }

        dwTestTimes++;
        if (dwTestTimes >= 1000)
        {
            return DRV_TDM_OK;
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_dbg_pwPldSizeModify
* 功能描述: 修改PW的payload size.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: STM单板的子槽位号,取值为1~4.
*           dwChannelNo: PW的channel NO.
*           dwEncapNum: 封包级联数.
* 输出参数:  
* 返 回 值: 
* 其它说明: 该函数仅仅用来调试.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-27   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_dbg_pwPldSizeModify(WORD32 dwSubslotId, 
                                               WORD32 dwChannelNo, 
                                               WORD32 dwEncapNum)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    T_BSP_SRV_FUNC_PW_BIND_ARG pwArg;
    
    memset(&pwArg, 0, sizeof(T_BSP_SRV_FUNC_PW_BIND_ARG));
    pwArg.ptPwInfo = (T_BSP_PW_INFO *)(malloc(sizeof(T_BSP_PW_INFO)));
    if (NULL == pwArg.ptPwInfo)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, allocate memory for pwArg.ptPwInfo failed.\n", __FILE__, __LINE__);
        return DRV_TDM_MEM_ALLOC_FAIL;
    }
    memset(pwArg.ptPwInfo, 0, sizeof(T_BSP_PW_INFO));
    pwArg.dwSubSlotId = dwSubslotId;
    pwArg.dwPWid = dwChannelNo;
    pwArg.ptPwInfo->dwFrameCnt = dwEncapNum;
    
    dwRetValue = drv_tdm_pwPldSizeUpdate(&pwArg);
    if (DRV_TDM_OK != dwRetValue)
    {
        free(pwArg.ptPwInfo);
        pwArg.ptPwInfo = NULL;
        return dwRetValue;  
    }
    
    free(pwArg.ptPwInfo);
    pwArg.ptPwInfo = NULL;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_dbg_pwJitBufSizeModify
* 功能描述: 修改PW的jitter buffer size.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: STM单板的子槽位号,取值为1~4.
*           dwChannelNo: PW的channel NO.
*           dwPktCntsInBuf: buffer中的报文个数.
* 输出参数:  
* 返 回 值: 
* 其它说明: 该函数仅仅用来调试.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-27   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_dbg_pwJitBufSizeModify(WORD32 dwSubslotId, 
                                                  WORD32 dwChannelNo, 
                                                  WORD32 dwPktCntsInBuf)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    T_BSP_SRV_FUNC_PW_BIND_ARG pwArg;
    
    memset(&pwArg, 0, sizeof(T_BSP_SRV_FUNC_PW_BIND_ARG));
    pwArg.ptPwInfo = (T_BSP_PW_INFO *)(malloc(sizeof(T_BSP_PW_INFO)));
    if (NULL == pwArg.ptPwInfo)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, allocate memory for pwArg.ptPwInfo failed.\n", __FILE__, __LINE__);
        return DRV_TDM_MEM_ALLOC_FAIL;
    }
    memset(pwArg.ptPwInfo, 0, sizeof(T_BSP_PW_INFO));
    pwArg.dwSubSlotId = dwSubslotId;
    pwArg.dwPWid = dwChannelNo;
    pwArg.ptPwInfo->dwJitterSize = dwPktCntsInBuf;
    
    dwRetValue = drv_tdm_pwJitterBufSizeUpdate(&pwArg);
    if (DRV_TDM_OK != dwRetValue)
    {
        free(pwArg.ptPwInfo);
        pwArg.ptPwInfo = NULL;
        return dwRetValue;  
    }
    
    free(pwArg.ptPwInfo);
    pwArg.ptPwInfo = NULL;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_dbg_pwVlan1PriModify
* 功能描述: 修改PW的VLAN1 priority.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: STM单板的子槽位号,取值为1~4.
*           dwChannelNo: PW的channel NO.
*           vlan1Pri: VLAN1 priority,取值为0~7.
* 输出参数:  
* 返 回 值: 
* 其它说明: 该函数仅仅用来调试.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-27   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_dbg_pwVlan1PriModify(WORD32 dwSubslotId, 
                                                WORD32 dwChannelNo, 
                                                WORD16 vlan1Pri)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    T_BSP_SRV_FUNC_PW_BIND_ARG pwArg;
    
    memset(&pwArg, 0, sizeof(T_BSP_SRV_FUNC_PW_BIND_ARG));
    pwArg.ptPwInfo = (T_BSP_PW_INFO *)(malloc(sizeof(T_BSP_PW_INFO)));
    if (NULL == pwArg.ptPwInfo)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, allocate memory for pwArg.ptPwInfo failed.\n", __FILE__, __LINE__);
        return DRV_TDM_MEM_ALLOC_FAIL;
    }
    memset(pwArg.ptPwInfo, 0, sizeof(T_BSP_PW_INFO));
    pwArg.dwSubSlotId = dwSubslotId;
    pwArg.dwPWid = dwChannelNo;
    pwArg.ptPwInfo->wVlan1Pri = vlan1Pri;

    dwRetValue = drv_tdm_pwVlan1PriUpdate(&pwArg);
    if (DRV_TDM_OK != dwRetValue)
    {
        free(pwArg.ptPwInfo);
        pwArg.ptPwInfo = NULL;
        return dwRetValue;  
    }
    
    free(pwArg.ptPwInfo);
    pwArg.ptPwInfo = NULL;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_dbg_nE1sFrmModeSet
* 功能描述: 设置某个STM端口的N条E1通道的成帧模式
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.
*           portId: STM端口的编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           startE1LinkId: E1通道的开始编号.
*           e1FrameCnt: E1通道的数量
*           frmMode: E1帧的成帧模式.具体参见DRV_TDM_E1_FRAME_MODE的定义.
* 输出参数:  
* 返 回 值: 
* 其它说明: 该函数仅仅用来调试.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-27   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_dbg_nE1sFrmModeSet(BYTE subslotId, 
                                               BYTE portId, 
                                               BYTE aug1Id, 
                                               BYTE startE1LinkId, 
                                               BYTE e1FrameCnt, 
                                               WORD32 frmMode)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE chipId = 0;
    BYTE i = 0;
    BYTE e1LinkId = 0;
    
    chipId = subslotId - (BYTE)1;
    
    for (i = 0; i < e1FrameCnt; i++)
    {
        e1LinkId = startE1LinkId + i;
        rv = drv_tdm_e1FramingModeSet(chipId, portId, aug1Id, e1LinkId, frmMode);
        if (DRV_TDM_OK != rv)
        {
            return rv;
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_dbg_e1TimingUpdate
* 功能描述: E1链路的时钟切换.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4.
*           clkDomainNo: clock domain NO.从1开始取值.
*           newPortId: 新的主E1链路所在STM端口的编号,取值为1~8.
*           newAu4Id: 新的主E1链路所在的AU4编号,从1开始取值.
*           newE1LinkId: 新的主E1链路的编号,取值为1~63.
*           oldPortId: 老的主E1链路所在STM端口的编号,取值为1~8.
*           oldAu4Id: 老的主E1链路所在的AU4编号,从1开始取值.
*           oldE1LinkId: 老的主E1链路的编号,取值为1~63.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 该函数仅仅用来调试.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-31   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_dbg_e1TimingUpdate(BYTE subslotId, 
                                              WORD32 clkDomainNo, 
                                              BYTE newPortId, 
                                              BYTE newAu4Id, 
                                              BYTE newE1LinkId, 
                                              BYTE oldPortId, 
                                              BYTE oldAu4Id, 
                                              BYTE oldE1LinkId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE newTug3Id = 0;
    BYTE newTug2Id = 0;
    BYTE newTu12Id = 0;
    BYTE oldTug3Id = 0;
    BYTE oldTug2Id = 0;
    BYTE oldTu12Id = 0;
    DRV_TDM_CLOCK_SWITCH clkSwitch;

    memset(&clkSwitch, 0, sizeof(DRV_TDM_CLOCK_SWITCH));
    rv = drv_tdm_tug3Tug2Tu12IdGet(newE1LinkId, &newTug3Id, &newTug2Id, &newTu12Id);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_tug3Tug2Tu12IdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    rv = drv_tdm_tug3Tug2Tu12IdGet(oldE1LinkId, &oldTug3Id, &oldTug2Id, &oldTu12Id);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_tug3Tug2Tu12IdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    clkSwitch.subslotId = subslotId;
    clkSwitch.clkDomainNo = clkDomainNo;
    clkSwitch.newMasterE1Link.portId = newPortId;
    clkSwitch.newMasterE1Link.au4Id = newAu4Id;
    clkSwitch.newMasterE1Link.tug3Id = newTug3Id;
    clkSwitch.newMasterE1Link.tug2Id = newTug2Id;
    clkSwitch.newMasterE1Link.tu12Id = newTu12Id;
    clkSwitch.oldMasterE1Link.portId = oldPortId;
    clkSwitch.oldMasterE1Link.au4Id = oldAu4Id;
    clkSwitch.oldMasterE1Link.tug3Id = oldTug3Id;
    clkSwitch.oldMasterE1Link.tug2Id = oldTug2Id;
    clkSwitch.oldMasterE1Link.tu12Id = oldTu12Id;

    rv = drv_tdm_e1TimingRefUpdate(&clkSwitch);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ALL_MSG, "ERROR: %s line %d, drv_tdm_e1TimingRefUpdate() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}



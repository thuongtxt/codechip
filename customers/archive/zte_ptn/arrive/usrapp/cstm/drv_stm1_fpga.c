/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_stm1_fpga.c
* 文件标识: 
* 其它说明: 驱动STM-1 TDM PWE3模块的FPGA芯片的实现函数.
* 当前版本: V1.0
* 作    者: 谢伟生10112265.
* 完成日期: 2013-06-20
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/


#include "drv_stm1_fpga.h"
#include "drv_tdm_init.h"
#include "bsp_cp3ban_ctrl.h"
#include "bsp_cp3ban_common.h"


/* 外部函数声明 */
extern void AtHalDiagBaseAddressSet(WORD32 baseAddress);
extern WORD32 rdd(WORD32 address);


/**************************************************************************
* 函数名称: drv_tdm_fpgaReg16Read
* 功能描述: 读取STM单板的FPGA芯片的16位寄存器.
* 访问的表: 无.
* 修改的表: 无.
* 输入参数: subslotId: STM单板的子槽位号.
*           offsetAddr: FPGA寄存器的偏移地址.
*           pValue: 用来保存FPGA寄存器的值.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 该函数只能在非15K-2系统中使用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_fpgaReg16Read(WORD32 subslotId, 
                                       WORD16 offsetAddr, 
                                       WORD16 *pValue)
{
    WORD32 rv = DRV_TDM_OK;
    WORD16 regValue = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(((BYTE)subslotId));
    DRV_TDM_CHECK_POINTER_IS_NULL(pValue);
    
    *pValue = 0;   /* 先清零再保存值 */
    rv = BSP_cp3banFpga16Read(subslotId, offsetAddr, &regValue);
    if (BSP_E_CP3BAN_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ACCESS_FPGA, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpga16Read() rv=0x%x.\n", __FILE__, __LINE__, subslotId, rv);
        return rv;
    }
    *pValue = 0;   /* 先清零再赋值 */
    *pValue = regValue;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_fpgaReg16Write
* 功能描述: 写STM单板的FPGA芯片的16位寄存器.
* 访问的表: 无.
* 修改的表: 无.
* 输入参数: subslotId: STM单板的子槽位号.
*           offsetAddr: FPGA寄存器的偏移地址.
*           value: 需要写入FPGA寄存器的值.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 该函数只能在非15K-2系统中使用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_fpgaReg16Write(WORD32 subslotId, 
                                       WORD16 offsetAddr, 
                                       WORD16 value)
{
    WORD32 rv = DRV_TDM_OK; 
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(((BYTE)subslotId));
    
    rv = BSP_cp3banFpga16Write(subslotId, offsetAddr, value);
    if (BSP_E_CP3BAN_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ACCESS_FPGA, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpga16Write() rv=0x%x.\n", __FILE__, __LINE__, subslotId, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atReg16Read
* 功能描述: 读取STM单板的FPGA芯片的16位寄存器.
* 访问的表: 无.
* 修改的表: 无.
* 输入参数: subslotId: STM单板的子槽位号.
*           offsetAddr: FPGA寄存器的偏移地址.
*           pValue: 用来保存FPGA寄存器的值.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 该函数只能用在15K-2系统上.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atReg16Read(WORD32 subslotId, WORD16 offsetAddr, WORD16 *pValue)
{
    WORD32 rv = DRV_TDM_OK;
    BSP_CP3BAN_CHIP_BASE_ADDR *pMappingAddr = NULL;
    VOID* pFpgaBaseAddr = NULL;
    WORD32 fpgaBaseAddr = 0;     /* FPGA芯片的基地址 */
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(((BYTE)subslotId));
    DRV_TDM_CHECK_POINTER_IS_NULL(pValue);

    rv = BSP_cp3banMappingBaseAddrGet(subslotId, &pMappingAddr);
    if (BSP_E_CP3BAN_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_ACCESS_FPGA, "ERROR: %s line %d, subslotId %u , BSP_cp3banMappingBaseAddrGet() rv=0x%x.\n", __FILE__, __LINE__, subslotId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pMappingAddr);
    pFpgaBaseAddr = pMappingAddr->fpgaBaseAddr;
    DRV_TDM_CHECK_POINTER_IS_NULL(pFpgaBaseAddr);
    fpgaBaseAddr = (WORD32)pFpgaBaseAddr;

    *pValue = 0;   /* 先清零再保存值 */
    AtHalDiagBaseAddressSet(fpgaBaseAddr);
    *pValue = (WORD16)(rdd((WORD32)offsetAddr));
    
    return DRV_TDM_OK;
}



/***********************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_tdm_pwe3.c
* 文件标识: 
* 其它说明: 驱动STM TDM PWE3模块的业务实现功能模块的函数实现
* 当前版本: V1.0
* 作    者: 谢伟生10112265.
* 完成日期: 2013-03-26.
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
************************************************************/


#include <time.h>
#include "bsp.h"
#include "bsp_oal_api.h"
#include "bsp_srv_pwe3.h"
#include "bsp_hdlc_common.h"
#include "bsp_sdh_common.h"
#include "bsp_cp3ban_ctrl.h"
#include "bsp_cp3ban_common.h"
#include "drv_tdm_pwe3.h"
#include "drv_tdm_intf.h"
#include "drv_tdm_init.h"
#include "attypes.h"
#include "AtCommon.h"
#include "AtClasses.h"
#include "AtDevice.h"
#include "AtChannel.h"
#include "AtModule.h"
#include "AtModulePw.h"
#include "AtPw.h"
#include "AtPwCountersInternal.h"
#include "AtPwPsn.h"
#include "AtModuleEth.h"
#include "AtEthFlow.h"
#include "AtPwCounters.h"
#include "AtModuleSdh.h"
#include "AtSdhChannel.h"
#include "AtSdhLine.h"
#include "AtModulePdh.h"
#include "AtPdhDe1.h"
#include "AtZtePtn.h"


/* 定义全局变量 */
/* 软件表g_drv_tdm_pw_info用来保存PW的配置信息 */
static DRV_TDM_PW_CFG_INFO* g_drv_tdm_pw_info[DRV_TDM_MAX_STM_CARD_NUM] = {NULL};

/* 软件表g_drv_tdm_pw_pkt_cnt用来保存PW报文统计,保存一次性的PW报文统计值 */
static DRV_TDM_PW_CNT_INFO* g_drv_tdm_pw_pkt_cnt[DRV_TDM_MAX_STM_CARD_NUM] = {NULL};

/* 软件表g_drv_tdm_pw_total_pkt_cnt用来保存PW报文统计,保存报文统计的累加值 */
static DRV_TDM_PW_CNT_INFO* g_drv_tdm_pw_total_pkt_cnt[DRV_TDM_MAX_STM_CARD_NUM] = {NULL};

/* 软件表g_drv_tdm_pw_alarm用来保存PW告警 */
static DRV_TDM_PW_ALM_INFO* g_drv_tdm_pw_alarm[DRV_TDM_MAX_STM_CARD_NUM] = {NULL};

/* E1链路绑定的PW的数量 */
static WORD32 g_drv_tdm_e1BindingPwNum[DRV_TDM_MAX_STM_CARD_NUM][DRV_TDM_E1_NUM_PER_BOARD + 1] = {{0}};

/* STM接口绑定的PW的数量 */
static WORD32 g_drv_tdm_stmIntfPwNum[DRV_TDM_MAX_STM_CARD_NUM][DRV_TDM_MAX_STM_PORT_NUM + 1] = {{0}};

/* Ethernet接口绑定的PW的数量 */
static WORD32 g_drv_tdm_ethIntfPwNum[DRV_TDM_MAX_STM_CARD_NUM][DRV_TDM_CHIP_ETH_INTF_NUM] = {{0}};

/* 信号量的创建标记.该信号量用来保护PW操作 */
static WORD32 g_drv_tdm_pw_sem_flag[DRV_TDM_MAX_STM_CARD_NUM] = {0};

/* 信号量ID.该信号量用来保护PW操作 */
static WORD32 g_drv_tdm_pw_sem_id[DRV_TDM_MAX_STM_CARD_NUM] = {0};

/* 信号量的创建标记.该信号量用来保护PWE3性能告警 */
static WORD32 g_drv_tdm_pwe3_perf_alm_sem_flag[DRV_TDM_MAX_STM_CARD_NUM] = {0};

/* 信号量ID.该信号量用来保护PWE3性能告警 */
static WORD32 g_drv_tdm_pwe3_perf_alm_sem_id[DRV_TDM_MAX_STM_CARD_NUM] = {0};

/* g_drv_tdm_pwe3PerfAlmThread用来保存PWE3性能告警线程的信息 */
static struct mod_thread g_drv_tdm_pwe3PerfAlmThread[DRV_TDM_MAX_STM_CARD_NUM] = {{0}};

/* PWE3性能告警线程的threadId */
static pthread_t g_drv_tdm_pwe3PerfAlmThreadId[DRV_TDM_MAX_STM_CARD_NUM] = {0};

/* PWE3性能告警线程的TID */
static int g_drv_tdm_pwe3PerfAlmTid[DRV_TDM_MAX_STM_CARD_NUM] = {0};

/* 使能PWE3性能告警的标记,默认非使能.在初始化单板时,使能标记;在卸载单板时,非使能标记.*/
static WORD32 g_drv_tdm_pwe3PerfAlmFlag[DRV_TDM_MAX_STM_CARD_NUM] = {0};

/* PWE3性能告警线程的延时 */
static WORD32 g_drv_tdm_pwe3PerfAlmDelay = 2;


/**************************************************************************
* 函数名称: drv_tdm_pwSemIdGet
* 功能描述: 获取信号量ID.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护PW操作.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-04-03   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwSemIdGet(BYTE chipId, WORD32 *pSemId)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pSemId);
    
    *pSemId = 0;  /* 先清零再赋值 */
    *pSemId = g_drv_tdm_pw_sem_id[chipId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwSemFlagGet
* 功能描述: 获取信号量的标记.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护PW操作.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-04-03   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwSemFlagGet(BYTE chipId, WORD32 *pFlag)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pFlag);
    
    *pFlag = 0;  /* 先清零再赋值 */
    *pFlag = g_drv_tdm_pw_sem_flag[chipId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwSemCreate
* 功能描述: 创建信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护PW操作.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-04-03   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwSemCreate(BYTE chipId)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    if (DRV_TDM_SEM_NONE_EXISTED == g_drv_tdm_pw_sem_flag[chipId])    
    {
        dwRetValue = BSP_OalCreateSem(1, MUTEX_STYLE, &(g_drv_tdm_pw_sem_id[chipId]));
        if (BSP_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, BSP_OalCreateSem() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwRetValue);
            return DRV_TDM_CREATE_SEM_FAIL;
        }
        g_drv_tdm_pw_sem_flag[chipId] = DRV_TDM_SEM_EXISTED;
    }  
    
    return DRV_TDM_OK;  /* 返回成功 */
}


/**************************************************************************
* 函数名称: drv_tdm_pwSemDestroy
* 功能描述: 销毁信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护PW操作.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-04-03   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwSemDestroy(BYTE chipId)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);

    /* 在销毁信号量之前必须LOCK一下信号量*/
    dwRetValue = drv_tdm_pwSemGet(chipId);
    if (DRV_TDM_SEM_IS_NOT_EXISTED == dwRetValue) /* 如果信号量已经被销毁的话,则直接返回成功 */
    {
        BSP_Print(BSP_DEBUG_ALL, "WARNING: %s line %d, g_drv_tdm_pw_sem_id[%u] has been destroyed.\n", __FILE__, __LINE__, chipId);
        return DRV_TDM_OK;
    }
    else if (DRV_TDM_OK == dwRetValue) /* 函数返回成功的话,则接着往下执行 */
    {
        ;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwRetValue);
        return dwRetValue;
    }
    
    if (DRV_TDM_SEM_NONE_EXISTED != g_drv_tdm_pw_sem_flag[chipId])    
    {
        dwRetValue = BSP_OalDestroySem(g_drv_tdm_pw_sem_id[chipId]);
        if (BSP_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, BSP_OalDestroySem() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwRetValue);
            return DRV_TDM_DESTROY_SEM_FAIL;
        }
        g_drv_tdm_pw_sem_id[chipId] = 0;
        g_drv_tdm_pw_sem_flag[chipId] = DRV_TDM_SEM_NONE_EXISTED;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwSemGet
* 功能描述: 获取信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护PW操作.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-04-03   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwSemGet(BYTE chipId)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    if (DRV_TDM_SEM_NONE_EXISTED != g_drv_tdm_pw_sem_flag[chipId])    
    {
        dwRetValue = BSP_OalSemP(g_drv_tdm_pw_sem_id[chipId], WAIT_FOREVER);
        if (BSP_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, BSP_OalSemP() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwRetValue);
            return DRV_TDM_GET_SEM_FAIL;
        }
    }
    else
    {
        return DRV_TDM_SEM_IS_NOT_EXISTED;
    }
    
    return DRV_TDM_OK;  /* 返回成功 */
}


/**************************************************************************
* 函数名称: drv_tdm_pwSemFree
* 功能描述: 释放信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护PW操作.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-04-03   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwSemFree(BYTE chipId)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    if (DRV_TDM_SEM_NONE_EXISTED != g_drv_tdm_pw_sem_flag[chipId])    
    {
        dwRetValue = BSP_OalSemV(g_drv_tdm_pw_sem_id[chipId]);
        if (BSP_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, BSP_OalSemV() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwRetValue);
            return DRV_TDM_FREE_SEM_FAIL;
        }
    }
    else
    {
        return DRV_TDM_SEM_IS_NOT_EXISTED;
    }
    
    return DRV_TDM_OK;  /* 返回成功 */
}


/**************************************************************************
* 函数名称: drv_tdm_pwe3PerfAlmSemIdGet
* 功能描述: 获取信号量ID.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护PWE3性能统计.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-06-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwe3PerfAlmSemIdGet(BYTE chipId, WORD32 *pSemId)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pSemId);
    
    *pSemId = 0;  /* 先清零再赋值 */
    *pSemId = g_drv_tdm_pwe3_perf_alm_sem_id[chipId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwe3PerfAlmSemFlagGet
* 功能描述: 获取信号量的标记.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护PWE3性能告警.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-06-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwe3PerfAlmSemFlagGet(BYTE chipId, WORD32 *pFlag)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pFlag);
    
    *pFlag = 0;  /* 先清零再赋值 */
    *pFlag = g_drv_tdm_pwe3_perf_alm_sem_flag[chipId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwe3PerfAlmSemCreate
* 功能描述: 创建信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护PWE3性能告警.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-06-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwe3PerfAlmSemCreate(BYTE chipId)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    if (DRV_TDM_SEM_NONE_EXISTED == g_drv_tdm_pwe3_perf_alm_sem_flag[chipId])    
    {
        dwRetValue = BSP_OalCreateSem(1, MUTEX_STYLE, &(g_drv_tdm_pwe3_perf_alm_sem_id[chipId]));
        if (BSP_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, BSP_OalCreateSem() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwRetValue);
            return DRV_TDM_CREATE_SEM_FAIL;
        }
        g_drv_tdm_pwe3_perf_alm_sem_flag[chipId] = DRV_TDM_SEM_EXISTED;
    }  
    
    return DRV_TDM_OK;  /* 返回成功 */
}


/**************************************************************************
* 函数名称: drv_tdm_pwe3PerfAlmSemDestroy
* 功能描述: 销毁信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护PWE3性能告警.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-06-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwe3PerfAlmSemDestroy(BYTE chipId)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);

    /* 在销毁信号量之前必须LOCK一下信号量*/
    dwRetValue = drv_tdm_pwe3PerfAlmSemGet(chipId);
    if (DRV_TDM_SEM_IS_NOT_EXISTED == dwRetValue) /* 如果信号量已经被销毁的话,则直接返回成功 */
    {
        BSP_Print(BSP_DEBUG_ALL, "WARNING: %s line %d, g_drv_tdm_pwe3_perf_alm_sem_id[%u] has been destroyed.\n", __FILE__, __LINE__, chipId);
        return DRV_TDM_OK;
    }
    else if (DRV_TDM_OK == dwRetValue) /* 函数返回成功的话,则接着往下执行 */
    {
        ;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3PerfAlmSemGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwRetValue);
        return dwRetValue;
    }
    
    if (DRV_TDM_SEM_NONE_EXISTED != g_drv_tdm_pwe3_perf_alm_sem_flag[chipId])    
    {
        dwRetValue = BSP_OalDestroySem(g_drv_tdm_pwe3_perf_alm_sem_id[chipId]);
        if (BSP_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, BSP_OalDestroySem() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwRetValue);
            return DRV_TDM_DESTROY_SEM_FAIL;
        }
        g_drv_tdm_pwe3_perf_alm_sem_id[chipId] = 0;
        g_drv_tdm_pwe3_perf_alm_sem_flag[chipId] = DRV_TDM_SEM_NONE_EXISTED;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwe3PerfAlmSemGet
* 功能描述: 获取信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护PWE3性能告警.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-06-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwe3PerfAlmSemGet(BYTE chipId)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    if (DRV_TDM_SEM_NONE_EXISTED != g_drv_tdm_pwe3_perf_alm_sem_flag[chipId])    
    {
        dwRetValue = BSP_OalSemP(g_drv_tdm_pwe3_perf_alm_sem_id[chipId], WAIT_FOREVER);
        if (BSP_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, BSP_OalSemP() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwRetValue);
            return DRV_TDM_GET_SEM_FAIL;
        }
    }
    else
    {
        return DRV_TDM_SEM_IS_NOT_EXISTED;
    }
    
    return DRV_TDM_OK;  /* 返回成功 */
}


/**************************************************************************
* 函数名称: drv_tdm_pwe3PerfAlmSemFree
* 功能描述: 释放信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护PWE3性能告警.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-06-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwe3PerfAlmSemFree(BYTE chipId)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    if (DRV_TDM_SEM_NONE_EXISTED != g_drv_tdm_pwe3_perf_alm_sem_flag[chipId])    
    {
        dwRetValue = BSP_OalSemV(g_drv_tdm_pwe3_perf_alm_sem_id[chipId]);
        if (BSP_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, BSP_OalSemV() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwRetValue);
            return DRV_TDM_FREE_SEM_FAIL;
        }
    }
    else
    {
        return DRV_TDM_SEM_IS_NOT_EXISTED;
    }
    
    return DRV_TDM_OK;  /* 返回成功 */
}


/**************************************************************************
* 函数名称: drv_tdm_pwMemAllocate
* 功能描述: 动态分配内存
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwMemAllocate(BYTE chipId)
{ 
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    if (NULL == g_drv_tdm_pw_info[chipId])
    {
        g_drv_tdm_pw_info[chipId] = (DRV_TDM_PW_CFG_INFO *)(malloc(DRV_TDM_MAX_PW_NUM_PER_STM1_CARD * sizeof(DRV_TDM_PW_CFG_INFO)));
        if (NULL == g_drv_tdm_pw_info[chipId])
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, allocate memory for g_drv_tdm_pw_info[%u] failed.\n", __FILE__, __LINE__, chipId);
            return DRV_TDM_MEM_ALLOC_FAIL;
        }
        memset(g_drv_tdm_pw_info[chipId], 0, (DRV_TDM_MAX_PW_NUM_PER_STM1_CARD * sizeof(DRV_TDM_PW_CFG_INFO)));
    }

    if (NULL == g_drv_tdm_pw_pkt_cnt[chipId])
    {
        g_drv_tdm_pw_pkt_cnt[chipId] = (DRV_TDM_PW_CNT_INFO *)(malloc(DRV_TDM_MAX_PW_NUM_PER_STM1_CARD * sizeof(DRV_TDM_PW_CNT_INFO)));
        if (NULL == g_drv_tdm_pw_pkt_cnt[chipId])
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, allocate memory for g_drv_tdm_pw_pkt_cnt[%u] failed.\n", __FILE__, __LINE__, chipId);
            return DRV_TDM_MEM_ALLOC_FAIL;
        }
        memset(g_drv_tdm_pw_pkt_cnt[chipId], 0, (DRV_TDM_MAX_PW_NUM_PER_STM1_CARD * sizeof(DRV_TDM_PW_CNT_INFO)));
    }

    if (NULL == g_drv_tdm_pw_total_pkt_cnt[chipId])
    {
        g_drv_tdm_pw_total_pkt_cnt[chipId] = (DRV_TDM_PW_CNT_INFO *)(malloc(DRV_TDM_MAX_PW_NUM_PER_STM1_CARD * sizeof(DRV_TDM_PW_CNT_INFO)));
        if (NULL == g_drv_tdm_pw_total_pkt_cnt[chipId])
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, allocate memory for g_drv_tdm_pw_total_pkt_cnt[%u] failed.\n", __FILE__, __LINE__, chipId);
            return DRV_TDM_MEM_ALLOC_FAIL;
        }
        memset(g_drv_tdm_pw_total_pkt_cnt[chipId], 0, (DRV_TDM_MAX_PW_NUM_PER_STM1_CARD * sizeof(DRV_TDM_PW_CNT_INFO)));
    }

    if (NULL == g_drv_tdm_pw_alarm[chipId])
    {
        g_drv_tdm_pw_alarm[chipId] = (DRV_TDM_PW_ALM_INFO *)(malloc(DRV_TDM_MAX_PW_NUM_PER_STM1_CARD * sizeof(DRV_TDM_PW_ALM_INFO)));
        if (NULL == g_drv_tdm_pw_alarm[chipId])
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, allocate memory for g_drv_tdm_pw_alarm[%u] failed.\n", __FILE__, __LINE__, chipId);
            return DRV_TDM_MEM_ALLOC_FAIL;
        }
        memset(g_drv_tdm_pw_alarm[chipId], 0, (DRV_TDM_MAX_PW_NUM_PER_STM1_CARD * sizeof(DRV_TDM_PW_ALM_INFO)));
    }

    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwMemFree
* 功能描述: 释放已经动态分配的内存
* 访问的表: 
* 修改的表: 
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwMemFree(BYTE chipId)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    if (NULL != g_drv_tdm_pw_info[chipId])
    {
        free(g_drv_tdm_pw_info[chipId]);
        g_drv_tdm_pw_info[chipId] = NULL; /* 防止成为野指针 */
    }

    if (NULL != g_drv_tdm_pw_pkt_cnt[chipId])
    {
        free(g_drv_tdm_pw_pkt_cnt[chipId]);
        g_drv_tdm_pw_pkt_cnt[chipId] = NULL; /* 防止成为野指针 */
    }

    if (NULL != g_drv_tdm_pw_total_pkt_cnt[chipId])
    {
        free(g_drv_tdm_pw_total_pkt_cnt[chipId]);
        g_drv_tdm_pw_total_pkt_cnt[chipId] = NULL;  /* 防止成为野指针 */
    }

    if (NULL != g_drv_tdm_pw_alarm[chipId])
    {
        free(g_drv_tdm_pw_alarm[chipId]);
        g_drv_tdm_pw_alarm[chipId] = NULL;  /* 防止成为野指针 */
    }

    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwMemGet
* 功能描述: 获取保存PW信息的内存
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 无
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwMemGet(BYTE chipId, WORD16 pwId, DRV_TDM_PW_CFG_INFO **ppPwInfo)
{ 
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(ppPwInfo);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    *ppPwInfo = &(g_drv_tdm_pw_info[chipId][pwId]);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwe3PktCntGet
* 功能描述: 获取PWE3报文统计.
* 访问的表: 软件表g_drv_tdm_pw_pkt_cnt
* 修改的表: 无
* 输入参数: chipId: 0~3
*           pwId: 0~1023
* 输出参数: *pPktCnt: 保存PWE3报文统计. 
* 返 回 值: 
* 其它说明: 从驱动的软件表中来获取PWE3报文统计.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwe3PktCntGet(BYTE chipId, WORD16 pwId, DRV_TDM_PW_CNT_INFO *pPktCnt)
{
    WORD32 rv = DRV_TDM_OK;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPktCnt);
    
    memset(pPktCnt, 0, sizeof(DRV_TDM_PW_CNT_INFO));
    rv = drv_tdm_pwe3PerfAlmSemGet(chipId);  /* 获取信号量 */
    if (DRV_TDM_OK != rv) 
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3PerfAlmSemGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    if (NULL != g_drv_tdm_pw_pkt_cnt[chipId])
    {
        memcpy(pPktCnt, &(g_drv_tdm_pw_pkt_cnt[chipId][pwId]), sizeof(DRV_TDM_PW_CNT_INFO));
    }
    rv = drv_tdm_pwe3PerfAlmSemFree(chipId);  /* 释放信号量 */
    if (DRV_TDM_OK != rv) 
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3PerfAlmSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwTotalPktCntGet
* 功能描述: 获取累加的PWE3报文统计.
* 访问的表: 软件表g_drv_tdm_pw_total_pkt_cnt
* 修改的表: 无
* 输入参数: chipId: 0~3
*           pwId: 0~1023
* 输出参数: *pPktCnt: 保存PWE3报文统计.  
* 返 回 值: 
* 其它说明:  
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwTotalPktCntGet(BYTE chipId, WORD16 pwId, DRV_TDM_PW_CNT_INFO *pPktCnt)
{
    WORD32 rv = DRV_TDM_OK;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPktCnt);
    
    memset(pPktCnt, 0, sizeof(DRV_TDM_PW_CNT_INFO));  /* 先清零再赋值 */
    rv = drv_tdm_pwe3PerfAlmSemGet(chipId);  /* 获取信号量 */
    if (DRV_TDM_OK != rv) 
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3PerfAlmSemGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    if (NULL != g_drv_tdm_pw_total_pkt_cnt[chipId])
    {
        memcpy(pPktCnt, &(g_drv_tdm_pw_total_pkt_cnt[chipId][pwId]), sizeof(DRV_TDM_PW_CNT_INFO));
    }
    rv = drv_tdm_pwe3PerfAlmSemFree(chipId);  /* 释放信号量 */
    if (DRV_TDM_OK != rv) 
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3PerfAlmSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwe3AlarmGet
* 功能描述: 获取PWE3告警.
* 访问的表: 软件表g_drv_tdm_pw_alarm
* 修改的表: 无
* 输入参数: chipId: 0~3
*           pwId: 0~1023          
* 输出参数: pPwAlm: 保存PWE3告警信息. 
* 返 回 值: 
* 其它说明: 从驱动的软件表中来读取PWE3告警.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwe3AlarmGet(BYTE chipId, WORD16 pwId, DRV_TDM_PW_ALM_INFO *pPwAlm)
{
    WORD32 rv = DRV_TDM_OK;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwAlm);

    memset(pPwAlm, 0, sizeof(DRV_TDM_PW_ALM_INFO));  /* 先清零再赋值 */
    rv = drv_tdm_pwe3PerfAlmSemGet(chipId);  /* 获取信号量 */
    if (DRV_TDM_OK != rv) 
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_ALM_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3PerfAlmSemGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    if (NULL != g_drv_tdm_pw_alarm[chipId])
    {
        memcpy(pPwAlm, &(g_drv_tdm_pw_alarm[chipId][pwId]), sizeof(DRV_TDM_PW_ALM_INFO));
    }
    rv = drv_tdm_pwe3PerfAlmSemFree(chipId);  /* 释放信号量 */
    if (DRV_TDM_OK != rv) 
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_ALM_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3PerfAlmSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwCfgInfoSave
* 功能描述: 保存PW配置信息到软件表g_drv_tdm_pw_info
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 软件表g_drv_tdm_pw_info
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwCfgInfoSave(BYTE chipId, WORD16 pwId, const DRV_TDM_PW_CFG_INFO *pPwInfo)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwInfo);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);

    /* 在保存PW配置信息到软件表之前,先清零软件表 */
    memset(&(g_drv_tdm_pw_info[chipId][pwId]), 0, sizeof(DRV_TDM_PW_CFG_INFO));
    memcpy(&(g_drv_tdm_pw_info[chipId][pwId]), pPwInfo, sizeof(DRV_TDM_PW_CFG_INFO));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwe3PktCntSave
* 功能描述: 保存PWE3报文统计.
* 访问的表: 软件表g_drv_tdm_pw_pkt_cnt
* 修改的表: 软件表g_drv_tdm_pw_pkt_cnt
* 输入参数: chipId: 0~3
*           pwId: 0~1023
*           *pPktCnt: 保存PWE3报文统计. 
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwe3PktCntSave(BYTE chipId, WORD16 pwId, const DRV_TDM_PW_CNT_INFO *pPktCnt)
{
    WORD32 rv = DRV_TDM_OK;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPktCnt);
    
    rv = drv_tdm_pwe3PerfAlmSemGet(chipId);  /* 获取信号量 */
    if (DRV_TDM_OK != rv) 
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3PerfAlmSemGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    if (NULL != g_drv_tdm_pw_pkt_cnt[chipId])
    {
        memset(&(g_drv_tdm_pw_pkt_cnt[chipId][pwId]), 0, sizeof(DRV_TDM_PW_CNT_INFO));
        memcpy(&(g_drv_tdm_pw_pkt_cnt[chipId][pwId]), pPktCnt, sizeof(DRV_TDM_PW_CNT_INFO));
    }
    rv = drv_tdm_pwe3PerfAlmSemFree(chipId);  /* 释放信号量 */
    if (DRV_TDM_OK != rv) 
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3PerfAlmSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwTotalPktCntSave
* 功能描述: 保存PW报文统计信息到软件表g_drv_tdm_pw_total_pkt_cnt
* 访问的表: 软件表g_drv_tdm_pw_total_pkt_cnt
* 修改的表: 软件表g_drv_tdm_pw_total_pkt_cnt
* 输入参数: chipId: 0~3
*           pwId: 0~1024
*           pPktCnt: 保存PWE3报文统计信息.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: g_drv_tdm_pw_total_pkt_cnt保存的是PWE3报文统计的累加值.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwTotalPktCntSave(BYTE chipId, 
                                            WORD16 pwId, 
                                            const DRV_TDM_PW_CNT_INFO *pPktCnt)
{
    WORD32 rv = DRV_TDM_OK;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPktCnt);

    rv = drv_tdm_pwe3PerfAlmSemGet(chipId);  /* 获取信号量 */
    if (DRV_TDM_OK != rv) 
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3PerfAlmSemGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    if (NULL != g_drv_tdm_pw_total_pkt_cnt[chipId])
    {
        g_drv_tdm_pw_total_pkt_cnt[chipId][pwId].txPkts += pPktCnt->txPkts;
        g_drv_tdm_pw_total_pkt_cnt[chipId][pwId].txPayloadBytes += pPktCnt->txPayloadBytes;
        g_drv_tdm_pw_total_pkt_cnt[chipId][pwId].rxPkts += pPktCnt->rxPkts;
        g_drv_tdm_pw_total_pkt_cnt[chipId][pwId].rxPayloadBytes += pPktCnt->rxPayloadBytes;
        g_drv_tdm_pw_total_pkt_cnt[chipId][pwId].rxDiscardedPkts += pPktCnt->rxDiscardedPkts;
        g_drv_tdm_pw_total_pkt_cnt[chipId][pwId].rxMalformedPkts += pPktCnt->rxMalformedPkts;
        g_drv_tdm_pw_total_pkt_cnt[chipId][pwId].rxReorderedPkts += pPktCnt->rxReorderedPkts;
        g_drv_tdm_pw_total_pkt_cnt[chipId][pwId].rxLostPkts += pPktCnt->rxLostPkts;
        g_drv_tdm_pw_total_pkt_cnt[chipId][pwId].rxOutofSeqDropPkts += pPktCnt->rxOutofSeqDropPkts;
        g_drv_tdm_pw_total_pkt_cnt[chipId][pwId].rxOamPkts += pPktCnt->rxOamPkts;
        g_drv_tdm_pw_total_pkt_cnt[chipId][pwId].txLbitPkts += pPktCnt->txLbitPkts;
        g_drv_tdm_pw_total_pkt_cnt[chipId][pwId].txRbitPkts += pPktCnt->txRbitPkts;
        g_drv_tdm_pw_total_pkt_cnt[chipId][pwId].rxLbitPkts += pPktCnt->rxLbitPkts;
        g_drv_tdm_pw_total_pkt_cnt[chipId][pwId].rxRbitPkts += pPktCnt->rxRbitPkts;
        g_drv_tdm_pw_total_pkt_cnt[chipId][pwId].rxJitBufOverrunPkts += pPktCnt->rxJitBufOverrunPkts;
        g_drv_tdm_pw_total_pkt_cnt[chipId][pwId].rxJitBufUnderrunPkts += pPktCnt->rxJitBufUnderrunPkts;
        g_drv_tdm_pw_total_pkt_cnt[chipId][pwId].rxLops += pPktCnt->rxLops;
        g_drv_tdm_pw_total_pkt_cnt[chipId][pwId].rxPktsSentToTdm += pPktCnt->rxPktsSentToTdm;
        g_drv_tdm_pw_total_pkt_cnt[chipId][pwId].txMbitPkts += pPktCnt->txMbitPkts;
        g_drv_tdm_pw_total_pkt_cnt[chipId][pwId].rxMbitPkts += pPktCnt->rxMbitPkts;
    }
    rv = drv_tdm_pwe3PerfAlmSemFree(chipId);  /* 释放信号量 */
    if (DRV_TDM_OK != rv) 
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3PerfAlmSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwAlarmSave
* 功能描述: 保存PW告警信息到软件表g_drv_tdm_pw_alarm
* 访问的表: 软件表g_drv_tdm_pw_alarm
* 修改的表: 软件表g_drv_tdm_pw_alarm
* 输入参数: chipId: 0~3
*           pwId: 0~1024
*           pPwAlarm: 保存PWE3告警信息.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwAlarmSave(BYTE chipId, WORD16 pwId, const DRV_TDM_PW_ALM_INFO *pPwAlarm)
{
    WORD32 rv = DRV_TDM_OK;

    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwAlarm);

    rv = drv_tdm_pwe3PerfAlmSemGet(chipId);  /* 获取信号量 */
    if (DRV_TDM_OK != rv) 
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_ALM_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3PerfAlmSemGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    if (NULL != g_drv_tdm_pw_alarm[chipId])
    {
        memset(&(g_drv_tdm_pw_alarm[chipId][pwId]), 0, sizeof(DRV_TDM_PW_ALM_INFO)); /* 先清零再赋值 */
        memcpy(&(g_drv_tdm_pw_alarm[chipId][pwId]), pPwAlarm, sizeof(DRV_TDM_PW_ALM_INFO));
    }
    rv = drv_tdm_pwe3PerfAlmSemFree(chipId);  /* 释放信号量 */
    if (DRV_TDM_OK != rv) 
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_ALM_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3PerfAlmSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwAlarmClear
* 功能描述: 清除PW告警信息软件表g_drv_tdm_pw_alarm
* 访问的表: 软件表g_drv_tdm_pw_alarm
* 修改的表: 软件表g_drv_tdm_pw_alarm
* 输入参数: chipId: 0~3
*           pwId: 0~1023.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwAlarmClear(BYTE chipId, WORD16 pwId)
{
    WORD32 rv = DRV_TDM_OK;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    
    rv = drv_tdm_pwe3PerfAlmSemGet(chipId);  /* 获取信号量 */
    if (DRV_TDM_OK != rv) 
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_ALM_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3PerfAlmSemGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    if (NULL != g_drv_tdm_pw_alarm[chipId])
    {
        memset(&(g_drv_tdm_pw_alarm[chipId][pwId]), 0, sizeof(DRV_TDM_PW_ALM_INFO));
    }
    rv = drv_tdm_pwe3PerfAlmSemFree(chipId);  /* 释放信号量 */
    if (DRV_TDM_OK != rv) 
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_ALM_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3PerfAlmSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwCfgInfoClear
* 功能描述: 清除PW配置信息软件表g_drv_tdm_pw_info
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 软件表g_drv_tdm_pw_info
* 输入参数: chipId: 0~3
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwCfgInfoClear(BYTE chipId, WORD16 pwId)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    memset(&(g_drv_tdm_pw_info[chipId][pwId]), 0, sizeof(DRV_TDM_PW_CFG_INFO));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwPktCntClear
* 功能描述: 清除PW报文统计信息软件表g_drv_tdm_pw_pkt_cnt
* 访问的表: 软件表g_drv_tdm_pw_pkt_cnt
* 修改的表: 软件表g_drv_tdm_pw_pkt_cnt
* 输入参数: chipId: 0~3
*           pwId: 0~1023.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwPktCntClear(BYTE chipId, WORD16 pwId)
{
    WORD32 rv = DRV_TDM_OK;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    
    rv = drv_tdm_pwe3PerfAlmSemGet(chipId);  /* 获取信号量 */
    if (DRV_TDM_OK != rv) 
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3PerfAlmSemGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    if (NULL != g_drv_tdm_pw_pkt_cnt[chipId])
    {
        memset(&(g_drv_tdm_pw_pkt_cnt[chipId][pwId]), 0, sizeof(DRV_TDM_PW_CNT_INFO));
    }
    rv = drv_tdm_pwe3PerfAlmSemFree(chipId);  /* 释放信号量 */
    if (DRV_TDM_OK != rv) 
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3PerfAlmSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwTotalPktCntClear
* 功能描述: 清除PW报文统计信息软件表g_drv_tdm_pw_total_pkt_cnt
* 访问的表: 软件表g_drv_tdm_pw_total_pkt_cnt
* 修改的表: 软件表g_drv_tdm_pw_total_pkt_cnt
* 输入参数: chipId: 0~3
*           pwId: 0~1023.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwTotalPktCntClear(BYTE chipId, WORD16 pwId)
{
    WORD32 rv = DRV_TDM_OK;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    
    rv = drv_tdm_pwe3PerfAlmSemGet(chipId);  /* 获取信号量 */
    if (DRV_TDM_OK != rv) 
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3PerfAlmSemGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    if (NULL != g_drv_tdm_pw_total_pkt_cnt[chipId])
    {
        memset(&(g_drv_tdm_pw_total_pkt_cnt[chipId][pwId]), 0, sizeof(DRV_TDM_PW_CNT_INFO));
    }
    rv = drv_tdm_pwe3PerfAlmSemFree(chipId);  /* 释放信号量 */
    if (DRV_TDM_OK != rv) 
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3PerfAlmSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwJitterBufSizeGet
* 功能描述: 获取PW的jitter buffer size. 
* 访问的表: 无. 
* 修改的表: 无. 
* 输入参数: channelType: PW类型.
*           timeslotNum: E1帧的时隙数.
*           payloadSize: payload size.
*           pktCntsInBuf: Buffer中存放的报文的个数.
* 输出参数: *pJitBufSize: 保存jitter buffer size的值.
* 返 回 值: 
* 其它说明: 计数PW的jitter buffer size.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-19   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwJitterBufSizeGet(BYTE channelType, 
                                  BYTE timeslotNum, 
                                  WORD16 payloadSize, 
                                  WORD32 pktCntsInBuf, 
                                  WORD32 *pJitBufSize)
{
    WORD32 e1Rate = 0;            /* bytes/ms */
    WORD32 ds0Rate = 0;           /* bytes/ms */
    WORD32 rateOfOnePkt = 0;      /* in microsecond */
    WORD32 minJitterBufSize = 0;
    WORD32 maxJitterBufSize = 0;
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pJitBufSize);
    *pJitBufSize = 0;    /* 先清零再赋值 */
    
    if (DRV_TDM_SATOP_CHANNEL == channelType)
    {
        e1Rate = DRV_TDM_E1_RATE;
        rateOfOnePkt = (((WORD32)payloadSize) * DRV_TDM_1000US_PER_1MS) / e1Rate;
        *pJitBufSize = 2 * pktCntsInBuf * rateOfOnePkt;  /* in microsecond */
        minJitterBufSize = 2 * DRV_TDM_MIN_PKT_NUM_IN_BUF * rateOfOnePkt;
        maxJitterBufSize = 2 * DRV_TDM_MAX_PKT_NUM_IN_BUF * rateOfOnePkt;
    }
    else if ((DRV_TDM_CESOP_BASIC_CHANNEL == channelType) 
            || (DRV_TDM_CESOP_CAS_CHANNEL == channelType))
    {
        ds0Rate = DRV_TDM_DS0_RATE;
        rateOfOnePkt = (((WORD32)payloadSize) * DRV_TDM_1000US_PER_1MS) / (ds0Rate * ((WORD32)timeslotNum));
        *pJitBufSize = 2 * pktCntsInBuf * rateOfOnePkt;        /* in microsecond */
        minJitterBufSize = 2 * DRV_TDM_MIN_PKT_NUM_IN_BUF * rateOfOnePkt;
        maxJitterBufSize = 2 * DRV_TDM_MAX_PKT_NUM_IN_BUF * rateOfOnePkt;
    }
    else 
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid parameter, channelType = 0x%x.\n", __FILE__, __LINE__, channelType);
        return DRV_TDM_INVALID_ARGUMENT;
    }

    /* FPGA能够支持的最大jitter buffer size为512000us. */
    if (maxJitterBufSize > DRV_TDM_MAX_JITTER_BUF_SIZE)
    {
        maxJitterBufSize = DRV_TDM_MAX_JITTER_BUF_SIZE;
    }
    if ((*pJitBufSize) < minJitterBufSize)
    {
        *pJitBufSize = minJitterBufSize;
    }
    if ((*pJitBufSize) > maxJitterBufSize)
    {
        *pJitBufSize = maxJitterBufSize;
    }

    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwJitterBufSizeCheck
* 功能描述: 检查PW的jitter buffer size. 
* 访问的表: 无. 
* 修改的表: 无. 
* 输入参数: channelType: PW类型.
*           timeslotNum: E1帧的时隙数.
*           payloadSize: payload size.
*           jitterBufSize: jitter buffer size.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 检查PW的jitter buffer size的范围.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-19   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwJitterBufSizeCheck(BYTE channelType, 
                                    BYTE timeslotNum, 
                                    WORD16 payloadSize, 
                                    WORD32 jitterBufSize)
{
    WORD32 e1Rate = 0;            /* bytes/ms */
    WORD32 ds0Rate = 0;           /* bytes/ms */
    WORD32 rateOfOnePkt = 0;      /* in microsecond */
    WORD32 minJitterBufSize = 0;
    WORD32 maxJitterBufSize = 0;
    
    if (DRV_TDM_SATOP_CHANNEL == channelType)
    {
        e1Rate = DRV_TDM_E1_RATE;
        rateOfOnePkt = (((WORD32)payloadSize) * DRV_TDM_1000US_PER_1MS) / e1Rate;
        minJitterBufSize = 2 * DRV_TDM_MIN_PKT_NUM_IN_BUF * rateOfOnePkt;
        maxJitterBufSize = 2 * DRV_TDM_MAX_PKT_NUM_IN_BUF * rateOfOnePkt;
    }
    else if ((DRV_TDM_CESOP_BASIC_CHANNEL == channelType) 
            || (DRV_TDM_CESOP_CAS_CHANNEL == channelType))
    {
        ds0Rate = DRV_TDM_DS0_RATE;
        rateOfOnePkt = (((WORD32)payloadSize) * DRV_TDM_1000US_PER_1MS) / (ds0Rate * ((WORD32)timeslotNum));
        minJitterBufSize = 2 * DRV_TDM_MIN_PKT_NUM_IN_BUF * rateOfOnePkt;
        maxJitterBufSize = 2 * DRV_TDM_MAX_PKT_NUM_IN_BUF * rateOfOnePkt;
    }
    else 
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid parameter, channelType = 0x%x.\n", __FILE__, __LINE__, channelType);
        return DRV_TDM_INVALID_ARGUMENT;
    }

    if ((jitterBufSize < minJitterBufSize) 
        || (jitterBufSize > maxJitterBufSize) 
        || (jitterBufSize > DRV_TDM_MAX_JITTER_BUF_SIZE))
    {
        return DRV_TDM_INVALID_PW_JIT_BUF_SIZE;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1BindingPwNumGet
* 功能描述: 获取E1链路绑定的PW的数量.
* 访问的表: 软件表g_drv_tdm_e1BindingPwNum.
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路的编号,取值为1~63.
* 输出参数: *pPwNum: 保存E1链路绑定的PW的数量. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-30   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1BindingPwNumGet(BYTE chipId, 
                                              BYTE portId, 
                                              BYTE aug1Id, 
                                              BYTE e1LinkId, 
                                              WORD32 *pPwNum)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 linkId = 0;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwNum);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    /* 根据portId,aug1Id和VC4中的e1LinkId来计算单板上的E1链路编号. */
    rv = drv_tdm_linkIdGet(chipId, portId, aug1Id, e1LinkId, &linkId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_linkIdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_LINK_ID(linkId);
    
    *pPwNum = 0;   /* 先清零再赋值 */
    *pPwNum = g_drv_tdm_e1BindingPwNum[chipId][linkId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1BindingPwNumSet
* 功能描述: 设置E1链路绑定的PW的数量.
* 访问的表: 软件表g_drv_tdm_e1BindingPwNum.
* 修改的表: 软件表g_drv_tdm_e1BindingPwNum.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路的编号,取值为1~63.
*           operateType: 操作类型,具体参见DRV_TDM_PW_NUM_OPERATE定义.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-30   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1BindingPwNumSet(BYTE chipId, 
                                             BYTE portId, 
                                             BYTE aug1Id, 
                                             BYTE e1LinkId, 
                                             WORD32 operateType)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 linkId = 0;  /* STM单板上的E1链路编号,取值为1~504. */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(aug1Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    /* 根据portId,aug1Id和VC4中的e1LinkId来计算单板上的E1链路编号. */
    rv = drv_tdm_linkIdGet(chipId, portId, aug1Id, e1LinkId, &linkId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_linkIdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_LINK_ID(linkId);

    if (DRV_TDM_PW_NUM_DECREMENT == operateType)
    {
        if (g_drv_tdm_e1BindingPwNum[chipId][linkId] > 0)
        {
            g_drv_tdm_e1BindingPwNum[chipId][linkId]--;
        }
    }
    else if (DRV_TDM_PW_NUM_INCREMENT == operateType)
    {
        g_drv_tdm_e1BindingPwNum[chipId][linkId]++;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid operate type, operateType=%u.\n", __FILE__, __LINE__, operateType);
        return DRV_TDM_INVALID_ARGUMENT;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1BindingPwNumClear
* 功能描述: 清空E1链路绑定的PW的数量.
* 访问的表: 软件表g_drv_tdm_e1BindingPwNum.
* 修改的表: 软件表g_drv_tdm_e1BindingPwNum.
* 输入参数: chipId: 芯片编号,取值为0~3.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-30   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1BindingPwNumClear(BYTE chipId)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    memset(&(g_drv_tdm_e1BindingPwNum[chipId][0]), 0, (sizeof(WORD32) * (DRV_TDM_E1_NUM_PER_BOARD + 1)));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stmIntfPwNumGet
* 功能描述: 获取STM端口绑定的PW的数量.
* 访问的表: 软件表g_drv_tdm_stmIntfPwNum.
* 修改的表: 无.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
* 输出参数: *pdwPwNum: 保存STM端口绑定的PW的数量. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-01-24   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stmIntfPwNumGet(BYTE chipId, BYTE portId, WORD32 *pdwPwNum)
{
    WORD32 rv = DRV_TDM_OK;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pdwPwNum);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    *pdwPwNum = 0;
    *pdwPwNum = g_drv_tdm_stmIntfPwNum[chipId][portId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stmIntfPwNumSet
* 功能描述: 设置STM端口绑定的PW的数量.
* 访问的表: 软件表g_drv_tdm_stmIntfPwNum.
* 修改的表: 软件表g_drv_tdm_stmIntfPwNum.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           operateType: 操作类型,具体参见DRV_TDM_PW_NUM_OPERATE定义.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-01-24   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stmIntfPwNumSet(BYTE chipId, BYTE portId, WORD32 operateType)
{
    WORD32 rv = DRV_TDM_OK;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    if (DRV_TDM_PW_NUM_DECREMENT == operateType)
    {
        if (g_drv_tdm_stmIntfPwNum[chipId][portId] > 0)
        {
            g_drv_tdm_stmIntfPwNum[chipId][portId]--;
        }
    }
    else if (DRV_TDM_PW_NUM_INCREMENT == operateType)
    {
        g_drv_tdm_stmIntfPwNum[chipId][portId]++;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid operate type, operateType=%u.\n", __FILE__, __LINE__, operateType);
        return DRV_TDM_INVALID_ARGUMENT;
    }

    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_stmIntfPwNumClear
* 功能描述: 清除STM端口绑定的PW的数量.
* 访问的表: 软件表g_drv_tdm_stmIntfPwNum.
* 修改的表: 软件表g_drv_tdm_stmIntfPwNum.
* 输入参数: chipId: 芯片编号,取值为0~3.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-01-24   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_stmIntfPwNumClear(BYTE chipId)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    memset(&(g_drv_tdm_stmIntfPwNum[chipId][0]), 0, (sizeof(WORD32) * (DRV_TDM_MAX_STM_PORT_NUM + 1)));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_ethIntfPwNumGet
* 功能描述: 获取Ethernet接口绑定的PW的数量.
* 访问的表: 软件表g_drv_tdm_ethIntfPwNum.
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3.
*           ethIntfId: Ethernet接口编号,取值为0~1..
* 输出参数: *pPwNum: 保存Ethernet接口绑定的PW的数量. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-30   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_ethIntfPwNumGet(BYTE chipId, BYTE ethIntfId, WORD32 *pPwNum)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_ETH_INTF_INDEX(ethIntfId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwNum);
    
    *pPwNum = 0;  /* 先清零再赋值 */
    *pPwNum = g_drv_tdm_ethIntfPwNum[chipId][ethIntfId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_ethIntfPwNumSet
* 功能描述: 设置Ethernet接口绑定的PW的数量.
* 访问的表: 软件表g_drv_tdm_ethIntfPwNum.
* 修改的表: 软件表g_drv_tdm_ethIntfPwNum.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           ethIntfId: Ethernet接口编号,取值为0~1.
*           operateType: 操作类型,具体参见DRV_TDM_PW_NUM_OPERATE定义.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-30   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_ethIntfPwNumSet(BYTE chipId, BYTE ethIntfId, WORD32 operateType)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_ETH_INTF_INDEX(ethIntfId);
    
    if (DRV_TDM_PW_NUM_DECREMENT == operateType)
    {
        if (g_drv_tdm_ethIntfPwNum[chipId][ethIntfId] > 0)
        {
            g_drv_tdm_ethIntfPwNum[chipId][ethIntfId]--;
        }
    }
    else if (DRV_TDM_PW_NUM_INCREMENT == operateType)
    {
        g_drv_tdm_ethIntfPwNum[chipId][ethIntfId]++;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid operate type, operateType=%u.\n", __FILE__, __LINE__, operateType);
        return DRV_TDM_INVALID_ARGUMENT;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_ethIntfPwNumClear
* 功能描述: 清空Ethernet接口绑定的PW的数量.
* 访问的表: 软件表g_drv_tdm_ethIntfPwNum.
* 修改的表: 软件表g_drv_tdm_ethIntfPwNum.
* 输入参数: chipId: 芯片编号,取值为0~3.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-30   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_ethIntfPwNumClear(BYTE chipId)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    memset(&(g_drv_tdm_ethIntfPwNum[chipId][0]), 0, (DRV_TDM_CHIP_ETH_INTF_NUM * sizeof(WORD32)));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwE1FramingModeSet
* 功能描述: 设置PW软件表中的E1帧模式.
* 访问的表: 软件表g_drv_tdm_pw_info.
* 修改的表: 软件表g_drv_tdm_pw_info.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           aug1Id: AUG1编号,取值为1~4.
*           e1LinkId: VC4中的E1链路的编号,取值为1~63.
*           e1FrameMode: E1 frame mode,具体参见定义.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-30   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwE1FramingModeSet(BYTE chipId, 
                                               BYTE portId, 
                                               BYTE aug1Id, 
                                               BYTE e1LinkId, 
                                               BYTE e1FrameMode)
{
    WORD16 pwId = 0;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    for (pwId = DRV_TDM_PW_ID_MIN; pwId <= DRV_TDM_PW_ID_MAX; pwId++)
    {
        if (DRV_TDM_PW_FREE != g_drv_tdm_pw_info[chipId][pwId].pwState)
        {
            if ((portId == g_drv_tdm_pw_info[chipId][pwId].acInfo.portId) 
                && (aug1Id == g_drv_tdm_pw_info[chipId][pwId].acInfo.au4Id) 
                && (e1LinkId == g_drv_tdm_pw_info[chipId][pwId].acInfo.e1LinkId))
            {
                g_drv_tdm_pw_info[chipId][pwId].acInfo.e1FramingMode = e1FrameMode;
            }
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwCfgInfoInitialize
* 功能描述: 通过产品管理的PW信息来初始化驱动的PW配置信息
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pPwArg: 产品管理的PW配置信息.
* 输出参数: *pPwCfgInfo: 保存驱动的PW配置信息. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-29   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwCfgInfoInitialize(const T_BSP_SRV_FUNC_PW_BIND_ARG *pPwArg, 
                                          DRV_TDM_PW_CFG_INFO *pPwCfgInfo)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 systemType = 0;   /* ZXUPN15000的系统类型 */
    WORD32 boardType = 0;    /* 单板类型 */
    BYTE e1LinkId = 0;
    BYTE e1FrmMode = 0;
    BYTE e1TsNum = 0;   /* E1帧的时隙数 */
    WORD32 jitBufSize = 0;
    WORD32 dwSubslotId = 0;  /* STM单板的子槽位号 */
    BYTE ucDstMacAddr[DRV_TDM_MAC_ADDR_SIZE] = {0};  /* STM单板的DMAC */
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwArg);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwArg->ptPwInfo);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCfgInfo);
    
    memset(pPwCfgInfo, 0, sizeof(DRV_TDM_PW_CFG_INFO));  /* 赋值之前先清零 */
    systemType = BSP_cp3banSystemTypeGet();  /* 获取ZXUPN15000系统类型. */

    pPwCfgInfo->acInfo.subslotId = (BYTE)(pPwArg->dwSubSlotId);
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM != systemType)   /* 15K-2系统 */
    {
        pPwCfgInfo->acInfo.subslotId = 1;   /* 对于15K-2系统,子槽位始终取值为1 */
    }
    dwSubslotId = (WORD32)(pPwCfgInfo->acInfo.subslotId);
    pPwCfgInfo->acInfo.chipId = pPwCfgInfo->acInfo.subslotId - (BYTE)1;
    pPwCfgInfo->acInfo.portId = (BYTE)(pPwArg->dwPortId);
    pPwCfgInfo->acInfo.au4Id = (BYTE)(pPwArg->dwAu4Id);
    pPwCfgInfo->acInfo.tug3Id = (BYTE)(pPwArg->dwTug3Id);
    pPwCfgInfo->acInfo.tug2Id = (BYTE)(pPwArg->dwTug2Id);
    pPwCfgInfo->acInfo.tu12Id = (BYTE)(pPwArg->dwTu12Id);
    rv = drv_tdm_e1LinkIdGet(pPwCfgInfo->acInfo.tug3Id, 
                             pPwCfgInfo->acInfo.tug2Id, 
                             pPwCfgInfo->acInfo.tu12Id, 
                             &e1LinkId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_e1LinkIdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    pPwCfgInfo->acInfo.e1LinkId = e1LinkId;
    rv = drv_tdm_e1FramingModeGet(pPwCfgInfo->acInfo.chipId, 
                                  pPwCfgInfo->acInfo.portId, 
                                  pPwCfgInfo->acInfo.au4Id, 
                                  pPwCfgInfo->acInfo.e1LinkId, 
                                  &e1FrmMode);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_e1FramingModeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    pPwCfgInfo->acInfo.e1FramingMode = e1FrmMode;
    if (BSP_SDH_TRIB_SYSTEM_TIMING == pPwArg->ptPwInfo->clock_mode)
    {
        pPwCfgInfo->acInfo.e1TimingMode = DRV_TDM_TIMING_MODE_SYS;
    }
    else if (BSP_SDH_TRIB_ACR_TIMING == pPwArg->ptPwInfo->clock_mode)
    {
        pPwCfgInfo->acInfo.e1TimingMode = DRV_TDM_TIMING_MODE_ACR;
    }
    else if (BSP_SDH_TRIB_DCR_TIMING == pPwArg->ptPwInfo->clock_mode)
    {
        pPwCfgInfo->acInfo.e1TimingMode = DRV_TDM_TIMING_MODE_DCR;
    }
    else if (BSP_SDH_TRIB_LOOP_TIMING == pPwArg->ptPwInfo->clock_mode)
    {
        pPwCfgInfo->acInfo.e1TimingMode = DRV_TDM_TIMING_MODE_LOOP;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid parameter, timingMode=0x%x.\n", __FILE__, __LINE__, pPwArg->ptPwInfo->clock_mode);
        return DRV_TDM_INVALID_ARGUMENT;
    }
    pPwCfgInfo->acInfo.clkDomainNo = pPwArg->ptPwInfo->dwClkDomainId;    
    if (T_BSP_E1_CLOCK_SLAVE == pPwArg->ptPwInfo->dwE1ClkState)
    {
        pPwCfgInfo->acInfo.e1LinkClkState = DRV_TDM_E1_LINK_SLAVE;
    }
    else if (T_BSP_E1_CLOCK_MASTER == pPwArg->ptPwInfo->dwE1ClkState)
    {
        pPwCfgInfo->acInfo.e1LinkClkState = DRV_TDM_E1_LINK_MASTER;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid parameter, e1LinkClkState=0x%x.\n", __FILE__, __LINE__, pPwArg->ptPwInfo->dwE1ClkState);
        return DRV_TDM_INVALID_ARGUMENT;
    }
    pPwCfgInfo->acInfo.timeslotBmp = pPwArg->dwTimeslotBmp;
    rv = drv_tdm_e1TimeslotNumGet(pPwCfgInfo->acInfo.timeslotBmp, &e1TsNum);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_e1TimeslotNumGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    pPwCfgInfo->acInfo.e1TsNum = e1TsNum;
    pPwCfgInfo->acInfo.channelNo = pPwArg->dwPWid;   

    rv = BSP_cp3ban_boardTypeGet((WORD32)(pPwCfgInfo->acInfo.subslotId), &boardType);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_boardTypeGet() rv=0x%x\n", __FILE__, __LINE__, pPwCfgInfo->acInfo.subslotId, rv);
        return rv;
    }
    
    if (BSP_CP3BAN_8PORT_BOARD == boardType)  /* 8端口的STM单板 */
    {
        if (pPwCfgInfo->acInfo.portId <= (DRV_TDM_MAX_STM_PORT_NUM / 2))
        {
            pPwCfgInfo->ethIntfIndex = DRV_TDM_CHIP_ETH_INTF_ID_START; /* 前4个STM端口上的E1业务经过ARRIVE芯片的SGMII 0接口;*/
        }
        else
        {
            pPwCfgInfo->ethIntfIndex = DRV_TDM_CHIP_ETH_INTF_ID_END; /* 后4个STM端口上的E1业务经过ARRIVE芯片的SGMII 1接口;*/
        }
    }
    else if (BSP_CP3BAN_4PORT_BOARD == boardType)  /* 4端口的STM单板 */
    {
        pPwCfgInfo->ethIntfIndex = DRV_TDM_CHIP_ETH_INTF_ID_START;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, invalid board type, boardType=%u.\n", __FILE__, __LINE__, pPwCfgInfo->acInfo.subslotId, boardType);
        return DRV_TDM_INVALID_BOARD_TYPE;
    }
    pPwCfgInfo->ethIntfSwitchFlag = DRV_TDM_ETH_INTF_NONE_SWITCH;
    
    pPwCfgInfo->encapNum = (WORD16)(pPwArg->ptPwInfo->dwFrameCnt);
    pPwCfgInfo->pldSize = (pPwCfgInfo->encapNum) * ((WORD16)e1TsNum);
    DRV_TDM_CHECK_PW_PAYLOAD_SIZE(pPwCfgInfo->pldSize); /* 检查PW的payload size */
    
    pPwCfgInfo->vlan1Tag.vlan1Id = pPwArg->ptPwInfo->wZteVlan1Id;
    pPwCfgInfo->vlan1Tag.priority = (BYTE)(pPwArg->ptPwInfo->wVlan1Pri);
    pPwCfgInfo->vlan1Tag.cfi = (BYTE)0;
    pPwCfgInfo->vlan2Tag.vlan2Id = pPwArg->ptPwInfo->wZteVlan2Id;
    pPwCfgInfo->vlan2Tag.priority = (BYTE)(pPwArg->ptPwInfo->wVlan2Pri);
    pPwCfgInfo->vlan2Tag.cfi = (BYTE)0;
    DRV_TDM_CHECK_PW_VLAN_PRIORITY(pPwCfgInfo->vlan1Tag.priority);
    DRV_TDM_CHECK_PW_VLAN_PRIORITY(pPwCfgInfo->vlan2Tag.priority);
    
    if (T_BSP_RTP_DISABLE == pPwArg->ptPwInfo->dwRtpHeaderMode)
    {
        pPwCfgInfo->rtpEnable = DRV_TDM_RTP_DISABLE;
    }
    else
    {
        pPwCfgInfo->rtpEnable = DRV_TDM_RTP_ENABLE;
    }
    if (DRV_TDM_TIMING_MODE_DCR == pPwCfgInfo->acInfo.e1TimingMode)  /* DCR时钟模式 */
    {
        pPwCfgInfo->rtpEnable = DRV_TDM_RTP_ENABLE;  /* DCR时钟模式下,必须使能RTP */
    }
    if (T_BSP_TIMESTAMP_DIFFERENTIAL == pPwArg->ptPwInfo->dwTimestampMethod)
    {
        pPwCfgInfo->rtpCfg.timestampMode = DRV_TDM_TIMESTAMP_DIFFERENTIAL;
    }
    else if (T_BSP_TIMESTAMP_ABSOLUTE == pPwArg->ptPwInfo->dwTimestampMethod)
    {
        pPwCfgInfo->rtpCfg.timestampMode = DRV_TDM_TIMESTAMP_ABSOLUTE;
    }
    else
    {
        pPwCfgInfo->rtpCfg.timestampMode = DRV_TDM_TIMESTAMP_DIFFERENTIAL;
    }
    pPwCfgInfo->rtpCfg.timestampMode = DRV_TDM_TIMESTAMP_DIFFERENTIAL; /* 驱动默认配置为differential mode. */
    pPwCfgInfo->rtpCfg.payloadTypeValue = (BYTE)(pPwArg->ptPwInfo->dwPtValue);
    pPwCfgInfo->rtpCfg.payloadTypeValue = DRV_TDM_RTP_PAYLOAD_VALUE_CES; /* 对于TDM CES业务,payload type需要设置为96. */
    if (T_BSP_CW_DISABLE == pPwArg->ptPwInfo->dwCwEnable)
    {
        pPwCfgInfo->cwEnable = DRV_TDM_PW_CW_DISABLE;
    }
    else
    {
        pPwCfgInfo->cwEnable = DRV_TDM_PW_CW_ENABLE;
    }
    if (T_BSP_SEQ_DISABLE == pPwArg->ptPwInfo->dwSeqEnable)
    {
        pPwCfgInfo->seqEnable = DRV_TDM_PW_CW_SEQ_DISABLE;
    }
    else
    {
        pPwCfgInfo->seqEnable = DRV_TDM_PW_CW_SEQ_ENABLE;
    }
    if (BSP_WP_SATOP == pPwArg->ptPwInfo->eFrameType)
    {
        pPwCfgInfo->channelMode = DRV_TDM_SATOP_CHANNEL;
    }
    else if (BSP_WP_CESOP_WITH_CAS == pPwArg->ptPwInfo->eFrameType)
    {
        pPwCfgInfo->channelMode = DRV_TDM_CESOP_CAS_CHANNEL;
    }
    else if (BSP_WP_CESOP_NO_CAS == pPwArg->ptPwInfo->eFrameType)
    {
        pPwCfgInfo->channelMode = DRV_TDM_CESOP_BASIC_CHANNEL;
    }
    else
    {
        pPwCfgInfo->channelMode = DRV_TDM_UNKNOWN_CHANNEL;
    }
    if (T_BSP_PW_IDLE_CODE_DISABLE == pPwArg->ptPwInfo->bUdIdle)
    {
        pPwCfgInfo->ucIdleCodeFlag = DRV_TDM_PW_IDLE_CODE_DISABLE;
    }
    else
    {
        pPwCfgInfo->ucIdleCodeFlag = DRV_TDM_PW_IDLE_CODE_ENABLE;
    }
    pPwCfgInfo->ucIdleCodeFlag = DRV_TDM_PW_IDLE_CODE_ENABLE; /* 此处将PW idle code使能,防止产品管理的参数错误 */
    pPwCfgInfo->ucIdleCode = (BYTE)(pPwArg->ptPwInfo->vIdleCode);  

    /* 获取PW的jitter buffer size.  */
    pPwCfgInfo->pktNumInBuf = pPwArg->ptPwInfo->dwJitterSize;
    rv = drv_tdm_pwJitterBufSizeGet(pPwCfgInfo->channelMode, 
                                    e1TsNum, 
                                    pPwCfgInfo->pldSize, 
                                    pPwCfgInfo->pktNumInBuf, 
                                    &jitBufSize);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_pwJitterBufSizeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    pPwCfgInfo->jitterBufSize = jitBufSize;
    pPwCfgInfo->jitterDelay = (pPwCfgInfo->jitterBufSize) / ((WORD32)2);

    rv = BSP_cp3banDstMacAddrGet(dwSubslotId, ucDstMacAddr);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, BSP_cp3banDstMacAddrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    memcpy(pPwCfgInfo->dstMacAddr, ucDstMacAddr, DRV_TDM_MAC_ADDR_SIZE);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwAcInfoCheck
* 功能描述: 检查新配置的PW是否与已经添加成功的PW的AC信息是否存在重叠
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 无
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwAcInfoCheck(const DRV_TDM_PW_CFG_INFO* pPwCfgInfo)
{
    BYTE chipId = 0;
    WORD16 pwId = 0;
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCfgInfo);
    chipId = pPwCfgInfo->acInfo.chipId;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    for (pwId = DRV_TDM_PW_ID_MIN; pwId <= DRV_TDM_PW_ID_MAX; pwId++)
    {
        if (DRV_TDM_PW_FREE != g_drv_tdm_pw_info[chipId][pwId].pwState) /* pw is used */
        {
            /* 新配置的PW与已经添加成功的PW存在重叠.只需要检查接入链路是否重叠即可. */
            if ((pPwCfgInfo->acInfo.portId == g_drv_tdm_pw_info[chipId][pwId].acInfo.portId) 
                && (pPwCfgInfo->acInfo.au4Id == g_drv_tdm_pw_info[chipId][pwId].acInfo.au4Id)
                && (pPwCfgInfo->acInfo.tug3Id == g_drv_tdm_pw_info[chipId][pwId].acInfo.tug3Id) 
                && (pPwCfgInfo->acInfo.tug2Id == g_drv_tdm_pw_info[chipId][pwId].acInfo.tug2Id) 
                && (pPwCfgInfo->acInfo.tu12Id == g_drv_tdm_pw_info[chipId][pwId].acInfo.tu12Id) 
                && (0 != (pPwCfgInfo->acInfo.timeslotBmp & g_drv_tdm_pw_info[chipId][pwId].acInfo.timeslotBmp)))
            {
                return DRV_TDM_OVERLAP_PW_CFG;
            }
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwVlanIdCheck
* 功能描述: 检查新配置的PW是否与已经添加成功的PW的VLANID相等
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 无
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 只需要检查VLAN2的vlan ID.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwVlanIdCheck(const DRV_TDM_PW_CFG_INFO* pPwCfgInfo)
{
    BYTE chipId = 0;
    WORD16 pwId = 0;
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCfgInfo);
    chipId = pPwCfgInfo->acInfo.chipId;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    for (pwId = DRV_TDM_PW_ID_MIN; pwId <= DRV_TDM_PW_ID_MAX; pwId++)
    {
        if (DRV_TDM_PW_FREE != g_drv_tdm_pw_info[chipId][pwId].pwState) /* pw is used */
        {
            /* 只需要检查VLAN2的vlan ID. */
            if (pPwCfgInfo->vlan2Tag.vlan2Id == g_drv_tdm_pw_info[chipId][pwId].vlan2Tag.vlan2Id)
            {
                return DRV_TDM_OVERLAP_VLANID_CFG;
            }
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1PwTypeCheck
* 功能描述: 检查PW类型与E1帧类型是否匹配
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pPwCfgInfo
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1PwTypeCheck(const DRV_TDM_PW_CFG_INFO *pPwCfgInfo)
{
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCfgInfo);

    if (DRV_TDM_SATOP_CHANNEL == pPwCfgInfo->channelMode)    /* SAToP PW */
    {
        if (DRV_TDM_UNFRAME_E1 != pPwCfgInfo->acInfo.e1FramingMode)
        {
            return DRV_TDM_E1_PW_TYPE_UNMATCH;
        }
    }
    else if (DRV_TDM_CESOP_BASIC_CHANNEL == pPwCfgInfo->channelMode)   /* CESoPSN PW without CAS */
    {
        if ((DRV_TDM_PCM30_E1 != pPwCfgInfo->acInfo.e1FramingMode) 
            && (DRV_TDM_PCM30CRC_E1 != pPwCfgInfo->acInfo.e1FramingMode) 
            && (DRV_TDM_PCM31_E1 != pPwCfgInfo->acInfo.e1FramingMode) 
            && (DRV_TDM_PCM31CRC_E1 != pPwCfgInfo->acInfo.e1FramingMode))
        {
            return DRV_TDM_E1_PW_TYPE_UNMATCH;
        }
    }
    else if (DRV_TDM_CESOP_CAS_CHANNEL == pPwCfgInfo->channelMode) /* CESoPSN PW with CAS */
    {
        if ((DRV_TDM_PCM30_E1 != pPwCfgInfo->acInfo.e1FramingMode)
            && (DRV_TDM_PCM30CRC_E1 != pPwCfgInfo->acInfo.e1FramingMode))
        {
            return DRV_TDM_E1_PW_TYPE_UNMATCH;
        }
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u channelNo 0x%x, invalid pw type, pwType=%u.\n", __FILE__, __LINE__, pPwCfgInfo->acInfo.subslotId, pPwCfgInfo->acInfo.channelNo, pPwCfgInfo->channelMode);
        return DRV_TDM_INVALID_PW_TYPE;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1TimeslotCheck
* 功能描述: 检查E1帧的时隙.
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pPwCfgInfo
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 检查E1帧的时隙与E1帧类型是否匹配
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1TimeslotCheck(const DRV_TDM_PW_CFG_INFO *pPwCfgInfo)
{
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCfgInfo);
    
    if (DRV_TDM_UNFRAME_E1 == pPwCfgInfo->acInfo.e1FramingMode)    
    {
        if (DRV_TDM_UNFRAME_E1_BMP != pPwCfgInfo->acInfo.timeslotBmp)
        {
            return DRV_TDM_E1_TIMESLOT_INVALID;
        }
    }
    else if ((DRV_TDM_PCM30_E1 == pPwCfgInfo->acInfo.e1FramingMode) 
            || (DRV_TDM_PCM30CRC_E1 == pPwCfgInfo->acInfo.e1FramingMode))
    {
        /*变更单号 613002721680  chenbo*/
        if (0 != (pPwCfgInfo->acInfo.timeslotBmp & DRV_TDM_E1_TIMESLOT_0))
        {
            return DRV_TDM_E1_TIMESLOT_INVALID;
        }
    }
    else if ((DRV_TDM_PCM31_E1 == pPwCfgInfo->acInfo.e1FramingMode) 
            || (DRV_TDM_PCM31CRC_E1 == pPwCfgInfo->acInfo.e1FramingMode))
    {
        if (0 != (pPwCfgInfo->acInfo.timeslotBmp & DRV_TDM_E1_TIMESLOT_0))
        {
            return DRV_TDM_E1_TIMESLOT_INVALID;
        }
    }
    else
    {
        return DRV_TDM_INVALID_ARGUMENT;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_rtpStateCheck
* 功能描述: 检查RTP的状态.
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pPwCfgInfo
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 检测RTP的状态,对于DCR时钟模式的业务,必须要使能RTP.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-05-29   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_rtpStateCheck(const DRV_TDM_PW_CFG_INFO *pPwCfgInfo)
{
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCfgInfo);
    
    if (DRV_TDM_TIMING_MODE_DCR == pPwCfgInfo->acInfo.e1TimingMode)  /* DCR clock */
    {
        if (DRV_TDM_RTP_DISABLE == pPwCfgInfo->rtpEnable)
        {
            return DRV_TDM_INVALID_RTP_STATE;
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwBindCheck
* 功能描述: 检查新配置的PW是否已经绑定了
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 无
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwBindCheck(const DRV_TDM_PW_CFG_INFO* pPwCfgInfo)
{
    BYTE chipId = 0;
    WORD16 pwId = 0;
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCfgInfo);
    chipId = pPwCfgInfo->acInfo.chipId;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
        
    for (pwId = DRV_TDM_PW_ID_MIN; pwId <= DRV_TDM_PW_ID_MAX; pwId++)
    {
        /* 新配置的PW已经绑定了 */
        if ((DRV_TDM_PW_FREE != g_drv_tdm_pw_info[chipId][pwId].pwState) 
            && (pPwCfgInfo->acInfo.channelNo == g_drv_tdm_pw_info[chipId][pwId].acInfo.channelNo))
        {
            return DRV_TDM_PW_EXISTED;
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_freePwIdGet
* 功能描述: 获取某块STM-1单板可以使用的空闲PWID
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 无
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_freePwIdGet(BYTE chipId, WORD16 *pwId)
{
    WORD16 i = 0;    /* 循环变量 */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    *pwId = DRV_TDM_PW_ID_INVALID;    /* 初始化情况下,赋值PWID为一个无效值 */
    
    for (i = DRV_TDM_PW_ID_MIN; i <= DRV_TDM_PW_ID_MAX; i++)
    {
        if (DRV_TDM_PW_FREE == g_drv_tdm_pw_info[chipId][i].pwState)
        {
            *pwId = i;
            break;
        }
    }
    if (i > DRV_TDM_PW_ID_MAX)
    {
        return DRV_TDM_INVALID_PW_ID;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwIdGet
* 功能描述: 通过channelNo来获取驱动的PWID.
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 无
* 输入参数: chipId: 芯片编号,从0开始取值.
*           channelNo: channel NO.
* 输出参数: *pPwId: 保存PWID. 
* 返 回 值: 
* 其它说明: channelNo与驱动内部的PWID映射关系: channelNo和PWID是一一对应的.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwIdGet(BYTE chipId, WORD32 channelNo, WORD16 *pPwId)
{
    WORD16 i = 0;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    *pPwId = DRV_TDM_PW_ID_INVALID;  /* 将初值赋值为无效值 */
    
    for (i = DRV_TDM_PW_ID_MIN; i <= DRV_TDM_PW_ID_MAX; i++)
    {
        if ((DRV_TDM_PW_FREE != g_drv_tdm_pw_info[chipId][i].pwState) 
            && (channelNo == g_drv_tdm_pw_info[chipId][i].acInfo.channelNo))
        {
            *pPwId = i;
            return DRV_TDM_OK;    /* 通过channelNo找到了pwId后直接返回成功 */
        }
    }
    
    return DRV_TDM_PW_IS_NOT_EXISTED;  /* 通过channelNo没有找到pwId,则直接返回PW不存在 */
}


/**************************************************************************
* 函数名称: drv_tdm_e1BindingPwIdGet
* 功能描述: 获取E1绑定的PWID.
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 无
* 输入参数: 
* 输出参数: *pPwId: 保存查找到的PWID. 
* 返 回 值: 
* 其它说明: 对于SATOP PW,e1LinkId与pwId是一一对应的;对于CESOPSN PW,获取e1LinkId绑定的第一条PW.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-18   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1BindingPwIdGet(BYTE subslotId, 
                                           BYTE portId, 
                                           BYTE au4Id, 
                                           BYTE e1LinkId, 
                                           WORD16 *pPwId)
{
    WORD32 rv = DRV_TDM_OK;
    WORD16 i = 0;
    BYTE chipId = 0;
    BYTE tug3Id = 0;
    BYTE tug2Id = 0;
    BYTE tu12Id = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId); 
    chipId = subslotId - (BYTE)1;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(au4Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CFG_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_tug3Tug2Tu12IdGet(e1LinkId, &tug3Id, &tug2Id, &tu12Id);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CFG_MSG, "ERROR: %s line %d, drv_tdm_tug3Tug2Tu12IdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    *pPwId = DRV_TDM_PW_ID_INVALID;
    for (i = DRV_TDM_PW_ID_MIN; i <= DRV_TDM_PW_ID_MAX; i++)
    {
        if ((DRV_TDM_PW_FREE != g_drv_tdm_pw_info[chipId][i].pwState)
            && (subslotId == g_drv_tdm_pw_info[chipId][i].acInfo.subslotId)
            && (portId == g_drv_tdm_pw_info[chipId][i].acInfo.portId)
            && (au4Id == g_drv_tdm_pw_info[chipId][i].acInfo.au4Id)
            && (tug3Id == g_drv_tdm_pw_info[chipId][i].acInfo.tug3Id)
            && (tug2Id == g_drv_tdm_pw_info[chipId][i].acInfo.tug2Id)
            && (tu12Id == g_drv_tdm_pw_info[chipId][i].acInfo.tu12Id))
        {
            *pPwId = i;
            break;
        }
    }
    if (i > DRV_TDM_PW_ID_MAX)
    {
        return DRV_TDM_E1_IS_NOT_BINDED;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_acBindingPwIdGet
* 功能描述: 获取接入电路AC绑定的PWID.
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 无
* 输入参数: 
* 输出参数: *pPwId: 保存查找到的PWID. 
* 返 回 值: 
* 其它说明: 接入电路attachment circuit与PWID是一一对应的.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-18   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_acBindingPwIdGet(BYTE subslotId, 
                                          BYTE portId, 
                                          BYTE au4Id, 
                                          BYTE e1LinkId, 
                                          WORD32 timeslotBmp, 
                                          WORD16 *pPwId)
{
    WORD32 rv = DRV_TDM_OK;
    WORD16 i = 0;
    BYTE chipId = 0;
    BYTE tug3Id = 0;
    BYTE tug2Id = 0;
    BYTE tu12Id = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId); 
    chipId = subslotId - (BYTE)1;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(au4Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CFG_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    rv = drv_tdm_tug3Tug2Tu12IdGet(e1LinkId, &tug3Id, &tug2Id, &tu12Id);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_tug3Tug2Tu12IdGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    *pPwId = DRV_TDM_PW_ID_INVALID;  /* 初始化时赋值为无效值 */
    for (i = DRV_TDM_PW_ID_MIN; i <= DRV_TDM_PW_ID_MAX; i++)
    {
        if ((DRV_TDM_PW_FREE != g_drv_tdm_pw_info[chipId][i].pwState)
            && (subslotId == g_drv_tdm_pw_info[chipId][i].acInfo.subslotId)
            && (portId == g_drv_tdm_pw_info[chipId][i].acInfo.portId)
            && (au4Id == g_drv_tdm_pw_info[chipId][i].acInfo.au4Id)
            && (tug3Id == g_drv_tdm_pw_info[chipId][i].acInfo.tug3Id)
            && (tug2Id == g_drv_tdm_pw_info[chipId][i].acInfo.tug2Id)
            && (tu12Id == g_drv_tdm_pw_info[chipId][i].acInfo.tu12Id)
            && (timeslotBmp == g_drv_tdm_pw_info[chipId][i].acInfo.timeslotBmp))
        {
            *pPwId = i;
            break;
        }
    }
    if (i > DRV_TDM_PW_ID_MAX)
    {
        return DRV_TDM_AC_IS_NOT_BINDED;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atDevPwPtrSave
* 功能描述: 保存ARRVIE芯片的PW指针.
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 软件表g_drv_tdm_pw_info
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atDevPwPtrSave(BYTE chipId, WORD16 pwId, AtPw pPw)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    g_drv_tdm_pw_info[chipId][pwId].pAtPw = pPw; /* 保存ARRIVE芯片的PW指针 */
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwAttributeSet
* 功能描述: 设置PW的属性状态
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 软件表g_drv_tdm_pw_info
* 输入参数: chipId: 芯片编号,取值为0~3.
*           pwId: pw ID,取值为0~1023.
*           attribute: 属性值,参见DRV_TDM_SLAVE_PW/DRV_TDM_MASTER_PW定义.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 只对CESoPSN PW才有意义.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwAttributeSet(BYTE chipId, WORD16 pwId, BYTE attribute)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    g_drv_tdm_pw_info[chipId][pwId].pwAttribute = attribute;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwAttributeGet
* 功能描述: 获取PW的属性状态
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 无.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           pwId: pw ID,取值为0~1023.
* 输出参数: *pAttribute: 保存PW的属性值.
* 返 回 值: 
* 其它说明: 只对CESoPSN PW才有意义.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwAttributeGet(BYTE chipId, WORD16 pwId, BYTE *pAttribute)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pAttribute);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    *pAttribute = 0;  /* 先清零再赋值 */
    *pAttribute = g_drv_tdm_pw_info[chipId][pwId].pwAttribute;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwStateSet
* 功能描述: 设置PW的状态
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 软件表g_drv_tdm_pw_info
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwStateSet(BYTE chipId, WORD16 pwId, BYTE state)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    g_drv_tdm_pw_info[chipId][pwId].pwState = state;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwStateGet
* 功能描述: 获取PW的状态
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwStateGet(BYTE chipId, WORD16 pwId, BYTE *state)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(state);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    *state = g_drv_tdm_pw_info[chipId][pwId].pwState;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwAcCfgInfoGet
* 功能描述: 获取PW的接入链路attachment circuit配置信息
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwAcCfgInfoGet(BYTE chipId, WORD16 pwId, DRV_TDM_PW_AC_INFO *pAcCfgInfo)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pAcCfgInfo);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    memcpy(pAcCfgInfo, &(g_drv_tdm_pw_info[chipId][pwId].acInfo), sizeof(DRV_TDM_PW_AC_INFO));
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwE1ClkStateGet
* 功能描述: 获取PW软件表中的E1链路的时钟状态.
* 访问的表: 软件表g_drv_tdm_pw_info.
* 修改的表: 
* 输入参数: chipId: 芯片编号,取值为0~3.
*           pwId: PWID,取值为0~1023.
* 输出参数: *pClkState: 保存E1链路的时钟状态.  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwE1ClkStateGet(BYTE chipId, WORD16 pwId, BYTE *pClkState)
{  
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pClkState);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    *pClkState = 0;  /* 先清零再赋值 */
    *pClkState = g_drv_tdm_pw_info[chipId][pwId].acInfo.e1LinkClkState;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwE1ClkStateSet
* 功能描述: 设置PW软件表中的E1链路的时钟状态.
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 软件表g_drv_tdm_pw_info
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: 端口编号,取值为1~8.
*           au4Id: AU4编号,从1开始取值.
*           e1LinkId: E1链路编号,取值为1~63.
*           e1ClkState: E1链路的时钟状态,参加定义.
* 输出参数: 无.  
* 返 回 值: 
* 其它说明: 在进行时钟切换时,需要更新PW软件表中的E1链路的时钟状态.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwE1ClkStateSet(BYTE chipId, 
                                         BYTE portId, 
                                         BYTE au4Id, 
                                         BYTE e1LinkId, 
                                         BYTE e1ClkState)
{
    WORD32 rv = DRV_TDM_OK;
    WORD16 pwId = DRV_TDM_PW_ID_INVALID;  /* 初始化时,赋值为无效值 */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(au4Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CFG_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    for (pwId = DRV_TDM_PW_ID_MIN; pwId <= DRV_TDM_PW_ID_MAX; pwId++)
    {
        if (DRV_TDM_PW_FREE != g_drv_tdm_pw_info[chipId][pwId].pwState)
        {
            if ((portId == g_drv_tdm_pw_info[chipId][pwId].acInfo.portId) 
                && (au4Id == g_drv_tdm_pw_info[chipId][pwId].acInfo.au4Id) 
                && (e1LinkId == g_drv_tdm_pw_info[chipId][pwId].acInfo.e1LinkId))
            {
                g_drv_tdm_pw_info[chipId][pwId].acInfo.e1LinkClkState = e1ClkState;
            }
        }
    }

    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwE1TimingModeSet
* 功能描述: 设置PW软件表中的E1链路的时钟模式.
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 软件表g_drv_tdm_pw_info
* 输入参数: chipId: 芯片编号,取值为0~3.
*           portId: STM端口编号,取值为1~8.
*           au4Id: AU4编号,取值为1~4.
*           e1LinkId: VC4中的E1链路编号,取值为1~63.
*           e1ClockMode: E1链路的时钟模式,具体参见定义.
* 输出参数: 无.  
* 返 回 值: 
* 其它说明: 在设置E1链路的时钟模式时,需要更新PW软件表中的E1链路的时钟模式.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwE1TimingModeSet(BYTE chipId, 
                                             BYTE portId, 
                                             BYTE au4Id, 
                                             BYTE e1LinkId, 
                                             BYTE e1ClockMode)
{
    WORD32 rv = DRV_TDM_OK;
    WORD16 pwId = DRV_TDM_PW_ID_INVALID;  /* 初始化时,赋值为无效值 */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_AUG1_ID(au4Id);
    DRV_TDM_CHECK_E1_LINK_ID(e1LinkId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    rv = drv_tdm_stmPortIdCheck(chipId, portId);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CFG_MSG, "ERROR: %s line %d, drv_tdm_stmPortIdCheck() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    for (pwId = DRV_TDM_PW_ID_MIN; pwId <= DRV_TDM_PW_ID_MAX; pwId++)
    {
        if (DRV_TDM_PW_FREE != g_drv_tdm_pw_info[chipId][pwId].pwState)
        {
            if ((portId == g_drv_tdm_pw_info[chipId][pwId].acInfo.portId) 
                && (au4Id == g_drv_tdm_pw_info[chipId][pwId].acInfo.au4Id) 
                && (e1LinkId == g_drv_tdm_pw_info[chipId][pwId].acInfo.e1LinkId))
            {
                g_drv_tdm_pw_info[chipId][pwId].acInfo.e1TimingMode = e1ClockMode;
            }
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwTypeGet
* 功能描述: 获取PW的类型
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwTypeGet(BYTE chipId, WORD16 pwId, BYTE *pPwType)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwType);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    *pPwType = g_drv_tdm_pw_info[chipId][pwId].channelMode;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwPldSizeSet
* 功能描述: 设置PW的payload size
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 软件表g_drv_tdm_pw_info
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwPldSizeSet(BYTE chipId, WORD16 pwId, WORD16 payloadSize)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    g_drv_tdm_pw_info[chipId][pwId].pldSize = payloadSize;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwEncapNumSet
* 功能描述: 设置PW的封包级联数
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 软件表g_drv_tdm_pw_info
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwEncapNumSet(BYTE chipId, WORD16 pwId, WORD16 encapNum)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    g_drv_tdm_pw_info[chipId][pwId].encapNum = encapNum;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwJitterBufPktNumSet
* 功能描述: 设置PW的Jitter Bffer packet number.
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 软件表g_drv_tdm_pw_info
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwJitterBufPktNumSet(BYTE chipId, WORD16 pwId, WORD32 pktCntsInBuf)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    g_drv_tdm_pw_info[chipId][pwId].pktNumInBuf = pktCntsInBuf;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwJitterBufSizeSet
* 功能描述: 设置PW的Jitter Bffer Size
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 软件表g_drv_tdm_pw_info
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwJitterBufSizeSet(BYTE chipId, WORD16 pwId, WORD32 jitBufSize)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    g_drv_tdm_pw_info[chipId][pwId].jitterBufSize = jitBufSize;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwJitterBufDelaySet
* 功能描述: 设置PW的Jitter Bffer Delay
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 软件表g_drv_tdm_pw_info
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwJitterBufDelaySet(BYTE chipId, WORD16 pwId, WORD32 jitBufDelay)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    g_drv_tdm_pw_info[chipId][pwId].jitterDelay = jitBufDelay;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwVlan1PriSet
* 功能描述: 设置PW软件表的vlan1 priority.
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 软件表g_drv_tdm_pw_info
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwVlan1PriSet(BYTE chipId, WORD16 pwId, BYTE vlan1Pri)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    g_drv_tdm_pw_info[chipId][pwId].vlan1Tag.priority = vlan1Pri;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwLbitEnableFlagSet
* 功能描述: 设置PW软件表的L-bit enable标记.
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 软件表g_drv_tdm_pw_info
* 输入参数: chipId: 芯片编号,取值为0~3.
*           pwId: PWID,取值为0~1023.
*           ucFlag: L-bit enable标记.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwLbitEnableFlagSet(BYTE chipId, WORD16 pwId, BYTE ucFlag)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    g_drv_tdm_pw_info[chipId][pwId].ucLbitEnable = ucFlag;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwLbitEnableFlagGet
* 功能描述: 获取PW软件表的L-bit enable标记.
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 无.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           pwId: PWID,取值为0~1023.
* 输出参数: *pucFlag: 保存L-bit enable标记. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwLbitEnableFlagGet(BYTE chipId, WORD16 pwId, BYTE *pucFlag)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pucFlag);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    *pucFlag = 0;  /* 先清零再赋值 */
    *pucFlag = g_drv_tdm_pw_info[chipId][pwId].ucLbitEnable;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwLbitForceFlagSet
* 功能描述: 设置PW软件表的L-bit force标记.
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 软件表g_drv_tdm_pw_info
* 输入参数: chipId: 芯片编号,取值为0~3.
*           pwId: PWID,取值为0~1023.
*           ucFlag: L-bit force标记.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwLbitForceFlagSet(BYTE chipId, WORD16 pwId, BYTE ucFlag)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    g_drv_tdm_pw_info[chipId][pwId].ucLbitForce = ucFlag;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwLbitForceFlagGet
* 功能描述: 获取PW软件表的L-bit force标记.
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 无.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           pwId: PWID,取值为0~1023.
* 输出参数: *pucFlag: 保存L-bit force标记. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwLbitForceFlagGet(BYTE chipId, WORD16 pwId, BYTE *pucFlag)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pucFlag);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    *pucFlag = 0;  /* 先清零再赋值 */
    *pucFlag = g_drv_tdm_pw_info[chipId][pwId].ucLbitForce;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwIdleCodeSet
* 功能描述: 设置PW软件表的PW Idle Code.
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 软件表g_drv_tdm_pw_info
* 输入参数: chipId: 芯片编号,取值为0~3.
*           pwId: PWID,取值为0~1023.
*           idleCode: PW idle code.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwIdleCodeSet(BYTE chipId, WORD16 pwId, BYTE idleCode)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    g_drv_tdm_pw_info[chipId][pwId].ucIdleCode = idleCode;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwIdleCodeGet
* 功能描述: 获取PW软件表的PW idle code.
* 访问的表: 软件表g_drv_tdm_pw_info
* 修改的表: 无.
* 输入参数: chipId: 芯片编号,取值为0~3.
*           pwId: PWID,取值为0~1023.
* 输出参数: *pucIdleCode: 保存PW idle code.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwIdleCodeGet(BYTE chipId, WORD16 pwId, BYTE *pucIdleCode)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pucIdleCode);
    DRV_TDM_CHECK_POINTER_IS_NULL(g_drv_tdm_pw_info[chipId]);
    
    *pucIdleCode = 0;  /* 先清零再赋值 */
    *pucIdleCode = g_drv_tdm_pw_info[chipId][pwId].ucIdleCode;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwEthIntfSet
* 功能描述: 设置PW的Ethernet接口配置信息
* 访问的表: 无
* 修改的表: 无
* 输入参数: pPwCfgInfo: PW配置信息.
* 输出参数: pPw: 芯片的PW
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwEthIntfSet(const DRV_TDM_PW_CFG_INFO *pPwCfgInfo, AtDevice pDevice, AtPw pPw)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE ethPortId = 0;  /* ARRIVE芯片的Ethernet接口的编号,从0开始编号 */
    AtModuleEth pEthModule = NULL;
    AtEthPort pEthPort = NULL;
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCfgInfo);
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPw);

    ethPortId = pPwCfgInfo->ethIntfIndex;
    /* Configure ethernet layer */
    pEthModule = (AtModuleEth)AtDeviceModuleGet(pDevice, cAtModuleEth); /* Get ethernet module */  
    DRV_TDM_CHECK_POINTER_IS_NULL(pEthModule);
    pEthPort = AtModuleEthPortGet(pEthModule, ethPortId); /* Get Ethernet port */
    DRV_TDM_CHECK_POINTER_IS_NULL(pEthPort);
    rv = AtPwEthPortSet(pPw, pEthPort);   /* Set ethernet port for PW */
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtPwEthPortSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }

    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwVlanTagSet
* 功能描述: 设置PW的VLAN tag信息
* 访问的表: 无
* 修改的表: 无
* 输入参数: pPwCfgInfo: PW配置信息.
* 输出参数: pPw: 芯片的PW
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwVlanTagSet(const DRV_TDM_PW_CFG_INFO *pPwCfgInfo, AtPw pPw)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE stmIntfIndex = 0;     /* STM接口的索引号,取值为0~7. */
    WORD32 dwStmIntfMode = 0;  /* STM端口模式 */
    tAtZtePtnTag1 atVlan1Tag;   /* VLAN 1 */
    tAtZtePtnTag2 atVlan2Tag;   /* VLAN 2 */
    BYTE destMac[DRV_TDM_MAC_ADDR_SIZE] = {0}; /* Destination MAC address */
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCfgInfo);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPw);

    rv = drv_tdm_stmIntfModeGet(pPwCfgInfo->acInfo.chipId, pPwCfgInfo->acInfo.portId, &dwStmIntfMode);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_stmIntfModeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_STM1_INTF == dwStmIntfMode)  /* STM-1 interface mode */
    {
        stmIntfIndex = pPwCfgInfo->acInfo.portId - (BYTE)1;
    }
    else if (DRV_TDM_STM4_INTF == dwStmIntfMode) /* STM-4 interface mode */
    {
        rv = drv_tdm_aug1IndexGet(pPwCfgInfo->acInfo.portId, pPwCfgInfo->acInfo.au4Id, &stmIntfIndex);
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_aug1IndexGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid stmIntfMode, stmIntfMode=%u.\n", __FILE__, __LINE__, dwStmIntfMode);
        return DRV_TDM_INVALID_STM_INTF_MODE;
    }
    DRV_TDM_CHECK_STM_INDEX(stmIntfIndex);
    
    memset(&atVlan1Tag, 0, sizeof(tAtZtePtnTag1));
    memset(&atVlan2Tag, 0, sizeof(tAtZtePtnTag2));
    memcpy(destMac, pPwCfgInfo->dstMacAddr, DRV_TDM_MAC_ADDR_SIZE);
    
    atVlan1Tag.priority = pPwCfgInfo->vlan1Tag.priority;
    atVlan1Tag.cfi = pPwCfgInfo->vlan1Tag.cfi;
    atVlan1Tag.cpuPktIndicator = (BYTE)((pPwCfgInfo->vlan1Tag.vlan1Id & (WORD16)0x0800) >> 11);
    atVlan1Tag.encapType = (BYTE)((pPwCfgInfo->vlan1Tag.vlan1Id & (WORD16)0x07c0) >> 6);
    atVlan1Tag.packetLength = (BYTE)(pPwCfgInfo->vlan1Tag.vlan1Id & (WORD16)0x003f);
    atVlan2Tag.priority = pPwCfgInfo->vlan2Tag.priority;
    atVlan2Tag.cfi = pPwCfgInfo->vlan2Tag.cfi;
    atVlan2Tag.stmPortId = (BYTE)((pPwCfgInfo->vlan2Tag.vlan2Id & (WORD16)0x0e00) >> 9);
    atVlan2Tag.isMlpppIma = (BYTE)((pPwCfgInfo->vlan2Tag.vlan2Id & (WORD16)0x0100) >> 8);
    atVlan2Tag.zteChannelId = pPwCfgInfo->vlan2Tag.vlan2Id & (WORD16)0x00ff;

    /* 检查VLAN TAG的值是否正确 */
    if (0 != atVlan1Tag.cpuPktIndicator)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid cpu pkt indicator in vlan1 tag, cpuPktIndicator=%u.\n", __FILE__, __LINE__, atVlan1Tag.cpuPktIndicator);
        return DRV_TDM_ERR_CPU_PKT_INDICATOR;
    }
    if (DRV_TDM_CES_ENCAP_TYPE != atVlan1Tag.encapType)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid encapsulation type in vlan1 tag, encapType=0x%x.\n", __FILE__, __LINE__, atVlan1Tag.encapType);
        return DRV_TDM_ERR_VLAN1_ENCAP_TYPE;
    }
    if (DRV_TDM_RTP_DISABLE == pPwCfgInfo->rtpEnable)  /* RTP disable */
    {    
        if ((pPwCfgInfo->pldSize) > 36)  /* 当报文长度大于60字节时(包括报文头及净荷,不包括RTP头) */
        {
            if (0 != atVlan1Tag.packetLength)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid pkt length in vlan1 tag, pktLength=%u.\n", __FILE__, __LINE__, atVlan1Tag.packetLength);
                return DRV_TDM_ERR_PKT_LENGTH;
            }
        }
        else  /* 当报文长度小于60字节时(包括报文头及净荷,不包括RTP头) */
        {
            if ((pPwCfgInfo->pldSize + 24) != atVlan1Tag.packetLength)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid pkt length in vlan1 tag, pktLength=%u.\n", __FILE__, __LINE__, atVlan1Tag.packetLength);
                return DRV_TDM_ERR_PKT_LENGTH;
            }
        }
    }
    else  /* RTP enable */
    {
        if ((pPwCfgInfo->pldSize) > 24)  /* 当报文长度大于60字节时(包括报文头及净荷,包括12字节的RTP头) */
        {
            if (0 != atVlan1Tag.packetLength)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid pkt length in vlan1 tag, pktLength=%u.\n", __FILE__, __LINE__, atVlan1Tag.packetLength);
                return DRV_TDM_ERR_PKT_LENGTH;
            }
        }
        else  /* 当报文长度小于60字节时(包括报文头及净荷,包括12字节的RTP头) */
        {
            if ((pPwCfgInfo->pldSize + 36) != atVlan1Tag.packetLength)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid pkt length in vlan1 tag, pktLength=%u.\n", __FILE__, __LINE__, atVlan1Tag.packetLength);
                return DRV_TDM_ERR_PKT_LENGTH;
            }
        }
    }
    if (atVlan2Tag.stmPortId != stmIntfIndex)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid port id in vlan2 tag, portId=%u.\n", __FILE__, __LINE__, atVlan2Tag.stmPortId);
        return DRV_TDM_ERR_VLAN2_PORT_ID;
    }
    if (0 != atVlan2Tag.isMlpppIma)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid mlppp ima flag in vlan2 tag, mlpppImaFlag=%u.\n", __FILE__, __LINE__, atVlan2Tag.isMlpppIma);
        return DRV_TDM_ERR_MLPPP_IMA_FLAG;
    }

    rv = AtZtePwPtnHeaderSet(pPw, destMac, &atVlan1Tag, &atVlan2Tag);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtZtePwPtnHeaderSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    /* Set expected vlan1 tag. */
    rv = AtZtePtnPwExpectedTag1Set(pPw, &atVlan1Tag);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtZtePtnPwExpectedTag1Set() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    /* Set expected vlan2 tag. */
    rv = AtZtePtnPwExpectedTag2Set(pPw, &atVlan2Tag);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtZtePtnPwExpectedTag2Set() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwCwSet
* 功能描述: 设置PW的Control Word信息
* 访问的表: 无
* 修改的表: 无
* 输入参数: pPwCfgInfo: PW配置信息.
* 输出参数: pPw: 芯片的PW
* 返 回 值: 
* 其它说明: 芯片厂商的SDK已经自动使能了L/R/M比特,所以不需要再设置它们了.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwCwSet(const DRV_TDM_PW_CFG_INFO *pPwCfgInfo, AtPw pPw)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    eBool cwState = cAtTrue;  /* control word state. 默认使能 */
    eAtPwCwSequenceMode sequenceMode = cAtPwCwSequenceModeWrapZero; /* Sequence mode */

    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCfgInfo);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPw);
    
    /* Configure control word. */
    cwState = cAtTrue;  /* control word state. 默认使能 */
    if (DRV_TDM_PW_CW_DISABLE == pPwCfgInfo->cwEnable)  /* disable control word */
    {
        cwState = cAtFalse;
    }
    rv = AtPwCwEnable(pPw, cwState);   /* Enable/disable control word. */
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtPwCwEnable() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    /* Set control word sequence mode for PW. */
    sequenceMode = cAtPwCwSequenceModeWrapZero; /* 默认情况下,控制字的序列号使能 */
    if (DRV_TDM_PW_CW_SEQ_DISABLE == pPwCfgInfo->seqEnable) /* disable sequence */
    {
        sequenceMode = cAtPwCwSequenceModeDisable;
    }
    rv = AtPwCwSequenceModeSet(pPw, sequenceMode); 
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtPwCwSequenceModeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }

    /* Set control word length mode */
    rv = AtPwCwLengthModeSet(pPw, cAtPwCwLengthModePayload); 
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtPwCwLengthModeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwRtpSet
* 功能描述: 设置PW的RTP信息
* 访问的表: 无
* 修改的表: 无
* 输入参数: pPwCfgInfo: PW配置信息.
* 输出参数: pPw: 芯片的PW
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwRtpSet(const DRV_TDM_PW_CFG_INFO *pPwCfgInfo, AtPw pPw)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    eBool rtpState = cAtFalse;  /* 默认不支持RTP */

    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCfgInfo);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPw);
    
    /* Configure RTP information. */
    rtpState = cAtFalse;  /* 默认不支持RTP */
    if (DRV_TDM_RTP_ENABLE == pPwCfgInfo->rtpEnable) /* Enable RTP */
    {
        rtpState = cAtTrue;   
    }
    rv = AtPwRtpEnable(pPw, rtpState); /* Enable/disable RTP */
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtPwRtpEnable() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    if (DRV_TDM_RTP_ENABLE == pPwCfgInfo->rtpEnable) /* 如果支持RTP的话,还需要配置RTP信息.*/
    {
        rv = AtPwRtpPayloadTypeSet(pPw, DRV_TDM_RTP_PAYLOAD_VALUE_CES);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtPwRtpPayloadTypeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
        rv = AtPwRtpSsrcSet(pPw, 0);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtPwRtpSsrcSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
        rv = AtPwRtpTxSsrcSet(pPw, 0);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtPwRtpTxSsrcSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
        rv = AtPwRtpExpectedSsrcSet(pPw, 0);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtPwRtpExpectedSsrcSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return DRV_TDM_SDK_API_FAIL;
        }

        /* FPGA芯片只支持Differential mode. ACR: FPGA use sequence number in RTP header 
        or control word to recover service clock, and don't care what mode is using for 
        timestamp generation, so absolute mode is really not used in this case. DCR: just
        because in this mode, common high quality timing source is required so FPGA always
        use "Differential mode" to help the DCR engine work well, changing to "Absolute 
        mode" is not supported. So, configuring timestamp mode mode is not necessary to 
        be supported for each timing mode. With the current way that we handle timestamp, 
        the FPGA is interconnected properly with the outside so far. */
        #if 0
        rv = AtPwRtpTimeStampModeSet(pPw, cAtPwRtpTimeStampModeDifferential);  /* Set RTP time stamp mode */
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtPwRtpTimeStampModeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
        #endif
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwGeneralCfgSet
* 功能描述: 设置PW的一般配置信息
* 访问的表: 无
* 修改的表: 无
* 输入参数: pPwCfgInfo: PW配置信息.
* 输出参数: pPw: 芯片的PW
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwGeneralCfgSet(const DRV_TDM_PW_CFG_INFO *pPwCfgInfo, AtPw pPw)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCfgInfo);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPw);

    if (DRV_TDM_SATOP_CHANNEL == pPwCfgInfo->channelMode) /* SAToP PW */
    {
        rv = AtPwJitterBufferAndPayloadSizeSet(pPw, pPwCfgInfo->jitterBufSize, pPwCfgInfo->jitterDelay, pPwCfgInfo->pldSize);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, jitterBufferSize=0x%x jitterBufferDelay=0x%x payloadSize=0x%x, AtPwJitterBufferAndPayloadSizeSet() rv=0x%x.\n", __FILE__, __LINE__, pPwCfgInfo->jitterBufSize, pPwCfgInfo->jitterDelay, pPwCfgInfo->pldSize, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    else if ((DRV_TDM_CESOP_BASIC_CHANNEL == pPwCfgInfo->channelMode) || (DRV_TDM_CESOP_CAS_CHANNEL == pPwCfgInfo->channelMode))  
    {
        rv = AtPwJitterBufferAndPayloadSizeSet(pPw, pPwCfgInfo->jitterBufSize, pPwCfgInfo->jitterDelay, pPwCfgInfo->encapNum);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, jitterBufferSize=0x%x jitterBufferDelay=0x%x payloadSize=0x%x, AtPwJitterBufferAndPayloadSizeSet() rv=0x%x.\n", __FILE__, __LINE__, pPwCfgInfo->jitterBufSize, pPwCfgInfo->jitterDelay, pPwCfgInfo->encapNum, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid parameter, channelType = 0x%x.\n", __FILE__, __LINE__, pPwCfgInfo->channelMode);
        return DRV_TDM_INVALID_ARGUMENT;
    }

    /* Enable reorder. */
    rv = AtPwReorderingEnable(pPw, cAtTrue); 
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtPwReorderingEnable() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }

    /* Disable suppression */
    rv = AtPwSuppressEnable(pPw, cAtFalse); 
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtPwSuppressEnable() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    if (DRV_TDM_PW_IDLE_CODE_DISABLE == pPwCfgInfo->ucIdleCodeFlag) /* idle code disable */
    {
        /* Set packet replacement mode when PSN side entering lost of packet condition. */
        rv = AtPwCwPktReplaceModeSet(pPw, cAtPwPktReplaceModeAis);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtPwCwPktReplaceModeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    else  /* idle code enable */
    {
        /* Set packet replacement mode when PSN side entering lost of packet condition. */
        rv = AtPwCwPktReplaceModeSet(pPw, cAtPwPktReplaceModeIdleCode);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtPwCwPktReplaceModeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
        rv = AtPwIdleCodeSet(pPw, pPwCfgInfo->ucIdleCode);  /* Set PW IDLE code */
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtPwIdleCodeSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    
    /* Set number of error packets to declare lost of packet synchronization. */
    rv = AtPwLopsSetThresholdSet(pPw, AT_DEV_LOFS_SET_THRESHOLD);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtPwLopsSetThresholdSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    /* Set number of good packets to clear lost of packet synchronization condition. */
    rv = AtPwLopsClearThresholdSet(pPw, AT_DEV_LOFS_CLEAR_THRESHOLD);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtPwLopsClearThresholdSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwAcSet
* 功能描述: 设置PW的Attachment Circuit信息
* 访问的表: 无
* 修改的表: 无
* 输入参数: pPwCfgInfo: PW配置信息.
* 输出参数: pPw: 芯片的PW
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwAcSet(const DRV_TDM_PW_CFG_INFO *pPwCfgInfo, AtPw pPw)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtPdhDe1 pE1 = NULL;
    AtPdhNxDS0 pNxDs0 = NULL;
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCfgInfo);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPw);

    rv = drv_tdm_atPdhE1PtrGet(pPwCfgInfo->acInfo.chipId, 
                               pPwCfgInfo->acInfo.portId,
                               pPwCfgInfo->acInfo.au4Id, 
                               pPwCfgInfo->acInfo.e1LinkId, 
                               &pE1);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atPdhE1PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pE1);

    /* CESoP PW */
    if ((DRV_TDM_CESOP_CAS_CHANNEL == pPwCfgInfo->channelMode) 
        || (DRV_TDM_CESOP_BASIC_CHANNEL == pPwCfgInfo->channelMode)) 
    {
        /* Create NxDS0 with bitmap. */
        pNxDs0 = AtPdhDe1NxDs0Create(pE1, pPwCfgInfo->acInfo.timeslotBmp);
        DRV_TDM_CHECK_POINTER_IS_NULL(pNxDs0);
        
        /* Bind E1 attachment circuit to PW. */
        rv = AtPwCircuitBind(pPw, (AtChannel)pNxDs0);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtPwCircuitBind() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    else if (DRV_TDM_SATOP_CHANNEL == pPwCfgInfo->channelMode) /* SAToP PW */
    {
        /* Bind E1 attachment circuit to PW. */
        rv = AtPwCircuitBind(pPw, (AtChannel)pE1);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtPwCircuitBind() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid pwType=%u.\n", __FILE__, __LINE__, pPwCfgInfo->channelMode);
        return DRV_TDM_INVALID_ARGUMENT;
    }
 
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_e1NxDs0Delete
* 功能描述: 删除成帧E1通道的NxDS0链路
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_e1NxDs0Delete(const DRV_TDM_PW_CFG_INFO *pPwCfgInfo)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    AtPdhDe1 pE1 = NULL;
    AtPdhNxDS0 pNxDs0 = NULL;
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCfgInfo);

    rv = drv_tdm_atPdhE1PtrGet(pPwCfgInfo->acInfo.chipId, 
                               pPwCfgInfo->acInfo.portId, 
                               pPwCfgInfo->acInfo.au4Id, 
                               pPwCfgInfo->acInfo.e1LinkId, 
                               &pE1);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atPdhE1PtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pE1);
    
    /* Get NxDS0 instance by its bitmap */
    pNxDs0 = AtPdhDe1NxDs0Get(pE1, pPwCfgInfo->acInfo.timeslotBmp);
    DRV_TDM_CHECK_POINTER_IS_NULL(pNxDs0);

    /* Delete NxDS0 instance */
    rv = AtPdhDe1NxDs0Delete(pE1, pNxDs0);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtPdhDe1NxDs0Delete() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atAcPwDelete
* 功能描述: 删除ARRIVE芯片的AC PW.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atAcPwDelete(const DRV_TDM_PW_CFG_INFO *pPwCfgInfo, 
                                     AtPw pPw, 
                                     WORD16 pwId)
{
    WORD32 rv = DRV_TDM_OK;
    BYTE chipId = 0;
    AtDevice pDevice = NULL;   /* ARRIVE芯片的device指针 */
    AtModulePw pPwModule = NULL;

    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCfgInfo);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPw);
    DRV_TDM_CHECK_PW_ID(pwId);
    chipId = pPwCfgInfo->acInfo.chipId;
    DRV_TDM_CHECK_CHIP_ID(chipId);

    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);

    /* Get PW Module */
    pPwModule = (AtModulePw)AtDeviceModuleGet(pDevice, cAtModulePw); 
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwModule);
    
    /* Unbind attachment circuit that bound PW before */
    rv = AtPwCircuitUnbind(pPw);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtPwCircuitUnbind() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    if ((DRV_TDM_CESOP_BASIC_CHANNEL == pPwCfgInfo->channelMode)
        || (DRV_TDM_CESOP_CAS_CHANNEL == pPwCfgInfo->channelMode))   /* CESoPSN PW */
    {
        rv = drv_tdm_e1NxDs0Delete(pPwCfgInfo); /* 删除成帧E1通道的NxDS0链路 */
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_e1NxDs0Delete() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    
    /* Delete Pseudowire */
    rv = AtModulePwDeletePw(pPwModule, pwId);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtModulePwDeletePw() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwParameterCheck
* 功能描述: 检查PW参数
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pPwCfgInfo: PW配置信息.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwParameterCheck(const DRV_TDM_PW_CFG_INFO *pPwCfgInfo)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCfgInfo);
    
    /* 检查新配置的PW是否已经绑定了 */
    rv = drv_tdm_pwBindCheck(pPwCfgInfo);
    if (DRV_TDM_OK != rv)  
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u channelNo 0x%x, drv_tdm_pwBindCheck() rv=0x%x.\n", __FILE__, __LINE__, pPwCfgInfo->acInfo.subslotId, pPwCfgInfo->acInfo.channelNo, rv);
        return rv;
    }
    
    /* 检查新配置的PW是否与已经添加成功的PW的AC信息是否存在重叠 */
    rv = drv_tdm_pwAcInfoCheck(pPwCfgInfo);
    if (DRV_TDM_OK != rv) 
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u channelNo 0x%x, drv_tdm_pwAcInfoCheck() rv=0x%x.\n", __FILE__, __LINE__, pPwCfgInfo->acInfo.subslotId, pPwCfgInfo->acInfo.channelNo, rv);
        return rv;
    }
    
    /* 检查新配置的PW是否与已经添加成功的PW的VLANID相等 */
    rv = drv_tdm_pwVlanIdCheck(pPwCfgInfo);
    if (DRV_TDM_OK != rv) 
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u channelNo 0x%x, drv_tdm_pwVlanIdCheck() rv=0x%x.\n", __FILE__, __LINE__, pPwCfgInfo->acInfo.subslotId, pPwCfgInfo->acInfo.channelNo, rv);
        return rv;
    }

    /* 检查E1类型和PW类型是否匹配 */
    rv = drv_tdm_e1PwTypeCheck(pPwCfgInfo);
    if (DRV_TDM_OK != rv)  
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u channelNo 0x%x, drv_tdm_e1PwTypeCheck() rv=0x%x.\n", __FILE__, __LINE__, pPwCfgInfo->acInfo.subslotId, pPwCfgInfo->acInfo.channelNo, rv);
        return rv;
    }

    /* 检查E1帧的时隙与E1帧类型是否匹配 */
    rv = drv_tdm_e1TimeslotCheck(pPwCfgInfo);  
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u channelNo 0x%x, drv_tdm_e1TimeslotCheck() rv=0x%x.\n", __FILE__, __LINE__, pPwCfgInfo->acInfo.subslotId, pPwCfgInfo->acInfo.channelNo, rv);
        return rv;
    }

    /* 检查RTP的状态. */
    rv = drv_tdm_rtpStateCheck(pPwCfgInfo);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u channelNo 0x%x, drv_tdm_rtpStateCheck() rv=0x%x.\n", __FILE__, __LINE__, pPwCfgInfo->acInfo.subslotId, pPwCfgInfo->acInfo.channelNo, rv);
        return rv;
    }
    
    /* 检查PW的jitter buffer size的合法性 */
    rv = drv_tdm_pwJitterBufSizeCheck(pPwCfgInfo->channelMode, 
                                      pPwCfgInfo->acInfo.e1TsNum, 
                                      pPwCfgInfo->pldSize, 
                                      pPwCfgInfo->jitterBufSize);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u channelNo 0x%x jitterBufferSize 0x%x, drv_tdm_pwJitterBufSizeCheck() rv=0x%x.\n", __FILE__, __LINE__, pPwCfgInfo->acInfo.subslotId, pPwCfgInfo->acInfo.channelNo, pPwCfgInfo->jitterBufSize, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atPwAdd
* 功能描述: 在FPGA上添加PW
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pPwCfgInfo: PW配置信息.
* 输出参数:  
* 返 回 值: 
* 其它说明: STM单板只支持SAToP PW 和 CESoP PW.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atPwAdd(const DRV_TDM_PW_CFG_INFO *pPwCfgInfo)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 tmpRv = DRV_TDM_OK;
    BYTE chipId = DRV_TDM_MAX_BYTE;    /* AARIVE芯片的编号 */
    WORD16 pwId = DRV_TDM_PW_ID_INVALID;
    WORD32 initFlag = 0;
    WORD32 e1BindingPwNum = 0;  /* E1链路绑定的PW数量 */
    AtDevice pDevice = NULL;   /* ARRIVE芯片的device指针 */
    AtModulePw pPwModule = NULL;
    AtPw pPw = NULL;
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCfgInfo);
    chipId = pPwCfgInfo->acInfo.chipId;
    
    rv = drv_tdm_pwe3InitFlagGet(chipId, &initFlag);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3InitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    if (DRV_TDM_STM_BOARD_UNINSTALL == initFlag)    /* 必须保证STM单板初始化完成 */
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, STM board has not been initialized.\n", __FILE__, __LINE__, pPwCfgInfo->acInfo.subslotId);
        return DRV_TDM_STM_BOARD_UNINIT;
    }
    
    rv = drv_tdm_pwParameterCheck(pPwCfgInfo);  /* 参数检查 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwParameterCheck() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    
    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    
    /* 获取可用的PWID */
    rv = drv_tdm_freePwIdGet(chipId, &pwId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_freePwIdGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    DRV_TDM_CHECK_PW_ID(pwId);
    
    /* Get PW Module */
    pPwModule = (AtModulePw)AtDeviceModuleGet(pDevice, cAtModulePw); 
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwModule);
    
    if (DRV_TDM_SATOP_CHANNEL == pPwCfgInfo->channelMode) /* SAToP PW */
    {
        /* Create SAToP PW. */
        pPw = (AtPw)AtModulePwSAToPCreate(pPwModule, pwId); 
        DRV_TDM_CHECK_POINTER_IS_NULL(pPw);
    }
    else if (DRV_TDM_CESOP_BASIC_CHANNEL == pPwCfgInfo->channelMode) /* CESoP Basic PW */
    {
        /* Create CESoP Basic PW. */
        pPw = (AtPw)AtModulePwCESoPCreate(pPwModule, pwId, cAtPwCESoPModeBasic);
        DRV_TDM_CHECK_POINTER_IS_NULL(pPw);
    }
    else if (DRV_TDM_CESOP_CAS_CHANNEL == pPwCfgInfo->channelMode)
    {
        /* Create CESoP Basic PW. */
        pPw = (AtPw)AtModulePwCESoPCreate(pPwModule, pwId, cAtPwCESoPModeWithCas);
        DRV_TDM_CHECK_POINTER_IS_NULL(pPw);
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, invalid pwType=%u.\n", __FILE__, __LINE__, chipId, pPwCfgInfo->channelMode);
        return DRV_TDM_INVALID_PW_TYPE;
    }
    
    /* Set ethernet interface for PW. */
    rv = drv_tdm_pwEthIntfSet(pPwCfgInfo, pDevice, pPw);
    if (DRV_TDM_OK != rv)
    {
        /* 解除芯片的资源,保证可以再次使用 */
        tmpRv = AtModulePwDeletePw(pPwModule, pwId);
        if (cAtOk != tmpRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, AtModulePwDeletePw() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRv);
        }
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwEthIntfSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    
    /* Set attachment circuit for PW. */
    rv = drv_tdm_pwAcSet(pPwCfgInfo, pPw);
    if (DRV_TDM_OK != rv)
    {
        /* 解除芯片的资源,保证可以再次使用 */
        tmpRv = AtModulePwDeletePw(pPwModule, pwId);
        if (cAtOk != tmpRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, AtModulePwDeletePw() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRv);
        }
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwAcSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    
    /* Set VLAN tag for PW. */
    rv = drv_tdm_pwVlanTagSet(pPwCfgInfo, pPw);
    if (DRV_TDM_OK != rv)
    {
        /* 解除芯片的资源,保证可以再次使用 */
        tmpRv = drv_tdm_atAcPwDelete(pPwCfgInfo, pPw, pwId);
        if (DRV_TDM_OK != tmpRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atAcPwDelete() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRv);
        }
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwVlanTagSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    
    /* Set control word for PW. */
    rv = drv_tdm_pwCwSet(pPwCfgInfo, pPw);
    if (DRV_TDM_OK != rv)
    {
        /* 解除芯片的资源,保证可以再次使用 */
        tmpRv = drv_tdm_atAcPwDelete(pPwCfgInfo, pPw, pwId);
        if (DRV_TDM_OK != tmpRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atAcPwDelete() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRv);
        }
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwCwSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    
    /* Set RTP for PW. */
    rv = drv_tdm_pwRtpSet(pPwCfgInfo, pPw);
    if (DRV_TDM_OK != rv)
    {
        /* 解除芯片的资源,保证可以再次使用 */
        tmpRv = drv_tdm_atAcPwDelete(pPwCfgInfo, pPw, pwId);
        if (DRV_TDM_OK != tmpRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atAcPwDelete() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRv);
        }
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwRtpSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    
    /* Set general configuration for PW. */
    rv = drv_tdm_pwGeneralCfgSet(pPwCfgInfo, pPw);
    if (DRV_TDM_OK != rv)
    {
        /* 解除芯片的资源,保证可以再次使用 */
        tmpRv = drv_tdm_atAcPwDelete(pPwCfgInfo, pPw, pwId);
        if (DRV_TDM_OK != tmpRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atAcPwDelete() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRv);
        }
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwGeneralCfgSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    
    /* Activate this PW. */
    rv = AtChannelEnable((AtChannel)pPw, cAtTrue);
    if (cAtOk != rv)
    {
        /* 解除芯片的资源,保证可以再次使用 */
        tmpRv = drv_tdm_atAcPwDelete(pPwCfgInfo, pPw, pwId);
        if (DRV_TDM_OK != tmpRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atAcPwDelete() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRv);
        }
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, AtChannelEnable() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return DRV_TDM_SDK_API_FAIL;
    }

    /* 增加E1链路绑定的PW的数量 */
    rv = drv_tdm_e1BindingPwNumSet(chipId, 
                                   pPwCfgInfo->acInfo.portId, 
                                   pPwCfgInfo->acInfo.au4Id, 
                                   pPwCfgInfo->acInfo.e1LinkId, 
                                   DRV_TDM_PW_NUM_INCREMENT);
    if (DRV_TDM_OK != rv)
    {
        /* 解除芯片的资源,保证可以再次使用 */
        tmpRv = drv_tdm_atAcPwDelete(pPwCfgInfo, pPw, pwId);
        if (DRV_TDM_OK != tmpRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atAcPwDelete() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRv);
        }
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_e1BindingPwNumSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;  
    }
    
    /* 增加STM接口绑定的PW的数量 */
    rv = drv_tdm_stmIntfPwNumSet(chipId, pPwCfgInfo->acInfo.portId, DRV_TDM_PW_NUM_INCREMENT);
    if (DRV_TDM_OK != rv)
    {
        /* 解除芯片的资源,保证可以再次使用 */
        tmpRv = drv_tdm_atAcPwDelete(pPwCfgInfo, pPw, pwId);
        if (DRV_TDM_OK != tmpRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atAcPwDelete() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRv);
        }
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_stmIntfPwNumSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;  
    }
    
    /* 增加Ethernet接口绑定的PW的数量 */
    rv = drv_tdm_ethIntfPwNumSet(chipId, pPwCfgInfo->ethIntfIndex, DRV_TDM_PW_NUM_INCREMENT);
    if (DRV_TDM_OK != rv)
    {
        /* 解除芯片的资源,保证可以再次使用 */
        tmpRv = drv_tdm_atAcPwDelete(pPwCfgInfo, pPw, pwId);
        if (DRV_TDM_OK != tmpRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atAcPwDelete() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRv);
        }
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_ethIntfPwNumSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;  
    }
    
    /* 将PW配置信息保存到软件表g_drv_tdm_pw_info. */
    rv = drv_tdm_pwCfgInfoSave(chipId, pwId, pPwCfgInfo);
    if (DRV_TDM_OK != rv)
    {
        /* 解除芯片的资源,保证可以再次使用 */
        tmpRv = drv_tdm_atAcPwDelete(pPwCfgInfo, pPw, pwId);
        if (DRV_TDM_OK != tmpRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atAcPwDelete() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRv);
        }
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwCfgInfoSave() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;  
    }
    /* 保存ARRIVE芯片的PW指针 */
    rv = drv_tdm_atDevPwPtrSave(chipId, pwId, pPw);
    if (DRV_TDM_OK != rv)
    {
        /* 解除芯片的资源,保证可以再次使用 */
        tmpRv = drv_tdm_atAcPwDelete(pPwCfgInfo, pPw, pwId);
        if (DRV_TDM_OK != tmpRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atAcPwDelete() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRv);
        }
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atDevPwPtrSave() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;  
    }

    /* 获取E1链路绑定的PW的数量 */
    rv = drv_tdm_e1BindingPwNumGet(chipId, 
                                   pPwCfgInfo->acInfo.portId, 
                                   pPwCfgInfo->acInfo.au4Id, 
                                   pPwCfgInfo->acInfo.e1LinkId, 
                                   &e1BindingPwNum);
    if (DRV_TDM_OK != rv)
    {
        /* 解除芯片的资源,保证可以再次使用 */
        tmpRv = drv_tdm_atAcPwDelete(pPwCfgInfo, pPw, pwId);
        if (DRV_TDM_OK != tmpRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atAcPwDelete() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRv);
        }
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_e1BindingPwNumGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;  
    }
    
    if ((DRV_TDM_CESOP_BASIC_CHANNEL == pPwCfgInfo->channelMode) 
        || (DRV_TDM_CESOP_CAS_CHANNEL == pPwCfgInfo->channelMode))
    {
        if (1 == e1BindingPwNum) /* 成帧E1链路绑定的第一条PW,设置为MASTER */
        {
            rv = drv_tdm_pwAttributeSet(chipId, pwId, DRV_TDM_MASTER_PW);
        }
        else  /* 当成帧的E1链路绑定的PW数量大于1时,其余的PW设置为SLAVE */
        {
            rv = drv_tdm_pwAttributeSet(chipId, pwId, DRV_TDM_SLAVE_PW);
        }
        if (DRV_TDM_OK != rv)
        {
            /* 解除芯片的资源,保证可以再次使用 */
            tmpRv = drv_tdm_atAcPwDelete(pPwCfgInfo, pPw, pwId);
            if (DRV_TDM_OK != tmpRv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atAcPwDelete() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRv);
            }
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwAttributeSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
            return rv;  
        }
    }

    /* 设置PW的状态为used状态*/
    rv = drv_tdm_pwStateSet(chipId, pwId, DRV_TDM_PW_USED);
    if (DRV_TDM_OK != rv)
    {
        /* 解除芯片的资源,保证可以再次使用 */
        tmpRv = drv_tdm_atAcPwDelete(pPwCfgInfo, pPw, pwId);
        if (DRV_TDM_OK != tmpRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atAcPwDelete() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRv);
        }
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwStateSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;  
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwAdd
* 功能描述: 添加PW
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pPwCfgInfo: PW配置信息.
* 输出参数:  
* 返 回 值: 
* 其它说明: STM单板只支持SAToP PW 和 CESoP PW.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwAdd(const DRV_TDM_PW_CFG_INFO *pPwCfgInfo)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 tmpRv = DRV_TDM_OK;
    WORD16 pwId = 0;
    BYTE chipId = DRV_TDM_MAX_BYTE;    /* AARIVE芯片的编号 */
    WORD32 e1BindingPwNum = 0;  /* E1链路绑定的PW数量 */
    time_t currentTime;    /* 以秒为单位 */
    DRV_TDM_PW_CFG_INFO *ptempPw = NULL;  /* 用来指示驱动的PW软件表 */
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCfgInfo);
    chipId = pPwCfgInfo->acInfo.chipId;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    memset(&currentTime, 0, sizeof(time_t));
    time(&currentTime); /* Get current calendar time in seconds */

    rv = drv_tdm_atPwAdd(pPwCfgInfo);  /* 在FPGA上添加PW. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atPwAdd() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;  
    }
    rv = drv_tdm_acBindingPwIdGet(pPwCfgInfo->acInfo.subslotId, 
                                  pPwCfgInfo->acInfo.portId, 
                                  pPwCfgInfo->acInfo.au4Id, 
                                  pPwCfgInfo->acInfo.e1LinkId, 
                                  pPwCfgInfo->acInfo.timeslotBmp, 
                                  &pwId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_acBindingPwIdGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    DRV_TDM_CHECK_PW_ID(pwId);
    /* 获取E1链路绑定的PW的数量 */
    rv = drv_tdm_e1BindingPwNumGet(chipId, 
                                   pPwCfgInfo->acInfo.portId, 
                                   pPwCfgInfo->acInfo.au4Id, 
                                   pPwCfgInfo->acInfo.e1LinkId, 
                                   &e1BindingPwNum);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_e1BindingPwNumGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;  
    }
    
    if (1 == e1BindingPwNum)  /* E1链路已经绑定了一条PW. */
    {
        /* 对于成帧的E1,当第一条CESoPSN PW被绑定时,才设置E1链路的时钟方式 */
        rv = drv_tdm_e1TimingModeSet(pPwCfgInfo->acInfo.subslotId, 
                                     pPwCfgInfo->acInfo.portId, 
                                     pPwCfgInfo->acInfo.au4Id, 
                                     pPwCfgInfo->acInfo.e1LinkId, 
                                     pPwCfgInfo->acInfo.e1TimingMode);
        if (DRV_TDM_OK != rv)
        {
            tmpRv = drv_tdm_pwMemGet(chipId, pwId, &ptempPw); /* 获取驱动的PW软件表.在此之前已经保存了驱动的PW软件表. */
            if (DRV_TDM_OK != tmpRv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwMemGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRv);
                return tmpRv;
            }
            DRV_TDM_CHECK_POINTER_IS_NULL(ptempPw);
            /* 解除芯片的资源,保证可以再次使用 */
            tmpRv = drv_tdm_atAcPwDelete(pPwCfgInfo, ptempPw->pAtPw, pwId);/*单号：613002709075 chenbo*/
            if (DRV_TDM_OK != tmpRv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atAcPwDelete() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRv);
            }
            /* 减少E1链路绑定的PW的数量 */
            tmpRv = drv_tdm_e1BindingPwNumSet(chipId, 
                                              pPwCfgInfo->acInfo.portId, 
                                              pPwCfgInfo->acInfo.au4Id,
                                              pPwCfgInfo->acInfo.e1LinkId, 
                                              DRV_TDM_PW_NUM_DECREMENT);
            if (DRV_TDM_OK != tmpRv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_e1BindingPwNumSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRv);
            }
            /* 减少STM接口绑定的PW的数量 */
            tmpRv = drv_tdm_stmIntfPwNumSet(chipId, pPwCfgInfo->acInfo.portId, DRV_TDM_PW_NUM_DECREMENT);
            if (DRV_TDM_OK != tmpRv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_stmIntfPwNumSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRv);
            }
            /* 减少Ethernet接口绑定的PW的数量 */
            tmpRv = drv_tdm_ethIntfPwNumSet(chipId, pPwCfgInfo->ethIntfIndex, DRV_TDM_PW_NUM_DECREMENT);
            if (DRV_TDM_OK != tmpRv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_ethIntfPwNumSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRv);
            }
            tmpRv = drv_tdm_pwCfgInfoClear(chipId, pwId);
            if (DRV_TDM_OK != tmpRv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwCfgInfoClear() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRv);
            }
            tmpRv = drv_tdm_pwStateSet(chipId, pwId, DRV_TDM_PW_FREE);
            if (DRV_TDM_OK != tmpRv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwStateSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRv);
            }
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_e1TimingModeSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
            return rv;  
        }
        /* 设置E1链路的绑定状态.必须放在add pw流程的最后,因为在设置时钟模式时会用到该状态 */
        rv = drv_tdm_e1BindingStateSet(chipId, 
                                       pPwCfgInfo->acInfo.portId, 
                                       pPwCfgInfo->acInfo.au4Id, 
                                       pPwCfgInfo->acInfo.e1LinkId, 
                                       DRV_TDM_E1_LINK_BINDING);
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_e1BindingStateSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
            return rv;  
        }
    }
    
    BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, %s, subslotId %u portId %u aug1Id %u e1LinkId %u timeslotBitmap 0x%x, add PW successfully.\n", __FILE__, __LINE__, ctime(&currentTime), pPwCfgInfo->acInfo.subslotId, pPwCfgInfo->acInfo.portId, pPwCfgInfo->acInfo.au4Id, pPwCfgInfo->acInfo.e1LinkId, pPwCfgInfo->acInfo.timeslotBmp);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwCreate
* 功能描述: 创建PW
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pPwArg: PW信息.
* 输出参数:  
* 返 回 值: 
* 其它说明: 本函数提供给产品管理调用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-30   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwCreate(const T_BSP_SRV_FUNC_PW_BIND_ARG *pPwArg)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 tmpRetValue = DRV_TDM_OK;
    WORD32 systemType = 0;   /* ZXUPN15000的系统类型 */
    BYTE subslotId = 0;     /* STM接口卡的子槽位号 */
    BYTE chipId = 0;
    time_t currentTime;    /* 以秒为单位 */
    DRV_TDM_PW_CFG_INFO pwCfgInfo;
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwArg);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwArg->ptPwInfo);
    memset(&pwCfgInfo, 0, sizeof(DRV_TDM_PW_CFG_INFO));
    memset(&currentTime, 0, sizeof(time_t));
    time(&currentTime); /* Get current calendar time in seconds */

    systemType = BSP_cp3banSystemTypeGet();  /* 获取ZXUPN15000系统类型. */
    subslotId = (BYTE)(pPwArg->dwSubSlotId);
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM != systemType)   /* 15K-2系统 */
    {
        subslotId = 1;   /* 对于15K-2系统,子槽位始终取值为1 */
    }
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    DRV_TDM_CHECK_CHIP_ID(chipId);

    rv = drv_tdm_pwSemGet(chipId);  /* Get semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    /* 通过产品管理的PW信息来初始化驱动的PW配置信息 */
    rv = drv_tdm_pwCfgInfoInitialize(pPwArg, &pwCfgInfo);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_pwCfgInfoInitialize() rv=0x%x.\n", __FILE__, __LINE__, rv);
        tmpRetValue = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != tmpRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRetValue);
            return tmpRetValue;
        }
        return rv;
    }
    /* 在FPGA上创建PW */
    rv = drv_tdm_pwAdd(&pwCfgInfo);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u portId %u aug1Id %u e1LinkId %u timeslotBitmap 0x%x, encapNum %u pktNumInBuf %u, drv_tdm_pwAdd() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), pwCfgInfo.acInfo.subslotId, pwCfgInfo.acInfo.portId, pwCfgInfo.acInfo.au4Id, pwCfgInfo.acInfo.e1LinkId, pwCfgInfo.acInfo.timeslotBmp, pwCfgInfo.encapNum, pwCfgInfo.pktNumInBuf, rv);
        tmpRetValue = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != tmpRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRetValue);
            return tmpRetValue;
        }
        return rv;
    }
    rv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwDelete
* 功能描述: 删除PW
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           pwId: PWID,取值为0~1023.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwDelete(BYTE chipId, WORD16 pwId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 initFlag = 0;
    WORD32 e1BindingPwNum = 0;  /* E1链路绑定的PW数量 */
    AtDevice pDevice = NULL;   /* ARRIVE芯片的device指针 */
    AtModulePw pPwModule = NULL;
    AtPw pPw = NULL;
    DRV_TDM_PW_CFG_INFO tmpPwCfgInfo;   /* 临时变量,用来保存保存本条PW的配置信息 */
    DRV_TDM_PW_CFG_INFO *pDrvPwCfgInfo = NULL; /* 驱动自己的软件表 */
    time_t currentTime;    /* 以秒为单位 */
        
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    memset(&tmpPwCfgInfo, 0, sizeof(DRV_TDM_PW_CFG_INFO));
    memset(&currentTime, 0, sizeof(time_t));
    time(&currentTime); /* Get current calendar time in seconds */

    rv = drv_tdm_pwe3InitFlagGet(chipId, &initFlag);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3InitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    if (DRV_TDM_STM_BOARD_UNINSTALL == initFlag)    /* 必须保证STM单板初始化完成 */
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, STM board has not been initialized.\n", __FILE__, __LINE__, chipId);
        return DRV_TDM_STM_BOARD_UNINIT;
    }
    
    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);

    rv = drv_tdm_pwMemGet(chipId, pwId, &pDrvPwCfgInfo);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwMemGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDrvPwCfgInfo);
    memcpy(&tmpPwCfgInfo, pDrvPwCfgInfo, sizeof(DRV_TDM_PW_CFG_INFO)); 
    
    /* Get PW Module */
    pPwModule = (AtModulePw)AtDeviceModuleGet(pDevice, cAtModulePw); 
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwModule);
    
    /* Get pseudowire object by its identifier */
    pPw = AtModulePwGetPw(pPwModule, pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPw);
    
    rv = drv_tdm_atAcPwDelete(pDrvPwCfgInfo, pPw, pwId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_atAcPwDelete() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }

    /* 减少E1链路绑定的PW的数量 */
    rv = drv_tdm_e1BindingPwNumSet(chipId, 
                                   pDrvPwCfgInfo->acInfo.portId, 
                                   pDrvPwCfgInfo->acInfo.au4Id,
                                   pDrvPwCfgInfo->acInfo.e1LinkId, 
                                   DRV_TDM_PW_NUM_DECREMENT);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_e1BindingPwNumSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;  
    }
    /* 减少STM接口绑定的PW的数量 */
    rv = drv_tdm_stmIntfPwNumSet(chipId, pDrvPwCfgInfo->acInfo.portId, DRV_TDM_PW_NUM_DECREMENT);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_stmIntfPwNumSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;  
    }
    /* 减少Ethernet接口绑定的PW的数量 */
    rv = drv_tdm_ethIntfPwNumSet(chipId, pDrvPwCfgInfo->ethIntfIndex, DRV_TDM_PW_NUM_DECREMENT);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_ethIntfPwNumSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;  
    }
    /* 获取E1链路绑定的PW的数量 */
    rv = drv_tdm_e1BindingPwNumGet(chipId, 
                                   pDrvPwCfgInfo->acInfo.portId, 
                                   pDrvPwCfgInfo->acInfo.au4Id,
                                   pDrvPwCfgInfo->acInfo.e1LinkId, 
                                   &e1BindingPwNum);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_e1BindingPwNumGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;  
    }
    /* 必须在清除时钟域软件表之前清除PW软件表,因为前者会访问后者 */
    /* 清除PW配置信息软件表g_drv_tdm_pw_info */
    rv = drv_tdm_pwCfgInfoClear(chipId, pwId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwCfgInfoClear() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;  
    }
    /* 清除PW报文统计信息软件表g_drv_tdm_pw_pkt_cnt */
    rv = drv_tdm_pwPktCntClear(chipId, pwId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwPktCntClear() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;  
    }
    /* 清除PW报文统计信息软件表g_drv_tdm_pw_total_pkt_cnt */
    rv = drv_tdm_pwTotalPktCntClear(chipId, pwId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwTotalPktCntClear() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;  
    }
    /* 清除PW告警信息软件表g_drv_tdm_pw_alarm */
    rv = drv_tdm_pwAlarmClear(chipId, pwId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwAlarmClear() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;  
    }
    /* 设置PW的状态为free状态*/
    rv = drv_tdm_pwStateSet(chipId, pwId, DRV_TDM_PW_FREE);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwStateSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;  
    }
    
    /* 当成帧的E1链路还绑定有PW时,需要设置该条E1链路的时钟方式 */
    if (e1BindingPwNum > 0)   
    {
        /* 设置E1链路的时钟模式 */
        rv = drv_tdm_e1TimingModeSet(tmpPwCfgInfo.acInfo.subslotId, 
                                     tmpPwCfgInfo.acInfo.portId, 
                                     tmpPwCfgInfo.acInfo.au4Id, 
                                     tmpPwCfgInfo.acInfo.e1LinkId, 
                                     tmpPwCfgInfo.acInfo.e1TimingMode);
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_e1TimingModeSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
            return rv;  
        }
    }
    /* 当E1链路绑定的PW的数量为0时,需要清除时钟域有关的软件表 */
    if (0 == e1BindingPwNum)
    {
        rv = drv_tdm_e1BindingStateSet(chipId, 
                                       tmpPwCfgInfo.acInfo.portId, 
                                       tmpPwCfgInfo.acInfo.au4Id,
                                       tmpPwCfgInfo.acInfo.e1LinkId, 
                                       DRV_TDM_E1_LINK_UNBINDING);
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u e1LinkId %u, drv_tdm_e1BindingStateSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpPwCfgInfo.acInfo.portId, tmpPwCfgInfo.acInfo.au4Id, tmpPwCfgInfo.acInfo.e1LinkId, rv);
            return rv;  
        }
        
        if ((DRV_TDM_TIMING_MODE_ACR == tmpPwCfgInfo.acInfo.e1TimingMode) 
            || (DRV_TDM_TIMING_MODE_DCR == tmpPwCfgInfo.acInfo.e1TimingMode))
        {
            /* 清除E1链路的时钟信息 */
            rv = drv_tdm_e1LinkClockClear(chipId, 
                                          tmpPwCfgInfo.acInfo.portId, 
                                          tmpPwCfgInfo.acInfo.au4Id,
                                          tmpPwCfgInfo.acInfo.e1LinkId, 
                                          tmpPwCfgInfo.acInfo.clkDomainNo);
            if (DRV_TDM_OK != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u e1LinkId %u, drv_tdm_e1LinkClockClear() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpPwCfgInfo.acInfo.portId, tmpPwCfgInfo.acInfo.au4Id, tmpPwCfgInfo.acInfo.e1LinkId, rv);
                return rv;  
            }
            /* 清除时钟域信息. */
            rv = drv_tdm_clkDomainClear(chipId, tmpPwCfgInfo.acInfo.clkDomainNo);
            if (DRV_TDM_OK != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u portId %u aug1Id %u e1LinkId %u, drv_tdm_clkDomainClear() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpPwCfgInfo.acInfo.portId, tmpPwCfgInfo.acInfo.au4Id, tmpPwCfgInfo.acInfo.e1LinkId, rv);
                return rv;  
            }
            /* 对于ACR/DCR时钟模式,当E1链路不绑定PW时,需要将E1链路的时钟模式设置为system*/
            rv = drv_tdm_systemE1TimingSet(chipId, 
                                           tmpPwCfgInfo.acInfo.portId,
                                           tmpPwCfgInfo.acInfo.au4Id, 
                                           tmpPwCfgInfo.acInfo.e1LinkId);
            if (DRV_TDM_OK != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_systemE1TimingSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
                return rv;  
            }
            /* 保存E1帧的时钟模式 */
            rv = drv_tdm_e1TimingModeSave(chipId, 
                                          tmpPwCfgInfo.acInfo.portId,
                                          tmpPwCfgInfo.acInfo.au4Id, 
                                          tmpPwCfgInfo.acInfo.e1LinkId, 
                                          DRV_TDM_TIMING_MODE_SYS);
            if (DRV_TDM_OK != rv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_e1TimingModeSave() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
                return rv;
            }
        }
    }

    BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, %s, subslotId %u portId %u aug1Id %u e1LinkId %u timeslotBitmap 0x%x, delete PW successfully.\n", __FILE__, __LINE__, ctime(&currentTime), tmpPwCfgInfo.acInfo.subslotId, tmpPwCfgInfo.acInfo.portId, tmpPwCfgInfo.acInfo.au4Id, tmpPwCfgInfo.acInfo.e1LinkId, tmpPwCfgInfo.acInfo.timeslotBmp);

    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwRemove
* 功能描述: 删除PW
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pUnbindPwArg: 删除PW信息.
* 输出参数:  
* 返 回 值: 
* 其它说明: 本函数提供给产品管理调用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-30   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwRemove(const T_BSP_SRV_FUNC_PW_UNBIND_ARG *pUnbindPwArg)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 tmpRetValue = DRV_TDM_OK;
    WORD32 systemType = 0;   /* ZXUPN15000的系统类型 */
    BYTE subslotId = 0;     /* STM-1接口卡的子槽位号 */
    BYTE chipId = 0;
    WORD32 channelNo = 0;
    WORD16 pwId = 0;
    time_t currentTime;    /* 以秒为单位 */
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pUnbindPwArg);
    memset(&currentTime, 0, sizeof(time_t));
    time(&currentTime); /* Get current calendar time in seconds */
    
    systemType = BSP_cp3banSystemTypeGet();  /* 获取ZXUPN15000系统类型. */
    subslotId = (BYTE)(pUnbindPwArg->dwSubslotId);
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM != systemType)   /* 15K-2系统 */
    {
        subslotId = 1;   /* 对于15K-2系统,子槽位始终取值为1 */
    }
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    channelNo = pUnbindPwArg->dwPWid;

    rv = drv_tdm_pwSemGet(chipId);  /* Get semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_pwIdGet(chipId, channelNo, &pwId);  /* 查表,通过channelNo来查找PWID. */
    if (DRV_TDM_OK != rv) 
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, drv_tdm_pwIdGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, rv);
        tmpRetValue = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != tmpRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRetValue);
            return tmpRetValue;
        }
        return rv;
    }
    rv = drv_tdm_pwDelete(chipId, pwId);  /* 在FPGA上删除PW */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, drv_tdm_pwDelete() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, rv);
        tmpRetValue = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != tmpRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, tmpRetValue);
            return tmpRetValue;
        }
        return rv;
    }
    rv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CFG_MSG, "SUCCESS: %s line %d, subslotId %u channelNo 0x%x, delete PW successfully.\n", __FILE__, __LINE__, subslotId, channelNo);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwJitterBufSizeModify
* 功能描述: 修改PW的Jitter Buffer Size
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           pwId: PW ID,取值为0~1023.
*           jitterBufSize: jitter buffer size.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwJitterBufSizeModify(BYTE chipId, WORD16 pwId, WORD32 jitterBufSize)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE pwState = DRV_TDM_PW_FREE;  /* PW state */
    WORD32 jitterDelay = 0;
    WORD16 wPayloadSize = 0;   /* payload size */
    AtDevice pDevice = NULL;   /* ARRIVE芯片的device指针 */
    AtModulePw pPwModule = NULL;
    AtPw pPw = NULL;
    DRV_TDM_PW_CFG_INFO *pPwCfgInfo = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    jitterDelay = jitterBufSize / ((WORD32)2);
    
    rv = drv_tdm_pwStateGet(chipId, pwId, &pwState);  /* 获取PW的状态 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_pwStateGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_PW_FREE == pwState)  
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u pwId %u, pw is free.\n", __FILE__, __LINE__, chipId, pwId);
        return DRV_TDM_INVALID_PW_STATE; 
    }
    
    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    
    /* Get PW Module */
    pPwModule = (AtModulePw)AtDeviceModuleGet(pDevice, cAtModulePw); 
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwModule);
    
    /* Get pseudowire object by its identifier */
    pPw = AtModulePwGetPw(pPwModule, pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPw);

    rv = drv_tdm_pwMemGet(chipId, pwId, &pPwCfgInfo);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_pwMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCfgInfo);
    if (DRV_TDM_SATOP_CHANNEL == pPwCfgInfo->channelMode)
    {
        wPayloadSize = pPwCfgInfo->pldSize;
    }
    else if ((DRV_TDM_CESOP_BASIC_CHANNEL == pPwCfgInfo->channelMode) 
            || (DRV_TDM_CESOP_CAS_CHANNEL == pPwCfgInfo->channelMode))
    {
        wPayloadSize = pPwCfgInfo->encapNum;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid parameter, channelType = 0x%x.\n", __FILE__, __LINE__, pPwCfgInfo->channelMode);
        return DRV_TDM_INVALID_ARGUMENT;
    }

    /* Set jitter buffer size, delay in microseconds and payload size at a time */
    rv = AtPwJitterBufferAndPayloadSizeSet(pPw, jitterBufSize, jitterDelay, wPayloadSize);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u pwId %u, jitterBufferSize=0x%x jitterBufferDelay=0x%x payloadSize=0x%x, AtPwJitterBufferAndPayloadSizeSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pwId, jitterBufSize, jitterDelay, wPayloadSize, rv);
        return DRV_TDM_SDK_API_FAIL;
    }

    /* 更新驱动的软件表 */
    rv = drv_tdm_pwJitterBufSizeSet(chipId, pwId, jitterBufSize);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u pwId %u, drv_tdm_pwJitterBufSizeSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pwId, rv);
        return rv;  
    }
    rv = drv_tdm_pwJitterBufDelaySet(chipId, pwId, jitterDelay);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u pwId %u, drv_tdm_pwJitterBufDelaySet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pwId, rv);
        return rv;  
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwJitterBufSizeUpdate
* 功能描述: 更新PW的Jitter Buffer Size
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pPwArg: PW配置信息
* 输出参数:  
* 返 回 值: 
* 其它说明: 本函数提供给产品管理调用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwJitterBufSizeUpdate(const T_BSP_SRV_FUNC_PW_BIND_ARG *pPwArg)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 dwTempRv = DRV_TDM_OK;
    WORD32 initFlag = 0;     /* STM单板初始化标记 */
    WORD32 systemType = 0;   /* ZXUPN15000的系统类型 */
    WORD32 channelNo = 0;
    WORD32 pktCntsInBuf = 0;  /* buffer中的报文数量 */
    WORD32 jitterBufSize = 0;
    BYTE subslotId = 0;  /* 接口卡的子槽位号 */
    BYTE chipId = DRV_TDM_MAX_BYTE;    /* ARRIVE芯片的编号 */
    WORD16 pwId = DRV_TDM_PW_ID_INVALID;
    BYTE e1TsNum = 0;     /* E1帧的时隙数 */
    BYTE channelType = 0;  /* channel type */
    WORD16 payloadSize = 0;
    DRV_TDM_PW_CFG_INFO *pPwCfgInfo = NULL;
    time_t currentTime;    /* 以秒为单位 */
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwArg);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwArg->ptPwInfo);
    memset(&currentTime, 0, sizeof(time_t));
    time(&currentTime); /* Get current calendar time in seconds */
    systemType = BSP_cp3banSystemTypeGet();  /* 获取ZXUPN15000系统类型. */
    subslotId = (BYTE)(pPwArg->dwSubSlotId);
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM != systemType)   /* 15K-2系统 */
    {
        subslotId = 1;   /* 对于15K-2系统,子槽位始终取值为1 */
    }
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    channelNo = pPwArg->dwPWid;  
    pktCntsInBuf = pPwArg->ptPwInfo->dwJitterSize;

    rv = drv_tdm_pwe3InitFlagGet(chipId, &initFlag);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3InitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    if (DRV_TDM_STM_BOARD_UNINSTALL == initFlag)    /* 必须保证STM单板初始化完成 */
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, STM board has not been initialized.\n", __FILE__, __LINE__, subslotId);
        return DRV_TDM_STM_BOARD_UNINIT;
    }

    rv = drv_tdm_pwSemGet(chipId);  /* Get semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_pwIdGet(chipId, channelNo, &pwId);  /* 查表,通过channelNo来查找PWID. */
    if (DRV_TDM_OK != rv) 
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, drv_tdm_pwIdGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, rv);
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return rv;
    }
    rv = drv_tdm_pwMemGet(chipId, pwId, &pPwCfgInfo);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, drv_tdm_pwMemGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), rv);
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return rv;
    }
    if (NULL == pPwCfgInfo)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, pPwCfgInfo is NULL pointer.\n", __FILE__, __LINE__, ctime(&currentTime));
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return DRV_TDM_NULL_POINTER;
    }
    channelType = pPwCfgInfo->channelMode;
    e1TsNum = pPwCfgInfo->acInfo.e1TsNum;    /* E1帧的时隙数 */
    payloadSize = pPwCfgInfo->pldSize;
    if ((payloadSize < DRV_TDM_PW_PAYLOAD_SIZE_MIN) || (payloadSize > DRV_TDM_PW_PAYLOAD_SIZE_MAX)) 
    { 
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, invalid payloadSize=%u.\n", __FILE__, __LINE__, ctime(&currentTime), payloadSize); 
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return DRV_TDM_INVALID_PW_PAYLOAD_SIZE; 
    } 
    /* 计数PW的jitter buffer size. */
    rv = drv_tdm_pwJitterBufSizeGet(channelType, e1TsNum, payloadSize, pktCntsInBuf, &jitterBufSize);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, drv_tdm_pwJitterBufSizeGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, rv);
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return rv;
    }
    rv = drv_tdm_pwJitterBufSizeCheck(channelType, e1TsNum, payloadSize, jitterBufSize);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, drv_tdm_pwJitterBufSizeCheck() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, rv);
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return rv;
    }
    rv = drv_tdm_pwJitterBufSizeModify(chipId, pwId, jitterBufSize);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, drv_tdm_pwJitterBufSizeModify() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, rv);
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return rv;
    }
    rv = drv_tdm_pwJitterBufPktNumSet(chipId, pwId, pktCntsInBuf);  /* 保存驱动的软件表 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, drv_tdm_pwJitterBufPktNumSet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, rv);
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return rv;
    }
    rv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, %s, subslotId %u channelNo 0x%x pktCntsInJitBuf %u, update jitter buffer size for PW successfully.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, pktCntsInBuf);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwPldSizeModify
* 功能描述: 修改PW的payload size
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           pwId: PW ID,取值为0~1023.
*           encapNum: 封包级联数.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwPldSizeModify(BYTE chipId, WORD16 pwId, WORD16 encapNum)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE pwState = DRV_TDM_PW_FREE;  /* PW state */
    AtDevice pDevice = NULL;   /* ARRIVE芯片的device指针 */
    AtModulePw pPwModule = NULL;
    AtPw pPw = NULL;
    DRV_TDM_PW_CFG_INFO *pPwCfgInfo = NULL;
    WORD32 pktCntsInJitBuf = 0;
    BYTE e1TsNum = 0;
    BYTE channelType = 0;  /* channel type */
    WORD32 jitBufSize = 0;
    WORD32 jitBufDelay = 0;
    WORD16 pldSize = 0;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    
    rv = drv_tdm_pwStateGet(chipId, pwId, &pwState);  /* 获取PW的状态 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_pwStateGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_PW_FREE == pwState)  
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId=%u pwId=%u, pw is free.\n", __FILE__, __LINE__, chipId, pwId);
        return DRV_TDM_INVALID_PW_STATE; 
    }

    rv = drv_tdm_pwMemGet(chipId, pwId, &pPwCfgInfo);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_pwMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCfgInfo);
    channelType = pPwCfgInfo->channelMode;
    pktCntsInJitBuf = pPwCfgInfo->pktNumInBuf;
    e1TsNum = pPwCfgInfo->acInfo.e1TsNum;    /* E1帧的时隙数 */
    pldSize = encapNum * ((WORD16)e1TsNum);
    DRV_TDM_CHECK_PW_PAYLOAD_SIZE(pldSize);

    /* 获取PW的jitter buffer size. */
    rv = drv_tdm_pwJitterBufSizeGet(channelType, e1TsNum, pldSize, pktCntsInJitBuf, &jitBufSize);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u pwId %u, drv_tdm_pwJitterBufSizeGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pwId, rv);
        return rv;
    }
    rv = drv_tdm_pwJitterBufSizeCheck(channelType, e1TsNum, pldSize, jitBufSize);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u pwId %u, drv_tdm_pwJitterBufSizeCheck() rv=0x%x.\n", __FILE__, __LINE__, chipId, pwId, rv);
        return rv;
    }
    jitBufDelay = jitBufSize / 2;

    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    
    /* Get PW Module */
    pPwModule = (AtModulePw)AtDeviceModuleGet(pDevice, cAtModulePw); 
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwModule);
    
    /* Get pseudowire object by its identifier */
    pPw = AtModulePwGetPw(pPwModule, pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPw);
    
    /* Set payload size in bytes for PW. */
    if (DRV_TDM_SATOP_CHANNEL == channelType) /* SAToP PW */
    {
        rv = AtPwJitterBufferAndPayloadSizeSet(pPw, jitBufSize, jitBufDelay, pldSize);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u pwId %u, jitterBufferSize=0x%x jitterBufferDelay=0x%x payloadSize=0x%x, AtPwJitterBufferAndPayloadSizeSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pwId, jitBufSize, jitBufDelay, pldSize, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    else if ((DRV_TDM_CESOP_BASIC_CHANNEL == channelType) || (DRV_TDM_CESOP_CAS_CHANNEL == channelType))  
    {
        rv = AtPwJitterBufferAndPayloadSizeSet(pPw, jitBufSize, jitBufDelay, encapNum);
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u pwId %u, jitterBufferSize=0x%x jitterBufferDelay=0x%x payloadSize=0x%x, AtPwJitterBufferAndPayloadSizeSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pwId, jitBufSize, jitBufDelay, encapNum, rv);
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid parameter, channelType = 0x%x.\n", __FILE__, __LINE__, channelType);
        return DRV_TDM_INVALID_ARGUMENT;
    }

    /* 更新驱动的软件表 */
    rv = drv_tdm_pwEncapNumSet(chipId, pwId, encapNum);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u pwId %u, drv_tdm_pwEncapNumSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pwId, rv);
        return rv;  
    }
    rv = drv_tdm_pwPldSizeSet(chipId, pwId, pldSize);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u pwId %u, drv_tdm_pwPldSizeSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pwId, rv);
        return rv;  
    }
    /* 更新驱动的软件表 */
    rv = drv_tdm_pwJitterBufSizeSet(chipId, pwId, jitBufSize);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u pwId %u, drv_tdm_pwJitterBufSizeSet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pwId, rv);
        return rv;  
    }
    rv = drv_tdm_pwJitterBufDelaySet(chipId, pwId, jitBufDelay);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u pwId %u, drv_tdm_pwJitterBufDelaySet() rv=0x%x.\n", __FILE__, __LINE__, chipId, pwId, rv);
        return rv;  
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwPldSizeUpdate
* 功能描述: 更新PW的payload size
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pPwArg: PW配置信息.
* 输出参数:  
* 返 回 值: 
* 其它说明: 本函数提供给产品管理调用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwPldSizeUpdate(const T_BSP_SRV_FUNC_PW_BIND_ARG *pPwArg)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwTempRv = DRV_TDM_OK;
    WORD32 systemType = 0;   /* ZXUPN15000的系统类型 */
    WORD32 channelNo = 0;
    WORD16 encapNum = 0;    /* 封包级联数 */
    BYTE subslotId = 0;  /* 接口卡的子槽位号 */
    BYTE chipId = DRV_TDM_MAX_BYTE;    /* ARRIVE芯片的编号 */
    WORD16 pwId = DRV_TDM_PW_ID_INVALID;
    WORD32 initFlag = 0;   /* 单板的初始化标记 */
    time_t currentTime;    /* 以秒为单位 */
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwArg);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwArg->ptPwInfo);
    memset(&currentTime, 0, sizeof(time_t));
    time(&currentTime); /* Get current calendar time in seconds */
    systemType = BSP_cp3banSystemTypeGet();  /* 获取ZXUPN15000系统类型. */
    subslotId = (BYTE)(pPwArg->dwSubSlotId);
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM != systemType)   /* 15K-2系统 */
    {
        subslotId = 1;   /* 对于15K-2系统,子槽位始终取值为1 */
    }
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    channelNo = pPwArg->dwPWid;   
    encapNum = (WORD16)(pPwArg->ptPwInfo->dwFrameCnt);  /* 每条PW所承载的E1帧的数量 */
    
    rv = drv_tdm_pwe3InitFlagGet(chipId, &initFlag);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, chipId %u, drv_tdm_pwe3InitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), chipId, rv);
        return rv;
    }
    if (DRV_TDM_STM_BOARD_UNINSTALL == initFlag)    /* 必须保证STM单板初始化完成 */
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, STM board has not been initialized.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId);
        return DRV_TDM_STM_BOARD_UNINIT;
    }

    rv = drv_tdm_pwSemGet(chipId);  /* Get semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_pwIdGet(chipId, channelNo, &pwId);  /* 查表,通过channelNo来查找PWID. */
    if (DRV_TDM_OK != rv) 
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, drv_tdm_pwIdGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, rv);
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return rv;
    }
    rv = drv_tdm_pwPldSizeModify(chipId, pwId, encapNum);  /* 更新PW的payload size */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, drv_tdm_pwPldSizeModify() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, rv);
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return rv;
    }
    rv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, %s, subslotId %u channelNo 0x%x encapNum %u, update payload size for PW successfully.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, encapNum);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwVlan1PriModify
* 功能描述: 修改PW的VLAN1 priority
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           pwId: PW ID,取值为0~1023.
*           vlan1Pri: vlan1 priority,取值为0~7.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwVlan1PriModify(BYTE chipId, WORD16 pwId, BYTE vlan1Pri)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE pwState = 0;
    tAtZtePtnTag1 atVlan1Tag;   /* VLAN 1 */
    tAtZtePtnTag2 atVlan2Tag;   /* VLAN 2 */
    BYTE destMac[DRV_TDM_MAC_ADDR_SIZE] = {0}; /* Destination MAC address */
    DRV_TDM_PW_CFG_INFO *pPwCfgInfo = NULL;
    AtDevice pDevice = NULL;   /* ARRIVE芯片的device指针 */
    AtModulePw pPwModule = NULL;
    AtPw pPw = NULL;
    
    memset(&atVlan1Tag, 0, sizeof(tAtZtePtnTag1));
    memset(&atVlan2Tag, 0, sizeof(tAtZtePtnTag2));
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_PW_VLAN_PRIORITY(vlan1Pri);
    
    rv = drv_tdm_pwStateGet(chipId, pwId, &pwState);  /* 获取PW的状态 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_pwStateGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_PW_FREE == pwState)  
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u pwId %u, pw is free.\n", __FILE__, __LINE__, chipId, pwId);
        return DRV_TDM_INVALID_PW_STATE; 
    }
    
    rv = drv_tdm_pwMemGet(chipId, pwId, &pPwCfgInfo);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_pwMemGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCfgInfo);
    
    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    
    /* Get PW Module */
    pPwModule = (AtModulePw)AtDeviceModuleGet(pDevice, cAtModulePw); 
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwModule);
    
    /* Get pseudowire object by its identifier */
    pPw = AtModulePwGetPw(pPwModule, pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPw);

    memcpy(destMac, pPwCfgInfo->dstMacAddr, DRV_TDM_MAC_ADDR_SIZE);
    atVlan1Tag.priority = vlan1Pri;
    atVlan1Tag.cfi = pPwCfgInfo->vlan1Tag.cfi;
    atVlan1Tag.cpuPktIndicator = (BYTE)((pPwCfgInfo->vlan1Tag.vlan1Id & (WORD16)0x0800) >> 11);
    atVlan1Tag.encapType = (BYTE)((pPwCfgInfo->vlan1Tag.vlan1Id & (WORD16)0x07c0) >> 6);
    atVlan1Tag.packetLength = (BYTE)(pPwCfgInfo->vlan1Tag.vlan1Id & (WORD16)0x003f);
    atVlan2Tag.priority = pPwCfgInfo->vlan2Tag.priority;
    atVlan2Tag.cfi = pPwCfgInfo->vlan2Tag.cfi;
    atVlan2Tag.stmPortId = (BYTE)((pPwCfgInfo->vlan2Tag.vlan2Id & (WORD16)0x0e00) >> 9);
    atVlan2Tag.isMlpppIma = (BYTE)((pPwCfgInfo->vlan2Tag.vlan2Id & (WORD16)0x0100) >> 8);
    atVlan2Tag.zteChannelId = pPwCfgInfo->vlan2Tag.vlan2Id & (WORD16)0x00ff;
    rv = AtZtePwPtnHeaderSet(pPw, destMac, &atVlan1Tag, &atVlan2Tag);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtZtePwPtnHeaderSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    /* Set expected vlan1 tag. */
    rv = AtZtePtnPwExpectedTag1Set(pPw, &atVlan1Tag);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtZtePtnPwExpectedTag1Set() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    
    /* Set expected vlan2 tag. */
    rv = AtZtePtnPwExpectedTag2Set(pPw, &atVlan2Tag);
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, AtZtePtnPwExpectedTag2Set() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }

    rv = drv_tdm_pwVlan1PriSet(chipId, pwId, vlan1Pri);  /* 更新驱动的软件表 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_pwVlan1PriSet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwVlan1PriUpdate
* 功能描述: 更新PW的vlan1 priority.
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pPwArg: PW配置信息.
* 输出参数:  
* 返 回 值: 
* 其它说明: 本函数提供给产品管理调用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwVlan1PriUpdate(const T_BSP_SRV_FUNC_PW_BIND_ARG *pPwArg)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwTempRv = DRV_TDM_OK;
    WORD32 systemType = 0;   /* ZXUPN15000的系统类型 */
    WORD32 channelNo = 0;
    WORD32 initFlag = 0;
    BYTE subslotId = 0;
    BYTE chipId = DRV_TDM_MAX_BYTE;    /* ARRIVE芯片的编号 */
    WORD16 pwId = DRV_TDM_PW_ID_INVALID;
    BYTE vlan1Pri = 0;
    time_t currentTime;    /* 以秒为单位 */
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwArg);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwArg->ptPwInfo);
    memset(&currentTime, 0, sizeof(time_t));
    time(&currentTime); /* Get current calendar time in seconds */
    systemType = BSP_cp3banSystemTypeGet();  /* 获取ZXUPN15000系统类型. */
    subslotId = (BYTE)(pPwArg->dwSubSlotId);
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM != systemType)   /* 15K-2系统 */
    {
        subslotId = 1;   /* 对于15K-2系统,子槽位始终取值为1 */
    }
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    channelNo = pPwArg->dwPWid;
    vlan1Pri = (BYTE)(pPwArg->ptPwInfo->wVlan1Pri);
    DRV_TDM_CHECK_PW_VLAN_PRIORITY(vlan1Pri);
    
    rv = drv_tdm_pwe3InitFlagGet(chipId, &initFlag);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, chipId %u, drv_tdm_pwe3InitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), chipId, rv);
        return rv;
    }
    if (DRV_TDM_STM_BOARD_UNINSTALL == initFlag)    /* 必须保证STM单板初始化完成 */
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, STM board has not been initialized.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId);
        return DRV_TDM_STM_BOARD_UNINIT;
    }

    rv = drv_tdm_pwSemGet(chipId);  /* Get semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_pwIdGet(chipId, channelNo, &pwId);  /* 查表,通过channelNo来查找PWID. */
    if (DRV_TDM_OK != rv) 
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, drv_tdm_pwIdGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, rv);
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return rv;
    }
    rv = drv_tdm_pwVlan1PriModify(chipId, pwId, vlan1Pri);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, drv_tdm_pwVlan1PriModify() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, rv);
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return rv;
    }
    rv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, %s, subslotId %u channelNo 0x%x vlan1Pri %u, update vlan1 pri field for PW successfully.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, vlan1Pri);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwLbitEnable
* 功能描述: enable/disable L-bit of PW.
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pPwArg: PW配置信息.
* 输出参数:  
* 返 回 值: 
* 其它说明: 本函数提供给产品管理调用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-03-12   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwLbitEnable(const T_BSP_SRV_FUNC_PW_BIND_ARG *pPwArg)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwTempRv = DRV_TDM_OK;
    WORD32 systemType = 0;   /* ZXUPN15000的系统类型 */
    WORD32 channelNo = 0;
    WORD32 initFlag = 0;
    BYTE subslotId = 0;
    BYTE chipId = DRV_TDM_MAX_BYTE;    /* ARRIVE芯片的编号 */
    WORD16 pwId = DRV_TDM_PW_ID_INVALID;
    BYTE ucLbitEnableFlag = 0;
    AtDevice pDevice = NULL;   /* ARRIVE芯片的device指针 */
    AtModulePw pPwModule = NULL;
    AtPw pPw = NULL;
    time_t currentTime;    /* 以秒为单位 */
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwArg);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwArg->ptPwInfo);
    memset(&currentTime, 0, sizeof(time_t));
    time(&currentTime); /* Get current calendar time in seconds */
    systemType = BSP_cp3banSystemTypeGet();  /* 获取ZXUPN15000系统类型. */
    subslotId = (BYTE)(pPwArg->dwSubSlotId);
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM != systemType)   /* 15K-2系统 */
    {
        subslotId = 1;   /* 对于15K-2系统,子槽位始终取值为1 */
    }
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    channelNo = pPwArg->dwPWid;
    if (T_BSP_L_BIT_DISABLED == pPwArg->ptPwInfo->dwLbitEnable)
    {
        ucLbitEnableFlag = DRV_TDM_PW_L_BIT_DISABLED;
    }
    else if (T_BSP_L_BIT_ENABLED == pPwArg->ptPwInfo->dwLbitEnable)
    {
        ucLbitEnableFlag = DRV_TDM_PW_L_BIT_ENABLED;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, pmEnableFlag=%u.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, pPwArg->ptPwInfo->dwLbitEnable);
        return DRV_TDM_INVALID_ARGUMENT;
    }
    
    rv = drv_tdm_pwe3InitFlagGet(chipId, &initFlag);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, chipId %u, drv_tdm_pwe3InitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), chipId, rv);
        return rv;
    }
    if (DRV_TDM_STM_BOARD_UNINSTALL == initFlag)    /* 必须保证STM单板初始化完成 */
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, STM board has not been initialized.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId);
        return DRV_TDM_STM_BOARD_UNINIT;
    }

    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, chipId %u, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), chipId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);

    /* Get PW Module */
    pPwModule = (AtModulePw)AtDeviceModuleGet(pDevice, cAtModulePw); 
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwModule);

    rv = drv_tdm_pwSemGet(chipId);  /* Get semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_pwIdGet(chipId, channelNo, &pwId);  /* 查表,通过channelNo来查找PWID. */
    if (DRV_TDM_OK != rv) 
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, drv_tdm_pwIdGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, rv);
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return rv;
    }
    if (pwId >= DRV_TDM_MAX_PW_NUM_PER_STM1_CARD) 
    { 
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid pwId=%u.\n", __FILE__, __LINE__, pwId); 
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return DRV_TDM_INVALID_PW_ID; 
    }
    pPw = AtModulePwGetPw(pPwModule, pwId);  /* Get pseudowire object by its identifier */
    if (NULL == pPw)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u pwId %u, pPw is NULL pointer.\n", __FILE__, __LINE__, chipId, pwId);
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return DRV_TDM_NULL_POINTER;
    }
    if (DRV_TDM_PW_L_BIT_DISABLED == ucLbitEnableFlag)
    {
        rv = AtPwCwAutoTxLBitEnable(pPw, cAtFalse); /* Disable auto transmitting TX L-bit packets when attachment circuit is fail */
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, AtPwCwAutoTxLBitEnable() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, rv);
            dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
            if (DRV_TDM_OK != dwTempRv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
                return dwTempRv;
            }
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    else if (DRV_TDM_PW_L_BIT_ENABLED == ucLbitEnableFlag)
    {
        rv = AtPwCwAutoTxLBitEnable(pPw, cAtTrue); /* Enable auto transmitting TX L-bit packets when attachment circuit is fail */
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, AtPwCwAutoTxLBitEnable() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, rv);
            dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
            if (DRV_TDM_OK != dwTempRv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
                return dwTempRv;
            }
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, drvEnableFlag=%u.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, ucLbitEnableFlag);
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return DRV_TDM_INVALID_ARGUMENT;
    }
    rv = drv_tdm_pwLbitEnableFlagSet(chipId, pwId, ucLbitEnableFlag);  /* 保存驱动的软件表 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, drv_tdm_pwLbitEnableFlagSet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, rv);
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return rv;
    }
    rv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, %s, subslotId %u channelNo 0x%x LbitEnableFlag %u, enable L-bit for PW successfully.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, ucLbitEnableFlag);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwLbitForce
* 功能描述: force/unforce L-bit of PW.
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pPwArg: PW配置信息.
* 输出参数:  
* 返 回 值: 
* 其它说明: 本函数提供给产品管理调用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-03-12   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwLbitForce(const T_BSP_SRV_FUNC_PW_BIND_ARG *pPwArg)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwTempRv = DRV_TDM_OK;
    WORD32 systemType = 0;   /* ZXUPN15000的系统类型 */
    WORD32 channelNo = 0;
    WORD32 initFlag = 0;
    BYTE subslotId = 0;
    BYTE chipId = DRV_TDM_MAX_BYTE;    /* ARRIVE芯片的编号 */
    WORD16 pwId = DRV_TDM_PW_ID_INVALID;
    BYTE ucLbitForceFlag = 0;
    AtDevice pDevice = NULL;   /* ARRIVE芯片的device指针 */
    AtModulePw pPwModule = NULL;
    AtPw pPw = NULL;
    time_t currentTime;    /* 以秒为单位 */
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwArg);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwArg->ptPwInfo);
    memset(&currentTime, 0, sizeof(time_t));
    time(&currentTime); /* Get current calendar time in seconds */
    systemType = BSP_cp3banSystemTypeGet();  /* 获取ZXUPN15000系统类型. */
    subslotId = (BYTE)(pPwArg->dwSubSlotId);
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM != systemType)   /* 15K-2系统 */
    {
        subslotId = 1;   /* 对于15K-2系统,子槽位始终取值为1 */
    }
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    channelNo = pPwArg->dwPWid;
    if (T_BSP_L_BIT_UNFORCED == pPwArg->ptPwInfo->dwLbitForce)
    {
        ucLbitForceFlag = DRV_TDM_PW_L_BIT_UNFORCED;
    }
    else if (T_BSP_L_BIT_FORCED == pPwArg->ptPwInfo->dwLbitForce)
    {
        ucLbitForceFlag = DRV_TDM_PW_L_BIT_FORCED;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, pmForceFlag=%u.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, pPwArg->ptPwInfo->dwLbitForce);
        return DRV_TDM_INVALID_ARGUMENT;
    }
    
    rv = drv_tdm_pwe3InitFlagGet(chipId, &initFlag);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, chipId %u, drv_tdm_pwe3InitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), chipId, rv);
        return rv;
    }
    if (DRV_TDM_STM_BOARD_UNINSTALL == initFlag)    /* 必须保证STM单板初始化完成 */
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, STM board has not been initialized.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId);
        return DRV_TDM_STM_BOARD_UNINIT;
    }

    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, chipId %u, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), chipId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    
    /* Get PW Module */
    pPwModule = (AtModulePw)AtDeviceModuleGet(pDevice, cAtModulePw); 
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwModule);

    rv = drv_tdm_pwSemGet(chipId);  /* Get semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_pwIdGet(chipId, channelNo, &pwId);  /* 查表,通过channelNo来查找PWID. */
    if (DRV_TDM_OK != rv) 
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, drv_tdm_pwIdGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, rv);
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return rv;
    }
    if (pwId >= DRV_TDM_MAX_PW_NUM_PER_STM1_CARD) 
    { 
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid pwId=%u.\n", __FILE__, __LINE__, pwId); 
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return DRV_TDM_INVALID_PW_ID; 
    }
    pPw = AtModulePwGetPw(pPwModule, pwId);  /* Get pseudowire object by its identifier */
    if (NULL == pPw)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u pwId %u, pPw is NULL pointer.\n", __FILE__, __LINE__, chipId, pwId);
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return DRV_TDM_NULL_POINTER;
    }

    if (DRV_TDM_PW_L_BIT_UNFORCED == ucLbitForceFlag)
    {
        rv = AtChannelTxAlarmUnForce((AtChannel)pPw, cAtPwAlarmTypeLBit);  /* Un-Force TX alarm */
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, AtChannelTxAlarmUnForce() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, rv);
            dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
            if (DRV_TDM_OK != dwTempRv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
                return dwTempRv;
            }
            return DRV_TDM_SDK_API_FAIL;
        }        
    }
    else if (DRV_TDM_PW_L_BIT_FORCED == ucLbitForceFlag)
    {        
        rv = AtChannelTxAlarmForce((AtChannel)pPw, cAtPwAlarmTypeLBit);  /* Force TX alarm */
        if (cAtOk != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, AtChannelTxAlarmForce() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, rv);
            dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
            if (DRV_TDM_OK != dwTempRv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
                return dwTempRv;
            }
            return DRV_TDM_SDK_API_FAIL;
        }
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, drvForceFlag=%u.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, ucLbitForceFlag);
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return DRV_TDM_INVALID_ARGUMENT;
    }

    rv = drv_tdm_pwLbitForceFlagSet(chipId, pwId, ucLbitForceFlag);  /* 保存驱动的软件表 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, drv_tdm_pwLbitForceFlagSet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, rv);
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return rv;
    }
    rv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, %s, subslotId %u channelNo 0x%x LbitForceFlag %u, force L-bit for PW successfully.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, ucLbitForceFlag);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwIdleCodeUpdate
* 功能描述: Update PW idle code.
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pPwArg: PW配置信息.
* 输出参数:  
* 返 回 值: 
* 其它说明: 本函数提供给产品管理调用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-03-12   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwIdleCodeUpdate(const T_BSP_SRV_FUNC_PW_BIND_ARG *pPwArg)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 dwTempRv = DRV_TDM_OK;
    WORD32 systemType = 0;   /* ZXUPN15000的系统类型 */
    WORD32 channelNo = 0;
    WORD32 initFlag = 0;
    BYTE subslotId = 0;
    BYTE chipId = DRV_TDM_MAX_BYTE;    /* ARRIVE芯片的编号 */
    WORD16 pwId = DRV_TDM_PW_ID_INVALID;
    BYTE ucIdleCode = 0;
    AtDevice pDevice = NULL;   /* ARRIVE芯片的device指针 */
    AtModulePw pPwModule = NULL;
    AtPw pPw = NULL;
    time_t currentTime;    /* 以秒为单位 */
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwArg);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwArg->ptPwInfo);
    memset(&currentTime, 0, sizeof(time_t));
    time(&currentTime); /* Get current calendar time in seconds */
    systemType = BSP_cp3banSystemTypeGet();  /* 获取ZXUPN15000系统类型. */
    subslotId = (BYTE)(pPwArg->dwSubSlotId);
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM != systemType)   /* 15K-2系统 */
    {
        subslotId = 1;   /* 对于15K-2系统,子槽位始终取值为1 */
    }
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    channelNo = pPwArg->dwPWid;
    ucIdleCode = (BYTE)(pPwArg->ptPwInfo->vIdleCode);
    
    rv = drv_tdm_pwe3InitFlagGet(chipId, &initFlag);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, chipId %u, drv_tdm_pwe3InitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), chipId, rv);
        return rv;
    }
    if (DRV_TDM_STM_BOARD_UNINSTALL == initFlag)    /* 必须保证STM单板初始化完成 */
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, STM board has not been initialized.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId);
        return DRV_TDM_STM_BOARD_UNINIT;
    }

    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, chipId %u, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), chipId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    
    /* Get PW Module */
    pPwModule = (AtModulePw)AtDeviceModuleGet(pDevice, cAtModulePw); 
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwModule);

    rv = drv_tdm_pwSemGet(chipId);  /* Get semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_pwIdGet(chipId, channelNo, &pwId);  /* 查表,通过channelNo来查找PWID. */
    if (DRV_TDM_OK != rv) 
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, drv_tdm_pwIdGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, rv);
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return rv;
    }
    if (pwId >= DRV_TDM_MAX_PW_NUM_PER_STM1_CARD) 
    { 
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid pwId=%u.\n", __FILE__, __LINE__, pwId); 
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return DRV_TDM_INVALID_PW_ID; 
    }
    pPw = AtModulePwGetPw(pPwModule, pwId);  /* Get pseudowire object by its identifier */
    if (NULL == pPw)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u pwId %u, pPw is NULL pointer.\n", __FILE__, __LINE__, chipId, pwId);
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return DRV_TDM_NULL_POINTER;
    }

    rv = AtPwIdleCodeSet(pPw, ucIdleCode);  /* Set PW IDLE code */
    if (cAtOk != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, AtPwIdleCodeSet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, rv);
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return DRV_TDM_SDK_API_FAIL;
    }
    
    rv = drv_tdm_pwIdleCodeSet(chipId, pwId, ucIdleCode);  /* 保存驱动的软件表 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u channelNo 0x%x, drv_tdm_pwIdleCodeSet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, rv);
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return rv;
    }
    rv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, %s, subslotId %u channelNo 0x%x idleCode 0x%x, update idle code for PW successfully.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, channelNo, ucIdleCode);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atPwJitBufSizeGet
* 功能描述: 获取FPGA芯片的PW jitter buffer size.
* 访问的表: 无
* 修改的表: 无
* 输入参数: ucChipId: 芯片编号,取值为0~3.
*           wPwId: PW ID,取值为0~1023.
* 输出参数: *pdwJitBufSize: 保存芯片的PW Jitter Buffe Size. 
* 返 回 值: 
* 其它说明: 直接从芯片中读取.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atPwJitBufSizeGet(BYTE ucChipId, 
                                           WORD16 wPwId, 
                                           WORD32 *pdwJitBufSize)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    AtDevice pDevice = NULL;   /* ARRIVE芯片的device指针 */
    AtModulePw pPwModule = NULL;
    AtPw pPw = NULL;
    BYTE ucPwState = 0;  /* PW state. */
    
    DRV_TDM_CHECK_CHIP_ID(ucChipId);
    DRV_TDM_CHECK_PW_ID(wPwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pdwJitBufSize);

    dwRetValue = drv_tdm_pwStateGet(ucChipId, wPwId, &ucPwState);
    if (DRV_TDM_OK != dwRetValue)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CFG_MSG, "ERROR: %s line %d, drv_tdm_pwStateGet() rv=0x%x.\n", __FILE__, __LINE__, dwRetValue);
        return dwRetValue;
    }
    if (DRV_TDM_PW_FREE == ucPwState)  
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CFG_MSG, "ERROR: %s line %d, chipId %u pwId %u, pw is free.\n", __FILE__, __LINE__, ucChipId, wPwId);
        return DRV_TDM_INVALID_PW_STATE; 
    }
    
    dwRetValue = drv_tdm_atDevicePtrGet(ucChipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != dwRetValue)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CFG_MSG, "ERROR: %s line %d, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, dwRetValue);
        return dwRetValue;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    
    /* Get PW Module */
    pPwModule = (AtModulePw)AtDeviceModuleGet(pDevice, cAtModulePw); 
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwModule);
    
    /* Get pseudowire object by its identifier */
    pPw = AtModulePwGetPw(pPwModule, wPwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPw);
    
    *pdwJitBufSize = 0;  /* 先清零再赋值 */
    *pdwJitBufSize = AtPwJitterBufferSizeGet(pPw); /* Get jitter buffer size in microseconds */
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atPwJitBufDelayGet
* 功能描述: 获取FPGA芯片的PW jitter buffer delay.
* 访问的表: 无
* 修改的表: 无
* 输入参数: ucChipId: 芯片编号,取值为0~3.
*           wPwId: PW ID,取值为0~1023.
* 输出参数: *pdwJitBufDelay: 保存芯片的PW Jitter Buffe delay. 
* 返 回 值: 
* 其它说明: 直接从芯片中读取.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atPwJitBufDelayGet(BYTE ucChipId, 
                                             WORD16 wPwId, 
                                             WORD32 *pdwJitBufDelay)
{
    WORD32 dwRetValue = DRV_TDM_OK;
    AtDevice pDevice = NULL;   /* ARRIVE芯片的device指针 */
    AtModulePw pPwModule = NULL;
    AtPw pPw = NULL;
    BYTE ucPwState = 0;  /* pw state */
    
    DRV_TDM_CHECK_CHIP_ID(ucChipId);
    DRV_TDM_CHECK_PW_ID(wPwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pdwJitBufDelay);

    dwRetValue = drv_tdm_pwStateGet(ucChipId, wPwId, &ucPwState);
    if (DRV_TDM_OK != dwRetValue)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CFG_MSG, "ERROR: %s line %d, drv_tdm_pwStateGet() rv=0x%x.\n", __FILE__, __LINE__, dwRetValue);
        return dwRetValue;
    }
    if (DRV_TDM_PW_FREE == ucPwState)  
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CFG_MSG, "ERROR: %s line %d, chipId %u pwId %u, pw is free.\n", __FILE__, __LINE__, ucChipId, wPwId);
        return DRV_TDM_INVALID_PW_STATE; 
    }
    
    dwRetValue = drv_tdm_atDevicePtrGet(ucChipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != dwRetValue)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CFG_MSG, "ERROR: %s line %d, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, dwRetValue);
        return dwRetValue;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    
    /* Get PW Module */
    pPwModule = (AtModulePw)AtDeviceModuleGet(pDevice, cAtModulePw); 
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwModule);
    
    /* Get pseudowire object by its identifier */
    pPw = AtModulePwGetPw(pPwModule, wPwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPw);
    
    *pdwJitBufDelay = 0;  /* 先清零再赋值 */
    *pdwJitBufDelay = AtPwJitterBufferDelayGet(pPw); /* Get jitter buffer delay in microseconds */
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwUpdate
* 功能描述: 更新PW.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           ethIntfId: 需要进行GE接口倒换的Ethernet接口编号,取值为0~1.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 当FPGA芯片的Ethernet接口发生倒换时,需要更新PW.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-11-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwUpdate(BYTE chipId, BYTE ethIntfId)
{
    WORD32 dwRetValue = DRV_TDM_OK;  /* 函数返回码 */
    WORD16 wPwId = 0;
    BYTE ucPwState = 0;
    WORD32 dwPwNum = 0;
    DRV_TDM_PW_CFG_INFO *pPwCfgInfo = NULL;
    DRV_TDM_PW_CFG_INFO newPwCfgInfo;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_ETH_INTF_INDEX(ethIntfId);
    
    for (wPwId = DRV_TDM_PW_ID_MIN; wPwId <= DRV_TDM_PW_ID_MAX; wPwId++)
    {
        memset(&newPwCfgInfo, 0, sizeof(DRV_TDM_PW_CFG_INFO));
        dwRetValue = drv_tdm_pwStateGet(chipId, wPwId, &ucPwState);
        if (DRV_TDM_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_pwStateGet() rv=0x%x.\n", __FILE__, __LINE__, dwRetValue);
            continue;
        }
        if (DRV_TDM_PW_FREE != ucPwState)
        {
            dwRetValue = drv_tdm_pwMemGet(chipId, wPwId, &pPwCfgInfo);
            if (DRV_TDM_OK != dwRetValue)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_pwMemGet() rv=0x%x.\n", __FILE__, __LINE__, dwRetValue);
                continue;
            }
            if (NULL == pPwCfgInfo)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, pPwCfgInfo is null pointer.\n", __FILE__, __LINE__);
                continue;
            }
            memcpy(&newPwCfgInfo, pPwCfgInfo, sizeof(DRV_TDM_PW_CFG_INFO));
    
            if ((ethIntfId == pPwCfgInfo->ethIntfIndex) 
                && (DRV_TDM_ETH_INTF_NONE_SWITCH == pPwCfgInfo->ethIntfSwitchFlag))
            {
                if (DRV_TDM_CHIP_ETH_INTF_ID_START == ethIntfId)
                {
                    newPwCfgInfo.ethIntfIndex = DRV_TDM_CHIP_ETH_INTF_ID_END;
                }
                else if (DRV_TDM_CHIP_ETH_INTF_ID_END == ethIntfId)
                {
                    newPwCfgInfo.ethIntfIndex = DRV_TDM_CHIP_ETH_INTF_ID_START;
                }
                else
                {
                    BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, invalid ethernet port id, ethPortId=%u.\n", __FILE__, __LINE__, chipId, ethIntfId);
                    continue;
                }

                dwRetValue = drv_tdm_ethIntfPwNumGet(chipId, ethIntfId, &dwPwNum);
                if (DRV_TDM_OK != dwRetValue)
                {
                    BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_ethIntfPwNumGet() rv=0x%x.\n", __FILE__, __LINE__, dwRetValue);
                    continue;
                }
                if (dwPwNum >= DRV_TDM_ETH_INTF_MAX_PW_NUM)
                {
                    break;
                }
                
                newPwCfgInfo.ethIntfSwitchFlag = DRV_TDM_ETH_INTF_SWITCH;
                dwRetValue = drv_tdm_pwDelete(chipId, wPwId);
                if (DRV_TDM_OK != dwRetValue)
                {
                    BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u channelNo 0x%x, drv_tdm_pwDelete() rv=0x%x.\n", __FILE__, __LINE__, newPwCfgInfo.acInfo.subslotId, newPwCfgInfo.acInfo.channelNo, dwRetValue);
                    continue;
                }
                dwRetValue = drv_tdm_pwAdd(&newPwCfgInfo);
                if (DRV_TDM_OK != dwRetValue)
                {
                    BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u channelNo 0x%x, drv_tdm_pwAdd() rv=0x%x.\n", __FILE__, __LINE__, newPwCfgInfo.acInfo.subslotId, newPwCfgInfo.acInfo.channelNo, dwRetValue);
                    continue;
                }
            }
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwRestore
* 功能描述: 恢复PW.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           ethIntfId: 需要进行GE接口回切的Ethernet接口编号,取值为0~1.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 当FPGA芯片的Ethernet接口发生回切时,需要恢复PW.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-11-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwRestore(BYTE chipId, BYTE ethIntfId)
{
    WORD32 dwRetValue = DRV_TDM_OK;  /* 函数返回码 */
    WORD16 wPwId = 0;
    BYTE ucPwState = 0;
    WORD32 dwPwNum = 0;
    BYTE oldEthIntfId = 0;
    DRV_TDM_PW_CFG_INFO *pPwCfgInfo = NULL;
    DRV_TDM_PW_CFG_INFO newPwCfgInfo;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_ETH_INTF_INDEX(ethIntfId);
    
    for (wPwId = DRV_TDM_PW_ID_MIN; wPwId <= DRV_TDM_PW_ID_MAX; wPwId++)
    {
        memset(&newPwCfgInfo, 0, sizeof(DRV_TDM_PW_CFG_INFO));
        dwRetValue = drv_tdm_pwStateGet(chipId, wPwId, &ucPwState);
        if (DRV_TDM_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_pwStateGet() rv=0x%x.\n", __FILE__, __LINE__, dwRetValue);
            continue;
        }
        if (DRV_TDM_PW_FREE != ucPwState)
        {
            dwRetValue = drv_tdm_pwMemGet(chipId, wPwId, &pPwCfgInfo);
            if (DRV_TDM_OK != dwRetValue)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_pwMemGet() rv=0x%x.\n", __FILE__, __LINE__, dwRetValue);
                continue;
            }
            if (NULL == pPwCfgInfo)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, pPwCfgInfo is null pointer.\n", __FILE__, __LINE__);
                continue;
            }
            memcpy(&newPwCfgInfo, pPwCfgInfo, sizeof(DRV_TDM_PW_CFG_INFO));

            if (DRV_TDM_CHIP_ETH_INTF_ID_START == ethIntfId)
            {
                oldEthIntfId = DRV_TDM_CHIP_ETH_INTF_ID_END;
            }
            else if (DRV_TDM_CHIP_ETH_INTF_ID_END == ethIntfId)
            {
                oldEthIntfId = DRV_TDM_CHIP_ETH_INTF_ID_START;
            }
            else
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, invalid ethernet port id, ethPortId=%u.\n", __FILE__, __LINE__, chipId, ethIntfId);
                continue;
            }
            
            if ((oldEthIntfId == pPwCfgInfo->ethIntfIndex) 
                && (DRV_TDM_ETH_INTF_NONE_SWITCH != pPwCfgInfo->ethIntfSwitchFlag))
            {
                dwRetValue = drv_tdm_ethIntfPwNumGet(chipId, ethIntfId, &dwPwNum);
                if (DRV_TDM_OK != dwRetValue)
                {
                    BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, drv_tdm_ethIntfPwNumGet() rv=0x%x.\n", __FILE__, __LINE__, dwRetValue);
                    continue;
                }
                if (dwPwNum >= DRV_TDM_ETH_INTF_MAX_PW_NUM)
                {
                    break;
                }

                newPwCfgInfo.ethIntfIndex = ethIntfId;
                newPwCfgInfo.ethIntfSwitchFlag = DRV_TDM_ETH_INTF_NONE_SWITCH;
                dwRetValue = drv_tdm_pwDelete(chipId, wPwId);
                if (DRV_TDM_OK != dwRetValue)
                {
                    BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u channelNo 0x%x, drv_tdm_pwDelete() rv=0x%x.\n", __FILE__, __LINE__, newPwCfgInfo.acInfo.subslotId, newPwCfgInfo.acInfo.channelNo, dwRetValue);
                    continue;
                }
                dwRetValue = drv_tdm_pwAdd(&newPwCfgInfo);
                if (DRV_TDM_OK != dwRetValue)
                {
                    BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u channelNo 0x%x, drv_tdm_pwAdd() rv=0x%x.\n", __FILE__, __LINE__, newPwCfgInfo.acInfo.subslotId, newPwCfgInfo.acInfo.channelNo, dwRetValue);
                    continue;
                }
            }
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atPwCntGet
* 功能描述: 获取PW的报文统计计数 
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           pwId: PWID,取值为0~1023.
* 输出参数: *pPwCntInfo: 保存PW报文统计. 
* 返 回 值: 
* 其它说明: 直接从FPGA芯片中来读取PWE3报文统计.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atPwCntGet(BYTE chipId, WORD16 pwId, DRV_TDM_PW_CNT_INFO *pPwCntInfo)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    BYTE pwState = DRV_TDM_PW_FREE;  /* PW state */
    BYTE pwType = 0;  /* PW type */
    AtDevice pDevice = NULL;   /* ARRIVE芯片的device指针 */
    AtModulePw pPwModule = NULL;
    AtPw pPw = NULL;
    AtPwCounters pPwCnts = NULL;  /* Variable to hold PW counter object */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCntInfo);
    
    rv = drv_tdm_pwStateGet(chipId, pwId, &pwState);  /* 获取PW的状态 */
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, drv_tdm_pwStateGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    if (DRV_TDM_PW_FREE == pwState)  /* 若PW处于FREE状态的话,则直接返回失败 */
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, chipId=%u pwId=%u, pw is free.\n", __FILE__, __LINE__, chipId, pwId);
        return DRV_TDM_INVALID_PW_STATE;
    }
    
    rv = drv_tdm_pwTypeGet(chipId, pwId, &pwType);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, drv_tdm_pwTypeGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    
    /* Get PW Module */
    pPwModule = (AtModulePw)AtDeviceModuleGet(pDevice, cAtModulePw); 
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwModule);
    
    /* Get pseudowire object by its identifier */
    pPw = AtModulePwGetPw(pPwModule, pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPw);
    
    memset(pPwCntInfo, 0, sizeof(DRV_TDM_PW_CNT_INFO));
    rv = AtChannelAllCountersClear((AtChannel)pPw, &pPwCnts); /* read clear */
    if (cAtOk != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, AtChannelAllCountersClear() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_TDM_SDK_API_FAIL;
    }
    pPwCntInfo->txPkts = (WORD64)(AtPwCountersTxPacketsGet(pPwCnts));
    pPwCntInfo->txPayloadBytes = (WORD64)(AtPwCountersTxPayloadBytesGet(pPwCnts));
    pPwCntInfo->rxPkts = (WORD64)(AtPwCountersRxPacketsGet(pPwCnts));
    pPwCntInfo->rxPayloadBytes = (WORD64)(AtPwCountersRxPayloadBytesGet(pPwCnts));
    pPwCntInfo->rxDiscardedPkts = (WORD64)(AtPwCountersRxDiscardedPacketsGet(pPwCnts));
    pPwCntInfo->rxMalformedPkts = (WORD64)(AtPwCountersRxMalformedPacketsGet(pPwCnts));
    pPwCntInfo->rxReorderedPkts = (WORD64)(AtPwCountersRxReorderedPacketsGet(pPwCnts));
    pPwCntInfo->rxLostPkts = (WORD64)(AtPwCountersRxLostPacketsGet(pPwCnts));
    pPwCntInfo->rxOutofSeqDropPkts = (WORD64)(AtPwCountersRxOutOfSeqDropPacketsGet(pPwCnts));
    pPwCntInfo->rxOamPkts = (WORD64)(AtPwCountersRxOamPacketsGet(pPwCnts));
    pPwCntInfo->txLbitPkts = (WORD64)(AtPwTdmCountersTxLbitPacketsGet((AtPwTdmCounters)pPwCnts));
    pPwCntInfo->txRbitPkts = (WORD64)(AtPwTdmCountersTxRbitPacketsGet((AtPwTdmCounters)pPwCnts));
    pPwCntInfo->rxLbitPkts = (WORD64)(AtPwTdmCountersRxLbitPacketsGet((AtPwTdmCounters)pPwCnts));
    pPwCntInfo->rxRbitPkts = (WORD64)(AtPwTdmCountersRxRbitPacketsGet((AtPwTdmCounters)pPwCnts));
    pPwCntInfo->rxJitBufOverrunPkts = (WORD64)(AtPwTdmCountersRxJitBufOverrunGet((AtPwTdmCounters)pPwCnts));
    pPwCntInfo->rxJitBufUnderrunPkts = (WORD64)(AtPwTdmCountersRxJitBufUnderrunGet((AtPwTdmCounters)pPwCnts));
    pPwCntInfo->rxLops = (WORD64)(AtPwTdmCountersRxLopsGet((AtPwTdmCounters)pPwCnts));
    pPwCntInfo->rxPktsSentToTdm = (WORD64)(AtPwTdmCountersRxPacketsSentToTdmGet((AtPwTdmCounters)pPwCnts));
    if ((DRV_TDM_CESOP_BASIC_CHANNEL == pwType) || (DRV_TDM_CESOP_CAS_CHANNEL == pwType)) /* CESoP PW */
    {
        pPwCntInfo->txMbitPkts = (WORD64)(AtPwCESoPCountersTxMbitPacketsGet((AtPwTdmCounters)pPwCnts));
        pPwCntInfo->rxMbitPkts = (WORD64)(AtPwCESoPCountersRxMbitPacketsGet((AtPwTdmCounters)pPwCnts));
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwCntGet
* 功能描述: 获取PW的报文统计计数 
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           pwId: PWID,取值为0~1023.
* 输出参数: *pPwCntInfo: 保存PW报文统计. 
* 返 回 值: 
* 其它说明: 直接从FPGA芯片中来读取PWE3报文统计.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-03-26   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwCntGet(BYTE chipId, WORD16 pwId, DRV_TDM_PW_CNT_INFO *pPwCntInfo)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 dwTempRv = DRV_TDM_OK;  /* 函数返回码 */
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwCntInfo);
    
    rv = drv_tdm_pwSemGet(chipId);  /* Get semaphore. */
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_atPwCntGet(chipId, pwId, pPwCntInfo);  
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_atPwCntGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return rv;
    }
    dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
    if (DRV_TDM_OK != dwTempRv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
        return dwTempRv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_atPwAlarmGet
* 功能描述: 获取PW告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           pwId: PWID,取值为0~1023.
* 输出参数: *pPwAlm: 保存PW告警. 
* 返 回 值: 
* 其它说明: 直接从FPGA芯片中来读取PWE3告警.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_atPwAlarmGet(BYTE chipId, WORD16 pwId, DRV_TDM_PW_ALM_INFO *pPwAlm)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 alarmState = 0;   /* PW alarm state bitmap */
    BYTE pwState = DRV_TDM_PW_FREE;  /* PW state */
    BYTE pwType = 0;  /* PW type */
    AtDevice pDevice = NULL;   /* ARRIVE芯片的device指针 */
    AtModulePw pPwModule = NULL;
    AtPw pPw = NULL;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwAlm);
    
    rv = drv_tdm_pwStateGet(chipId, pwId, &pwState);  /* 获取PW的状态 */
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_ALM_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwStateGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    if (DRV_TDM_PW_FREE == pwState)  /* 若PW处于FREE状态的话,则直接返回失败 */
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_ALM_MSG, "ERROR: %s line %d, chipId=%u pwId=%u, pw is free.\n", __FILE__, __LINE__, chipId, pwId);
        return DRV_TDM_INVALID_PW_STATE;
    }
    
    rv = drv_tdm_pwTypeGet(chipId, pwId, &pwType);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_ALM_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwTypeGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    
    rv = drv_tdm_atDevicePtrGet(chipId, &pDevice); /* 获取ARRIVE芯片的device指针 */
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_ALM_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_atDevicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    DRV_TDM_CHECK_POINTER_IS_NULL(pDevice);
    
    /* Get PW Module */
    pPwModule = (AtModulePw)AtDeviceModuleGet(pDevice, cAtModulePw); 
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwModule);
    
    /* Get pseudowire object by its identifier */
    pPw = AtModulePwGetPw(pPwModule, pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPw);
    
    /* Get PW alarm */
    alarmState = AtChannelAlarmGet((AtChannel)pPw);
    
    memset(pPwAlm, 0, sizeof(DRV_TDM_PW_ALM_INFO));
    /* Loss of Packet Alarm */
    if (0 != (alarmState & cAtPwAlarmTypeLops)) 
    {
        pPwAlm->lopsAlm = 1;
    }
    else
    {
        pPwAlm->lopsAlm = 0;
    }
    
    /* Jitter buffer overrun alarm */
    if (0 != (alarmState & cAtPwAlarmTypeJitterBufferOverrun))
    {
        pPwAlm->jitterBufOverrunAlm = 1;
    }
    else
    {
        pPwAlm->jitterBufOverrunAlm = 0;
    }
    
    /* Jitter buffer underrun alarm */
    if (0 != (alarmState & cAtPwAlarmTypeJitterBufferUnderrun))
    {
        pPwAlm->jitterBufUnderrunAlm = 1;
    }
    else
    {
        pPwAlm->jitterBufUnderrunAlm = 0;
    }
    
    /* L bit alarm */
    if (0 != (alarmState & cAtPwAlarmTypeLBit))
    {
        pPwAlm->lBitAlm = 1;
    }
    else
    {
        pPwAlm->lBitAlm = 0;
    }
    
    /* R bit alarm */
    if (0 != (alarmState & cAtPwAlarmTypeRBit))
    {
        pPwAlm->rBitAlm = 1;
    }
    else
    {
        pPwAlm->rBitAlm = 0;
    }
    
    /* M bit alarm */
    if ((DRV_TDM_CESOP_BASIC_CHANNEL == pwType) || (DRV_TDM_CESOP_CAS_CHANNEL == pwType)) /* CESoP PW */
    {
        if (0 != (alarmState & cAtPwAlarmTypeMBit))
        {
            pPwAlm->mBitAlm = 1;
        }
        else
        {
            pPwAlm->mBitAlm = 0;
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwAlarmGet
* 功能描述: 获取PW告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           pwId: PWID,取值为0~1023.
* 输出参数: *pPwAlm: 保存PW告警. 
* 返 回 值: 
* 其它说明: 直接从FPGA芯片中来读取PWE3告警.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-05-16   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwAlarmGet(BYTE chipId, WORD16 pwId, DRV_TDM_PW_ALM_INFO *pPwAlm)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 dwTempRv = DRV_TDM_OK;
    
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_PW_ID(pwId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwAlm);
    
    rv = drv_tdm_pwSemGet(chipId);  /* Get semaphore. */
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_ALM_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        return rv;
    }
    rv = drv_tdm_atPwAlarmGet(chipId, pwId, pPwAlm);  /* 获取PW的状态 */
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_ALM_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_atPwAlarmGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
        dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
        if (DRV_TDM_OK != dwTempRv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_ALM_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
            return dwTempRv;
        }
        return rv;
    }
    dwTempRv = drv_tdm_pwSemFree(chipId);  /* Free semaphore. */
    if (DRV_TDM_OK != dwTempRv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_ALM_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwSemFree() rv=0x%x.\n", __FILE__, __LINE__, chipId, dwTempRv);
        return dwTempRv;
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwe3PerfAlmFlagSet
* 功能描述: 设置PWE3性能告警的标记.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
*           dwFlag: 标记值.
* 输出参数: 无
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-06-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwe3PerfAlmFlagSet(BYTE chipId, WORD32 dwFlag)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    g_drv_tdm_pwe3PerfAlmFlag[chipId] = dwFlag;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwe3PerfAlmFlagGet
* 功能描述: 获取PWE3性能告警的标记.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
* 输出参数: *pdwFlag: 保存标记值.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-06-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwe3PerfAlmFlagGet(BYTE chipId, WORD32 *pdwFlag)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pdwFlag);
    
    *pdwFlag = 0;
    *pdwFlag = g_drv_tdm_pwe3PerfAlmFlag[chipId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwe3PerfAlmTidGet
* 功能描述: 获取PWE3性能告警线程的TID.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
* 输出参数: *pTid: 保存TID.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-06-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwe3PerfAlmTidGet(BYTE chipId, int *pTid)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pTid);
    
    *pTid = 0;
    *pTid = g_drv_tdm_pwe3PerfAlmTid[chipId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwe3PerfAlmThreadIdGet
* 功能描述: 获取PWE3性能告警线程的Thread ID.
* 访问的表: 无
* 修改的表: 无
* 输入参数: chipId: 芯片编号,取值为0~3.
* 输出参数: *pThreadId: 保存Thread ID.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-06-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwe3PerfAlmThreadIdGet(BYTE chipId, pthread_t *pThreadId)
{
    DRV_TDM_CHECK_CHIP_ID(chipId);
    DRV_TDM_CHECK_POINTER_IS_NULL(pThreadId);
    
    *pThreadId = 0;
    *pThreadId = g_drv_tdm_pwe3PerfAlmThreadId[chipId];
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwe3PerfAlmMonitor
* 功能描述: 监测PWE3性能告警.
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pSubslotId: 保存STM单板的子槽位号 
* 输出参数: 无
* 返 回 值: 
* 其它说明: PWE3性能告警线程的入口函数,用来监测PWE3性能统计和告警.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-06-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwe3PerfAlmMonitor(VOID *pSubslotId)
{
    WORD32 rv = DRV_TDM_OK;  /* 函数返回码 */
    WORD32 dwTempRv = DRV_TDM_OK;
    BYTE subslotId = 0;
    WORD32 dwBoardStatus = 0;  /* CP3BAN单板的状态 */
    WORD32 initFlag = 0;  /* STM TDM PWE3模块的初始化标记 */
    BYTE chipId = 0;
    WORD16 pwId = 0;
    BYTE pwState = 0;
    WORD32 *pdwSubslotId = NULL;
    DRV_TDM_PW_CNT_INFO pwPktCnt;
    DRV_TDM_PW_ALM_INFO pwAlarm;
    
    pdwSubslotId = (WORD32 *)pSubslotId;
    if (NULL == pdwSubslotId)
    { 
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, pdwSubslotId is NULL pointer.\n", __FILE__, __LINE__); 
        return DRV_TDM_NULL_POINTER; 
    }
    subslotId = (BYTE)(*pdwSubslotId);
    free(pdwSubslotId);
    pdwSubslotId = NULL;  /* 释放内存空间,以防成为野指针 */
    BSP_Print(BSP_DEBUG_ALL, "%s line %d, drv_tdm_pwe3PerfAlmMonitor(), subslotId=%u.\n", __FILE__, __LINE__, subslotId);
    if ((subslotId < DRV_TDM_SUBCARD_ID_START) || (subslotId > DRV_TDM_SUBCARD_ID_END))
    { 
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid subslotId, subslotId=%u.\n", __FILE__, __LINE__, subslotId); 
        return DRV_TDM_INVALID_SUBSLOT_ID; 
    }
    chipId = subslotId - (BYTE)1;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    g_drv_tdm_pwe3PerfAlmTid[chipId] = __gettid();  /* save TID */

    while (DRV_TDM_TRUE)
    {
        rv = BSP_cp3banDeviceSemGet((WORD32)subslotId);  /* Get semaphore. */
        if (BSP_E_CP3BAN_SEM_NONE_EXISTED == rv)  /* 如果信号量被销毁的话,则直接返回成功 */
        {
            BSP_Print(BSP_DEBUG_ALL, "WARNING: %s line %d, g_bsp_cp3ban_device_sem_id[%u] has been destroyed.\n", __FILE__, __LINE__, subslotId);
            return DRV_TDM_OK;
        }
        else if (BSP_E_CP3BAN_OK == rv)  /* 成功的话,接着往下执行 */
        {
            ;
        }
        else  /*其它错误返回值,接着下一次循环 */
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banDeviceSemGet() rv=0x%x.\n", __FILE__, __LINE__, subslotId, rv);
            continue;
        }
        rv = BSP_cp3banBoardStatusGet((WORD32)subslotId, &dwBoardStatus);  /* 获取单板的状态 */
        dwTempRv = BSP_cp3banDeviceSemFree((WORD32)subslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTempRv)
        {
            DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banDeviceSemFree() rv=0x%x.\n", __FILE__, __LINE__, subslotId, dwTempRv);
            continue;
        }
        if (BSP_E_CP3BAN_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banBoardStatusGet() rv=0x%x.\n", __FILE__, __LINE__, subslotId, rv);
            return rv;
        }
        if (BSP_CP3BAN_BOARD_OFFLINE == dwBoardStatus)  /* 单板不在位,直接返回成功. */
        {
            BSP_Print(BSP_DEBUG_ALL, "WARNING: %s line %d, subslotId %u, CP3BAN board is offline.\n", __FILE__, __LINE__, subslotId);
            return DRV_TDM_OK;
        }
        rv = drv_tdm_pwe3InitFlagGet(chipId, &initFlag);  /* 获取STM TDM PWE3模块的初始化标记 */
        if (DRV_TDM_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3InitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
            return rv;
        }
        if (DRV_TDM_STM_BOARD_UNINSTALL != initFlag) /* STM TDM PWE3模块已经初始化 */
        {
            if (DRV_TDM_DISABLE != g_drv_tdm_pwe3PerfAlmFlag[chipId]) /* enable monitor */
            {
                for (pwId = DRV_TDM_PW_ID_MIN; pwId <= DRV_TDM_PW_ID_MAX; pwId++)
                {
                    rv = drv_tdm_pwStateGet(chipId, pwId, &pwState);
                    if (DRV_TDM_OK != rv)
                    {
                        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwStateGet() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
                        continue;
                    }
                    if (DRV_TDM_PW_FREE == pwState)  /* PW is free. */
                    {
                        continue;
                    }

                    memset(&pwPktCnt, 0, sizeof(DRV_TDM_PW_CNT_INFO));
                    rv = BSP_cp3banDeviceSemGet((WORD32)subslotId);  /* Get semaphore. */
                    if (BSP_E_CP3BAN_SEM_NONE_EXISTED == rv)  /* 如果信号量被销毁的话,则直接返回成功 */
                    {
                        BSP_Print(BSP_DEBUG_ALL, "WARNING: %s line %d, g_bsp_cp3ban_device_sem_id[%u] has been destroyed.\n", __FILE__, __LINE__, subslotId);
                        return DRV_TDM_OK;
                    }
                    else if (BSP_E_CP3BAN_OK == rv)  /* 成功的话,接着往下执行 */
                    {
                        ;
                    }
                    else  /*其它错误返回值,接着下一次循环 */
                    {
                        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banDeviceSemGet() rv=0x%x.\n", __FILE__, __LINE__, subslotId, rv);
                        continue;
                    }
                    rv = drv_tdm_pwCntGet(chipId, pwId, &pwPktCnt);
                    dwTempRv = BSP_cp3banDeviceSemFree((WORD32)subslotId);  /* Free semaphore. */
                    if (BSP_E_CP3BAN_OK != dwTempRv)
                    {
                        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banDeviceSemFree() rv=0x%x.\n", __FILE__, __LINE__, subslotId, dwTempRv);
                        continue;
                    }
                    if (BSP_E_CP3BAN_OK != rv)
                    {
                        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, subslotId %u, drv_tdm_pwCntGet() rv=0x%x.\n", __FILE__, __LINE__, subslotId, rv);
                        continue;
                    }
                    rv = drv_tdm_pwe3PktCntSave(chipId, pwId, &pwPktCnt);
                    if (DRV_TDM_OK != rv)
                    {
                        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwe3PktCntSave() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
                        continue;
                    }
                    rv = drv_tdm_pwTotalPktCntSave(chipId, pwId, &pwPktCnt);
                    if (DRV_TDM_OK != rv)
                    {
                        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwTotalPktCntSave() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
                        continue;
                    }
                    BSP_DelayMs(g_drv_tdm_pwe3PerfAlmDelay);  /* delay ms. */
                    
                    memset(&pwAlarm, 0, sizeof(DRV_TDM_PW_ALM_INFO));
                    rv = BSP_cp3banDeviceSemGet((WORD32)subslotId);  /* Get semaphore. */
                    if (BSP_E_CP3BAN_SEM_NONE_EXISTED == rv)  /* 如果信号量被销毁的话,则直接返回成功 */
                    {
                        BSP_Print(BSP_DEBUG_ALL, "WARNING: %s line %d, g_bsp_cp3ban_device_sem_id[%u] has been destroyed.\n", __FILE__, __LINE__, subslotId);
                        return DRV_TDM_OK;
                    }
                    else if (BSP_E_CP3BAN_OK == rv)  /* 成功的话,接着往下执行 */
                    {
                        ;
                    }
                    else  /*其它错误返回值,接着下一次循环 */
                    {
                        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_ALM_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banDeviceSemGet() rv=0x%x.\n", __FILE__, __LINE__, subslotId, rv);
                        continue;
                    }
                    rv = drv_tdm_pwAlarmGet(chipId, pwId, &pwAlarm);
                    dwTempRv = BSP_cp3banDeviceSemFree((WORD32)subslotId);  /* Free semaphore. */
                    if (BSP_E_CP3BAN_OK != dwTempRv)
                    {
                        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_ALM_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banDeviceSemFree() rv=0x%x.\n", __FILE__, __LINE__, subslotId, dwTempRv);
                        continue;
                    }
                    if (BSP_E_CP3BAN_OK != rv)
                    {
                        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_ALM_MSG, "ERROR: %s line %d, subslotId %u, drv_tdm_pwAlarmGet() rv=0x%x.\n", __FILE__, __LINE__, subslotId, rv);
                        continue;
                    }
                    rv = drv_tdm_pwAlarmSave(chipId, pwId, &pwAlarm);
                    if (DRV_TDM_OK != rv)
                    {
                        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_ALM_MSG, "ERROR: %s line %d, chipId %u, drv_tdm_pwAlarmSave() rv=0x%x.\n", __FILE__, __LINE__, chipId, rv);
                        continue;
                    }
                    BSP_DelayMs(g_drv_tdm_pwe3PerfAlmDelay);  /* delay ms. */
                }
                BSP_DelayMs(1000);  /* delay 1000ms. */
            }
            else  /* disable monitor sdh alarm */
            {
                BSP_DelayMs(1000);  /* delay 1000ms. */
            }
        }
        else /* 初始化未完成 */
        {
            BSP_DelayMs(1000);  /* delay 1000ms. */
        }
    }
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwe3PerfAlmThreadCreate
* 功能描述: 创建监测PWE3性能告警的线程.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号,取值为1~4. 
* 输出参数: 无
* 返 回 值: 
* 其它说明: 该线程用来监测PWE3性能统计和告警.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-06-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwe3PerfAlmThreadCreate(BYTE subslotId)
{
    int rv = DRV_TDM_OK;
    BYTE chipId = 0;
    WORD32 *pdwSubslotId = NULL;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);  
    chipId = subslotId - (BYTE)1;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    pdwSubslotId = (WORD32 *)(malloc(sizeof(WORD32)));
    if (NULL == pdwSubslotId)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, allocate memory for pdwSubslotId failed.\n", __FILE__, __LINE__, subslotId);
        return DRV_TDM_MEM_ALLOC_FAIL;
    }
    memset(pdwSubslotId, 0, sizeof(WORD32));  /* 先清零再赋值 */
    *pdwSubslotId = (WORD32)subslotId;
    
    init_thread(&(g_drv_tdm_pwe3PerfAlmThread[chipId]), 0, 0, 20, drv_tdm_pwe3PerfAlmMonitor, (VOID *)pdwSubslotId);
    rv = start_mod_thread(&(g_drv_tdm_pwe3PerfAlmThread[chipId]));
    if (0 != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, start_mod_thread() rv=%d.\n", __FILE__, __LINE__, subslotId, rv);
        return DRV_TDM_CREATE_THREAD_FAIL;    
    }
    g_drv_tdm_pwe3PerfAlmThreadId[chipId] = g_drv_tdm_pwe3PerfAlmThread[chipId].pthrid;
    
    BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, subslotId %u, Create thread pwe3PerfAlarm successfully, threadId=0x%x.\n", __FILE__, __LINE__, subslotId, g_drv_tdm_pwe3PerfAlmThread[chipId].pthrid);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwe3PerfAlmThreadDelete
* 功能描述: 删除监测PWE3性能告警的线程.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: STM单板的子槽位号 
* 输出参数: 无
* 返 回 值: 
* 其它说明: 该线程用来监测PWE3性能告警.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-06-21   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwe3PerfAlmThreadDelete(BYTE subslotId)
{
    BYTE chipId = 0;
    
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);  
    chipId = subslotId - (BYTE)1;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    
    stop_mod_thread(&(g_drv_tdm_pwe3PerfAlmThread[chipId]));
    g_drv_tdm_pwe3PerfAlmThreadId[chipId] = g_drv_tdm_pwe3PerfAlmThread[chipId].pthrid;
    g_drv_tdm_pwe3PerfAlmTid[chipId] = 0;
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwAlarmQuery
* 功能描述: 查询PWE3告警
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pPwe3Alarm
* 输出参数: *pPwe3Alarm 
* 返 回 值: 
* 其它说明: 本函数供产品管理调用.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-28   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwAlarmQuery(T_BSP_SRV_FUNC_CHIP_CESALARM_ARG *pPwe3Alarm)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 systemType = 0;   /* ZXUPN15000的系统类型 */
    BYTE subslotId = 0;
    BYTE chipId = 0;
    WORD32 channelNo = 0;
    WORD16 pwId = 0;
    DRV_TDM_PW_ALM_INFO pwAlarm;
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwe3Alarm);
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwe3Alarm->pAlarm);
    systemType = BSP_cp3banSystemTypeGet();  /* 获取ZXUPN15000系统类型. */
    subslotId = (BYTE)(pPwe3Alarm->dwSubSlotId);
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM != systemType)   /* 15K-2系统 */
    {
        subslotId = 1;   /* 对于15K-2系统,子槽位始终取值为1 */
    }
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    channelNo = pPwe3Alarm->dwPWId;
    
    /* 查表,通过channelNo来查找PWID. */
    rv = drv_tdm_pwIdGet(chipId, channelNo, &pwId);
    if (DRV_TDM_OK != rv) 
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_ALM_MSG, "ERROR: %s line %d, subslotId %u channelNo 0x%x, drv_tdm_pwIdGet() rv=0x%x.\n", __FILE__, __LINE__, subslotId, channelNo, rv);
        return rv;
    }

    memset(&pwAlarm, 0, sizeof(DRV_TDM_PW_ALM_INFO));
    rv = drv_tdm_pwe3AlarmGet(chipId, pwId, &pwAlarm); /* 从驱动的软件表中来读取PWE3告警. */
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_ALM_MSG, "ERROR: %s line %d, drv_tdm_pwe3AlarmGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }

    if (0 == pwAlarm.lopsAlm)
    {
        pPwe3Alarm->pAlarm->bPktLos = 0;
    }
    else
    {
        pPwe3Alarm->pAlarm->bPktLos = 1;
    }
    
    if (0 == pwAlarm.lBitAlm)
    {
        pPwe3Alarm->pAlarm->bCSF = 0;
    }
    else
    {
        pPwe3Alarm->pAlarm->bCSF = 1;
    }
    
    if (0 == pwAlarm.rBitAlm)
    {
        pPwe3Alarm->pAlarm->bRDI = 0;
    }
    else
    {
        pPwe3Alarm->pAlarm->bRDI = 1;
    }
    
    if (0 == pwAlarm.jitterBufOverrunAlm)
    {
        pPwe3Alarm->pAlarm->bOverRun = 0;
    }
    else
    {
        pPwe3Alarm->pAlarm->bOverRun = 1;
    }

    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_ALM_MSG, "subslotId %u channelNo 0x%x, lopsAlm=%u lBitAlm=%u rBitAlm=%u jitterBufOverrunAlm=%u.\n", 
                        subslotId, channelNo, pPwe3Alarm->pAlarm->bPktLos, pPwe3Alarm->pAlarm->bCSF, 
                        pPwe3Alarm->pAlarm->bRDI, pPwe3Alarm->pAlarm->bOverRun);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwCntPrint
* 功能描述: 打印PWE3报文统计.
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pPwe3Cnt
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 内部调试函数.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-28   V1.0   谢伟生10112265     create    
**************************************************************************/
static WORD32 drv_tdm_pwCntPrint(const T_BSP_SRV_FUNC_CHIP_CESSTATIC_ARG *pPwe3Cnt)
{
    T_BSP_TDM_PW_STATISTICS *pCesStatic = NULL;
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwe3Cnt);
    pCesStatic = (T_BSP_TDM_PW_STATISTICS *)(pPwe3Cnt->pStatic);
    DRV_TDM_CHECK_POINTER_IS_NULL(pCesStatic);
    
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "[subslotId %u channelNo 0x%x]'s PWE3 counter information:\n", pPwe3Cnt->dwSubSlotId, pPwe3Cnt->dwPWId);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->RxTDMByt.dwFlag : %u\n", pCesStatic->RxTDMByt.dwFlag);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->RxTDMByt.dwLow : %u\n", pCesStatic->RxTDMByt.dwLow);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->RxTDMByt.dwHigh : %u\n", pCesStatic->RxTDMByt.dwHigh);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->RxTDMPkt.dwFlag : %u", pCesStatic->RxTDMPkt.dwFlag);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->RxTDMPkt.dwLow : %u\n", pCesStatic->RxTDMPkt.dwLow);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->RxTDMPkt.dwHigh : %u\n", pCesStatic->RxTDMPkt.dwHigh);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->RxFwdPkt.dwFlag : %u\n", pCesStatic->RxFwdPkt.dwFlag);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->RxFwdPkt.dwLow : %u\n", pCesStatic->RxFwdPkt.dwLow);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->RxFwdPkt.dwHigh : %u\n", pCesStatic->RxFwdPkt.dwHigh);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->NearSendByt.dwFlag : %u\n", pCesStatic->NearSendByt.dwFlag);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->NearSendByt.dwLow : %u\n", pCesStatic->NearSendByt.dwLow);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->NearSendByt.dwHigh : %u\n", pCesStatic->NearSendByt.dwHigh);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->NearSendPkt.dwFlag : %u\n", pCesStatic->NearSendPkt.dwFlag);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->NearSendPkt.dwLow : %u\n", pCesStatic->NearSendPkt.dwLow);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->NearSendPkt.dwHigh : %u\n", pCesStatic->NearSendPkt.dwHigh);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->NearRecvPkt.dwFlag : %u\n", pCesStatic->NearRecvPkt.dwFlag);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->NearRecvPkt.dwLow : %u\n", pCesStatic->NearRecvPkt.dwLow);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->NearRecvPkt.dwHigh : %u\n", pCesStatic->NearRecvPkt.dwHigh);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->MalformPkt.dwFlag : %u\n", pCesStatic->MalformPkt.dwFlag);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->MalformPkt.dwLow : %u\n", pCesStatic->MalformPkt.dwLow);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->MalformPkt.dwHigh : %u\n", pCesStatic->MalformPkt.dwHigh);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->BufOverCont.dwFlag : %u\n", pCesStatic->BufOverCont.dwFlag);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->BufOverCont.dwLow : %u\n", pCesStatic->BufOverCont.dwLow);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->BufOverCont.dwHigh : %u\n", pCesStatic->BufOverCont.dwHigh);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->NearLosPkt.dwFlag : %u\n", pCesStatic->NearLosPkt.dwFlag);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->NearLosPkt.dwLow : %u\n", pCesStatic->NearLosPkt.dwLow);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->NearLosPkt.dwHigh : %u\n", pCesStatic->NearLosPkt.dwHigh);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->FarLosPkt.dwFlag : %u\n", pCesStatic->FarLosPkt.dwFlag);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->FarLosPkt.dwLow : %u\n", pCesStatic->FarLosPkt.dwLow);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->FarLosPkt.dwHigh : %u\n", pCesStatic->FarLosPkt.dwHigh);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->RdiPkt.dwFlag : %u\n", pCesStatic->RdiPkt.dwFlag);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->RdiPkt.dwLow : %u\n", pCesStatic->RdiPkt.dwLow);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->RdiPkt.dwHigh : %u\n", pCesStatic->RdiPkt.dwHigh);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->CsfPkt.dwFlag : %u\n", pCesStatic->CsfPkt.dwFlag);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->CsfPkt.dwLow : %u\n", pCesStatic->CsfPkt.dwLow);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->CsfPkt.dwHigh : %u\n", pCesStatic->CsfPkt.dwHigh);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->SNErrPkt.dwFlag : %u\n", pCesStatic->SNErrPkt.dwFlag);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->SNErrPkt.dwLow : %u\n", pCesStatic->SNErrPkt.dwLow);
    DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "pCesStatic->SNErrPkt.dwHigh : %u\n", pCesStatic->SNErrPkt.dwHigh);
    
    return DRV_TDM_OK;
}


/**************************************************************************
* 函数名称: drv_tdm_pwCntQuery
* 功能描述: 查询PWE3性能统计
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pPwe3Cnt
* 输出参数: *pPwe3Cnt
* 返 回 值: 
* 其它说明: 本函数供产品管理调用,产品管理每隔10秒钟来查询一次PWE3报文统计.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-28   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 drv_tdm_pwCntQuery(T_BSP_SRV_FUNC_CHIP_CESSTATIC_ARG *pPwe3Cnt)
{
    WORD32 rv = DRV_TDM_OK;
    WORD32 systemType = 0;   /* ZXUPN15000的系统类型 */
    BYTE subslotId = 0;
    BYTE chipId = 0;
    WORD32 channelNo = 0;
    WORD16 pwId = 0;
    T_BSP_TDM_PW_STATISTICS *pCesStatic = NULL;
    DRV_TDM_PW_CNT_INFO pwCount;
    
    DRV_TDM_CHECK_POINTER_IS_NULL(pPwe3Cnt);
    pCesStatic = (T_BSP_TDM_PW_STATISTICS *)(pPwe3Cnt->pStatic);
    DRV_TDM_CHECK_POINTER_IS_NULL(pCesStatic);
    systemType = BSP_cp3banSystemTypeGet();  /* 获取ZXUPN15000系统类型. */
    subslotId = (BYTE)(pPwe3Cnt->dwSubSlotId);
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM != systemType)   /* 15K-2系统 */
    {
        subslotId = 1;   /* 对于15K-2系统,子槽位始终取值为1 */
    }
    DRV_TDM_CHECK_STM_SUBSLOT_ID(subslotId);
    chipId = subslotId - (BYTE)1;
    DRV_TDM_CHECK_CHIP_ID(chipId);
    channelNo = pPwe3Cnt->dwPWId;
    
    /* 查表,通过channelNo来查找PWID. */
    rv = drv_tdm_pwIdGet(chipId, channelNo, &pwId);
    if (DRV_TDM_OK != rv) 
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, subslotId %u channelNo 0x%x, drv_tdm_pwIdGet() rv=0x%x.\n", __FILE__, __LINE__, subslotId, channelNo, rv);
        return rv;
    }
    
    memset(&pwCount, 0, sizeof(DRV_TDM_PW_CNT_INFO));
    rv = drv_tdm_pwTotalPktCntGet(chipId, pwId, &pwCount);
    if (DRV_TDM_OK != rv)
    {
        DRV_TDM_DEBUG_TRACE(DRV_TDM_DBG_PW_CNT_MSG, "ERROR: %s line %d, drv_tdm_pwTotalPktCntGet() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    
    memset(pCesStatic, 0, sizeof(T_BSP_TDM_PW_STATISTICS));  /* 先清零再赋值 */
    pCesStatic->RxTDMByt.dwFlag = 64;  /* 64位位宽 */
    pCesStatic->RxTDMByt.dwLow = (WORD32)(pwCount.txPayloadBytes & (WORD64)0x00000000ffffffff);
    pCesStatic->RxTDMByt.dwHigh = (WORD32)((pwCount.txPayloadBytes & (WORD64)0xffffffff00000000) >> 32);
    pCesStatic->RxTDMPkt.dwFlag = 64;  /* 64位位宽 */
    pCesStatic->RxTDMPkt.dwLow = (WORD32)(pwCount.txPkts & (WORD64)0x00000000ffffffff);
    pCesStatic->RxTDMPkt.dwHigh = (WORD32)((pwCount.txPkts & (WORD64)0xffffffff00000000) >> 32);
    pCesStatic->RxFwdPkt.dwFlag = 64;  /* 64位位宽 */
    pCesStatic->RxFwdPkt.dwLow = (WORD32)(pwCount.txPkts & (WORD64)0x00000000ffffffff);
    pCesStatic->RxFwdPkt.dwHigh = (WORD32)((pwCount.txPkts & (WORD64)0xffffffff00000000) >> 32);
    pCesStatic->NearSendByt.dwFlag = 64;  /* 64位位宽 */
    pCesStatic->NearSendByt.dwLow = (WORD32)(pwCount.rxPayloadBytes & (WORD64)0x00000000ffffffff);
    pCesStatic->NearSendByt.dwHigh = (WORD32)((pwCount.rxPayloadBytes & (WORD64)0xffffffff00000000) >> 32);
    pCesStatic->NearSendPkt.dwFlag = 64;  /* 64位位宽 */
    pCesStatic->NearSendPkt.dwLow = (WORD32)(pwCount.rxPkts & (WORD64)0x00000000ffffffff);
    pCesStatic->NearSendPkt.dwHigh = (WORD32)((pwCount.rxPkts & (WORD64)0xffffffff00000000) >> 32);
    pCesStatic->NearRecvPkt.dwFlag = 64;  /* 64位位宽 */
    pCesStatic->NearRecvPkt.dwLow = (WORD32)(pwCount.rxPkts & (WORD64)0x00000000ffffffff);
    pCesStatic->NearRecvPkt.dwHigh = (WORD32)((pwCount.rxPkts & (WORD64)0xffffffff00000000) >> 32);
    pCesStatic->MalformPkt.dwFlag = 64;  /* 64位位宽 */
    pCesStatic->MalformPkt.dwLow = (WORD32)(pwCount.rxMalformedPkts & (WORD64)0x00000000ffffffff);
    pCesStatic->MalformPkt.dwHigh = (WORD32)((pwCount.rxMalformedPkts & (WORD64)0xffffffff00000000) >> 32);
    pCesStatic->BufOverCont.dwFlag = 64;  /* 64位位宽 */
    pCesStatic->BufOverCont.dwLow = (WORD32)(pwCount.rxJitBufOverrunPkts & (WORD64)0x00000000ffffffff);
    pCesStatic->BufOverCont.dwHigh = (WORD32)((pwCount.rxJitBufOverrunPkts & (WORD64)0xffffffff00000000) >> 32);
    pCesStatic->NearLosPkt.dwFlag = 64;  /* 64位位宽 */
    pCesStatic->NearLosPkt.dwLow = (WORD32)(pwCount.rxDiscardedPkts & (WORD64)0x00000000ffffffff);
    pCesStatic->NearLosPkt.dwHigh = (WORD32)((pwCount.rxDiscardedPkts & (WORD64)0xffffffff00000000) >> 32);
    pCesStatic->FarLosPkt.dwFlag = 64;  /* 64位位宽 */
    pCesStatic->FarLosPkt.dwLow = (WORD32)(pwCount.rxRbitPkts & (WORD64)0x00000000ffffffff);
    pCesStatic->FarLosPkt.dwHigh = (WORD32)((pwCount.rxRbitPkts & (WORD64)0xffffffff00000000) >> 32);
    pCesStatic->RdiPkt.dwFlag = 64;  /* 64位位宽 */
    pCesStatic->RdiPkt.dwLow = (WORD32)(pwCount.rxRbitPkts & (WORD64)0x00000000ffffffff);
    pCesStatic->RdiPkt.dwHigh = (WORD32)((pwCount.rxRbitPkts & (WORD64)0xffffffff00000000) >> 32);
    pCesStatic->CsfPkt.dwFlag = 64;  /* 64位位宽 */
    pCesStatic->CsfPkt.dwLow = (WORD32)(pwCount.rxLbitPkts & (WORD64)0x00000000ffffffff);
    pCesStatic->CsfPkt.dwHigh = (WORD32)((pwCount.rxLbitPkts & (WORD64)0xffffffff00000000) >> 32);
    pCesStatic->SNErrPkt.dwFlag = 64;  /* 64位位宽 */
    pCesStatic->SNErrPkt.dwLow = (WORD32)(pwCount.rxOutofSeqDropPkts & (WORD64)0x00000000ffffffff);
    pCesStatic->SNErrPkt.dwHigh = (WORD32)((pwCount.rxOutofSeqDropPkts & (WORD64)0xffffffff00000000) >> 32);
    
    drv_tdm_pwCntPrint(pPwe3Cnt);
    
    return DRV_TDM_OK;
}



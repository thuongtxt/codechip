/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_pwe3_main.c
* 文件标识: 
* 其它说明: 驱动PWE3模块的公共接口函数.
* 当前版本: V1.0
* 作    者: 谢伟生10112265.
* 完成日期: 2013-08-16
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/


#include "bsp.h"
#include "bsp_oal_api.h"
#include "drv_pwe3_main.h"


/* 全局变量的定义 */
static WORD32 g_drv_pwe3_initSemaphoreId = 0;  /* 信号量ID.该信号量用来保护接口卡的初始化 */
static WORD32 g_drv_pwe3_cardCntSemaphoreId = 0;  /* 信号量ID.该信号量用来保护全局变量g_drv_pwe3_subcard_cnt */
static WORD32 g_drv_pwe3_subcard_cnt = 0;  /* 线卡上的接口卡的数量 */


/**************************************************************************
* 函数名称: drv_pwe3_initSemaphoreIdGet
* 功能描述: 获取信号量ID.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 无.
* 输出参数: 无.
* 返 回 值: 信号量ID.
* 其它说明: 该信号量用来保护接口卡的初始化 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-11   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_pwe3_initSemaphoreIdGet(VOID)
{
    return g_drv_pwe3_initSemaphoreId;
}


/**************************************************************************
* 函数名称: drv_pwe3_initSemaphoreCreate
* 功能描述: 创建信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: 无.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护接口卡的初始化 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-11   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_pwe3_initSemaphoreCreate(VOID)
{
    WORD32 rv = DRV_PWE3_OK;
    
    if (0 == g_drv_pwe3_initSemaphoreId)
    {
        rv = BSP_OalCreateSem(1, MUTEX_STYLE, &g_drv_pwe3_initSemaphoreId);
        if (BSP_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, BSP_OalCreateSem() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return DRV_PWE3_CREATE_SEM_FAIL;
        }  
    }
    
    return DRV_PWE3_OK;  /* 返回成功 */
}


/**************************************************************************
* 函数名称: drv_pwe3_initSemaphoreCreate
* 功能描述: 销毁信号量.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 无.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护接口卡的初始化 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-11   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_pwe3_initSemaphoreDestroy(VOID)
{
    WORD32 rv = DRV_PWE3_OK;
    
    if (0 != g_drv_pwe3_initSemaphoreId)
    {
        rv = BSP_OalDestroySem(g_drv_pwe3_initSemaphoreId);
        if (BSP_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, BSP_OalDestroySem() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return DRV_PWE3_DESTROY_SEM_FAIL;
        }
        g_drv_pwe3_initSemaphoreId = 0;
    }
    
    return DRV_PWE3_OK;
}


/**************************************************************************
* 函数名称: drv_pwe3_initSemaphoreGet
* 功能描述: 获取信号量.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 无.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护接口卡的初始化 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-11   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_pwe3_initSemaphoreGet(VOID)
{
    WORD32 rv = DRV_PWE3_OK;
    
    rv = BSP_OalSemP(g_drv_pwe3_initSemaphoreId, WAIT_FOREVER);
    if (BSP_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, BSP_OalSemP() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_PWE3_GET_SEM_FAIL;
    }
    
    return DRV_PWE3_OK;  /* 返回成功 */
}


/**************************************************************************
* 函数名称: drv_pwe3_initSemaphoreFree
* 功能描述: 释放信号量.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 无.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护接口卡的初始化 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-11   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_pwe3_initSemaphoreFree(VOID)
{
    WORD32 rv = DRV_PWE3_OK;
    
    rv = BSP_OalSemV(g_drv_pwe3_initSemaphoreId);
    if (BSP_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, BSP_OalSemV() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_PWE3_FREE_SEM_FAIL;
    }
    
    return DRV_PWE3_OK;  /* 返回成功 */
}


/**************************************************************************
* 函数名称: drv_pwe3_cardCntSemaphoreIdGet
* 功能描述: 获取信号量ID.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 无.
* 输出参数: 无.
* 返 回 值: 信号量ID.
* 其它说明: 该信号量用来保护全局变量g_drv_pwe3_subcard_cnt.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-11   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_pwe3_cardCntSemaphoreIdGet(VOID)
{
    return g_drv_pwe3_cardCntSemaphoreId;
}


/**************************************************************************
* 函数名称: drv_pwe3_cardCntSemaphoreCreate
* 功能描述: 创建信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: 无.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护全局变量g_drv_pwe3_subcard_cnt.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-11   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_pwe3_cardCntSemaphoreCreate(VOID)
{
    WORD32 rv = DRV_PWE3_OK;
    
    if (0 == g_drv_pwe3_cardCntSemaphoreId)
    {
        rv = BSP_OalCreateSem(1, MUTEX_STYLE, &g_drv_pwe3_cardCntSemaphoreId);
        if (BSP_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, BSP_OalCreateSem() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return DRV_PWE3_CREATE_SEM_FAIL;
        }  
    }
    
    return DRV_PWE3_OK;  /* 返回成功 */
}


/**************************************************************************
* 函数名称: drv_pwe3_cardCntSemaphoreDestroy
* 功能描述: 销毁信号量.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 无.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护全局变量g_drv_pwe3_subcard_cnt. 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-11   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_pwe3_cardCntSemaphoreDestroy(VOID)
{
    WORD32 rv = DRV_PWE3_OK;
    
    if (0 != g_drv_pwe3_cardCntSemaphoreId)
    {
        rv = BSP_OalDestroySem(g_drv_pwe3_cardCntSemaphoreId);
        if (BSP_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, BSP_OalDestroySem() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return DRV_PWE3_DESTROY_SEM_FAIL;
        }
        g_drv_pwe3_cardCntSemaphoreId = 0;
    }
    
    return DRV_PWE3_OK;
}


/**************************************************************************
* 函数名称: drv_pwe3_cardCntSemaphoreGet
* 功能描述: 获取信号量.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 无.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护全局变量g_drv_pwe3_subcard_cnt. 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-11   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_pwe3_cardCntSemaphoreGet(VOID)
{
    WORD32 rv = DRV_PWE3_OK;
    
    rv = BSP_OalSemP(g_drv_pwe3_cardCntSemaphoreId, WAIT_FOREVER);
    if (BSP_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, BSP_OalSemP() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_PWE3_GET_SEM_FAIL;
    }
    
    return DRV_PWE3_OK;  /* 返回成功 */
}


/**************************************************************************
* 函数名称: drv_pwe3_cardCntSemaphoreFree
* 功能描述: 释放信号量.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 无.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护全局变量g_drv_pwe3_subcard_cnt.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-10-11   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_pwe3_cardCntSemaphoreFree(VOID)
{
    WORD32 rv = DRV_PWE3_OK;
    
    rv = BSP_OalSemV(g_drv_pwe3_cardCntSemaphoreId);
    if (BSP_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, BSP_OalSemV() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return DRV_PWE3_FREE_SEM_FAIL;
    }
    
    return DRV_PWE3_OK;  /* 返回成功 */
}


/**************************************************************************
* 函数名称: drv_pwe3_subcardCntSet
* 功能描述: 设置窄带子卡的数量.
* 访问的表: 无
* 修改的表: 无
* 输入参数: opType: 操作类型,具体参见DRV_TDM_SUBCARD_OP_TYPE定义.
* 输出参数: 无.
* 返 回 值: 无.
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-16   V1.0  谢伟生10112265     create    
**************************************************************************/
VOID drv_pwe3_subcardCntSet(WORD32 opType)
{
    if (DRV_TDM_SUBCARD_DECREASE == opType)  /* decrease */
    {
        if (g_drv_pwe3_subcard_cnt > 0)
        {
            g_drv_pwe3_subcard_cnt--;
        }
    }
    else  /* increase */
    {
        g_drv_pwe3_subcard_cnt++;
    }
    
    return;
}


/**************************************************************************
* 函数名称: drv_pwe3_subcardCntGet
* 功能描述: 获取窄带子卡的数量.
* 访问的表: 无
* 修改的表: 无
* 输入参数: 无.
* 输出参数: 无.
* 返 回 值: 窄带子卡的数量.
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-16   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 drv_pwe3_subcardCntGet(VOID)
{
    return g_drv_pwe3_subcard_cnt;
}



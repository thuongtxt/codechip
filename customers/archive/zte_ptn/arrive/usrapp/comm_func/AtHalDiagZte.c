/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HAL
 *
 * File        : AtHalDiagZte.c
 *
 * Created Date: Apr 08, 2013
 *
 * Description : Diagnostic HAL for ZTE_PTN project. This source file is used to
 *               diagnostic hardware read/write. Usage
 *               - Set base address: AtHalDiagBaseAddressSet(baseAddress)
 *               - Read: AtHalDiagRead(address)
 *               - Write: AtHalDiagWrite(address, value)
 *
 *               Example:
 *               AtHalDiagBaseAddressSet(0x3200000)
 *               AtHalDiagRead(0x0)
 *               AtHalDiagWrite(0x40000, 0x2)
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <stdio.h>
#include <Pub_TypeDef.h>

/*--------------------------- Define -----------------------------------------*/
/* Indirect registers */
#define cIndirectControl1 0x702
#define cIndirectControl2 0x703
#define cIndirectData1    0x704
#define cIndirectData2    0x705
#define cInvalidReadValue 0xCAFECAFE

/* Bit fields */
#define cBit23_0       0x00FFFFFFL
#define cBit15_0       0x0000FFFFL
#define cBit15         0x00008000L
#define cBit14         0x00004000L
#define cBit23_16      0x00FF0000L
#define cBit7_0        0x000000FFL
#define cBit31_16      0xFFFF0000L

/*--------------------------- Macros -----------------------------------------*/
#define mRealAddress(localAddress, baseAddress) (((localAddress) << 1) + baseAddress)

#ifdef __ARCH_POWERPC__
#define mAsm(asmCmd) __asm__(asmCmd)
#else
#define mAsm(asmCmd)
#endif

#define mFieldGet( regVal , mask , shift , type, pOutVal )                     \
    do                                                                         \
        {                                                                      \
        *(pOutVal) = (type)( (WORD32)( (regVal) & (mask) ) >> ((WORD32)(shift)) ); \
        } while(0)

#define mFieldIns( pRegVal , mask , shift , inVal )                            \
    do                                                                         \
        {                                                                      \
        *(pRegVal) =  (WORD32)( (WORD32)( ( *(pRegVal) ) & ( ~( mask ) ) )       \
                        | ( ( (WORD32)( inVal ) << ((WORD32)(shift)) )  & ( mask ) ) \
                      );                                                       \
        }while(0)



/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static WORD32 m_baseAddress = 0x080d1000;  /* This base addres is of software after converting from EPLD */
static WORD32 m_retry = 500000;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static BYTE IsDirectRegister(WORD32 address)
    {
    return ((address >= 0xFFF00) && (address <= 0xFFFFF));
    }

static WORD32 BaseAddress()
    {
    return m_baseAddress;
    }

static WORD32 RetryTime()
    {
    return m_retry;
    }

static WORD16 _Read(WORD32 address)
{
    WORD16 value;
    
    value = (WORD16)(*((volatile WORD16 *)mRealAddress(address, BaseAddress())));
    mAsm("sync");
    
    return value;
}

static void _Write(WORD32 address, volatile WORD16 value)
{
    *((volatile WORD16 *)mRealAddress(address, BaseAddress())) = value;
    mAsm("sync");
}

void AtHalDiagRetryTimeSet(WORD32 retryTime)
    {
    m_retry = retryTime;
    }

void AtHalDiagBaseAddressSet(WORD32 baseAddress)
    {
    m_baseAddress = baseAddress;
    }

WORD32 rdd(WORD32 address)
    {
    WORD16 regVal = 0, tmpValue;
    WORD32 data = 0;
    WORD32 retryCount = 0;

    /* Read a control register */
    address = address & cBit23_0;
    if (IsDirectRegister(address))
        return _Read(address);

    /* Make read request */
    _Write(cIndirectControl1, address & cBit15_0);
    regVal |= cBit15;
    regVal |= cBit14;
    mFieldGet(address, cBit23_16, 16, WORD16, &tmpValue);
    mFieldIns(&regVal, cBit7_0, 0, tmpValue);
    _Write(cIndirectControl2, regVal);

    /* Wait for hardware ready */
    while (retryCount < RetryTime())
        {
        /* If hardware is ready, return data */
        regVal = _Read(cIndirectControl2);
        if ((regVal & cBit15) == 0)
            {
            regVal = _Read(cIndirectData1);
            data |= (regVal & cBit15_0);
            mFieldIns(&data, cBit31_16, 16, _Read(cIndirectData2));

            return data;
            }

        /* Or retry */
        retryCount = retryCount + 1;
        }

    /* Timeout */
    printf("ERROR: Read 0x%08x timeout\n", address);

    return cInvalidReadValue;
    }

void wrd(WORD32 address, WORD32 value)
    {
    WORD16 tmpValue;
    WORD16 regValue = 0;
    WORD32 retryCount = 0;

    /* _Write data */
    address = address & cBit23_0;
    if (IsDirectRegister(address))
        {
        _Write(address, value);
        return;
        }

    _Write(cIndirectData1, (value & cBit15_0));
    mFieldGet(value, cBit31_16, 16, WORD16, &tmpValue);
    _Write(cIndirectData2, tmpValue);

    /* _Write address */
    _Write(cIndirectControl1, address & cBit15_0);

    mFieldGet(address, cBit23_16, 16, WORD16, &tmpValue);
    regValue = tmpValue;
    mFieldIns(&regValue, cBit15, 15, 1);
    _Write(cIndirectControl2, regValue);

    /* Check if hardware done */
    while (retryCount < RetryTime())
        {
        regValue = _Read(cIndirectControl2);
        if ((regValue & cBit15) == 0)
            return;

        /* Or retry */
        retryCount = retryCount + 1;
        }

    printf("ERROR: Write 0x%08x, value 0x%08x timeout\n", address, value);
    }



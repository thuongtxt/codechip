/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_at_e1_access.h
* 文件标识: 
* 其它说明: 通用参数
* 当前版本: V1.0
* 作    者: 陈博10140635.
* 完成日期: 2013-04-25
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/

#ifndef _DRV_AT_E1_ACCESS_H_
#define _DRV_AT_E1_ACCESS_H_


#include "drv_at_e1_common.h"



/*函数声明*/
WORD32 ArriveFpga8Read(WORD32 subcardId, WORD16 offsetAddr, WORD16 *value);
WORD32 ArriveFpga8Write(WORD32 subcardId, WORD16 offsetAddr, WORD16 value);
WORD32 ArriveFpga2Read(WORD32 subcardId, WORD32 offsetAddr, WORD32 *value);
WORD32 ArriveFpga2Write(WORD32 subcardId, WORD32 offsetAddr, WORD32 value);



#endif



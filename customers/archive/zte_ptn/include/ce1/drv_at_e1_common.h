/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_at_e1_common.h
* 文件标识: 
* 其它说明: 通用参数
* 当前版本: V1.0
* 作    者: 陈博10140635.
* 完成日期: 2013-04-25
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/

#ifndef _DRV_AT_E1_COMMON_H_
#define _DRV_AT_E1_COMMON_H_



#include"stdio.h"
#include"stdlib.h"
#include"string.h"
#include"bsp.h"
#include"../../../../../../common/Pub_TypeDef.h"
#include"bsp_mml.h"
#include"bsp_oal_api.h"
#include"eipmod.h"
#include"modthread.h"
#include"bsp_sdh_common.h"
#include"bsp_srv_pwe3.h"




/*定义数据类型*/


extern WORD32 g_at_print ;


/***通用参数***/
#define CE1_FALSE (BYTE)0
#define CE1_TURE (BYTE)1
#define CE1_DISABLE (BYTE)0
#define CE1_ENABLE (BYTE)1

#define  MONITOR_REAL_TIME   0
#define  MONITOR_PERFORMANCE 1
#define  CES_STATIC_SEND_TO_PM 2
#define  PDH_ERROR_STATIC_SEND_TO_PM 3

/***器件参数***/
/*card*/
#define  AT_MAX_SUBCARD_NUM_PER_SLOT  4 /*一块窄带业务单板最多可支持的子卡数*/
#define  AT_MIN_SUBCARD_NUM_PER_SLOT  1
/*LIU*/
#define  AT_LIU_NUM   3
#define  AT_PDH_CHAN_NUM_PER_LIU  8

/*e1*/
#define  AT_E1_INTF_NUM    24
#define  AT_E1_START_ID    0
#define  AT_E1_END_ID      23

/*eth*/
#define  AT_ETH_PORT_NUM       1
#define  AT_ETH_TX_IPG     (BYTE)12 
#define  AT_ETH_RX_IPG     (BYTE)4 

/*pw & mlppp*/
#define  AT_MAX_PW_NUM_PER_SUBCARD    128
#define  AT_MAX_PPP_NUM_PER_SUBCARD   128   /*待定，向at确认*/
#define  AT_MAX_ENCAP_CHAN_NUM_PER_SUBCARD     128
#define  AT_MAX_ETH_FLOW_NUM_PER_SUBCARD         256
#define  AT_MAX_MLPPP_BUNDLE_NUM_PER_SUBCARD  16
#define  AT_MAX_PPP_LINK_NUM_PER_MLPPP_BUNDLE  32




/* 调试信息类型 */
#define ROSNG_TRACE_DEBUG        printf

/*这两个开关是普通开关，用于常用的参数打印*/
#define DRV_AT_COMMON_PRINT      0x00000001       /*参数打印*/
#define DRV_AT_ERROR_PRINT       0x00000002      /*错误打印*/

/*下面这些开关用于一些对外接口和轮询线程*/
#define DRV_AT_E1_ALM_QUERY                0x00000010
#define DRV_AT_E1_ERR_QUERY                 0x00000020
#define DRV_AT_CES_STATIC_QUERY          0x00000040
#define DRV_AT_CES_ALARM_QUERY          0x00000080
#define DRV_AT_E1_ALM_THREAD              0x00000100
#define DRV_AT_E1_ERR_THREAD               0x00000200
#define DRV_AT_PW_COUNT_THREAD         0x00000400

#define DRV_AT_LIU_LOOP_SET         0x00001000
#define DRV_AT_E1_FRAME_SET        0x00002000
#define DRV_AT_PORT_CTRL              0x00004000
#define DRV_AT_LINECODE_SET         0x00008000
#define DRV_AT_E1_CLK_SET             0x00010000
#define DRV_AT_LINECLK_RECV           0x00020000
#define DRV_AT_PW_CFG                      0x00040000
#define DRV_AT_CLK_DOMAIN_ALM_GET         0x00080000
#define DRV_AT_CLK_DOMAIN_SWITCH          0x00100000
#define DRV_AT_PPP_CFG                       0x00200000
#define DRV_AT_MLPPP_CFG                   0x00400000

#define DRV_AT_ALL_PRINT                     0xFFFFFFFF       

/* 函数的返回码 */
typedef enum
{ 
    /*COMMON*/
    DRV_AT_SUCCESS = 0,                    /*0*/      
    DRV_AT_INVALID_CHIP_ID,
    DRV_AT_INVALID_SUBCARD_ID,
    DRV_AT_INVALID_E1PORT_ID,
    DRV_AT_INVALID_E1LINK_ID,
    DRV_AT_INVALID_ETHPORT_ID,
    DRV_AT_INVALID_PW_ID,
    DRV_AT_INVALID_PPP_LINK_NO,
    DRV_AT_INVALID_MLPPP_BUNDLE_INDEX,
    DRV_AT_INVALID_CESOP_LINK_ID,
    DRV_AT_NULL_POINTER ,
    DRV_AT_INVALID_FUNC_NO,
    DRV_AT_INVALID_OPTION,
    
    
    /*DEV INIT & REMOVE*/ 
    DRV_AT_FPGA_NOT_DOWNLOAD = 15,
    DRV_AT_BASE_ADDR_GET_ERROR,
    DRV_AT_HAL_CREATE_FAILED,
    DRV_AT_HAL_LIST_CREATE_FAILED,
    DRV_AT_CORE_GET_FAILED,
    DRV_AT_HAL_SET_FAILED,
    DRV_AT_HAL_GET_FAILED,
    DRV_AT_DEV_CREATE_ERROR,
    DRV_AT_DEV_HANDLE_GET_FAILED,
    DRV_AT_HAL_LIST_ADD_FAILED,
    DRV_AT_FPGA_IMAGE_NONE,

    /* READ& WRITE*/
    DRV_AT_FPGA_READ_FAILED = 30,
    DRV_AT_FPGA_WRITE_FAILED,
    DRV_LIU_READ_FAILED,
    DRV_LIU_WRITE_FAILED,
    
    /*THREAD */
    DRV_PDH_ALM_THR_LAUNCH_FAILED = 40,
    DRV_PDH_ERR_THR_LAUNCH_FAILED,
    DRV_PW_COUNT_THR_LAUNCH_FAILED,
    DRV_PW_ALARM_THR_LAUNCH_FAILED,

    /**PDH &LIU*/
    DRV_LIU_LINECODE_NOT_SUPPORT = 50,
    DRV_AT_PDH_FRAME_SET_ERROR,
    DRV_AT_PDH_FRAME_GET_ERROR,
    DRV_AT_PDH_NOT_BIND_PW,
    DRV_AT_PDH_TIMING_SET_ERROR,
    DRV_AT_PDH_TIMING_GET_ERROR,
    DRV_AT_PDH_TIMING_SRC_GET_ERROR,
    DRV_AT_PDH_LINECODE_SET_ERROR,
    DRV_AT_PDH_LINECODE_GET_ERROR,
    DRV_AT_PDH_SIGNAL_MASK_SET_ERROR,
    DRV_AT_PDH_SIGNAL_ENABLE_ERROR,   
    DRV_AT_PDH_GET_OBJ_FAILED, 
    DRV_AT_PDH_GET_MODULE_FAILED, 
    DRV_AT_PDH_CESOP_E1_CREATE_FAILED,
    DRV_AT_PDH_CESOP_E1_GET_FAILED,
    DRV_AT_PDH_CESOP_E1_DELETE_FAILED,
    DRV_AT_PDH_ALARM_NOT_SUPPORT,
    DRV_AT_TX_ALM_INSERT_ON_ERROR,
    DRV_AT_TX_ALM_INSERT_OFF_ERROR,
    DRV_AT_RX_ALM_INSERT_ON_ERROR,
    DRV_AT_RX_ALM_INSERT_OFF_ERROR,
    DRV_AT_TX_ERR_INSERT_ON_ERROR,
    DRV_AT_TX_ERR_INSERT_OFF_ERROR,
    DRV_AT_PDH_LOOPBACK_SET_ERROR,
    DRV_AT_PDH_LOOPBACK_GET_ERROR,
    DRV_AT_PDH_LOOPBACK_MODE_NOT_SUPPORT,

    /*ETH */
    DRV_AT_ETH_MOUDULE_GET_FAILED = 85,   
    DRV_AT_ETH_PORT_GET_FAILED,
    DRV_AT_ETH_SMAC_SET_ERROR,
    DRV_AT_ETH_MAC_CHECK_SET_ERROR,
    DRV_AT_ETH_PORT_TYPE_SET_ERROR,
    DRV_AT_ETH_PORT_SPEED_SET_ERROR,
    DRV_AT_ETH_PORT_CFG_FAILED,
    DRV_AT_ETH_PORT_SWITCH_FAILED,
    DRV_AT_ETH_PORT_RELEASE_FAILED,
    DRV_AT_ETH_SERDES_LOOP_FAILED,
    DRV_AT_ETH_PORT_CRTLR_GET_FAILED,
    DRV_AT_ETH_WORK_MODE_SET_ERROR,
    DRV_AT_ETH_AUTO_NEG_SET_ERROR,
    DRV_AT_ETH_TXIPG_SET_ERROR,
    DRV_AT_ETH_RXIPG_SET_ERROR,
    DRV_AT_ETH_PHP_SET_ERROR,
    DRV_AT_ETH_QUEUE_MODE_SET_ERROR,
    DRV_AT_ETH_FLOW_CREATE_FAILED,
    DRV_AT_ETH_FLOW_DELETE_FAILD,
    DRV_AT_ETH_FLOW_GET_FAILD,
    DRV_AT_ETH_FLOW_EXP_HEADER_SET_FAILD,
    DRV_AT_ETH_FLOW_TX_HEADER_FAILD,
    DRV_AT_ETH_VLAN_DESC_CONSTRUCT_FAILED,
    DRV_AT_ETH_ING_VLAN_ADD_ERROR,
    DRV_AT_ETH_ENG_VLAN_SET_ERROR,
    DRV_AT_ETH_DMAC_SET_ERROR,
    DRV_AT_ETH_LOOPBACK_SET_ERROR,
    DRV_AT_ETH_LOOPBACK_GET_ERROR,
    DRV_AT_ETH_LOOPBACK_MODE_NOT_SUPPORT,
      
    /*PW */
    DRV_AT_PW_ALREADY_EXISTED = 120,
    DRV_AT_PW_NOT_CREATE,
    DRV_AT_PW_ID_GET_FAILED,
    DRV_AT_PW_INFO_GET_FAILED,
    DRV_AT_PW_MODULE_GET_FAILED,
    DRV_AT_PW_OBJ_GET_FAILED,
    DRV_AT_PW_SATOP_ADD_FAILED ,         
    DRV_AT_PW_CESOP_ADD_FAILED,
    DRV_AT_PW_DELETE_FAILED,
    
    DRV_AT_PW_LOP_OCURR_THRES_SET_FAILED,
    DRV_AT_PW_LOP_CLEAR_THRES_SET_FAILED,
    DRV_AT_PW_PAYLOAD_SET_FAILED,
    DRV_AT_PW_PAYLOAD_NOT_SUPPORT,
    DRV_AT_PW_JITBUFF_SIZE_SET_FAILED,
    DRV_AT_PW_JITBUFF_DELAY_SET_FAILED,
    DRV_AT_PW_JITBUFF_NOT_SUPPORT,
    DRV_AT_PW_PRI_SET_FAILED,
    DRV_AT_PW_REORDER_SET_FAILED,   
    DRV_AT_PW_SUPPRESS_SET_FAILED,
    DRV_AT_PW_CW_ENABLE_FAILED,
    DRV_AT_PW_CW_SET_FAILED,
    DRV_AT_PW_AUTO_RBIT_ENABLE_FAILED,
    DRV_AT_PW_SEQ_MODE_SET_FAILED,
    DRV_AT_PW_LEN_MODE_SET_FAILED,    
    DRV_AT_PW_PKT_REP_MODE_SET_FAILED,
    DRV_AT_PW_RTP_SET_FAILED,

    DRV_AT_PW_ETH_PORT_SET_FAILED,
    DRV_AT_PW_ETH_VLAN_SET_FAILED,
    DRV_AT_PW_ETH_VLAN_GET_FAILED,
    DRV_AT_PW_CIRCUIT_BIND_FAILED,
    DRV_AT_PW_CIRCUIT_UNBIND_FAILED,
    DRV_AT_PW_CIRCUIT_ALREADY_BIND,
    DRV_AT_PW_ACTIVE_FAILED,
    DRV_AT_PW_INACTIVE_FAILED,
    DRV_AT_PKT_ANALYZE_MODULE_GET_FAILED,
    DRV_AT_PKT_ANALYZE_OBJ_CREATE_FAILED,

    /*encap chanNo*/
    DRV_AT_ENCAP_MODULE_GET_FAILED =170,
    DRV_AT_PPP_MODULE_GET_FAILED,
    DRV_AT_PPP_CREATE_FAILED,
    DRV_AT_PPP_DELETE_FAILED,
    DRV_AT_PPP_LINK_NOT_CREATE,
    DRV_AT_PPP_LINK_ALREADY_EXISTED,
    DRV_AT_PPP_LINK_PHASE_SET_FAILED,
    DRV_AT_PPP_INFO_GET_FAILED,
    DRV_AT_PPP_LINK_ALREADY_ADD_TO_BUNDLE,
    DRV_AT_PPP_LINK_ALREADY_REMOVE_TO_BUNDLE,
    DRV_AT_PPP_PID_SET_FAILED,
    DRV_AT_MLPPP_PID_SET_FAILED,
    DRV_AT_MLPPP_BUNDLE_CREATE_FAILED,
    DRV_AT_MLPPP_BUNDLE_GET_FAILED,
    DRV_AT_MLPPP_BUNDLE_DELETE_FAILED,
    DRV_AT_MLPPP_BUNDLE_NOT_CREATE,
    DRV_AT_MLPPP_BUNDLE_ADD_LINK_FAILED,
    DRV_AT_MLPPP_BUNDLE_REMOVE_LINK_FAILED,
    DRV_AT_MLPPP_BUNDLE_ETH_FLOW_BIND_FAILED,
    DRV_AT_MLPPP_BUNDLE_ETH_FLOW_UNBIND_FAILED,
    DRV_AT_MLPPP_BUNDLE_ALREADY_EXISTED,
    DRV_AT_MLPPP_BUNDLE_SEQMODE_SET_FAILED,
    DRV_AT_MLPPP_BUNDLE_FRAG_SIZE_SET_FAILED,
    DRV_AT_MLPPP_BUNDLE_MRRU_SET_FAILED,
    DRV_AT_MLPPP_BUNDLE_ACTIVE_FAILED,
    DRV_AT_ENCAP_CHAN_OBJ_CREATE_FAILED,
    DRV_AT_ENCAP_CHAN_OBJ_DELETE_FAILED,
    DRV_AT_ENCAP_CHAN_OBJ_GET_FAILED,
    DRV_AT_ENCAP_CHAN_PHY_BIND_FAILED,
    DRV_AT_ENCAP_CHAN_PHY_UNBIND_FAILED,
    DRV_AT_ENCAP_CHAN_NOT_CREATED,
    DRV_AT_HDLC_LINK_GET_FAILED,
    DRV_AT_HDLC_LINK_TRAFFIC_ENABLE_FAILED,
    DRV_AT_HDLC_LINK_ETH_FLOW_BIND_FAILED,
    DRV_AT_HDLC_LINK_ETH_FLOW_UNBIND_FAILED,
    DRV_AT_HDLC_LINK_OAM_MODE_SET_FAILED,
    DRV_AT_HDLC_LINK_ETH_FLOW_OAM_GET_FAILED,
    DRV_AT_HDLC_CHAN_FCS_SET_FAILED,
    DRV_AT_HDLC_CHAN_SCRAMBLE_SET_FAILED,
    
   /*clk*/
    DRV_AT_CLK_SRC_LINK_GET_FAILED = 220,
    DRV_AT_CLK_DOMAIN_NOT_ENABLE,
    DRV_AT_CLK_DOMAIN_TABLE_FULL,
    DRV_AT_CLK_DOMAIN_MEMBER_FULL,
    DRV_AT_CLK_DOMAIN_MASTER_CAN_NOT_DEL,
    DRV_AT_CLK_DOMAIN_NO_SLAVE_MEMBER,
    DRV_AT_CLK_DOMAIN_MEMBER_NOT_FIND,
    DRV_AT_CLK_MODULE_GET_FAILED,
    DRV_AT_CLK_EXTRACTOR_GET_FAILED,
    DRV_AT_CLK_EXTRACTOR_SRC_GET_FAILED,
    DRV_AT_CLK_DOMAIN_ALREADY_HAVE_MASTER,
    DRV_AT_CLK_DOMAIN_INFO_WRONG,
    
} AT_E1_RETURN_CODE;  

/*vlan bitmap*/
#define  VLAN_TAG1_BIT0_5  0x0000003f
#define  VLAN_TAG1_BIT6_10 0x000007c0
#define  VLAN_TAG1_BIT11   0x00000800

#define  VLAN_TAG2_BIT0_7  0X000000ff
#define  VLAN_TAG2_BIT8    0x00000100
#define  VLAN_TAG2_BIT9_11 0x00000e00

/*thread Id*/
typedef struct
{
    BYTE E1_ALARM_THR_HANDLE;
    BYTE E1_ERROR_THR_HANDLE;
    BYTE PW_COUNT_THR_HANDLE;
}CE1TAN_THREAD_HANDLE;

/*thread No*/
typedef struct
{
    WORD32 E1_ALARM_THR_ID;
    WORD32 E1_ERROR_THR_ID;
    WORD32 PW_COUNT_THR_ID;
}CE1TAN_THREAD_ID;


/*thread flag*/
typedef struct
{
    BYTE E1_ALARM_THR_FLAG;
    BYTE E1_ERROR_THR_FLAG;
    BYTE PW_COUNT_THR_FLAG;
}CE1TAN_THREAD_FLAG;

/*thread type*/
typedef enum
{
    E1_ALARM_THREAD,
    E1_ERROR_THREAD,
    PW_COUNT_THREAD,
    ALL_THREAD,
}CE1TAN_THREAD_FLAG_TYPE;


/* 调试打印宏 */
#define DRV_AT_E1_PRINT(level, format, args...)\
    do\
    { \
        if ((g_at_print & (level)) || (DRV_AT_ALL_PRINT == (level))) \
        { \
            ROSNG_TRACE_DEBUG(format, args); \
        } \
    } while(0) \

/*记录错误log*/
#define DRV_AT_E1_ERROR_LOG(err_format, args...) \
do{\
    BSP_Print(BSP_DEBUG_ALL, "\n");\
    BSP_Print(BSP_DEBUG_ALL, "[CE1TAN][ERROR][TRACE]: ");\
    BSP_Print(BSP_DEBUG_ALL, err_format, args);\
    BSP_Print(BSP_DEBUG_ALL, "\n");\
}while(0)

/*记录普通log*/
#define DRV_AT_E1_COMMON_LOG(err_format, args...) \
do{\
    BSP_Print(BSP_DEBUG_ALL, "\n");\
    BSP_Print(BSP_DEBUG_ALL, "[CE1TAN][COMMON][TRACE]: ");\
    BSP_Print(BSP_DEBUG_ALL, err_format, args);\
    BSP_Print(BSP_DEBUG_ALL, "\n");\
}while(0)

/*检查参数宏*/

#define CHECK_AT_CHIP_ID(chipId)\
    do\
    {\
        if ( (chipId)>= AT_MAX_SUBCARD_NUM_PER_SLOT )\
        {\
            DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT, "\n%s%d, invalid chipId: %u !\n", __FILE__, __LINE__, chipId);\
            return DRV_AT_INVALID_CHIP_ID;\
        }\
    } while (0)

#define CHECK_AT_SUBCARD_ID(subcardId)\
    do \
    {\
        if ( ((subcardId) > AT_MAX_SUBCARD_NUM_PER_SLOT )||(((subcardId) < 1  )) )\
        {\
            DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT, "\n%s%d, invalid subcardId: %u !\n", __FILE__, __LINE__, subcardId);\
            return DRV_AT_INVALID_SUBCARD_ID;\
        }\
    } while (0)

#define CHECK_AT_E1PORT_ID(e1PortId)\
    do \
    {\
        if ( ((e1PortId) > AT_E1_INTF_NUM )||(((e1PortId) < 1  )))\
        {\
            DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT, "\n%s%d, invalid e1PortId: %u !\n", __FILE__, __LINE__, e1PortId);\
            return DRV_AT_INVALID_E1PORT_ID;\
        }\
    } while (0)

#define CHECK_AT_E1LINK_ID(e1LinkId)\
    do \
    {\
        if ( (e1LinkId) >= AT_E1_INTF_NUM )\
        {\
            DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT, "\n%s%d, invalid e1LinkId: %u !\n", __FILE__, __LINE__, e1LinkId);\
            return DRV_AT_INVALID_E1LINK_ID;\
        }\
    } while (0)

#define CHECK_AT_ETHPORT_ID(ethPortId)\
    do \
    {\
        if ( (ethPortId) > AT_ETH_PORT_NUM )\
        {\
            DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT, "\n%s%d, invalid ethPortId: %u !\n", __FILE__, __LINE__, ethPortId);\
            return DRV_AT_INVALID_ETHPORT_ID;\
        }\
    } while (0)

#define CHECK_AT_PPPLINK_NO(pppLinkNo)\
    do \
    {\
        if ( (pppLinkNo) >= AT_MAX_PPP_NUM_PER_SUBCARD)\
        {\
            DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT, "\n%s%d, invalid pppLinkNo: %u !\n", __FILE__, __LINE__, pppLinkNo);\
            return DRV_AT_INVALID_PPP_LINK_NO;\
        }\
    } while (0)
    
#define CHECK_AT_MLPPP_BUDNDLE_INDEX(bundleIndex)\
    do \
    {\
        if ( (bundleIndex) >= AT_MAX_MLPPP_BUNDLE_NUM_PER_SUBCARD)\
        {\
            DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT, "\n%s%d, invalid bundleIndex: %u !\n", __FILE__, __LINE__, bundleIndex);\
            return DRV_AT_INVALID_MLPPP_BUNDLE_INDEX;\
        }\
    } while (0)
    
#define CHECK_AT_PW_ID(pwId)\
    do \
    {\
        if ( (pwId) >= AT_MAX_PW_NUM_PER_SUBCARD )\
        {\
            DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT, "\n%s%d, invalid pwId: %u !\n", __FILE__, __LINE__, pwId);\
            return DRV_AT_INVALID_PW_ID;\
        }\
    } while (0)    

#define CHECK_AT_POINTER_NULL(p)\
    do \
    {\
        if ( NULL == (p) )\
        {\
            DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT, "\n%s%d, pointer is NULL ! !\n", __FILE__, __LINE__);\
            return DRV_AT_NULL_POINTER;\
        }\
    } while (0)




    


#endif

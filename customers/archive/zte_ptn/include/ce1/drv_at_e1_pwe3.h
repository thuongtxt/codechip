/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_at_e1_pwe3.h
* 文件标识: 
* 其它说明: 
* 当前版本: V1.0
* 作    者: 陈博10140635.
* 完成日期: 2013-05-14
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/

#ifndef _DRV_AT_E1_PWE3_H_
#define _DRV_AT_E1_PWE3_H_

#include "drv_at_e1_common.h"


/*PW 参数*/

#define  MASTER_CESOP_LINK 1

#define  SATOP_MODE  0
#define  CESOP_MODE  1


#define MIN_PKT_NUM_IN_BUF  3
#define MAX_PKT_NUM_IN_BUF  2048

#define MIN_PAYLOAD_SIZE  8
#define MAX_PAYLOAD_SIZE  1800

#define  TS_NUM_PER_E1 32
#define  E1_SIZE_IN_BYTE  32
#define  E1_FRAME_NUM_PER_MS 8
#define  NXDS0_SIZE_IN_BYTE 1
#define  NXDS0_FRAME_NUM_PER_MS 8
#define  INVALID_E1LINK_NO  (WORD32)0xffffffff
#define  INVALID_PW_ID    (WORD32)0xffffffff
#define  INVALID_CESOP_LINK_ID  (WORD32)0xffffffff
#define  AT_MAX_PW_NUM_PER_SUBCARD    128




#define  RTP_DISABLE  0
#define  RTP_ENABLE   1

#define  CTRLWORD_DISABLE 0
#define  CTRLWORD_ENABLE  1

#define  REORDER_DISABLE  0
#define  REORDER_ENABLE   1

#define  SUPPRESS_DISABLE  0
#define  SUPPRESS_ENABLE   1

#define  CHANNEL_DISABLE  0
#define  CHANNEL_ENABLE   1


#define  PAKCTES_RX_DIR      0
#define  PAKCTES_TX_DIR      1



typedef struct
{
    WORD32 zteVlan1Id;
    BYTE cpu;
    BYTE encapType;
    BYTE pktLen;
}DRV_AT_PW_VLANID1_INFO;


typedef struct
{
    WORD32 zteVlan2Id;
    BYTE portNo;
    BYTE isMlppp;
    WORD16 chanNo;
}DRV_AT_PW_VLANID2_INFO;


/* 驱动内部使用的数据结构 */
typedef struct
{
    BYTE   clkState;
    BYTE   clockMode;
    BYTE   resv1;              /*预留*/
   
    BYTE   tsNum;          /* number of time slots */           
    BYTE   pwStatus;
    BYTE   chipId;
    BYTE   isClkSrc;      /*pw是否是e1的时钟源，只适用于acr或dcr*/

    WORD16 payloadSize;   
    BYTE   servMode;
    BYTE   resv3 ;
    
    WORD32 e1LinkNum;        /* 0-based, [0~23] */
    WORD32 frameNum ;
    WORD32 masterE1LinkNo;  /*时钟域内的主时钟*/
    WORD32 clkDomainId;
    WORD32 timeSlotBitMap;
    WORD32 jitterBufferSize;  
    WORD32 cip;          /* 平台的cip*/
    WORD32 pwId;
    WORD32 cwEnable;     /* 控制字是否使能,默认使能.参见T_BSP_CW_STATUS定义 */
    WORD32 seqEnable;    /* 序列号使能使能,默认使能.参见T_BSP_SEQ_STATUS定义 */
    BYTE   destMac[6];
    WORD32 vlan1Pri;     /* ZTE TAG, vlan1 priority */
    WORD32 vlan2Pri;     /* ZTE TAG, vlan2 priority */
    WORD32 vlan1Cfi;     /* ZTE TAG, vlan1 cfi  */
    WORD32 vlan2Cfi;     /* ZTE TAG, vlan2 cfi  */
    DRV_AT_PW_VLANID1_INFO vlan1Info;
    DRV_AT_PW_VLANID2_INFO vlan2Info;    
}  DRV_AT_PW_PARA;



typedef struct
{
    WORD32 txPackets;
    WORD32 txPayloadBytes ;
    WORD32 rxPackets ;
    WORD32 rxPayloadBytes ;    
    WORD32 rxDiscardedPackets ;
    WORD32 rxMalformedPackets ;
    WORD32 rxReorderedPackets ;
    WORD32 rxLostPackets ;
    WORD32 rxOutOfSeqDropPackets;
    WORD32 rxOamPackets ;
      
    WORD32 txLbitPackets ;
    WORD32 txRbitPackets ;
    WORD32 rxLbitPackets ;
    WORD32 rxRbitPackets ;
    WORD32 rxJitBufOverrun ;
    WORD32 rxJitBufUnderrun ;
    WORD32 rxLops ;
    WORD32 rxPacketsSentToTdm ;
}DRV_AT_PW_COUNT;

typedef struct
{
    WORD32 lopsAlm;                          /* Loss of Packet Alarm in RX direction */
    WORD32 jitBufOverRunAlm;       /* Jitter buffer overrun alarm in RX direction*/
    WORD32 jitBufUnderRunAlm;    /* Jitter buffer underrun alarm in RX direction */
    WORD32 lbitAlm;                       /* L bit alarm in RX direction */
    WORD32 rbitAlm;                     /* R bit alarm in RX direction */
    WORD32 mbitAlm;                  /* M bit alarm in RX direction */
} DRV_AT_PW_ALM;  /* PW alarm structure */

typedef enum
{
     PW_IDLE,
     PW_USED ,
}AT_PW_ENABLE ;

typedef enum
{
     TX_LBIT,
     RX_LBIT ,
     MBIT,
     RBIT,
}AT_PW_CW_TYPE ;

WORD32 DrvAtPwInfoGetFromPwId(BYTE chipId,WORD32 pwId,DRV_AT_PW_PARA* drvPwInfo);
WORD32 DrvAtPwClkAlarmGetFromPwId(BYTE chipId,WORD32 pwId);
WORD32 DrvAtPwStausGet(BYTE chipId,WORD32 pwId,BYTE*isEnable );
WORD32 DrvAtPwStatusCheck(BYTE chipId ,WORD32 pwId);
WORD32 DrvAtPwCountMemGet(BYTE chipId, WORD32 pwId,DRV_AT_PW_COUNT*pwCount,BYTE countMode);
WORD32 DrvAtPwCountMemClear(BYTE chipId, WORD32 pwId,BYTE countMode);
WORD32 DrvAtCesopLinksNumGet(BYTE chipId, WORD32 e1LinkNo,WORD32* cesopPwNum);
WORD32 DrvAtPwInfoGetFromPwId(BYTE chipId,WORD32 pwId,DRV_AT_PW_PARA* drvPwInfo);
WORD32 DrvAtPwe3ServiceCreate(T_BSP_SRV_FUNC_PW_BIND_ARG * pmPwAddInfo);
WORD32 DrvAtPwe3ServiceDelete(T_BSP_SRV_FUNC_PW_UNBIND_ARG *pmPwDelInfo);
WORD32 DrvAtPwe3ServPldSizeUpdate(T_BSP_SRV_FUNC_PW_BIND_ARG *pmPwInfo);
WORD32 DrvAtPwe3ServJitBufSizeUpdate(T_BSP_SRV_FUNC_PW_BIND_ARG *pmPwInfo);
WORD32 DrvAtPwe3ServVlan1PriUpdate(T_BSP_SRV_FUNC_PW_BIND_ARG *pmPwInfo);
WORD32 DrvAtPwe3ServCesStatic(T_BSP_SRV_FUNC_CHIP_CESSTATIC_ARG *cesStaticInfo);
WORD32 DrvAtPwe3ServCesAlarm(T_BSP_SRV_FUNC_CHIP_CESALARM_ARG* cesAlarmInfo);
WORD32 DrvAtPwInfoGetFromCip(BYTE chipId,WORD32 cip,DRV_AT_PW_PARA* drvPwInfo);
WORD32 DrvAtPwAdd(DRV_AT_PW_PARA *atPwInfo);
WORD32 DrvAtPwDelete(DRV_AT_PW_PARA *atPwInfo);
WORD32 DrvAtPwClkModeInfoUpdate(BYTE chipId,WORD32 e1LinkNo,BYTE newClkMode );
WORD32 DrvAtNoneClkSrcPwIdFind(BYTE chipId,WORD32 e1LinkNo,WORD32*pwId);
WORD32 DrvAtClkSrcPwIdFind(BYTE chipId,WORD32 e1LinkNo,WORD32*pwId);
WORD32 DrvAtPwCountGet(BYTE chipId,WORD32 pwId);


#endif

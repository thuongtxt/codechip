/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_at_e1_pm.h
* 文件标识: 
* 其它说明: 
* 当前版本: V1.0
* 作    者: 陈博10140635.
* 完成日期: 2013-05-15
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/

#ifndef _DRV_AT_E1_PM_H_
#define _DRV_AT_E1_PM_H_

#include "drv_at_e1_common.h"

/*PDH E1 alarm bitmap*/

#define    AtPdhE1AlarmNone         0           /**< No alarm */
#define    AtPdhE1AlarmLos           0x00000001  /**< LOS */
#define    AtPdhE1AlarmLof           0x00000002  /**< LOF */
#define    AtPdhE1AlarmAis           0x00000004  /**< AIS */
#define    AtPdhE1AlarmRai           0x00000008  /**< RAI */
#define    AtPdhE1AlarmLomf        0x00000010  /**< LOMF */
#define    AtPdhE1AlarmSfBer       0x00000020  /**< SF BER */
#define    AtPdhE1AlarmSdBer      0x00000040  /**< SD BER */
#define    AtPdhE1AlarmSigLof      0x00000080  /**< LOF on signaling channel */
#define    AtPdhE1AlarmSigRai      0x000000f1  /**< RAI on signaling channel */
#define    AtPdhE1AlarmAll           0x000001FF  /**< All alarms */


typedef enum
{
    INSERT_OFF,
    INSERT_ON,
}PDH_E1_ALARM_INSERT ;


typedef struct 
{
    /* E1 defects */
    BYTE port_los;
    BYTE e1_rai;
    BYTE e1_ais;  
    BYTE e1_lof;   /* same as e1_oof */
    BYTE e1_los;  
    BYTE e1_lomf;
    BYTE res[2];
}PDH_E1_ALARM;


typedef struct
{
    WORD32 bpv;
    WORD32 fe;
    WORD32 rei;
    WORD32 crc;
}PDH_E1_ERR_COUNT;

WORD32 DrvAtE1AlarmGet(BYTE chipId,WORD32 e1LinkNo);
WORD32 DrvAt24E1AlarmGet(BYTE chipId);
WORD32 DrvAtE1ErrCountGet(BYTE chipId,WORD32 e1LinkNo);
WORD32 DrvAt24E1ErrCountGet(BYTE chipId);
WORD32 DrvAtE1AlarmQuery(BYTE subcardId,WORD32 e1PortId, WORD32 *QueryResult);
WORD32 DrvAtE1ErrorQuery(BYTE subcardId,WORD32 e1PortId, T_BSP_SDH_ERR_E1*QueryResult);
#endif


/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_at_e1_port.h
* 文件标识: 
* 其它说明: 
* 当前版本: V1.0
* 作    者: 陈博10140635.
* 完成日期: 2013-05-14
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/


#ifndef _DRV_AT_E1_PORT_H_
#define _DRV_AT_E1_PORT_H_

#include"drv_at_e1_common.h"


/*pcm30信令掩码*/
#define    AT_E1_SIGNAL_MASK  (WORD32)0Xfffefffe

/* e1 信令是否使能 */
 #define   AT_E1_SIGNAL_DISABLE 0
 #define   AT_E1_SIGNAL_ENABLE 1

/*自协商是否使能*/
#define    AT_ETH_AUTO_NEG_DISABLE   0
#define    AT_ETH_AUTO_NEG_ENABLE    1

/*php 是否使能*/
#define    AT_ETH_PHP_DISABLE   0
#define    AT_ETH_PHP_ENABLE    1

/*mac checkng 是否使能*/
#define    AT_ETH_MAC_CHECK_DISABLE   0
#define    AT_ETH_MAC_CHECK_ENABLE    1

/*环回开关*/
#define    LIU_LOOP_OFF   0
#define    LIU_LOOP_ON    1

/*插入*/
#define    LIU_INSERT_OFF  (BYTE)0
#define    LIU_INSERT_ON  (BYTE)1

/*接收时钟设置*/
#define    LIU_RECV_CLK_LINE   0
#define    LIU_RECV_CLK_SYS    1


/*LIU 寄存器地址*/
//第一页
#define  LIU_LOOP_INTERNAL_REG     0x1
#define  LIU_LOOP_LINE_REG         0x2
#define  LIU_TX_FORCE_AIS_REG      0x3
#define  LIU_RX_LOS_DETECT_REG     0x4
#define  LIU_RESET_REG             0xa
#define  LIU_PERF_MONITOR_REG      0xb
#define  LIU_LOOP_DIGITAL_REG      0xc
#define  LIU_ALARM_LEVEL_REG       0xd
#define  LIU_GLB_CFG_REG           0xf
#define  LIU_CHANN_SELECT_REG      0x10
#define  LIU_CHANN_MODE_SELECT_REG 0x11
#define  LIU_AIS_INTERRUPT_SET_REG 0x14
//第二页
#define  LIU_SINGLE_RAIL_SET_REG   0x0
#define  LIU_LINECODE_SET_REG      0x1
#define  LIU_RECV_CLK_SET          0x2
#define  LIU_RX_POWER_DOWN_REG   0x3
#define  LIU_TX_POWER_DOWN_REG   0x4
//第三页
#define  LIU_NEW_CDR_REG  0x1
#define  LIU_TX_CIRCUIT_CTRL_REG  0x2
#define  LIU_EQZ_SET_REG          0x6
//切换页
#define  LIU_ADDR_PONITER_CTRL_REG 0x1f


/*LIU 环回模式*/
typedef enum
{
    LIU_LOOP_LINE,
    LIU_LOOP_INTERNAL,
    LIU_LOOP_NONE,
}LIU_LOOPBACK_TYPE; 

/*LIU 线路编码*/
typedef enum
{
    LIU_LINECODE_HDB3,
    LIU_LINECODE_AMI,
}LIU_LINE_CODE_TYPE; 

/*E1端口操作类型*/
typedef enum
{
    AT_E1_FRAME_TYPE_SET,
    AT_E1_CLK_MODE_SET,
}AT_E1_PORT_SET_TYPE; 

/*E1时钟源类型*/
typedef enum
{
    AT_NULL,
    AT_PDH_OBJ,
    AT_PW_OBJ,
}AT_TIMING_SRC_TYPE; 


/*E1 帧格式类型*/
typedef enum
{
    AT_E1_UNFRAME,
    AT_E1_PCM30,
    AT_E1_PCM31,
    AT_E1_PCM30CRC,
    AT_E1_PCM31CRC,
    AT_E1_UNKNOWN_FRAME,
}AT_E1_FRAME_TYPE; 


/*e1 时钟模式*/
typedef enum
{
    SYS_CLK_MODE,
    ACR_CLK_MODE,
    DCR_CLK_MODE,
    SLAVE_CLK_MODE,
    LOOP_CLK_MODE,
    UNKNOWN_CLK_MODE,
}AT_E1_CLK_MODE; 


/*e1 时钟状态*/
typedef enum
{
    CLK_NOT_APPLICABLE,
    CLK_INIT_STATUS,
    CLK_HOLDOVER_STATUS,
    CLK_LEARNING_STATUS,
    CLK_LOCKED_STATUS,
    CLK_UNKNOWN_STATUS,
}AT_E1_CLK_STATUS; 


/*e1 通道的时钟域状态*/
typedef enum
{
    NONE_STATUS,
    MASTER_STATUS,
    SLAVE_STATUS,
}AT_E1_CLK_DOMIAN_STATUS;


/*e1 环回模式*/
typedef enum
{  
    AT_E1_LOOPBACK_CANCEL,
    AT_E1_LINE_LOOPBACK_LOCAL,
    AT_E1_LINE_LOOPBACK_REMOTE,
    AT_E1_FRAMER_LOOPBACK_LOCAL,
    AT_E1_FRAMER_LOOPBACK_REMOTE,
    AT_E1_UNKONWN_LOOPBACK,
}AT_E1_LOOP_MODE; 


/*e1端口状态统计*/
typedef struct
{
    WORD32 loopbackMode;
    WORD32 frameMode;
    WORD32 lineCode;
    WORD32 isShutDown;
    WORD32 recvClkMode;
}AT_E1_PORT_STATS;

/*eth 环回模式*/
typedef enum
{
    AT_ETH_LOOPBACK_CANCEL,
    AT_ETH_LOOPBACK_LOCAL,
    AT_ETH_LOOPBACK_REMOTE,
    AT_ETH_UNKONWN_LOOPBACK,
}AT_ETH_LOOP_MODE; 


/*以太端口状态*/
typedef enum
{
    AT_ETH_PORT_DOWN,
    AT_ETH_PORT_UP,
}AT_ETH_STATUS; 

/*serdes 环回模式*/
typedef enum
{
    AT_SERDES_LOOPBACK_CANCEL,
    AT_SERDES_LOOPBACK_LOCAL,
    AT_SERDES_LOOPBACK_REMOTE,
}AT_ETH_SERDES_LOOP_MODE; 

/*以太报文统计*/
typedef struct
{
    WORD32 txPackets ; 
    WORD32 txBytes;
    WORD32 txOamPackets ;
    WORD32 txPeriodOamPackets ;
    WORD32 txPwPackets ;
    WORD32 txPacketsLen0_64 ;
    WORD32 txPacketsLen65_127 ;
    WORD32 txPacketsLen128_255 ;
    WORD32 txPacketsLen256_511 ;
    WORD32 txPacketsLen512_1024 ;
    WORD32 txPacketsLen1025_1528 ;
    WORD32 txPacketsJumbo ;
    WORD32 txTopPackets ;
    WORD32 rxPackets ;
    WORD32 rxBytes ;
    WORD32 rxErrEthHdrPackets ;
    WORD32 rxErrBusPackets ;
    WORD32 rxErrFcsPackets ;
    WORD32 rxOversizePackets ;
    WORD32 rxUndersizePackets ;
    WORD32 rxPacketsLen0_64 ;
    WORD32 rxPacketsLen65_127 ;
    WORD32 rxPacketsLen128_255 ;
    WORD32 rxPacketsLen256_511 ;
    WORD32 rxPacketsLen512_1024 ;
    WORD32 rxPacketsLen1025_1528 ;
    WORD32 rxPacketsJumbo ;
    WORD32 rxPwUnsupportedPackets ;
    WORD32 rxErrPwLabelPackets ;
    WORD32 rxDiscardedPackets ;
    WORD32 rxPausePackets ;
    WORD32 rxErrPausePackets ;
    WORD32 rxBrdCastPackets ;
    WORD32 rxMultCastPackets ;
    WORD32 rxArpPackets ;
    WORD32 rxOamPackets ;
    WORD32 rxEfmOamPackets ;
    WORD32 rxErrEfmOamPackets ;
    WORD32 rxEthOamType1Packets ;
    WORD32 rxEthOamType2Packets ;
    WORD32 rxMplsPackets ;
    WORD32 rxErrMplsPackets ;
    WORD32 rxMplsErrOuterLblPackets ;
    WORD32 rxMplsDataPackets ;
    WORD32 rxErrPsnPackets ;
    WORD32 rxPacketsSendToCpu ;
    WORD32 rxPacketsSendToPw ;
}DRV_AT_ETH_COUNT;

/*函数声明*/
WORD32 DrvLiuLosGet(BYTE subcardId,WORD32 e1LinkNo,BYTE*liuLos);
WORD32 DrvAtEthIntfInit(BYTE chipId,BYTE ethPortId);
WORD32 DrvAtE1Nxds0ObjDelete(BYTE chipId,WORD32 e1LinkNo,WORD32 tsBitMap);
WORD32 DrvAtE1FrameTypeSet(BYTE chipId,WORD32 e1LinkNo,BYTE frameType);
WORD32 DrvAtE1DefaultCfg(BYTE chipId,WORD32 e1LinkId);
WORD32 DrvAtE1IntfInit(BYTE chipId);
WORD32 DrvAtE1LoopBackOp(BYTE chipId,WORD32 e1LinkNo,BYTE loopbackMode);
WORD32 DrvAtE1LoopBackSet(BYTE chipId,WORD32 e1LinkNo,BYTE loopbackMode);
WORD32 DrvAtEthLoopBackOp(BYTE chipId,BYTE ethPortId,BYTE loopbackMode);
WORD32 DrvAtEthLoopBackSet(BYTE chipId,BYTE ethPortId,BYTE loopbackMode);
WORD32 DrvAtEthStatusGet(BYTE chipId,BYTE ethPortId,WORD32* status);
WORD32 DrvAtE1ClkStateGet(BYTE chipId,WORD32 e1LinkNo,WORD32*clkState);
WORD32 DrvLiuLinecodeSet(BYTE subcardId,WORD32 e1LinkNo,WORD32 linecodeType);
WORD32 DrvLiuPortControl(BYTE subcardId,WORD32 e1LinkNo,WORD32 action);
WORD32 DrvAtE1FrameTypeGet(BYTE chipId,WORD32 e1LinkNo,BYTE *frameType);
WORD32 DrvAtE1ClkModeGet(BYTE chipId,WORD32 e1LinkNo,BYTE *clkMode);
WORD32 DrvAtE1TimingSrcGet(BYTE chipId,WORD32 e1LinkNo,BYTE*sourceType,WORD32* sourceId);
WORD32 DrvAtE1LoopBackGet(BYTE chipId,WORD32 e1LinkNo,BYTE* loopbackMode);
WORD32 DrvAtE1StausGet(BYTE chipId,WORD32 e1LinkNo,BYTE*isEnable );
WORD32 DrvAtEthLoopBackGet(BYTE chipId,BYTE ethPortId,BYTE *loopbackMode);
WORD32 DrvLiuLoopBackOp(BYTE subcardId,WORD32 e1LinkNo,BYTE loopbackMode ,BYTE option);
WORD32 DrvAtE1ClkInfoGet(BYTE chipId,WORD32 e1LinkNo,T_BSP_SDH_TRIB_TIMING_INFO *e1ClkInfo);
WORD32 DrvAtEthSerdesLoopBack(BYTE chipId, BYTE ethPortId,WORD32  loopbackMode);
WORD32 DrvAtE1BindPwIdGet(BYTE chipId,WORD32 e1LinkNo,WORD32* pwId);
WORD32 DrvAtE1ClkModeSet(BYTE chipId,WORD32 e1LinkNo,BYTE clkMode,WORD32 srcE1LinkNo,WORD32 pwId);
WORD32 DrvAtE1ClkDomainNoGet(T_BSP_SRV_FUNC_TDM_E1_CLK_INFOARG *e1ClkdomainInfo);
WORD32 DrvAtE1LineClkRecvSrcSet(BYTE chipId,WORD32 e1LinkNo,BYTE isEnable);
WORD32 DrvAtE1LineClkRecvSrcGet(BYTE chipId,WORD32* e1LinkNo);
WORD32 DrvAtEthStaticGet(BYTE chipId,BYTE ethPortId,DRV_AT_ETH_COUNT* ethCouter);
WORD32 DrvLiuRecvClkSet(BYTE subcardId,WORD32 e1LinkNo,BYTE recvClkMode);


#endif


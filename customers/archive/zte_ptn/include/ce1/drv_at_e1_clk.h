/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_tha_e1_clk.h
* 文件标识: 
* 其它说明: 
* 当前版本: V1.0
* 作    者: 陈博10140635.
* 完成日期: 2013-04-25
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/

#ifndef _DRV_AT_E1_CLK_H_
#define _DRV_AT_E1_CLK_H_

#include "drv_at_e1_common.h"



/*时钟域参数*/
#define   CLK_DOMAIN_DISABLE  0
#define   CLK_DOMAIN_ENABLE   1

#define AT_MAX_DOMAIN_NUM_PER_SUBCARD 24
#define AT_MAX_E1_NUM_PER_DOMAIN           24


/*时钟域内成员信息*/
typedef struct
{
    WORD32 e1LinkNo;
    BYTE state;
    BYTE isEnable;
    BYTE resv[2];
}AT_CLK_DOMAIN_MEMBER;


typedef struct
{
    WORD32 domainId;
    WORD32 masterE1LinkNo;  /*时钟域的当前主时钟E1链路*/
    AT_CLK_DOMAIN_MEMBER memberInfo[AT_MAX_E1_NUM_PER_DOMAIN];  /*域内成员信息*/
    BYTE domainEnable; 
    BYTE clkMode;
    BYTE rev[2];
}AT_CLK_DOMAIN_INFO;



WORD32 DrvAtClkDomainMasterGet(BYTE chipId,WORD32 clkDomainId,BYTE clkMode ,WORD32 *masterE1LinkNo);
WORD32 DrvAtClkDomainInfoCreate(BYTE chipId,WORD32 clkDomainId,BYTE clkMode ,WORD32 masterE1LinkNo);
WORD32 DrvAtClkDomainMasterGet(BYTE chipId,WORD32 clkDomainId,BYTE clkMode ,WORD32 *masterE1LinkNo);
WORD32 DrvAtClkDomainMemberInfoCreate(BYTE chipId,WORD32 clkDomainId,BYTE clkMode,WORD32 e1LinkNo);
WORD32 DrvAtClkDomainSlaveMemberGet(BYTE chipId,WORD32 clkDomainId,BYTE clkMode ,WORD32 masterE1LinkNo,WORD32* e1LinkNo);
WORD32 DrvAtClkDomainMemberInfoDelete(BYTE chipId,WORD32 clkDomainId,BYTE clkMode,WORD32 e1LinkNo);
WORD32 DrvAtClkDomainInfoDelete(BYTE chipId,WORD32 clkDomainId,BYTE clkMode );
WORD32 DrvAtClkSrcSwitch(BYTE chipId,BYTE clkMode,WORD32 clkDomainId,WORD32 oldMasterE1No, WORD32 newMasterE1No);
WORD32 DrvAtClkDomainMemberAlmGet(BYTE chipId,WORD32 e1LinkNo,WORD32*isAlmState);

#endif

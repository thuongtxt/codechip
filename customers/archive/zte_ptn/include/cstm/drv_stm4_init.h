/***********************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_stm4_init.h
* 文件标识: 
* 其它说明: 驱动STM-4 TDM PWE3模块的初始化头文件
* 当前版本: V1.0
* 作    者: 谢伟生10112265.
* 完成日期: 2014-01-13.
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
************************************************************/

#ifndef _DRV_STM4_INIT_H_
#define _DRV_STM4_INIT_H_


extern WORD32 drv_tdm_stm4PortIdCheck(BYTE ucChipId, BYTE ucPortId);


#endif


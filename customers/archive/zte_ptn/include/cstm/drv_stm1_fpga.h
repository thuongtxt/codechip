/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_stm1_fpga.h
* 文件标识: 
* 其它说明: 驱动STM-1 TDM PWE3模块的FPGA芯片的实现头文件.
* 当前版本: V1.0
* 作    者: 谢伟生10112265.
* 完成日期: 2013-06-20
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/


#ifndef _DRV_STM1_FPGA_H_
#define _DRV_STM1_FPGA_H_

#include "drv_tdm_init.h"

/* 函数原型声明 */
extern WORD32 drv_tdm_fpgaReg16Read(WORD32 subslotId, WORD16 offsetAddr, WORD16 *pValue);
extern WORD32 drv_tdm_fpgaReg16Write(WORD32 subslotId, WORD16 offsetAddr, WORD16 value);


#endif


/***********************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_pos_overhead.h
* 文件标识: 
* 其它说明: 驱动STM-1 POS PWE3模块的STM-1接口的开销字节处理头文件
* 当前版本: V1.0
* 作    者: 刘钰00130907.
* 完成日期: 2013-03-26.
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
************************************************************/

#ifndef _DRV_POS_OVERHEAD_H_
#define _DRV_POS_OVERHEAD_H_

#include "drv_pos_comm.h"


#define DRV_POS_SDH_OH_TASK_NAME_SIZE    40  /* SDH overhead任务的任务名大小 */
#define DRV_POS_SDH_OH_TASK_PRIORITY     (WORD16)(75)  /* SDH overhead进程的优先级 */
#define DRV_POS_SDH_OH_TASK_STACK_SIZE   (WORD32)(200*1024)  /* SDH overhead进程的stack大小 */



/* The following enumeration defines constants indicating OCN trace message modes */
typedef enum
{
    DRV_POS_TRACE_MSG_1BYTE  = 0,  /* Message 1 byte */
    DRV_POS_TRACE_MSG_16BYTE = 1,  /* Message 16 byte */
    DRV_POS_TRACE_MSG_64BYTE = 2,  /* Message 64 byte */
} DRV_POS_TRACE_MSG_MODE;

/* The following enumeration defines constants indicating OCN trace message length */
typedef enum
{
    DRV_POS_TRACE_MSG_LEN_1  = 1,    /* Message 1 bytes */
    DRV_POS_TRACE_MSG_LEN_16 = 16,   /* Message 16 bytes */
    DRV_POS_TRACE_MSG_LEN_64 = 64,   /* Message 64 bytes */
} DRV_POS_TRACE_MSG_LEN;

#define DRV_POS_TRACE_MSG_MAX_LEN  DRV_POS_TRACE_MSG_LEN_64  /* J字节的最大长度为64字节 */

typedef struct
{
    BYTE txJ0MsgBuf[DRV_POS_TRACE_MSG_MAX_LEN];  /* transmit J0 message buffer*/
    BYTE rxJ0MsgBuf[DRV_POS_TRACE_MSG_MAX_LEN];  /* receive J0 message buffer*/
    BYTE expectedRxJ0MsgBuf[DRV_POS_TRACE_MSG_MAX_LEN];  /* expected receive J0 message buffer*/
    BYTE txK1Value;                            /* transmit K1 */
    BYTE rxK1Value;                            /* receive K1 */
    BYTE txK2Value;                            /* transmit K2 */
    BYTE rxK2Value;                            /* receive K2 */
    BYTE txS1Value;                            /* transmit S1 */
    BYTE rxS1Value;                            /* receive S1 */
    BYTE reserved[2];                          /* 保留字节,用于字节对齐 */
} DRV_POS_SECTION_OH; /* STM-1帧的段开销 */

typedef struct
{
    BYTE txJ1MsgBuf[DRV_POS_TRACE_MSG_MAX_LEN];  /* transmit J1 message buffer*/
    BYTE rxJ1MsgBuf[DRV_POS_TRACE_MSG_MAX_LEN];  /* receive J1 message buffer*/
    BYTE expectedRxJ1MsgBuf[DRV_POS_TRACE_MSG_MAX_LEN];  /* expected receive J1 message buffer*/
    BYTE txC2Value;                              /* transmit C2*/
    BYTE rxC2Value;                              /* receive C2 */
    BYTE expectedRxC2Value;                      /* expected receive C2 */
    BYTE reserved[1];                          /* 保留字节,用于字节对齐 */
} DRV_POS_VC4_OH; /* STM-1帧的VC4开销 */

typedef struct
{
    DRV_POS_SECTION_OH sectionOh;                        /* STM-1帧的段开销 */
    DRV_POS_VC4_OH vc4Oh;                                /* STM-1帧的VC4开销 */
} DRV_POS_STM1_OVERHEAD; /* STM-1帧的开销 */


/* 函数原型声明 */
DRV_POS_RET drv_pos_stm1OhMemGet(BYTE chipId, BYTE portIdx, DRV_POS_STM1_OVERHEAD** ppOverhead);
/*DRV_POS_RET drv_pos_stm1OverheadSave(BYTE chipId, BYTE portIdx, const DRV_POS_STM1_OVERHEAD *pOverhead);
DRV_POS_RET drv_pos_traceMsgStrLenGet(WORD32 msgMode, WORD32 *length);
DRV_POS_RET drv_pos_txJ0Get(BYTE chipId, BYTE portIdx, BYTE *msgBuf);*/
DRV_POS_RET drv_pos_txJ0Set(BYTE chipId, BYTE portIdx, WORD32 traceMode, const BYTE *msgBuf);
/*DRV_POS_RET drv_pos_expectedRxJ0Get(BYTE chipId, BYTE portIdx, BYTE *msgBuf);*/
DRV_POS_RET drv_pos_expectedRxJ0Set(BYTE chipId, BYTE portIdx, WORD32 traceMode,const BYTE *msgBuf);
/*DRV_POS_RET drv_pos_rxJ0Get(BYTE chipId, BYTE portIdx, BYTE *msgBuf);*/
DRV_POS_RET drv_pos_txJ1Set(BYTE chipId, BYTE portIdx, WORD32 traceMode, const BYTE *msgBuf);
/*DRV_POS_RET drv_pos_txJ1Get(BYTE chipId, BYTE portIdx, BYTE au4Id, BYTE *msgBuf);*/
DRV_POS_RET drv_pos_expectedRxJ1Set(BYTE chipId, BYTE portIdx, WORD32 traceMode, const BYTE *msgBuf);
/*DRV_POS_RET drv_pos_expectedRxJ1Get(BYTE chipId, BYTE portIdx, BYTE au4Id, BYTE *msgBuf);*/
/*DRV_POS_RET drv_pos_rxJ1Get(BYTE chipId, BYTE portIdx, BYTE au4Id, BYTE *msgBuf);*/
DRV_POS_RET drv_pos_txC2Set(BYTE chipId, BYTE portIdx, BYTE c2Value);
/*DRV_POS_RET drv_pos_txC2Get(BYTE chipId, BYTE portIdx, BYTE au4Id, BYTE *pC2Value);*/
DRV_POS_RET drv_pos_expectedRxC2Set(BYTE chipId, BYTE portIdx, BYTE c2Value);
/*DRV_POS_RET drv_pos_expectedRxC2Get(BYTE chipId, BYTE portIdx, BYTE au4Id, BYTE *pC2Value);
DRV_POS_RET drv_pos_rxC2Get(BYTE chipId, BYTE portIdx, BYTE au4Id, BYTE *pC2Value);*/
DRV_POS_RET drv_pos_txS1Set(BYTE chipId, BYTE portIdx, BYTE s1Value);
/*DRV_POS_RET drv_pos_txS1Get(BYTE chipId, BYTE portIdx, BYTE *pS1Value);
DRV_POS_RET drv_pos_rxS1Get(BYTE chipId, BYTE portIdx, BYTE *pS1Value);*/
DRV_POS_RET drv_pos_txK1Set(BYTE chipId, BYTE portIdx, BYTE k1Value);
/*DRV_POS_RET drv_pos_txK1Get(BYTE chipId, BYTE portIdx, BYTE *pK1Value);
DRV_POS_RET drv_pos_rxK1Get(BYTE chipId, BYTE portIdx, BYTE *pK1Value);*/
DRV_POS_RET drv_pos_txK1Query(BYTE chipId, BYTE portIdx, BYTE *pValue);
DRV_POS_RET drv_pos_rxK1Query(BYTE chipId, BYTE portIdx, BYTE *pValue);
DRV_POS_RET drv_pos_txK2Set(BYTE chipId, BYTE portIdx, BYTE k2Value);
/*DRV_POS_RET drv_pos_txK2Get(BYTE chipId, BYTE portIdx, BYTE *pK2Value);
DRV_POS_RET drv_pos_rxK2Get(BYTE chipId, BYTE portIdx, BYTE *pK2Value);*/
DRV_POS_RET drv_pos_txK2Query(BYTE chipId, BYTE portIdx, BYTE *pValue);
DRV_POS_RET drv_pos_rxK2Query(BYTE chipId, BYTE portIdx, BYTE *pValue);
DRV_POS_RET drv_pos_sdhOhFlagGet(BYTE chipId, WORD32 *pFlag);
DRV_POS_RET drv_pos_sdhOhFlagSet(BYTE chipId, WORD32 flag);
DRV_POS_RET drv_pos_sdhSecOhFlagGet(BYTE chipId, BYTE portIdx, WORD32 *pFlag);
DRV_POS_RET drv_pos_sdhSecOhFlagSet(BYTE chipId, BYTE portIdx, WORD32 flag);
DRV_POS_RET drv_pos_sdhVc4OhFlagGet(BYTE chipId, BYTE portIdx, WORD32 *pFlag);
DRV_POS_RET drv_pos_sdhVc4OhFlagSet(BYTE chipId, BYTE portIdx, WORD32 flag);
DRV_POS_RET drv_pos_txJ0Query(BYTE chipId, BYTE portIdx, BYTE *msgBuf);
DRV_POS_RET drv_pos_rxJ0Query(BYTE chipId, BYTE portIdx, BYTE *msgBuf);
DRV_POS_RET drv_pos_expectedRxJ0Query(BYTE chipId, BYTE portIdx, BYTE *msgBuf);
DRV_POS_RET drv_pos_txS1Query(BYTE chipId, BYTE portIdx, BYTE *pValue);
DRV_POS_RET drv_pos_rxS1Query(BYTE chipId, BYTE portIdx, BYTE *pValue);
DRV_POS_RET drv_pos_txJ1Query(BYTE chipId, BYTE portIdx, BYTE *msgBuf);
DRV_POS_RET drv_pos_rxJ1Query(BYTE chipId, BYTE portIdx, BYTE *msgBuf);
DRV_POS_RET drv_pos_expectedRxJ1Query(BYTE chipId, BYTE portIdx, BYTE *msgBuf);
DRV_POS_RET drv_pos_txC2Query(BYTE chipId, BYTE portIdx, BYTE *pValue);
DRV_POS_RET drv_pos_rxC2Query(BYTE chipId, BYTE portIdx, BYTE *pValue);
DRV_POS_RET drv_pos_expectedRxC2Query(BYTE chipId, BYTE portIdx, BYTE *pValue);
DRV_POS_RET drv_pos_sdhOhThreadCreate(BYTE subslotId);
DRV_POS_RET drv_pos_sdhOhThreadDelete(BYTE subslotId);
DRV_POS_RET drv_pos_sdhOhThreadIdGet(BYTE chipId, int *pThreadId);

#endif



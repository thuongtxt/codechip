/***********************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_pos_sdh_alarm.h
* 文件标识: 
* 其它说明: 驱动STM-1 POS PWE3模块的STM-1接口的SDH告警处理头文件
* 当前版本: V1.0
* 作    者: 刘钰00130907.
* 完成日期: 2013-08-30.
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
************************************************************/

#ifndef _DRV_POS_SDH_ALARM_H_
#define _DRV_POS_SDH_ALARM_H_

#include "drv_pos_comm.h"
   

#define DRV_POS_SDH_ALM_TASK_NAME_SIZE    40  /* SDH Alarm任务的任务名大小 */
#define DRV_POS_SDH_ALM_TASK_PRIORITY     ((WORD16)75)  /* SDH Alarm进程的优先级 */
#define DRV_POS_SDH_ALM_TASK_STACK_SIZE   ((WORD32)(16*1024))  /* SDH Alarm进程的stack大小 */

#define DRV_POS_SDH_ALARM_INTR_ALL_MASK     ((WORD32)0xffff)

#define DRV_POS_SDH_ALARM_INTR_ALL_ENABLE   ((WORD32)0xffff)
#define DRV_POS_SDH_ALARM_INTR_ALL_DISABLE  ((WORD32)0x0)


#define DRV_POS_SDH_LINE_LOS_ALARM             cAtSdhLineAlarmLos
#define DRV_POS_SDH_LINE_OOF_ALARM             cAtSdhLineAlarmOof
#define DRV_POS_SDH_LINE_LOF_ALARM             cAtSdhLineAlarmLof         
#define DRV_POS_SDH_LINE_TIM_ALARM             cAtSdhLineAlarmTim         
#define DRV_POS_SDH_LINE_AIS_ALARM             cAtSdhLineAlarmAis         
#define DRV_POS_SDH_LINE_RDI_ALARM             cAtSdhLineAlarmRdi         
#define DRV_POS_SDH_LINE_BERSD_ALARM           cAtSdhLineAlarmBerSd       
#define DRV_POS_SDH_LINE_BERSF_ALARM           cAtSdhLineAlarmBerSf       
#define DRV_POS_SDH_LINE_KBYTECHANGE_ALARM     cAtSdhLineAlarmKByteChange 
#define DRV_POS_SDH_LINE_KBYTEFAIL_ALARM       cAtSdhLineAlarmKByteFail   
#define DRV_POS_SDH_LINE_S1CHANGE_ALARM        cAtSdhLineAlarmS1Change    
#define DRV_POS_SDH_LINE_RSBERSD_ALARM         cAtSdhLineAlarmRsBerSd     
#define DRV_POS_SDH_LINE_RSBERSF_ALARM         cAtSdhLineAlarmRsBerSf     
#define DRV_POS_SDH_LINE_K1CHANGE_ALARM        cAtSdhLineAlarmK1Change    
#define DRV_POS_SDH_LINE_K2CHANGE_ALARM        cAtSdhLineAlarmK2Change    

typedef struct
{
    BYTE rsOof;  /* RS out of frame */
    BYTE rsLof;  /* RS loss of frame */
    BYTE rsLos;  /* RS loss of signal */
    BYTE rsTim;  /* RS trace identifier mismatch */
    BYTE rsSd;   /* RS signal degrate*/
    BYTE rsSf;   /* RS signal fail*/
    BYTE msAis;  /* MS alarm indication signal */
    BYTE msRdi;  /* RS remote defect indication */
    BYTE msSd;   /* MS signal degrade */
    BYTE msSf;   /* MS signal fail */
} DRV_POS_SECTION_ALARM;  /* STM-1帧的段告警*/

typedef struct
{
    BYTE au4Lop;       /* AU4 loss of pointer */
    BYTE vc4Ais;       /* VC4 alarm indication signal */
    BYTE vc4Uneq;      /* VC4 unequipped */
    BYTE vc4Plm;       /* VC4 payload label mismatch  */
    BYTE vc4Tim;       /* VC4 trace identifier mismatch  */
    BYTE vc4Rdi;       /* VC4 remote defect indication */
    BYTE vc4Sd;        /* VC4 signal degrade */
    BYTE vc4Sf;        /* VC4 signal fail */
} DRV_POS_VC4_ALARM;   /* STM-1帧的高阶通道告警*/

typedef struct
{
    DRV_POS_SECTION_ALARM sectionAlm;   /* STM-1帧的段告警*/
    DRV_POS_VC4_ALARM vc4Alm;           /* STM-1帧的高阶通道告警*/
} DRV_POS_SDH_ALARM;

/* 函数原型声明 */
DRV_POS_RET drv_pos_sdhAlmMemGet(BYTE chipId, BYTE portIdx, DRV_POS_SDH_ALARM** ppAlarm);
DRV_POS_RET drv_pos_sectionAlarmGet(BYTE chipId, BYTE portIdx, DRV_POS_SECTION_ALARM *pSectionAlm);
DRV_POS_RET drv_pos_sectionAlmSave(BYTE chipId, BYTE portIdx, const DRV_POS_SECTION_ALARM *pSectionAlm);
/*DRV_POS_RET drv_pos_sdhAlarmClear(BYTE chipId, BYTE portIdx);
DRV_POS_RET drv_pos_highPathAlmSave(BYTE chipId, BYTE portIdx, const DRV_POS_VC4_ALARM *pVc4Alm);
DRV_POS_RET drv_pos_highPathAlarmGet(BYTE chipId, BYTE portIdx, BYTE au4Id, DRV_POS_VC4_ALARM *pVc4Alm);*/
DRV_POS_RET drv_pos_sdhAlmFlagGet(BYTE chipId, WORD32 *pFlag);
DRV_POS_RET drv_pos_sdhAlmFlagSet(BYTE chipId, WORD32 flag);
DRV_POS_RET drv_pos_sdhSecAlmFlagGet(BYTE chipId, BYTE portIdx, WORD32 *pFlag);
DRV_POS_RET drv_pos_sdhSecAlmFlagSet(BYTE chipId, BYTE portIdx, WORD32 flag);
DRV_POS_RET drv_pos_sdhVc4AlmFlagGet(BYTE chipId, BYTE portIdx, WORD32 *pFlag);
DRV_POS_RET drv_pos_sdhVc4AlmFlagSet(BYTE chipId, BYTE portIdx, WORD32 flag);
/*DRV_POS_RET drv_pos_sdhAlarmMonitor(const WORD32 *pDwSubslotId);*/
DRV_POS_RET drv_pos_sectionAlarmQuery(BYTE chipId, BYTE portIdx, WORD32 *pAlmResult);
DRV_POS_RET drv_pos_highPathAlarmQuery(BYTE chipId, BYTE portIdx, WORD32 *pAlmResult);
DRV_POS_RET drv_pos_sdhAlmThreadCreate(BYTE subslotId);
DRV_POS_RET drv_pos_sdhAlmThreadDelete(BYTE subslotId);
DRV_POS_RET drv_pos_sdhAlmThreadIdGet(BYTE chipId, int *pThreadId);
DRV_POS_RET drv_pos_rxRsLosForce(BYTE chipId, BYTE portIdx);
DRV_POS_RET drv_pos_rxRsLosUnForce(BYTE chipId, BYTE portIdx);

DRV_POS_RET drv_pos_alarmIntrAllDisable(BYTE chipId, BYTE portIdx);
DRV_POS_RET drv_pos_alarmIntrMaskSet(BYTE chipId, BYTE portIdx, WORD32 defectMask, WORD32 enableMask);
DRV_POS_RET drv_pos_alarmIntrDefectMaskGet(BYTE chipId, BYTE portIdx, WORD32* pDefectMask);
DRV_POS_RET drv_pos_alarmIntrStateGet(BYTE chipId, BYTE portIdx, WORD32* pEnableMask);
DRV_POS_RET drv_pos_alarmIntrClear(BYTE chipId, BYTE portIdx);

/*DRV_POS_RET drv_pos_msAisForce(BYTE chipId, BYTE portIdx, WORD32 direction);
DRV_POS_RET drv_pos_msAisUnForce(BYTE chipId, BYTE portIdx, WORD32 direction);
DRV_POS_RET drv_pos_au4AisForce(BYTE chipId, BYTE portIdx, BYTE au4Id, WORD32 direction);
DRV_POS_RET drv_pos_au4AisUnForce(BYTE chipId, BYTE portIdx, BYTE au4Id, WORD32 direction);
*/
#endif



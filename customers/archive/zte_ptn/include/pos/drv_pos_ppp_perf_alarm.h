/***********************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_pos_ppp_perf.h
* 文件标识: 
* 其它说明: 驱动POS PPP模块的性能统计处理头文件
* 当前版本: V1.0
* 作    者: 刘钰00130907.
* 完成日期: 2013-9-3.
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
************************************************************/

#ifndef _DRV_POS_PPP_PERF_ALARM_H_
#define _DRV_POS_PPP_PERF_ALARM_H_

#include "drv_pos_comm.h"

#define DRV_POS_PPP_PERF_TASK_NAME_SIZE    ((WORD32)40)  /* PPP performance任务的任务名大小 */
#define DRV_POS_PPP_PERF_TASK_PRIORITY     ((WORD16)75)  /* PPP performance进程的优先级 */
#define DRV_POS_PPP_PERF_TASK_STACK_SIZE   ((WORD32)(200*1024))  /* PPP performance进程的stack大小 */
   
typedef struct
{
    WORD32 tx_pkt;
    WORD32 tx_byte;
    WORD32 tx_goodPkt;
    WORD32 tx_abortPkt;
    WORD32 rx_pkt;
    WORD32 rx_byte;
    WORD32 rx_goodPkt;
    WORD32 rx_abortPkt;
    WORD32 rx_fcsErrPkt;
    WORD32 rx_addrCtrlErrPkt;
    WORD32 rx_sapiErrPkt;
    WORD32 rx_errPkt;
    WORD32 tick;
} DRV_POS_PPP_PERF;
#if 0
typedef struct 
{
    BYTE lcpState;     /*pos板上报lcp失效告警状态*/
    BYTE ipcpState;    /*pos板上报ipcp失效告警状态*/
}DRV_POS_PPP_ALARM;
#endif


DRV_POS_RET drv_pos_pppPerfMemGet(BYTE chipId, BYTE portIdx, DRV_POS_PPP_PERF** ppPerf);
/*DRV_POS_RET drv_pos_pppPerfClear(BYTE chipId, BYTE portIdx);
DRV_POS_RET drv_pos_pppPerfSave(BYTE chipId, BYTE portIdx, const DRV_POS_PPP_PERF *pPppPerf);
DRV_POS_RET drv_pos_pppPerfGet(BYTE chipId, BYTE portIdx, DRV_POS_PPP_PERF *pPppPerf);*/
DRV_POS_RET drv_pos_pppPerfFlagGet(BYTE chipId, BYTE portIdx, WORD32 *pFlag);
DRV_POS_RET drv_pos_pppPerfFlagSet(BYTE chipId, BYTE portIdx, WORD32 flag);
/*DRV_POS_RET drv_pos_pppPerfMonitor(const WORD32 *pDwSubslotId);
DRV_POS_RET drv_pos_pppPerfQuery(BYTE chipId, BYTE portIdx, DRV_POS_PPP_PERF *pPerfResult);*/
DRV_POS_RET drv_pos_pppPerfThreadCreate(BYTE subslotId);
DRV_POS_RET drv_pos_pppPerfThreadDelete(BYTE subslotId);
DRV_POS_RET drv_pos_pppPerfThreadIdGet(BYTE chipId, int *pThreadId);


#endif


/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: drv_pos.h
* 文件标识: 
* 其它说明: 15k pos模块主代码头文件
* 当前版本: V1.0
* 作    者: 刘钰00130907
* 完成日期: 2013-08-16
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/
#ifndef _DRV_POS_H_
#define _DRV_POS_H_
#include "drv_pos_comm.h"

#define  DRV_POS_IPV4_PID              0x0021
#define  DRV_POS_IPV6_PID              0x0057
#define  DRV_POS_MPLSU_PID             0x0281
#define  DRV_POS_MPLSM_PID             0x0283
#define  DRV_POS_ISIS_PID              0x0023
#define  DRV_POS_PPP_PID               0xc000

#define  DRV_POS_IPV4_ETHTYPE          0x0800
#define  DRV_POS_IPV6_ETHTYPE          0x86dd
#define  DRV_POS_MPLSU_ETHTYPE         0x8847
#define  DRV_POS_MPLSM_ETHTYPE         0x8848

#define  DRV_POS_PPP_ROUTE_ENCAPTYP    1

#define  DRV_POS_PPP_PRI_DEFAULT       5
#define  DRV_POS_PPP_CFI_DEFAULT       0
/*DRV_POS_RET drv_pos_vlan_mapping(BYTE chipId, BYTE portIdx, tAtZtePtnTag1* vlan1Tag, tAtZtePtnTag2* vlan2Tag);*/
DRV_POS_RET drv_atPos_hal_create(BYTE chipId);
DRV_POS_RET drv_atPos_hal_delete(BYTE chipId);
/*DRV_POS_RET drv_atPos_get_productCode(BYTE chipId, AtHal atHal, WORD32 *productCode);*/
DRV_POS_RET drv_atPos_driver_create(BYTE chipId);
DRV_POS_RET drv_atPos_driver_delete(BYTE chipId);
DRV_POS_RET drv_atPos_device_create(BYTE chipId);
DRV_POS_RET drv_atPos_device_delete(BYTE chipId);
DRV_POS_RET drv_atPos_device_setup(BYTE chipId);
DRV_POS_RET drv_atPos_device_initial(BYTE chipId);
/*DRV_POS_RET drv_atPos_set_sdhModeRate(AtSdhLine pSdhLine);
DRV_POS_RET drv_atPos_set_sdhMaping(AtSdhLine pSdhLine, BYTE aug1Id);
DRV_POS_RET drv_atPos_sdhScramble_cfg(AtSdhLine pSdhLine, BYTE isEnable);
DRV_POS_RET drv_atPos_sdhSectionOh_cfg(AtSdhLine pSdhLine);
DRV_POS_RET drv_atPos_sdhHpOh_cfg(AtSdhLine pSdhLine);
DRV_POS_RET drv_atPos_sdhOh_cfg(AtSdhLine pSdhLine);
DRV_POS_RET drv_atPos_sdhBer_cfg(AtSdhLine pSdhLine);
DRV_POS_RET drv_atPos_pppSdh_cfg(BYTE chipId, BYTE portIdx);*/
DRV_POS_RET drv_atPos_sdh_init_cfg(BYTE chipId, BYTE portIdx);
DRV_POS_RET drv_atPos_enet_init_cfg(BYTE chipId, BYTE ethIdx);
/*DRV_POS_RET drv_atPos_EncapChannel_create(BYTE chipId, BYTE portIdx);
DRV_POS_RET drv_atPos_EncapChannel_delete(BYTE chipId, BYTE portIdx);
DRV_POS_RET drv_atPos_EthFlow_delete(BYTE chipId, BYTE portIdx);
DRV_POS_RET drv_atPos_EthFlow_create(BYTE chipId, BYTE portIdx);
DRV_POS_RET drv_atPos_link_disconnect(BYTE chipId, BYTE portIdx);
DRV_POS_RET drv_atPos_link_connect(BYTE chipId, BYTE portIdx);*/
DRV_POS_RET drv_atPos_ppp_delete(BYTE chipId, BYTE portIdx);
DRV_POS_RET drv_atPos_ppp_create(BYTE chipId, BYTE portIdx);
DRV_POS_RET drv_atPos_pppPidSet(BYTE chipId);
DRV_POS_RET drv_atPos_sdhModuleInterruptEnable(BYTE chipId);
DRV_POS_RET drv_atPos_pppModeSet155(BYTE chipId, DRV_POS_PORT_GROUP portGrp);
DRV_POS_RET drv_atPos_pppModeSet622(BYTE chipId, DRV_POS_PORT_GROUP portGrp);

#endif

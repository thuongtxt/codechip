/*************************************************************************
* 版权所有(C)2013, 中兴通讯股份有限公司.
*
* 文件名称: bsp_cp3ban_common.c
* 文件标识: 
* 其它说明: CP3BAN单板的与单板有关的公共函数实现.
* 当前版本: V1.0
* 作    者: 谢伟生10112265.
* 完成日期: 2013-03-26
*
* 修改记录: 
*    修改日期: 
*    版本号: V1.0
*    修改人: 
*    修改内容: create
**************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "bsp.h"
#include "Pub_TypeDef.h"
#include "eipmod.h"
#include "bsp_boardctrl_common.h"
#include "bsp_boardctrl_sp.h"
#include "bsp_subcard_common_api.h"
#include "bsp_subcard_ctrl.h"
#include "bsp_max3997_api.h"
#include "bsp_max3997_ctrl.h"
#include "bsp_optinfo.h"
#include "drv_tdm_init.h"
#include "drv_tdm_intf.h"
#include "drv_tdm_sdh_alarm.h"
#include "bsp_cp3ban_ctrl.h"
#include "bsp_cp3ban_common.h"
#include "bsp_cp3ban_intf.h"


/* 全局变量定义 */
static WORD32 g_bsp_cp3ban_sfp_delay = 7001;  /* SFP线程的延时间隔 */

/* CP3BAN单板的卸载标记.具体参见T_BSP_CP3BAN_UNINIT_FLAG定义 */
static WORD32 g_bsp_cp3ban_uninit_Flag[BSP_CP3BAN_MAX_BOARD_NUM + 1] = {0, 0, 0, 0, 0};

/* 信号量的创建标记.该信号量用来保护访问MAX3997 */
static WORD32 g_bsp_cp3ban_max3997_sem_flag[BSP_CP3BAN_MAX_BOARD_NUM + 1] = {0, 0, 0, 0, 0};

/* 信号量ID.该信号量用来保护访问MAX3997. */
static WORD32 g_bsp_cp3ban_max3997_sem_id[BSP_CP3BAN_MAX_BOARD_NUM + 1] = {0, 0, 0, 0, 0};

/* 信号量的创建标记.该信号量用来保护访问SFP. */
static WORD32 g_bsp_cp3ban_sfp_sem_flag[BSP_CP3BAN_MAX_BOARD_NUM + 1] = {0};

/* 信号量ID.该信号量用来保护访问SFP. */
static WORD32 g_bsp_cp3ban_sfp_sem_id[BSP_CP3BAN_MAX_BOARD_NUM + 1] = {0};

/* 信号量的创建标记.该信号量用来保护保存I2C光模块信息的内存 */
static WORD32 g_bsp_cp3ban_i2c_sem_flag[BSP_CP3BAN_MAX_BOARD_NUM + 1] = {0};

/* 信号量ID.该信号量用来保护保存I2C光模块信息的内存 */
static WORD32 g_bsp_cp3ban_i2c_sem_id[BSP_CP3BAN_MAX_BOARD_NUM + 1] = {0};

/* 信号量的创建标记.该信号量用来保护读写AD9557 */
static WORD32 g_bsp_cp3ban_ad9557_sem_flag[BSP_CP3BAN_MAX_BOARD_NUM + 1] = {0, 0, 0, 0, 0};

/* 信号量ID.该信号量用来保护读写AD9557 */
static WORD32 g_bsp_cp3ban_ad9557_sem_id[BSP_CP3BAN_MAX_BOARD_NUM + 1] = {0, 0, 0, 0, 0};

/* 信号量的创建标记.该信号量用来保护读写FPGA */
static WORD32 g_bsp_cp3ban_fpga_sem_flag[BSP_CP3BAN_MAX_BOARD_NUM + 1] = {0, 0, 0, 0, 0};

/* 信号量ID.该信号量用来保护读写FPGA */
static WORD32 g_bsp_cp3ban_fpga_sem_id[BSP_CP3BAN_MAX_BOARD_NUM + 1] = {0, 0, 0, 0, 0};

/* 调试光模块 */
static WORD32 g_bsp_cp3ban_sfp_debug[BSP_CP3BAN_MAX_BOARD_NUM + 1][BSP_CP3BAN_MAX_STM_PORT_NUM + 1] = {{0}};

/* 保存光模块信息 */
static T_BSP_OPT_INFO* g_bsp_cp3ban_sfp_info[BSP_CP3BAN_MAX_BOARD_NUM] = {NULL, NULL, NULL, NULL};

/* SFP线程的状态,具体参见定义T_BSP_CP3BAN_THREAD_STATE. */
static WORD32 g_bsp_cp3banSfpThreadState[BSP_CP3BAN_MAX_BOARD_NUM + 1] = {0};

/* SFP线程的创建标记 */
static WORD32 g_bsp_cp3banSfpThreadFlag[BSP_CP3BAN_MAX_BOARD_NUM + 1] = {0};

/* g_bsp_cp3ban_sfp_thread用来保存SFP线程的信息 */
static struct mod_thread g_bsp_cp3ban_sfp_thread[BSP_CP3BAN_MAX_BOARD_NUM] = {{0}};

/* SFP线程的threadId */
static pthread_t g_bsp_cp3ban_sfp_thread_id[BSP_CP3BAN_MAX_BOARD_NUM] = {0}; 

/* SFP线程的TID */
static int g_bsp_cp3ban_sfp_tid[BSP_CP3BAN_MAX_BOARD_NUM] = {0};

/* 使能SFP标记,默认非使能.在初始化单板时,使能标记;在卸载单板时,非使能标记.*/
static WORD32 g_bsp_cp3ban_sfp_flag[BSP_CP3BAN_MAX_BOARD_NUM + 1] = {0};  

/* FPGA加载标记,0表示未加载,非0表示已经加载 */
static WORD32 g_bsp_cp3banFpgaLoadFlag[BSP_CP3BAN_MAX_BOARD_NUM + 1] = {0}; 

/* EPLD的FLASH设备创建标记 */
static WORD32 g_bsp_cp3banEpldFlashDevFlag[BSP_CP3BAN_MAX_BOARD_NUM + 1] = {0};

/* 1G ethernet CP3BAN单板的MAX3997配置表 */
static T_BSP_MAX3997CFG g_bsp_cp3ban_1g_eth_max3997Cfg[BSP_CP3BAN_MAX3997_NUM] = {{0, {0xfc, 0x77, 0x77, 0x05, 0xff, 0x0f, 0x77, 0x77}}};

/* 10G ethernet CP3BAN单板的MAX3997配置表 */
static T_BSP_MAX3997CFG g_bsp_cp3ban_10g_eth_max3997Cfg[BSP_CP3BAN_MAX3997_NUM] = {{0, {0xf0, 0x00, 0x77, 0x05, 0xff, 0x0f, 0x77, 0x77}}};

/* CP3BAN单板的1号AD9557时钟芯片的寄存器配置表.用于监控155M时钟. *//* 不要修改0x0000和0x0005寄存器 */
static T_BSP_CP3BAN_AD9557_CFG g_bsp_cp3ban_ad9557_1_reg[] = 
{
    {0x0A01,0x20,1},{0x0A02,0x02,1},{0x0405,0x20,1},{0x0005,0x01,1},{0x0000,0x80,1}, 
    {0x0002,0x00,1},{0x0003,0x00,1},{0x0004,0x00,1},{0x0005,0x00,1},{0x0006,0x00,1}, 
    {0x0007,0x00,1},{0x000A,0x02,1},{0x000B,0x00,1},{0x0100,0x10,1},{0x0101,0x01,1}, 
    {0x0103,0xD0,1},{0x0104,0x12,1},{0x0105,0x13,1},{0x0106,0x32,1},{0x0107,0x00,1}, 
    {0x0108,0x00,1},{0x0200,0x00,1},{0x0201,0xB0,1},{0x0202,0xB1,1},{0x0203,0xC0,1}, 
    {0x0204,0xC1,1},{0x0205,0xB2,1},{0x0206,0xB3,1},{0x0207,0xC2,1},{0x0208,0xC3,1}, 
    {0x0209,0x1F,1},{0x020A,0x00,1},{0x020B,0x00,1},{0x020C,0x00,1},{0x020D,0x00,1}, 
    {0x020E,0x00,1},{0x020F,0x00,1},{0x0210,0x00,1},{0x0211,0x00,1},{0x0300,0x6E,1}, 
    {0x0301,0x64,1},{0x0302,0xB2,1},{0x0303,0x24,1},{0x0304,0x10,1},{0x0305,0x00,1}, 
    {0x0306,0x00,1},{0x0307,0x00,1},{0x0308,0x00,1},{0x0309,0xFF,1},{0x030A,0xFF,1}, 
    {0x030B,0xFF,1},{0x030C,0x00,1},{0x030D,0x00,1},{0x030E,0x00,1},{0x030F,0x00,1}, 
    {0x0310,0x00,1},{0x0311,0x00,1},{0x0312,0x00,1},{0x0313,0x00,1},{0x0314,0x0A,1}, 
    {0x0315,0x00,1},{0x0316,0x00,1},{0x0317,0x8C,1},{0x0318,0xAD,1},{0x0319,0x4C,1}, 
    {0x031A,0xF5,1},{0x031B,0xCB,1},{0x031C,0x73,1},{0x031D,0x24,1},{0x031E,0xD8,1}, 
    {0x031F,0x59,1},{0x0320,0xD2,1},{0x0321,0x8D,1},{0x0322,0x5A,1},{0x0323,0x24,1}, 
    {0x0324,0x8C,1},{0x0325,0x49,1},{0x0326,0x55,1},{0x0327,0xC9,1},{0x0328,0x7B,1}, 
    {0x0329,0x9C,1},{0x032A,0xFA,1},{0x032B,0x55,1},{0x032C,0xEA,1},{0x032D,0xE2,1}, 
    {0x032E,0x57,1},{0x0005,0x01,1},{0x0400,0x81,1},{0x0401,0x14,1},{0x0402,0x00,1},
    {0x0403,0x07,1},{0x0404,0x00,1},{0x0405,0x20,1},{0x0406,0x00,1},{0x0407,0x12,1},
    {0x0408,0x00,1},{0x0500,0x02,1},{0x0501,0xC3,1},{0x0502,0x2F,1},{0x0503,0x00,1},
    {0x0504,0x00,1},{0x0505,0x23,1},{0x0506,0x10,1},{0x0507,0x05,1},{0x0508,0x00,1},
    {0x0509,0x00,1},{0x050A,0x10,1},{0x050B,0x10,1},{0x050C,0x17,1},{0x050D,0x00,1},
    {0x050E,0x00,1},{0x050F,0x10,1},{0x0510,0x17,1},{0x0511,0x00,1},{0x0512,0x00,1},
    {0x0513,0x00,1},{0x0514,0x00,1},{0x0515,0x00,1},{0x0600,0x00,1},{0x0601,0x0F,1},
    {0x0602,0x00,1},{0x0603,0x00,1},{0x0640,0x00,1},{0x0641,0x00,1},{0x0700,0x00,1},
    {0x0701,0x5A,1},{0x0702,0x62,1},{0x0703,0x02,1},{0x0704,0x00,1},{0x0705,0x20,1},
    {0x0706,0x4E,1},{0x0707,0x00,1},{0x0708,0x10,1},{0x0709,0x27,1},{0x070A,0x00,1},
    {0x070B,0x0A,1},{0x070C,0x00,1},{0x070D,0x00,1},{0x070E,0x00,1},{0x070F,0xF4,1},
    {0x0710,0x01,1},{0x0711,0x00,1},{0x0712,0xF9,1},{0x0713,0x00,1},{0x0714,0x00,1},
    {0x0715,0x49,1},{0x0716,0x07,1},{0x0717,0x00,1},{0x0718,0xEE,1},{0x0719,0x02,1},
    {0x071A,0x00,1},{0x071B,0x35,1},{0x071C,0x0C,1},{0x071D,0x00,1},{0x071E,0xBC,1},
    {0x071F,0x02,1},{0x0720,0x0A,1},{0x0721,0x0A,1},{0x0722,0xBC,1},{0x0723,0x02,1},
    {0x0724,0x00,1},{0x0725,0x0A,1},{0x0726,0x0A,1},{0x0740,0x00,1},{0x0741,0x5A,1},
    {0x0742,0x62,1},{0x0743,0x02,1},{0x0744,0x00,1},{0x0745,0x40,1},{0x0746,0x9C,1},
    {0x0747,0x00,1},{0x0748,0x20,1},{0x0749,0x4E,1},{0x074A,0x00,1},{0x074B,0x0A,1},
    {0x074C,0x00,1},{0x074D,0x00,1},{0x074E,0x00,1},{0x074F,0xF4,1},{0x0750,0x01,1},
    {0x0751,0x00,1},{0x0752,0xF9,1},{0x0753,0x00,1},{0x0754,0x00,1},{0x0755,0x49,1},
    {0x0756,0x07,1},{0x0757,0x00,1},{0x0758,0xEE,1},{0x0759,0x02,1},{0x075A,0x00,1},
    {0x075B,0x35,1},{0x075C,0x0C,1},{0x075D,0x00,1},{0x075E,0xBC,1},{0x075F,0x02,1},
    {0x0760,0x0A,1},{0x0761,0x0A,1},{0x0762,0xBC,1},{0x0763,0x02,1},{0x0764,0x00,1},
    {0x0765,0x0A,1},{0x0766,0x0A,1},{0x0780,0x00,1},{0x0781,0x5A,1},{0x0782,0x62,1},
    {0x0783,0x02,1},{0x0784,0x00,1},{0x0785,0x14,1},{0x0786,0x00,1},{0x0787,0x00,1},
    {0x0788,0x0A,1},{0x0789,0x00,1},{0x078A,0x00,1},{0x078B,0x0A,1},{0x078C,0x00,1},
    {0x078D,0x00,1},{0x078E,0x00,1},{0x078F,0xF4,1},{0x0790,0x01,1},{0x0791,0x00,1},
    {0x0792,0xF9,1},{0x0793,0x00,1},{0x0794,0x00,1},{0x0795,0x49,1},{0x0796,0x07,1},
    {0x0797,0x00,1},{0x0798,0xEE,1},{0x0799,0x02,1},{0x079A,0x00,1},{0x079B,0x35,1},
    {0x079C,0x0C,1},{0x079D,0x00,1},{0x079E,0xBC,1},{0x079F,0x02,1},{0x07A0,0x0A,1},
    {0x07A1,0x0A,1},{0x07A2,0xBC,1},{0x07A3,0x02,1},{0x07A4,0x00,1},{0x07A5,0x0A,1},
    {0x07A6,0x0A,1},{0x07C0,0x00,1},{0x07C1,0x5A,1},{0x07C2,0x62,1},{0x07C3,0x02,1},
    {0x07C4,0x00,1},{0x07C5,0x14,1},{0x07C6,0x00,1},{0x07C7,0x00,1},{0x07C8,0x0A,1},
    {0x07C9,0x00,1},{0x07CA,0x00,1},{0x07CB,0x0A,1},{0x07CC,0x00,1},{0x07CD,0x00,1},
    {0x07CE,0x00,1},{0x07CF,0xF4,1},{0x07D0,0x01,1},{0x07D1,0x00,1},{0x07D2,0xF9,1},
    {0x07D3,0x00,1},{0x07D4,0x00,1},{0x07D5,0x49,1},{0x07D6,0x07,1},{0x07D7,0x00,1},
    {0x07D8,0xEE,1},{0x07D9,0x02,1},{0x07DA,0x00,1},{0x07DB,0x35,1},{0x07DC,0x0C,1},
    {0x07DD,0x00,1},{0x07DE,0xBC,1},{0x07DF,0x02,1},{0x07E0,0x0A,1},{0x07E1,0x0A,1},
    {0x07E2,0xBC,1},{0x07E3,0x02,1},{0x07E4,0x00,1},{0x07E5,0x0A,1},{0x07E6,0x0A,1},
    {0x0A00,0x00,1},{0x0A01,0x00,1},{0x0A02,0x00,1},{0x0A03,0x00,1},{0x0A04,0x00,1},
    {0x0A05,0x00,1},{0x0A06,0x00,1},{0x0A07,0x00,1},{0x0A08,0x00,1},{0x0A09,0x00,1},
    {0x0A0A,0x00,1},{0x0A0B,0x00,1},{0x0A0C,0x00,1},{0x0A0D,0x00,1},{0x0C00,0x00,1},
    {0x0C01,0x00,1},{0x0C02,0x00,1},{0x0C03,0x00,1},{0x0C04,0x00,1},{0x0C05,0x00,1},
    {0x0C06,0x00,1},{0x0C07,0x00,1},{0x0C08,0x00,1},{0x0D00,0x00,1},{0x0D01,0x00,1},
    {0x0D02,0x00,1},{0x0D03,0x00,1},{0x0D04,0x00,1},{0x0D05,0x00,1},{0x0D06,0x00,1},
    {0x0D07,0x00,1},{0x0D08,0x00,1},{0x0D09,0x00,1},{0x0D0A,0x00,1},{0x0D0B,0x00,1},
    {0x0D0C,0x00,1},{0x0D0D,0x00,1},{0x0D0E,0x00,1},{0x0D0F,0x00,1},{0x0D10,0x00,1},
    {0x0E00,0x00,1},{0x0E01,0x00,1},{0x0E02,0x00,1},{0x0E03,0x00,1},{0x0E10,0x01,1},
    {0x0E11,0x00,1},{0x0E12,0x06,1},{0x0E13,0x08,1},{0x0E14,0x01,1},{0x0E15,0x00,1},
    {0x0E16,0x80,1},{0x0E17,0x11,1},{0x0E18,0x02,1},{0x0E19,0x00,1},{0x0E1A,0x2E,1},
    {0x0E1B,0x03,1},{0x0E1C,0x00,1},{0x0E1D,0x08,1},{0x0E1E,0x04,1},{0x0E1F,0x00,1},
    {0x0E20,0x15,1},{0x0E21,0x05,1},{0x0E22,0x00,1},{0x0E23,0x80,1},{0x0E24,0x03,1},
    {0x0E25,0x06,1},{0x0E26,0x00,1},{0x0E27,0x01,1},{0x0E28,0x06,1},{0x0E29,0x40,1},
    {0x0E2A,0x26,1},{0x0E2B,0x07,1},{0x0E2C,0x00,1},{0x0E2D,0x26,1},{0x0E2E,0x07,1},
    {0x0E2F,0x40,1},{0x0E30,0x26,1},{0x0E31,0x07,1},{0x0E32,0x80,1},{0x0E33,0x26,1},
    {0x0E34,0x07,1},{0x0E35,0xC0,1},{0x0E36,0x80,1},{0x0E37,0x0D,1},{0x0E38,0x0A,1},
    {0x0E39,0x00,1},{0x0E3A,0xA0,1},{0x0E3B,0x80,1},{0x0E3C,0xFF,1},{0x0E3D,0x00,1},
    {0x0E3E,0x00,1},{0x0E3F,0x00,1},{0x0E40,0x00,1},{0x0E41,0x00,1},{0x0E42,0x00,1},
    {0x0E43,0x00,1},{0x0E44,0x00,1},{0x0E45,0x00,1},{0x0005,0x01,1},{0x0405,0x21,1},
    {0x0005,0x01,1},{0x0500,0x00,1},{0x0005,0x01,1},{0x0A02,0x02,1},{0x0005,0x01,1},
    {0x0A02,0x00,1},{0x0005,0x01,1},{0x0A01,0x04,1},{0x0005,0x01,1} 
};                              

/* CP3BAN单板的2号AD9557时钟芯片的寄存器配置表.用于监控644M时钟. *//* 不要修改0x0000和0x0005寄存器 */
static T_BSP_CP3BAN_AD9557_CFG g_bsp_cp3ban_ad9557_2_reg[] =
{
    {0x0A01,0x20,1},{0x0A02,0x02,1},{0x0405,0x20,1},{0x0005,0x01,1},
    {0x0000,0x80,1},{0x0002,0x00,1},{0x0003,0x00,1},{0x0004,0x00,1},
    {0x0005,0x00,1},{0x0006,0x00,1},{0x0007,0x00,1},{0x000A,0x02,1},
    {0x000B,0x00,1},{0x0100,0x10,1},{0x0101,0x01,1},{0x0103,0xD0,1},
    {0x0104,0x12,1},{0x0105,0x13,1},{0x0106,0x32,1},{0x0107,0x00,1},
    {0x0108,0x00,1},{0x0200,0x00,1},{0x0201,0xB0,1},{0x0202,0xB1,1},
    {0x0203,0xC0,1},{0x0204,0xC1,1},{0x0205,0xB2,1},{0x0206,0xB3,1},
    {0x0207,0xC2,1},{0x0208,0xC3,1},{0x0209,0x1F,1},{0x020A,0x00,1},
    {0x020B,0x00,1},{0x020C,0x00,1},{0x020D,0x00,1},{0x020E,0x00,1},
    {0x020F,0x00,1},{0x0210,0x00,1},{0x0211,0x00,1},{0x0300,0x6E,1},
    {0x0301,0x76,1},{0x0302,0x95,1},{0x0303,0x11,1},{0x0304,0x10,1},
    {0x0305,0x00,1},{0x0306,0x00,1},{0x0307,0x00,1},{0x0308,0x00,1},
    {0x0309,0xFF,1},{0x030A,0xFF,1},{0x030B,0xFF,1},{0x030C,0x00,1},
    {0x030D,0x00,1},{0x030E,0x00,1},{0x030F,0x00,1},{0x0310,0x00,1},
    {0x0311,0x00,1},{0x0312,0x00,1},{0x0313,0x00,1},{0x0314,0x0A,1},
    {0x0315,0x00,1},{0x0316,0x00,1},{0x0317,0x8C,1},{0x0318,0xAD,1},
    {0x0319,0x4C,1},{0x031A,0xF5,1},{0x031B,0xCB,1},{0x031C,0x73,1},
    {0x031D,0x24,1},{0x031E,0xD8,1},{0x031F,0x59,1},{0x0320,0xD2,1},
    {0x0321,0x8D,1},{0x0322,0x5A,1},{0x0323,0x24,1},{0x0324,0x8C,1},
    {0x0325,0x49,1},{0x0326,0x55,1},{0x0327,0xC9,1},{0x0328,0x7B,1},
    {0x0329,0x9C,1},{0x032A,0xFA,1},{0x032B,0x55,1},{0x032C,0xEA,1},
    {0x032D,0xE2,1},{0x032E,0x57,1},{0x0005,0x01,1},{0x0400,0x81,1},
    {0x0401,0x14,1},{0x0402,0x00,1},{0x0403,0x07,1},{0x0404,0x00,1},
    {0x0405,0x20,1},{0x0406,0x00,1},{0x0407,0x41,1},{0x0408,0x02,1},
    {0x0500,0x02,1},{0x0501,0x23,1},{0x0502,0x01,1},{0x0503,0x00,1},
    {0x0504,0x00,1},{0x0505,0x22,1},{0x0506,0x10,1},{0x0507,0x00,1},
    {0x0508,0x08,1},{0x0509,0x00,1},{0x050A,0x10,1},{0x050B,0x10,1},
    {0x050C,0x00,1},{0x050D,0x00,1},{0x050E,0x00,1},{0x050F,0x10,1},
    {0x0510,0x00,1},{0x0511,0x00,1},{0x0512,0x00,1},{0x0513,0x00,1},
    {0x0514,0x00,1},{0x0515,0x00,1},{0x0600,0x00,1},{0x0601,0x0F,1},
    {0x0602,0x00,1},{0x0603,0x00,1},{0x0640,0x00,1},{0x0641,0x00,1},
    {0x0700,0x00,1},{0x0701,0x5A,1},{0x0702,0x62,1},{0x0703,0x02,1},
    {0x0704,0x00,1},{0x0705,0x20,1},{0x0706,0x4E,1},{0x0707,0x00,1},
    {0x0708,0x10,1},{0x0709,0x27,1},{0x070A,0x00,1},{0x070B,0x0A,1},
    {0x070C,0x00,1},{0x070D,0x00,1},{0x070E,0x00,1},{0x070F,0xF4,1},
    {0x0710,0x01,1},{0x0711,0x00,1},{0x0712,0xF9,1},{0x0713,0x00,1},
    {0x0714,0x00,1},{0x0715,0x8C,1},{0x0716,0x07,1},{0x0717,0x00,1},
    {0x0718,0x26,1},{0x0719,0x00,1},{0x071A,0x00,1},{0x071B,0x40,1},
    {0x071C,0x00,1},{0x071D,0x00,1},{0x071E,0xBC,1},{0x071F,0x02,1},
    {0x0720,0x0A,1},{0x0721,0x0A,1},{0x0722,0xBC,1},{0x0723,0x02,1},
    {0x0724,0x00,1},{0x0725,0x0A,1},{0x0726,0x0A,1},{0x0740,0x00,1},
    {0x0741,0x5A,1},{0x0742,0x62,1},{0x0743,0x02,1},{0x0744,0x00,1},
    {0x0745,0x40,1},{0x0746,0x9C,1},{0x0747,0x00,1},{0x0748,0x20,1},
    {0x0749,0x4E,1},{0x074A,0x00,1},{0x074B,0x0A,1},{0x074C,0x00,1},
    {0x074D,0x00,1},{0x074E,0x00,1},{0x074F,0xF4,1},{0x0750,0x01,1},
    {0x0751,0x00,1},{0x0752,0xF9,1},{0x0753,0x00,1},{0x0754,0x00,1},
    {0x0755,0x8C,1},{0x0756,0x07,1},{0x0757,0x00,1},{0x0758,0x26,1},
    {0x0759,0x00,1},{0x075A,0x00,1},{0x075B,0x40,1},{0x075C,0x00,1},
    {0x075D,0x00,1},{0x075E,0xBC,1},{0x075F,0x02,1},{0x0760,0x0A,1},
    {0x0761,0x0A,1},{0x0762,0xBC,1},{0x0763,0x02,1},{0x0764,0x00,1},
    {0x0765,0x0A,1},{0x0766,0x0A,1},{0x0780,0x00,1},{0x0781,0x5A,1},
    {0x0782,0x62,1},{0x0783,0x02,1},{0x0784,0x00,1},{0x0785,0x14,1},
    {0x0786,0x00,1},{0x0787,0x00,1},{0x0788,0x0A,1},{0x0789,0x00,1},
    {0x078A,0x00,1},{0x078B,0x0A,1},{0x078C,0x00,1},{0x078D,0x00,1},
    {0x078E,0x00,1},{0x078F,0xF4,1},{0x0790,0x01,1},{0x0791,0x00,1},
    {0x0792,0xF9,1},{0x0793,0x00,1},{0x0794,0x00,1},{0x0795,0x8C,1},
    {0x0796,0x07,1},{0x0797,0x00,1},{0x0798,0x26,1},{0x0799,0x00,1},
    {0x079A,0x00,1},{0x079B,0x40,1},{0x079C,0x00,1},{0x079D,0x00,1},
    {0x079E,0xBC,1},{0x079F,0x02,1},{0x07A0,0x0A,1},{0x07A1,0x0A,1},
    {0x07A2,0xBC,1},{0x07A3,0x02,1},{0x07A4,0x00,1},{0x07A5,0x0A,1},
    {0x07A6,0x0A,1},{0x07C0,0x00,1},{0x07C1,0x5A,1},{0x07C2,0x62,1},
    {0x07C3,0x02,1},{0x07C4,0x00,1},{0x07C5,0x14,1},{0x07C6,0x00,1},
    {0x07C7,0x00,1},{0x07C8,0x0A,1},{0x07C9,0x00,1},{0x07CA,0x00,1},
    {0x07CB,0x0A,1},{0x07CC,0x00,1},{0x07CD,0x00,1},{0x07CE,0x00,1},
    {0x07CF,0xF4,1},{0x07D0,0x01,1},{0x07D1,0x00,1},{0x07D2,0xF9,1},
    {0x07D3,0x00,1},{0x07D4,0x00,1},{0x07D5,0x8C,1},{0x07D6,0x07,1},
    {0x07D7,0x00,1},{0x07D8,0x26,1},{0x07D9,0x00,1},{0x07DA,0x00,1},
    {0x07DB,0x40,1},{0x07DC,0x00,1},{0x07DD,0x00,1},{0x07DE,0xBC,1},
    {0x07DF,0x02,1},{0x07E0,0x0A,1},{0x07E1,0x0A,1},{0x07E2,0xBC,1},
    {0x07E3,0x02,1},{0x07E4,0x00,1},{0x07E5,0x0A,1},{0x07E6,0x0A,1},
    {0x0A00,0x00,1},{0x0A01,0x00,1},{0x0A02,0x00,1},{0x0A03,0x00,1},
    {0x0A04,0x00,1},{0x0A05,0x00,1},{0x0A06,0x00,1},{0x0A07,0x00,1},
    {0x0A08,0x00,1},{0x0A09,0x00,1},{0x0A0A,0x00,1},{0x0A0B,0x00,1},
    {0x0A0C,0x00,1},{0x0A0D,0x00,1},{0x0C00,0x00,1},{0x0C01,0x00,1},
    {0x0C02,0x00,1},{0x0C03,0x00,1},{0x0C04,0x00,1},{0x0C05,0x00,1},
    {0x0C06,0x00,1},{0x0C07,0x00,1},{0x0C08,0x00,1},{0x0D00,0x00,1},
    {0x0D01,0x00,1},{0x0D02,0x00,1},{0x0D03,0x00,1},{0x0D04,0x00,1},
    {0x0D05,0x00,1},{0x0D06,0x00,1},{0x0D07,0x00,1},{0x0D08,0x00,1},
    {0x0D09,0x00,1},{0x0D0A,0x00,1},{0x0D0B,0x00,1},{0x0D0C,0x00,1},
    {0x0D0D,0x00,1},{0x0D0E,0x00,1},{0x0D0F,0x00,1},{0x0D10,0x00,1},
    {0x0E00,0x00,1},{0x0E01,0x00,1},{0x0E02,0x00,1},{0x0E03,0x00,1},
    {0x0E10,0x01,1},{0x0E11,0x00,1},{0x0E12,0x06,1},{0x0E13,0x08,1},
    {0x0E14,0x01,1},{0x0E15,0x00,1},{0x0E16,0x80,1},{0x0E17,0x11,1},
    {0x0E18,0x02,1},{0x0E19,0x00,1},{0x0E1A,0x2E,1},{0x0E1B,0x03,1},
    {0x0E1C,0x00,1},{0x0E1D,0x08,1},{0x0E1E,0x04,1},{0x0E1F,0x00,1},
    {0x0E20,0x15,1},{0x0E21,0x05,1},{0x0E22,0x00,1},{0x0E23,0x80,1},
    {0x0E24,0x03,1},{0x0E25,0x06,1},{0x0E26,0x00,1},{0x0E27,0x01,1},
    {0x0E28,0x06,1},{0x0E29,0x40,1},{0x0E2A,0x26,1},{0x0E2B,0x07,1},
    {0x0E2C,0x00,1},{0x0E2D,0x26,1},{0x0E2E,0x07,1},{0x0E2F,0x40,1},
    {0x0E30,0x26,1},{0x0E31,0x07,1},{0x0E32,0x80,1},{0x0E33,0x26,1},
    {0x0E34,0x07,1},{0x0E35,0xC0,1},{0x0E36,0x80,1},{0x0E37,0x0D,1},
    {0x0E38,0x0A,1},{0x0E39,0x00,1},{0x0E3A,0xA0,1},{0x0E3B,0x80,1},
    {0x0E3C,0xFF,1},{0x0E3D,0x00,1},{0x0E3E,0x00,1},{0x0E3F,0x00,1},
    {0x0E40,0x00,1},{0x0E41,0x00,1},{0x0E42,0x00,1},{0x0E43,0x00,1},
    {0x0E44,0x00,1},{0x0E45,0x00,1},{0x0005,0x01,1},{0x0405,0x21,1},
    {0x0005,0x01,1},{0x0500,0x00,1},{0x0005,0x01,1},{0x0A02,0x02,1},
    {0x0005,0x01,1},{0x0A02,0x00,1},{0x0005,0x01,1},{0x0A01,0x04,1},
    {0x0005,0x01,1}
};


/**************************************************************************
* 函数名称: BSP_cp3banMaxStmPortIdGet
* 功能描述: 获取CP3BAN单板的STM端口的最大编号.
* 访问的表: 无.
* 修改的表: 无.
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: *pdwMaxPortId: 保存STM端口的最大编号. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-08-19   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banMaxStmPortIdGet(WORD32 dwSubslotId, WORD32 *pdwMaxPortId)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    WORD32 dwBoardType = 0;

    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pdwMaxPortId);
    
    dwRetValue = BSP_cp3ban_boardTypeGet(dwSubslotId, &dwBoardType);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_COMMON_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_boardTypeGet() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return dwRetValue;
    }
    if (BSP_CP3BAN_8PORT_BOARD == dwBoardType)  /* 8端口的CP3BAN单板 */
    {
        *pdwMaxPortId = BSP_CP3BAN_MAX_STM1_8PORT_ID;
    }
    else if (BSP_CP3BAN_4PORT_BOARD == dwBoardType)  /* 4端口的CP3BAN单板 */
    {
        *pdwMaxPortId = BSP_CP3BAN_MAX_STM1_4PORT_ID;
    }
    else
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_COMMON_MSG, "ERROR: %s line %d, subslotId %u, invalid board type, boardType=%u.\n", __FILE__, __LINE__, dwSubslotId, dwBoardType);
        return BSP_E_CP3BAN_INVALID_BOARD_TYPE;
    }
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banUninitFlagSet
* 功能描述: 设置CP3BAN单板的卸载标记.
* 访问的表: 无.
* 修改的表: 无.
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
*           dwFlag: 卸载标记,具体参见T_BSP_CP3BAN_UNINIT_FLAG定义.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-07-28   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banUninitFlagSet(WORD32 dwSubslotId, WORD32 dwFlag)
{
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    
    g_bsp_cp3ban_uninit_Flag[dwSubslotId] = dwFlag;
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banUninitFlagGet
* 功能描述: 获取CP3BAN单板的卸载标记.
* 访问的表: 无.
* 修改的表: 无.
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: *pdwFlag: 保存卸载标记,具体参见T_BSP_CP3BAN_UNINIT_FLAG定义. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-07-28   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banUninitFlagGet(WORD32 dwSubslotId, WORD32 *pdwFlag)
{
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pdwFlag);
    
    *pdwFlag = g_bsp_cp3ban_uninit_Flag[dwSubslotId];
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banSfpDebugFlagSet
* 功能描述: 设置光模块的调试开关的值.
* 访问的表: 无.
* 修改的表: 无.
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
*           dwPortId: STM端口的编号,取值为1~8.
*           dwFlag: 调试开关的值.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-24   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banSfpDebugFlagSet(WORD32 dwSubslotId, WORD32 dwPortId, WORD32 dwFlag)
{
    WORD32 rv = BSP_E_CP3BAN_OK;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    rv = BSP_cp3banPortIdCheck(dwSubslotId, dwPortId);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banPortIdCheck() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    
    g_bsp_cp3ban_sfp_debug[dwSubslotId][dwPortId] = dwFlag;
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banSfpDebugFlagGet
* 功能描述: 获取光模块的调试开关的值.
* 访问的表: 无.
* 修改的表: 无.
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
*           dwPortId: STM端口的编号,取值为1~8.
* 输出参数: *pDwFlag: 保存调试开关的值. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-24   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banSfpDebugFlagGet(WORD32 dwSubslotId, WORD32 dwPortId, WORD32 *pDwFlag)
{
    WORD32 rv = BSP_E_CP3BAN_OK;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    rv = BSP_cp3banPortIdCheck(dwSubslotId, dwPortId);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banPortIdCheck() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pDwFlag);
    
    *pDwFlag = 0;  /* 先清零再赋值 */
    *pDwFlag = g_bsp_cp3ban_sfp_debug[dwSubslotId][dwPortId];
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banSfpMemAllocate
* 功能描述: 动态分配内存
* 访问的表: 
* 修改的表: 
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 该内存用来保存光模块SFP的信息.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-24   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banSfpMemAllocate(WORD32 dwSubslotId)
{
    WORD32 dwSubslotIdx = 0;  /* 单板索引号,从0开始取值 */
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    dwSubslotIdx = dwSubslotId - 1;
    BSP_CHECK_CP3BAN_SUBSLOT_INDEX(dwSubslotIdx);
    
    if (NULL == g_bsp_cp3ban_sfp_info[dwSubslotIdx])
    {
        g_bsp_cp3ban_sfp_info[dwSubslotIdx] = (T_BSP_OPT_INFO *)(malloc(BSP_CP3BAN_MAX_STM_PORT_NUM * sizeof(T_BSP_OPT_INFO)));
        if (NULL == g_bsp_cp3ban_sfp_info[dwSubslotIdx])
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, allocate memory for g_bsp_cp3ban_sfp_info[%u] failed.\n", __FILE__, __LINE__, dwSubslotIdx);
            return BSP_E_CP3BAN_ALLOC_MEM_FAIL;
        }
        memset(g_bsp_cp3ban_sfp_info[dwSubslotIdx], 0, (BSP_CP3BAN_MAX_STM_PORT_NUM * sizeof(T_BSP_OPT_INFO)));
    }

    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banSfpMemFree
* 功能描述: 释放已经分配的内存空间.
* 访问的表: 
* 修改的表: 
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 该内存用来保存光模块SFP的信息.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-24   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banSfpMemFree(WORD32 dwSubslotId)
{
    WORD32 dwSubslotIdx = 0;  /* 单板索引号,从0开始取值 */
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    dwSubslotIdx = dwSubslotId - 1;
    BSP_CHECK_CP3BAN_SUBSLOT_INDEX(dwSubslotIdx);
    
    if (NULL != g_bsp_cp3ban_sfp_info[dwSubslotIdx])
    {
        free(g_bsp_cp3ban_sfp_info[dwSubslotIdx]);
        g_bsp_cp3ban_sfp_info[dwSubslotIdx] = NULL; /* 防止成为野指针 */
    }
    BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, subslotId %u, Free memory successfully.\n", __FILE__, __LINE__, dwSubslotId);
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banSfpSemFlagGet
* 功能描述: 获取信号量的创建标记.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: *pdwFlag: 保存信号量的创建标记.
* 返 回 值: 
* 其它说明: 该信号量用来保护访问SFP.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-04-10   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banSfpSemFlagGet(WORD32 dwSubslotId, WORD32 *pdwFlag)
{
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pdwFlag);
    
    *pdwFlag = g_bsp_cp3ban_sfp_sem_flag[dwSubslotId];
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banSfpSemIdGet
* 功能描述: 获取信号量ID.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: *pdwSemId: 保存信号量ID.
* 返 回 值: 
* 其它说明: 该信号量用来保护访问SFP.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-04-10   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banSfpSemIdGet(WORD32 dwSubslotId, WORD32 *pdwSemId)
{
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pdwSemId);
    
    *pdwSemId = g_bsp_cp3ban_sfp_sem_id[dwSubslotId];
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banSfpSemCreate
* 功能描述: 创建信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护访问SFP.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-04-10   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banSfpSemCreate(WORD32 dwSubslotId)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    
    if (BSP_CP3BAN_SEM_NONE_EXISTED == g_bsp_cp3ban_sfp_sem_flag[dwSubslotId])    
    {
        dwRetValue = BSP_OalCreateSem(1, MUTEX_STYLE, &(g_bsp_cp3ban_sfp_sem_id[dwSubslotId]));
        if (BSP_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_OalCreateSem() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
            return BSP_E_CP3BAN_CREATE_SEM_FAIL;
        }
        g_bsp_cp3ban_sfp_sem_flag[dwSubslotId] = BSP_CP3BAN_SEM_EXISTED;
    }
    
    return BSP_E_CP3BAN_OK;  /* 返回成功 */
}


/**************************************************************************
* 函数名称: BSP_cp3banSfpSemDestroy
* 功能描述: 销毁信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护访问SFP.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-04-10   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banSfpSemDestroy(WORD32 dwSubslotId)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    
    /* 销毁信号量之前,必须先LOCK一下信号量 */
    dwRetValue = BSP_cp3banSfpSemGet(dwSubslotId);
    if (BSP_E_CP3BAN_SEM_NONE_EXISTED == dwRetValue)
    {
        BSP_Print(BSP_DEBUG_ALL, "WARNING: %s line %d, g_bsp_cp3ban_sfp_sem_id[%u] has been destroyed.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_OK;
    }
    else if (BSP_E_CP3BAN_OK == dwRetValue)
    {
        ;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banSfpSemGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return dwRetValue;
    }
    
    if (BSP_CP3BAN_SEM_NONE_EXISTED != g_bsp_cp3ban_sfp_sem_flag[dwSubslotId])    
    {
        dwRetValue = BSP_OalDestroySem(g_bsp_cp3ban_sfp_sem_id[dwSubslotId]);
        if (BSP_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_OalDestroySem() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
            return BSP_E_CP3BAN_DESTROY_SEM_FAIL;
        }
        g_bsp_cp3ban_sfp_sem_id[dwSubslotId] = 0;
        g_bsp_cp3ban_sfp_sem_flag[dwSubslotId] = BSP_CP3BAN_SEM_NONE_EXISTED;
    }
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banSfpSemGet
* 功能描述: 获取信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护SFP访问.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-04-10   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banSfpSemGet(WORD32 dwSubslotId)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    
    if (BSP_CP3BAN_SEM_NONE_EXISTED != g_bsp_cp3ban_sfp_sem_flag[dwSubslotId])    
    {
        dwRetValue = BSP_OalSemP(g_bsp_cp3ban_sfp_sem_id[dwSubslotId], WAIT_FOREVER);
        if (BSP_OK != dwRetValue)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u, BSP_OalSemP() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
            return BSP_E_CP3BAN_GET_SEM_FAIL;
        }
    }
    else
    {
        return BSP_E_CP3BAN_SEM_NONE_EXISTED;
    }
    
    return BSP_E_CP3BAN_OK;  /* 返回成功 */
}


/**************************************************************************
* 函数名称: BSP_cp3banSfpSemFree
* 功能描述: 释放信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护访问SFP.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-04-10   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banSfpSemFree(WORD32 dwSubslotId)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    
    if (BSP_CP3BAN_SEM_NONE_EXISTED != g_bsp_cp3ban_sfp_sem_flag[dwSubslotId])    
    {
        dwRetValue = BSP_OalSemV(g_bsp_cp3ban_sfp_sem_id[dwSubslotId]);
        if (BSP_OK != dwRetValue)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u, BSP_OalSemV() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
            return BSP_E_CP3BAN_FREE_SEM_FAIL;
        }
    }
    else
    {
        return BSP_E_CP3BAN_SEM_NONE_EXISTED;
    }
    
    return BSP_E_CP3BAN_OK;  /* 返回成功 */
}


/**************************************************************************
* 函数名称: BSP_cp3banI2cSemFlagGet
* 功能描述: 获取信号量的创建标记.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: *pdwFlag: 保存信号量的创建标记.
* 返 回 值: 
* 其它说明: 该信号量用来保护保存I2C信息的内存.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-08-30   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banI2cSemFlagGet(WORD32 dwSubslotId, WORD32 *pdwFlag)
{
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pdwFlag);
    
    *pdwFlag = g_bsp_cp3ban_i2c_sem_flag[dwSubslotId];
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banI2cSemIdGet
* 功能描述: 获取信号量ID.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: *pdwSemId: 保存信号量ID.
* 返 回 值: 
* 其它说明: 该信号量用来保护保存I2C光模块信息的内存.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-08-30   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banI2cSemIdGet(WORD32 dwSubslotId, WORD32 *pdwSemId)
{
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pdwSemId);
    
    *pdwSemId = g_bsp_cp3ban_i2c_sem_id[dwSubslotId];
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banI2cSemCreate
* 功能描述: 创建信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护保存I2C光模块信息的内存.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-08-30   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banI2cSemCreate(WORD32 dwSubslotId)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    
    if (BSP_CP3BAN_SEM_NONE_EXISTED == g_bsp_cp3ban_i2c_sem_flag[dwSubslotId])    
    {
        dwRetValue = BSP_OalCreateSem(1, MUTEX_STYLE, &(g_bsp_cp3ban_i2c_sem_id[dwSubslotId]));
        if (BSP_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_OalCreateSem() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
            return BSP_E_CP3BAN_CREATE_SEM_FAIL;
        }
        g_bsp_cp3ban_i2c_sem_flag[dwSubslotId] = BSP_CP3BAN_SEM_EXISTED;
    }
    
    return BSP_E_CP3BAN_OK;  /* 返回成功 */
}


/**************************************************************************
* 函数名称: BSP_cp3banI2cSemDestroy
* 功能描述: 销毁信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护保存I2C光模块信息的内存.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-08-30   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banI2cSemDestroy(WORD32 dwSubslotId)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    
    /* 销毁信号量之前,必须先LOCK一下信号量 */
    dwRetValue = BSP_cp3banI2cSemGet(dwSubslotId);
    if (BSP_E_CP3BAN_SEM_NONE_EXISTED == dwRetValue)
    {
        BSP_Print(BSP_DEBUG_ALL, "WARNING: %s line %d, g_bsp_cp3ban_i2c_sem_id[%u] has been destroyed.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_OK;
    }
    else if (BSP_E_CP3BAN_OK == dwRetValue)
    {
        ;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banI2cSemGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return dwRetValue;
    }
    
    if (BSP_CP3BAN_SEM_NONE_EXISTED != g_bsp_cp3ban_i2c_sem_flag[dwSubslotId])    
    {
        dwRetValue = BSP_OalDestroySem(g_bsp_cp3ban_i2c_sem_id[dwSubslotId]);
        if (BSP_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_OalDestroySem() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
            return BSP_E_CP3BAN_DESTROY_SEM_FAIL;
        }
        g_bsp_cp3ban_i2c_sem_id[dwSubslotId] = 0;
        g_bsp_cp3ban_i2c_sem_flag[dwSubslotId] = BSP_CP3BAN_SEM_NONE_EXISTED;
    }
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banI2cSemGet
* 功能描述: 获取信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护保存I2C光模块信息的内存.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-08-30   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banI2cSemGet(WORD32 dwSubslotId)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    
    if (BSP_CP3BAN_SEM_NONE_EXISTED != g_bsp_cp3ban_i2c_sem_flag[dwSubslotId])    
    {
        dwRetValue = BSP_OalSemP(g_bsp_cp3ban_i2c_sem_id[dwSubslotId], WAIT_FOREVER);
        if (BSP_OK != dwRetValue)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u, BSP_OalSemP() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
            return BSP_E_CP3BAN_GET_SEM_FAIL;
        }
    }
    else
    {
        return BSP_E_CP3BAN_SEM_NONE_EXISTED;
    }
    
    return BSP_E_CP3BAN_OK;  /* 返回成功 */
}


/**************************************************************************
* 函数名称: BSP_cp3banI2cSemFree
* 功能描述: 释放信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护保存I2C光模块信息的内存.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-08-30   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banI2cSemFree(WORD32 dwSubslotId)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    
    if (BSP_CP3BAN_SEM_NONE_EXISTED != g_bsp_cp3ban_i2c_sem_flag[dwSubslotId])    
    {
        dwRetValue = BSP_OalSemV(g_bsp_cp3ban_i2c_sem_id[dwSubslotId]);
        if (BSP_OK != dwRetValue)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u, BSP_OalSemV() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
            return BSP_E_CP3BAN_FREE_SEM_FAIL;
        }
    }
    else
    {
        return BSP_E_CP3BAN_SEM_NONE_EXISTED;
    }
    
    return BSP_E_CP3BAN_OK;  /* 返回成功 */
}


/**************************************************************************
* 函数名称: BSP_cp3banSfpInfoGet
* 功能描述: 获取光模块的信息.
* 访问的表: 
* 修改的表: 
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
*           dwPortId: STM端口的编号,取值为1~8.
* 输出参数: *pSfpInfo: 保存光模块信息. 
* 返 回 值: 
* 其它说明: 从BSP的缓冲区中来获取光模块的信息.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-24   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banSfpInfoGet(WORD32 dwSubslotId, WORD32 dwPortId, T_BSP_OPT_INFO *pSfpInfo)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    WORD32 dwSubslotIdx = 0;  /* 单板索引号,从0开始取值 */
    WORD32 dwPortIdx = 0;     /* STM端口的索引号,从0开始取值 */
    
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pSfpInfo);
    dwSubslotIdx = dwSubslotId - 1;
    dwPortIdx = dwPortId - 1;
    BSP_CHECK_CP3BAN_SUBSLOT_INDEX(dwSubslotIdx);
    BSP_CHECK_CP3BAN_STM_PORT_INDEX(dwPortIdx);
    
    dwRetValue = BSP_cp3banI2cSemGet(dwSubslotId);  /* 获取信号量 */
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banI2cSemGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return BSP_E_CP3BAN_OK;
    }
    if (NULL != g_bsp_cp3ban_sfp_info[dwSubslotIdx])
    {
        memcpy(pSfpInfo, &(g_bsp_cp3ban_sfp_info[dwSubslotIdx][dwPortIdx]), sizeof(T_BSP_OPT_INFO));
    }
    dwRetValue = BSP_cp3banI2cSemFree(dwSubslotId);  /* 释放信号量 */
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banI2cSemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return BSP_E_CP3BAN_OK;
    }
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banSfpInfoSave
* 功能描述: 保存光模块的信息.
* 访问的表: 
* 修改的表: 
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
*           dwPortId: STM端口的编号,取值为1~8.
*           *pSfpInfo: 保存光模块信息.
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-24   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banSfpInfoSave(WORD32 dwSubslotId, 
                                       WORD32 dwPortId, 
                                       const T_BSP_OPT_INFO *pSfpInfo)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    WORD32 dwSubslotIdx = 0;  /* 单板索引号,从0开始取值 */
    WORD32 dwPortIdx = 0;     /* STM端口的索引号,从0开始取值 */
    
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pSfpInfo);
    dwSubslotIdx = dwSubslotId - 1;
    dwPortIdx = dwPortId - 1;
    BSP_CHECK_CP3BAN_SUBSLOT_INDEX(dwSubslotIdx);
    BSP_CHECK_CP3BAN_STM_PORT_INDEX(dwPortIdx);
    
    dwRetValue = BSP_cp3banI2cSemGet(dwSubslotId);  /* 获取信号量 */
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banI2cSemGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return BSP_E_CP3BAN_OK;
    }
    if (NULL != g_bsp_cp3ban_sfp_info[dwSubslotIdx])
    {
        memset(&(g_bsp_cp3ban_sfp_info[dwSubslotIdx][dwPortIdx]), 0, sizeof(T_BSP_OPT_INFO));  /* 先清零再赋值 */
        memcpy(&(g_bsp_cp3ban_sfp_info[dwSubslotIdx][dwPortIdx]), pSfpInfo, sizeof(T_BSP_OPT_INFO));
    }
    dwRetValue = BSP_cp3banI2cSemFree(dwSubslotId);  /* 释放信号量 */
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banI2cSemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return BSP_E_CP3BAN_OK;
    }
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banSfpThreadStateSet
* 功能描述: 设置SFP线程的状态.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
*           dwState: 线程的状态,具体参见T_BSP_CP3BAN_THREAD_STATE定义.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-07-29   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banSfpThreadStateSet(WORD32 dwSubslotId, WORD32 dwState)
{
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    
    g_bsp_cp3banSfpThreadState[dwSubslotId] = dwState;
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banSfpThreadStateGet
* 功能描述: 获取SFP线程的状态.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: *pdwState: 保存线程的状态,具体参见T_BSP_CP3BAN_THREAD_STATE定义. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-07-29   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banSfpThreadStateGet(WORD32 dwSubslotId, WORD32 *pdwState)
{
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pdwState);
    
    *pdwState = g_bsp_cp3banSfpThreadState[dwSubslotId];
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banSfpMonitorFlagGet
* 功能描述: 获取监测CP3BAN单板的光模块SFP的标记.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号. 
* 输出参数: *pdwFlag: 保存标记.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-24   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banSfpMonitorFlagGet(WORD32 dwSubslotId, WORD32 *pdwFlag)
{
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pdwFlag);
    
    *pdwFlag = g_bsp_cp3ban_sfp_flag[dwSubslotId];
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banSfpMonitorFlagSet
* 功能描述: 设置监测CP3BAN单板的光模块SFP的标记.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号. 
*           dwFlag: 监测标记,具体参见BSP_CP3BAN_FLAG_STATE定义.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-24   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banSfpMonitorFlagSet(WORD32 dwSubslotId, WORD32 dwFlag)
{
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    
    g_bsp_cp3ban_sfp_flag[dwSubslotId] = dwFlag;
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banSfpTidGet
* 功能描述: 获取光模块SFP监测线程的TID.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号. 
* 输出参数: *pTid: 保存线程的TID.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-24   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banSfpTidGet(WORD32 dwSubslotId, int *pTid)
{
    WORD32 dwSubslotIdx = 0;
    
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pTid);
    dwSubslotIdx = dwSubslotId - 1;
    BSP_CHECK_CP3BAN_SUBSLOT_INDEX(dwSubslotIdx);
    
    *pTid = g_bsp_cp3ban_sfp_tid[dwSubslotIdx];
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banSfpThreadIdGet
* 功能描述: 获取光模块SFP监测线程的Thread ID.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号. 
* 输出参数: *pThreadId: 保存线程的Thread ID.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-24   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banSfpThreadIdGet(WORD32 dwSubslotId, pthread_t *pThreadId)
{
    WORD32 dwSubslotIdx = 0;
    
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pThreadId);
    dwSubslotIdx = dwSubslotId - 1;
    BSP_CHECK_CP3BAN_SUBSLOT_INDEX(dwSubslotIdx);

    *pThreadId = g_bsp_cp3ban_sfp_thread_id[dwSubslotIdx];  
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banSfpMonitor
* 功能描述: 监测CP3BAN单板的光模块SFP.
* 访问的表: 无
* 修改的表: 无
* 输入参数: *pSubslotId: 保存单板的子槽位号,取值为1~4. 
* 输出参数: 无
* 返 回 值: 
* 其它说明: SFP线程的入口函数
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-24   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banSfpMonitor(VOID *pSubslotId)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;  /* 函数返回码 */
    WORD32 dwTempRv = BSP_E_CP3BAN_OK;
    WORD32 dwBoardStatus = 0;  /* CP3BAN单板的状态 */
    WORD32 dwUninitFlag = 0;  /* CP3BAN单板的卸载标记 */
    WORD32 dwInitFlag = 0;    /* CP3BAN单板的初始化标记 */
    WORD32 *pdwSubslotId = NULL; 
    WORD32 dwSubslotId = 0;  /* CP3BAN单板的子槽位号 */
    WORD32 dwSubslotIdx = 0;  /* 单板的索引号,从0开始取值 */
    WORD32 dwPortId = 0;
    WORD32 dwMaxPortId = 0;
    WORD32 dwSfpState = 0;
    T_BSP_OPT_INFO sfpInfo;
    BSP_CP3BAN_PORT_INFO portInfo;
    time_t currentTime;    /* 以秒为单位 */
    
    memset(&sfpInfo, 0, sizeof(T_BSP_OPT_INFO));    
    pdwSubslotId = (WORD32 *)pSubslotId;
    if (NULL == pdwSubslotId)
    { 
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, pdwSubslotId is NULL pointer.\n", __FILE__, __LINE__); 
        return BSP_E_CP3BAN_NULL_POINTER; 
    }
    dwSubslotId = *pdwSubslotId;
    free(pdwSubslotId);
    pdwSubslotId = NULL;  /* 释放内存空间,以防成为野指针 */
    BSP_Print(BSP_DEBUG_ALL, "%s line %d, BSP_cp3banSfpMonitor(), subslotId=%u.\n", __FILE__, __LINE__, dwSubslotId);
    if ((dwSubslotId < BSP_CP3BAN_MIN_SUBSLOT_ID) || (dwSubslotId > BSP_CP3BAN_MAX_SUBSLOT_ID)) 
    { 
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, invalid subslotId, subslotId=%u.\n", __FILE__, __LINE__, dwSubslotId); 
        return DRV_TDM_INVALID_SUBSLOT_ID; 
    }
    dwSubslotIdx = dwSubslotId - 1;
    BSP_CHECK_CP3BAN_SUBSLOT_INDEX(dwSubslotIdx);
    
    g_bsp_cp3ban_sfp_tid[dwSubslotIdx] = __gettid();  /* save TID */
    
    dwRetValue = BSP_cp3banMaxStmPortIdGet(dwSubslotId, &dwMaxPortId);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banMaxStmPortIdGet() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return dwRetValue;
    }
    
    while (BSP_CP3BAN_TRUE)
    {
        time(&currentTime); /* Get current calendar time in seconds */
        dwRetValue = BSP_cp3banBoardStatusGet(dwSubslotId, &dwBoardStatus);  /* 获取单板的状态 */
        if (BSP_E_CP3BAN_OK != dwRetValue)
        {
            g_bsp_cp3banSfpThreadState[dwSubslotId] = BSP_CP3BAN_THREAD_STOP;
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, BSP_cp3banBoardStatusGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), dwSubslotId, dwRetValue);
            return dwRetValue;
        }
        if (BSP_CP3BAN_BOARD_ONLINE != dwBoardStatus) /* 如果单板不在位,则直接返回. */
        {
            g_bsp_cp3banSfpThreadState[dwSubslotId] = BSP_CP3BAN_THREAD_STOP;
            BSP_Print(BSP_DEBUG_ALL, "WARNING: %s line %d, %s, subslotId %u, CP3BAN board is not OK, boardStatus=%u.\n", __FILE__, __LINE__, ctime(&currentTime), dwSubslotId, dwBoardStatus);
            return BSP_E_CP3BAN_OK;         
        }
        dwRetValue = BSP_cp3banUninitFlagGet(dwSubslotId, &dwUninitFlag);
        if (BSP_E_CP3BAN_OK != dwRetValue)  
        {
            g_bsp_cp3banSfpThreadState[dwSubslotId] = BSP_CP3BAN_THREAD_STOP;
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, BSP_cp3banUninitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), dwSubslotId, dwRetValue);
            return dwRetValue;
        }
        if (BSP_CP3BAN_UNINIT_ENABLE == dwUninitFlag)  /* 如果使能卸载STM单板的话,则直接返回成功. */
        {
            g_bsp_cp3banSfpThreadState[dwSubslotId] = BSP_CP3BAN_THREAD_STOP;
            BSP_Print(BSP_DEBUG_ALL, "WARNING: %s line %d, %s, subslotId %u, Enable uninitialize CP3BAN board.\n", __FILE__, __LINE__, ctime(&currentTime), dwSubslotId);
            return BSP_E_CP3BAN_OK;
        }

        dwRetValue = BSP_cp3banBoardInitFlagGet(dwSubslotId, &dwInitFlag); /* 获取CP3BAN单板的初始化标记 */
        if (BSP_E_CP3BAN_OK != dwRetValue)
        {
            g_bsp_cp3banSfpThreadState[dwSubslotId] = BSP_CP3BAN_THREAD_STOP;
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, BSP_cp3banBoardInitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), dwSubslotId, dwRetValue);
            return dwRetValue;
        }
        if (BSP_CP3BAN_BOARD_UNINSTALL == dwInitFlag)  /* CP3BAN单板初始化未完成 */
        {
            BSP_DelayMs(1000);  /* delay 1000ms. */
            continue;
        }
        if (BSP_CP3BAN_DISABLE != g_bsp_cp3ban_sfp_flag[dwSubslotId])
        {
            for (dwPortId = BSP_CP3BAN_MIN_STM1_PORT_ID; dwPortId <= dwMaxPortId; dwPortId++)
            {
                dwRetValue = BSP_cp3banBoardStatusGet(dwSubslotId, &dwBoardStatus);  /* 获取单板的状态 */
                if (BSP_E_CP3BAN_OK != dwRetValue)
                {
                    g_bsp_cp3banSfpThreadState[dwSubslotId] = BSP_CP3BAN_THREAD_STOP;
                    BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, BSP_cp3banBoardStatusGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), dwSubslotId, dwRetValue);
                    return dwRetValue;
                }
                if (BSP_CP3BAN_BOARD_ONLINE != dwBoardStatus) /* 如果单板不在位,则直接返回. */
                {
                    g_bsp_cp3banSfpThreadState[dwSubslotId] = BSP_CP3BAN_THREAD_STOP;
                    BSP_Print(BSP_DEBUG_ALL, "WARNING: %s line %d, %s, subslotId %u, CP3BAN board is not OK, boardStatus=%u.\n", __FILE__, __LINE__, ctime(&currentTime), dwSubslotId, dwBoardStatus);
                    return BSP_E_CP3BAN_OK;         
                }
                dwRetValue = BSP_cp3banUninitFlagGet(dwSubslotId, &dwUninitFlag);
                if (BSP_E_CP3BAN_OK != dwRetValue)  
                {
                    g_bsp_cp3banSfpThreadState[dwSubslotId] = BSP_CP3BAN_THREAD_STOP;
                    BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, BSP_cp3banUninitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), dwSubslotId, dwRetValue);
                    return dwRetValue;
                }
                if (BSP_CP3BAN_UNINIT_ENABLE == dwUninitFlag)  /* 如果使能卸载STM单板的话,则直接返回成功. */
                {
                    g_bsp_cp3banSfpThreadState[dwSubslotId] = BSP_CP3BAN_THREAD_STOP;
                    BSP_Print(BSP_DEBUG_ALL, "WARNING: %s line %d, %s, subslotId %u, Enable uninitialize CP3BAN board.\n", __FILE__, __LINE__, ctime(&currentTime), dwSubslotId);
                    return BSP_E_CP3BAN_OK;
                }
                memset(&sfpInfo, 0, sizeof(T_BSP_OPT_INFO));
                memset(&portInfo, 0, sizeof(BSP_CP3BAN_PORT_INFO));
                portInfo.dwSubslotId = dwSubslotId;
                portInfo.dwPortId = dwPortId;
                sfpInfo.dwOptInvalid = BSP_OPT_VALID;
                sfpInfo.dwOptStand = BSP_OPT_INFO_SFF_8472_STAND;

                dwRetValue = BSP_cp3banSfpSemGet(dwSubslotId);  /* Get semaphore. */
                if (BSP_E_CP3BAN_OK != dwRetValue)  
                {
                    g_bsp_cp3banSfpThreadState[dwSubslotId] = BSP_CP3BAN_THREAD_STOP;
                    BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, BSP_cp3banSfpSemGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), dwSubslotId, dwRetValue);
                    return dwRetValue;
                }
                dwRetValue = BSP_cp3banOpticStateGet(dwSubslotId, dwPortId, &dwSfpState);
                if (BSP_E_CP3BAN_OK != dwRetValue)
                {
                    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u portId %u, BSP_cp3banOpticStateGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwPortId, dwRetValue);
                    dwTempRv = BSP_cp3banSfpSemFree(dwSubslotId);  /* Free semaphore. */
                    if (BSP_E_CP3BAN_OK != dwTempRv)
                    {
                        g_bsp_cp3banSfpThreadState[dwSubslotId] = BSP_CP3BAN_THREAD_STOP;
                        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, BSP_cp3banSfpSemFree() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), dwSubslotId, dwTempRv);
                        return dwTempRv;
                    }
                    continue;
                }
                if (BSP_CP3BAN_OPTIC_OFFLINE == dwSfpState)  /* SFP is offline. */
                {
                    sfpInfo.dwOptOnline = BSP_OFF;
                    memset(&(sfpInfo.tInfo), 0, sizeof(T_BSP_OPT_INFO_COMMON));
                    sfpInfo.tInfo.tSff8472.dwInfoMask = BSP_OPT_INFO_ALL;
                }
                else  /* SFP is online. */
                {
                    sfpInfo.dwOptOnline = BSP_ON;
                    memset(&(sfpInfo.tInfo), 0, sizeof(T_BSP_OPT_INFO_COMMON));
                    sfpInfo.tInfo.tSff8472.dwInfoMask = BSP_OPT_INFO_ALL;
                    dwRetValue = BSP_GetOptInfo(BSP_cp3banOpticRead, BSP_OPT_INFO_SFF_8472_STAND, &(sfpInfo.tInfo), &portInfo);
                    if (BSP_OK != dwRetValue)
                    {
                        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u portId %u, BSP_GetOptInfo() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwPortId, dwRetValue);
                        dwTempRv = BSP_cp3banSfpSemFree(dwSubslotId);  /* Free semaphore. */
                        if (BSP_E_CP3BAN_OK != dwTempRv)
                        {
                            g_bsp_cp3banSfpThreadState[dwSubslotId] = BSP_CP3BAN_THREAD_STOP;
                            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, BSP_cp3banSfpSemFree() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), dwSubslotId, dwTempRv);
                            return dwTempRv;
                        }
                        continue;
                    }
                }
                dwTempRv = BSP_cp3banSfpSemFree(dwSubslotId);  /* Free semaphore. */
                if (BSP_E_CP3BAN_OK != dwTempRv)
                {
                    g_bsp_cp3banSfpThreadState[dwSubslotId] = BSP_CP3BAN_THREAD_STOP;
                    BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, BSP_cp3banSfpSemFree() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), dwSubslotId, dwTempRv);
                    return dwTempRv;
                }

                dwRetValue = BSP_cp3banSfpInfoSave(dwSubslotId, dwPortId, &sfpInfo);  
                if (BSP_E_CP3BAN_OK != dwRetValue)
                {
                    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u portId %u, BSP_cp3banSfpInfoSave() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwPortId, dwRetValue);
                    continue;
                }
                BSP_DelayMs(g_bsp_cp3ban_sfp_delay);  /* delay ms. */
            }
        }
        else
        {
            BSP_DelayMs(1000);  /* delay 1000ms. */
        }
    }

    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banSfpThreadFlagGet
* 功能描述: 获取光模块SFP线程的创建标记.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4. 
* 输出参数: *pdwFlag: 保存标记.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-07-14   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banSfpThreadFlagGet(WORD32 dwSubslotId, WORD32 *pdwFlag)
{
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pdwFlag);
    
    *pdwFlag = 0;
    *pdwFlag = g_bsp_cp3banSfpThreadFlag[dwSubslotId];
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banSfpThreadCreate
* 功能描述: 创建获取光模块SFP信息的线程.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4. 
* 输出参数: 无
* 返 回 值: 
* 其它说明: 该线程用来获取光模块信息.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-14   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banSfpThreadCreate(WORD32 dwSubslotId)
{
    int rv = BSP_E_CP3BAN_OK;
    WORD32 dwSubslotIdx = 0;  /* 从0开始取值 */
    WORD32 *pdwSubslotId = NULL;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    dwSubslotIdx = dwSubslotId - 1;
    BSP_CHECK_CP3BAN_SUBSLOT_INDEX(dwSubslotIdx);
    
    if (BSP_CP3BAN_THREAD_NONE_EXISTED != g_bsp_cp3banSfpThreadFlag[dwSubslotId])
    {
        BSP_Print(BSP_DEBUG_ALL, "WARNING: %s line %d, subslotId %u, sfpThread has been created.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_OK;
    }
    
    pdwSubslotId = (WORD32 *)(malloc(sizeof(WORD32)));
    if (NULL == pdwSubslotId)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, allocate memory for pdwSubslotId failed.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_ALLOC_MEM_FAIL;
    }
    memset(pdwSubslotId, 0, sizeof(WORD32));
    *pdwSubslotId = dwSubslotId;
    
    init_thread(&(g_bsp_cp3ban_sfp_thread[dwSubslotIdx]), 0, 0, 20, (VOID *)BSP_cp3banSfpMonitor, (VOID *)pdwSubslotId);
    rv = start_mod_thread(&(g_bsp_cp3ban_sfp_thread[dwSubslotIdx]));
    if (0 != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, start_mod_thread() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, rv);
        free(pdwSubslotId);
        pdwSubslotId = NULL;
        return BSP_E_CP3BAN_CREATE_THREAD_FAIL;    
    }
    g_bsp_cp3ban_sfp_thread_id[dwSubslotIdx] = g_bsp_cp3ban_sfp_thread[dwSubslotIdx].pthrid;
    g_bsp_cp3banSfpThreadFlag[dwSubslotId] = BSP_CP3BAN_THREAD_EXISTED;
    
    BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, subslotId %u, Create thread sfpThread successfully, threadId=0x%x.\n", __FILE__, __LINE__, dwSubslotId, g_bsp_cp3ban_sfp_thread[dwSubslotIdx].pthrid);
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banSfpThreadDelete
* 功能描述: 删除获取光模块SFP信息的线程.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4. 
* 输出参数: 无
* 返 回 值: 
* 其它说明: 该线程用来获取光模块信息.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-02-14   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banSfpThreadDelete(WORD32 dwSubslotId)
{
    WORD32 dwSubslotIdx = 0;
    
    dwSubslotIdx = dwSubslotId - 1;
    BSP_CHECK_CP3BAN_SUBSLOT_INDEX(dwSubslotIdx);

    memset(&(g_bsp_cp3ban_sfp_thread[dwSubslotIdx]), 0, sizeof(g_bsp_cp3ban_sfp_thread[dwSubslotIdx]));
    g_bsp_cp3ban_sfp_thread_id[dwSubslotIdx] = 0;
    g_bsp_cp3ban_sfp_tid[dwSubslotIdx] = 0;
    g_bsp_cp3banSfpThreadFlag[dwSubslotId] = BSP_CP3BAN_THREAD_NONE_EXISTED;
    
    return BSP_E_CP3BAN_OK;
}


/****************************************************************************
* 函数名称: BSP_cp3banEpld16Read
* 功能描述: 读取CP3BAN单板的EPLD寄存器
* 输入参数: dwSubslotId   --CP3BAN单板的子槽位号,取值为1~4.
*           wAddr         --CPLD寄存器的偏移地址
* 输出参数: *pwData       --寄存器值
* 返 回 值: BSP_E_CP3BAN_OK     --读成功
*         : 其他                --读失败          
* 其它说明
* 修改日期     版本号                 修改人              修改内容
* -----------------------------------------------------------------------------
* 2013/04/01   V1.0                  xieweisheng         create
******************************************************************************/
WORD32 BSP_cp3banEpld16Read(WORD32 dwSubslotId, WORD16 wAddr, WORD16 *pwData)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    WORD32 systemType = 0;  /* ZXUPN15000的系统类型 */
    WORD16 wData = 0;
    WORD16 offsetAddr = 0;
    BSP_CP3BAN_CHIP_BASE_ADDR *pMappingAddr = NULL;
    BYTE *pucAddr = NULL;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pwData);
    
    dwRetValue = BSP_cp3banMappingBaseAddrGet(dwSubslotId, &pMappingAddr);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_CPLD_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_devicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return dwRetValue;
    }
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pMappingAddr);

    if (NULL == pMappingAddr->epldBaseAddr)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_CPLD_ACCESS_MSG, "ERROR: %s line %d, g_bsp_cp3ban_chip_base_addr[%u].epldBaseAddr is NULL pointer.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_NULL_POINTER;
    }

    systemType = BSP_cp3banSystemTypeGet(); /* 获取ZXUPN15000系统类型. */
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM == systemType)   /* 非15K-2系统 */
    {
        /* 对于非15K-2系统,通过PCI总线来访问CPLD时,PCI的访问地址为(CPLD的偏移地址*2 + 基地址). */
        offsetAddr = wAddr << 1;
    }
    else  /* 15K-2系统 */
    {
        offsetAddr = wAddr;
    }
    pucAddr = (BYTE *)(pMappingAddr->epldBaseAddr) + offsetAddr;

    BSP_REG_READ_WORD16(pucAddr, wData);
    *pwData = wData;
    
    return BSP_E_CP3BAN_OK;
}


/****************************************************************************
* 函数名称: BSP_cp3banEpld16Write
* 功能描述: 写CP3BAN单板的EPLD寄存器
* 输入参数: dwSubslotId    --CP3BAN单板的子槽位号,取值为1~4.
*           wAddr          --CPLD的偏移地址
*           wData          --CPLD寄存器的值
* 输出参数: 无
* 返 回 值: BSP_E_CP3BAN_OK      --成功
*         : 其他                 --失败          
* 其它说明
* 修改日期     版本号                 修改人              修改内容
* -----------------------------------------------------------------------------
* 2013/04/01   V1.0               xieweisheng            create
******************************************************************************/
WORD32 BSP_cp3banEpld16Write(WORD32 dwSubslotId, WORD16 wAddr, WORD16 wData)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    WORD32 systemType = 0;  /* ZXUPN15000的系统类型 */
    WORD16 offsetAddr = 0;
    BSP_CP3BAN_CHIP_BASE_ADDR *pMappingAddr = NULL;
    BYTE *pucAddr = NULL;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    
    dwRetValue = BSP_cp3banMappingBaseAddrGet(dwSubslotId, &pMappingAddr);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_CPLD_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_devicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return dwRetValue;
    }
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pMappingAddr);

    if (NULL == pMappingAddr->epldBaseAddr)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_CPLD_ACCESS_MSG, "ERROR: %s line %d, g_bsp_cp3ban_chip_base_addr[%u].epldBaseAddr is NULL pointer.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_NULL_POINTER;
    }

    systemType = BSP_cp3banSystemTypeGet(); /* 获取ZXUPN15000系统类型. */
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM == systemType)   /* 非15K-2系统 */
    {
        /* 对于非15K-2系统,通过PCI总线来访问CPLD时,PCI的访问地址为(CPLD的偏移地址*2 + 基地址). */
        offsetAddr = wAddr << 1;
    }
    else   /* 15K-2系统 */
    {
        offsetAddr = wAddr;
    }
    pucAddr = (BYTE *)(pMappingAddr->epldBaseAddr) + offsetAddr;

    BSP_REG_WRITE_WORD16(pucAddr, wData);
    
    return BSP_E_CP3BAN_OK;
}


/****************************************************************************
* 函数名称: BSP_cp3banEpldFlashDevFlagSet
* 功能描述: 设置CP3BAN单板的EPLD FLASH设备的创建标记.
* 输入参数: dwSubslotId   --CP3BAN单板的子槽位号,取值为1~4.
*           flag          --创建标记.
* 输出参数: 无.
* 返 回 值: BSP_E_CP3BAN_OK     --成功
*         : 其他                --失败          
* 其它说明
* 修改日期     版本号                 修改人              修改内容
* -----------------------------------------------------------------------------
* 2013/11/11   V1.0                  xieweisheng         create
******************************************************************************/
WORD32 BSP_cp3banEpldFlashDevFlagSet(WORD32 dwSubslotId, WORD32 flag)
{
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    
    g_bsp_cp3banEpldFlashDevFlag[dwSubslotId] = flag;
    
    return BSP_E_CP3BAN_OK;
}


/****************************************************************************
* 函数名称: BSP_cp3banEpldFlashDevFlagGet
* 功能描述: 获取CP3BAN单板的EPLD FLASH设备的创建标记.
* 输入参数: dwSubslotId   --CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: *pFlag: 保存创建标记.
* 返 回 值: BSP_E_CP3BAN_OK     --成功
*         : 其他                --失败          
* 其它说明
* 修改日期     版本号                 修改人              修改内容
* -----------------------------------------------------------------------------
* 2013/11/11   V1.0                  xieweisheng         create
******************************************************************************/
WORD32 BSP_cp3banEpldFlashDevFlagGet(WORD32 dwSubslotId, WORD32 *pFlag)
{
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pFlag);
    
    *pFlag = 0;  /* 先清零再赋值 */
    *pFlag = g_bsp_cp3banEpldFlashDevFlag[dwSubslotId];
    
    return BSP_E_CP3BAN_OK;
}


/****************************************************************************
* 函数名称: BSP_cp3banEpldFlashDevCreate
* 功能描述: 创建CP3BAN单板的EPLD FLASH设备
* 输入参数: dwSubslotId   --CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: 无.
* 返 回 值: BSP_E_CP3BAN_OK     --成功
*         : 其他                --失败          
* 其它说明
* 修改日期     版本号                 修改人              修改内容
* -----------------------------------------------------------------------------
* 2013/11/11   V1.0                  xieweisheng         create
******************************************************************************/
WORD32 BSP_cp3banEpldFlashDevCreate(WORD32 dwSubslotId)
{
    WORD32 dwRet = BSP_E_CP3BAN_OK;
    SWORD32 retValue = 0;
    WORD32 systemType = 0;   /* ZXUPN15000的系统类型 */
    WORD32 dwFlag = 0;  /* EPLD flash的创建标记 */
    WORD32 dwSubslotIdx = 0;
    WORD64 qwJtagIndex = 0;
    WORD64 qwEpldIndex = 0;
    WORD64 qwEpldAddr = 0;  /* EPLD的物理基地址 */
    WORD64 qwEpldFlashAddr = 0;  /* EPLD的FLASH物理起始地址 */
    WORD16  wWidth = 16;
    FILE *pFile = NULL;
    struct Device *pDevice = NULL;
    CHAR acFileName[BSP_CP3BAN_MAX_FILE_NAME] = {"/sys/bsp/epldflash/create"};
    CHAR acDataBuf[BSP_CP3BAN_FLASH_DATA_BUF_SIZE] = {0};
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    dwSubslotIdx = dwSubslotId - 1;
    memset(acDataBuf, 0, sizeof(acDataBuf));

    dwRet = BSP_cp3banEpldFlashDevFlagGet(dwSubslotId, &dwFlag); /* 获取CP3BAN单板的EPLD FLASH设备的创建标记. */
    if (BSP_E_CP3BAN_OK != dwRet)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpldFlashDevFlagGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
        return dwRet;
    }
    if (BSP_CP3BAN_EPLD_FASH_DESTROYED != dwFlag)  /* 如果EPLD flash已经创建成功过了,则直接返回成功 */
    {
        BSP_Print(BSP_DEBUG_ALL, "WARNING: %s line %d, subslotId %u, EPLD flash has been created.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_OK;
    }

    systemType = BSP_cp3banSystemTypeGet();  /* 获取ZXUPN15000系统类型. */
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM == systemType)   /* 非15K-2系统 */
    {
        dwRet = BSP_cp3banEpldPhyAddrGet(dwSubslotId, &qwEpldAddr);
        if (BSP_E_CP3BAN_OK != dwRet)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpldPhyAddrGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
            return dwRet;
        }
        qwEpldFlashAddr = qwEpldAddr + (WORD64)(BSP_CP3BAN_EPLD_ISP_IN_DATA_REG << 1);
    }
    else  /* 15K-2系统 */
    {
        qwEpldAddr = BSP_CP3BAN_INIT_EPLD_BASE_ADDR_15K_2;
        qwEpldFlashAddr = qwEpldAddr + (WORD64)(BSP_CP3BAN_EPLD_ISP_IN_DATA_REG_15K_2);
    }

    dwRet = BSP_cp3ban_devicePtrGet(dwSubslotId, &pDevice);
    if (BSP_E_CP3BAN_OK != dwRet)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_devicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
        return dwRet;
    }
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pDevice);
    qwJtagIndex = pDevice->res.iores[0].sjtagindex;
    qwEpldIndex = pDevice->res.iores[0].epldindex;
    
    pFile = fopen(acFileName, "w");
    if (NULL == pFile)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fopen() return NULL pointer.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    
    retValue = fseek(pFile, 0, SEEK_SET);  /* 将文件位置指示符指向文件头 */
    if (retValue < 0)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fseek() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
        retValue = fclose(pFile);
        if (0 != retValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fclose() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
            return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
        }
        pFile = NULL;
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    
    retValue = sprintf(acDataBuf, "slot%uepld0 0x%llx 0x%llx 0x%llx 0x%x", dwSubslotIdx, qwJtagIndex, qwEpldIndex, qwEpldFlashAddr, wWidth);
    if (retValue <= 0)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, sprintf() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
        retValue = fclose(pFile);
        if (0 != retValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fclose() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
            return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
        }
        pFile = NULL;
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    BSP_Print(BSP_DEBUG_ALL, "%s line %d, subslotId %u, slot%uepld0 0x%llx 0x%llx 0x%llx 0x%x\n", __FILE__, __LINE__, dwSubslotId, dwSubslotIdx, qwJtagIndex, qwEpldIndex, qwEpldFlashAddr, wWidth);

    retValue = fwrite(acDataBuf, sizeof(CHAR), strnlen(acDataBuf, BSP_CP3BAN_FLASH_DATA_BUF_SIZE), pFile);
    if (retValue < strnlen(acDataBuf, BSP_CP3BAN_FLASH_DATA_BUF_SIZE))
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fwrite() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
        retValue = fclose(pFile);
        if (0 != retValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fclose() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
            return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
        }
        pFile = NULL;
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }

    retValue = fclose(pFile);
    if (0 != retValue)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fclose() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    pFile = NULL;

    /* create EPLD flash successfully. */
    dwRet = BSP_cp3banEpldFlashDevFlagSet(dwSubslotId, BSP_CP3BAN_EPLD_FASH_CREATED);
    if (BSP_E_CP3BAN_OK != dwRet)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpldFlashDevFlagSet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
        return dwRet;
    }
    
    return BSP_E_CP3BAN_OK;
}


/****************************************************************************
* 函数名称: BSP_cp3banEpldFlashArea2Create
* 功能描述: 创建CP3BAN单板的EPLD FLASH设备的2nd Area
* 输入参数: dwSubslotId   --CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: 无.
* 返 回 值: BSP_E_CP3BAN_OK     --成功
*         : 其他                --失败          
* 其它说明: 
* 修改日期     版本号                 修改人              修改内容
* -----------------------------------------------------------------------------
* 2014/03/20   V1.0                  xieweisheng         create
******************************************************************************/
WORD32 BSP_cp3banEpldFlashArea2Create(WORD32 dwSubslotId)
{
    SWORD32 retValue = 0;
    WORD32 dwSubslotIdx = 0;
    FILE *pFile = NULL;
    CHAR acFileName[BSP_CP3BAN_MAX_FILE_NAME] = {0};
    CHAR acDataBuf[BSP_CP3BAN_FLASH_DATA_BUF_SIZE] = {0};
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    memset(acDataBuf, 0, sizeof(acDataBuf));
    dwSubslotIdx = dwSubslotId - 1;
    
    retValue = sprintf(acFileName, "/sys/bsp/epldflash/slot%uepld0/bitstream", dwSubslotIdx);
    if (retValue <= 0)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, sprintf() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    
    pFile = fopen(acFileName, "w");
    if (NULL == pFile)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fopen() return NULL pointer.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    
    retValue = fseek(pFile, 0, SEEK_SET);  /* 将文件位置指示符指向文件头 */
    if (retValue < 0)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fseek() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
        retValue = fclose(pFile);
        if (0 != retValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fclose() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
            return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
        }
        pFile = NULL;
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    
    retValue = sprintf(acDataBuf, "0x2");
    if (retValue <= 0)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, sprintf() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
        retValue = fclose(pFile);
        if (0 != retValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fclose() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
            return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
        }
        pFile = NULL;
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    
    retValue = fwrite(acDataBuf, sizeof(CHAR), strnlen(acDataBuf, BSP_CP3BAN_FLASH_DATA_BUF_SIZE), pFile);
    if (retValue < strnlen(acDataBuf, BSP_CP3BAN_FLASH_DATA_BUF_SIZE))
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fwrite() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
        retValue = fclose(pFile);
        if (0 != retValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fclose() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
            return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
        }
        pFile = NULL;
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    
    retValue = fclose(pFile);
    if (0 != retValue)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fclose() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    pFile = NULL;
    
    return BSP_E_CP3BAN_OK;
}


/****************************************************************************
* 函数名称: BSP_cp3banEpldFlashDevDestroy
* 功能描述: 销毁CP3BAN单板的EPLD FLASH设备
* 输入参数: dwSubslotId   --CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: 无.
* 返 回 值: BSP_E_CP3BAN_OK     --成功
*         : 其他                --失败          
* 其它说明
* 修改日期     版本号                 修改人              修改内容
* -----------------------------------------------------------------------------
* 2013/11/11   V1.0                  xieweisheng         create
******************************************************************************/
WORD32 BSP_cp3banEpldFlashDevDestroy(WORD32 dwSubslotId)
{
    WORD32 dwRet = BSP_E_CP3BAN_OK;
    SWORD32 retValue = 0;
    WORD32 dwFlag = 0;  /* EPLD flash的创建标记 */
    WORD32 dwSubslotIdx = 0;
    CHAR acFileName[BSP_CP3BAN_MAX_FILE_NAME] = {"/sys/bsp/epldflash/destroy"};
    CHAR acDataBuf[BSP_CP3BAN_FLASH_DATA_BUF_SIZE] = {0};
    FILE *pFile = NULL;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    dwSubslotIdx = dwSubslotId - 1;
    memset(acDataBuf, 0, sizeof(acDataBuf));

    dwRet = BSP_cp3banEpldFlashDevFlagGet(dwSubslotId, &dwFlag); /* 获取CP3BAN单板的EPLD FLASH设备的创建标记. */
    if (BSP_E_CP3BAN_OK != dwRet)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpldFlashDevFlagGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
        return dwRet;
    }
    if (BSP_CP3BAN_EPLD_FASH_DESTROYED == dwFlag)  /* 如果EPLD flash已经销毁成功过了,则直接返回成功 */
    {
        BSP_Print(BSP_DEBUG_ALL, "WARNING: %s line %d, subslotId %u, EPLD flash has been destroyed.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_OK;
    }
    
    pFile = fopen(acFileName, "w");
    if (NULL == pFile)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fopen() return NULL pointer.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    
    retValue = fseek(pFile, 0, SEEK_SET);  /* 将文件位置指示符指向文件头 */
    if (retValue < 0)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fseek() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
        retValue = fclose(pFile);
        if (0 != retValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fclose() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
            return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
        }
        pFile = NULL;
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    
    retValue = sprintf(acDataBuf, "slot%uepld0", dwSubslotIdx);
    if (retValue <= 0)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, sprintf() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
        retValue = fclose(pFile);
        if (0 != retValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fclose() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
            return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
        }
        pFile = NULL;
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    BSP_Print(BSP_DEBUG_ALL, "%s line %d, subslotId %u, slot%uepld0\n", __FILE__, __LINE__, dwSubslotId, dwSubslotIdx);
    
    retValue = fwrite(acDataBuf, sizeof(CHAR), strnlen(acDataBuf, BSP_CP3BAN_FLASH_DATA_BUF_SIZE), pFile);
    if (retValue < strnlen(acDataBuf, BSP_CP3BAN_FLASH_DATA_BUF_SIZE))
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fwrite() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
        retValue = fclose(pFile);
        if (0 != retValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fclose() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
            return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
        }
        pFile = NULL;
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }

    /* 同时需要销毁temp下的丝印文件 */
    dwRet = BSP_UpdateSilkInfo(dwSubslotIdx, NULL);
    if (BSP_OK != dwRet)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_UpdateSilkInfo() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    
    retValue = fclose(pFile);
    if (0 != retValue)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fclose() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }

    /* destory EPLD flash successfully. */
    dwRet = BSP_cp3banEpldFlashDevFlagSet(dwSubslotId, BSP_CP3BAN_EPLD_FASH_DESTROYED);
    if (BSP_E_CP3BAN_OK != dwRet)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpldFlashDevFlagSet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
        return dwRet;
    }
    
    return BSP_E_CP3BAN_OK;
}


/****************************************************************************
* 函数名称: BSP_cp3banEpldFlashRead
* 功能描述: 读取CP3BAN单板的EPLD的FLASH寄存器的值
* 输入参数: dwSubslotId   --CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: *pBData       --寄存器值
* 返 回 值: BSP_E_CP3BAN_OK     --读成功
*         : 其他                --读失败          
* 其它说明
* 修改日期     版本号                 修改人              修改内容
* -----------------------------------------------------------------------------
* 2013/11/11   V1.0                  xieweisheng         create
******************************************************************************/
WORD32 BSP_cp3banEpldFlashRead(WORD32 dwSubslotId, BYTE *pBData)
{
    WORD32 dwRet = BSP_E_CP3BAN_OK;
    SWORD32 retValue = 0;
    WORD32 dwFlag = 0;  /* EPLD flash的创建标记 */
    WORD32 dwSubslotIdx = 0;
    WORD16 wBomId = 0;    /* BOM ID */
    FILE *pFile = NULL;
    CHAR acFileName[BSP_CP3BAN_MAX_FILE_NAME] = {0};
    BYTE acDataBuf[BSP_CP3BAN_FLASH_DATA_BUF_SIZE] = {0};
    T_BSP_SUBCARD_SILK_INFO tSilkInfo;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pBData);
    dwSubslotIdx = dwSubslotId - 1;
    
    dwRet = BSP_cp3banEpldFlashDevFlagGet(dwSubslotId, &dwFlag); /* 获取CP3BAN单板的EPLD FLASH设备的创建标记. */
    if (BSP_E_CP3BAN_OK != dwRet)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpldFlashDevFlagGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
        return dwRet;
    }
    if (BSP_CP3BAN_EPLD_FASH_DESTROYED == dwFlag)  /* 如果EPLD flash已经销毁过了,则直接返回失败 */
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, EPLD flash has not been created.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_EPLD_FLASH_UNCREATE;
    }
    
    memset(acFileName, 0, sizeof(acFileName));
    retValue = sprintf(acFileName, "/sys/bsp/epldflash/slot%uepld0/silk", dwSubslotIdx);
    if (retValue <= 0)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, sprintf() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    
    pFile = fopen(acFileName, "r");
    if (NULL == pFile)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fopen() return NULL pointer.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }

    retValue = fseek(pFile, 0, SEEK_SET);  /* 将文件位置指示符指向文件头 */
    if (retValue < 0)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fseek() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
        retValue = fclose(pFile);
        if (0 != retValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fclose() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
            return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
        }
        pFile = NULL;
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    
    if (NULL == fgets((char *)acDataBuf, sizeof(acDataBuf), pFile))
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fgets() return NULL pointer.\n", __FILE__, __LINE__, dwSubslotId);
        retValue = fclose(pFile);
        if (0 != retValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fclose() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
            return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
        }
        pFile = NULL;
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    BSP_Print(BSP_DEBUG_ALL, "%s line %d, subslotId %u, acDataBuf[0]=0x%x.\n", __FILE__, __LINE__, dwSubslotId, acDataBuf[0]);

    *pBData = 0;  /* 先清零再赋值 */
    *pBData = acDataBuf[0]; 
    BSP_Print(BSP_DEBUG_ALL, "%s line %d, subslotId %u, *pBData = 0x%x.\n", __FILE__, __LINE__, dwSubslotId, *pBData);
    
    /* update the subcard silkinfo to temporary file */
    dwRet = BSP_cp3banBomIdGet(dwSubslotId, &wBomId);
    if (BSP_E_CP3BAN_OK != dwRet)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banBomIdGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
        retValue = fclose(pFile);
        if (0 != retValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fclose() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
            return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
        }
        pFile = NULL;
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    memset(&tSilkInfo, 0, sizeof(tSilkInfo));
    tSilkInfo.dwHwExBomID = (WORD32)(*pBData);  /* 丝印值 */
    tSilkInfo.dwExBomID = tSilkInfo.dwHwExBomID;
    tSilkInfo.dwBomID = (WORD32)wBomId;
    dwRet = BSP_UpdateSilkInfo(dwSubslotIdx, &tSilkInfo);
    if (BSP_OK != dwRet)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_UpdateSilkInfo() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
        retValue = fclose(pFile);
        if (0 != retValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fclose() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
            return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
        }
        pFile = NULL;
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    
    retValue = fclose(pFile);
    if (0 != retValue)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fclose() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    pFile = NULL;
    
    return BSP_E_CP3BAN_OK;
}


/****************************************************************************
* 函数名称: BSP_cp3banEpldFlashWrite
* 功能描述: 写入CP3BAN单板的EPLD的FLASH寄存器的值
* 输入参数: dwSubslotId   --CP3BAN单板的子槽位号,取值为1~4.
*           bData:        --寄存器的值.
* 输出参数: 无.
* 返 回 值: BSP_E_CP3BAN_OK     --写成功
*         : 其他                --写失败          
* 其它说明
* 修改日期     版本号                 修改人              修改内容
* -----------------------------------------------------------------------------
* 2013/11/11   V1.0                  xieweisheng         create
******************************************************************************/
WORD32 BSP_cp3banEpldFlashWrite(WORD32 dwSubslotId, BYTE bData)
{
    WORD32 dwRet = BSP_E_CP3BAN_OK;
    SWORD32 retValue = 0;
    WORD32 dwFlag = 0;  /* EPLD flash的创建标记 */
    WORD32 dwSubslotIdx = 0;
    WORD16 wBomId = 0;    /* BOM ID */
    FILE *pFile = NULL;
    CHAR acFileName[BSP_CP3BAN_MAX_FILE_NAME] = {0};
    BYTE acDataBuf[BSP_CP3BAN_FLASH_DATA_BUF_SIZE] = {0};
    T_BSP_SUBCARD_SILK_INFO tSilkInfo;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    dwSubslotIdx = dwSubslotId - 1;

    dwRet = BSP_cp3banEpldFlashDevFlagGet(dwSubslotId, &dwFlag); /* 获取CP3BAN单板的EPLD FLASH设备的创建标记. */
    if (BSP_E_CP3BAN_OK != dwRet)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpldFlashDevFlagGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
        return dwRet;
    }
    if (BSP_CP3BAN_EPLD_FASH_DESTROYED == dwFlag)  /* 如果EPLD flash已经销毁过了,则直接返回失败 */
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, EPLD flash has not been created.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_EPLD_FLASH_UNCREATE;
    }
    
    memset(acFileName, 0, sizeof(acFileName));
    retValue = sprintf(acFileName, "/sys/bsp/epldflash/slot%uepld0/silk", dwSubslotIdx);
    if (retValue <= 0)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, sprintf() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    
    pFile = fopen(acFileName, "w");
    if (NULL == pFile)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fopen() return NULL pointer.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    
    retValue = fseek(pFile, 0, SEEK_SET);  /* 将文件位置指示符指向文件头 */
    if (retValue < 0)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fseek() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
        retValue = fclose(pFile);
        if (0 != retValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fclose() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
            return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
        }
        pFile = NULL;
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    
    retValue = sprintf((char *)acDataBuf, "0 0x%x", bData);
    if (retValue <= 0)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, sprintf() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
        retValue = fclose(pFile);
        if (0 != retValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fclose() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
            return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
        }
        pFile = NULL;
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    BSP_Print(BSP_DEBUG_ALL, "%s line %d, subslotId %u, acDataBuf %s.\n", __FILE__, __LINE__, dwSubslotId, (char *)acDataBuf);
    
    retValue = fwrite(acDataBuf, sizeof(BYTE), strnlen((char *)acDataBuf, BSP_CP3BAN_FLASH_DATA_BUF_SIZE), pFile);
    if (retValue < strnlen((char *)acDataBuf, BSP_CP3BAN_FLASH_DATA_BUF_SIZE))
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fwrite() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
        retValue = fclose(pFile);
        if (0 != retValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fclose() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
            return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
        }
        pFile = NULL;
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }

    /* update the subcard silkinfo to temporary file */
    dwRet = BSP_cp3banBomIdGet(dwSubslotId, &wBomId);
    if (BSP_E_CP3BAN_OK != dwRet)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banBomIdGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
        retValue = fclose(pFile);
        if (0 != retValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fclose() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
            return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
        }
        pFile = NULL;
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    memset(&tSilkInfo, 0, sizeof(tSilkInfo));
    tSilkInfo.dwHwExBomID = (WORD32)bData;  /* 丝印值 */
    tSilkInfo.dwExBomID = tSilkInfo.dwHwExBomID;
    tSilkInfo.dwBomID = (WORD32)wBomId;
    dwRet = BSP_UpdateSilkInfo(dwSubslotIdx, &tSilkInfo);
    if (BSP_OK != dwRet)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_UpdateSilkInfo() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
        retValue = fclose(pFile);
        if (0 != retValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fclose() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
            return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
        }
        pFile = NULL;
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }

    retValue = fclose(pFile);
    if (0 != retValue)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, fclose() rv=%d.\n", __FILE__, __LINE__, dwSubslotId, retValue);
        return BSP_E_CP3BAN_EPLD_FLASH_FAIL;
    }
    pFile = NULL;
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banSilkInfoManage
* 功能描述: CP3BAN单板的丝印信息管理.
* 访问的表: 无
* 修改的表: 无
* 输入参数:  
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-11-11   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banSilkInfoManage(struct Device *ptDev, T_BSP_DEVICEDRIVER_PARA *ptPara)
{
    WORD32 dwRetVal = BSP_E_CP3BAN_OK;
    WORD32 systemType = 0;   /* ZXUPN15000的系统类型 */
    WORD32 dwSubslotId = 0;  /* 单板的子槽位号 */
    WORD16 wBomId = 0;       /* BOM ID */
    BYTE ucSilkValue = 0;
    T_BSP_SUBCARD_SILK_INFO *pSilkInfo = NULL;
    
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptDev);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptPara);

    systemType = BSP_cp3banSystemTypeGet();  /* 获取ZXUPN15000系统类型. */
    dwSubslotId = ptDev->ext + CP3BAN_INDX_MIN;
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM != systemType)   /* 15K-2系统 */
    {
        dwSubslotId = 1;   /* 对于15K-2系统,子槽位始终取值为1 */
    }

    if (BSP_Q_SUBCARD_GET_SILK_INFO == ptPara->dwQueryCode)
    {
        pSilkInfo = (T_BSP_SUBCARD_SILK_INFO *)(ptPara->pucBuf);
        BSP_CP3BAN_CHECK_POINTER_IS_NULL(pSilkInfo);
        memset(pSilkInfo, 0, sizeof(T_BSP_SUBCARD_SILK_INFO));
        dwRetVal = BSP_cp3banEpldFlashRead(dwSubslotId, &ucSilkValue);
        if (BSP_E_CP3BAN_OK != dwRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpldFlashRead() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
            return dwRetVal;
        }
        dwRetVal = BSP_cp3banBomIdGet(dwSubslotId, &wBomId);
        if (BSP_E_CP3BAN_OK != dwRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banBomIdGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
            return dwRetVal;
        }
        pSilkInfo->dwHwExBomID = (WORD32)ucSilkValue;  /* 丝印值 */
        pSilkInfo->dwExBomID = pSilkInfo->dwHwExBomID;
        pSilkInfo->dwBomID = (WORD32)wBomId;
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SILK_MSG, "%s line %d, subslotId %u, dwHwExBomID %u dwExBomID %u dwBomID %u.\n", __FILE__, __LINE__, dwSubslotId, pSilkInfo->dwHwExBomID, pSilkInfo->dwExBomID, pSilkInfo->dwBomID);
    }
    else if (BSP_Q_SUBCARD_SET_SILK_INFO == ptPara->dwQueryCode)
    {
        ucSilkValue = (BYTE)(ptPara->adwAddr[1]);
        dwRetVal = BSP_cp3banEpldFlashWrite(dwSubslotId, ucSilkValue);
        if (BSP_E_CP3BAN_OK != dwRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpldFlashWrite() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
            return dwRetVal;
        }
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SILK_MSG, "%s line %d, subslotId %u, silkValue=%u.\n", __FILE__, __LINE__, dwSubslotId, ptPara->adwAddr[1]);
    }
    else
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SILK_MSG, "ERROR: %s line %d, subslotId %u, invalid query code, queryCode=0x%x.\n", __FILE__, __LINE__, dwSubslotId, ptPara->dwQueryCode);
        return BSP_E_CP3BAN_PARA;
    }
    
    return BSP_E_CP3BAN_OK;
}


/******************************************************************************
 * 函数名称：BSP_cp3banEpldVerify
 * 功能描述：校验CP3BAN单板的EPLD芯片
 * 输入参数：dwSubslotId   --CP3BAN单板的子槽位号
 * 输出参数：无
 * 返 回 值：BSP_E_CP3BAN_OK     --成功
 *           其他                --失败
 * 其它说明： 
 * 修改日期      版本号      修改人        修改内容
 * -----------------------------------------------------------
 * 2013/07/25        v1.0     xieweisheng10112265       create
 ******************************************************************************/
WORD32 BSP_cp3banEpldVerify(WORD32 dwSubslotId)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    WORD16 wRegVal = 0;   /* EPLD寄存器的值 */

    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    
    dwRetValue = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_EPLD_VERIFY_REG, 0x55aa);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return dwRetValue;
    }
    dwRetValue = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_EPLD_VERIFY_REG, &wRegVal);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return dwRetValue;
    }
    if (0xaa55 != wRegVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, rdRegValue=0x%x\n", __FILE__, __LINE__, dwSubslotId, wRegVal);
        return BSP_E_CP3BAN_EPLD_CHECK_FAIL;
    }
    
    wRegVal = 0;
    dwRetValue = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_EPLD_VERIFY_REG, 0xffff);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return dwRetValue;
    }
    dwRetValue = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_EPLD_VERIFY_REG, &wRegVal);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return dwRetValue;
    }
    if (0 != wRegVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, rdRegValue=0x%x\n", __FILE__, __LINE__, dwSubslotId, wRegVal);
        return BSP_E_CP3BAN_EPLD_CHECK_FAIL;
    }
    
    wRegVal = 0;
    dwRetValue = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_EPLD_VERIFY_REG, 0xaa55);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return dwRetValue;
    }
    dwRetValue = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_EPLD_VERIFY_REG, &wRegVal);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return dwRetValue;
    }
    if (0x55aa != wRegVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, rdRegValue=0x%x\n", __FILE__, __LINE__, dwSubslotId, wRegVal);
        return BSP_E_CP3BAN_EPLD_CHECK_FAIL;
    }
    
    wRegVal = 0;
    dwRetValue = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_EPLD_VERIFY_REG, 0);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return dwRetValue;
    }
    dwRetValue = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_EPLD_VERIFY_REG, &wRegVal);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return dwRetValue;
    }
    if (0xffff != wRegVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, rdRegValue=0x%x\n", __FILE__, __LINE__, dwSubslotId, wRegVal);
        return BSP_E_CP3BAN_EPLD_CHECK_FAIL;
    }
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banFpgaSemFlagGet
* 功能描述: 获取信号量的创建标记.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号.
* 输出参数: *pdwFlag: 保存信号量的创建标记.
* 返 回 值: 
* 其它说明: 该信号量用来保护读写FPGA. 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-03-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banFpgaSemFlagGet(WORD32 dwSubslotId, WORD32 *pdwFlag)
{
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pdwFlag);
    
    *pdwFlag = 0;  /* 先清零再赋值 */
    *pdwFlag = g_bsp_cp3ban_fpga_sem_flag[dwSubslotId];
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banFpgaSemIdGet
* 功能描述: 获取信号量ID.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号.
* 输出参数: *pdwSemId: 保存信号量ID.
* 返 回 值: 
* 其它说明: 该信号量用来保护读写FPGA. 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-03-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banFpgaSemIdGet(WORD32 dwSubslotId, WORD32 *pdwSemId)
{
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pdwSemId);
    
    *pdwSemId = 0;  /* 先清零再赋值 */
    *pdwSemId = g_bsp_cp3ban_fpga_sem_id[dwSubslotId];
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banFpgaSemCreate
* 功能描述: 创建信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护读写FPGA.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-03-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banFpgaSemCreate(WORD32 dwSubslotId)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);

    if (BSP_CP3BAN_SEM_NONE_EXISTED == g_bsp_cp3ban_fpga_sem_flag[dwSubslotId])    
    {
        dwRetValue = BSP_OalCreateSem(1, MUTEX_STYLE, &(g_bsp_cp3ban_fpga_sem_id[dwSubslotId]));
        if (BSP_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_OalCreateSem() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
            return BSP_E_CP3BAN_CREATE_SEM_FAIL;
        }
        g_bsp_cp3ban_fpga_sem_flag[dwSubslotId] = BSP_CP3BAN_SEM_EXISTED;
    }  

    return BSP_E_CP3BAN_OK;  /* 返回成功 */
}


/**************************************************************************
* 函数名称: BSP_cp3banFpgaSemDestroy
* 功能描述: 销毁信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护读写FPGA.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-03-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banFpgaSemDestroy(WORD32 dwSubslotId)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);

    /* 在销毁信号量之前,先必须LOCK一下信号量 */
    dwRetValue = BSP_cp3banFpgaSemGet(dwSubslotId);
    if (BSP_E_CP3BAN_SEM_NONE_EXISTED == dwRetValue)
    {
        BSP_Print(BSP_DEBUG_ALL, "WARNING: %s line %d, g_bsp_cp3ban_fpga_sem_id[%u] has been destroyed.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_OK;
    }
    else if (BSP_E_CP3BAN_OK == dwRetValue)
    {
        ;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpgaSemGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return dwRetValue;
    }

    if (BSP_CP3BAN_SEM_NONE_EXISTED != g_bsp_cp3ban_fpga_sem_flag[dwSubslotId])    
    {
        dwRetValue = BSP_OalDestroySem(g_bsp_cp3ban_fpga_sem_id[dwSubslotId]);
        if (BSP_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_OalDestroySem() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
            return BSP_E_CP3BAN_DESTROY_SEM_FAIL;
        }
        g_bsp_cp3ban_fpga_sem_id[dwSubslotId] = 0;
        g_bsp_cp3ban_fpga_sem_flag[dwSubslotId] = BSP_CP3BAN_SEM_NONE_EXISTED;
    }
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banFpgaSemGet
* 功能描述: 获取信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护读写FPGA.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-03-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banFpgaSemGet(WORD32 dwSubslotId)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);

    if (BSP_CP3BAN_SEM_NONE_EXISTED != g_bsp_cp3ban_fpga_sem_flag[dwSubslotId])    
    {
        dwRetValue = BSP_OalSemP(g_bsp_cp3ban_fpga_sem_id[dwSubslotId], WAIT_FOREVER);
        if (BSP_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_OalSemP() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
            return BSP_E_CP3BAN_GET_SEM_FAIL;
        }
    }
    else
    {
        return BSP_E_CP3BAN_SEM_NONE_EXISTED;
    }
    
    return BSP_E_CP3BAN_OK;  /* 返回成功 */
}


/**************************************************************************
* 函数名称: BSP_cp3banFpgaSemFree
* 功能描述: 释放信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护读写FPGA.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-03-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banFpgaSemFree(WORD32 dwSubslotId)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);

    if (BSP_CP3BAN_SEM_NONE_EXISTED != g_bsp_cp3ban_fpga_sem_flag[dwSubslotId])    
    {
        dwRetValue = BSP_OalSemV(g_bsp_cp3ban_fpga_sem_id[dwSubslotId]);
        if (BSP_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_OalSemV() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
            return BSP_E_CP3BAN_FREE_SEM_FAIL;
        }
    }
    else
    {
        return BSP_E_CP3BAN_SEM_NONE_EXISTED;
    }
    
    return BSP_E_CP3BAN_OK;  /* 返回成功 */
}


/****************************************************************************
* 函数名称: BSP_cp3banFpga16Read
* 功能描述: 读取CP3BAN单板的FPGA寄存器
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
*           wAddr: FPGA寄存器的偏移地址,16位的地址
* 输出参数: *pwData: 寄存器值
* 返 回 值: BSP_E_CP3BAN_OK: 读成功
*         : 其他: 读失败          
* 其它说明
* 修改日期     版本号                 修改人              修改内容
* -----------------------------------------------------------------------------
* 2013/06/17    V1.0                 xieweisheng           create
******************************************************************************/
WORD32 BSP_cp3banFpga16Read(WORD32 dwSubslotId, WORD16 wAddr, WORD16 *pwData)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    WORD32 dwTmpRetValue = BSP_E_CP3BAN_OK;
    WORD32 dwLoadFlag = 0;  /* FPGA加载标记 */
    WORD16 regValue = 0;
    WORD16 tmpRegValue = 0;
    WORD32 dwLoopTimes = 0;

    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pwData);

    dwRetValue = BSP_cp3banFpgaLoadFlagGet(dwSubslotId, &dwLoadFlag);  /* 获取FPGA加载标记 */
    if (BSP_E_CP3BAN_OK != dwRetValue)
        return ERROR;

    if (BSP_CP3BAN_FPGA_LOAD_UNFINISHED == dwLoadFlag)
        return ERROR;

    dwRetValue = BSP_cp3banFpgaSemGet(dwSubslotId);   /* Get semaphore. */
    if (BSP_E_CP3BAN_OK != dwRetValue)
        return ERROR;

    for (dwLoopTimes = 0; dwLoopTimes < BSP_CP3BAN_FPGA_STASUS_CHECK_NUM; dwLoopTimes++)
    {
        regValue = 0;
        dwRetValue = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_FPGA_STATUS_REG, &regValue);
        if (BSP_E_CP3BAN_OK != dwRetValue)
            return ERROR;

        if (0 == (regValue & ((WORD16)0x1)))
            break;
    }

    if (dwLoopTimes >= BSP_CP3BAN_FPGA_STASUS_CHECK_NUM)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, FPGA is not ready.\n", __FILE__, __LINE__, dwSubslotId);
        dwTmpRetValue = BSP_cp3banFpgaSemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetValue)
            return ERROR;
        return BSP_E_CP3BAN_FPGA_NOT_READY;
    }
    
    /* Write FPGA address */
    dwRetValue = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_FPGA_ADDR_REG, wAddr);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        dwTmpRetValue = BSP_cp3banFpgaSemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetValue)
            return ERROR;
        return dwRetValue;
    }

    /* Clear register control read/write. We only use bit0~bit2 of 0x1502 register. */
    regValue = 0;
    dwRetValue = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_FPGA_CTRL_REG, &regValue);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        dwTmpRetValue = BSP_cp3banFpgaSemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetValue)
            return ERROR;
        return dwRetValue;
    }
    tmpRegValue = (regValue & ((WORD16)0xfff8)) | ((WORD16)0x7);
    dwRetValue = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_FPGA_CTRL_REG, tmpRegValue);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        dwTmpRetValue = BSP_cp3banFpgaSemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetValue)
            return ERROR;
        return dwRetValue;
    }
    
    /* Create signal for read sequence. We only use bit0~bit2 of 0x1502 register. */
    regValue = 0;
    dwRetValue = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_FPGA_CTRL_REG, &regValue);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        dwTmpRetValue = BSP_cp3banFpgaSemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetValue)
            return ERROR;
        return dwRetValue;
    }
    tmpRegValue = (regValue & ((WORD16)0xfff8)) | ((WORD16)0x1);
    dwRetValue = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_FPGA_CTRL_REG, tmpRegValue);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        dwTmpRetValue = BSP_cp3banFpgaSemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetValue)
            return ERROR;
        return dwRetValue;
    }
        
    for (dwLoopTimes = 0; dwLoopTimes < BSP_CP3BAN_FPGA_STASUS_CHECK_NUM; dwLoopTimes++)
    {
        regValue = 0;
        dwRetValue = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_FPGA_STATUS_REG, &regValue);
        if (BSP_E_CP3BAN_OK != dwRetValue)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
            dwTmpRetValue = BSP_cp3banFpgaSemFree(dwSubslotId);  /* Free semaphore. */
            if (BSP_E_CP3BAN_OK != dwTmpRetValue)
                return ERROR;
            return dwRetValue;
        }
        if (0 == (regValue & ((WORD16)0x1)))
        {
            break;
        }
    }
    if (dwLoopTimes >= BSP_CP3BAN_FPGA_STASUS_CHECK_NUM)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, FPGA is not ready.\n", __FILE__, __LINE__, dwSubslotId);
        dwTmpRetValue = BSP_cp3banFpgaSemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetValue)
            return ERROR;
        return BSP_E_CP3BAN_FPGA_NOT_READY;
    }
    
    /* Read data from FPGA */
    *pwData = 0;
    regValue = 0;
    dwRetValue = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_FPGA_DATA_READ_REG, &regValue);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
       dwTmpRetValue = BSP_cp3banFpgaSemFree(dwSubslotId);  /* Free semaphore. */
       if (BSP_E_CP3BAN_OK != dwTmpRetValue)
           return ERROR;
       return dwRetValue;
    }
    *pwData = regValue;
    
    /* Clear register control read/write. We only use bit0~bit2 of 0x1502 register. */
    regValue = 0;
    dwRetValue = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_FPGA_CTRL_REG, &regValue);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        dwTmpRetValue = BSP_cp3banFpgaSemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetValue)
            return ERROR;
        return dwRetValue;
    }
    tmpRegValue = (regValue & ((WORD16)0xfff8)) | ((WORD16)0x7);
    dwRetValue = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_FPGA_CTRL_REG, tmpRegValue);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        dwTmpRetValue = BSP_cp3banFpgaSemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetValue)
            return ERROR;
        return dwRetValue;
    }

    dwRetValue = BSP_cp3banFpgaSemFree(dwSubslotId);  /* Free semaphore. */
    if (BSP_E_CP3BAN_OK != dwRetValue)
        return ERROR;
    return BSP_E_CP3BAN_OK;
}


/****************************************************************************
* 函数名称: BSP_cp3banFpga16Write
* 功能描述: 写CP3BAN单板的FPGA寄存器
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
*           wAddr: FPGA的偏移地址
*           wData: FPGA寄存器的值
* 输出参数:无
* 返 回 值:BSP_E_CP3BAN_OK: 写成功
*         :其他: 写失败          
* 其它说明
* 修改日期     版本号                 修改人              修改内容
* -----------------------------------------------------------------------------
* 2013/06/17   V1.0               xieweisheng             create
******************************************************************************/
WORD32 BSP_cp3banFpga16Write(WORD32 dwSubslotId, WORD16 wAddr, WORD16 wData)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    WORD32 dwTmpRetValue = BSP_E_CP3BAN_OK;
    WORD32 dwLoadFlag = 0;  /* FPGA加载标记 */
    WORD16 regValue = 0;
    WORD16 tmpRegValue = 0;
    WORD32 dwLoopTimes = 0;

    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);

    dwRetValue = BSP_cp3banFpgaLoadFlagGet(dwSubslotId, &dwLoadFlag);  /* 获取FPGA加载标记 */
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpgaLoadFlagGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return dwRetValue;
    }
    if (BSP_CP3BAN_FPGA_LOAD_UNFINISHED == dwLoadFlag) /* FPGA未加载 */
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, FPGA has not been loaded.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_FPGA_NOT_LOAD;
    }

    dwRetValue = BSP_cp3banFpgaSemGet(dwSubslotId);   /* Get semaphore. */
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpgaSemGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return dwRetValue;
    }
    
    for (dwLoopTimes = 0; dwLoopTimes < BSP_CP3BAN_FPGA_STASUS_CHECK_NUM; dwLoopTimes++)
    {
        regValue = 0;
        dwRetValue = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_FPGA_STATUS_REG, &regValue);
        if (BSP_E_CP3BAN_OK != dwRetValue)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
            dwTmpRetValue = BSP_cp3banFpgaSemFree(dwSubslotId);  /* Free semaphore. */
            if (BSP_E_CP3BAN_OK != dwTmpRetValue)
            {
                BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpgaSemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetValue);
                return dwTmpRetValue;
            }
            return dwRetValue;
        }
        if (0 == (regValue & ((WORD16)0x1)))
        {
            break;
        }
    }
    if (dwLoopTimes >= BSP_CP3BAN_FPGA_STASUS_CHECK_NUM)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, FPGA is not ready.\n", __FILE__, __LINE__, dwSubslotId);
        dwTmpRetValue = BSP_cp3banFpgaSemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetValue)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpgaSemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetValue);
            return dwTmpRetValue;
        }
        return BSP_E_CP3BAN_FPGA_NOT_READY;
    }
    
    /* Write FPGA address  */
    dwRetValue = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_FPGA_ADDR_REG, wAddr);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        dwTmpRetValue = BSP_cp3banFpgaSemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetValue)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpgaSemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetValue);
            return dwTmpRetValue;
        }
        return dwRetValue;
    }

    /* Write data to FPGA */
    dwRetValue = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_FPGA_DATA_WRITE_REG, wData);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        dwTmpRetValue = BSP_cp3banFpgaSemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetValue)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpgaSemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetValue);
            return dwTmpRetValue;
        }
        return dwRetValue;
    }
    
    /* Clear register control read/write. We only use bit0~bit2 of 0x1502 register. */
    regValue = 0;
    dwRetValue = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_FPGA_CTRL_REG, &regValue);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        dwTmpRetValue = BSP_cp3banFpgaSemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetValue)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpgaSemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetValue);
            return dwTmpRetValue;
        }
        return dwRetValue;
    }
    tmpRegValue = (regValue & ((WORD16)0xfff8)) | ((WORD16)0x7);
    dwRetValue = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_FPGA_CTRL_REG, tmpRegValue);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        dwTmpRetValue = BSP_cp3banFpgaSemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetValue)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpgaSemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetValue);
            return dwTmpRetValue;
        }
        return dwRetValue;
    }
    
    /* Create signal for write sequence. We only use bit0~bit2 of 0x1502 register. */
    regValue = 0;
    dwRetValue = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_FPGA_CTRL_REG, &regValue);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        dwTmpRetValue = BSP_cp3banFpgaSemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetValue)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpgaSemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetValue);
            return dwTmpRetValue;
        }
        return dwRetValue;
    }
    tmpRegValue = (regValue & ((WORD16)0xfff8)) | ((WORD16)0x2);
    dwRetValue = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_FPGA_CTRL_REG, tmpRegValue);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        dwTmpRetValue = BSP_cp3banFpgaSemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetValue)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpgaSemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetValue);
            return dwTmpRetValue;
        }
        return dwRetValue;
    }
        
    for (dwLoopTimes = 0; dwLoopTimes < BSP_CP3BAN_FPGA_STASUS_CHECK_NUM; dwLoopTimes++)
    {
        regValue = 0;
        dwRetValue = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_FPGA_STATUS_REG, &regValue);
        if (BSP_E_CP3BAN_OK != dwRetValue)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
            dwTmpRetValue = BSP_cp3banFpgaSemFree(dwSubslotId);  /* Free semaphore. */
            if (BSP_E_CP3BAN_OK != dwTmpRetValue)
            {
                BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpgaSemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetValue);
                return dwTmpRetValue;
            }
            return dwRetValue;
        }
        if (0 == (regValue & ((WORD16)0x1)))
        {
            break;
        }
    }
    if (dwLoopTimes >= BSP_CP3BAN_FPGA_STASUS_CHECK_NUM)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, FPGA is not ready.\n", __FILE__, __LINE__, dwSubslotId);
        dwTmpRetValue = BSP_cp3banFpgaSemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetValue)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpgaSemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetValue);
            return dwTmpRetValue;
        }
        return BSP_E_CP3BAN_FPGA_NOT_READY;
    }
    
    /* Clear register control read/write. We only use bit0~bit2 of 0x1502 register. */
    regValue = 0;
    dwRetValue = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_FPGA_CTRL_REG, &regValue);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        dwTmpRetValue = BSP_cp3banFpgaSemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetValue)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpgaSemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetValue);
            return dwTmpRetValue;
        }
        return dwRetValue;
    }
    tmpRegValue = (regValue & ((WORD16)0xfff8)) | ((WORD16)0x7);
    dwRetValue = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_FPGA_CTRL_REG, tmpRegValue);
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        dwTmpRetValue = BSP_cp3banFpgaSemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetValue)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpgaSemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetValue);
            return dwTmpRetValue;
        }
        return dwRetValue;
    }

    dwRetValue = BSP_cp3banFpgaSemFree(dwSubslotId);  /* Free semaphore. */
    if (BSP_E_CP3BAN_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpgaSemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return dwRetValue;
    }
    return BSP_E_CP3BAN_OK;
}


/******************************************************************************
 * 函数名称：BSP_cp3banFpgaVerify
 * 功能描述：校验CP3BAN单板的FPGA芯片
 * 输入参数：dwSubslotId   --CP3BAN单板的子槽位号
 * 输出参数：无
 * 返 回 值：BSP_E_CP3BAN_OK     --成功
 *           其他                --失败
 * 其它说明： 
 * 修改日期      版本号      修改人        修改内容
 * -----------------------------------------------------------
 * 2013/07/25        v1.0     xieweisheng10112265       create
 ******************************************************************************/
WORD32 BSP_cp3banFpgaVerify(WORD32 dwSubslotId)
{
    WORD32 dwRet  = BSP_E_CP3BAN_OK;
    WORD32 dwLoadFlag = 0;   /* FPGA加载标记 */
    WORD32 dwSystemType = 0;  /* ZXUPN15000的系统类型 */
    WORD32 dwCheckTimes = 0;  /* FPGA校验次数 */
    WORD16 wRdRegAddr = 0;   /* FPGA读校验寄存器的偏移地址 */
    WORD16 wWrRegAddr = 0;   /* FPGA写校验寄存器的偏移地址 */
    WORD16 wRegValue  = 0;   /* FPGA寄存器的值 */
    WORD32 dwRdFailTimes = 0;   /* FPGA读失败次数 */
    WORD32 dwWrFailTimes = 0;   /* FPGA写失败次数 */
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);

    /* 获取FPGA加载标记 */
    dwRet = BSP_cp3banFpgaLoadFlagGet(dwSubslotId, &dwLoadFlag);
    if (BSP_E_CP3BAN_OK != dwRet)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpgaLoadFlagGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
        return dwRet;
    }
    if (BSP_CP3BAN_FPGA_LOAD_UNFINISHED == dwLoadFlag) /* FPGA未加载 */
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, FPGA has not been loaded.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_FPGA_NOT_LOAD;
    }

    dwSystemType = BSP_cp3banSystemTypeGet(); /* 获取ZXUPN15000系统类型. */
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM == dwSystemType)   /* 非15K-2系统 */
    {
        wRdRegAddr = BSP_CP3BAN_FPGA_CHECK_RD_REG << 1;
        wWrRegAddr = BSP_CP3BAN_FPGA_CHECK_WR_REG << 1;
    }
    else  /* 15K-2系统 */
    {
        wRdRegAddr = BSP_CP3BAN_FPGA_CHECK_RD_REG;
        wWrRegAddr = BSP_CP3BAN_FPGA_CHECK_WR_REG;
    }

    if (BSP_CP3BAN_NONE_15K_2_SYSTEM == dwSystemType)  /* 非15K-2系统 */
    {
        /* 校验FPGA读操作 */    
        for (dwCheckTimes = 0; dwCheckTimes < BSP_CP3BAN_FPGA_VERIFY_TIMES; dwCheckTimes++)
        {
            wRegValue = 0;
            dwRet = BSP_cp3banFpga16Read(dwSubslotId, wRdRegAddr, &wRegValue);
            if (BSP_E_CP3BAN_OK != dwRet)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpga16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
                return dwRet;
            }
            if (((WORD16)0xaaf6) != wRegValue)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, checkTime=%u, FPGA regAddr=0x0700 regValue=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwCheckTimes, wRegValue);
                dwRdFailTimes++;
            }
        }
        if (dwRdFailTimes > 0)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, Verify FPGA reading unsuccessully, readFailTimes=%u.\n", __FILE__, __LINE__, dwSubslotId, dwRdFailTimes);
            return BSP_E_CP3BAN_FPGA_CHECK_FAIL;
        }
        
        /* 校验FPGA写操作 */
        for (dwCheckTimes = 0; dwCheckTimes < BSP_CP3BAN_FPGA_VERIFY_TIMES; dwCheckTimes++)
        {
            dwRet = BSP_cp3banFpga16Write(dwSubslotId, wWrRegAddr, ((WORD16)0xaa55));
            if (BSP_E_CP3BAN_OK != dwRet)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpga16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
                return dwRet;
            }
            wRegValue = 0;
            dwRet = BSP_cp3banFpga16Read(dwSubslotId, wWrRegAddr, &wRegValue);
            if (BSP_E_CP3BAN_OK != dwRet)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpga16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
                return dwRet;
            }
            if (((WORD16)0xaa55) != wRegValue)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, checkTime=%u, FPGA regAddr=0x0701 regValue=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwCheckTimes, wRegValue);
                dwWrFailTimes++;
            }
        }
        if (dwWrFailTimes > 0)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, Verify FPGA writing unsuccessully, writeFailTimes=%u.\n", __FILE__, __LINE__, dwSubslotId, dwWrFailTimes);
            return BSP_E_CP3BAN_FPGA_CHECK_FAIL;
        }
    }

    return BSP_E_CP3BAN_OK;
}


/******************************************************************************
 * 函数名称: BSP_cp3banDeviceVerify
 * 功能描述: 校验CP3BAN单板的芯片
 * 输入参数: dwSubslotId   --CP3BAN单板的子槽位号
 *           dwDevType     --芯片类型
 * 输出参数: 无
 * 返 回 值: BSP_E_CP3BAN_OK     --成功
 *           其他                --失败
 * 其它说明: 需要校验CP3BAN单板的EPLD和FPGA芯片. 
 * 修改日期      版本号      修改人        修改内容
 * -----------------------------------------------------------
 * 2013/07/25        v1.0     xieweisheng10112265       create
 ******************************************************************************/
WORD32 BSP_cp3banDeviceVerify(WORD32 dwSubslotId, WORD32 dwDevType, WORD32 *pResult)
{
    WORD32 dwRet = BSP_E_CP3BAN_OK;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pResult);
    *pResult = BSP_SUBCARD_DEVICE_VERIFY_FAIL; /* 赋初值 */
    
    switch (dwDevType)
    {
        case BSP_DEVID_EPLD:   /* EPLD芯片 */
            dwRet = BSP_cp3banEpldVerify(dwSubslotId);
            if (BSP_E_CP3BAN_OK != dwRet)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpldVerify() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
                *pResult = BSP_SUBCARD_DEVICE_VERIFY_FAIL;
                return dwRet;
            }
            *pResult = BSP_SUBCARD_DEVICE_VERIFY_SUCCESS;
            break;
        case BSP_DEVID_FPGA:  /* FPGA芯片 */
            dwRet = BSP_cp3banFpgaVerify(dwSubslotId);
            if (BSP_E_CP3BAN_OK != dwRet)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpgaVerify() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
                *pResult = BSP_SUBCARD_DEVICE_VERIFY_FAIL;
                return dwRet;
            }
            *pResult = BSP_SUBCARD_DEVICE_VERIFY_SUCCESS;
            break;
        default:
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, invalid device type, deviceType=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwDevType);
            *pResult = BSP_SUBCARD_DEVICE_VERIFY_NOTSUPPORT;
            return BSP_E_CP3BAN_PARA;
    }
    
    return BSP_E_CP3BAN_OK;
}


/******************************************************************************
* 函数名称: BSP_cp3banFpgaDoneCheck
* 功能描述: FPGA下载,DONE信号检查.
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号
* 输出参数:
* 返 回 值:                      
* 其它说明:
* 修改日期      版本号           修改人             修改内容
* -----------------------------------------------------------------------------
* 2013/06/18     v1.0          XieWeisheng10112265   create
******************************************************************************/
WORD32 BSP_cp3banFpgaDoneCheck(WORD32 dwSubslotId)
{
    WORD32 dwRet = BSP_E_CP3BAN_OK;
    WORD16 wRegVal = 0;
    WORD32 dwLoopIndex = 0;

    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    
    for (dwLoopIndex = 0; dwLoopIndex < BSP_CP3BAN_FPGA_DONE_CHECK_NUM; dwLoopIndex++) 
    {
        wRegVal = 0;
        dwRet = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_EPLD_FPGA_LOAD_CTRL_REG, &wRegVal);
        if (BSP_E_CP3BAN_OK != dwRet)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
            return dwRet;
        }
        if (0 != (wRegVal & BSP_CP3BAN_FPGA_DONE))
        {
            break;
        }
        BSP_DelayMs(10);  /* delay 10ms. */
    }
    
    if (dwLoopIndex >= BSP_CP3BAN_FPGA_DONE_CHECK_NUM)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, CP3BAN board downloads FPAG unsuccessfully!\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_FPGA_LOAD_FAIL;
    } 
    
    return BSP_E_CP3BAN_OK;
}


/******************************************************************************
* 函数名称: BSP_cp3banFpgaLoadFlagGet
* 功能描述: 获取FPGA加载标记
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值范围为1~4.
* 输出参数: *pFlag: 保存FPGA加载标记,0表示未加载,非0表示已经加载
* 返 回 值:                      
* 其它说明:
* 修改日期     版本号                 修改人             修改内容
* ----------------------------------------------------------------------------
* 2013/06/18   v1.0                XieWeisheng10112265   create
******************************************************************************/
WORD32 BSP_cp3banFpgaLoadFlagGet(WORD32 dwSubslotId, WORD32 *pFlag)
{
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pFlag);
    
    *pFlag = g_bsp_cp3banFpgaLoadFlag[dwSubslotId];
    
    return BSP_E_CP3BAN_OK;
}


/******************************************************************************
* 函数名称: BSP_cp3banFpgaLoadFlagSet
* 功能描述: 设置FPGA加载标记
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值范围为1~4.
* 输出参数: flag: FPGA加载标记,0表示未加载,非0表示已经加载
* 返 回 值:                      
* 其它说明:
* 修改日期     版本号                 修改人             修改内容
* ----------------------------------------------------------------------------
* 2013/06/18   v1.0                XieWeisheng10112265   create
******************************************************************************/
WORD32 BSP_cp3banFpgaLoadFlagSet(WORD32 dwSubslotId, WORD32 flag)
{
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    
    g_bsp_cp3banFpgaLoadFlag[dwSubslotId] = flag;
    
    return BSP_E_CP3BAN_OK;
}


/******************************************************************************
* 函数名称: BSP_cp3banFpgaLoad
* 功能描述: 加载FPGA逻辑
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号.
* 输出参数:
* 返 回 值:                      
* 其它说明:
* 修改日期     版本号                 修改人             修改内容
* ----------------------------------------------------------------------------
* 2013/06/18   v1.0                XieWeisheng10112265   create
******************************************************************************/
WORD32 BSP_cp3banFpgaLoad(WORD32 dwSubslotId, 
                                    WORD32 dwSegmentIndex, 
                                    BYTE *pucLogicDataBuf, 
                                    WORD32 dwBufLen)
{
    WORD32 dwRet = BSP_E_CP3BAN_OK;
    WORD32 initFlag = 0;
    WORD16 wCtrlRegVal = 0;
    WORD16 wData       = 0;
    WORD32 dwLoopIndex = 0;
    time_t currentTime;    /* 以秒为单位 */

    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pucLogicDataBuf);
    memset(&currentTime, 0, sizeof(time_t));

    dwRet = BSP_cp3banDeviceInitFlagGet(dwSubslotId, &initFlag);
    if (BSP_E_CP3BAN_OK != dwRet)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banDeviceInitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
        return dwRet;
    }
    if (BSP_CP3BAN_DEVICE_UNINIT == initFlag)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, CP3BAN device has not been initialized.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_NOT_READY;
    }
    
    /*first segment*/
    if (BSP_BRDCTRL_FIRMLOAD_FIRSEG == dwSegmentIndex)
    {
        dwRet = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_EPLD_FPGA_LOAD_CTRL_REG, &wCtrlRegVal);
        if (BSP_E_CP3BAN_OK != dwRet)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
            return dwRet;
        }
        wCtrlRegVal &= ~BSP_SUBCARD_FPGA_PROG_CTRL;
        
        /* 先写0xaa,再写0x33,把PROGRAM 置低,清FPGA写0x5a,把PROGRAM置高.清除结束 */
        wData = wCtrlRegVal | ((WORD16)0xaa00);
        dwRet = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_EPLD_FPGA_LOAD_CTRL_REG, wData);
        if (BSP_E_CP3BAN_OK != dwRet)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
            return dwRet;
        }
        
        wData = wCtrlRegVal | ((WORD16)0x3300);
        dwRet = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_EPLD_FPGA_LOAD_CTRL_REG, wData);
        if (BSP_E_CP3BAN_OK != dwRet)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
            return dwRet;
        }
        BSP_DelayMs(1);
        
        wData = wCtrlRegVal | ((WORD16)0x5a00);
        dwRet = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_EPLD_FPGA_LOAD_CTRL_REG, wData);
        if (BSP_E_CP3BAN_OK != dwRet)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
            return dwRet;
        }
        
        /* Check status */
        for (dwLoopIndex = 0; dwLoopIndex < BSP_CP3BAN_FPGA_STASUS_CHECK_NUM; dwLoopIndex++)
        {
            /*wait sometimes*/
            BSP_DelayMs(10);
            wCtrlRegVal = 0;
            dwRet = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_EPLD_FPGA_LOAD_CTRL_REG, &wCtrlRegVal);
            if (BSP_E_CP3BAN_OK != dwRet)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
                return dwRet;
            }
            /*CPLD的0x14寄存器的bit5,若该比特的值为1表示FPGA初始化成功,为0表示FPGA初始化失败 */
            if (0!= (wCtrlRegVal & BSP_SUBCARD_FPGA_INIT))
            {
                break;
            }
        }
        
        if (dwLoopIndex >= BSP_CP3BAN_FPGA_STASUS_CHECK_NUM)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, CP3BAN board (subslotId=%u) initializes FPGA unsuccessfully, CPLD offsetAddr=0x0014, value=0x%x.\n", __FILE__, __LINE__, dwSubslotId, wCtrlRegVal);
            return BSP_E_CP3BAN_FPGA_LOAD_FAIL;
        }
        
        /*CPLD的0x14寄存器的bit4,若该比特的值为1表示FPGA配置成功,为0表示FPGA配置失败或未结束 */
        if (0 != (wCtrlRegVal & BSP_SUBCARD_FPGA_DONE))
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, CP3BAN board (subslotId=%u) configures FPGA unsuccessfully, CPLD offsetAddr=0x0014, value=0x%x.\n", __FILE__, __LINE__, dwSubslotId, wCtrlRegVal);
            return BSP_E_CP3BAN_ERROR;
        }
        
        dwRet = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_EPLD_FPGA_LOAD_CTRL_REG, &wCtrlRegVal);
        if (BSP_E_CP3BAN_OK != dwRet)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
            return dwRet;
        }
        /*CPLD的0x14寄存器的bit0,若该比特的值为0表示允许片选,为1表示禁止片选 */
        wData = wCtrlRegVal & (~BSP_CP3BAN_FPGA_CS_FLAG);
        dwRet = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_EPLD_FPGA_LOAD_CTRL_REG, wData);
        if (BSP_E_CP3BAN_OK != dwRet)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
            return dwRet;
        }
        /*CPLD的0x14寄存器的bit1,若该比特的值为0表示允许写入配置数据,为1表示禁止写入配置数据 */
        wData = wCtrlRegVal & (~BSP_SUBCARD_FPGA_WR);  /* 配置写信号状态 */
        dwRet = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_EPLD_FPGA_LOAD_CTRL_REG, wData); 
        if (BSP_E_CP3BAN_OK != dwRet)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
            return dwRet;
        }
    }
    
    for (dwLoopIndex = 0; dwLoopIndex < dwBufLen; dwLoopIndex += 2)
    {
        wData = ((WORD16)(pucLogicDataBuf[dwLoopIndex + 1]) & ((WORD16)0xff)) << 8;
        wData |= (WORD16)(pucLogicDataBuf[dwLoopIndex]) & 0xff;
        dwRet = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_EPLD_FPGA_LOAD_DATA_REG, wData); /* 写入数据 */
        if (BSP_E_CP3BAN_OK != dwRet)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
            return dwRet;
        }
    }
    
    if (BSP_BRDCTRL_FIRMLOAD_LASTSEG == dwSegmentIndex)
    {
        dwRet = BSP_cp3banFpgaDoneCheck(dwSubslotId);
        if (BSP_E_CP3BAN_OK != dwRet)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpgaDoneCheck() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
            return dwRet;
        }
        
        dwRet = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_EPLD_FPGA_LOAD_CTRL_REG, &wCtrlRegVal);
        if (BSP_E_CP3BAN_OK != dwRet)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
            return dwRet;
        }
        /* CPLD的0x14寄存器的bit0,若该比特的值为0表示允许片选,为1表示禁止片选 */
        wData = wCtrlRegVal | BSP_CP3BAN_FPGA_CS_FLAG;
        dwRet = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_EPLD_FPGA_LOAD_CTRL_REG, wData);
        if (BSP_E_CP3BAN_OK != dwRet)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
            return dwRet;
        }
        /*CPLD的0x14寄存器的bit1,若该比特的值为0表示允许写入配置数据,为1表示禁止写入配置数据 */
        wData = wCtrlRegVal | BSP_SUBCARD_FPGA_WR;   
        dwRet = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_EPLD_FPGA_LOAD_CTRL_REG, wData);
        if (BSP_E_CP3BAN_OK != dwRet)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
            return dwRet;
        }

        dwRet = BSP_cp3banFpgaLoadFlagSet(dwSubslotId, BSP_CP3BAN_FPGA_LOAD_FINISHED);  /* FPGA加载完毕 */
        if (BSP_E_CP3BAN_OK != dwRet)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpgaLoadFlagSet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
            return dwRet;
        }
        time(&currentTime); /* Get current calendar time in seconds */
        BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, %s, subslotId %u, Download FPAG successfully.\n", __FILE__, __LINE__, ctime(&currentTime), dwSubslotId);
    }

    return BSP_E_CP3BAN_OK;
}    


/**************************************************************************
* 函数名称: BSP_cp3banAd9557SemFlagGet
* 功能描述: 获取信号量的创建标记.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号.
* 输出参数: *pdwFlag: 保存信号量的创建标记.
* 返 回 值: 
* 其它说明: 该信号量用来保护读写AD9557. 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-03-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banAd9557SemFlagGet(WORD32 dwSubslotId, WORD32 *pdwFlag)
{
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pdwFlag);
    
    *pdwFlag = 0;  /* 先清零再赋值 */
    *pdwFlag = g_bsp_cp3ban_ad9557_sem_flag[dwSubslotId];
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banAd9557SemIdGet
* 功能描述: 获取信号量ID.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号.
* 输出参数: *pdwSemId: 保存信号量ID.
* 返 回 值: 
* 其它说明: 该信号量用来保护读写AD9557. 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-03-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banAd9557SemIdGet(WORD32 dwSubslotId, WORD32 *pdwSemId)
{
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pdwSemId);
    
    *pdwSemId = 0;  /* 先清零再赋值 */
    *pdwSemId = g_bsp_cp3ban_ad9557_sem_id[dwSubslotId];
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banAd9557SemCreate
* 功能描述: 创建信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护读写AD9557.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-03-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banAd9557SemCreate(WORD32 dwSubslotId)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);

    if (BSP_CP3BAN_SEM_NONE_EXISTED == g_bsp_cp3ban_ad9557_sem_flag[dwSubslotId])
    {
        dwRetValue = BSP_OalCreateSem(1, MUTEX_STYLE, &(g_bsp_cp3ban_ad9557_sem_id[dwSubslotId]));
        if (BSP_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_OalCreateSem() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
            return BSP_E_CP3BAN_CREATE_SEM_FAIL;
        } 
        g_bsp_cp3ban_ad9557_sem_flag[dwSubslotId] = BSP_CP3BAN_SEM_EXISTED;
    }  

    return BSP_E_CP3BAN_OK;  /* 返回成功 */
}


/**************************************************************************
* 函数名称: BSP_cp3banAd9557SemDestroy
* 功能描述: 销毁信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护读写AD9557.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-03-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banAd9557SemDestroy(WORD32 dwSubslotId)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);

    /* 在销毁信号量之前,先必须LOCK一下信号量 */
    dwRetValue = BSP_cp3banAd9557SemGet(dwSubslotId);
    if (BSP_E_CP3BAN_SEM_NONE_EXISTED == dwRetValue)
    {
        BSP_Print(BSP_DEBUG_ALL, "WARNING: %s line %d, g_bsp_cp3ban_ad9557_sem_id[%u] has been destroyed.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_OK;
    }
    else if (BSP_E_CP3BAN_OK == dwRetValue)
    {
        ;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return dwRetValue;
    }

    if (BSP_CP3BAN_SEM_NONE_EXISTED != g_bsp_cp3ban_ad9557_sem_flag[dwSubslotId])    
    {
        dwRetValue = BSP_OalDestroySem(g_bsp_cp3ban_ad9557_sem_id[dwSubslotId]);
        if (BSP_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_OalDestroySem() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
            return BSP_E_CP3BAN_DESTROY_SEM_FAIL;
        }
        g_bsp_cp3ban_ad9557_sem_id[dwSubslotId] = 0;
        g_bsp_cp3ban_ad9557_sem_flag[dwSubslotId] = BSP_CP3BAN_SEM_NONE_EXISTED;
    }

    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banAd9557SemGet
* 功能描述: 获取信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护读写AD9557.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-03-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banAd9557SemGet(WORD32 dwSubslotId)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);

    if (BSP_CP3BAN_SEM_NONE_EXISTED != g_bsp_cp3ban_ad9557_sem_flag[dwSubslotId])    
    {
        dwRetValue = BSP_OalSemP(g_bsp_cp3ban_ad9557_sem_id[dwSubslotId], WAIT_FOREVER);
        if (BSP_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_OalSemP() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
            return BSP_E_CP3BAN_GET_SEM_FAIL;
        }
    }
    else
    {
        return BSP_E_CP3BAN_SEM_NONE_EXISTED;
    }

    return BSP_E_CP3BAN_OK;  /* 返回成功 */
}


/**************************************************************************
* 函数名称: BSP_cp3banAd9557SemFree
* 功能描述: 释放信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护读写AD9557.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-03-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banAd9557SemFree(WORD32 dwSubslotId)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);

    if (BSP_CP3BAN_SEM_NONE_EXISTED != g_bsp_cp3ban_ad9557_sem_flag[dwSubslotId])    
    {
        dwRetValue = BSP_OalSemV(g_bsp_cp3ban_ad9557_sem_id[dwSubslotId]);
        if (BSP_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_OalSemV() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
            return BSP_E_CP3BAN_FREE_SEM_FAIL;
        }
    }
    else
    {
        return BSP_E_CP3BAN_SEM_NONE_EXISTED;
    }

    return BSP_E_CP3BAN_OK;  /* 返回成功 */
}


/**************************************************************************
* 函数名称: BSP_cp3ban_Ad9557_1_Check
* 功能描述: 检查CP3BAN单板的1号AD9557时钟芯片的读写状态
* 输入参数: dwSubslotId         --CP3BAN单板的子槽位号
* 输出参数: 无
* 返 回 值: BSP_E_CP3BAN_OK     
*         : 其他                
* 其它说明：
* 修改日期     版本号                     修改人                 修该内容
* -----------------------------------------------------------------------------
* 2013/03/26    V1.0                      gaoyun                 create
******************************************************************************/
WORD32 BSP_cp3ban_Ad9557_1_Check(WORD32 dwSubslotId)
{
    WORD32 dwRet = BSP_E_CP3BAN_OK;
    WORD16 wData = 0;
    WORD32 ii = 0;
    WORD32 dwToaltime = 0;

    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);

    for (ii = 0; ii < BSP_CP3BAN_AD9557_TIMEOUT; ii++)
    {
        wData = 0;
        /* bit1表示操作控制信号,读写完成后该位自动恢复为0.根据该位是否为0来判断读写操作是否结束 */
        dwRet = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_AD9557_1_CTRL_REG, &wData);
        if (BSP_E_CP3BAN_OK != dwRet)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
            return dwRet;
        }
        if (0 == (wData & ((WORD16)0x2)))
        {
            break;
        }
        
        BSP_DelayUs(1); /* delay 1us. */
        dwToaltime++;
    }
    if (dwToaltime >= BSP_CP3BAN_AD9557_TIMEOUT)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, read/write AD9557_1 timeout.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_AD9557_TIMEOUT;
    }
    
    return BSP_E_CP3BAN_OK;    
}


/**************************************************************************
* 函数名称: BSP_cp3ban_Ad9557_2_Check
* 功能描述: 检查CP3BAN单板的2号AD9557时钟芯片的读写状态
* 输入参数: dwSubslotId         --CP3BAN单板的子槽位号
* 输出参数: 无
* 返 回 值: BSP_E_CP3BAN_OK     
*         : 其他                
* 其它说明：
* 修改日期     版本号                     修改人                 修该内容
* -----------------------------------------------------------------------------
* 2013/03/26    V1.0                      gaoyun                 create
******************************************************************************/
WORD32 BSP_cp3ban_Ad9557_2_Check(WORD32 dwSubslotId)
{
    WORD32 dwRet = BSP_E_CP3BAN_OK;
    WORD16 wData = 0;
    WORD32 ii = 0;
    WORD32 dwToaltime = 0;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    
    for (ii = 0; ii < BSP_CP3BAN_AD9557_TIMEOUT; ii++)
    {
        wData = 0;
        /* bit1表示操作控制信号,读写完成后该位自动恢复为0.根据该位是否为0来判断读写操作是否结束 */
        dwRet = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_AD9557_2_CTRL_REG, &wData);
        if (BSP_E_CP3BAN_OK != dwRet)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRet);
            return dwRet;
        }
        if (0 == (wData & ((WORD16)0x2)))
        {
            break;
        }
        
        BSP_DelayUs(1); /* delay 1us. */
        dwToaltime++;
    }
    if (dwToaltime >= BSP_CP3BAN_AD9557_TIMEOUT)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, read/write AD9557_2 timeout.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_AD9557_TIMEOUT;
    }
    
    return BSP_E_CP3BAN_OK;    
}


/**************************************************************************
* 函数名称: BSP_cp3ban_ad9557_1_read
* 功能描述: 读取1号AD9557芯片的寄存器
* 输入参数: dwSubslotId:     --CP3BAN单板的子槽位号
*           wRegAddr         --AD9557的寄存器地址
* 输出参数: *pwData          --寄存器值
* 返 回 值: BSP_E_CP3BAN_OK     --读成功
*         : 其他                 --读失败
* 其它说明：
* 修改日期     版本号                     修改人                 修该内容
* -----------------------------------------------------------------------------
* 2013/03/26    V1.0                      gaoyun                 create
******************************************************************************/ 
WORD32 BSP_cp3ban_ad9557_1_read(WORD32 dwSubslotId, 
                                            WORD16 wRegAddr, 
                                            WORD16 *pwData)
{
    WORD32 dwRetVal = BSP_E_CP3BAN_OK;
    WORD32 dwTmpRetVal = BSP_E_CP3BAN_OK;
    WORD16 wRegData = 0;
    WORD16 tmpRegValue = 0;

    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pwData);

    dwRetVal = BSP_cp3banAd9557SemGet(dwSubslotId);  /* Get semaphore. */
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        return dwRetVal;
    }

    dwRetVal = BSP_cp3ban_Ad9557_1_Check(dwSubslotId);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_Ad9557_1_Check() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        dwTmpRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetVal);
            return dwTmpRetVal;
        }
        return dwRetVal;
    }

    /* bit0~bit11为有效寄存器地址空间. */
    wRegAddr &= ((WORD16)0x0fff);
    
    /* write the AD9557'address to reg. */
    dwRetVal = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_AD9557_1_ADDR_REG, wRegAddr);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        dwTmpRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetVal);
            return dwTmpRetVal;
        }
        return dwRetVal;
    }
    
    /* bit0表示读写命令标志,0表示写操作,1表示读操作. */
    dwRetVal = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_AD9557_1_CTRL_REG, &tmpRegValue);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        dwTmpRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetVal);
            return dwTmpRetVal;
        }
        return dwRetVal;
    }
    tmpRegValue = tmpRegValue | ((WORD16)0x3);
    dwRetVal = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_AD9557_1_CTRL_REG, tmpRegValue);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        dwTmpRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetVal);
            return dwTmpRetVal;
        }
        return dwRetVal;
    }

    BSP_DelayUs(5); /* delay 5us. */
    
    dwRetVal = BSP_cp3ban_Ad9557_1_Check(dwSubslotId);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_Ad9557_1_Check() rv=0x%x,\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        dwTmpRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetVal);
            return dwTmpRetVal;
        }
        return dwRetVal;
    }
    
    /*get the data*/
    dwRetVal = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_AD9557_1_DATA_REG, &wRegData);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        dwTmpRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetVal);
            return dwTmpRetVal;
        }
        return dwRetVal;
    }
    *pwData = wRegData;

    dwRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        return dwRetVal;
    }
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3ban_ad9557_2_read
* 功能描述: 读取2号AD9557芯片的寄存器
* 输入参数: dwSubslotId:     --CP3BAN单板的子槽位号
*           wRegAddr         --AD9557的寄存器地址
* 输出参数: *pwData          --寄存器值
* 返 回 值: BSP_E_CP3BAN_OK     --读成功
*         : 其他                 --读失败
* 其它说明：
* 修改日期     版本号                     修改人                 修该内容
* -----------------------------------------------------------------------------
* 2013/03/26    V1.0                      gaoyun                 create
******************************************************************************/ 
WORD32 BSP_cp3ban_ad9557_2_read(WORD32 dwSubslotId, 
                                            WORD16 wRegAddr, 
                                            WORD16 *pwData)
{
    WORD32 dwRetVal = BSP_E_CP3BAN_OK;
    WORD32 dwTmpRetVal = BSP_E_CP3BAN_OK;
    WORD16 wRegData = 0;
    WORD16 tmpRegValue = 0;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pwData);

    dwRetVal = BSP_cp3banAd9557SemGet(dwSubslotId);  /* Get semaphore. */
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        return dwRetVal;
    }

    dwRetVal = BSP_cp3ban_Ad9557_2_Check(dwSubslotId);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_Ad9557_2_Check() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        dwTmpRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetVal);
            return dwTmpRetVal;
        }
        return dwRetVal;
    }
    
    /* bit0~bit11为有效寄存器地址空间. */
    wRegAddr &= ((WORD16)0x0fff);
    
    /* write the AD9557'address to reg. */
    dwRetVal = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_AD9557_2_ADDR_REG, wRegAddr);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        dwTmpRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetVal);
            return dwTmpRetVal;
        }
        return dwRetVal;
    }
    
    /* bit0表示读写命令标志,0表示写操作,1表示读操作. */
    dwRetVal = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_AD9557_2_CTRL_REG, &tmpRegValue);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        dwTmpRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetVal);
            return dwTmpRetVal;
        }
        return dwRetVal;
    }
    tmpRegValue = tmpRegValue | ((WORD16)0x3);
    dwRetVal = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_AD9557_2_CTRL_REG, tmpRegValue);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        dwTmpRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetVal);
            return dwTmpRetVal;
        }
        return dwRetVal;
    }
    
    BSP_DelayUs(5); /* delay 5us. */
    
    dwRetVal = BSP_cp3ban_Ad9557_2_Check(dwSubslotId);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_Ad9557_2_Check() rv=0x%x,\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        dwTmpRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetVal);
            return dwTmpRetVal;
        }
        return dwRetVal;
    }
    
    /*get the data*/
    dwRetVal = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_AD9557_2_DATA_REG, &wRegData);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        dwTmpRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetVal);
            return dwTmpRetVal;
        }
        return dwRetVal;
    }
    *pwData = wRegData;

    dwRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        return dwRetVal;
    }
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3ban_ad9557_1_write
* 功能描述: 写1号AD9557芯片的寄存器
* 输入参数: dwSubslotId         --CP3BAN单板的子槽位号
*           wRegAddr            --子卡AD9557的寄存器地址
*           wData               --寄存器值
* 输出参数: 无
* 返 回 值: BSP_E_CP3BAN_OK     --写成功
*         : 其他                --写失败
* 其它说明：
* 修改日期     版本号                     修改人                 修该内容
* -----------------------------------------------------------------------------
* 2013/03/26    V1.0                      gaoyun                 create
******************************************************************************/ 
WORD32 BSP_cp3ban_ad9557_1_write(WORD32 dwSubslotId, 
                                             WORD16 wRegAddr, 
                                             WORD16 wData)
{
    WORD32 dwRetVal = BSP_E_CP3BAN_OK;
    WORD32 dwTmpRetVal = BSP_E_CP3BAN_OK;
    WORD16 tmpRegValue = 0;

    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);

    dwRetVal = BSP_cp3banAd9557SemGet(dwSubslotId);  /* Get semaphore. */
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        return dwRetVal;
    }

    dwRetVal = BSP_cp3ban_Ad9557_1_Check(dwSubslotId);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_Ad9557_1_Check() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        dwTmpRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetVal);
            return dwTmpRetVal;
        }
        return dwRetVal;
    }

    /* bit0~bit11为有效寄存器地址空间. */
    wRegAddr &= ((WORD16)0x0fff);

    /* write offset address */
    dwRetVal = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_AD9557_1_ADDR_REG, wRegAddr);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        dwTmpRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetVal);
            return dwTmpRetVal;
        }
        return dwRetVal;
    }

    /* write data */
    dwRetVal = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_AD9557_1_DATA_REG, wData);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        dwTmpRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetVal);
            return dwTmpRetVal;
        }
        return dwRetVal;
    }

    /* write the writing command */
    /* bit0表示读写命令标志,0表示写操作,1表示读操作. */
    dwRetVal = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_AD9557_1_CTRL_REG, &tmpRegValue);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        dwTmpRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetVal);
            return dwTmpRetVal;
        }
        return dwRetVal;
    }
    tmpRegValue = (tmpRegValue | ((WORD16)0x2)) & ((WORD16)0xFFFE);
    dwRetVal = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_AD9557_1_CTRL_REG, tmpRegValue);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        dwTmpRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetVal);
            return dwTmpRetVal;
        }
        return dwRetVal;
    }

    BSP_DelayUs(5); /* delay 5us. */
    
    dwRetVal = BSP_cp3ban_Ad9557_1_Check(dwSubslotId);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_Ad9557_1_Check() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        dwTmpRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetVal);
            return dwTmpRetVal;
        }
        return dwRetVal;
    }
    
    dwRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        return dwRetVal;
    }
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3ban_ad9557_2_write
* 功能描述: 写2号AD9557芯片的寄存器
* 输入参数: dwSubslotId         --CP3BAN单板的子槽位号
*           wRegAddr            --子卡AD9557的寄存器地址
*           wData               --寄存器值
* 输出参数: 无
* 返 回 值: BSP_E_CP3BAN_OK     --写成功
*         : 其他                --写失败
* 其它说明：
* 修改日期     版本号                     修改人                 修该内容
* -----------------------------------------------------------------------------
* 2013/03/26    V1.0                      gaoyun                 create
******************************************************************************/ 
WORD32 BSP_cp3ban_ad9557_2_write(WORD32 dwSubslotId, 
                                            WORD16 wRegAddr, 
                                            WORD16 wData)
{
    WORD32 dwRetVal = BSP_E_CP3BAN_OK;
    WORD32 dwTmpRetVal = BSP_E_CP3BAN_OK;
    WORD16 tmpRegValue = 0;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);

    dwRetVal = BSP_cp3banAd9557SemGet(dwSubslotId);  /* Get semaphore. */
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        return dwRetVal;
    }

    dwRetVal = BSP_cp3ban_Ad9557_2_Check(dwSubslotId);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_Ad9557_2_Check() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        dwTmpRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetVal);
            return dwTmpRetVal;
        }
        return dwRetVal;
    }
    
    /* bit0~bit11为有效寄存器地址空间. */
    wRegAddr &= ((WORD16)0x0fff);
    
    /* write offset address */
    dwRetVal = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_AD9557_2_ADDR_REG, wRegAddr);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        dwTmpRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetVal);
            return dwTmpRetVal;
        }
        return dwRetVal;
    }
    
    /* write data */
    dwRetVal = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_AD9557_2_DATA_REG, wData);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        dwTmpRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetVal);
            return dwTmpRetVal;
        }
        return dwRetVal;
    }
    
    /* write the writing command */
    /* bit0表示读写命令标志,0表示写操作,1表示读操作. */
    dwRetVal = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_AD9557_2_CTRL_REG, &tmpRegValue);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        dwTmpRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetVal);
            return dwTmpRetVal;
        }
        return dwRetVal;
    }
    tmpRegValue = (tmpRegValue | ((WORD16)0x2)) & ((WORD16)0xFFFE);
    dwRetVal = BSP_cp3banEpld16Write(dwSubslotId, BSP_CP3BAN_AD9557_2_CTRL_REG, tmpRegValue);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        dwTmpRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetVal);
            return dwTmpRetVal;
        }
        return dwRetVal;
    }
    
    BSP_DelayUs(5); /* delay 5us. */
    
    dwRetVal = BSP_cp3ban_Ad9557_2_Check(dwSubslotId);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_Ad9557_2_Check() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        dwTmpRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwTmpRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwTmpRetVal);
            return dwTmpRetVal;
        }
        return dwRetVal;
    }

    dwRetVal = BSP_cp3banAd9557SemFree(dwSubslotId);  /* Free semaphore. */
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557SemFree() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        return dwRetVal;
    }
    return BSP_E_CP3BAN_OK;
}


/******************************************************************************
* 函数名称: BSP_cp3ban_ad9557_1_init
* 功能描述: 初始化CP3BAN单板的1号AD9557时钟芯片.
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数:
* 返 回 值: BSP_E_CP3BAN_OK   ----成功
* 其它说明
* 修改日期     版本号                     修改人                 修该内容
* -----------------------------------------------------------------------------
* 2013/06/25    V1.0                xieweisheng10112265          create
******************************************************************************/ 
WORD32 BSP_cp3ban_ad9557_1_init(WORD32 dwSubslotId)
{
    WORD32 dwRetVal = BSP_E_CP3BAN_OK;
    WORD32 dwLoopTime = 0;
    WORD32 dwTotalTimes = 0;
    WORD16 wRegData = 0;
    WORD16 wRegAddr = 0;
    WORD16 wRegValue = 0;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    
    dwTotalTimes = sizeof(g_bsp_cp3ban_ad9557_1_reg) / sizeof(T_BSP_CP3BAN_AD9557_CFG);
    for (dwLoopTime = 0; dwLoopTime < dwTotalTimes; dwLoopTime++)
    {
        if (0 != g_bsp_cp3ban_ad9557_1_reg[dwLoopTime].ucFlag)
        {
            wRegAddr = g_bsp_cp3ban_ad9557_1_reg[dwLoopTime].wAddr;
            wRegValue = (WORD16)(g_bsp_cp3ban_ad9557_1_reg[dwLoopTime].ucValue);
            dwRetVal = BSP_cp3ban_ad9557_1_write(dwSubslotId, wRegAddr, wRegValue);
            if (BSP_E_CP3BAN_OK != dwRetVal)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_ad9557_1_write() rv=0x%x, wRegAddr=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal, wRegAddr);
                continue;
            }          
        }
    }
    
    BSP_DelayMs(1000);  /* delay 1s */
    for (dwLoopTime = 0; dwLoopTime < BSP_CP3BAN_AD9557_LOCK_QUERY_TIMES; dwLoopTime++)
    {
        wRegData = 0;
        dwRetVal = BSP_cp3ban_ad9557_1_read(dwSubslotId, ((WORD16)0x0d01), &wRegData);
        if (BSP_E_CP3BAN_OK != dwRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_ad9557_1_read() rv=0x%x, wRegAddr=0x0d01.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
            continue;
        }
        /* 表示时钟已经锁定 */
        if (((WORD16)0x0077) == (wRegData & ((WORD16)0x0077)))
        {
            break;            
        }
        BSP_DelayMs(1);  /* delay 1ms */
    }
    if (dwLoopTime >= BSP_CP3BAN_AD9557_LOCK_QUERY_TIMES)
    {
        BSP_Print(BSP_DEBUG_ALL, "WARNING: %s line %d, subslotId %u, AD9557_1 is not locked, register offset address 0x0d01, value 0x%x.\n", __FILE__, __LINE__, dwSubslotId, wRegData);
    }
        
    return BSP_E_CP3BAN_OK;  
}


/******************************************************************************
* 函数名称: BSP_cp3ban_ad9557_2_init
* 功能描述: 初始化CP3BAN单板的2号AD9557时钟芯片.
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数:
* 返 回 值: BSP_E_CP3BAN_OK   ----成功
* 其它说明
* 修改日期     版本号                     修改人                 修该内容
* -----------------------------------------------------------------------------
* 2013/06/25    V1.0                xieweisheng10112265          create
******************************************************************************/ 
WORD32 BSP_cp3ban_ad9557_2_init(WORD32 dwSubslotId)
{
    WORD32 dwRetVal = BSP_E_CP3BAN_OK;
    WORD32 dwLoopTime = 0;
    WORD32 dwTotalTimes = 0;
    WORD16 wRegData = 0;
    WORD16 wRegAddr = 0;
    WORD16 wRegValue = 0;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    
    dwTotalTimes = sizeof(g_bsp_cp3ban_ad9557_2_reg) / sizeof(T_BSP_CP3BAN_AD9557_CFG);
    for (dwLoopTime = 0; dwLoopTime < dwTotalTimes; dwLoopTime++)
    {
        if (0 != g_bsp_cp3ban_ad9557_2_reg[dwLoopTime].ucFlag)
        {
            wRegAddr = g_bsp_cp3ban_ad9557_2_reg[dwLoopTime].wAddr;
            wRegValue = (WORD16)(g_bsp_cp3ban_ad9557_2_reg[dwLoopTime].ucValue);
            dwRetVal = BSP_cp3ban_ad9557_2_write(dwSubslotId, wRegAddr, wRegValue);
            if (BSP_E_CP3BAN_OK != dwRetVal)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_ad9557_2_write() rv=0x%x, wRegAddr=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal, wRegAddr);
                continue;
            }          
        }
    }
    
    BSP_DelayMs(1000);  /* delay 1s */
    for (dwLoopTime = 0; dwLoopTime < BSP_CP3BAN_AD9557_LOCK_QUERY_TIMES; dwLoopTime++)
    {
        wRegData = 0;
        dwRetVal = BSP_cp3ban_ad9557_2_read(dwSubslotId, ((WORD16)0x0d01), &wRegData);
        if (BSP_E_CP3BAN_OK != dwRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_ad9557_2_read() rv=0x%x, wRegAddr=0x0d01.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
            continue;
        }
        /* 表示时钟已经锁定 */
        if (((WORD16)0x0077) == (wRegData & ((WORD16)0x0077)))
        {
            break;            
        }
        BSP_DelayMs(1);  /* delay 1ms */
    }
    if (dwLoopTime >= BSP_CP3BAN_AD9557_LOCK_QUERY_TIMES)
    {
        BSP_Print(BSP_DEBUG_ALL, "WARNING: %s line %d, subslotId %u, AD9557_2 is not locked, register offset address 0x0d01, value 0x%x.\n", __FILE__, __LINE__, dwSubslotId, wRegData);
    }
    
    return BSP_E_CP3BAN_OK;  
}


/******************************************************************************
* 函数名称: BSP_cp3banAd9557Initialize
* 功能描述: 初始化CP3BAN单板的AD9557时钟芯片
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数:
* 返 回 值: BSP_E_CP3BAN_OK   ----成功
* 其它说明: 包括初始化CP3BAN单板的1号AD9557芯片和2号AD9557芯片.
* 修改日期     版本号                     修改人                 修该内容
* -----------------------------------------------------------------------------
* 2013/06/25    V1.0                xieweisheng10112265          create
******************************************************************************/  
WORD32 BSP_cp3banAd9557Initialize(WORD32 dwSubslotId)
{
    WORD32 dwRetVal = BSP_E_CP3BAN_OK;
    WORD16 wPcbId = 0;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);

    dwRetVal = BSP_cp3banPcbIdGet(dwSubslotId, &wPcbId);  /* 获取CP3BAN单板的pcb ID. */
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banPcbIdGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        return dwRetVal;
    }
    
    dwRetVal = BSP_cp3ban_ad9557_1_init(dwSubslotId);  /* 初始化1号AD9557芯片 */
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_ad9557_1_init() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
        return dwRetVal;
    }
    BSP_Print(BSP_DEBUG_ALL, "%s line %d, subslotId %u, Initialize AD9557_1 successfully.\n", __FILE__, __LINE__, dwSubslotId);

    if (0 != wPcbId)  /* pcbId为1的CP3BAN单板有2块AD9557时钟芯片 */
    {
        dwRetVal = BSP_cp3ban_ad9557_2_init(dwSubslotId);  /* 初始化2号AD9557芯片 */
        if (BSP_E_CP3BAN_OK != dwRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_ad9557_2_init() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
            return dwRetVal;
        }
        BSP_Print(BSP_DEBUG_ALL, "%s line %d, subslotId %u, Initialize AD9557_2 successfully.\n", __FILE__, __LINE__, dwSubslotId);
    }
    
    return BSP_E_CP3BAN_OK;  
}


/**************************************************************************
* 函数名称: BSP_cp3banAd9557LockStatusGet
* 功能描述: 获取AD9557芯片的时钟锁定状态
* 输入参数: dwSubslotId      --CP3BAN单板的子槽位号
* 输出参数: *pLockState      --时钟锁定状态
* 返 回 值: BSP_E_CP3BAN_OK  ---成功
* 其它说明
* 修改日期     版本号                     修改人                 修该内容
* -----------------------------------------------------------------------------
* 2013/03/26    V1.0                      gaoyun                 create
******************************************************************************/   
WORD32 BSP_cp3banAd9557LockStatusGet(WORD32 dwSubslotId, WORD32 *pLockState)
{
    WORD32 dwRetVal = BSP_E_CP3BAN_OK;
    WORD16 wRegData = 0;   
    WORD32 dwTry = 0;

    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pLockState);

    /* 连续读10次,10次都是锁定状态,AD9557才锁住 */
    for (dwTry = 0; dwTry < BSP_CP3BAN_AD9557_LOCK_QUERY_TIMES; dwTry++)
    {
        wRegData = 0;
        dwRetVal = BSP_cp3ban_ad9557_1_read(dwSubslotId, BSP_CP3BAN_AD9557_LOCK_STATUS_REG, &wRegData);
        if (BSP_E_CP3BAN_OK != dwRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_ad9557_1_read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetVal);
            return dwRetVal;
        }
        if (BSP_CP3BAN_AD9557_ALL_STATUS_LOCK != (wRegData & BSP_CP3BAN_AD9557_ALL_STATUS_LOCK))
        {
            break;
        }        
    }
    if (dwTry >= BSP_CP3BAN_AD9557_LOCK_QUERY_TIMES)
    {
        *pLockState = BSP_CP3BAN_AD9557_LOCKED;
    }
    else 
    {
        *pLockState = BSP_CP3BAN_AD9557_UNLOCK;
    }
 
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banBoardResetRemove
* 功能描述: 解除CP3BAN单板的芯片复位.
* 访问的表: 无.
* 修改的表: 无.
* 输入参数: subslotId: CP3BAN单板的子槽位号.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-06-25   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banBoardResetRemove(WORD32 subslotId)
{
    WORD32 rv = BSP_E_CP3BAN_OK; 
    WORD16 regValue = 0;

    BSP_CHECK_CP3BAN_SUBSLOT_ID(subslotId);
        
    rv = BSP_cp3banEpld16Read(subslotId, BSP_CP3BAN_BOARD_RESET_REG, &regValue);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_ERROR_PRINT("ERROR: %s line %d, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    /* 0x0104寄存器的bit0表示FPGA芯片的复位标志,1复位,0解除复位 */
    if (0 != (regValue & BSP_CP3BAN_FPGA_RESET_FLAG))
    {
        regValue = regValue & ((WORD16)0xFFFE);
        rv = BSP_cp3banEpld16Write(subslotId, BSP_CP3BAN_BOARD_RESET_REG, regValue);
        if (BSP_E_CP3BAN_OK != rv)
        {
            BSP_ERROR_PRINT("ERROR: %s line %d, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    
    regValue = 0;
    rv = BSP_cp3banEpld16Read(subslotId, BSP_CP3BAN_BOARD_RESET_REG, &regValue);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_ERROR_PRINT("ERROR: %s line %d, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, rv);        
        return rv;
    }
    /* 0x0104寄存器的bit8表示PCI桥8112芯片的复位标志,1复位,0解除复位 */
    if (0 != (regValue & BSP_CP3BAN_PCI_RESET_FLAG))
    {
        regValue = regValue & ((WORD16)0xFEFF);
        rv = BSP_cp3banEpld16Write(subslotId, BSP_CP3BAN_BOARD_RESET_REG, regValue);
        if (BSP_E_CP3BAN_OK != rv)
        {
            BSP_ERROR_PRINT("ERROR: %s line %d, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    
    regValue = 0;
    rv = BSP_cp3banEpld16Read(subslotId, BSP_CP3BAN_BOARD_RESET_REG, &regValue);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_ERROR_PRINT("ERROR: %s line %d, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    /* 0x0104寄存器的bit9表示AD9557芯片的复位标志,1复位,0解除复位 */
    if (0 != (regValue & BSP_CP3BAN_AD9557_RESET_FLAG))
    {
        regValue = regValue & ((WORD16)0xFDFF);
        rv = BSP_cp3banEpld16Write(subslotId, BSP_CP3BAN_BOARD_RESET_REG, regValue);
        if (BSP_E_CP3BAN_OK != rv)
        {
            BSP_ERROR_PRINT("ERROR: %s line %d, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }

    regValue = 0;
    rv = BSP_cp3banEpld16Read(subslotId, BSP_CP3BAN_BOARD_RESET_REG, &regValue);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_ERROR_PRINT("ERROR: %s line %d, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, rv);
        return rv;
    }
    /* 0x0104寄存器的bit10表示MAX3997芯片的复位标志,1复位,0解除复位 */
    if (0 != (regValue & BSP_CP3BAN_MAX3997_RESET_FLAG))
    {
        regValue = regValue & ((WORD16)0xFBFF);
        rv = BSP_cp3banEpld16Write(subslotId, BSP_CP3BAN_BOARD_RESET_REG, regValue);
        if (BSP_E_CP3BAN_OK != rv)
        {
            BSP_ERROR_PRINT("ERROR: %s line %d, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, rv);
            return rv;
        }
    }
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banMax3997SemFlagGet
* 功能描述: 获取信号量的创建标记.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: *pdwFlag: 保存信号量的创建标记.
* 返 回 值: 
* 其它说明: 该信号量用来保护访问MAX3997. 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-07-15   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banMax3997SemFlagGet(WORD32 dwSubslotId, WORD32 *pdwFlag)
{
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pdwFlag);
    
    *pdwFlag = 0;  /* 先清零再赋值 */
    *pdwFlag = g_bsp_cp3ban_max3997_sem_flag[dwSubslotId];
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banMax3997SemIdGet
* 功能描述: 获取信号量ID.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: *pdwSemId: 保存信号量ID.
* 返 回 值: 
* 其它说明: 该信号量用来保护访问MAX3997. 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-07-15   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banMax3997SemIdGet(WORD32 dwSubslotId, WORD32 *pdwSemId)
{
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pdwSemId);
    
    *pdwSemId = 0;  /* 先清零再赋值 */
    *pdwSemId = g_bsp_cp3ban_max3997_sem_id[dwSubslotId];
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banMax3997SemCreate
* 功能描述: 创建信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护访问MAX3997.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-07-15   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banMax3997SemCreate(WORD32 dwSubslotId)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    
    if (BSP_CP3BAN_SEM_NONE_EXISTED == g_bsp_cp3ban_max3997_sem_flag[dwSubslotId])
    {
        dwRetValue = BSP_OalCreateSem(1, MUTEX_STYLE, &(g_bsp_cp3ban_max3997_sem_id[dwSubslotId]));
        if (BSP_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_OalCreateSem() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
            return BSP_E_CP3BAN_CREATE_SEM_FAIL;
        } 
        g_bsp_cp3ban_max3997_sem_flag[dwSubslotId] = BSP_CP3BAN_SEM_EXISTED;
    }  
    
    return BSP_E_CP3BAN_OK;  /* 返回成功 */
}


/**************************************************************************
* 函数名称: BSP_cp3banMax3997SemDestroy
* 功能描述: 销毁信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护访问MAX3997.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-03-04   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banMax3997SemDestroy(WORD32 dwSubslotId)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    
    /* 在销毁信号量之前,先必须LOCK一下信号量 */
    dwRetValue = BSP_cp3banMax3997SemGet(dwSubslotId);
    if (BSP_E_CP3BAN_SEM_NONE_EXISTED == dwRetValue)
    {
        BSP_Print(BSP_DEBUG_ALL, "WARNING: %s line %d, g_bsp_cp3ban_max3997_sem_id[%u] has been destroyed.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_OK;
    }
    else if (BSP_E_CP3BAN_OK == dwRetValue)
    {
        ;
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banMax3997SemGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return dwRetValue;
    }
    
    if (BSP_CP3BAN_SEM_NONE_EXISTED != g_bsp_cp3ban_max3997_sem_flag[dwSubslotId])    
    {
        dwRetValue = BSP_OalDestroySem(g_bsp_cp3ban_max3997_sem_id[dwSubslotId]);
        if (BSP_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_OalDestroySem() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
            return BSP_E_CP3BAN_DESTROY_SEM_FAIL;
        }
        g_bsp_cp3ban_max3997_sem_id[dwSubslotId] = 0;
        g_bsp_cp3ban_max3997_sem_flag[dwSubslotId] = BSP_CP3BAN_SEM_NONE_EXISTED;
    }
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banMax3997SemGet
* 功能描述: 获取信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护访问MAX3997.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-07-15   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banMax3997SemGet(WORD32 dwSubslotId)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    
    if (BSP_CP3BAN_SEM_NONE_EXISTED != g_bsp_cp3ban_max3997_sem_flag[dwSubslotId])    
    {
        dwRetValue = BSP_OalSemP(g_bsp_cp3ban_max3997_sem_id[dwSubslotId], WAIT_FOREVER);
        if (BSP_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_OalSemP() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
            return BSP_E_CP3BAN_GET_SEM_FAIL;
        }
    }
    else
    {
        return BSP_E_CP3BAN_SEM_NONE_EXISTED;
    }
    
    return BSP_E_CP3BAN_OK;  /* 返回成功 */
}


/**************************************************************************
* 函数名称: BSP_cp3banMax3997SemFree
* 功能描述: 释放信号量(互斥信号量).
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号,取值为1~4.
* 输出参数: 无.
* 返 回 值: 
* 其它说明: 该信号量用来保护访问MAX3997.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2014-07-15   V1.0  谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banMax3997SemFree(WORD32 dwSubslotId)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    
    if (BSP_CP3BAN_SEM_NONE_EXISTED != g_bsp_cp3ban_max3997_sem_flag[dwSubslotId])    
    {
        dwRetValue = BSP_OalSemV(g_bsp_cp3ban_max3997_sem_id[dwSubslotId]);
        if (BSP_OK != dwRetValue)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_OalSemV() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
            return BSP_E_CP3BAN_FREE_SEM_FAIL;
        }
    }
    else
    {
        return BSP_E_CP3BAN_SEM_NONE_EXISTED;
    }
    
    return BSP_E_CP3BAN_OK;  /* 返回成功 */
}


/******************************************************************************
* 函数名称: BSP_cp3banMax3997Read
* 功能描述: 读取MAX3997的寄存器的值
* 输入参数: 
*         : wRegOffset        --寄存器的偏移地址
*         : *pwRegVal         --寄存器值
* 输出参数: 
* 返 回 值: BSP_E_CP3BAN_OK    --成功
*           其他:              --失败
* 其它说明
*
* 修改日期     版本号           修改人            修改内容
* -----------------------------------------------------------------------------
* 2013/03/26    V1.0        xieweisheng10112265     modify
******************************************************************************/
WORD32 BSP_cp3banMax3997Read(struct Max39xxDevice *ptMax39xxDev, 
                                         WORD16 wRegOffset, 
                                         WORD16 *pwRegVal)
{
    WORD32 systemType = 0;  /* ZXUPN15000的系统类型 */
    WORD16 offsetAddr = 0;
    WORD16 wRegValue        = 0;
    VOID *pvMax39xxBaseAddr = NULL;
    BYTE *pucAddr           = NULL;

    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptMax39xxDev);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pwRegVal);

    pvMax39xxBaseAddr = ptMax39xxDev->pvMax39xxBaseAddr;
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pvMax39xxBaseAddr);
    
    pvMax39xxBaseAddr = pvMax39xxBaseAddr - BSP_CP3BAN_MAX3997_BASE_REG;
    wRegOffset = wRegOffset + BSP_CP3BAN_MAX3997_BASE_REG;
    systemType = BSP_cp3banSystemTypeGet(); /* 获取ZXUPN15000系统类型. */
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM == systemType)   /* 非15K-2系统 */
    {
        offsetAddr = wRegOffset << 1;
    }
    else   /* 15K-2系统 */
    {
        offsetAddr = wRegOffset;
    }

    pucAddr = (BYTE *)pvMax39xxBaseAddr + offsetAddr;
    BSP_REG_READ_WORD16(pucAddr, wRegValue);
    *pwRegVal = wRegValue;
    
    return BSP_E_CP3BAN_OK;
}


/******************************************************************************
* 函数名称: BSP_cp3banMax3997Write
* 功能描述: 写MAX3997的寄存器的值
* 输入参数: 
*         : wRegOffset     --寄存器地址
*         : wRegVal        --寄存器值
* 输出参数: 
* 返 回 值: BSP_E_CP3BAN_OK    --成功
*           其他:              --失败
* 其它说明
*
* 修改日期     版本号           修改人            修改内容
* -----------------------------------------------------------------------------
* 2013/03/26    V1.0       xieweisheng10112265      modify
******************************************************************************/
WORD32 BSP_cp3banMax3997Write(struct Max39xxDevice *ptMax39xxDev, 
                                         WORD16 wRegOffset, 
                                         WORD16 wRegVal)
{
    WORD32 systemType = 0;  /* ZXUPN15000的系统类型 */
    WORD16 offsetAddr = 0;
    VOID *pvMax39xxBaseAddr = NULL;
    BYTE *pucAddr           = NULL;

    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptMax39xxDev);
    
    pvMax39xxBaseAddr = ptMax39xxDev->pvMax39xxBaseAddr;
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pvMax39xxBaseAddr);

    pvMax39xxBaseAddr = pvMax39xxBaseAddr - BSP_CP3BAN_MAX3997_BASE_REG;
    wRegOffset = wRegOffset + BSP_CP3BAN_MAX3997_BASE_REG;
    systemType = BSP_cp3banSystemTypeGet(); /* 获取ZXUPN15000系统类型. */
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM == systemType)   /* 非15K-2系统 */
    {
        offsetAddr = wRegOffset << 1;
    }
    else    /* 15K-2系统 */
    {
        offsetAddr = wRegOffset;
    }

    pucAddr = (BYTE *)pvMax39xxBaseAddr + offsetAddr;
    
    BSP_REG_WRITE_WORD16(pucAddr, wRegVal);
    
    return BSP_E_CP3BAN_OK;
}


/******************************************************************************
* 函数名称: BSP_cp3banMax3997Configure
* 功能描述: 配置MAX3997的资源
* 输入参数: *ptDev        
* 输出参数: 无
* 返 回 值: BSP_E_CP3BAN_OK       --操作成功
*         : 其他                  --操作失败 
* 其它说明: 
* 修改日期     版本号                     修改人                 修该内容
* -----------------------------------------------------------------------------
* 2013/03/26    V1.0                 xieweisheng10112265          modify
******************************************************************************/  
WORD32 BSP_cp3banMax3997Configure(struct Device *ptDev)
{
    struct Max39xxDevice *ptMax39xxDev  = NULL; 
    T_CP3BAN_PRIVATE   *ptPriv          = NULL;
    VOID *pvMax39xxBaseAddr             = NULL;
    
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptDev);
    ptPriv = (T_CP3BAN_PRIVATE *)(ptDev->priv);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptPriv);
    pvMax39xxBaseAddr = ptPriv->pvBaseAddr + BSP_CP3BAN_MAX3997_BASE_REG;
    ptMax39xxDev = BSP_Max3997DevCreat(pvMax39xxBaseAddr);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptMax39xxDev);
    
    ptPriv->ptMax3997Drv       = ptMax39xxDev;
    
    /* 如果需要子卡实现max芯片的读写则需要在此初始化 */
    ptMax39xxDev->EpldRead16  = BSP_cp3banMax3997Read;
    ptMax39xxDev->EpldWrite16 = BSP_cp3banMax3997Write;
    ptMax39xxDev->dwMax3997DevAddr = BSP_CP3BAN_MAX3997_DEVICE_ADDR;
    
    return BSP_E_CP3BAN_OK;
}


/*******************************************************************************
* 函数名称: BSP_cp3banMax3997Initialize 
* 功能描述: 初始化MAX3997芯片
* 输入参数: subslotId                --CP3BAN单板的子槽位号
*           *ptCfgVal                --MAX3997芯片的寄存器配置信息
*           dwChipNum                --CP3BAN单板上的MAX3997芯片的数量
* 输出参数: 无
* 返 回 值: BSP_E_CP3BAN_OK          --成功
*         : 其他                     --失败           
* 其它说明：
* 修改日期     版本号                     修改人                 修该内容
* -----------------------------------------------------------------------------
* 2013/03/26    V1.0                  xieweisheng10112265          create
******************************************************************************/ 
WORD32 BSP_cp3banMax3997Initialize(WORD32 subslotId, T_BSP_MAX3997CFG *ptCfgVal, WORD32 dwChipNum)
{
    WORD32 dwRetVal = BSP_E_CP3BAN_OK;
    WORD32 dwChipLoop = 0;
    WORD32 dwChipId = 0;
    T_CP3BAN_PRIVATE  *ptPriv = NULL;
    struct Max39xxDevice *ptMaxDev = NULL;
    struct Device *ptDev = NULL;

    BSP_CHECK_CP3BAN_SUBSLOT_ID(subslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptCfgVal);
    
    dwRetVal = BSP_cp3ban_devicePtrGet(subslotId, &ptDev);
    if (BSP_E_CP3BAN_OK != dwRetVal)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_devicePtrGet() rv=0x%x.\n", __FILE__, __LINE__, subslotId, dwRetVal);
        return dwRetVal;
    }
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptDev);
    ptPriv = (T_CP3BAN_PRIVATE *)(ptDev->priv);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptPriv);
    
    ptMaxDev = ((T_CP3BAN_PRIVATE *)ptDev->priv)->ptMax3997Drv;
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptMaxDev);

    for (dwChipLoop = 0; dwChipLoop < dwChipNum; dwChipLoop++)
    {
        dwChipId = (WORD32)(ptCfgVal[dwChipLoop].wChipId);
        dwRetVal = BSP_cp3banMax3997SemGet(subslotId);  /* Get Semphore*/
        if (BSP_E_CP3BAN_OK != dwRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banMax3997SemGet() rv=0x%x.\n", __FILE__, __LINE__, subslotId, dwRetVal);
            return dwRetVal;
        }
        dwRetVal = ptMaxDev->Max39xxInit(ptMaxDev, (T_BSP_MAX_CFG_COMMON *)&ptCfgVal[dwChipLoop]);
        if (BSP_OK != dwRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, Max39xxInit() rv=0x%x.\n", __FILE__, __LINE__, subslotId, dwRetVal);
            dwRetVal = BSP_cp3banMax3997SemFree(subslotId);  /* Free semaphore. */
            if (BSP_E_CP3BAN_OK != dwRetVal)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banMax3997SemFree() rv=0x%x.\n", __FILE__, __LINE__, subslotId, dwRetVal);
                return dwRetVal;
            }
            return BSP_E_CP3BAN_ERROR;
        }

        dwRetVal = BSP_cp3banMax3997SemFree(subslotId);  /* Free semaphore. */
        if (BSP_E_CP3BAN_OK != dwRetVal)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banMax3997SemFree() rv=0x%x.\n", __FILE__, __LINE__, subslotId, dwRetVal);
            return dwRetVal;
        }
    }

    return BSP_E_CP3BAN_OK;    
}


/**************************************************************************
* 函数名称: BSP_cp3banBoardStatusGet
* 功能描述: 获取CP3BAN单板的状态.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号, 取值为1~4. 
* 输出参数: *pdwStatus: 保存CP3BAN单板的状态,具体参见BSP_CP3BAN_BOARD_STATUS定义. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-03   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banBoardStatusGet(WORD32 dwSubslotId, WORD32 *pdwStatus)
{
    WORD32 dwRetValue = BSP_E_CP3BAN_OK;
    T_BSP_BRDCTRL_PARA tIoctl;
    T_BSP_BRDCTRL_FUNCCARD_INFO tSubCardInfo;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pdwStatus);
    
    memset(&tIoctl, 0, sizeof(T_BSP_BRDCTRL_PARA));
    memset(&tSubCardInfo, 0, sizeof(T_BSP_BRDCTRL_FUNCCARD_INFO));

    tSubCardInfo.wSlotID = dwSubslotId;
    tIoctl.dwCmd   = BSP_IOCMD_BRDCTRL_FUNCCARD_INFO;
    tIoctl.pParam = (VOID*)(&tSubCardInfo);
    tIoctl.dwLen    = sizeof(T_BSP_BRDCTRL_FUNCCARD_INFO);
    dwRetValue = BSP_BrdCtrlIoCtrl(&tIoctl);
    if (BSP_OK != dwRetValue)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_COMMON_MSG, "ERROR: %s line %d, subslotId %u, BSP_BrdCtrlIoCtrl() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwRetValue);
        return dwRetValue;
    }
    if (BSP_BOARD_ONLINE == tSubCardInfo.wOnBoard) /* 单板在位 */
    {
        *pdwStatus = BSP_CP3BAN_BOARD_ONLINE;
    }
    else if (BSP_BOARD_OFFLINE == tSubCardInfo.wOnBoard)  /* 单板不在位 */
    {
        *pdwStatus = BSP_CP3BAN_BOARD_OFFLINE;
    }
    else if (BSP_BOARD_ONLINE_POWER_OFF == tSubCardInfo.wOnBoard)  /* 单板在位,下电 */
    {
        *pdwStatus = BSP_CP3BAN_BOARD_ONLINE_POWER_OFF;
    }
    else  /* 未知状态 */
    {
        *pdwStatus = BSP_CP3BAN_BOARD_UNKNOWN_STATUS;
    }
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banBoardInitialize
* 功能描述: 初始化CP3BAN单板.
* 访问的表: 无
* 修改的表: 无
* 输入参数: subslotId: CP3BAN单板的子槽位号,取值为1~4. 
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 以单板的子槽位号为单位来初始化CP3BAN单板.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-03   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banBoardInitialize(WORD32 subslotId)
{
    WORD32 rv = BSP_E_CP3BAN_OK;
    WORD32 dwTmpRv = BSP_E_CP3BAN_OK;
    WORD16 regValue = 0;  /* EPLD寄存器的值 */
    WORD32 initFlag = 0;  /* CP3BAN单板的初始化标记 */
    WORD32 loadFlag = 0;  /* FPGA加载标记 */
    WORD32 systemType = 0;   /* ZXUPN15000的系统类型 */
    WORD32 dwEthIntfBoardType = 0;  /* CP3BAN单板的Ethernet类型 */
    BYTE chipId = 0;
    time_t currentTime;    /* 以秒为单位 */
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(subslotId);
    chipId = (BYTE)(subslotId - 1);
    systemType = BSP_cp3banSystemTypeGet();  /* 获取ZXUPN15000系统类型. */
    memset(&currentTime, 0, sizeof(time_t));

    time(&currentTime); /* Get current calendar time in seconds */
    BSP_Print(BSP_DEBUG_ALL, "%s line %d, %s, subslotId %u, Begin to initialize CP3BAN board.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId);
    rv = BSP_cp3banBoardInitFlagGet(subslotId, &initFlag);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, BSP_cp3banBoardInitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), rv);
        return rv;
    }
    if (BSP_CP3BAN_BOARD_UNINSTALL != initFlag)  /* 如果CP3BAN单板已经初始化成功了,则直接返回成功. */
    {
        BSP_Print(BSP_DEBUG_ALL, "WARNING: %s line %d, %s, subslotId %u, CP3BAN board has been initialized.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId);
        return BSP_E_CP3BAN_OK;
    }

    rv = BSP_cp3banFpgaLoadFlagGet(subslotId, &loadFlag);  /* 获取FPGA加载标记 */
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, BSP_cp3banFpgaLoadFlagGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), rv);
        return rv;
    }
    if (BSP_CP3BAN_FPGA_LOAD_UNFINISHED == loadFlag) /* FPGA未加载 */
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, FPGA has not been loaded.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId);
        return BSP_E_CP3BAN_FPGA_NOT_LOAD;
    }

    rv = BSP_cp3banBoardResetRemove(subslotId);  /* 解除复位. */
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, BSP_cp3banBoardResetRemove() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, rv);
        return BSP_E_CP3BAN_REMOVE_RESET_FAIL;
    }
    BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, subslotId %u, Remove board reset successfully.\n", __FILE__, __LINE__, subslotId);
    BSP_DelayMs(500);  /* delay 500ms. */

    rv = BSP_cp3banAd9557Initialize(subslotId);  /* 初始化AD9557芯片 */
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, BSP_cp3banAd9557Initialize() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, rv);
        return rv;
    }

    rv = BSP_cp3banEpld16Read(subslotId, BSP_CP3BAN_MAX3997_ENABLE_REG, &regValue);  /* 使能MAX3997配置 */
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, rv);
        return rv;
    }
    if (0 == (regValue & ((WORD16)0x1)))
    {
        regValue = regValue | ((WORD16)0x1);
        rv = BSP_cp3banEpld16Write(subslotId, BSP_CP3BAN_MAX3997_ENABLE_REG, regValue);
        if (BSP_E_CP3BAN_OK != rv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, rv);
            return rv;
        }
    }
    BSP_DelayMs(500);  /* delay 500ms. */
    rv = BSP_cp3banEthIntfBoardTypeGet(subslotId, &dwEthIntfBoardType);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, BSP_cp3banEthIntfBoardTypeGet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, rv);
        return rv;
    }
    if (BSP_CP3BAN_1G_ETH_INTF_BOARD == dwEthIntfBoardType)
    {
        rv = BSP_cp3banMax3997Initialize(subslotId, g_bsp_cp3ban_1g_eth_max3997Cfg, BSP_CP3BAN_MAX3997_NUM);
    }
    else if (BSP_CP3BAN_10G_ETH_INTF_BOARD == dwEthIntfBoardType)
    {
        rv = BSP_cp3banMax3997Initialize(subslotId, g_bsp_cp3ban_10g_eth_max3997Cfg, BSP_CP3BAN_MAX3997_NUM);
    }
    else
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, invalid ethernet interface board type, ethIntfBoardType=%u.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, dwEthIntfBoardType);
        return BSP_E_CP3BAN_INVALID_ETH_INTF_BOARD_TYPE;
    }
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, BSP_cp3banMax3997Initialize() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, rv);
        return rv;
    }
    BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, subslotId %u, Initialize MAX3997 successfully.\n", __FILE__, __LINE__, subslotId);
    
    rv = drv_tdm_pwe3Initialize((BYTE)subslotId);  /* 初始化STM TDM PWE3模块 */
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, drv_tdm_pwe3Initialize() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, rv);
        dwTmpRv = drv_tdm_memoryFree(chipId);  /* Free memory */
        if (DRV_TDM_OK != dwTmpRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, chipId %u, drv_tdm_memoryFree() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), chipId, dwTmpRv);
            return dwTmpRv;
        }
        dwTmpRv = BSP_cp3banSfpMemFree(subslotId); /* 释放已经动态分配的内存 */
        if (BSP_E_CP3BAN_OK != dwTmpRv)
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, BSP_cp3banSfpMemFree() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, dwTmpRv);
            return dwTmpRv;
        }
        return rv;
    }
    BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, subslotId %u, Initialize STM TDM PWE3 successfully.\n", __FILE__, __LINE__, subslotId);

    rv = BSP_cp3banQueryFlagSet(subslotId, BSP_CP3BAN_FLAG_ENABLE);  /* 使能CP3BAN模块的查询标记 */
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, BSP_cp3banQueryFlagSet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, rv);
        return rv;
    }

    if (BSP_CP3BAN_NONE_15K_2_SYSTEM == systemType)  /* 非15K-2系统 */
    {
        rv = BSP_IfCardIntEnbl(subslotId);  /* 使能线卡级别的接口卡的中断 */
        if (BSP_OK != rv)            
        {
            BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, BSP_IfCardIntEnbl() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, rv);
            dwTmpRv = drv_tdm_memoryFree(chipId);  /* Free memory */
            if (DRV_TDM_OK != dwTmpRv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, chipId %u, drv_tdm_memoryFree() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), chipId, dwTmpRv);
                return dwTmpRv;
            }
            dwTmpRv = BSP_cp3banSfpMemFree(subslotId); /* 释放已经动态分配的内存 */
            if (BSP_E_CP3BAN_OK != dwTmpRv)
            {
                BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, BSP_cp3banSfpMemFree() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, dwTmpRv);
                return dwTmpRv;
            }
            return rv;
        }
    }
    
    rv = BSP_cp3banBoardInitFlagSet(subslotId, BSP_CP3BAN_BOARD_INSTALL);  /* CP3BAN单板初始化成功 */
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, %s, subslotId %u, BSP_cp3banBoardInitFlagSet() rv=0x%x.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId, rv);
        return rv;
    }
    time(&currentTime); /* Get current calendar time in seconds */
    BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, %s, subslotId %u, Initialize CP3BAN board successfully.\n", __FILE__, __LINE__, ctime(&currentTime), subslotId);
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banBoardUnInitialize
* 功能描述: 去初始化CP3BAN单板.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: CP3BAN单板的子槽位号, 取值为1~4. 
* 输出参数:  
* 返 回 值: 
* 其它说明: 以单板的子槽位号为单位来去初始化CP3BAN单板.
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-07-03   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banBoardUnInitialize(WORD32 dwSubslotId)
{
    WORD32 rv = BSP_E_CP3BAN_OK;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);

    rv = BSP_cp3banQueryFlagSet(dwSubslotId, BSP_CP3BAN_FLAG_DISABLE);  /* 非使能CP3BAN模块的查询标记 */
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banQueryFlagSet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    rv = drv_tdm_pwe3UnInitialize((BYTE)dwSubslotId);
    if (DRV_TDM_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, drv_tdm_pwe3UnInitialize() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    BSP_Print(BSP_DEBUG_ALL, "SUCCESS: %s line %d, subslotId %u, Uninitialize STM TDM PWE3 successfully.\n", __FILE__, __LINE__, dwSubslotId);
    rv = BSP_cp3banFpgaLoadFlagSet(dwSubslotId, BSP_CP3BAN_FPGA_LOAD_UNFINISHED);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banFpgaLoadFlagSet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    rv = BSP_cp3banBoardInitFlagSet(dwSubslotId, BSP_CP3BAN_BOARD_UNINSTALL); /* CP3BAN单板去初始化成功 */
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u, BSP_cp3banBoardInitFlagSet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    
    memset(&(g_bsp_cp3ban_sfp_debug[dwSubslotId][0]), 0, ((CP3BAN_PORT_MAX + 1) * sizeof(WORD32)));
    
    return BSP_E_CP3BAN_OK;
}


/****************************************************************************
* 函数名称: BSP_cp3banSfpRegRead
* 功能描述: 读取CP3BAN单板的SFP寄存器
* 输入参数: dwSubslotId         --CP3BAN单板的子槽位号
*           portId              --CP3BAN单板的STM端口的编号,取值为1~8.
*           ucI2CDeviceAddr     --I2C设备地址.
*           ucRegAddr     --I2C内部寄存器的地址.
* 输出参数: *pI2cData     --保存I2C寄存器的值
* 返 回 值: BSP_E_CP3BAN_OK     --读成功
*         : 其他                --读失败          
* 其它说明
* 修改日期     版本号                 修改人              修改内容
* -----------------------------------------------------------------------------
* 2013/07/05   V1.0            xieweisheng10112265         create
******************************************************************************/
WORD32 BSP_cp3banSfpRegRead(WORD32 dwSubslotId, 
                                       WORD16 portId, 
                                       BYTE ucI2CDeviceAddr, 
                                       BYTE ucI2CRegAddr, 
                                       BYTE *pI2cData)
{
    WORD32 rv = BSP_E_CP3BAN_OK;
    WORD16 regValue = 0;     /* 写入CPLD寄存器的值 */
    WORD16 tmpRegValue = 0;  /* 读取CPLD寄存器的值 */
    WORD16 offsetAddr = 0;   /* CPLD寄存器的偏移地址 */
    WORD32 dwLoopTimes = 0;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pI2cData);
    rv = BSP_cp3banPortIdCheck(dwSubslotId, (WORD32)portId);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banPortIdCheck() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    
    /* 写端口选择寄存器 */
    if (portId <= (BSP_CP3BAN_MAX_STM1_PORT_NUM / ((WORD16)2)))  /* port 1#~4#*/
    {
        offsetAddr = BSP_CP3BAN_PORT1TO4_SELECTED_REG;
        rv = BSP_cp3banEpld16Read(dwSubslotId, offsetAddr, &tmpRegValue);
        if (BSP_E_CP3BAN_OK != rv)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
            return rv;
        }
        regValue = (tmpRegValue & ((WORD16)0xFF00)) | (portId - ((WORD16)1));
        rv = BSP_cp3banEpld16Write(dwSubslotId, offsetAddr, regValue);
        if (BSP_E_CP3BAN_OK != rv)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
            return rv;
        }
    }
    else   /* port 5#~8#*/
    {
        offsetAddr = BSP_CP3BAN_PORT5TO8_SELECTED_REG;
        rv = BSP_cp3banEpld16Read(dwSubslotId, offsetAddr, &tmpRegValue);
        if (BSP_E_CP3BAN_OK != rv)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
            return rv;
        }
        regValue = (tmpRegValue & ((WORD16)0xFF00)) | (portId - ((WORD16)5));
        rv = BSP_cp3banEpld16Write(dwSubslotId, offsetAddr, regValue);
        if (BSP_E_CP3BAN_OK != rv)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
            return rv;
        }
    }
    
    /* 写I2C设备 */
    tmpRegValue = 0;
    regValue = 0;
    if (portId <= (BSP_CP3BAN_MAX_STM1_PORT_NUM / ((WORD16)2)))  /* port 1#~4#*/
    {
        offsetAddr = BSP_CP3BAN_PORT1TO4_I2C_REG;   
    }
    else  /* port 5#~8#*/
    {
        offsetAddr = BSP_CP3BAN_PORT5TO8_I2C_REG;    
    }
    rv = BSP_cp3banEpld16Read(dwSubslotId, offsetAddr, &tmpRegValue);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    regValue = (tmpRegValue & ((WORD16)0xFF00)) | (((WORD16)ucI2CDeviceAddr) | ((WORD16)0x1));  
    rv = BSP_cp3banEpld16Write(dwSubslotId, offsetAddr, regValue);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }

    /* 写I2C内部寄存器的地址 */
    tmpRegValue = 0;
    regValue = 0;
    if (portId <= (BSP_CP3BAN_MAX_STM1_PORT_NUM / ((WORD16)2)))  /* port 1#~4#*/
    {
        offsetAddr = BSP_CP3BAN_PORT1TO4_I2C_IN_REG;
    
    }
    else  /* port 5#~8#*/
    {
        offsetAddr = BSP_CP3BAN_PORT5TO8_I2C_IN_REG;
    
    }
    rv = BSP_cp3banEpld16Read(dwSubslotId, offsetAddr, &tmpRegValue);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    regValue = (tmpRegValue & ((WORD16)0xFF00)) | (((WORD16)ucI2CRegAddr) & ((WORD16)0x00FF));
    rv = BSP_cp3banEpld16Write(dwSubslotId, offsetAddr, regValue);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }

    /* 触发读I2C */
    tmpRegValue = 0;
    regValue = 0;
    if (portId <= (BSP_CP3BAN_MAX_STM1_PORT_NUM / ((WORD16)2)))  /* port 1#~4#*/
    {
        offsetAddr = BSP_CP3BAN_PORT1TO4_I2C_CTRL_REG;  
    }
    else  /* port 5#~8#*/
    {
        offsetAddr = BSP_CP3BAN_PORT5TO8_I2C_CTRL_REG;
    }
    rv = BSP_cp3banEpld16Read(dwSubslotId, offsetAddr, &tmpRegValue);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    regValue = (tmpRegValue & ((WORD16)0xFFFE)) | ((WORD16)0x1);
    rv = BSP_cp3banEpld16Write(dwSubslotId, offsetAddr, regValue);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }

    BSP_DelayMs(1); /* delay 1ms. */
    /* 读I2C */
    tmpRegValue = 0;
    if (portId <= (BSP_CP3BAN_MAX_STM1_PORT_NUM / ((WORD16)2)))  /* port 1#~4#*/
    {
        offsetAddr = BSP_CP3BAN_PORT1TO4_I2C_READ_REG;
    }
    else  /* port 5#~8#*/
    {
        offsetAddr = BSP_CP3BAN_PORT5TO8_I2C_READ_REG;  
    }
    rv = BSP_cp3banEpld16Read(dwSubslotId, offsetAddr, &tmpRegValue);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    *pI2cData = (BYTE)(tmpRegValue & ((WORD16)0x00FF));

    /* 判断读取I2C是否超时 */
    tmpRegValue = 0;
    if (portId <= (BSP_CP3BAN_MAX_STM1_PORT_NUM / ((WORD16)2)))  /* port 1#~4#*/
    {
        offsetAddr = BSP_CP3BAN_PORT1TO4_I2C_CTRL_REG; 
    }
    else  /* port 5#~8#*/
    {
        offsetAddr = BSP_CP3BAN_PORT5TO8_I2C_CTRL_REG;
    }
    for (dwLoopTimes = 0; dwLoopTimes < BSP_CP3BAN_I2C_TIMEOUT_TIMES; dwLoopTimes++)
    {
        rv = BSP_cp3banEpld16Read(dwSubslotId, offsetAddr, &tmpRegValue);
        if (BSP_E_CP3BAN_OK != rv)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
            return rv;
        }
        if (0 == (tmpRegValue & ((WORD16)0x1)))
        {
            break;
        }
        BSP_DelayMs(1); /* delay 1ms. */
    }
    if (dwLoopTimes >= BSP_CP3BAN_I2C_TIMEOUT_TIMES)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u portId %u ucI2CRegAddr 0x%x, reading I2C timeout.\n", __FILE__, __LINE__, dwSubslotId, portId, ucI2CRegAddr);
        return BSP_E_CP3BAN_I2C_ACCESS_TIMEOUT;
    }
    
    return BSP_E_CP3BAN_OK;
}


/****************************************************************************
* 函数名称: BSP_cp3banSfpRegWrite
* 功能描述: 写CP3BAN单板的SFP寄存器
* 输入参数: dwSubslotId      --CP3BAN单板的子槽位号
*           portId           --CP3BAN单板的STM端口的编号,取值为1~8.
*           ucI2CDeviceAddr  --I2C设备地址
*           ucI2CRegAddr     --I2C内部寄存器的地址.
*           i2cData          --I2C寄存器的值.
* 输出参数: 
* 返 回 值: BSP_E_CP3BAN_OK     --写成功
*         : 其他                --写失败          
* 其它说明
* 修改日期     版本号                 修改人              修改内容
* -----------------------------------------------------------------------------
* 2013/07/05   V1.0            xieweisheng10112265         create
******************************************************************************/
WORD32 BSP_cp3banSfpRegWrite(WORD32 dwSubslotId, 
                                       WORD16 portId, 
                                       BYTE ucI2CDeviceAddr, 
                                       BYTE ucI2CRegAddr, 
                                       BYTE i2cData)
{
    WORD32 rv = BSP_E_CP3BAN_OK;
    WORD16 regValue = 0;     /* 写入CPLD寄存器的值 */
    WORD16 tmpRegValue = 0;  /* 读取CPLD寄存器的值 */
    WORD16 offsetAddr = 0;   /* CPLD寄存器的偏移地址 */
    WORD32 dwLoopTimes = 0;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    rv = BSP_cp3banPortIdCheck(dwSubslotId, (WORD32)portId);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banPortIdCheck() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }

    /* 写端口选择寄存器 */
    if (portId <= (BSP_CP3BAN_MAX_STM1_PORT_NUM / ((WORD16)2)))  /* port 1#~4#*/
    {
        offsetAddr = BSP_CP3BAN_PORT1TO4_SELECTED_REG;
        rv = BSP_cp3banEpld16Read(dwSubslotId, offsetAddr, &tmpRegValue);
        if (BSP_E_CP3BAN_OK != rv)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
            return rv;
        }
        regValue = (tmpRegValue & ((WORD16)0xFF00)) | (portId - ((WORD16)1));
        rv = BSP_cp3banEpld16Write(dwSubslotId, offsetAddr, regValue);
        if (BSP_E_CP3BAN_OK != rv)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
            return rv;
        }
    }
    else   /* port 5#~8#*/
    {
        offsetAddr = BSP_CP3BAN_PORT5TO8_SELECTED_REG;
        rv = BSP_cp3banEpld16Read(dwSubslotId, offsetAddr, &tmpRegValue);
        if (BSP_E_CP3BAN_OK != rv)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
            return rv;
        }
        regValue = (tmpRegValue & ((WORD16)0xFF00)) | (portId - ((WORD16)5));
        rv = BSP_cp3banEpld16Write(dwSubslotId, offsetAddr, regValue);
        if (BSP_E_CP3BAN_OK != rv)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
            return rv;
        }
    }
    
    /* 写I2C设备 */
    tmpRegValue = 0;
    regValue = 0;
    if (portId <= (BSP_CP3BAN_MAX_STM1_PORT_NUM / ((WORD16)2)))  /* port 1#~4#*/
    {
        offsetAddr = BSP_CP3BAN_PORT1TO4_I2C_REG;   
    }
    else  /* port 5#~8#*/
    {
        offsetAddr = BSP_CP3BAN_PORT5TO8_I2C_REG;    
    }
    rv = BSP_cp3banEpld16Read(dwSubslotId, offsetAddr, &tmpRegValue);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    regValue = (tmpRegValue & ((WORD16)0xFF00)) | (((WORD16)ucI2CDeviceAddr) | ((WORD16)0x0));  
    rv = BSP_cp3banEpld16Write(dwSubslotId, offsetAddr, regValue);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }

    /* 写I2C寄存器的地址 */
    tmpRegValue = 0;
    regValue = 0;
    if (portId <= (BSP_CP3BAN_MAX_STM1_PORT_NUM / ((WORD16)2)))  /* port 1#~4#*/
    {
        offsetAddr = BSP_CP3BAN_PORT1TO4_I2C_IN_REG;
    
    }
    else  /* port 5#~8#*/
    {
        offsetAddr = BSP_CP3BAN_PORT5TO8_I2C_IN_REG;
    
    }
    rv = BSP_cp3banEpld16Read(dwSubslotId, offsetAddr, &tmpRegValue);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    regValue = (tmpRegValue & ((WORD16)0xFF00)) | (((WORD16)ucI2CRegAddr) & ((WORD16)0x00FF));
    rv = BSP_cp3banEpld16Write(dwSubslotId, offsetAddr, regValue);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }

    /* 写I2C寄存器的值 */
    tmpRegValue = 0;
    regValue = 0;
    if (portId <= (BSP_CP3BAN_MAX_STM1_PORT_NUM / ((WORD16)2))) /* port 1#~4#*/
    {
        offsetAddr = BSP_CP3BAN_PORT1TO4_I2C_WRITE_REG;
    }
    else /* port 5#~8#*/
    {
        offsetAddr = BSP_CP3BAN_PORT5TO8_I2C_WRITE_REG;
    }
    rv = BSP_cp3banEpld16Read(dwSubslotId, offsetAddr, &tmpRegValue);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    regValue = (tmpRegValue & ((WORD16)0xFF00)) | (((WORD16)i2cData) & ((WORD16)0x00FF));
    rv = BSP_cp3banEpld16Write(dwSubslotId, offsetAddr, regValue);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    
    BSP_DelayMs(1); /* delay 1ms. */
    /* 触发写I2C */
    tmpRegValue = 0;
    regValue = 0;
    if (portId <= (BSP_CP3BAN_MAX_STM1_PORT_NUM / ((WORD16)2)))  /* port 1#~4#*/
    {
        offsetAddr = BSP_CP3BAN_PORT1TO4_I2C_CTRL_REG;  
    }
    else  /* port 5#~8#*/
    {
        offsetAddr = BSP_CP3BAN_PORT5TO8_I2C_CTRL_REG;
    }
    rv = BSP_cp3banEpld16Read(dwSubslotId, offsetAddr, &tmpRegValue);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    regValue = (tmpRegValue & ((WORD16)0xFFFE)) | ((WORD16)0x1);
    rv = BSP_cp3banEpld16Write(dwSubslotId, offsetAddr, regValue);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Write() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }

    BSP_DelayMs(1); /* delay 1ms. */

    tmpRegValue = 0;
    /* 判断写I2C是否超时 */
    if (portId <= (BSP_CP3BAN_MAX_STM1_PORT_NUM / ((WORD16)2)))  /* port 1#~4#*/
    {
        offsetAddr = BSP_CP3BAN_PORT1TO4_I2C_CTRL_REG;  
    }
    else  /* port 5#~8#*/
    {
        offsetAddr = BSP_CP3BAN_PORT5TO8_I2C_CTRL_REG;
    }
    for (dwLoopTimes = 0; dwLoopTimes < BSP_CP3BAN_I2C_TIMEOUT_TIMES; dwLoopTimes++)
    {
        rv = BSP_cp3banEpld16Read(dwSubslotId, offsetAddr, &tmpRegValue);
        if (BSP_E_CP3BAN_OK != rv)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
            return rv;
        }
        if (0 == (tmpRegValue & ((WORD16)0x1)))
        {
            break;
        }
        BSP_DelayMs(1); /* delay 1ms. */
    }
    if (dwLoopTimes >= BSP_CP3BAN_I2C_TIMEOUT_TIMES)
    {
        BSP_Print(BSP_DEBUG_ALL, "ERROR: %s line %d, subslotId %u portId %u ucI2CRegAddr 0x%x, writing I2C timeout.\n", __FILE__, __LINE__, dwSubslotId, portId, ucI2CRegAddr);
        return BSP_E_CP3BAN_I2C_ACCESS_TIMEOUT;
    }
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banOpticRead
* 功能描述: 读取光模块的寄存器.
* 访问的表: 无
* 修改的表: 无
* 输入参数: ucI2CAddr: I2C设备地址.
*           ucRegAddr: I2C寄存器地址.
*           *pPassThrough: 保存其它需要的参数信息.
* 输出参数: *pucRegVal: 保存I2C寄存器的值.
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-23   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banOpticRead(BYTE ucI2CAddr, 
                                    BYTE ucRegAddr, 
                                    BYTE *pucRegVal, 
                                    VOID *pPassThrough)
{
    WORD32 rv = BSP_E_CP3BAN_OK;
    WORD32 dwSubslotId = 0;   /* 单板的子槽位号 */
    WORD16 wPortId = 0;        /* 155M端口的编号 */
    BSP_CP3BAN_PORT_INFO *pPortInfo = NULL;
    
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pucRegVal);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pPassThrough);
    
    pPortInfo = (BSP_CP3BAN_PORT_INFO *)pPassThrough;
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pPortInfo);
    dwSubslotId = pPortInfo->dwSubslotId;
    wPortId = (WORD16)(pPortInfo->dwPortId);
    
    rv = BSP_cp3banSfpRegRead(dwSubslotId, wPortId, ucI2CAddr, ucRegAddr, pucRegVal);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_SFP_ACCESS_MSG, "ERROR: %s line %d, subslotId %u portId %u, BSP_cp3banSfpRegRead() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, wPortId, rv);
        return rv;
    }
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banOpticLosStateGet
* 功能描述: 获取光模块的LOS状态.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: 单板的子槽位号,取值为1~4.
*           dwPortId: STM端口的编号,取值为1~8.
* 输出参数: *pState: 保存光模块的LOS状态,具体参见BSP_CP3BAN_OPTIC_LOS_STATE定义. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-23   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banOpticLosStateGet(WORD32 dwSubslotId, 
                                              WORD32 dwPortId, 
                                              WORD32 *pState)
{
    WORD32 rv = BSP_E_CP3BAN_OK;
    WORD16 wPcbId = 0;
    BYTE ucChipId = 0;  /* FPGA的芯片编号 */
    WORD16 regValue = 0;  /* EPLD寄存器的值 */
    WORD16 dwPortIdx = 0;
    WORD32 dwAlmStatus = 0;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pState);
    rv = BSP_cp3banPortIdCheck(dwSubslotId, dwPortId);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banPortIdCheck() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    
    rv = BSP_cp3banPcbIdGet(dwSubslotId, &wPcbId);  /* 获取CP3BAN单板的pcb ID. */
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banPcbIdGet() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }

    if (0 == wPcbId)  /* 改版前的CP3BAN单板 */
    {
        /* 老CP3BAN单板,通过EPLD来获取光模块的LOS状态. */
        dwPortIdx = (WORD16)(dwPortId - 1);
        rv = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_I2C_LOS_STATE_REG, &regValue);
        if (BSP_E_CP3BAN_OK != rv)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u portId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwPortId, rv);
            return rv;
        }
        *pState = 0;  /* 先清零再赋值 */
        if (0 == ((regValue >> dwPortIdx) & 0x1))  /* SFP无LOS */
        {
            *pState = BSP_CP3BAN_OPTIC_NONE_LOS;
        }
        else  /* SFP有LOS */
        {
            *pState = BSP_CP3BAN_OPTIC_LOS;
        }
    }
    else if (1 == wPcbId)  /* 改版后的CP3BAN单板 */
    {
        /* 新CP3BAN单板,通过FPGA来获取光模块的LOS状态 */
        ucChipId = (BYTE)(dwSubslotId - 1); 
        rv = drv_tdm_stmSerdesAlarmGet(ucChipId, (BYTE)dwPortId, &dwAlmStatus);
        if (DRV_TDM_OK != rv)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u portId %u, drv_tdm_stmSerdesAlarmGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwPortId, rv);
            return rv;
        }
        *pState = 0;  /* 先清零再赋值 */
        if (0 != (dwAlmStatus & DRV_TDM_STM_SERDES_LOS_ALM))  /* SFP有LOS */
        {
            *pState = BSP_CP3BAN_OPTIC_LOS;
        }
        else  /* SFP无LOS */
        {
            *pState = BSP_CP3BAN_OPTIC_NONE_LOS;
        }
    }
    else
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u, invalid pcb id, pcbId=%u.\n", __FILE__, __LINE__, dwSubslotId, wPcbId);
        return BSP_E_CP3BAN_INVALID_PCB_ID;
    }
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banOpticStateGet
* 功能描述: 获取光模块的在位状态.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: 单板的子槽位号,取值为1~4.
*           dwPortId: STM端口的编号,取值为1~8.
* 输出参数: *pState: 保存光模块的状态,具体参见BSP_CP3BAN_OPTIC_STATE定义. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-23   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banOpticStateGet(WORD32 dwSubslotId, 
                                         WORD32 dwPortId, 
                                         WORD32 *pState)
{
    WORD32 rv = BSP_E_CP3BAN_OK;
    WORD16 regValue = 0;  /* EPLD寄存器的值 */
    WORD16 dwPortIdx = 0;
    
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pState);
    rv = BSP_cp3banPortIdCheck(dwSubslotId, dwPortId);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banPortIdCheck() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    dwPortIdx = (WORD16)(dwPortId - 1);
    
    rv = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_I2C_STATE_REG, &regValue);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u portId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwPortId, rv);
        return rv;
    }

    *pState = 0;  /* 先清零再赋值 */
    if (0 == ((regValue >> dwPortIdx) & 0x1))  /* 光模块在位 */
    {
        *pState = BSP_CP3BAN_OPTIC_ONLINE;
    }
    else  /* 光模块不在位 */
    {
        *pState = BSP_CP3BAN_OPTIC_OFFLINE;
    }
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banBoardInfoGet
* 功能描述: 获取CP3BAN单板的信息.
* 访问的表: 无
* 修改的表: 无
* 输入参数:  
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-08-23   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banBoardInfoGet(struct Device *ptDev, T_BSP_DEVICEDRIVER_PARA *ptPara)
{
    WORD32 rv = BSP_E_CP3BAN_OK;
    WORD32 systemType = 0;   /* ZXUPN15000的系统类型 */
    WORD32 dwSubslotId = 0;
    WORD32 initFlag = 0;  /* CP3BAN单板的初始化标记 */
    WORD32 fpgaVerNo = 0;  /* version number of FPGA. */
    T_BSP_SUBCARD_BOARDINFO *pTmpBoardInfo = NULL;
    T_BSP_SUBCARD_BOARDINFO *pBoardInfo = NULL;
    
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptDev);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptPara);
    systemType = BSP_cp3banSystemTypeGet();  /* 获取ZXUPN15000系统类型. */
    dwSubslotId = ptDev->ext + CP3BAN_INDX_MIN;
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM != systemType)   /* 15K-2系统 */
    {
        dwSubslotId = 1;   /* 对于15K-2系统,子槽位始终取值为1 */
    }

    rv = BSP_cp3banBoardInitFlagGet(dwSubslotId, &initFlag);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_COMMON_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banBoardInitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    if (BSP_CP3BAN_BOARD_UNINSTALL == initFlag)  
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_COMMON_MSG, "ERROR: %s line %d, subslotId %u, Board has not been initialized.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_NOT_INITIALIZED;
    }
    
    if (BSP_Q_SUBCARD_BOARD_INFO == ptPara->dwQueryCode)
    {
        pBoardInfo = (T_BSP_SUBCARD_BOARDINFO *)(ptPara->pucBuf);
        BSP_CP3BAN_CHECK_POINTER_IS_NULL(pBoardInfo);
        if (ptPara->dwBufLen != sizeof(T_BSP_SUBCARD_BOARDINFO))
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_COMMON_MSG, "ERROR: %s line %d, subslotId %u, invalid buffer length, bufferLength=%u.\n", __FILE__, __LINE__, dwSubslotId, ptPara->dwBufLen);
            return BSP_E_CP3BAN_PARA;
        }
        rv = drv_tdm_fpgaVersionNumberGet((BYTE)dwSubslotId, &fpgaVerNo);
        if (DRV_TDM_OK != rv)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_COMMON_MSG, "ERROR: %s line %d, subslotId %u, drv_tdm_fpgaVersionNumberGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, rv);
            return rv;
        }
        rv = BSP_cp3ban_boardInfoMemGet(dwSubslotId, &pTmpBoardInfo);
        if (BSP_E_CP3BAN_OK != rv)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_COMMON_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3ban_boardInfoMemGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, rv);
            return rv;
        }
        BSP_CP3BAN_CHECK_POINTER_IS_NULL(pTmpBoardInfo);
        memset(pBoardInfo, 0, sizeof(T_BSP_SUBCARD_BOARDINFO));  /* 先清零再赋值 */
        memcpy(pBoardInfo, pTmpBoardInfo, sizeof(T_BSP_SUBCARD_BOARDINFO));
        pBoardInfo->tFpgaDevInfo[0].dwVersion = fpgaVerNo;
    }
    else
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_COMMON_MSG, "ERROR: %s line %d, subslotId %u, invalid query code, queryCode=0x%x.\n", __FILE__, __LINE__, dwSubslotId, ptPara->dwQueryCode);
        return BSP_E_CP3BAN_PARA;
    }
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banJtagNumGet
* 功能描述: 获取CP3BAN单板的JTAG数量.
* 访问的表: 无
* 修改的表: 无
* 输入参数:  
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-23   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banJtagNumGet(struct Device *ptDev, T_BSP_DEVICEDRIVER_PARA *ptPara)
{
    WORD32 rv = BSP_E_CP3BAN_OK;
    WORD32 systemType = 0;   /* ZXUPN15000的系统类型 */
    WORD32 dwSubslotId = 0;
    WORD32 initFlag = 0;  /* CP3BAN单板的初始化标记 */
    T_BSP_BRD_JTAG_NUM_INFO *pJtagInfo = NULL;
    
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptDev);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptPara);
    systemType = BSP_cp3banSystemTypeGet();  /* 获取ZXUPN15000系统类型. */
    dwSubslotId = ptDev->ext + CP3BAN_INDX_MIN;
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM != systemType)   /* 15K-2系统 */
    {
        dwSubslotId = 1;   /* 对于15K-2系统,子槽位始终取值为1 */
    }
    
    rv = BSP_cp3banBoardInitFlagGet(dwSubslotId, &initFlag);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_COMMON_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banBoardInitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    if (BSP_CP3BAN_BOARD_UNINSTALL == initFlag)  
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_COMMON_MSG, "ERROR: %s line %d, subslotId %u, Board has not been initialized.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_NOT_INITIALIZED;
    }
    
    if (BSP_Q_SUBCARD_JTAG_NUM == ptPara->dwQueryCode)
    {
        pJtagInfo = (T_BSP_BRD_JTAG_NUM_INFO *)(ptPara->pucBuf);
        BSP_CP3BAN_CHECK_POINTER_IS_NULL(pJtagInfo);
        if (ptPara->dwBufLen != sizeof(T_BSP_BRD_JTAG_NUM_INFO))
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_COMMON_MSG, "ERROR: %s line %d, subslotId %u, invalid buffer length, bufferLength=%u.\n", __FILE__, __LINE__, dwSubslotId, ptPara->dwBufLen);
            return BSP_E_CP3BAN_PARA;
        }
        memset(pJtagInfo, 0, sizeof(T_BSP_BRD_JTAG_NUM_INFO));
        pJtagInfo->dwJtagNum = BSP_CP3BAN_JTAG_NUM;  /* 单板上JTAG链的数目 */
    }
    else
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_COMMON_MSG, "ERROR: %s line %d, subslotId %u, invalid query code, queryCode=0x%x.\n", __FILE__, __LINE__, dwSubslotId, ptPara->dwQueryCode);
        return BSP_E_CP3BAN_PARA;
    }
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banJtagEpldNumGet
* 功能描述: 获取子卡某条JTAG链的EPLD数目.
* 访问的表: 无
* 修改的表: 无
* 输入参数:  
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-23   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banJtagEpldNumGet(struct Device *ptDev, T_BSP_DEVICEDRIVER_PARA *ptPara)
{
    WORD32 rv = BSP_E_CP3BAN_OK;
    WORD32 systemType = 0;   /* ZXUPN15000的系统类型 */
    WORD32 dwSubslotId = 0;
    WORD32 initFlag = 0;  /* CP3BAN单板的初始化标记 */
    T_BSP_JTAG_EPLD_NUM_INFO *pJtagEpldNum = NULL;
    
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptDev);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptPara);
    systemType = BSP_cp3banSystemTypeGet();  /* 获取ZXUPN15000系统类型. */
    dwSubslotId = ptDev->ext + CP3BAN_INDX_MIN;
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM != systemType)   /* 15K-2系统 */
    {
        dwSubslotId = 1;   /* 对于15K-2系统,子槽位始终取值为1 */
    }
    
    rv = BSP_cp3banBoardInitFlagGet(dwSubslotId, &initFlag);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_COMMON_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banBoardInitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    if (BSP_CP3BAN_BOARD_UNINSTALL == initFlag)  
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_COMMON_MSG, "ERROR: %s line %d, subslotId %u, Board has not been initialized.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_NOT_INITIALIZED;
    }

    if (BSP_Q_SUBCARD_JTAG_EPLD_NUM == ptPara->dwQueryCode)
    {
        pJtagEpldNum = (T_BSP_JTAG_EPLD_NUM_INFO *)(ptPara->pucBuf);
        BSP_CP3BAN_CHECK_POINTER_IS_NULL(pJtagEpldNum);
        if (ptPara->dwBufLen != sizeof(T_BSP_JTAG_EPLD_NUM_INFO))
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_COMMON_MSG, "ERROR: %s line %d, subslotId %u, invalid buffer length, bufferLength=%u.\n", __FILE__, __LINE__, dwSubslotId, ptPara->dwBufLen);
            return BSP_E_CP3BAN_PARA;
        }
        memset(pJtagEpldNum, 0, sizeof(T_BSP_JTAG_EPLD_NUM_INFO)); /* 先清零再赋值 */
        pJtagEpldNum->dwJtagIndex = BSP_CP3BAN_JTAG_INDEX;
        pJtagEpldNum->dwEpldNum = BSP_CP3BAN_JTAG_EPLD_NUM;
    }
    else
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_COMMON_MSG, "ERROR: %s line %d, subslotId %u, invalid query code, queryCode=0x%x.\n", __FILE__, __LINE__, dwSubslotId, ptPara->dwQueryCode);
        return BSP_E_CP3BAN_PARA;
    }
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banJtagEpldInfoGet
* 功能描述: 获取子卡上某条JTAG链的EPLD信息.
* 访问的表: 无
* 修改的表: 无
* 输入参数:  
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-23   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banJtagEpldInfoGet(struct Device *ptDev, T_BSP_DEVICEDRIVER_PARA *ptPara)
{
    WORD32 rv = BSP_E_CP3BAN_OK;
    WORD32 systemType = 0;   /* ZXUPN15000的系统类型 */
    WORD32 dwSubslotId = 0;
    WORD32 initFlag = 0;  /* CP3BAN单板的初始化标记 */
    WORD16 epldVersion = 0;
    T_BSP_EPLD_VERSION_INFO *pJtagEpldInfo = NULL;
    
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptDev);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptPara);
    systemType = BSP_cp3banSystemTypeGet();  /* 获取ZXUPN15000系统类型. */
    dwSubslotId = ptDev->ext + CP3BAN_INDX_MIN;
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM != systemType)   /* 15K-2系统 */
    {
        dwSubslotId = 1;   /* 对于15K-2系统,子槽位始终取值为1 */
    }
    
    rv = BSP_cp3banBoardInitFlagGet(dwSubslotId, &initFlag);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_COMMON_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banBoardInitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    if (BSP_CP3BAN_BOARD_UNINSTALL == initFlag)  
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_COMMON_MSG, "ERROR: %s line %d, subslotId %u, Board has not been initialized.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_NOT_INITIALIZED;
    }
    
    if (BSP_Q_SUBCARD_JTAG_EPLD_INFO == ptPara->dwQueryCode)
    {
        pJtagEpldInfo = (T_BSP_EPLD_VERSION_INFO *)(ptPara->pucBuf);
        BSP_CP3BAN_CHECK_POINTER_IS_NULL(pJtagEpldInfo);
        if (ptPara->dwBufLen != sizeof(T_BSP_EPLD_VERSION_INFO))
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_COMMON_MSG, "ERROR: %s line %d, subslotId %u, invalid buffer length, bufferLength=%u.\n", __FILE__, __LINE__, dwSubslotId, ptPara->dwBufLen);
            return BSP_E_CP3BAN_PARA;
        }
        memset(pJtagEpldInfo, 0, sizeof(T_BSP_EPLD_VERSION_INFO)); /* 先清零再赋值 */
        rv = BSP_cp3banEpld16Read(dwSubslotId, BSP_CP3BAN_EPLD_VERSION_REG, &epldVersion);
        if (BSP_E_CP3BAN_OK != rv)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_COMMON_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banEpld16Read() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, rv);
            return rv;
        }
        pJtagEpldInfo->dwJtagIndex = BSP_CP3BAN_JTAG_INDEX;
        pJtagEpldInfo->dwEpldIndex = BSP_CP3BAN_JTAG_EPLD_INDEX;
        pJtagEpldInfo->dwLableEpldFlag = BSP_IS_LABLE_EPLD;
        pJtagEpldInfo->dwEpldVer = (WORD32)epldVersion;
        pJtagEpldInfo->dwEpldVerCheckResult = BSP_EPLD_CHECK_NONSUPPORT;
        rv = BSP_cp3banEpldVerify(dwSubslotId);
        if (BSP_E_CP3BAN_OK != rv)
        {
            pJtagEpldInfo->dwEpldRwCheckResult = BSP_EPLD_CHECK_FAILURE;
        }
        else
        {
            pJtagEpldInfo->dwEpldRwCheckResult = BSP_EPLD_CHECK_SUCCESS;
        }
    }
    else
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_COMMON_MSG, "ERROR: %s line %d, subslotId %u, invalid query code, queryCode=0x%x.\n", __FILE__, __LINE__, dwSubslotId, ptPara->dwQueryCode);
        return BSP_E_CP3BAN_PARA;
    }
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banOpticLosStateQuery
* 功能描述: 查询单板的光模块LOS状态.
* 访问的表: 无
* 修改的表: 无
* 输入参数:  
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-23   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banOpticLosStateQuery(struct Device *ptDev, 
                                                 T_BSP_DEVICEDRIVER_PARA *ptPara)
{
    WORD32 rv = BSP_E_CP3BAN_OK;
    WORD32 systemType = 0;   /* ZXUPN15000的系统类型 */
    WORD32 dwSubslotId = 0;  /* 单板的子槽位号 */
    WORD32 initFlag = 0;  /* CP3BAN单板的初始化标记 */
    WORD32 dwPortId = 0;   /* 155M端口的编号 */
    WORD32 dwSfpLosState = 0;  /* 光模块的LOS状态 */
    
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptDev);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptPara);
    systemType = BSP_cp3banSystemTypeGet();  /* 获取ZXUPN15000系统类型. */
    dwSubslotId = ptDev->ext + CP3BAN_INDX_MIN;
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM != systemType)   /* 15K-2系统 */
    {
        dwSubslotId = 1;   /* 对于15K-2系统,子槽位始终取值为1 */
    }
    
    rv = BSP_cp3banBoardInitFlagGet(dwSubslotId, &initFlag);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banBoardInitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    if (BSP_CP3BAN_BOARD_UNINSTALL == initFlag)  
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u, Board has not been initialized.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_NOT_INITIALIZED;
    }

    if (BSP_Q_SUBCARD_LOS_STATUS == ptPara->dwQueryCode)
    {
        dwPortId = ptPara->adwAddr[1];
        if (BSP_READ == ptPara->dwMode)  /* 查询光模块的LOS状态 */
        {
            rv = BSP_cp3banOpticLosStateGet(dwSubslotId, dwPortId, &dwSfpLosState);
            if (BSP_E_CP3BAN_OK != rv)
            {
                BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u portId %u, BSP_cp3banOpticLosStateGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwPortId, rv);
                return rv;
            }
            if (BSP_CP3BAN_OPTIC_LOS == dwSfpLosState)
            {
                ptPara->adwAddr[2] = BSP_LOS;
            }
            else if (BSP_CP3BAN_OPTIC_NONE_LOS == dwSfpLosState)
            {
                ptPara->adwAddr[2] = BSP_NO_LOS;
            }
            else
            {
                BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u portId %u, invalid sfp los state, sfpLosState=%u.\n", __FILE__, __LINE__, dwSubslotId, dwPortId, dwSfpLosState);
                return BSP_E_CP3BAN_PARA;
            }
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "%s line %d, subslotId %u portId %u, gettingSfpLosState=%u.\n", __FILE__, __LINE__, dwSubslotId, dwPortId, ptPara->adwAddr[2]);
        }
        else
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u portId %u, invalid operate mode, operateMode=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwPortId, ptPara->dwMode);
            return BSP_E_CP3BAN_PARA;
        }
    }
    else
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u, invalid query code, queryCode=0x%x.\n", __FILE__, __LINE__, dwSubslotId, ptPara->dwQueryCode);
        return BSP_E_CP3BAN_PARA;
    }
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banOpticInfoPrint
* 功能描述: 打印某个155M端口的光模块信息.
* 访问的表: 无
* 修改的表: 无
* 输入参数: dwSubslotId: 单板的子槽位号,取值为1~4. 
*           dwPortId: STM端口的编号,取值为1~8.
*           *pOptInfo: 光模块信息的结构.
* 输出参数: 无. 
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-23   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banOpticInfoPrint(WORD32 dwSubslotId, WORD32 dwPortId, const T_BSP_OPT_INFO *pOptInfo)
{
    WORD32 rv = BSP_E_CP3BAN_OK;

    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    rv = BSP_cp3banPortIdCheck(dwSubslotId, dwPortId);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_PARAMETER_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banPortIdCheck() rv=0x%x\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(pOptInfo);
    
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "\nsubslotId %u portId %u, Optic module information:\n", dwSubslotId, dwPortId);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "dwOptInvalid: %u\n", pOptInfo->dwOptInvalid);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "dwOptOnline: %u\n", pOptInfo->dwOptOnline);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "dwOptStand: %u\n", pOptInfo->dwOptStand);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "dwInfoMask: %u\n", pOptInfo->tInfo.tSff8472.dwInfoMask);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "dwInfoVer: %u\n", pOptInfo->tInfo.tSff8472.dwInfoVer);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ucOptVer: %u\n", pOptInfo->tInfo.tSff8472.ucOptVer);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ucOptType: %u\n", pOptInfo->tInfo.tSff8472.ucOptType);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ucCompCode1: %u\n", pOptInfo->tInfo.tSff8472.ucCompCode1);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ucCompCode2: %u\n", pOptInfo->tInfo.tSff8472.ucCompCode2);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ucCompCode3: %u\n", pOptInfo->tInfo.tSff8472.ucCompCode3);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ucCompCode4: %u\n", pOptInfo->tInfo.tSff8472.ucCompCode4);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "uc9umLengthkm: %u\n", pOptInfo->tInfo.tSff8472.uc9umLengthkm);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "uc9umLength100m: %u\n", pOptInfo->tInfo.tSff8472.uc9umLength100m);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "uc50umOm2Length10m: %u\n", pOptInfo->tInfo.tSff8472.uc50umOm2Length10m);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "uc62umOm1Length10m: %u\n", pOptInfo->tInfo.tSff8472.uc62umOm1Length10m);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "uc50umOm3Length10m: %u\n", pOptInfo->tInfo.tSff8472.uc50umOm3Length10m);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ucCopperLength1m: %u\n", pOptInfo->tInfo.tSff8472.ucCopperLength1m);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wLaserWavelength: %u\n", pOptInfo->tInfo.tSff8472.wLaserWavelength);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "aVendorName: %s\n", pOptInfo->tInfo.tSff8472.aVendorName);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "aVendorPN: %s\n", pOptInfo->tInfo.tSff8472.aVendorPN);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "aVendorREV: %s\n", pOptInfo->tInfo.tSff8472.aVendorREV);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "aVendorSN: %s\n", pOptInfo->tInfo.tSff8472.aVendorSN);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "dwDiaMonitoring: %u\n", pOptInfo->tInfo.tSff8472.dwDiaMonitoring);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wTxPower: %u\n", pOptInfo->tInfo.tSff8472.wTxPower);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wRxPower: %u\n", pOptInfo->tInfo.tSff8472.wRxPower);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wTxBiasA: %u\n", pOptInfo->tInfo.tSff8472.wTxBiasA);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wTemperature: %u\n", pOptInfo->tInfo.tSff8472.wTemperature);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "dwRxPwr4: %u\n", pOptInfo->tInfo.tSff8472.dwRxPwr4);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "dwRxPwr3: %u\n", pOptInfo->tInfo.tSff8472.dwRxPwr3);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "dwRxPwr2: %u\n", pOptInfo->tInfo.tSff8472.dwRxPwr2);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "dwRxPwr1: %u\n", pOptInfo->tInfo.tSff8472.dwRxPwr1);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "dwRxPwr0: %u\n", pOptInfo->tInfo.tSff8472.dwRxPwr0);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wTxISlope: %u\n", pOptInfo->tInfo.tSff8472.wTxISlope);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wTxIOffset: %u\n", pOptInfo->tInfo.tSff8472.wTxIOffset);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wTxPwrSlope: %u\n", pOptInfo->tInfo.tSff8472.wTxPwrSlope);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wTxPwrOffset: %u\n", pOptInfo->tInfo.tSff8472.wTxPwrOffset);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wTSlope: %u\n", pOptInfo->tInfo.tSff8472.wTSlope);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wTOffset: %u\n", pOptInfo->tInfo.tSff8472.wTOffset);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "w33Voltage: %u\n", pOptInfo->tInfo.tSff8472.w33Voltage);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "w5Voltage: %u\n", pOptInfo->tInfo.tSff8472.w5Voltage);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wTxPowerHighAlarm: %u\n", pOptInfo->tInfo.tSff8472.wTxPowerHighAlarm);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wTxPowerLowAlarm: %u\n", pOptInfo->tInfo.tSff8472.wTxPowerLowAlarm);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wTxPowerHighWarning: %u\n", pOptInfo->tInfo.tSff8472.wTxPowerHighWarning);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wTxPowerLowWarning: %u\n", pOptInfo->tInfo.tSff8472.wTxPowerLowWarning);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wRxPowerHighAlarm: %u\n", pOptInfo->tInfo.tSff8472.wRxPowerHighAlarm);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wRxPowerLowAlarm: %u\n", pOptInfo->tInfo.tSff8472.wRxPowerLowAlarm);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wRxPowerHighWarning: %u\n", pOptInfo->tInfo.tSff8472.wRxPowerHighWarning);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wRxPowerLowWarning: %u\n", pOptInfo->tInfo.tSff8472.wRxPowerLowWarning);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wTxCurrentHighAlarm: %u\n", pOptInfo->tInfo.tSff8472.wTxCurrentHighAlarm);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wTxCurrentLowAlarm: %u\n", pOptInfo->tInfo.tSff8472.wTxCurrentHighAlarm);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wTxCurrentLowAlarm: %u\n", pOptInfo->tInfo.tSff8472.wTxCurrentLowAlarm);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wTxCurrentHighWarning: %u\n", pOptInfo->tInfo.tSff8472.wTxCurrentHighWarning);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wTxCurrentLowWarning: %u\n", pOptInfo->tInfo.tSff8472.wTxCurrentLowWarning);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wTempHighAlarm: %u\n", pOptInfo->tInfo.tSff8472.wTempHighAlarm);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wTempLowAlarm: %u\n", pOptInfo->tInfo.tSff8472.wTempLowAlarm);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wTempHighWarning: %u\n", pOptInfo->tInfo.tSff8472.wTempHighWarning);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "wTempLowWarning: %u\n", pOptInfo->tInfo.tSff8472.wTempLowWarning);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "w33VoltageHighAlarm: %u\n", pOptInfo->tInfo.tSff8472.w33VoltageHighAlarm);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "w33VoltageLowAlarm: %u\n", pOptInfo->tInfo.tSff8472.w33VoltageLowAlarm);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "w33VoltageHighWarning: %u\n", pOptInfo->tInfo.tSff8472.w33VoltageHighWarning);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "w33VoltageLowWarning: %u\n", pOptInfo->tInfo.tSff8472.w33VoltageLowWarning);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "w5VoltageHighAlarm: %u\n", pOptInfo->tInfo.tSff8472.w5VoltageHighAlarm);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "w5VoltageLowAlarm: %u\n", pOptInfo->tInfo.tSff8472.w5VoltageLowAlarm);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "w5VoltageHighWarning: %u\n", pOptInfo->tInfo.tSff8472.w5VoltageHighWarning);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "w5VoltageLowWarning: %u\n", pOptInfo->tInfo.tSff8472.w5VoltageLowWarning);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ucMediaType: %u\n", pOptInfo->tInfo.tSff8472.ucMediaType);
    BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ucConnectorType: %u\n", pOptInfo->tInfo.tSff8472.ucConnectorType);
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banOpticInfoGet
* 功能描述: 获取单板的光模块信息.
* 访问的表: 无
* 修改的表: 无
* 输入参数:  
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-23   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banOpticInfoGet(struct Device *ptDev, 
                                       T_BSP_DEVICEDRIVER_PARA *ptPara)
{
    WORD32 rv = BSP_E_CP3BAN_OK;
    WORD32 systemType = 0;   /* ZXUPN15000的系统类型 */
    WORD32 dwSubslotId = 0;
    WORD32 initFlag = 0;  /* CP3BAN单板的初始化标记 */
    WORD32 dwPortId = 0;   /* STM端口的编号 */
    T_BSP_OPT_INFO *pPmOptInfo = NULL;  /* 保存产品管理的光模块信息. */
    T_BSP_OPT_INFO bspSfpInfo;  /* 保存BSP的光模块信息. */
    
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptDev);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptPara);
    systemType = BSP_cp3banSystemTypeGet();  /* 获取ZXUPN15000系统类型. */
    dwSubslotId = ptDev->ext + CP3BAN_INDX_MIN;
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM != systemType)   /* 15K-2系统 */
    {
        dwSubslotId = 1;   /* 对于15K-2系统,子槽位始终取值为1 */
    }
    BSP_CHECK_CP3BAN_SUBSLOT_ID(dwSubslotId);
    memset(&bspSfpInfo, 0, sizeof(T_BSP_OPT_INFO));
    
    rv = BSP_cp3banBoardInitFlagGet(dwSubslotId, &initFlag);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banBoardInitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    if (BSP_CP3BAN_BOARD_UNINSTALL == initFlag)  
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u, Board has not been initialized.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_NOT_INITIALIZED;
    }

    if (BSP_Q_SUBCARD_OPT_INFO == ptPara->dwQueryCode)
    {
        dwPortId = ptPara->adwAddr[1];
        pPmOptInfo = (T_BSP_OPT_INFO *)(ptPara->pucBuf);
        BSP_CP3BAN_CHECK_POINTER_IS_NULL(pPmOptInfo);
        if (ptPara->dwBufLen != sizeof(T_BSP_OPT_INFO))
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u, invalid buffer length, bufferLength=%u.\n", __FILE__, __LINE__, dwSubslotId, ptPara->dwBufLen);
            return BSP_E_CP3BAN_PARA;
        }

        rv = BSP_cp3banSfpInfoGet(dwSubslotId, dwPortId, &bspSfpInfo);
        if (BSP_E_CP3BAN_OK != rv)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u portId %u, BSP_cp3banSfpInfoGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwPortId, rv);
            return rv;
        }
        memcpy(pPmOptInfo, &bspSfpInfo, sizeof(T_BSP_OPT_INFO));
        if (0 != g_bsp_cp3ban_sfp_debug[dwSubslotId][dwPortId]) /* 打印光模块信息 */
        {
            BSP_cp3banOpticInfoPrint(dwSubslotId, dwPortId, pPmOptInfo);
        }
    }
    else
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_OPTIC_MSG, "ERROR: %s line %d, subslotId %u, invalid query code, queryCode=0x%x.\n", __FILE__, __LINE__, dwSubslotId, ptPara->dwQueryCode);
        return BSP_E_CP3BAN_PARA;
    }
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banAd9557StateQuery
* 功能描述: 查询AD9557的时钟锁定状态.
* 访问的表: 无
* 修改的表: 无
* 输入参数:  
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-09-23   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banAd9557StateQuery(struct Device *ptDev, 
                                               T_BSP_DEVICEDRIVER_PARA *ptPara)
{
    WORD32 rv = BSP_E_CP3BAN_OK;
    WORD32 systemType = 0;   /* ZXUPN15000的系统类型 */
    WORD32 dwSubslotId = 0;
    WORD32 initFlag = 0;  /* CP3BAN单板的初始化标记 */
    WORD32 dwLockState = 0;  /* AD9557的锁定状态 */
    
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptDev);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptPara);
    systemType = BSP_cp3banSystemTypeGet();  /* 获取ZXUPN15000系统类型. */
    dwSubslotId = ptDev->ext + CP3BAN_INDX_MIN;
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM != systemType)   /* 15K-2系统 */
    {
        dwSubslotId = 1;   /* 对于15K-2系统,子槽位始终取值为1 */
    }
    
    rv = BSP_cp3banBoardInitFlagGet(dwSubslotId, &initFlag);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_AD9557_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banBoardInitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    if (BSP_CP3BAN_BOARD_UNINSTALL == initFlag)  
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_AD9557_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, Board has not been initialized.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_NOT_INITIALIZED;
    }

    if (BSP_Q_SUBCARD_AD9557_LOCK_STATE == ptPara->dwQueryCode)
    {
        rv = BSP_cp3banAd9557LockStatusGet(dwSubslotId, &dwLockState);
        if (BSP_E_CP3BAN_OK != rv)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_AD9557_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banAd9557LockStatusGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, rv);
            return rv;
        }
        if (BSP_CP3BAN_AD9557_LOCKED == dwLockState)
        {
            ptPara->adwAddr[1] = BSP_AD9557_CLK_LOCKED;
        }
        else if (BSP_CP3BAN_AD9557_UNLOCK == dwLockState)
        {
            ptPara->adwAddr[1] = BSP_AD9557_CLK_UNLOCKED;
        }
        else
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_AD9557_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, invalid lock state, lockState=0x%x.\n", __FILE__, __LINE__, dwSubslotId, dwLockState);
            return BSP_E_CP3BAN_PARA;
        }
    }
    else
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_AD9557_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, invalid query code, queryCode=0x%x.\n", __FILE__, __LINE__, dwSubslotId, ptPara->dwQueryCode);
        return BSP_E_CP3BAN_PARA;
    }
    
    return BSP_E_CP3BAN_OK;
}


/**************************************************************************
* 函数名称: BSP_cp3banFpgaAlmStatusGet
* 功能描述: 获取FPGA的告警状态.
* 访问的表: 无
* 修改的表: 无
* 输入参数:  
* 输出参数:  
* 返 回 值: 
* 其它说明: 
* 修改日期    版本号     修改人      修改内容
* -----------------------------------------------------------------------
* 2013-12-02   V1.0   谢伟生10112265     create    
**************************************************************************/
WORD32 BSP_cp3banFpgaAlmStatusGet(struct Device *ptDev, 
                                               T_BSP_DEVICEDRIVER_PARA *ptPara)
{
    WORD32 rv = BSP_E_CP3BAN_OK;
    WORD32 systemType = 0;   /* ZXUPN15000的系统类型 */
    WORD32 dwSubslotId = 0;
    WORD32 initFlag = 0;  /* CP3BAN单板的初始化标记 */
    WORD32 dwAlmStatus = 0;  /* 告警状态 */
    T_BSP_SUBCARD_FPGA_ALM *pFpgaAlm = NULL;
    
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptDev);
    BSP_CP3BAN_CHECK_POINTER_IS_NULL(ptPara);
    systemType = BSP_cp3banSystemTypeGet();  /* 获取ZXUPN15000系统类型. */
    dwSubslotId = ptDev->ext + CP3BAN_INDX_MIN;
    if (BSP_CP3BAN_NONE_15K_2_SYSTEM != systemType)   /* 15K-2系统 */
    {
        dwSubslotId = 1;   /* 对于15K-2系统,子槽位始终取值为1 */
    }
    rv = BSP_cp3banBoardInitFlagGet(dwSubslotId, &initFlag);
    if (BSP_E_CP3BAN_OK != rv)
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, BSP_cp3banBoardInitFlagGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, rv);
        return rv;
    }
    if (BSP_CP3BAN_BOARD_UNINSTALL == initFlag)  
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, Board has not been initialized.\n", __FILE__, __LINE__, dwSubslotId);
        return BSP_E_CP3BAN_NOT_INITIALIZED;
    }
    
    if (BSP_Q_SUBCARD_FPGA_STATUS == ptPara->dwQueryCode)
    {
        pFpgaAlm = (T_BSP_SUBCARD_FPGA_ALM *)(ptPara->pucBuf);
        BSP_CP3BAN_CHECK_POINTER_IS_NULL(pFpgaAlm);
        if (ptPara->dwBufLen != sizeof(T_BSP_SUBCARD_FPGA_ALM))
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, invalid buffer length, bufferLength=0x%x.\n", __FILE__, __LINE__, dwSubslotId, ptPara->dwBufLen);
            return BSP_E_CP3BAN_PARA;
        }
        dwAlmStatus = 0;
        #if 0
        rv = drv_tdm_fpgaClockStateQuery((BYTE)dwSubslotId, &dwAlmStatus);
        if (DRV_TDM_OK != rv)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, drv_tdm_fpgaClockStateQuery() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, rv);
            return rv;
        }
        #endif
        pFpgaAlm->wClkAlm = (WORD16)dwAlmStatus; /* 0表示时钟没有告警,1表示时钟有告警 */

        dwAlmStatus = 0;
        #if 0
        rv = drv_tdm_fpgaRamTestedStateGet((BYTE)dwSubslotId, &dwAlmStatus);
        if (DRV_TDM_OK != rv)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, drv_tdm_fpgaRamTestedStateGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, rv);
            return rv;
        }
        #endif
        pFpgaAlm->wRamFinishStatus = (WORD16)dwAlmStatus; /* 0表示OK */

        dwAlmStatus = 0;
        #if 0
        rv = drv_tdm_fpgaRamStateGet((BYTE)dwSubslotId, &dwAlmStatus);
        if (DRV_TDM_OK != rv)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, drv_tdm_fpgaRamStateGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, rv);
            return rv;
        }
        #endif
        pFpgaAlm->wRamAlm = (WORD16)dwAlmStatus;  /* 0表示RAM没有告警,1表示RAM有告警 */

        dwAlmStatus = 0;
        #if 0
        rv = drv_tdm_fpgaPhyIntfStateGet((BYTE)dwSubslotId, &dwAlmStatus);
        if (DRV_TDM_OK != rv)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, drv_tdm_fpgaPhyIntfStateGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, rv);
            return rv;
        }
        #endif
        pFpgaAlm->wIntfAlm = (WORD16)dwAlmStatus; /* 0表示物理接口没有告警,1表示物理接口有告警 */

        dwAlmStatus = 0;
        #if 0
        rv = drv_tdm_fpgaLogicStateGet((BYTE)dwSubslotId, &dwAlmStatus);
        if (DRV_TDM_OK != rv)
        {
            BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, drv_tdm_fpgaLogicStateGet() rv=0x%x.\n", __FILE__, __LINE__, dwSubslotId, rv);
            return rv;
        }
        #endif
        pFpgaAlm->awReserved[0] = (WORD16)dwAlmStatus; /* 0表示FPGA逻辑没有告警,1表示有告警 */
    }
    else
    {
        BSP_CP3BAN_DEBUG_TRACE(BSP_CP3BAN_DBG_FPGA_ACCESS_MSG, "ERROR: %s line %d, subslotId %u, invalid query code, queryCode=0x%x.\n", __FILE__, __LINE__, dwSubslotId, ptPara->dwQueryCode);
        return BSP_E_CP3BAN_PARA;
    }
    
    return BSP_E_CP3BAN_OK;
}



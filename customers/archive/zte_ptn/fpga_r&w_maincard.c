WORD32 ArriveFpga8Read(WORD32 subcardId, WORD16 offsetAddr, WORD16 *value)
{
    WORD32 ret = DRV_AT_SUCCESS; 
	WORD16 tmpValue = 0 ;
	
    CHECK_AT_SUBCARD_ID(subcardId);
    CHECK_AT_POINTER_NULL(value);
    
    *value = 0;   /* 先清零再保存值 */
    /*offsetAddr = (offsetAddr << 1);*/ /* 通过EPLD间接访问FPGA,FPGA的偏移地址需要乘以2. */
    ret = Bsp_CE1TAN8_FPGA_Read(subcardId, offsetAddr, &tmpValue);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, FPGA READ FAILED !!\n", __FILE__, __LINE__); 
        return DRV_AT_FPGA_READ_FAILED; 
    }
    *value = tmpValue;
    
    return DRV_AT_SUCCESS;
}



WORD32 ArriveFpga8Write(WORD32 subcardId, WORD16 offsetAddr, WORD16 value)
{
    WORD32 ret = DRV_AT_SUCCESS; 
	
    CHECK_AT_SUBCARD_ID(subcardId);
    
    /*offsetAddr = (offsetAddr << 1);*/ /* 通过EPLD间接访问FPGA,FPGA的偏移地址需要乘以2. */
    ret = Bsp_CE1TAN8_FPGA_Write(subcardId, offsetAddr, value);
    if (DRV_AT_SUCCESS != ret)
    {
        DRV_AT_E1_PRINT(DRV_AT_ERROR_PRINT,"ERROR: %s line %d, FPGA WRITE FAILED !!\n", __FILE__, __LINE__); 
        return DRV_AT_FPGA_WRITE_FAILED; 
    }
    
    return DRV_AT_SUCCESS;
}


WORD32 Bsp_CE1TAN8_FPGA_Read(WORD16 subcardId, WORD16 address, WORD16 *readData)
{
    WORD32 dwRet = BSP_E_CE1TAN_OK;
    WORD16 i = 0;
	WORD16 data = 0;
    
    if((subcardId > CE1TAN_DEV_MAX)&&(subcardId < CE1TAN_DEV_MIN))
    {
        return BSP_E_CE1TAN_PARA ;
    }
    if (address > 0xffff)
    {
        BSP_ERROR_PRINT("dwAddr is lager than 16bits!\n");        
        return BSP_E_CE1TAN_PARA;        
    }
    if (NULL == readData)
    {
        return BSP_E_CE1TAN_PARA;
    }
    /*Read FPGA addr 16 bit nees << 1 bit "Don't use addr bit 0*/
    WORD16 realAddress = address;
    
    /* Wait for bit 0 return to 0 */
    for(i = 0;i < BSP_CE1TAN_FPGA_TIMEOUT; i++)
    {
        data = 0;
        Bsp_CE1TAN_EPLD_Read(subcardId,BSP_CE1TAN_FPGA_RW_STATUS,&data);
        if (0 == (data & BSP_CE1TAN_REG_BIT0))
        {
            break;
        }
    }
    if(i >= BSP_CE1TAN_FPGA_TIMEOUT)
    {
        return BSP_E_CE1TAN_EPLD_BUSY;
    }

    /* Write FPGA address */
    Bsp_CE1TAN_EPLD_Write(subcardId,BSP_CE1TAN_FPGA_ADDR_CTRL, realAddress);
    
    /* Clear register control read/write */
    Bsp_CE1TAN_EPLD_Write(subcardId,BSP_CE1TAN_FPGA_CHIP_SEL_CTRL, 0x7);
    
    /* Create signal for read sequence */
    Bsp_CE1TAN_EPLD_Write(subcardId,BSP_CE1TAN_FPGA_CHIP_SEL_CTRL, 0x1);

    /* Wait for bit 0 return to 0 */
    for(i = 0;i < BSP_CE1TAN_FPGA_TIMEOUT; i++)
    {
        data = 0;
        Bsp_CE1TAN_EPLD_Read(subcardId,BSP_CE1TAN_FPGA_RW_STATUS,&data);
        if (0 == (data & BSP_CE1TAN_REG_BIT0))
        {
            break;
        }
    }
    if(i >= BSP_CE1TAN_FPGA_TIMEOUT)
    {
        return BSP_E_CE1TAN_FPGA_TIMEOUT;
    }
    
    /* Read data from FPGA */
    data = 0;
    Bsp_CE1TAN_EPLD_Read(subcardId,BSP_CE1TAN_FPGA_DATA_READ,&data);
    *readData = data ;
	
    /* Clear register control read/write */
    Bsp_CE1TAN_EPLD_Write(subcardId,BSP_CE1TAN_FPGA_CHIP_SEL_CTRL,0x7);
    
    return dwRet;
}



WORD32 Bsp_CE1TAN8_FPGA_Write(WORD16 subcardId,WORD16 address, WORD16 writeData)
{
    WORD32 dwRet = BSP_E_CE1TAN_OK;
    WORD16 i = 0;
    WORD16 data = 0;

    if((subcardId > CE1TAN_DEV_MAX)&&(subcardId < CE1TAN_DEV_MIN))
    {
        return BSP_E_CE1TAN_PARA ;
    }
	
    if (address > 0XFFFF)
    {
      	BSP_ERROR_PRINT("dwAddr is lager than 16bits!\n");        
      	return BSP_E_CE1TAN_PARA;        
    }
    /*Read FPGA addr 16 bit nees << 1 bit "Don't use addr bit 0*/
    WORD16 realAddress = address;
	
    /* Wait for bit 0 return to 0 */
    for(i = 0;i < BSP_CE1TAN_FPGA_TIMEOUT; i++)
    {
        data = 0;
        Bsp_CE1TAN_EPLD_Read(subcardId,BSP_CE1TAN_FPGA_RW_STATUS,&data);
        if (0 == (data & BSP_CE1TAN_REG_BIT0))
        {
            break;
        }
    }
    if(i >= BSP_CE1TAN_FPGA_TIMEOUT)
    {
        return BSP_E_CE1TAN_EPLD_BUSY;
    }

    /* Write FPGA address  */
    Bsp_CE1TAN_EPLD_Write(subcardId,BSP_CE1TAN_FPGA_ADDR_CTRL, realAddress);
    /* Write data to FPGA */
    Bsp_CE1TAN_EPLD_Write(subcardId,BSP_CE1TAN_FPGA_DATA_WRITE, writeData);
    /* Clear register control read/write */
    Bsp_CE1TAN_EPLD_Write(subcardId,BSP_CE1TAN_FPGA_CHIP_SEL_CTRL, 0x7);
    /* Create signal for write sequence */
    Bsp_CE1TAN_EPLD_Write(subcardId,BSP_CE1TAN_FPGA_CHIP_SEL_CTRL, 0x2);
    /* Wait for bit 0 return to 0 */
    for(i = 0;i < BSP_CE1TAN_FPGA_TIMEOUT; i++)
    {
        data = 0;
        Bsp_CE1TAN_EPLD_Read(subcardId,BSP_CE1TAN_FPGA_RW_STATUS,&data);
        if (0 == (data & BSP_CE1TAN_REG_BIT0))
        {
            break;
        }
    }
    if(i >= BSP_CE1TAN_FPGA_TIMEOUT)
    {
        return BSP_E_CE1TAN_FPGA_TIMEOUT;
    }

    /* Clear register control read/write */
    Bsp_CE1TAN_EPLD_Write(subcardId,BSP_CE1TAN_FPGA_CHIP_SEL_CTRL, 0x7);

    /* Write done */
    return dwRet;
}



WORD32 Bsp_CE1TAN_EPLD_Read(WORD16 subcardId, WORD32 dwAddr, WORD16 *pwData )
{
    WORD32 ret = BSP_OK;
    WORD16 wData = 0;
    WORD16 offsetAddr = 0;
    BYTE *pucAddr = NULL;
    struct Device *ptDev = NULL;
    T_CE1TAN_PRIVATE *ptPriv = NULL;
    
    if((subcardId > CE1TAN_DEV_MAX)&&(subcardId < CE1TAN_DEV_MIN))
    {
        return BSP_E_CE1TAN_PARA ;
    }
    ptDev = CE1TAN_Dev(subcardId);
    BSP_ASSERT(ptDev);
    if (NULL == ptDev)
    {
        BSP_ERROR_PRINT("ptDev is Null!\n");
        return BSP_ERROR;
    }
    
    ptPriv = CE1TAN_Priv(ptDev);  
    /* Parameter Checking */
    if (NULL == ptPriv)
    {
        BSP_ASSERT(FALSE);
        return BSP_ERROR;
    }
    offsetAddr = (CE1_TURE == g_is15k8)?(dwAddr << 1): dwAddr ;   /*15k-8做偏移,15K-2不需要偏移*/
    if (dwAddr > 0xffff)
    {
        BSP_ERROR_PRINT("dwAddr is lager than 16bits!\n");        
        return BSP_ERROR;        
    }
    pucAddr = (BYTE *)ptPriv->pvBaseAddr + offsetAddr; 
    BSP_REG_READ_WORD16(pucAddr, wData);
    /*wData = BSP_SWAP_WORD16(wData);*/
    *pwData = wData;	
    
    return ret;
}


WORD32 Bsp_CE1TAN_EPLD_Write(WORD16 subcardId, WORD32 dwAddr, WORD16 wData )
{
    WORD32 ret = BSP_OK;
    WORD16 offsetAddr = 0;
    BYTE *pucAddr = NULL;
    struct Device *ptDev = NULL;
    T_CE1TAN_PRIVATE *ptPriv = NULL;
    
    if((subcardId > CE1TAN_DEV_MAX)&&(subcardId < CE1TAN_DEV_MIN))
    {
        return BSP_E_CE1TAN_PARA ;
    }
	
    ptDev = CE1TAN_Dev(subcardId);
    BSP_ASSERT(ptDev);
    if (NULL == ptDev)
    {
        BSP_ERROR_PRINT("ptDev is Null!\n");
        return BSP_ERROR;
    }
    
    ptPriv = CE1TAN_Priv(ptDev);  
    /* Parameter Checking */
    if (NULL == ptPriv)
    {
        BSP_ASSERT(FALSE);
        return BSP_ERROR;
    }
    offsetAddr = (CE1_TURE == g_is15k8)?(dwAddr << 1): dwAddr ;  /*15k-8做偏移,15K-2不需要偏移*/
    if (dwAddr > 0XFFFF)
    {
        BSP_ERROR_PRINT("dwAddr is lager than 16bits !\n");        
        return BSP_ERROR;        
    }
    
    /*wData = BSP_SWAP_WORD16(wData);*/
    pucAddr = (BYTE *)ptPriv->pvBaseAddr + offsetAddr; 
    BSP_REG_WRITE_WORD16(pucAddr, wData);	
    
    return ret;
}



#define BSP_REG_READ_WORD16(addr,val)  \
    do{\
       BSP_ASSERT(NULL != (VOID *)(addr));\
       val = (WORD16)(*(volatile WORD16*)(addr));\
    }while(0)
    
#define BSP_REG_WRITE_WORD16(addr,val) \
    do{\
       BSP_ASSERT(NULL != (VOID *)(addr)); \
       *(volatile WORD16*)(addr) = (val) & 0xFFFFL; \
       BSP_MEM_BA;\
    }while(0)
    
    
    
#define BSP_CE1TAN_REG_BIT0          0x00000001L

/* This register is used to get FPGA read/write status
 * Bit 0 is 1: busy
 * Bit 0 is 0: ready */
#define BSP_CE1TAN_FPGA_RW_STATUS 		0x1508

/* This register is used to control chip select signal
 * Write 7 to clear
 * Write 1 to create read signal
 * Write 2 to create write signal */
#define BSP_CE1TAN_FPGA_CHIP_SEL_CTRL  0x1502

/* This register is used to configure FPGA address which need to access
 * [15:0]: 16 bit */
#define BSP_CE1TAN_FPGA_ADDR_CTRL		0x1504

/* This register is used to get data from register in FPGA
 * [15:0]: 16 bit*/
#define BSP_CE1TAN_FPGA_DATA_READ  	0x150a

/* This register is used to write data to register in FPGA */
#define BSP_CE1TAN_FPGA_DATA_WRITE  	0x1506

/* This register is used to configure FPGA read/write time out
 * [3..0]: 4 bit */
#define BSP_CE1TAN_FPGA_RW_TIMEOUT	0x1500
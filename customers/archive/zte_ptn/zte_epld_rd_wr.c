#define BSP_REG_READ_WORD16(addr,val)  \
    do{\
       BSP_ASSERT(NULL != (VOID *)(addr));\
       val = (WORD16)(*(volatile WORD16*)(addr));\
    }while(0)
	

	
#define BSP_REG_WRITE_WORD16(addr,val) \
    do{\
       BSP_ASSERT(NULL != (VOID *)(addr)); \
       *(volatile WORD16*)(addr) = (val) & 0xFFFFL; \
       BSP_MEM_BA;\
    }while(0)


	

/* dwSlot: boardId for STM-1 board;  
   dwAddr: offset address of EPLD;
   pwData: to save EPLD register's value.*/
unsigned int BSP_CP3BANEpldRead16(unsigned int dwSlot, unsigned short dwAddr, unsigned short *pwData)
{
    WORD16 wData = 0;
    BYTE *pucAddr = NULL;
	struct Device *ptDev = NULL;
    T_CP3BAN_PRIVATE *ptPriv = NULL;

    ptDev = CP3BAN_GET_DEV(dwSlot);
    BSP_ASSERT(ptDev);
    if (NULL == ptDev)
    {
        BSP_ERROR_PRINT("ptDev is Null!\n");
        return BSP_E_CP3BAN_PARA;
    }

    ptPriv = CP3BAN_PRIV(ptDev);  
    /* Parameter Checking */
    if (NULL == ptPriv)
    {
        BSP_ASSERT(FALSE);
        return BSP_E_CP3BAN_PARA;
    }

    if (dwAddr > BSP_CP3BAN_WORD16_MAX_VALUE)
    {
        BSP_ERROR_PRINT("dwAddr is lager than 16bits!\n");        
        return BSP_E_CP3BAN_PARA;        
    }
    pucAddr = (BYTE *)ptPriv->pvBaseAddr + dwAddr; 
    BSP_REG_READ_WORD16(pucAddr, wData);
	wData = BSP_SWAP_WORD16(wData);
    *pwData = wData;
	
    return BSP_E_CP3BAN_OK;
}


/* dwSlot: boardId for STM-1 board;  
   dwAddr: offset address of EPLD;
   wData: data is writed in EPLD.*/
unsigned int BSP_CP3BANEpldWrite16(unsigned int dwSlot, unsigned short dwAddr, unsigned short wData)
{
    BYTE *pucAddr = NULL;
	struct Device *ptDev = NULL;
    T_CP3BAN_PRIVATE *ptPriv = NULL;

    ptDev = CP3BAN_GET_DEV(dwSlot);
    BSP_ASSERT(ptDev);
    if (NULL == ptDev)
    {
        BSP_ERROR_PRINT("ptDev is Null!\n");
        return BSP_E_CP3BAN_PARA;
    }

    ptPriv = CP3BAN_PRIV(ptDev);  
    /* Parameter Checking */
    if (NULL == ptPriv)
    {
        BSP_ASSERT(FALSE);
        return BSP_E_CP3BAN_PARA;
    }

    if (dwAddr > BSP_CP3BAN_WORD16_MAX_VALUE)
    {
        BSP_ERROR_PRINT("dwAddr is lager than 16bits !\n");        
        return BSP_E_CP3BAN_PARA;        
    }

    wData = BSP_SWAP_WORD16(wData);
    pucAddr = (BYTE *)ptPriv->pvBaseAddr + dwAddr; 
    BSP_REG_WRITE_WORD16(pucAddr, wData);

    return BSP_E_CP3BAN_OK;
}


/* dwSlot: boardId for STM-1 board;  
   wAddr: offset address of FPGA;
   pwData: to save FPGA register's value.*/
unsigned int BSP_CP3BANFpgaRead16(unsigned int dwSlot, unsigned short wAddr, unsigned short *pwData)
{
    WORD16 wData                = 0;
    WORD32 i                    = 0;

    if (wAddr > BSP_CP3BAN_WORD16_MAX_VALUE)  /* BSP_CP3BAN_WORD16_MAX_VALUE==0xffff*/
    {
        BSP_ERROR_PRINT("dwAddr is lager than 16bits!\n");        
        return BSP_E_CP3BAN_PARA;        
    }
    if (NULL == pwData)
    {
        return BSP_E_CP3BAN_NULL_POINTER;
    }

    /* 1, write FPGA's offset address in EPLD. */
    BSP_CP3BANEpldWrite16(dwSlot, (BSP_CP3BAN_FPGA_ADDR_REG << 1), wAddr);
    /* 2, enable read flag */
    wData = 0;
    BSP_CP3BANEpldRead16(dwSlot, (BSP_CP3BAN_FPGA_RWENABLE_REG << 1), &wData);
    wData = wData | ((WORD16)0x1);
    BSP_CP3BANEpldWrite16(dwSlot, (BSP_CP3BAN_FPGA_RWENABLE_REG << 1), wData);
    /* 3, trigger read. */
    wData = 0;
    BSP_CP3BANEpldRead16(dwSlot, (BSP_CP3BAN_FPGA_TRIGGER_REG << 1), &wData);
    wData = wData & ((WORD16)0xFFFE);
    BSP_CP3BANEpldWrite16(dwSlot, (BSP_CP3BAN_FPGA_TRIGGER_REG << 1), wData);
    wData = 0;
    BSP_CP3BANEpldRead16(dwSlot, (BSP_CP3BAN_FPGA_TRIGGER_REG << 1), &wData);
    wData |= ((WORD16)0x1);
    BSP_CP3BANEpldWrite16(dwSlot, (BSP_CP3BAN_FPGA_TRIGGER_REG << 1), wData);
    
    /* 4, wait ACK */
    for (i = 0; i < BSP_CP3BAN_FPGA_TIMEOUT; i++) /* BSP_CP3BAN_FPGA_TIMEOUT==10 */
    {
        wData = 0;
        BSP_CP3BANEpldRead16(dwSlot, (BSP_CP3BAN_FPGA_RWENABLE_REG << 1), &wData);
        if (0 != (wData & BSP_CP3BAN_FPGA_ACK_STATUS))
        {
            break;
        }
        else
        {
            BSP_DelayUs(1);  /* delay 1us */
            wData = 0;
            BSP_CP3BANEpldRead16(dwSlot, (BSP_CP3BAN_FPGA_ACCESS_TIMEOUT << 1), &wData);
            if (((WORD16)0x000D) == (wData & ((WORD16)0x000F))) /* FPGA no ACK */
            {
                /* clear reading flag */ 
                wData = 0;
                BSP_CP3BANEpldRead16(dwSlot, (BSP_CP3BAN_FPGA_ACK_REG << 1), &wData);
                wData = wData & ((WORD16)0xFFFE);
                BSP_CP3BANEpldWrite16(dwSlot, (BSP_CP3BAN_FPGA_ACK_REG << 1), wData);
                wData = 0;
                BSP_CP3BANEpldRead16(dwSlot, (BSP_CP3BAN_FPGA_ACK_REG << 1), &wData);
                wData = wData | ((WORD16)0x1);
                BSP_CP3BANEpldWrite16(dwSlot, (BSP_CP3BAN_FPGA_ACK_REG << 1), wData);
                return BSP_E_CP3BAN_FPGA_TIMEOUT;
            }
        }
    }
    
    if (i >= BSP_CP3BAN_FPGA_TIMEOUT)  /* BSP_CP3BAN_FPGA_TIMEOUT==10 */
    {
        return BSP_E_CP3BAN_FPGA_ACR_TIMEOUT;
    }
    
    /* 5, read data for FPGA. */
    wData = 0;
    BSP_CP3BANEpldRead16(dwSlot, (BSP_CP3BAN_FPGA_DATA_READ_REG << 1), &wData);
    *pwData = wData;

	/* 6, clear reading flag */
    wData = 0;
    BSP_CP3BANEpldRead16(dwSlot, (BSP_CP3BAN_FPGA_ACK_REG << 1), &wData);
    wData = wData & ((WORD16)0xFFFE);
    BSP_CP3BANEpldWrite16(dwSlot, (BSP_CP3BAN_FPGA_ACK_REG << 1), wData);
    wData = 0;
    BSP_CP3BANEpldRead16(dwSlot, (BSP_CP3BAN_FPGA_ACK_REG << 1), &wData);
    wData = wData | ((WORD16)0x1);
    BSP_CP3BANEpldWrite16(dwSlot, (BSP_CP3BAN_FPGA_ACK_REG << 1), wData);
    
    return BSP_E_CP3BAN_OK;
}


/* dwSlot: boardId for STM-1 board;  
   wAddr: offset address of FPGA;
   wData: data is writed to FPGA.*/
unsigned int BSP_CP3BANFpgaWrite16(unsigned int dwSlot, unsigned short wAddr, unsigned short wData)
{
    WORD16 wRegData             = 0;
    WORD32 i                    = 0;

    if (wAddr > BSP_CP3BAN_WORD16_MAX_VALUE)
    {
        BSP_ERROR_PRINT("dwAddr is lager than 16bits!\n");        
        return BSP_E_CP3BAN_PARA;        
    }
    
    /* 1, write FPGA's offset address in EPLD. */
    BSP_CP3BANEpldWrite16(dwSlot, (BSP_CP3BAN_FPGA_ADDR_REG << 1), wAddr);
    /* 2, write FPGA's data in EPLD */
    BSP_CP3BANEpldWrite16(dwSlot, (BSP_CP3BAN_FPGA_DATA_WRITE_REG << 1), wData);
    /* 3, enable write flag */
    wRegData = 0;
    BSP_CP3BANEpldRead16(dwSlot, (BSP_CP3BAN_FPGA_RWENABLE_REG << 1), &wRegData);
    wRegData = wRegData & ((WORD16)0xFFFE);
    BSP_CP3BANEpldWrite16(dwSlot, (BSP_CP3BAN_FPGA_RWENABLE_REG << 1), wRegData);
    /* 4, trigger write. */
    wRegData = 0;
    BSP_CP3BANEpldRead16(dwSlot, (BSP_CP3BAN_FPGA_TRIGGER_REG << 1), &wRegData);
    wRegData = wRegData & ((WORD16)0xFFFE);
    BSP_CP3BANEpldWrite16(dwSlot, (BSP_CP3BAN_FPGA_TRIGGER_REG << 1), wRegData);
    wRegData = 0;
    BSP_CP3BANEpldRead16(dwSlot, (BSP_CP3BAN_FPGA_TRIGGER_REG << 1), &wRegData);
    wRegData = wRegData | ((WORD16)0x1);
    BSP_CP3BANEpldWrite16(dwSlot, (BSP_CP3BAN_FPGA_TRIGGER_REG << 1), wRegData);
    /* 5, wait ack */
    for (i = 0; i < BSP_CP3BAN_FPGA_TIMEOUT; i++)
    {
        wRegData = 0;
        BSP_CP3BANEpldRead16(dwSlot, (BSP_CP3BAN_FPGA_RWENABLE_REG << 1), &wRegData);
        if (0 != (wRegData & BSP_CP3BAN_FPGA_ACK_STATUS))
        {
            break;
        }
		else
        {
            BSP_DelayUs(1);  /* delay 1us */
            wRegData = 0;
            BSP_CP3BANEpldRead16(dwSlot, (BSP_CP3BAN_FPGA_ACCESS_TIMEOUT << 1), &wRegData);
            if (((WORD16)0x000D) == (wRegData & ((WORD16)0x000F))) /* FPGA no ACK */
            {
                /* clear writing flag. */
                wRegData = 0;
                BSP_CP3BANEpldRead16(dwSlot, (BSP_CP3BAN_FPGA_ACK_REG << 1), &wRegData);
                wRegData = wRegData & ((WORD16)0xFFFE);
                BSP_CP3BANEpldWrite16(dwSlot, (BSP_CP3BAN_FPGA_ACK_REG << 1), wRegData);
                wRegData = 0;
                BSP_CP3BANEpldRead16(dwSlot, (BSP_CP3BAN_FPGA_ACK_REG << 1), &wRegData);
                wRegData = wRegData | ((WORD16)0x1);
                BSP_CP3BANEpldWrite16(dwSlot, (BSP_CP3BAN_FPGA_ACK_REG << 1), wRegData);
                return BSP_E_CP3BAN_FPGA_TIMEOUT;   
            }
        }
    }
    
    if (i >= BSP_CP3BAN_FPGA_TIMEOUT)
    {
        return BSP_E_CP3BAN_FPGA_ACR_TIMEOUT;
    }
    
    /* 6, clear writing flag.  */
    wRegData = 0;
    BSP_CP3BANEpldRead16(dwSlot, (BSP_CP3BAN_FPGA_ACK_REG << 1), &wRegData);
    wRegData = wRegData & ((WORD16)0xFFFE);
    BSP_CP3BANEpldWrite16(dwSlot, (BSP_CP3BAN_FPGA_ACK_REG << 1), wRegData);
    wRegData = 0;
    BSP_CP3BANEpldRead16(dwSlot, (BSP_CP3BAN_FPGA_ACK_REG << 1), &wRegData);
    wRegData = wRegData | ((WORD16)0x1);
    BSP_CP3BANEpldWrite16(dwSlot, (BSP_CP3BAN_FPGA_ACK_REG << 1), wRegData);
    
    return BSP_E_CP3BAN_OK;
}
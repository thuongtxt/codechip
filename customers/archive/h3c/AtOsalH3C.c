/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OSAL
 *
 * File        : AtOsalH3C.c
 *
 * Created Date: Aug 1, 2012
 *
 * Author      : namnn
 *
 * Description : OSAL - Concrete OSAL implementation for H3C platform
 *
 * Notes       :

*----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 199309
#endif

/* Modified by H3C */
#if (SYS_VERSION_DEBUG == 0)
    void *MEM_Malloc(unsigned long ulInfo,unsigned long ulSize);
    unsigned long MEM_Free_X(void **ppBuf);
    #define MEM_Free(pBuf) MEM_Free_X((void**)(&(pBuf)))
#else
    void *MEM_Malloc_X(unsigned long ulInfo, unsigned long ulSize, char *pcFileName,unsigned long  ulLine);
#define MEM_Malloc(ulInfo,ulSize) MEM_Malloc_X(ulInfo,ulSize,__FILE__,__LINE__)
    unsigned long MEM_Free_X(void **ppBuf,char *pcFileName,unsigned long ulLine);
#define MEM_Free(pBuf) MEM_Free_X((void**)(&(pBuf)),__FILE__,__LINE__)
#endif

#include "AtOsal.h"
#include <string.h>

#include <semaphore.h>
#include <sys/time.h>
#include <errno.h>

/*--------------------------- Define -----------------------------------------*/
#define NO_EINTR(stmt, _ret) while ((_ret = (stmt)) == -1 && (errno == EINTR));

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
/* Linux semaphore */
typedef struct tAtOsalLinuxSem
    {
    tAtOsalSem class;
    sem_t semaphore;
    AtOsal osal;
    }tAtOsalLinuxSem;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* This OSAL */
static tAtOsal m_osal;

/* OSAL Implementation data structures */
static tAtOsalMethods m_osalMethods;
static eBool m_osalMethodsInit = 0;

/* Semaphore Implementation data structures */
static tAtOsalSemMethods m_osalSemMethods;
static eBool m_osalSemMethodsInit = 0;

/*--------------------------- Forward declarations ---------------------------*/
extern void TASK_Delay(unsigned long ulMillSecs);
extern unsigned long TIME_Now(unsigned long *pulRetTimeInMillSecsHigh, unsigned long *pulRetTimeInMillSecsLow);

/*--------------------------- Implementation ---------------------------------*/
/* Memory operations */
static void* MemAlloc(AtOsal self, uint32 size)
    {
    return MEM_Malloc(0xcc21550b, size);
    }

static void MemFree(AtOsal self, void* pMem)
    {
    MEM_Free(pMem);
    }

static void* MemInit(AtOsal self, void* pMem, uint32 val, uint32 size)
    {
    return memset(pMem, val, size);
    }

static void* MemCpy(AtOsal self, void *pDest, const void *pSrc, uint32 size)
    {
    return memcpy(pDest, pSrc, size);
    }

static int MemCmp(AtOsal self, void *pMem1, void *pMem2, uint32 size)
    {
    return memcmp(pMem1, pMem2, size);
    }

static eAtOsalRet SemDelete(AtOsalSem self)
    {
    tAtOsalLinuxSem *sem = (tAtOsalLinuxSem *)self;
    eAtOsalRet ret = (sem_destroy(&(sem->semaphore)) == 0) ? cAtOsalOk : cAtOsalFailed;
    AtOsalMemFree(self);

    return ret;
    }

static eAtOsalRet SemTake(AtOsalSem sem)
    {
    int ret;

    NO_EINTR(sem_wait(&(((tAtOsalLinuxSem *)sem)->semaphore)), ret);
    return ret ? cAtOsalFailed : cAtOsalOk;
    }

static eAtOsalRet SemTryTake(AtOsalSem sem)
    {
    int ret;

    NO_EINTR(sem_trywait(&(((tAtOsalLinuxSem *)sem)->semaphore)), ret);
    return (ret ?  cAtOsalFailed :  cAtOsalOk);
    }

static eAtOsalRet SemGive(AtOsalSem sem)
    {
    return (sem_post(&(((tAtOsalLinuxSem *)sem)->semaphore)) == 0) ? cAtOsalOk : cAtOsalFailed;
    }

static AtOsal OsalGet(AtOsalSem sem)
    {
    return ((tAtOsalLinuxSem *)sem)->osal;
    }

/* Initialize a semaphore object */
static void SemObjectInitialize(AtOsalSem self, AtOsal osal)
    {
    tAtOsalSem *semIntf = (tAtOsalSem *)self;

    /* Initialize interface */
    if (!m_osalSemMethodsInit)
        {
        m_osalSemMethods.Delete  = SemDelete;
        m_osalSemMethods.Take    = SemTake;
        m_osalSemMethods.TryTake = SemTryTake;
        m_osalSemMethods.Give    = SemGive;
        m_osalSemMethods.OsalGet = OsalGet;
        m_osalSemMethodsInit     = 1;
        }
    semIntf->methods = &m_osalSemMethods;

    /* Add a reference to OSAL */
    ((tAtOsalLinuxSem *)self)->osal = osal;
    }

/* Semaphore */
static AtOsalSem SemCreate(AtOsal self, eBool procEn, uint32 initVal)
    {
    tAtOsalLinuxSem *semaphore;
    uint16 size;

    /* Create semaphore object */
    size = sizeof(tAtOsalLinuxSem);
    semaphore = AtOsalMemAlloc(size);
    AtOsalMemInit(semaphore, 0, size);
    if (sem_init(&(semaphore->semaphore), procEn, initVal) != 0)
        return NULL;

    /* Initialize this semaphore object */
    SemObjectInitialize((AtOsalSem)semaphore, self);

    return (AtOsalSem)semaphore;
    }

/* Delay functions */
static void USleep(AtOsal self, uint32 timeToSleep)
    {
    uint32 ms = timeToSleep / 1000;

    if (ms == 0)
        ms = 1;

    TASK_Delay(ms);
    }

static void Sleep(AtOsal self, uint32 timeToSleep)
    {
    USleep(self, timeToSleep * 1000000);
    }

static void CurTimeGet(AtOsal self, tAtOsalCurTime *currentTime)
    {
    uint32 timeInMillSecsHigh;
    uint32 timeInMillSecsLow;

    /* Get current time */
    if (TIME_Now(&timeInMillSecsHigh, &timeInMillSecsLow) != 0)
        return;

    /* Just save this time information. The field sec and usec are used for easy
     * calculating difference time. They are not used for other purpose.
     * In H3C OS, current time information is retrieved differently from Linux,
     * with this return, it is not good to convert to sec and usec, not enough
     * bit. So, just save 32MSB to sec field and 32LSB to usec field.
     * Now, sec/usec fields do not mean second and microsecond anymore.
     *
     * By the way, time subtracting is now different among OSs, so new method
     * AtOsalDifferenceTimeInMs is defined to polymorphism this calculating. */

    /* Now, save this return */
    currentTime->sec  = timeInMillSecsHigh;
    currentTime->usec = timeInMillSecsLow;
    }

/*
   Idea. The following explanation assumes that time2 >= time1

   uint32 timeInMillSecsHigh1;
   uint32 timeInMillSecsLow1;
   uint32 timeInMillSecsHigh2;
   uint32 timeInMillSecsLow2;

   time1Ms = (timeInMillSecsHigh1 * 4294967296) + timeInMillSecsLow1
   time2Ms = (timeInMillSecsHigh2 * 4294967296) + timeInMillSecsLow2
   diffTimeHi = (timeInMillSecsHigh2 - timeInMillSecsHigh1)

   diffMs = time2Ms - time1Ms = (timeInMillSecsHigh2 * 4294967296) - (timeInMillSecsHigh1 * 4294967296) + timeInMillSecsLow2 - timeInMillSecsLow1
   diffMs = (4294967296 * diffTimeHi) + timeInMillSecsLow2 - timeInMillSecsLow1

   In 32-bit CPU, (4294967296 * diffTimeHi) is overflow when diffTimeHi >= 1.
   Normal, software does not delay to much to make diffTimeHi > 1 and 1 is
   also the big value in this case. So, to avoid overflow, we have
   4294967296 * diffTimeHi = (4294967295 * diffTimeHi) + diffTimeHi

   Then,
   diffMs = 4294967295 * diffTimeHi + timeInMillSecsLow2 - timeInMillSecsLow1 + diffTimeHi
   diffMs = 0xFFFFFFFF * diffTimeHi + timeInMillSecsLow2 - timeInMillSecsLow1 + diffTimeHi

   And for performance, it can be changed like this:
   if (diffTimeHi == 0)
       diffMs = timeInMillSecsLow2 - timeInMillSecsLow1;
   else
       diffMs = 0xFFFFFFFF + timeInMillSecsLow2 - timeInMillSecsLow1 + 1
 */
static uint32 DifferenceTimeInMs(AtOsal self, const tAtOsalCurTime *currentTime, const tAtOsalCurTime *previousTime)
    {
    uint32 diffMs = currentTime->sec - previousTime->sec;

    if (diffMs == 0)
        return currentTime->usec - previousTime->usec;
    else
        return 0xFFFFFFFF + currentTime->usec - previousTime->usec + 1;
    }

/**
 * Get H3C OSAL implementation
 *
 * @return H3C OSAL
 */
AtOsal AtOsalH3C()
    {
    /* Initialize implementation */
    if (!m_osalMethodsInit)
        {
        /* Memory operations */
        m_osalMethods.MemAlloc = MemAlloc;
        m_osalMethods.MemFree  = MemFree;
        m_osalMethods.MemInit  = MemInit;
        m_osalMethods.MemCpy   = MemCpy;
        m_osalMethods.MemCmp   = MemCmp;

        /* Semaphore */
        m_osalMethods.SemCreate = SemCreate;

        /* Delay functions */
        m_osalMethods.Sleep              = Sleep;
        m_osalMethods.USleep             = USleep;
        m_osalMethods.CurTimeGet         = CurTimeGet;
        m_osalMethods.DifferenceTimeInMs = DifferenceTimeInMs;

        m_osal.methods = &m_osalMethods;

        m_osalMethodsInit = 1;
        }

    /* Return this OSAL */
    return &m_osal;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APP
 * 
 * File        : AtFhnDriver.h
 * 
 * Created Date: Jun 21, 2013
 *
 * Description : Device management for FHN project
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATFHNDRIVER_H_
#define _ATFHNDRIVER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDriver.h"
#include "AtFhnSlot.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Driver creating/deleting */
AtDriver AtFhnDriverCreate(uint8 numSlots);
void AtFhnDriverDelete();

/* Slot management */
eAtRet AtFhnDriverSlotCreate(uint8 slotId);
eAtRet AtFhnDriverSlotDelete(uint8 slotId);
AtFhnSlot AtFhnDriverSlotGet(uint8 slotId);

#ifdef __cplusplus
}
#endif
#endif /* _ATFHNDRIVER_H_ */


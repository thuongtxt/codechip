/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : AtFhnSample.c
 *
 * Created Date: Jun 17, 2013
 *
 * Description : Sample code for FHN project
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <string.h>
#include "AtFhnObjectAccess.h"
#include "AtFhnDriver.h"

/*--------------------------- Define -----------------------------------------*/
/* The defined length of TTI message */
#define cAtSdhTti64LenFhn          64
#define cAtSdhTti16LenFhn          16
#define cAtSdhTti1LenFhn           1
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef enum eAtEthFlowTypeFhn
    {
    cAtEthFlowNpoPppFhn,
    cAtEthFlowEthoPppFhn
    }eAtEthFlowTypeFhn;
/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/**
 * Bind Ethernet flow to Link
 *
 * flowId range:    [1-128]
 * channelId range: [1-128]
 */
int Udr_ce1_port_channel_bind_flow_samplecode(uint8 slotId, uint32 encapId, uint32 flowId)
    {
    return (AtHdlcLinkFlowBind(AtFhnHdlcLinkGet(slotId, encapId), AtFhnEthFlowGet(slotId, flowId)) == cAtOk) ? 0 : -1;
    }

/**
 * Unbind Ethernet flow from Link
 *
 * channelId range: [1-128]
 */
int Udr_ce1_port_channel_unbind_flow_samplecode(uint8 slotId, uint32 channelId)
    {
    return (AtHdlcLinkFlowBind(AtFhnHdlcLinkGet(slotId, channelId), NULL) == cAtOk) ? 0 : -1;
    }

/**
 * Create Ethernet flow
 * flowId range:    [1-128]
 * flowType:
 *   cAtEthFlowNpoPppFhn,
 *   cAtEthFlowEthoPppFhn
 */

int Udr_ce1_port_flow_create_samplecode(uint8 slotId, uint32 flowId, eAtEthFlowTypeFhn flowType)
    {
    AtEthFlow flow;

    if (flowType == cAtEthFlowNpoPppFhn)
        flow = AtFhnNopFlowCreate(slotId, flowId);
    else
        flow = AtFhnEopFlowCreate(slotId, flowId);

    return flow ? 0 : -1;
    }

/**
 *  Map vc1x to de1
 */
int Udr_cpos_port_stm1_sdh_map_vc1x_to_de1_samplecode(uint8 slotId, uint8 lineId, uint8 augId, uint8 tu3Id, uint8 tug2Id, uint8 tuId)
    {
    AtSdhVc vc1x = AtFhnVc1xGet(slotId, lineId, augId, tu3Id, tug2Id, tuId);
    return (AtSdhChannelMapTypeSet((AtSdhChannel)vc1x, cAtSdhVcMapTypeVc1xMapDe1) == cAtOk) ? 0 : -1;
    }

static uint8 MsgLengthFromTtiMode(eAtSdhTtiMode ttiMode)
    {
    if (ttiMode == cAtSdhTtiMode1Byte) return 1;
    if (ttiMode == cAtSdhTtiMode16Byte) return 16;
    return 64;
    }

/**
 * Set Tx TTI for VC1x
 *
 * @param deviceId
 * @param lineId
 * @param augId
 * @param tu3Id
 * @param tug2Id
 * @param tuId
 * @param ttiMode
 * @param message
 * @param paddingMode: 0 - null padding; other - space padding
 * @return
 */
int Udr_cpos_port_path_tti_tx_set_samplecode(uint8 slotId,
                                             uint8 lineId,
                                             uint8 augId,
                                             uint8 tu3Id,
                                             uint8 tug2Id,
                                             uint8 tuId,
                                             eAtSdhTtiMode ttiMode,
                                             uint8 *message,
                                             uint8 paddingMode)
    {
    uint8       ttiMsgBuf[64], i;
    tAtSdhTti   tti;

    uint8       ttiMsgLen = MsgLengthFromTtiMode(ttiMode);
    AtSdhVc     vc1x = AtFhnVc1xGet(slotId, lineId, augId, tu3Id, tug2Id, tuId);
    char        padChar = (paddingMode == 0) ? '\0' : ' ';

    /* Build TTI message */
    uint8 inputMsgLen = strlen((const char *)message);
    if (inputMsgLen > 64)
        return 0;

    /* Copy TTI message to buffer */
    AtOsalMemCpy((void *)ttiMsgBuf, (void *)message, inputMsgLen);

    /* Padding */
    for(i = inputMsgLen; i < 64; i++)
        ttiMsgBuf[i] = padChar;

    AtSdhTtiMake(ttiMode, ttiMsgBuf, ttiMsgLen, &tti);
    return (AtSdhChannelTxTtiSet((AtSdhChannel)vc1x, &tti) == cAtOk) ? 0 : - 1;
    }

/* The following sample code is to show how to create/delete slots */
int Udr_cpos_manage_device_samplecode(void)
    {
    static const uint8 cNumSlots = 2;
    static const uint8 cSlot1 = 1;
    static const uint8 cSlot2 = 2;

    /* Create the driver in the beginning */
    if (AtFhnDriverCreate(cNumSlots) == NULL)
        return cAtErrorNullPointer;

    /* Check and see that E1 card is in slot 1 and STM card is in slot 2 */
    AtFhnDriverSlotCreate(cSlot1);
    AtFhnDriverSlotCreate(cSlot2);

    /* Create Ethernet flow in E1 card */
    Udr_ce1_port_flow_create_samplecode(cSlot1, 1, cAtEthFlowNpoPppFhn);

    /* Create Ethernet flow in STM card */
    Udr_ce1_port_flow_create_samplecode(cSlot2, 1, cAtEthFlowNpoPppFhn);

    /* Plug out E1 card from slot 1 */
    AtFhnDriverSlotDelete(cSlot1);

    /* Plug in second STM card to slot 1 */
    AtFhnDriverSlotCreate(cSlot1);

    /* Plug out both cards */
    AtFhnDriverSlotDelete(cSlot1);
    AtFhnDriverSlotDelete(cSlot2);

    return cAtOk;
    }

/*
	LIU IDT82V2058 

	designed by ssq 2013.03.28
 
*/


#define Uint8T unsigned char 
#define Uint32T unsigned int


#define 	IDT82V2058_INIT_CHECK(act,  rtv,label)	\
		rtv = act;							\
		if(rtv!=OK)				\
			goto label;	


typedef enum
{
	UDR_POS_LOOPBACK_SET=0

	/*
.........

.........

........

.........

........
	*/

} udr_cmd_t_pos;



#define IDT82V2058_DEVICEID                 (Uint8T)0x10

/* device id reg read only  */
#define IDT82V2058_DEVICEID_REG		(Uint32T )(0x00000000)

/* analog loopback  */
#define IDT82V2058_ALB_REG		(Uint32T )(0x00000001)

/* remote loopback */
#define IDT82V2058_RLB_REG		(Uint32T )(0x00000002)

/* TAO confige tran all ones reg R/W*/
#define IDT82V2058_TAO_REG		(Uint32T )(0x00000003)

/* LOS status reg */
#define IDT82V2058_LOS_REG		(Uint32T )(0x00000004)

/* short circuit  */
#define IDT82V2058_SC_REG		(Uint32T )(0x00000005)

/* LOS interrupe mask */
#define IDT82V2058_LOSM_REG		(Uint32T )(0x00000006)

/* short circuit  interrupt mask*/
#define IDT82V2058_SCM_REG		(Uint32T )(0x00000007)

/* LOS interrupt Status reg */
#define IDT82V2058_LOSI_REG		(Uint32T )(0x00000008)

/* short circuit interrupt  */
#define IDT82V2058_SCI_REG		(Uint32T )(0x00000009)

/* software reset  */
#define IDT82V2058_RS_REG		(Uint32T )(0x0000000A)

/* perform monitor  */
#define IDT82V2058_PMON_REG		(Uint32T )(0x0000000B)

/* digital loopback */
#define IDT82V2058_DLB_REG		(Uint32T )(0x0000000C)

/*  LOS/AIS config G755/ETSI*/
#define IDT82V2058_LAC_REG		(Uint32T )(0x0000000D)

/* TAO confige tran all ones auto reg R/W*/
#define IDT82V2058_ATAO_REG		(Uint32T )(0x0000000E)

/* groble confige reg R/W*/
#define IDT82V2058_GCF_REG		(Uint32T )(0x0000000F)
/*
码型选择： 							0－HDB3   1－AMI
抖动衰减选择：						0－禁止   1－发送通道   2－接收通道
抖动衰减FIFO深度选择： 			0－32bits   1－64bits
抖动衰减FIFO中心频率选择： 		0－1.7HZ   1－6.6HZ
*/
typedef enum
{
	B8ZS_HDB3		=0,
	AMI 			= 0x1,
} codeTypeE;

typedef enum
{
	NO_USE					=0,
	JITTER_TX 				= 0x1,
	JITTER_FX				= 0x2,
} liuGrobleConfigE;

typedef enum
{
	FIFO_BITS32 	= 0,
	FIFO_BITS64	= 0x1
} jitterFifoLengthE;

typedef enum
{
	FIFO_FREQUENCE17 	= 0,
	FIFO_FREQUENCE66	= 0x1
} jitterCornerFrequenceE;


#define IDT82V2058_BASE_ADDRESS	(Uint32T )(0xD0000000)
#define IDT82V2058_CHIP_CS	 (Uint32T )(0x8000000)


/* output enable reg R/W */
#define IDT82V2058_OE_REG		(Uint32T )(0x00000012)

/* ais status reg read only */
#define IDT82V2058_AIS_REG		(Uint32T )(0x00000013)

/* ais interrupt mask reg R/W */
#define IDT82V2058_AISM_REG		(Uint32T )(0x00000014)

/* ais interrupt status reg read only */
#define IDT82V2058_AISI_REG		(Uint32T )(0x00000015)

#define IDT82V2058_ADDP_REG		(Uint32T )(0x0000001F)



typedef enum
{
    idt82V2058_FALSE = 0,
    idt82V2058_TRUE = 1
} idt82V2058BooleanE;

typedef enum
{
    idt82V2058_RX = 0,
    idt82V2058_TX = 1
} idt82V2058ModeE;



/*This mode is config in E1.  T1.231 Compliant LOS/AIS already used in T1*/
typedef enum
{
	ITU_G_775		= 0,
	ETSI_300_233 	= 0x1
} LosAisModeE;


typedef enum
{
	DISNABLE_LOOP = 0,
	ANALOG 	= 0x1,
	REMOTE		= 0x2,
	DIGITAL 	= 0x3
} loopBackModeE;


typedef enum
{
	DISNABLE_TAOS 	= 0,
	AUTO_TAOS		= 0x1,
	FORCE_TAOS		= 0x2
} taosModeE;

typedef struct
{
   Uint32T baseAdd;
    idt82V2058BooleanE initialised;
} idt82V2058ParamsS;

int idt82V2058ParamsInit(Uint8T slotID, Uint32T chipIndex);
int	csLosIntMaskSet(Uint32T chipIndex, Uint8T set);
int	csSoftwareReset(Uint32T chipIndex) ;
int	csEnableLosAisMode(Uint32T chipIndex, Uint8T enable);
int	csSetGlobalControl(Uint32T chipIndex, Uint8T set);
int	csSetLineLengthData(Uint32T chipIndex, Uint8T chSel, Uint8T set);
int	 idt82V2058Read(idt82V2058ParamsS* CS61884Params, Uint32T address, Uint8T *memValue);
int	 idt82V2058Write(idt82V2058ParamsS* CS61884Params, Uint32T address, Uint8T memValue);
void	cpldAisLedSet(Uint8T lightled);
void	m8416CpldAisLedSet(Uint32T lightled);
void	m8416CpldAisLedGet(Uint32T *lightled);
int	csAisDetectTask(void);



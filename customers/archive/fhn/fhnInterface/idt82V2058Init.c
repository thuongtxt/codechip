/******************************************************************************
*
*  File name:             idt82V2058Init.c
*
*  Version:                1.0
*
*  Author:                 SSQ
*
*  Date created:           28/03/2013
*
*             
*******************************************************************************/

/***********************   INCLUDE FILES   ************************************/

#include "semLib.h"
#include "idt82V2058Init.h"


/**********************   GLOBAL VARIABLES   **********************************/

SEM_ID CsAndCpldSem;

int idt82V2058InitFlag=0;

idt82V2058ParamsS idt82V2058Params[5]={0};

#define IDT_SLOT_NUMBLE  2
#define IDT_CHIP_NUM_PER_SLOT  2

Uint32T idtBaseAddress[IDT_SLOT_NUMBLE][IDT_CHIP_NUM_PER_SLOT]=
{
	0xD8000000,
	0xD0000000,
	0xDC000000,
	0xD4000000
};

Uint8T 			idt82V2058Num = 4;

#if	0
Uint8T idt82V2058RegMap[]={
	0x0,
	0x40,
	0x10,
	0x80,
	0x20,
	0x04,
	0x01,
	0x08,
	0x02,	

	0x40,
	0x10,
	0x80,
	0x20,
	0x04,
	0x01,
	0x08,
	0x02
};

#else
Uint8T idt82V2058RegMap[]={
	0x0,
	0x80,
	0x40,
	0x20,
	0x10,
	0x08,
	0x04,
	0x02,
	0x01,

	0x80,
	0x40,
	0x20,
	0x10,
	0x08,
	0x04,
	0x02,
	0x01,
};
#endif


Uint8T idt82V2058Regbuf[0x20];
const char *idt82V2058RegName[]={
	"Revision/IDcode Reg		",					/* 0x0 */
	"Analog Loopback Reg		",					/* 0x1 */
	"Remote Loopback Reg		",					/* 0x2 */
	"TAO Enable Reg			",						/* 0x3 */
	"LOS Status Reg			",						/* 0x4 */
	"SC Status Reg			",						/* 0x5 */
	"LOSM Interrupt mask		",				/* 0x6 */
	"SC Interrupt mask		",				/* 0x7 */
	"LOSI Interrupt Status		",				/* 0x8 */
	"SCI Interrupt Status		",				/* 0x9 */
	"RS Software Reset Reg		",					/* 0x0a */
	"Performance Monitor		",				/* 0x0b */
	"DLB Loopback config		",				/* 0xc */
	"LAC config Enable		",			/* 0xd */
	"ATAO Automatic TAO		",					/* 0xe */
	"GCF Global Control Reg		",					/* 0xf */
	"",
	"",
	"OE output enable Reg		",			/* 0x12 */
	"AIS Status Reg			",					/* 0x13 */
	"AIS Interrupt mask		",				/* 0x14 */
	"AIS Interrupt Status		",				/* 0x15 */
	"",										/* 0x16 */
	"",										/* 0x17 */
	"",										/* 0x18 */
	"",										/* 0x19 */
	"",										/* 0x1A */
	"",										/* 0x1B */
	"",										/* 0x1C */
	"",										/* 0x1D */
	"",										/* 0x1E */	
	"ADDP Address point control Reg	",			/* 0x1F */
};

 int idt82V2058Read(idt82V2058ParamsS* CS61884Params, Uint32T address, Uint8T *memValue);


/*******************   STATIC FUNCTION DEFINTIONS   ***************************/

/*****************   EXPORTED FUNCTION DEFINTIONS   ***************************/

/*******************************************************************************

 Function:
    idt82V2058init

 Description:

	slotId = [1,2]
 Remarks:

*******************************************************************************/
int idt82V2058init(Uint8T slotID){
	int status = OK;
	Uint8T chipIndex,chIndex,cslldreg=0;

	if(idt82V2058InitFlag == 0)
	{
		CsAndCpldSem = semCreate();
		semGive(CsAndCpldSem);
		idt82V2058InitFlag=1;
	}

	if(slotID<1||slotID>2)
	{
		printf("#%d error slot id =%d\r\n",__LINE__, slotID);
		return -1;
	}
	
	for(chipIndex=0;chipIndex<2;chipIndex++){
		memset(&(idt82V2058Params[2*(slotID-1)+chipIndex]), 0, sizeof(idt82V2058ParamsS));
		 idt82V2058ParamsInit(slotID, chipIndex);
		csSoftwareReset(2*(slotID-1)+chipIndex);
		taskDelay(60);
			
		/* csLosIntMaskSet(chipIndex, 0xff); */

		idt82V2058Params[2*(slotID-1)+chipIndex].initialised = idt82V2058_TRUE;
	}

	return(status);
}

/*
slotId = [1,2]
chipIndex = [0-1]

*/
int idt82V2058ParamsInit(Uint8T slotID, Uint32T chipIndex){
	int status = OK;
	Uint8T RevisionIDcode;

	idt82V2058Params[2*(slotID-1)+chipIndex].baseAdd = idtBaseAddress[slotID-1][chipIndex];

	status =  idt82V2058Read(&(idt82V2058Params[2*(slotID-1)+chipIndex]), IDT82V2058_DEVICEID_REG,&RevisionIDcode);

	if((status == OK) &&(RevisionIDcode == IDT82V2058_DEVICEID)){
		idt82V2058Params[2*(slotID-1)+chipIndex].initialised = ERROR;
	}else{
		status = ERROR;
		printf("slot[%d] chipIndex[%d ]RevisionIDcode = %x\r\n", slotID,chipIndex,RevisionIDcode);
		return(status);
	}

	return(status);
}


int csAnalogLoopbackSet(Uint32T chipIndex, Uint8T enable){
	int status = OK;
	status = idt82V2058Write(&(idt82V2058Params[chipIndex]), IDT82V2058_ALB_REG,enable);
	return(status);
}

int csAnalogLoopbackGet(Uint32T chipIndex, Uint8T *enable){
	int status = OK;
	status = idt82V2058Read(&(idt82V2058Params[chipIndex]), IDT82V2058_ALB_REG,enable);
	return(status);
}

int csRemoteLoopbackSet(Uint32T chipIndex, Uint8T enable){
	int status = OK;
	status = idt82V2058Write(&(idt82V2058Params[chipIndex]), IDT82V2058_RLB_REG,enable);
	return(status);
}

int csRemoteLoopbackGet(Uint32T chipIndex, Uint8T *enable){
	int status = OK;
	status = idt82V2058Read(&(idt82V2058Params[chipIndex]), IDT82V2058_RLB_REG,enable);
	return(status);
}

int csDigitalLoopbackSet(Uint32T chipIndex, Uint8T enable) {
	int status = OK;
	status = idt82V2058Write(&(idt82V2058Params[chipIndex]), IDT82V2058_DLB_REG, enable);
	return(status);
}

int csDigitalLoopbackGet(Uint32T chipIndex, Uint8T *enable) {
	int status = OK;
	status = idt82V2058Read(&(idt82V2058Params[chipIndex]), IDT82V2058_DLB_REG, enable);
	return(status);
}

int csTaosSet(Uint32T chipIndex, Uint8T enable){
	int status = OK;
	status = idt82V2058Write(&(idt82V2058Params[chipIndex]), IDT82V2058_TAO_REG,enable);
	return(status);
}

int csTaosGet(Uint32T chipIndex, Uint8T *enable){
	int status = OK;
	status = idt82V2058Read(&(idt82V2058Params[chipIndex]), IDT82V2058_TAO_REG,enable);
	return(status);
}

int csAutomaticTaosSet(Uint32T chipIndex, Uint8T enable){
	int status = OK;
	status = idt82V2058Write(&(idt82V2058Params[chipIndex]), IDT82V2058_ATAO_REG, enable);
	return(status);
}

int csAutomaticTaosGet(Uint32T chipIndex, Uint8T *enable){
	int status = OK;
	status = idt82V2058Read(&(idt82V2058Params[chipIndex]), IDT82V2058_ATAO_REG, enable);
	return(status);
}


int csPerformanceMonitorSet(Uint32T chipIndex, Uint8T enable){
	int status = OK;
	status = idt82V2058Write(&(idt82V2058Params[chipIndex]), IDT82V2058_PMON_REG, enable);
	return(status);
}

int csPerformanceMonitorGet(Uint32T chipIndex, Uint8T *enable){
	int status = OK;
	status = idt82V2058Read(&(idt82V2058Params[chipIndex]), IDT82V2058_PMON_REG, enable);
	return(status);
}


int csAisStatusGet(Uint32T chipIndex, Uint8T *aissStatus){
	int status = OK;
	status = idt82V2058Read(&(idt82V2058Params[chipIndex]), IDT82V2058_AIS_REG, aissStatus);
	return(status);  
}

int csLosStatusGet(Uint32T chipIndex, Uint8T *losStatus){
	int status = OK;
	status = idt82V2058Read(&(idt82V2058Params[chipIndex]), IDT82V2058_LOS_REG, losStatus);
	return(status);  
}

int csLosIntStatusGet(Uint32T chipIndex,Uint8T *losIntStatus) {
	int status = OK;
	status = idt82V2058Read(&(idt82V2058Params[chipIndex]), IDT82V2058_LOSI_REG, losIntStatus);
	return(status);
}

int csLosIntMaskSet(Uint32T chipIndex,Uint8T set) {
	int status = OK;
	status = idt82V2058Write(&(idt82V2058Params[chipIndex]), IDT82V2058_LOSM_REG, set);
	return(status);
}

int csSoftwareReset(Uint32T chipIndex) {
	int status = OK;
	status = idt82V2058Write(&(idt82V2058Params[chipIndex]), IDT82V2058_RS_REG, 0xff);
	return(status);
}

int csSetGlobalControl(Uint32T chipIndex, Uint8T set) {
	int status = OK;
	status = idt82V2058Write(&(idt82V2058Params[chipIndex]), IDT82V2058_GCF_REG,set);
	return(status);
}

int csGetGlobalControl(Uint32T chipIndex,Uint8T *control) {
	int status = OK;
	status = idt82V2058Read(&(idt82V2058Params[chipIndex]), IDT82V2058_GCF_REG, control);
	return(status);
}


int idt82V2058Read(idt82V2058ParamsS* CS61884Params, Uint32T address, Uint8T *memValue){
	int status = OK;

	if(CS61884Params == NULL)
	{
		*memValue = 0;
		return ERROR;
	}

	semTake(CsAndCpldSem, WAIT_FOREVER);
	*memValue = *(Uint8T *)(CS61884Params->baseAdd + address);   
	semGive(CsAndCpldSem);
	return(status);
}

int idt82V2058Write(idt82V2058ParamsS* CS61884Params, Uint32T address, Uint8T memValue){
	int status = OK;

	if(CS61884Params == NULL)
	{
		return ERROR;
	}
	
	semTake(CsAndCpldSem, WAIT_FOREVER);
	*(Uint8T *)(CS61884Params->baseAdd + address) = memValue;
	/*taskDelay(1);*/
	semGive(CsAndCpldSem);

	return(status);
}


/*
port : 1-16
*/
idt_remote_loopback(Uint32T port, Uint8T enable)
{
	int status = OK;
	Uint32T chipIndex=0;
	Uint8T regbuf;

	if((port < 1)||(port > 16))
	{
		return ERROR;
	}

	chipIndex = (port <= 8) ? 0:1; 

	csRemoteLoopbackGet(chipIndex, &regbuf);

	printf("regbuf =%x\r\n",regbuf);
	
	regbuf =  regbuf | idt82V2058RegMap[port];

	printf("regbuf =%x\r\n",regbuf);
	
	csRemoteLoopbackSet(chipIndex, regbuf);

	return status;

}

/*
port : 1-16
*/
idt_analog_loopback(Uint32T port, Uint8T enable)
{
	int status = OK;
	Uint32T chipIndex=0;
	Uint8T regbuf;

	if((port < 1)||(port > 16))
	{
		return ERROR;
	}

	chipIndex = (port <= 8) ? 0:1; 

	csAnalogLoopbackGet(chipIndex, &regbuf);

	printf("regbuf =%x\r\n",regbuf);
	
	regbuf =  regbuf | idt82V2058RegMap[port];

	printf("regbuf =%x\r\n",regbuf);
	
	csAnalogLoopbackSet(chipIndex, regbuf);

	return status;

}


/*
port : 1-16

*/
int idt_los_status_get(Uint32T port, Uint8T *status)
{
	Uint32T chipIndex=0;
	Uint8T regbuf;

	if((port < 1)||(port > 16))
	{
		return ERROR;
	}

	chipIndex = (port <= 8) ? 0:1; 

	csLosStatusGet(chipIndex, &regbuf);

	printf("regbuf =%x\r\n",regbuf);
	
	regbuf =  regbuf &  idt82V2058RegMap[port];

	if(regbuf)
	{
		/* LOS */
		*(Uint8T *)status = 0;	
	}
	else
	{
		/* LINK */
		*(Uint8T *)status = 1;	
	}

	return (*status);

}


/*
slotId [1,2]
port : 1-16
0: HDB3 code (default)
1: AMI code
*/
idt_line_code_set(Uint8T slotId, Uint8T code)
{
	int status = OK;
	Uint32T chipIndex=0;
	Uint8T regbuf;

	if(slotId ==1)
	{
		for(chipIndex=0;chipIndex<2;chipIndex++)
		{
			csGetGlobalControl(chipIndex, &regbuf);

			printf("regbuf =%x\r\n",regbuf);

			if(code)
			{
				regbuf =  regbuf &0xef;
			}
			else
			{
				regbuf =  regbuf |0x10;
			}
			
			printf("regbuf =%x\r\n",regbuf);
			
			csSetGlobalControl(chipIndex, regbuf);
		}
	}
	else if(slotId ==2)
	{
		for(chipIndex=2;chipIndex<4;chipIndex++)
		{
			csGetGlobalControl(chipIndex, &regbuf);

			printf("regbuf =%x\r\n",regbuf);

			if(code)
			{
				regbuf =  regbuf &0xef;
			}
			else
			{
				regbuf =  regbuf |0x10;
			}
			
			printf("regbuf =%x\r\n",regbuf);
			
			csSetGlobalControl(chipIndex, regbuf);
		}
	}
	else
	{
		return -1;
	}
	return status;

}


int udr_E1_loopback_set(Uint32T port, void *arg)
{
	Uint8T enable = *(Uint8T*)arg;

	printf("udr_remote_loopback(%x,%x) ",port,*(Uint8T*)arg);

	if(enable==0)
	{
		idt_remote_loopback(port, 0);
		idt_analog_loopback(port, 0);
	}
	else if(enable==1)
	{
		idt_analog_loopback(port, 1);
	}
	else if(enable==2)
	{
		idt_remote_loopback(port, 1);
	}
	else
	{
		printf("error arg=%x ",*(Uint8T*)arg);
	}
	
	return OK;
}

void testIdtRegRead(Uint32T address,int chipIndex)
{
	idt82V2058Read(&(idt82V2058Params[chipIndex]), (0x00000000+address),&idt82V2058Regbuf[address]);
	printf("%s : 0x%x \r\n", idt82V2058RegName[address],idt82V2058Regbuf[address]);
	return;
}

void testIdtRegWrite(Uint32T address,Uint8T memValue,int chipIndex)
{
	idt82V2058Write(&(idt82V2058Params[chipIndex]), address,memValue);
	printf("%s : %x \r\n", idt82V2058RegName[address],memValue);
	return;
}

void dumpIDTall(int chipIndex)
{
	int regIndex;

	printf("chip%x baseAdd=%x\n", chipIndex, idt82V2058Params[chipIndex].baseAdd);
	for(regIndex=0;regIndex<0x1F;regIndex++){
   		idt82V2058Read(&(idt82V2058Params[chipIndex]), (0x00000000+regIndex), &idt82V2058Regbuf[regIndex]);
		printf("%s %x	", idt82V2058RegName[regIndex], idt82V2058Regbuf[regIndex]);
		if(regIndex%2){
			printf("\n");
		}
		taskDelay(1);
   	}

}

void includeIdt82V2058( void )
{
	return ;
}



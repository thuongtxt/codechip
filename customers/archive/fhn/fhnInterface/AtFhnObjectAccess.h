/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Application
 * 
 * File        : AtFhnObjectAccess.h
 * 
 * Created Date: Jun 17, 2013
 *
 * Description : Object accessing for FHN project
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATFHNOBJECTACCESS_H_
#define _ATFHNOBJECTACCESS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDevice.h"

#include "AtModuleEth.h"
#include "AtHdlcChannel.h"
#include "AtHdlcLink.h"

#include "AtModuleEncap.h"
#include "AtModulePpp.h"

#include "AtModulePdh.h"
#include "AtModuleSdh.h"
#include "AtSdhLine.h"
#include "AtSdhVc.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Access devices */
AtDevice AtFhnDeviceGet(uint8 slotId, uint8 deviceId);

/* Access modules */
AtModuleEth   AtFhnEthModule  (uint8 slotId, uint8 deviceId);
AtModuleEncap AtFhnEncapModule(uint8 slotId, uint8 deviceId);
AtModulePdh   AtFhnPdhModule  (uint8 slotId, uint8 deviceId);
AtModulePpp   AtFhnPppModule  (uint8 slotId, uint8 deviceId);
AtModuleSdh   AtFhnSdhModule  (uint8 slotId, uint8 deviceId);

/* Access DE1 for DS1/E1 of card */
AtPdhDe1 AtFhnDe1Get(uint8 slotId, uint16 de1Id);

/* Access SDH channels */
AtSdhLine AtFhnLineGet(uint8 slotId, uint8 lineId);
AtSdhAug  AtFhnAug1Get(uint8 slotId, uint8 lineId, uint8 aug1Id);
AtSdhVc   AtFhnVc4Get(uint8 slotId, uint8 lineId, uint8 aug1Id);
AtSdhAu   AtFhnAu4Get(uint8 slotId, uint8 lineId, uint8 aug1Id);
AtSdhVc   AtFhnVc3Get(uint8 slotId, uint8 lineId, uint8 aug1Id, uint8 vc3Id);
AtSdhAu   AtFhnAu3Get(uint8 slotId, uint8 lineId, uint8 aug1Id, uint8 au3Id);
AtSdhTug  AtFhnTug3Get(uint8 slotId, uint8 lineId, uint8 augId, uint8 tug3Id);
AtSdhTug  AtFhnTug2Get(uint8 slotId, uint8 lineId, uint8 augId, uint8 au3tug3id, uint8 tug2Id);
AtSdhTu   AtFhnTu3Get(uint8 slotId, uint8 lineId, uint8 augId, uint8 tug3Id);
AtSdhTu   AtFhnTu1xGet(uint8 slotId, uint8 lineId, uint8 augId, uint8 tu3Id, uint8 tug2Id, uint8 tuId);
AtSdhVc   AtFhnVc1xGet(uint8 slotId, uint8 lineId, uint8 augId, uint8 tu3Id, uint8 tug2Id, uint8 tuId);

/* Access PDH channels mapped to SDH VCs */
AtPdhDe1  AtFhnVcDe1Get(uint8 slotId, uint8 lineId, uint8 augId, uint8 tu3Id, uint8 tug2Id, uint8 tuId);
AtPdhDe3  AtFhnVcDe3Get(uint8 slotId, uint8 lineId, uint8 augId, uint8 tu3Id);

/* Bundle management */
AtMpBundle AtFhnMpBundleCreate(uint8 slotId, uint32 bundleId);
eAtRet     AtFhnMpBundleDelete(uint8 slotId, uint32 bundleId);
AtMpBundle AtFhnMpBundleGet(uint8 slotId, uint8 deviceId, uint16 bundleId);

/* Encapsulation resources management */
AtHdlcChannel AtFhnHdlcPppChannelCreate(uint8 slotId, uint32 channelId);
AtHdlcChannel AtFhnCiscoHdlcChannelCreate(uint8 slotId, uint32 channelId);
eAtRet        AtFhnEncapChannelDelete(uint8 slotId, uint32 channelId);
AtHdlcLink AtFhnHdlcLinkGet(uint8 slotId, uint8 deviceId, uint16 encapChannelId);
AtHdlcChannel AtFhnHdlcChannelGet(uint8 slotId, uint8 deviceId, uint16 encapChannelId);

/* Ethernet resource management */
AtEthFlow AtFhnNopFlowCreate(uint8 slotId, uint16 flowId);
AtEthFlow AtFhnEopFlowCreate(uint8 slotId, uint16 flowId);
eAtRet    AtFhnEthFlowDelete(uint8 slotId, uint16 flowId);
AtEthFlow AtFhnEthFlowGet(uint8 slotId, uint8 deviceId, uint16 flowId);
AtEthPort AtFhnEthPortGet(uint8 slotId, uint8 deviceId, uint16 portId);

#ifdef __cplusplus
}
#endif
#endif /* _ATFHNOBJECTACCESS_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SONET/SDH
 *
 * File        : udr_at_fhn_e1.c
 *
 * Created Date: July 24, 2013
 *
 * Description : 
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
//#include "AtCli.h"
#include "AtDevice.h"
#include "AtSdhAug.h"
#include "AtSdhTug.h"
#include "AtSdhVc.h"
#include "AtSdhLine.h"
#include "AtEthFlow.h"
#include "AtPppLink.h"
//#include "../encap/CliAtModuleEncap.h"
#include "AtPdhDe1.h"
#include "../../cli/libs/text-ui/include/AtTextUI.h"
#include "AtDriver.h"
#include "../../driver/include/encap/AtHdlcChannel.h"
#include "fhninterface.h"
#include "semLib.h"

#define POS_AT_STM_SEM_TAKE(slotId, time) 	if (OK!=semTake(semStmLogicalChannelNode[slotId],(time)*sysClkRateGet())){printf("%s,#%d,error\r\n",__FUNCTION__,__LINE__);return -1;}

#define POS_AT_STM_SEM_GIVE(slotId) 		semGive(semStmLogicalChannelNode[slotId])


uint32 gui4AtUdrFhnStmApiDebug=0;
uint32 gui4AtFhnPosStmSoftDebug=0;

extern uint32 gui4posSdhAugMapRecord[AT_POS_STM_MAX_SLOT_FHN][AT_POS_STM_MAX_PORT_FHN];
extern SEM_ID semStmLogicalChannelNode[AT_POS_STM_MAX_SLOT_FHN];
extern uint32 gu4AtStmBundleMemberRecord[AT_POS_STM_MAX_SLOT_FHN][AT_POS_STM_MAX_FPGA_FHN][AT_POS_STM_MAX_MLPPP_BUNDLE_FHN+1];
extern tAtHwStmconfigRecordFhn gui4AtStmlogicalchannelEncap[AT_POS_STM_MAX_SLOT_FHN][AT_POS_STM_MAX_PORT_FHN+1][AT_POS_STM_MAX_LOGICAL_CHANNEL_PERPORT_FHN+1];
extern tAtHwStmchannelRecordNodeFhn gtAtHwStmchannelRecordNode[AT_POS_STM_MAX_SLOT_FHN][AT_POS_STM_MAX_FPGA_FHN][AT_POS_STM_MAX_HW_CHANNEL_FHN+1];
extern uint32 gui4AtslotCardType[3];

int _udr_if_stm_pos_encap_type_get(uint8 cardId, uint32 cposPort, uint32 logicalChannel, uint32 *encapType);

/*
HW_IF_POSMEDIUMTYPE,
HW_IF_POSAUGMAPPING,
HW_IF_POSLOOPBACK,
HW_IF_POSCLOCK,
HW_IF_POSCRC,
HW_IF_POSFLAGC2,
HW_IF_POSFLAGJ0,
HW_IF_POSFLAGJ1,
HW_IF_POSSDTHRSHLD,
HW_IF_POSSFTHRSHLD,
HW_IF_POSFLAGJ0MODE,
HW_IF_POSFLAGJ1MODE,
HW_IF_POSCHANNELIZATION,
HW_IF_POSADDCHANNEL,
HW_IF_POSDELCHANNEL,

HW_IF_ADMIN_STATUS
HW_IF_MTU
HW_IF_PHYADDR

HW_IF_OUTERTPID
HW_IF_OUTERVLANID（HW_IF_VLANID）
HW_IF_OUTERPRIORITY（HW_IF_PRIORITY）

HW_IF_POSLOOPBACK
HW_IF_STATISTICS（HW_IF_STATS）
HW_IF_ENCAPTYPE
HW_IF_TRUNKADDMEMBER
HW_IF_TRUNKDELMEMBER
HW_IF_TRUNK
HW_IF_TRUNKACTIVEMEMBER
HW_IF_TRUNKINACTIVEMEMBER

*/



/*
set ppp / mlppp
*/

int At_fhn_pos_stm_soft_channel_set(uint8 cardId, uint32 cposPort, uint32 logicalChannel, uint32 cposPortVc12Ch)
{
	int ret=0;
	uint32 i=0,encapType=1;
	uint32 slotId = cardId-1;
	uint32 fpgaId = (cposPort > 4)?1:0;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(cposPort<1||cposPort>AT_POS_STM_MAX_PORT_FHN)
	{
		printf("#%d,%s,error ce1Port=%d\r\n",__LINE__,__FUNCTION__,cposPort);
		return -1;
	}	

	ret = udr_cpos_port_encap_type_get(cardId, cposPort, logicalChannel, &encapType);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	POS_AT_STM_SEM_TAKE(slotId,10);
	for(i=1;i<=AT_POS_STM_MAX_HW_CHANNEL_FHN;i++)
	{
		if(gtAtHwStmchannelRecordNode[slotId][fpgaId][i].usedFlag==0)
		{
			gtAtHwStmchannelRecordNode[slotId][fpgaId][i].usedFlag=1;
			gtAtHwStmchannelRecordNode[slotId][fpgaId][i].channelId=i;
			gtAtHwStmchannelRecordNode[slotId][fpgaId][i].cposPort=cposPort;
			gtAtHwStmchannelRecordNode[slotId][fpgaId][i].cposPortVc12Ch=cposPortVc12Ch;
			gtAtHwStmchannelRecordNode[slotId][fpgaId][i].cposPortVc12ChTimeslot=0;
			gtAtHwStmchannelRecordNode[slotId][fpgaId][i].logicalChannelId=logicalChannel;	
			gtAtHwStmchannelRecordNode[slotId][fpgaId][i].encapType=encapType;	

				if(gui4AtFhnPosStmSoftDebug){
					printf("slot/port/logCh[%d/%d/%d]==fpga/hwch[%d/%d] bindTo port/vc12/ts/encap[%d/%d/%x/%x]\r\n",
					slotId,				
					gtAtHwStmchannelRecordNode[slotId][fpgaId][i].cposPort,
					gtAtHwStmchannelRecordNode[slotId][fpgaId][i].logicalChannelId,
					
					fpgaId,
					gtAtHwStmchannelRecordNode[slotId][fpgaId][i].channelId,
					
					gtAtHwStmchannelRecordNode[slotId][fpgaId][i].cposPort,
					gtAtHwStmchannelRecordNode[slotId][fpgaId][i].cposPortVc12Ch,
					gtAtHwStmchannelRecordNode[slotId][fpgaId][i].cposPortVc12ChTimeslot,
					gtAtHwStmchannelRecordNode[slotId][fpgaId][i].encapType);
				}
			POS_AT_STM_SEM_GIVE(slotId);
			return 0;
		}
	}

	if(i>AT_POS_STM_MAX_HW_CHANNEL_FHN){
		if(gui4AtFhnPosStmSoftDebug){
			printf("#%d,%s,card[%d]cposPort[%d]logicalChannel[%d] set have not find resource\r\n",__LINE__,__FUNCTION__,cardId,cposPort,logicalChannel);
		}
		POS_AT_STM_SEM_GIVE(slotId);
		return -1;
	}

	POS_AT_STM_SEM_GIVE(slotId);
	return 0;

}


int At_fhn_pos_stm_soft_channel_timeslot_set(uint8 cardId, uint32 cposPort, uint32 logicalChannel,uint32 cposPortVc12Ch,uint32 cposPortVc12Timeslot)
{
	uint32 i=0;
	uint32 slotId= cardId-1;
	uint32 fpgaId = (cposPort > 4)?1:0;
	
	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(cposPort<1||cposPort>AT_POS_STM_MAX_PORT_FHN)
	{
		printf("#%d,%s,error cposPort=%d\r\n",__LINE__,__FUNCTION__,cposPort);
		return -1;
	}	

	POS_AT_STM_SEM_TAKE(slotId,10);
	for(i=1;i<=AT_POS_STM_MAX_HW_CHANNEL_FHN;i++)
	{
		if(	(gtAtHwStmchannelRecordNode[slotId][fpgaId][i].usedFlag!=0)&&
			(gtAtHwStmchannelRecordNode[slotId][fpgaId][i].cposPort==cposPort)&&
			(gtAtHwStmchannelRecordNode[slotId][fpgaId][i].logicalChannelId==logicalChannel))
		{
			gtAtHwStmchannelRecordNode[slotId][fpgaId][i].cposPortVc12Ch= cposPortVc12Ch;
			gtAtHwStmchannelRecordNode[slotId][fpgaId][i].cposPortVc12ChTimeslot= cposPortVc12Timeslot;
			if(gui4AtFhnPosStmSoftDebug){
				printf("slot/port/logCh[%d/%d/%d]==fpga/hwch[%d/%d] bindTo port/vc12/ts/encap[%d/%d/%x/%x]\r\n",
					slotId,				
					gtAtHwStmchannelRecordNode[slotId][fpgaId][i].cposPort,
					gtAtHwStmchannelRecordNode[slotId][fpgaId][i].logicalChannelId,
					
					fpgaId,
					gtAtHwStmchannelRecordNode[slotId][fpgaId][i].channelId,
					
					gtAtHwStmchannelRecordNode[slotId][fpgaId][i].cposPort,
					gtAtHwStmchannelRecordNode[slotId][fpgaId][i].cposPortVc12Ch,
					gtAtHwStmchannelRecordNode[slotId][fpgaId][i].cposPortVc12ChTimeslot,
					gtAtHwStmchannelRecordNode[slotId][fpgaId][i].encapType);
			}
			
			POS_AT_STM_SEM_GIVE(slotId);
			return 0;
		}
	}

	if(i>AT_POS_STM_MAX_HW_CHANNEL_FHN){
		if(gui4AtFhnPosStmSoftDebug){
			printf("#%d,%s,card[%d]cposPort[%d]logicalChannel[%d] set ts have not find channel\r\n",__LINE__,__FUNCTION__,cardId,cposPort,logicalChannel);
		}
		POS_AT_STM_SEM_GIVE(slotId);
		return -1;
	}

	POS_AT_STM_SEM_GIVE(slotId);
	return 0;

}


int At_fhn_pos_stm_soft_channel_del(uint8 cardId, uint32 cposPort, uint32 logicalChannel)
{
	uint32 i=0;
	uint32 slotId= cardId-1;
	uint32 fpgaId = (cposPort > 4)?1:0;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(cposPort<1||cposPort>AT_POS_STM_MAX_PORT_FHN)
	{
		printf("#%d,%s,error cposPort=%d\r\n",__LINE__,__FUNCTION__,cposPort);
		return -1;
	}	

	POS_AT_STM_SEM_TAKE(slotId,10);
	for(i=1;i<=AT_POS_STM_MAX_HW_CHANNEL_FHN;i++)
	{
		if(	(gtAtHwStmchannelRecordNode[slotId][fpgaId][i].usedFlag!=0)&&
			(gtAtHwStmchannelRecordNode[slotId][fpgaId][i].cposPort==cposPort)&&
			(gtAtHwStmchannelRecordNode[slotId][fpgaId][i].logicalChannelId==logicalChannel))
		{
				if(gui4AtFhnPosStmSoftDebug){
					printf("del slot/port/logCh[%d/%d/%d]==fpga/hwch[%d/%d] bindTo port/vc12/ts/encap[%d/%d/%x/%x]\r\n",
						slotId,				
						gtAtHwStmchannelRecordNode[slotId][fpgaId][i].cposPort,
						gtAtHwStmchannelRecordNode[slotId][fpgaId][i].logicalChannelId,
						
						fpgaId,
						gtAtHwStmchannelRecordNode[slotId][fpgaId][i].channelId,
						
						gtAtHwStmchannelRecordNode[slotId][fpgaId][i].cposPort,
						gtAtHwStmchannelRecordNode[slotId][fpgaId][i].cposPortVc12Ch,
						gtAtHwStmchannelRecordNode[slotId][fpgaId][i].cposPortVc12ChTimeslot,
						gtAtHwStmchannelRecordNode[slotId][fpgaId][i].encapType);
				}
				gtAtHwStmchannelRecordNode[slotId][fpgaId][i].usedFlag = 0;
				gtAtHwStmchannelRecordNode[slotId][fpgaId][i].channelId=0;
				gtAtHwStmchannelRecordNode[slotId][fpgaId][i].cposPort=0;
				gtAtHwStmchannelRecordNode[slotId][fpgaId][i].cposPortVc12Ch=0;
				gtAtHwStmchannelRecordNode[slotId][fpgaId][i].cposPortVc12ChTimeslot=0;
				gtAtHwStmchannelRecordNode[slotId][fpgaId][i].logicalChannelId=0;
				gtAtHwStmchannelRecordNode[slotId][fpgaId][i].encapType=1;

			POS_AT_STM_SEM_GIVE(slotId);
			return 0;
		}
	}

	if(i>AT_POS_STM_MAX_HW_CHANNEL_FHN){
		if(gui4AtFhnPosStmSoftDebug){
			printf("#%d,%s,card[%d]cposPort[%d]logicalChannel[%d] del have not find channel\r\n",__LINE__,__FUNCTION__,cardId,cposPort,logicalChannel);
		}
		POS_AT_STM_SEM_GIVE(slotId);
		return -1;
	}

	POS_AT_STM_SEM_GIVE(slotId);
	return 0;

}


/*
input :
	uint8 cardId, 
	uint32 ce1Port, 
	uint32 ce1portTimeslot,
output:
	uint32 *channelId, 
	uint32 *encapType

*/
int At_fhn_pos_stm_soft_channel_get(uint8 cardId, uint32 cposPort, uint32 logicalChannel, 
										uint32 *cposPortVc12Ch,uint32 *cposPortVc12Timeslot, uint32 *channelId, uint32 *encapType)
{
	uint32 i=0;
	uint32 slotId= cardId-1;
	uint32 fpgaId = (cposPort > 4)?1:0;
	
	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(cposPort<1||cposPort>AT_POS_STM_MAX_PORT_FHN)
	{
		printf("#%d,%s,error cposPort=%d\r\n",__LINE__,__FUNCTION__,cposPort);
		return -1;
	}

	POS_AT_STM_SEM_TAKE(slotId,10);
	for(i=1;i<=AT_POS_STM_MAX_HW_CHANNEL_FHN;i++)
	{
		if(	(gtAtHwStmchannelRecordNode[slotId][fpgaId][i].usedFlag!=0)&&
			(gtAtHwStmchannelRecordNode[slotId][fpgaId][i].cposPort==cposPort)&&
			(gtAtHwStmchannelRecordNode[slotId][fpgaId][i].logicalChannelId==logicalChannel))
		{
			*(uint32 *)cposPortVc12Ch = gtAtHwStmchannelRecordNode[slotId][fpgaId][i].cposPortVc12Ch;
			*(uint32 *)cposPortVc12Timeslot = gtAtHwStmchannelRecordNode[slotId][fpgaId][i].cposPortVc12ChTimeslot;
			*(uint32 *)channelId = gtAtHwStmchannelRecordNode[slotId][fpgaId][i].channelId;
			*(uint32 *)encapType = gtAtHwStmchannelRecordNode[slotId][fpgaId][i].encapType;
			POS_AT_STM_SEM_GIVE(slotId);
			return 0;
		}
	}

	if(i>AT_POS_STM_MAX_HW_CHANNEL_FHN){
		if(gui4AtFhnPosStmSoftDebug){
			printf("#%d,%s,get can not find\r\n",__LINE__,__FUNCTION__);
		}
		POS_AT_STM_SEM_GIVE(slotId);
		return -1;
	}

	POS_AT_STM_SEM_GIVE(slotId);
	return 0;
}


int At_fhn_pos_stm_soft_channel_show(uint8 cardId)
{
	uint32 channelID=0,bundleID,stmport=0,logicalChannel=0;
	uint32 slotId= cardId-1;
	uint32 fpgaId = 0;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	POS_AT_STM_SEM_TAKE(slotId,10);
	for(fpgaId=0;fpgaId<2;fpgaId++){
		for(channelID=1;channelID<=AT_POS_STM_MAX_HW_CHANNEL_FHN;channelID++){
			if(gtAtHwStmchannelRecordNode[slotId][fpgaId][channelID].usedFlag != 0){
				printf("slot/port/logCh[%d/%d/%d] == fpga/hwch[%d/%d] bindTo port/vc12/ts/encap[%d/%d/%x/%x]\r\n",
					slotId,				
					gtAtHwStmchannelRecordNode[slotId][fpgaId][channelID].cposPort,
					gtAtHwStmchannelRecordNode[slotId][fpgaId][channelID].logicalChannelId,
					
					fpgaId,
					gtAtHwStmchannelRecordNode[slotId][fpgaId][channelID].channelId,
					
					gtAtHwStmchannelRecordNode[slotId][fpgaId][channelID].cposPort,
					gtAtHwStmchannelRecordNode[slotId][fpgaId][channelID].cposPortVc12Ch,
					gtAtHwStmchannelRecordNode[slotId][fpgaId][channelID].cposPortVc12ChTimeslot,
					gtAtHwStmchannelRecordNode[slotId][fpgaId][channelID].encapType);

					stmport = gtAtHwStmchannelRecordNode[slotId][fpgaId][channelID].cposPort;
					logicalChannel = gtAtHwStmchannelRecordNode[slotId][fpgaId][channelID].logicalChannelId;

					printf("slot/port/logCh[%d/%d/%d]: timeModen[%d]/refPort[%d]/framerType[%d]/encapType[%d]\r\n",
					slotId,				
					gtAtHwStmchannelRecordNode[slotId][fpgaId][channelID].cposPort,
					gtAtHwStmchannelRecordNode[slotId][fpgaId][channelID].logicalChannelId,
					
					gui4AtStmlogicalchannelEncap[slotId][stmport][logicalChannel].timeModen,/*cAtTimingModeSys*/
					gui4AtStmlogicalchannelEncap[slotId][stmport][logicalChannel].refPort,/* default  */
					gui4AtStmlogicalchannelEncap[slotId][stmport][logicalChannel].framerType,/*default cAtPdhE1UnFrm*/
					gui4AtStmlogicalchannelEncap[slotId][stmport][logicalChannel].encapType/* default PPP */);

				
			}
		}
	}

	for(fpgaId=0;fpgaId<2;fpgaId++){	
		for(bundleID=1;bundleID<=AT_POS_E1_MAX_MLPPP_BUNDLE_FHN;bundleID++){
			if(gu4AtStmBundleMemberRecord[slotId][fpgaId][bundleID] !=0){
				printf("slot[%d]trunk[%d] member=%d\r\n",slotId,bundleID,gu4AtStmBundleMemberRecord[slotId][fpgaId][bundleID]);
			}
		}
	}
	POS_AT_STM_SEM_GIVE(slotId);
	return 0;
}


int _udr_at_stm_pos_card_smac_set(uint8 cardId, uint8 *cposPort1to4sSmac, uint8 *cposPort5to8sSmac)
{
	int ret=0;

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);
	
	ret = Udr_at_eth_port_smac_set(cardId,1,1,cposPort1to4sSmac);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	if(cposPort5to8sSmac != NULL)
	{
		ret = Udr_at_eth_port_smac_set(cardId,2,1,cposPort5to8sSmac);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		}
	}

	POS_AT_UDR_SEM_GIVE(cardId-1);

	return 0;
}

/*
HW_IF_DS1SENDCODE
*/

int _udr_if_stm_pos_send_code(uint8 cardId, uint32 cposPort, uint32 logicalChannel, int *arg)
{
	int ret=0,enable=0;
	uint32 channelId=0,encapType=0,flowId=0,cposPortVc12Channel=0,cposPortVc12Timeslot=0;
	uint32 fpgaId = (cposPort > 4)?1:0;
	uint32 port=cposPort;

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
			fpgaId=1;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	if(At_fhn_pos_stm_soft_channel_get( cardId,  port,  logicalChannel, &cposPortVc12Channel, &cposPortVc12Timeslot,&channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

  	ret = Udr_cpos_port_channel_scramle_set(cardId, fpgaId+1,channelId, arg);

	POS_AT_UDR_SEM_GIVE(cardId-1);

	return ret;
}

/*
LOS ALARM
phy port tx enable or disable
1: link up
0: link down
*/
int _udr_if_stm_pos_port_status_set(uint8 cardId, uint32 cposPort, void *arg)
{
	uint32 port=cposPort;
	int ret=0;

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	Udr_cpos_port_enable(cardId,  port, arg);/* added by ssq 20140529 */

	ret = Udr_cpos_port_admin_status(cardId,  port, arg);

	POS_AT_UDR_SEM_GIVE(cardId-1);

	return ret;
	
}

/* 
HW_IF_ADMIN_STATUS
1: link up
0: link down
*/
int _udr_if_stm_pos_admin_status_set(uint8 cardId, uint32 cposPort, uint32 logicalChannel, int *arg)
{
	int ret=0,enable=0;
	uint32 channelId=0,encapType=0,flowId=0,cposPortVc12Channel=0,cposPortVc12Timeslot=0;
	uint32 fpgaId = (cposPort > 4)?1:0;
	uint32 port=cposPort;

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
			fpgaId=1;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	if(At_fhn_pos_stm_soft_channel_get( cardId,  port,  logicalChannel, &cposPortVc12Channel, &cposPortVc12Timeslot,&channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	if( *(int*)arg){
		/* enable */
		enable = 1;/*enable channel tx*/
		ret = Udr_cpos_port_channel_txenable(cardId,fpgaId+1,channelId,&enable);
		if(ret !=0){
			printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}
		

		enable = 1;/*enable channel rx*/
		ret = Udr_cpos_port_channel_rxenable(cardId,fpgaId+1,channelId,&enable);
		if(ret !=0){
			printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}	

		enable = 5;/*cAtPppLinkPhaseNetworkActive*/
		ret = Udr_cpos_port_channel_ppp_enable(cardId,fpgaId+1,channelId,&enable);
		if(ret !=0){
			printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}

	}else{
		/* disable */
		enable = 0;/*disable channel tx*/
		ret = Udr_cpos_port_channel_txenable(cardId,fpgaId+1,channelId,&enable);
		if(ret !=0){
			printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}
		
		enable = 0;/*disable channel rx*/
		ret = Udr_cpos_port_channel_rxenable(cardId,fpgaId+1,channelId,&enable);
		if(ret !=0){
			printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}	

		enable = 1;/*cAtPppLinkPhaseDead*/
		ret = Udr_cpos_port_channel_ppp_enable(cardId,fpgaId+1,channelId,&enable);
		if(ret !=0){
			printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}

	}

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return 0;

}



/*


CmdPdhDe1FrameModeSet
typedef enum eAtPdhDe1FrameType
    {
    cAtPdhDe1FrameUnknown , /**< Unknown frame 

    cAtPdhDs1J1UnFrm      , /**< DS1/J1 Unframe 
    cAtPdhDs1FrmSf        , /**< DS1 SF (D4) 
    cAtPdhDs1FrmEsf       , /**< DS1 ESF 
    cAtPdhDs1FrmDDS       , /**< DS1 DDS 
    cAtPdhDs1FrmSLC       , /**< DS1 SLC 

    cAtPdhJ1FrmSf         , /**< J1 SF 
    cAtPdhJ1FrmEsf        , /**< J1 ESF 

    cAtPdhE1UnFrm         , /**< E1 unframe 
    cAtPdhE1Frm           , /**< E1 basic frame, FAS/NFAS framing 
    cAtPdhE1MFCrc           /**< E1 Multi-Frame with CRC4 
    }eAtPdhDe1FrameType;

*/


int _udr_if_stm_pos_line_type(uint8 cardId, uint32 cposPort, uint32 logicalChannel,  eAtPdhDe1FrameType *arg)
{
	int ret=0;
	uint32 channelId=0,encapType=0,flowId=0,cposPortVc12Channel=0,cposPortVc12Timeslot=0;
	uint32 fpgaId = (cposPort > 4)?1:0;
	uint32 port=cposPort;

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	if(logicalChannel>AT_POS_STM_MAX_LOGICAL_CHANNEL_PERPORT_FHN){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error logicalChannel=%d\r\n",logicalChannel);
		return -1;
	}


	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
			fpgaId=1;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);
	gui4AtStmlogicalchannelEncap[cardId-1][port][logicalChannel].framerType = *(eAtPdhDe1FrameType *)arg;

	if(At_fhn_pos_stm_soft_channel_get( cardId,  port,  logicalChannel, &cposPortVc12Channel, &cposPortVc12Timeslot,&channelId, &encapType) == 0){
		ret = Udr_cpos_e1_framer_mode_set (cardId, port, cposPortVc12Channel,arg);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}
	}

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return 0;
}

/*
HW_IF_POSCRC

typedef enum eAtHdlcFcsModeFhn
    {
    cAtHdlcFcsModeNoFcsFhn, /**< Disable FCS inserting 
    cAtHdlcFcsModeFcs16Fhn, /**< FCS-16 
    cAtHdlcFcsModeFcs32Fhn  /**< FCS-32 
    }eAtHdlcFcsModeFhn;
*/

int _udr_if_stm_pos_serial_crc_set(uint8 cardId, uint32 cposPort, uint32 logicalChannel, eAtHdlcFcsModeFhn *crcMode)
{
	int ret=0;
	uint32 channelId=0,encapType=0,flowId=0,cposPortVc12Channel=0,cposPortVc12Timeslot=0;
	uint32 fpgaId = (cposPort > 4)?1:0;
	uint32 port=cposPort;

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
			fpgaId=1;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	if(At_fhn_pos_stm_soft_channel_get( cardId,  port,  logicalChannel, &cposPortVc12Channel, &cposPortVc12Timeslot,&channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}
	
	ret = Udr_cpos_port_channel_fcs(cardId, fpgaId+1, channelId, crcMode);

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;
}


/*
HW_IF_POSLOOPBACK
cposPort = 1-8
arg:
typedef enum eAtLoopbackModeFhn
    {
    cAtLoopbackModeReleaseFhn, /**< Release loopback 
    cAtLoopbackModeLocalFhn,   /**< Local loopback 
    cAtLoopbackModeRemoteFhn   /**< Remote loopback 
    }eAtLoopbackModeFhn;
*/
int _udr_if_stm_pos_loopback_config(uint8 cardId, uint32 cposPort, eAtLoopbackModeFhn *arg)
{
	uint32 port=cposPort;
	int ret=0;

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

  	ret = Udr_cpos_port_loopback_set(cardId, port, arg);
	
	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;
	
}


/*HW_IF_DS1LOOPBACKCFG
CmdPdhDe1LoopbackSet
portId = 1-16
arg:
           + release
            + payloadloopin
            + payloadloopout
            + lineloopin
            + lineloopout
*/
int _udr_if_stm_pos_ce1_loopback_config(uint8 cardId, uint32 cposPort, uint32 logicalChannel, eAtPdhLoopbackModeFhn *arg)
{
	int ret=0;
	uint32 channelId=0,encapType=0,flowId=0,cposPortVc12Channel=0,cposPortVc12Timeslot=0;
	uint32 fpgaId = (cposPort > 4)?1:0;
	uint32 port=cposPort;

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
			fpgaId=1;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);
	
	if(At_fhn_pos_stm_soft_channel_get( cardId,  port,  logicalChannel, &cposPortVc12Channel, &cposPortVc12Timeslot,&channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret = Udr_cpos_e1_loopback_set( cardId,  port,  channelId, arg);

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;
}


/*
HW_IF_STATISTICS
typedef struct tAtHdlcChannelCountersFhn
{
    uint32 txGoodPktFhn;       /**< Transmit good packet 1
    uint32 txAbortPktFhn;      /**< Transmit Abort packet 2

    uint32 rxGoodPktFhn;       /**< Receive good packet 3
    uint32 rxAbortPktFhn;      /**< Receive Abort packet 4
    uint32 rxFcsErrPktFhn;     /**< Receive Fcs Error packet 5
    uint32 rxAddrCtrlErrPktFhn;/**< Receive Address Control error packet 6
    uint32 rxSapiErrPktFhn;    /**< Receive Sapi error packet 7
    uint32 rxErrPktFhn;        /**< Receive error packet 8
}tAtHdlcChannelCountersFhn;
*/

int _udr_if_stm_pos_statistics_get(uint8 cardId, uint32 cposPort, uint32 logicalChannel, tAtHdlcChannelCountersFhn *counters)
{
	int ret=0;
	uint32 channelId=0,encapType=0,flowId=0,cposPortVc12Channel=0,cposPortVc12Timeslot=0;
	uint32 fpgaId = (cposPort > 4)?1:0;
	uint32 port=cposPort;

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
			fpgaId=1;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	if(At_fhn_pos_stm_soft_channel_get( cardId,  port,  logicalChannel, &cposPortVc12Channel, &cposPortVc12Timeslot,&channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret =  Udr_cpos_channel_counter_get(cardId, fpgaId+1, channelId, counters);

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;

}

int _udr_if_stm_pos_statistics_clear(uint8 cardId, uint32 cposPort, uint32 logicalChannel)
{
	int ret=0;
	uint32 channelId=0,encapType=0,flowId=0,cposPortVc12Channel=0,cposPortVc12Timeslot=0;
	uint32 fpgaId = (cposPort > 4)?1:0;
	uint32 port=cposPort;

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
			fpgaId=1;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);
	
	if(At_fhn_pos_stm_soft_channel_get( cardId,  port,  logicalChannel, &cposPortVc12Channel, &cposPortVc12Timeslot,&channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret = Udr_cpos_channel_counter_clear(cardId, fpgaId+1, channelId, &channelId);

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;

}


/*
show sdh line alarm 1
CmdSdhLineShowAlarm
typedef struct eUdrSdhLineAlarmTypeFhn
    {
    uint8 udrSdhLineAlarmLosFhn;      /**< @brief RS-LOS defect mask   
    uint8 udrSdhLineAlarmOOFFhn;       /**< @brief RS-OOF defect mask 
    uint8 udrSdhLineAlarmLOFFhn;        /**< @brief RS-LOF defect mask 
    uint8 udrSdhLineAlarmTIMFhn;         /**< @brief RS-TIM defect mask 
    uint8 udrSdhLineAlarmAISFhn;          /**< @brief MS-AIS defect mask 
    uint8 udrSdhLineAlarmRDIFhn;        /**< @brief MS-RDI defect mask 
    uint8 udrSdhLineAlarmBERSDFhn;         /**< @brief MS-BER-SD defect mask 
    uint8 udrSdhLineAlarmBERSFFhn;         /**< @brief MS-BER-SF defect mask 
    uint8 udrSdhLineAlarmKBCHANGEFhn;    /**< @brief K-byte change event.
                                             Stable K-byte is changed 
    uint8 udrtSdhLineAlarmKBFAILFhn;      /**< @brief K-byte fail. There is no stable K-byte 
    uint8 udrSdhLineAlarmS1CHANGEFhn;     /**< @brief S1 byte change event. 
    }eUdrSdhLineAlarmTypeFhn;

0: clear alarm
1 : have alarm
*/

int _udr_if_stm_pos_port_alarm_status(uint8 cardId, uint32 cposPort, void *arg)
{
	uint32 port=cposPort;
	int ret=0;

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	ret = Udr_cpos_port_alarm_status(cardId, port, arg);
	
	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;
	
}


/*
/** @brief BER rates 
typedef enum eAtBerRateFhn
    {
    cAtBerRateUnknownFhn, /**< Unknown error rate 
    cAtBerRate1E3Fhn    , /**< Error rate is 10-3 
    cAtBerRate1E4Fhn    , /**< Error rate is 10-4 
    cAtBerRate1E5Fhn    , /**< Error rate is 10-5 
    cAtBerRate1E6Fhn    , /**< Error rate is 10-6 
    cAtBerRate1E7Fhn    , /**< Error rate is 10-7 
    cAtBerRate1E8Fhn    , /**< Error rate is 10-8 
    cAtBerRate1E9Fhn      /**< Error rate is 10-9 
    }eAtBerRateFhn;
*/
int _udr_if_stm_pos_ber_sd_set(uint8 cardId, uint32 cposPort, eAtBerRateFhn *arg)
{
	uint32 port=cposPort;
	int ret=0;

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	ret = Udr_cpos_port_ber_sd_set(cardId,  port, arg);
	
	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;
}

int _udr_if_stm_pos_ber_sf_set(uint8 cardId, uint32 cposPort, eAtBerRateFhn *arg)
{
	uint32 port=cposPort;
	int ret=0;

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	ret = Udr_cpos_port_ber_sf_set(cardId,  port, arg);

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;
}

/*
cposPort = 1-8
0:disable
1:enable
*/
int _udr_if_stm_pos_ber_enable_set(uint8 cardId, uint32 cposPort, uint32 *arg)
{
	uint32 port=cposPort;
	int ret=0;

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

 	ret = Udr_cpos_port_ber_enable(cardId,  port, arg);

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;
}

/*
HW_IF_DS1TRANSMITCLOCKSOURCE

typedef enum eAtTimingModeFhn
    {
    cAtTimingModeUnknownFhn    , /**< Unknown timing mode 
    cAtTimingModeSysFhn        , /**< System timing mode 
    cAtTimingModeLoopFhn       , /**< Loop timing mode 
    cAtTimingModeSdhLineRefFhn , /**< SDH Line 
    cAtTimingModeExt1RefFhn    , /**< Line EXT #1 timing mode 
    cAtTimingModeExt2RefFhn    , /**< Line EXT #2 timing mode 
    cAtTimingModeAcrFhn        , /**< ACR timing mode 
    cAtTimingModeDcrFhn        , /**< DCR timing mode 
    cAtTimingModeFreeFhn       , /**< Free running mode - using system clock for CDR
                                    source to generate service TX clock
    cAtTimingModeTopFhn        , /**< ToP timing mode 
    cAtTimingModeSlaveFhn        /**< Slave timing mode 
    }eAtTimingModeFhn;


typedef struct tAtTimeModeFhn
{
	eAtTimingModeFhn TimeModen;
	uint32 refPort;
}tAtTimeModeFhn;
*/

int _udr_if_stm_pos_txclock_src_config(uint8 cardId, uint32 cposPort, uint32 logicalChannel,  tAtTimeModeFhn *arg)

{
	int ret=0;
	uint32 channelId=0,encapType=0,flowId=0,cposPortVc12Channel=0,cposPortVc12Timeslot=0;
	uint32 fpgaId = (cposPort > 4)?1:0;
	uint32 port=cposPort;

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	if(logicalChannel>AT_POS_STM_MAX_LOGICAL_CHANNEL_PERPORT_FHN){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error logicalChannel=%d\r\n",logicalChannel);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
			fpgaId=1;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	//printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);

	POS_AT_UDR_SEM_TAKE(cardId-1,10);
	gui4AtStmlogicalchannelEncap[cardId-1][port][logicalChannel].timeModen= arg->TimeModen;
	gui4AtStmlogicalchannelEncap[cardId-1][port][logicalChannel].refPort= arg->refPort;	

	if(At_fhn_pos_stm_soft_channel_get( cardId,  port,  logicalChannel, &cposPortVc12Channel, &cposPortVc12Timeslot,&channelId, &encapType) == 0){	
		ret = Udr_cpos_e1_clock_mode_set(cardId, port, cposPortVc12Channel,arg);
		if(ret !=0){
			printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}
	}

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return 0;
}


/*
typedef enum eAtSdhLineRateFhn
    {
    cAtSdhLineRateStm1Fhn,    /**< STM-1 
    cAtSdhLineRateStm4Fhn,    /**< STM-4 
    cAtSdhLineRateStm16fhn    /**< STM-16 
    }eAtSdhLineRateFhn;
*/
int _udr_if_stm_pos_rate_set(uint8 cardId, uint32 cposPort, eAtSdhLineRateFhn *arg)
{
	uint32 port=cposPort;
	int ret=0;

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);
	ret = Udr_cpos_port_linerate_set(cardId, port, arg);

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;
	
}

/*
HW_IF_POSMEDIUMTYPE
*/
/*
typedef enum eAtSdhLineModeFhn
    {
    cAtSdhLineModeSdhFhn,     /**< SDH 
    cAtSdhLineModeSonetFhn    /**< SONET 
    }eAtSdhLineModeFhn;
*/

int _udr_if_stm_pos_medium_type(uint8 cardId, uint32 cposPort, eAtSdhLineModeFhn *arg)
{
	uint32 port=cposPort;
	int ret=0;

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	ret = Udr_cpos_port_linemode_set(cardId, port, arg);
	
	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;
	
}


/*
HW_IF_POSFLAGJ0MODE
HW_IF_POSFLAGJ0,
CmdSdhLineTxTti

typedef enum eAtSdhTtiMode
    {
    cAtSdhTtiMode1Byte,  /**< 1-byte 
    cAtSdhTtiMode16Byte, /**< 16-bytes 
    cAtSdhTtiMode64Byte  /**< 64-bytes 
    }eAtSdhTtiMode;



typedef struct tAtSdhTtiFhn{
	eAtSdhTtiMode mode;                       
	uint8 message[cAtSdhChannelMaxTtiLength];
	int8 paddingMode;/* 0: null_padding  1: space_padding
}tAtSdhTtiFhn;

show sdh line tti receive 1
*/
int _udr_if_stm_pos_j0_mode(uint8 cardId, uint32 cposPort, tAtSdhTtiFhn *joModen)
{
	int ret=0;
	uint32 cposPortChannel=0;
	uint32 port=cposPort;
	
	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	ret = Udr_cpos_port_tti_tx_set( cardId,  port,  joModen);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}


	ret = Udr_cpos_port_tti_rx_set( cardId,  port,  joModen);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}			

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return 0;
}


/*
HW_IF_POSFLAGJ1MODE
HW_IF_POSFLAGJ1
CmdSdhLineTxTti

typedef enum eAtSdhTtiMode
    {
    cAtSdhTtiMode1Byte,  /**< 1-byte 
    cAtSdhTtiMode16Byte, /**< 16-bytes 
    cAtSdhTtiMode64Byte  /**< 64-bytes 
    }eAtSdhTtiMode;



typedef struct tAtSdhTtiFhn{
	eAtSdhTtiMode mode;                       
	uint8 message[cAtSdhChannelMaxTtiLength];
	int8 paddingMode;/* 0: null_padding  1: space_padding
}tAtSdhTtiFhn;

show sdh path tti receive 1
*/
int _udr_if_stm_pos_j1_mode(uint8 cardId, uint32 cposPort, tAtSdhTtiFhn *joModen)
{
	int ret=0,i=0;
	uint32 cposPortChannel=0;
	uint32 port=cposPort;

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}


	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	if((gui4AtslotCardType[cardId] == cAtCardTypeline4STM)||(gui4AtslotCardType[cardId] == cAtCardTypeline8STM)){
		POS_AT_UDR_SEM_TAKE(cardId-1,10);

		/*ret = Udr_cpos_port_path_tti_tx_set( cardId,  cposPort,  joModen);*/
		ret = Udr_cpos_port_path_vc4_tti_tx_set( cardId,  port,  joModen);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}
		
		/*ret = Udr_cpos_port_path_tti_rx_set( cardId,  cposPort,  j0Moden);*/
		ret = Udr_cpos_port_path_vc4_tti_rx_set( cardId,  port,  joModen);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}			
	}else{
		POS_AT_UDR_SEM_TAKE(cardId-1,10);

		if(gui4posSdhAugMapRecord[cardId-1][port-1]==AT_POS_STM4_MAPPING){
			ret = Udr_cpos_port_path_vc4c_tti_tx_set( cardId,  port,  joModen);
			if(ret !=0){
				printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
				POS_AT_UDR_SEM_GIVE(cardId-1);
				return -1;
			}
			
			ret = Udr_cpos_port_path_vc4c_tti_rx_set( cardId,  port,  joModen);
			if(ret !=0){
				printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
				POS_AT_UDR_SEM_GIVE(cardId-1);
				return -1;
			}
		}else if(gui4posSdhAugMapRecord[cardId-1][port-1]==AT_CPOS_STM4_E3_MAPPING){
			ret = Udr_cpos_port_path_vc3_tti_tx_set( cardId,  port,  joModen);
			if(ret !=0){
				printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
				POS_AT_UDR_SEM_GIVE(cardId-1);
				return -1;
			}
			
			ret = Udr_cpos_port_path_vc3_tti_rx_set( cardId,  port,  joModen);
			if(ret !=0){
				printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
				POS_AT_UDR_SEM_GIVE(cardId-1);
				return -1;
			}
		}else if(gui4posSdhAugMapRecord[cardId-1][port-1]==AT_CPOS_STM4_155_MAPPING){
			for(i=0;i<=3;i++){
				ret = Udr_cpos_port_path_vc4_tti_tx_set( cardId,  port+i,  joModen);
				if(ret !=0){
					printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
					POS_AT_UDR_SEM_GIVE(cardId-1);
					return -1;
				}
				
				ret = Udr_cpos_port_path_vc4_tti_rx_set( cardId,  port+i,  joModen);
				if(ret !=0){
					printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
					POS_AT_UDR_SEM_GIVE(cardId-1);
					return -1;
				}
			}
		}else{
			ret = Udr_cpos_port_path_vc4_tti_tx_set( cardId,  port,  joModen);
			if(ret !=0){
				printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
				POS_AT_UDR_SEM_GIVE(cardId-1);
				return -1;
			}
			
			ret = Udr_cpos_port_path_vc4_tti_rx_set( cardId,  port,  joModen);
			if(ret !=0){
				printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
				POS_AT_UDR_SEM_GIVE(cardId-1);
				return -1;
			}
		}

	}
	
	POS_AT_UDR_SEM_GIVE(cardId-1);
	return 0;
}


int _udr_if_stm_pos_c2_flag(uint8 cardId, uint32 cposPort, uint8 *c2str)
{
	int ret=0,i=0;
	uint32 cposPortChannel=0;
	uint32 port=cposPort;

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	if((gui4AtslotCardType[cardId] == cAtCardTypeline4STM)||(gui4AtslotCardType[cardId] == cAtCardTypeline8STM)){
	
		POS_AT_UDR_SEM_TAKE(cardId-1,10);
		ret = Udr_cpos_port_path_vc4_psl_tx_set( cardId,  port,  c2str);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}

		ret = Udr_cpos_port_path_vc4_psl_rx_set( cardId,  port,  c2str);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}
	}else{
		POS_AT_UDR_SEM_TAKE(cardId-1,10);

		if(gui4posSdhAugMapRecord[cardId-1][port-1]==AT_POS_STM4_MAPPING){
			ret = Udr_cpos_port_path_vc4c_psl_tx_set( cardId,  port,  c2str);
			if(ret !=0){
				printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
				POS_AT_UDR_SEM_GIVE(cardId-1);
				return -1;
			}

			ret = Udr_cpos_port_path_vc4c_psl_rx_set( cardId,  port,  c2str);
			if(ret !=0){
				printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
				POS_AT_UDR_SEM_GIVE(cardId-1);
				return -1;
			}
		}else if(gui4posSdhAugMapRecord[cardId-1][port-1]==AT_CPOS_STM4_E3_MAPPING){
			ret = Udr_cpos_port_path_vc3_psl_tx_set( cardId,  port,  c2str);
			if(ret !=0){
				printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
				POS_AT_UDR_SEM_GIVE(cardId-1);
				return -1;
			}

			ret = Udr_cpos_port_path_vc3_psl_rx_set( cardId,  port,  c2str);
			if(ret !=0){
				printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
				POS_AT_UDR_SEM_GIVE(cardId-1);
				return -1;
			}
		}else if(gui4posSdhAugMapRecord[cardId-1][port-1]==AT_CPOS_STM4_155_MAPPING){
			for(i=0;i<=3;i++){
				ret = Udr_cpos_port_path_vc4_psl_tx_set( cardId,  port+i,  c2str);
				if(ret !=0){
					printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
					POS_AT_UDR_SEM_GIVE(cardId-1);
					return -1;
				}
				
				ret = Udr_cpos_port_path_vc4_psl_rx_set( cardId,  port+i,  c2str);
				if(ret !=0){
					printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
					POS_AT_UDR_SEM_GIVE(cardId-1);
					return -1;
				}
			}
		}else{
			ret = Udr_cpos_port_path_vc4_psl_tx_set( cardId,  port,  c2str);
			if(ret !=0){
				printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
				POS_AT_UDR_SEM_GIVE(cardId-1);
				return -1;
			}

			ret = Udr_cpos_port_path_vc4_psl_rx_set( cardId,  port,  c2str);
			if(ret !=0){
				printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
				POS_AT_UDR_SEM_GIVE(cardId-1);
				return -1;
			}
		}
	}

	POS_AT_UDR_SEM_GIVE(cardId-1);	
	return 0;
}

/*
HW_IF_MTU
only 9600
*/

int _udr_if_stm_pos_mtu_set(uint8 cardId, uint32 cposPort, uint32 logicalChannel, uint32 *mtu)
{
	int ret=0;
	uint32 channelId=0,encapType=0,flowId=0,cposPortVc12Channel=0,cposPortVc12Timeslot=0;
	uint32 fpgaId = (cposPort > 4)?1:0;
	uint32 port=cposPort;

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
			fpgaId=1;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);
	if(At_fhn_pos_stm_soft_channel_get( cardId,  port,  logicalChannel, &cposPortVc12Channel, &cposPortVc12Timeslot, &channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret = Udr_cpos_port_channel_mtu_set(cardId, fpgaId+1,channelId, mtu);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return 0;
}


/*
HW_IF_POSAUGMAPPING
mapType
#define AT_CPOS_STM1_MAPPING	1 
#define AT_CPOS_STM4_MAPPING	2 
#define AT_POS_STM1_MAPPING		3 
#define AT_POS_STM4_MAPPING		4 
#define AT_CPOS_STM4_155_MAPPING 5

*/
int _udr_if_stm_pos_aug_mapping(uint8 cardId, uint32 cposPort, uint32 mapType)
{
	return 0;
}

/*

mapType
#define AT_CPOS_STM1_MAPPING	1 
#define AT_CPOS_STM4_MAPPING	2 
#define AT_POS_STM1_MAPPING		3 
#define AT_POS_STM4_MAPPING		4 
#define AT_CPOS_STM4_155_MAPPING 5
#define AT_CPOS_STM4_E3_MAPPING 6
*/
int _udr_if_stm_pos_sdh_aug_mapping(uint8 cardId, uint32 cposPort, uint32 mapType)
{
	int ret=0;
	uint32 cposPortChannel=0;
	uint32 port=cposPort;
	
	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	if((mapType<AT_CPOS_STM1_MAPPING)||(mapType>AT_CPOS_STM4_E3_MAPPING)){
		printf("#%d,%s,error mapType=%d\r\n",__LINE__,__FUNCTION__,mapType);
		return -1;
	}


	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}
	
	POS_AT_UDR_SEM_TAKE(cardId-1,10);
	
	switch(mapType)
	{
		case AT_CPOS_STM1_MAPPING:
			for(cposPortChannel=1;cposPortChannel<=63;cposPortChannel++){
		 		ret = Udr_cpos_port_stm1_sdh_map( cardId,  port,  cposPortChannel);
				if(ret !=0){
					printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
					POS_AT_UDR_SEM_GIVE(cardId-1);
					return -1;
				}
			}
			break;

		case AT_CPOS_STM4_MAPPING:
			for(cposPortChannel=1;cposPortChannel<=252;cposPortChannel++){
				ret = Udr_cpos_port_stm4_sdh_map( cardId,  port,  cposPortChannel);
				if(ret !=0){
					printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
					POS_AT_UDR_SEM_GIVE(cardId-1);
					return -1;
				}
			}
		break;

		case AT_POS_STM1_MAPPING:	
		 	ret = Udr_pos_stm1_port_sdh_map(cardId,  port);
			if(ret !=0){
				printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
				POS_AT_UDR_SEM_GIVE(cardId-1);
				return -1;
			}
		break;

		case AT_POS_STM4_MAPPING:		
			ret = Udr_pos_stm4_vc4c_port_sdh_map(cardId, port);
			if(ret !=0){
				printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
				POS_AT_UDR_SEM_GIVE(cardId-1);
				return -1;
			}
		break;


		case AT_CPOS_STM4_155_MAPPING:		
			ret = Udr_pos_stm4_port_sdh_map(cardId, port);
			if(ret !=0){
				printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
				POS_AT_UDR_SEM_GIVE(cardId-1);
				return -1;
			}
		break;

		case AT_CPOS_STM4_E3_MAPPING:		
			ret = Udr_cpos_port_stm4_e3_sdh_map(cardId, port);
			if(ret !=0){
				printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
				POS_AT_UDR_SEM_GIVE(cardId-1);
				return -1;
			}
		break;

		default:
			printf("#%d,%s,error mapType=%d\r\n",__LINE__,__FUNCTION__,mapType);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		
	}

	gui4posSdhAugMapRecord[cardId-1][port-1]=mapType;
	
	POS_AT_UDR_SEM_GIVE(cardId-1);
	return 0;
}

/*
typedef enum eAtEncapTypeCreateFhn
    {
    cAtHdlcFrmCiscoHdlcCreateFhn,/**< Cisco HDLC 
    cAtHdlcFrmPppCreateFhn,      /**< PPP 
    cAtHdlcFrmFrCreateFhn,       /**< Frame relay 
    cAtEncapAtmCreateFhn         /**< ATM 
    } eAtEncapTypeCreateFhn;
*/

int _udr_if_stm_pos_encap_type_change(uint8 cardId, uint32 cposPort, uint32 logicalChannel, uint32 *framerType)
{
	uint32 slotId = cardId-1;
	uint32 arg = *(uint32 *)framerType +1;
	uint32 channelId=0,encapType=0,flowId=0,cposPortVc12Channel=0,cposPortVc12Timeslot=0;
	uint32 fpgaId = (cposPort > 4)?1:0;
	uint32 port = cposPort;

	if(gui4AtUdrFhnStmApiDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,port=%d,logicalChannel=%d,encapType=%d\r\n",cardId,cposPort,logicalChannel,*(uint32 *)framerType);
	}
	
	if(cardId<1||cardId>2)	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(cposPort<1||cposPort>AT_POS_STM_MAX_PORT_FHN)	{
		printf("#%d,%s,error cposPort=%d\r\n",__LINE__,__FUNCTION__,cposPort);
		return -1;
	}	

	if(logicalChannel>AT_POS_STM_MAX_LOGICAL_CHANNEL_PERPORT_FHN)	{
		printf("#%d,%s,error logicalChannel=%d\r\n",__LINE__,__FUNCTION__,logicalChannel);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
			fpgaId=1;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);
	/**/
	if(At_fhn_pos_stm_soft_channel_get( cardId,  port,  logicalChannel, &cposPortVc12Channel,&cposPortVc12Timeslot, &channelId, &encapType) == -1){
		printf("%s,#%d,do not have node:card[%d]/[%d]/[%d]\r\n",__FILE__,__LINE__,cardId, cposPort, logicalChannel);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	Udr_cpos_port_channel_framer_type_set(cardId, fpgaId+1,channelId,&arg);
	/**/

	gui4AtStmlogicalchannelEncap[slotId][port][logicalChannel].encapType= *(uint32 *)framerType;		

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return 0;
}


/*
HW_IF_ENCAPTYPE
typedef enum eAtEncapTypeCreateFhn
    {
    cAtHdlcFrmCiscoHdlcCreateFhn,/**< Cisco HDLC 
    cAtHdlcFrmPppCreateFhn,      /**< PPP 
    cAtHdlcFrmFrCreateFhn,       /**< Frame relay 
    cAtEncapAtmCreateFhn         /**< ATM 
    } eAtEncapTypeCreateFhn;
*/
int _udr_if_stm_pos_encap_type_set(uint8 cardId, uint32 cposPort, uint32 logicalChannel, uint32 *encapType)
{
	uint32 slotId = cardId-1;
	uint32 arg = *(uint32 *)encapType;
	uint32 port = cposPort;

	if(gui4AtUdrFhnStmApiDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,port=%d,logicalChannel=%d,encapType=%d\r\n",cardId,cposPort,logicalChannel,*(uint32 *)encapType);
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(cposPort<1||cposPort>AT_POS_STM_MAX_PORT_FHN)	{
		printf("#%d,%s,error cposPort=%d\r\n",__LINE__,__FUNCTION__,cposPort);
		return -1;
	}	

	if(logicalChannel>AT_POS_STM_MAX_LOGICAL_CHANNEL_PERPORT_FHN)	{
		printf("#%d,%s,error logicalChannel=%d\r\n",__LINE__,__FUNCTION__,logicalChannel);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);
	gui4AtStmlogicalchannelEncap[slotId][port][logicalChannel].encapType= arg;		
	POS_AT_UDR_SEM_GIVE(cardId-1);
	return 0;
}


int _udr_if_stm_pos_encap_type_get(uint8 cardId, uint32 cposPort, uint32 logicalChannel, uint32 *encapType)
{
	uint32 slotId = cardId-1;
	uint32 port = cposPort;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(cposPort<1||cposPort>AT_POS_STM_MAX_PORT_FHN)
	{
		printf("#%d,%s,error cposPort=%d\r\n",__LINE__,__FUNCTION__,cposPort);
		return -1;
	}	

	if(logicalChannel>AT_POS_STM_MAX_LOGICAL_CHANNEL_PERPORT_FHN)
	{
		printf("#%d,%s,error logicalChannel=%d\r\n",__LINE__,__FUNCTION__,logicalChannel);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);
	*(uint32 *)encapType = gui4AtStmlogicalchannelEncap[slotId][port][logicalChannel].encapType;		
	POS_AT_UDR_SEM_GIVE(cardId-1);
	return 0;
}

/*
HW_IF_POSADDCHANNEL

typedef struct tAtEthVlanTagFhn
    {
    uint8 priority; 
    uint8 cfi;      
    word vlanId;    [0..4095] VLAN ID 
    }tAtEthVlanTagFhn;

mapType
#define AT_CPOS_STM1_MAPPING			1 
#define AT_CPOS_STM4_MAPPING			2 
#define AT_POS_STM1_MAPPING				3 
#define AT_POS_STM4_MAPPING				4 
*/

int _udr_if_stm_pos_add_channel(uint8 cardId, uint32 cposPort, uint32 logicalChannel, uint32 cposPortVc12Ch, uint32 mapType)
{
	tAtTimeModeFhn timeModen;
	uint32 arg;
	tAtPdhDs0PortFhn E1TsPort={0};
	int ret=0;
	uint32 channelId=0,encapType=0,flowId=0,cposPortVc12Channel=0,cposPortVc12Timeslot=0;
	uint32 fpgaId = (cposPort > 4)?1:0;
	uint32 port=cposPort;
	
	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(cposPort<1||cposPort>AT_POS_STM_MAX_PORT_FHN)
	{
		printf("#%d,%s,error cposPort=%d\r\n",__LINE__,__FUNCTION__,cposPort);
		return -1;
	}	

	if(logicalChannel>AT_POS_STM_MAX_LOGICAL_CHANNEL_PERPORT_FHN)
	{
		printf("#%d,%s,error logicalChannel=%d\r\n",__LINE__,__FUNCTION__,logicalChannel);
		return -1;
	}

	if((mapType<AT_CPOS_STM1_MAPPING)||(mapType>AT_CPOS_STM4_E3_MAPPING)){
		printf("#%d,%s,error mapType=%d\r\n",__LINE__,__FUNCTION__,mapType);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
			fpgaId=1;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	//printf("card[%d]cposPort[%d]port[%d]logicalChannel[%d]\r\n",cardId, cposPort,port, logicalChannel);


	POS_AT_UDR_SEM_TAKE(cardId-1,10);
	if(gui4AtStmlogicalchannelEncap[cardId-1][port][logicalChannel].framerType == cAtPdhE1Frm){
		if((mapType == AT_CPOS_STM1_MAPPING)||(mapType == AT_CPOS_STM4_MAPPING)){
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return 0;
		}
	}

	/*  不允许存在该logicalchannel 没有删除就配置，必须先把以前的删除*/
	if(At_fhn_pos_stm_soft_channel_get( cardId,  port,  logicalChannel, &cposPortVc12Channel,&cposPortVc12Timeslot, &channelId, &encapType) == 0){
		printf("%s,#%d,you should del card[%d]ce1port[%d]logicalChannel[%d]\r\n",__FILE__,__LINE__,cardId, cposPort, logicalChannel);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}
	/**/


	if(At_fhn_pos_stm_soft_channel_set(cardId,port,logicalChannel,cposPortVc12Ch) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	if(At_fhn_pos_stm_soft_channel_get(cardId, port, logicalChannel, &cposPortVc12Channel, &cposPortVc12Timeslot, &channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret = Udr_cpos_port_channel_create(cardId, fpgaId+1, channelId, &encapType);
	/*if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		goto lable;
	}*/

	if((logicalChannel != 0)&&(mapType != AT_CPOS_STM4_155_MAPPING)&&(mapType != AT_CPOS_STM4_E3_MAPPING)){
		arg = gui4AtStmlogicalchannelEncap[cardId-1][port][logicalChannel].framerType;
		ret = Udr_cpos_e1_framer_mode_set (cardId, port, cposPortVc12Channel, &arg);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			goto lable;
		}

		timeModen.TimeModen = gui4AtStmlogicalchannelEncap[cardId-1][port][logicalChannel].timeModen;
		timeModen.refPort = gui4AtStmlogicalchannelEncap[cardId-1][port][logicalChannel].refPort;
		ret = Udr_cpos_e1_clock_mode_set (cardId, port, cposPortVc12Channel, &timeModen);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			goto lable;
		}
	}

	switch(mapType){
		case AT_POS_STM1_MAPPING :
			ret = Udr_pos_stm1_port_channel_bind_port(cardId, channelId, port);
			if(ret !=0){
				printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
				goto lable;
			}
			break;
			
		case AT_POS_STM4_MAPPING :
			ret = Udr_pos_stm4_vc4c_port_channel_bind_port(cardId, port,channelId);
			if(ret !=0){
				printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
				goto lable;
			}
			break;

		case AT_CPOS_STM1_MAPPING:
		case AT_CPOS_STM4_MAPPING:
			ret = Udr_cpos_port_channel_bind_port(cardId, channelId, port, cposPortVc12Channel);
			if(ret !=0){
				printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
				goto lable;
			}
			break;

		case AT_CPOS_STM4_155_MAPPING:
			ret = Udr_pos_stm4_port_channel_bind_port(cardId, port,channelId,cposPortVc12Channel);
			if(ret !=0){
				printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
				goto lable;
			}
			break;

		case AT_CPOS_STM4_E3_MAPPING:
			ret = Udr_cpos_port_channel_e3_bind_port(cardId, channelId, port, cposPortVc12Channel);
			if(ret !=0){
				printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
				goto lable;
			}
			break;

		default:
			printf("#%d,%s,error mapType=%d\r\n",__LINE__,__FUNCTION__,mapType);
			goto lable;;
			
	}

	/*fcs16
	arg = 1;
	ret = Udr_cpos_port_channel_fcs(cardId,fpgaId+1, channelId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		goto lable;
	}
	*/
	
	arg = 1;/*enable channel tx*/
	ret = Udr_cpos_port_channel_txenable(cardId,fpgaId+1, channelId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		goto lable;
	}

	arg = 1;/*enable channel rx*/
	ret = Udr_cpos_port_channel_rxenable(cardId,fpgaId+1, channelId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		goto lable;
	}
	
	arg = 5;/*cAtPppLinkPhaseNetworkActive*/
	ret = Udr_cpos_port_channel_ppp_enable(cardId,fpgaId+1, channelId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		goto lable;
	}

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return 0;

lable:
	At_fhn_pos_stm_soft_channel_del(cardId,port,logicalChannel);
	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;
}



/* HW_IF_DS1CHANNELTIMESLOT */
int _udr_if_stm_pos_add_channel_timeslot(uint8 cardId, uint32 cposPort, uint32 logicalChannel, uint32 cposPortVc12Ch, uint32 cposPortVc12ChTimeslot)
{
	uint32 arg;
	tAtPdhDs0PortFhn E1TsPort={0};
	int ret=0;
	uint32 channelId=0,encapType=0,flowId=0,cposPortVc12Channel=0,cposPortVc12Timeslot=0;
	uint32 fpgaId = (cposPort > 4)?1:0;
	uint32 port = cposPort;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(cposPort<1||cposPort>AT_POS_STM_MAX_PORT_FHN)
	{
		printf("#%d,%s,error cposPort=%d\r\n",__LINE__,__FUNCTION__,cposPort);
		return -1;
	}	

	if(logicalChannel>AT_POS_STM_MAX_LOGICAL_CHANNEL_PERPORT_FHN)
	{
		printf("#%d,%s,error logicalChannel=%d\r\n",__LINE__,__FUNCTION__,logicalChannel);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
			fpgaId=1;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);
	/*  不允许存在该logicalchannel 没有删除就配置，必须先把以前的删除*/
	if(At_fhn_pos_stm_soft_channel_get( cardId,  port,  logicalChannel, &cposPortVc12Channel,&cposPortVc12Timeslot, &channelId, &encapType) == 0){
		printf("%s,#%d,you should del card[%d]ce1port[%d]logicalChannel[%d]\r\n",__FILE__,__LINE__,cardId, port, logicalChannel);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}
	/**/
	
	if(At_fhn_pos_stm_soft_channel_set(cardId,port,logicalChannel,cposPortVc12Ch) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	if(At_fhn_pos_stm_soft_channel_timeslot_set(cardId,  port,  logicalChannel, cposPortVc12Ch,cposPortVc12ChTimeslot) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	if(At_fhn_pos_stm_soft_channel_get( cardId,  port,  logicalChannel, &cposPortVc12Channel, &cposPortVc12Timeslot, &channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret = Udr_cpos_port_channel_create(cardId, fpgaId+1, channelId, &encapType);
	/*if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		goto lable;
	}*/

	arg = cAtPdhE1Frm;
	ret = _udr_if_stm_pos_line_type (cardId, port, logicalChannel,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		goto lable;
	}


 	ret = Udr_cpos_ce1_create_port_ts(cardId, port, cposPortVc12Channel, &cposPortVc12Timeslot);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		goto lable;
	}

 	ret = Udr_cpos_ce1_channel_bind_port_ts(cardId, port, cposPortVc12Channel, channelId, &cposPortVc12Timeslot);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		goto lable;
	}
	

	arg = 1;/*enable channel tx*/
	ret = Udr_cpos_port_channel_txenable(cardId,fpgaId+1,channelId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		goto lable;
	}

	arg = 1;/*enable channel rx*/
	ret = Udr_cpos_port_channel_rxenable(cardId,fpgaId+1,channelId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		goto lable;
	}

	
	arg = 5;/*cAtPppLinkPhaseNetworkActive*/
	ret = Udr_cpos_port_channel_ppp_enable(cardId,fpgaId+1,channelId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		goto lable;
	}

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;

lable:
	At_fhn_pos_stm_soft_channel_del(cardId,port,logicalChannel);
	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;
	
}



int _udr_if_stm_pos_channel_acl_add(uint8 cardId, uint32 cposPort, uint32 logicalChannel, uint8 *dmac, 
								tAtEthVlanTagFhn *ouputvlan,tAtEthVlanTagFhn *inputvlan)
{
	uint32 arg;
	tAtPdhDs0PortFhn E1TsPort={0};
	int ret=0;
	uint32 channelId=0,encapType=0,flowId=0,cposPortVc12Channel=0,cposPortVc12Timeslot=0;
	uint32 fpgaId = (cposPort > 4)?1:0;
	uint32 port = cposPort;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(cposPort<1||cposPort>AT_POS_STM_MAX_PORT_FHN)
	{
		printf("#%d,%s,error cposPort=%d\r\n",__LINE__,__FUNCTION__,cposPort);
		return -1;
	}	

	if(logicalChannel>AT_POS_STM_MAX_LOGICAL_CHANNEL_PERPORT_FHN)
	{
		printf("#%d,%s,error logicalChannel=%d\r\n",__LINE__,__FUNCTION__,logicalChannel);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
			fpgaId=1;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	if(At_fhn_pos_stm_soft_channel_get( cardId,  port,  logicalChannel, &cposPortVc12Channel, &cposPortVc12Timeslot,&channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	
	flowId = channelId;
	arg=0;/*cAtEthFlowNpoPpp*/
	ret = Udr_cpos_port_flow_create(cardId,fpgaId+1,flowId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret = Udr_cpos_port_flow_egress_dmac_set(cardId,fpgaId+1,flowId,dmac);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	ret = Udr_cpos_port_channel_bind_flow(cardId,fpgaId+1,channelId,flowId);/*绑定业务通道到流ID*/
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	ret = Udr_cpos_port_flow_egress_vlan_set(cardId,fpgaId+1,1,flowId, ouputvlan);/*设置流标示的出口VLAN  */
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	ret = Udr_cpos_port_flow_set_vlan(cardId, fpgaId+1,1, flowId, inputvlan);/*流标示的分类规则,只能基于VLAN */
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	ret =Udr_cpos_port_oam_channel_mode(cardId, fpgaId+1,channelId);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	ret =Udr_cpos_port_oam_channel_egress_dmac_set(cardId,fpgaId+1,channelId,dmac);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	ret =Udr_cpos_port_oam_channel_egress_vlan_set(cardId, fpgaId+1,1, channelId, ouputvlan);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}
	
	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;
}



int _udr_if_stm_pos_channel_acl_del(uint8 cardId, uint32 cposPort, uint32 logicalChannel)
{
	uint32 arg;
	int ret=0;
	uint32 channelId=0,encapType=0,flowId=0,cposPortVc12Channel=0,cposPortVc12Timeslot=0;
	uint32 fpgaId = (cposPort > 4)?1:0;
	uint32 port = cposPort;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(cposPort<1||cposPort>AT_POS_STM_MAX_PORT_FHN)
	{
		printf("#%d,%s,error cposPort=%d\r\n",__LINE__,__FUNCTION__,cposPort);
		return -1;
	}	

	if(logicalChannel>AT_POS_STM_MAX_LOGICAL_CHANNEL_PERPORT_FHN)
	{
		printf("#%d,%s,error logicalChannel=%d\r\n",__LINE__,__FUNCTION__,logicalChannel);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
			fpgaId=1;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	if(At_fhn_pos_stm_soft_channel_get(cardId, port, logicalChannel, &cposPortVc12Channel, &cposPortVc12Timeslot,&channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	arg = 0;/*disable channel tx*/
	ret = Udr_cpos_port_channel_txenable(cardId,fpgaId+1,channelId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}
	
	arg = 0;/*disable channel rx*/
	ret = Udr_cpos_port_channel_rxenable(cardId,fpgaId+1,channelId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}
	
	arg = 1;/*cAtPppLinkPhaseDead*/
	ret = Udr_cpos_port_channel_ppp_enable(cardId,fpgaId+1,channelId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}
	
	flowId = channelId;
	ret = Udr_cpos_port_channel_unbind_flow(cardId,fpgaId+1,channelId,&flowId);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	flowId = channelId;
	ret = Udr_cpos_port_flow_del(cardId,fpgaId+1,flowId,&encapType);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;
}


/*
HW_IF_DS1DELCHANNEL,
*/
int _udr_if_stm_pos_del_channel(uint8 cardId, uint32 cposPort, uint32 logicalChannel)
{

	int ret=0;
	uint32 arg=0;
	uint32 channelId=0,encapType=0,flowId=0,cposPortVc12Channel=0,cposPortVc12Timeslot=0;
	uint32 fpgaId = (cposPort > 4)?1:0;
	uint32 port = cposPort;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(cposPort<1||cposPort>AT_POS_STM_MAX_PORT_FHN)
	{
		printf("#%d,%s,error cposPort=%d\r\n",__LINE__,__FUNCTION__,cposPort);
		return -1;
	}	

	if(logicalChannel>AT_POS_STM_MAX_LOGICAL_CHANNEL_PERPORT_FHN)
	{
		printf("#%d,%s,error logicalChannel=%d\r\n",__LINE__,__FUNCTION__,logicalChannel);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
			fpgaId=1;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	if(At_fhn_pos_stm_soft_channel_get( cardId,  port,  logicalChannel, &cposPortVc12Channel, &cposPortVc12Timeslot,&channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}
	
	if(cposPortVc12Timeslot){
	 	ret = Udr_cpos_ce1_channel_unbind_port_ts(cardId,port,cposPortVc12Channel,channelId, &cposPortVc12Timeslot);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}

	 	ret = Udr_cpos_ce1_delete_port_ts(cardId,port,cposPortVc12Channel, &cposPortVc12Timeslot);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}
	}else{
		arg = port;
		ret = Udr_cpos_port_channel_unbind_port(cardId,fpgaId+1,channelId,&arg);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}
	}

	arg = encapType;
	ret = Udr_cpos_port_channel_delete(cardId,fpgaId+1,channelId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret = At_fhn_pos_stm_soft_channel_del(cardId, port, logicalChannel);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;
}



int _udr_if_stm_pos_trunk_acl_add(uint8 cardId, uint32 cposPort, uint32 cposPortVc12Ch, uint32 logicalChannel, uint32 trunkId,
												uint8 *dmac, tAtEthVlanTagFhn *ouputvlan,tAtEthVlanTagFhn *inputvlan)
{
	int ret=0;
	uint32 arg=0;
	uint32 channelId=0,encapType=0,flowId=0,cposPortVc12Channel=0,cposPortVc12Timeslot=0;
	uint32 fpgaId = (cposPort > 4)?1:0;
 	uint32 port = cposPort;

	if((cardId>2) || (cardId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cardId=%d\r\n",cardId);
		return -1;
	}

	if(cposPort<1||cposPort>AT_POS_STM_MAX_PORT_FHN)
	{
		printf("#%d,%s,error cposPort=%d\r\n",__LINE__,__FUNCTION__,cposPort);
		return -1;
	}

	if((trunkId>AT_POS_STM_MAX_MLPPP_BUNDLE_FHN) || (trunkId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error trunkId=%d\r\n",trunkId);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
			fpgaId=1;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	if(At_fhn_pos_stm_soft_channel_get( cardId,  port,  logicalChannel, &cposPortVc12Channel, &cposPortVc12Timeslot,&channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	flowId = AT_POS_STM_MAX_HW_CHANNEL_FHN+trunkId;
	arg=0;
	ret = Udr_cpos_port_flow_create(cardId,fpgaId+1,flowId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret = Udr_cpos_port_flow_egress_dmac_set(cardId,fpgaId+1,flowId,dmac);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	ret = Udr_cpos_port_flow_egress_vlan_set(cardId,fpgaId+1,1,flowId, ouputvlan);/*设置流标示的出口VLAN  */
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	ret = Udr_cpos_port_flow_set_vlan(cardId, fpgaId+1, 1, flowId, inputvlan);/*流标示的分类规则,只能基于VLAN */
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	ret = Udr_cpos_mlppp_bundle_bind_flow(cardId, fpgaId+1, trunkId,flowId);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;
}


int _udr_if_stm_pos_trunk_acl_del(uint8 cardId, uint32 cposPort, uint32 cposPortVc12Ch, uint32 logicalChannel, uint32 trunkId)
{
	uint32 arg;
	tAtPdhDs0PortFhn E1TsPort={0};
	int ret=0;
	uint32 channelId=0,encapType=0,flowId=0,timeslotMap=0;
	uint32 fpgaId = (cposPort > 4)?1:0;
	uint32 port = cposPort;

	if((cardId>2) || (cardId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cardId=%d\r\n",cardId);
		return -1;
	}

	if(cposPort<1||cposPort>AT_POS_STM_MAX_PORT_FHN)
	{
		printf("#%d,%s,error cposPort=%d\r\n",__LINE__,__FUNCTION__,cposPort);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
			fpgaId=1;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);
	flowId = AT_POS_STM_MAX_HW_CHANNEL_FHN+trunkId;
	ret = Udr_cpos_mlppp_bundle_unbind_flow(cardId, fpgaId+1, trunkId,flowId);/*绑定业务通道到流ID*/
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	encapType = 0;/*cAtEthFlowNpoPpp*/
	ret = Udr_cpos_port_flow_del(cardId,fpgaId+1,flowId,&encapType);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;
}

/*
HW_IF_TRUNKADDMEMBER
*/
int	_udr_if_stm_pos_trunk_add_member(uint8 cardId, uint32 cposPort, uint32 cposPortVc12Ch, uint32 logicalChannel, uint32 trunkId,
												uint8 *dmac, tAtEthVlanTagFhn *ouputvlan,tAtEthVlanTagFhn *inputvlan, tAtEthVlanTagFhn *OamInputvlan)
{
	int ret=0;
	uint32 arg=0;
	uint32 channelId=0,encapType=0,flowId=0,cposPortVc12Channel=0,cposPortVc12Timeslot=0;
	uint32 fpgaId = (cposPort > 4)?1:0;
	uint32 port = cposPort;

	if((cardId>2) || (cardId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cardId=%d\r\n",cardId);
		return -1;
	}

	if((trunkId>AT_POS_STM_MAX_MLPPP_BUNDLE_FHN) || (trunkId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error trunkId=%d\r\n",trunkId);
		return -1;
	}

	if(cposPort<1||cposPort>AT_POS_STM_MAX_PORT_FHN)
	{
		printf("#%d,%s,error cposPort=%d\r\n",__LINE__,__FUNCTION__,cposPort);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
			fpgaId=1;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);
	if(At_fhn_pos_stm_soft_channel_get( cardId,  port,  logicalChannel, &cposPortVc12Channel, &cposPortVc12Timeslot,&channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}
	
	ret = Udr_cpos_mlppp_bundle_add_channel(cardId, fpgaId+1, trunkId, channelId);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	gu4AtStmBundleMemberRecord[cardId-1][fpgaId][trunkId]++;
	printf("card[%d],fpgaId[%d],trunk[%d],mem[%d]\r\n", cardId, fpgaId,trunkId, gu4AtStmBundleMemberRecord[cardId-1][fpgaId][trunkId]);
	
	if(gu4AtStmBundleMemberRecord[cardId-1][fpgaId][trunkId] == 1)
	{
		ret = udr_pos_stm_trunk_acl_add(cardId, cposPort, cposPortVc12Ch,logicalChannel, trunkId, dmac, ouputvlan, inputvlan);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}
	}

	/* added by ssq 20131114 */
	/*
	flowId = channelId;
	arg=0;
	ret = Udr_cpos_port_flow_create(cardId,fpgaId+1,flowId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	ret = Udr_cpos_port_channel_bind_flow(cardId,fpgaId+1,channelId,flowId);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}
	
	ret =Udr_cpos_port_oam_channel_mode(cardId,fpgaId+1, channelId);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	ret =Udr_cpos_port_oam_channel_egress_dmac_set(cardId,fpgaId+1,channelId,dmac);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	ret =Udr_cpos_port_oam_channel_egress_vlan_set(cardId, fpgaId+1,1, channelId, OamInputvlan);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	ret =Udr_cpos_port_oam_channel_ingress_vlan_set(cardId, fpgaId+1,1, channelId, OamInputvlan);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}
	*/
	/* end added */


	arg=1;
	ret = Udr_cpos_mlppp_bundle_enable(cardId, fpgaId+1, trunkId, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return 0;
}


/*
HW_IF_TRUNKDELMEMBER
*/
int	_udr_if_stm_pos_trunk_del_member(uint8 cardId, uint32 cposPort, uint32 logicalChannel, uint32 trunkId)
{
	int ret=0;
	uint32 arg=0;
	uint32 channelId=0,encapType=0,flowId=0,cposPortVc12Channel=0,cposPortVc12Timeslot=0;
	uint32 fpgaId = (cposPort > 4)?1:0;
	uint32 port = cposPort;

	if((cardId>2) || (cardId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cardId=%d\r\n",cardId);
		return -1;
	}

	if((trunkId>AT_POS_STM_MAX_MLPPP_BUNDLE_FHN) || (trunkId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error trunkId=%d\r\n",trunkId);
		return -1;
	}

	if(cposPort<1||cposPort>AT_POS_STM_MAX_PORT_FHN)
	{
		printf("#%d,%s,error cposPort=%d\r\n",__LINE__,__FUNCTION__,cposPort);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
			fpgaId=1;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	if(At_fhn_pos_stm_soft_channel_get(cardId, port, logicalChannel, &cposPortVc12Channel, &cposPortVc12Timeslot,&channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}
	
	ret =  Udr_cpos_mlppp_bundle_delete_channel(cardId,fpgaId+1, trunkId,channelId);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}
	
	gu4AtStmBundleMemberRecord[cardId-1][fpgaId][trunkId]--;
	printf("card[%d],fpgaId[%d],trunk[%d],mem[%d]\r\n", cardId, fpgaId,trunkId, gu4AtStmBundleMemberRecord[cardId-1][fpgaId][trunkId]);

	/* added by ssq 20131114 */
	/*
	flowId = channelId;
	ret = Udr_cpos_port_channel_unbind_flow(cardId,fpgaId+1,channelId,&flowId);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	
	encapType = 0;
	ret = Udr_cpos_port_flow_del(cardId,fpgaId+1,flowId,&encapType);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}
	*/
	/* added by ssq 20131114 */

	if(gu4AtStmBundleMemberRecord[cardId-1][fpgaId][trunkId] == 0){
		ret = udr_pos_stm_trunk_acl_del(cardId, cposPort, cposPortVc12Channel, logicalChannel, trunkId);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}
	}

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return 0;
}


/*
cardId = 1,2
cposPort = 1-8, 
linkStatus:1 =  linkUp 
linkStatus:0 =  linkDown
*/
int _udr_at_stm_port_los_status_get(uint8 cardId, uint32 cposPort, uint32 *linkStatus)
{
	uint8 *EncapTypeBuf;
	AtSdhLine sdhModuleLine;
	eAtRet  ret;
	AtEncapChannel encapChannel;
	uint32 i,alarmStat=0;
	uint8 slotId=cardId;
	uint32 port = cposPort;
	
	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	sdhModuleLine = (AtSdhLine)AtFhnLineGet(slotId,port);
	if(sdhModuleLine == NULL){
		*linkStatus=0;
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return 0;
	}

	alarmStat = AtChannelAlarmGet((AtChannel)sdhModuleLine);

	if ((alarmStat & cCmdSdhLineAlarmTypeValFhn[0])||(alarmStat & cCmdSdhLineAlarmTypeValFhn[2])){
		*linkStatus=0;
	}else{
		*linkStatus=1;
	}

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return 0;
}

int _udr_if_stm_fpga_version_get(uint8 cardId, uint32* fpgaVersion)
{
	uint32 channelId=0,encapType=0,flowId=0,timeslotMap=0;

	if((cardId>2) || (cardId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cardId=%d\r\n",cardId);
		return -1;
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);
	 At_register_rd(cardId,1,2,fpgaVersion);
	POS_AT_UDR_SEM_GIVE(cardId-1);

	return 0;
}


/*
_test_udr_if_stm1_cpos_add_by_port 1,1
_test_udr_if_stm1_cpos_add_by_port 1,5
*/

int _test_udr_if_stm1_cpos_add_by_port(uint32 cardId, uint32 cposPort)
{
	tAtEthVlanTagFhn ouputvlan,inputvlan;
	tAtTimeModeFhn clock;
	int ret=0;
	uint32 cposPortVc12Ch=0,logicalChannel=0;
	uint8 smac1[]={0x00,0x04,0x67,0x00,0x00,0x66};
	uint8 smac2[]={0x00,0x04,0x67,0x00,0x00,0x77};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint32 arg=0;

	/* stm1 moden */
	arg = 0;
	ret = _udr_if_stm_pos_rate_set(cardId, cposPort, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
	
	/* sdh moden */
	arg = 0;
	ret = _udr_if_stm_pos_medium_type(cardId, cposPort, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_at_stm_pos_card_smac_set(cardId, smac1,smac2);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_if_stm_pos_sdh_aug_mapping(cardId, cposPort, AT_CPOS_STM1_MAPPING);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	for(cposPortVc12Ch=1;cposPortVc12Ch<=63;cposPortVc12Ch++)
	{
		arg = 1;/*cAtHdlcFrmPppCreate*/
		logicalChannel = cposPortVc12Ch;
		ret = _udr_if_stm_pos_encap_type_set(cardId,cposPort,logicalChannel, &arg);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_stm_pos_add_channel(cardId,cposPort,logicalChannel,cposPortVc12Ch,AT_CPOS_STM1_MAPPING);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ouputvlan.cfi=0;
		ouputvlan.priority=0;
		ouputvlan.vlanId=cposPortVc12Ch;
		inputvlan.cfi=0;
		inputvlan.priority=0;
		inputvlan.vlanId = cposPortVc12Ch;
		ret = _udr_if_stm_pos_channel_acl_add(cardId, cposPort, logicalChannel, dmac, &ouputvlan, &inputvlan);		
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		clock.TimeModen = 1;/*cAtTimingModeSys*/
		clock.refPort = 1;/* only used in slave*/
		ret = _udr_if_stm_pos_txclock_src_config(cardId,cposPort,logicalChannel,&clock);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}
		
	}

	return 0;
}


/*
_test_udr_if_stm1_cpos_del_by_port 1,1
_test_udr_if_stm1_cpos_del_by_port 1,5
*/
int _test_udr_if_stm1_cpos_del_by_port(uint32 cardId, uint32 cposPort)
{
	int ret=0;
	uint32 cposPortVc12Ch=0,logicalChannel=0;

	for(cposPortVc12Ch=1;cposPortVc12Ch<=63;cposPortVc12Ch++)
	{
		logicalChannel = cposPortVc12Ch;
		ret = _udr_if_stm_pos_channel_acl_del(cardId, cposPort, logicalChannel);	
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_stm_pos_del_channel(cardId,cposPort,logicalChannel);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}
		
	}

	return 0;

}

/*
_test_udr_if_stm1_pos_add_by_port 1,1,1
_test_udr_if_stm1_pos_add_by_port 1,5,2
*/
int _test_udr_if_stm1_pos_add_by_port(uint32 cardId, uint32 cposPort, uint32 vlan)
{
	tAtEthVlanTagFhn ouputvlan,inputvlan;
	tAtTimeModeFhn clock;
	int ret=0;
	uint32 cposPortVc12Ch=0,logicalChannel=0;
	uint8 smac1[]={0x00,0x04,0x67,0x00,0x00,0x66};
	uint8 smac2[]={0x00,0x04,0x67,0x00,0x00,0x77};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint32 arg=0;

	arg = 0;
	ret = _udr_if_stm_pos_rate_set(cardId, cposPort, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
	
	/* sdh moden */
	arg = 0;
	ret = _udr_if_stm_pos_medium_type(cardId, cposPort, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_at_stm_pos_card_smac_set(cardId, smac1,smac2);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_if_stm_pos_sdh_aug_mapping(cardId, cposPort, AT_POS_STM1_MAPPING);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}


	arg = 1;
	logicalChannel = 0;
	ret = _udr_if_stm_pos_encap_type_set(cardId,cposPort,logicalChannel, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	logicalChannel=0;
	ret = _udr_if_stm_pos_add_channel(cardId,cposPort,logicalChannel,cposPortVc12Ch,AT_POS_STM1_MAPPING);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ouputvlan.cfi=0;
	ouputvlan.priority=7;
	ouputvlan.vlanId=vlan;
	inputvlan.cfi=0;
	inputvlan.priority=7;
	inputvlan.vlanId = vlan;
	ret = _udr_if_stm_pos_channel_acl_add(cardId, cposPort, logicalChannel, dmac, &ouputvlan, &inputvlan);		
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

#if	0
	clock.TimeModen = 1;
	clock.refPort = 1;
	ret = _udr_if_stm_pos_txclock_src_config(cardId,cposPort,logicalChannel,&clock);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
#endif		
	return 0;

}


/*
_test_udr_if_stm1_pos_del_by_port 1,1
_test_udr_if_stm1_pos_del_by_port 1,5
*/
int _test_udr_if_stm1_pos_del_by_port(uint32 cardId, uint32 cposPort)
{
	int ret=0;
	uint32 cposPortVc12Ch=0,logicalChannel=0;

	ret = _udr_if_stm_pos_channel_acl_del(cardId, cposPort, logicalChannel);	
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_if_stm_pos_del_channel(cardId,cposPort,logicalChannel);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
		
	return 0;

}




/*
_test_udr_if_stm1_cpos_trunk_add_by_ch 1,1,1,1
_test_udr_if_stm1_cpos_trunk_add_by_ch 1,1,2,1
_test_udr_if_stm1_cpos_trunk_add_by_ch 1,1,3,1
_test_udr_if_stm1_cpos_trunk_add_by_ch 1,1,4,1

_test_udr_if_stm1_cpos_trunk_add_by_ch 1,5,1,2
_test_udr_if_stm1_cpos_trunk_add_by_ch 1,5,2,2
_test_udr_if_stm1_cpos_trunk_add_by_ch 1,5,3,2
_test_udr_if_stm1_cpos_trunk_add_by_ch 1,5,4,2
*/
int _test_udr_if_stm1_cpos_trunk_add_by_ch(uint8 cardId, uint32 cposPort, uint32 logicalChannel, uint32 trunkId)
{
	int ret;
	tAtEthVlanTagFhn ouputvlan,inputvlan,oamInputvlan;
	tAtTimeModeFhn clock;
	uint8 smac1[]={0x00,0x04,0x67,0x00,0x00,0x66};
	uint8 smac2[]={0x00,0x04,0x67,0x00,0x00,0x77};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint32 arg=0;
	uint32 channelId=0,encapType=0,flowId=0,cposPortVc12Channel=0,cposPortVc12Timeslot=0;

	ret = _udr_at_stm_pos_card_smac_set(cardId, smac1,smac2);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_if_stm_pos_sdh_aug_mapping(cardId, cposPort, AT_CPOS_STM1_MAPPING);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
	
	arg = 1;/*cAtHdlcFrmPppCreate*/
	ret = _udr_if_stm_pos_encap_type_set(cardId,cposPort,logicalChannel, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	if(At_fhn_pos_stm_soft_channel_get(cardId, cposPort, logicalChannel, &cposPortVc12Channel, &cposPortVc12Timeslot, &channelId, &encapType) == -1){
		/* have not create channel, so creat it*/
		ret = _udr_if_stm_pos_add_channel(cardId,cposPort,logicalChannel,logicalChannel,AT_CPOS_STM1_MAPPING);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}
	}

	clock.TimeModen = 1;/*cAtTimingModeSys*/
	clock.refPort = 1;/* only used in slave*/
	ret = _udr_if_stm_pos_txclock_src_config(cardId,cposPort, logicalChannel,&clock);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ouputvlan.cfi=0;
	ouputvlan.priority=1;
	ouputvlan.vlanId = 3000+trunkId;
	inputvlan.cfi=0;
	inputvlan.priority=0;
	inputvlan.vlanId = 3000+trunkId;
	oamInputvlan.cfi=0;
	oamInputvlan.priority=7;
	oamInputvlan.vlanId = logicalChannel;
	ret = _udr_if_stm_pos_trunk_add_member(cardId, cposPort, cposPortVc12Channel,logicalChannel, trunkId, dmac, &ouputvlan, &inputvlan,&oamInputvlan);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
	
	return 0;

}


/*
_test_udr_if_stm1_cpos_trunk_del_by_ch 1,1,1,1
_test_udr_if_stm1_cpos_trunk_del_by_ch 1,1,2,1
_test_udr_if_stm1_cpos_trunk_del_by_ch 1,1,3,1
_test_udr_if_stm1_cpos_trunk_del_by_ch 1,1,4,1

_test_udr_if_stm1_cpos_trunk_del_by_ch 1,5,1,2
_test_udr_if_stm1_cpos_trunk_del_by_ch 1,5,2,2
_test_udr_if_stm1_cpos_trunk_del_by_ch 1,5,3,2
_test_udr_if_stm1_cpos_trunk_del_by_ch 1,5,4,2
*/
int _test_udr_if_stm1_cpos_trunk_del_by_ch(uint8 cardId, uint32 cposPort, uint32 logicalChannel, uint32 trunkId)
{
	int ret;

	ret = _udr_if_stm_pos_trunk_del_member(cardId, cposPort, logicalChannel, trunkId);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	return 0;

}


/*
_test_udr_if_stm1_pos_trunk_add_by_ch 1,1,0,1
_test_udr_if_stm1_pos_trunk_add_by_ch 1,2,0,1

_test_udr_if_stm1_pos_trunk_add_by_ch 1,5,0,2
_test_udr_if_stm1_pos_trunk_add_by_ch 1,6,0,2

*/
int _test_udr_if_stm1_pos_trunk_add_by_ch(uint8 cardId, uint32 cposPort, uint32 logicalChannel, uint32 trunkId)
{
	int ret;
	tAtEthVlanTagFhn ouputvlan,inputvlan,oamInputvlan;
	tAtTimeModeFhn clock;
	uint8 smac1[]={0x00,0x04,0x67,0x00,0x00,0x66};
	uint8 smac2[]={0x00,0x04,0x67,0x00,0x00,0x77};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint32 arg=0;
	uint32 channelId=0,encapType=0,flowId=0,cposPortVc12Channel=0,cposPortVc12Timeslot=0;

	ret = _udr_at_stm_pos_card_smac_set(cardId, smac1,smac2);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_if_stm_pos_sdh_aug_mapping(cardId, cposPort, AT_POS_STM1_MAPPING);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
	
	arg = 1;/*cAtHdlcFrmPppCreate*/
	ret = _udr_if_stm_pos_encap_type_set(cardId,cposPort,logicalChannel, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	if(At_fhn_pos_stm_soft_channel_get(cardId, cposPort, logicalChannel, &cposPortVc12Channel, &cposPortVc12Timeslot, &channelId, &encapType) == -1){
		/* have not create channel, so creat it*/
		ret = _udr_if_stm_pos_add_channel(cardId,cposPort,logicalChannel,logicalChannel,AT_POS_STM1_MAPPING);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}
	}


	ouputvlan.cfi=0;
	ouputvlan.priority=1;
	ouputvlan.vlanId = 10+trunkId;
	inputvlan.cfi=0;
	inputvlan.priority=0;
	inputvlan.vlanId = 10+trunkId;
	oamInputvlan.cfi=0;
	oamInputvlan.priority=7;
	oamInputvlan.vlanId = 20+trunkId;
	ret = _udr_if_stm_pos_trunk_add_member(cardId, cposPort, cposPortVc12Channel,logicalChannel, trunkId, dmac, &ouputvlan, &inputvlan,&oamInputvlan);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
	
	return 0;

}




/*
_test_udr_if_stm1_cpos_add_ts_by_port 1,1,1,1,0xf,1
_test_udr_if_stm1_cpos_add_ts_by_port 1,5,1,1,0xf,2

_test_udr_if_stm1_cpos_add_ts_by_port 1,1,2,1,0xff0,3
_test_udr_if_stm1_cpos_add_ts_by_port 1,5,2,1,0xff0,4

*/
int _test_udr_if_stm1_cpos_add_ts_by_port(uint8 cardId, uint32 cposPort ,uint32 logicalChannel, uint32 cposPortVc12Ch, uint32 timeSlotBitmap,uint32 vlan)
{
	tAtEthVlanTagFhn ouputvlan,inputvlan;
	tAtTimeModeFhn clock;
	int ret=0;
	uint8 smac1[]={0x00,0x04,0x67,0x00,0x00,0x66};
	uint8 smac2[]={0x00,0x04,0x67,0x00,0x00,0x77};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint32 arg=0;

	/* stm1 moden */
	
	arg = 0;
	ret = _udr_if_stm_pos_rate_set(cardId, cposPort, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
	
	/* sdh moden */
	arg = 0;
	ret = _udr_if_stm_pos_medium_type(cardId, cposPort, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	arg = 9;/*cAtPdhE1Frm*/
	ret = _udr_if_stm_pos_line_type(cardId, cposPort, logicalChannel, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
	
	clock.TimeModen = 1;/*cAtTimingModeSys*/
	clock.refPort = 1;/* only used in slave*/
	ret = _udr_if_stm_pos_txclock_src_config(cardId,cposPort, logicalChannel, &clock);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
	
	arg = 1;/*cAtHdlcFrmPppCreate*/
	ret = _udr_if_stm_pos_encap_type_set(cardId,cposPort,logicalChannel, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	
	ret = _udr_if_stm_pos_sdh_aug_mapping(cardId, cposPort, AT_CPOS_STM1_MAPPING);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
	
	
	ret = _udr_if_stm_pos_add_channel_timeslot(cardId,cposPort,logicalChannel,cposPortVc12Ch,timeSlotBitmap);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ouputvlan.cfi=0;
	ouputvlan.priority=7;
	ouputvlan.vlanId=vlan;
	inputvlan.cfi=0;
	inputvlan.priority=7;
	inputvlan.vlanId = vlan;
	ret = _udr_if_stm_pos_channel_acl_add(cardId, cposPort, logicalChannel, dmac, &ouputvlan, &inputvlan);		
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

}


/*
_test_udr_if_stm1_cpos_del_ts_by_port 1,1,1
_test_udr_if_stm1_cpos_del_ts_by_port 1,5,1

_test_udr_if_stm1_cpos_del_ts_by_port 1,1,2
_test_udr_if_stm1_cpos_del_ts_by_port 1,5,2

*/
int _test_udr_if_stm1_cpos_del_ts_by_port(uint8 cardId, uint32 cposPort ,uint32 logicalChannel)
{
	int ret=0;

	ret = _udr_if_stm_pos_channel_acl_del(cardId, cposPort, logicalChannel);	
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_if_stm_pos_del_channel(cardId,cposPort,logicalChannel);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

}







int _test_udr_if_stm4_cpos_add_by_card(uint32 cardId)
{
	tAtEthVlanTagFhn ouputvlan,inputvlan;
	tAtTimeModeFhn clock;
	int ret=0;
	uint32 cposPortVc12Ch=0,logicalChannel=0;
	uint8 smac1[]={0x00,0x04,0x67,0x00,0x00,0x66};
	uint8 smac2[]={0x00,0x04,0x67,0x00,0x00,0x77};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint32 arg=0;

	/* stm4 moden */
	arg = 1;
	ret = _udr_if_stm_pos_rate_set(cardId, 1, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	/* stm4 moden */
	arg = 1;
	ret = _udr_if_stm_pos_rate_set(cardId, 2, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
	
	/* sdh moden */
	arg = 0;
	ret = _udr_if_stm_pos_medium_type(cardId, 1, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
	
	/* sdh moden */
	arg = 0;
	ret = _udr_if_stm_pos_medium_type(cardId, 2, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_at_stm_pos_card_smac_set(cardId, smac1,smac2);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_if_stm_pos_sdh_aug_mapping(cardId, 1, AT_CPOS_STM4_MAPPING);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_if_stm_pos_sdh_aug_mapping(cardId, 2, AT_CPOS_STM4_MAPPING);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	for(cposPortVc12Ch=1;cposPortVc12Ch<=252;cposPortVc12Ch++)
	{
		arg = 1;/*cAtHdlcFrmPppCreate*/
		logicalChannel = cposPortVc12Ch;
		ret = _udr_if_stm_pos_encap_type_set(cardId,1,logicalChannel, &arg);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_stm_pos_encap_type_set(cardId,2,logicalChannel, &arg);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_stm_pos_add_channel(cardId,1,logicalChannel,cposPortVc12Ch,AT_CPOS_STM4_MAPPING);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_stm_pos_add_channel(cardId,2,logicalChannel,cposPortVc12Ch,AT_CPOS_STM4_MAPPING);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ouputvlan.cfi=0;
		ouputvlan.priority=7;
		ouputvlan.vlanId=cposPortVc12Ch;
		inputvlan.cfi=0;
		inputvlan.priority=7;
		inputvlan.vlanId = cposPortVc12Ch;
		ret = _udr_if_stm_pos_channel_acl_add(cardId, 1, logicalChannel, dmac, &ouputvlan, &inputvlan);		
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_stm_pos_channel_acl_add(cardId, 2, logicalChannel, dmac, &ouputvlan, &inputvlan);		
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		clock.TimeModen = 1;/*cAtTimingModeSys*/
		clock.refPort = 1;/* only used in slave*/
		ret = _udr_if_stm_pos_txclock_src_config(cardId,1,logicalChannel,&clock);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_stm_pos_txclock_src_config(cardId,2,logicalChannel,&clock);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}
		
	}

}


int _test_udr_if_stm4_cpos_del_by_card(uint32 cardId)
{
	int ret=0;
	uint32 cposPortVc12Ch=0,logicalChannel=0;

	for(cposPortVc12Ch=1;cposPortVc12Ch<=252;cposPortVc12Ch++)
	{
		logicalChannel = cposPortVc12Ch;
		ret = _udr_if_stm_pos_channel_acl_del(cardId, 1, logicalChannel);	
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_stm_pos_channel_acl_del(cardId, 2, logicalChannel);	
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_stm_pos_del_channel(cardId,1,logicalChannel);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_stm_pos_del_channel(cardId,2,logicalChannel);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}
		
	}

}

int _test_udr_if_stm4_cpos_stm1(uint32 cardId)
{
	tAtEthVlanTagFhn ouputvlan,inputvlan;
	tAtTimeModeFhn clock;
	int ret=0;
	uint32 cposPortVc4Ch=0,logicalChannel=0;
	uint8 smac1[]={0x00,0x04,0x67,0x00,0x00,0x66};
	uint8 smac2[]={0x00,0x04,0x67,0x00,0x00,0x77};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint32 arg=0;

	/* stm4 moden */
	arg = 1;
	ret = _udr_if_stm_pos_rate_set(cardId, 1, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	/* stm4 moden */
	arg = 1;
	ret = _udr_if_stm_pos_rate_set(cardId, 2, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
	
	/* sdh moden */
	arg = 0;
	ret = _udr_if_stm_pos_medium_type(cardId, 1, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
	
	/* sdh moden */
	arg = 0;
	ret = _udr_if_stm_pos_medium_type(cardId, 2, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_at_stm_pos_card_smac_set(cardId, smac1,smac2);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_if_stm_pos_sdh_aug_mapping(cardId, 1, AT_CPOS_STM4_155_MAPPING);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_if_stm_pos_sdh_aug_mapping(cardId, 2, AT_CPOS_STM4_155_MAPPING);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	for(cposPortVc4Ch=1;cposPortVc4Ch<=4;cposPortVc4Ch++)
	{
		arg = 1;/*cAtHdlcFrmPppCreate*/
		logicalChannel = cposPortVc4Ch;
		ret = _udr_if_stm_pos_encap_type_set(cardId,1,logicalChannel, &arg);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_stm_pos_encap_type_set(cardId,2,logicalChannel, &arg);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_stm_pos_add_channel(cardId,1,logicalChannel,cposPortVc4Ch,AT_CPOS_STM4_155_MAPPING);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_stm_pos_add_channel(cardId,2,logicalChannel,cposPortVc4Ch,AT_CPOS_STM4_155_MAPPING);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ouputvlan.cfi=0;
		ouputvlan.priority=7;
		ouputvlan.vlanId=cposPortVc4Ch;
		inputvlan.cfi=0;
		inputvlan.priority=7;
		inputvlan.vlanId = cposPortVc4Ch;
		ret = _udr_if_stm_pos_channel_acl_add(cardId, 1, logicalChannel, dmac, &ouputvlan, &inputvlan);		
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_stm_pos_channel_acl_add(cardId, 2, logicalChannel, dmac, &ouputvlan, &inputvlan);		
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		#if	0
			clock.TimeModen = 1;/*cAtTimingModeSys*/
			clock.refPort = 1;/* only used in slave*/
			ret = _udr_if_stm_pos_txclock_src_config(cardId,1,logicalChannel,&clock);
			if(ret !=0){
				printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
				return -1;
			}

			ret = _udr_if_stm_pos_txclock_src_config(cardId,2,logicalChannel,&clock);
			if(ret !=0){
				printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
				return -1;
			}
		#endif
	}

}

int _test_udr_if_stm4_cpos_stm1_del(uint32 cardId)
{
	int ret=0;
	uint32 cposPortVc4Ch=0,logicalChannel=0;

	for(cposPortVc4Ch=1;cposPortVc4Ch<=4;cposPortVc4Ch++)
	{
		logicalChannel = cposPortVc4Ch;
		ret = _udr_if_stm_pos_channel_acl_del(cardId, 1, logicalChannel);	
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_stm_pos_channel_acl_del(cardId, 2, logicalChannel);	
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_stm_pos_del_channel(cardId,1,logicalChannel);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_stm_pos_del_channel(cardId,2,logicalChannel);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}
		
	}

}

int _test_udr_if_stm4_cpos_e3(uint32 cardId)
{
	tAtEthVlanTagFhn ouputvlan,inputvlan;
	tAtTimeModeFhn clock;
	int ret=0;
	uint32 cposPortVc4Ch=0,logicalChannel=0;
	uint8 smac1[]={0x00,0x04,0x67,0x00,0x00,0x66};
	uint8 smac2[]={0x00,0x04,0x67,0x00,0x00,0x77};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint32 arg=0;

	/* stm4 moden */
	arg = 1;
	ret = _udr_if_stm_pos_rate_set(cardId, 1, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	/* stm4 moden */
	arg = 1;
	ret = _udr_if_stm_pos_rate_set(cardId, 2, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
	
	/* sdh moden */
	arg = 0;
	ret = _udr_if_stm_pos_medium_type(cardId, 1, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
	
	/* sdh moden */
	arg = 0;
	ret = _udr_if_stm_pos_medium_type(cardId, 2, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_at_stm_pos_card_smac_set(cardId, smac1,smac2);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_if_stm_pos_sdh_aug_mapping(cardId, 1, AT_CPOS_STM4_E3_MAPPING);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_if_stm_pos_sdh_aug_mapping(cardId, 2, AT_CPOS_STM4_E3_MAPPING);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	for(cposPortVc4Ch=1;cposPortVc4Ch<=12;cposPortVc4Ch++)
	{
		arg = 1;/*cAtHdlcFrmPppCreate*/
		logicalChannel = cposPortVc4Ch;
		ret = _udr_if_stm_pos_encap_type_set(cardId,1,logicalChannel, &arg);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_stm_pos_encap_type_set(cardId,2,logicalChannel, &arg);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_stm_pos_add_channel(cardId,1,logicalChannel,cposPortVc4Ch,AT_CPOS_STM4_E3_MAPPING);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_stm_pos_add_channel(cardId,2,logicalChannel,cposPortVc4Ch,AT_CPOS_STM4_E3_MAPPING);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ouputvlan.cfi=0;
		ouputvlan.priority=7;
		ouputvlan.vlanId=cposPortVc4Ch;
		inputvlan.cfi=0;
		inputvlan.priority=7;
		inputvlan.vlanId = cposPortVc4Ch;
		ret = _udr_if_stm_pos_channel_acl_add(cardId, 1, logicalChannel, dmac, &ouputvlan, &inputvlan);		
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_stm_pos_channel_acl_add(cardId, 2, logicalChannel, dmac, &ouputvlan, &inputvlan);		
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		#if	0
			clock.TimeModen = 1;/*cAtTimingModeSys*/
			clock.refPort = 1;/* only used in slave*/
			ret = _udr_if_stm_pos_txclock_src_config(cardId,1,logicalChannel,&clock);
			if(ret !=0){
				printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
				return -1;
			}

			ret = _udr_if_stm_pos_txclock_src_config(cardId,2,logicalChannel,&clock);
			if(ret !=0){
				printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
				return -1;
			}
		#endif
	}

}

int _test_udr_if_stm4_cpos_e3_del(uint32 cardId)
{
	int ret=0;
	uint32 cposPortVc4Ch=0,logicalChannel=0;

	for(cposPortVc4Ch=1;cposPortVc4Ch<=12;cposPortVc4Ch++)
	{
		logicalChannel = cposPortVc4Ch;
		ret = _udr_if_stm_pos_channel_acl_del(cardId, 1, logicalChannel);	
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_stm_pos_channel_acl_del(cardId, 2, logicalChannel);	
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_stm_pos_del_channel(cardId,1,logicalChannel);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_stm_pos_del_channel(cardId,2,logicalChannel);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}
		
	}

}


int _test_udr_if_stm4_pos_add_by_card(uint32 cardId)
{
	tAtEthVlanTagFhn ouputvlan,inputvlan;
	tAtTimeModeFhn clock;
	int ret=0;
	uint32 cposPortVc12Ch=0,logicalChannel=0;
	uint8 smac1[]={0x00,0x04,0x67,0x00,0x00,0x66};
	uint8 smac2[]={0x00,0x04,0x67,0x00,0x00,0x77};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint32 arg=0;

	/* stm4 moden */
	arg = 1;
	ret = _udr_if_stm_pos_rate_set(cardId, 1, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	/* stm4 moden */
	arg = 1;
	ret = _udr_if_stm_pos_rate_set(cardId, 2, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
	
	/* sdh moden */
	arg = 0;
	ret = _udr_if_stm_pos_medium_type(cardId, 1, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
	
	/* sdh moden */
	arg = 0;
	ret = _udr_if_stm_pos_medium_type(cardId, 2, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_at_stm_pos_card_smac_set(cardId, smac1,smac2);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_if_stm_pos_sdh_aug_mapping(cardId, 1, AT_POS_STM4_MAPPING);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_if_stm_pos_sdh_aug_mapping(cardId, 2, AT_POS_STM4_MAPPING);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}


	arg = 1;/*cAtHdlcFrmPppCreate*/
	logicalChannel = 0;
	cposPortVc12Ch=1;
	ret = _udr_if_stm_pos_encap_type_set(cardId,1,logicalChannel, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_if_stm_pos_encap_type_set(cardId,2,logicalChannel, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_if_stm_pos_add_channel(cardId,1,logicalChannel,cposPortVc12Ch,AT_POS_STM4_MAPPING);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_if_stm_pos_add_channel(cardId,2,logicalChannel,cposPortVc12Ch,AT_POS_STM4_MAPPING);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ouputvlan.cfi=0;
	ouputvlan.priority=7;
	ouputvlan.vlanId=10+cposPortVc12Ch;
	inputvlan.cfi=0;
	inputvlan.priority=7;
	inputvlan.vlanId = 10+cposPortVc12Ch;
	ret = _udr_if_stm_pos_channel_acl_add(cardId, 1, logicalChannel, dmac, &ouputvlan, &inputvlan);		
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ouputvlan.cfi=0;
	ouputvlan.priority=7;
	ouputvlan.vlanId=20+cposPortVc12Ch;
	inputvlan.cfi=0;
	inputvlan.priority=7;
	inputvlan.vlanId = 20+cposPortVc12Ch;
	ret = _udr_if_stm_pos_channel_acl_add(cardId, 2, logicalChannel, dmac, &ouputvlan, &inputvlan);		
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	#if	0
	clock.TimeModen = 1;/*cAtTimingModeSys*/
	clock.refPort = 1;/* only used in slave*/
	ret = _udr_if_stm_pos_txclock_src_config(cardId,1,logicalChannel,&clock);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_if_stm_pos_txclock_src_config(cardId,5,logicalChannel,&clock);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
	#endif
		
	return 0;

}


int _test_udr_if_stm4_pos_del_by_card(uint32 cardId)
{
	int ret=0;
	uint32 logicalChannel=0;

	ret = _udr_if_stm_pos_channel_acl_del(cardId, 1, logicalChannel);	
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_if_stm_pos_channel_acl_del(cardId, 2, logicalChannel);	
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_if_stm_pos_del_channel(cardId,1,logicalChannel);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_if_stm_pos_del_channel(cardId,2,logicalChannel);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
		
	return 0;

}



/*
J0:s
_test_udr_if_poh_set 1,1,1,0
_test_udr_if_poh_set 1,1,1,1
J1:d
_test_udr_if_poh_set 1,1,2,0
_test_udr_if_poh_set 1,1,2,1
c2:1
_test_udr_if_poh_set 1,1,3,0
_test_udr_if_poh_set 1,1,3,1
*/
_test_udr_if_poh_set(uint8 cardId, uint32 cposPort, uint32 jc01Flag, eAtSdhTtiMode Moden)
{
	uint8 c2Flag;
	int ret=0;
	tAtSdhTtiFhn poh;

	poh.mode = Moden;
	poh.paddingMode = 0;

	switch(jc01Flag)
	{
		case 1 :
		if(Moden == 0){
			sprintf(poh.message,"S");
		}else{
			sprintf(poh.message,"Ssq");
		}
		ret = _udr_if_stm_pos_j0_mode(cardId, cposPort, &poh);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}
		break;

		case 2 :
		if(Moden == 0){
			sprintf(poh.message,"L");
		}else{
			sprintf(poh.message,"Liao");
		}
		ret = _udr_if_stm_pos_j1_mode(cardId, cposPort, &poh);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}
		break;		

		case 3 :
		c2Flag = 7;
		ret = _udr_if_stm_pos_c2_flag(cardId, cposPort, &c2Flag);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}
		break;
	}
	return 0;
}


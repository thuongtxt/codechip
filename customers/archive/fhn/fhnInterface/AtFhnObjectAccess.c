/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : AtFhnObjectAccess.c
 *
 * Created Date: Jun 17, 2013
 *
 * Description : Object accessing for FHN project
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtFhnObjectAccess.h"
#include "AtFhnDriver.h"

/*--------------------------- Define -----------------------------------------*/
#define cAtFhnFirstDevice 1

/*--------------------------- Macros -----------------------------------------*/
#define mChannelDeviceId(channelId_, maxNumChannels_) (((channelId_) <= (maxNumChannels_)) ? 1 : 2)
#define mChannelDriverId(channelId_, maxNumChannels_) (((channelId_) - 1) % (maxNumChannels_))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
AtDevice AtFhnDeviceGet(uint8 slotId, uint8 deviceId)
    {
    return AtFhnSlotDeviceGet(AtFhnDriverSlotGet(slotId), deviceId);
    }

AtModuleEth AtFhnEthModule(uint8 slotId, uint8 deviceId)
    {
    return (AtModuleEth)AtDeviceModuleGet(AtFhnDeviceGet(slotId, deviceId), cAtModuleEth);
    }

AtModuleEncap AtFhnEncapModule(uint8 slotId, uint8 deviceId)
    {
    return (AtModuleEncap)AtDeviceModuleGet(AtFhnDeviceGet(slotId, deviceId), cAtModuleEncap);
    }

#if	0
AtModulePdh AtFhnPdhModule(uint8 slotId, uint8 deviceId)
    {
    return (AtModulePdh)AtDeviceModuleGet(AtFhnDeviceGet(slotId, deviceId), cAtModulePdh);
    }
#else
int AtFhnPdhModuleTestFlag=0;
AtModulePdh AtFhnPdhModule(uint8 slotId, uint8 deviceId)
{
	AtModulePdh pdhtemp;
	AtDevice deviceTemp;
	uint32 productCode;

	//deviceTemp = m_slots[slotId - 1]->devices[deviceId-1]; 
	deviceTemp = AtFhnDeviceGet(slotId, deviceId);
	productCode = *((uint32*)deviceTemp+2);

	if(AtFhnPdhModuleTestFlag==1){
		printf("#%d,%s,device=%x\r\n",__LINE__,__FUNCTION__,deviceTemp);
		printf("#%d,%s,device[0-2]=%x %x %x\r\n",__LINE__,__FUNCTION__,*((uint32*)deviceTemp+0),*((uint32*)deviceTemp+1),*((uint32*)deviceTemp+2));
		printf("#%d,%s,device[module]=%x %x %x\r\n",__LINE__,__FUNCTION__,*((uint32*)deviceTemp+36),*((uint32*)deviceTemp+37),*((uint32*)deviceTemp+38));
	}
	
	//printf("#%d,%s,produnct code=%x\r\n",__LINE__,__FUNCTION__,*((uint32*)deviceTemp+2));
	if((productCode == 0x60070013) || (productCode == 0x60030022)||(productCode == 0x60030023)||(productCode == 0x60070023) ||(productCode == 0x60030051))
	{
		//pdhtemp = deviceTemp->pdhModule; 
		pdhtemp = (AtModulePdh)AtDeviceModuleGet(deviceTemp, cAtModulePdh);
		if(AtFhnPdhModuleTestFlag==1){
			printf("#%d,%s,pdhtemp=%x\r\n",__LINE__,__FUNCTION__,pdhtemp);
		}
	    	return pdhtemp;

	}else{
		printf("#%d,%s,device=%x\r\n",__LINE__,__FUNCTION__,deviceTemp);
		printf("#%d,%s,device[0-2]=%x %x %x\r\n",__LINE__,__FUNCTION__,*((uint32*)deviceTemp+0),*((uint32*)deviceTemp+1),*((uint32*)deviceTemp+2));
		printf("#%d,%s,device[module]=%x %x %x\r\n",__LINE__,__FUNCTION__,*((uint32*)deviceTemp+36),*((uint32*)deviceTemp+37),*((uint32*)deviceTemp+38));
		assert(0);
	}
	
    	return pdhtemp;
}

#endif


AtModulePpp AtFhnPppModule(uint8 slotId, uint8 deviceId)
    {
    return (AtModulePpp)AtDeviceModuleGet(AtFhnDeviceGet(slotId, deviceId), cAtModulePpp);
    }

AtModuleSdh AtFhnSdhModule(uint8 slotId, uint8 deviceId)
    {
    return (AtModuleSdh)AtDeviceModuleGet(AtFhnDeviceGet(slotId, deviceId), cAtModuleSdh);
    }

AtHdlcLink AtFhnHdlcLinkGet(uint8 slotId, uint8 deviceId, uint16 encapChannelId)
    {
    AtHdlcChannel hdlcChannel;
    uint16 maxNumChannels    = AtModuleEncapMaxChannelsGet(AtFhnEncapModule(slotId, cAtFhnFirstDevice));
    //uint8 deviceId           = mChannelDeviceId(encapChannelId, maxNumChannels);
    uint16 drvEncapChannelId = mChannelDriverId(encapChannelId, maxNumChannels);

    hdlcChannel = (AtHdlcChannel)AtModuleEncapChannelGet(AtFhnEncapModule(slotId, deviceId), drvEncapChannelId);
    if (hdlcChannel == NULL)
        return NULL;

    /* Currently, there are two types of Encap Channel is HDLC and ATM. Just support HDLC now */
    if (AtEncapChannelEncapTypeGet((AtEncapChannel)hdlcChannel) != cAtEncapHdlc)
        AtPrintc(cSevCritical, "Channel %d invalid encapsulation type\r\n", encapChannelId);

    return AtHdlcChannelHdlcLinkGet(hdlcChannel);
    }

AtHdlcChannel AtFhnHdlcChannelGet(uint8 slotId, uint8 deviceId, uint16 encapChannelId)
    {
    AtHdlcChannel hdlcChannel;
    uint16 maxNumChannels    = AtModuleEncapMaxChannelsGet(AtFhnEncapModule(slotId, cAtFhnFirstDevice));
    //uint8 deviceId           = mChannelDeviceId(encapChannelId, maxNumChannels);
    uint16 drvEncapChannelId = mChannelDriverId(encapChannelId, maxNumChannels);

    hdlcChannel = (AtHdlcChannel)AtModuleEncapChannelGet(AtFhnEncapModule(slotId, deviceId), drvEncapChannelId);
    if (hdlcChannel == NULL)
        return NULL;

    /* Currently, there are two types of Encap Channel is HDLC and ATM. Just support HDLC now */
    if (AtEncapChannelEncapTypeGet((AtEncapChannel)hdlcChannel) != cAtEncapHdlc)
        AtPrintc(cSevCritical, "Channel %d invalid encapsulation type\r\n", encapChannelId);

    return hdlcChannel;
    }


AtEthFlow AtFhnEthFlowGet(uint8 slotId, uint8 deviceId, uint16 flowId)
    {
    uint16 maxNumFlows = AtModuleEthMaxFlowsGet(AtFhnEthModule(slotId, cAtFhnFirstDevice));
    //uint8 deviceId     = mChannelDeviceId(flowId, maxNumFlows);
    uint16 drvFlowId   = mChannelDriverId(flowId, maxNumFlows);
    return AtModuleEthFlowGet(AtFhnEthModule(slotId, deviceId), drvFlowId);
    }

AtEthPort AtFhnEthPortGet(uint8 slotId, uint8 deviceId, uint16 portId)
    {
    uint8 maxNumPort = AtModuleEthMaxPortsGet(AtFhnEthModule(slotId, cAtFhnFirstDevice));
    //uint8 deviceId   = mChannelDeviceId(portId, maxNumPort);
    uint8 drvPortId  = mChannelDriverId(portId, maxNumPort);
    return AtModuleEthPortGet(AtFhnEthModule(slotId, deviceId), drvPortId);
    }

/* For E1 card */
AtPdhDe1 AtFhnDe1Get(uint8 slotId, uint16 de1Id)
    {
    return AtModulePdhDe1Get(AtFhnPdhModule(slotId, cAtFhnFirstDevice), de1Id - 1);
    }

AtMpBundle AtFhnMpBundleGet(uint8 slotId, uint8 deviceId, uint16 bundleId)
    {
    uint16 maxNumBundles = AtModulePppMaxBundlesGet(AtFhnPppModule(slotId, cAtFhnFirstDevice));
    //uint8 deviceId       = mChannelDeviceId(bundleId, maxNumBundles);
    uint16 drvBundleId   = mChannelDriverId(bundleId, maxNumBundles);
    return AtModulePppMpBundleGet(AtFhnPppModule(slotId, deviceId), drvBundleId);
    }

AtSdhLine AtFhnLineGet(uint8 slotId, uint8 lineId)
    {
    uint8 maxNumLines = AtModuleSdhMaxLinesGet(AtFhnSdhModule(slotId, cAtFhnFirstDevice));
    uint8 deviceId    = mChannelDeviceId(lineId, maxNumLines);
    uint8 drvLineId   = mChannelDriverId(lineId, maxNumLines);
    return AtModuleSdhLineGet(AtFhnSdhModule(slotId, deviceId), drvLineId);
    }

AtSdhAug AtFhnAug4Get(uint8 slotId, uint8 lineId, uint8 aug1Id)
{
    return AtSdhLineAug4Get(AtFhnLineGet(slotId, lineId), aug1Id - 1);
}

AtSdhAug AtFhnAug1Get(uint8 slotId, uint8 lineId, uint8 aug1Id)
    {
    return AtSdhLineAug1Get(AtFhnLineGet(slotId, lineId), aug1Id - 1);
    }

/* added by ssq 20130924 */
AtSdhVc AtFhnVc4cGet(uint8 slotId, uint8 lineId, uint8 aug1Id)
    {
    return AtSdhLineVc4_4cGet(AtFhnLineGet(slotId, lineId), aug1Id - 1);
    }
/**/

AtSdhVc AtFhnVc4Get(uint8 slotId, uint8 lineId, uint8 aug1Id)
    {
    return AtSdhLineVc4Get(AtFhnLineGet(slotId, lineId), aug1Id - 1);
    }

AtSdhAu AtFhnAu4Get(uint8 slotId, uint8 lineId, uint8 aug1Id)
    {
    return AtSdhLineAu4Get(AtFhnLineGet(slotId, lineId), aug1Id - 1);
    }

AtSdhVc AtFhnVc3Get(uint8 slotId, uint8 lineId, uint8 aug1Id, uint8 vc3Id)
    {
    return AtSdhLineVc3Get(AtFhnLineGet(slotId, lineId), aug1Id - 1, vc3Id - 1);
    }

AtSdhAu AtFhnAu3Get(uint8 slotId, uint8 lineId, uint8 aug1Id, uint8 au3Id)
    {
    return AtSdhLineAu3Get(AtFhnLineGet(slotId, lineId), aug1Id - 1, au3Id - 1);
    }

AtSdhTug AtFhnTug3Get(uint8 slotId, uint8 lineId, uint8 augId, uint8 tug3Id)
    {
    return AtSdhLineTug3Get(AtFhnLineGet(slotId, lineId), augId - 1, tug3Id - 1);
    }

AtSdhTug AtFhnTug2Get(uint8 slotId, uint8 lineId, uint8 augId, uint8 au3tug3id, uint8 tug2Id)
    {
    return AtSdhLineTug2Get(AtFhnLineGet(slotId, lineId), augId - 1, au3tug3id - 1, tug2Id - 1);
    }

AtSdhTu AtFhnTu3Get(uint8 slotId, uint8 lineId, uint8 augId, uint8 tug3Id)
    {
    return AtSdhLineTu3Get(AtFhnLineGet(slotId, lineId), augId - 1, tug3Id - 1);
    }

AtSdhTu AtFhnTu1xGet(uint8 slotId, uint8 lineId, uint8 augId, uint8 tu3Id, uint8 tug2Id, uint8 tuId)
    {
    return AtSdhLineTu1xGet(AtFhnLineGet(slotId, lineId), augId - 1, tu3Id - 1, tug2Id - 1, tuId - 1);
    }

AtSdhVc AtFhnVc1xGet(uint8 slotId, uint8 lineId, uint8 augId, uint8 tu3Id, uint8 tug2Id, uint8 tuId)
    {
    return AtSdhLineVc1xGet(AtFhnLineGet(slotId, lineId), augId - 1, tu3Id - 1, tug2Id - 1, tuId - 1);
    }

AtPdhDe1 AtFhnVcDe1Get(uint8 slotId, uint8 lineId, uint8 augId, uint8 tu3Id, uint8 tug2Id, uint8 tuId)
    {
    AtSdhVc vc1x = AtFhnVc1xGet(slotId, lineId, augId, tu3Id, tug2Id, tuId);
    return (AtPdhDe1)AtSdhChannelMapChannelGet((AtSdhChannel)vc1x);
    }

AtPdhDe3 AtFhnVcDe3Get(uint8 slotId, uint8 lineId, uint8 augId, uint8 tu3Id)
    {
    AtSdhVc vc3 = AtFhnVc3Get(slotId, lineId, augId, tu3Id);
    return (AtPdhDe3)AtSdhChannelMapChannelGet((AtSdhChannel)vc3);
    }

AtMpBundle AtFhnMpBundleCreate(uint8 slotId, uint32 bundleId)
    {
    uint32 maxBundles = AtModulePppMaxBundlesGet(AtFhnPppModule(slotId, cAtFhnFirstDevice));
    uint8 deviceId    = mChannelDeviceId(bundleId, maxBundles);
    uint8 driverId    = mChannelDriverId(bundleId, maxBundles);
    return AtModulePppMpBundleCreate(AtFhnPppModule(slotId, deviceId), driverId);
    }

eAtRet AtFhnMpBundleDelete(uint8 slotId, uint32 bundleId)
    {
    uint32 maxBundles = AtModulePppMaxBundlesGet(AtFhnPppModule(slotId, cAtFhnFirstDevice));
    uint8 deviceId    = mChannelDeviceId(bundleId, maxBundles);
    uint8 driverId    = mChannelDriverId(bundleId, maxBundles);
    return AtModulePppMpBundleDelete(AtFhnPppModule(slotId, deviceId), driverId);
    }

AtHdlcChannel AtFhnHdlcPppChannelCreate(uint8 slotId, uint32 channelId)
    {
    uint32 maxNumChannels = AtModuleEncapMaxChannelsGet(AtFhnEncapModule(slotId, cAtFhnFirstDevice));
    uint8 deviceId        = mChannelDeviceId(channelId, maxNumChannels);
    uint8 driverId        = mChannelDriverId(channelId, maxNumChannels);
    return AtModuleEncapHdlcPppChannelCreate(AtFhnEncapModule(slotId, deviceId), driverId);
    }

AtHdlcChannel AtFhnCiscoHdlcChannelCreate(uint8 slotId, uint32 channelId)
    {
    uint32 maxNumChannels = AtModuleEncapMaxChannelsGet(AtFhnEncapModule(slotId, cAtFhnFirstDevice));
    uint8 deviceId        = mChannelDeviceId(channelId, maxNumChannels);
    uint8 driverId        = mChannelDriverId(channelId, maxNumChannels);
    return AtModuleEncapCiscoHdlcChannelCreate(AtFhnEncapModule(slotId, deviceId), driverId);
    }

eAtRet AtFhnEncapChannelDelete(uint8 slotId, uint32 channelId)
    {
    uint32 maxNumChannels = AtModuleEncapMaxChannelsGet(AtFhnEncapModule(slotId, cAtFhnFirstDevice));
    uint8 deviceId        = mChannelDeviceId(channelId, maxNumChannels);
    uint8 driverId        = mChannelDriverId(channelId, maxNumChannels);
    return AtModuleEncapChannelDelete(AtFhnEncapModule(slotId, deviceId), driverId);
    }

AtEthFlow AtFhnNopFlowCreate(uint8 slotId, uint16 flowId)
    {
    uint16 maxNumFlows = AtModuleEthMaxFlowsGet(AtFhnEthModule(slotId, cAtFhnFirstDevice));
    uint8 deviceId     = mChannelDeviceId(flowId, maxNumFlows);
    uint8 driverId     = mChannelDriverId(flowId, maxNumFlows);
    return AtModuleEthNopFlowCreate(AtFhnEthModule(slotId, deviceId), driverId);
    }

AtEthFlow AtFhnEopFlowCreate(uint8 slotId, uint16 flowId)
    {
    uint16 maxNumFlows = AtModuleEthMaxFlowsGet(AtFhnEthModule(slotId, cAtFhnFirstDevice));
    uint8 deviceId     = mChannelDeviceId(flowId, maxNumFlows);
    uint8 driverId     = mChannelDriverId(flowId, maxNumFlows);
    return AtModuleEthEopFlowCreate(AtFhnEthModule(slotId, deviceId), driverId);
    }

eAtRet AtFhnEthFlowDelete(uint8 slotId, uint16 flowId)
    {
    uint16 maxNumFlows = AtModuleEthMaxFlowsGet(AtFhnEthModule(slotId, cAtFhnFirstDevice));
    uint8 deviceId     = mChannelDeviceId(flowId, maxNumFlows);
    uint8 driverId     = mChannelDriverId(flowId, maxNumFlows);
    return AtModuleEthFlowDelete(AtFhnEthModule(slotId, deviceId), driverId);
    }

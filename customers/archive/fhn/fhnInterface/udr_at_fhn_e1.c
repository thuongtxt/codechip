/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SONET/SDH
 *
 * File        : udr_at_fhn_e1.c
 *
 * Created Date: July 24, 2013
 *
 * Description : 
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
//#include "AtCli.h"
#include "AtDevice.h"
#include "AtSdhAug.h"
#include "AtSdhTug.h"
#include "AtSdhVc.h"
#include "AtSdhLine.h"
#include "AtEthFlow.h"
#include "AtPppLink.h"
//#include "../encap/CliAtModuleEncap.h"
#include "AtPdhDe1.h"
#include "../../cli/libs/text-ui/include/AtTextUI.h"
#include "AtDriver.h"
#include "../../driver/include/encap/AtHdlcChannel.h"
#include "fhninterface.h"
#include "semLib.h"

uint32 gui4AtUdrFhnE1ApiDebug=0;
uint32 gui4AtFhnPosE1SoftDebug=0;

#define POS_AT_E1_SEM_TAKE(slotId, time) 	if (OK!=semTake(semE1LogicalChannelNode[slotId],(time)*sysClkRateGet())){printf("%s,#%d,error\r\n",__FUNCTION__,__LINE__);return -1;}

#define POS_AT_E1_SEM_GIVE(slotId) 		semGive(semE1LogicalChannelNode[slotId])

extern uint32 gui4AtslotCardType[3];

extern SEM_ID semE1LogicalChannelNode[AT_POS_E1_MAX_SLOT_FHN];

extern uint32 gui4AtE1logicalchannelEncap[AT_POS_E1_MAX_SLOT_FHN][AT_POS_E1_MAX_MLPPP_BUNDLE_FHN+1][AT_POS_E1_MAX_LOGICAL_CHANNEL_PERPORT_FHN+1];

extern tAtHwE1channelRecordNodeFhn gtAtHwE1channelRecordNode[AT_POS_E1_MAX_SLOT_FHN][AT_POS_E1_MAX_HW_CHANNEL_FHN+1];

extern uint32 gu4AtE1BundleMemberRecord[AT_POS_E1_MAX_SLOT_FHN][AT_POS_E1_MAX_MLPPP_BUNDLE_FHN+1];


int _udr_if_ds1_encap_type_get(uint8 cardId, uint32 ce1Port, uint32 logicalChannel, uint32 *encapType);
/*
HW_IF_DS1CHANNELIZATION,
HW_IF_DS1LINETYPE,
HW_IF_DS1LINECODE,
HW_IF_DS1SENDCODE,
HW_IF_DS1LOOPBACKCFG,
HW_IF_DS1TRANSMITCLOCKSOURCE,
HW_IF_DS1STATUSCHGTRAPENABLE,
HW_IF_DS1LINEIMPEDANCE,
HW_IF_DS1CRC,
HW_IF_DS1LINESTATUS,
HW_IF_DS1LOOPBACKSTATUS,
HW_IF_DS1LINESTATUSLASTCHANGE,
HW_IF_DS1ADDCHANNEL,
HW_IF_DS1DELCHANNEL,
HW_IF_DS1CHANNELTIMESLOT,
HW_IF_ENCAPTYPE,
HW_IF_SERIALCRC,
HW_IF_ADMIN_STATUS,
HW_IF_MTU,
HW_IF_PHYADDR,
HW_IF_STATISTICS（HW_IF_STATS）
*/







/*
set ppp / mlppp
*/

int At_fhn_pos_e1_soft_channel_set(uint8 cardId, uint32 ce1Port, uint32 logicalChannel)
{
	int ret=0;
	uint32 i=0,encapType=1;
	uint32 slotId= cardId-1;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(ce1Port<1||ce1Port>16)
	{
		printf("#%d,%s,error ce1Port=%d\r\n",__LINE__,__FUNCTION__,ce1Port);
		return -1;
	}	

	if(logicalChannel<1||logicalChannel>32)
	{
		printf("#%d,%s,error logicalChannel=%d\r\n",__LINE__,__FUNCTION__,logicalChannel);
		return -1;
	}

	/*  modified by ssq 20140514 */
	encapType = gui4AtE1logicalchannelEncap[slotId][ce1Port][logicalChannel] ;		


	POS_AT_E1_SEM_TAKE(slotId,10);
	for(i=1;i<=AT_POS_E1_MAX_HW_CHANNEL_FHN;i++)
	{
		if(gtAtHwE1channelRecordNode[slotId][i].usedFlag==0)
		{
			gtAtHwE1channelRecordNode[slotId][i].usedFlag=1;
			gtAtHwE1channelRecordNode[slotId][i].channelId=i;
			gtAtHwE1channelRecordNode[slotId][i].ce1Port=ce1Port;
			gtAtHwE1channelRecordNode[slotId][i].logicalChannelId=logicalChannel;	
			gtAtHwE1channelRecordNode[slotId][i].encapType=encapType;	
			POS_AT_E1_SEM_GIVE(slotId);
			if(gui4AtFhnPosE1SoftDebug){
				printf("slotId[%d] add hwch[%d] bind to port[%d],logch[%d],timeslot[%x],encapType[%x]\r\n",
					slotId,
					gtAtHwE1channelRecordNode[slotId][i].channelId,
					gtAtHwE1channelRecordNode[slotId][i].ce1Port,
					gtAtHwE1channelRecordNode[slotId][i].logicalChannelId,
					gtAtHwE1channelRecordNode[slotId][i].ce1portTimeslot,
					gtAtHwE1channelRecordNode[slotId][i].encapType);
			}
			return 0;
		}
	}

	if(i>AT_POS_E1_MAX_HW_CHANNEL_FHN){
		if(gui4AtFhnPosE1SoftDebug){
			printf("#%d,%s,set have not channel resource\r\n",__LINE__,__FUNCTION__);
		}
		POS_AT_E1_SEM_GIVE(slotId);
		return -1;
	}

	POS_AT_E1_SEM_GIVE(slotId);
	return 0;

}


int At_fhn_pos_e1_soft_channel_timeslot_set(uint8 cardId, uint32 ce1Port, uint32 logicalChannel, uint32 ce1portTimeslot)
{
	uint32 i=0;
	uint32 slotId= cardId-1;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(ce1Port<1||ce1Port>16)
	{
		printf("#%d,%s,error ce1Port=%d\r\n",__LINE__,__FUNCTION__,ce1Port);
		return -1;
	}	

	POS_AT_E1_SEM_TAKE(slotId,10);
	for(i=1;i<=AT_POS_E1_MAX_HW_CHANNEL_FHN;i++)
	{
		if(	(gtAtHwE1channelRecordNode[slotId][i].usedFlag!=0)&&
			(gtAtHwE1channelRecordNode[slotId][i].ce1Port==ce1Port)&&
			(gtAtHwE1channelRecordNode[slotId][i].logicalChannelId==logicalChannel))
		{
			gtAtHwE1channelRecordNode[slotId][i].ce1portTimeslot= ce1portTimeslot;
			POS_AT_E1_SEM_GIVE(slotId);
			if(gui4AtFhnPosE1SoftDebug){
				printf("slotId[%d] set hwch[%d] bind to port[%d],logch[%d],timeslot[%x],encapType[%x]\r\n",
						slotId,
						gtAtHwE1channelRecordNode[slotId][i].channelId,
						gtAtHwE1channelRecordNode[slotId][i].ce1Port,
						gtAtHwE1channelRecordNode[slotId][i].logicalChannelId,
						gtAtHwE1channelRecordNode[slotId][i].ce1portTimeslot,
						gtAtHwE1channelRecordNode[slotId][i].encapType);
			}
			return 0;
		}
	}

	if(i>AT_POS_E1_MAX_HW_CHANNEL_FHN){
		printf("#%d,%s,set ts have not channel resource\r\n",__LINE__,__FUNCTION__);
		POS_AT_E1_SEM_GIVE(slotId);
		return -1;
	}

	POS_AT_E1_SEM_GIVE(slotId);
	return 0;

}


int At_fhn_pos_e1_soft_channel_del(uint8 cardId, uint32 ce1Port, uint32 logicalChannel)
{
	uint32 i=0;
	uint32 slotId= cardId-1;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(ce1Port<1||ce1Port>16)
	{
		printf("#%d,%s,error ce1Port=%d\r\n",__LINE__,__FUNCTION__,ce1Port);
		return -1;
	}	

	POS_AT_E1_SEM_TAKE(slotId,10);
	for(i=1;i<=AT_POS_E1_MAX_HW_CHANNEL_FHN;i++)
	{
		if(	(gtAtHwE1channelRecordNode[slotId][i].usedFlag!=0)&&
			(gtAtHwE1channelRecordNode[slotId][i].ce1Port==ce1Port)&&
			(gtAtHwE1channelRecordNode[slotId][i].logicalChannelId==logicalChannel))
		{
			if(gui4AtFhnPosE1SoftDebug){
				printf("slotId[%d] del hwch[%d] bind to port[%d],logch[%d],timeslot[%x],encapType[%x]\r\n",
					slotId,
					gtAtHwE1channelRecordNode[slotId][i].channelId,
					gtAtHwE1channelRecordNode[slotId][i].ce1Port,
					gtAtHwE1channelRecordNode[slotId][i].logicalChannelId,
					gtAtHwE1channelRecordNode[slotId][i].ce1portTimeslot,
					gtAtHwE1channelRecordNode[slotId][i].encapType);
			}
		
			gtAtHwE1channelRecordNode[slotId][i].usedFlag = 0;
			gtAtHwE1channelRecordNode[slotId][i].ce1Port=0;
			gtAtHwE1channelRecordNode[slotId][i].ce1portTimeslot=0;
			gtAtHwE1channelRecordNode[slotId][i].logicalChannelId=0;
			gtAtHwE1channelRecordNode[slotId][i].encapType=1;
			POS_AT_E1_SEM_GIVE(slotId);

			
			return 0;
		}
	}

	if(i>AT_POS_E1_MAX_HW_CHANNEL_FHN){
		if(gui4AtFhnPosE1SoftDebug){
			printf("#%d,%s,del have not find channel\r\n",__LINE__,__FUNCTION__,cardId,ce1Port,logicalChannel);
		}
		POS_AT_E1_SEM_GIVE(slotId);
		return -1;
	}

	POS_AT_E1_SEM_GIVE(slotId);
	return 0;

}


/*
input :
	uint8 cardId, 
	uint32 ce1Port, 
	uint32 ce1portTimeslot,
output:
	uint32 *channelId, 
	uint32 *encapType

*/
int At_fhn_pos_e1_soft_channel_get(uint8 cardId, uint32 ce1Port, uint32 logicalChannel, uint32 *ce1portTimeslot, uint32 *channelId, uint32 *encapType)
{
	uint32 i=0;
	uint32 slotId= cardId-1;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(ce1Port<1||ce1Port>16)
	{
		printf("#%d,%s,error ce1Port=%d\r\n",__LINE__,__FUNCTION__,ce1Port);
		return -1;
	}

	POS_AT_E1_SEM_TAKE(slotId,10);
	for(i=1;i<=AT_POS_E1_MAX_HW_CHANNEL_FHN;i++)
	{
		if(	(gtAtHwE1channelRecordNode[slotId][i].usedFlag!=0)&&
			(gtAtHwE1channelRecordNode[slotId][i].ce1Port==ce1Port)&&
			(gtAtHwE1channelRecordNode[slotId][i].logicalChannelId==logicalChannel))
		{
			*(uint32 *)ce1portTimeslot = gtAtHwE1channelRecordNode[slotId][i].ce1portTimeslot;
			*(uint32 *)channelId = gtAtHwE1channelRecordNode[slotId][i].channelId;
			*(uint32 *)encapType = gtAtHwE1channelRecordNode[slotId][i].encapType;
			POS_AT_E1_SEM_GIVE(slotId);
			return 0;
		}
	}

	if(i>AT_POS_E1_MAX_HW_CHANNEL_FHN){
		if(gui4AtFhnPosE1SoftDebug){
			printf("#%d,%s,get can not find\r\n",__LINE__,__FUNCTION__);
		}
		POS_AT_E1_SEM_GIVE(slotId);
		return -1;
	}

	POS_AT_E1_SEM_GIVE(slotId);
	return 0;
}


int At_fhn_pos_e1_soft_channel_show(uint8 cardId)
{
	uint32 channelID=0;
	uint32 slotId= cardId-1;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	POS_AT_E1_SEM_TAKE(slotId,10);
	for(channelID=1;channelID<=AT_POS_E1_MAX_HW_CHANNEL_FHN;channelID++){
		if(gtAtHwE1channelRecordNode[slotId][channelID].usedFlag != 0)
		{
			printf("slotId[%d]used hwch[%d] bind to port[%d],logch[%d],timeslot[%x],encapType[%x]\r\n",
									slotId,
									gtAtHwE1channelRecordNode[slotId][channelID].channelId,
									gtAtHwE1channelRecordNode[slotId][channelID].ce1Port,
									gtAtHwE1channelRecordNode[slotId][channelID].logicalChannelId,
									gtAtHwE1channelRecordNode[slotId][channelID].ce1portTimeslot,
									gtAtHwE1channelRecordNode[slotId][channelID].encapType);
		}
	}

	for(channelID=1;channelID<=AT_POS_E1_MAX_MLPPP_BUNDLE_FHN;channelID++){
		if(gu4AtE1BundleMemberRecord[slotId][channelID] !=0){
			printf("slot[%d]trunk[%d] member=%d\r\n",slotId,channelID,gu4AtE1BundleMemberRecord[slotId][channelID]);
		}
	}

	POS_AT_E1_SEM_GIVE(slotId);
	return 0;
}

#if	0

/*
liuChipNum=1 ，slot1 的 1-8口
liuChipNum=2 ，slot1 的 9-16口
liuChipNum=3 ，slot2 的 1-8口
liuChipNum=4 ，slot2 的 9-16口
liuChipNum = 1,2,3,4
portStatusBitmap = 8 port bitmap, 
1: linkUp 
0: linkDown
*/
int _udr_at_ds1_port_los_status_get(uint32 liuChipNum, uint8 *portStatusBitmap)
{
	uint8 portStatusBuf=0,regbuf=0,port=0;

	if((liuChipNum<1) ||(liuChipNum>4))
	{
		return -1;
	}
	
	csLosStatusGet(liuChipNum-1, &portStatusBuf);

	for(port=0;port<8;port++)
	{
		if((portStatusBuf & (1<<(7-port))) == 0)
		{
			/* LOS */
			regbuf = regbuf |(1<<port);	
			//printf("port =%x,regbuf = %x\r\n",port, regbuf);	
		}
	}
	*(uint8 *)portStatusBitmap = regbuf;

	//printf("portStatusbitmap = %x\r\n",*(uint8 *)portStatusBitmap);
}

#else

/*
cardId = 1,2
e1Port = 1-16, 
*linkStatus:1 =  linkUp 
*linkStatus:0 =  linkDown
*/
int _udr_at_ds1_port_los_status_get(uint8 cardId, uint32 ce1Port, uint32 *linkStatus)
{
	uint8 *EncapTypeBuf;
	AtSdhLine sdhModuleLine;
	eAtRet  ret;
	AtEncapChannel encapChannel;
	uint32 i,alarmStat=0;
	uint8 slotId=cardId;
	uint32 port = ce1Port;
	
	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((ce1Port>16) || (ce1Port<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",ce1Port);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		port = port+8;
		if((port>32) || (port<1))
		{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	alarmStat = AtChannelAlarmGet((AtChannel)AtFhnDe1Get(slotId,port));
	if ((alarmStat & cCmdPdhDe1AlarmTypeValFhn[0])||(alarmStat & cCmdPdhDe1AlarmTypeValFhn[1])){
		*linkStatus=0;
	}else{
		*linkStatus=1;
	}

	POS_AT_UDR_SEM_GIVE(cardId-1);

	return 0;

}

#endif
int _udr_at_pos_e1_card_smac_set(uint8 cardId, uint8 *smac)
{
	int ret=0;

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	ret = Udr_at_eth_port_smac_set(cardId,1,1,smac);

	POS_AT_UDR_SEM_GIVE(cardId-1);

	return ret;
	
}


/*
HW_IF_DS1LINETYPE

CmdPdhDe1FrameModeSet
typedef enum eAtPdhDe1FrameTypeFhn
    {
    cAtPdhDe1FrameUnknownFhn , /**< Unknown frame 

    cAtPdhDs1J1UnFrmFhn      , /**< DS1/J1 Unframe 
    cAtPdhDs1FrmSfFhn        , /**< DS1 SF (D4) 
    cAtPdhDs1FrmEsfFhn       , /**< DS1 ESF 
    cAtPdhDs1FrmDDSFhn       , /**< DS1 DDS 
    cAtPdhDs1FrmSLCFhn       , /**< DS1 SLC 

    cAtPdhJ1FrmSfFhn         , /**< J1 SF 
    cAtPdhJ1FrmEsfFhn        , /**< J1 ESF 

    cAtPdhE1UnFrmFhn         , /**< E1 unframe 
    cAtPdhE1FrmFhn           , /**< E1 basic frame, FAS/NFAS framing 
    cAtPdhE1MFCrcFhn           /**< E1 Multi-Frame with CRC4 
    }eAtPdhDe1FrameTypeFhn;
*/


int _udr_if_ds1_line_type(uint8 cardId, uint32 ce1Port, eAtPdhDe1FrameTypeFhn *arg)
{
	uint32 port = ce1Port;
	int ret=0;

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		port = port+8;
		if((port>32) || (port<1))
		{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	ret = Udr_ce1_port_framer_mode_set (cardId, port, arg);

	POS_AT_UDR_SEM_GIVE(cardId-1);

	return ret;
}


/*
HW_IF_DS1LINECODE
0: HDB3 code (default)
1: AMI code
*/
int _udr_if_ds1_line_code(uint8 cardId, int *arg)
{
	int ret=0;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

  	ret = Udr_ce1_card_interfece_code_set(cardId, 1, arg);

	POS_AT_UDR_SEM_GIVE(cardId-1);

	return ret;
}


/*
phy port tx enable or disable
1: link up
0: link down
*/
int _udr_if_ds1_port_status_set(uint8 cardId, uint32 ce1Port, void *arg)
{
	uint32 port = ce1Port;

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		port = port+8;
		if((port>32) || (port<1))
		{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}


	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	Udr_ce1_port_enable(cardId,  port, arg);/* added by ssq 20140529 */

	Udr_ce1_port_admin_status(cardId,  port, arg);

	POS_AT_UDR_SEM_GIVE(cardId-1);

	return 0;
}


/*
1: link up
0: link down
*/
int _udr_if_ds1_admin_status_set(uint8 cardId, uint32 ce1Port, uint32 logicalChannel, int *arg)
{
	int ret=0,enable=0;
	uint32 channelId=0,encapType=0,flowId=0,timeslotMap=0;
	uint32 port = ce1Port;

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		port = port+8;
		if((port>32) || (port<1))
		{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	if(At_fhn_pos_e1_soft_channel_get( cardId,  port,  logicalChannel, &timeslotMap, &channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	if( *(int*)arg){
		/* enable */
		enable = 1;/*enable channel tx*/
		ret = Udr_ce1_port_channel_txenable(cardId,channelId,&enable);
		if(ret !=0){
			printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}
		

		enable = 1;/*enable channel rx*/
		ret = Udr_ce1_port_channel_rxenable(cardId,channelId,&enable);
		if(ret !=0){
			printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}	

		enable = 5;/*cAtPppLinkPhaseNetworkActive*/
		ret = Udr_ce1_port_channel_ppp_enable(cardId,channelId,&enable);
		if(ret !=0){
			printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}

	}else{
		/* disable */
		enable = 0;/*disable channel tx*/
		ret = Udr_ce1_port_channel_txenable(cardId,channelId,&enable);
		if(ret !=0){
			printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}
		
		enable = 0;/*disable channel rx*/
		ret = Udr_ce1_port_channel_rxenable(cardId,channelId,&enable);
		if(ret !=0){
			printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}	

		enable = 1;/*cAtPppLinkPhaseDead*/
		ret = Udr_ce1_port_channel_ppp_enable(cardId,channelId,&enable);
		if(ret !=0){
			printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}

	}

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return 0;

}


/*
HW_IF_DS1SENDCODE
*/

int _udr_if_ds1_send_code(uint8 cardId, uint32 ce1Port, uint32 logicalChannel, int *arg)
{
	int ret=0;
	uint32 channelId=0,encapType=0,flowId=0,timeslotMap=0;
	uint32 port = ce1Port;

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		port = port+8;
		if((port>32) || (port<1))
		{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	if(At_fhn_pos_e1_soft_channel_get( cardId,  port,  logicalChannel, &timeslotMap, &channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

  	ret = Udr_ce1_port_channel_scramle_set(cardId, channelId, arg);

	POS_AT_UDR_SEM_GIVE(cardId-1);

	return ret;
	
}




/*
HW_IF_DS1LOOPBACKCFG
portId = 1-16
typedef enum eAtPdhLoopbackModeFhn
    {
    cAtPdhLoopbackModeReleaseFhn,        /**< Release loopback 
    cAtPdhLoopbackModeLocalPayloadFhn,   /**< Payload Local loopback 
    cAtPdhLoopbackModeRemotePayloadFhn,  /**< Payload Remote loopback 
    cAtPdhLoopbackModeLocalLineFhn,      /**< Line Local loopback 
    cAtPdhLoopbackModeRemoteLineFhn      /**< Line Local loopback 
    }eAtPdhLoopbackModeFhn;
*/
int _udr_if_ds1_loopback_config(uint8 cardId, uint32 ce1Port, int *arg)
{
	uint32 port = ce1Port;
	int ret=0;

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		port = port+8;
		if((port>32) || (port<1))
		{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

  	ret = Udr_ce1_port_loopback_set(cardId, port, arg);
	
	POS_AT_UDR_SEM_GIVE(cardId-1);

	return ret;
	
}

/*
HW_IF_DS1TRANSMITCLOCKSOURCE

typedef enum eAtTimingModeFhn
    {
    cAtTimingModeUnknownFhn    , /**< Unknown timing mode 
    cAtTimingModeSysFhn        , /**< System timing mode 
    cAtTimingModeLoopFhn       , /**< Loop timing mode 
    cAtTimingModeSdhLineRefFhn , /**< SDH Line 
    cAtTimingModeExt1RefFhn    , /**< Line EXT #1 timing mode 
    cAtTimingModeExt2RefFhn    , /**< Line EXT #2 timing mode 
    cAtTimingModeAcrFhn        , /**< ACR timing mode 
    cAtTimingModeDcrFhn        , /**< DCR timing mode 
    cAtTimingModeFreeFhn       , /**< Free running mode - using system clock for CDR
                                    source to generate service TX clock
    cAtTimingModeTopFhn        , /**< ToP timing mode 
    cAtTimingModeSlaveFhn        /**< Slave timing mode 
    }eAtTimingModeFhn;


typedef struct tAtTimeModeFhn
{
	eAtTimingModeFhn TimeModen;
	uint32 refPort;
}tAtTimeModeFhn;
*/

int _udr_if_ds1_txclock_src_config(uint8 cardId, uint32 ce1Port, tAtTimeModeFhn *arg)
{
	uint32 port = ce1Port;
	int ret=0;

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		port = port+8;
		if((port>32) || (port<1))
		{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

  	ret = Udr_ce1_port_clock_mode_set(cardId, port, arg);

	POS_AT_UDR_SEM_GIVE(cardId-1);

	return ret;
}

/*
HW_IF_DS1STATUSCHGTRAPENABLE
*/
/*
int _udr_if_ds1_status_tx(uint8 cardId, uint32 port, void *arg)
{
  	return 0;
}
*/


/*
HW_IF_DS1CRC
CmdPdhDe1FrameModeSet
typedef enum eAtPdhDe1FrameTypeFhn
    {
    cAtPdhDe1FrameUnknownFhn , /**< Unknown frame 

    cAtPdhDs1J1UnFrmFhn      , /**< DS1/J1 Unframe 
    cAtPdhDs1FrmSfFhn        , /**< DS1 SF (D4) 
    cAtPdhDs1FrmEsfFhn       , /**< DS1 ESF 
    cAtPdhDs1FrmDDSFhn       , /**< DS1 DDS 
    cAtPdhDs1FrmSLCFhn       , /**< DS1 SLC 

    cAtPdhJ1FrmSfFhn         , /**< J1 SF 
    cAtPdhJ1FrmEsfFhn        , /**< J1 ESF 

    cAtPdhE1UnFrmFhn         , /**< E1 unframe 
    cAtPdhE1FrmFhn           , /**< E1 basic frame, FAS/NFAS framing 
    cAtPdhE1MFCrcFhn           /**< E1 Multi-Frame with CRC4 
    }eAtPdhDe1FrameTypeFhn;
*/

int _udr_if_ds1_crc(uint8 cardId, uint32 ce1Port, eAtPdhDe1FrameTypeFhn *crcType)
{
	uint32 port = ce1Port;
	int ret=0;

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		port = port+8;
		if((port>32) || (port<1))
		{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	ret = Udr_ce1_port_framer_mode_set(cardId, port, crcType);
	
	POS_AT_UDR_SEM_GIVE(cardId-1);

	return ret;
	
}


/*
HW_IF_DS1LINESTATUS

typedef struct eUdrPdhDe1AlarmTypeFhn
{
    uint8 cUdrAtPdhDe1AlarmLosFhn;  /**< LOS 
    uint8 cUdrAtPdhDe1AlarmLofFhn;  /**< LOF 
    uint8 cUdrAtPdhDe1AlarmAisFhn;  /**< AIS 
    uint8 cUdrAtPdhDe1AlarmRaiFhn;  /**< RAI 
    uint8 cUdrAtPdhDe1AlarmLomfFhn;  /**< LOMF 
    uint8 cUdrAtPdhDe1AlarmSfBerFhn;  /**< SF BER 
    uint8 cUdrAtPdhDe1AlarmSdBerFhn;  /**< SD BER 
    uint8 cUdrAtPdhDe1AlarmSigLofFhn;  /**< LOF on signaling channel 
    uint8 cUdrAtPdhDe1AlarmSigRaiFhn;  /**< RAI on signaling channel 
}eUdrPdhDe1AlarmTypeFhn;

*/
int _udr_if_ds1_line_status(uint8 cardId, uint32 ce1Port, eUdrPdhDe1AlarmTypeFhn *alarmStatus)
{
	uint32 port = ce1Port;
	int ret=0;

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		port = port+8;
		if((port>32) || (port<1))
		{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	ret = Udr_ce1_port_alarm_status(cardId, port, alarmStatus);
	
	POS_AT_UDR_SEM_GIVE(cardId-1);

	return ret;
	
}


/*
HW_IF_ENCAPTYPE
typedef enum eAtEncapTypeCreateFhn
    {
    cAtHdlcFrmCiscoHdlcCreateFhn,/**< Cisco HDLC 
    cAtHdlcFrmPppCreateFhn,      /**< PPP 
    cAtHdlcFrmFrCreateFhn,       /**< Frame relay 
    cAtEncapAtmCreateFhn         /**< ATM 
    } eAtEncapTypeCreateFhn;
*/
int _udr_if_ds1_encap_type_set(uint8 cardId, uint32 ce1Port, uint32 logicalChannel, eAtEncapTypeCreateFhn *encapType)
{
	uint32 slotId = cardId-1;
	uint32 arg = *(uint32 *)encapType;
	uint32 port = ce1Port;
	
	if(gui4AtUdrFhnE1ApiDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,port=%d,logicalChannel=%d,encapType=%d\r\n",cardId,ce1Port,logicalChannel,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(ce1Port<1||ce1Port>16){
		printf("#%d,%s,error ce1Port=%d\r\n",__LINE__,__FUNCTION__,ce1Port);
		return -1;
	}	

	if(logicalChannel<1||logicalChannel>32){
		printf("#%d,%s,error logicalChannel=%d\r\n",__LINE__,__FUNCTION__,logicalChannel);
		return -1;
	}

	if(arg<0||arg>3){
		printf("#%d,%s,error encapType=%d\r\n",__LINE__,__FUNCTION__,arg);
		return -1;
	}


	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		port = port+8;
		if((port>32) || (port<1))
		{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	gui4AtE1logicalchannelEncap[slotId][port][logicalChannel] = arg;		

	POS_AT_UDR_SEM_GIVE(cardId-1);

	return 0;
}

/*
typedef enum eAtEncapTypeCreateFhn
    {
    cAtHdlcFrmCiscoHdlcCreateFhn,/**< Cisco HDLC 
    cAtHdlcFrmPppCreateFhn,      /**< PPP 
    cAtHdlcFrmFrCreateFhn,       /**< Frame relay 
    cAtEncapAtmCreateFhn         /**< ATM 
    } eAtEncapTypeCreateFhn;
*/
int _udr_if_ds1_encap_type_change(uint8 cardId, uint32 ce1Port, uint32 logicalChannel, uint32 *framerType)
{
	uint32 slotId = cardId-1;
	uint32 arg = *(uint32 *)framerType +1;
	uint32 channelId=0,flowId=0,timeslotMap=0,encapType=0;
	uint32 port = ce1Port;

	if(gui4AtUdrFhnE1ApiDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,port=%d,logicalChannel=%d,encapType=%d\r\n",cardId,ce1Port,logicalChannel,*(uint32 *)framerType);
	}
	
	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(ce1Port<1||ce1Port>16){
		printf("#%d,%s,error cposPort=%d\r\n",__LINE__,__FUNCTION__,ce1Port);
		return -1;
	}	

	if(logicalChannel<1||logicalChannel>32){
		printf("#%d,%s,error logicalChannel=%d\r\n",__LINE__,__FUNCTION__,logicalChannel);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		port = port+8;
		if((port>32) || (port<1))
		{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	/**/
	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	if(At_fhn_pos_e1_soft_channel_get( cardId,  port,  logicalChannel, &timeslotMap, &channelId, &encapType) == -1){
		printf("%s,#%d,do not have node:card[%d]/[%d]/[%d]\r\n",__FILE__,__LINE__,cardId, port, logicalChannel);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}
	
	Udr_ce1_port_channel_framer_type_set(cardId, channelId,&arg);
	/**/

	gui4AtE1logicalchannelEncap[slotId][port][logicalChannel]= *(uint32 *)framerType;		

	POS_AT_UDR_SEM_GIVE(cardId-1);

	return 0;
}

int _udr_if_ds1_encap_type_get(uint8 cardId, uint32 ce1Port, uint32 logicalChannel, uint32 *encapType)
{
	uint32 slotId = cardId-1;
	uint32 port = ce1Port;
	
	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(ce1Port<1||ce1Port>16)
	{
		printf("#%d,%s,error ce1Port=%d\r\n",__LINE__,__FUNCTION__,ce1Port);
		return -1;
	}	

	if(logicalChannel<1||logicalChannel>32)
	{
		printf("#%d,%s,error logicalChannel=%d\r\n",__LINE__,__FUNCTION__,logicalChannel);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		port = port+8;
		if((port>32) || (port<1))
		{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	*(uint32 *)encapType = gui4AtE1logicalchannelEncap[slotId][port][logicalChannel] ;		

	POS_AT_UDR_SEM_GIVE(cardId-1);

	return 0;
}

/*
HW_IF_DS1ADDCHANNEL

*/

int _udr_if_ds1_add_channel(uint8 cardId, uint32 ce1Port, uint32 logicalChannel)
{
	uint32 arg;
	tAtPdhDs0PortFhn E1TsPort={0};
	int ret=0;
	uint32 channelId=0,encapType=0,flowId=0,timeslotMap=0;
	uint32 port = ce1Port;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(ce1Port<1||ce1Port>16)
	{
		printf("#%d,%s,error ce1Port=%d\r\n",__LINE__,__FUNCTION__,ce1Port);
		return -1;
	}	

	if(logicalChannel<1||logicalChannel>32)
	{
		printf("#%d,%s,error logicalChannel=%d\r\n",__LINE__,__FUNCTION__,logicalChannel);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		port = port+8;
		if((port>32) || (port<1))
		{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);
	/*  不允许存在该logicalchannel 没有删除就配置，必须先把以前的删除*/
	if(At_fhn_pos_e1_soft_channel_get( cardId,  port,  logicalChannel, &timeslotMap, &channelId, &encapType) == 0){
		printf("%s,#%d,you should del card[%d]ce1port[%d]logicalChannel[%d]\r\n",__FILE__,__LINE__,cardId, port, logicalChannel);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}
	/**/

	if(At_fhn_pos_e1_soft_channel_set(cardId,port,logicalChannel) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	if(At_fhn_pos_e1_soft_channel_get(cardId, port, logicalChannel, &timeslotMap, &channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret = Udr_ce1_port_channel_create(cardId, channelId, &encapType);
	/*if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		goto lable;
	}*/

	arg = port;
	ret = Udr_ce1_port_channel_bind_port(cardId,channelId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		goto lable;
	}

	/*fcs16 added by ssq for strange packets 20131008
	arg = 1;
	ret = Udr_ce1_port_channel_fcs(cardId, channelId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		goto lable;
	}
	*/
	
	arg = 1;/*enable channel tx*/
	ret = Udr_ce1_port_channel_txenable(cardId,channelId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		goto lable;
	}

	arg = 1;/*enable channel rx*/
	ret = Udr_ce1_port_channel_rxenable(cardId,channelId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		goto lable;
	}
	
	arg = 5;/*cAtPppLinkPhaseNetworkActive*/
	ret = Udr_ce1_port_channel_ppp_enable(cardId,channelId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		goto lable;
	}

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return 0;

lable:
	At_fhn_pos_e1_soft_channel_del(cardId,port,logicalChannel);
	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;
}


int _udr_if_ds1_channel_acl_add(uint8 cardId, uint32 ce1Port, uint32 logicalChannel, uint8 *dmac, 
								tAtEthVlanTagFhn *ouputvlan,tAtEthVlanTagFhn *inputvlan)
{
	uint32 arg;
	tAtPdhDs0PortFhn E1TsPort={0};
	int ret=0;
	uint32 channelId=0,encapType=0,flowId=0,timeslotMap=0;
	uint32 port = ce1Port;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(ce1Port<1||ce1Port>16)
	{
		printf("#%d,%s,error ce1Port=%d\r\n",__LINE__,__FUNCTION__,ce1Port);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		port = port+8;
		if((port>32) || (port<1))
		{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	if(At_fhn_pos_e1_soft_channel_get( cardId,  port,  logicalChannel, &timeslotMap, &channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}
	
	flowId = channelId;
	arg=0;
	ret = Udr_ce1_port_flow_create(cardId,flowId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret = Udr_ce1_port_flow_egress_dmac_set(cardId,flowId,dmac);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret = Udr_ce1_port_channel_bind_flow(cardId,channelId,flowId);/*绑定业务通道到流ID*/
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret = Udr_ce1_port_flow_egress_vlan_set(cardId,1,flowId, ouputvlan);/*设置流标示的出口VLAN  */
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret = Udr_ce1_port_flow_set_vlan(cardId, 1, flowId, inputvlan);/*流标示的分类规则,只能基于VLAN */
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret =Udr_ce1_port_oam_channel_mode(cardId, channelId);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret =Udr_ce1_port_oam_channel_egress_dmac_set(cardId,channelId,dmac);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret =Udr_ce1_port_oam_channel_egress_vlan_set(cardId, 1, channelId, ouputvlan);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;
}


int _udr_if_ds1_channel_acl_del(uint8 cardId, uint32 ce1Port, uint32 logicalChannel)
{
	uint32 arg;
	tAtPdhDs0PortFhn E1TsPort={0};
	int ret=0;
	uint32 channelId=0,encapType=0,flowId=0,timeslotMap=0;
	uint32 port = ce1Port;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(ce1Port<1||ce1Port>16)
	{
		printf("#%d,%s,error ce1Port=%d\r\n",__LINE__,__FUNCTION__,ce1Port);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		port = port+8;
		if((port>32) || (port<1))
		{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	if(At_fhn_pos_e1_soft_channel_get( cardId,  port,  logicalChannel, &timeslotMap, &channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	arg = 0;/*disable channel tx*/
	ret = Udr_ce1_port_channel_txenable(cardId,channelId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}
	
	arg = 0;/*disable channel rx*/
	ret = Udr_ce1_port_channel_rxenable(cardId,channelId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}
	
	arg = 1;/*cAtPppLinkPhaseDead*/
	ret = Udr_ce1_port_channel_ppp_enable(cardId,channelId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}
	
	flowId = channelId;
	ret = Udr_ce1_port_channel_unbind_flow(cardId,channelId,&flowId);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret = Udr_ce1_port_flow_del(cardId,flowId,&encapType);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}
	
	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;
}

/* HW_IF_DS1CHANNELTIMESLOT */
int _udr_if_ds1_add_channel_timeslot(uint8 cardId, uint32 ce1Port, uint32 logicalChannel, uint32 ce1portTimeslot)
{
	uint32 arg;
	tAtPdhDs0PortFhn E1TsPort={0};
	int ret=0;
	uint32 channelId=0,encapType=0,flowId=0,timeslotMap=0;
	uint32 port = ce1Port;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(ce1Port<1||ce1Port>16)
	{
		printf("#%d,%s,error ce1Port=%d\r\n",__LINE__,__FUNCTION__,ce1Port);
		return -1;
	}

	if(logicalChannel<1||logicalChannel>32)
	{
		printf("#%d,%s,error logicalChannel=%d\r\n",__LINE__,__FUNCTION__,logicalChannel);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		port = port+8;
		if((port>32) || (port<1))
		{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	/*  不允许存在该logicalchannel 没有删除就配置，必须先把以前的删除*/
	if(At_fhn_pos_e1_soft_channel_get( cardId,  port,  logicalChannel, &timeslotMap, &channelId, &encapType) == 0){
		printf("%s,#%d,you should del card[%d]ce1port[%d]logicalChannel[%d]\r\n",__FILE__,__LINE__,cardId, port, logicalChannel);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}
	/**/

	if(At_fhn_pos_e1_soft_channel_set(cardId,port,logicalChannel) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	if(At_fhn_pos_e1_soft_channel_timeslot_set(cardId,  port,  logicalChannel, ce1portTimeslot) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	if(At_fhn_pos_e1_soft_channel_get( cardId,  port,  logicalChannel, &timeslotMap, &channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret = Udr_ce1_port_channel_create(cardId, channelId, &encapType);
	/*if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		goto lable;
	}*/

	E1TsPort.e1PortId = port;
	E1TsPort.timeslotBitmap = ce1portTimeslot;
 	ret = Udr_ce1_port_create_port_ts(cardId, channelId, &E1TsPort);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		goto lable;
	}

 	ret = Udr_ce1_port_channel_bind_port_ts(cardId, channelId, &E1TsPort);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		goto lable;
	}
	

	arg = 1;/*enable channel tx*/
	ret = Udr_ce1_port_channel_txenable(cardId,channelId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		goto lable;
	}

	arg = 1;/*enable channel rx*/
	ret = Udr_ce1_port_channel_rxenable(cardId,channelId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		goto lable;
	}

	
	arg = 5;/*cAtPppLinkPhaseNetworkActive*/
	ret = Udr_ce1_port_channel_ppp_enable(cardId,channelId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		goto lable;
	}


	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;

lable:
	At_fhn_pos_e1_soft_channel_del(cardId,port,logicalChannel);
	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;
	
}


/*
HW_IF_DS1DELCHANNEL,
*/
int _udr_if_ds1_del_channel(uint8 cardId, uint32 ce1Port, uint32 logicalChannel)
{
	tAtPdhDs0PortFhn E1TsPort={0};
	int ret=0;
	uint32 arg=0;
	uint32 channelId=0,encapType=0,flowId=0,timeslotMap=0;
	uint32 port = ce1Port;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(ce1Port<1||ce1Port>16)
	{
		printf("#%d,%s,error ce1Port=%d\r\n",__LINE__,__FUNCTION__,ce1Port);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		port = port+8;
		if((port>32) || (port<1))
		{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	if(At_fhn_pos_e1_soft_channel_get( cardId,  port,  logicalChannel, &timeslotMap, &channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}	
	
	if(timeslotMap){
		E1TsPort.e1PortId = port;
		E1TsPort.timeslotBitmap = timeslotMap;
	 	ret = Udr_ce1_port_channel_unbind_port_ts(cardId,channelId, &E1TsPort);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}

	 	ret = Udr_ce1_port_delete_port_ts(cardId,channelId, &E1TsPort);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}
	}else{
		arg = port;
		ret = Udr_ce1_port_channel_unbind_port(cardId,channelId,&arg);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}
	}

	arg = encapType;
	ret = Udr_ce1_port_channel_Delete(cardId,channelId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret = At_fhn_pos_e1_soft_channel_del(cardId, port, logicalChannel);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;

}


/*
HW_IF_TRUNK

*/
int _udr_if_ds1_trunk_create(uint8 cardId, uint32 trunkId)
{

}


/*
HW_IF_TRUNKACTIVEMEMBER
*/
int _udr_if_ds1_trunk_active(uint8 cardId, uint32 ce1Port, uint32 logicalChannel, uint32 trunkId)
{
	/*always active*/
	return 0;
}

/*
HW_IF_TRUNKINACTIVEMEMBER
*/
int _udr_if_ds1_trunk_inactive(uint8 cardId, uint32 ce1Port, uint32 logicalChannel, uint32 trunkId)
{
	/*always active*/
	return -1;
}


int _udr_if_ds1_trunk_acl_add(uint8 cardId, uint32 ce1Port, uint32 logicalChannel, uint32 trunkId, uint8 *dmac, 
								tAtEthVlanTagFhn *ouputvlan,tAtEthVlanTagFhn *inputvlan)
{
	uint32 arg;
	tAtPdhDs0PortFhn E1TsPort={0};
	int ret=0;
	uint32 channelId=0,encapType=0,flowId=0,timeslotMap=0;
	uint32 port = ce1Port;

	if((cardId>2) || (cardId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cardId=%d\r\n",cardId);
		return -1;
	}

	if((trunkId>AT_POS_E1_MAX_MLPPP_BUNDLE_FHN) || (trunkId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error trunkId=%d\r\n",trunkId);
		return -1;
	}

	if(ce1Port<1||ce1Port>16)
	{
		printf("#%d,%s,error ce1Port=%d\r\n",__LINE__,__FUNCTION__,ce1Port);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		port = port+8;
		if((port>32) || (port<1))
		{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	if(At_fhn_pos_e1_soft_channel_get( cardId,  port,  logicalChannel, &timeslotMap, &channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	flowId = AT_POS_E1_MAX_HW_CHANNEL_FHN+trunkId;
	arg=0;
	ret = Udr_ce1_port_flow_create(cardId,flowId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret = Udr_ce1_port_flow_egress_dmac_set(cardId,flowId,dmac);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret = Udr_ce1_port_flow_egress_vlan_set(cardId,1,flowId, ouputvlan);/*设置流标示的出口VLAN  */
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret = Udr_ce1_port_flow_set_vlan(cardId, 1, flowId, inputvlan);/*流标示的分类规则,只能基于VLAN */
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret = Udr_ce1_mlppp_bundle_bind_flow(cardId,trunkId,flowId);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;
}


int _udr_if_ds1_trunk_acl_del(uint8 cardId, uint32 ce1Port, uint32 logicalChannel, uint32 trunkId)
{
	uint32 arg;
	tAtPdhDs0PortFhn E1TsPort={0};
	int ret=0;
	uint32 channelId=0,encapType=0,flowId=0,timeslotMap=0;

	POS_AT_UDR_SEM_TAKE(cardId-1,10);
	flowId = AT_POS_E1_MAX_HW_CHANNEL_FHN+trunkId;
	ret = Udr_ce1_mlppp_bundle_unbind_flow(cardId,trunkId,flowId);/*绑定业务通道到流ID*/
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	encapType = 0;/*cAtEthFlowNpoPpp*/
	ret = Udr_ce1_port_flow_del(cardId,flowId,&encapType);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;
}



/*
HW_IF_TRUNKADDMEMBER
*/
int	_udr_if_ds1_trunk_add_member(uint8 cardId, uint32 ce1Port, uint32 logicalChannel, uint32 trunkId,uint8 *dmac, 
								tAtEthVlanTagFhn *ouputvlan,tAtEthVlanTagFhn *inputvlan, tAtEthVlanTagFhn *OamInputvlan)
{
	int ret=0;
	uint32 channelId=0,encapType=0,flowId=0,timeslotMap=0,arg=0;
	uint32 port = ce1Port;

	if((cardId>2) || (cardId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cardId=%d\r\n",cardId);
		return -1;
	}

	if((trunkId>AT_POS_E1_MAX_MLPPP_BUNDLE_FHN) || (trunkId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error trunkId=%d\r\n",trunkId);
		return -1;
	}

	if(ce1Port<1||ce1Port>16)
	{
		printf("#%d,%s,error ce1Port=%d\r\n",__LINE__,__FUNCTION__,ce1Port);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		port = port+8;
		if((port>32) || (port<1))
		{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);
	if(At_fhn_pos_e1_soft_channel_get(cardId,  port,  logicalChannel, &timeslotMap, &channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}
	
	ret = Udr_ce1_mlppp_bundle_add_channel(cardId, trunkId,channelId);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	gu4AtE1BundleMemberRecord[cardId-1][trunkId]++;
	printf("card[%d],trunk[%d],mem[%d]\r\n", cardId, trunkId, gu4AtE1BundleMemberRecord[cardId-1][trunkId]);
	
	if(gu4AtE1BundleMemberRecord[cardId-1][trunkId] == 1)
	{
		ret = udr_ce1_trunk_acl_add(cardId, ce1Port, logicalChannel, trunkId, dmac, ouputvlan, inputvlan);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}
	}

	/* added by ssq 20131114 */
	/*
	flowId = channelId;
	arg=0;
	ret = Udr_ce1_port_flow_create(cardId,flowId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	ret = Udr_ce1_port_channel_bind_flow(cardId,channelId,flowId);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}
	*/
	/* modified by ssq 20131221
	ret =Udr_ce1_port_oam_channel_mode(cardId, channelId);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	ret =Udr_ce1_port_oam_channel_egress_dmac_set(cardId,channelId,dmac);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	ret =Udr_ce1_port_oam_channel_egress_vlan_set(cardId, 1, channelId, OamInputvlan);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	ret =Udr_ce1_port_oam_channel_ingress_vlan_set(cardId, 1, channelId, OamInputvlan);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}
	*/
	/* end added */

	arg=1;
	ret = Udr_ce1_mlppp_bundle_enable(cardId,trunkId, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return 0;
}


/*
HW_IF_TRUNKDELMEMBER
*/
int	_udr_if_ds1_trunk_del_member(uint8 cardId, uint32 ce1Port, uint32 logicalChannel, uint32 trunkId)
{
	int ret=0;
	uint32 channelId=0,encapType=0,flowId=0,timeslotMap=0;
	uint32 port = ce1Port;

	if((cardId>2) || (cardId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cardId=%d\r\n",cardId);
		return -1;
	}

	if((trunkId>AT_POS_E1_MAX_MLPPP_BUNDLE_FHN) || (trunkId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error trunkId=%d\r\n",trunkId);
		return -1;
	}

	if(ce1Port<1||ce1Port>16)
	{
		printf("#%d,%s,error ce1Port=%d\r\n",__LINE__,__FUNCTION__,ce1Port);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		port = port+8;
		if((port>32) || (port<1))
		{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	if(At_fhn_pos_e1_soft_channel_get( cardId,  port,  logicalChannel, &timeslotMap, &channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}
	
	ret =  Udr_ce1_mlppp_bundle_delete_channel(cardId, trunkId,channelId);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	/* added by ssq 20131114 */
	/*
	flowId = channelId;
	ret = Udr_ce1_port_channel_unbind_flow(cardId,channelId,&flowId);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}
	
	encapType = 0;
	ret = Udr_ce1_port_flow_del(cardId,flowId,&encapType);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
	*/
	/* added by ssq 20131114 */

	
	gu4AtE1BundleMemberRecord[cardId-1][trunkId]--;
	printf("card[%d],trunk[%d],mem[%d]\r\n", cardId, trunkId, gu4AtE1BundleMemberRecord[cardId-1][trunkId]);

	if(gu4AtE1BundleMemberRecord[cardId-1][trunkId] == 0){
		ret = udr_ce1_trunk_acl_del(cardId, ce1Port, logicalChannel, trunkId);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			POS_AT_UDR_SEM_GIVE(cardId-1);
			return -1;
		}
	}

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return 0;
}



/*
HW_IF_SERIALCRC
typedef enum eAtHdlcFcsModeFhn
    {
    cAtHdlcFcsModeNoFcsFhn, /**< Disable FCS inserting 
    cAtHdlcFcsModeFcs16Fhn, /**< FCS-16 
    cAtHdlcFcsModeFcs32Fhn  /**< FCS-32 
    }eAtHdlcFcsModeFhn;
*/

int _udr_if_ds1_serial_crc_set(uint8 cardId, uint32 ce1Port, uint32 logicalChannel, eAtHdlcFcsModeFhn *crcMode)
{
	uint32 channelId=0,encapType=0,flowId=0,timeslotMap=0;
	uint32 port = ce1Port;
	int ret=0;

	if((cardId>2) || (cardId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cardId=%d\r\n",cardId);
		return -1;
	}

	if(ce1Port<1||ce1Port>16)
	{
		printf("#%d,%s,error ce1Port=%d\r\n",__LINE__,__FUNCTION__,ce1Port);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		port = port+8;
		if((port>32) || (port<1))
		{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	if(At_fhn_pos_e1_soft_channel_get( cardId,  port,  logicalChannel, &timeslotMap, &channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}
	
	ret = Udr_ce1_port_channel_fcs(cardId, channelId, crcMode);

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;
}


/*
HW_IF_MTU

warning : only 9600 !!
*/

int _udr_if_ds1_mtu_set(uint8 cardId, uint32 ce1Port, uint32 logicalChannel, uint32 *mtu)
{
	uint32 channelId=0,encapType=0,flowId=0,timeslotMap=0;
	uint32 port = ce1Port;
	int ret=0;

	if((cardId>2) || (cardId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cardId=%d\r\n",cardId);
		return -1;
	}

	if(ce1Port<1||ce1Port>16)
	{
		printf("#%d,%s,error ce1Port=%d\r\n",__LINE__,__FUNCTION__,ce1Port);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		port = port+8;
		if((port>32) || (port<1))
		{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);
	if(At_fhn_pos_e1_soft_channel_get( cardId,  port,  logicalChannel, &timeslotMap, &channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret = Udr_ce1_port_channel_mtu_set(cardId, channelId, mtu);

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;
}


/*
HW_IF_STATISTICS
typedef struct tAtHdlcChannelCountersFhn
{
    uint32 txGoodPktFhn;       /**< Transmit good packet 1
    uint32 txAbortPktFhn;      /**< Transmit Abort packet 2

    uint32 rxGoodPktFhn;       /**< Receive good packet 3
    uint32 rxAbortPktFhn;      /**< Receive Abort packet 4
    uint32 rxFcsErrPktFhn;     /**< Receive Fcs Error packet 5
    uint32 rxAddrCtrlErrPktFhn;/**< Receive Address Control error packet 6
    uint32 rxSapiErrPktFhn;    /**< Receive Sapi error packet 7
    uint32 rxErrPktFhn;        /**< Receive error packet 8
}tAtHdlcChannelCountersFhn;
*/

int _udr_if_ds1_statistics_get(uint8 cardId, uint32 ce1Port, uint32 logicalChannel, tAtHdlcChannelCountersFhn *counters)
{
	uint32 channelId=0,encapType=0,flowId=0,timeslotMap=0;
	uint32 port = ce1Port;
	int ret=0;

	if((cardId>2) || (cardId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cardId=%d\r\n",cardId);
		return -1;
	}

	if(ce1Port<1||ce1Port>16)
	{
		printf("#%d,%s,error ce1Port=%d\r\n",__LINE__,__FUNCTION__,ce1Port);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		port = port+8;
		if((port>32) || (port<1))
		{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	if(At_fhn_pos_e1_soft_channel_get( cardId,  port,  logicalChannel, &timeslotMap, &channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret = Udr_ce1_channel_counter_get(cardId, channelId, counters);

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;

}

int _udr_if_ds1_statistics_clear(uint8 cardId, uint32 ce1Port, uint32 logicalChannel)
{
	uint32 channelId=0,encapType=0,flowId=0,timeslotMap=0;
	uint32 port = ce1Port;
	int ret=0;

	if((cardId>2) || (cardId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cardId=%d\r\n",cardId);
		return -1;
	}

	if(ce1Port<1||ce1Port>16)
	{
		printf("#%d,%s,error ce1Port=%d\r\n",__LINE__,__FUNCTION__,ce1Port);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		port = port+8;
		if((port>32) || (port<1))
		{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);

	if(At_fhn_pos_e1_soft_channel_get( cardId,  port,  logicalChannel, &timeslotMap, &channelId, &encapType) == -1){
		POS_AT_UDR_SEM_GIVE(cardId-1);
		return -1;
	}

	ret = Udr_ce1_channel_counter_clear(cardId, channelId, &channelId);
	
	POS_AT_UDR_SEM_GIVE(cardId-1);
	return ret;

}


int _udr_if_ds1_fpga_version_get(uint8 cardId, uint32* fpgaVersion)
{
	uint32 channelId=0,encapType=0,flowId=0,timeslotMap=0;

	if((cardId>2) || (cardId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cardId=%d\r\n",cardId);
		return -1;
	}

	POS_AT_UDR_SEM_TAKE(cardId-1,10);
	At_register_rd(cardId,1,2,fpgaVersion);

	POS_AT_UDR_SEM_GIVE(cardId-1);
	return 0;
}



int _test_udr_if_16ds1_add_by_card(uint32 cardId)
{
	tAtEthVlanTagFhn ouputvlan,inputvlan;
	tAtTimeModeFhn clock;
	int ret=0;
	uint32 e1Port=0,logicalChannel=0;
	uint8 smac[]={0x00,0x04,0x67,0x00,0x00,0x66};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint32 arg=0,e1CardPort=16;

	ret = _udr_at_pos_e1_card_smac_set(cardId, smac);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		e1CardPort=8;
	}

	for(e1Port=1;e1Port<=e1CardPort;e1Port++)
	{
		clock.TimeModen = 1;/*cAtTimingModeSys*/
		clock.refPort = 1;/* only used in slave*/
		ret = _udr_if_ds1_txclock_src_config(cardId,e1Port,&clock);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}
		
		arg = 1;/*cAtHdlcFrmPppCreate*/
		logicalChannel = e1Port;
		ret = _udr_if_ds1_encap_type_set(cardId,e1Port,logicalChannel, &arg);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_ds1_add_channel(cardId,e1Port,logicalChannel);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ouputvlan.cfi=0;
		ouputvlan.priority=7;
		ouputvlan.vlanId=e1Port;
		inputvlan.cfi=0;
		inputvlan.priority=7;
		inputvlan.vlanId = e1Port;
		ret = _udr_if_ds1_channel_acl_add(cardId, e1Port, logicalChannel, dmac, &ouputvlan, &inputvlan);		
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		arg=1;
		_udr_if_ds1_port_status_set(cardId, e1Port, &arg);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		
	}

}


int _test_udr_if_16ds1_del_by_card(uint32 cardId)
{
	int ret=0;
	uint32 e1Port=0,logicalChannel=0,e1CardPort=16;

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		e1CardPort=8;
	}

	for(e1Port=1;e1Port<=e1CardPort;e1Port++)
	{
		logicalChannel = e1Port;
		ret = _udr_if_ds1_channel_acl_del(cardId, e1Port, logicalChannel);	
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = _udr_if_ds1_del_channel(cardId,e1Port,logicalChannel);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}
		
	}

}


/*
_test_udr_if_ds1_trunk_add_by_port 1,1,1,1
_test_udr_if_ds1_trunk_add_by_port 1,3,1,1
_test_udr_if_ds1_trunk_add_by_port 1,5,1,1
_test_udr_if_ds1_trunk_add_by_port 1,7,1,1

_test_udr_if_ds1_trunk_add_by_port 1,2,1,2
_test_udr_if_ds1_trunk_add_by_port 1,4,1,2
_test_udr_if_ds1_trunk_add_by_port 1,6,1,2
_test_udr_if_ds1_trunk_add_by_port 1,8,1,2


_test_udr_if_ds1_trunk_add_by_port 2,1,1,1
_test_udr_if_ds1_trunk_add_by_port 2,3,1,1
_test_udr_if_ds1_trunk_add_by_port 2,5,1,1
_test_udr_if_ds1_trunk_add_by_port 2,7,1,1

_test_udr_if_ds1_trunk_add_by_port 2,2,1,2
_test_udr_if_ds1_trunk_add_by_port 2,4,1,2
_test_udr_if_ds1_trunk_add_by_port 2,6,1,2
_test_udr_if_ds1_trunk_add_by_port 2,8,1,2

*/
int _test_udr_if_ds1_trunk_add_by_port(uint8 cardId, uint32 e1Port, uint32 logicalChannel, uint32 trunkId)
{
	int ret;
	tAtEthVlanTagFhn ouputvlan,inputvlan,oamInputvlan;
	tAtTimeModeFhn clock;
	uint8 smac[]={0x00,0x04,0x67,0x00,0x00,0x66};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint32 arg=0;
	uint32 channelId=0,encapType=0,flowId=0,timeslotMap=0;

	ret = _udr_at_pos_e1_card_smac_set(cardId, smac);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	clock.TimeModen = 1;/*cAtTimingModeSys*/
	clock.refPort = 1;/* only used in slave*/
	ret = _udr_if_ds1_txclock_src_config(cardId,e1Port,&clock);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
	
	arg = 1;/*cAtHdlcFrmPppCreate*/
	ret = _udr_if_ds1_encap_type_set(cardId,e1Port,logicalChannel, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	if(At_fhn_pos_e1_soft_channel_get(cardId, e1Port, logicalChannel, &timeslotMap, &channelId, &encapType) == -1){
		/* have not create channel, so creat it*/
		ret = _udr_if_ds1_add_channel(cardId,e1Port,logicalChannel);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}
	}

	ouputvlan.cfi=0;
	ouputvlan.priority=0;
	ouputvlan.vlanId = 10+trunkId;
	inputvlan.cfi=0;
	inputvlan.priority=0;
	inputvlan.vlanId = 10+trunkId;
	oamInputvlan.cfi=0;
	oamInputvlan.priority=7;
	oamInputvlan.vlanId = 20+e1Port;
	
	ret = _udr_if_ds1_trunk_add_member(cardId, e1Port, logicalChannel, trunkId, dmac, &ouputvlan, &inputvlan,&oamInputvlan);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
	
	return 0;

}


/*
_test_udr_if_ds1_trunk_del_by_port 1,1,1,1
_test_udr_if_ds1_trunk_del_by_port 1,3,1,1
_test_udr_if_ds1_trunk_del_by_port 1,5,1,1
_test_udr_if_ds1_trunk_del_by_port 1,7,1,1

_test_udr_if_ds1_trunk_del_by_port 1,2,1,2
_test_udr_if_ds1_trunk_del_by_port 1,4,1,2
_test_udr_if_ds1_trunk_del_by_port 1,6,1,2
_test_udr_if_ds1_trunk_del_by_port 1,8,1,2


_test_udr_if_ds1_trunk_del_by_port 2,1,1,1
_test_udr_if_ds1_trunk_del_by_port 2,3,1,1
_test_udr_if_ds1_trunk_del_by_port 2,5,1,1
_test_udr_if_ds1_trunk_del_by_port 2,7,1,1

_test_udr_if_ds1_trunk_del_by_port 2,2,1,2
_test_udr_if_ds1_trunk_del_by_port 2,4,1,2
_test_udr_if_ds1_trunk_del_by_port 2,6,1,2
_test_udr_if_ds1_trunk_del_by_port 2,8,1,2
*/
int _test_udr_if_ds1_trunk_del_by_port(uint8 cardId, uint32 e1Port, uint32 logicalChannel, uint32 trunkId)
{
	int ret;

	ret = _udr_if_ds1_trunk_del_member(cardId, e1Port, logicalChannel, trunkId);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	return 0;

}


/*
_test_udr_if_16ds1_add_ts_by_port 1,1,1,0xf,1
_test_udr_if_16ds1_add_ts_by_port 1,2,1,0xf,2

_test_udr_if_16ds1_add_ts_by_port 1,1,2,0xff0,3
_test_udr_if_16ds1_add_ts_by_port 1,2,2,0xff0,4


_test_udr_if_16ds1_add_ts_by_port 2,1,1,0xf,1
_test_udr_if_16ds1_add_ts_by_port 2,2,1,0xf,2

_test_udr_if_16ds1_add_ts_by_port 2,1,2,0xff0,3
_test_udr_if_16ds1_add_ts_by_port 2,2,2,0xff0,4


*/
int _test_udr_if_16ds1_add_ts_by_port(uint8 cardId, uint32 e1Port ,uint32 logicalChannel,uint32 timeSlotBitmap,uint32 vlan)
{
	tAtEthVlanTagFhn ouputvlan,inputvlan;
	tAtTimeModeFhn clock;
	int ret=0;
	uint8 smac[]={0x00,0x04,0x67,0x00,0x00,0x66};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint32 arg=0;

	ret = _udr_at_pos_e1_card_smac_set(cardId, smac);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	arg = 9;/*cAtPdhE1Frm*/
	ret = _udr_if_ds1_line_type(cardId, e1Port,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
	
	clock.TimeModen = 1;/*cAtTimingModeSys*/
	clock.refPort = 1;/* only used in slave*/
	ret = _udr_if_ds1_txclock_src_config(cardId,e1Port,&clock);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}
	
	arg = 1;/*cAtHdlcFrmPppCreate*/
	ret = _udr_if_ds1_encap_type_set(cardId,e1Port,logicalChannel, &arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_if_ds1_add_channel_timeslot(cardId,e1Port,logicalChannel,timeSlotBitmap);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ouputvlan.cfi=0;
	ouputvlan.priority=7;
	ouputvlan.vlanId=vlan;
	inputvlan.cfi=0;
	inputvlan.priority=7;
	inputvlan.vlanId = vlan;
	ret = _udr_if_ds1_channel_acl_add(cardId, e1Port, logicalChannel, dmac, &ouputvlan, &inputvlan);		
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

}


/*
_test_udr_if_16ds1_del_ts_by_port 1,1,1
_test_udr_if_16ds1_del_ts_by_port 1,2,1

_test_udr_if_16ds1_del_ts_by_port 1,1,2
_test_udr_if_16ds1_del_ts_by_port 1,2,2


_test_udr_if_16ds1_del_ts_by_port 2,1,1
_test_udr_if_16ds1_del_ts_by_port 2,2,1

_test_udr_if_16ds1_del_ts_by_port 2,1,2
_test_udr_if_16ds1_del_ts_by_port 2,2,2

*/
int _test_udr_if_16ds1_del_ts_by_port(uint8 cardId, uint32 e1Port ,uint32 logicalChannel)
{
	int ret=0;

	ret = _udr_if_ds1_channel_acl_del(cardId, e1Port, logicalChannel);	
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = _udr_if_ds1_del_channel(cardId,e1Port,logicalChannel);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

}







/* ssq 20130522 */
#include "semLib.h"

typedef enum eAtEncapTypeCreateFhn
    {
    cAtHdlcFrmCiscoHdlcCreateFhn,/**< Cisco HDLC */
    cAtHdlcFrmPppCreateFhn,      /**< PPP */
    cAtHdlcFrmFrCreateFhn,       /**< Frame relay */
    cAtEncapAtmCreateFhn         /**< ATM */
    } eAtEncapTypeCreateFhn;

typedef enum eAtHdlcFrameTypeFhn
    {
    cAtHdlcFrmUnknownFhn,   /**< Un-known frame type */
    cAtHdlcFrmCiscoHdlcFhn, /**< Cisco HDLC */
    cAtHdlcFrmPppFhn,       /**< PPP */
    cAtHdlcFrmFrFhn,         /**< Frame relay */
    }eAtHdlcFrameTypeFhn;

typedef enum eAtHdlcFcsModeFhn
    {
    cAtHdlcFcsModeNoFcsFhn, /**< Disable FCS inserting */
    cAtHdlcFcsModeFcs16Fhn, /**< FCS-16 */
    cAtHdlcFcsModeFcs32Fhn  /**< FCS-32 */
    }eAtHdlcFcsModeFhn;

typedef struct tAtchannelMapToSdh
{
	uint32 au3Tug3Id;
	uint32 tug2Id;
	uint32 tuId;
}tAtchannelMapToSdh;

typedef struct tAtchannelMapToSdhSTM4ToE3
{
	uint32 aug1Id;
	uint32 tug3Id;
}tAtchannelMapToSdhSTM4ToE3;

typedef struct tAtEthVlanTagFhn
    {
    uint8 priority; /**< [0..7] Vlan priority, can be used to in QoS. The field
                         indicates the frame priority level which can be used for
                         the priority of traffic. The field can represent
                         8 levels */
    uint8 cfi;      /**< [0..1] Canonical Format Indicator. If the value of this
                         field is 1, the MAC address is in noncanonical format.
                         If the value is 0, the MAC address is in canonical format */
    word vlanId;   /**<  [0..4095] VLAN ID */
    }tAtEthVlanTagFhn;
/* end */

typedef enum eAtEthFlowTypeFhn
    {
    cAtEthFlowIpoPppFhn,
    cAtEthFlowEthoPppFhn
    }eAtEthFlowTypeFhn;

typedef enum eAtSdhChannelTypeFhn
{
	cAtSdhChannelTypeFhnUnknown, /**< Unknown */
	cAtSdhChannelTypeFhnaug1,    /** aug1-vc4 */
	cAtSdhChannelTypeFhnvc4,   /** vc4-tug3 */
	cAtSdhChannelTypeFhnau3Tug3,    /** tug3-tug2 */
	cAtSdhChannelTypeFhntug2,    /** tug2-tu12 */
	cAtSdhChannelTypeFhntu,    /** vc1x-de1 */

}eAtSdhChannelTypeFhn;

/* PDH module */
typedef struct tAtsdhMapPort
{
	eAtSdhChannelTypeFhn channelType;
	uint32 lineId;
	uint32 aug4Id;
	uint32 aug1Id;
	uint32 vc4Id;
	uint32 au3Tug3Id;
	uint32 tug2Id;
	uint32 tuId;
}tAtsdhMapPort;

/* PDH module */
typedef struct tAtPdhDe1Port
{
	uint32 lineId;
	uint32 aug1Id;
	uint32 au3Tug3Id;
	uint32 tug2Id;
	uint32 tuId;
}tAtPdhDe1Port;


typedef struct tAtSdhTtiFhn{
	eAtSdhTtiMode mode;                       
	uint8 message[cAtSdhChannelMaxTtiLength];
	int8 paddingMode;/* 0: null_padding  1: space_padding*/
}tAtSdhTtiFhn;

/* PDH module */
typedef struct tAtPdhDe1PortFhn
{
	uint32 e1PortId;
	uint32 timeslotBitmap;;
}tAtPdhDs0PortFhn;

typedef enum eAtPdhLoopbackModeFhn
    {
    cAtPdhLoopbackModeReleaseFhn,        /**< Release loopback */

    cAtPdhLoopbackModeLocalPayloadFhn,   /**< Payload Local loopback */
    cAtPdhLoopbackModeRemotePayloadFhn,  /**< Payload Remote loopback */

    cAtPdhLoopbackModeLocalLineFhn,      /**< Line Local loopback */
    cAtPdhLoopbackModeRemoteLineFhn      /**< Line Local loopback */
    }eAtPdhLoopbackModeFhn;

/** @brief Timing modes */
typedef enum eAtTimingModeFhn
    {
    cAtTimingModeUnknownFhn    , /**< Unknown timing mode */
    cAtTimingModeSysFhn        , /**< System timing mode */
    cAtTimingModeLoopFhn       , /**< Loop timing mode */
    cAtTimingModeSdhLineRefFhn , /**< SDH Line */
    cAtTimingModeExt1RefFhn    , /**< Line EXT #1 timing mode */
    cAtTimingModeExt2RefFhn    , /**< Line EXT #2 timing mode */
    cAtTimingModeAcrFhn        , /**< ACR timing mode */
    cAtTimingModeDcrFhn        , /**< DCR timing mode */
    cAtTimingModeFreeFhn       , /**< Free running mode - using system clock for CDR
                                    source to generate service TX clock*/
    cAtTimingModeTopFhn        , /**< ToP timing mode */
    cAtTimingModeSlaveFhn        /**< Slave timing mode */
    }eAtTimingModeFhn;


typedef struct tAtTimeModeFhn
{
	eAtTimingModeFhn TimeModen;
	uint32 refPort;
}tAtTimeModeFhn;

typedef enum eAtSdhLineRateFhn
    {
    cAtSdhLineRateStm1Fhn,    /**< STM-1 */
    cAtSdhLineRateStm4Fhn,    /**< STM-4 */
    cAtSdhLineRateStm16fhn    /**< STM-16 */
    }eAtSdhLineRateFhn;


typedef enum eAtSdhLineAlarmTypeFhn
    {
    cAtSdhLineAlarmLosFhn   = cBit0,       /**< @brief RS-LOS defect mask */
    cAtSdhLineAlarmOofFhn   = cBit1,       /**< @brief RS-OOF defect mask */
    cAtSdhLineAlarmLofFhn   = cBit2,       /**< @brief RS-LOF defect mask */
    cAtSdhLineAlarmTimFhn   = cBit3,       /**< @brief RS-TIM defect mask */
    cAtSdhLineAlarmAisFhn   = cBit4,       /**< @brief MS-AIS defect mask */
    cAtSdhLineAlarmRdiFhn   = cBit5,       /**< @brief MS-RDI defect mask */
    cAtSdhLineAlarmBerSdFhn = cBit6,       /**< @brief MS-BER-SD defect mask */
    cAtSdhLineAlarmBerSfFhn = cBit7,       /**< @brief MS-BER-SF defect mask */
    cAtSdhLineAlarmKByteChangeFhn = cBit8, /**< @brief K-byte change event.
                                             Stable K-byte is changed */
    cAtSdhLineAlarmKByteFailFhn = cBit9,   /**< @brief K-byte fail. There is no stable K-byte */
    cAtSdhLineAlarmS1ChangeFhn  = cBit10   /**< @brief S1 byte change event. */
    }eAtSdhLineAlarmTypeFhn;



typedef struct eUdrSdhLineAlarmTypeFhn
    {
    uint8 udrSdhLineAlarmLosFhn;      /**< @brief RS-LOS defect mask */
    uint8 udrSdhLineAlarmOOFFhn;       /**< @brief RS-OOF defect mask */
    uint8 udrSdhLineAlarmLOFFhn;        /**< @brief RS-LOF defect mask */
    uint8 udrSdhLineAlarmTIMFhn;         /**< @brief RS-TIM defect mask */
    uint8 udrSdhLineAlarmAISFhn;          /**< @brief MS-AIS defect mask */
    uint8 udrSdhLineAlarmRDIFhn;        /**< @brief MS-RDI defect mask */
    uint8 udrSdhLineAlarmBERSDFhn;         /**< @brief MS-BER-SD defect mask */
    uint8 udrSdhLineAlarmBERSFFhn;         /**< @brief MS-BER-SF defect mask */
    uint8 udrSdhLineAlarmKBCHANGEFhn;    /**< @brief K-byte change event.
                                             Stable K-byte is changed */
    uint8 udrtSdhLineAlarmKBFAILFhn;      /**< @brief K-byte fail. There is no stable K-byte */
    uint8 udrSdhLineAlarmS1CHANGEFhn;     /**< @brief S1 byte change event. */
    }eUdrSdhLineAlarmTypeFhn;


/**
 * @brief Alarm types
 */
typedef enum eAtPdhDe1AlarmTypeFhn
    {
    cAtPdhDe1AlarmNoneFhn   = 0,      /**< No alarm */
    cAtPdhDe1AlarmLosFhn    = cBit0,  /**< LOS */
    cAtPdhDe1AlarmLofFhn    = cBit1,  /**< LOF */
    cAtPdhDe1AlarmAisFhn    = cBit2,  /**< AIS */
    cAtPdhDe1AlarmRaiFhn    = cBit3,  /**< RAI */
    cAtPdhDe1AlarmLomfFhn   = cBit4,  /**< LOMF */
    cAtPdhDe1AlarmSfBerFhn  = cBit5,  /**< SF BER */
    cAtPdhDe1AlarmSdBerFhn  = cBit6,  /**< SD BER */
    cAtPdhDe1AlarmSigLofFhn = cBit7,  /**< LOF on signaling channel */
    cAtPdhDe1AlarmSigRaiFhn = cBit8,  /**< RAI on signaling channel */
    cAtPdhDe1AlarmAllFhn    = cBit8_0 /**< All alarms */
    }eAtPdhDe1AlarmTypeFhn;

typedef struct eUdrPdhDe1AlarmTypeFhn
{
    uint8 cUdrAtPdhDe1AlarmLosFhn;  /**< LOS */
    uint8 cUdrAtPdhDe1AlarmLofFhn;  /**< LOF */
    uint8 cUdrAtPdhDe1AlarmAisFhn;  /**< AIS */
    uint8 cUdrAtPdhDe1AlarmRaiFhn;  /**< RAI */
    uint8 cUdrAtPdhDe1AlarmLomfFhn;  /**< LOMF */
    uint8 cUdrAtPdhDe1AlarmSfBerFhn;  /**< SF BER */
    uint8 cUdrAtPdhDe1AlarmSdBerFhn;  /**< SD BER */
    uint8 cUdrAtPdhDe1AlarmSigLofFhn;  /**< LOF on signaling channel */
    uint8 cUdrAtPdhDe1AlarmSigRaiFhn;  /**< RAI on signaling channel */
}eUdrPdhDe1AlarmTypeFhn;


/** @brief All counters of Ethernet port */
typedef struct tAtEthPortCountersFhn
    {
    /* Transmitted packets */
    uint32 txPacketsFhn;              /**< Number of TX packets 1*/
    uint32 txBytesFhn;                /**< Number of TX bytes 2*/
    uint32 txOamPacketsFhn;           /**< Number of TX OAM Packets, which are packets sent from CPU or
                                        supported OAM packets such as VCCV, LDP, ARP, ICMP, ...  3*/
    uint32 txPeriodOamPacketsFhn;     /**< Number of OAM packets which are periodically sent by THALASSA device 4*/
    uint32 txPwPacketsFhn;            /**< Number of TX PW data packet counter, not include OAM packets 5*/
    uint32 txPacketsLen0_64Fhn;       /**< Number of TX packets with length from 0 to 64 6*/
    uint32 txPacketsLen65_127Fhn;     /**< Number of TX packets with length from 65 to 127 7*/
    uint32 txPacketsLen128_255Fhn;    /**< Number of TX packets with length from 128 to 255 8*/
    uint32 txPacketsLen256_511Fhn;    /**< Number of TX packets with length from 256 to 511 9*/
    uint32 txPacketsLen512_1024Fhn;   /**< Number of TX packets with length from 512 to 1023 10*/
    uint32 txPacketsLen1025_1528Fhn;  /**< Number of TX packets with length from 1024 to 1518 11*/
    uint32 txPacketsJumboFhn;         /**< Number of TX packets with length greater than 1518 12*/
    uint32 txTopPacketsFhn;           /**< Number of TX Timing over packet counter 13*/

    /* Received packets */
    /* MAC layer */
    uint32 rxPacketsFhn;              /**< Number of RX packets 14*/
    uint32 rxBytesFhn;                /**< Number of RX bytes 15*/
    uint32 rxErrEthHdrPacketsFhn;     /**< Number of Rx error Ethernet header packets 16*/
    uint32 rxErrBusPacketsFhn;        /**< Number of Rx bus error packets 17*/
    uint32 rxErrFcsPacketsFhn;        /**< Number of Rx FCS error packets 18*/
    uint32 rxOversizePacketsFhn;      /**< Number of packets has size greater than programmable maximum size 19*/
    uint32 rxUndersizePacketsFhn;     /**< Number of packets has size greater than programmable minimum size 20*/
    uint32 rxPacketsLen0_64Fhn;       /**< Number of RX packets with length from 0 to 64 21*/
    uint32 rxPacketsLen65_127Fhn;     /**< Number of RX packets with length from 65 to 127 22*/
    uint32 rxPacketsLen128_255Fhn;    /**< Number of RX packets with length from 128 to 255 23*/
    uint32 rxPacketsLen256_511Fhn;    /**< Number of RX packets with length from 256 to 511 24*/
    uint32 rxPacketsLen512_1024Fhn;   /**< Number of RX packets with length from 512 to 1023 25*/
    uint32 rxPacketsLen1025_1528Fhn;  /**< Number of RX packets with length from 1024 to 1518 26*/
    uint32 rxPacketsJumboFhn;         /**< Number of RX packets with length greater than 1518 27*/
    uint32 rxPwUnsupportedPacketsFhn; /**< Number of DA MAC address not matched with one of SA MAC addresses 28*/
    uint32 rxErrPwLabelPacketsFhn;    /**< Number of packets have Unsupported Ethernet Type or Unknown PW labels 29*/
    uint32 rxDiscardedPacketsFhn;     /**< Number of discarded packets has packet type is configured to be discarded 30*/
    uint32 rxPausePacketsFhn;         /**< Number of RX Pause packets 31*/
    uint32 rxErrPausePacketsFhn;      /**< Number of RX Error Pause packets 32*/
    uint32 rxBrdCastPacketsFhn;       /**< Number of RX broadcast packets 33*/
    uint32 rxMultCastPacketsFhn;      /**< Number of RX multicast packets 34*/
    uint32 rxArpPacketsFhn;           /**< Number of RX ARP packets 35*/
    uint32 rxOamPacketsFhn;           /**< Number of RX clasified OAM Packets 36*/
    uint32 rxEfmOamPacketsFhn;        /**< Number of RX EFM OAM packets 37*/
    uint32 rxErrEfmOamPacketsFhn;     /**< Number of RX Error EFM OAM packets 38*/
    uint32 rxEthOamType1PacketsFhn;   /**< Number of RX ETHERNET OAM type 1 packets,
                                        which are packets have DA MAC match the programmable
                                        DA MAC of the type of Ethernet OAM 39*/
    uint32 rxEthOamType2PacketsFhn;   /**< Number of RX ETHERNET OAM type 1 packets,
                                        which are packets have DA MAC match the programmable
                                        DA MAC of the other type of Ethernet OAM 40*/

    /* Upper layer */
    /* IPv4 */
    uint32 rxIpv4PacketsFhn;          /**< Number of RX Ipv4 packets 41*/
    uint32 rxErrIpv4PacketsFhn;       /**< Number of RX error IPv4 packets 42*/
    uint32 rxIcmpIpv4PacketsFhn;      /**< Number of RX ICMP IPv4 packets 43*/

    /* IPv6 */
    uint32 rxIpv6PacketsFhn;          /**< Number of RX multicast packets 44*/
    uint32 rxErrIpv6PacketsFhn;       /**< Number of RX error IPv4 packets 45*/
    uint32 rxIcmpIpv6PacketsFhn;      /**< Number of RX ICMP IPv4 packets 46*/

    /* MEF */
    uint32 rxMefPacketsFhn;           /**< Number of RX MEF packets 47*/
    uint32 rxErrMefPacketsFhn;        /**< Number of RX Error MEF packets 48*/

    /* MPLS */
    uint32 rxMplsPacketsFhn;            /**< Number of RX MPLS packets 49*/
    uint32 rxErrMplsPacketsFhn;         /**< Number of RX Error MPLS packets 50*/
    uint32 rxMplsErrOuterLblPacketsFhn; /**< Number of RX Error outer label MPLS packets 51*/
    uint32 rxMplsDataPacketsFhn;        /**< Number of RX Data MPLS packets 52*/
    uint32 rxLdpIpv4PacketsFhn;         /**< Number of RX LDP Ipv4 packets 53*/
    uint32 rxLdpIpv6PacketsFhn;         /**< Number of RX LDP Ipv6 packets 54*/
    uint32 rxMplsIpv4PacketsFhn;        /**< Number of RX MPLS Ipv4 packets 55*/
    uint32 rxMplsIpv6PacketsFhn;        /**< Number of RX MPLS Ipv6 packets 56*/

    /* L2TP */
    uint32 rxL2tpv3PacketsFhn;          /**< Number of RX L2TPv3 packets 57*/
    uint32 rxErrL2tpv3PacketsFhn;       /**< Number of RX Error L2TPv3 packets 58*/
    uint32 rxL2tpv3Ipv4PacketsFhn;      /**< Number of RX L2TPv3 Ipv4 packets 59*/
    uint32 rxL2tpv3Ipv6PacketsFhn;      /**< Number of RX L2TPv3 Ipv6 packets 60*/

    /* UDP */
    uint32 rxUdpPacketsFhn;             /**< Number of RX UDP packets 61*/
    uint32 rxErrUdpPacketsFhn;          /**< Number of RX Error UDP packets 62*/
    uint32 rxUdpIpv4PacketsFhn;         /**< Number of RX UDP Ipv4 packets 63*/
    uint32 rxUdpIpv6PacketsFhn;         /**< Number of RX UDP Ipv6 packets 64*/

    /* Other types */
    uint32 rxErrPsnPacketsFhn;         /**< Number of Rx error PSN header packets 65*/
    uint32 rxPacketsSendToCpuFhn;       /**< Number of packets sent to CPU 66*/
    uint32 rxPacketsSendToPwFhn;        /**< Number of packets sent to all PWs use this ports 67*/
    uint32 rxTopPacketsFhn;             /**< Number of Timing Over Packets 68*/
    uint32 rxPacketDaMisFhn;            /**< Number of RX Mismatch DA Packets 69*/
    }tAtEthPortCountersFhn;

/**
 * @brief HDLC channel counters
 */
typedef struct tAtHdlcChannelCountersFhn
{
    uint32 txGoodPktFhn;       /**< Transmit good packet 1*/
    uint32 txAbortPktFhn;      /**< Transmit Abort packet 2*/

    uint32 rxGoodPktFhn;       /**< Receive good packet 3*/
    uint32 rxAbortPktFhn;      /**< Receive Abort packet 4*/
    uint32 rxFcsErrPktFhn;     /**< Receive Fcs Error packet 5*/
    uint32 rxAddrCtrlErrPktFhn;/**< Receive Address Control error packet 6*/
    uint32 rxSapiErrPktFhn;    /**< Receive Sapi error packet 7*/
    uint32 rxErrPktFhn;        /**< Receive error packet 8*/
}tAtHdlcChannelCountersFhn;

typedef struct tAtEthFlowCountersFhn
    {
    uint32 txByteFhn;           /**< Transmit byte counter 1*/
    uint32 txPacketFhn;         /**< Transmit packet counter 2*/
    uint32 txGoodByteFhn;       /**< Transmit good byte counter 3*/
	uint32 txFragmentFhn;       /**< Transmit fragment counter 4*/
	uint32 txNullFragmentFhn;   /**< Transmit null fragments counter 5*/    
	uint32 txDiscardPacketFhn;  /**< Transmit discard packet counter 6*/
    uint32 txDiscardByteFhn;    /**< Transmit discard byte counter 7*/
    uint32 txGoodPktFhn;        /**< Transmit good packet counter 8*/
    uint32 txGoodFragmentFhn;   /**< Transmit good fragment 9*/
    uint32 txDiscardFragmentFhn;/**< Transmit discard fragment 10*/
    uint32 windowViolationFhn;  /**< Window violation counter 11*/
    uint32 rxByteFhn;           /**< Receive byte counter 12*/
    uint32 rxPacketFhn;         /**< Receive packet counter 13*/
    uint32 rxGoodByteFhn;       /**< Receive good byte counter 14*/
    uint32 rxGoodPktFhn;        /**< Receive good packet counter 15*/
    uint32 rxFragmentFhn;       /**< Receive fragment counter 16*/
    uint32 rxNullFragmentFhn;   /**< Receive null fragments counter 17*/
    uint32 rxDiscardPktFhn;     /**< Receive discard packet counter 18*/
    uint32 rxDiscardByteFhn;    /**< Receive discard packet byte 19*/
    uint32 mrruExceedFhn;       /**< MRRU exceeded counter 20*/
    }tAtEthFlowCountersFhn;

/* added by ssq 20130325 from main.c demo */
/*******************************************************************************************************/
typedef struct tAtHalDefault * AtHalDefault;

 typedef struct tAtHalDefaultMethods
    {
    uint8 dummy; /* To make some compiler be happy */
    }tAtHalDefaultMethods;

 typedef struct tAtHalDefault
    {
    tAtHal super;
    const tAtHalDefaultMethods *methods;

    /* Private variables */
    uint32 baseAddress;
    }tAtHalDefault;


/*--------------------------- Define -----------------------------------------*/
#define cOptString           "hs:"

#define cDeviceBaseAddress 0xC0000000

/*--------------------------- Macros -----------------------------------------*/
#define mBaseAddressOfCore(coreId)     (0xC0000000 + (coreId * 0x4000000))

/*--------------------------- Local typedefs ---------------------------------*/
typedef enum eImageType
    {
    cImageTypeReal,
    cImageType16E1 = 0x60070013,
    cImageTypeStm  = 0x60070023
    }eImageType;

typedef struct tAtHwE1channelRecordNodeFhn
{
	uint32	usedFlag; 	/* 0: unused 1: used*/
	uint32	channelId;	/* 128 in E1  */
	uint32	ce1Port;
	uint32     logicalChannelId;
	uint32	ce1portTimeslot;
	uint32 	encapType; /* PPP / MLPPP */
}tAtHwE1channelRecordNodeFhn;

typedef struct tAtHwStmchannelRecordNodeFhn
{
	uint32	usedFlag; 		/* 0: unused 1: used*/
	uint32	channelId;		/* 512 in STM*/
	uint32	cposPort; 		/* 1-8 */
	uint32	cposPortVc12Ch; 	/* 1-252*/
	uint32	cposPortVc12ChTimeslot;
	uint32     logicalChannelId;
	uint32 	encapType; 		/* PPP / MLPPP */
}tAtHwStmchannelRecordNodeFhn;

typedef struct tAtHwStmconfigRecordFhn
{
	eAtTimingModeFhn	timeModen;
	uint32	refPort;
	uint32     framerType;
	uint32 	encapType; 		/* PPP / MLPPP */
}tAtHwStmconfigRecordFhn;


typedef enum eAtSdhLineModeFhn
    {
    cAtSdhLineModeSdhFhn,     /**< SDH */
    cAtSdhLineModeSonetFhn    /**< SONET */
    }eAtSdhLineModeFhn;

typedef enum eAtLoopbackModeFhn
    {
    cAtLoopbackModeReleaseFhn, /**< Release loopback */
    cAtLoopbackModeLocalFhn,   /**< Local loopback */
    cAtLoopbackModeRemoteFhn   /**< Remote loopback */
    }eAtLoopbackModeFhn;

typedef enum eAtPdhDe1FrameTypeFhn
    {
    cAtPdhDe1FrameUnknownFhn , /**< Unknown frame */

    cAtPdhDs1J1UnFrmFhn      , /**< DS1/J1 Unframe */
    cAtPdhDs1FrmSfFhn        , /**< DS1 SF (D4) */
    cAtPdhDs1FrmEsfFhn       , /**< DS1 ESF */
    cAtPdhDs1FrmDDSFhn       , /**< DS1 DDS */
    cAtPdhDs1FrmSLCFhn       , /**< DS1 SLC */

    cAtPdhJ1FrmSfFhn         , /**< J1 SF */
    cAtPdhJ1FrmEsfFhn        , /**< J1 ESF */

    cAtPdhE1UnFrmFhn         , /**< E1 unframe */
    cAtPdhE1FrmFhn           , /**< E1 basic frame, FAS/NFAS framing */
    cAtPdhE1MFCrcFhn           /**< E1 Multi-Frame with CRC4 */
    }eAtPdhDe1FrameTypeFhn;


/** @brief BER rates */
typedef enum eAtBerRateFhn
    {
    cAtBerRateUnknownFhn, /**< Unknown error rate */
    cAtBerRate1E3Fhn    , /**< Error rate is 10-3 */
    cAtBerRate1E4Fhn    , /**< Error rate is 10-4 */
    cAtBerRate1E5Fhn    , /**< Error rate is 10-5 */
    cAtBerRate1E6Fhn    , /**< Error rate is 10-6 */
    cAtBerRate1E7Fhn    , /**< Error rate is 10-7 */
    cAtBerRate1E8Fhn    , /**< Error rate is 10-8 */
    cAtBerRate1E9Fhn      /**< Error rate is 10-9 */
    }eAtBerRateFhn;

typedef enum eAtCardTypeFhn
{
    cAtCardType8E1 = 0x10,
    cAtCardType16E1 = 0x11,
    cAtCardTypeline4STM  = 0x12,
    cAtCardTypeline8STM = 0x13,
    cAtCardTypeline1STM4 = 0x14,
    cAtCardTypeline2STM4 = 0x15,
}eAtCardTypeFhn;

#define AT_CPOS_STM1_MAPPING	1 
#define AT_CPOS_STM4_MAPPING	2 
#define AT_POS_STM1_MAPPING		3 
#define AT_POS_STM4_MAPPING		4 
#define AT_CPOS_STM4_155_MAPPING		5 
#define AT_CPOS_STM4_E3_MAPPING		6 

#define AT_POS_E1_MAX_SLOT_FHN	2
#define AT_POS_E1_MAX_PORT_FHN	16
#define AT_POS_E1_MAX_LOGICAL_CHANNEL_PERPORT_FHN	32
#define AT_POS_E1_MAX_MLPPP_BUNDLE_FHN	16
#define AT_POS_E1_MAX_HW_CHANNEL_FHN	128


#define AT_POS_STM_MAX_SLOT_FHN	2
#define AT_POS_STM_MAX_FPGA_FHN	2
#define AT_POS_STM_MAX_PORT_FHN	8
#define AT_POS_STM_MAX_LOGICAL_CHANNEL_PERPORT_FHN	512
#define AT_POS_STM_MAX_MLPPP_BUNDLE_FHN	128
#define AT_POS_STM_MAX_HW_CHANNEL_FHN	512
#define AT_POS_STM_MAX_FLOW_FHN	1024


static const eAtSdhLineAlarmTypeFhn cCmdSdhLineAlarmTypeValFhn[]={cAtSdhLineAlarmLosFhn,
                                                     cAtSdhLineAlarmOofFhn,
                                                     cAtSdhLineAlarmLofFhn,
                                                     cAtSdhLineAlarmTimFhn,
                                                     cAtSdhLineAlarmAisFhn,
                                                     cAtSdhLineAlarmRdiFhn,
                                                     cAtSdhLineAlarmBerSdFhn,
                                                     cAtSdhLineAlarmBerSfFhn,
                                                     cAtSdhLineAlarmKByteChangeFhn,
                                                     cAtSdhLineAlarmKByteFailFhn,
                                                     cAtSdhLineAlarmS1ChangeFhn};

static const eAtPdhDe1AlarmTypeFhn cCmdPdhDe1AlarmTypeValFhn[]={				    
							    cAtPdhDe1AlarmLosFhn,  
							    cAtPdhDe1AlarmLofFhn,  
							    cAtPdhDe1AlarmAisFhn,  
							    cAtPdhDe1AlarmRaiFhn,  
							    cAtPdhDe1AlarmLomfFhn,  
							    cAtPdhDe1AlarmSfBerFhn, 
							    cAtPdhDe1AlarmSdBerFhn,  
							    cAtPdhDe1AlarmSigLofFhn,
							    cAtPdhDe1AlarmSigRaiFhn, 
							    };
/*******************************************************************************

 initFpga - FPGA NS upgrade function

 The function writes FPGA images to FPGA of either STM1 or E1 card.

 parameters:

int  portNum	-	values from 1-4. 
				For STM1 SC1, porNum = 1,2.
				For STM1 SC2, porNum = 3,4
				For E1 SC1, 	portNum = 1.
				For E1 SC2,	 porNum = 3.

int type	-	For STM1 card, type =0.				
			For E1 card, type =1;
			
Return:
OK or Error.
*/
extern int initFpga(int portNum, int type);

/* added by ssq 20140609 */
SEM_ID semAtUdrProtect[2];
#define POS_AT_UDR_SEM_TAKE(slotId, time) 	if (OK!=semTake(semAtUdrProtect[slotId],(time)*sysClkRateGet())){printf("%s,#%d,error\r\n",__FUNCTION__,__LINE__);return -1;}
#define POS_AT_UDR_SEM_GIVE(slotId) 		semGive(semAtUdrProtect[slotId])
/* end */


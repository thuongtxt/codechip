/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APP
 * 
 * File        : AtFhnSlot.h
 * 
 * Created Date: Jun 29, 2013
 *
 * Description : Slot object
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATFHNSLOT_H_
#define _ATFHNSLOT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDevice.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtFhnSlot * AtFhnSlot;

typedef struct tAtFhnSlot
    {
    AtDevice *devices;
    uint8 numDevices;
    }tAtFhnSlot;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtFhnSlot AtFhnSlotNew(uint8 numDevices);
void AtFhnSlotDelete(AtFhnSlot self);

void AtFhnSlotDeviceSet(AtFhnSlot self, uint8 deviceId, AtDevice device);
AtDevice AtFhnSlotDeviceGet(AtFhnSlot slot, uint8 deviceId);
uint8 AtFhnSlotNumDevices(AtFhnSlot slot);

#ifdef __cplusplus
}
#endif
#endif /* _ATFHNSLOT_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APP
 *
 * File        : AtFhnSlot.c
 *
 * Created Date: Jun 29, 2013
 *
 * Description : Slot object
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDriver.h"
#include "AtFhnSlot.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize()
    {
    return sizeof(tAtFhnSlot);
    }

static void DeviceHalCleanup(AtDevice device)
    {
    uint8 core_i;

    for (core_i = 0; core_i < AtDeviceNumIpCoresGet(device); core_i++)
        AtHalDelete(AtDeviceIpCoreHalGet(device, core_i));
    }

AtFhnSlot AtFhnSlotNew(uint8 numDevices)
    {
    uint32 memorySize;
    AtFhnSlot newSlot;

    /* New object */
    memorySize = ObjectSize();
    newSlot = AtOsalMemAlloc(memorySize);
    if (newSlot == NULL)
        return NULL;
    AtOsalMemInit(newSlot, 0, memorySize);

    /* Create memory to hold devices of this slot */
    memorySize = sizeof(AtDevice) * numDevices;
    newSlot->devices = AtOsalMemAlloc(memorySize);
    if (newSlot->devices == NULL)
        {
        AtOsalMemFree(newSlot);
        return NULL;
        }
    AtOsalMemInit(newSlot->devices, 0, memorySize);
    newSlot->numDevices = numDevices;

    return newSlot;
    }

void AtFhnSlotDelete(AtFhnSlot self)
    {
    uint8 device_i;

    if (self == NULL)
        return;

    /* Delete devices */
    for (device_i = 0; device_i < self->numDevices; device_i++)
        {
        DeviceHalCleanup(self->devices[device_i]);
        AtDriverDeviceDelete(AtDriverSharedDriverGet(), self->devices[device_i]);
        }

    /* And memory that hold them */
    AtOsalMemFree(self->devices);
    AtOsalMemFree(self);
    }

void AtFhnSlotDeviceSet(AtFhnSlot self, uint8 deviceId, AtDevice device)
    {
    self->devices[deviceId - 1] = device;
    }

AtDevice AtFhnSlotDeviceGet(AtFhnSlot slot, uint8 deviceId)
{
	if(slot==NULL)
	{
		return NULL;
	}
	return slot->devices[deviceId - 1];
}

uint8 AtFhnSlotNumDevices(AtFhnSlot slot)
{
	if(slot==NULL)
	{
		return NULL;
	}
	return slot->numDevices;
}

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2011 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SONET/SDH
 *
 * File        : fhninterface.c
 *
 * Created Date: May 24, 2013
 *
 * Description : 
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
//#include "AtCli.h"
#include "AtDevice.h"
#include "AtSdhAug.h"
#include "AtSdhTug.h"
#include "AtSdhVc.h"
#include "AtSdhLine.h"
#include "AtEthFlow.h"
#include "AtPppLink.h"
//#include "../encap/CliAtModuleEncap.h"
#include "AtPdhDe1.h"
#include "../../cli/libs/text-ui/include/AtTextUI.h"
#include "AtDriver.h"
#include "../../driver/include/encap/AtHdlcChannel.h"
#include "fhninterface.h"
#include "semLib.h"
#include "AtFhnSlot.h"

//#include "AtDriverInternal.h"
//#include "AtDeviceInternal.h"

//#include "../../util/AtIteratorInternal.h"
//#include "../../implement/default/man/ThaDevice.h"

extern tCmdConf cmdConf[];
extern int cmdConfCount;

/*mapType
#define AT_CPOS_STM1_MAPPING	1 
#define AT_CPOS_STM4_MAPPING	2 
#define AT_POS_STM1_MAPPING		3 
#define AT_POS_STM4_MAPPING		4 
#define AT_CPOS_STM4_155_MAPPING 5
*/
uint32 gui4posSdhAugMapRecord[AT_POS_STM_MAX_SLOT_FHN][AT_POS_STM_MAX_PORT_FHN];
/* E1 card soft pool */
SEM_ID semE1LogicalChannelNode[AT_POS_E1_MAX_SLOT_FHN];
uint32 gu4AtE1BundleMemberRecord[AT_POS_E1_MAX_SLOT_FHN][AT_POS_E1_MAX_MLPPP_BUNDLE_FHN+1]={0};
uint32 gui4AtE1logicalchannelEncap[AT_POS_E1_MAX_SLOT_FHN][AT_POS_E1_MAX_PORT_FHN+1][AT_POS_E1_MAX_LOGICAL_CHANNEL_PERPORT_FHN+1];
tAtHwE1channelRecordNodeFhn gtAtHwE1channelRecordNode[AT_POS_E1_MAX_SLOT_FHN][AT_POS_E1_MAX_HW_CHANNEL_FHN+1];

/* STM card soft pool */
SEM_ID semStmLogicalChannelNode[AT_POS_STM_MAX_SLOT_FHN];
uint32 gu4AtStmBundleMemberRecord[AT_POS_STM_MAX_SLOT_FHN][AT_POS_STM_MAX_FPGA_FHN][AT_POS_STM_MAX_MLPPP_BUNDLE_FHN+1]={0};
tAtHwStmconfigRecordFhn gui4AtStmlogicalchannelEncap[AT_POS_STM_MAX_SLOT_FHN][AT_POS_STM_MAX_PORT_FHN+1][AT_POS_STM_MAX_LOGICAL_CHANNEL_PERPORT_FHN+1];
tAtHwStmchannelRecordNodeFhn gtAtHwStmchannelRecordNode[AT_POS_STM_MAX_SLOT_FHN][AT_POS_STM_MAX_FPGA_FHN][AT_POS_STM_MAX_HW_CHANNEL_FHN+1];

int gui4AttestFlag=0;

uint32 gui4GrobleDebug=0;
uint32 gui4AtSDHPathDebug=0;
uint32 gui4AtEthDebug=0;
uint32 gui4EncapDebug=0;
uint32 gui4PPPdebug=0;
uint32 gui4AtSDHMapDebug=0;
uint32 gui4AtSDHLineDebug=0;
uint32 gui4AtFlowDebug=0;
uint32 gui4PdhDebug=0;
uint32 gui4MLPPPDebug=0;

uint32 outputClockAdress[2]=
{
	0xf000007c,
	0xf000007d
};

tAtchannelMapToSdh gsChannelMapToSdh[64] = 
{
	{0,0,0},
		
	{1,1,1},   /* No.1 */
	{1,1,2},   /* No.2 */ 
	{1,1,3},   /* No.3 */

	{1,2,1},   /* No.4 */
	{1,2,2},   /* No.5 */
	{1,2,3},   /* No.6 */

	{1,3,1},   /* No.7 */
	{1,3,2},   /* No.8 */
	{1,3,3},   /* No.9 */

	{1,4,1},   /* No.10 */
	{1,4,2},   /* No.11 */
	{1,4,3},   /* No.12 */

	{1,5,1},   /* No.13 */
	{1,5,2},   /* No.14 */
	{1,5,3},   /* No.15 */

	{1,6,1},   /* No.16 */
	{1,6,2},   /* No.17 */
	{1,6,3},   /* No.18 */

	{1,7,1},   /* No.19 */
	{1,7,2},   /* No.20 */
	{1,7,3},   /* No.21 */


	{2,1,1},   /* No.22 */
	{2,1,2},   /* No.23 */
	{2,1,3},   /* No.24 */

	{2,2,1},   /* No.25 */
	{2,2,2},   /* No.26 */
	{2,2,3},   /* No.27 */

	{2,3,1},   /* No.28 */
	{2,3,2},   /* No.29 */
	{2,3,3},   /* No.30 */

	{2,4,1},   /* No.31 */
	{2,4,2},   /* No.32 */
	{2,4,3},   /* No.33 */

	{2,5,1},   /* No.34 */
	{2,5,2},   /* No.35 */
	{2,5,3},   /* No.36 */

	{2,6,1},   /* No.37 */
	{2,6,2},   /* No.38 */
	{2,6,3},   /* No.39 */

	{2,7,1},   /* No.40 */
	{2,7,2},   /* No.41 */
	{2,7,3},   /* No.42 */

	{3,1,1},   /* No.43 */
	{3,1,2},   /* No.44 */
	{3,1,3},   /* No.45 */

	{3,2,1},   /* No.46 */
	{3,2,2},   /* No.47 */
	{3,2,3},   /* No.48 */

	{3,3,1},   /* No.49 */
	{3,3,2},   /* No.50 */
	{3,3,3},   /* No.51 */

	{3,4,1},   /* No.52 */
	{3,4,2},   /* No.53 */
	{3,4,3},   /* No.54 */

	{3,5,1},   /* No.55 */
	{3,5,2},   /* No.56 */
	{3,5,3},   /* No.57 */

	{3,6,1},   /* No.58 */
	{3,6,2},   /* No.59 */
	{3,6,3},   /* No.60 */

	{3,7,1},   /* No.61 */
	{3,7,2},   /* No.62 */
	{3,7,3} /* No.63 */

};

tAtchannelMapToSdhSTM4ToE3 gsChannelMapToSdhSTM4ToE3[13] = 
{
	{0,0},
		
	{1,1},   /* No.1 */
	{1,2},   /* No.2 */ 
	{1,3},   /* No.3 */

	{2,1},   /* No.4 */
	{2,2},   /* No.5 */
	{2,3},   /* No.6 */

	{3,1},   /* No.7 */
	{3,2},   /* No.8 */
	{3,3},   /* No.9 */

	{4,1},   /* No.10 */
	{4,2},   /* No.11 */
	{4,3},   /* No.12 */
};

#define cUdrPdhDe1NumAlarmFhn                              9
#define cUdrSdhLineNumAlarmFhn                             11
#define cUdrAtEthPortCountersFhn					69
#define cUdrAtChannelCountersFhnFhn				8
#define cUdrAtEthFlowCountersFhnFhn				20

uint32 gui4PosStartBerMonitor[2];

uint32 gui4AtslotAddress[3]=
{
	0,
	0xf000000A,
	0xf0000009
};

uint32 gui4AtslotCardType[3]=
{
	0,
	0,
	0
};


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

static AtDevice m_device = null;

uint32 atFhnAbnormalTestFlag=0;

int Udr_ce1_port_enable(uint8 cardId, uint32 port, void *arg);
int Udr_ce1_port_framer_mode_set (uint8 cardId, uint32 port, void *arg);
int Udr_ce1_mlppp_bundle_create(uint8 cardId,uint32 bundleId, void *arg);
int Udr_cpos_mlppp_bundle_create(uint8 cardId, uint32 fpgaID, uint32 bundleId, void *arg);
int Udr_cpos_port_stm1_sdh_map(uint8 cardId, uint32 cposPort, uint32 cposPortChannel);
int Udr_cpos_port_linerate_set(uint8 cardId, uint32 cposPort, void *arg);
int Udr_ce1_port_admin_status(uint8 cardId, uint32 port, void *arg);
int Udr_cpos_port_admin_status(uint8 cardId, uint32 port, void *arg);
int Udr_cpos_port_ber_sd_set(uint8 cardId, uint32 cposPort, void *arg);
int Udr_cpos_port_ber_sf_set(uint8 cardId, uint32 cposPort, void *arg);
int Udr_cpos_port_tti_tx_set(uint8 cardId, uint32 cposPort, void *arg);
int Udr_cpos_port_tti_rx_set(uint8 cardId, uint32 cposPort, void *arg);
int udr_if_stm_pos_j0_mode(uint8 cardId, uint32 cposPort, tAtSdhTtiFhn *joModen);
int udr_if_stm_pos_j1_mode(uint8 cardId, uint32 cposPort, tAtSdhTtiFhn *joModen);
int udr_if_stm_pos_c2_flag(uint8 cardId, uint32 cposPort, uint8 *c2str);
int Udr_cpos_port_path_psl_tx_set(uint8 cardId, uint32 cposPort, uint32 cposPortChannel, void *arg);
int Udr_cpos_port_path_psl_rx_set(uint8 cardId, uint32 cposPort, uint32 cposPortChannel, void *arg);
int Udr_cpos_port_path_vc4_tti_tx_set(uint8 cardId, uint32 cposPort, void *arg);
int Udr_cpos_port_path_vc4_tti_rx_set(uint8 cardId, uint32 cposPort, void *arg);
int Udr_cpos_port_path_vc4_psl_tx_set(uint8 cardId, uint32 cposPort, void *arg);
int Udr_cpos_port_path_vc4_psl_rx_set(uint8 cardId, uint32 cposPort, void *arg);
int Udr_cpos_port_path_vc4c_tti_tx_set(uint8 cardId, uint32 cposPort, void *arg);
int Udr_cpos_port_path_vc4c_tti_rx_set(uint8 cardId, uint32 cposPort, void *arg);
int Udr_cpos_port_path_vc4c_psl_tx_set(uint8 cardId, uint32 cposPort, void *arg);
int Udr_cpos_port_path_vc4c_psl_rx_set(uint8 cardId, uint32 cposPort, void *arg);
int Udr_cpos_port_enable(uint8 cardId, uint32 cposPort, void *arg);
int Udr_pos_stm4_port_sdh_map(uint8 cardId, uint32 posPort);
int Udr_pos_stm4_vc4c_port_sdh_map(uint8 cardId, uint32 posPort);

extern AtComponenentLinkTest();


/*
0: disable monitor
1: start monitor
*/
int At_fhn_start_pos_ber_set(uint32 slotId, uint32 value)
{
	if(slotId<1||slotId>2)
	{
		printf("#%d,%s,error slotId=%d\r\n",__LINE__,__FUNCTION__,slotId);
		return -1;
	}

	gui4PosStartBerMonitor[slotId-1] = value;
}

int At_fhn_start_pos_ber_get(uint32 slotId, uint32 *value)
{
	if(slotId<1||slotId>2)
	{
		printf("#%d,%s,error slotId=%d\r\n",__LINE__,__FUNCTION__,slotId);
		return -1;
	}

	*value = gui4PosStartBerMonitor[slotId-1];
	return gui4PosStartBerMonitor[slotId-1];
}



void AtFhnBerMonitor(AtFhnSlot slot)
{
	AtDevice device;
	uint8 device_i;
	const uint32 cTaskPeriodInMs = 100;
	int startTicks, stopTicks, elapseTicks, remainTicks;
	AtModuleBer berModule;

	/* Monitor all devices and save time that it takes */
	startTicks = tickGet();
	
	for (device_i=0; device_i < (slot->numDevices); device_i++){
		device = slot->devices[device_i];
		berModule = (AtModuleBer)AtDeviceModuleGet(device, cAtModuleBer);
		AtModuleBerPeriodicProcess(berModule, cTaskPeriodInMs);
	}
	stopTicks = tickGet();

	/* Calculate remaining time to delay */
	elapseTicks = stopTicks - startTicks;
	remainTicks = (4 > elapseTicks) ? (4 - elapseTicks) : 0;
	taskDelay(remainTicks);
}


int AtFhnPosBerMonitorSlotOneTask( void )
{
	uint32 arg=0;

	while(1)
    	{
		taskDelay(2);
		if(At_fhn_start_pos_ber_get(1,&arg)){
			POS_AT_UDR_SEM_TAKE(0,10);
			AtFhnBerMonitor((AtFhnSlot)AtFhnDriverSlotGet(1));
			POS_AT_UDR_SEM_GIVE(0);
		}else{
			taskDelay(60);
		}
		
	 }

}

int AtFhnPosBerMonitorSlotTwoTask( void )
{
	uint32 arg=0;

	while(1)
    	{
		taskDelay(2);
		if(At_fhn_start_pos_ber_get(2,&arg)){
			POS_AT_UDR_SEM_TAKE(0,10);
			AtFhnBerMonitor((AtFhnSlot)AtFhnDriverSlotGet(2));
			POS_AT_UDR_SEM_GIVE(0);
		}else{
			taskDelay(60);
		}
	 }

}


int At_fhn_pos_ber_sd_sf_monitor(){
	if (taskSpawn("PosBerSlotOneMonitor",  100,	0, 10000,
		(FUNCPTR)AtFhnPosBerMonitorSlotOneTask,0,0,0,0,0,0,0,0,0,0) == ERROR)
	{
		printf("Create receive from arm task fail!\r\n");
	}

	if (taskSpawn("PosBerSlotTwoMonitor",  100,	0, 10000,
		(FUNCPTR)AtFhnPosBerMonitorSlotTwoTask,0,0,0,0,0,0,0,0,0,0) == ERROR)
	{
		printf("Create receive from arm task fail!\r\n");
	}
	
}


int AtFhnPortMonitorTestTask( void )
{
	uint32 arg=0,i=0;
	uint32 oldPortStatus[8]={0};
	uint32 newPortStatus[8]={0};	

	while(1)
    	{
		for(i=1;i<=8;i++)
		{
			_udr_at_stm_port_los_status_get(1,i,&newPortStatus[i-1]);
			if(oldPortStatus[i-1] != newPortStatus[i-1]){
				if(newPortStatus[i-1] == 0){
					printf("port[%d] DOWN \r\n",i);
				}else{
					printf("port[%d] UP \r\n",i);
				}
			}
			oldPortStatus[i-1] =newPortStatus[i-1];
		}
		taskDelay(60);
	 }

}


int At_fhn_port_monitor_test(){
	if (taskSpawn("PosPortMonitorTest",  100,	0, 10000,
		(FUNCPTR)AtFhnPortMonitorTestTask,0,0,0,0,0,0,0,0,0,0) == ERROR)
	{
		printf("Create receive from arm task fail!\r\n");
	}
}



int At_fhn_pos_e1_soft_init(int slotId)
{
	int *arg=NULL;
	int ret=0,mlBundleId=0;
	int channelID=0,e1port,logicalChannel;
	
	if(slotId<1||slotId>2)
	{
		printf("#%d,%s,error slotId=%d\r\n",__LINE__,__FUNCTION__,slotId);
		return -1;
	}

	for(mlBundleId=0;mlBundleId<=AT_POS_E1_MAX_MLPPP_BUNDLE_FHN;mlBundleId++){
		gu4AtE1BundleMemberRecord[slotId-1][mlBundleId]=0;
	}


	for(e1port=1;e1port<=AT_POS_E1_MAX_PORT_FHN;e1port++){
		for(logicalChannel=0;logicalChannel<=AT_POS_E1_MAX_LOGICAL_CHANNEL_PERPORT_FHN;logicalChannel++){
			gui4AtE1logicalchannelEncap[slotId-1][e1port][logicalChannel]=1;/* default PPP */
		}
	}

	for(channelID=0;channelID<=AT_POS_E1_MAX_HW_CHANNEL_FHN;channelID++){
		gtAtHwE1channelRecordNode[slotId-1][channelID].usedFlag =0;
		gtAtHwE1channelRecordNode[slotId-1][channelID].channelId=channelID;
		gtAtHwE1channelRecordNode[slotId-1][channelID].ce1Port=0;	/* default  */
		gtAtHwE1channelRecordNode[slotId-1][channelID].ce1portTimeslot=0;	/* default  */
		gtAtHwE1channelRecordNode[slotId-1][channelID].logicalChannelId=0;	/* default  */
		gtAtHwE1channelRecordNode[slotId-1][channelID].encapType=1;	/* default PPP */
	}
	
}

int At_fhn_pos_stm_soft_init(int slotId)
{
	int *arg=NULL;
	int ret=0,mlBundleId=0;
	int channelID=0,stmport=0,logicalChannel=0,fpgaId=0;
	
	if(slotId<1||slotId>2)
	{
		printf("#%d,%s,error slotId=%d\r\n",__LINE__,__FUNCTION__,slotId);
		return -1;
	}

	for(fpgaId=0;fpgaId<2;fpgaId++){
		for(mlBundleId=0;mlBundleId<=AT_POS_STM_MAX_MLPPP_BUNDLE_FHN;mlBundleId++){
			gu4AtStmBundleMemberRecord[slotId-1][fpgaId][mlBundleId]=0;
		}
	}

	for(stmport=0;stmport<=AT_POS_STM_MAX_PORT_FHN;stmport++){
		for(logicalChannel=0;logicalChannel<=AT_POS_STM_MAX_LOGICAL_CHANNEL_PERPORT_FHN;logicalChannel++){
			gui4AtStmlogicalchannelEncap[slotId-1][stmport][logicalChannel].timeModen=1;/*cAtTimingModeSys*/
			gui4AtStmlogicalchannelEncap[slotId-1][stmport][logicalChannel].refPort=1;/* default  */
			gui4AtStmlogicalchannelEncap[slotId-1][stmport][logicalChannel].framerType=8;/*default cAtPdhE1UnFrm*/
			gui4AtStmlogicalchannelEncap[slotId-1][stmport][logicalChannel].encapType=1;/* default PPP */
		}
	}

	for(fpgaId=0;fpgaId<2;fpgaId++){
		for(channelID=0;channelID<AT_POS_STM_MAX_HW_CHANNEL_FHN;channelID++){
			gtAtHwStmchannelRecordNode[slotId-1][fpgaId][channelID].usedFlag =0;
			gtAtHwStmchannelRecordNode[slotId-1][fpgaId][channelID].channelId=channelID;
			gtAtHwStmchannelRecordNode[slotId-1][fpgaId][channelID].cposPort=0;			
			gtAtHwStmchannelRecordNode[slotId-1][fpgaId][channelID].cposPortVc12Ch=0;		/* default  */
			gtAtHwStmchannelRecordNode[slotId-1][fpgaId][channelID].cposPortVc12ChTimeslot=0;	/* default  */
			gtAtHwStmchannelRecordNode[slotId-1][fpgaId][channelID].logicalChannelId=0;		/* default  */
			gtAtHwStmchannelRecordNode[slotId-1][fpgaId][channelID].encapType=1;			/* default PPP */
		}
	}
	
}


int At_register_wr(uint8 slot, uint32 fpgaID, uint32 address, uint32 Value)
{
	AtHal hal;
	uint32 deviceId=fpgaID;

	hal = AtIpCoreHalGet(AtDeviceIpCoreGet((AtDevice)AtFhnDeviceGet(slot,deviceId), 0));
	if (hal == NULL)
	{
		printf("ERROR: No HAL\r\n");
		return cAtFalse;
	}

	AtHalWrite(hal, address, Value);

	return 0;
}

/*
slot [1,2]
fpgaID[1,2]
*/
uint32 At_register_rd(uint8 slot, uint32 fpgaID, uint32 address, uint32 *value)
{
	AtHal hal;
	uint32 deviceId=fpgaID;
	
	hal = AtIpCoreHalGet(AtDeviceIpCoreGet((AtDevice)AtFhnDeviceGet(slot,deviceId), 0));
	if (hal == NULL)
	{
		printf("ERROR: No HAL\r\n");
		return cAtFalse;
	}

	*value = AtHalRead(hal, address);

	return (*value);
}

uint8 MsgLengthFromTtiMode(eAtSdhTtiMode ttiMode)
{
    if (ttiMode == cAtSdhTtiMode1Byte) return 1;
    if (ttiMode == cAtSdhTtiMode16Byte) return 16;
    return 64;
}

int At_cpos_port_tti_tx_set(uint8 slot,
                                             uint8 lineId,
                                             uint8 augId,
                                             uint8 tu3Id,
                                             uint8 tug2Id,
                                             uint8 tuId,
                                             eAtSdhTtiMode ttiMode,
                                             uint8 *message,
                                             uint8 paddingMode)
    {
        	int ret=0;
    uint8       ttiMsgBuf[64], i;
    tAtSdhTti   tti;

	uint8       ttiMsgLen = MsgLengthFromTtiMode(ttiMode);
	AtSdhLine sdhModuleLine=(AtSdhLine)AtFhnLineGet(slot, lineId);
	char        padChar = (paddingMode == 0) ? '\0' : ' ';

    /* Build TTI message */
    uint8 inputMsgLen = strlen(message);
    if (inputMsgLen > 64)
        return 0;

    /* Copy TTI message to buffer */
    AtOsalMemCpy((void *)ttiMsgBuf, (void *)message, inputMsgLen);

    /* Padding */
    for(i = inputMsgLen; i < 64; i++)
        ttiMsgBuf[i] = padChar;

    AtSdhTtiMake(ttiMode, ttiMsgBuf, ttiMsgLen, &tti);
    ret = AtSdhChannelTxTtiSet((AtSdhChannel)sdhModuleLine, &tti);
		return ret;
    }


int At_cpos_port_tti_rx_set(uint8 slot,
                                             uint8 lineId,
                                             uint8 augId,
                                             uint8 tu3Id,
                                             uint8 tug2Id,
                                             uint8 tuId,
                                             eAtSdhTtiMode ttiMode,
                                             uint8 *message,
                                             uint8 paddingMode)
    {
    	int ret=0;
    uint8       ttiMsgBuf[64], i;
    tAtSdhTti   tti;

	uint8       ttiMsgLen = MsgLengthFromTtiMode(ttiMode);
	AtSdhLine sdhModuleLine=(AtSdhLine)AtFhnLineGet(slot, lineId);
	char        padChar = (paddingMode == 0) ? '\0' : ' ';

    /* Build TTI message */
    uint8 inputMsgLen = strlen(message);
    if (inputMsgLen > 64)
        return 0;

    /* Copy TTI message to buffer */
    AtOsalMemCpy((void *)ttiMsgBuf, (void *)message, inputMsgLen);

    /* Padding */
    for(i = inputMsgLen; i < 64; i++)
        ttiMsgBuf[i] = padChar;

    AtSdhTtiMake(ttiMode, ttiMsgBuf, ttiMsgLen, &tti);
    ret = AtSdhChannelExpectedTtiSet((AtSdhChannel)sdhModuleLine, &tti);
	return ret;
    }


/**
 * Set Tx TTI for VC1x
 *
 * @param deviceId
 * @param lineId
 * @param augId
 * @param tu3Id
 * @param tug2Id
 * @param tuId
 * @param ttiMode
 * @param message
 * @param paddingMode: 0 - null padding; other - space padding
 * @return
 */
 /*
 
a='s'
 Udr_cpos_port_path_tti_tx_set_samplecode 1,1,1,1,1,1, 0,&a,0
 Udr_cpos_port_path_tti_rx_set_samplecode 1,1,1,1,1,1, 0,&a,0

 show sdh path tti receive vc1x.1.1.1.1.1

 */
int At_cpos_port_path_tti_tx_set(uint8 slot,
                                             uint8 lineId,
                                             uint8 augId,
                                             uint8 tu3Id,
                                             uint8 tug2Id,
                                             uint8 tuId,
                                             eAtSdhTtiMode ttiMode,
                                             uint8 *message,
                                             uint8 paddingMode)
    {
        	int ret=0;
    uint8       ttiMsgBuf[64], i;
    tAtSdhTti   tti;

    uint8       ttiMsgLen = MsgLengthFromTtiMode(ttiMode);
    AtSdhVc     vc1x =(AtSdhVc) AtFhnVc1xGet(slot, lineId, augId, tu3Id, tug2Id, tuId);
    char        padChar = (paddingMode == 0) ? '\0' : ' ';

    /* Build TTI message */
    uint8 inputMsgLen = strlen(message);
    if (inputMsgLen > 64)
        return 0;

    /* Copy TTI message to buffer */
    AtOsalMemCpy((void *)ttiMsgBuf, (void *)message, inputMsgLen);

    /* Padding */
    for(i = inputMsgLen; i < 64; i++)
        ttiMsgBuf[i] = padChar;

    AtSdhTtiMake(ttiMode, ttiMsgBuf, ttiMsgLen, &tti);
    ret = AtSdhChannelTxTtiSet((AtSdhChannel)vc1x, &tti);
		return ret;
    }


int At_cpos_port_path_vc3_tti_tx_set(uint8 slot,
                                             uint8 lineId,
                                             uint8 augId,
                                             uint8 vc3Id,
                                             eAtSdhTtiMode ttiMode,
                                             uint8 *message,
                                             uint8 paddingMode)
    {
        	int ret=0;
    uint8       ttiMsgBuf[64], i;
    tAtSdhTti   tti;

    uint8       ttiMsgLen = MsgLengthFromTtiMode(ttiMode);
    AtSdhVc     vc3 =(AtSdhVc) AtFhnVc3Get(slot, lineId, augId,vc3Id);
    char        padChar = (paddingMode == 0) ? '\0' : ' ';

    /* Build TTI message */
    uint8 inputMsgLen = strlen(message);
    if (inputMsgLen > 64)
        return 0;

    /* Copy TTI message to buffer */
    AtOsalMemCpy((void *)ttiMsgBuf, (void *)message, inputMsgLen);

    /* Padding */
    for(i = inputMsgLen; i < 64; i++)
        ttiMsgBuf[i] = padChar;

    AtSdhTtiMake(ttiMode, ttiMsgBuf, ttiMsgLen, &tti);
    ret = AtSdhChannelTxTtiSet((AtSdhChannel)vc3, &tti);
		return ret;
    }

int At_cpos_port_path_vc4_tti_tx_set(uint8 slot,
                                             uint8 lineId,
                                             uint8 augId,
                                             eAtSdhTtiMode ttiMode,
                                             uint8 *message,
                                             uint8 paddingMode)
    {
        	int ret=0;
    uint8       ttiMsgBuf[64], i;
    tAtSdhTti   tti;

    uint8       ttiMsgLen = MsgLengthFromTtiMode(ttiMode);
    AtSdhVc     vc4 =(AtSdhVc) AtFhnVc4Get(slot, lineId, augId);
    char        padChar = (paddingMode == 0) ? '\0' : ' ';

    /* Build TTI message */
    uint8 inputMsgLen = strlen(message);
    if (inputMsgLen > 64)
        return 0;

    /* Copy TTI message to buffer */
    AtOsalMemCpy((void *)ttiMsgBuf, (void *)message, inputMsgLen);

    /* Padding */
    for(i = inputMsgLen; i < 64; i++)
        ttiMsgBuf[i] = padChar;

    AtSdhTtiMake(ttiMode, ttiMsgBuf, ttiMsgLen, &tti);
    ret = AtSdhChannelTxTtiSet((AtSdhChannel)vc4, &tti);
		return ret;
    }

int At_cpos_port_path_vc4c_tti_tx_set(uint8 slot,
                                             uint8 lineId,
                                             uint8 augId,
                                             eAtSdhTtiMode ttiMode,
                                             uint8 *message,
                                             uint8 paddingMode)
    {
        	int ret=0;
    uint8       ttiMsgBuf[64], i;
    tAtSdhTti   tti;

    uint8       ttiMsgLen = MsgLengthFromTtiMode(ttiMode);
    AtSdhVc     vc4 =(AtSdhVc) AtFhnVc4cGet(slot, lineId, augId);
    char        padChar = (paddingMode == 0) ? '\0' : ' ';

    /* Build TTI message */
    uint8 inputMsgLen = strlen(message);
    if (inputMsgLen > 64)
        return 0;

    /* Copy TTI message to buffer */
    AtOsalMemCpy((void *)ttiMsgBuf, (void *)message, inputMsgLen);

    /* Padding */
    for(i = inputMsgLen; i < 64; i++)
        ttiMsgBuf[i] = padChar;

    AtSdhTtiMake(ttiMode, ttiMsgBuf, ttiMsgLen, &tti);
    ret = AtSdhChannelTxTtiSet((AtSdhChannel)vc4, &tti);
		return ret;
    }

int At_cpos_port_path_tti_rx_set(uint8 slot,
                                             uint8 lineId,
                                             uint8 augId,
                                             uint8 tu3Id,
                                             uint8 tug2Id,
                                             uint8 tuId,
                                             eAtSdhTtiMode ttiMode,
                                             uint8 *message,
                                             uint8 paddingMode)
    {
        	int ret=0;
    uint8       ttiMsgBuf[64], i;
    tAtSdhTti   tti;

    uint8       ttiMsgLen = MsgLengthFromTtiMode(ttiMode);
    AtSdhVc     vc1x = (AtSdhVc)AtFhnVc1xGet(slot, lineId, augId, tu3Id, tug2Id, tuId);
    char        padChar = (paddingMode == 0) ? '\0' : ' ';

    /* Build TTI message */
    uint8 inputMsgLen = strlen(message);
    if (inputMsgLen > 64)
        return 0;

    /* Copy TTI message to buffer */
    AtOsalMemCpy((void *)ttiMsgBuf, (void *)message, inputMsgLen);

    /* Padding */
    for(i = inputMsgLen; i < 64; i++)
        ttiMsgBuf[i] = padChar;

    AtSdhTtiMake(ttiMode, ttiMsgBuf, ttiMsgLen, &tti);
    ret = AtSdhChannelExpectedTtiSet((AtSdhChannel)vc1x, &tti);
		return ret;
    }

int At_cpos_port_path_vc3_tti_rx_set(uint8 slot,
                                             uint8 lineId,
                                             uint8 augId,    
                                             uint8 vc3Id,
                                             eAtSdhTtiMode ttiMode,
                                             uint8 *message,
                                             uint8 paddingMode)
    {
        	int ret=0;
    uint8       ttiMsgBuf[64], i;
    tAtSdhTti   tti;

    uint8       ttiMsgLen = MsgLengthFromTtiMode(ttiMode);
    AtSdhVc     vc3 =(AtSdhVc) AtFhnVc3Get(slot, lineId, augId, vc3Id);
    char        padChar = (paddingMode == 0) ? '\0' : ' ';

    /* Build TTI message */
    uint8 inputMsgLen = strlen(message);
    if (inputMsgLen > 64)
        return 0;

    /* Copy TTI message to buffer */
    AtOsalMemCpy((void *)ttiMsgBuf, (void *)message, inputMsgLen);

    /* Padding */
    for(i = inputMsgLen; i < 64; i++)
        ttiMsgBuf[i] = padChar;

    AtSdhTtiMake(ttiMode, ttiMsgBuf, ttiMsgLen, &tti);
    ret = AtSdhChannelExpectedTtiSet((AtSdhChannel)vc3, &tti);
		return ret;
    }


int At_cpos_port_path_vc4_tti_rx_set(uint8 slot,
                                             uint8 lineId,
                                             uint8 augId,    
                                             eAtSdhTtiMode ttiMode,
                                             uint8 *message,
                                             uint8 paddingMode)
    {
        	int ret=0;
    uint8       ttiMsgBuf[64], i;
    tAtSdhTti   tti;

    uint8       ttiMsgLen = MsgLengthFromTtiMode(ttiMode);
    AtSdhVc     vc4 =(AtSdhVc) AtFhnVc4Get(slot, lineId, augId);
    char        padChar = (paddingMode == 0) ? '\0' : ' ';

    /* Build TTI message */
    uint8 inputMsgLen = strlen(message);
    if (inputMsgLen > 64)
        return 0;

    /* Copy TTI message to buffer */
    AtOsalMemCpy((void *)ttiMsgBuf, (void *)message, inputMsgLen);

    /* Padding */
    for(i = inputMsgLen; i < 64; i++)
        ttiMsgBuf[i] = padChar;

    AtSdhTtiMake(ttiMode, ttiMsgBuf, ttiMsgLen, &tti);
    ret = AtSdhChannelExpectedTtiSet((AtSdhChannel)vc4, &tti);
		return ret;
    }

int At_cpos_port_path_vc4c_tti_rx_set(uint8 slot,
                                             uint8 lineId,
                                             uint8 augId,    
                                             eAtSdhTtiMode ttiMode,
                                             uint8 *message,
                                             uint8 paddingMode)
    {
        	int ret=0;
    uint8       ttiMsgBuf[64], i;
    tAtSdhTti   tti;

    uint8       ttiMsgLen = MsgLengthFromTtiMode(ttiMode);
    AtSdhVc     vc4 =(AtSdhVc) AtFhnVc4cGet(slot, lineId, augId);
    char        padChar = (paddingMode == 0) ? '\0' : ' ';

    /* Build TTI message */
    uint8 inputMsgLen = strlen(message);
    if (inputMsgLen > 64)
        return 0;

    /* Copy TTI message to buffer */
    AtOsalMemCpy((void *)ttiMsgBuf, (void *)message, inputMsgLen);

    /* Padding */
    for(i = inputMsgLen; i < 64; i++)
        ttiMsgBuf[i] = padChar;

    AtSdhTtiMake(ttiMode, ttiMsgBuf, ttiMsgLen, &tti);
    ret = AtSdhChannelExpectedTtiSet((AtSdhChannel)vc4, &tti);
		return ret;
    }

int At_cpos_port_path_psl_tx_set(uint8 slotId,
                                             uint8 lineId,
                                             uint8 augId,
                                             uint8 tu3Id,
                                             uint8 tug2Id,
                                             uint8 tuId,
                                             uint8 pslMessage)
{
    uint8       pslBuf=pslMessage ;
    	int ret=0;
    AtSdhVc     vc1x;

	if(slotId<1||slotId>2)
	{
		printf("error slot id =%d\r\n",slotId);
	}

  vc1x =(AtSdhVc) AtFhnVc1xGet(slotId, lineId, augId, tu3Id, tug2Id, tuId);
	
    ret = AtSdhPathTxPslSet((AtSdhPath)vc1x, pslBuf);
		return ret;
}

int At_cpos_port_path_vc3_psl_tx_set(uint8 slotId,
                                             uint8 lineId,
                                             uint8 augId,
                                             uint8 vc3Id,
                                             uint8 pslMessage)
{
    uint8       pslBuf=pslMessage ;
    	int ret=0;
    AtSdhVc     vc3;

	if(slotId<1||slotId>2)
	{
		printf("error slot id =%d\r\n",slotId);
	}

    vc3 =(AtSdhVc) AtFhnVc3Get(slotId, lineId, augId, vc3Id);
	
    ret = AtSdhPathTxPslSet((AtSdhPath)vc3, pslBuf);
		return ret;
}

int At_cpos_port_path_vc4_psl_tx_set(uint8 slotId,
                                             uint8 lineId,
                                             uint8 augId,
                                             uint8 pslMessage)
{
    uint8       pslBuf=pslMessage ;
    	int ret=0;
    AtSdhVc     vc4;

	if(slotId<1||slotId>2)
	{
		printf("error slot id =%d\r\n",slotId);
	}

    vc4 =(AtSdhVc) AtFhnVc4Get(slotId, lineId, augId);
	
    ret = AtSdhPathTxPslSet((AtSdhPath)vc4, pslBuf);
		return ret;
}

int At_cpos_port_path_vc4c_psl_tx_set(uint8 slotId,
                                             uint8 lineId,
                                             uint8 augId,
                                             uint8 pslMessage)
{
    uint8       pslBuf=pslMessage ;
    	int ret=0;
    AtSdhVc     vc4;

	if(slotId<1||slotId>2)
	{
		printf("error slot id =%d\r\n",slotId);
	}

    vc4 =(AtSdhVc) AtFhnVc4cGet(slotId, lineId, augId);
	
    ret = AtSdhPathTxPslSet((AtSdhPath)vc4, pslBuf);
		return ret;
}

int At_cpos_port_path_psl_rx_set(uint8 slotId,
                                             uint8 lineId,
                                             uint8 augId,
                                             uint8 tu3Id,
                                             uint8 tug2Id,
                                             uint8 tuId,
                                             uint8 pslMessage)
{
    uint8       pslBuf=pslMessage ;
    AtSdhVc     vc1x;
    	int ret=0;
		
	if(slotId<1||slotId>2)
	{
		printf("error slot id =%d\r\n",slotId);
	}
	
	vc1x =(AtSdhVc) AtFhnVc1xGet(slotId, lineId, augId, tu3Id, tug2Id, tuId);

    return (AtSdhPathTxPslSet((AtSdhPath)vc1x, pslBuf) == cAtOk) ? 0 : - 1;
}

int At_cpos_port_path_vc3_psl_rx_set(uint8 slotId,
                                             uint8 lineId,
                                             uint8 augId,
                                             uint8 vc3Id,
                                             uint8 pslMessage)
{
    uint8       pslBuf=pslMessage ;
    AtSdhVc     vc3;
    	int ret=0;
		
	if(slotId<1||slotId>2)
	{
		printf("error slot id =%d\r\n",slotId);
	}
	
    vc3 =(AtSdhVc) AtFhnVc3Get(slotId, lineId, augId, vc3Id);

    return (AtSdhPathExpectedPslSet((AtSdhPath)vc3, pslBuf) == cAtOk) ? 0 : - 1;
}

int At_cpos_port_path_vc4_psl_rx_set(uint8 slotId,
                                             uint8 lineId,
                                             uint8 augId,
                                             uint8 pslMessage)
{
    uint8       pslBuf=pslMessage ;
    AtSdhVc     vc4;
    	int ret=0;
		
	if(slotId<1||slotId>2)
	{
		printf("error slot id =%d\r\n",slotId);
	}
	
    vc4 =(AtSdhVc) AtFhnVc4Get(slotId, lineId, augId);

    return (AtSdhPathExpectedPslSet((AtSdhPath)vc4, pslBuf) == cAtOk) ? 0 : - 1;
}

int At_cpos_port_path_vc4c_psl_rx_set(uint8 slotId,
                                             uint8 lineId,
                                             uint8 augId,
                                             uint8 pslMessage)
{
    uint8       pslBuf=pslMessage ;
    AtSdhVc     vc4;
    	int ret=0;
		
	if(slotId<1||slotId>2)
	{
		printf("error slot id =%d\r\n",slotId);
	}
	
    vc4 =(AtSdhVc) AtFhnVc4cGet(slotId, lineId, augId);

    return (AtSdhPathExpectedPslSet((AtSdhPath)vc4, pslBuf) == cAtOk) ? 0 : - 1;
}

/*
sdh map aug4.1.1 4xaug1s
sdh map aug1.1.1 vc4
sdh map vc4.1.1 3xtug3s
sdh map tug3.1.1.1 7xtug2s
sdh map tug2.1.1.1.1 tu12
sdh map vc1x.1.1.1.1.1 de1
*/
int At_cpos_port_stm_sdh_map_aug4_to_aug1(uint8 slotId, uint8 lineId, uint8 augId, uint8 tu3Id, uint8 tug2Id, uint8 tuId)
{
	int ret=0;
	AtSdhAug Aug4;

	Aug4 = (AtSdhAug)AtFhnAug4Get(slotId, lineId, augId);


	if(AtSdhChannelMapTypeGet((AtSdhChannel)Aug4) == cAtSdhAugMapTypeAug4Map4xAug1s)
	{
		//printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return cAtOk; 
	}
	
	ret = AtSdhChannelMapTypeSet((AtSdhChannel)Aug4, cAtSdhAugMapTypeAug4Map4xAug1s);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	
	return 0; 
}

int At_cpos_port_stm_sdh_map_aug4_to_vc4c(uint8 slotId, uint8 lineId, uint8 augId, uint8 tu3Id, uint8 tug2Id, uint8 tuId)
{
	int ret=0;
	AtSdhAug Aug4;

	Aug4 = (AtSdhAug)AtFhnAug4Get(slotId, lineId, augId);

	if(AtSdhChannelMapTypeGet((AtSdhChannel)Aug4) == cAtSdhAugMapTypeAug4MapVc4_4c)
	{
		//printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return cAtOk; 
	}
	
	ret = AtSdhChannelMapTypeSet((AtSdhChannel)Aug4, cAtSdhAugMapTypeAug4MapVc4_4c);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0; 
	
}

int At_cpos_port_stm_sdh_map_aug1_to_vc4(uint8 slotId, uint8 lineId, uint8 augId, uint8 tu3Id, uint8 tug2Id, uint8 tuId)
{
	int ret=0;
	AtSdhAug Aug1;

	Aug1 = (AtSdhAug)AtFhnAug1Get(slotId, lineId, augId);

	if(AtSdhChannelMapTypeGet((AtSdhChannel)Aug1) == cAtSdhAugMapTypeAug1MapVc4)
	{
		//printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return cAtOk; 
	}
	
	ret = AtSdhChannelMapTypeSet((AtSdhChannel)Aug1, cAtSdhAugMapTypeAug1MapVc4);
	/*if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}*/

	return 0; 
	
}

int At_cpos_port_stm_sdh_map_vc4_to_tug3(uint8 slotId, uint8 lineId, uint8 augId, uint8 tu3Id, uint8 tug2Id, uint8 tuId)
{
	int ret=0;
	AtSdhVc vc4;

	vc4 = (AtSdhVc)AtFhnVc4Get(slotId, lineId, augId);

	if(AtSdhChannelMapTypeGet((AtSdhChannel)vc4) == cAtSdhVcMapTypeVc4Map3xTug3s)
	{
		//printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return cAtOk; 
	}
	
	ret = AtSdhChannelMapTypeSet((AtSdhChannel)vc4, cAtSdhVcMapTypeVc4Map3xTug3s);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	
	return 0; 

}

/* added by ssq 20130723 */
int At_cpos_port_stm_sdh_map_vc4_to_c4(uint8 slotId, uint8 lineId, uint8 augId, uint8 tu3Id, uint8 tug2Id, uint8 tuId)
{
	int ret=0;
	AtSdhVc vc4;

	vc4 = (AtSdhVc)AtFhnVc4Get(slotId, lineId, augId);

	if(AtSdhChannelMapTypeGet((AtSdhChannel)vc4) == cAtSdhVcMapTypeVc4MapC4)
	{
		//printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return cAtOk; 
	}
	
	ret = AtSdhChannelMapTypeSet((AtSdhChannel)vc4, cAtSdhVcMapTypeVc4MapC4);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	
	return 0; 

}
/* end */

int At_cpos_port_stm_sdh_map_tug3_to_tug2(uint8 slotId, uint8 lineId, uint8 augId, uint8 tu3Id, uint8 tug2Id, uint8 tuId)
{
	int ret=0;
	AtSdhTug Tug3;

	Tug3 = (AtSdhTug)AtFhnTug3Get(slotId, lineId, augId, tu3Id);

	
	if(AtSdhChannelMapTypeGet((AtSdhChannel)Tug3) == cAtSdhTugMapTypeTug3Map7xTug2s)
	{
		//printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return cAtOk; 
	}

	ret = AtSdhChannelMapTypeSet((AtSdhChannel)Tug3, cAtSdhTugMapTypeTug3Map7xTug2s);

	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	
	return 0; 

}

int At_cpos_port_stm_sdh_map_tug2_to_tu12(uint8 slotId, uint8 lineId, uint8 augId, uint8 tu3Id, uint8 tug2Id, uint8 tuId)
{
	int ret=0;
	AtSdhTug Tug2;

	Tug2 = (AtSdhTug)AtFhnTug2Get(slotId, lineId, augId, tu3Id, tug2Id);

	if(AtSdhChannelMapTypeGet((AtSdhChannel)Tug2) == cAtSdhTugMapTypeTug2Map3xTu12s)
	{
		//printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return cAtOk; 
	}
	
	ret = AtSdhChannelMapTypeSet((AtSdhChannel)Tug2, cAtSdhTugMapTypeTug2Map3xTu12s);

	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	
	return 0; 

}

int At_cpos_port_stm_sdh_map_vc1x_to_de1(uint8 slotId, uint8 lineId, uint8 augId, uint8 tu3Id, uint8 tug2Id, uint8 tuId)
{
	int ret=0;
	AtSdhVc vc1x;

	vc1x = (AtSdhVc)AtFhnVc1xGet(slotId, lineId, augId, tu3Id, tug2Id, tuId);
	
	if(AtSdhChannelMapTypeGet((AtSdhChannel)vc1x) == cAtSdhVcMapTypeVc1xMapDe1)
	{
		//printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return cAtOk; 
	}
	
	ret = AtSdhChannelMapTypeSet((AtSdhChannel)vc1x, cAtSdhVcMapTypeVc1xMapDe1);

	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	
	return 0; 

}

/* added by ssq 20140611 */
int At_cpos_port_stm_sdh_map_tug3_to_vc3(uint8 slotId, uint8 lineId, uint8 augId, uint8 tu3Id)
{
	int ret=0;
	AtSdhTug Tug3;

	Tug3 = (AtSdhTug)AtFhnTug3Get(slotId, lineId, augId, tu3Id);

	if(AtSdhChannelMapTypeGet((AtSdhChannel)Tug3) == cAtSdhTugMapTypeTug3MapVc3)
	{
		//printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return cAtOk; 
	}
	
	ret = AtSdhChannelMapTypeSet((AtSdhChannel)Tug3, cAtSdhTugMapTypeTug3MapVc3);

	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	
	return 0; 

}

int At_cpos_port_stm_sdh_map_vc3_to_de3(uint8 slotId, uint8 lineId, uint8 augId, uint8 vc3Id)
{
	int ret=0;
	AtSdhVc vc1x;

	vc1x = (AtSdhVc)AtFhnVc3Get(slotId, lineId, augId, vc3Id);
	
	if(AtSdhChannelMapTypeGet((AtSdhChannel)vc1x) == cAtSdhVcMapTypeVc3MapDe3)
	{
		//printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return cAtOk; 
	}
	
	ret = AtSdhChannelMapTypeSet((AtSdhChannel)vc1x, cAtSdhVcMapTypeVc3MapDe3);

	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	
	return 0; 

}
/**/

/*
port = 1-8

*/
int	At_cpos_port_clock_src_set(uint8 cardId, uint32 cposPort)
{
	int ret=0;
	uint32 refPort;
	AtModuleSdh encapModule;
	uint8 deviceNum=1,slotId=cardId;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error port=%d\r\n",cposPort);
		return -1;
	}

	if(cposPort<=4){
		deviceNum=1;
	}else if((cposPort>4)&&(cposPort<=8)){
		deviceNum=2;
	}else{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error port=%d\r\n",cposPort);
		return -1;
	}

	encapModule = (AtModuleSdh)AtFhnSdhModule(slotId, deviceNum);
	
       ret = AtModuleSdhOutputClockSourceSet(encapModule, 0, (AtSdhLine)AtFhnLineGet(slotId, cposPort));
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	return 0;
}

int	At_cpos_port_clock_src_delete(uint8 cardId, uint32 fpgaId)
{
	int ret=0;
	uint32 refPort;
	AtModuleSdh encapModule;
	uint8 deviceNum=fpgaId,slotId=cardId;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(fpgaId<1||fpgaId>2)
	{
		printf("#%d,%s,error fpgaId=%d\r\n",__LINE__,__FUNCTION__,fpgaId);
		return -1;
	}

	encapModule = (AtModuleSdh)AtFhnSdhModule(slotId, deviceNum);
	
       ret = AtModuleSdhOutputClockSourceSet(encapModule, 0, NULL);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	return 0;
}


/*
port = 1-32

*/
int	At_ce1_port_clock_src_set(uint8 cardId, uint32 port)
{
	int ret=0;
	uint32 refPort;
	AtModulePdh encapModule;
	uint8 deviceNum=1,slotId=cardId;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((port>32) || (port<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error port=%d\r\n",port);
		return -1;
	}

	encapModule = (AtModulePdh)AtFhnPdhModule(slotId, deviceNum);
	
       ret = AtModulePdhOutputClockSourceSet(encapModule, 0, (AtPdhChannel)AtFhnDe1Get(slotId, port));
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	return 0;
}

int	At_ce1_port_clock_src_delete(uint8 cardId)
{
	int ret=0;
	uint32 refPort;
	AtModulePdh encapModule;
	uint8 deviceNum=1,slotId=cardId;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	encapModule = (AtModulePdh)AtFhnPdhModule(slotId, deviceNum);
	
       ret = AtModulePdhOutputClockSourceSet(encapModule, 0, NULL);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	return 0;
}


/*  


*/

int At_slot_card_type_get(uint8 cardId)
{
	uint32 cardType=0;
	int portId=0,ret=0; 
	int slotId = cardId;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	cardType = *(uint8 *)gui4AtslotAddress[cardId];
	gui4AtslotCardType[cardId]  = *(uint8 *)gui4AtslotAddress[cardId];
	printf("#%d,%s,cardType[%x]\r\n",__LINE__,__FUNCTION__,cardType);

	switch(cardType)
	{
		case cAtCardType8E1:
		case  cAtCardType16E1:
			portId = slotId*2 -1;
			printf("#%d,%s,initFpga(%d,1)\r\n",__LINE__,__FUNCTION__,portId);
			ret = initFpga(portId,1);	
			if(ret != 0){
				*(uint8*)(outputClockAdress[slotId-1]) = 0x3f;
				printf("#%d,%s,initFpga(%d,1) error \r\n",__LINE__,__FUNCTION__,portId);
				return -1;
			}
			*(uint8*)(outputClockAdress[slotId-1]) = 0x3f;
			printf("outputClockAdress[%x]=%x",slotId-1, *(uint8*)(outputClockAdress[slotId-1]));
			break;
		

		case cAtCardTypeline2STM4:
		case cAtCardTypeline8STM:
			portId = slotId*2 -1;
			printf("#%d,%s,initFpga(%d,0)\r\n",__LINE__,__FUNCTION__,portId);
			ret = initFpga(portId,0);	
			if(ret != 0){
				*(uint8*)(outputClockAdress[slotId-1]) = 0x3d;
				printf("#%d,%s,initFpga(%d,0) error \r\n",__LINE__,__FUNCTION__,portId);
				return -1;
			}
			taskDelay(10*60);
			printf("#%d,%s,initFpga(%d,0)\r\n",__LINE__,__FUNCTION__,portId+1);
			ret = initFpga(portId+1,0);	
			if(ret != 0){
				*(uint8*)(outputClockAdress[slotId-1]) = 0x3d;
				printf("#%d,%s,initFpga(%d,0) error\r\n",__LINE__,__FUNCTION__,portId+1);
				return -1;
			}
			*(uint8*)(outputClockAdress[slotId-1]) = 0x3d;
			break;

		case cAtCardTypeline1STM4:
		case cAtCardTypeline4STM:
			portId = slotId*2 -1;
			printf("#%d,%s,initFpga(%d,0)\r\n",__LINE__,__FUNCTION__,portId);
			ret = initFpga(portId,0);	
			if(ret != 0){
				*(uint8*)(outputClockAdress[slotId-1]) = 0x3d;
				printf("#%d,%s,initFpga(%d,0) error \r\n",__LINE__,__FUNCTION__,portId);
				return -1;
			}
			taskDelay(60);
			break;
			
        	default:
			printf("%s,#%d,%s,slot[%d] has not POS card [%x]\r\n",__FILE__,__LINE__,__FUNCTION__,cardId, cardType);
			return -1;

	}

	return cardType;
	
}

int At_slot_card_probe( void )
{
	At_slot_card_type_get(1);
	At_slot_card_type_get(2);
	return 0;
}

uint32 NumFpgasOfProduct(uint32 slotId)
{
	uint32 cardType,cardId=0;;
	if(slotId<1||slotId>2)
	{
		printf("#%d,%s,error slotId=%d\r\n",__LINE__,__FUNCTION__,slotId);
		return -1;
	}

	cardId = slotId;
		
	cardType = *(uint8 *)gui4AtslotAddress[cardId];
	printf("#%d,%s,cardType[%x]\r\n",__LINE__,__FUNCTION__,cardType);

	switch(cardType)
	{
		case cAtCardType8E1:
		case  cAtCardType16E1:
			return 1;		

		case cAtCardTypeline2STM4:
		case cAtCardTypeline8STM:
			return 2;

		case cAtCardTypeline1STM4:
		case cAtCardTypeline4STM:
			return 1;

		default:
			printf("#%d,%s,error cardType[%x]\r\n",__LINE__,__FUNCTION__,cardType);
	}

	return 1;
}



#if	0
/*
CmdSdhLineSerdesTimingModeSet 
sdh line serdes timingmode 1 auto
*/
At_cpos_port_serdes_time_mode(uint32 cposPort)
{
	int ret=0;
	eAtSerdesTimingMode serdesTiming=cAtSerdesTimingModeAuto;
	AtSdhLine line;
	uint32 fpgaID =  (cposPort <= 4) ? 1:2; 
	uint32 linePort =  (cposPort <= 4) ? cposPort : (cposPort-4); 

	At_register_wr(fpgaID, 0x40000,1);
	At_register_wr(fpgaID, 0x40001,1);
	At_register_wr(fpgaID, 0x40004,1);
	At_register_wr(fpgaID, 0x40005,1);
			
	line = (AtSdhLine)AtFhnLineGet(fpgaID, linePort);
	
	ret = AtSdhLineSerdesTimingModeSet(line, serdesTiming);
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}
#endif


eAtRet ce1_port_mapto_channle(uint32 phyport, uint32 timeslot, AtChannel *channels)
{
	AtDevice *pDevice;
	eAtRet  ret=cAtOk;
	uint8 deviceNum=0;
	AtModulePdh pdhModule;
	AtPdhDe1 pdhDe1;

	if(gui4PdhDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("phyport=%x,timeslot = %x\r\n",phyport,timeslot);
	}

	pDevice = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), &deviceNum);
	if(timeslot != 0)
	{
		/* Get nxDS0 of this DS1/E1 channel */
		pdhModule = (AtModulePdh)AtDeviceModuleGet(*pDevice, cAtModulePdh);
		pdhDe1 = AtModulePdhDe1Get(pdhModule, phyport - 1);
		*channels = (AtChannel)AtPdhDe1NxDs0Get(pdhDe1, timeslot);
	}
	else
	{
		pdhModule = (AtModulePdh)AtDeviceModuleGet(*pDevice, cAtModulePdh);
		*channels = (AtChannel)AtModulePdhDe1Get(pdhModule, phyport - 1);
	}


	return ret;

}

int cpos_card_fpga_select(int fpgaId)
{
	int ret=0;
	char argc=0;
	char *argv[10];
	char pStrId1List[64];

	if((fpgaId>2) || (fpgaId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error fpgaId=%d\r\n",fpgaId);
		return -1;
	}

	/* channelId = 1-1281 */
	sprintf(pStrId1List, "%d", fpgaId);

	argv[0] = pStrId1List;

	if(gui4GrobleDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("select fpga [%s]\r\n",argv[0]);
	}
	
  	ret = CmdAtDeviceSelect(argc, argv);
	if(ret != cAtTrue)
	{
		return -1;
	}
	return 0;
}


/*
cposPort = 1-8
0:disable
1:enable

*/
int Udr_cpos_port_ber_enable(uint8 cardId, uint32 cposPort, void *arg)
{
       AtBerController berController;
	uint32 enable = *(uint32 *)arg;
	AtSdhLine sdhModuleLine;
	eAtRet  ret;
	uint8 slotId=cardId;

	if(gui4EncapDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x, cposPort=%d,arg=%x\r\n",cardId,cposPort,enable);
	}
	
	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

        berController = AtSdhChannelBerControllerGet((AtSdhChannel)AtFhnLineGet(slotId,cposPort));
        ret = AtBerControllerEnable(berController, enable);
       if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
       }

	return 0;
}

#if	0
int Udr_cpos_card_init( void )
{
	uint32 mlBundleId=0,fpgaId=0;
	uint8 slot;
	int cNumSlots=2,ret=0;
	int productCode=0;
	char * creationDate   = __DATE__ ", " __TIME__;
	eAtEthPortInterface EncapTypeBuf = cAtEthPortInterface1000BaseX;

	//At_slot_card_probe();
	
    	printf ("atsdk.o Created: %s\n", creationDate);

	if ((AtDriver)AtFhnDriverCreate(cNumSlots) == NULL)
		   return -1;

	*(uint8*)(outputClockAdress[0]) = 0x3f;
	*(uint8*)(outputClockAdress[1]) = 0x3f;

	for(slot=1;slot<3;slot++)
	{
		semE1LogicalChannelNode[slot-1] = semBCreate(SEM_Q_FIFO, SEM_FULL);
		semStmLogicalChannelNode[slot-1] = semBCreate(SEM_Q_FIFO, SEM_FULL);
		
		if(At_slot_card_type_get(slot) == -1){
			continue;
		}
		
		taskDelay(300);
	    	productCode = ProductCodeOfSlot(slot);
		printf("#%d,%s,productCode=%x\r\n",__LINE__,__FUNCTION__,productCode);
		/*if((productCode != 0x60070013)&&(productCode != 0x60030022)&&(productCode != 0x60030023))
		{
			*(uint8*)(outputClockAdress[slot-1]) = 0x3d;
			productCode = ProductCodeOfSlot(slot);
		}*/
		
		if((productCode == 0x60070013) || (productCode == 0x60030022)||(productCode == 0x60030023))
		{
			idt82V2058init(slot);
			ret = AtFhnDriverSlotCreate(slot);
			if(ret != cAtOk)
			{
				printf("init slot[%d] error ret =%x\r\n",slot,ret);
			}

			At_fhn_pos_e1_soft_init(slot);
			
			if(productCode == 0x60030023){
				for(mlBundleId=1;mlBundleId<=AT_POS_E1_MAX_MLPPP_BUNDLE_FHN;mlBundleId++)
				{
					ret = Udr_ce1_mlppp_bundle_create(slot, mlBundleId, &mlBundleId);
					if(ret !=0){
						printf("%s,#%d,ret=%x,slot=%d,bindId=%d\r\n",__FILE__,__LINE__,ret,slot,mlBundleId);
					}
				}
				printf("#%d,%s,slot[%d] is 16E1 MLPPP card\r\n",__LINE__,__FUNCTION__,slot);
			}else{
				printf("#%d,%s,slot[%d] is 16E1 PPP card\r\n",__LINE__,__FUNCTION__,slot);
			}
			

		}
		else if ((productCode == 0x60070023) ||(productCode == 0x60030051))
		{
			printf("#%d,%s,slot[%d] is STM card\r\n",__LINE__,__FUNCTION__,slot);
			ret = AtFhnDriverSlotCreate(slot);
			if(ret != cAtOk)
			{
				printf("init slot[%d] error ret =%x\r\n",slot,ret);
			}
			At_fhn_pos_stm_soft_init(slot);
			for(fpgaId=0;fpgaId<2;fpgaId++){
				for(mlBundleId=1;mlBundleId<=AT_POS_STM_MAX_MLPPP_BUNDLE_FHN;mlBundleId++){
					ret = Udr_cpos_mlppp_bundle_create(slot, fpgaId+1,mlBundleId, &mlBundleId);
					if(ret !=0){
						printf("%s,#%d,ret=%x,slot=%d,bindId=%d\r\n",__FILE__,__LINE__,ret,slot,mlBundleId);
					}
				}
			}
			printf("#%d,%s,slot[%d] is 16E1 MLPPP card\r\n",__LINE__,__FUNCTION__,slot);
		}
		else
		{
			printf("%s,#%d,%s,slot[%d] has unknown POSCardID[%x]\r\n",__FILE__,__LINE__,__FUNCTION__,slot,productCode);
			continue;
		}
	}
	printf("Udr_cpos_card_init complete\r\n");
	return 0;
}
#else

int Udr_cpos_card_init( void )
{
	uint32 mlBundleId=0,fpgaId=0;
	uint8 slot;
	int cNumSlots=2,ret=0;
	int productCode=0;
	char * creationDate   = __DATE__ ", " __TIME__;
	eAtEthPortInterface EncapTypeBuf = cAtEthPortInterface1000BaseX;

	//At_slot_card_probe();
	
    	printf ("atsdk.o Created: %s\n", creationDate);

	gui4PosStartBerMonitor[0]=0;
	gui4PosStartBerMonitor[1]=0;

	At_fhn_pos_ber_sd_sf_monitor();

	if ((AtDriver)AtFhnDriverCreate(cNumSlots) == NULL)
		   return -1;

	*(uint8*)(outputClockAdress[0]) = 0x3f;
	*(uint8*)(outputClockAdress[1]) = 0x3f;

	for(slot=1;slot<3;slot++)
	{
		semE1LogicalChannelNode[slot-1] = semBCreate(SEM_Q_FIFO, SEM_FULL);
		semStmLogicalChannelNode[slot-1] = semBCreate(SEM_Q_FIFO, SEM_FULL);
		/* added by ssq 20140609 */
		semAtUdrProtect[slot-1] = semBCreate(SEM_Q_FIFO, SEM_FULL);
		gui4posSdhAugMapRecord[slot-1][0]=0;
		gui4posSdhAugMapRecord[slot-1][1]=0;
	}
	printf("Udr_cpos_card_init complete\r\n");
	return 0;
}


#endif



/*
slot [1-2]
*/
int Udr_cpos_card_plug_in(uint8 cardId)
{
	uint32 mlBundleId=0,fpgaId=0,cardType=0;
	uint8 slot = cardId;
	int ret;
	uint32 productCode,port=0,arg=0,ch=0;
	uint8 c2Flag;
	tAtSdhTtiFhn poh;

	poh.mode = 1;
	poh.paddingMode = 0;

	if(slot<1||slot>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	ret = At_slot_card_type_get(slot);
	if(ret == -1){
		return -1;
	}

	taskDelay(300);

    	productCode = ProductCodeOfSlot(slot);
	printf("%s,#%d,%s,productCode=%x\r\n",__FILE__,__LINE__,__FUNCTION__,productCode);
	/*
	if((productCode != 0x60070013)&&(productCode != 0x60030022)&&(productCode != 0x60030023))
	{
		*(uint8*)(outputClockAdress[slot-1]) = 0x3d;
		productCode = ProductCodeOfSlot(slot);
	}
	*/
	
	if((productCode == 0x60070013) || (productCode == 0x60030022)||(productCode == 0x60030023))
	{
		idt82V2058init(slot);
		ret = AtFhnDriverSlotCreate(slot);
		if(ret != cAtOk)
		{
			printf("init slot[%d] error ret =%x\r\n",slot,ret);
		}

		At_fhn_pos_e1_soft_init(slot);
		
		if(productCode == 0x60030023){
			for(mlBundleId=1;mlBundleId<=AT_POS_E1_MAX_MLPPP_BUNDLE_FHN;mlBundleId++)
			{
				ret = Udr_ce1_mlppp_bundle_create(slot, mlBundleId, &mlBundleId);
				if(ret !=0){
					printf("%s,#%d,ret=%x,slot=%d,bindId=%d\r\n",__FILE__,__LINE__,ret,slot,mlBundleId);
				}
			}
			printf("%s,#%d,%s,slot[%d] is E1 MLPPP card\r\n",__FILE__,__LINE__,__FUNCTION__,slot);
		}else{
			printf("%s,#%d,%s,slot[%d] is 16E1 PPP card\r\n",__FILE__,__LINE__,__FUNCTION__,slot);
		}

		
		for(port=1;port<=AT_POS_E1_MAX_PORT_FHN;port++)
		{
			arg = 0;
			Udr_ce1_port_enable(slot,port,&arg);/* added by ssq 20140529 */
			Udr_ce1_port_admin_status(slot,port,&arg);
		}
		
		//cpos_card_fpga_select(1);
		//Udr_at_eth_port_mode_set(1, &EncapTypeBuf);
	}
	else if ((productCode == 0x60070023) ||(productCode == 0x60030051))
	{
		printf("%s,#%d,%s,slot[%d] is STM card\r\n",__FILE__,__LINE__,__FUNCTION__,slot);
		ret = AtFhnDriverSlotCreate(slot);
		//Udr_at_eth_port_mode_set(1, &EncapTypeBuf);
		//Udr_at_eth_port_mode_set(2, &EncapTypeBuf);
		if(ret != cAtOk)
		{
			printf("init slot[%d] error ret =%x\r\n",slot,ret);
		}

		At_fhn_pos_stm_soft_init(slot);

		cardType = *(uint8 *)gui4AtslotAddress[cardId];

		switch(cardType)
		{
			case cAtCardTypeline4STM:

				for(fpgaId=0;fpgaId<1;fpgaId++){
					for(mlBundleId=1;mlBundleId<=AT_POS_STM_MAX_MLPPP_BUNDLE_FHN;mlBundleId++){
						ret = Udr_cpos_mlppp_bundle_create(slot, fpgaId+1,mlBundleId, &mlBundleId);
						if(ret !=0){
							printf("%s,#%d,ret=%x,slot=%d,bindId=%d\r\n",__FILE__,__LINE__,ret,slot,mlBundleId);
						}
					}
				}

				for(port=1;port<=4;port++){
					/* stm1 moden */
					arg = 0;
					ret = Udr_cpos_port_linerate_set(slot, port, &arg);
					if(ret !=0){
						printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
						return -1;
					}
					
					/* enable ber */
					arg=1;			
					Udr_cpos_port_ber_enable(slot,port,&arg);

					for(ch=1;ch<=63;ch++){
						ret = Udr_cpos_port_stm1_sdh_map(slot, port, ch);
						if(ret !=0){
							printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
							return -1;
						}
					}

					gui4posSdhAugMapRecord[slot-1][port-1]=AT_CPOS_STM1_MAPPING;

					arg=0;
					Udr_cpos_port_enable(slot,  port, &arg);/* added by ssq 20140529 */
					Udr_cpos_port_admin_status(slot,port,&arg);

					sprintf(poh.message,"default");
					ret = udr_if_stm_pos_j0_mode(cardId, port, &poh);
					if(ret !=0){
						printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
						return -1;
					}
					ret = udr_if_stm_pos_j1_mode(cardId, port, &poh);
					if(ret !=0){
						printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
						return -1;
					}
					c2Flag = 2;
					ret = udr_if_stm_pos_c2_flag(cardId, port, &c2Flag);
					if(ret !=0){
						printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
						return -1;
					}

					arg=4;
					ret = Udr_cpos_port_ber_sd_set(cardId, port, &arg);
					if(ret !=0){
						printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
						return -1;
					}

					arg=1;
					ret = Udr_cpos_port_ber_sf_set(cardId, port, &arg);
					if(ret !=0){
						printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
						return -1;
					}
					
				}

				//At_fhn_start_pos_ber_set(slot,1);
				break;

			case  cAtCardTypeline1STM4:

				for(fpgaId=0;fpgaId<1;fpgaId++){
					for(mlBundleId=1;mlBundleId<=AT_POS_STM_MAX_MLPPP_BUNDLE_FHN;mlBundleId++){
						ret = Udr_cpos_mlppp_bundle_create(slot, fpgaId+1,mlBundleId, &mlBundleId);
						if(ret !=0){
							printf("%s,#%d,ret=%x,slot=%d,bindId=%d\r\n",__FILE__,__LINE__,ret,slot,mlBundleId);
						}
					}
				}

				port=1;
				/* stm4 moden */
				arg = 1;
				ret = Udr_cpos_port_linerate_set(slot, port,&arg);
				if(ret !=0){
					printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
					return -1;
				}
				
				/* enable ber */
				arg=1;			
				Udr_cpos_port_ber_enable(slot,port,&arg);

				Udr_pos_stm4_vc4c_port_sdh_map(slot,port);
				gui4posSdhAugMapRecord[slot-1][port-1]=AT_POS_STM4_MAPPING;

				arg=0;
				Udr_cpos_port_enable(slot,  port, &arg);/* added by ssq 20140529 */
				Udr_cpos_port_admin_status(slot,port,&arg);

				sprintf(poh.message,"default");
				ret = udr_if_stm_pos_j0_mode(cardId, port, &poh);
				if(ret !=0){
					printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
					return -1;
				}
				ret = udr_if_stm_pos_j1_mode(cardId, port, &poh);
				if(ret !=0){
					printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
					return -1;
				}
				c2Flag = 2;
				ret = udr_if_stm_pos_c2_flag(cardId, port, &c2Flag);
				if(ret !=0){
					printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
					return -1;
				}

				arg=4;
				ret = Udr_cpos_port_ber_sd_set(cardId, port, &arg);
				if(ret !=0){
					printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
					return -1;
				}

				arg=1;
				ret = Udr_cpos_port_ber_sf_set(cardId, port, &arg);
				if(ret !=0){
					printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
					return -1;
				}
					
				//At_fhn_start_pos_ber_set(slot,1);
				break;

			case cAtCardTypeline8STM:
				for(fpgaId=0;fpgaId<2;fpgaId++){
					for(mlBundleId=1;mlBundleId<=AT_POS_STM_MAX_MLPPP_BUNDLE_FHN;mlBundleId++){
						ret = Udr_cpos_mlppp_bundle_create(slot, fpgaId+1,mlBundleId, &mlBundleId);
						if(ret !=0){
							printf("%s,#%d,ret=%x,slot=%d,bindId=%d\r\n",__FILE__,__LINE__,ret,slot,mlBundleId);
						}
					}
					gui4posSdhAugMapRecord[cardId-1][fpgaId]=AT_CPOS_STM1_MAPPING;
				}

				for(port=1;port<=AT_POS_STM_MAX_PORT_FHN;port++){

					/* stm1 moden */
					arg = 0;
					ret = Udr_cpos_port_linerate_set(slot, port, &arg);
					if(ret !=0){
						printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
						return -1;
					}
					
					/* enable ber */
					arg=1;			
					Udr_cpos_port_ber_enable(slot,port,&arg);
					
					for(ch=1;ch<=63;ch++){
						ret = Udr_cpos_port_stm1_sdh_map(slot, port, ch);
						if(ret !=0){
							printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
							return -1;
						}
					}
					gui4posSdhAugMapRecord[slot-1][port-1]=AT_CPOS_STM1_MAPPING;

					arg=0;
					Udr_cpos_port_enable(slot,  port, &arg);/* added by ssq 20140529 */
					Udr_cpos_port_admin_status(slot,port,&arg);

					sprintf(poh.message,"default");
					ret = udr_if_stm_pos_j0_mode(cardId, port, &poh);
					if(ret !=0){
						printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
						return -1;
					}
					ret = udr_if_stm_pos_j1_mode(cardId, port, &poh);
					if(ret !=0){
						printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
						return -1;
					}
					c2Flag = 2;
					ret = udr_if_stm_pos_c2_flag(cardId, port, &c2Flag);
					if(ret !=0){
						printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
						return -1;
					}

					arg=4;
					ret = Udr_cpos_port_ber_sd_set(cardId, port, &arg);
					if(ret !=0){
						printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
						return -1;
					}

					arg=1;
					ret = Udr_cpos_port_ber_sf_set(cardId, port, &arg);
					if(ret !=0){
						printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
						return -1;
					}
					
				}
				//At_fhn_start_pos_ber_set(slot,1);
				break;

			case  cAtCardTypeline2STM4:
				for(fpgaId=0;fpgaId<2;fpgaId++){
					for(mlBundleId=1;mlBundleId<=AT_POS_STM_MAX_MLPPP_BUNDLE_FHN;mlBundleId++){
						ret = Udr_cpos_mlppp_bundle_create(slot, fpgaId+1,mlBundleId, &mlBundleId);
						if(ret !=0){
							printf("%s,#%d,ret=%x,slot=%d,bindId=%d\r\n",__FILE__,__LINE__,ret,slot,mlBundleId);
						}
					}
				}

				for(port=1;port<=AT_POS_STM_MAX_PORT_FHN;port=port*5){
					/* stm4 moden */
					arg = 1;
					ret = Udr_cpos_port_linerate_set(slot, port, &arg);
					if(ret !=0){
						printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
						return -1;
					}
					
					/* enable ber */
					arg=1;			
					Udr_cpos_port_ber_enable(slot,port,&arg);
					
					Udr_pos_stm4_vc4c_port_sdh_map(slot,port);
					gui4posSdhAugMapRecord[slot-1][port-1]=AT_POS_STM4_MAPPING;

					arg=0;
					Udr_cpos_port_enable(slot,  port, &arg);/* added by ssq 20140529 */
					Udr_cpos_port_admin_status(slot,port,&arg);

					sprintf(poh.message,"default");
					ret = udr_if_stm_pos_j0_mode(cardId, port, &poh);
					if(ret !=0){
						printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
						return -1;
					}
					ret = udr_if_stm_pos_j1_mode(cardId, port, &poh);
					if(ret !=0){
						printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
						return -1;
					}
					c2Flag = 2;
					ret = udr_if_stm_pos_c2_flag(cardId, port, &c2Flag);
					if(ret !=0){
						printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
						return -1;
					}

					arg=4;
					ret = Udr_cpos_port_ber_sd_set(cardId, port, &arg);
					if(ret !=0){
						printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
						return -1;
					}

					arg=1;
					ret = Udr_cpos_port_ber_sf_set(cardId, port, &arg);
					if(ret !=0){
						printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
						return -1;
					}
					
				}
				//At_fhn_start_pos_ber_set(slot,1);
				break;

			default:
				printf("init slot[%d] error cardType =%x\r\n",slot,cardType);
				return -1;

		}

	}else{
		//*(uint32*)(outputClockAdress[slot-1]) = 0x3f3f0000;/*  output local clock */
		printf("%s,#%d,%s,slot[%d] has not POS card\r\n",__FILE__,__LINE__,__FUNCTION__,slot);
	}


	printf("Udr_cpos_card_plug_in complete\r\n");
	return 0;
}


int Udr_cpos_card_plug_out(uint8 cardId)
{ 
	int ret = 0;
	uint8 slot = cardId;
	
	if(slot<1||slot>2)
	{
		printf("error slot id =%d\r\n",slot);
	}

	At_fhn_start_pos_ber_set(slot,0);

	ret = AtFhnDriverSlotDelete(slot);

	gui4PosStartBerMonitor[0]=0;
	gui4PosStartBerMonitor[1]=0;

	semFlush(semE1LogicalChannelNode[slot-1]);
	semFlush(semStmLogicalChannelNode[slot-1]);
	/* added by ssq 20140609 */
	semFlush(semAtUdrProtect[slot-1]);
	return ret; 
}

/*

*/
int Udr_at_eth_port_smac_set(uint32 cardId, uint32 fpgaID, uint32 ethport, void *arg)
{
	AtDevice *pDevice;
	uint8 slotId=cardId;
	AtModuleEth ethModule;
	AtEthPort portChannel;
	uint8 mac[cAtMacAddressLen];
	eAtRet ret = cAtOk;

	if(gui4AtEthDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		memcpy(mac, (char *)arg,6);
		printf("port=%d,MAC =%x:%x:%x:%x:%x:%x \r\n",ethport,mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(fpgaID<1||fpgaID>2){
		printf("#%d,%s,error fpgaID=%d\r\n",__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}


	if((ethport>2) || (ethport<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error port=%d\r\n",ethport);
		return -1;
	}

	memcpy(mac, (char *)arg,6);

	ethModule = (AtModuleEth)AtFhnEthModule(slotId,fpgaID);
	portChannel = AtModuleEthPortGet(ethModule, ethport - 1);
	ret = AtEthPortSourceMacAddressSet(portChannel, mac);
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	
	return ret;
	
}


 
/*
CmdEthFlowCreate
    eAtEthPortInterface intfMode;
	+ 1000basex: 1000Base-X
	+ sgmii: SGMII
	+ rgmii: RGMII
	+ gmii: GMII
	+ mii: MII  

typedef enum eAtEthPortInterface
    {
    cAtEthPortInterfaceUnknown,   /**< Unknown interface 
    cAtEthPortInterface1000BaseX, /**< 1000Base-X interface 
    cAtEthPortInterfaceSgmii,     /**< SGMII interface 
    cAtEthPortInterfaceRgmii,     /**< RGMII interface 
    cAtEthPortInterfaceGmii,      /**< GMII interface 
    cAtEthPortInterfaceMii        /**< MII interface 
    }eAtEthPortInterface;

*/
int Udr_at_eth_port_mode_set(uint32 cardId, void *arg)
{
	uint32 ethport=1;
	AtDevice *pDevice;
	uint8 slotId=cardId;
	AtModuleEth ethModule;
	AtEthPort portChannel;
   	eAtEthPortInterface intfMode = *(eAtEthPortInterface *)arg;
	eAtRet ret = cAtOk;

	if(gui4AtEthDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,intfMode =%x \r\n",cardId,intfMode);
	}

	if((intfMode<cAtEthPortInterfaceUnknown)||(intfMode>cAtEthPortInterfaceMii))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error arg=%d\r\n",intfMode);
		return -1;
	}

	ethModule = (AtModuleEth)AtFhnEthModule(slotId,1);
	portChannel = AtModuleEthPortGet(ethModule, ethport - 1);
	ret = AtEthPortInterfaceSet(portChannel, intfMode);
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}


	ethModule = (AtModuleEth)AtFhnEthModule(slotId,2);
	if(ethModule != NULL)
	{
		portChannel = AtModuleEthPortGet(ethModule, ethport - 1);
		ret = AtEthPortInterfaceSet(portChannel, intfMode);
		if (ret != cAtOk){
			printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
			return -1;
		}
	}

	return 0;
}



/*
0: HDB3 code (default)
1: AMI code
*/
int Udr_ce1_card_interfece_code_set(uint8 cardId, uint32 port, void *arg)
{
	uint8 slotId=cardId;
	uint32 code = *(uint32 *)arg;

	if(gui4AtEthDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,port =%x code=%x\r\n",cardId,port,code);
	}


	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	printf("%s,#%d,%s,code=%d\r\n",__FILE__,__LINE__,__FUNCTION__,code);
	
	idt_line_code_set(slotId, code);
	return 0;
}


/*
enable 	0: enable force down
		1: diable force down
*/
int Udr_ce1_port_admin_status(uint8 cardId, uint32 e1port, void *arg)
{
	uint32 enable = *(uint32 *)arg;
	AtSdhLine sdhModuleLine;
	eAtRet  ret;
	AtEncapChannel encapChannel;
	uint8 slotId=cardId;
	uint32 port = e1port;

	if(gui4EncapDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x, port=%d,enable=%x\r\n",cardId,port,enable);
	}
	
	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((port>32) || (port<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",port);
		return -1;
	}

	if(enable){
		ret = AtChannelRxAlarmUnForce((AtChannel)AtFhnDe1Get(slotId, port), cAtSdhLineAlarmLos);
	}else{
		ret = AtChannelRxAlarmForce((AtChannel)AtFhnDe1Get(slotId, port), cAtSdhLineAlarmLos);
	}
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
       }
	
	return 0;

}

/*
CmdEthFlowCreate
arg:
    cAtEthFlowNpoPpp,
    cAtEthFlowEthoPpp
*/

int Udr_ce1_port_flow_create(uint8 cardId, uint32 flowId, void *arg)
{
	AtEthFlow flow;
	eAtEthFlowTypeFhn EncapTypeBuf = *(eAtEthFlowTypeFhn *)arg;
	uint32 bufferSize;
	AtModuleEth ethModule;
	AtDevice *pDevice;
	uint8 deviceNum=1,slotId=cardId;
	
	if(gui4AtFlowDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,flowId=%d,arg = %x\r\n",cardId,flowId,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

/*
	if((flowId>128) || (flowId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",flowId);
		return -1;
	}
*/

	if((EncapTypeBuf<cAtEthFlowIpoPppFhn)||(EncapTypeBuf>cAtEthFlowEthoPppFhn))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error arg=%d\r\n",EncapTypeBuf);
		return -1;
	}

    if (EncapTypeBuf == cAtEthFlowIpoPppFhn)
        flow = AtModuleEthNopFlowCreate((AtModuleEth)AtFhnEthModule(slotId,deviceNum), flowId - 1);
    else
        flow = AtModuleEthEopFlowCreate((AtModuleEth)AtFhnEthModule(slotId,deviceNum), flowId - 1);

    return flow ? 0 : -1;

}

/*
CmdEthFlowDelete
*/
int Udr_ce1_port_flow_del(uint8 cardId, uint32 flowId, void *arg)
{
    	eAtRet ret = cAtOk;
	AtEthFlow flow;
	eAtEthFlowTypeFhn EncapTypeBuf = *(eAtEthFlowTypeFhn *)arg;
	uint32 bufferSize;
	AtModuleEth ethModule;
	AtDevice *pDevice;
	uint8 deviceNum=1,slotId=cardId;;
	
	if(gui4AtFlowDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,arg = %x\r\n", cardId,flowId,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

/*
	if((flowId>128) || (flowId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",flowId);
		return -1;
	}
*/

	if((EncapTypeBuf<cAtEthFlowIpoPppFhn)||(EncapTypeBuf>cAtEthFlowEthoPppFhn))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error arg=%d\r\n",EncapTypeBuf);
		return -1;
	}

        /* delete flow */
	ret = AtModuleEthFlowDelete((AtModuleEth)AtFhnEthModule(slotId, deviceNum), flowId - 1);
        /* Check if it is created */
	if (ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

    return 0;
}

int Udr_ce1_port_flow_egress_dmac_set(uint8 cardId, uint32 flowId, void *arg)
{
	uint8 mac[cAtMacAddressLen];
	AtEthFlow flow;
	uint16 i;
	uint32 *idBuf;
	uint32 numFlows = flowId;
	tAtEthVlanDesc desc;
	AtModuleEth ethModule;
	eAtRet ret = cAtOk;
	uint8 deviceNum=1,slotId=cardId;
	AtDevice *pDevice;

	if(gui4AtFlowDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		memcpy(mac, (char *)arg,6);
		printf("cardId=%x,flowId=%d,MAC =%x:%x:%x:%x:%x:%x \r\n",cardId,flowId,mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

/*
	if((flowId>128) || (flowId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",flowId);
		return -1;
	}
*/

	memcpy(mac, (char *)arg,6);


	/* Get Ethernet Module */
	ethModule = (AtModuleEth)AtFhnEthModule(slotId, deviceNum);
	
	/* get flow */
	flow = AtModuleEthFlowGet(ethModule, numFlows- 1);

        if (flow != NULL)
            ret |= AtEthFlowEgressDestMacSet(flow, mac);
        else
        {
            printf("Flow %d does not exist\r\n", numFlows);
            return -1;
        }
        
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

    return 0;
}

int Udr_ce1_port_flow_egress_vlan_set(uint8 cardId, uint32 ethport,uint32 flowId, void *arg)
{
	tAtEthVlanTag *vlanTag = arg;
	AtEthFlow flow;
	uint32 numFlows = flowId;
	tAtEthVlanDesc desc;
	AtModuleEth ethModule;
	eAtRet ret = cAtOk;
	uint8 deviceNum=1,slotId=cardId;

	if(gui4AtFlowDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,ethport=%x,flow=%d,arg = %x\r\n",cardId,ethport,flowId,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((ethport>2) || (ethport<0))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error ethport=%d\r\n",ethport);
		return -1;
	}
	
/*
	if((flowId>128) || (flowId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",flowId);
		return -1;
	}
*/

	/* create vlan desc */
	AtEthVlanTagConstruct(vlanTag->priority, vlanTag->cfi, vlanTag->vlanId, &desc.vlans[0]);
	desc.numberOfVlans=1;
	desc.ethPortId =ethport- 1;

	/* Get Ethernet Module */
	ethModule = (AtModuleEth)AtFhnEthModule(slotId, deviceNum);
	
	/* get flow */
	flow = AtModuleEthFlowGet(ethModule, numFlows- 1);

        if (flow != NULL)
            ret |= AtEthFlowEgressVlanSet(flow, &desc);
        else
        {
            printf("Flow %d does not exist\r\n", numFlows);
            return -1;
        }
        
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

    return 0;
}


int  Udr_ce1_port_flow_set_vlan(uint8 cardId, uint32 ethport,uint32 flowId, void *arg)
{
	tAtEthVlanTag *vlanTag = arg;
	AtEthFlow flow;
	uint16 i;
	uint32 *idBuf;
	uint32 numFlows = flowId;
	tAtEthVlanDesc desc;
	AtModuleEth ethModule;
	eAtRet ret = cAtOk;
	uint8 deviceNum=1,slotId=cardId;
	AtDevice *pDevice;

	if(gui4AtFlowDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,ethport=%x,flow=%d,arg = %x\r\n",cardId,ethport,flowId,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((ethport>2) || (ethport<0))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error ethport=%d\r\n",ethport);
		return -1;
	}

/*
	if((flowId>128) || (flowId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",flowId);
		return -1;
	}
*/


	/* create vlan desc */
	AtEthVlanTagConstruct(vlanTag->priority, vlanTag->cfi, vlanTag->vlanId, &desc.vlans[0]);
	desc.numberOfVlans=1;
	desc.ethPortId =ethport- 1;

	/* Get Ethernet Module */
	ethModule = (AtModuleEth)AtFhnEthModule(slotId, deviceNum);
	
	/* get flow */
	flow = AtModuleEthFlowGet(ethModule, numFlows- 1);

	if (flow != NULL)
		ret |= AtEthFlowIngressVlanAdd(flow, &desc);
	else
	{
		printf("Flow %d does not exist\r\n", numFlows);
		return -1;
	}

	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}	

    return 0;
}


int  Udr_ce1_port_flow_delete_vlan(uint8 cardId, uint32 ethport,uint32 flowId, void *arg)
{
	tAtEthVlanTag *vlanTag = arg;
	AtEthFlow flow;
	uint32 numFlows = flowId;
	tAtEthVlanDesc desc;
	AtModuleEth ethModule;
	eAtRet ret = cAtOk;
	uint8 deviceNum=1,slotId=cardId;

	if(gui4AtFlowDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,ethport=%x,flow=%d,arg = %x\r\n",cardId,ethport,flowId,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((ethport>2) || (ethport<0))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error ethport=%d\r\n",ethport);
		return -1;
	}
	
/*
	if((flowId>128) || (flowId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",flowId);
		return -1;
	}
*/


	/* create vlan desc */
	AtEthVlanTagConstruct(vlanTag->priority, vlanTag->cfi, vlanTag->vlanId, &desc.vlans[0]);
	desc.numberOfVlans=1;
	desc.ethPortId =ethport- 1;

	/* Get Ethernet Module */
	ethModule = (AtModuleEth)AtFhnEthModule(slotId,deviceNum);
	
	/* get flow */
	flow = AtModuleEthFlowGet(ethModule, numFlows- 1);

        if (flow != NULL)
            ret |= AtEthFlowIngressVlanRemove(flow, &desc);
        else
            {
            printf("Flow %d does not exist\r\n", numFlows);
            return -1;
            }
        

	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

    return 0;
}

/*
CmdEncapChannelScramble

channel = 1-128

0:disable
1:enable

*/
int Udr_ce1_port_channel_scramle_set(uint8 cardId, uint32 channel, void *arg)
{
	int ret=0;
	uint32 EncapTypeBuf = *(uint32 *)arg;
	uint8 deviceNum =1,slotId=cardId;
	AtHdlcChannel hdlcChannel;
	
	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,arg = %x\r\n",cardId,channel,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>128) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	if((EncapTypeBuf<0)||(EncapTypeBuf>1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error arg=%d\r\n",EncapTypeBuf);
		return -1;
	}

    	hdlcChannel = (AtHdlcChannel)AtModuleEncapChannelGet((AtModuleEncap)AtFhnEncapModule(slotId,deviceNum), channel - 1);
		
	ret = AtHdlcChannelScrambleEnable(hdlcChannel, EncapTypeBuf);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	return 0;
	
}


/*
channel = 1-128

CmdEncapHdlcMtu

MTU 46-9600

*/
int Udr_ce1_port_channel_mtu_set(uint8 cardId, uint32 channel, void *arg)
{
	int ret=0;
	uint32 mtu = *(uint32 *)arg;
	uint8 deviceNum =1,slotId=cardId;
	AtHdlcChannel hdlcChannel;

	if(gui4PPPdebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,arg = %x\r\n",cardId,channel,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>128) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return cAtFalse;
	}

	if((mtu<46)||(mtu>9600))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error arg=%d\r\n",mtu);
		return cAtFalse;
	}

    	hdlcChannel = (AtHdlcChannel)AtModuleEncapChannelGet((AtModuleEncap)AtFhnEncapModule(slotId, deviceNum), channel - 1);

	ret = AtHdlcChannelMtuSet(hdlcChannel, mtu);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	return 0;
	
}

/*
CmdPdhDe1LoopbackSet
portId = 1-16
arg:
typedef enum eAtPdhLoopbackModeFhn
    {
    cAtPdhLoopbackModeReleaseFhn,        /**< Release loopback 
    cAtPdhLoopbackModeLocalPayloadFhn,   /**< Payload Local loopback 
    cAtPdhLoopbackModeRemotePayloadFhn,  /**< Payload Remote loopback 
    cAtPdhLoopbackModeLocalLineFhn,      /**< Line Local loopback 
    cAtPdhLoopbackModeRemoteLineFhn      /**< Line Local loopback 
    }eAtPdhLoopbackModeFhn;

*/

int Udr_ce1_port_loopback_set(uint8 cardId, uint32 port, void *arg)
{
	int ret=0;
	eAtPdhLoopbackModeFhn EncapTypeBuf = *(eAtPdhLoopbackModeFhn *)arg;
	uint32 deviceNum =1,slotId=cardId;

	if(gui4AtFlowDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,arg = %x\r\n",cardId,port,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((port>16) || (port<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",port);
		return -1;
	}

	if((EncapTypeBuf<cAtPdhLoopbackModeReleaseFhn)||(EncapTypeBuf>cAtPdhLoopbackModeRemoteLineFhn))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error arg=%d\r\n",EncapTypeBuf);
		return -1;
	}

	ret = AtChannelLoopbackSet((AtChannel)AtFhnDe1Get(slotId, port),EncapTypeBuf);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	return 0;
}

/*
CmdPdhDe1Enable
CmdPdhDe1Disable
port = 1-32

0:disable
1:enable

*/
int Udr_ce1_port_enable(uint8 cardId, uint32 port, void *arg)
{
	int ret=0;
	uint32 enable = *(uint32 *)arg;
	uint8 deviceNum =1,slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,arg = %x\r\n",cardId,port,enable);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((port>32) || (port<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",port);
		return -1;
	}

	ret = AtChannelEnable((AtChannel)AtFhnDe1Get(slotId, port), enable);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	return 0;
	
}


int Udr_ce1_port_channel_ppp_enable(uint8 cardId, uint32 channel, void *arg)
{
    	eAtPppLinkPhase phase = *(eAtPppLinkPhase *)arg;
	AtModuleEncap encapModule;
	AtChannel channels;
	AtHdlcLink link;
	AtDevice *pDevice;
	AtHdlcChannel  hdlcChannel;
	eAtRet  ret;
	uint8 deviceNum=1,slotId=cardId;

	if(gui4PPPdebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,arg = %x\r\n",cardId,channel,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>128) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	if((phase<cAtPppLinkPhaseUnknown)||(phase>cAtPppLinkPhaseNetworkActive))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error arg=%d\r\n",phase);
		return -1;
	}

	//pDevice = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), &deviceNum);
	//encapModule = (AtModuleEncap)AtDeviceModuleGet(*pDevice, cAtModuleEncap);

	encapModule = (AtModuleEncap)AtFhnEncapModule(slotId, deviceNum);

	/* get hdlc channle */
	hdlcChannel = (AtHdlcChannel)AtModuleEncapChannelGet(encapModule, channel - 1);
	if (hdlcChannel == NULL)
	    return 0;


    if (AtEncapChannelEncapTypeGet((AtEncapChannel)hdlcChannel) != cAtEncapHdlc)
        printf("port %d invalid encapsulation type\r\n", channel);
	
    	link = (AtHdlcLink)AtHdlcChannelHdlcLinkGet(hdlcChannel) ;

	
	/* enable this channle */
	ret = AtPppLinkPhaseSet((AtPppLink)link, phase);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}

int Udr_ce1_port_channel_txenable(uint8 cardId, uint32 channel, void *arg)
{
    	uint32 enable = *(uint32 *)arg;
	AtModuleEncap encapModule;
	AtChannel channels;
	AtHdlcLink link;
	AtDevice *pDevice;
	AtHdlcChannel  hdlcChannel;
	eAtRet  ret;
	uint8 deviceNum=1,slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,arg = %x\r\n",cardId,channel,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>128) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	if((enable<0)||(enable>1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error arg=%d\r\n",enable);
		return -1;
	}

	//pDevice = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), &deviceNum);
	//encapModule = (AtModuleEncap)AtDeviceModuleGet(*pDevice, cAtModuleEncap);

	encapModule = (AtModuleEncap)AtFhnEncapModule(slotId, deviceNum);

	/* get hdlc channle */
	hdlcChannel = (AtHdlcChannel)AtModuleEncapChannelGet(encapModule, channel - 1);
	if (hdlcChannel == NULL)
	    return 0;


    if (AtEncapChannelEncapTypeGet((AtEncapChannel)hdlcChannel) != cAtEncapHdlc)
        printf("port %d invalid encapsulation type\r\n", channel);
	
    	link = (AtHdlcLink)AtHdlcChannelHdlcLinkGet(hdlcChannel) ;

	
	/* enable this channle */
	ret = AtHdlcLinkTxTrafficEnable(link, enable);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}

/*
channel = 1-128

AtHdlcLinkTxTrafficEnable
AtHdlcLinkTxTrafficDisable

0:disable
1:enable

*/
eAtRet Udr_ce1_port_channel_rxenable(uint8 cardId, uint32 channel, void *arg)
{
    	uint32 enableBool = *(uint32 *)arg;
	AtModuleEncap encapModule;
	AtChannel channels;
	AtHdlcLink link;
	AtDevice *pDevice;
	AtHdlcChannel  hdlcChannel;
	eAtRet  ret;
	uint8 deviceNum=1,slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,arg = %x\r\n",cardId,channel,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>128) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error port=%d\r\n",channel);
		return -1;
	}

	if((enableBool<0)||(enableBool>1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error arg=%d\r\n",enableBool);
		return -1;
	}

	//pDevice = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), &deviceNum);
	//encapModule = (AtModuleEncap)AtDeviceModuleGet(*pDevice, cAtModuleEncap);

	encapModule = (AtModuleEncap)AtFhnEncapModule(slotId, deviceNum);

	/* get hdlc channle */
	hdlcChannel = (AtHdlcChannel)AtModuleEncapChannelGet(encapModule, channel - 1);
	if (hdlcChannel == NULL)
	    return -1;


    if (AtEncapChannelEncapTypeGet((AtEncapChannel)hdlcChannel) != cAtEncapHdlc)
        printf("port %d invalid encapsulation type\r\n", channel);
	
    	link = (AtHdlcLink)AtHdlcChannelHdlcLinkGet(hdlcChannel) ;

	
	/* enable this channle */
	ret = AtHdlcLinkRxTrafficEnable(link, enableBool);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}

/*
CmdEncapChannelCreate

channelId = 1-128

typedef enum eAtEncapTypeCreate
    {
    cAtHdlcFrmCiscoHdlcCreate,//**< Cisco HDLC 
    cAtHdlcFrmPppCreate,      //**< PPP 
    cAtHdlcFrmFrCreate,       //**< Frame relay 
    cAtEncapAtmCreate         //**< ATM 
    } eAtEncapTypeCreate;
*/


int Udr_ce1_port_channel_create(uint8 cardId, uint32 channel, void *arg)
{
    	eAtEncapTypeCreateFhn encapType = *(eAtEncapTypeCreateFhn *)arg;
	AtModuleEncap encapModule;
	AtDevice *pDevice;
	eAtRet  ret;
	AtEncapChannel encapChannel;
	uint8 deviceNum=1,slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,arg = %x\r\n",cardId,channel,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>128) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	if((encapType<cAtHdlcFrmCiscoHdlcCreateFhn)||(encapType>cAtEncapAtmCreateFhn))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error arg=%d\r\n",encapType);
		return -1;
	}

	//pDevice = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), &deviceNum);
	//encapModule = (AtModuleEncap)AtDeviceModuleGet(*pDevice, cAtModuleEncap);
	encapModule = (AtModuleEncap)AtFhnEncapModule(slotId, deviceNum);


    /* SET */
        switch (encapType)
            {
            case cAtHdlcFrmPppCreateFhn:
                encapChannel = (AtEncapChannel)AtModuleEncapHdlcPppChannelCreate(encapModule, channel - 1);
                break;

            case cAtHdlcFrmFrCreateFhn:
                encapChannel = (AtEncapChannel)AtModuleEncapFrChannelCreate(encapModule, channel - 1);
                break;

            case cAtHdlcFrmCiscoHdlcCreateFhn:
                encapChannel = (AtEncapChannel)AtModuleEncapCiscoHdlcChannelCreate(encapModule, channel - 1);
                break;

            case cAtEncapAtmCreateFhn:
                encapChannel = (AtEncapChannel)AtModuleEncapAtmTcCreate(encapModule, channel - 1);
                break;

            default:
                printf("ERROR: Encap channel type is %x,not support. Expected cisco|ppp|framerelay|atm\r\n",encapType);
                return -1;
            }

        /* Check if channel is created */
        if (encapChannel == NULL)
            {
            printf("ERROR: The channel %d is exists or invalid id\r\n", channel);
		return -1;
            }

	return 0;

}

/*
CmdEncapChannelDelete

channelId = 1-128

*/


int Udr_ce1_port_channel_Delete(uint8 cardId, uint32 channel, void *arg)
{
    	eAtEncapTypeCreateFhn encapType = *(eAtEncapTypeCreateFhn *)arg;
	AtModuleEncap encapModule;
	AtDevice *pDevice;
	eAtRet  ret;
	AtEncapChannel encapChannel;
	uint8 deviceNum=1,slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,arg = %x\r\n",cardId,channel,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>128) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	if((encapType<cAtHdlcFrmCiscoHdlcCreateFhn)||(encapType>cAtEncapAtmCreateFhn))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error arg=%d\r\n",encapType);
		return -1;
	}

	ret = AtModuleEncapChannelDelete((AtModuleEncap)AtFhnEncapModule(slotId, deviceNum), channel - 1);
        /* Check if it is created */
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

    return 0;

}


/* 
crc16/crc32
typedef enum eAtHdlcFcsModeFhn
    {
    cAtHdlcFcsModeNoFcsFhn, /**< Disable FCS inserting 
    cAtHdlcFcsModeFcs16Fhn, /**< FCS-16 
    cAtHdlcFcsModeFcs32Fhn  /**< FCS-32 
    }eAtHdlcFcsModeFhn;

*/
int Udr_ce1_port_channel_fcs(uint8 cardId, uint32 channel, void *arg)
{
    	eAtHdlcFcsModeFhn fcsMode = *(eAtHdlcFcsModeFhn *)arg;
	AtModuleEncap encapModule;
	AtChannel channels;
	AtHdlcLink link;
	AtDevice *pDevice;
	AtHdlcChannel  hdlcChannel;
	eAtRet  ret;
	uint8 deviceNum=1,slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,arg = %x\r\n",cardId,channel,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>128) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	if((fcsMode<cAtHdlcFcsModeNoFcsFhn)||(fcsMode>cAtHdlcFcsModeFcs32Fhn))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error arg=%d\r\n",fcsMode);
		return -1;
	}

	//pDevice = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), &deviceNum);
	//encapModule = (AtModuleEncap)AtDeviceModuleGet(*pDevice, cAtModuleEncap);

	encapModule = (AtModuleEncap)AtFhnEncapModule(slotId, deviceNum);

	/* get hdlc channle */
	hdlcChannel = (AtHdlcChannel)AtModuleEncapChannelGet(encapModule, channel - 1);
	if (hdlcChannel == NULL)
	    return cAtOk;


    if (AtEncapChannelEncapTypeGet((AtEncapChannel)hdlcChannel) != cAtEncapHdlc)
        printf("port %d invalid encapsulation type\r\n", channel);
	
	/* set  this channle */
	ret = AtHdlcChannelFcsModeSet(hdlcChannel, fcsMode);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	return ret;

}

/* 
CmdEncapBindPhy

phyport 	1-32
timeslot  	bitmap:1-32

*/


int Udr_ce1_port_channel_bind_port(uint8 cardId, uint32 channel, void *arg)
{
	uint32 phyport = *(uint32 *)arg;
	uint32 timeslot = 0;
	AtModuleEncap encapModule;
	AtDevice *pDevice;
	eAtRet  ret;
	AtEncapChannel encapChannel;
	uint8 deviceNum=1,slotId=cardId;
	AtChannel channels;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,phyport=%x\r\n",cardId,channel,phyport);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>128) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	if((phyport<1)||(phyport>32))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error phyport=%x,timeslot=%x\r\n",phyport,timeslot);
		return -1;
	}

	//pDevice = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), &deviceNum);
	//encapModule = (AtModuleEncap)AtDeviceModuleGet(*pDevice, cAtModuleEncap);

	encapModule = (AtModuleEncap)AtFhnEncapModule(slotId, deviceNum);
	
	encapChannel = (AtEncapChannel)AtModuleEncapChannelGet(encapModule, channel - 1);


	ce1_port_mapto_channle(phyport,timeslot,&channels);

        ret = AtEncapChannelPhyBind(encapChannel, channels);			
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}

/* Flow binding channel */
/*
	flowId = 1-128
	channelId = 1-128
*/
int Udr_ce1_port_channel_bind_flow(uint8 cardId, uint32 channel, uint32 flowId)
{
	uint8 deviceNum=1,slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,flowId=%x\r\n",cardId,channel, flowId);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>128) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

    return (AtHdlcLinkFlowBind((AtHdlcLink)AtFhnHdlcLinkGet(slotId, 1,channel), (AtEthFlow)AtFhnEthFlowGet(slotId,1, flowId)) == cAtOk) ? 0 : -1;

}

int Udr_ce1_port_channel_unbind_port(uint8 cardId, uint32 channel, void *arg)
{
	uint32 phyport = *(uint32 *)arg;
	uint32 timeslot = 0;
	AtModuleEncap encapModule;
	AtDevice *pDevice;
	eAtRet  ret;
	AtEncapChannel encapChannel;
	uint8 deviceNum=1,slotId=cardId;
	AtChannel channels;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,phyport=%x\r\n",cardId,channel,phyport);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>128) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	if((phyport<1)||(phyport>32))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error phyport=%x,timeslot=%x\r\n",phyport,timeslot);
		return -1;
	}

	//pDevice = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), &deviceNum);
	//encapModule = (AtModuleEncap)AtDeviceModuleGet(*pDevice, cAtModuleEncap);

	encapModule = (AtModuleEncap)AtFhnEncapModule(slotId, deviceNum);
	
	encapChannel = (AtEncapChannel)AtModuleEncapChannelGet(encapModule, channel - 1);


	channels = NULL;

        ret = AtEncapChannelPhyBind(encapChannel, channels);			
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;
}

/* Flow binding channel */
/* CmdEncapHdlcLkFlowBind */
/*
	flowId = 1-128
	channelId = 1-128
*/
int Udr_ce1_port_channel_unbind_flow(uint8 cardId, uint32 channel, void *arg)
{
	uint32 flowId = *(uint32 *)arg;
	AtEthFlow flow;
	eAtRet ret = cAtOk;
	uint8 deviceNum=1,slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,flowId=%x\r\n",cardId,channel, flowId);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>128) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}
	
	flow = NULL;

	return (AtHdlcLinkFlowBind((AtHdlcLink)AtFhnHdlcLinkGet(slotId, 1, channel), flow) == cAtOk) ? 0 : -1;

}


/*
typedef enum eAtHdlcFrameTypeFhn
    {
    cAtHdlcFrmUnknownFhn,   /**< Un-known frame type 
    cAtHdlcFrmCiscoHdlcFhn, /**< Cisco HDLC 
    cAtHdlcFrmPppFhn,       /**< PPP 
    cAtHdlcFrmFrFhn,         /**< Frame relay 
    }eAtHdlcFrameTypeFhn;
*/
int Udr_ce1_port_channel_framer_type_set(uint8 cardId, uint32 channel, void *arg)
{
	uint8 frameType = *(eAtHdlcFrameTypeFhn *)arg;
	uint32 timeslot = 0;
	AtModuleEncap encapModule;
	AtDevice *pDevice;
	eAtRet  ret;
	AtHdlcChannel encapChannel;
	uint8 deviceNum=1,slotId=cardId;
	AtChannel channels;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,frameType=%x\r\n",cardId,channel,frameType);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>128) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	if(frameType>3)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error frameType=%d\r\n",frameType);
		return -1;
	}


	encapModule = (AtModuleEncap)AtFhnEncapModule(slotId, deviceNum);
	
	encapChannel = (AtHdlcChannel)AtModuleEncapChannelGet(encapModule, channel - 1);

        ret = AtHdlcChannelFrameTypeSet(encapChannel, frameType);			
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;
}



/*
CmdPdhDe1FrameModeSet
typedef enum eAtPdhDe1FrameType
    {
    cAtPdhDe1FrameUnknown , /**< Unknown frame 

    cAtPdhDs1J1UnFrm      , /**< DS1/J1 Unframe 
    cAtPdhDs1FrmSf        , /**< DS1 SF (D4) 
    cAtPdhDs1FrmEsf       , /**< DS1 ESF 
    cAtPdhDs1FrmDDS       , /**< DS1 DDS 
    cAtPdhDs1FrmSLC       , /**< DS1 SLC 

    cAtPdhJ1FrmSf         , /**< J1 SF 
    cAtPdhJ1FrmEsf        , /**< J1 ESF 

    cAtPdhE1UnFrm         , /**< E1 unframe 
    cAtPdhE1Frm           , /**< E1 basic frame, FAS/NFAS framing 
    cAtPdhE1MFCrc           /**< E1 Multi-Frame with CRC4 
    }eAtPdhDe1FrameType;
*/
int Udr_ce1_port_framer_mode_set (uint8 cardId, uint32 port, void *arg)
{
	int ret=0;
	eAtPdhDe1FrameType EncapTypeBuf = *(eAtPdhDe1FrameType *)arg;
	AtModulePdh encapModule;
	uint8 deviceNum=1,slotId=cardId;

	if(gui4PdhDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x, port=%d,arg = %x\r\n",cardId, port,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((port>32) || (port<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error port=%d\r\n",port);
		return -1;
	}

	if((EncapTypeBuf<cAtPdhDe1FrameUnknown)||(EncapTypeBuf>cAtPdhE1MFCrc))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error arg=%d\r\n",EncapTypeBuf);
		return -1;
	}


	encapModule = (AtModulePdh)AtFhnPdhModule(slotId, deviceNum);
	ret = AtPdhChannelFrameTypeSet((AtPdhChannel)AtModulePdhDe1Get(encapModule, port-1), EncapTypeBuf);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	return 0;
}


/* CmdPdhDe1TimingSet */
/*
/** @brief Timing modes 
typedef enum eAtTimingMode
    {
    cAtTimingModeUnknown    , /**< Unknown timing mode 
    cAtTimingModeSys        , /**< System timing mode 
    cAtTimingModeLoop       , /**< Loop timing mode 
    cAtTimingModeSdhLineRef , /**< SDH Line 
    cAtTimingModeExt1Ref    , /**< Line EXT #1 timing mode 
    cAtTimingModeExt2Ref    , /**< Line EXT #2 timing mode 
    cAtTimingModeAcr        , /**< ACR timing mode 
    cAtTimingModeDcr        , /**< DCR timing mode 
    cAtTimingModeFree       , /**< Free running mode - using system clock for CDR
                                    source to generate service TX clock
    cAtTimingModeTop        , /**< ToP timing mode 
    cAtTimingModeSlave        /**< Slave timing mode 
    }eAtTimingMode;

eAtTimingMode timeDoden,
uint32 refPort,

*/
int	Udr_ce1_port_clock_mode_set(uint8 cardId, uint32 port, void *arg)
{
	int ret=0;
	tAtTimeModeFhn EncapTypeBuf;
	uint32 refPort;
	AtModulePdh encapModule;
	uint8 deviceNum=1,slotId=cardId;

	if(gui4PdhDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		memcpy(&EncapTypeBuf, arg, sizeof(tAtTimeModeFhn));
		printf("cardId=%x,port=%d,TimeModen =%x ref=%d\r\n",cardId,port, EncapTypeBuf.TimeModen, EncapTypeBuf.refPort);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((port>32) || (port<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error port=%d\r\n",port);
		return -1;
	}

	memcpy(&EncapTypeBuf, arg, sizeof(tAtTimeModeFhn));

	if((EncapTypeBuf.refPort>32) || (EncapTypeBuf.refPort<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error port=%d,refPort=%d\r\n",port,EncapTypeBuf.refPort);
		return -1;
	}


	if((EncapTypeBuf.TimeModen<cAtTimingModeUnknownFhn)||(EncapTypeBuf.TimeModen>cAtTimingModeSlaveFhn))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error TimeModen=%d\r\n",EncapTypeBuf.TimeModen);
		return -1;
	}

	if(EncapTypeBuf.TimeModen == cAtTimingModeSlaveFhn){
		EncapTypeBuf.TimeModen =  cAtTimingModeSysFhn;
		At_ce1_port_clock_src_set(slotId,EncapTypeBuf.refPort);
	}else if(EncapTypeBuf.TimeModen == cAtTimingModeSysFhn){
		At_ce1_port_clock_src_delete(slotId);
	}

	encapModule = (AtModulePdh)AtFhnPdhModule(slotId, deviceNum);
	
	ret = AtChannelTimingSet((AtChannel)AtModulePdhDe1Get(encapModule, port-1), (eAtTimingMode)(EncapTypeBuf.TimeModen), 
							(AtChannel)AtModulePdhDe1Get(encapModule, EncapTypeBuf.refPort-1));
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	return 0;
}


/*
CmdPdhNxDs0Create
creat ts in CE1
input channel: no use
*/
int Udr_ce1_port_create_port_ts(uint8 cardId, uint32 channel, void *arg)
{
	int i=0,ret=0;
	tAtPdhDs0PortFhn port;
	AtModulePdh encapModule;
	uint8 deviceNum=1,slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		memcpy(&port, arg , sizeof(tAtPdhDs0PortFhn));
		printf("cardId=%x,channel=%d,e1PortId = %x,timeslotId=%x\r\n",cardId,channel,port.e1PortId, port.timeslotBitmap);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>128) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	memcpy(&port, arg , sizeof(tAtPdhDs0PortFhn));

	if((port.e1PortId<1)||(port.e1PortId>32))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error e1PortId=%d timeslotId=%d\r\n",port.e1PortId, port.timeslotBitmap);
		return -1;
	}

	port.timeslotBitmap = port.timeslotBitmap << 1;

	if(AtPdhDe1NxDs0Create((AtPdhDe1)AtFhnDe1Get(slotId, port.e1PortId), port.timeslotBitmap) == NULL)
	{
		return -1;
	}
	return 0;
}


/* 
CmdEncapBindPhy

phyport 	1-32
timeslot  	bitmap:1-32

*/

int Udr_ce1_port_channel_bind_port_ts(uint8 cardId, uint32 channel, void *arg)
{
	int i=0,ret=0;
	tAtPdhDs0PortFhn port;
	AtModuleEncap encapModule;
	AtEncapChannel encapChannel;
	AtPdhDe1 pdhDe1Module;
	uint8 deviceNum=1, slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,e1PortId = %x,timeslotBitmap=%x\r\n",cardId,channel,port.e1PortId, port.timeslotBitmap);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>128) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	memcpy(&port, arg , sizeof(tAtPdhDs0PortFhn));

	if((port.e1PortId<1)||(port.e1PortId>32))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error e1PortId=%d timeslotId=%d\r\n",port.e1PortId, port.timeslotBitmap);
		return -1;
	}

	port.timeslotBitmap = port.timeslotBitmap << 1;

	encapModule = (AtModuleEncap)AtFhnEncapModule(slotId, deviceNum);
	encapChannel = (AtEncapChannel)AtModuleEncapChannelGet(encapModule, channel - 1);
	
	//pdhDe1Module = AtModulePdhDe1Get((AtModulePdh)AtFhnPdhModule(deviceNum),port.e1PortId-1);
	pdhDe1Module = (AtPdhDe1)AtFhnDe1Get(slotId, port.e1PortId);
	ret = AtEncapChannelPhyBind(encapChannel, 
			(AtChannel)AtPdhDe1NxDs0Get(pdhDe1Module, port.timeslotBitmap));			
	if (ret != cAtOk)
	{
		printf("ERROR: The error return = %x\r\n", ret);
		return -1;
	}

	return 0;
	
}


/*
CmdPdhNxDs0Delete
creat ts in CE1
input channel: no use
*/
int Udr_ce1_port_delete_port_ts(uint8 cardId, uint32 channel, void *arg)
{
	int i=0,ret=0;
	tAtPdhDs0PortFhn port;
	AtModuleEncap encapModule;
	AtPdhDe1 pdhDe1Module;
	uint8 deviceNum=1,slotId=cardId;

	if(gui4EncapDebug)
	{
		memcpy(&port, arg , sizeof(tAtPdhDs0PortFhn));
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,e1PortId = %x,timeslotBitmap=%x\r\n",cardId,channel,port.e1PortId, port.timeslotBitmap);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>128) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	memcpy(&port, arg , sizeof(tAtPdhDs0PortFhn));

	if((port.e1PortId<1)||(port.e1PortId>32))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error e1PortId=%d timeslotId=%d\r\n",port.e1PortId, port.timeslotBitmap);
		return -1;
	}

	port.timeslotBitmap = port.timeslotBitmap << 1;

	pdhDe1Module = (AtPdhDe1)AtFhnDe1Get(slotId, port.e1PortId);

	ret = AtPdhDe1NxDs0Delete(pdhDe1Module, (AtPdhNxDS0)AtPdhDe1NxDs0Get(pdhDe1Module, port.timeslotBitmap));
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;
	
}


int Udr_ce1_port_channel_unbind_port_ts(uint8 cardId, uint32 channel, void *arg)
{

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d\r\n",cardId,channel);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>128) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	return Udr_ce1_port_channel_unbind_port(cardId,channel,arg);
}

/*
output:
 *arg = 1   E1 LINK
 *arg = 0   E1 LOS

*/
int Udr_ce1_port_los_status(uint32 port, void *arg)
{

	idt_los_status_get(port, arg);
	return 0;
}

/*
show pdh de1 alarm 1-16
CmdPdhDe1AlarmShow
typedef struct eUdrPdhDe1AlarmTypeFhn
{
    uint8 cUdrAtPdhDe1AlarmLosFhn;  /**< LOS 
    uint8 cUdrAtPdhDe1AlarmLofFhn;  /**< LOF 
    uint8 cUdrAtPdhDe1AlarmAisFhn;  /**< AIS 
    uint8 cUdrAtPdhDe1AlarmRaiFhn;  /**< RAI 
    uint8 cUdrAtPdhDe1AlarmLomfFhn;  /**< LOMF 
    uint8 cUdrAtPdhDe1AlarmSfBerFhn;  /**< SF BER 
    uint8 cUdrAtPdhDe1AlarmSdBerFhn;  /**< SD BER 
    uint8 cUdrAtPdhDe1AlarmSigLofFhn;  /**< LOF on signaling channel 
    uint8 cUdrAtPdhDe1AlarmSigRaiFhn;  /**< RAI on signaling channel 
}eUdrPdhDe1AlarmTypeFhn;
0: clear alarm
1 : have alarm

*/

int Udr_ce1_port_alarm_status(uint8 cardId, uint32 port, void *arg)
{
	uint8 *EncapTypeBuf;
	AtDevice *pDevice;
	eAtRet  ret;
	uint8 slotId=cardId;
	uint32 alarmStat=0,i=0;
	
	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,port=%d,arg = %x\r\n",cardId,port,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((port>16) || (port<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",port);
		return -1;
	}

	EncapTypeBuf = (uint8 *)arg;

	alarmStat = AtChannelAlarmGet((AtChannel)AtFhnDe1Get(slotId,port));
        for (i = 0; i < cUdrPdhDe1NumAlarmFhn; i++)
            {
            if (alarmStat & cCmdPdhDe1AlarmTypeValFhn[i])
                	EncapTypeBuf[i]=1;
            else
			EncapTypeBuf[i]=0;
            }

	return 0;

}


/*
show eth port counters 1 r2c
show encap hdlc counters 1 r2c
show ppp link counters 1 r2c
show eth flow counters 1 r2c
show eth flow 1

*/

/*
show eth port counters 1 r2c
CmdEthPortCounterGet
*/
int Udr_ce1_ethport_counter_get(uint8 cardId, uint32 port, void *arg)
{
    	tAtEthPortCountersFhn *counters = (tAtEthPortCountersFhn *)arg;
	eAtRet  ret;
	uint8 slotId=cardId;
	
	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,port=%d,arg = %x\r\n",cardId,port,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((port>16) || (port<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",port);
		return -1;
	}

	ret = AtChannelAllCountersGet((AtChannel)AtFhnEthPortGet(slotId,port), counters);
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}

int Udr_ce1_ethport_counter_clear(uint8 cardId, uint32 port, void *arg)
{
    	tAtEthPortCountersFhn counters;
	eAtRet  ret;
	uint8 slotId=cardId;
	
	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,port=%d,arg = %x\r\n",cardId,port,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((port>16) || (port<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",port);
		return -1;
	}

	ret = AtChannelAllCountersClear((AtChannel)AtFhnEthPortGet(slotId,port), &counters);
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}


/*
show encap hdlc counters 1 r2c
CmdEncapHdlcCounter
*/

int Udr_ce1_channel_counter_get(uint8 cardId, uint32 channel, void *arg)
{
	tAtHdlcChannelCountersFhn *hdlcCounter = (tAtHdlcChannelCountersFhn *)arg;
	tAtHdlcChannelCounters hdlcCounterbuf={0};
	eAtRet  ret;
	uint8 slotId=cardId;
	
	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,arg = %x\r\n",cardId,channel,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>128) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	/* use it to get ppp link counter */
	/*  ret = AtChannelAllCountersGet((AtChannel)AtFhnHdlcLinkGet(fpgaID,channel), hdlcCounter); */
	ret = AtChannelAllCountersGet((AtChannel)AtFhnHdlcChannelGet(slotId,1,channel), &hdlcCounterbuf);
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	hdlcCounter->txGoodPktFhn = hdlcCounterbuf.txGoodPkt;
	hdlcCounter->txAbortPktFhn= hdlcCounterbuf.txAbortPkt;	
	hdlcCounter->rxGoodPktFhn = hdlcCounterbuf.rxGoodPkt;
	hdlcCounter->rxAbortPktFhn= hdlcCounterbuf.rxAbortPkt;	
	hdlcCounter->rxFcsErrPktFhn = hdlcCounterbuf.rxFcsErrPkt;
	hdlcCounter->rxAddrCtrlErrPktFhn= hdlcCounterbuf.rxAddrCtrlErrPkt;	
	hdlcCounter->rxSapiErrPktFhn = hdlcCounterbuf.rxSapiErrPkt;
	hdlcCounter->rxErrPktFhn= hdlcCounterbuf.rxErrPkt;	
	
	return 0;

}


int Udr_ce1_channel_counter_clear(uint8 cardId,uint32 channel, void *arg)
{
	tAtHdlcChannelCounters hdlcCounter;
	eAtRet  ret;
	uint8 slotId=cardId;
	
	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d channel=%d\r\n",cardId,channel);
	}


	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>128) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	ret = AtChannelAllCountersClear((AtChannel)AtFhnHdlcChannelGet(slotId,1,channel), &hdlcCounter);
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}


int Udr_ce1_mlppp_bundle_create(uint8 cardId,uint32 bundleId, void *arg)
{
	int ret;
	AtModulePpp pppModule;
	uint8 deviceNum=1,slotId=cardId;
	
	if(gui4MLPPPDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,arg = %x\r\n",cardId,bundleId,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((bundleId>AT_POS_E1_MAX_MLPPP_BUNDLE_FHN) || (bundleId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",bundleId);
		return -1;
	}

	
	pppModule = (AtModulePpp)AtFhnPppModule(slotId, deviceNum);

	if(AtModulePppMpBundleCreate(pppModule, bundleId-1) == NULL)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return -1;
	}

	return 0;

}

int Udr_ce1_mlppp_bundle_Delete(uint8 cardId, uint32 bundleId)
{
	int ret=0;
	AtModulePpp pppModule;
	uint8 deviceNum=1,slotId=cardId;
	
	if(gui4MLPPPDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,slotId = %x\r\n",cardId,bundleId,slotId);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((bundleId>AT_POS_E1_MAX_MLPPP_BUNDLE_FHN) || (bundleId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",bundleId);
		return -1;
	}

	pppModule = (AtModulePpp)AtFhnPppModule(slotId, deviceNum);

	ret = AtModulePppMpBundleDelete(pppModule, bundleId-1);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}

/*
CmdPppBundleEnable

bundleId = 1-128
0:disable
1:enable
*/


int Udr_ce1_mlppp_bundle_enable(uint8 cardId, uint32 bundleId, void *arg)
{
	uint32 enable= *(uint32*)arg;
	int ret;
	AtMpBundle MpppModule;
	uint8 deviceNum=1,slotId=cardId;

	if(gui4MLPPPDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,arg = %x\r\n",cardId,bundleId,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((bundleId>AT_POS_E1_MAX_MLPPP_BUNDLE_FHN) || (bundleId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",bundleId);
		return -1;
	}


	MpppModule = (AtMpBundle)AtFhnMpBundleGet(slotId,1,bundleId);

	ret = AtChannelEnable((AtChannel)MpppModule, enable);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%d\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
		return -1;
	}

	return 0;

}


/* CmdPppBundleAddLink */
int Udr_ce1_mlppp_bundle_add_channel(uint8 cardId, uint32 bundleId, uint32 channel)
{
	int ret;
	AtHdlcBundle hdlcModule;
	AtHdlcLink hdlcLinkModule;
	uint8 deviceNum=1,slotId=cardId;
	
	if(gui4MLPPPDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,bundleId=%x\r\n",cardId,channel, bundleId);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((bundleId>AT_POS_E1_MAX_MLPPP_BUNDLE_FHN) || (bundleId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error bundleId=%d\r\n",bundleId);
		return -1;
	}

	hdlcModule = (AtHdlcBundle)AtFhnMpBundleGet(slotId,1,bundleId);

	hdlcLinkModule = (AtHdlcLink)AtFhnHdlcLinkGet(slotId,1,channel);

	ret = AtHdlcBundleLinkAdd(hdlcModule, hdlcLinkModule);
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%d\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
		return -1;
	}

	return 0;


}

/* CmdPppBundleAddLink */
int Udr_ce1_mlppp_bundle_delete_channel(uint8 cardId, uint32 bundleId, uint32 channel)
{
	int ret;
	AtHdlcBundle hdlcModule;
	AtHdlcLink hdlcLinkModule;
	uint8 deviceNum=1,slotId=cardId;
	
	if(gui4MLPPPDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,bundleId=%x\r\n",cardId,channel, bundleId);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((bundleId>AT_POS_E1_MAX_MLPPP_BUNDLE_FHN) || (bundleId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",bundleId);
		return -1;
	}

	hdlcModule = (AtHdlcBundle)AtFhnMpBundleGet(slotId,1,bundleId);

	hdlcLinkModule = (AtHdlcLink)AtFhnHdlcLinkGet(slotId,1,channel);

	ret = AtHdlcBundleLinkRemove(hdlcModule, hdlcLinkModule);
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;


}

/* CmdPppBundleAddFlow */
int Udr_ce1_mlppp_bundle_bind_flow(uint8 cardId, uint32 bundleId, uint32 flow)
{
	int ret;
	AtMpBundle MpbundleModule;
	AtEthFlow flowModule;
	uint8 deviceNum=1,slotId=cardId;

	
	if(gui4MLPPPDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,flow=%d,bundleId=%x\r\n",cardId,flow, bundleId);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((bundleId>AT_POS_E1_MAX_MLPPP_BUNDLE_FHN) || (bundleId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",bundleId);
		return -1;
	}

	MpbundleModule = (AtMpBundle)AtFhnMpBundleGet(slotId,1,bundleId);

	flowModule = (AtEthFlow)AtFhnEthFlowGet(slotId,1,flow);

	ret = AtMpBundleFlowAdd(MpbundleModule, flowModule, 0);
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%d\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
		return -1;
	}

	return 0;
}

/* CmdPppBundleAddFlow */
int Udr_ce1_mlppp_bundle_unbind_flow(uint8 cardId, uint32 bundleId, uint32 flow)
{
	int ret;
	AtMpBundle MpbundleModule;
	AtEthFlow flowModule;
	uint8 deviceNum=1,slotId=cardId;

	
	if(gui4MLPPPDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,flow=%d,bundleId=%x\r\n",cardId,flow, bundleId);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((bundleId>AT_POS_E1_MAX_MLPPP_BUNDLE_FHN) || (bundleId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",bundleId);
		return -1;
	}

	MpbundleModule = (AtMpBundle)AtFhnMpBundleGet(slotId,1,bundleId);

	flowModule = (AtEthFlow)AtFhnEthFlowGet(slotId,1,flow);

	ret = AtMpBundleFlowRemove(MpbundleModule, flowModule);
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;
}


/*
1:enable
0:diable
*/
int Udr_ce1_port_oam_channel_mode(uint8 cardId, uint32 channel)
{
	uint32 fpgaID=1;
	AtModuleEncap encapModule;
	AtHdlcLink link;
	AtHdlcChannel  hdlcChannel;
	eAtRet  ret;
	uint8 slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("card=%x,fpga=%x,channel=%x\r\n",cardId,fpgaID,channel);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>AT_POS_E1_MAX_HW_CHANNEL_FHN) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	if((fpgaID<1)||(fpgaID>2)){
		printf("%s,#%d,%s,error fpga=%x\r\n",__FILE__,__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}


	/* enable this channle */
	ret = AtHdlcLinkOamPacketModeSet((AtHdlcLink)AtFhnHdlcLinkGet(slotId,fpgaID, channel), cAtHdlcLinkOamModeToPsn);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));	
		return -1;
	}

	return 0;

}

int Udr_ce1_port_oam_channel_egress_dmac_set(uint8 cardId, uint32 channel, void *arg)
{
	uint32 fpgaID=1;
	uint8 mac[cAtMacAddressLen];
	AtEthFlow flow;
	AtModuleEth ethModule;
	eAtRet ret = cAtOk;
	uint8 slotId=cardId;

	if(gui4AtFlowDebug){
		printf("%s,#%d,%s cardId=%x,fpgaID=%x\r\n",__FILE__,__LINE__,__FUNCTION__,cardId,fpgaID);
		memcpy(mac, (char *)arg,6);
		printf("channel=%d,MAC =%x:%x:%x:%x:%x:%x \r\n",channel,mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>AT_POS_E1_MAX_HW_CHANNEL_FHN) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	memcpy(mac, (char *)arg,6);

	/* Get flow Module */
	if((fpgaID==1)||(fpgaID==2)){
		/* get flow */
		flow = AtHdlcLinkOamFlowGet((AtHdlcLink)AtFhnHdlcLinkGet(slotId,fpgaID, channel));
		if (flow != NULL){
            		ret = AtEthFlowEgressDestMacSet(flow, mac);
			if (ret != cAtOk){
				printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
				return -1;
			}
	        }else{
	        	printf("#%d,%s,Flow=%d,slotId=%d,fpgaID=%d,channel=%d\r\n",__LINE__,__FUNCTION__,flow,slotId,fpgaID, channel);
	              return -1;
	        }
	}else{
		printf("%s,#%d,%s,error fpga=%x\r\n",__FILE__,__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}

    	return 0;
}


int Udr_ce1_port_oam_channel_egress_vlan_set(uint8 cardId, uint32 ethPort, uint32 channel, void *arg)
{
	uint32 fpgaID=1;
	tAtEthVlanTag *vlanTag = arg;
	AtEthFlow flow;
	tAtEthVlanDesc desc;
	AtModuleEth ethModule;
	eAtRet ret = cAtOk;
	uint8 slotId=cardId;

	if(gui4AtFlowDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("card=%x,fpga=%x,ethport=%x,channel=%x,arg=%x\r\n",cardId,fpgaID,ethPort,channel,*(uint32 *)arg);
	}


	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}
	
	if((ethPort>2) || (ethPort<0)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error ethport=%d\r\n",ethPort);
		return -1;
	}

	if((channel>AT_POS_E1_MAX_HW_CHANNEL_FHN) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	/* create vlan desc */
	AtEthVlanTagConstruct(vlanTag->priority, vlanTag->cfi, vlanTag->vlanId, &desc.vlans[0]);
	desc.numberOfVlans=1;
	desc.ethPortId = ethPort - 1;

	/* Get flow Module */
	if((fpgaID==1)||(fpgaID==2)){
		/* get flow */
		flow = AtHdlcLinkOamFlowGet((AtHdlcLink)AtFhnHdlcLinkGet(slotId,fpgaID, channel));
		if (flow != NULL){
            		ret = AtEthFlowEgressVlanSet(flow, &desc);
			if (ret != cAtOk){
				printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
				return -1;
			}
	        }else{
	        	printf("#%d,%s,Flow=%d,slotId=%d,fpgaID=%d,channel=%d\r\n",__LINE__,__FUNCTION__,flow,slotId,fpgaID, channel);
	              return -1;
	        }
	}else{
		printf("%s,#%d,%s,error fpga=%x\r\n",__FILE__,__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}
	
	return 0;
}

int Udr_ce1_port_oam_channel_ingress_vlan_set(uint8 cardId, uint32 ethPort, uint32 channel, void *arg)
{
	uint32 fpgaID=1;
	tAtEthVlanTag *vlanTag = arg;
	AtEthFlow flow;
	tAtEthVlanDesc desc;
	AtModuleEth ethModule;
	eAtRet ret = cAtOk;
	uint8 slotId=cardId;

	if(gui4AtFlowDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("card=%x,fpga=%x,ethport=%x,channel=%x,arg=%x\r\n",cardId,fpgaID,ethPort,channel,*(uint32 *)arg);
	}


	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}
	
	if((ethPort>2) || (ethPort<0)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error ethport=%d\r\n",ethPort);
		return -1;
	}

	if((channel>AT_POS_E1_MAX_HW_CHANNEL_FHN) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	/* create vlan desc */
	AtEthVlanTagConstruct(vlanTag->priority, vlanTag->cfi, vlanTag->vlanId, &desc.vlans[0]);
	desc.numberOfVlans=1;
	desc.ethPortId = ethPort - 1;

	/* Get flow Module */
	if((fpgaID==1)||(fpgaID==2)){
		/* get flow */
		flow = AtHdlcLinkOamFlowGet((AtHdlcLink)AtFhnHdlcLinkGet(slotId,fpgaID, channel));
		if (flow != NULL){
            		ret = AtEthFlowIngressVlanAdd(flow, &desc);
			if (ret != cAtOk){
				printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
				return -1;
			}
	        }else{
	        	printf("#%d,%s,Flow=%d,slotId=%d,fpgaID=%d,channel=%d\r\n",__LINE__,__FUNCTION__,flow,slotId,fpgaID, channel);
	              return -1;
	        }
	}else{
		printf("%s,#%d,%s,error fpga=%x\r\n",__FILE__,__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}
	
	return 0;
}

int udr_ce1_trunk_acl_add(uint8 cardId, uint32 ce1Port, uint32 logicalChannel, uint32 trunkId, uint8 *dmac, 
								tAtEthVlanTagFhn *ouputvlan,tAtEthVlanTagFhn *inputvlan)
{
	uint32 arg;
	tAtPdhDs0PortFhn E1TsPort={0};
	int ret=0;
	uint32 channelId=0,encapType=0,flowId=0,timeslotMap=0;
	uint32 port = ce1Port;

	if((cardId>2) || (cardId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cardId=%d\r\n",cardId);
		return -1;
	}

	if((trunkId>AT_POS_E1_MAX_MLPPP_BUNDLE_FHN) || (trunkId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error trunkId=%d\r\n",trunkId);
		return -1;
	}

	if(ce1Port<1||ce1Port>16)
	{
		printf("#%d,%s,error ce1Port=%d\r\n",__LINE__,__FUNCTION__,ce1Port);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardType8E1){
		port = port+8;
		if((port>32) || (port<1))
		{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	if(At_fhn_pos_e1_soft_channel_get( cardId,  port,  logicalChannel, &timeslotMap, &channelId, &encapType) == -1){
		return -1;
	}

	flowId = AT_POS_E1_MAX_HW_CHANNEL_FHN+trunkId;
	arg=0;
	ret = Udr_ce1_port_flow_create(cardId,flowId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = Udr_ce1_port_flow_egress_dmac_set(cardId,flowId,dmac);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = Udr_ce1_port_flow_egress_vlan_set(cardId,1,flowId, ouputvlan);/*��������ʾ�ĳ���VLAN  */
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = Udr_ce1_port_flow_set_vlan(cardId, 1, flowId, inputvlan);/*����ʾ�ķ������,ֻ�ܻ���VLAN */
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = Udr_ce1_mlppp_bundle_bind_flow(cardId,trunkId,flowId);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}


	return ret;
}


int udr_ce1_trunk_acl_del(uint8 cardId, uint32 ce1Port, uint32 logicalChannel, uint32 trunkId)
{
	uint32 arg;
	tAtPdhDs0PortFhn E1TsPort={0};
	int ret=0;
	uint32 channelId=0,encapType=0,flowId=0,timeslotMap=0;

	flowId = AT_POS_E1_MAX_HW_CHANNEL_FHN+trunkId;
	ret = Udr_ce1_mlppp_bundle_unbind_flow(cardId,trunkId,flowId);/*��ҵ��ͨ������ID*/
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	encapType = 0;/*cAtEthFlowNpoPpp*/
	ret = Udr_ce1_port_flow_del(cardId,flowId,&encapType);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}


	return ret;
}


#if	0
#endif

/*
CmdSdhMapSet
channel = 1-63

sdh map aug1.1.1 vc4
sdh map vc4.1.1 3xtug3s
sdh map tug3.1.1.1 7xtug2s
sdh map tug2.1.1.1.1 tu12
sdh map vc1x.1.1.1.1.1 de1

*/
int Udr_cpos_port_stm1_sdh_map(uint8 cardId, uint32 cposPort, uint32 cposPortChannel)
{
	int ret=0,aug1Id=0;
	tAtchannelMapToSdh mapBuf;
	tAtEthVlanTagFhn vlanTag;
	uint32 cposPortID=cposPort;
	uint8 slotId=cardId;

	if(gui4AtSDHMapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,cposPort=%d,cposPortChannel=%x\r\n",cardId,cposPort, cposPortChannel);
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}
	
	if((cposPortChannel>63*4) || (cposPortChannel<1)){
		printf("%s,#%d,%s error channel=%d\r\n",__FILE__,__LINE__,__FUNCTION__,cposPortChannel);
		return -1;
	}

	aug1Id = ((cposPortChannel-1)/63) + 1; 
	
	memcpy(&mapBuf, &(gsChannelMapToSdh[cposPortChannel-63*(aug1Id-1)]), sizeof(tAtchannelMapToSdh));
	//printf("aug1Id=%d,au3Tug3Id=%d, tug2Id=%d, tuId=%d\r\n",aug1Id,mapBuf.au3Tug3Id,mapBuf.tug2Id,mapBuf.tuId);
	
 	ret = At_cpos_port_stm_sdh_map_aug1_to_vc4(slotId, cposPortID, aug1Id, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	if(ret != cAtOk){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return -1;
	}

 	ret = At_cpos_port_stm_sdh_map_vc4_to_tug3(slotId, cposPortID, aug1Id, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	if(ret != cAtOk){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return -1;
	}


	//printf("map(%d,%d,%d,%d,%d,%d)\r\n",slotId, cposPortID, aug1Id, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	ret = At_cpos_port_stm_sdh_map_tug3_to_tug2(slotId, cposPortID, aug1Id, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	if(ret != cAtOk){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return -1;
	}
	
	
  	ret = At_cpos_port_stm_sdh_map_tug2_to_tu12(slotId, cposPortID, aug1Id, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	if(ret != cAtOk){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return -1;
	}

	 ret = At_cpos_port_stm_sdh_map_vc1x_to_de1(slotId, cposPortID, aug1Id, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	if(ret != cAtOk){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return -1;
	}
	
	return 0;
}



/*
CmdSdhMapSet
channel = 1-63*4

sdh map aug4.1.1 4xaug1s
sdh map aug1.1.1 vc4
sdh map vc4.1.1 3xtug3s
sdh map tug3.1.1.1 7xtug2s
sdh map tug2.1.1.1.1 tu12
sdh map vc1x.1.1.1.1.1 de1

*/
int Udr_cpos_port_stm4_sdh_map(uint8 cardId, uint32 cposPort, uint32 cposPortChannel)
{
	int ret=0,aug1Id=0;
	tAtchannelMapToSdh mapBuf;
	tAtEthVlanTagFhn vlanTag;
	uint32 cposPortID=cposPort;
	uint8 slotId=cardId;

	if(gui4AtSDHMapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,cposPort=%d,cposPortChannel=%x\r\n",cardId,cposPort, cposPortChannel);
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPortChannel>63*4) || (cposPortChannel<1)){
		printf("%s,#%d,%s,error channel=%d\r\n",__FILE__,__LINE__,__FUNCTION__,cposPortChannel);
		printf("error channel=%d\r\n",cposPortChannel);
		return -1;
	}

	aug1Id = ((cposPortChannel-1)/63) + 1; 
	
	memcpy(&mapBuf, &(gsChannelMapToSdh[cposPortChannel-63*(aug1Id-1)]), sizeof(tAtchannelMapToSdh));
	//printf("aug1Id=%d,au3Tug3Id=%d, tug2Id=%d, tuId=%d\r\n",aug1Id,mapBuf.au3Tug3Id,mapBuf.tug2Id,mapBuf.tuId);

	ret = At_cpos_port_stm_sdh_map_aug4_to_aug1(slotId, cposPortID, 1, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	if(ret != cAtOk){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return -1;
	}
	
 	ret = At_cpos_port_stm_sdh_map_aug1_to_vc4(slotId, cposPortID, aug1Id, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	if(ret != cAtOk){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return -1;
	}

 	ret = At_cpos_port_stm_sdh_map_vc4_to_tug3(slotId, cposPortID, aug1Id, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	if(ret != cAtOk){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return -1;
	}

  	ret = At_cpos_port_stm_sdh_map_tug3_to_tug2(slotId, cposPortID, aug1Id, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	if(ret != cAtOk){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return -1;
	}
	
  	ret = At_cpos_port_stm_sdh_map_tug2_to_tu12(slotId, cposPortID, aug1Id, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	if(ret != cAtOk){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return -1;
	}

	 ret = At_cpos_port_stm_sdh_map_vc1x_to_de1(slotId, cposPortID, aug1Id, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	if(ret != cAtOk){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return -1;
	}
	
	return 0;
}


/*
CmdSdhMapSet
cposPortChannel = 1-12 e3

sdh map aug4.1.1 4xaug1s
sdh map aug1.1.1-1.4 vc4
sdh map vc4.1.1-1.4 3xtug3s
sdh map tug3.1.1.1-1.4.3 vc3
sdh map vc3.1.1.1-1.4.3 de3
*/
int Udr_cpos_port_stm4_e3_sdh_map(uint8 cardId, uint32 cposPort)
{
	int ret=0,aug1Id=0,tug3Id=0,vc3Id=0;
	int au3Tug3Id=0,tug2Id=0,tuId=0;
	tAtchannelMapToSdh mapBuf;
	tAtEthVlanTagFhn vlanTag;
	uint32 cposPortID=cposPort;
	uint8 slotId=cardId;

	if(gui4AtSDHMapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,cposPort=%d\r\n",cardId,cposPort);
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	for(aug1Id=1; aug1Id<5; aug1Id++){
		for(tug3Id=1; tug3Id<4; tug3Id++){	
			ret = At_cpos_port_stm_sdh_map_aug4_to_aug1(slotId, cposPortID, 1, au3Tug3Id, tug2Id, tuId);
			if(ret != cAtOk){
				printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
				return -1;
			}
			
		 	ret = At_cpos_port_stm_sdh_map_aug1_to_vc4(slotId, cposPortID, aug1Id, au3Tug3Id, tug2Id, tuId);
			if(ret != cAtOk){
				printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
				return -1;
			}

		 	ret = At_cpos_port_stm_sdh_map_vc4_to_tug3(slotId, cposPortID, aug1Id, au3Tug3Id, tug2Id, tuId);
			if(ret != cAtOk){
				printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
				return -1;
			}

		  	ret = At_cpos_port_stm_sdh_map_tug3_to_vc3(slotId, cposPortID, aug1Id, tug3Id);
			if(ret != cAtOk){
				printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
				return -1;
			}

			 ret = At_cpos_port_stm_sdh_map_vc3_to_de3(slotId, cposPortID, aug1Id, tug3Id);
			if(ret != cAtOk){
				printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
				return -1;
			}
		}
	}
	return 0;
}

int udr_if_stm_pos_j0_mode(uint8 cardId, uint32 cposPort, tAtSdhTtiFhn *joModen)
{
	int ret=0;
	uint32 cposPortChannel=0;

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	ret = Udr_cpos_port_tti_tx_set( cardId,  cposPort,  joModen);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}


	ret = Udr_cpos_port_tti_rx_set( cardId,  cposPort,  joModen);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}			

	return 0;
}


/*
HW_IF_POSFLAGJ1MODE
HW_IF_POSFLAGJ1
CmdSdhLineTxTti

typedef enum eAtSdhTtiMode
    {
    cAtSdhTtiMode1Byte,  /**< 1-byte 
    cAtSdhTtiMode16Byte, /**< 16-bytes 
    cAtSdhTtiMode64Byte  /**< 64-bytes 
    }eAtSdhTtiMode;



typedef struct tAtSdhTtiFhn{
	eAtSdhTtiMode mode;                       
	uint8 message[cAtSdhChannelMaxTtiLength];
	int8 paddingMode;/* 0: null_padding  1: space_padding
}tAtSdhTtiFhn;

show sdh path tti receive 1
*/
int udr_if_stm_pos_j1_mode(uint8 cardId, uint32 cposPort, tAtSdhTtiFhn *joModen)
{
	int ret=0;
	uint32 cposPortChannel=0;

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}			

	if((gui4AtslotCardType[cardId] == cAtCardTypeline4STM)||(gui4AtslotCardType[cardId] == cAtCardTypeline8STM)){
		/*ret = Udr_cpos_port_path_tti_tx_set( cardId,  cposPort,  joModen);*/
		ret = Udr_cpos_port_path_vc4_tti_tx_set( cardId,  cposPort,  joModen);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}
		
		/*ret = Udr_cpos_port_path_tti_rx_set( cardId,  cposPort,  j0Moden);*/
		ret = Udr_cpos_port_path_vc4_tti_rx_set( cardId,  cposPort,  joModen);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}			
	}else{
		ret = Udr_cpos_port_path_vc4c_tti_tx_set( cardId,  cposPort,  joModen);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}
		
		ret = Udr_cpos_port_path_vc4c_tti_rx_set( cardId,  cposPort,  joModen);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

	}

	return 0;
}


int udr_if_stm_pos_c2_flag(uint8 cardId, uint32 cposPort, uint8 *c2str)
{
	int ret=0;
	uint32 cposPortChannel=0;

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}			

	if((gui4AtslotCardType[cardId] == cAtCardTypeline4STM)||(gui4AtslotCardType[cardId] == cAtCardTypeline8STM)){
		ret = Udr_cpos_port_path_vc4_psl_tx_set( cardId,  cposPort,  c2str);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = Udr_cpos_port_path_vc4_psl_rx_set( cardId,  cposPort,  c2str);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}
	}else{
		ret = Udr_cpos_port_path_vc4c_psl_tx_set( cardId,  cposPort,  c2str);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}

		ret = Udr_cpos_port_path_vc4c_psl_rx_set( cardId,  cposPort,  c2str);
		if(ret !=0){
			printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
			return -1;
		}
	}


	return 0;
}


/*
CmdEthFlowCreate
fpgaID =[1,2]
cpos_card_fpga_select
flowId = 1-128
arg:
    cAtEthFlowNpoPpp,
    cAtEthFlowEthoPpp
*/

int Udr_cpos_port_flow_create(uint8 cardId, uint32 fpgaID, uint32 flowId, void *arg)
{
	AtEthFlow flow;
	eAtEthFlowTypeFhn EncapTypeBuf = *(eAtEthFlowTypeFhn *)arg;
	AtModuleEth ethModule;
	uint8 slotId=cardId;
	
	if(gui4AtFlowDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("channel=%d,arg = %x\r\n",flowId,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((flowId>AT_POS_STM_MAX_FLOW_FHN) || (flowId<1)){
		printf("%s,#%d,%s,error flow=%d\r\n",__FILE__,__LINE__,__FUNCTION__,flowId);
		return -1;
	}

	if((EncapTypeBuf<cAtEthFlowIpoPppFhn)||(EncapTypeBuf>cAtEthFlowEthoPppFhn)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error arg=%d\r\n",EncapTypeBuf);
		return -1;
	}

	if((fpgaID==1)||(fpgaID==2)){
		ethModule = (AtModuleEth)AtFhnEthModule(slotId, fpgaID);
	}else{
		printf("%s,#%d,%s,error fpga=%x\r\n",__FILE__,__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}

	if(EncapTypeBuf == cAtEthFlowIpoPppFhn){
		flow = AtModuleEthNopFlowCreate(ethModule, flowId - 1);
	}else{
		flow = AtModuleEthEopFlowCreate(ethModule, flowId - 1);
	}
	
	return flow ? 0 : -1;

}

int Udr_cpos_port_flow_del(uint8 cardId, uint32 fpgaID,uint32 flowId, void *arg)
{
    	eAtRet ret = cAtOk;
	AtEthFlow flow;
	eAtEthFlowTypeFhn EncapTypeBuf = *(eAtEthFlowTypeFhn *)arg;
	AtModuleEth ethModule;
	uint8 slotId=cardId;

	if(gui4AtFlowDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("channel=%d,arg = %x\r\n",flowId,*(uint32 *)arg);
	}
	
	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((flowId>AT_POS_STM_MAX_FLOW_FHN) || (flowId<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",flowId);
		return -1;
	}

	if((EncapTypeBuf<cAtEthFlowIpoPppFhn)||(EncapTypeBuf>cAtEthFlowEthoPppFhn)){
		printf("%s,#%d,%s,error arg=%d\r\n",__FILE__,__LINE__,__FUNCTION__,EncapTypeBuf);
		return -1;
	}

	if((fpgaID==1)||(fpgaID==2)){
		ethModule = (AtModuleEth)AtFhnEthModule(slotId, fpgaID);
	}else{
		printf("%s,#%d,%s,error fpga=%x\r\n",__FILE__,__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}

        /* delete flow */
	ret = AtModuleEthFlowDelete(ethModule, flowId - 1);
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;
}

/*
CmdEthFlowEgDestMacSet
flowId = 1-128
arg:
	dest mac    
CmdEthFlowEgDestMacSet
*/
int Udr_cpos_port_flow_egress_dmac_set(uint8 cardId, uint32 fpgaID, uint32 flowId, void *arg)
{
	uint8 mac[cAtMacAddressLen];
	AtEthFlow flow;
	uint32 numFlows = flowId;
	AtModuleEth ethModule;
	eAtRet ret = cAtOk;
	uint8 slotId=cardId;

	if(gui4AtFlowDebug){
		printf("%s,#%d,%s cardId=%x,fpgaID=%x\r\n",__FILE__,__LINE__,__FUNCTION__,cardId,fpgaID);
		memcpy(mac, (char *)arg,6);
		printf("flowId=%d,MAC =%x:%x:%x:%x:%x:%x \r\n",flowId,mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((flowId>AT_POS_STM_MAX_FLOW_FHN) || (flowId<1)){
		printf("%s,#%d,%s error flow=%d\r\n",__FILE__,__LINE__,__FUNCTION__,flowId);
		return -1;
	}

	memcpy(mac, (char *)arg,6);

	/* Get Ethernet Module */
	if((fpgaID==1)||(fpgaID==2)){
		ethModule = (AtModuleEth)AtFhnEthModule(slotId, fpgaID);
	}else{
		printf("%s,#%d,%s,error fpga=%x\r\n",__FILE__,__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}
	
	/* get flow */
	flow = AtModuleEthFlowGet(ethModule, numFlows- 1);

        if (flow != NULL){
            ret |= AtEthFlowEgressDestMacSet(flow, mac);
        }else{
            printf("Flow %d does not exist\r\n", numFlows);
            return -1;
        }

	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

    	return 0;
}

/*
CmdEthFlowEgVlanSet
port = 1-32
flowId = 1-128
arg:
	cfi + vlan    
*/
int Udr_cpos_port_flow_egress_vlan_set(uint8 cardId, uint32 fpgaID, uint32 ethPort, uint32 flowId, void *arg)
{
	tAtEthVlanTag *vlanTag = arg;
	AtEthFlow flow;
	uint32 numFlows = flowId;
	tAtEthVlanDesc desc;
	AtModuleEth ethModule;
	eAtRet ret = cAtOk;
	uint8 slotId=cardId;

	if(gui4AtFlowDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("card=%x,fpga=%x,ethport=%x,flowId=%x,arg=%x\r\n",cardId,fpgaID,ethPort,flowId,*(uint32 *)arg);
	}


	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((ethPort>2) || (ethPort<0)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error ethport=%d\r\n",ethPort);
		return -1;
	}
	
	if((flowId>AT_POS_STM_MAX_FLOW_FHN) || (flowId<1)){
		printf("%s,#%d,%s,errorflow=%d\r\n",__FILE__,__LINE__,__FUNCTION__,flowId);
		return -1;
	}

	/* create vlan desc */
	AtEthVlanTagConstruct(vlanTag->priority, vlanTag->cfi, vlanTag->vlanId, &desc.vlans[0]);
	desc.numberOfVlans=1;
	desc.ethPortId = ethPort - 1;

	/* Get Ethernet Module */
	if((fpgaID==1)||(fpgaID==2)){
		ethModule = (AtModuleEth)AtFhnEthModule(slotId,fpgaID);
	}else{
		printf("%s,#%d,%s,error fpga=%x\r\n",__FILE__,__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}
	
	/* get flow */
	flow = AtModuleEthFlowGet(ethModule, numFlows- 1);

        if (flow != NULL){
            ret |= AtEthFlowEgressVlanSet(flow, &desc);
        }else{
            printf("Flow %d does not exist\r\n", numFlows);
            return -1;
        }
        
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	
	return 0;
}


/*
CmdEthFlowIgVlanAdd
port must 1 
flowId = 1-128
arg:
	cfi + vlan    
*/

int  Udr_cpos_port_flow_set_vlan(uint8 cardId, uint32 fpgaID, uint32 ethPort, uint32 flowId, void *arg)
{
	tAtEthVlanTagFhn *vlanTag = arg;
	AtEthFlow flow;
	uint32 numFlows = flowId;
	tAtEthVlanDesc desc;
	AtModuleEth ethModule;
	eAtRet ret = cAtOk;
	uint8 slotId=cardId;

	if(gui4AtFlowDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("card=%x,fpga=%x,ethport=%x,flowId=%x\r\n",cardId,fpgaID,ethPort,flowId);
		printf("vlanID=%x,pri=%x,CFI=%x\r\n",vlanTag->vlanId,vlanTag->priority,vlanTag->cfi);
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((ethPort>2) || (ethPort<0)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error ethport=%d\r\n",ethPort);
		return -1;
	}

	if((flowId>AT_POS_STM_MAX_FLOW_FHN) || (flowId<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",flowId);
		return -1;
	}


	/* create vlan desc */
	AtEthVlanTagConstruct(vlanTag->priority, vlanTag->cfi, vlanTag->vlanId, &desc.vlans[0]);
	desc.numberOfVlans=1;
	desc.ethPortId =ethPort- 1;

	/* Get Ethernet Module */
	if((fpgaID==1)||(fpgaID==2)){
		ethModule = (AtModuleEth)AtFhnEthModule(slotId, fpgaID);
	}else{
		printf("%s,#%d,%s,error fpga=%x\r\n",__FILE__,__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}
	
	/* get flow */
	flow = AtModuleEthFlowGet(ethModule, numFlows- 1);
	if (flow != NULL){
		ret |= AtEthFlowIngressVlanAdd(flow, &desc);
	}else{
		printf("Flow %d does not exist\r\n", numFlows);
		return -1;
	}
        
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	
	return 0;
}


int  Udr_cpos_port_flow_delete_vlan(uint8 cardId, uint32 fpgaID, uint32 ethPort, uint32 flowId, void *arg)
{
	tAtEthVlanTagFhn *vlanTag = arg;
	AtEthFlow flow;
	uint32 numFlows = flowId;
	tAtEthVlanDesc desc;
	AtModuleEth ethModule;
	eAtRet ret = cAtOk;
	uint8 slotId=cardId;

	if(gui4AtFlowDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("card=%x,fpga=%x,ethport=%x,flowId=%x\r\n",cardId,fpgaID,ethPort,flowId);
		printf("vlanID=%x,pri=%x,CFI=%x\r\n",vlanTag->vlanId,vlanTag->priority,vlanTag->cfi);
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((ethPort>2) || (ethPort<0)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error ethport=%d\r\n",ethPort);
		return -1;
	}

	if((flowId>AT_POS_STM_MAX_FLOW_FHN) || (flowId<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",flowId);
		return -1;
	}


	/* create vlan desc */
	AtEthVlanTagConstruct(vlanTag->priority, vlanTag->cfi, vlanTag->vlanId, &desc.vlans[0]);
	desc.numberOfVlans=1;
	desc.ethPortId =ethPort- 1;

	/* Get Ethernet Module */
	if((fpgaID==1)||(fpgaID==2)){
		ethModule = (AtModuleEth)AtFhnEthModule(slotId, fpgaID);
	}else{
		printf("%s,#%d,%s,error fpga=%x\r\n",__FILE__,__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}
	
	/* get flow */
	flow = AtModuleEthFlowGet(ethModule, numFlows- 1);
	if (flow != NULL){
		ret |= AtEthFlowIngressVlanRemove(flow, &desc);
	}else{
		printf("Flow %d does not exist\r\n", numFlows);
		return -1;
	}
        
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	
	return 0;
}

int Udr_cpos_port_channel_mtu_set(uint8 cardId, uint32 fpgaID, uint32 channel, void *arg)
{
	uint32 mtuVal = *(uint32*)arg;
	AtModuleEncap encapModule;
	int ret=0,aug1Id=0;
	AtEncapChannel encapChannel;
	AtChannel channels;
	uint8 slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("card=%x,fpga=%x,channel=%x,mtuVal=%x\r\n",cardId,fpgaID,channel,mtuVal);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(fpgaID<1||fpgaID>2)
	{
		printf("#%d,%s,error fpgaID=%d\r\n",__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}

	if((channel>AT_POS_STM_MAX_HW_CHANNEL_FHN) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return cAtFalse;
	}

        ret = AtHdlcChannelMtuSet((AtHdlcChannel)AtFhnHdlcChannelGet(slotId,fpgaID,channel), mtuVal);
        if (ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}


int Udr_cpos_port_channel_ppp_enable(uint8 cardId, uint32 fpgaID, uint32 channel, void *arg)
{
    	eAtPppLinkPhase phase = *(eAtPppLinkPhase *)arg;
	AtModuleEncap encapModule;
	AtHdlcLink link;
	AtHdlcChannel  hdlcChannel;
	eAtRet  ret;
	uint8 slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("card=%x,fpga=%x,channel=%x,phase=%x\r\n",cardId,fpgaID,channel,phase);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>AT_POS_STM_MAX_HW_CHANNEL_FHN) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	if((phase<cAtPppLinkPhaseUnknown)||(phase>cAtPppLinkPhaseNetworkActive))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error arg=%d\r\n",phase);
		return -1;
	}

	if((fpgaID==1)||(fpgaID==2)){
		encapModule = (AtModuleEncap)AtFhnEncapModule(slotId, fpgaID);
	}else{
		printf("%s,#%d,%s,error fpga=%x\r\n",__FILE__,__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}

	/* get hdlc channle */
	hdlcChannel = (AtHdlcChannel)AtModuleEncapChannelGet(encapModule, channel - 1);
	if (hdlcChannel == NULL)
	    return 0;


    if (AtEncapChannelEncapTypeGet((AtEncapChannel)hdlcChannel) != cAtEncapHdlc)
        printf("port %d invalid encapsulation type\r\n", channel);
	
    	link = AtHdlcChannelHdlcLinkGet(hdlcChannel) ;

	
	/* enable this channle */
	ret = AtPppLinkPhaseSet((AtPppLink)link, phase);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}

/*
1:enable
0:diable
*/
int Udr_cpos_port_channel_txenable(uint8 cardId, uint32 fpgaID, uint32 channel, void *arg)
{
    	uint32 enable = *(uint32 *)arg;
	AtModuleEncap encapModule;
	AtHdlcLink link;
	AtHdlcChannel  hdlcChannel;
	eAtRet  ret;
	uint8 slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("card=%x,fpga=%x,channel=%x,enable=%x\r\n",cardId,fpgaID,channel,enable);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>AT_POS_STM_MAX_HW_CHANNEL_FHN) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	if((enable<0)||(enable>1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error arg=%d\r\n",enable);
		return -1;
	}

	if((fpgaID==1)||(fpgaID==2)){
		encapModule = (AtModuleEncap)AtFhnEncapModule(slotId, fpgaID);
	}else{
		printf("%s,#%d,%s,error fpga=%x\r\n",__FILE__,__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}

	/* get hdlc channle */
	hdlcChannel = (AtHdlcChannel)AtModuleEncapChannelGet(encapModule, channel - 1);
	if (hdlcChannel == NULL)
	    return 0;


    if (AtEncapChannelEncapTypeGet((AtEncapChannel)hdlcChannel) != cAtEncapHdlc)
        printf("port %d invalid encapsulation type\r\n", channel);
	
    	link = AtHdlcChannelHdlcLinkGet(hdlcChannel) ;

	
	/* enable this channle */
	ret = AtHdlcLinkTxTrafficEnable(link, enable);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));	
		return -1;
	}

	return 0;

}

/*
channel = 1-128

AtHdlcLinkTxTrafficEnable
AtHdlcLinkTxTrafficDisable

0:disable
1:enable

*/
eAtRet Udr_cpos_port_channel_rxenable(uint8 cardId, uint32 fpgaID, uint32 channel, void *arg)
{
    	uint32 enable = *(uint32 *)arg;
	AtModuleEncap encapModule;
	AtHdlcLink link;
	AtHdlcChannel  hdlcChannel;
	eAtRet  ret;
	uint8 slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("card=%x,fpga=%x,channel=%x,enable=%x\r\n",cardId,fpgaID,channel,enable);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}


	if((channel>AT_POS_STM_MAX_HW_CHANNEL_FHN) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error port=%d\r\n",channel);
		return -1;
	}

	if((enable<0)||(enable>1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error arg=%d\r\n",enable);
		return -1;
	}

	if((fpgaID==1)||(fpgaID==2)){
		encapModule = (AtModuleEncap)AtFhnEncapModule(slotId, fpgaID);
	}else{
		printf("%s,#%d,%s,error fpga=%x\r\n",__FILE__,__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}

	/* get hdlc channle */
	hdlcChannel = (AtHdlcChannel)AtModuleEncapChannelGet(encapModule, channel - 1);
	if (hdlcChannel == NULL)
	    return -1;


    if (AtEncapChannelEncapTypeGet((AtEncapChannel)hdlcChannel) != cAtEncapHdlc)
        printf("port %d invalid encapsulation type\r\n", channel);
	
    	link = AtHdlcChannelHdlcLinkGet(hdlcChannel) ;

	
	/* enable this channle */
	ret = AtHdlcLinkRxTrafficEnable(link, enable);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));	
		return -1;
	}

	return 0;

}

/*
1:enable
0:diable
*/
int Udr_cpos_port_oam_channel_mode(uint8 cardId, uint32 fpgaID, uint32 channel)
{
	AtModuleEncap encapModule;
	AtHdlcLink link;
	AtHdlcChannel  hdlcChannel;
	eAtRet  ret;
	uint8 slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("card=%x,fpga=%x,channel=%x\r\n",cardId,fpgaID,channel);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>AT_POS_STM_MAX_HW_CHANNEL_FHN) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	if((fpgaID<1)||(fpgaID>2)){
		printf("%s,#%d,%s,error fpga=%x\r\n",__FILE__,__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}


	/* enable this channle */
	ret = AtHdlcLinkOamPacketModeSet((AtHdlcLink)AtFhnHdlcLinkGet(slotId,fpgaID, channel), cAtHdlcLinkOamModeToPsn);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));	
		return -1;
	}

	return 0;

}

int Udr_cpos_port_oam_channel_egress_dmac_set(uint8 cardId, uint32 fpgaID, uint32 channel, void *arg)
{
	uint8 mac[cAtMacAddressLen];
	AtEthFlow flow;
	AtModuleEth ethModule;
	eAtRet ret = cAtOk;
	uint8 slotId=cardId;

	if(gui4AtFlowDebug){
		printf("%s,#%d,%s cardId=%x,fpgaID=%x\r\n",__FILE__,__LINE__,__FUNCTION__,cardId,fpgaID);
		memcpy(mac, (char *)arg,6);
		printf("channel=%d,MAC =%x:%x:%x:%x:%x:%x \r\n",channel,mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>AT_POS_STM_MAX_HW_CHANNEL_FHN) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	memcpy(mac, (char *)arg,6);

	/* Get flow Module */
	if((fpgaID==1)||(fpgaID==2)){
		/* get flow */
		flow = AtHdlcLinkOamFlowGet((AtHdlcLink)AtFhnHdlcLinkGet(slotId,fpgaID, channel));
		if (flow != NULL){
            		ret = AtEthFlowEgressDestMacSet(flow, mac);
			if (ret != cAtOk){
				printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
				return -1;
			}
	        }else{
	        	printf("#%d,%s,Flow=%d,slotId=%d,fpgaID=%d,channel=%d\r\n",__LINE__,__FUNCTION__,flow,slotId,fpgaID, channel);
	              return -1;
	        }
	}else{
		printf("%s,#%d,%s,error fpga=%x\r\n",__FILE__,__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}

    	return 0;
}


int Udr_cpos_port_oam_channel_egress_vlan_set(uint8 cardId, uint32 fpgaID, uint32 ethPort, uint32 channel, void *arg)
{
	tAtEthVlanTag *vlanTag = arg;
	AtEthFlow flow;
	tAtEthVlanDesc desc;
	AtModuleEth ethModule;
	eAtRet ret = cAtOk;
	uint8 slotId=cardId;

	if(gui4AtFlowDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("card=%x,fpga=%x,ethport=%x,channel=%x,arg=%x\r\n",cardId,fpgaID,ethPort,channel,*(uint32 *)arg);
	}


	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}
	
	if((ethPort>2) || (ethPort<0)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error ethport=%d\r\n",ethPort);
		return -1;
	}

	if((channel>AT_POS_STM_MAX_HW_CHANNEL_FHN) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	/* create vlan desc */
	AtEthVlanTagConstruct(vlanTag->priority, vlanTag->cfi, vlanTag->vlanId, &desc.vlans[0]);
	desc.numberOfVlans=1;
	desc.ethPortId = ethPort - 1;

	/* Get flow Module */
	if((fpgaID==1)||(fpgaID==2)){
		/* get flow */
		flow = AtHdlcLinkOamFlowGet((AtHdlcLink)AtFhnHdlcLinkGet(slotId,fpgaID, channel));
		if (flow != NULL){
            		ret = AtEthFlowEgressVlanSet(flow, &desc);
			if (ret != cAtOk){
				printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
				return -1;
			}
	        }else{
	        	printf("#%d,%s,Flow=%d,slotId=%d,fpgaID=%d,channel=%d\r\n",__LINE__,__FUNCTION__,flow,slotId,fpgaID, channel);
	              return -1;
	        }
	}else{
		printf("%s,#%d,%s,error fpga=%x\r\n",__FILE__,__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}
	
	return 0;
}

int Udr_cpos_port_oam_channel_ingress_vlan_set(uint8 cardId, uint32 fpgaID, uint32 ethPort, uint32 channel, void *arg)
{
	tAtEthVlanTag *vlanTag = arg;
	AtEthFlow flow;
	tAtEthVlanDesc desc;
	AtModuleEth ethModule;
	eAtRet ret = cAtOk;
	uint8 slotId=cardId;

	if(gui4AtFlowDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("card=%x,fpga=%x,ethport=%x,channel=%x,arg=%x\r\n",cardId,fpgaID,ethPort,channel,*(uint32 *)arg);
	}


	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}
	
	if((ethPort>2) || (ethPort<0)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error ethport=%d\r\n",ethPort);
		return -1;
	}

	if((channel>AT_POS_STM_MAX_HW_CHANNEL_FHN) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	/* create vlan desc */
	AtEthVlanTagConstruct(vlanTag->priority, vlanTag->cfi, vlanTag->vlanId, &desc.vlans[0]);
	desc.numberOfVlans=1;
	desc.ethPortId = ethPort - 1;

	/* Get flow Module */
	if((fpgaID==1)||(fpgaID==2)){
		/* get flow */
		flow = AtHdlcLinkOamFlowGet((AtHdlcLink)AtFhnHdlcLinkGet(slotId,fpgaID, channel));
		if (flow != NULL){
            		ret = AtEthFlowIngressVlanAdd(flow, &desc);
			if (ret != cAtOk){
				printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
				return -1;
			}
	        }else{
	        	printf("#%d,%s,Flow=%d,slotId=%d,fpgaID=%d,channel=%d\r\n",__LINE__,__FUNCTION__,flow,slotId,fpgaID, channel);
	              return -1;
	        }
	}else{
		printf("%s,#%d,%s,error fpga=%x\r\n",__FILE__,__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}
	
	return 0;
}

#if	0
int  Udr_cpos_port_aom_channel_set_vlan(uint8 cardId, uint32 fpgaID, uint32 ethPort, uint32 channel, void *arg)
{
	tAtEthVlanTagFhn *vlanTag = arg;
	AtEthFlow flow;
	tAtEthVlanDesc desc;
	AtModuleEth ethModule;
	eAtRet ret = cAtOk;
	uint8 slotId=cardId;

	if(gui4AtFlowDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("card=%x,fpga=%x,ethport=%x,channel=%x\r\n",cardId,fpgaID,ethPort,channel);
		printf("vlanID=%x,pri=%x,CFI=%x\r\n",vlanTag->vlanId,vlanTag->priority,vlanTag->cfi);
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((ethPort>2) || (ethPort<0)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error ethport=%d\r\n",ethPort);
		return -1;
	}

	if((channel>AT_POS_STM_MAX_HW_CHANNEL_FHN) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	/* create vlan desc */
	AtEthVlanTagConstruct(vlanTag->priority, vlanTag->cfi, vlanTag->vlanId, &desc.vlans[0]);
	desc.numberOfVlans=1;
	desc.ethPortId =ethPort- 1;

	/* Get flow Module */
	if((fpgaID==1)||(fpgaID==2)){
		/* get flow */
		flow = AtHdlcLinkOamFlowGet((AtHdlcLink)AtFhnHdlcLinkGet(slotId,fpgaID, channel));
		if (flow != NULL){
            		ret = AtEthFlowIngressVlanAdd(flow, &desc);
			if (ret != cAtOk){
				printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
				return -1;
			}
	        }else{
	            printf("Flow %d does not exist\r\n", flow);
	            return -1;
	        }
	}else{
		printf("%s,#%d,%s,error fpga=%x\r\n",__FILE__,__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}
	
	return 0;
}
#endif



/*
CmdEncapChannelCreate

channelId = 1-512

typedef enum eAtEncapTypeCreate
    {
    cAtHdlcFrmCiscoHdlcCreate,//**< Cisco HDLC 
    cAtHdlcFrmPppCreate,      //**< PPP 
    cAtHdlcFrmFrCreate,       //**< Frame relay 
    cAtEncapAtmCreate         //**< ATM 
    } eAtEncapTypeCreate;
*/


int Udr_cpos_port_channel_create(uint8 cardId, uint32 fpgaID, uint32 channel, void *arg)
{
    	eAtEncapTypeCreateFhn encapType = *(eAtEncapTypeCreateFhn *)arg;
	AtModuleEncap encapModule;
	eAtRet  ret;
	AtEncapChannel encapChannel;
	uint8 slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("card=%x,fpga=%x,channel=%x,encapType=%x\r\n",cardId,fpgaID,channel,encapType);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}


	if((channel>AT_POS_STM_MAX_HW_CHANNEL_FHN) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	if((encapType<cAtHdlcFrmCiscoHdlcCreateFhn)||(encapType>cAtEncapAtmCreateFhn))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error arg=%d\r\n",encapType);
		return -1;
	}

	if((fpgaID==1)||(fpgaID==2)){
		encapModule = (AtModuleEncap)AtFhnEncapModule(slotId, fpgaID);
	}else{
		printf("%s,#%d,%s,error fpga=%x\r\n",__FILE__,__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}


    /* SET */
        switch (encapType)
            {
            case cAtHdlcFrmPppCreateFhn:
                encapChannel = (AtEncapChannel)AtModuleEncapHdlcPppChannelCreate(encapModule, channel - 1);
                break;

            case cAtHdlcFrmFrCreateFhn:
                encapChannel = (AtEncapChannel)AtModuleEncapFrChannelCreate(encapModule, channel - 1);
                break;

            case cAtHdlcFrmCiscoHdlcCreateFhn:
                encapChannel = (AtEncapChannel)AtModuleEncapCiscoHdlcChannelCreate(encapModule, channel - 1);
                break;

            case cAtEncapAtmCreateFhn:
                encapChannel = (AtEncapChannel)AtModuleEncapAtmTcCreate(encapModule, channel - 1);
                break;

            default:
                printf("ERROR: Encap channel type is %x,not support. Expected cisco|ppp|framerelay|atm\r\n",encapType);
                return -1;
            }

        /* Check if channel is created */
        if (encapChannel == NULL)
            {
            printf("ERROR: The channel %d is exists or invalid id\r\n", channel);
		return -1;
            }

	return 0;

}

/*
CmdEncapChannelDelete

channelId = 1-128

*/

int Udr_cpos_port_channel_delete(uint8 cardId, uint32 fpgaID, uint32 channel, void *arg)
    {
    	eAtEncapTypeCreateFhn encapType = *(eAtEncapTypeCreateFhn *)arg;
	AtModuleEncap encapModule;
	eAtRet  ret;
	AtEncapChannel encapChannel;
	uint8 slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("card=%x,fpga=%x,channel=%x,encapType=%x\r\n",cardId,fpgaID,channel,encapType);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>AT_POS_STM_MAX_HW_CHANNEL_FHN) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	if((encapType<cAtHdlcFrmCiscoHdlcCreateFhn)||(encapType>cAtEncapAtmCreateFhn))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error arg=%d\r\n",encapType);
		return -1;
	}

	if((fpgaID==1)||(fpgaID==2)){
		encapModule = (AtModuleEncap)AtFhnEncapModule(slotId, fpgaID);
	}else{
		printf("%s,#%d,%s,error fpga=%x\r\n",__FILE__,__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}

	ret = AtModuleEncapChannelDelete(encapModule, channel - 1);
        /* Check if it is created */
	if (ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

    return 0;

}


/* 
crc16/crc32
typedef enum eAtHdlcFcsModeFhn
    {
    cAtHdlcFcsModeNoFcsFhn, /**< Disable FCS inserting 
    cAtHdlcFcsModeFcs16Fhn, /**< FCS-16 
    cAtHdlcFcsModeFcs32Fhn  /**< FCS-32 
    }eAtHdlcFcsModeFhn;
*/
int Udr_cpos_port_channel_fcs(uint8 cardId, uint32 fpgaID,uint32 channel, void *arg)
{
    	eAtHdlcFcsModeFhn fcsMode = *(eAtHdlcFcsModeFhn *)arg;
	AtModuleEncap encapModule;
	AtChannel channels;
	AtHdlcLink link;
	AtDevice *pDevice;
	AtHdlcChannel  hdlcChannel;
	eAtRet  ret;
	uint8 deviceNum=fpgaID,slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("card=%x,fpga=%x,channel=%x,fcsMode=%x\r\n",cardId,fpgaID,channel,fcsMode);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>AT_POS_STM_MAX_HW_CHANNEL_FHN) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	if((fcsMode<cAtHdlcFcsModeNoFcsFhn)||(fcsMode>cAtHdlcFcsModeFcs32Fhn))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error arg=%d\r\n",fcsMode);
		return -1;
	}

	//pDevice = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), &deviceNum);
	//encapModule = (AtModuleEncap)AtDeviceModuleGet(*pDevice, cAtModuleEncap);

	encapModule = (AtModuleEncap)AtFhnEncapModule(slotId, deviceNum);

	/* get hdlc channle */
	hdlcChannel = (AtHdlcChannel)AtModuleEncapChannelGet(encapModule, channel - 1);
	if (hdlcChannel == NULL)
	    return cAtOk;


    if (AtEncapChannelEncapTypeGet((AtEncapChannel)hdlcChannel) != cAtEncapHdlc)
        printf("port %d invalid encapsulation type\r\n", channel);
	
	/* set  this channle */
	ret = AtHdlcChannelFcsModeSet(hdlcChannel, fcsMode);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	return ret;

}



/*
CmdSdhLineRate
typedef enum eAtSdhLineRateFhn
    {
    cAtSdhLineRateStm1Fhn,    /**< STM-1 
    cAtSdhLineRateStm4Fhn,    /**< STM-4 
    cAtSdhLineRateStm16fhn    /**< STM-16 
    }eAtSdhLineRateFhn;

*/
int Udr_cpos_port_linerate_set(uint8 cardId, uint32 cposPort, void *arg)
{
	eAtSdhLineRateFhn EncapTypeBuf = *(eAtSdhLineRateFhn *)arg;
	eAtSdhLineRate EncapDriverBuf = EncapTypeBuf+1;
	AtSdhLine sdhModuleLine;
	eAtRet  ret;
	AtEncapChannel encapChannel;
	uint8 slotId=cardId;

	if(gui4EncapDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x, cposPort=%d,arg=%x\r\n",cardId,cposPort,EncapTypeBuf);
	}
	
	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	if((EncapTypeBuf<cAtSdhLineRateStm1Fhn)||(EncapTypeBuf>cAtSdhLineRateStm4Fhn)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error arg=%d\r\n",EncapTypeBuf);
		return -1;
	}

	sdhModuleLine = (AtSdhLine)AtFhnLineGet(slotId,cposPort);

	ret = AtSdhLineRateSet(sdhModuleLine, EncapDriverBuf);
       if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
       }

	return 0;

}


/*
CmdSdhLineRate
typedef enum eAtSdhLineRateFhn
    {
    cAtSdhLineRateStm1Fhn,    /**< STM-1 
    cAtSdhLineRateStm4Fhn,    /**< STM-4 
    cAtSdhLineRateStm16fhn    /**< STM-16 
    }eAtSdhLineRateFhn;

*/
int Udr_cpos_port_linerate_get(uint8 cardId, uint32 cposPort, void *arg)
{
	eAtSdhLineRate EncapDriverBuf;
	AtSdhLine sdhModuleLine;
	eAtRet  ret;
	AtEncapChannel encapChannel;
	uint8 slotId=cardId;

	if(gui4EncapDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x, cposPort=%d\r\n",cardId,cposPort);
	}
	
	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	sdhModuleLine = (AtSdhLine)AtFhnLineGet(slotId,cposPort);

	EncapDriverBuf = AtSdhLineRateGet(sdhModuleLine);

	*(eAtSdhLineRateFhn *)arg = EncapDriverBuf+1;

	return 0;

}


/*
enable 	0: enable force down
		1: diable force down
*/
int Udr_cpos_port_admin_status(uint8 cardId, uint32 cposPort, void *arg)
{
	uint32 enable = *(uint32 *)arg;
	AtSdhLine sdhModuleLine;
	eAtRet  ret;
	AtEncapChannel encapChannel;
	uint8 slotId=cardId;

	if(gui4EncapDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x, cposPort=%d,enable=%x\r\n",cardId,cposPort,enable);
	}
	
	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	sdhModuleLine = (AtSdhLine)AtFhnLineGet(slotId,cposPort);

	if(enable){
		ret = AtChannelRxAlarmUnForce((AtChannel)sdhModuleLine, cAtSdhLineAlarmLos);
	}else{
		ret = AtChannelRxAlarmForce((AtChannel)sdhModuleLine, cAtSdhLineAlarmLos);
	}
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
       }
	
	return 0;

}



/*
cposPort = 1-8
0:disable
1:enable

*/
int Udr_cpos_port_enable(uint8 cardId, uint32 cposPort, void *arg)
{
	int ret=0;
	uint32 enable = *(uint32 *)arg;
	uint8 deviceNum =1,slotId=cardId;
	AtSdhLine sdhModuleLine;
	
	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,cposPort=%d,arg = %x\r\n",cardId,cposPort,enable);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	sdhModuleLine = (AtSdhLine)AtFhnLineGet(slotId,cposPort);

	ret = AtChannelEnable((AtChannel)sdhModuleLine, enable);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	return 0;
	
}



/*
typedef enum eAtSdhLineModeFhn
    {
    cAtSdhLineModeSdhFhn,     /**< SDH 
    cAtSdhLineModeSonetFhn    /**< SONET 
    }eAtSdhLineModeFhn;

*/
int Udr_cpos_port_linemode_set(uint8 cardId, uint32 cposPort, void *arg)
{
	eAtSdhLineModeFhn EncapTypeBuf = *(eAtSdhLineModeFhn *)arg;
	AtSdhLine sdhModuleLine;
	AtDevice *pDevice;
	eAtRet  ret;
	AtEncapChannel encapChannel;
	uint8 slotId=cardId;

	if(gui4EncapDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,cposPort=%d,arg = %x\r\n",cardId,cposPort,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	if((EncapTypeBuf<cAtSdhLineModeSdhFhn)||(EncapTypeBuf>cAtSdhLineModeSonetFhn)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error arg=%d\r\n",EncapTypeBuf);
		return -1;
	}

	sdhModuleLine = (AtSdhLine)AtFhnLineGet(slotId, cposPort);

	ret = AtSdhLineModeSet(sdhModuleLine, EncapTypeBuf);
        if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
        }

	return 0;

}

/*
show sdh line alarm 1
CmdSdhLineShowAlarm
typedef struct eUdrSdhLineAlarmTypeFhn
    {
    uint8 udrSdhLineAlarmLosFhn;      /**< @brief RS-LOS defect mask  1: clear 
    uint8 udrSdhLineAlarmOOFFhn;       /**< @brief RS-OOF defect mask 
    uint8 udrSdhLineAlarmLOFFhn;        /**< @brief RS-LOF defect mask 
    uint8 udrSdhLineAlarmTIMFhn;         /**< @brief RS-TIM defect mask 
    uint8 udrSdhLineAlarmAISFhn;          /**< @brief MS-AIS defect mask 
    uint8 udrSdhLineAlarmRDIFhn;        /**< @brief MS-RDI defect mask 
    uint8 udrSdhLineAlarmBERSDFhn;         /**< @brief MS-BER-SD defect mask 
    uint8 udrSdhLineAlarmBERSFFhn;         /**< @brief MS-BER-SF defect mask 
    uint8 udrSdhLineAlarmKBCHANGEFhn;    /**< @brief K-byte change event.
                                             Stable K-byte is changed 
    uint8 udrtSdhLineAlarmKBFAILFhn;      /**< @brief K-byte fail. There is no stable K-byte 
    uint8 udrSdhLineAlarmS1CHANGEFhn;     /**< @brief S1 byte change event. 
    }eUdrSdhLineAlarmTypeFhn;

0: clear alarm
1 : have alarm
*/

int Udr_cpos_port_alarm_status(uint8 cardId, uint32 cposPort, void *arg)
{
	uint8 *EncapTypeBuf;
	AtSdhLine sdhModuleLine;
	eAtRet  ret;
	AtEncapChannel encapChannel;
	uint32 i,alarmStat=0;
	uint8 slotId=cardId;
	
	if(gui4EncapDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,cposPort=%d,arg = %x\r\n",cardId,cposPort,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	EncapTypeBuf = (uint8 *)arg;

	sdhModuleLine = (AtSdhLine)AtFhnLineGet(slotId,cposPort);

	alarmStat = AtChannelAlarmGet((AtChannel)sdhModuleLine);
	for (i = 0; i < cUdrSdhLineNumAlarmFhn; i++){
		if (alarmStat & cCmdSdhLineAlarmTypeValFhn[i]){
			EncapTypeBuf[i]=1;
		}else{
			EncapTypeBuf[i]=0;
		}
	}

	return 0;

}

/*
J0
CmdSdhLineTxTti

typedef enum eAtSdhTtiMode
    {
    cAtSdhTtiMode1Byte,  /**< 1-byte 
    cAtSdhTtiMode16Byte, /**< 16-bytes 
    cAtSdhTtiMode64Byte  /**< 64-bytes 
    }eAtSdhTtiMode;


typedef struct tAtSdhTti
    {
	eAtSdhTtiMode mode;                       
	uint8 message[cAtSdhChannelMaxTtiLength];
	int8 paddingMode;/* 0: null_padding  1: space_padding
    }tAtSdhTti;

show sdh line tti receive 1
*/


int Udr_cpos_port_tti_tx_set(uint8 cardId, uint32 cposPort, void *arg)
{
	int ret=0;
     	tAtSdhTtiFhn ttiMode;
	uint32 cposPortID=cposPort;
	uint32 fpgaID=0;
	uint8 slotId=cardId;

	if(gui4EncapDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,cposPort=%d,arg = %x\r\n",cardId,cposPort,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cposPort=%d\r\n",cposPort);
		return cAtFalse;
	}

	memcpy(&ttiMode, arg, sizeof(tAtSdhTtiFhn));
	
	ret = At_cpos_port_tti_tx_set(slotId,cposPortID,0,0,0,0,ttiMode.mode,ttiMode.message,ttiMode.paddingMode);	
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}

/*
J0 
CmdSdhLineExptTti

typedef enum eAtSdhTtiMode
    {
    cAtSdhTtiMode1Byte,  /**< 1-byte 
    cAtSdhTtiMode16Byte, /**< 16-bytes 
    cAtSdhTtiMode64Byte  /**< 64-bytes 
    }eAtSdhTtiMode;


typedef struct tAtSdhTti
    {
    eAtSdhTtiMode mode;                       /**< Message mode 
    uint8 message[cAtSdhChannelMaxTtiLength]; /**< Message content 
    }tAtSdhTti;

*/


int Udr_cpos_port_tti_rx_set(uint8 cardId, uint32 cposPort, void *arg)
{
	int ret=0;
     	tAtSdhTtiFhn ttiMode;
	uint32 cposPortID=cposPort;
	uint32 fpgaID=0;
	uint8 slotId=cardId;

	memcpy(&ttiMode, arg, sizeof(tAtSdhTtiFhn));

	if(gui4EncapDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("slot=%d,cposPort=%d,mode=%d,mes=%s,pad=%d\r\n",cardId,cposPort,ttiMode.mode,ttiMode.message,ttiMode.paddingMode);
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cposPort=%d\r\n",cposPort);
		return cAtFalse;
	}

	
	ret = At_cpos_port_tti_rx_set(slotId,cposPortID,0,0,0,0,ttiMode.mode,ttiMode.message,ttiMode.paddingMode);	
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}


/* J1 */

int Udr_cpos_port_path_tti_tx_set(uint8 cardId, uint32 cposPort, uint32 cposPortChannel, void *arg)
{
	int ret=0,aug1Id=0;
     	tAtSdhTtiFhn ttiMode;
	tAtchannelMapToSdh mapBuf;
	uint32 cposPortID=cposPort;
	uint32 fpgaID=0;
	uint8 slotId=cardId;

	memcpy(&ttiMode, arg, sizeof(tAtSdhTtiFhn));
	
	if(gui4EncapDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,cposPort=%d,ch=%d,mode=%d,mess=[%s],pad=%d\r\n",cardId,cposPort,cposPortChannel,		
			ttiMode.mode,
			ttiMode.message,
			ttiMode.paddingMode);
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cposPort=%d\r\n",cposPort);
		return cAtFalse;
	}

	if((cposPortChannel>63*4) || (cposPortChannel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPortChannel);
		return cAtFalse;
	}

	aug1Id = ((cposPortChannel-1)/63) + 1; 
	memcpy(&mapBuf, &(gsChannelMapToSdh[cposPortChannel-63*(aug1Id-1)]), sizeof(tAtchannelMapToSdh));
	
	ret = At_cpos_port_path_tti_tx_set(slotId,cposPortID,
		aug1Id,mapBuf.au3Tug3Id,mapBuf.tug2Id,mapBuf.tuId,ttiMode.mode,ttiMode.message,ttiMode.paddingMode);	
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}

int Udr_cpos_port_path_vc3_tti_tx_set(uint8 cardId, uint32 cposPort, void *arg)
{
	int ret=0,aug1Id=1;
     	tAtSdhTtiFhn ttiMode;
	tAtchannelMapToSdh mapBuf;
	uint32 cposPortID=cposPort;
	uint32 fpgaID=0;
	uint8 slotId=cardId,vc3Id=0;

	memcpy(&ttiMode, arg, sizeof(tAtSdhTtiFhn));
	
	if(gui4EncapDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,cposPort=%d,mode=%d,mess=[%s],pad=%d\r\n",cardId,cposPort,		
			ttiMode.mode,
			ttiMode.message,
			ttiMode.paddingMode);
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cposPort=%d\r\n",cposPort);
		return cAtFalse;
	}

	for(aug1Id=1;aug1Id<=4;aug1Id++){
		for(vc3Id=1;vc3Id<=3;vc3Id++){
			ret = At_cpos_port_path_vc3_tti_tx_set(slotId,
				cposPortID,
				aug1Id,
				vc3Id,
				ttiMode.mode,
				ttiMode.message,
				ttiMode.paddingMode);	
			if (ret != cAtOk){
				printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
				return -1;
			}
		}
	}
	return 0;

}

int Udr_cpos_port_path_vc4_tti_tx_set(uint8 cardId, uint32 cposPort, void *arg)
{
	int ret=0,aug1Id=1;
     	tAtSdhTtiFhn ttiMode;
	tAtchannelMapToSdh mapBuf;
	uint32 cposPortID=cposPort;
	uint32 fpgaID=0;
	uint8 slotId=cardId;

	memcpy(&ttiMode, arg, sizeof(tAtSdhTtiFhn));
	
	if(gui4EncapDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,cposPort=%d,mode=%d,mess=[%s],pad=%d\r\n",cardId,cposPort,		
			ttiMode.mode,
			ttiMode.message,
			ttiMode.paddingMode);
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cposPort=%d\r\n",cposPort);
		return cAtFalse;
	}


	ret = At_cpos_port_path_vc4_tti_tx_set(slotId,
		cposPortID,
		aug1Id,
		ttiMode.mode,
		ttiMode.message,
		ttiMode.paddingMode);	
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}

int Udr_cpos_port_path_vc4c_tti_tx_set(uint8 cardId, uint32 cposPort, void *arg)
{
	int ret=0,aug1Id=1;
     	tAtSdhTtiFhn ttiMode;
	tAtchannelMapToSdh mapBuf;
	uint32 cposPortID=cposPort;
	uint32 fpgaID=0;
	uint8 slotId=cardId;

	memcpy(&ttiMode, arg, sizeof(tAtSdhTtiFhn));
	
	if(gui4EncapDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,cposPort=%d,mode=%d,mess=[%s],pad=%d\r\n",cardId,cposPort,		
			ttiMode.mode,
			ttiMode.message,
			ttiMode.paddingMode);
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cposPort=%d\r\n",cposPort);
		return cAtFalse;
	}


	ret = At_cpos_port_path_vc4c_tti_tx_set(slotId,
		cposPortID,
		aug1Id,
		ttiMode.mode,
		ttiMode.message,
		ttiMode.paddingMode);	
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}

int Udr_cpos_port_path_tti_rx_set(uint8 cardId, uint32 cposPort, uint32 cposPortChannel, void *arg)
{
	int ret=0,aug1Id=0;
     	tAtSdhTtiFhn ttiMode;
	tAtchannelMapToSdh mapBuf;
	uint32 cposPortID=cposPort;
	uint32 fpgaID=0;
	uint8 slotId=cardId;

	memcpy(&ttiMode, arg, sizeof(tAtSdhTtiFhn));

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,cposPort=%d,ch=%d,mode=%d,mess=[%s],pad=%d\r\n",cardId,cposPort,cposPortChannel,		
			ttiMode.mode,
			ttiMode.message,
			ttiMode.paddingMode);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cposPort=%d\r\n",cposPort);
		return cAtFalse;
	}

	if((cposPortChannel>63*4) || (cposPortChannel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPortChannel);
		return cAtFalse;
	}

	aug1Id = ((cposPortChannel-1)/63) + 1; 
	memcpy(&mapBuf, &(gsChannelMapToSdh[cposPortChannel-63*(aug1Id-1)]), sizeof(tAtchannelMapToSdh));
	
	ret = At_cpos_port_path_tti_rx_set(slotId,cposPortID,
		aug1Id,mapBuf.au3Tug3Id,mapBuf.tug2Id,mapBuf.tuId,ttiMode.mode,ttiMode.message,ttiMode.paddingMode);	
	if (ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}

int Udr_cpos_port_path_vc3_tti_rx_set(uint8 cardId, uint32 cposPort, void *arg)
{
	int ret=0,aug1Id=1;
     	tAtSdhTtiFhn ttiMode;
	tAtchannelMapToSdh mapBuf;
	uint32 cposPortID=cposPort;
	uint32 fpgaID=0;
	uint8 slotId=cardId,vc3Id=0;

	memcpy(&ttiMode, arg, sizeof(tAtSdhTtiFhn));

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,cposPort=%d,mode=%d,mess=[%s],pad=%d\r\n",cardId,cposPort,		
			ttiMode.mode,
			ttiMode.message,
			ttiMode.paddingMode);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cposPort=%d\r\n",cposPort);
		return cAtFalse;
	}

	for(aug1Id=1;aug1Id<=4;aug1Id++){
		for(vc3Id=1;vc3Id<=3;vc3Id++){
			ret = At_cpos_port_path_vc3_tti_rx_set(slotId,
				cposPortID,
				aug1Id,
				vc3Id,
				ttiMode.mode,
				ttiMode.message,
				ttiMode.paddingMode);	
			if (ret != cAtOk)
			{
				printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
				return -1;
			}
		}
	}
	return 0;

}

int Udr_cpos_port_path_vc4_tti_rx_set(uint8 cardId, uint32 cposPort, void *arg)
{
	int ret=0,aug1Id=1;
     	tAtSdhTtiFhn ttiMode;
	tAtchannelMapToSdh mapBuf;
	uint32 cposPortID=cposPort;
	uint32 fpgaID=0;
	uint8 slotId=cardId;

	memcpy(&ttiMode, arg, sizeof(tAtSdhTtiFhn));

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,cposPort=%d,mode=%d,mess=[%s],pad=%d\r\n",cardId,cposPort,		
			ttiMode.mode,
			ttiMode.message,
			ttiMode.paddingMode);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cposPort=%d\r\n",cposPort);
		return cAtFalse;
	}
	
	ret = At_cpos_port_path_vc4_tti_rx_set(slotId,
		cposPortID,
		aug1Id,
		ttiMode.mode,
		ttiMode.message,
		ttiMode.paddingMode);	
	if (ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}

int Udr_cpos_port_path_vc4c_tti_rx_set(uint8 cardId, uint32 cposPort, void *arg)
{
	int ret=0,aug1Id=1;
     	tAtSdhTtiFhn ttiMode;
	tAtchannelMapToSdh mapBuf;
	uint32 cposPortID=cposPort;
	uint32 fpgaID=0;
	uint8 slotId=cardId;

	memcpy(&ttiMode, arg, sizeof(tAtSdhTtiFhn));

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,cposPort=%d,mode=%d,mess=[%s],pad=%d\r\n",cardId,cposPort,		
			ttiMode.mode,
			ttiMode.message,
			ttiMode.paddingMode);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cposPort=%d\r\n",cposPort);
		return cAtFalse;
	}
	
	ret = At_cpos_port_path_vc4c_tti_rx_set(slotId,
		cposPortID,
		aug1Id,
		ttiMode.mode,
		ttiMode.message,
		ttiMode.paddingMode);	
	if (ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}

/*
C2 
CmdSdhPathTxPsl
*/
int Udr_cpos_port_path_psl_tx_set(uint8 cardId, uint32 cposPort, uint32 cposPortChannel, void *arg)
{
	int ret=0,aug1Id=0;
	uint8 pslMessage = *(uint8*)arg;
	tAtchannelMapToSdh mapBuf;
	uint32 cposPortID=cposPort;
	uint8 slotId=cardId;
	
	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,cposPort=%d,channel=%d,mess=[%s]\r\n",cardId,cposPort, cposPortChannel,pslMessage);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cposPort=%d\r\n",cposPort);
		return cAtFalse;
	}

	if((cposPortChannel>63*4) || (cposPortChannel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPortChannel);
		return cAtFalse;
	}

	aug1Id = ((cposPortChannel-1)/63) + 1; 
	memcpy(&mapBuf, &(gsChannelMapToSdh[cposPortChannel-63*(aug1Id-1)]), sizeof(tAtchannelMapToSdh));
	
	ret = At_cpos_port_path_psl_tx_set(slotId, cposPortID,
		aug1Id,mapBuf.au3Tug3Id,mapBuf.tug2Id,mapBuf.tuId,pslMessage);	
	if (ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}

int Udr_cpos_port_path_vc3_psl_tx_set(uint8 cardId, uint32 cposPort, void *arg)
{
	int ret=0,aug1Id=1;
	uint8 pslMessage = *(uint8*)arg;
	tAtchannelMapToSdh mapBuf;
	uint32 cposPortID=cposPort;
	uint8 slotId=cardId,vc3Id=0;
	
	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,cposPort=%d,mess=[%d]\r\n",cardId,cposPort, pslMessage);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cposPort=%d\r\n",cposPort);
		return cAtFalse;
	}

	for(aug1Id=1;aug1Id<=4;aug1Id++){
		for(vc3Id=1;vc3Id<=3;vc3Id++){
			ret = At_cpos_port_path_vc3_psl_tx_set(slotId, 
				cposPortID,
				aug1Id,
				vc3Id,
				pslMessage);	
			if (ret != cAtOk)
			{
				printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
				return -1;
			}
		}
	}
	return 0;

}

int Udr_cpos_port_path_vc4_psl_tx_set(uint8 cardId, uint32 cposPort, void *arg)
{
	int ret=0,aug1Id=1;
	uint8 pslMessage = *(uint8*)arg;
	tAtchannelMapToSdh mapBuf;
	uint32 cposPortID=cposPort;
	uint8 slotId=cardId;
	
	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,cposPort=%d,mess=[%d]\r\n",cardId,cposPort, pslMessage);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cposPort=%d\r\n",cposPort);
		return cAtFalse;
	}

	ret = At_cpos_port_path_vc4_psl_tx_set(slotId, 
		cposPortID,
		aug1Id,
		pslMessage);	
	if (ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}

int Udr_cpos_port_path_vc4c_psl_tx_set(uint8 cardId, uint32 cposPort, void *arg)
{
	int ret=0,aug1Id=1;
	uint8 pslMessage = *(uint8*)arg;
	tAtchannelMapToSdh mapBuf;
	uint32 cposPortID=cposPort;
	uint8 slotId=cardId;
	
	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,cposPort=%d,mess=[%d]\r\n",cardId,cposPort, pslMessage);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cposPort=%d\r\n",cposPort);
		return cAtFalse;
	}

	ret = At_cpos_port_path_vc4c_psl_tx_set(slotId, 
		cposPortID,
		aug1Id,
		pslMessage);	
	if (ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}


int Udr_cpos_port_path_psl_rx_set(uint8 cardId, uint32 cposPort, uint32 cposPortChannel, void *arg)
{
	int ret=0,aug1Id=0;
     	uint8  pslMessage = *(uint8*)arg;
	tAtchannelMapToSdh mapBuf;
	uint32 cposPortID=cposPort;
	uint8 slotId=cardId;
	
	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,cposPort=%d,channel=%d,mess=[%s]\r\n",cardId,cposPort, cposPortChannel,pslMessage);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cposPort=%d\r\n",cposPort);
		return cAtFalse;
	}

	if((cposPortChannel>63*4) || (cposPortChannel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPortChannel);
		return cAtFalse;
	}

	aug1Id = ((cposPortChannel-1)/63) + 1; 
	memcpy(&mapBuf, &(gsChannelMapToSdh[cposPortChannel-63*(aug1Id-1)]), sizeof(tAtchannelMapToSdh));
	
	ret = At_cpos_port_path_psl_rx_set(slotId,cposPortID,
		aug1Id,mapBuf.au3Tug3Id,mapBuf.tug2Id,mapBuf.tuId,pslMessage);	
	if (ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}

int Udr_cpos_port_path_vc3_psl_rx_set(uint8 cardId, uint32 cposPort, void *arg)
{
	int ret=0,aug1Id=1;
     	uint8  pslMessage = *(uint8*)arg;
	tAtchannelMapToSdh mapBuf;
	uint32 cposPortID=cposPort;
	uint8 slotId=cardId,vc3Id=0;
	
	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,cposPort=%d,mess=[%d]\r\n",cardId,cposPort, pslMessage);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cposPort=%d\r\n",cposPort);
		return cAtFalse;
	}

	for(aug1Id=1;aug1Id<=4;aug1Id++){
		for(vc3Id=1;vc3Id<=3;vc3Id++){
			ret = At_cpos_port_path_vc3_psl_rx_set(slotId,
				cposPortID,
				aug1Id,
				vc3Id,
				pslMessage);	
			if (ret != cAtOk)
			{
				printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
				return -1;
			}
		}
	}
	return 0;

}

int Udr_cpos_port_path_vc4_psl_rx_set(uint8 cardId, uint32 cposPort, void *arg)
{
	int ret=0,aug1Id=1;
     	uint8  pslMessage = *(uint8*)arg;
	tAtchannelMapToSdh mapBuf;
	uint32 cposPortID=cposPort;
	uint8 slotId=cardId;
	
	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,cposPort=%d,mess=[%d]\r\n",cardId,cposPort, pslMessage);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cposPort=%d\r\n",cposPort);
		return cAtFalse;
	}
	
	ret = At_cpos_port_path_vc4_psl_rx_set(slotId,
		cposPortID,
		aug1Id,
		pslMessage);	
	if (ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}

int Udr_cpos_port_path_vc4c_psl_rx_set(uint8 cardId, uint32 cposPort, void *arg)
{
	int ret=0,aug1Id=1;
     	uint8  pslMessage = *(uint8*)arg;
	tAtchannelMapToSdh mapBuf;
	uint32 cposPortID=cposPort;
	uint8 slotId=cardId;
	
	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,cposPort=%d,mess=[%d]\r\n",cardId,cposPort, pslMessage);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cposPort=%d\r\n",cposPort);
		return cAtFalse;
	}
	
	ret = At_cpos_port_path_vc4c_psl_rx_set(slotId,
		cposPortID,
		aug1Id,
		pslMessage);	
	if (ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}



int Udr_cpos_port_channel_bind_port(uint8 cardId, uint32 channel, uint32 cposPort, uint32 cposPortChannel)
{
	AtModuleEncap encapModule;
	int ret=0,aug1Id=0;
	AtEncapChannel encapChannel;
	uint8 deviceNum=1;
	AtChannel channels;
	tAtchannelMapToSdh mapBuf;
	uint32 cposPortID=cposPort;
	uint8 slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,channel=%d,cposPort=%x,cposPortChannel = %x\r\n",cardId,channel,cposPort,cposPortChannel);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>AT_POS_STM_MAX_HW_CHANNEL_FHN) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return cAtFalse;
	}

	if((cposPortChannel>63*4) || (cposPortChannel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPortChannel);
		return cAtFalse;
	}

	aug1Id = ((cposPortChannel-1)/63) + 1; 
	memcpy(&mapBuf, &(gsChannelMapToSdh[cposPortChannel-63*(aug1Id-1)]), sizeof(tAtchannelMapToSdh));
	//printf("au3Tug3Id=%d, tug2Id=%d, tuId=%d\r\n",mapBuf.au3Tug3Id,mapBuf.tug2Id,mapBuf.tuId);
	

	if(cposPort<=4){
		deviceNum=1;
	}else if((cposPort>4)&&(cposPort<=8)){
		deviceNum=2;
	}else{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error port=%d\r\n",cposPort);
		return -1;
	}

	encapModule = (AtModuleEncap)AtFhnEncapModule(slotId, deviceNum);
	encapChannel = (AtEncapChannel)AtModuleEncapChannelGet(encapModule, channel - 1);

	channels = (AtChannel)AtFhnVcDe1Get(slotId, cposPortID, aug1Id, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);

	ret = AtEncapChannelPhyBind(encapChannel, channels);			
	if (ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}

int Udr_cpos_port_channel_e3_bind_port(uint8 cardId, uint32 channel, uint32 cposPort, uint32 cposPortChannel)
{
	AtModuleEncap encapModule;
	int ret=0,aug1Id=0;
	AtEncapChannel encapChannel;
	uint8 deviceNum=1;
	AtChannel channels;
	tAtchannelMapToSdhSTM4ToE3 mapBuf;
	uint32 cposPortID=cposPort;
	uint8 slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,channel=%d,cposPort=%x,cposPortChannel = %x\r\n",cardId,channel,cposPort,cposPortChannel);
		printf("cposPortID[%d]=cposPort[%d]\r\n",cposPortID,cposPort);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>AT_POS_STM_MAX_HW_CHANNEL_FHN) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return cAtFalse;
	}

	if((cposPortChannel>4*3) || (cposPortChannel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPortChannel);
		return cAtFalse;
	}

	memcpy(&mapBuf, &(gsChannelMapToSdhSTM4ToE3[cposPortChannel]), sizeof(tAtchannelMapToSdhSTM4ToE3));
	//printf("cposPortID=%x,cposPortChannel=%d, aug1Id=%d, tug3Id=%d\r\n",cposPortID,cposPortChannel,mapBuf.aug1Id, mapBuf.tug3Id);

	if(cposPort<=4){
		deviceNum=1;
	}else if((cposPort>4)&&(cposPort<=8)){
		deviceNum=2;
	}else{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error port=%d\r\n",cposPort);
		return -1;
	}

	encapModule = (AtModuleEncap)AtFhnEncapModule(slotId, deviceNum);
	encapChannel = (AtEncapChannel)AtModuleEncapChannelGet(encapModule, channel - 1);

	channels = (AtChannel)AtFhnVcDe3Get(slotId, cposPortID, mapBuf.aug1Id, mapBuf.tug3Id);

	ret = AtEncapChannelPhyBind(encapChannel, channels);			
	if (ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}


int Udr_cpos_port_channel_unbind_port(uint8 cardId, uint32 fpgaID, uint32 channel, void *arg)
{
	AtModuleEncap encapModule;
	int ret=0;
	AtEncapChannel encapChannel;
	AtChannel channels;
	uint8 slotId=cardId;
	
	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("fpgaID=%d,channel=%x\r\n",fpgaID,channel);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(fpgaID<1||fpgaID>2)
	{
		printf("#%d,%s,error fpgaID=%d\r\n",__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}

	if((channel>AT_POS_STM_MAX_HW_CHANNEL_FHN) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return cAtFalse;
	}

	encapModule = (AtModuleEncap)AtFhnEncapModule(slotId, fpgaID);
	encapChannel = (AtEncapChannel)AtModuleEncapChannelGet(encapModule, channel - 1);

	channels = NULL;
        ret = AtEncapChannelPhyBind(encapChannel, channels);			
        if (ret != cAtOk){
            printf("ERROR: The error return = %x\r\n", ret);
            return -1;
       }

	return 0;

}


/* Flow binding channel */
/* CmdEncapHdlcLkFlowBind */
/*
	flowId = 1-1024
	channelId = 1-512
*/
int Udr_cpos_port_channel_bind_flow(uint8 cardId, uint32 fpgaID, uint32 channel, uint32 flowId)
{
	uint8 deviceNum=fpgaID;
	int ret=0;
	uint8 slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("channel=%d,flowId=%x\r\n",channel, flowId);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(fpgaID<1||fpgaID>2)
	{
		printf("#%d,%s,error fpgaID=%d\r\n",__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}

	if((channel>AT_POS_STM_MAX_HW_CHANNEL_FHN) || (channel<1)){
		printf("%s,#%d,%s,error flow=%d\r\n",__FILE__,__LINE__,__FUNCTION__,flowId);
		return -1;
	}

	if((flowId>AT_POS_STM_MAX_FLOW_FHN) || (flowId<1)){
		printf("%s,#%d,%s,error flow=%d\r\n",__FILE__,__LINE__,__FUNCTION__,flowId);
		return -1;
	}

	ret = AtHdlcLinkFlowBind((AtHdlcLink)AtFhnHdlcLinkGet(slotId,deviceNum, channel), (AtEthFlow)AtFhnEthFlowGet(slotId,deviceNum, flowId));
	if (ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;
	
}


int Udr_cpos_port_channel_unbind_flow(uint8 cardId, uint32 fpgaID,uint32 channel, void *arg)
{
	uint32 flowId = *(uint32 *)arg;
	AtEthFlow flow;
	uint8 deviceNum=fpgaID;
	uint8 slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("channel=%d,flowId=%x\r\n",channel, flowId);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(fpgaID<1||fpgaID>2)
	{
		printf("#%d,%s,error fpgaID=%d\r\n",__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}

	if((channel>AT_POS_STM_MAX_HW_CHANNEL_FHN) || (channel<1)){
		printf("%s,#%d,%s,error flow=%d\r\n",__FILE__,__LINE__,__FUNCTION__,flowId);
		return -1;
	}
	
	flow = NULL;

	return (AtHdlcLinkFlowBind((AtHdlcLink)AtFhnHdlcLinkGet(slotId, deviceNum, channel), flow) == cAtOk) ? 0 : -1;

}


/*
typedef enum eAtHdlcFrameTypeFhn
    {
    cAtHdlcFrmUnknownFhn,   /**< Un-known frame type 
    cAtHdlcFrmCiscoHdlcFhn, /**< Cisco HDLC 
    cAtHdlcFrmPppFhn,       /**< PPP 
    cAtHdlcFrmFrFhn,         /**< Frame relay 
    }eAtHdlcFrameTypeFhn;
*/
int Udr_cpos_port_channel_framer_type_set(uint8 cardId, uint32 fpgaID,uint32 channel, void *arg)
{
	eAtHdlcFrameTypeFhn frameType = *(eAtHdlcFrameTypeFhn *)arg;
	uint32 timeslot = 0;
	AtModuleEncap encapModule;
	AtDevice *pDevice;
	eAtRet  ret;
	AtHdlcChannel encapChannel;
	uint8 deviceNum=fpgaID,slotId=cardId;
	AtChannel channels;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,frameType=%x\r\n",cardId,channel,frameType);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(fpgaID<1||fpgaID>2)
	{
		printf("#%d,%s,error fpgaID=%d\r\n",__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}

	if((channel>AT_POS_STM_MAX_HW_CHANNEL_FHN) || (channel<1)){
		printf("%s,#%d,%s,error channel=%d\r\n",__FILE__,__LINE__,__FUNCTION__,channel);
		return -1;
	}

	if((frameType>cAtHdlcFrmFrFhn) || (frameType<cAtHdlcFrmUnknownFhn))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error frameType=%d\r\n",frameType);
		return -1;
	}


	encapModule = (AtModuleEncap)AtFhnEncapModule(slotId, deviceNum);
	
	encapChannel = (AtHdlcChannel)AtModuleEncapChannelGet(encapModule, channel - 1);

        ret = AtHdlcChannelFrameTypeSet(encapChannel, frameType);			
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;
}

/*
CmdSdhLineLoopbackSet

typedef enum eAtLoopbackMode
    {
    cAtLoopbackModeRelease, /**< Release loopback 
    cAtLoopbackModeLocal,   /**< Local loopback 
    cAtLoopbackModeRemote   /**< Remote loopback 
    }eAtLoopbackMode;
*/
int Udr_cpos_port_loopback_set(uint8 cardId, uint32 cposPort, void *arg)
{
	eAtLoopbackModeFhn loopBackBuf = *(eAtLoopbackModeFhn *)arg;
	AtSdhLine sdhModuleLine;
	AtDevice *pDevice;
	eAtRet  ret;
	AtEncapChannel encapChannel;
	uint32 fpgaID=0;
	uint8 deviceNum=1;
	uint8 slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("channel=%d,arg = %x\r\n",cposPort,*(uint32 *)arg);
	}


	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

	if((loopBackBuf<cAtLoopbackModeReleaseFhn)||(loopBackBuf>cAtLoopbackModeRemoteFhn))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error arg=%d\r\n",loopBackBuf);
		return -1;
	}

	sdhModuleLine = (AtSdhLine)AtFhnLineGet(slotId,cposPort);

	ret = AtChannelLoopbackSet((AtChannel)sdhModuleLine, loopBackBuf);
        if (ret != cAtOk)
        {
              printf("%s,#%d,%s,error ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
		return -1;
        }

	return 0;

}

/*
CmdPdhDe1LoopbackSet
portId = 1-16
arg:
           + release
            + payloadloopin
            + payloadloopout
            + lineloopin
            + lineloopout
*/

int Udr_cpos_e1_loopback_set(uint8 cardId, uint32 cposPort, uint32 cposPortChannel, void *arg)
{
	int ret=0;
	eAtPdhLoopbackModeFhn EncapTypeBuf = *(eAtPdhLoopbackModeFhn *)arg;
	tAtchannelMapToSdh mapBuf;
	int aug1Id=0;
	AtPdhChannel channels;
	uint32 cposPortID=cposPort;
	uint32 deviceNum =1,slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cposPort=%x,cposPortChannel = %x\r\n",cposPort,cposPortChannel);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return cAtFalse;
	}

	if((cposPortChannel>63*4) || (cposPortChannel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPortChannel);
		return cAtFalse;
	}

	aug1Id = ((cposPortChannel-1)/63) + 1; 
	memcpy(&mapBuf, &(gsChannelMapToSdh[cposPortChannel-63*(aug1Id-1)]), sizeof(tAtchannelMapToSdh));
	//printf("au3Tug3Id=%d, tug2Id=%d, tuId=%d\r\n",mapBuf.au3Tug3Id,mapBuf.tug2Id,mapBuf.tuId);
	
	channels = (AtPdhChannel)AtFhnVcDe1Get(slotId, cposPortID, aug1Id, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);

	if((EncapTypeBuf<cAtPdhLoopbackModeReleaseFhn)||(EncapTypeBuf>cAtPdhLoopbackModeRemoteLineFhn))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error arg=%d\r\n",EncapTypeBuf);
		return -1;
	}

	ret = AtChannelLoopbackSet((AtChannel)channels, EncapTypeBuf);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	return 0;
}

/*

<line>.<aug1>.<au3Tug3>.<tug2>.<tu>

typedef enum eAtPdhDe1FrameType
    {
    cAtPdhDe1FrameUnknown , /**< Unknown frame 

    cAtPdhDs1J1UnFrm      , /**< DS1/J1 Unframe 
    cAtPdhDs1FrmSf        , /**< DS1 SF (D4) 
    cAtPdhDs1FrmEsf       , /**< DS1 ESF 
    cAtPdhDs1FrmDDS       , /**< DS1 DDS 
    cAtPdhDs1FrmSLC       , /**< DS1 SLC 

    cAtPdhJ1FrmSf         , /**< J1 SF 
    cAtPdhJ1FrmEsf        , /**< J1 ESF 

    cAtPdhE1UnFrm         , /**< E1 unframe 
    cAtPdhE1Frm           , /**< E1 basic frame, FAS/NFAS framing 
    cAtPdhE1MFCrc           /**< E1 Multi-Frame with CRC4 
    }eAtPdhDe1FrameType;
*/

int Udr_cpos_e1_framer_mode_set(uint8 cardId, uint32 cposPort, uint32 cposPortChannel, void *arg)
{
	eAtPdhDe1FrameType EncapTypeBuf = *(eAtPdhDe1FrameType *)arg;
	AtModuleEncap encapModule;
	int ret=0,aug1Id=0;
	AtEncapChannel encapChannel;
	uint8 deviceNum=1;
	AtPdhChannel channels;
	tAtchannelMapToSdh mapBuf;
	uint32 cposPortID=cposPort;
	uint8 slotId=cardId;


	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cposPort=%x,cposPortChannel = %x\r\n",cposPort,cposPortChannel);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return cAtFalse;
	}

	if((cposPortChannel>63*4) || (cposPortChannel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPortChannel);
		return cAtFalse;
	}

	aug1Id = ((cposPortChannel-1)/63) + 1; 
	memcpy(&mapBuf, &(gsChannelMapToSdh[cposPortChannel-63*(aug1Id-1)]), sizeof(tAtchannelMapToSdh));
	//printf("au3Tug3Id=%d, tug2Id=%d, tuId=%d\r\n",mapBuf.au3Tug3Id,mapBuf.tug2Id,mapBuf.tuId);
	
	channels = (AtPdhChannel)AtFhnVcDe1Get(slotId, cposPortID, aug1Id, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);

       ret = AtPdhChannelFrameTypeSet(channels, EncapTypeBuf);			
       if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}


int  Udr_cpos_e1_clock_mode_set(uint8 cardId, uint32 cposPort, uint32 cposPortChannel, void *arg)
{
	tAtTimeModeFhn EncapTypeBuf;
	AtModuleEncap encapModule;
	int ret=0,aug1Id=0;
	AtEncapChannel encapChannel;
	uint8 deviceNum=1;
	AtChannel channels, refChannels;
	tAtchannelMapToSdh mapBuf;
	uint32 cposPortID=cposPort;
	uint32 fpgaId = 0;
	uint8 slotId=cardId;


	if(gui4EncapDebug)
	{
		memcpy(&EncapTypeBuf, arg, sizeof(tAtTimeModeFhn));
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,cposPort=%x,Channel=%x,timeMode=%x,refPort=%x\r\n",cardId,cposPort,cposPortChannel,EncapTypeBuf.TimeModen,EncapTypeBuf.refPort);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return cAtFalse;
	}

	if((cposPortChannel>63*4) || (cposPortChannel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPortChannel);
		return -1;
	}

	memcpy(&EncapTypeBuf, arg, sizeof(tAtTimeModeFhn));

	aug1Id = ((cposPortChannel-1)/63) + 1; 
	memcpy(&mapBuf, &(gsChannelMapToSdh[cposPortChannel-63*(aug1Id-1)]), sizeof(tAtchannelMapToSdh));
	//printf("au3Tug3Id=%d, tug2Id=%d, tuId=%d\r\n",mapBuf.au3Tug3Id,mapBuf.tug2Id,mapBuf.tuId);
	

	if((EncapTypeBuf.refPort>16) || (EncapTypeBuf.refPort<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error refPort=%d\r\n",EncapTypeBuf.refPort);
		return -1;
	}


	if((EncapTypeBuf.TimeModen<cAtTimingModeUnknownFhn)||(EncapTypeBuf.TimeModen>cAtTimingModeSlaveFhn))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error TimeModen=%d\r\n",EncapTypeBuf.TimeModen);
		return -1;
	}

	if(EncapTypeBuf.TimeModen == cAtTimingModeSlaveFhn){
		At_cpos_port_clock_src_set(slotId,EncapTypeBuf.refPort);
		return 0;
	}else{
		fpgaId = (cposPortID>4) ? 2:1 ;
		At_cpos_port_clock_src_delete(slotId,fpgaId);
	}


	channels = (AtChannel)AtFhnVcDe1Get(slotId, cposPortID, aug1Id, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);

	refChannels = (AtChannel)AtFhnVcDe1Get(slotId, EncapTypeBuf.refPort, aug1Id, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	
	ret = AtChannelTimingSet(channels, (eAtTimingMode)(EncapTypeBuf.TimeModen), 
							refChannels);

}


/*
CmdPdhNxDs0Create
creat ts in CE1
input channel: 
*/
int Udr_cpos_ce1_create_port_ts(uint8 cardId, uint32 cposPort, uint32 cposPortChannel, void *arg)
{
	int i=0,ret=0,aug1Id=0;
	tAtchannelMapToSdh mapBuf;
	uint32 timeslotBitmap = *(uint32 *)arg;
	AtModulePdh encapModule;
	uint32 cposPortID = cposPort;
	uint8 deviceNum=1,slotId=cardId;
	AtPdhDe1 channels;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,cposPort = %d,cposPortChannel=%d,timeslotBitmap=%x\r\n",cardId, cposPortChannel,timeslotBitmap);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPortChannel>252) || (cposPortChannel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPortChannel);
		return -1;
	}


	if((cposPort<1)||(cposPort>8))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cposPort=%d \r\n",cposPort);
		return -1;
	}

	timeslotBitmap = timeslotBitmap << 1;

	aug1Id = ((cposPortChannel-1)/63) + 1; 
	memcpy(&mapBuf, &(gsChannelMapToSdh[cposPortChannel-63*(aug1Id-1)]), sizeof(tAtchannelMapToSdh));

	channels = (AtPdhDe1)AtFhnVcDe1Get(slotId, cposPortID, aug1Id, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);

	printf("channels=%x, timeslotBitmap=%x\r\n",channels,timeslotBitmap);

	if(AtPdhDe1NxDs0Create((AtPdhDe1)channels, timeslotBitmap) == NULL)
	{
		return -1;
	}
	return 0;
}

int Udr_cpos_ce1_delete_port_ts(uint8 cardId, uint32 cposPort, uint32 cposPortChannel, void *arg)
{
	int i=0,ret=0,aug1Id=0;
	tAtchannelMapToSdh mapBuf;
	uint32 timeslotBitmap = *(uint32 *)arg;
	AtModulePdh encapModule;
	uint32 cposPortID = cposPort;
	uint8 deviceNum=1,slotId=cardId;
	AtPdhDe1 channels;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,cposPort = %d,cposPortChannel=%d,timeslotBitmap=%x\r\n",cardId,cposPort, cposPortChannel,timeslotBitmap);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort<1)||(cposPort>8))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cposPort=%d \r\n",cposPort);
		return -1;
	}

	if((cposPortChannel>63*4) || (cposPortChannel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPortChannel);
		return cAtFalse;
	}

	timeslotBitmap = timeslotBitmap << 1;

	aug1Id = ((cposPortChannel-1)/63) + 1; 
	memcpy(&mapBuf, &(gsChannelMapToSdh[cposPortChannel-63*(aug1Id-1)]), sizeof(tAtchannelMapToSdh));

	channels = (AtPdhDe1)AtFhnVcDe1Get(slotId, cposPortID, aug1Id, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);

	ret = AtPdhDe1NxDs0Delete((AtPdhDe1)channels,  (AtPdhNxDS0)AtPdhDe1NxDs0Get(channels, timeslotBitmap));
	if (ret != cAtOk)
	{
		printf("ERROR: The error return = %x\r\n", ret);
		return -1;
	}
	return 0;
}

/* 
CmdEncapBindPhy
timeslot  	bitmap:1-32
*/

int Udr_cpos_ce1_channel_bind_port_ts(uint8 cardId, uint32 cposPort, uint32 cposPortChannel, uint32 channel, void *arg)
{
	int i=0,ret=0;
	uint32 aug1Id=0;
	tAtchannelMapToSdh mapBuf;
	uint32 timeslotBitmap = *(uint32 *)arg;
	AtModuleEncap encapModule;
	AtHdlcChannel encapChannel;
	AtPdhDe1 pdhDe1Module;
	uint8 slotId=cardId;
	uint32 fpgaID =  (cposPort <= 4) ? 1:2; 
	AtChannel channels;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardID=%d,port=%d,ch=%d,channel=%d,phyport=%x,timeslot=%x\r\n",cardId,cposPort,cposPortChannel,channel,timeslotBitmap);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>AT_POS_STM_MAX_HW_CHANNEL_FHN) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	if((cposPort<1)||(cposPort>8))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cposPort=%d\r\n",cposPort);
		return -1;
	}

	if((cposPortChannel>63*4) || (cposPortChannel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPortChannel);
		return cAtFalse;
	}

	timeslotBitmap = timeslotBitmap << 1;

	//encapModule = (AtModuleEncap)AtFhnEncapModule(slotId, deviceNum);
	//encapChannel = (AtEncapChannel)AtModuleEncapChannelGet(encapModule, channel - 1);

	encapChannel = (AtHdlcChannel)AtFhnHdlcChannelGet(slotId, fpgaID, channel);

	aug1Id = ((cposPortChannel-1)/63) + 1; 
	memcpy(&mapBuf, &(gsChannelMapToSdh[cposPortChannel-63*(aug1Id-1)]), sizeof(tAtchannelMapToSdh));
	channels = (AtChannel)AtFhnVcDe1Get(slotId, cposPort, aug1Id, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	
	ret = AtEncapChannelPhyBind((AtEncapChannel)encapChannel, 
			(AtChannel)AtPdhDe1NxDs0Get((AtPdhDe1)channels, timeslotBitmap));			
	if (ret != cAtOk)
	{
		printf("ERROR: The error return = %x\r\n", ret);
		return -1;
	}

	return 0;
	
}


int Udr_cpos_ce1_channel_unbind_port_ts(uint8 cardId, uint32 cposPort, uint32 cposPortChannel, uint32 channel, void *arg)
{
	uint32 phyport = *(uint32 *)arg;
	uint32 timeslot = 0;
	AtModuleEncap encapModule;
	AtDevice *pDevice;
	eAtRet  ret;
	AtEncapChannel encapChannel;
	uint8 slotId=cardId;
	uint32 fpgaID =  (cposPort <= 4) ? 1:2; 
	AtChannel channels;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardID=%d,port=%d,ch=%d,channel=%d,phyport=%x\r\n",cardId,cposPort,cposPortChannel,channel,phyport);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>AT_POS_STM_MAX_HW_CHANNEL_FHN) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	if((cposPort<1)||(cposPort>8))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cposPort=%d\r\n",cposPort);
		return -1;
	}

	//pDevice = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), &deviceNum);
	//encapModule = (AtModuleEncap)AtDeviceModuleGet(*pDevice, cAtModuleEncap);

	encapModule = (AtModuleEncap)AtFhnEncapModule(slotId, fpgaID);
	
	encapChannel = (AtEncapChannel)AtModuleEncapChannelGet(encapModule, channel - 1);


	channels = NULL;

        ret = AtEncapChannelPhyBind(encapChannel, channels);			
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;
}


/*
HW_IF_ENCAPTYPE
typedef enum eAtEncapTypeCreateFhn
    {
    cAtHdlcFrmCiscoHdlcCreateFhn,/**< Cisco HDLC 
    cAtHdlcFrmPppCreateFhn,      /**< PPP 
    cAtHdlcFrmFrCreateFhn,       /**< Frame relay 
    cAtEncapAtmCreateFhn         /**< ATM 
    } eAtEncapTypeCreateFhn;
*/
int udr_cpos_port_encap_type_set(uint8 cardId, uint32 cposPort, uint32 logicalChannel, uint32 *encapType)
{
	uint32 slotId = cardId-1;
	uint32 arg = *(uint32 *)encapType;

	if(gui4EncapDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,port=%d,logicalChannel=%d,encapType=%d\r\n",cardId,cposPort,logicalChannel,*(uint32 *)encapType);
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(cposPort<1||cposPort>AT_POS_STM_MAX_PORT_FHN)	{
		printf("#%d,%s,error cposPort=%d\r\n",__LINE__,__FUNCTION__,cposPort);
		return -1;
	}	

	if(logicalChannel>AT_POS_STM_MAX_LOGICAL_CHANNEL_PERPORT_FHN)	{
		printf("#%d,%s,error logicalChannel=%d\r\n",__LINE__,__FUNCTION__,logicalChannel);
		return -1;
	}

	gui4AtStmlogicalchannelEncap[slotId][cposPort][logicalChannel].encapType= arg;		

	return 0;
}


int udr_cpos_port_encap_type_get(uint8 cardId, uint32 cposPort, uint32 logicalChannel, uint32 *encapType)
{
	uint32 slotId = cardId-1;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(cposPort<1||cposPort>AT_POS_STM_MAX_PORT_FHN)
	{
		printf("#%d,%s,error cposPort=%d\r\n",__LINE__,__FUNCTION__,cposPort);
		return -1;
	}	

	if(logicalChannel>AT_POS_STM_MAX_LOGICAL_CHANNEL_PERPORT_FHN)
	{
		printf("#%d,%s,error logicalChannel=%d\r\n",__LINE__,__FUNCTION__,logicalChannel);
		return -1;
	}


	*(uint32 *)encapType = gui4AtStmlogicalchannelEncap[slotId][cposPort][logicalChannel].encapType;		

	return 0;
}


int Udr_cpos_mlppp_bundle_create(uint8 cardId, uint32 fpgaID, uint32 bundleId, void *arg)
{
	int ret;
	AtModulePpp pppModule;
	uint8 slotId=cardId;

	if(gui4MLPPPDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,fpgaID=%d,bundleId=%d\r\n",cardId,fpgaID,bundleId);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((bundleId>128) || (bundleId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",bundleId);
		return -1;
	}

	if((fpgaID==1)||(fpgaID==2)){
		pppModule = (AtModulePpp)AtFhnPppModule(slotId,fpgaID);
	}else{
		printf("%s,#%d,%s,error fpga=%x\r\n",__FILE__,__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}

	if(AtModulePppMpBundleCreate(pppModule, bundleId-1) == NULL)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return -1;
	}

	return 0;

}

int Udr_cpos_mlppp_bundle_Delete(uint8 cardId, uint32 fpgaID, uint32 bundleId)
{
	int ret=0;
	AtModulePpp pppModule;
	uint8 slotId=cardId;

	if(gui4MLPPPDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,fpgaID=%d,bundleId=%d\r\n",cardId,fpgaID,bundleId);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((bundleId>128) || (bundleId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",bundleId);
		return -1;
	}


	if((fpgaID==1)||(fpgaID==2)){
		pppModule = (AtModulePpp)AtFhnPppModule(slotId, fpgaID);
	}else{
		printf("%s,#%d,%s,error fpga=%x\r\n",__FILE__,__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}

	ret = AtModulePppMpBundleDelete(pppModule, bundleId-1);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}


/*
CmdPppBundleEnable

bundleId = 1-128
0:disable
1:enable
*/


int Udr_cpos_mlppp_bundle_enable(uint8 cardId, uint32 fpgaID, uint32 bundleId, void *arg)
{
	int enable = *(int *)arg;
	int ret;
	AtMpBundle MpppModule;
	uint8 slotId=cardId;

	if(gui4MLPPPDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,fpgaID=%d,bundleId=%d,enable=%d\r\n",cardId,fpgaID,bundleId,enable);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((bundleId>128) || (bundleId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",bundleId);
		return -1;
	}


	if((fpgaID==1)||(fpgaID==2)){
		MpppModule = (AtMpBundle)AtFhnMpBundleGet(slotId, fpgaID,bundleId);
	}else{
		printf("%s,#%d,%s,error fpga=%x\r\n",__FILE__,__LINE__,__FUNCTION__,fpgaID);
		return -1;
	}

	ret = AtChannelEnable((AtChannel)MpppModule, enable);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%d\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
		return -1;
	}

	return 0;

}


/* CmdPppBundleAddLink */
int Udr_cpos_mlppp_bundle_add_channel(uint8 cardId, uint32 fpgaID, uint32 bundleId, uint32 channel)
{
	int ret;
	AtHdlcBundle hdlcModule;
	AtHdlcLink hdlcLinkModule;
	uint8 slotId=cardId;

	if(gui4MLPPPDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,fpgaID=%d,bundleId=%d,ch=%d\r\n",cardId,fpgaID,bundleId,channel);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	hdlcModule = (AtHdlcBundle)AtFhnMpBundleGet(slotId,fpgaID,bundleId);

	hdlcLinkModule = (AtHdlcLink)AtFhnHdlcLinkGet(slotId,fpgaID,channel);

	ret = AtHdlcBundleLinkAdd(hdlcModule, hdlcLinkModule);
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%d\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
		return -1;
	}

	return 0;


}


/* CmdPppBundleAddLink */
int Udr_cpos_mlppp_bundle_delete_channel(uint8 cardId, uint32 fpgaID, uint32 bundleId, uint32 channel)
{
	int ret;
	AtHdlcBundle hdlcModule;
	AtHdlcLink hdlcLinkModule;
	uint8 slotId=cardId;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}
	
	if(gui4MLPPPDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("channel=%d,bundleId=%x\r\n",channel, bundleId);
	}

	hdlcModule = (AtHdlcBundle)AtFhnMpBundleGet(slotId, fpgaID,bundleId);

	hdlcLinkModule = (AtHdlcLink)AtFhnHdlcLinkGet(slotId, fpgaID,channel);

	ret = AtHdlcBundleLinkRemove(hdlcModule, hdlcLinkModule);
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;


}


/* CmdPppBundleAddFlow */
int Udr_cpos_mlppp_bundle_bind_flow(uint8 cardId, uint32 fpgaID, uint32 bundleId, uint32 flow)
{
	int ret;
	AtMpBundle MpbundleModule;
	AtEthFlow flowModule;
	uint8 slotId=cardId;

	if(gui4MLPPPDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,fpgaID=%d,bundleId=%d,flow=%d\r\n",cardId,fpgaID,bundleId,flow);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	MpbundleModule = (AtMpBundle)AtFhnMpBundleGet(slotId, fpgaID,bundleId);

	flowModule = (AtEthFlow)AtFhnEthFlowGet(slotId, fpgaID,flow);

	ret = AtMpBundleFlowAdd(MpbundleModule, flowModule, 0);
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%d\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
		return -1;
	}

	return 0;
}


/* CmdPppBundleAddFlow */
int Udr_cpos_mlppp_bundle_unbind_flow(uint8 cardId, uint32 fpgaID, uint32 bundleId, uint32 flow)
{
	int ret;
	AtMpBundle MpbundleModule;
	AtEthFlow flowModule;
	uint8 slotId=cardId;

	if(gui4MLPPPDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,fpgaID=%d,bundleId=%d,flow=%d\r\n",cardId,fpgaID,bundleId,flow);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	MpbundleModule = (AtMpBundle)AtFhnMpBundleGet(slotId,fpgaID,bundleId);

	flowModule = (AtEthFlow)AtFhnEthFlowGet(slotId,fpgaID,flow);

	ret = AtMpBundleFlowRemove(MpbundleModule, flowModule);
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;
}


/*
show eth port counters 1 r2c
show encap hdlc counters 1 r2c
show ppp link counters 1 r2c
show eth flow counters 1 r2c
show eth flow 1

*/

/*
show eth port counters 1 r2c
CmdEthPortCounterGet
*/

int Udr_cpos_ethport_counter_get(uint8 cardId, uint32 fpgaID, uint32 port, void *arg)
{
    	tAtEthPortCountersFhn *counters = (tAtEthPortCountersFhn *)arg;
	eAtRet  ret;
	uint8 slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,fpgaID=%d,port=%d\r\n",cardId,fpgaID,port);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((port>2) || (port<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",port);
		return -1;
	}

	ret = AtChannelAllCountersGet((AtChannel)AtFhnEthPortGet(slotId,fpgaID,port), counters);
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}

int Udr_cpos_ethport_counter_clear(uint8 cardId, uint32 fpgaID, uint32 port, void *arg)
{
    	tAtEthPortCountersFhn counters;
	eAtRet  ret;
	uint8 slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,fpgaID=%d,port=%d\r\n",cardId,fpgaID,port);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((port>2) || (port<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",port);
		return -1;
	}

	ret = AtChannelAllCountersClear((AtChannel)AtFhnEthPortGet(slotId,fpgaID,port), &counters);
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}

int Udr_cpos_channel_counter_get(uint8 cardId, uint32 fpgaID, uint32 channel, void *arg)
{
	tAtHdlcChannelCountersFhn *hdlcCounter = (tAtHdlcChannelCountersFhn *)arg;
	tAtHdlcChannelCounters hdlcCounterbuf={0};
	eAtRet  ret;
	uint8 slotId=cardId;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}
	
	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("channel=%d,arg = %x\r\n",channel,*(uint32 *)arg);
	}


	if((channel>128) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	/* use it to get ppp link counter */
	/*  ret = AtChannelAllCountersGet((AtChannel)AtFhnHdlcLinkGet(fpgaID,channel), hdlcCounter); */
	ret = AtChannelAllCountersGet((AtChannel)AtFhnHdlcChannelGet(slotId,fpgaID,channel), &hdlcCounterbuf);
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	hdlcCounter->txGoodPktFhn = hdlcCounterbuf.txGoodPkt;
	hdlcCounter->txAbortPktFhn= hdlcCounterbuf.txAbortPkt;	
	hdlcCounter->rxGoodPktFhn = hdlcCounterbuf.rxGoodPkt;
	hdlcCounter->rxAbortPktFhn= hdlcCounterbuf.rxAbortPkt;	
	hdlcCounter->rxFcsErrPktFhn = hdlcCounterbuf.rxFcsErrPkt;
	hdlcCounter->rxAddrCtrlErrPktFhn= hdlcCounterbuf.rxAddrCtrlErrPkt;	
	hdlcCounter->rxSapiErrPktFhn = hdlcCounterbuf.rxSapiErrPkt;
	hdlcCounter->rxErrPktFhn= hdlcCounterbuf.rxErrPkt;	
	
	return 0;

}


int Udr_cpos_channel_counter_clear(uint8 cardId, uint32 fpgaID, uint32 channel, void *arg)
{
	tAtHdlcChannelCounters hdlcCounter;
	eAtRet  ret;
	uint8 slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,fpgaID=%d,channel=%d\r\n",cardId,fpgaID,channel);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>128) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	ret = AtChannelAllCountersClear((AtChannel)AtFhnHdlcChannelGet(slotId, fpgaID,channel), &hdlcCounter);
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}



/* CmdSdhPathBerSdThres */
/*
 + 1e-3   : Error rate is 1E-3 
                                  + 1e-4   : Error rate is 1E-4
                                  + 1e-5   : Error rate is 1E-5
                                  + 1e-6   : Error rate is 1E-6
                                  + 1e-7   : Error rate is 1E-7
                                  + 1e-8   : Error rate is 1E-8
                                  + 1e-9   : Error rate is 1E-9

    AtBerController berController = AtSdhChannelBerControllerGet(line);
    return AtBerControllerSdThresholdSet(berController, berRate);
    

*/
int Udr_cpos_port_ber_sd_set(uint8 cardId, uint32 cposPort, void *arg)
{
       AtBerController berController;
	eAtBerRateFhn berRate = *(eAtBerRateFhn *)arg;
	AtSdhLine sdhModuleLine;
	eAtRet  ret;
	uint8 slotId=cardId;

	if(gui4EncapDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x, cposPort=%d,arg=%x\r\n",cardId,cposPort,berRate);
	}
	
	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

        berController = AtSdhChannelBerControllerGet((AtSdhChannel)AtFhnLineGet(slotId,cposPort));
        ret = AtBerControllerSdThresholdSet(berController, berRate);
       if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
       }

	return 0;
}
/* CmdSdhPathBerSfThres */
/*
 + 1e-3   : Error rate is 1E-3 
                                  + 1e-4   : Error rate is 1E-4
                                  + 1e-5   : Error rate is 1E-5
                                  + 1e-6   : Error rate is 1E-6
                                  + 1e-7   : Error rate is 1E-7
                                  + 1e-8   : Error rate is 1E-8
                                  + 1e-9   : Error rate is 1E-9

                              AtBerControllerSfThresholdSet    
*/
int Udr_cpos_port_ber_sf_set(uint8 cardId, uint32 cposPort, void *arg)
{
       AtBerController berController;
	eAtBerRateFhn berRate = *(eAtBerRateFhn *)arg;
	AtSdhLine sdhModuleLine;
	eAtRet  ret;
	uint8 slotId=cardId;

	if(gui4EncapDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x, cposPort=%d,arg=%x\r\n",cardId,cposPort,berRate);
	}
	
	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",cposPort);
		return -1;
	}

        berController = AtSdhChannelBerControllerGet((AtSdhChannel)AtFhnLineGet(slotId,cposPort));
        ret = AtBerControllerSfThresholdSet(berController, berRate);
       if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
       }

	return 0;
}


/*
CmdEncapChannelScramble

channel = 1-128

0:disable
1:enable

*/
int Udr_cpos_port_channel_scramle_set(uint8 cardId, uint32 fpgaID, uint32 channel, void *arg)
{
	int ret=0;
	uint32 EncapTypeBuf = *(uint32 *)arg;
	uint8 deviceNum =fpgaID,slotId=cardId;
	AtHdlcChannel hdlcChannel;
	
	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,channel=%d,arg = %x\r\n",cardId,channel,*(uint32 *)arg);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if(fpgaID<1||fpgaID>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>128) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return -1;
	}

	if((EncapTypeBuf<0)||(EncapTypeBuf>1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error arg=%d\r\n",EncapTypeBuf);
		return -1;
	}

    	hdlcChannel = (AtHdlcChannel)AtModuleEncapChannelGet((AtModuleEncap)AtFhnEncapModule(slotId,deviceNum), channel - 1);
		
	ret = AtHdlcChannelScrambleEnable(hdlcChannel, EncapTypeBuf);
	if(ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}
	return 0;
	
}



int Udr_pos_stm1_port_channel_bind_port(uint8 cardId, uint32 channel, uint32 posPort)
{
	AtModuleEncap encapModule;
	int ret=0,aug1Id=0;
	AtEncapChannel encapChannel;
	uint8 deviceNum=1;
	AtChannel channels;
	tAtchannelMapToSdh mapBuf;
	uint32 cposPortID=posPort;
	uint8 slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,channel=%d,posPort=%d\r\n",cardId,channel,posPort);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((channel>AT_POS_STM_MAX_HW_CHANNEL_FHN) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return cAtFalse;
	}
	
	if((posPort>8) || (posPort<1)){
		printf("%s,#%d,%s error posPort=%d\r\n",__FILE__,__LINE__,__FUNCTION__,posPort);
		return -1;
	}

	aug1Id =  1; 

	if(posPort<=4){
		deviceNum=1;
	}else if((posPort>4)&&(posPort<=8)){
		deviceNum=2;
	}else{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error port=%d\r\n",posPort);
		return -1;
	}

	encapModule = (AtModuleEncap)AtFhnEncapModule(slotId, deviceNum);
	encapChannel = (AtEncapChannel)AtModuleEncapChannelGet(encapModule, channel - 1);

	channels = (AtChannel)AtFhnVc4Get(slotId, cposPortID, aug1Id);

	ret = AtEncapChannelPhyBind(encapChannel, channels);			
	if (ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}

int Udr_pos_stm1_port_channel_unbind_port(uint8 cardId, uint32 fpgaID, uint32 channel, void *arg)
{
	return Udr_cpos_port_channel_unbind_port(cardId, fpgaID, channel, arg);
}

/*
CmdSdhMapSet
channel = 1-63

sdh map aug1.1.1 vc4
sdh map vc4.1.1 c4

*/
int Udr_pos_stm1_port_sdh_map(uint8 cardId, uint32 posPort)
{
	int ret=0,aug1Id=0;
	tAtchannelMapToSdh mapBuf;
	tAtEthVlanTagFhn vlanTag;
	uint32 cposPortID=posPort;
	uint8 slotId=cardId;

	if(gui4AtSDHMapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,posPort=%d\r\n",cardId,posPort );
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}
	
	if((posPort>8) || (posPort<1)){
		printf("%s,#%d,%s error posPort=%d\r\n",__FILE__,__LINE__,__FUNCTION__,posPort);
		return -1;
	}

	aug1Id = 1; 
	
 	ret = At_cpos_port_stm_sdh_map_aug1_to_vc4(slotId, cposPortID, aug1Id, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	if(ret != cAtOk){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return -1;
	}

 	ret = At_cpos_port_stm_sdh_map_vc4_to_c4(slotId, cposPortID, aug1Id, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	if(ret != cAtOk){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return -1;
	}
	
	return 0;
}

/**/
int Udr_pos_stm4_port_sdh_map(uint8 cardId, uint32 posPort)
{
	int ret=0,aug1Id=0;
	tAtchannelMapToSdh mapBuf;
	tAtEthVlanTagFhn vlanTag;
	uint32 cposPortID=posPort;
	uint8 slotId=cardId;

	if(gui4AtSDHMapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,posPort=%d\r\n",cardId,posPort);
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}
	
	if((posPort!=1) && (posPort!=5)){
		printf("%s,#%d,%s error posPort=%d\r\n",__FILE__,__LINE__,__FUNCTION__,posPort);
		return -1;
	}


	ret = At_cpos_port_stm_sdh_map_aug4_to_aug1(slotId, cposPortID, 1, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	if(ret != cAtOk){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return -1;
	}


	
 	ret = At_cpos_port_stm_sdh_map_aug1_to_vc4(slotId, cposPortID, 1, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	if(ret != cAtOk){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return -1;
	}
 	ret = At_cpos_port_stm_sdh_map_aug1_to_vc4(slotId, cposPortID, 2, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	if(ret != cAtOk){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return -1;
	}
	ret = At_cpos_port_stm_sdh_map_aug1_to_vc4(slotId, cposPortID, 3, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	if(ret != cAtOk){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return -1;
	}
	ret = At_cpos_port_stm_sdh_map_aug1_to_vc4(slotId, cposPortID, 4, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	if(ret != cAtOk){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return -1;
	}


	
 	ret = At_cpos_port_stm_sdh_map_vc4_to_c4(slotId, cposPortID, 1, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	if(ret != cAtOk){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return -1;
	}
 	ret = At_cpos_port_stm_sdh_map_vc4_to_c4(slotId, cposPortID, 2, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	if(ret != cAtOk){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return -1;
	}
 	ret = At_cpos_port_stm_sdh_map_vc4_to_c4(slotId, cposPortID, 3, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	if(ret != cAtOk){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return -1;
	}
 	ret = At_cpos_port_stm_sdh_map_vc4_to_c4(slotId, cposPortID, 4, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	if(ret != cAtOk){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return -1;
	}	

	
	return 0;
}

int Udr_pos_stm4_vc4c_port_sdh_map(uint8 cardId, uint32 posPort)
{
	int ret=0,aug1Id=0;
	tAtchannelMapToSdh mapBuf;
	tAtEthVlanTagFhn vlanTag;
	uint32 cposPortID=posPort;
	uint8 slotId=cardId;

	if(gui4AtSDHMapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%x,posPort=%d\r\n",cardId,posPort);
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}
	
	if((posPort!=1) && (posPort!=5)){
		printf("%s,#%d,%s error posPort=%d\r\n",__FILE__,__LINE__,__FUNCTION__,posPort);
		return -1;
	}


	ret = At_cpos_port_stm_sdh_map_aug4_to_vc4c(slotId, cposPortID, 1, mapBuf.au3Tug3Id, mapBuf.tug2Id, mapBuf.tuId);
	if(ret != cAtOk){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		return -1;
	}

	
	return 0;
}


/*
vc4Ch = 1-4
*/
int Udr_pos_stm4_port_channel_bind_port(uint8 cardId, uint32 posPort, uint32 channel, uint32 vc4Ch)
{
	AtModuleEncap encapModule;
	int ret=0,aug1Id=0;
	AtEncapChannel encapChannel;
	uint8 deviceNum;
	AtChannel channels;
	tAtchannelMapToSdh mapBuf;
	uint32 cposPortID=posPort;
	uint8 slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d, posPort=%d,channel=%d,vc4Ch=%d\r\n",cardId,posPort, channel,vc4Ch);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((posPort!=1) && (posPort!=5)){
		printf("%s,#%d,%s error posPort=%d\r\n",__FILE__,__LINE__,__FUNCTION__,posPort);
		return -1;
	}

	if((channel>AT_POS_STM_MAX_HW_CHANNEL_FHN) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return cAtFalse;
	}
	
	if((vc4Ch>4) || (vc4Ch<1)){
		printf("%s,#%d,%s error posPort=%d\r\n",__FILE__,__LINE__,__FUNCTION__,posPort);
		return -1;
	}

	aug1Id =  vc4Ch; 

	if(posPort<=4){
		deviceNum=1;
	}else if((posPort>4)&&(posPort<=8)){
		deviceNum=2;
	}else{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error port=%d\r\n",posPort);
		return -1;
	}

	encapModule = (AtModuleEncap)AtFhnEncapModule(slotId, deviceNum);
	encapChannel = (AtEncapChannel)AtModuleEncapChannelGet(encapModule, channel - 1);

	channels = (AtChannel)AtFhnVc4Get(slotId, cposPortID, aug1Id);

	ret = AtEncapChannelPhyBind(encapChannel, channels);			
	if (ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}


/*
vc4Ch = 1-4
*/
int Udr_pos_stm4_vc4c_port_channel_bind_port(uint8 cardId, uint32 posPort, uint32 channel)
{
	AtModuleEncap encapModule;
	int ret=0,aug1Id=0;
	AtEncapChannel encapChannel;
	uint8 deviceNum;
	AtChannel channels;
	tAtchannelMapToSdh mapBuf;
	uint32 cposPortID=posPort;
	uint8 slotId=cardId;

	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d, posPort=%d,channel=%d\r\n",cardId,posPort, channel);
	}

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((posPort!=1) && (posPort!=5)){
		printf("%s,#%d,%s error posPort=%d\r\n",__FILE__,__LINE__,__FUNCTION__,posPort);
		return -1;
	}

	if((channel>AT_POS_STM_MAX_HW_CHANNEL_FHN) || (channel<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",channel);
		return cAtFalse;
	}
	

	if(posPort<=4){
		deviceNum=1;
	}else if((posPort>4)&&(posPort<=8)){
		deviceNum=2;
	}else{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error port=%d\r\n",posPort);
		return -1;
	}

	encapModule = (AtModuleEncap)AtFhnEncapModule(slotId, deviceNum);
	encapChannel = (AtEncapChannel)AtModuleEncapChannelGet(encapModule, channel - 1);

	channels = (AtChannel)AtFhnVc4cGet(slotId, cposPortID, 1);

	ret = AtEncapChannelPhyBind(encapChannel, channels);			
	if (ret != cAtOk)
	{
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}

int Udr_pos_stm4_port_channel_unbind_port(uint8 cardId, uint32 fpgaID, uint32 channel, void *arg)
{
	return Udr_cpos_port_channel_unbind_port(cardId, fpgaID, channel, arg);
}

int Udr_pos_stm4_port_path_vc4_tti_tx_set(uint8 cardId, uint32 cposPort,  uint32 vc4Ch, void *arg)
{
	int ret=0,aug1Id=vc4Ch;
     	tAtSdhTtiFhn ttiMode;
	tAtchannelMapToSdh mapBuf;
	uint32 cposPortID=cposPort;
	uint32 fpgaID=0;
	uint8 slotId=cardId;

	memcpy(&ttiMode, arg, sizeof(tAtSdhTtiFhn));
	
	if(gui4EncapDebug){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("cardId=%d,cposPort=%d,mode=%d,mess=[%s],pad=%d\r\n",cardId,cposPort,		
			ttiMode.mode,
			ttiMode.message,
			ttiMode.paddingMode);
	}

	if(cardId<1||cardId>2){
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	if((cposPort>8) || (cposPort<1)){
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cposPort=%d\r\n",cposPort);
		return cAtFalse;
	}


	ret = At_cpos_port_path_vc4_tti_tx_set(slotId,
		cposPortID,
		aug1Id,
		ttiMode.mode,
		ttiMode.message,
		ttiMode.paddingMode);	
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}

	return 0;

}

int udr_pos_stm_trunk_acl_add(uint8 cardId, uint32 cposPort, uint32 cposPortVc12Ch, uint32 logicalChannel, uint32 trunkId,
												uint8 *dmac, tAtEthVlanTagFhn *ouputvlan,tAtEthVlanTagFhn *inputvlan)
{
	int ret=0;
	uint32 arg=0;
	uint32 channelId=0,encapType=0,flowId=0,cposPortVc12Channel=0,cposPortVc12Timeslot=0;
	uint32 fpgaId = (cposPort > 4)?1:0;
 	uint32 port = cposPort;

	if((cardId>2) || (cardId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cardId=%d\r\n",cardId);
		return -1;
	}

	if(cposPort<1||cposPort>AT_POS_STM_MAX_PORT_FHN)
	{
		printf("#%d,%s,error cposPort=%d\r\n",__LINE__,__FUNCTION__,cposPort);
		return -1;
	}

	if((trunkId>AT_POS_STM_MAX_MLPPP_BUNDLE_FHN) || (trunkId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error trunkId=%d\r\n",trunkId);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
			fpgaId=1;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}

	if(At_fhn_pos_stm_soft_channel_get( cardId,  port,  logicalChannel, &cposPortVc12Channel, &cposPortVc12Timeslot,&channelId, &encapType) == -1){
		return -1;
	}

	flowId = AT_POS_STM_MAX_HW_CHANNEL_FHN+trunkId;
	arg=0;
	ret = Udr_cpos_port_flow_create(cardId,fpgaId+1,flowId,&arg);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	ret = Udr_cpos_port_flow_egress_dmac_set(cardId,fpgaId+1,flowId,dmac);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	ret = Udr_cpos_port_flow_egress_vlan_set(cardId,fpgaId+1,1,flowId, ouputvlan);/*��������ʾ�ĳ���VLAN  */
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	ret = Udr_cpos_port_flow_set_vlan(cardId, fpgaId+1, 1, flowId, inputvlan);/*����ʾ�ķ������,ֻ�ܻ���VLAN */
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	ret = Udr_cpos_mlppp_bundle_bind_flow(cardId, fpgaId+1, trunkId,flowId);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}
	
	return ret;
}


int udr_pos_stm_trunk_acl_del(uint8 cardId, uint32 cposPort, uint32 cposPortVc12Ch, uint32 logicalChannel, uint32 trunkId)
{
	uint32 arg;
	tAtPdhDs0PortFhn E1TsPort={0};
	int ret=0;
	uint32 channelId=0,encapType=0,flowId=0,timeslotMap=0;
	uint32 fpgaId = (cposPort > 4)?1:0;
	uint32 port = cposPort;

	if((cardId>2) || (cardId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error cardId=%d\r\n",cardId);
		return -1;
	}

	if(cposPort<1||cposPort>AT_POS_STM_MAX_PORT_FHN)
	{
		printf("#%d,%s,error cposPort=%d\r\n",__LINE__,__FUNCTION__,cposPort);
		return -1;
	}

	if(gui4AtslotCardType[cardId] == cAtCardTypeline2STM4){
		if(port==1){
			// do nothing;
		}else if(port==2){
			port=5;
			fpgaId=1;
		}else{
			printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
			printf("error port=%d\r\n",port);
			return -1;
		}
	}
	
	flowId = AT_POS_STM_MAX_HW_CHANNEL_FHN+trunkId;
	ret = Udr_cpos_mlppp_bundle_unbind_flow(cardId, fpgaId+1, trunkId,flowId);/*��ҵ��ͨ������ID*/
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	encapType = 0;/*cAtEthFlowNpoPpp*/
	ret = Udr_cpos_port_flow_del(cardId,fpgaId+1,flowId,&encapType);
	if(ret !=0){
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		return -1;
	}

	return ret;
}


#if	0
/*
show eth flow counters 1 r2c
CmdEthFlowCounterGet
*/

int Udr_ce1_flow_counter_get(uint32 flowId, void *arg)
{
	tAtEthFlowCountersFhn  *flowCounter = (tAtEthFlowCountersFhn *)arg;
	eAtRet  ret;
	uint8 fpgaID=1;
	
	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("flowId=%d,arg = %x\r\n",flowId,*(uint32 *)arg);
	}


	if((flowId>128) || (flowId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",flowId);
		return -1;
	}

	ret = AtChannelAllCountersGet((AtChannel)AtFhnEthFlowGet(fpgaID,flowId), flowCounter);
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}


	return 0;

}

/*
show eth flow counters 1 r2c
CmdEthFlowCounterGet
*/

int Udr_ce1_flow_counter_clear(uint32 flowId, void *arg)
{
	tAtEthFlowCountersFhn  *flowCounter = (tAtEthFlowCountersFhn *)arg;
	eAtRet  ret;
	uint8 fpgaID=1;
	
	if(gui4EncapDebug)
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("flowId=%d,arg = %x\r\n",flowId,*(uint32 *)arg);
	}


	if((flowId>128) || (flowId<1))
	{
		printf("%s,#%d,%s\r\n",__FILE__,__LINE__,__FUNCTION__);
		printf("error channel=%d\r\n",flowId);
		return -1;
	}

	ret = AtChannelAllCountersClear((AtChannel)AtFhnEthFlowGet(fpgaID,flowId), flowCounter);
	if (ret != cAtOk){
		printf("%s,#%d,%s,ret=%s\r\n",__FILE__,__LINE__,__FUNCTION__,AtRet2String(ret));
		return -1;
	}


	return 0;

}
#endif




void AtFhnCli( void )
{
	AtCliStartWithTextUI(AtDefaultTinyTextUIGet(cmdConf, cmdConfCount));
}



/*
pdh de1 framing 1 e1_unframed
pdh de1 timing 1 system 1
encap channel create 1 ppp
encap channel bind 1 de1.1
eth port srcmac 1 00.00.00.00.00.08
eth flow create 1 nop
eth flow egress destmac 1 00.00.00.00.00.01
encap hdlc link flowbind 1 1 
encap hdlc link traffic txenable 1
encap hdlc link traffic rxenable 1
ppp link phase 1 networkactive
eth flow egress vlan 1 1 0.0.4 none
eth flow ingress vlan add 1 1 0.0.4 none

pdh de1 framing 2 e1_unframed
pdh de1 timing 2 system 1
encap channel create 2 ppp
encap channel bind 2 de1.2
eth flow create 2 nop
eth flow egress destmac 2 00.00.00.00.00.02
encap hdlc link flowbind 2 2 
encap hdlc link traffic txenable 2
encap hdlc link traffic rxenable 2
ppp link phase 2 networkactive
eth flow egress vlan 2 1 0.0.5 none
eth flow ingress vlan add 2 1 0.0.5 none

*/

int  Udr_ce1_test_byport(uint8 cardId, uint32 port1, uint32 vlan1)
{
	tAtEthVlanTagFhn vlanTag;
	uint32 port1ID;
	uint32 channel,flow;
	eAtRet ret = cAtOk;
	uint8 smac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint8 slotId = cardId;
	uint32 arg[10]={0};
	eAtEthPortInterface EncapTypeBuf = cAtEthPortInterface1000BaseX;

	printf(" config %d \r\n",port1);
	
	ret = Udr_at_eth_port_mode_set(slotId, &EncapTypeBuf);
		if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);

	arg[0] = 8;/*cAtPdhE1UnFrm*/
	ret = Udr_ce1_port_framer_mode_set(slotId,port1,arg);
		if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);

	arg[0] = 1;/*cAtTimingModeSys*/
	arg[1] = 1;/* only used in slave*/
	ret = Udr_ce1_port_clock_mode_set(slotId,port1,arg);
		if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	channel = port1;
	arg[0] = 1;/*cAtHdlcFrmPppCreate*/
	ret = Udr_ce1_port_channel_create(slotId,channel,arg);
		if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	channel = port1;
	arg[0] = port1;/*cAtHdlcFrmPppCreate*/
	ret = Udr_ce1_port_channel_bind_port(slotId,channel,arg);
		if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	ret = Udr_at_eth_port_smac_set(slotId,1,1,smac);
		if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	flow = port1;
	arg[0] = 0;/*cAtEthFlowNpoPpp*/
	ret = Udr_ce1_port_flow_create(slotId,flow,arg);
		if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	flow = port1;
	dmac[5] = port1;
	ret = Udr_ce1_port_flow_egress_dmac_set(slotId,flow,dmac);
		if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	channel = port1;
	flow = port1;
	ret = Udr_ce1_port_channel_bind_flow(slotId,channel,flow);
		if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	channel = port1;
	arg[0] = 1;/*enable channel tx*/
	ret = Udr_ce1_port_channel_txenable(slotId,channel,arg);
		if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	channel = port1;
	arg[0] = 1;/*enable channel rx*/
	ret = Udr_ce1_port_channel_rxenable(slotId,channel,arg);
		if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	channel = port1;
	arg[0] = 5;/*cAtPppLinkPhaseNetworkActive*/
	ret = Udr_ce1_port_channel_ppp_enable(slotId,channel,arg);
		if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	flow = port1;
	vlanTag.priority = 0;
	vlanTag.cfi = 0;
	vlanTag.vlanId = vlan1;
	ret = Udr_ce1_port_flow_egress_vlan_set(slotId,1,flow, &vlanTag);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	flow = port1;
	vlanTag.priority = 0;
	vlanTag.cfi = 0;
	vlanTag.vlanId = vlan1;
	ret = Udr_ce1_port_flow_set_vlan(slotId,1,flow, &vlanTag);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);

	return ret;

}


/*
encap hdlc link traffic txdisable 1
encap hdlc link traffic rxdisable 1
ppp link phase 1 dead
encap hdlc link flowbind 1 none
encap channel bind 1 none
encap channel delete 1
eth flow delete 1
*/
int  Udr_ce1_test_delete_byport(uint8 cardId, uint32 port1, uint32 vlan1)
{
	tAtEthVlanTagFhn vlanTag;
	uint32 port1ID;
	uint32 channel,flow;
	eAtRet ret = cAtOk;
	uint8 smac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint32 arg[10]={0};
	uint8 slotId= cardId;


		printf(" delete %d \r\n",port1);
	
	channel = port1;
	arg[0] = 0;/*disable channel tx*/
	ret = Udr_ce1_port_channel_txenable(slotId,channel,arg);
			if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	channel = port1;
	arg[0] = 0;/*disable channel rx*/
	ret = Udr_ce1_port_channel_rxenable(slotId,channel,arg);
			if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	channel = port1;
	arg[0] = 1;/*cAtPppLinkPhaseDead*/
	ret = Udr_ce1_port_channel_ppp_enable(slotId,channel,arg);
			if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	
	channel = port1;
	arg[0] = port1;/*cAtHdlcFrmPppCreate*/
	ret = Udr_ce1_port_channel_unbind_port(slotId,channel,arg);
			if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	channel = port1;
	flow = port1;
	ret = Udr_ce1_port_channel_unbind_flow(slotId,channel, &flow);
		if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);


	channel = port1;
	arg[0] = 1;/*cAtHdlcFrmPppCreate*/
	ret = Udr_ce1_port_channel_Delete(slotId,channel,arg);
		if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	flow = port1;
	arg[0] = 0;/*cAtEthFlowNpoPpp*/
	ret = Udr_ce1_port_flow_del(slotId,flow,arg);
		if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);

	
	return ret;

}


/*
Udr_ce1_test_byts 1,1,0xffff,1
Udr_ce1_test_byts 1,2,0xffff,2

Udr_ce1_test_byts 2,1,0xffff,1
Udr_ce1_test_byts 2,2,0xffff,2

*/
int  Udr_ce1_test_byts(uint8 cardId, uint32 port1, uint32 timeSlot, uint32 vlan1)
{
	tAtPdhDs0PortFhn E1TsPort={0};
	tAtEthVlanTagFhn vlanTag;
	uint32 port1ID;
	uint32 channel,flow;
	eAtRet ret = cAtOk;
	uint8 smac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint32 arg[10]={0};
	eAtEthPortInterface EncapTypeBuf = cAtEthPortInterface1000BaseX;
	uint8 slotId=cardId;

	
	ret = Udr_at_eth_port_mode_set(slotId, &EncapTypeBuf);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	arg[0] = 9;/*cAtPdhE1Frm*/
	ret = Udr_ce1_port_framer_mode_set(slotId,port1,arg);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);

	arg[0] = 1;/*cAtTimingModeSys*/
	arg[1] = 1;/* only used in slave*/
	ret = Udr_ce1_port_clock_mode_set(slotId,port1,arg);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	channel = port1;
	arg[0] = 1;/*cAtHdlcFrmPppCreate*/
	ret = Udr_ce1_port_channel_create(slotId,channel,arg);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	

/* main diffirent from E1 */
		channel = port1;
		E1TsPort.e1PortId = port1;
		E1TsPort.timeslotBitmap= timeSlot;

	 	ret = Udr_ce1_port_create_port_ts(slotId,channel, &E1TsPort);
		printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	 	ret = Udr_ce1_port_channel_bind_port_ts(slotId,channel, &E1TsPort);
		printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
/**/	
	
	ret = Udr_at_eth_port_smac_set(slotId,1,1,smac);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	flow = port1;
	arg[0] = 0;/*cAtEthFlowNpoPpp*/
	ret = Udr_ce1_port_flow_create(slotId,flow,arg);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	flow = port1;
	dmac[5] = port1;
	ret = Udr_ce1_port_flow_egress_dmac_set(slotId,flow,dmac);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	channel = port1;
	flow = port1;
	ret = Udr_ce1_port_channel_bind_flow(slotId,channel,flow);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	channel = port1;
	arg[0] = 1;/*enable channel tx*/
	ret = Udr_ce1_port_channel_txenable(slotId,channel,arg);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	channel = port1;
	arg[0] = 1;/*enable channel rx*/
	ret = Udr_ce1_port_channel_rxenable(slotId,channel,arg);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	channel = port1;
	arg[0] = 5;/*cAtPppLinkPhaseNetworkActive*/
	ret = Udr_ce1_port_channel_ppp_enable(slotId,channel,arg);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	flow = port1;
	vlanTag.priority = 0;
	vlanTag.cfi = 0;
	vlanTag.vlanId = vlan1;
	ret = Udr_ce1_port_flow_egress_vlan_set(slotId,1,flow, &vlanTag);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	flow = port1;
	vlanTag.priority = 0;
	vlanTag.cfi = 0;
	vlanTag.vlanId = vlan1;
	ret = Udr_ce1_port_flow_set_vlan(slotId,1,flow, &vlanTag);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	return ret;

}

/*
Udr_ce1_test_delete_byts 1,1,0xffff,1
Udr_ce1_test_delete_byts 1,2,0xffff,2

Udr_ce1_test_delete_byts 2,1,0xffff,1
Udr_ce1_test_delete_byts 2,2,0xffff,2

encap hdlc link traffic txdisable 1
encap hdlc link traffic rxdisable 1
ppp link phase 1 dead
encap hdlc link flowbind 1 none
encap channel bind 1 none
encap channel delete 1
eth flow delete 1
*/
int  Udr_ce1_test_delete_byts(uint8 cardId,uint32 port1, uint32 timeSlot, uint32 vlan1)
{
	tAtPdhDs0PortFhn E1TsPort={0};
	tAtEthVlanTagFhn vlanTag;
	uint32 port1ID;
	uint32 channel,flow;
	eAtRet ret = cAtOk;
	uint8 smac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint32 arg[10]={0};
	uint8 slotId= cardId;
	
	channel = port1;
	arg[0] = 0;/*disable channel tx*/
	ret = Udr_ce1_port_channel_txenable(slotId,channel,arg);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	channel = port1;
	arg[0] = 0;/*disable channel rx*/
	ret = Udr_ce1_port_channel_rxenable(slotId,channel,arg);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	channel = port1;
	arg[0] = 1;/*cAtPppLinkPhaseDead*/
	ret = Udr_ce1_port_channel_ppp_enable(slotId,channel,arg);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
/* main diffirent from E1 */
		channel = port1;
		E1TsPort.e1PortId = port1;
		E1TsPort.timeslotBitmap= timeSlot;
	 	ret = Udr_ce1_port_channel_unbind_port_ts(slotId,channel, &E1TsPort);
		printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);

	 	ret = Udr_ce1_port_delete_port_ts(slotId,channel, &E1TsPort);
		printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
		
/**/	

	channel = port1;
	flow = port1;
	ret = Udr_ce1_port_channel_unbind_flow(slotId,channel, &flow);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);


	channel = port1;
	arg[0] = 1;/*cAtHdlcFrmPppCreate*/
	ret = Udr_ce1_port_channel_Delete(slotId,channel,arg);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	flow = port1;
	arg[0] = 0;/*cAtEthFlowNpoPpp*/
	ret = Udr_ce1_port_flow_del(slotId,flow,arg);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);

	
	return ret;

}

/* test one FPGA two ports */
int Udr_ce1_test_bycard( uint8 cardId )
{
	int port=1;
	int vlan=1;

	for(port=1;port<=16;port++)
	{
		Udr_ce1_test_byport(cardId, port,  vlan);
		vlan++;
	}

	return 0;
	
}

int Udr_ce1_test_delete_bycard( uint8 cardId )
{
	int port=1;
	int vlan=1;

	for(port=1;port<=16;port++)
	{
		Udr_ce1_test_delete_byport(cardId, port,  vlan);
		vlan++;
	}

	return 0;
	
}

int Udr_ce1_test_flowvlan(uint8 cardId, uint32 flow, uint16 vlan, uint8 priority)
{
	tAtEthVlanTagFhn vlanTag;
	uint32 port1ID;
	eAtRet ret = cAtOk;

	vlanTag.priority = priority;
	vlanTag.cfi = 0;
	vlanTag.vlanId = vlan;

	printf("flow[%d],vlanid[%d],vlanpri[%d] \r\n",flow, vlanTag.vlanId, vlanTag.priority);
	ret = Udr_ce1_port_flow_set_vlan(cardId,1,flow, &vlanTag);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	return ret;

}


/*
Udr_ce1_test_mlppp 1,1,4,1,11
Udr_ce1_test_mlppp 1,5,8,2,22
*/
int Udr_ce1_test_mlppp(uint8 cardId, uint32 e1PortStart, uint32 e1PortEnd, uint32 mlBundleId, uint32 vlan)
{
	int e1Port= e1Port;
	int ret=0,ch=0;
	uint32 channel,flow;
	uint8 smac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	tAtSdhTtiFhn ttiInfo;
	int arg[10];
	tAtEthVlanTagFhn vlanTag;
	eAtEthPortInterface EncapTypeBuf = cAtEthPortInterface1000BaseX;
	eAtPdhDe1FrameType frameMode;
	tAtTimeModeFhn timeMode;
	uint8 slotId= cardId;

	ret = Udr_at_eth_port_smac_set(slotId,1,1,smac);
	if(ret !=0)
	printf("%s,#%d,ret=%x,e1PortStart=%d,e1PortEnd=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,e1PortStart,e1PortEnd,mlBundleId,vlan);


	ret = Udr_ce1_mlppp_bundle_create(slotId,mlBundleId,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,e1PortStart=%d,e1PortEnd=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,e1PortStart,e1PortEnd,mlBundleId,vlan);

	/* sdh map */
	for(ch=e1PortStart; ch<=e1PortEnd;ch++)
	{
		/* E1 framer mode */
		frameMode = cAtPdhE1UnFrm;
		ret = Udr_ce1_port_framer_mode_set(slotId, ch, &frameMode);
		if(ret !=0)
		printf("%s,#%d,ret=%x,e1PortStart=%d,e1PortEnd=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,e1PortStart,e1PortEnd,mlBundleId,vlan);

		/* clock mode */
		timeMode.TimeModen =  cAtTimingModeSysFhn;
		timeMode.refPort = 1;
		ret = Udr_ce1_port_clock_mode_set(slotId, ch, &timeMode);
		if(ret !=0)
		printf("%s,#%d,ret=%x,e1PortStart=%d,e1PortEnd=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,e1PortStart,e1PortEnd,mlBundleId,vlan);		

		/* create channel */
		channel = ch;
		arg[0] = 1;/*cAtHdlcFrmPppCreate*/
		ret = Udr_ce1_port_channel_create(slotId, channel,arg);
		if(ret !=0)
		printf("%s,#%d,ret=%x,e1PortStart=%d,e1PortEnd=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,e1PortStart,e1PortEnd,mlBundleId,vlan);			

		channel = ch;
		arg[0] = ch;/*cAtHdlcFrmPppCreate*/
		ret = Udr_ce1_port_channel_bind_port(slotId, channel,arg);
		if(ret !=0)
		printf("%s,#%d,ret=%x,e1PortStart=%d,e1PortEnd=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,e1PortStart,e1PortEnd,mlBundleId,vlan);

		channel = ch;
		ret = Udr_ce1_mlppp_bundle_add_channel(slotId, mlBundleId,channel);
		if(ret !=0)
		printf("%s,#%d,ret=%x,e1PortStart=%d,e1PortEnd=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,e1PortStart,e1PortEnd,mlBundleId,vlan);

	}


	flow = mlBundleId;
	arg[0] = 0;/*cAtEthFlowNpoPpp*/
	ret = Udr_ce1_port_flow_create(slotId, flow,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,e1PortStart=%d,e1PortEnd=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,e1PortStart,e1PortEnd,mlBundleId,vlan);

	flow = mlBundleId;
	dmac[5] = ch;
	ret = Udr_ce1_port_flow_egress_dmac_set(slotId, flow,dmac);
	if(ret !=0)
	printf("%s,#%d,ret=%x,e1PortStart=%d,e1PortEnd=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,e1PortStart,e1PortEnd,mlBundleId,vlan);

	for(ch=e1PortStart; ch<=e1PortEnd;ch++)
	{
		channel = ch;
		arg[0] = 1;/*enable channel tx*/
		ret = Udr_ce1_port_channel_txenable(slotId,channel,arg);
		if(ret !=0)
		printf("%s,#%d,ret=%x,e1PortStart=%d,e1PortEnd=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,e1PortStart,e1PortEnd,mlBundleId,vlan);			

		channel = ch;
		arg[0] = 1;/*enable channel rx*/
		ret = Udr_ce1_port_channel_rxenable(slotId,channel,arg);
		if(ret !=0)
		printf("%s,#%d,ret=%x,e1PortStart=%d,e1PortEnd=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,e1PortStart,e1PortEnd,mlBundleId,vlan);	

		channel = ch;
		arg[0] = 5;/*cAtPppLinkPhaseNetworkActive*/
		ret = Udr_ce1_port_channel_ppp_enable(slotId,channel,arg);
		if(ret !=0)
		printf("%s,#%d,ret=%x,e1PortStart=%d,e1PortEnd=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,e1PortStart,e1PortEnd,mlBundleId,vlan);
		
	}


	flow = mlBundleId;
	vlanTag.priority = 0;
	vlanTag.cfi = 0;
	vlanTag.vlanId = vlan;
	ret = Udr_ce1_port_flow_egress_vlan_set(slotId,1, flow, &vlanTag);
	if(ret !=0)
	printf("%s,#%d,ret=%x,e1PortStart=%d,e1PortEnd=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,e1PortStart,e1PortEnd,mlBundleId,vlan);

	flow = mlBundleId;
	vlanTag.priority = 0;
	vlanTag.cfi = 0;
	vlanTag.vlanId = vlan;
	ret = Udr_ce1_port_flow_set_vlan(slotId, 1, flow, &vlanTag);
	if(ret !=0)
	printf("%s,#%d,ret=%x,e1PortStart=%d,e1PortEnd=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,e1PortStart,e1PortEnd,mlBundleId,vlan);

	flow = mlBundleId;
	ret = Udr_ce1_mlppp_bundle_bind_flow(slotId,mlBundleId,flow);
	if(ret !=0)
	printf("%s,#%d,ret=%x,e1PortStart=%d,e1PortEnd=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,e1PortStart,e1PortEnd,mlBundleId,vlan);
	
	arg[0]=1;
	ret = Udr_ce1_mlppp_bundle_enable(slotId,mlBundleId,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,e1PortStart=%d,e1PortEnd=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,e1PortStart,e1PortEnd,mlBundleId,vlan);
	
	return ret;
}

/*
Udr_ce1_test_mlppp_delete 1,1,3,1
Udr_ce1_test_mlppp_delete 1,4,6,2

// remove links and flows out of bundle before delete bundle 
ppp bundle remove flow 1 1
ppp bundle remove link 1 1-3
ppp bundle delete 1

// Delete ethernet flow 
eth flow delete 1

// Unbind encap channel out of physical before delete 
encap channel bind 1-3 none
encap channel delete 1-3


*/
int Udr_ce1_test_mlppp_delete(uint8 cardId, uint32 e1PortStart, uint32 e1PortEnd, uint32 mlBundleId)
{
	int ret=0,arg[10];
	uint32 flowId = mlBundleId,channel = mlBundleId;
	uint32 i=e1PortStart;
	uint8 slotId= cardId;

	ret = Udr_ce1_mlppp_bundle_unbind_flow(slotId,flowId, mlBundleId);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);

	for(i=e1PortStart; i<=e1PortEnd; i++)
	{
		ret = Udr_ce1_mlppp_bundle_delete_channel(slotId,mlBundleId,i);
		if(ret !=0)
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	}

	ret = Udr_ce1_mlppp_bundle_Delete(slotId,mlBundleId);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);


	for(i=e1PortStart; i<=e1PortEnd; i++)
	{
		channel = i;
		arg[0] = i;/*cAtHdlcFrmPppCreate*/
		ret = Udr_ce1_port_channel_unbind_port(slotId,channel,arg);
		if(ret !=0)
		printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);

	
		ret = Udr_ce1_port_channel_unbind_flow(slotId, i, &flowId);
		if(ret !=0)
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);

		arg[0] = 1;/*cAtHdlcFrmPppCreate*/
		ret = Udr_ce1_port_channel_Delete(slotId,i,arg);
		if(ret !=0)
		printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);

	}

	ret = Udr_ce1_port_flow_del(slotId, flowId, arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);


	return 0;
}

/*
master
Udr_ce1_test_clock 1,1,2,2
slave
Udr_ce1_test_clock 1,10,2,2
*/
int Udr_ce1_test_clock(uint8 cardId, uint32 timeMode, uint32 port, uint32 refPort)
{
	uint8 slotId=cardId;
	tAtTimeModeFhn EncapTypeBuf;

	EncapTypeBuf.TimeModen = timeMode;
	EncapTypeBuf.refPort = refPort;
	
	Udr_ce1_port_clock_mode_set(slotId, port, &EncapTypeBuf);
}


int Udr_pos_test_byport(uint8 cardId, uint32 cposPort, uint32 channelID, uint32 vlan)
{
	int ret=0;
	uint32 channel,flow;
	uint8 smac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	tAtSdhTtiFhn ttiInfo;
	int arg[10];
	tAtEthVlanTagFhn vlanTag;
	eAtPdhDe1FrameType frameMode;
	tAtTimeModeFhn timeMode;
	uint32 fpgaID =  (cposPort <= 4) ? 1:2; 
	uint8 slotId=cardId;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}
	
	/* relese loopback moden */
	arg[0] = 0;
	ret = Udr_cpos_port_loopback_set(slotId, cposPort,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);
	
	/*
		"abc" 16B and  null padding
	*/
	sprintf(ttiInfo.message,"s");
	ttiInfo.mode = 0;
	ttiInfo.paddingMode = 0;
	ret = Udr_cpos_port_tti_tx_set(slotId, cposPort,&ttiInfo);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);


	sprintf(ttiInfo.message,"s");
	ttiInfo.mode = 0;
	ttiInfo.paddingMode = 0;		
	ret = Udr_cpos_port_tti_rx_set(slotId, cposPort,&ttiInfo);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);

	
	/* sdh map */
	ret = Udr_pos_stm1_port_sdh_map(slotId,cposPort);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);


	/* create channel */
	channel = channelID;
	arg[0] = 1;/*cAtHdlcFrmPppCreate*/
	ret = Udr_cpos_port_channel_create(slotId, fpgaID, channel, arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);
	
	channel = channelID;
	arg[0] = cposPort;/*cAtHdlcFrmPppCreate*/
	ret = Udr_pos_stm1_port_channel_bind_port(slotId, channel,cposPort);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);
	
	smac[5] = smac[5] + fpgaID;
	ret = Udr_at_eth_port_smac_set(slotId, fpgaID, 1, smac);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);

	flow = channelID;
	arg[0] = 0;/*cAtEthFlowNpoPpp*/
	ret = Udr_cpos_port_flow_create(slotId, fpgaID,flow,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);

	sprintf(ttiInfo.message,"s");
	ttiInfo.mode = 0;
	ttiInfo.paddingMode = 0;
	ret = Udr_cpos_port_path_vc4_tti_tx_set(slotId, cposPort,&ttiInfo);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);

	sprintf(ttiInfo.message,"s");
	ttiInfo.mode = 0;
	ttiInfo.paddingMode = 0;
	ret = Udr_cpos_port_path_vc4_tti_rx_set(slotId, cposPort, &ttiInfo);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);
	
	flow = channelID;
	dmac[5] = cposPort;
	ret = Udr_cpos_port_flow_egress_dmac_set(slotId, fpgaID, flow, dmac);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);
	
	channel = channelID;
	flow = channelID;
	ret = Udr_cpos_port_channel_bind_flow(slotId, fpgaID, channel,flow);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);
	
	channel = channelID;
	arg[0] = 1;/*enable channel tx*/
	ret = Udr_cpos_port_channel_txenable(slotId, fpgaID,channel,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);
	
	channel = channelID;
	arg[0] = 1;/*enable channel rx*/
	ret = Udr_cpos_port_channel_rxenable(slotId, fpgaID,channel,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);
	
	channel = channelID;
	arg[0] = 5;/*cAtPppLinkPhaseNetworkActive*/
	ret = Udr_cpos_port_channel_ppp_enable(slotId, fpgaID, channel,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);
	
	flow = channelID;
	vlanTag.priority = 0;
	vlanTag.cfi = 0;
	vlanTag.vlanId = vlan;
	ret = Udr_cpos_port_flow_egress_vlan_set(slotId, fpgaID, 1,flow, &vlanTag);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);
	
	flow = channelID;
	vlanTag.priority = 0;
	vlanTag.cfi = 0;
	vlanTag.vlanId = vlan;
	ret = Udr_cpos_port_flow_set_vlan(slotId, fpgaID, 1, flow, &vlanTag);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);

	
	return ret;
}

int Udr_pos_stm4_test_byport(uint8 cardId, uint32 cposPort, uint32 channelID, uint32 vlan,uint32 vc4Id)
{
	int ret=0;
	uint32 channel,flow;
	uint8 smac[]={0x00,0x04,0x67,0x00,0x00,0x01};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	tAtSdhTtiFhn ttiInfo;
	int arg[10];
	tAtEthVlanTagFhn vlanTag;
	eAtPdhDe1FrameType frameMode;
	tAtTimeModeFhn timeMode;
	uint32 fpgaID =  (cposPort <= 4) ? 1:2; 
	uint8 slotId=cardId;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}
	
	/* relese loopback moden */
	arg[0] = 0;
	ret = Udr_cpos_port_loopback_set(slotId, cposPort,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);
	
	/*
		"abc" 16B and  null padding
	*/
	sprintf(ttiInfo.message,"s");
	ttiInfo.mode = 0;
	ttiInfo.paddingMode = 0;
	ret = Udr_cpos_port_tti_tx_set(slotId, cposPort,&ttiInfo);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);


	sprintf(ttiInfo.message,"s");
	ttiInfo.mode = 0;
	ttiInfo.paddingMode = 0;		
	ret = Udr_cpos_port_tti_rx_set(slotId, cposPort,&ttiInfo);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);

	/* create channel */
	channel = channelID;
	arg[0] = 1;/*cAtHdlcFrmPppCreate*/
	ret = Udr_cpos_port_channel_create(slotId, fpgaID, channel, arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);


	channel = channelID;
	arg[0] = cposPort;/*cAtHdlcFrmPppCreate*/
	ret = Udr_pos_stm4_port_channel_bind_port(slotId, cposPort,channel,vc4Id);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);


	sprintf(ttiInfo.message,"s");
	ttiInfo.mode = 0;
	ttiInfo.paddingMode = 0;
	ret = Udr_pos_stm4_port_path_vc4_tti_tx_set(slotId, cposPort,vc4Id,&ttiInfo);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,vc4Id=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,vc4Id,channelID,vlan);
	

	sprintf(ttiInfo.message,"s");
	ttiInfo.mode = 0;
	ttiInfo.paddingMode = 0;
	ret = Udr_pos_stm4_port_path_vc4_tti_tx_set(slotId, cposPort, vc4Id, &ttiInfo);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,vc4Id=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,vc4Id,channelID,vlan);
	

	

	flow = channelID;
	arg[0] = 0;/*cAtEthFlowNpoPpp*/
	ret = Udr_cpos_port_flow_create(slotId, fpgaID,flow,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);


	smac[5] = smac[5] + fpgaID;
	ret = Udr_at_eth_port_smac_set(slotId, fpgaID, 1, smac);
	if(ret !=0)
	printf("%s,#%d,ret=%x,vid=%d\r\n",__FILE__,__LINE__,ret,vlan);

	
	flow = channelID;
	dmac[5] = cposPort;
	ret = Udr_cpos_port_flow_egress_dmac_set(slotId, fpgaID, flow, dmac);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);
	
	channel = channelID;
	flow = channelID;
	ret = Udr_cpos_port_channel_bind_flow(slotId, fpgaID, channel,flow);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);
	
	channel = channelID;
	arg[0] = 1;/*enable channel tx*/
	ret = Udr_cpos_port_channel_txenable(slotId, fpgaID,channel,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);
	
	channel = channelID;
	arg[0] = 1;/*enable channel rx*/
	ret = Udr_cpos_port_channel_rxenable(slotId, fpgaID,channel,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);
	
	channel = channelID;
	arg[0] = 5;/*cAtPppLinkPhaseNetworkActive*/
	ret = Udr_cpos_port_channel_ppp_enable(slotId, fpgaID, channel,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);
	
	flow = channelID;
	vlanTag.priority = 0;
	vlanTag.cfi = 0;
	vlanTag.vlanId = vlan;
	ret = Udr_cpos_port_flow_egress_vlan_set(slotId, fpgaID, 1,flow, &vlanTag);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);
	
	flow = channelID;
	vlanTag.priority = 0;
	vlanTag.cfi = 0;
	vlanTag.vlanId = vlan;
	ret = Udr_cpos_port_flow_set_vlan(slotId, fpgaID, 1, flow, &vlanTag);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,channelID,vlan);

	
	return ret;
}


/*  test two FPGA port1  */
int Udr_pos_test_two_stm1( uint8 cardId, uint32 portA,uint32 portB)
{
	uint8 arg[10]={0};
	int ch=1,vlan=1,ret=0;
	eAtEthPortInterface EncapTypeBuf = cAtEthPortInterface1000BaseX;
	uint8 slotId=cardId;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	/* stm1 moden */
	arg[0] = 0;
	ret = Udr_cpos_port_linerate_set(slotId, portA,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	
	/* sdh moden */
	arg[0] = 0;
	ret = Udr_cpos_port_linemode_set(slotId, portA,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);

	vlan = 11;
	Udr_pos_test_byport(slotId,  portA,  1,  vlan);

	/* stm1 moden */
	arg[0] = 0;
	ret = Udr_cpos_port_linerate_set(slotId, portB,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	
	/* sdh moden */
	arg[0] = 0;
	ret = Udr_cpos_port_linemode_set(slotId, portB,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	
	vlan=22;
	Udr_pos_test_byport(slotId, portB,  1,  vlan);

	return 0;
}


int  Udr_pos_test_two_stm1_delete( uint8 cardId)
{
	tAtEthVlanTagFhn vlanTag;
	uint32 port1ID;
	uint32 channel,flow;
	eAtRet ret = cAtOk;
	uint8 smac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint32 arg[10]={0};
	uint8 slotId=cardId;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}
	
	//channel = cposPort;
	arg[0] = 0;/*disable channel tx*/
	ret = Udr_cpos_port_channel_txenable(slotId, 1, 1,arg);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	//channel = cposPort;
	arg[0] = 0;/*disable channel rx*/
	ret = Udr_cpos_port_channel_rxenable(slotId, 1, 1,arg);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	//channel = cposPort;
	arg[0] = 1;/*cAtPppLinkPhaseDead*/
	ret = Udr_cpos_port_channel_ppp_enable(slotId, 1, 1,arg);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	
	//channel = cposPort;
	ret = Udr_cpos_port_channel_unbind_port(slotId, 1, 1,arg);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	//channel = cposPort;
	flow = 1;
	ret = Udr_cpos_port_channel_unbind_flow(slotId, 1, 1, &flow);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);


	//channel = cposPort;
	ret = Udr_cpos_port_channel_delete(slotId, 1, 1, arg);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	flow = 1;
	arg[0] = 0;/*cAtEthFlowNpoPpp*/
	ret = Udr_cpos_port_flow_del(slotId, 1, 1,arg);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);


	/* FPGA 2 */

	//channel = cposPort;
	arg[0] = 0;/*disable channel tx*/
	ret = Udr_cpos_port_channel_txenable(slotId, 2, 1,arg);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	//channel = cposPort;
	arg[0] = 0;/*disable channel rx*/
	ret = Udr_cpos_port_channel_rxenable(slotId, 2, 1,arg);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	//channel = cposPort;
	arg[0] = 1;/*cAtPppLinkPhaseDead*/
	ret = Udr_cpos_port_channel_ppp_enable(slotId, 2, 1,arg);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	
	//channel = cposPort;
	ret = Udr_cpos_port_channel_unbind_port(slotId, 2, 1,arg);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	//channel = cposPort;
	flow = 1;
	ret = Udr_cpos_port_channel_unbind_flow(slotId, 2, 1, &flow);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);


	//channel = cposPort;
	ret = Udr_cpos_port_channel_delete(slotId, 2, 1, arg);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	flow = channel;
	arg[0] = 0;/*cAtEthFlowNpoPpp*/
	ret = Udr_cpos_port_flow_del(slotId, 2, 1,arg);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);


	printf("have delete \r\n");
	return ret;

}

/*  test two FPGA port1  */
int Udr_pos_test_two_stm4( uint8 cardId )
{
	int arg[10]={0};
	int ch=1,vlan=1,ret=0,fpgaID=1;
	eAtEthPortInterface EncapTypeBuf = cAtEthPortInterface1000BaseX;
	uint8 slotId=cardId;
	uint32 vc4Id=0;
	uint8 smac[]={0x00,0x04,0x67,0x00,0x00,0x01};

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	/* stm4 moden */
	arg[0] = 1;
	ret = Udr_cpos_port_linerate_set(slotId, 1,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	
	/* sdh moden */
	arg[0] = 0;
	ret = Udr_cpos_port_linemode_set(slotId, 1,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);

	/* sdh map */
	ret = Udr_pos_stm4_port_sdh_map(slotId,1);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);


	vc4Id=1;
	vlan = 10; 
	for(vc4Id=1;vc4Id<5;vc4Id++)
	{
		ch = vc4Id;
		ret = Udr_pos_stm4_test_byport(slotId, 1, ch, vlan, vc4Id);
		if(ret !=0)
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		vlan ++;
	}
	
	/* stm1 moden */
	arg[0] = 1;
	ret = Udr_cpos_port_linerate_set(slotId, 5,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	
	/* sdh moden */
	arg[0] = 0;
	ret = Udr_cpos_port_linemode_set(slotId, 5,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);

	/* sdh map */
	ret = Udr_pos_stm4_port_sdh_map(slotId,5);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);


	vc4Id=1;
	vlan = 20;
	for(vc4Id=1;vc4Id<5;vc4Id++)
	{
		ch = vc4Id;
		ret = Udr_pos_stm4_test_byport(slotId, 5, ch, vlan, vc4Id);
		if(ret !=0)
		printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
		vlan ++;
	}

	return 0;
}


int  Udr_pos_test_two_stm4_delete( uint8 cardId)
{
	tAtEthVlanTagFhn vlanTag;
	uint32 port1ID;
	uint32 channel,flow;
	eAtRet ret = cAtOk;
	uint8 smac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint32 arg[10]={0};
	uint8 slotId=cardId;



	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	for(channel=1;channel<5;channel++)
	{
		//channel = cposPort;
		arg[0] = 0;/*disable channel tx*/
		ret = Udr_cpos_port_channel_txenable(slotId, 1, channel,arg);
		if(ret !=0)
		printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
		
		//channel = cposPort;
		arg[0] = 0;/*disable channel rx*/
		ret = Udr_cpos_port_channel_rxenable(slotId, 1, channel,arg);
		if(ret !=0)
		printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
		
		//channel = cposPort;
		arg[0] = 1;/*cAtPppLinkPhaseDead*/
		ret = Udr_cpos_port_channel_ppp_enable(slotId, 1, channel,arg);
		if(ret !=0)
		printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
		
		
		//channel = cposPort;
		ret = Udr_cpos_port_channel_unbind_port(slotId, 1, channel,arg);
		if(ret !=0)
		printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
		
		//channel = cposPort;
		flow = 1;
		ret = Udr_cpos_port_channel_unbind_flow(slotId, 1, channel, &flow);
		if(ret !=0)
		printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);


		//channel = cposPort;
		ret = Udr_cpos_port_channel_delete(slotId, 1, channel, arg);
		if(ret !=0)
		printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
		
		flow = 1;
		arg[0] = 0;/*cAtEthFlowNpoPpp*/
		ret = Udr_cpos_port_flow_del(slotId, 1, channel,arg);
		if(ret !=0)
		printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);


		/* FPGA 2 */

		//channel = cposPort;
		arg[0] = 0;/*disable channel tx*/
		ret = Udr_cpos_port_channel_txenable(slotId, 2, channel,arg);
		if(ret !=0)
		printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
		
		//channel = cposPort;
		arg[0] = 0;/*disable channel rx*/
		ret = Udr_cpos_port_channel_rxenable(slotId, 2, channel,arg);
		if(ret !=0)
		printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
		
		//channel = cposPort;
		arg[0] = 1;/*cAtPppLinkPhaseDead*/
		ret = Udr_cpos_port_channel_ppp_enable(slotId, 2, channel,arg);
		if(ret !=0)
		printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
		
		
		//channel = cposPort;
		ret = Udr_cpos_port_channel_unbind_port(slotId, 2, channel,arg);
		if(ret !=0)
		printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
		
		//channel = cposPort;
		flow = 1;
		ret = Udr_cpos_port_channel_unbind_flow(slotId, 2, channel, &flow);
		if(ret !=0)
		printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);


		//channel = cposPort;
		ret = Udr_cpos_port_channel_delete(slotId, 2, channel, arg);
		if(ret !=0)
		printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
		
		flow = channel;
		arg[0] = 0;/*cAtEthFlowNpoPpp*/
		ret = Udr_cpos_port_flow_del(slotId, 2, channel,arg);
		if(ret !=0)
		printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);

	}
	printf("have delete \r\n");
	return ret;

}


int Udr_cpos_test_byport(uint8 cardId, uint32 cposPort, uint32 cposPortChannel, uint32 channelID, uint32 vlan)
{
	int ret=0;
	uint32 channel,flow;
	uint8 smac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	tAtSdhTtiFhn ttiInfo;
	int arg[10];
	tAtEthVlanTagFhn vlanTag;
	eAtPdhDe1FrameType frameMode;
	tAtTimeModeFhn timeMode;
	uint32 fpgaID =  (cposPort <= 4) ? 1:2; 
	uint8 slotId=cardId;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}
	
	/* relese loopback moden */
	arg[0] = 0;
	ret = Udr_cpos_port_loopback_set(slotId, cposPort,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	
	/*
		"abc" 16B and  null padding
	*/
	sprintf(ttiInfo.message,"s");
	ttiInfo.mode = 0;
	ttiInfo.paddingMode = 0;
	ret = Udr_cpos_port_tti_tx_set(slotId, cposPort,&ttiInfo);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	

	sprintf(ttiInfo.message,"s");
	ttiInfo.mode = 0;
	ttiInfo.paddingMode = 0;		
	ret = Udr_cpos_port_tti_rx_set(slotId, cposPort,&ttiInfo);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	

	
	/* sdh map */
	ret = Udr_cpos_port_stm1_sdh_map(slotId,cposPort,cposPortChannel);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);

	
	/* E1 framer mode */
	frameMode = cAtPdhE1UnFrm;
	ret = Udr_cpos_e1_framer_mode_set(slotId, cposPort, cposPortChannel, &frameMode);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);

	/* clock mode */
	timeMode.TimeModen =  cAtTimingModeSysFhn;
	timeMode.refPort = 1;
	ret = Udr_cpos_e1_clock_mode_set(slotId, cposPort, cposPortChannel, &timeMode);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	
	/* create channel */
	channel = channelID;
	arg[0] = 1;/*cAtHdlcFrmPppCreate*/
	ret = Udr_cpos_port_channel_create(slotId, fpgaID, channel, arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	

	
	channel = channelID;
	arg[0] = cposPort;/*cAtHdlcFrmPppCreate*/
	ret = Udr_cpos_port_channel_bind_port(slotId, channel,cposPort,cposPortChannel);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	
	smac[5] = smac[5] + fpgaID;
	ret = Udr_at_eth_port_smac_set(slotId, fpgaID, 1, smac);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	


	flow = channelID;
	arg[0] = 0;/*cAtEthFlowNpoPpp*/
	ret = Udr_cpos_port_flow_create(slotId, fpgaID,flow,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	


	sprintf(ttiInfo.message,"s");
	ttiInfo.mode = 0;
	ttiInfo.paddingMode = 0;
	ret = Udr_cpos_port_path_tti_tx_set(slotId, cposPort,cposPortChannel,&ttiInfo);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	


	sprintf(ttiInfo.message,"s");
	ttiInfo.mode = 0;
	ttiInfo.paddingMode = 0;
	ret = Udr_cpos_port_path_tti_rx_set(slotId, cposPort, cposPortChannel, &ttiInfo);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	


	flow = channelID;
	dmac[5] = cposPort;
	ret = Udr_cpos_port_flow_egress_dmac_set(slotId, fpgaID, flow, dmac);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	
	
	channel = channelID;
	flow = channelID;
	ret = Udr_cpos_port_channel_bind_flow(slotId, fpgaID, channel,flow);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	
	
	channel = channelID;
	arg[0] = 1;/*enable channel tx*/
	ret = Udr_cpos_port_channel_txenable(slotId, fpgaID,channel,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	
	
	channel = channelID;
	arg[0] = 1;/*enable channel rx*/
	ret = Udr_cpos_port_channel_rxenable(slotId, fpgaID,channel,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	
	
	channel = channelID;
	arg[0] = 5;/*cAtPppLinkPhaseNetworkActive*/
	ret = Udr_cpos_port_channel_ppp_enable(slotId, fpgaID, channel,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	
	
	flow = channelID;
	vlanTag.priority = 0;
	vlanTag.cfi = 0;
	vlanTag.vlanId = vlan;
	ret = Udr_cpos_port_flow_egress_vlan_set(slotId, fpgaID, 1,flow, &vlanTag);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	
	
	flow = channelID;
	vlanTag.priority = 0;
	vlanTag.cfi = 0;
	vlanTag.vlanId = vlan;
	ret = Udr_cpos_port_flow_set_vlan(slotId, fpgaID, 1, flow, &vlanTag);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);

	return ret;
}



/*
encap hdlc link traffic txdisable 1
encap hdlc link traffic rxdisable 1
ppp link phase 1 dead
encap hdlc link flowbind 1 none
encap channel bind 1 none
encap channel delete 1
eth flow delete 1
*/
int  Udr_cpos_test_byport_delete(uint8 cardId, uint32 cposPort, uint32 channelID, uint32 vlan1)
{
	tAtEthVlanTagFhn vlanTag;
	uint32 port1ID;
	uint32 channel=channelID, flow;
	eAtRet ret = cAtOk;
	uint8 smac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint32 arg[10]={0};
	uint32 fpgaID =  (cposPort <= 4) ? 1:2; 
	uint8 slotId=cardId;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}
	
	//channel = cposPort;
	arg[0] = 0;/*disable channel tx*/
	ret = Udr_cpos_port_channel_txenable(slotId, fpgaID, channel,arg);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	//channel = cposPort;
	arg[0] = 0;/*disable channel rx*/
	ret = Udr_cpos_port_channel_rxenable(slotId, fpgaID, channel,arg);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	//channel = cposPort;
	arg[0] = 1;/*cAtPppLinkPhaseDead*/
	ret = Udr_cpos_port_channel_ppp_enable(slotId, fpgaID, channel,arg);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	
	//channel = cposPort;
	ret = Udr_cpos_port_channel_unbind_port(slotId, fpgaID, channel,arg);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	//channel = cposPort;
	flow = cposPort;
	ret = Udr_cpos_port_channel_unbind_flow(slotId, fpgaID, channel, &flow);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);


	//channel = cposPort;
	ret = Udr_cpos_port_channel_delete(slotId, fpgaID, channel,arg);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	flow = channel;
	arg[0] = 0;/*cAtEthFlowNpoPpp*/
	ret = Udr_cpos_port_flow_del(slotId, fpgaID, flow,arg);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);

	printf("have delete %d\r\n",channelID);
	return ret;

}

int  Udr_cpos_test_byport_mlppp_delete(uint8 cardId, uint32 cposPort, uint32 mlBundleId,uint32 channelID, uint32 vlan1)
{
	tAtEthVlanTagFhn vlanTag;
	uint32 port1ID;
	uint32 channel=channelID, flow;
	eAtRet ret = cAtOk;
	uint8 smac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint32 arg[10]={0};
	uint32 fpgaID =  (cposPort <= 4) ? 1:2; 
	uint8 slotId=cardId;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	
	//channel = cposPort;
	arg[0] = 0;/*disable channel tx*/
	ret = Udr_cpos_port_channel_txenable(slotId, fpgaID, channel,arg);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	//channel = cposPort;
	arg[0] = 0;/*disable channel rx*/
	ret = Udr_cpos_port_channel_rxenable(slotId, fpgaID, channel,arg);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	//channel = cposPort;
	arg[0] = 1;/*cAtPppLinkPhaseDead*/
	ret = Udr_cpos_port_channel_ppp_enable(slotId, fpgaID, channel,arg);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	
	//channel = cposPort;
	ret = Udr_cpos_port_channel_unbind_port(slotId, fpgaID, channel,arg);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);

	ret = Udr_cpos_mlppp_bundle_delete_channel(slotId, fpgaID, mlBundleId, channel);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);

	//channel = cposPort;
	ret = Udr_cpos_port_channel_delete(slotId, fpgaID, channel,arg);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);


	printf("have delete %d\r\n",channelID);
	return ret;

}


/*  test two FPGA port1  */
int Udr_cpos_test_two_stm1( uint8 cardId, uint32 portA,uint32 portB)
{
	uint8 arg[10]={0};
	int ch=1,vlan=1,ret=0;
	eAtEthPortInterface EncapTypeBuf = cAtEthPortInterface1000BaseX;
	uint8 slotId=cardId;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	/* stm1 moden */
	arg[0] = 0;
	ret = Udr_cpos_port_linerate_set(slotId, portA,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	
	/* sdh moden */
	arg[0] = 0;
	ret = Udr_cpos_port_linemode_set(slotId, portA,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);


	for(ch=1;ch<=63;ch++)
	{
		Udr_cpos_test_byport(slotId,  portA,  ch,  vlan,  vlan);
		vlan++;
	}


	/* stm1 moden */
	arg[0] = 0;
	ret = Udr_cpos_port_linerate_set(slotId, portB,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	
	/* sdh moden */
	arg[0] = 0;
	ret = Udr_cpos_port_linemode_set(slotId, portB,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	vlan=1;
	for(ch=1;ch<=63;ch++)
	{
		Udr_cpos_test_byport(slotId, portB,  ch,  vlan,  vlan);
		vlan++;
	}

	return 0;
}

/*  test two FPGA port1  */
int Udr_cpos_test_two_stm1_delete( uint8 cardId, uint32 portA,uint32 portB)
{
	int ch=1,vlan=1;
	uint8 slotId=cardId;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	for(ch=1;ch<=63;ch++)
	{
		Udr_cpos_test_byport_delete(slotId, portA, ch,vlan);
		vlan++;
	}

	vlan=1;
	for(ch=1;ch<=63;ch++)
	{
		Udr_cpos_test_byport_delete(slotId, portB, ch,vlan);
		vlan++;
	}

	return 0;
}

/*
Udr_cpos_test_byts 1,1,1,0xfffff,1
Udr_cpos_test_byts 1,5,1,0xfffff,2

*/
int  Udr_cpos_test_byts(uint8 cardId, uint32 cposPort, uint32 cposPortChannel, uint32 timeSlot, uint32 vlan)
{
	int ret=0,i=0;
	uint32 channel,flow,channelID=vlan,timeslotbitmap=timeSlot;
	uint8 smac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	tAtSdhTtiFhn ttiInfo;
	int arg[10];
	tAtEthVlanTagFhn vlanTag;
	eAtPdhDe1FrameType frameMode;
	tAtTimeModeFhn timeMode;
	uint32 fpgaID =  (cposPort <= 4) ? 1:2; 
	uint8 slotId=cardId;
	eAtEthPortInterface EncapTypeBuf = cAtEthPortInterface1000BaseX;
	/*
	for(i=1; i<64; i++)
	{
		Udr_cpos_port_stm1_sdh_map(cardId,cposPort,i);
	}
	*/
	ret = Udr_at_eth_port_mode_set(slotId, &EncapTypeBuf);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);

	smac[5] = smac[5] + fpgaID;
	ret = Udr_at_eth_port_smac_set(slotId, fpgaID, 1, smac);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	


	/* stm1 moden */
	arg[0] = 0;
	ret = Udr_cpos_port_linerate_set(slotId, cposPort,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	
	/* sdh moden */
	arg[0] = 0;
	ret = Udr_cpos_port_linemode_set(slotId, cposPort,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);

	/* relese loopback moden */
	arg[0] = 0;
	ret = Udr_cpos_port_loopback_set(slotId, cposPort,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	
	/*
		"abc" 16B and  null padding
	*/
	sprintf(ttiInfo.message,"s");
	ttiInfo.mode = 0;
	ttiInfo.paddingMode = 0;
	ret = Udr_cpos_port_tti_tx_set(slotId, cposPort,&ttiInfo);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	

	sprintf(ttiInfo.message,"s");
	ttiInfo.mode = 0;
	ttiInfo.paddingMode = 0;		
	ret = Udr_cpos_port_tti_rx_set(slotId, cposPort,&ttiInfo);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
		
	/* sdh map */
	ret = Udr_cpos_port_stm1_sdh_map(slotId,cposPort,cposPortChannel);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);

	/* E1 framer mode */
	frameMode = cAtPdhE1Frm;
	ret = Udr_cpos_e1_framer_mode_set(slotId, cposPort, cposPortChannel, &frameMode);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);

	/* clock mode */
	timeMode.TimeModen =  cAtTimingModeSysFhn;
	timeMode.refPort = 1;
	ret = Udr_cpos_e1_clock_mode_set(slotId, cposPort, cposPortChannel, &timeMode);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	
	/* create channel */
	channel = channelID;
	arg[0] = 1;/*cAtHdlcFrmPppCreate*/
	ret = Udr_cpos_port_channel_create(slotId, fpgaID, channel, arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	
	
		/* create bitmap */
		ret = Udr_cpos_ce1_create_port_ts(slotId, cposPort, cposPortChannel, &timeslotbitmap);
		if(ret !=0)
		printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
		
		channel = channelID;
		arg[0] = timeslotbitmap;/**/	
		ret = Udr_cpos_ce1_channel_bind_port_ts(slotId,cposPort, cposPortChannel,channel,&timeslotbitmap);
		if(ret !=0)
		printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);




	flow = channelID;
	arg[0] = 0;/*cAtEthFlowNpoPpp*/
	ret = Udr_cpos_port_flow_create(slotId, fpgaID,flow,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	

	flow = channelID;
	dmac[5] = cposPort;
	ret = Udr_cpos_port_flow_egress_dmac_set(slotId, fpgaID, flow, dmac);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	
	
	channel = channelID;
	flow = channelID;
	ret = Udr_cpos_port_channel_bind_flow(slotId, fpgaID, channel,flow);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	
	
	channel = channelID;
	arg[0] = 1;/*enable channel tx*/
	ret = Udr_cpos_port_channel_txenable(slotId, fpgaID,channel,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	
	
	channel = channelID;
	arg[0] = 1;/*enable channel rx*/
	ret = Udr_cpos_port_channel_rxenable(slotId, fpgaID,channel,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	
	
	channel = channelID;
	arg[0] = 5;/*cAtPppLinkPhaseNetworkActive*/
	ret = Udr_cpos_port_channel_ppp_enable(slotId, fpgaID, channel,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	
	
	flow = channelID;
	vlanTag.priority = 0;
	vlanTag.cfi = 0;
	vlanTag.vlanId = vlan;
	ret = Udr_cpos_port_flow_egress_vlan_set(slotId, fpgaID, 1,flow, &vlanTag);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	
	
	flow = channelID;
	vlanTag.priority = 0;
	vlanTag.cfi = 0;
	vlanTag.vlanId = vlan;
	ret = Udr_cpos_port_flow_set_vlan(slotId, fpgaID, 1, flow, &vlanTag);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);

	return ret;

}

/*
Udr_cpos_test_delete_byts 1,1,1,0xfffff,1
Udr_cpos_test_delete_byts 1,5,1,0xfffff,2

*/
int  Udr_cpos_test_delete_byts(uint8 cardId, uint32 cposPort, uint32 cposPortChannel, uint32 timeSlot, uint32 vlan)
{
	tAtPdhDs0PortFhn E1TsPort={0};
	tAtEthVlanTagFhn vlanTag;
	uint32 port1ID;
	uint32 channel,flow;
	eAtRet ret = cAtOk;
	uint32 arg[10]={0};
	uint32 timeslotBitmap=timeSlot;
	uint32 fpgaID =  (cposPort <= 4) ? 1:2; 
	uint8 slotId=cardId;
	
	channel = vlan;
	arg[0] = 0;/*disable channel tx*/
	ret = Udr_cpos_port_channel_txenable(slotId,fpgaID,channel,arg);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	channel = vlan;
	arg[0] = 0;/*disable channel rx*/
	ret = Udr_cpos_port_channel_rxenable(slotId,fpgaID,channel,arg);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	channel = vlan;
	arg[0] = 1;/*cAtPppLinkPhaseDead*/
	ret = Udr_cpos_port_channel_ppp_enable(slotId,fpgaID,channel,arg);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
/* main diffirent from E1 */
		channel = vlan;
	 	ret = Udr_cpos_ce1_channel_unbind_port_ts(slotId,cposPort, cposPortChannel,channel, &timeslotBitmap);
		printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);

	 	ret = Udr_cpos_ce1_delete_port_ts(slotId,cposPort, cposPortChannel, &timeslotBitmap);
		printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
		
/**/	

	channel = vlan;
	flow = vlan;
	ret = Udr_cpos_port_channel_unbind_flow(slotId,fpgaID,channel, &flow);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);


	channel = vlan;
	arg[0] = 1;/*cAtHdlcFrmPppCreate*/
	ret = Udr_cpos_port_channel_delete(slotId,fpgaID,channel,arg);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	flow = vlan;
	arg[0] = 0;/*cAtEthFlowNpoPpp*/
	ret = Udr_cpos_port_flow_del(slotId,fpgaID,flow,arg);
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);

	
	return ret;

}


int Udr_cpos_test_stm4_byport(uint8 cardId, uint32 cposPort, uint32 cposPortChannel, uint32 channelID, uint32 vlan)
{
	int ret=0;
	uint32 channel,flow;
	uint8 smac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	tAtSdhTtiFhn ttiInfo;
	int arg[10];
	tAtEthVlanTagFhn vlanTag;
	eAtPdhDe1FrameType frameMode;
	tAtTimeModeFhn timeMode;
	uint32 fpgaID =  (cposPort <= 4) ? 1:2; 
	uint8 slotId=cardId;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}
	
	/* sdh map */
	ret = Udr_cpos_port_stm4_sdh_map(slotId, cposPort,cposPortChannel);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);

	
	/* E1 framer mode */
	frameMode = cAtPdhE1UnFrm;
	ret = Udr_cpos_e1_framer_mode_set(slotId, cposPort, cposPortChannel, &frameMode);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);

	/* clock mode */
	timeMode.TimeModen =  cAtTimingModeSysFhn;
	timeMode.refPort = 1;
	ret = Udr_cpos_e1_clock_mode_set(slotId, cposPort, cposPortChannel, &timeMode);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	
	/* create channel */
	channel = channelID;
	arg[0] = 1;/*cAtHdlcFrmPppCreate*/
	ret = Udr_cpos_port_channel_create(slotId, fpgaID, channel,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	

	
	channel = channelID;
	arg[0] = cposPort;/*cAtHdlcFrmPppCreate*/
	ret = Udr_cpos_port_channel_bind_port(slotId, channel,cposPort,cposPortChannel);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);

	smac[5] = smac[5] + fpgaID;
	ret = Udr_at_eth_port_smac_set(slotId, fpgaID, 1,smac);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);

	flow = channelID;
	arg[0] = 0;/*cAtEthFlowNpoPpp*/
	ret = Udr_cpos_port_flow_create(slotId, fpgaID, flow,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	


	sprintf(ttiInfo.message,"s");
	ttiInfo.mode = 0;
	ttiInfo.paddingMode = 0;
	ret = Udr_cpos_port_path_tti_tx_set(slotId, cposPort,cposPortChannel,&ttiInfo);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	


	sprintf(ttiInfo.message,"s");
	ttiInfo.mode = 0;
	ttiInfo.paddingMode = 0;
	ret = Udr_cpos_port_path_tti_rx_set(slotId, cposPort, cposPortChannel, &ttiInfo);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	


	flow = channelID;
	dmac[5] = cposPort;
	ret = Udr_cpos_port_flow_egress_dmac_set(slotId, fpgaID, flow,dmac);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	
	
	channel = channelID;
	flow = channelID;
	ret = Udr_cpos_port_channel_bind_flow(slotId, fpgaID, channel,flow);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	
	
	channel = channelID;
	arg[0] = 1;/*enable channel tx*/
	ret = Udr_cpos_port_channel_txenable(slotId, fpgaID, channel,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	
	
	channel = channelID;
	arg[0] = 1;/*enable channel rx*/
	ret = Udr_cpos_port_channel_rxenable(slotId, fpgaID, channel,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	
	
	channel = channelID;
	arg[0] = 5;/*cAtPppLinkPhaseNetworkActive*/
	ret = Udr_cpos_port_channel_ppp_enable(slotId, fpgaID, channel,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	
	
	flow = channelID;
	vlanTag.priority = 0;
	vlanTag.cfi = 0;
	vlanTag.vlanId = vlan;
	ret = Udr_cpos_port_flow_egress_vlan_set(slotId, fpgaID, 1, flow, &vlanTag);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);
	
	
	flow = channelID;
	vlanTag.priority = 0;
	vlanTag.cfi = 0;
	vlanTag.vlanId = vlan;
	ret = Udr_cpos_port_flow_set_vlan(slotId, fpgaID, 1,flow, &vlanTag);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannel,channelID,vlan);

	return ret;
}

int Udr_cpos_mlppp_bundle_test( uint8 cardId )
{
	int *arg=NULL;
	int ret=0,mlBundleId=0;
	uint8 slotId=cardId;

	for(mlBundleId=1;mlBundleId<129;mlBundleId++)
	{
		ret = Udr_cpos_mlppp_bundle_create(slotId, 1, mlBundleId, arg);
		if(ret !=0)
			printf("%s,#%d,ret=%x,slot=%d,bindId=%d\r\n",__FILE__,__LINE__,ret,slotId,mlBundleId);
	}

	for(mlBundleId=1;mlBundleId<129;mlBundleId++)
	{
		ret = Udr_cpos_mlppp_bundle_create(slotId, 2, mlBundleId, arg);
		if(ret !=0)
			printf("%s,#%d,ret=%x,slot=%d,bindId=%d\r\n",__FILE__,__LINE__,ret,slotId,mlBundleId);
	}

}
	


/*  test two FPGA port1  */
int Udr_cpos_test_two_stm4( uint8 cardId )
{
	int ch=1,vlan=1;
	int ret=0;
	uint32 channel,flow;
	uint8 smac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	tAtSdhTtiFhn ttiInfo;
	int arg[10];
	uint32 fpgaID=1;
	unsigned long tickOld=0, tickNew=0;
	uint8 slotId=cardId;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}
	
		tickSet(0);
		tickOld = tickGet();


	arg[0] = cAtSdhLineRateStm4Fhn;
	ret = Udr_cpos_port_linerate_set(slotId, 1,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	

	arg[0] = 0;
	ret = Udr_cpos_port_linemode_set(slotId, 1,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);


	arg[0] = 0;
	ret = Udr_cpos_port_loopback_set(slotId, 1,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);


	sprintf(ttiInfo.message,"s");
	ttiInfo.mode = 0;
	ttiInfo.paddingMode = 0;
	ret = Udr_cpos_port_tti_tx_set(slotId, 1,&ttiInfo);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);	

	sprintf(ttiInfo.message,"s");
	ttiInfo.mode = 0;
	ttiInfo.paddingMode = 0;		
	ret = Udr_cpos_port_tti_rx_set(slotId, 1,&ttiInfo);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);

	ret = Udr_at_eth_port_smac_set(slotId, 1, 1,smac);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);

	for(ch=1;ch<=63*4;ch++)
	{
		Udr_cpos_test_stm4_byport(slotId, 1,  ch,  vlan,  vlan);
		vlan++;
	}


	arg[0] = cAtSdhLineRateStm4Fhn;
	ret = Udr_cpos_port_linerate_set(slotId, 5,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);
	

	arg[0] = 0;
	ret = Udr_cpos_port_linemode_set(slotId, 5,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);


	arg[0] = 0;
	ret = Udr_cpos_port_loopback_set(slotId, 5,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);

	sprintf(ttiInfo.message,"s");
	ttiInfo.mode = 0;
	ttiInfo.paddingMode = 0;
	ret = Udr_cpos_port_tti_tx_set(slotId, 5,&ttiInfo);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);	

	sprintf(ttiInfo.message,"s");
	ttiInfo.mode = 0;
	ttiInfo.paddingMode = 0;		
	ret = Udr_cpos_port_tti_rx_set(slotId, 5,&ttiInfo);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);

	smac[5]++;
	ret = Udr_at_eth_port_smac_set(slotId, 2, 1,smac);
	if(ret !=0)
	printf("%s,#%d,ret=%x\r\n",__FILE__,__LINE__,ret);

	vlan=1;
	for(ch=1;ch<=63*4;ch++)
	{
		Udr_cpos_test_stm4_byport(slotId, 5,  ch,  vlan,  vlan);
		vlan++;
	}

	//At_cpos_port_serdes_time_mode(1);
	//At_cpos_port_serdes_time_mode(5);

		tickNew = tickGet();
		printf("all STM4 time=%d\n",tickNew-tickOld);

	
	return 0;
}


/*  test two FPGA port1  */
int Udr_cpos_test_two_stm4_delete( uint8 cardId )
{
	int ch=1,vlan=1;
	uint8 slotId=cardId;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}

	for(ch=1;ch<=63*4;ch++)
	{
		Udr_cpos_test_byport_delete(slotId, 1, ch,vlan);
		vlan++;
	}

	vlan=1;
	for(ch=1;ch<=63*4;ch++)
	{
		Udr_cpos_test_byport_delete(slotId, 5, ch,vlan);
		vlan++;
	}

	return 0;
}


/*  
Udr_cpos_test_two_stm1_mlppp 1,1,32,1,1
Udr_cpos_test_two_stm1_mlppp 1,5,32,1,2

test two FPGA port1  
*/
int Udr_cpos_test_two_stm1_mlppp(uint8 cardId, uint32 cposPort, uint32 cposPortChannelNum, uint32 mlBundleId, uint32 vlan)
{
	int cposPortId= cposPort;
	int ret=0,ch=0;
	uint32 channel,flow;
	uint8 smac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	uint8 dmac[]={0x00,0x04,0x67,0x00,0x00,0x88};
	tAtSdhTtiFhn ttiInfo;
	int arg[10];
	tAtEthVlanTagFhn vlanTag;
	eAtEthPortInterface EncapTypeBuf = cAtEthPortInterface1000BaseX;
	eAtPdhDe1FrameType frameMode;
	tAtTimeModeFhn timeMode;
	uint32 fpgaID =  (cposPort <= 4) ? 1:2; 
	uint8 slotId=cardId;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}
/*
	if(cposPort > 4)
	{
		cposPortId = cposPortId-4;
		cpos_card_fpga_select(2);
		Udr_at_eth_port_mode_set(1, &EncapTypeBuf);
	}
	else
	{
		cpos_card_fpga_select(1);
		Udr_at_eth_port_mode_set(1, &EncapTypeBuf);
	}
*/

	printf("fpga select %x\r\n", fpgaID);
	/* stm1 moden */
	arg[0] = 0;
	ret = Udr_cpos_port_linerate_set(slotId, cposPortId,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannelNum,mlBundleId,vlan);
	
	/* sdh moden */
	arg[0] = 0;
	ret = Udr_cpos_port_linemode_set(slotId, cposPortId,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannelNum,mlBundleId,vlan);

	/* relese loopback moden */
	arg[0] = 0;
	ret = Udr_cpos_port_loopback_set(slotId, cposPortId,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannelNum,mlBundleId,vlan);

	/*
		"abc" 16B and  null padding
	*/
	sprintf(ttiInfo.message,"b");
	ttiInfo.mode = 0;
	ttiInfo.paddingMode = 0;
	ret = Udr_cpos_port_tti_tx_set(slotId, cposPortId,&ttiInfo);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannelNum,mlBundleId,vlan);


	sprintf(ttiInfo.message,"b");
	ttiInfo.mode = 0;
	ttiInfo.paddingMode = 0;		
	ret = Udr_cpos_port_tti_rx_set(slotId, cposPortId,&ttiInfo);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannelNum,mlBundleId,vlan);
	

	ret = Udr_cpos_mlppp_bundle_create(slotId, fpgaID, mlBundleId,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannelNum,mlBundleId,vlan);

	/* sdh map */
	for(ch=1; ch<=cposPortChannelNum;ch++)
	{
		ret = Udr_cpos_port_stm1_sdh_map(slotId,cposPortId,ch);
		if(ret !=0)
		printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannelNum,mlBundleId,vlan);

		/* E1 framer mode */
		frameMode = cAtPdhE1UnFrm;
		ret = Udr_cpos_e1_framer_mode_set(slotId, cposPort, ch, &frameMode);
		if(ret !=0)
		printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannelNum,mlBundleId,vlan);

		/* clock mode */
		timeMode.TimeModen =  cAtTimingModeSysFhn;
		timeMode.refPort = 1;
		ret = Udr_cpos_e1_clock_mode_set(slotId, cposPort, ch, &timeMode);
		if(ret !=0)
		printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannelNum,mlBundleId,vlan);
		

		/* create channel */
		channel = ch;
		arg[0] = 1;/*cAtHdlcFrmPppCreate*/
		ret = Udr_cpos_port_channel_create(slotId, fpgaID, channel,arg);
		if(ret !=0)
		printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannelNum,mlBundleId,vlan);
			
		channel = ch;
		arg[0] = cposPortId;/*cAtHdlcFrmPppCreate*/
		ret = Udr_cpos_port_channel_bind_port(slotId, channel,cposPortId,channel);
		if(ret !=0)
		printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannelNum,mlBundleId,vlan);

		channel = ch;
		ret = Udr_cpos_mlppp_bundle_add_channel(slotId, fpgaID, mlBundleId,channel);
		if(ret !=0)
		printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannelNum,mlBundleId,vlan);

	}

	ret = Udr_at_eth_port_smac_set(slotId,  1,1, smac);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannelNum,mlBundleId,vlan);

	flow = mlBundleId;
	arg[0] = 0;/*cAtEthFlowNpoPpp*/
	ret = Udr_cpos_port_flow_create(slotId, fpgaID, flow,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannelNum,mlBundleId,vlan);

	flow = mlBundleId;
	dmac[5] = cposPort;
	ret = Udr_cpos_port_flow_egress_dmac_set(slotId, fpgaID, flow,dmac);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannelNum,mlBundleId,vlan);

	sprintf(ttiInfo.message,"s");
	ttiInfo.mode = 0;
	ttiInfo.paddingMode = 0;
	ret = Udr_cpos_port_path_tti_tx_set(slotId, cposPortId, 1,&ttiInfo);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannelNum,mlBundleId,vlan);


	for(ch=1; ch<=cposPortChannelNum;ch++)
	{
		sprintf(ttiInfo.message,"s");
		ttiInfo.mode = 0;
		ttiInfo.paddingMode = 0;
		ret = Udr_cpos_port_path_tti_rx_set(slotId, cposPortId, ch, &ttiInfo);
		if(ret !=0)
		printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannelNum,mlBundleId,vlan);

		
		channel = ch;
		arg[0] = 1;/*enable channel tx*/
		ret = Udr_cpos_port_channel_txenable(slotId, fpgaID, channel,arg);
		if(ret !=0)
		printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannelNum,mlBundleId,vlan);
			
		channel = ch;
		arg[0] = 1;/*enable channel rx*/
		ret = Udr_cpos_port_channel_rxenable(slotId, fpgaID, channel,arg);
		if(ret !=0)
		printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannelNum,mlBundleId,vlan);
		
		channel = ch;
		arg[0] = 5;/*cAtPppLinkPhaseNetworkActive*/
		ret = Udr_cpos_port_channel_ppp_enable(slotId, fpgaID, channel,arg);
		if(ret !=0)
		printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannelNum,mlBundleId,vlan);

		
	}

	smac[5]++;
	ret = Udr_at_eth_port_smac_set(slotId,  2, 1,smac);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannelNum,mlBundleId,vlan);


	flow = mlBundleId;
	vlanTag.priority = 0;
	vlanTag.cfi = 0;
	vlanTag.vlanId = vlan;
	ret = Udr_cpos_port_flow_egress_vlan_set(slotId, fpgaID, 1, flow, &vlanTag);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannelNum,mlBundleId,vlan);
	
	
	flow = mlBundleId;
	vlanTag.priority = 0;
	vlanTag.cfi = 0;
	vlanTag.vlanId = vlan;
	ret = Udr_cpos_port_flow_set_vlan(slotId, fpgaID, 1, flow, &vlanTag);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannelNum,mlBundleId,vlan);

	flow = mlBundleId;
	ret = Udr_cpos_mlppp_bundle_bind_flow(slotId, fpgaID, mlBundleId,flow);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannelNum,mlBundleId,vlan);

	arg[0]=1;
	ret = Udr_cpos_mlppp_bundle_enable(slotId, fpgaID, mlBundleId,arg);
	if(ret !=0)
	printf("%s,#%d,ret=%x,port=%d,port_ch=%d,chID=%d,vid=%d\r\n",__FILE__,__LINE__,ret,cposPort,cposPortChannelNum,mlBundleId,vlan);

	return ret;
}

/* 
Udr_cpos_test_two_stm1_mlppp_delete 2,1,10,1,1 
Udr_cpos_test_two_stm1_mlppp_delete 2,5,10,1,2 
*/
int  Udr_cpos_test_two_stm1_mlppp_delete(uint8 cardId, uint32 cposPort, uint32 cposPortChannelNum, uint32 mlBundleId, uint32 vlan)
{
	int ret=0, ch=1, *arg=NULL,flow=0;
	uint32 fpgaID =  (cposPort <= 4) ? 1:2; 
	uint8 slotId=cardId;

	if(cardId<1||cardId>2)
	{
		printf("#%d,%s,error cardId=%d\r\n",__LINE__,__FUNCTION__,cardId);
		return -1;
	}
	
	ret = Udr_cpos_mlppp_bundle_enable(slotId, fpgaID,mlBundleId,0);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);
	
	for(ch=1;ch<=cposPortChannelNum;ch++)
	{
		Udr_cpos_test_byport_mlppp_delete(slotId, cposPort, mlBundleId, ch, vlan);
	}

	flow = mlBundleId;
	ret = Udr_cpos_mlppp_bundle_unbind_flow(slotId, fpgaID, mlBundleId, flow);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);

	
	flow = mlBundleId;
	arg[0] = 0;/*cAtEthFlowNpoPpp*/
	ret = Udr_cpos_port_flow_del(slotId, fpgaID, flow, arg);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);


	ret = Udr_cpos_mlppp_bundle_Delete(slotId, fpgaID, mlBundleId);
	if(ret !=0)
	printf("%s,#%d,%s ret=%x\r\n",__FILE__,__LINE__,__FUNCTION__,ret);


	return 0;
}


int Udr_at_alarm_test(uint8 cardId, int port)
{
	int i=0;
	uint8 slotId=cardId;
	uint8 alarmbuf[11];
	uint32 productCode=0;
	eUdrSdhLineAlarmTypeFhn sdhLineBuf={0};
	eUdrPdhDe1AlarmTypeFhn pdhDe1Buf={0};
    const char *pHeadingPdh[] = {"LOS", "LOF", "AIS", "RAI", "LOMF","SF-BER", "SD-BER", "SigLOF", "SigRAI"};
    const char *pHeadingSdh[] = {"LOS", "OOF", "LOF", "TIM", "AIS", "RDI",
                              "BER-SD", "BER-SF", "KByte-Change", "KByte-Fail","S1-Change"};

	if(cardId<1 || cardId>2)
	{
		printf("error cardId=%d\r\n",cardId);
		return -1;
	}


	At_register_rd(slotId,1,0,&productCode);

	if((productCode == 0x60070013) || (productCode == 0x60030022)||(productCode == 0x60030023))
	{

		Udr_ce1_port_alarm_status(slotId, port, &pdhDe1Buf);
		memcpy(alarmbuf, (uint8 *)&pdhDe1Buf, 11);
		for (i = 0; i < cUdrPdhDe1NumAlarmFhn; i++)
		{
			if (alarmbuf[i])
			    	printf("%s  SET \r\n",pHeadingPdh[i]);
			else
			    	printf("%s  CLEAR \r\n",pHeadingPdh[i]);
		}

	}
	else if ((productCode == 0x60070023) ||(productCode == 0x60030051))
	{
		Udr_cpos_port_alarm_status(slotId, port, &sdhLineBuf);
		memcpy(alarmbuf, (uint8 *)&sdhLineBuf, 11);
		for (i = 0; i < cUdrSdhLineNumAlarmFhn; i++)
		{
			if (alarmbuf[i])
			    	printf("%s  SET \r\n",pHeadingSdh[i]);
			else
			    	printf("%s  CLEAR \r\n",pHeadingSdh[i]);
		}
	}
	else
	{
		printf("%s,#%d,%s,productCode=%x\r\n",__FILE__,__LINE__,__FUNCTION__,productCode);
		return -1;
	}

}


int Udr_at_counter_test(uint8 cardId, int channel)
{
	int i=0;
	uint8 slotId=cardId;
	uint32 counterBuf[cUdrAtEthPortCountersFhn];
	tAtEthPortCountersFhn ethCounters;
	tAtHdlcChannelCountersFhn hdlcCounter;
	tAtEthFlowCountersFhn  flowCounter;
    	const char *ethCounterStrings[] = {"txPackets", "txBytes", "txOamPackets", "txPeriodOamPackets",
                                    "txPwPackets", "txPacketsLen0_64", "txPacketsLen65_127", "txPacketsLen128_255",
                                    "txPacketsLen256_511", "txPacketsLen512_1024", "txPacketsLen1025_1528",
                                    "txPacketsJumbo", "txTopPackets", "rxPackets", "rxBytes", "rxErrEthHdrPackets",
                                    "rxErrBusPackets", "rxErrFcsPackets", "rxOversizePackets", "rxUndersizePackets",
                                    "rxPacketsLen0_64", "rxPacketsLen65_127", "rxPacketsLen128_255","rxPacketsLen256_511",
                                    "rxPacketsLen512_1024", "rxPacketsLen1025_1528", "rxPacketsJumbo", "rxPwUnsupportedPackets",
                                    "rxErrPwLabelPackets", "rxDiscardedPackets", "rxPausePackets", "rxErrPausePackets",
                                    "rxBrdCastPackets", "rxMultCastPackets", "rxArpPackets", "rxOamPackets",
                                    "rxEfmOamPackets", "rxErrEfmOamPackets", "rxEthOamType1Packets", "rxEthOamType2Packets",
                                    "rxIpv4Packets", "rxErrIpv4Packets", "rxIcmpIpv4Packets", "rxIpv6Packets",
                                    "rxErrIpv6Packets", "rxIcmpIpv6Packets", "rxMefPackets", "rxErrMefPackets",
                                    "rxMplsPackets", "rxErrMplsPackets", "rxMplsErrOuterLblPackets",
                                    "rxMplsDataPackets", "rxLdpIpv4Packets", "rxLdpIpv6Packets", "rxMplsIpv4Packets", "rxMplsIpv6Packets", "rxErrL2tpv3Packets",
                                    "rxL2tpv3Ipv4Packets", "rxL2tpv3Ipv6Packets", "rxL2tpv3Packets", "rxErrUdpPackets",
                                    "rxUdpIpv4Packets", "rxUdpIpv6Packets", "rxUdpPackets", "rxErrPsnPackets",
                                    "rxPacketsSendToCpu", "rxPacketsSendToPw", "rxTopPackets", "rxPacketDaMis"};

    const char *channelCounterStrings[] = {
					"txGoodPkt",
					"txAbortPkt",
					"rxGoodPkt",
					"rxAbortPkt",
					"rxFcsErrPkt",
					"rxAddrCtrlErrPkt",
					"rxSapiErrPkt",
					"rxErrPkt"
                                };
	
    const char *flowCounterStrings[] = {
				"txByte","txPkt", "txGoodPkt", "txFragment","txNullFragment",
				"txdiscardPacket","txdiscardByte","txGoodPacket", "txGoodFragment", "txdiscradFragment",
				"windowViolation", "rxByte","rxPacket", "rxGoodByte","rxGoodPkt",
				"rxFragment", "rxNullFragment", "rxDiscardPkt", "rxDiscardByte","mrruExceed"};

	printf("ETH PORT COUNTER \r\n");
	Udr_ce1_ethport_counter_get(slotId, 1, counterBuf);
        for (i = 0; i < cUdrAtEthPortCountersFhn; i++)
        {
            if (counterBuf[i])
                	printf("%s  : %x\r\n",ethCounterStrings[i], counterBuf[i]);
        }

	printf("\nCHANNEL %d COUNTER \r\n",channel);
	Udr_ce1_channel_counter_get(slotId,channel, counterBuf);
        for (i = 0; i < cUdrAtChannelCountersFhnFhn; i++)
        {
            if (counterBuf[i])
                	printf("%s  : %x\r\n",channelCounterStrings[i],counterBuf[i]);
        }

/*
	printf("\nFLOW %d COUNTER \r\n", channel);
	Udr_ce1_flow_counter_get(channel, counterBuf);
        for (i = 0; i < cUdrAtEthFlowCountersFhnFhn; i++)
        {
            if (counterBuf[i])
                	printf("%s  : %x\r\n",flowCounterStrings[i],counterBuf[i]);
        }
*/
}

int Udr_at_counter_clear_test(uint8 cardId, uint32 channel)
{
	int i=0;
	uint8 slotId=cardId;
	uint32 counterBuf[cUdrAtEthPortCountersFhn];

	Udr_ce1_ethport_counter_clear(slotId,1, counterBuf);
	Udr_ce1_channel_counter_clear(slotId,channel, counterBuf);

	return 0;	
}

int Udr_at_include(void)
{

}


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APP
 *
 * File        : AtFhnDriver.c
 *
 * Created Date: Jun 21, 2013
 *
 * Description : Driver management for FHN project
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtFhnDriver.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* All slots that this driver manages */
static AtFhnSlot *m_slots    = NULL;
uint8      m_numSlots = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/*
 * Get base address of core in slot
 *
 * @param fpgaId [1..2] FPGA ID
 * @param slotId Slot ID start from 1
 *
 * @return Base address
 */
static uint32 BaseAddressOfFpgaInSlot(uint8 fpgaId, uint8 slotId)
    {
    if (slotId == 1)
        return (fpgaId == 1) ? 0xC0000000 : 0xC8000000;

    /* TODO: Need to correct the following code for other slots */
    return (fpgaId == 1) ? 0xC4000000 : 0xCC000000;
    }

void AtFhnSlotShow()
{
	
	printf("pslot1[%x],pslot2[%x]\r\n ",m_slots[0],m_slots[1]);
}

static eBool SlotIdIsValid(uint8 slotId)
{
	if(m_slots[slotId-1]==NULL){
		printf("%s, slot[%d] is not exist\r\n",__FUNCTION__,slotId);
		return cAtFalse;
	}
	
	return (slotId <= m_numSlots) ? cAtTrue : cAtFalse;
}

static AtHal CreateHalForFpgaInSlot(uint8 fpgaId, uint8 slotId)
    {
    return AtHalFhnNew(BaseAddressOfFpgaInSlot(fpgaId, slotId));
    }

uint32 ProductCodeOfSlot(uint8 slotId)
    {
    AtHal hal = CreateHalForFpgaInSlot(1, slotId); /* Use the first FPGA to get product code is OK */
    uint32 productCode = AtHalRead(hal, 0x0);
    AtHalDelete(hal);

    return productCode;
    }

#if	0 /* modified by ssq 20140218 */
static uint32 NumFpgasOfProduct(uint32 productCode)
    {
    return (productCode == 0x60030051) ? 2 : 1;
    }
#endif

static void DeleteAllSlots()
    {
    uint8 slot_i;

    for (slot_i = 0; slot_i < m_numSlots; slot_i++)
        AtFhnSlotDelete(m_slots[slot_i]);
    AtOsalMemFree(m_slots);

    m_slots    = NULL;
    m_numSlots = 0;
    }

/**
 * Create a slot
 *
 * @param slotId Slot ID starts from 1
 *
 * @return AT return code
 */
eAtRet AtFhnDriverSlotCreate(uint8 slotId)
    {
    eAtRet ret = cAtOk;
    uint8 fpgaId;
    uint32 productCode;
    AtFhnSlot newSlot = NULL;
    uint8 numDevices;

#if	0
    if (!SlotIdIsValid(slotId))
        return cAtErrorInvlParm;

    /* Slot is added */
    if (AtFhnDriverSlotGet(slotId))
        return cAtErrorDevBusy;
#else

	if(slotId<1||slotId>2)
	{
		printf("#%d,%s,error slotId=%d\r\n",__LINE__,__FUNCTION__,slotId);
		return -1;
	}
#endif


    /* Add device and set HAL for IP core */
    productCode = ProductCodeOfSlot(slotId);

	//modified by ssq 20140218
	//numDevices = NumFpgasOfProduct(productCode);
	numDevices = NumFpgasOfProduct(slotId);
	//end modified
	
	newSlot = AtFhnSlotNew(numDevices);
    for (fpgaId = 1; fpgaId <= numDevices; fpgaId++)
        {
        static const uint8 cFirstCore = 0;

        /* Create device for this FPGA */
        AtDevice newDevice;
        newDevice = AtDriverDeviceCreate(AtDriverSharedDriverGet(), productCode);
        if (newDevice == NULL)
            {
            AtPrintc(cSevCritical, "ERROR: Cannot create device with product code 0x%08x\r\n", productCode);
            return cAtErrorInvlParm;
            }

        /* Setup HAL and initialize device */
        AtDeviceIpCoreHalSet(newDevice, cFirstCore, CreateHalForFpgaInSlot(fpgaId, slotId));

        ret |= AtDeviceInit(newDevice);
	if (ret != cAtOk){
		printf("#%d,%s,pslot[%x] FPGA[%d] ,ret=%s\r\n",__LINE__,__FUNCTION__,slotId,fpgaId,AtRet2String(ret));
	}else{
		printf("#%d,%s,pslot%d[%x] FPGA[%d] ret=%d \r\n",__LINE__,__FUNCTION__,slotId,newSlot,fpgaId,ret);
	}
        /* Cache device */
        AtFhnSlotDeviceSet(newSlot, fpgaId, newDevice);
        }

    /* Save this slot */
    m_slots[slotId - 1] = newSlot;
    return ret;
    }

/**
 * Delete a slot
 *
 * @param slotId Slot ID starts from 1
 *
 * @return AT return code
 */
eAtRet AtFhnDriverSlotDelete(uint8 slotId)
    {
    AtFhnSlotDelete(AtFhnDriverSlotGet(slotId));
    m_slots[slotId - 1] = NULL;
    return cAtOk;
    }

/**
 * Get slot object by ID
 *
 * @param slotId Slot ID starts from 1
 * @return
 */
AtFhnSlot AtFhnDriverSlotGet(uint8 slotId)
    {
    if (SlotIdIsValid(slotId))
        return m_slots[slotId - 1];

    return NULL;
    }

/**
 * Create driver
 *
 * @param numSlots Number of slots
 *
 * @return Driver instance
 */
AtDriver AtFhnDriverCreate(uint8 numSlots)
    {
    static const uint8 cMaxNumDevicesPerSlot = 2;
    AtDriver driver = AtDriverCreate(numSlots * cMaxNumDevicesPerSlot, AtOsalVxWorks());
    uint32 memorySize = sizeof(AtFhnSlot) * numSlots;

    m_slots = AtOsalMemAlloc(memorySize);
    AtOsalMemInit(m_slots, 0, memorySize);
    m_numSlots = numSlots;

    return driver;
    }

/**
 * Delete driver
 *
 * @return None
 */
void AtFhnDriverDelete()
    {
    DeleteAllSlots();
    AtDriverDelete(AtDriverSharedDriverGet());
    }

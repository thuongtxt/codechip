
#include <arrive/AtClasses.h>
#include <arrive/AtDevice.h>
#include <arrive/AtDriver.h>
#include <arrive/AtHal.h>
#include <arrive/AtOsal.h>
#include <arrive/AtHalTcp.h>
#include <arrive/AtList.h>
#include <arrive/AtModuleSdh.h>
#include <arrive/AtSdhLine.h>
#include <arrive/AtSdhAug.h>
#include <arrive/AtSdhVc.h>
#include <arrive/AtSdhTug.h>
#include <arrive/AtPw.h>
#include <arrive/AtPdhDe1.h>

#define NUM_PATHS_IN_AUG1 3
#define EGRESS_ETH_PORT 0
#define CEM_VLAN_ID 4095

#if defined CEM_FPGA_SIMULATION
static dev_status
cem_fpga_driver_init (dev_object_t * dev)
{
    dev_cem_fpga_object_t *cem_fpga = 
                        (dev_cem_fpga_object_t *)dev;
    AtDriver driver = AtDriverCreate(1, AtOsalLinux());
    AtHal hal = AtHalTcpNew("172.33.44.126", cAtHalTcpDefaultPort);
    uint32 productCode = AtHalRead(hal, 0x0);

    AtDevice newDevice = AtDriverDeviceCreate(driver, productCode);
    if (newDevice == NULL)
        return(DEV_CEM_FPGA_FAILURE);

    /* Use this HAL for the IP Core */
    AtDeviceIpCoreHalSet(newDevice, 0, hal);
    AtDeviceInit(newDevice);
    cem_fpga->cem_fpga_driver = driver;
    return (DEV_CEM_FPGA_SUCCESS);
}
#endif


static AtSdhLine cem_fpga_get_line (AtDriver cem_fpga_driver, 
        uint8_t line_id)
{
    AtDevice device =
        AtDriverDeviceGet(cem_fpga_driver, CEM_FPGA_CORE_ID);
    AtModuleSdh sdhModule = 
        (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    return (AtModuleSdhLineGet(sdhModule, line_id));
}

static boolean cem_fpga_sts1_path_provision (AtDriver cem_fpga_driver,
        uint16_t port, uint16_t path_num, path_mode_e path_mode)
{
    AtSdhLine line_inst = cem_fpga_get_line(cem_fpga_driver, port);
    AtSdhChannel aug1, vc3;
    uint8_t aug_id = (path_num / NUM_PATHS_IN_AUG1);
    uint8_t vc3_id = (path_num % NUM_PATHS_IN_AUG1);

    /* Setup mapping for AUG-1#aug_id to map VC-3 */
    aug1 = (AtSdhChannel)AtSdhLineAug1Get(line_inst, aug_id);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1Map3xVc3s);
    vc3 = (AtSdhChannel)AtSdhLineVc3Get(line_inst, aug_id, vc3_id);

    switch (path_mode) {
    case PATH_MODE_VT15:
    case PATH_MODE_C12:
    case PATH_MODE_C11:
    case PATH_MODE_VT20:
        AtSdhChannelMapTypeSet(vc3, cAtSdhVcMapTypeVc3Map7xTug2s);
        if (!vc3) {
            ERR("STS channel mapping, vc3 is null");
        }
        if (!aug1) {
            ERR("STS channel mapping, aug1 is null");
        }
        ERR("STS channel map: aug: %d, vc3: %d, path_num %d", 
                aug_id, vc3_id, path_num);
        return (TRUE);
    case PATH_MODE_CT3:
    case PATH_MODE_T3:
    case PATH_MODE_E3:
    case PATH_MODE_CT3_E1:
        AtSdhChannelMapTypeSet(vc3, cAtSdhVcMapTypeVc3MapDe3);
        return (TRUE);
    case PATH_MODE_STS1: 
        AtSdhChannelMapTypeSet(vc3, cAtSdhVcMapTypeVc3MapC3);
        return (TRUE);
    default:
        ERR("path_mode not set correctly");
        return (FALSE);
    }

    /* Shouldnt hit here */
    return (TRUE);
}

static boolean cem_fpga_sts3c_path_provision (AtDriver cem_fpga_driver,
        uint16_t port, uint16_t path_num)
{
    AtSdhLine line_inst = cem_fpga_get_line(cem_fpga_driver, port);
    AtSdhChannel aug1, vc4;
    uint8_t aug_id = (path_num / NUM_PATHS_IN_AUG1);

    aug1 = (AtSdhChannel)AtSdhLineAug1Get(line_inst, aug_id);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1MapVc4);
    vc4 = (AtSdhChannel)AtSdhLineVc4Get(line_inst, aug_id);
    AtSdhChannelMapTypeSet(vc4, cAtSdhVcMapTypeVc4MapC4);
    return (TRUE);
}

static boolean cem_fpga_t3e3_intf_provision (AtDriver cem_fpga_driver,
        uint16_t port_num, uint16_t path_num, uint16_t t3e3_num)
{
    AtSdhLine line_inst = cem_fpga_get_line(cem_fpga_driver, port_num);
    uint8_t vc3_id = (path_num % NUM_PATHS_IN_AUG1);
    uint8_t aug_id = (path_num / NUM_PATHS_IN_AUG1);
    AtSdhChannel vc3_inst;

    /* AU-3 <--> VC-3 <--> DS3 */
    vc3_inst = (AtSdhChannel)AtSdhLineVc3Get(line_inst, aug_id, vc3_id);
    AtSdhChannelMapTypeSet(vc3_inst, cAtSdhVcMapTypeVc3MapDe3);
    return (TRUE);
}

static boolean cem_fpga_t1e1_intf_provision (AtDriver cem_fpga_driver,
        uint16_t port_num, uint16_t path_num, uint16_t tug2_num, 
        uint16_t t1e1_num)
{
    AtSdhLine line_inst = cem_fpga_get_line(cem_fpga_driver, port_num);
    uint8_t aug_id = (path_num / NUM_PATHS_IN_AUG1);
    AtSdhChannel tug2, vc11;
    AtPdhChannel de1;

    ERR("RAGHA---> in ARRIVE cem_fpga_t1e1_intf_provision aug_id %d", aug_id);

    /* AU-3 <--> VC-3 <--> TUG-2 <--> VC-11 <--> DS1 */
    tug2 = (AtSdhChannel)AtSdhLineTug2Get(line_inst, aug_id, 
            path_num, tug2_num);
    AtSdhChannelMapTypeSet(tug2, cAtSdhTugMapTypeTug2Map4xTu11s);
    vc11 = (AtSdhChannel)AtSdhLineVc1xGet(line_inst, aug_id, 
            path_num, tug2_num, t1e1_num);
    ERR("t1e1 prov path:%d, tug2: %d, t1e1_num: %d", path_num, tug2_num, t1e1_num);
    AtSdhChannelMapTypeSet(vc11, cAtSdhVcMapTypeVc1xMapDe1);
    de1 = (AtPdhChannel)AtSdhChannelMapChannelGet(vc11);
    /* set the framing type here as of now */
    AtPdhChannelFrameTypeSet(de1, cAtPdhDs1J1UnFrm);
        if (!tug2) {
            ERR("te1 channel mapping, tug2 is null");
        }
        if (!vc11) {
            ERR("te1 channel mapping, vc11 is null");
        }
        if (!de1) {
            ERR("te1 channel mapping, de1 is null");
        }
    return (TRUE);
}

static boolean cem_fpga_vt_intf_provision (AtDriver cem_fpga_driver,
        uint16_t port_num, uint16_t path_num, uint16_t tug2_num, 
        uint16_t t1e1_num)
{
    AtSdhLine line_inst = cem_fpga_get_line(cem_fpga_driver, port_num);
    uint8_t aug_id = (path_num / NUM_PATHS_IN_AUG1);
    AtSdhChannel vc11, tug2;

    /* Setup mapping for AUG-1 to map AU-3 <--> 
     * <--> TUG-2 <--> VC-11 */
    tug2 = (AtSdhChannel)AtSdhLineTug2Get(line_inst, aug_id, 
            path_num, tug2_num);
    AtSdhChannelMapTypeSet(tug2, cAtSdhTugMapTypeTug2Map4xTu11s);
    vc11 = (AtSdhChannel)AtSdhLineVc1xGet(line_inst, aug_id, 
            path_num, tug2_num, t1e1_num);
    AtSdhChannelMapTypeSet(vc11, cAtSdhVcMapTypeVc1xMapC1x);
    return (TRUE);
}

static boolean cem_fpga_cem_intf_config (AtDriver cem_fpga_driver,
        uint16_t port_num, uint16_t path_num, uint16_t vtg_num, 
        uint16_t t1e1_num, pw_info_t *pw_info, path_mode_e cem_intf_mode,
        uint32_t timeslot_bitmap)
{
    AtPw pw;
    AtModulePw pwModule;
    AtSdhChannel vc3, vc11;
    AtChannel de1, de3, vc3_sts, vc4_sts3;
    AtPdhNxDS0 nxDs0;
    AtPdhDe1 nxde1;
    AtDevice device =
        AtDriverDeviceGet(cem_fpga_driver, CEM_FPGA_CORE_ID);
    uint8_t aug_id = (path_num / NUM_PATHS_IN_AUG1);
    uint8_t vc3_id = (path_num % NUM_PATHS_IN_AUG1);
    AtSdhLine line_inst = cem_fpga_get_line(cem_fpga_driver, port_num);

    pwModule = 
        (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);

    switch (cem_intf_mode) {
    case PATH_MODE_VT15:
        vc11 = (AtSdhChannel)AtSdhLineVc1xGet(line_inst, aug_id, 
                path_num, vtg_num, t1e1_num);
        if (pw_info->pw_type == PW_TYPE_SATOP) {
            de1 = AtSdhChannelMapChannelGet(vc11);
            pw = (AtPw)AtModulePwSAToPCreate(pwModule, pw_info->cem_id);
            AtPwCircuitBind(pw, de1);
        if (!pw) {
            ERR("te1 channel mapping, pw is null");
        }
        if (!de1) {
            ERR("te1 channel mapping, de1 is null");
        }
        ERR("cem intf cem_id %d", pw_info->cem_id);
        ERR("cem intf aug %d, path_num %d, vtg: %d, t1e1 %d", aug_id, path_num, vtg_num, t1e1_num);
        } else if (pw_info->pw_type == PW_TYPE_CESOP) {
            nxde1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc11);
            nxDs0 = AtPdhDe1NxDs0Create(nxde1, timeslot_bitmap);
            pw = (AtPw)AtModulePwCESoPCreate(pwModule, 
                    pw_info->cem_id, cAtPwCESoPModeBasic);
            AtPwCircuitBind(pw, (AtChannel)nxDs0);
        } else {
            ERR("Wrong pw_circuit_type !");
        }
        break;
    case PATH_MODE_CT3:
        vc3 = (AtSdhChannel)AtSdhLineVc3Get(line_inst, aug_id, vc3_id);
        de3  = AtSdhChannelMapChannelGet(vc3);
        if (pw_info->pw_type == PW_TYPE_SATOP) {
            pw   = (AtPw)AtModulePwSAToPCreate(pwModule, pw_info->cem_id);
            AtPwCircuitBind(pw, de3);
        } else {
            ERR("Wrong pw_circuit_type !");
        }
        break;
    case PATH_MODE_STS1: 
        vc3_sts = (AtChannel)AtSdhLineVc3Get(line_inst, aug_id, vc3_id);
        if (pw_info->pw_type == PW_TYPE_CEP) {
            pw  = (AtPw)AtModulePwCepCreate(pwModule, pw_info->cem_id, 
                cAtPwCepModeBasic);
            AtPwCircuitBind(pw, vc3_sts);
        } else {
            ERR("Wrong pw_circuit_type !");
        }
        break;
    case PATH_MODE_STS3C: 
        vc4_sts3 = (AtChannel)AtSdhLineVc4Get(line_inst, aug_id);
        if (pw_info->pw_type == PW_TYPE_CEP) {
            pw  = (AtPw)AtModulePwCepCreate(pwModule, pw_info->cem_id, 
                cAtPwCepModeBasic);
            AtPwCircuitBind(pw, vc4_sts3);
        } else {
            ERR("Wrong pw_circuit_type !");
        }
        break;
    default:
        break;
    }
    return (TRUE);
}

static boolean cem_fpga_cem_connect_config (AtDriver cem_fpga_driver,
        pw_info_t *pw_info)
{
    AtModulePw pwModule;
    AtPw pw;
    AtPwMplsPsn mplsPsn = AtPwMplsPsnNew();
    tAtEthVlanTag cVlan;
    tAtPwMplsLabel label;
    AtDevice device =
        AtDriverDeviceGet(cem_fpga_driver, CEM_FPGA_CORE_ID);
    AtModuleEth ethModule = 
        (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);

    /* Get the pw module */
    pwModule = 
        (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);

    /* Get the Pw */
    pw = AtModulePwGetPw(pwModule, (uint16_t)pw_info->cem_id);

    /* Set mpls header */
    label.label = pw_info->local_mpls_label;
    label.timeToLive = 255; /* Check with Anand */
    label.experimental = 0;
    AtPwMplsPsnInnerLabelSet(mplsPsn, &label);
    AtPwMplsPsnOuterLabelAdd(mplsPsn, &label);

    /* Set mpls expected label as inner label */
    AtPwMplsPsnExpectedLabelSet(mplsPsn, pw_info->local_mpls_label); 

    /* bind the mpls info with pw */
    AtPwPsnSet(pw, (AtPwPsn)mplsPsn);

    /* delete the mplspsn obj in tis context */
    AtObjectDelete((AtObject)mplsPsn);

    /* Get the egress Ethernet port */
    AtPwEthPortSet(pw, AtModuleEthPortGet(ethModule, 
                EGRESS_ETH_PORT));

    /* Fill the vlan fields */
    cVlan.cfi = 0;
    cVlan.priority = 0;
    cVlan.vlanId = CEM_VLAN_ID;

    /* Fill Ethernet header */
    AtPwEthHeaderSet(pw, pw_info->dst_mac, &cVlan, NULL);

    ERR("cem connect done");
    return (TRUE);
}

static boolean cem_fpga_cem_channel_enable (AtDriver cem_fpga_driver,
        uint16_t cem_id)
{
    AtModulePw pwModule;
    AtPw pw;
    AtDevice device =
        AtDriverDeviceGet(cem_fpga_driver, CEM_FPGA_CORE_ID);

    /* Get the pw module */
    pwModule = 
        (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);

    /* Get the pw for the given cem_id */
    pw = AtModulePwGetPw(pwModule, cem_id);

    /* Enable the pw */
    AtChannelEnable((AtChannel)pw, cAtTrue);
    return (TRUE);
}

static boolean cem_fpga_cem_connect_unconfig (AtDriver cem_fpga_driver,
        uint16_t cem_id)
{
    AtModulePw pwModule;
    eAtRet ret = cAtOk;
    AtDevice device =
        AtDriverDeviceGet(cem_fpga_driver, CEM_FPGA_CORE_ID);

    /* Get the pw module */
    pwModule = 
        (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);

    /* Delete the pw for the given cem_id */
    ret = AtModulePwDeletePw(pwModule, cem_id)
    if (!ret) {
        return (TRUE);
    } else {
        return (FALSE);
    }
}

static boolean cem_fpga_cem_intf_unconfig (AtDriver cem_fpga_driver,
        uint32_t port_num, uint16_t t3e3, uint16_t t1e1_num)
{
    /* Delete the mapping done by SDK */
    return(TRUE);
}

static boolean cem_fpga_t1e1_config (AtDriver cem_fpga_driver)
{
    return (TRUE);
}

static boolean cem_fpga_t3e3_config (AtDriver cem_fpga_driver)
{
    return (TRUE);
}

static boolean cem_fpga_line_config (AtDriver cem_fpga_driver,
        uint16_t port_num, ctrlr_framing_e framing, port_rate_e rate)
{
    AtSdhLine line_inst = cem_fpga_get_line(cem_fpga_driver, port_num);

    switch (framing) {
    case FRAMING_SONET:
        ERR("FRAMING SONET");
        AtSdhLineModeSet(line_inst, cAtSdhLineModeSonet);
        break;
    case FRAMING_SDH:
        ERR("FRAMING SDH");
        AtSdhLineModeSet(line_inst, cAtSdhLineModeSdh);
        break;
    default:
        ERR("Wrong framing type");
        return (FALSE);
    }

    switch (rate) {
    case OC3:
        ERR("Rate OC3");
        AtSdhLineRateSet(line_inst, cAtSdhLineRateStm1);
        break;
    case OC12:
        ERR("Rate OC12");
        AtSdhLineRateSet(line_inst, cAtSdhLineRateStm4);
        break;
    case OC48:
        ERR("Rate OC48");
        AtSdhLineRateSet(line_inst, cAtSdhLineRateStm16);
        break;
    case OC192:
    default:
        ERR("Wrong rate type");
        return (FALSE);
    }
    return (TRUE);
}



/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : sdsf_tree.c
 *
 * Created Date: Dec 14, 2015
 *
 * Description : SD/SF tree triggering example
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCiscoUpsr.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumStsInOc48 48
#define cNumStsInAug1 3
#define cNumBitsInDword 32

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 SdSfBaseAddress(AtDevice device)
    {
    return (uint32_t)(AtHalBaseAddressGet(AtDeviceIpCoreHalGet(device, 0)) + (AtCiscoUpsrDwordStartOffset() << 2));
    }

static uint32 FlatStsId(AtDevice device, uint8 lineId, uint8 aug1Id, uint8 au3Id)
    {
    return (lineId * cNumStsInOc48) + (aug1Id * cNumStsInAug1) + au3Id;
    }

void Au3SdSfInterruptEnable(AtDevice device, uint8 lineId, uint8 aug1Id, uint8 au3Id)
    {
    uint32 baseAddress = SdSfBaseAddress(device);
    uint32 sts1Id = FlatStsId(device, lineId, aug1Id, au3Id);
    uint32 rowId    = sts1Id / cNumBitsInDword;
    uint32 bitIndex = sts1Id % cNumBitsInDword;
    uint32 regVal;

    /* Enable global interrupt. */
    AtCiscoUpsrMASRMaskWrite(baseAddress, 1);

    /* Enable STS SD interrupt */
    regVal  = AtCiscoUpsrStsMaskRead(baseAddress, cAtCiscoUpsrAlarmSd, rowId);
    regVal |= (1 << bitIndex);
    AtCiscoUpsrStsMaskWrite(baseAddress, cAtCiscoUpsrAlarmSd, rowId, regVal);

    /* Enable STS SF interrupt */
    regVal  = AtCiscoUpsrStsMaskRead(baseAddress, cAtCiscoUpsrAlarmSf, rowId);
    regVal |= (1 << bitIndex);
    AtCiscoUpsrStsMaskWrite(baseAddress, cAtCiscoUpsrAlarmSf, rowId, regVal);
    }

/* As HyPhy may terminate Pointer, it maybe not possible to detect Pointer and
 * Overhead alarm. Just for testing the interrupt PIN purpose, let's make a
 * TFI-5 local loopback.
 *
 * This function will trigger interrupt by force/unforce AIS alarm */
void Au3SdSfTriggerInterrupt(AtDevice device, uint8 lineId, uint8 aug1Id, uint8 au3Id)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtChannel line = (AtChannel)AtModuleSdhLineGet(sdhModule, lineId);
    AtChannel au3 = (AtChannel)AtSdhLineAu3Get((AtSdhLine)line, aug1Id, au3Id);

    /* Make a loopback it it does not exist */
    if (AtChannelLoopbackGet(line) != cAtLoopbackModeLocal)
        AtChannelLoopbackSet(line, cAtLoopbackModeLocal);

    /* Toggle alarm forcing and interrupt PIN is expected to be raised */
    if (AtChannelTxForcedAlarmGet(au3) & cAtSdhPathAlarmAis)
        AtChannelTxAlarmForce(au3, cAtSdhPathAlarmAis);
    else
        AtChannelTxAlarmUnForce(au3, cAtSdhPathAlarmAis);
    }

/* When interrupt happen, it needs to be processed, otherwise, the interrupt
 * PIN will raise forever */
void InterruptProcess(AtDevice device)
    {
    AtCiscoUpsrInterruptProcess(SdSfBaseAddress(device));
    }

/* $Id: diag_im_3gms_traffic.c,v $
 * $Source: plat/rudy/im/cem_lc/3gms/diag_im_3gms_traffic.c,v  $
 *------------------------------------------------------------------
 * Filename: diag_im_3gms_traffic.c
 *
 * Description: Vz IM 3G MS Traffic file
 *
 * Copyright (c) 2016 by CISCO Systems, Inc.
 * All rights reserved.
 *------------------------------------------------------------------
 */

#include <stdio.h>
#include <time.h>

#include "diag.h"
#include "types.h"
#include "diag_im_3gms_lib.h"
#include "diag_im_3gms_liu.h"
#include "diag_im_cem_lib.h"
#include "diag_arad_tests.h"
#include "diag_arrive.h"
#include "diag_im_common_lib.h"
#include "diag_rsp_common_lib.h"
#include "diag_data_fpga_lib.h"
#include "queryflags.h"

#define int32 dontcare
#include "diag_arad_lib.h" /* This is as per im_lpbk.c fix */
#undef int32

int printf(const char *, ...); /* Not able to find #include */
//extern int sprintf (char *destbuf, char *fmtptr, ...); /* Not able to find #include */
int bshell(int unit, char *cmd); /* No include file */
int im_get_arad_port (int imslot, int im_port, arad_nif_t *arad_port_info);

int total_engines[IM_SLOT_MAX] = {0};
extern uchar continuousStart[IM_SLOT_MAX];
extern int serdesSelectContinuousTest[IM_SLOT_MAX];
extern arad_nif_t aradPortIinfoContinuousTest[IM_SLOT_MAX];
extern uchar numberOfSlotsInQSGMII;

static int cem_bert_prbs_traffic_test (int param);
static int cem_bert_prbs_traffic_test_start (int param);
static int cem_bert_prbs_traffic_test_check (int param);
static int cem_bert_prbs_traffic_test_stop (int param);

static diag_menu_t diag_im_3gms_traffic_menu[] = {
    {"DS1 PRBS traffic Test", cem_bert_prbs_traffic_test, 0x11, IM_3GMS_DEF_FLAG,
            NULL, NULL, 0, 0},
    {"DS3 PRBS traffic Test", cem_bert_prbs_traffic_test, 0x12, IM_3GMS_DEF_FLAG,
            NULL, NULL, 0, 0},
    {"OCx PRBS traffic Test", cem_bert_prbs_traffic_test, 0x14, IM_3GMS_DEF_FLAG,
            NULL, NULL, 0, 0},
    {"PRBS Continuous Transmit Start", cem_bert_prbs_traffic_test_start, 0, IM_3GMS_DEF_FLAG,
            NULL, NULL, 0, 0},
    {"PRBS Continuous Transmit Status Check", cem_bert_prbs_traffic_test_check, 0, IM_3GMS_DEF_FLAG,
            NULL, NULL, 0, 0},
    {"PRBS Continuous Transmit Stop", cem_bert_prbs_traffic_test_stop, 0, IM_3GMS_DEF_FLAG,
            NULL, NULL, 0, 0},
    {"Arrive CLI", ocn_arr_cli_test, 0, 0, NULL, NULL, 0, 0},
    {"BCM Shell", diag_invoke_bcm_shell, 0, 0, NULL, NULL, 0, 0},
};

#define IM_3GMS_TRAFFIC_MENU_SIZE  \
    (sizeof(diag_im_3gms_traffic_menu) / sizeof(diag_menu_t))

int diag_menu_traffic_test (int show_menu)
{
    int ret_val = PASSED;

    if (show_menu == FOX_TREE_MAGIC_WORD) {
        ret_val = diag_build_menu_tree("3GMS Traffic test menu",
                diag_im_3gms_traffic_menu, IM_3GMS_TRAFFIC_MENU_SIZE);
        return (ret_val);
    } else {
        testname("3GMS Traffic test menu");
        ret_val = diag_menu_exec_all_items(diag_im_3gms_traffic_menu,
                IM_3GMS_TRAFFIC_MENU_SIZE);
        return (ret_val);
    }
}

/*******************************************************************
 *
 * Function: cem_bert_prbs_traffic_test
 *
 * Description: Function to start different traffic
 *
 * Input : Param shall contain following details
 *          0x1  = DS1
 *          0x2  = DS3
 *          0x4  = OCn
 *
 *          0x10 = XFI
 *          0x20 = QSGMII
 *
 * Output: PASSED/FAILED
 *
 *******************************************************************
 */
static int cem_bert_prbs_traffic_test (int param)
{
    int value = 0, ix = 0, ret = PASSED;
    arad_nif_t arad_port_info = {0,0};
    char buffer[100];
    time_t start_time = 0;
    time_t current_time = 0;
    time_t last_print_count = 0;
    unsigned long wait_time = 0;
    int liu_mode = 0, loopback_mode = 0, serdes_select = 0;
    unsigned long ittr = 0;
    uint lockCheck = 0;
    uint32 lineId = 0, engineId = 0, engines = 0;

    if (param == FOX_TREE_MAGIC_WORD) {
        return (PASSED);
    }

    if (!(diag_global_flag & DIAG_FLAG_EXT_LOOPBACK)) {
        /* Do not run test if EXT_LOOPBACK flag is OFF */
        printf("\nExt Loopback OFF. External Loopback Test skipped.\n");
        return (PASSED);
    }

    if (FALSE == g_arad_init_flag) {
        printf("Warning: Arad in not initialized.First initialize Arad.\n");
        return (FAILED);
    }

    /* XFI or QSGMII */
    serdes_select = param & 0x10;

    tLiuSatopDe1 de1 = { 0, cAtPdhDs1J1UnFrm, cPsnMpls, cAtEthPortInterfaceXGMii,
        cAtLoopbackModeRelease,
        cAtPdhLoopbackModeRelease
    };
    tLiuSatopDe3 de3 = { 0, cAtPdhDs3Unfrm, cPsnMpls, cAtEthPortInterfaceXGMii,
        cAtLoopbackModeRelease,
        cAtPdhLoopbackModeRelease
    };

    /* If 907, check if slot supports XFI or QSGMII or both */
    if (IS_BOARD_TYPE_ASR907_RSP3_400()) {
        /* If XFI, not supported in slot 0 and 1 */
        if(serdes_select == 0) {
            if((im_slot == 0) || (im_slot == 1)) {
                printf("XFI not supported in slot %d \n", im_slot);
                return (PASSED);
            }
        } else if(serdes_select == 1) {
            if((im_slot == 3) || (im_slot == 4) || (im_slot == 7)
                    || (im_slot == 8) || (im_slot == 11) || (im_slot == 12)) {
                printf("QSGMII not supported in slot %d \n", im_slot);
                return (PASSED);
            }
        }
    }

    /* If DS1 needs to be enabled */
    if ((param & 0x1) == 0x1) {
        if (diag_global_flag & DIAG_FLAG_EDVT_ON)
            liu_mode = 0;
        else
            liu_mode = gethex_answer("Select LIU mode 0-E1  1-DS1 ", 0, 0, 1);
        de1.frameType = (liu_mode == 0) ? cAtPdhE1UnFrm : cAtPdhDs1J1UnFrm;

        if (liu_mode == 0)
            liu_mode = LIU316_MODE_E1;
        else
            liu_mode = LIU316_MODE_DS1;

        if (diag_global_flag & DIAG_FLAG_EDVT_ON)
            value = 0;
        else
            value = gethex_answer
                ("Select LIU Loopback mode 0-External 1-Analog 2-Digital ",
                 0, 0, 2);
        if (value == 0)
            loopback_mode = 0; // No loopback [External Loopback]
        else if (value == 1)
            loopback_mode = LIU316_ANALOG_LPBK_SEL;
        else if (value == 2)
            loopback_mode = LIU316_DIGITAL_LPBK_SEL;

        de1.ethPortInterface = (serdes_select == 0) ?
                cAtEthPortInterfaceXGMii : cAtEthPortInterfaceQSgmii;

        /* Call LIU Init */
        debug_print("LIU initialization\n");
        im_3gms_liu316_lpbk(liu_mode | loopback_mode);

//        /* Call CEM Init */
//        if (setup_liu_de1(de1) == FAILED) {
//            printf("Loopback setup failed\n");
//            return (FAILED);
//        }
    }
    /* If DS3 needs to be enabled */
    if ((param & 0x2) == 0x2) {

        de3.ethPortInterface = (serdes_select == 0) ?
                cAtEthPortInterfaceXGMii : cAtEthPortInterfaceQSgmii;

        if (diag_global_flag & DIAG_FLAG_EDVT_ON)
            value = 0;
        else
            value = gethex_answer
                ("Select LIU Loopback mode 0-External 1-Analog 2-Digital ",
                 0, 0, 2);

        if (value == 0)
            loopback_mode = 0;  // No loopback [External Loopback]
        else if (value == 1)
            loopback_mode = LIU75L04_ANALOG_LPBK_SEL;
        else if (value == 2)
            loopback_mode = LIU75L04_DIGITAL_LPBK_SEL;

        if (diag_global_flag & DIAG_FLAG_EDVT_ON)
            liu_mode = 0;
        else
            liu_mode = gethex_answer("Select LIU mode 0-E3  1-DS3 ", 0, 0, 1);
        de3.frameType = (liu_mode == 0) ? cAtPdhE3Unfrm : cAtPdhDs3Unfrm;

        if (liu_mode == 0)
            liu_mode = LIU75L04_MODE_E3;
        else
            liu_mode = LIU75L04_MODE_DS3;

        /* Call DS3 liu init */
        im_3gms_liu75l04_lpbk(liu_mode | loopback_mode);

//        /* Call CEM init */
//        if (setup_liu_de3(de3) == FAILED) {
//            printf("Loopback setup failed\n");
//            return FAILED;
//        }
    }
    /* If OCn needs to be enabled */
    if ((param & 0x4) == 0x4) {
        /* TODO_Sathya Add OCn traffic path here */
    }

    /* Call CEM INIT, DS1 will be always 0 to 11 and DS3 will be 12 to 15 */
    create_pw_SAToP_3gms(de1, de3);

    /* Check for SERDES PLL lock status */
    do {
        if (checkSerdesPll() == PASSED)
            break;
        lockCheck++;
    } while (lockCheck < 100);
    if (lockCheck >= 100) {
        err('f', 0, "%s: CEM Eth SERDES PLL not locked", __FUNCTION__);
        return FAILED;
    }

    for (ix = 0; ix < 2; ix++) {
        /* Reset FPGA XFI serdes */
        DiagSerdesControllerReset(ix);
    }
    arad_serdes_link_reset(im_slot);

    /* If QSGMII, assign PE_PORT_0 to get QSGMII numbers */
    if (serdes_select == 1)
        arad_port_info.nif_no = ARAD_PE_PORT_0;
    im_get_arad_port(im_slot, 0, &arad_port_info);

    /* Enable remote loopback */
    if (serdes_select == 1) {
        rsp3_fpga_qsgmii_reset(im_slot);
        rsp3_qsgmii_mode_mux_config(im_slot);

        /* 903 we are looping at MUX, so ARAD loopback not required */
        if (IS_BOARD_TYPE_ASR907_RSP3_400())
        {
            sprintf(buffer, "phy diag xl48 loopback mode=r");
            printf(buffer, "\n");
            bshell(0, buffer);
            bshell(1, buffer); // ARAD 1 QSGMII is not used

            /* Enable data fpga loopback tags */
            diag_data_fpga_loopback_tags(1);
        }
    } else {
        sprintf(buffer, "phy diag xe%d loopback mode=r", arad_port_info.nif_no);
        printf(buffer, "\n");
        bshell(arad_port_info.unit, buffer);
    }

    printf("\n");
    msleep(1000);

    /* Clean up counters here both ARAD and ARRIVE */
    prverbose("Clear C and G RFCS\n");
    bcm_stat_clear(arad_port_info.unit, arad_port_info.nif_no);

    /* Create BERT engines */
    eAtPrbsMode prbsMode = cAtPrbsModePrbs31;
    eAtPrbsSide monitoringSide = cAtPrbsSidePsn;

    if ((param & 0x1) == 0x1) {
        for(lineId = 0; lineId < IM_3GMS_DS1_LINES; lineId++, engineId++)
        {
            if (cAtOk != DS1_bert_prbs_create(engineId, lineId, prbsMode,
                    monitoringSide)) {
                err('f', 0, "DS1 PRBS create failed for line %d", lineId);
                return (FAILED);    // No Clean up - Debug why failed
            }
        }
    }
    lineId = lineId+IM_3GMS_DS1_LINES;

    if ((param & 0x2) == 0x2) {
        for(; lineId < (lineId+IM_3GMS_DS3_LINES); lineId++, engineId++)
        {
            if (cAtOk != DS3_bert_prbs_create(engineId, lineId, prbsMode,
                    monitoringSide)) {
                err('f', 0, "DS3 PRBS create failed for line %d", lineId);
                return (FAILED);    // No Clean up - Debug why failed
            }
        }
    }
    /* TODO_Sathya Add for OCn also */
    if ((param & 0x4) == 0x4) {

    }

    printf("Sleep for 5 seconds\n");
    msleep(5000);
    /* Clear sticky and counters after 5 seconds wait*/
    for(engines = 0; engines < engineId; engines++)
    {
        bert_prbs_check(engines);
    }
    /* Clear Counters */
    clearCountersAndDisaplay(0);

    /* Get time for test to run*/
    if (diag_global_flag & DIAG_FLAG_EDVT_ON)
        wait_time = 60;
    else
        wait_time = getdec_answer("Enter time in seconds for test to run ", 6, 6,
                          500000);

    printf("Start \r\n");
    time(&start_time);

    /* Check till time expire */
    do {

        for(engines = 0; engines < engineId; engines++)
        {
            if (cAtOk != bert_prbs_check(engines)) {
                err('f', 0, "PRBS check failed for engine %d", engines);
                for (ix = 0; ix < gNoAradDetected; ix++) {
                    bshell(ix, "show c");
                    bshell(ix, "g rfcs");
                }
                clearCountersAndDisaplay(1);
                return (FAILED);        // No Clean up - Debug why failed
            }
            ittr++;
        }
        if (last_print_count != current_time) {
            printf("\nTime : %d        ",
                   (start_time + wait_time) - current_time);
            last_print_count = current_time;
        }

    } while ((start_time + wait_time) > time(&current_time));

    /* Clean BERT engines */
    for(engines = 0; engines < engineId; engines++)
    {
        if (cAtOk != bert_prbs_stop(engines)) {
            err('f', 0, "PRBS clean failed");
            return (FAILED);
        }
    }

    if (serdes_select == 1) {
        /* Remove Loopback */
        if (IS_BOARD_TYPE_ASR907_RSP3_400()) {
            sprintf(buffer, "phy diag xl48 loopback mode=none");
            bshell(0, buffer);
            bshell(1, buffer);

            /* Remove data fpga loopback tags */
            diag_data_fpga_loopback_tags(0);
        }
    } else {
        sprintf(buffer, "phy diag xe%d loopback mode=none",
                arad_port_info.nif_no);
        printf(buffer, "\n");
        /* Remove loopback */
        bshell(arad_port_info.unit, buffer);
    }
    printf("\n");

    return (ret);
}



static int cem_bert_prbs_traffic_test_start (int param)
{
    int value = 0, ix = 0, ret = PASSED;
    arad_nif_t arad_port_info = {0,0};
    char buffer[100];
    int liu_mode = 0, loopback_mode = 0, serdes_select = 0;
    uint lockCheck = 0;
    uint32 lineId = 0, engineId = 0, engines = 0;

    if (param == FOX_TREE_MAGIC_WORD) {
        return (PASSED);
    }

    if (!(diag_global_flag & DIAG_FLAG_EXT_LOOPBACK)) {
        /* Do not run test if EXT_LOOPBACK flag is OFF */
        printf("\nExt Loopback OFF. External Loopback Test skipped.\n");
        return (PASSED);
    }

    if (FALSE == g_arad_init_flag) {
        printf("Warning: Arad in not initialized.First initialize Arad.\n");
        return (FAILED);
    }

    /* XFI or QSGMII */
    serdes_select = param & 0x10;

    tLiuSatopDe1 de1 = { 0, cAtPdhDs1J1UnFrm, cPsnMpls, cAtEthPortInterfaceXGMii,
        cAtLoopbackModeRelease,
        cAtPdhLoopbackModeRelease
    };
    tLiuSatopDe3 de3 = { 0, cAtPdhDs3Unfrm, cPsnMpls, cAtEthPortInterfaceXGMii,
        cAtLoopbackModeRelease,
        cAtPdhLoopbackModeRelease
    };

    /* If 907, check if slot supports XFI or QSGMII or both */
    if (IS_BOARD_TYPE_ASR907_RSP3_400()) {
        /* If XFI, not supported in slot 0 and 1 */
        if(serdes_select == 0) {
            if((im_slot == 0) || (im_slot == 1)) {
                printf("XFI not supported in slot %d \n", im_slot);
                return (PASSED);
            }
        } else if(serdes_select == 1) {
            if((im_slot == 3) || (im_slot == 4) || (im_slot == 7)
                    || (im_slot == 8) || (im_slot == 11) || (im_slot == 12)) {
                printf("QSGMII not supported in slot %d \n", im_slot);
                return (PASSED);
            }
        }
    }

    if (continuousStart[im_slot] == 0) {
        /* If DS1 needs to be enabled */
        if ((param & 0x1) == 0x1) {
            if (diag_global_flag & DIAG_FLAG_EDVT_ON)
                liu_mode = 0;
            else
                liu_mode = gethex_answer("Select LIU mode 0-E1  1-DS1 ", 0, 0, 1);
            de1.frameType = (liu_mode == 0) ? cAtPdhE1UnFrm : cAtPdhDs1J1UnFrm;

            if (liu_mode == 0)
                liu_mode = LIU316_MODE_E1;
            else
                liu_mode = LIU316_MODE_DS1;

            if (diag_global_flag & DIAG_FLAG_EDVT_ON)
                value = 0;
            else
                value = gethex_answer
                    ("Select LIU Loopback mode 0-External 1-Analog 2-Digital ",
                     0, 0, 2);
            if (value == 0)
                loopback_mode = 0; // No loopback [External Loopback]
            else if (value == 1)
                loopback_mode = LIU316_ANALOG_LPBK_SEL;
            else if (value == 2)
                loopback_mode = LIU316_DIGITAL_LPBK_SEL;

            de1.ethPortInterface = (serdes_select == 0) ?
                    cAtEthPortInterfaceXGMii : cAtEthPortInterfaceQSgmii;

            /* Call LIU Init */
            debug_print("LIU initialization\n");
            im_3gms_liu316_lpbk(liu_mode | loopback_mode);

    //        /* Call CEM Init */
    //        if (setup_liu_de1(de1) == FAILED) {
    //            printf("Loopback setup failed\n");
    //            return (FAILED);
    //        }
        }
        /* If DS3 needs to be enabled */
        if ((param & 0x2) == 0x2) {

            de3.ethPortInterface = (serdes_select == 0) ?
                    cAtEthPortInterfaceXGMii : cAtEthPortInterfaceQSgmii;

            if (diag_global_flag & DIAG_FLAG_EDVT_ON)
                value = 0;
            else
                value = gethex_answer
                    ("Select LIU Loopback mode 0-External 1-Analog 2-Digital ",
                     0, 0, 2);

            if (value == 0)
                loopback_mode = 0;  // No loopback [External Loopback]
            else if (value == 1)
                loopback_mode = LIU75L04_ANALOG_LPBK_SEL;
            else if (value == 2)
                loopback_mode = LIU75L04_DIGITAL_LPBK_SEL;

            if (diag_global_flag & DIAG_FLAG_EDVT_ON)
                liu_mode = 0;
            else
                liu_mode = gethex_answer("Select LIU mode 0-E3  1-DS3 ", 0, 0, 1);
            de3.frameType = (liu_mode == 0) ? cAtPdhE3Unfrm : cAtPdhDs3Unfrm;

            if (liu_mode == 0)
                liu_mode = LIU75L04_MODE_E3;
            else
                liu_mode = LIU75L04_MODE_DS3;

            /* Call DS3 liu init */
            im_3gms_liu75l04_lpbk(liu_mode | loopback_mode);

    //        /* Call CEM init */
    //        if (setup_liu_de3(de3) == FAILED) {
    //            printf("Loopback setup failed\n");
    //            return FAILED;
    //        }
        }
        /* If OCn needs to be enabled */
        if ((param & 0x4) == 0x4) {
            /* TODO_Sathya Add OCn traffic path here */
        }

        /* Call CEM INIT, DS1 will be always 0 to 11 and DS3 will be 12 to 15 */
        create_pw_SAToP_3gms(de1, de3);

        do {
            if (checkSerdesPll() == PASSED)
                break;
            lockCheck++;
        } while (lockCheck < 100);

        if (lockCheck >= 100) {
            err('f', 0, "%s: CEM SERDES PLL not locked", __FUNCTION__);
            return FAILED;
        }

        for (ix = 0; ix < 2; ix++) {
            /* Reset FPGA XFI serdes */
            DiagSerdesControllerReset(ix);
        }
        arad_serdes_link_reset(im_slot);

        /* Assign to global */
        serdesSelectContinuousTest[im_slot] = serdes_select;

        /* If QSGMII, assign PE_PORT_0 to get QSGMII numbers */
        if (serdes_select == 1)
            arad_port_info.nif_no = ARAD_PE_PORT_0;
        im_get_arad_port(im_slot, 0, &arad_port_info);

        /* Assign the global */
        aradPortIinfoContinuousTest[im_slot] = arad_port_info;

        /* Enable remote loopback */
        if (serdes_select == 1) {
            rsp3_fpga_qsgmii_reset(im_slot);
            rsp3_qsgmii_mode_mux_config(im_slot);

            /* 903 we are looping at MUX, so ARAD loopback not required */
            if (IS_BOARD_TYPE_ASR907_RSP3_400())
            {
                sprintf(buffer, "phy diag xl48 loopback mode=r");
                printf(buffer, "\n");
                bshell(0, buffer);
                bshell(1, buffer); // ARAD 1 QSGMII is not used

                /* Enable data fpga loopback tags */
                diag_data_fpga_loopback_tags(1);
            }
        } else {
            sprintf(buffer, "phy diag xe%d loopback mode=r", arad_port_info.nif_no);
            printf(buffer, "\n");
            bshell(arad_port_info.unit, buffer);
        }

        /* Set start to 1 */
        continuousStart[im_slot] = 1;

        printf("\n");
        msleep(1000);

        /* Clean up counters here both ARAD and ARRIVE */
        prverbose("Clear C and G RFCS\n");
        bcm_stat_clear(arad_port_info.unit, arad_port_info.nif_no);


        /* Create BERT engines */
        eAtPrbsMode prbsMode = cAtPrbsModePrbs31;
        eAtPrbsSide monitoringSide = cAtPrbsSidePsn;

        if ((param & 0x1) == 0x1) {
            for(lineId = 0; lineId < IM_3GMS_DS1_LINES; lineId++, engineId++)
            {
                if (cAtOk != DS1_bert_prbs_create(engineId, lineId, prbsMode,
                        monitoringSide)) {
                    err('f', 0, "DS1 PRBS create failed for line %d", lineId);
                    return (FAILED);    // No Clean up - Debug why failed
                }
            }
        }
        lineId = lineId+IM_3GMS_DS1_LINES;

        if ((param & 0x2) == 0x2) {
            for(; lineId < (lineId+IM_3GMS_DS3_LINES); lineId++, engineId++)
            {
                if (cAtOk != DS3_bert_prbs_create(engineId, lineId, prbsMode,
                        monitoringSide)) {
                    err('f', 0, "DS3 PRBS create failed for line %d", lineId);
                    return (FAILED);    // No Clean up - Debug why failed
                }
            }
        }
        /* TODO_Sathya Add for OCn also */
        if ((param & 0x4) == 0x4) {

        }

        total_engines[im_slot] = engineId;

        printf("Sleep for 5 seconds\n");
        msleep(5000);
        /* Clear sticky and counters after 5 seconds wait*/
        for(engines = 0; engines < total_engines[im_slot]; engines++)
        {
            /* Don't check for alarms, just clear */
            bert_prbs_check(engines);
        }

        /* Clear Counters */
        clearCountersAndDisaplay(0);
    } else {
        printf("Already running\n");
    }
    return ret;
}

static int cem_bert_prbs_traffic_test_check (int param)
{
    int ix = 0;
    int engines = 0;
    arad_nif_t arad_port_info = {0,0};

    if (param == FOX_TREE_MAGIC_WORD) {
        return (PASSED);
    }
    printf("\n");

    if (continuousStart[im_slot] == 1) {

        /* Check till time expire */
        for(engines = 0; engines < total_engines[im_slot]; engines++)
        {
            if (cAtOk != bert_prbs_check(engines)) {
                err('f', 0, "PRBS check failed for engine %d", engines);
                for (ix = 0; ix < gNoAradDetected; ix++) {
                    bshell(ix, "show c");
                    bshell(ix, "g rfcs");
                }
                clearCountersAndDisaplay(1);
                return (FAILED);        // No Clean up - Debug why failed
            }
        }
        printf("No alarms\n");

        /* Clear ARAD and CEM counters */
        im_get_arad_port(im_slot, 0, &arad_port_info);
        bcm_stat_clear(arad_port_info.unit, arad_port_info.nif_no);
        clearCountersAndDisaplay(0);

    } else {
        printf("Continuous test not running\n");
    }
    return (PASSED);
}

static int cem_bert_prbs_traffic_test_stop (int param)
{
    char buffer[100];
    int engines = 0;
    arad_nif_t arad_port_info = aradPortIinfoContinuousTest[im_slot];

    if (param == FOX_TREE_MAGIC_WORD) {
        return (PASSED);
    }
    printf("\n");

    if (continuousStart[im_slot] == 1) {
        printf("Stopping simple BERT PRBS\n");
        for(engines = 0; engines < total_engines[im_slot]; engines++)
        {
            if (cAtOk != bert_prbs_stop(engines)) {
                err('f', 0, "PRBS clean failed");
                return (FAILED);
            }
        }

        if (serdesSelectContinuousTest[im_slot] == 1) {
            /* Check if any other slots running in QSGMII */
            numberOfSlotsInQSGMII--;
            if(numberOfSlotsInQSGMII == 0)
            {
                /* Remove Loopback */
                if (IS_BOARD_TYPE_ASR907_RSP3_400()) {
                    sprintf(buffer, "phy diag xl48 loopback mode=none");
                    printf(buffer, "\n");
                    bshell(0, buffer);
                    bshell(1, buffer);
                    /* Disable data fpga loopback tage */
                    diag_data_fpga_loopback_tags(0);
                }
            }

        } else {
            sprintf(buffer, "phy diag xe%d loopback mode=none",
                    arad_port_info.nif_no);
            printf(buffer, "\n");
            /* Remove loopback */
            bshell(arad_port_info.unit, buffer);
        }
        continuousStart[im_slot] = 0;
    } else {
        printf("Continuous test not running!!\n");
    }
    return (PASSED);
}


/*------------------------------------------------------------------
 * arrive_cep.h :  Header file  Arrive CEM/CES FPGA support.
 *
 * May 2015, Yasdixit
 *
 * Copyright (c) 2015-2016 by Cisco Systems, Inc.
 *
 * Description : 
 *------------------------------------------------------------------
 */
 
/*--------------------------- Include files ----------------------------------*/

#ifndef __ARRIVE_CEP_H__
#define __ARRIVE_CEP_H__

#include "AtDriver.h"
#include "AtEthPort.h"
#include "AtSdhLine.h"
#include "AtSdhAug.h"
#include "AtSdhVc.h"
#include "AtSdhTug.h"
#include "AtPw.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumAug4InAug16 4
#define cNumAug1InAug16 16
#define cNumVc3InAug1   3
#define cNumTug2InVc3   7
#define cNumVc12InTug2  3


/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

#endif  /* __ARRIVE_CEP_H__ */


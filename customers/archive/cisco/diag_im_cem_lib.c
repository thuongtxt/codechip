/*
 * Filename: diag_im_cem_lib.c
 *
 * Description: OCx and DSx CEM Line card library	
 *
 * May 2015, Yasdixit
 *
 * Copyright (c) 2015-2016 by Cisco Systems, Inc.
 * All rights reserved.
 *------------------------------------------------------------------
 */

/********************** Include Files ***************************************/
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "fcntl.h"
#include "errno.h"
#include <sys/mman.h>
 
#include "diag.h"
#include "types.h"
#include "../../../common/include/byteswap.h"
#include "queryflags.h"
#include "diag_im_tests.h"
#include "diag_pcie_lib.h"
#include "diag_im_t1e1.h"
#include "diag_im_common_lib.h"
#include "diag_im_wintegra_lib.h"
#include "diag_rsp_common_lib.h"
#include "diag_im_cem_lib.h"

/***********************************************************************
 *  Macro Processor Definitions
 ************************************************************************/
#include "diag_hyphy.h"


#define CEM_DEBUG   \
        if (diag_global_flag & DIAG_FLAG_DEBUG) printf


#define CEM_FPGA_DEF_FLAG   (DIAG_FLAG_CONTINUOUS | DIAG_FLAG_SHOW_ERR_COUNT \
                         | DIAG_FLAG_DO_ALL)

/***********************************************************************
 *  Static Functions Declaration 
 ************************************************************************/

static uint cem_pci_config_read_by_tlp(int, uint32, uint32, int);
static void cem_pci_config_write_by_tlp(int, uint32, uint32, uint32, int);

static int cem_im_wait_tlp_done(int, int);

static uint hyphy_pci_config_read_by_tlp(int, uint32, uint32, int);
static void hyphy_pci_config_write_by_tlp(int, uint32, uint32, uint32, int);
static int hyphy_im_wait_tlp_done(int, int);
static dumpHyPhyConfig(int);
static int diag_menu_cem_fpga_utility(int);
static int diag_menu_cem_fpga_tests (int );
static int cem_reg_peek (int );
static int cem_reg_poke (int );
static int cem_reg_data_peek (int );
static int cem_reg_data_poke (int );
 int ocn_arr_cli_test (int );
static int cem_cfg_peek (int );
static int cem_cfg_poke (int );

/***********************************************************************
 *  Functions Declaration
 ************************************************************************/
uint hyphy_pci_config_read (int slot_no, uint32 func, uint32 offset, int rc_id);
int diag_menu_cem_fpga(int);

/***********************************************************************
 *  Extern Functions Declaration
 ************************************************************************/

extern void wastetime(long);

int testFunc(int );
extern int diag_cem_fpga_device_id_test (int );
extern int diag_cem_fpga_prbs_test (int);
extern int diag_cem_fpga_prbs_test_e1 (int show_menu);
extern int PwPrbsEnable(int enable);
extern int PwPrbsCheck(int stickyClear);
extern int im_ocn_ddr4_test (int );
extern int im_ocn_qdr_test (int );
extern int im_reconfigure_cem_fpga (int);
extern int im_ocn_ddr4_BIST_test (int);
extern int im_ocn_qdr_BIST_test (int);
int arrive_cli_shell (int );


 /***********************************************************************
 *  Global Variable
 ************************************************************************/
static diag_menu_t cem_fpga_utility_menu[] = {
    {"CEM Peek",                cem_reg_peek, 0, 0, NULL, NULL, 0, 0},
    {"CEM Poke",                cem_reg_poke, 0, 0, NULL, NULL, 0, 0},
    {"DATA FPGA Peek",          cem_reg_data_peek, 0, 0, NULL, NULL, 0, 0},
    {"DATA FPGA Poke",          cem_reg_data_poke, 0, 0, NULL, NULL, 0, 0},
    {"CEM Cfg Peek",            cem_cfg_peek, 0, 0, NULL, NULL, 0, 0},
    {"CEM Cfg Poke",            cem_cfg_poke, 0, 0, NULL, NULL, 0, 0},
    {"Arrive CLI",              ocn_arr_cli_test, 0, 0, NULL, NULL, 0, 0},
    {"Reconfig FPGA",           im_reconfigure_cem_fpga, 0, 0, NULL, NULL, 0, 0},
};

#define CEM_FPGA_UTILITY_MENU_SIZE \
        (sizeof(cem_fpga_utility_menu) / sizeof(diag_menu_t))

static diag_menu_t cem_fpga_menu[] = {
    {"CEM FPGA Tests",   diag_menu_cem_fpga_tests, 0, CEM_FPGA_DEF_FLAG, 
                                        NULL, 0, 0, 0},
    {"CEM FPGA Utility", diag_menu_cem_fpga_utility, 0, 0, 
                                        NULL, NULL, NULL, 0},                                        
};

#define CEM_FPGA_MENU_SIZE \
        (sizeof(cem_fpga_menu) / sizeof(diag_menu_t))

static diag_menu_t cem_fpga_test_menu[] = {
    {"Cem Fpga Device ID Test",       diag_cem_fpga_device_id_test, 0, 0, 
                                        NULL, NULL, NULL, 0},
    {"Cem Fpga PRBS Tests",           diag_cem_fpga_prbs_test, 0, 0, \
                                        NULL, NULL, NULL, 0},
    {"CEM DDR4(OCN)/DDR3(DSx)  Test", im_ocn_ddr4_BIST_test, 0, CEM_FPGA_DEF_FLAG, \
                                        NULL, NULL, 0, 0},
    {"CEM QDR   Test",                im_ocn_qdr_BIST_test, 0, CEM_FPGA_DEF_FLAG, \
                                        NULL, NULL, 0, 0},
};

#define CEM_FPGA_TEST_MENU_SIZE \
        (sizeof(cem_fpga_test_menu) / sizeof(diag_menu_t))

 /***********************************************************************
 *  Static Variable
 ************************************************************************/


/* At the time this code is writtent, the FPGA is not ready to provide the
 * PCIe configuration read/write function
 * Instead, we need to construct TLP by ourselves.
 */
/* static int pcie_config_by_tlp = TRUE; */

 static int  fd_im0 = 0, fd_im1 = 0, fd_im2 = 0, fd_im3 = 0, fd_im4 = 0, fd_im5 = 0;
 static int  cem_fd_im6 = 0, cem_fd_im7 = 0, cem_fd_im8 = 0, cem_fd_im9 = 0, cem_fd_im10 = 0, cem_fd_im11 = 0;
 static int  cem_fd_im12 = 0, cem_fd_im13 = 0, cem_fd_im14 = 0, cem_fd_im15 = 0;

 static int  fd_im6 = 0, fd_im7 = 0, fd_im8 = 0, fd_im9 = 0, fd_im10 = 0, fd_im11 = 0;
 static int  fd_im12 = 0, fd_im13 = 0, fd_im14 = 0, fd_im15 = 0;

 static int  hyphy_fd_im0 = 0, hyphy_fd_im1 = 0, hyphy_fd_im2 = 0, hyphy_fd_im3 = 0, hyphy_fd_im4 = 0, hyphy_fd_im5 = 0;
 static int  hyphy_fd_im6 = 0, hyphy_fd_im7 = 0, hyphy_fd_im8 = 0, hyphy_fd_im9 = 0, hyphy_fd_im10 = 0, hyphy_fd_im11 = 0;
 static int  hyphy_fd_im12 = 0, hyphy_fd_im13 = 0, hyphy_fd_im14 = 0, hyphy_fd_im15 = 0;


/***********************************************************************
 *  Functions
 ************************************************************************/

/**********************************************************************
 *
 * Function: cem_config_fpga_bar
 *
 * Description: This function configures IM FPGA BAR2/3 and BAR4/5 for
 *              address translation between the RSP domain and winpath
 *              domain
 *
 **********************************************************************
 */
void cem_config_fpga_iofpga_bar (void)
{
    int slot_no;

    slot_no = im_get_current_slot_no();

    /* Write BAR2/3 into FPGA register */
//    im_fpga_reg_write(slot_no, IM_CEM_PCIE_BAR2_OFFSET, IM_CEM_FPGA_BAR2_ADDR);
    im_fpga_reg_write(slot_no, IM_CEM_PCIE_BAR2_OFFSET, 0x00000000);
    im_fpga_reg_write(slot_no, IM_CEM_PCIE_BAR3_OFFSET, 4);

}

/**********************************************************************
 *
 * Function: cem_config_fpga_bar
 *
 * Description: This function configures IM FPGA BAR2/3 and BAR4/5 for
 *              address translation between the RSP domain and winpath
 *              domain
 *
 **********************************************************************
 */
void cem_config_hyphy_iofpga_bar (void)
{
    int slot_no;

    slot_no = im_get_current_slot_no();

    /* Write BAR2/3 into FPGA register */
    im_fpga_reg_write(slot_no, IM_HYPHY_PCIE_BAR2_OFFSET, 0);
    im_fpga_reg_write(slot_no, IM_HYPHY_PCIE_BAR3_OFFSET, 0x0);

}


/**********************************************************************
 *
 * Function: cem_ocn_pci_init
 *
 * Description: Init OCn IM PCIe sybsystem
 *
 **********************************************************************
 */
int cem_ocn_pci_init ()
{
    int slot_no;

    slot_no = im_get_current_slot_no();

    system("mknod /dev/mem30 c `sed -n 's/ mem$//p' /proc/devices` 1");
    system("mknod /dev/mem31 c `sed -n 's/ mem$//p' /proc/devices` 1");	
    system("mknod /dev/mem32 c `sed -n 's/ mem$//p' /proc/devices` 1");
    system("mknod /dev/mem33 c `sed -n 's/ mem$//p' /proc/devices` 1");	
    system("mknod /dev/mem34 c `sed -n 's/ mem$//p' /proc/devices` 1");
    system("mknod /dev/mem35 c `sed -n 's/ mem$//p' /proc/devices` 1");	

    /* Configure BAR offset at IM FPGA */
    cem_config_fpga_iofpga_bar();

    cem_config_hyphy_iofpga_bar();
	
    /* Reset ?? */

    /* Init PCI interface */
    if (cem_fpga_pci_init(slot_no) == FAILED) {
        return (FAILED);
    }

    if (cem_hyphy_pci_init(slot_no) == FAILED) {
        return (FAILED);
    }

#if 0
    if( PASSED != cem_fpga_pcie_mmap()) {
        printf("\n cem_fpga_pcie_mmap FAILED : %s %d \n",__FUNCTION__,__LINE__);
        return FAILED;
    }

    if( PASSED != cem_hyphy_pcie_mmap()) {
        printf("\n cem_hyphy_pcie_mmap FAILED : %s %d \n",__FUNCTION__,__LINE__);
        return FAILED;
     }
#endif

    return (PASSED);
}


/**********************************************************************
 *
 * Function: cem_ds1_ds3_pci_init
 *
 * Description: Init DS1/DS3 IM PCIe sybsystem
 *
 **********************************************************************
 */
int cem_ds1_ds3_pci_init ()
{
    int slot_no;

    slot_no = im_get_current_slot_no();

    system("mknod /dev/mem30 c `sed -n 's/ mem$//p' /proc/devices` 1");
    system("mknod /dev/mem31 c `sed -n 's/ mem$//p' /proc/devices` 1");	
    system("mknod /dev/mem32 c `sed -n 's/ mem$//p' /proc/devices` 1");
    system("mknod /dev/mem33 c `sed -n 's/ mem$//p' /proc/devices` 1");	
    system("mknod /dev/mem34 c `sed -n 's/ mem$//p' /proc/devices` 1");
    system("mknod /dev/mem35 c `sed -n 's/ mem$//p' /proc/devices` 1");	


    /* Configure BAR offset at IM FPGA */
    cem_config_fpga_iofpga_bar();

    /* Reset ?? */

    /* Init PCI interface */
    if (cem_fpga_pci_init(slot_no) == FAILED) {
        return (FAILED);
    }
#if 0
    if( PASSED != cem_fpga_pcie_mmap()) {
        printf("\ncem_fpga_reg_read FAILED : %s %d \n",__FUNCTION__,__LINE__);
    }
#endif
    return (PASSED);
}

static dumpCEMConfig(int slot_no)
{
debug_print("\n....CEM Dump...\n");
debug_print("dev id : %x\n",cem_pci_config_read(slot_no, 0, VENDOR_DEVICE_ID_REG, 0));
debug_print("CMD_STATUS_REG : %x\n",cem_pci_config_read(slot_no, 0, CMD_STATUS_REG, 0));
debug_print("REVISION_ID : %x\n",cem_pci_config_read(slot_no, 0, REVISION_ID, 0));
debug_print("CASH_SIZE_LATENCY_REG : %x\n",cem_pci_config_read(slot_no, 0, CASH_SIZE_LATENCY_REG, 0));
debug_print("PCI_BASE_ADDR0_OFFSET : %x\n",cem_pci_config_read(slot_no, 0, PCI_BASE_ADDR0_OFFSET, 0));
debug_print("PCI_BASE_ADDR1_OFFSET : %x\n",cem_pci_config_read(slot_no, 0, PCI_BASE_ADDR1_OFFSET, 0));
debug_print("PCI_BASE_ADDR2_OFFSET : %x\n",cem_pci_config_read(slot_no, 0, PCI_BASE_ADDR2_OFFSET, 0));
debug_print("PCI_BASE_ADDR3_OFFSET : %x\n",cem_pci_config_read(slot_no, 0, PCI_BASE_ADDR3_OFFSET, 0));
debug_print("PCI_BASE_ADDR4_OFFSET : %x\n",cem_pci_config_read(slot_no, 0, PCI_BASE_ADDR4_OFFSET, 0));
debug_print("PCI_BASE_ADDR5_OFFSET : %x\n",cem_pci_config_read(slot_no, 0, PCI_BASE_ADDR5_OFFSET, 0));

}

/**********************************************************************
 *
 * Function: cem_fpga_pci_init
 *
 * Description: Init the PCI configuration on CEM FPGA 
 *
 **********************************************************************
 */
uint cem_fpga_pci_init (int slot_no)
{
    uint dev_id, val;

    /* Read Device ID first to make sure that it is present */
    dev_id = cem_pci_config_read(slot_no, 0, VENDOR_DEVICE_ID_REG, 0);

    if (dev_id == 0xFFFFFFFF) {
        err('f', 0, "%s: WP3 PCIe is not accessible", __FUNCTION__);
        return (FAILED);
    }

    debug_print("%s %d CEM dev_id : %x",__FUNCTION__,__LINE__,dev_id);
    prverbose("%s: CEM device id is 0x%08x\n", __FUNCTION__, dev_id);

    /* Enable Bus master and Memory space */
    val = (PCI_COMMAND_SERR | PCI_COMMAND_PARITY |
           PCI_COMMAND_BMEN | PCI_COMMAND_MEMEN);
    cem_pci_config_write(slot_no, 0, CMD_STATUS_REG, val, 0);

    /* Setup BAR 2-3 for  memory window */
//    cem_pci_config_write(slot_no, 0, PCI_BASE_ADDR0_OFFSET, IM_CEM_FPGA_BAR2_ADDR, 0);
    cem_pci_config_write(slot_no, 0, PCI_BASE_ADDR0_OFFSET, 0x00000000, 0);
    cem_pci_config_write(slot_no, 0, PCI_BASE_ADDR1_OFFSET, 4, 0);
    debug_print("\n<........................................>\n");
    dumpCEMConfig(slot_no);

    return (PASSED);
}

/**********************************************************************
 *
 * Function: cem_fpga_pci_init
 *
 * Description: Init the PCI configuration on CEM FPGA 
 * HyPhy is 32 bit pcie end point and BAR0 is configured 
 *
 **********************************************************************
 */
uint cem_hyphy_pci_init (int slot_no)
{
    uint dev_id, val;

    /* Read Device ID first to make sure that it is present */
    dev_id = hyphy_pci_config_read(slot_no, 0, VENDOR_DEVICE_ID_REG, 0);

    if (dev_id == 0xFFFFFFFF) {
        err('f', 0, "%s: WP3 PCIe is not accessible", __FUNCTION__);
        return (FAILED);
    }

    debug_print("%s %d HyPhy dev_id : %x",__FUNCTION__,__LINE__,dev_id);
    prverbose("%s: HyPhy device id is 0x%08x\n", __FUNCTION__, dev_id);

    /* Enable Bus master and Memory space */
    val = (PCI_COMMAND_SERR | PCI_COMMAND_PARITY |
           PCI_COMMAND_BMEN | PCI_COMMAND_MEMEN);
    hyphy_pci_config_write(slot_no, 0, CMD_STATUS_REG, val, 0);

    /* Setup BAR 2-3 for  memory window */
    hyphy_pci_config_write(slot_no, 0, PCI_BASE_ADDR0_OFFSET, 0, 0);
    hyphy_pci_config_write(slot_no, 0, PCI_BASE_ADDR1_OFFSET, 0, 0);

   dumpHyPhyConfig(slot_no);


    return (PASSED);
}

static dumpHyPhyConfig(int slot_no)
{
debug_print("\n....HyPhyconfig Dump...\n");
debug_print("dev id : %x\n",hyphy_pci_config_read(slot_no, 0, VENDOR_DEVICE_ID_REG, 0));
debug_print("CMD_STATUS_REG : %x\n",hyphy_pci_config_read(slot_no, 0, CMD_STATUS_REG, 0));
debug_print("REVISION_ID : %x\n",hyphy_pci_config_read(slot_no, 0, REVISION_ID, 0));
debug_print("CASH_SIZE_LATENCY_REG : %x\n",hyphy_pci_config_read(slot_no, 0, CASH_SIZE_LATENCY_REG, 0));
debug_print("PCI_BASE_ADDR0_OFFSET : %x\n",hyphy_pci_config_read(slot_no, 0, PCI_BASE_ADDR0_OFFSET, 0));
debug_print("PCI_BASE_ADDR1_OFFSET : %x\n",hyphy_pci_config_read(slot_no, 0, PCI_BASE_ADDR1_OFFSET, 0));
debug_print("PCI_BASE_ADDR2_OFFSET : %x\n",hyphy_pci_config_read(slot_no, 0, PCI_BASE_ADDR2_OFFSET, 0));
debug_print("PCI_BASE_ADDR3_OFFSET : %x\n",hyphy_pci_config_read(slot_no, 0, PCI_BASE_ADDR3_OFFSET, 0));
debug_print("PCI_BASE_ADDR4_OFFSET : %x\n",hyphy_pci_config_read(slot_no, 0, PCI_BASE_ADDR4_OFFSET, 0));
debug_print("PCI_BASE_ADDR5_OFFSET : %x\n",hyphy_pci_config_read(slot_no, 0, PCI_BASE_ADDR5_OFFSET, 0));

}

/**********************************************************************
 *
 * Function: cem_pci_config_read
 *
 * Description: Performs PCIe configuration read on WinPath3
 *
 **********************************************************************
 */
uint cem_pci_config_read (int slot_no, uint32 func, uint32 offset, int rc_id)
{

    return (cem_pci_config_read_by_tlp(slot_no, func, offset, rc_id));

}

/**********************************************************************
 *
 * Function: cem_pci_config_write
 *
 * Description: Performs PCIe configuration write on WinPath3
 *
 **********************************************************************
 */
void cem_pci_config_write (int slot_no, uint32 func, uint32 offset, uint32 data, int rc_id)
{

    return(cem_pci_config_write_by_tlp(slot_no, func, offset, data, rc_id));
}


/**********************************************************************
 *
 * Function: cem_pci_memory_read_by_tlp
 *
 * Description: Performs PCIe memory read on WinPath3 by using
 *              TLP transaction
 *
 **********************************************************************
 */
uint cem_pci_memory_read_by_tlp (int slot_no, uint addr, int rc_id)
{
    uint val;
    uint dw1, dw2, dw3, dw4, dw5;
    uint reg_offset;

    /* Load them with default value */
    dw1 = IM_MEMORY_RD_DW1_VAL;
    dw2 = IM_MEMORY_DW2_VAL;

    /* Set the requester id for CPU */
    dw2 |= IM_WINTEGRA_REQUESTER_ID;

    /* Load the offset */
    dw3 = addr;

    /* Load them up */
    reg_offset = IM_CEM_TLP_TX_DW1 + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, dw1);
    prverbose("Writing 0x%08x @%#x\n", dw1, IM_CEM_TLP_TX_DW1);

    reg_offset = IM_CEM_TLP_TX_DW2 + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, dw2);
    prverbose("Writing 0x%08x @%#x\n", dw2, IM_CEM_TLP_TX_DW2);

    reg_offset = IM_CEM_TLP_TX_DW3 + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, dw3);
    prverbose("Writing 0x%08x @%#x\n", dw3, IM_CEM_TLP_TX_DW3);

    /* Setup NUM_DWORDS to 3 and give it a go */
    val = IM_WIN_PCIE_CTRL_GO | (0x3 << IM_WIN_PCIE_CTRL_NUM_DWORDS_SHIFT);
    reg_offset = IM_CEM_PCIE_CONTROL + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, val);
    prverbose("Writing 0x%08x @%#x\n", val, IM_CEM_PCIE_CONTROL);

    if (cem_im_wait_tlp_done(slot_no, rc_id) == FAILED) {
        printf("%s: Wait TLP done failed\n", __FUNCTION__);
        return (FAILED);
    }

    reg_offset = IM_CEM_TLP_RX_DW1 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &dw1);
    prverbose("Read back 0x%08x from @%#x\n", dw1, IM_CEM_TLP_RX_DW1);

    reg_offset = IM_CEM_TLP_RX_DW2 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &dw2);
    prverbose("Read back 0x%08x from @%#x\n", dw2, IM_CEM_TLP_RX_DW2);

    reg_offset = IM_CEM_TLP_RX_DW3 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &dw3);
    prverbose("Read back 0x%08x from @%#x\n", dw3, IM_CEM_TLP_RX_DW3);

    reg_offset = IM_CEM_TLP_RX_DW4 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &dw4);
    prverbose("Read back 0x%08x from @%#x\n", dw4, IM_CEM_TLP_RX_DW4);

    reg_offset = IM_CEM_TLP_RX_DW5 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &dw5);
    prverbose("Read back 0x%08x from @%#x\n", dw5, IM_CEM_TLP_RX_DW5);

    /* Check the completion status here and length value */

    return (dw4);
}


/**********************************************************************
 *
 * Function: cem_pci_memory_write_by_tlp
 *
 * Description: Performs PCIe configuration write on WinPath3 by using
 *              TLP transaction
 *
 **********************************************************************
 */
void cem_pci_memory_write_by_tlp (int slot_no, uint32 offset, uint32 data, int rc_id)
{
    uint val;
    uint dw1, dw2, dw3, dw4;
    uint reg_offset;

    /* Load them with default value */
    dw1 = IM_MEMORY_WR_DW1_VAL;
    dw2 = IM_MEMORY_DW2_VAL;

    /* Set the requester id for CPU */
    dw2 |= IM_WINTEGRA_REQUESTER_ID;

    /* Set the bus number, device number, func number */
    dw3 = offset;

    /* Load the data into DW4 */
    dw4 = data;

    /* Load them up */
    reg_offset = IM_CEM_TLP_TX_DW1 + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, dw1);
    prverbose("Writing 0x%08x @%#x\n", dw1, IM_CEM_TLP_TX_DW1);

    reg_offset = IM_CEM_TLP_TX_DW2 + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, dw2);
    prverbose("Writing 0x%08x @%#x\n", dw2, IM_CEM_TLP_TX_DW2);

    reg_offset = IM_CEM_TLP_TX_DW3 + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, dw3);
    prverbose("Writing 0x%08x @%#x\n", dw3, IM_CEM_TLP_TX_DW3);

    reg_offset = IM_CEM_TLP_TX_DW4 + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, dw4);
    prverbose("Writing 0x%08x @%#x\n", dw4, IM_CEM_TLP_TX_DW4);

    /* Setup NUM_DWORDS to 4 and give it a go */
    val = IM_WIN_PCIE_CTRL_GO | (0x4 << IM_WIN_PCIE_CTRL_NUM_DWORDS_SHIFT);
    reg_offset = IM_CEM_PCIE_CONTROL + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, val);
    prverbose("Writing 0x%08x @%#x\n", val, IM_CEM_PCIE_CONTROL);

    if (cem_im_wait_tlp_done(slot_no, rc_id) == FAILED) {
        printf("%s: Wait TLP done failed\n", __FUNCTION__);
        return;
    }

    reg_offset = IM_CEM_TLP_RX_DW1 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &dw1);
    prverbose("Read back 0x%08x from @%#x\n", dw1, IM_CEM_TLP_RX_DW1);

    reg_offset = IM_CEM_TLP_RX_DW2 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &dw2);
    prverbose("Read back 0x%08x from @%#x\n", dw2, IM_CEM_TLP_RX_DW2);

    reg_offset = IM_CEM_TLP_RX_DW3 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &dw3);
    prverbose("Read back 0x%08x from @%#x\n", dw3, IM_CEM_TLP_RX_DW3);

    reg_offset = IM_CEM_TLP_RX_DW4 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &val);
    prverbose("Read back 0x%08x from @%#x\n", val, IM_CEM_TLP_RX_DW4);

    reg_offset = IM_CEM_TLP_RX_DW5 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &val);
    prverbose("Read back 0x%08x from @%#x\n", val, IM_CEM_TLP_RX_DW5);

    /* Check the completion status here and length value */
}


/***********************************************************************
 *  Static Functions
 ************************************************************************/

/**********************************************************************
 *
 * Function: cem_im_wait_tlp_done
 *
 * Description: This functions waits till the TLP transaction is done
 *              by FPGA
 *
 **********************************************************************
 */
static int cem_im_wait_tlp_done (int slot_no, int rc_id)
{
    uint ix, data, count = IM_WINTEGRA_TLP_TIMEOUT;
    uint reg_offset;	

    reg_offset =  IM_CEM_PCIE_CONTROL + (rc_id * 0x100);
    for (ix = 0; ix < count; ix++) {
        if (im_fpga_reg_read(slot_no, reg_offset, &data) == FAILED) {
            return (FAILED);
        }
        if ((data & IM_WIN_PCIE_CTRL_GO) == 0) {  /* complete */
            return (PASSED);
        }
        msleep(1);
    }

    return (PASSED);
}

/**********************************************************************
 *
 * Function: cem_pci_config_read_by_tlp
 *
 * Description: Performs PCIe configuration read on WinPath3 by using
 *              TLP transaction
 *
 **********************************************************************
 */
static uint cem_pci_config_read_by_tlp (int slot_no, uint32 func, uint32 offset, int rc_id)
{
    uint val;
    uint dw1, dw2, dw3, dw4, dw5;
    uint reg_offset;	

    /* Load them with default value */
    dw1 = IM_CONFIG_RD_DW1_VAL;
    dw2 = IM_CONFIG_DW2_VAL;
    dw3 = 0x0;

    /* Set the requester id for CPU */
    dw2 |= IM_WINTEGRA_REQUESTER_ID;

    /* Set the bus number, device number, func number */
    dw3 |= (IM_WINTEGRA_BUS_NO << IM_WINTEGRA_BUS_SHIFT) |
           (IM_WINTEGRA_DEV_NO << IM_WINTEGRA_DEV_SHIFT) |
           (func << IM_WINTEGRA_FUNC_SHIFT) | (offset);

    /* Load them up */
    reg_offset = IM_CEM_TLP_TX_DW1 + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, dw1);
    prverbose("Writing 0x%08x @%#x\n", dw1, IM_CEM_TLP_TX_DW1);

    reg_offset = IM_CEM_TLP_TX_DW2 + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, dw2);
    prverbose("Writing 0x%08x @%#x\n", dw2, IM_CEM_TLP_TX_DW2);

    reg_offset = IM_CEM_TLP_TX_DW3 + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, dw3);
    prverbose("Writing 0x%08x @%#x\n", dw3, IM_CEM_TLP_TX_DW3);

    /* Setup NUM_DWORDS to 3 and give it a go */
    val = IM_WIN_PCIE_CTRL_GO | (0x3 << IM_WIN_PCIE_CTRL_NUM_DWORDS_SHIFT);
    reg_offset = IM_CEM_PCIE_CONTROL + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, val);
    prverbose("Writing 0x%08x @%#x\n", val, IM_CEM_PCIE_CONTROL);

    if (cem_im_wait_tlp_done(slot_no, rc_id) == FAILED) {
        printf("%s: Wait TLP done failed\n", __FUNCTION__);
        return (FAILED);
    }

    reg_offset = IM_CEM_TLP_RX_DW1 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &dw1);
    prverbose("Read back 0x%08x from @%#x\n", dw1, IM_CEM_TLP_RX_DW1);

    reg_offset = IM_CEM_TLP_RX_DW2 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &dw2);
    prverbose("Read back 0x%08x from @%#x\n", dw2, IM_CEM_TLP_RX_DW2);

    reg_offset = IM_CEM_TLP_RX_DW3 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &dw3);
    prverbose("Read back 0x%08x from @%#x\n", dw3, IM_CEM_TLP_RX_DW3);

    reg_offset = IM_CEM_TLP_RX_DW4 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &dw4);
    prverbose("Read back 0x%08x from @%#x\n", dw4, IM_CEM_TLP_RX_DW4);

    reg_offset = IM_CEM_TLP_RX_DW5 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &dw5);
    prverbose("Read back 0x%08x from @%#x\n", dw5, IM_CEM_TLP_RX_DW5);

    /* Check the completion status here and length value */

    /* Need to swap the configuration value as FPGA flips all the incoming
     * data, including memory and configuration space. (FPGA Rev C)
     */
    dw4 = dswap4(dw4);

    return (dw4);
}


/**********************************************************************
 *
 * Function: cem_pci_config_write_by_tlp
 *
 * Description: Performs PCIe configuration write on WinPath3 by using
 *              TLP transaction
 *
 **********************************************************************
 */
static void cem_pci_config_write_by_tlp (int slot_no, uint32 func,
                                         uint32 offset, uint32 data, int rc_id)
{
    uint val;
    uint dw1, dw2, dw3, dw4;
    uint reg_offset;

    /* Load them with default value */
    dw1 = IM_CONFIG_WR_DW1_VAL;
    dw2 = IM_CONFIG_DW2_VAL;
    dw3 = 0x0;

    /* Set the requester id for CPU */
    dw2 |= IM_WINTEGRA_REQUESTER_ID;

    /* Set the bus number, device number, func number */
    dw3 |= (IM_WINTEGRA_BUS_NO << IM_WINTEGRA_BUS_SHIFT) |
           (IM_WINTEGRA_DEV_NO << IM_WINTEGRA_DEV_SHIFT) |
           (func << IM_WINTEGRA_FUNC_SHIFT) | (offset);

    /* Load the data into DW4 */
    dw4 = data;

    /* Load them up */
    reg_offset = IM_CEM_TLP_TX_DW1 + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, dw1);
    prverbose("Writing 0x%08x @%#x\n", dw1, IM_CEM_TLP_TX_DW1);

    reg_offset = IM_CEM_TLP_TX_DW2 + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, dw2);
    prverbose("Writing 0x%08x @%#x\n", dw2, IM_CEM_TLP_TX_DW2);

    reg_offset = IM_CEM_TLP_TX_DW3 + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, dw3);
    prverbose("Writing 0x%08x @%#x\n", dw3, IM_CEM_TLP_TX_DW3);

    reg_offset = IM_CEM_TLP_TX_DW4 + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, dw4);
    prverbose("Writing 0x%08x @%#x\n", dw4, IM_CEM_TLP_TX_DW4);

    /* Setup NUM_DWORDS to 4 and give it a go */
    val = IM_WIN_PCIE_CTRL_GO | (0x4 << IM_WIN_PCIE_CTRL_NUM_DWORDS_SHIFT);
    reg_offset = IM_CEM_PCIE_CONTROL + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, val);
    prverbose("Writing 0x%08x @%#x\n", val, IM_CEM_PCIE_CONTROL);

    if (cem_im_wait_tlp_done(slot_no, rc_id) == FAILED) {
        printf("%s: Wait TLP done failed\n", __FUNCTION__);
        return;
    }

    reg_offset = IM_CEM_TLP_RX_DW1 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &dw1);
    prverbose("Read back 0x%08x from @%#x\n", dw1, IM_CEM_TLP_RX_DW1);

    reg_offset = IM_CEM_TLP_RX_DW2 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &dw2);
    prverbose("Read back 0x%08x from @%#x\n", dw2, IM_CEM_TLP_RX_DW2);
	
    reg_offset = IM_CEM_TLP_RX_DW3 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &dw3);
    prverbose("Read back 0x%08x from @%#x\n", dw3, IM_CEM_TLP_RX_DW3);

    reg_offset = IM_CEM_TLP_RX_DW4 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &val);
    prverbose("Read back 0x%08x from @%#x\n", val, IM_CEM_TLP_RX_DW4);

    reg_offset = IM_CEM_TLP_RX_DW5 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &val);
    prverbose("Read back 0x%08x from @%#x\n", val, IM_CEM_TLP_RX_DW5);

    /* Check the completion status here and length value */
}

static size_t FileSize(char *file)
    {
    struct stat status;
    stat(file, &status);
    return (size_t)status.st_size;
    }

static const char *Error(void)
    {
    return strerror(errno);
    }

static void *ArriveFpgaMemoryMap(char *imageFilePath, int *fileDescriptor, size_t *fileSize)
    {
    char *address;

    *fileDescriptor = open(imageFilePath, O_RDWR);
    if (*fileDescriptor < 0)
        {
        err('f',0, "File open fail: %s\r\n", Error());
        return NULL;
        }

    *fileSize = FileSize(imageFilePath);
    address = (char *)mmap(NULL, *fileSize, PROT_READ | PROT_WRITE, MAP_SHARED, *fileDescriptor, (off_t)0);
    if (address == ((void *)-1))
        {
        close(*fileDescriptor);
        err('f',0, "Memory mapping fail, error = %s\r\n", Error());
        return NULL;
        }

    return address;
    }

/**********************************************************************
 *
 * Function:diag_prg_data_fpga 
 *
 * Description: This function program data FPGA 
 *
 **********************************************************************
 */
 int diag_prg_cem_fpga(int show_menu)
{
    uint jx,ix,timeout =0;
    uint copy_size = 0;
    uchar buffer[IM_CEM_FPGA_PAGE_SIZE];
    uchar *img_ptr;
    uint data;
    uint  *buf_ptr;
    unsigned long data_fpga_size;
    int ret_val = PASSED,file_found = FALSE;

    FILE *fp;
    unsigned char *cem_fpga_fw_buff;
    char img_name[80];
    unsigned long cem_fpga_fw_buff_size, result;

    int slot_no;
    slot_no = im_get_current_slot_no();
	
    if (show_menu == FOX_TREE_MAGIC_WORD) {
        return (PASSED);
    }

	//Chk if CEM is programmed donot load image again
	if(im_type[slot_no] == IM_DS1 || im_type[slot_no] == IM_DS3 
			|| im_type[slot_no] == IM_OCn){
	  im_fpga_reg_read(slot_no, IM_DATA_FPGA_CTL_STATUS, &data);
      if((data & IM_DATA_PROG_DONE))
       {
	    ret_val = PASSED;
		printf("CEM FPGA Already Programed");
		if(show_menu != 1)
		    return ret_val;
       }
	}
	/* Open the command for reading. */
	if(im_type[slot_no] == IM_DS1){
		fp = popen("/bin/ls *ds1cem_FPGA*.bin", "r");
	}else if(im_type[slot_no] == IM_DS3){
		fp = popen("/bin/ls *ds3cem_FPGA*.bin", "r");
	}else if(im_type[slot_no] == IM_OCn){
		fp = popen("/bin/ls *_OCNCEM_*.bin", "r");
	}else {
		printf("IM Type %d not proper for IM%d ",im_type[slot_no],slot_no);
		ret_val = FAILED;
		return ret_val;
	}
	if ((fp != NULL) ) {
		if(fgets(img_name, sizeof(img_name)-1, fp) != NULL){	
			printf(" Found %s\n", img_name);
			file_found = TRUE;
			img_name[strcspn(img_name, "\n")] = 0; //remove \n at end
		}	
        pclose(fp);
    }
  
	if(file_found == FALSE) {
	 switch (im_type[slot_no])
	  {
	   	case IM_OCn :
		 printf("Enter OCn CEM Image name: \t");
	    break ;
		case IM_DS1:
	   	 printf("Enter Ds1 CEM Image name: \t");
	    break ;
		case IM_DS3:
	   	 printf("Enter Ds3 CEM Image name: \t");
	    break ;
		default:
			printf("*Err* Im Type Slot %d \n Enter CEM Image name: \t",slot_no);
	     break ;
	  }
   	  gets(img_name);
	}

    printf("\nFPGA File: %s\n",img_name);

    fp = fopen (img_name, "r");
    if (fp == NULL)
    {
        printf("FPGA bin file not found in /media/ram\n");
        printf("Please copy FPGA in /media/ram via tftp with below command\n");
        printf("busybox tftp -b 64000 -g -r fpga_file tftp_server_ip \n");
        return FAILED;
    }

    /* Find out file length */
    fseek(fp, 0, SEEK_END);
    cem_fpga_fw_buff_size = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    
    /* Allocate memory */
    cem_fpga_fw_buff = (unsigned char *) malloc ((sizeof(char)*cem_fpga_fw_buff_size));
    if (cem_fpga_fw_buff == NULL) {
        printf ("Memory allocation failed\n");
        fclose (fp);
        return FAILED;
    }
    printf("Reading %s\n", img_name);
    result = fread (cem_fpga_fw_buff, 1, cem_fpga_fw_buff_size, fp);
    if (result != cem_fpga_fw_buff_size)
    {
        printf ("Reading error\n");
        fclose (fp);
        free (cem_fpga_fw_buff);
        return FAILED;
    }
    
    printf("Read %ld Bytes\n", result);
	printf("File size %ld MB\n",result/(1024*1024));	
    fclose (fp);

/* Step-1: set the 0th bit and poll till it become zero */
   
    im_fpga_reg_read(slot_no, IM_DATA_FPGA_CTL_STATUS, &data);
    data = data | 0x1;
    im_fpga_reg_write(slot_no, IM_DATA_FPGA_CTL_STATUS, data);
    debug_print("Before the DATA_PROG\n");
    while(1)
    {
       im_fpga_reg_read(slot_no, IM_DATA_FPGA_CTL_STATUS, &data);
       if(!(data & IM_DATA_PROG))
       {
	    break;
       }
    }
   msleep(10);

/* Step-2: Check init bit to become 1 */   
    debug_print("Before the DATA_INIT_STATA\n");
    while(1)
    {
       im_fpga_reg_read(slot_no, IM_DATA_FPGA_CTL_STATUS, &data);
       if((data & IM_DATA_INIT_STATUS))
       {
	    break;
       }
    }
    /* Clear error bit by setting bit */
    data = 0x8;
    im_fpga_reg_write(slot_no, IM_DATA_FPGA_CTL_STATUS, data);

/* Step-3: Program the data fpga array */   
    debug_print("Before the Program\n");
    img_ptr = cem_fpga_fw_buff;
    data_fpga_size  = cem_fpga_fw_buff_size +IM_CEM_FPGA_PAGE_SIZE;

    for (ix = 0; ix <  data_fpga_size;ix += IM_CEM_FPGA_PAGE_SIZE){
        if ((ix % 0x10000) == 0) {
          printf("Writing addr %#x,\r", ix);
        }

        memset((char *)buffer, 0xFF, IM_CEM_FPGA_PAGE_SIZE);

        if ((copy_size + IM_CEM_FPGA_PAGE_SIZE) > data_fpga_size) {
            /* The last remaining block doesn't reach 1 page size, so just copy
             * whatever left
             */
            memcpy((char *)buffer, (char *)img_ptr, data_fpga_size - copy_size);
        } else {
            /* Copy one page size of array to the buffer */
            memcpy((char *)buffer, (char *)img_ptr, IM_CEM_FPGA_PAGE_SIZE);
            img_ptr += IM_CEM_FPGA_PAGE_SIZE;
            copy_size += IM_CEM_FPGA_PAGE_SIZE;
        }

    prverbose("Writing SPI Flash into page @%#x\n", ix);
    buf_ptr = (uint *)buffer;
    //  Writing the data into the write register 
    for (jx = 0; jx < IM_CEM_FPGA_PAGE_SIZE; jx += 4, buf_ptr++) {
        //value = ((*buf_ptr & 0xff) << 24) |
        //         (((*buf_ptr >> 8) & 0xff)<< 16) |
        //         (((*buf_ptr >> 16) & 0xff) << 8) |
        //         ((*buf_ptr >> 24) & 0xff);
        im_fpga_reg_write(slot_no,  IM_DATA_FPGA_REG , *buf_ptr);
    }
    /* set the 1st bit and poll till it become zero */
    data = 0x2;
    im_fpga_reg_write(slot_no, IM_DATA_FPGA_CTL_STATUS, data);

	debug_print("Before the IM_DATA_WRITE_IN_PROG\n");
	timeout =0;

    while(1)
    {
       im_fpga_reg_read(slot_no, IM_DATA_FPGA_CTL_STATUS, &data);
       if(!(data & IM_DATA_WRITE_IN_PROG))
       {
	       break;
       }
	   if(timeout < 10000) {
	       timeout++;
       }else{
	       if(timeout >= 9000) {
	           printf("\nExiting due to time out IM_DATA_WRITE_IN_PROG \n");
		       ret_val = FAILED;
		       return ret_val;
	       }
       }
   }
    if(data & IM_DATA_INIT_ERR)
    {
      printf("Data init error INIT_B while flasing\n");
      ret_val = FAILED;
      free (cem_fpga_fw_buff);	  
      return ret_val;
    }
 }
	debug_print("\n Data transfered done\n");

  sleep(1);
    
/* Step-4: poll 2nd bit till it become 1 */
    debug_print("Before the IM_DATA_PROG_DONE\n");
	timeout =0 ;

    while(1)
    {
       im_fpga_reg_read(slot_no, IM_DATA_FPGA_CTL_STATUS, &data);
       if((data & IM_DATA_PROG_DONE))
       {
	    ret_val = PASSED;
		break;
       }
	   if(timeout < 10000) {
	       timeout++;
            msleep(1);
       } else{
		    if(timeout >= 9000) {
			    printf("\nExiting due to time out IM_DATA_FPGA_CTL_STATUS %x\n",data);
            }
		    ret_val = FAILED;
		    break;
       }
   }
    debug_print("After the IM_DATA_PROG_DONE\n");
    free (cem_fpga_fw_buff);
	/* we need delay here ... why ? */
    /* sync not working */
    printf("After free !!!!!!!!!!!\n");
    
#if 0
    printf("After free !!!!!!!!!!!\n");
    printf("After free !!!!!!!!!!!\n");
	    printf("After free !!!!!!!!!!!\n");
		    printf("After free !!!!!!!!!!!\n");
#endif
    if(ret_val == FAILED ) {
        printf("%sCEM FPGA download Failed");
    }
	else {
        printf("%sCEM FPGA download done");
    }
	return ret_val;
}

#if 0
static int CemFpgaMemoryMap(char *imageFilePath, int *fileDescriptor, size_t *fileSize, unsigned long long  offset)
    {
    char *address;

    if(!(*fileDescriptor)) {
        *fileDescriptor = open(imageFilePath, O_RDWR | O_SYNC);
        if (*fileDescriptor < 0)
            {
            err('f', 0, "File open fail: \r\n");
            return FAILED;
           }
    }
	
    if(adrspc_virt_im_bar_2_3) {
        prverbose("%s %d\r\n",__FUNCTION__,__LINE__);

        if (munmap(adrspc_virt_im_bar_2_3, *fileSize) == -1) 
        {
            err('f', 0, "Error un-mmapping the file");
            return FAILED;
        }
        prverbose("%s %d\r\n",__FUNCTION__,__LINE__);
	} 				
    prverbose("%s %d\r\n",__FUNCTION__,__LINE__);
	__asm__("sync");
	printf("offset : 0x%llx \r\n",offset);
    adrspc_virt_im_bar_2_3  = (char *)mmap64(NULL, *fileSize, PROT_READ | PROT_WRITE, MAP_SHARED, *fileDescriptor, offset);
    if (NULL == adrspc_virt_im_bar_2_3) {
        err('f', 0, "mmap failed:");
        return FAILED;
    }
    print("adrspc_virt_im_bar_2_3 : %x\n",adrspc_virt_im_bar_2_3);
while(1);
    return (PASSED);
}

int cem_fpga_pcie_mmap() {

    int slot_no;
    slot_no = im_get_current_slot_no();
	size_t fileSize;

    prverbose("%s %d\n",__FUNCTION__,__LINE__);

    fileSize = 0xA000000;

    switch(slot_no) {
        case 0 : 
                if(FAILED == CemFpgaMemoryMap("/dev/mem", &fd_im0, &fileSize, 0x420000000)) {
	                return FAILED;
            }
        break;   
        case 1 : 
                if(FAILED == CemFpgaMemoryMap("/dev/mem", &fd_im1, &fileSize, 0x4a0000000)) {
	                return FAILED;
                }
        break;   
        case 2 : 
                if(FAILED == CemFpgaMemoryMap("/dev/mem", &fd_im2, &fileSize, 0x520000000)) {
	                return FAILED;
                }
        break;   
        case 3 : 
                if(FAILED == CemFpgaMemoryMap("/dev/mem", &fd_im3, &fileSize, 0x5a0000000)) {
	                return FAILED;
                }
        break;   
        case 4 : 
                if(FAILED == CemFpgaMemoryMap("/dev/mem", &fd_im4, &fileSize, 0x620000000)) {
	                return FAILED;
                }
        break;   
        case 5 : 
                if(FAILED == CemFpgaMemoryMap("/dev/mem", &fd_im5, &fileSize, 0x6a0000000)) {
	                return FAILED;
                }
        break;   

		
        default : 
            break;
   }

    return PASSED;	
}	

#endif

#if 1
static int CemFpgaMemoryMap(char *imageFilePath, int *fileDescriptor, size_t *fileSize)
    {
    char *address;
    int slot_no;
    slot_no = im_get_current_slot_no();

    if(!(*fileDescriptor)) {
        *fileDescriptor = open(imageFilePath, O_RDWR | O_SYNC);
        if (*fileDescriptor < 0)
            {
            err('f', 0, "File open fail: \r\n");
            return FAILED;
           }
    }
	
    if(adrspc_virt_im_bar_2_3) {
        if (munmap(adrspc_virt_im_bar_2_3, *fileSize) == -1) 
        {
            err('f', 0, "Error un-mmapping the file");
            return FAILED;
        }
	} 				
	__asm__("sync");
    switch(slot_no) {
        case 0 :
        	adrspc_virt_im_bar_2_3  = (char *)mmap64(NULL, *fileSize, PROT_READ | PROT_WRITE, MAP_SHARED, *fileDescriptor, 0x420000000);
        break;
        case 1 :
        	adrspc_virt_im_bar_2_3  = (char *)mmap64(NULL, *fileSize, PROT_READ | PROT_WRITE, MAP_SHARED, *fileDescriptor, 0x4a0000000);
        break;
        case 2 :
        	adrspc_virt_im_bar_2_3  = (char *)mmap64(NULL, *fileSize, PROT_READ | PROT_WRITE, MAP_SHARED, *fileDescriptor, 0x520000000);
        break;
        case 3 :
        	adrspc_virt_im_bar_2_3  = (char *)mmap64(NULL, *fileSize, PROT_READ | PROT_WRITE, MAP_SHARED, *fileDescriptor, 0x5a0000000);
        break;
        case 4 :
        	adrspc_virt_im_bar_2_3  = (char *)mmap64(NULL, *fileSize, PROT_READ | PROT_WRITE, MAP_SHARED, *fileDescriptor, 0x620000000);
        break;
        case 5 :
        	adrspc_virt_im_bar_2_3  = (char *)mmap64(NULL, *fileSize, PROT_READ | PROT_WRITE, MAP_SHARED, *fileDescriptor, 0x6a0000000);
        break;
        case 6 :
        	adrspc_virt_im_bar_2_3  = (char *)mmap64(NULL, *fileSize, PROT_READ | PROT_WRITE, MAP_SHARED, *fileDescriptor, 0x720000000);
        break;
        case 7 :
        	adrspc_virt_im_bar_2_3  = (char *)mmap64(NULL, *fileSize, PROT_READ | PROT_WRITE, MAP_SHARED, *fileDescriptor, 0x7a0000000);
        break;
        case 8 :
        	adrspc_virt_im_bar_2_3  = (char *)mmap64(NULL, *fileSize, PROT_READ | PROT_WRITE, MAP_SHARED, *fileDescriptor, 0x820000000);
        break;
        case 9 :
        	adrspc_virt_im_bar_2_3  = (char *)mmap64(NULL, *fileSize, PROT_READ | PROT_WRITE, MAP_SHARED, *fileDescriptor, 0x8a0000000);
        break;
        case 10 :
        	adrspc_virt_im_bar_2_3  = (char *)mmap64(NULL, *fileSize, PROT_READ | PROT_WRITE, MAP_SHARED, *fileDescriptor, 0x920000000);
        break;
        case 11 :
        	adrspc_virt_im_bar_2_3  = (char *)mmap64(NULL, *fileSize, PROT_READ | PROT_WRITE, MAP_SHARED, *fileDescriptor, 0x9a0000000);
        break;
        case 12 :
        	adrspc_virt_im_bar_2_3  = (char *)mmap64(NULL, *fileSize, PROT_READ | PROT_WRITE, MAP_SHARED, *fileDescriptor, 0xa20000000);
        break;
        case 13 :
        	adrspc_virt_im_bar_2_3  = (char *)mmap64(NULL, *fileSize, PROT_READ | PROT_WRITE, MAP_SHARED, *fileDescriptor, 0xaa0000000);
        break;
        case 14 :
        	adrspc_virt_im_bar_2_3  = (char *)mmap64(NULL, *fileSize, PROT_READ | PROT_WRITE, MAP_SHARED, *fileDescriptor, 0xb20000000);
        break;
        case 15 :
        	adrspc_virt_im_bar_2_3  = (char *)mmap64(NULL, *fileSize, PROT_READ | PROT_WRITE, MAP_SHARED, *fileDescriptor, 0xba0000000);
        break;
        default :
        	err('f',"unsupported slot");
            break;
    }
    if (NULL == adrspc_virt_im_bar_2_3) {
        err('f', 0, "mmap failed:");
        return FAILED;
    }
    prverbose("adrspc_virt_im_bar_2_3 : %x\n",adrspc_virt_im_bar_2_3);

    return (PASSED);
}

int cem_fpga_pcie_mmap() {

    int slot_no;
    slot_no = im_get_current_slot_no();
	size_t fileSize;

    prverbose("%s %d\n",__FUNCTION__,__LINE__);

    fileSize = 0xA000000;

    switch(slot_no) {
        case 0 : 
                if(FAILED == CemFpgaMemoryMap("/dev/mem", &fd_im0, &fileSize)) {
	                return FAILED;
            }
        break;   
        case 1 : 
                if(FAILED == CemFpgaMemoryMap("/dev/mem", &fd_im1, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 2 : 
                if(FAILED == CemFpgaMemoryMap("/dev/mem", &fd_im2, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 3 : 
                if(FAILED == CemFpgaMemoryMap("/dev/mem", &fd_im3, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 4 : 
                if(FAILED == CemFpgaMemoryMap("/dev/mem", &fd_im4, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 5 : 
                if(FAILED == CemFpgaMemoryMap("/dev/mem", &fd_im5, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 6 : 
                if(FAILED == CemFpgaMemoryMap("/dev/mem", &cem_fd_im6, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 7 : 
                if(FAILED == CemFpgaMemoryMap("/dev/mem", &cem_fd_im7, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 8 : 
                if(FAILED == CemFpgaMemoryMap("/dev/mem", &cem_fd_im8, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 9 : 
                if(FAILED == CemFpgaMemoryMap("/dev/mem", &cem_fd_im9, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 10 : 
                if(FAILED == CemFpgaMemoryMap("/dev/mem", &cem_fd_im10, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 11 : 
                if(FAILED == CemFpgaMemoryMap("/dev/mem", &cem_fd_im11, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 12 : 
                if(FAILED == CemFpgaMemoryMap("/dev/mem", &cem_fd_im12, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 13 : 
                if(FAILED == CemFpgaMemoryMap("/dev/mem", &cem_fd_im13, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 14 : 
                if(FAILED == CemFpgaMemoryMap("/dev/mem", &cem_fd_im14, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 15 : 
                if(FAILED == CemFpgaMemoryMap("/dev/mem", &cem_fd_im15, &fileSize)) {
	                return FAILED;
                }
        break;   
		
        default : 
            break;
   }

    return PASSED;	
}	

#endif

#if 0

int cem_fpga_pcie_mmap() {

    int slot_no;
    slot_no = im_get_current_slot_no();

    switch(slot_no) {
        case 0 :
            if (!fd_im0){
                    fd_im0 = open("/dev/mem", O_RDWR | O_SYNC);
                    if (-1 == fd_im0) {
                    err('f', 0, "Mem open failed:");
                    return -1;
                    }

                adrspc_virt_im_0_bar_2_3 = (uint8_t *)mmap64(NULL, 0xa000000 , PROT_READ | PROT_WRITE, MAP_SHARED| O_SYNC, fd_im0, 0x420000000);

                if (NULL == adrspc_virt_im_0_bar_2_3) {
                            err('f', 0, "mmap failed:");
                            return -1;
                    }
            }
        break;
        case 1 :
            if (!fd_im1){
                    fd_im1 = open("/dev/mem", O_RDWR | O_SYNC);
                    if (-1 == fd_im1) {
                    err('f', 0, "Mem open failed:");
                    return -1;
                    }

                adrspc_virt_im_1_bar_2_3 = (uint8_t *)mmap64(NULL, 0xa000000 , PROT_READ | PROT_WRITE, MAP_SHARED| O_SYNC, fd_im1, 0x4A0000000);

                if (NULL == adrspc_virt_im_1_bar_2_3) {
                            err('f', 0, "mmap failed:");
                            return -1;
                    }
            }
        break;
        case 2 :
            if (!fd_im2){
                    fd_im2 = open("/dev/mem", O_RDWR | O_SYNC);
                    if (-1 == fd_im2) {
                    err('f', 0, "Mem open failed:");
                    return -1;
                    }

                adrspc_virt_im_2_bar_2_3 = (uint8_t *)mmap64(NULL, 0xa000000 , PROT_READ | PROT_WRITE, MAP_SHARED| O_SYNC, fd_im2, 0x520000000);

                if (NULL == adrspc_virt_im_2_bar_2_3) {
                            err('f', 0, "mmap failed:");
                            return -1;
                    }
            }
        break;
        case 3 :
            if (!fd_im3){
                    fd_im3 = open("/dev/mem", O_RDWR | O_SYNC);
                    if (-1 == fd_im3) {
                    err('f', 0, "Mem open failed:");
                    return -1;
                    }

                adrspc_virt_im_3_bar_2_3 = (uint8_t *)mmap64(NULL, 0xa000000 , PROT_READ | PROT_WRITE, MAP_SHARED| O_SYNC, fd_im3, 0x5A0000000);

                if (NULL == adrspc_virt_im_3_bar_2_3) {
                            err('f', 0, "mmap failed:");
                            return -1;
                    }
            }
        break;
        case 4 :
            if (!fd_im4){
                    fd_im4 = open("/dev/mem", O_RDWR | O_SYNC);
                    if (-1 == fd_im4) {
                    err('f', 0, "Mem open failed:");
                    return -1;
                    }

                adrspc_virt_im_4_bar_2_3 = (uint8_t *)mmap64(NULL, 0xa000000 , PROT_READ | PROT_WRITE, MAP_SHARED| O_SYNC, fd_im4, 0x620000000);

                if (NULL == adrspc_virt_im_4_bar_2_3) {
                            err('f', 0, "mmap failed:");
                            return -1;
                    }
            }
        break;
        case 5 :
            if (!fd_im5){
                    fd_im5 = open("/dev/mem", O_RDWR | O_SYNC);
                    if (-1 == fd_im5) {
                    err('f', 0, "Mem open failed:");
                    return -1;
                    }

                adrspc_virt_im_5_bar_2_3 = (uint8_t *)mmap64(NULL, 0xa000000 , PROT_READ | PROT_WRITE, MAP_SHARED| O_SYNC, fd_im5, 0x6A0000000);

                if (NULL == adrspc_virt_im_5_bar_2_3) {
                            err('f', 0, "mmap failed:");
                            return -1;
                    }
            }
        break;

        default :
            break;
   }

    return PASSED;
}
#endif

/**********************************************************************
 *
 * Function: hyphy_pci_config_read
 *
 * Description: Performs PCIe configuration read on WinPath3
 *
 **********************************************************************
 */
uint hyphy_pci_config_read (int slot_no, uint32 func, uint32 offset, int rc_id)
{

    return (hyphy_pci_config_read_by_tlp(slot_no, func, offset, rc_id));

}

/**********************************************************************
 *
 * Function: hyphy_pci_config_write
 *
 * Description: Performs PCIe configuration write on WinPath3
 *
 **********************************************************************
 */
void hyphy_pci_config_write (int slot_no, uint32 func, uint32 offset, uint32 data, int rc_id)
{

    return(hyphy_pci_config_write_by_tlp(slot_no, func, offset, data, rc_id));
}

/**********************************************************************
 *
 * Function: hyphy_im_wait_tlp_done
 *
 * Description: This functions waits till the TLP transaction is done
 *              by FPGA
 *
 **********************************************************************
 */
static int hyphy_im_wait_tlp_done (int slot_no, int rc_id)
{
    uint ix, data, count = IM_WINTEGRA_TLP_TIMEOUT;
    uint reg_offset;	

    reg_offset =  IM_HYPHY_PCIE_CONTROL + (rc_id * 0x100);
    for (ix = 0; ix < count; ix++) {
        if (im_fpga_reg_read(slot_no, reg_offset, &data) == FAILED) {
            return (FAILED);
        }
        if ((data & IM_WIN_PCIE_CTRL_GO) == 0) {  /* complete */
            return (PASSED);
        }
        msleep(1);
    }

    return (PASSED);
}

/**********************************************************************
 *
 * Function: hyphy_pci_config_read_by_tlp
 *
 * Description: Performs PCIe configuration read on WinPath3 by using
 *              TLP transaction
 *
 **********************************************************************
 */
static uint hyphy_pci_config_read_by_tlp (int slot_no, uint32 func, uint32 offset, int rc_id)
{
    uint val;
    uint dw1, dw2, dw3, dw4, dw5;
    uint reg_offset;	

    /* Load them with default value */
    dw1 = IM_CONFIG_RD_DW1_VAL;
    dw2 = IM_CONFIG_DW2_VAL;
    dw3 = 0x0;

    /* Set the requester id for CPU */
    dw2 |= IM_WINTEGRA_REQUESTER_ID;

    /* Set the bus number, device number, func number */
    dw3 |= (IM_WINTEGRA_BUS_NO << IM_WINTEGRA_BUS_SHIFT) |
           (IM_WINTEGRA_DEV_NO << IM_WINTEGRA_DEV_SHIFT) |
           (func << IM_WINTEGRA_FUNC_SHIFT) | (offset);

    /* Load them up */
    reg_offset = IM_HYPHY_TLP_TX_DW1 + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, dw1);
    prverbose("Writing 0x%08x @%#x\n", dw1, IM_HYPHY_TLP_TX_DW1);

    reg_offset = IM_HYPHY_TLP_TX_DW2 + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, dw2);
    prverbose("Writing 0x%08x @%#x\n", dw2, IM_HYPHY_TLP_TX_DW2);

    reg_offset = IM_HYPHY_TLP_TX_DW3 + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, dw3);
    prverbose("Writing 0x%08x @%#x\n", dw3, IM_HYPHY_TLP_TX_DW3);

    /* Setup NUM_DWORDS to 3 and give it a go */
    val = IM_WIN_PCIE_CTRL_GO | (0x3 << IM_WIN_PCIE_CTRL_NUM_DWORDS_SHIFT);
    reg_offset = IM_HYPHY_PCIE_CONTROL + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, val);
    prverbose("Writing 0x%08x @%#x\n", val, IM_HYPHY_PCIE_CONTROL);

    if (hyphy_im_wait_tlp_done(slot_no, rc_id) == FAILED) {
        printf("%s: Wait TLP done failed\n", __FUNCTION__);
        return (FAILED);
    }

    reg_offset = IM_HYPHY_TLP_RX_DW1 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &dw1);
    prverbose("Read back 0x%08x from @%#x\n", dw1, IM_HYPHY_TLP_RX_DW1);

    reg_offset = IM_HYPHY_TLP_RX_DW2 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &dw2);
    prverbose("Read back 0x%08x from @%#x\n", dw2, IM_HYPHY_TLP_RX_DW2);

    reg_offset = IM_HYPHY_TLP_RX_DW3 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &dw3);
    prverbose("Read back 0x%08x from @%#x\n", dw3, IM_HYPHY_TLP_RX_DW3);

    reg_offset = IM_HYPHY_TLP_RX_DW4 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &dw4);
    prverbose("Read back 0x%08x from @%#x\n", dw4, IM_HYPHY_TLP_RX_DW4);

    reg_offset = IM_HYPHY_TLP_RX_DW5 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &dw5);
    prverbose("Read back 0x%08x from @%#x\n", dw5, IM_HYPHY_TLP_RX_DW5);

    /* Check the completion status here and length value */

    /* Need to swap the configuration value as FPGA flips all the incoming
     * data, including memory and configuration space. (FPGA Rev C)
     */
    dw4 = dswap4(dw4);

    return (dw4);
}


/**********************************************************************
 *
 * Function: hyphy_pci_config_write_by_tlp
 *
 * Description: Performs PCIe configuration write on WinPath3 by using
 *              TLP transaction
 *
 **********************************************************************
 */
static void hyphy_pci_config_write_by_tlp (int slot_no, uint32 func,
                                         uint32 offset, uint32 data, int rc_id)
{
    uint val;
    uint dw1, dw2, dw3, dw4;
    uint reg_offset;

    /* Load them with default value */
    dw1 = IM_CONFIG_WR_DW1_VAL;
    dw2 = IM_CONFIG_DW2_VAL;
    dw3 = 0x0;

    /* Set the requester id for CPU */
    dw2 |= IM_WINTEGRA_REQUESTER_ID;

    /* Set the bus number, device number, func number */
    dw3 |= (IM_WINTEGRA_BUS_NO << IM_WINTEGRA_BUS_SHIFT) |
           (IM_WINTEGRA_DEV_NO << IM_WINTEGRA_DEV_SHIFT) |
           (func << IM_WINTEGRA_FUNC_SHIFT) | (offset);

    /* Load the data into DW4 */
    dw4 = data;

    /* Load them up */
    reg_offset = IM_HYPHY_TLP_TX_DW1 + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, dw1);
    prverbose("Writing 0x%08x @%#x\n", dw1, IM_HYPHY_TLP_TX_DW1);

    reg_offset = IM_HYPHY_TLP_TX_DW2 + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, dw2);
    prverbose("Writing 0x%08x @%#x\n", dw2, IM_HYPHY_TLP_TX_DW2);

    reg_offset = IM_HYPHY_TLP_TX_DW3 + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, dw3);
    prverbose("Writing 0x%08x @%#x\n", dw3, IM_HYPHY_TLP_TX_DW3);

    reg_offset = IM_HYPHY_TLP_TX_DW4 + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, dw4);
    prverbose("Writing 0x%08x @%#x\n", dw4, IM_HYPHY_TLP_TX_DW4);

    /* Setup NUM_DWORDS to 4 and give it a go */
    val = IM_WIN_PCIE_CTRL_GO | (0x4 << IM_WIN_PCIE_CTRL_NUM_DWORDS_SHIFT);
    reg_offset = IM_HYPHY_PCIE_CONTROL + (rc_id * 0x100);
    im_fpga_reg_write(slot_no, reg_offset, val);
    prverbose("Writing 0x%08x @%#x\n", val, IM_HYPHY_PCIE_CONTROL);

    if (cem_im_wait_tlp_done(slot_no, rc_id) == FAILED) {
        printf("%s: Wait TLP done failed\n", __FUNCTION__);
        return;
    }

    reg_offset = IM_HYPHY_TLP_RX_DW1 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &dw1);
    prverbose("Read back 0x%08x from @%#x\n", dw1, IM_HYPHY_TLP_RX_DW1);

    reg_offset = IM_HYPHY_TLP_RX_DW2 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &dw2);
    prverbose("Read back 0x%08x from @%#x\n", dw2, IM_HYPHY_TLP_RX_DW2);
	
    reg_offset = IM_HYPHY_TLP_RX_DW3 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &dw3);
    prverbose("Read back 0x%08x from @%#x\n", dw3, IM_HYPHY_TLP_RX_DW3);

    reg_offset = IM_HYPHY_TLP_RX_DW4 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &val);
    prverbose("Read back 0x%08x from @%#x\n", val, IM_HYPHY_TLP_RX_DW4);

    reg_offset = IM_HYPHY_TLP_RX_DW5 + (rc_id * 0x100);
    im_fpga_reg_read(slot_no, reg_offset, &val);
    prverbose("Read back 0x%08x from @%#x\n", val, IM_HYPHY_TLP_RX_DW5);

    /* Check the completion status here and length value */
}

#if 0
static int HyPhyMemoryMap(char *imageFilePath, int *fileDescriptor, size_t *fileSize)
    {
    char *address;

    if(!(*fileDescriptor)) {
    *fileDescriptor = open(imageFilePath, O_RDWR | O_SYNC);
    if (*fileDescriptor < 0)
        {
        err('f', 0, "File open fail: \r\n");
        return FAILED;
       }
    	}
    if(adrspc_virt_im_bar_4_5) {
        prverbose("%s %d\r\n",__FUNCTION__,__LINE__);
        fflush(stdout);
        if (munmap(adrspc_virt_im_bar_4_5, *fileSize) == -1) 
        {
            err('f', 0, "Error un-mmapping the file");
            return FAILED;
        }
	} 				

	__asm__("sync");
	
    adrspc_virt_im_bar_4_5  = (char *)mmap64(NULL, *fileSize, PROT_READ | PROT_WRITE, MAP_SHARED, *fileDescriptor, (off_t)0);
    if (NULL == adrspc_virt_im_bar_4_5) {
        err('f', 0, "mmap failed:");
        return FAILED;
    }
    prverbose("adrspc_virt_im_bar_4_5 : %x\n",adrspc_virt_im_bar_4_5);

    return (PASSED);
}

int cem_hyphy_pcie_mmap() {

    int slot_no;
    slot_no = im_get_current_slot_no();
	size_t fileSize;

    prverbose("%s %d\n",__FUNCTION__,__LINE__);

    fileSize = 0xA000000;

    switch(slot_no) {
        case 0 : 
                if(FAILED == HyPhyMemoryMap("/dev/mysdev4", &fd_im6, &fileSize)) {
	                return FAILED;
            }
        break;   
        case 1 : 
                if(FAILED == HyPhyMemoryMap("/dev/mysdev7", &fd_im7, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 2 : 
                if(FAILED == HyPhyMemoryMap("/dev/mysdev10", &fd_im8, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 3 : 
                if(FAILED == HyPhyMemoryMap("/dev/mysdev13", &fd_im9, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 4 : 
                if(FAILED == HyPhyMemoryMap("/dev/mysdev16", &fd_im10, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 5 : 
                if(FAILED == HyPhyMemoryMap("/dev/mysdev19", &fd_im11, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 6 : 
                if(FAILED == HyPhyMemoryMap("/dev/mysdev23", &cem_fd_im6, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 7 : 
                if(FAILED == HyPhyMemoryMap("/dev/mysdev26", &hyphy_fd_im7, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 8 : 
                if(FAILED == HyPhyMemoryMap("/dev/mysdev29", &hyphy_fd_im8, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 9 : 
                if(FAILED == HyPhyMemoryMap("/dev/mysdev32", &hyphy_fd_im9, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 10 : 
                if(FAILED == HyPhyMemoryMap("/dev/mysdev35", &hyphy_fd_im10, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 11 : 
                if(FAILED == HyPhyMemoryMap("/dev/mysdev38", &hyphy_fd_im11, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 12 : 
                if(FAILED == HyPhyMemoryMap("/dev/mysdev41", &hyphy_fd_im12, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 13 : 
                if(FAILED == HyPhyMemoryMap("/dev/mysdev44", &hyphy_fd_im13, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 14 : 
                if(FAILED == HyPhyMemoryMap("/dev/mysdev47", &hyphy_fd_im14, &fileSize)) {
	                return FAILED;
                }
        break;   
        case 15 : 
                if(FAILED == HyPhyMemoryMap("/dev/mysdev50", &hyphy_fd_im15, &fileSize)) {
	                return FAILED;
                }
        break;   
		
        default : 
            break;
   }

    return PASSED;	
}

#endif

#if 1
int cem_hyphy_pcie_mmap() {

    int slot_no;
    slot_no = im_get_current_slot_no();

    switch(slot_no) {
        case 0 :
        case 1 :
        case 2 :
        case 3 :
            if (!hyphy_fd_im3){
                    hyphy_fd_im3 = open("/dev/mem", O_RDWR | O_SYNC);
                    if (-1 == hyphy_fd_im3) {
                    err('f', 0, "Mem open failed:");
                    return -1;
                    }

                adrspc_virt_im_3_bar_4_5 = (uint8_t *)mmap64(NULL, 0xa000000 , PROT_READ | PROT_WRITE, MAP_SHARED| O_SYNC, hyphy_fd_im3, 0x5c0000000);

                if (NULL == adrspc_virt_im_3_bar_4_5) {
                            err('f', 0, "mmap failed:");
                            return -1;
                    }
            }
        break;

        case 4 :
            if (!hyphy_fd_im4){
                    hyphy_fd_im4 = open("/dev/mem", O_RDWR | O_SYNC);
                    if (-1 == hyphy_fd_im4) {
                    err('f', 0, "Mem open failed:");
                    return -1;
                    }

                adrspc_virt_im_4_bar_4_5 = (uint8_t *)mmap64(NULL, 0xa000000 , PROT_READ | PROT_WRITE, MAP_SHARED| O_SYNC, hyphy_fd_im4, 0x640000000);

                if (NULL == adrspc_virt_im_4_bar_4_5) {
                            err('f', 0, "mmap failed:");
                            return -1;
                    }
            }
        break;
        case 5 :
            if (!hyphy_fd_im5){
                    hyphy_fd_im5 = open("/dev/mem", O_RDWR | O_SYNC);
                    if (-1 == hyphy_fd_im5) {
                    err('f', 0, "Mem open failed:");
                    return -1;
                    }

                adrspc_virt_im_5_bar_4_5 = (uint8_t *)mmap64(NULL, 0xa000000 , PROT_READ | PROT_WRITE, MAP_SHARED| O_SYNC, hyphy_fd_im5, 0x6c0000000);

                if (NULL == adrspc_virt_im_5_bar_4_5) {
                            err('f', 0, "mmap failed:");
                            return -1;
                    }
            }
        break;
        case 7 :
            if (!hyphy_fd_im7){
                    hyphy_fd_im7 = open("/dev/mem", O_RDWR | O_SYNC);
                    if (-1 == hyphy_fd_im7) {
                    err('f', 0, "Mem open failed:");
                    return -1;
                    }

                adrspc_virt_im_7_bar_4_5 = (uint8_t *)mmap64(NULL, 0xa000000 , PROT_READ | PROT_WRITE, MAP_SHARED| O_SYNC, hyphy_fd_im7, 0x7c0000000);

                if (NULL == adrspc_virt_im_7_bar_4_5) {
                            err('f', 0, "mmap failed:");
                            return -1;
                    }
            }
        break;

        case 8 :
            if (!hyphy_fd_im8){
                    hyphy_fd_im8 = open("/dev/mem", O_RDWR | O_SYNC);
                    if (-1 == hyphy_fd_im8) {
                    err('f', 0, "Mem open failed:");
                    return -1;
                    }

                adrspc_virt_im_8_bar_4_5 = (uint8_t *)mmap64(NULL, 0xa000000 , PROT_READ | PROT_WRITE, MAP_SHARED| O_SYNC, hyphy_fd_im8, 0x840000000);

                if (NULL == adrspc_virt_im_8_bar_4_5) {
                            err('f', 0, "mmap failed:");
                            return -1;
                    }
            }
        break;
        case 11 :
            if (!hyphy_fd_im11){
                    hyphy_fd_im11 = open("/dev/mem", O_RDWR | O_SYNC);
                    if (-1 == hyphy_fd_im11) {
                    err('f', 0, "Mem open failed:");
                    return -1;
                    }

                adrspc_virt_im_11_bar_4_5 = (uint8_t *)mmap64(NULL, 0xa000000 , PROT_READ | PROT_WRITE, MAP_SHARED| O_SYNC, hyphy_fd_im11, 0x9c0000000);

                if (NULL == adrspc_virt_im_11_bar_4_5) {
                            err('f', 0, "mmap failed:");
                            return -1;
                    }
            }
        break;
        case 12 :
            if (!hyphy_fd_im12){
                    hyphy_fd_im12 = open("/dev/mem", O_RDWR | O_SYNC);
                    if (-1 == hyphy_fd_im12) {
                    err('f', 0, "Mem open failed:");
                    return -1;
                    }

                adrspc_virt_im_12_bar_4_5 = (uint8_t *)mmap64(NULL, 0xa000000 , PROT_READ | PROT_WRITE, MAP_SHARED| O_SYNC, hyphy_fd_im12, 0xa40000000);

                if (NULL == adrspc_virt_im_12_bar_4_5) {
                            err('f', 0, "mmap failed:");
                            return -1;
                    }
            }
        break;

        default :
            break;
   }

    return PASSED;
}
#endif


/*******************************************************************
 *
 * Function    : cem_fpga_reg_read
 * Description : CEM FPGA Register Read
 *
 *******************************************************************
 */
int cem_fpga_reg_read (int im_slot_num, uint reg_offset, uint *buf) 
{
    volatile unsigned long base_addr;
    volatile uint *reg_addr, x;

    int slot_no;
    slot_no = im_get_current_slot_no();
	assert(slot_no < 16);

    base_addr = adrspc_virt_im_bar_2_3;

    reg_addr = (volatile uint *)(base_addr + (reg_offset<<0));
	
    *buf = *reg_addr;
    sync();

    CEM_DEBUG(" In %s, Base Addr = %#x offset = %#x\n", __FUNCTION__, base_addr, reg_offset );

	debug_print("\n%s Addr: 0x%x ",__FUNCTION__,reg_addr);
    return (PASSED);
}

/*******************************************************************
 *
 * Function    : cem_hyphy_reg_write
 * Description : CEM HYPHY Register Write
 *
 *******************************************************************
 */
int cem_fpga_reg_write (int im_slot_num, uint reg_offset, uint data) 
{
    volatile ulong base_addr;
    volatile uint *reg_addr, x;

    int slot_no;
    slot_no = im_get_current_slot_no();

	assert(slot_no < 16);

    base_addr = adrspc_virt_im_bar_2_3;
    
    reg_addr = (volatile uint *)(base_addr + (reg_offset<<0));

    *reg_addr = data;
    sync();
	
    return (PASSED);
}

/*******************************************************************
 *
 * Function    : cem_fpga_reg_read
 * Description : CEM FPGA Register Read
 *
 *******************************************************************
 */
int cem_data_fpga_reg_read (int im_slot_num, uint reg_offset, uint *buf) 
{
    volatile unsigned long base_addr;
    volatile uint *reg_addr;

    int slot_no;
    slot_no = im_get_current_slot_no();

    base_addr = adrspc_virt_im_bar_2_3;

    reg_addr = (volatile uint *)(base_addr + 0x08000000 + reg_offset);
	
    *buf = *reg_addr;
    __asm__("sync");

    debug_print("\nR-> Offset = %#x Value = %#x\n",reg_offset, *buf );

    return (PASSED);
}

/*******************************************************************
 *
 * Function    : cem_hyphy_reg_write
 * Description : CEM HYPHY Register Write
 *
 *******************************************************************
 */
int cem_data_fpga_reg_write (int im_slot_num, uint reg_offset, uint data) 
{
    volatile ulong base_addr;
    volatile uint *reg_addr;

    int slot_no;
    slot_no = im_get_current_slot_no();

    base_addr = adrspc_virt_im_bar_2_3;

    reg_addr = (volatile uint *)(base_addr + 0x08000000 + reg_offset);
    *reg_addr = data;
    __asm__("sync");
    (void)*((volatile uint32 *)(base_addr + 0x08000000 + reg_offset));
    __asm__("sync");

    debug_print("\nW-> Offset = %#x Value = %#x\n",reg_offset, data );
	
    return (PASSED);
}

/*******************************************************************
 *
 * Function    : cem_hyphy_reg_read
 * Description : CEM HYPHY Register Read
 *
 *******************************************************************
 */
int cem_hyphy_reg_read (uint reg_offset, uint *buf) 
{
    volatile unsigned long base_addr;
    volatile uint *reg_addr;
printf("Implementation Removed\n");
#if 0

    base_addr = ( ulong )adrspc_virt_im_bar_4_5;
    reg_addr = (volatile uint *)(base_addr + reg_offset);
	
    *buf = *reg_addr;
    sync();
#endif
    return (PASSED);
}

/*******************************************************************
 *
 * Function    : cem_hyphy_reg_write
 * Description : CEM HYPHY Register Write
 *
 *******************************************************************
 */
int cem_hyphy_reg_write (uint reg_offset, uint data) 
{
    volatile ulong base_addr;
    volatile uint *reg_addr;

printf("Implementation Removed\n");
#if 0
    base_addr = ( ulong )adrspc_virt_im_bar_4_5;
    
    reg_addr = (volatile uint *)(base_addr + reg_offset);
    *reg_addr = data;
#endif

    return (PASSED);
}


static int cem_reg_peek (int show_menu)
{
    uint reg_addr, reg_val; 

    if (show_menu == FOX_TREE_MAGIC_WORD) {
        return (PASSED);
    }
 
    printf("Peek FPGA Register.\n");
    reg_addr = gethex_answer("Enter register address (0x0~0x0xFFFFFFFF): ", 0, 0, 
                             0xFFFFFFFF);

    /* display original value */
    if (cem_fpga_reg_read(im_slot, (reg_addr<<2), &reg_val) == FAILED) {
        printf("FPGA Register Read Fail.\n");
        return (FAILED);
    }

    printf("Register value: reg %#.8x, data %#.8x\n", reg_addr, reg_val);
    return (PASSED);
}


static int cem_reg_poke (int show_menu)
{
    uint reg_addr, reg_val; 

    if (show_menu == FOX_TREE_MAGIC_WORD) {
        return (PASSED);
    }
  
    printf("Peek FPGA Register.\n");
    reg_addr = gethex_answer("Enter register address (0x0~0xFFFFFFFF): ", 0, 0, 
                             0xFFFFFFFF);

    reg_val = gethex_answer("Enter Value             (0x0~0xFFFFFFFF): ", 0, 0, 
                             0xFFFFFFFF);

    if (cem_fpga_reg_write(im_slot, (reg_addr<<2), reg_val) == FAILED) {
        printf("FPGA Register Read Fail.\n");
        return (FAILED);
    }
    return (PASSED);
}

static int cem_reg_data_peek (int show_menu)
{
    uint reg_addr, reg_val;

    if (show_menu == FOX_TREE_MAGIC_WORD) {
        return (PASSED);
    }

    printf("\nPeek FPGA Register.\n");
    reg_addr = gethex_answer("Enter register address (0x0~0x0xFFFFFFFF): ", 0, 0,
                             0xFFFFFFFF);

    /* display original value */
    if (cem_data_fpga_reg_read(im_slot, (reg_addr), &reg_val) == FAILED) {
        printf("FPGA Register Read Fail.\n");
        return (FAILED);
    }

    printf("Register value: reg %#.8x, data %#.8x\n", reg_addr, reg_val);
    return (PASSED);
}


static int cem_reg_data_poke (int show_menu)
{
    uint reg_addr, reg_val;

    if (show_menu == FOX_TREE_MAGIC_WORD) {
        return (PASSED);
    }

    printf("\nPeek FPGA Register.\n");
    reg_addr = gethex_answer("Enter register address (0x0~0xFFFFFFFF): ", 0, 0,
                             0xFFFFFFFF);

    reg_val = gethex_answer("Enter Value             (0x0~0xFFFFFFFF): ", 0, 0,
                             0xFFFFFFFF);

    if (cem_data_fpga_reg_write(im_slot, (reg_addr), reg_val) == FAILED) {
        printf("FPGA Register Read Fail.\n");
        return (FAILED);
    }
    return (PASSED);
}



static int cem_cfg_peek (int show_menu)
{
    uint reg_addr, reg_val; 

    if (show_menu == FOX_TREE_MAGIC_WORD) {
        return (PASSED);
    }
  
    printf("Peek Cfg Register.\n");
    reg_addr = gethex_answer("Enter register offset (0x0~0x1FFF): ", 0, 0, 
                             0xFFFFFFF);

    reg_val = cem_pci_config_read(im_slot, 0, reg_addr, 0);
    printf("Register value: reg %#.8x, data %#.8x\n", reg_addr, reg_val);
    return (PASSED);
}


static int cem_cfg_poke (int show_menu)
{
    uint reg_addr, reg_val; 

    if (show_menu == FOX_TREE_MAGIC_WORD) {
        return (PASSED);
    }
 
    printf("Poke CEM Cfg Register.\n");
    reg_addr = gethex_answer("Enter register offset (0x0~0x1FFF): ", 0, 0, 
                             0xFFFFFFF);

    reg_val = gethex_answer("Enter register value (0x0~0x1FFF): ", 0, 0, 
                             0xFFFFFFF);

    cem_pci_config_write(im_slot, 0, reg_addr, reg_val, 0);

    return (PASSED);
}

 int ocn_arr_cli_test (int show_menu)
{
    if (show_menu == FOX_TREE_MAGIC_WORD) {
        return (PASSED);
    }

    arrive_fpag_invoke_cli();
    return (PASSED);
}



/**********************************************************************
 *
 * Function: diag_menu_cem_fpga_utility
 *
 * Description: Build CEM_FPGA ASIC Utility menu.
 *
 **********************************************************************
 */
static int diag_menu_cem_fpga_utility (int show_menu)
{
    int ret_val = PASSED;

    if (show_menu == FOX_TREE_MAGIC_WORD) {
        ret_val = diag_build_menu_tree("CEM_FPGA Utility", cem_fpga_utility_menu,
                    CEM_FPGA_UTILITY_MENU_SIZE);
        return (ret_val);
    } 

    return (ret_val);
}

/**********************************************************************
 *
 * Function: diag_menu_cem_fpga_asic_0
 *
 * Description: Build CEM_FPGA ASIC test menu.
 *
 **********************************************************************
 */
static int diag_menu_cem_fpga_tests (int show_menu)
{
    int ret_val = PASSED;

    if (show_menu == FOX_TREE_MAGIC_WORD) {
        ret_val = diag_build_menu_tree("CEM_FPGA TEST", cem_fpga_test_menu, CEM_FPGA_TEST_MENU_SIZE);
    } else {
        testname("CEM_FPGA TEST");
        ret_val = diag_menu_exec_all_items(cem_fpga_test_menu, CEM_FPGA_TEST_MENU_SIZE);
    }
    return (ret_val);
}


/**********************************************************************
 *
 * Function: diag_menu_cem_fpga
 *
 * Description: Build Cem FPGA test menu.
 *
 **********************************************************************
 */
int diag_menu_cem_fpga (int show_menu)
{
    int ret_val = PASSED;

    if (show_menu == FOX_TREE_MAGIC_WORD) {
        ret_val = diag_build_menu_tree("CEM FPGA", cem_fpga_menu,
                                       CEM_FPGA_MENU_SIZE);
        return (ret_val);
    } else {
        testname("CEM FPGA ");
        ret_val = diag_menu_exec_all_items(cem_fpga_menu, CEM_FPGA_MENU_SIZE);
        return (ret_val);
    }
}



uint32_t hyphy_pcie_init(struct file *fileHndl){
    uint32_t slot_no;
    static device_no = 0;
    slot_no = im_get_current_slot_no();

#if 0
    prverbose("\nHYPHY DEV ID          : %#x\n",hyphy_pci_config_read(slot_no, 0, VENDOR_DEVICE_ID_REG, 0));
    prverbose("PCI_BASE_ADDR0_OFFSET : %#x\n",hyphy_pci_config_read(slot_no, 0, PCI_BASE_ADDR0_OFFSET, 0));
#endif
#if 0
    if( PASSED != cem_hyphy_pcie_mmap()) {
        printf("\nError FAILED : %s %d \n",__FUNCTION__,__LINE__);
    }
#endif
#if 0
    fileHndl->baseAddr = adrspc_virt_im_bar_4_5; //(unsigned char *)hyphy_base_address;
#endif

    switch(slot_no) {
        case 3 :
                fileHndl->baseAddr = adrspc_virt_im_3_bar_4_5;
                break;
        case 4 :
                fileHndl->baseAddr = adrspc_virt_im_4_bar_4_5;
                break;
        case 5 :
                fileHndl->baseAddr = adrspc_virt_im_5_bar_4_5;
                break;
        case 7 :
                fileHndl->baseAddr = adrspc_virt_im_7_bar_4_5;
                break;
        case 8 :
                fileHndl->baseAddr = adrspc_virt_im_8_bar_4_5;
                break;
        case 11 :
                fileHndl->baseAddr = adrspc_virt_im_11_bar_4_5;
                break;
        case 12 :
                fileHndl->baseAddr = adrspc_virt_im_12_bar_4_5;
                break;
        default :
			err('f',"Unsupported slot for HyPhy init %d",slot_no);
            break;
   }


    fileHndl->devIdx = ++device_no;

    prverbose("HYPHY PCI INIT DONE\n");
    return 0;
}

int arrive_cli_shell (int show_menu)
{
    if (show_menu == FOX_TREE_MAGIC_WORD) {
        return (PASSED);
    }

    arrive_fpag_invoke_cli();
    return (PASSED);
}


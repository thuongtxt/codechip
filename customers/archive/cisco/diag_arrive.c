/*------------------------------------------------------------------
 * diag_arrive.c :  C file for Arrive CEM/CES FPGA support.
 *
 * May 2015, Yasdixit
 *
 * Copyright (c) 2015-2016 by Cisco Systems, Inc.
 *
 *------------------------------------------------------------------
 */

/*--------------------------- Include files ----------------------------------*/
#include "AtCliModule.h"
#include "AtDriver.h"
#include "AtSdhLine.h"
#include "AtSdhAug.h"
#include "AtSdhVc.h"
#include "AtSdhTug.h"
#include "AtPw.h"
#include "AtHalCisco.h"
#include "AtCiscoUpsr.h"
	
#include "diag_arrive.h"
#include "diag_im_tests.h"
#include "diag_im_ds1_liu.h"
 
/*--------------------------- Include files ----------------------------------*/
#include <AtDriver.h>
#include <AtEthPort.h>
#include <AtSdhLine.h>
#include <AtSdhAug.h>
#include <AtSdhVc.h>
#include <AtSdhTug.h>
#include <AtPw.h>
#include <AtPdhDe1.h>
#include <AtPdhDe3.h>
#include "AtModuleSdh.h"
#include "AtClasses.h"
#include "diag_arrive.h"
#include "AtObject.h"
#include "AtPdhSerialLine.h"

#include "commacro.h"
#include "time.h"
#include "AtCisco.h"
#include "AtChannel.h"

#include "AtModuleRam.h"
#include "AtRam.h"
#include "commacro.h"
#include "AtPwCounters.h"

#include <types.h>
#include "diag_rsp_common_lib.h"
#include "diag_hyphy.h"
#define int32 dontcare
#include "diag_arad_lib.h"
#undef int32

/*--------------------------- Define -----------------------------------------*/
#ifdef WARRIOR
#define cMaxNumSlots 16
#else
#define cMaxNumSlots 6
#endif
#define cNumAug4InAug16 4
#define cNumAug1InAug16 16
#define cNumVc3InAug1   3
#define cNumTug2InVc3   7
#define cNumVc12InTug2  3

#define cNumStsInOc48 48
#define cNumStsInAug1 3
#define cNumBitsInDword 32

#define IM_OCN_10G_BKPLANE_SERDES_COUNT 2
#define IM_OCN_10G_TFI_LINES 4

/*--------------------------- Macros -----------------------------------------*/
#define mSuccessAssert(ret)                                                    \
    do                                                                         \
        {                                                                      \
        eAtRet ret__ = ret;                                                    \
        if (ret__ != cAtOk)                                                    \
            return ret__;                                                      \
        }while(0)
/*--------------------------- Local typedefs ---------------------------------*/
typedef unsigned short      uint16_t;	/* 16-bit */
typedef unsigned char       uint8_t;	/* 8-bit */
typedef unsigned int        uint32_t;	/* 32-bit */
typedef short               int16_t;
typedef int                 int32_t;

#if 0
typedef struct arad_nif_t_ {
    int unit;                   /* Arad-0/Arad-1 */
    int nif_no;                 /* Network IF number 1-28 */
} arad_nif_t;
#endif

/*--------------------------- External variables -------------------------------*/
#if 0
extern void Tfi5Prbs(AtSdhLine ,  eAtPrbsMode );
#endif

extern volatile uint8_t *adrspc_virt_im_bar_2_3; /* 512 MB */

#if 0
extern volatile uint8_t *adrspc_virt_im_0_bar_2_3; /* 512 MB */
extern volatile uint8_t *adrspc_virt_im_1_bar_2_3; /* 512 MB */
extern volatile uint8_t *adrspc_virt_im_2_bar_2_3; /* 512 MB */
extern volatile uint8_t *adrspc_virt_im_3_bar_2_3; /* 512 MB */
extern volatile uint8_t *adrspc_virt_im_4_bar_2_3; /* 512 MB */
extern volatile uint8_t *adrspc_virt_im_5_bar_2_3; /* 512 MB */
#endif
extern int g_arad_init_flag;
extern uchar continuousStart[];
extern int serdesSelectContinuousTest[];
extern arad_nif_t aradPortIinfoContinuousTest[];
extern int gNoAradDetected;
extern int bshell(int unit, char *cmd);
int im_get_arad_port (int imslot, int im_port, arad_nif_t *arad_port_info);
/*--------------------------- Global variables -------------------------------*/
//extern tCiscoIm m_ims[];-
extern AtDevice DeviceAtSlot(uint8);		
extern int config_arad_remote_lpbk(int, int);
extern int im_type[];
/*--------------------------- Local variables --------------------------------*/
static tCiscoIm m_ims[cMaxNumSlots];

/* For command processing */
static AtTextUI m_textUI = NULL;

static int m_inited[cMaxNumSlots] = { [0 ... (cMaxNumSlots-1)] = FALSE};
static dev_hyphy_param_t hyphy_param;

/*--------------------------- Forward declarations ---------------------------*/
static CiscoIm ImAtSlot(uint8);
void ethCountersClears(void);
void DisplayAndClearCounters(int clearFlag, int StartPw, int EndPw, int Display);
int clearCountersAndDisaplay(uint8 display);
static void Au3TriggerInterrupt(uint8, uint8 , uint8);
static void Au3InterruptEnable(uint8 , uint8 , uint8 );
static uint32 SdSfBaseAddress(AtDevice);
static uint32 FlatStsId(AtDevice, uint8, uint8, uint8 );
static void Au3SdSfInterruptEnable(AtDevice, uint8, uint8, uint8);
static void Au3SdSfTriggerInterrupt(AtDevice , uint8 , uint8 , uint8 );
/*--------------------------- Implementation ---------------------------------*/
AtDevice DeviceGet(void) {

    //return AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
    uint8 slotNumber = (uint8) im_get_current_slot_no();
    return DeviceAtSlot(slotNumber);

}

static void PCIeWriteFunc(uint32 address, uint32 value) {
    *((volatile uint32 *) address) = value;
    __asm__("sync");
    (void) *((volatile uint32 *) address);
    __asm__("sync");
}

static uint32 PCIeReadFunc(uint32 address) {
    uint32 regVal = *((volatile uint32 *) address);
    __asm__("sync");
    return regVal;
}

static uint32 SlotBaseAddress(uint8 slotId) {
#if 0
    switch (slotId)
    {
        case 0: return (uint32)adrspc_virt_im_0_bar_2_3;
        case 1: return (uint32)adrspc_virt_im_1_bar_2_3;
        case 2: return (uint32)adrspc_virt_im_2_bar_2_3;
        case 3: return (uint32)adrspc_virt_im_3_bar_2_3;
        case 4: return (uint32)adrspc_virt_im_4_bar_2_3;
        case 5: return (uint32)adrspc_virt_im_5_bar_2_3;

        /* Impossible, but... */
        default: return 0;
    }
#endif
    return (uint32) adrspc_virt_im_bar_2_3;
}
static AtHal CreateHal(uint8 slotId) {
    return AtHalCiscoNew(SlotBaseAddress(slotId), PCIeWriteFunc, PCIeReadFunc);
}
static eAtRet SlotSetup(uint8 slotId) {
    CiscoIm im = ImAtSlot(slotId);
    AtHal hal = CreateHal(slotId);
    uint32 productCode = AtProductCodeGetByHal(hal);
    AtDevice newDevice;

    if (!AtDriverProductCodeIsValid(AtDriverSharedDriverGet(), productCode)) {
        AtPrintc(cSevCritical, "ERROR: product code 0x%08x is invalid\r\n", productCode);
        return cAtErrorInvlParm;
    }

    newDevice = AtDriverDeviceCreate(AtDriverSharedDriverGet(), productCode);
    AtDeviceIpCoreHalSet(newDevice, 0, hal);
    im->device = newDevice;

    return cAtOk;
}

int EnterSlot(uint8 slotId) {
    eAtRet ret;
    AtDevice device = DeviceAtSlot(slotId);

    /* Device has been added, just select it */
    if (device) {
        CliDeviceSet(device);
        return PASSED;
    }

    /* Create a new one */
    ret = SlotSetup(slotId);
    if (ret != cAtOk) {
        AtPrintc(cSevCritical, "Cannot initialize device, ret = %s\r\n", AtRet2String(ret));
        return FAILED;
    }

    /* Select it */
    CliDeviceSet(DeviceAtSlot(slotId));

    return PASSED;
}

static AtModuleSdh SdhModule(AtDevice device) {

    return (AtModuleSdh) AtDeviceModuleGet(device, cAtModuleSdh);
}

static AtModuleEth EthModule(AtDevice device) {

    return (AtModuleEth) AtDeviceModuleGet(device, cAtModuleEth);
}

static AtModulePdh PdhModule(AtDevice device) {

    return (AtModulePdh) AtDeviceModuleGet(device, cAtModulePdh);
}

static AtModulePw PwModule(AtDevice device) {

    return (AtModulePw) AtDeviceModuleGet(device, cAtModulePw);
}

static uint16 PwIdFromSdhChannelId(uint16 aug1Id, uint8 vc3Id, uint8 tug2Id, uint8 vc1xId, uint16 lineId) {

    /* TODO: just an example, user can replace it by another formula */
    return (aug1Id * 63) + (vc3Id * 21) + (tug2Id * 3) + vc1xId + lineId;
}

static uint8 *DefaultEthPortSourceMac(uint32 ethPortId) {

    /* TODO: just an example, user can replace it by another value */
    static uint8 buf[6] = { 0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA };
    return buf;
}

static uint8 *DefaultEthPortSourceIpv4(uint32 ethPortId) {

    /* TODO: just an example, user can replace it by another value */
    static uint8 buf[4] = { 172, 33, 34, 35 };
    return buf;
}

static uint8 *DefaultPwDestMac(uint32 ethPortId) {

    /* TODO: just an example, user can replace it by another value */
    static uint8 buf[6] = { 0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA };
    return buf;
}

static uint8 *DefaultPwSourceIpv4(uint32 ethPortId) {

    /* TODO: just an example, user can replace it by another value */
    static uint8 buf[4] = { 172, 0, 0, 0 };
    return buf;
}

static uint8 *DefaultPwDestIpv4(uint32 ethPortId) {

    /* TODO: just an example, user can replace it by another value */
    static uint8 buf[4] = { 172, 0, 0, 0 };
    return buf;
}

static uint32 DefaultPwUdpSourcePort(uint32 pwId) {

    /* TODO: just an example, user can replace it by another value */
    return pwId;
}

static uint32 DefaultPwUdpDestPort(uint32 pwId) {

    /* TODO: just an example, user can replace it by another value */
    return pwId;
}

static uint32 DefaultPwUdpExpectedPort(uint32 pwId) {

    /* TODO: just an example, user can replace it by another value */
    return pwId;
}

static uint32 DefaultPwPayloadSize(uint32 pwId) {

    /* TODO: just an example, user can replace it by another value */
    return 140;
}

static AtPwPsn UdpIpv4PsnCreate(uint16 pwId, uint16 ethPortId) {

    AtPwUdpPsn udpPsn = AtPwUdpPsnNew();
    AtPwIpV4Psn ipv4Psn = AtPwIpV4PsnNew();

    /* Setup UDP */
    AtPwUdpPsnSourcePortSet(udpPsn, DefaultPwUdpSourcePort(pwId));
    AtPwUdpPsnDestPortSet(udpPsn, DefaultPwUdpDestPort(pwId));
    AtPwUdpPsnExpectedPortSet(udpPsn, DefaultPwUdpExpectedPort(pwId));

    /* Setup IP */
    AtPwIpPsnSourceAddressSet((AtPwIpPsn) ipv4Psn, DefaultPwSourceIpv4(ethPortId));
    AtPwIpPsnDestAddressSet((AtPwIpPsn) ipv4Psn, DefaultPwDestIpv4(ethPortId));

    /* TODO: can set other attributes of each PSN here ... */

    /* Build PSN hierarchy */
    AtPwPsnLowerPsnSet((AtPwPsn) udpPsn, (AtPwPsn) ipv4Psn);

    return (AtPwPsn) udpPsn;
}

static AtPwPsn MplsPsnCreate(uint16 pwId, uint16 ethPortId) {

    AtPwMplsPsn mplsPsn = AtPwMplsPsnNew();
    tAtPwMplsLabel label;

    /* Make default label content, just for simple, the PW ID is used as label,
     * experimental field is set to 0 and time-to-live to 255 */
    /* CISCO modified , pw was undefined */
    AtPwMplsLabelMake((AtChannel) pwId, 0, 255, &label);

    /* Setup label stack:
     * - Inner label: is the label has S bit set to 1
     * - Outter labels: is labels have S bit set to 0
     * These labels are for MPLS packets transmitted to Ethernet side. Application
     * can flexible configure any labels.
     */
    AtPwMplsPsnInnerLabelSet(mplsPsn, &label);
    AtPwMplsPsnOuterLabelAdd(mplsPsn, &label);

    /* For direction from ETH to TDM, when MPLS packets are received, their
     * inner label (S = 1) are used to look for Pseudowire that they belong to.
     * The following line is to setup */
    /* CISCO modified , pw was undefined */
    AtPwMplsPsnExpectedLabelSet(mplsPsn, AtChannelIdGet((AtChannel) pwId));

    /* Apply this MPLS for this PW. Note, since PSN object is allocated in this
     * context, so it should also be deleted in this context. The internal
     * implementation already clones it. */
    /*    AtPwPsnSet(pw, (AtPwPsn)mplsPsn); */
    /*    AtObjectDelete((AtObject)mplsPsn); */
    return (AtPwPsn) mplsPsn;
}

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(void) /*{

 return AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
 }*/
{

    //return AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
    uint8 slotNumber = (uint8) im_get_current_slot_no();
    return DeviceAtSlot(slotNumber);

}

static AtSdhLine Line1(void) {

    AtModuleSdh sdhModule = (AtModuleSdh) AtDeviceModuleGet(Device(), cAtModuleSdh);
    return AtModuleSdhLineGet(sdhModule, 0);
}

/*
 * Mapping detail for line 1
 * - AUG-1#1: VC-4
 * - AUG-1#2: AU-4 <--> VC-4 <--> TUG-3 <--> VC-3
 * - AUG-1#3: AU-4 <--> VC-4 <--> TUG-3 <--> TUG-2 <--> VC-12
 */
static void SetupSdhMapping(void) {

    AtSdhLine line1 = Line1();
    uint8 aug1Id;
    AtSdhChannel aug1, vc4, vc3, vc12, tug3, tug2;

    /* Setup mapping for AUG-1#1 to map VC-4 */
    aug1Id = 0;
    aug1 = (AtSdhChannel) AtSdhLineAug1Get(line1, aug1Id);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1MapVc4);
    vc4 = (AtSdhChannel) AtSdhLineVc4Get(line1, aug1Id);
    AtSdhChannelMapTypeSet(vc4, cAtSdhVcMapTypeVc4MapC4);

    /* Setup mapping for AUG-1#2 to map AU-4 <--> VC-4 <--> TUG-3 <--> VC-3 */
    aug1Id = 1;
    aug1 = (AtSdhChannel) AtSdhLineAug1Get(line1, aug1Id);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1MapVc4);
    vc4 = (AtSdhChannel) AtSdhLineVc4Get(line1, aug1Id);
    AtSdhChannelMapTypeSet(vc4, cAtSdhVcMapTypeVc4Map3xTug3s);
    tug3 = (AtSdhChannel) AtSdhLineTug3Get(line1, aug1Id, 0); /* Use the first one for this example */
    AtSdhChannelMapTypeSet(tug3, cAtSdhTugMapTypeTug3MapVc3);
    vc3 = (AtSdhChannel) AtSdhLineVc3Get(line1, aug1Id, 0);
    AtSdhChannelMapTypeSet(vc3, cAtSdhVcMapTypeVc3MapC3);

    /* Setup mapping for AUG-1#3 to map AU-4 <--> VC-4 <--> TUG-3 <--> TUG-2 <--> VC-12 */
    aug1Id = 2;
    aug1 = (AtSdhChannel) AtSdhLineAug1Get(line1, aug1Id);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1MapVc4);
    vc4 = (AtSdhChannel) AtSdhLineVc4Get(line1, aug1Id);
    AtSdhChannelMapTypeSet(vc4, cAtSdhVcMapTypeVc4Map3xTug3s);
    tug3 = (AtSdhChannel) AtSdhLineTug3Get(line1, aug1Id, 0); /* Use the first one for this example */
    AtSdhChannelMapTypeSet(tug3, cAtSdhTugMapTypeTug3Map7xTug2s);
    tug2 = (AtSdhChannel) AtSdhLineTug2Get(line1, aug1Id, 0, 0); /* Use the first one for this example */
    AtSdhChannelMapTypeSet(tug2, cAtSdhTugMapTypeTug2Map3xTu12s);
    vc12 = (AtSdhChannel) AtSdhLineVc1xGet(line1, aug1Id, 0, 0, 0); /* Use the first one for this example */
    AtSdhChannelMapTypeSet(vc12, cAtSdhVcMapTypeVc1xMapC1x);
}

static void SetupPwMpls(AtPw pw) {

    AtPwMplsPsn mplsPsn = AtPwMplsPsnNew();
    tAtPwMplsLabel label;

    /* Make default label content, just for simple, the PW ID is used as label,
     * experimental field is set to 0 and time-to-live to 255 */
    if (AtPwMplsLabelMake(AtChannelIdGet((AtChannel) pw) + 1, 1, 1, &label) == NULL)
        printf("MPLS label make failure");
    else
        debug_print("%d %d, %d\n", label.experimental, label.label, label.timeToLive);

    /* Setup label stack:
     * - Inner label: is the label has S bit set to 1
     * - Outter labels: is labels have S bit set to 0
     * These labels are for MPLS packets transmitted to Ethernet side. Application
     * can flexible configure any labels.
     */
    if (AtPwMplsPsnInnerLabelSet(mplsPsn, &label) != cAtOk)
        printf("Error inner label error\n");
//    if(AtPwMplsPsnOuterLabelAdd(mplsPsn, &label) != cAtOk)
//        printf("Error outer label error\n");

    /* For direction from ETH to TDM, when MPLS packets are received, their
     * inner label (S = 1) are used to look for Pseudowire that they belong to.
     * The following line is to setup */
    if (AtPwMplsPsnExpectedLabelSet(mplsPsn, AtChannelIdGet((AtChannel) pw) + 1) != cAtOk)
        printf("Error expected label error\n");

    /* Apply this MPLS for this PW. Note, since PSN object is allocated in this
     * context, so it should also be deleted in this context. The internal
     * implementation already clones it. */
    AtPwPsnSet(pw, (AtPwPsn) mplsPsn);
    AtObjectDelete((AtObject) mplsPsn);
}

/* This function is to setup DMAC and VLANs for Ethernet frame transmitted to
 * Ethernet port. In this example, one VLAN is used. Ethernet port 1 is used for
 * physical layer */
static void SetupPwMac(AtPw pw) {

    static uint8 mac[] = { 0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA }; /* Application has to know */
    static tAtEthVlanTag cVlan;
    AtModuleEth ethModule = (AtModuleEth) AtDeviceModuleGet(Device(), cAtModuleEth);

    AtEthVlanTagConstruct(0, 0, AtChannelIdGet((AtChannel) pw), &cVlan);

    AtPwEthPortSet(pw, AtModuleEthPortGet(ethModule, 0));
//    AtPwEthHeaderSet(pw, mac, &cVlan, NULL);
    AtPwEthHeaderSet(pw, mac, NULL, NULL);
}

/* This function make a default setup for PW:
 * - MPLS information
 * - MAC information
 */
static void SetupPw(AtPw pw) {

    SetupPwMpls(pw);
    SetupPwMac(pw);
}

static uint32 De1Id(void) {

    /* Use first DE1 for demonstration purpose */
    return 0;
}

static uint32 De3Id(void) {

    /* Use first DE1 for demonstration purpose */
    return 0;
}

static AtPdhDe1 PdhDe1Get(uint32 de1Id) {

    return AtModulePdhDe1Get(PdhModule(DeviceGet()), de1Id);
}

static AtPdhDe3 PdhDe3Get(uint32 de3Id) {
    return AtModulePdhDe3Get(PdhModule(DeviceGet()), de3Id);
}

static eAtPdhDe3FrameType DefaultDe3FrameType(void) {
    return cAtPdhE3Unfrm;
}

static eAtPdhDe3SerialLineMode DefaultSerialLineMode(void) {
    /* Use this mode for LIU - DE3 */
    return cAtPdhDe3SerialLineModeE3;
}

static eAtPdhDe1FrameType DefaultDe1FrameType(void) {
    return cAtPdhE1UnFrm;
}

static AtModulePrbs PrbsModule(AtDevice device) {
    return (AtModulePrbs) AtDeviceModuleGet(device, cAtModulePrbs);
}

static AtPrbsEngine PrbsEngine(uint32 engineId) {
    AtModulePrbs prbsModule = PrbsModule(DeviceGet());
    return AtModulePrbsEngineGet(prbsModule, engineId);
}

static uint32 StickyGet(uint32 engineId, eBool read2Clear) {
    AtPrbsEngine prbsEngine = PrbsEngine(engineId);
    return (read2Clear) ? AtPrbsEngineAlarmHistoryClear(prbsEngine) : AtPrbsEngineAlarmHistoryGet(prbsEngine);
}

static void PrbsEngineAllCountersClear(uint32 engineId) {
    AtPrbsEngine engine = PrbsEngine(engineId);

    /* Latch all counters */
    AtPrbsEngineAllCountersLatch(engine);

    /* Get and clears */
    AtPrbsEngineCounterClear(engine, cAtPrbsEngineCounterTxBit);
    AtPrbsEngineCounterClear(engine, cAtPrbsEngineCounterRxBit);
    AtPrbsEngineCounterClear(engine, cAtPrbsEngineCounterRxSync);
    AtPrbsEngineCounterClear(engine, cAtPrbsEngineCounterRxBitError);
}

eAtRet DiagSerdesControllerReset(uint8 line) {
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);
    AtDevice device = DeviceGet();

    AtSerdesController serdesControler = AtModuleEthSerdesController(EthModule(device), line);
    return (AtSerdesControllerReset(serdesControler));

}

eAtRet SetupLiuSAToPMplsDe1(tLiuSatopDe1 d1) {
    uint16 liuId;
    eAtRet ret;
    eAtModulePwRet returnValue;
    AtPw pw;
    AtPwPsn psn;
    debug_print("\nPre Execution\n");
    AtSerdesController serdesControler = NULL;
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);
    AtDevice device = DeviceGet();

    ret = AtDeviceDiagnosticModeEnable(device, cAtFalse);
    if (ret != cAtOk) {
        printf("ERROR: Device Disable Diagnostic err code : %d  \r\n", ret);
        return cAtError;
    }

    if (!m_inited[slotId]) {
        //m_inited[slotId] = TRUE;
        prverbose("device init\r\n");
        ret = AtDeviceInit(device);  // device init
        if (ret != cAtOk)
        {
            printf("ERROR: Device Init Failed err code : %d  \r\n",ret);
            return cAtError;
        }
    }


    AtModulePw pwModule = (AtModulePw) AtDeviceModuleGet(device, cAtModulePw);
    if (pwModule == NULL) {
        printf("WARNING: module does not exist\r\n");
        return cAtErrorNullPointer;
    }

    /* Delete PW's (if any) before creating */
    for (liuId = 0; liuId < 48; liuId++) {
        AtPw pw = AtModulePwGetPw(pwModule, liuId);
        if (pw == NULL)
            continue;
        AtChannelEnable((AtChannel) pw, cAtFalse); // pw disable 1
        ret = AtPwCircuitUnbind(pw); // pw circuit unbind 1
        if (ret != cAtOk) {
            printf("ERROR: Cannot Un-Bind Circuit %d\n", liuId);
            return ret;
        }
        ret = AtPwEthPortSet(pw, NULL); // pw ethport 1 none
        if (ret != cAtOk) {
            printf("ERROR: Cannot Un-Bind Circuit %d\n", liuId);
            return ret;
        }
        ret = AtModulePwDeletePw(pwModule, liuId);
        if (ret != cAtOk) {
            printf("ERROR: Cannot Delete-Pw %d\n", liuId);
            return ret;
        }
    }

    /*disable jitterbuffer automatically center after init 1 minute*/
    returnValue = AtModulePwJitterBufferCenteringEnable(pwModule, cAtFalse);
    if (returnValue != cAtOk) {
        printf("ERROR: Cannot disable jitter buffer centering\n");
        return ret;
    }

    if (AtModulePwJitterBufferCenteringIsEnabled(pwModule) != cAtFalse) {
        printf("ERROR: Jitter buffer centering is not disabled\n");
        return ret;
    }

    uint8 ethPortId = 0; // Try 0 also, DefaultEthernetPortId();
    AtEthPort ethPort = AtModuleEthPortGet(EthModule(device), ethPortId);

    if (ethPort == NULL) {
        printf("WARNING: Port %u does not exist\r\n", ethPortId);
        return cAtErrorNullPointer;
    }

    ret = AtEthPortInterfaceSet(ethPort, d1.ethPortInterface); //eth port interface 1 xgmii
    if (ret != cAtOk) {
        printf("ERROR: Cannot set port interface  \r\n");
        return ret;
    }

    ret = AtChannelLoopbackSet((AtChannel) ethPort, d1.loopbackMode); // eth port loopback 1 release
    if (ret != cAtOk) {
        printf("ERROR: Cannot set loopback\r\n");
        return ret;
    }

    ret = AtEthPortSourceMacAddressSet(ethPort, DefaultEthPortSourceMac(ethPortId)); //eth port srcmac 1 C0.CA.C0.CA.C0.CA
    if (ret != cAtOk) {
        printf("ERROR: Cannot set source mac address \r\n");
        return ret;
    }

    ret = AtEthPortIpV4AddressSet(ethPort, DefaultEthPortSourceIpv4(ethPortId)); //eth port ipv4 1 172.33.34.35
    if (ret != cAtOk) {
        printf("ERROR: Cannot set IPV4 address \r\n");
        return ret;
    }

    if (rsp2_get_slot_no() == RSP_1) {
        serdesControler = AtModuleEthSerdesController(EthModule(device), 1);
        AtEthPortTxSerdesBridge(ethPort, NULL); //Remove if any bridge before
        AtEthPortTxSerdesBridge(ethPort, serdesControler);
        ret = AtEthPortRxSerdesSelect(ethPort, serdesControler);
        if (ret != cAtOk) {
            printf("ERROR: Cannot set port interface  \r\n");
            return ret;
        }
    } else if (rsp2_get_slot_no() == RSP_0) {
        serdesControler = AtModuleEthSerdesController(EthModule(device), 0);
        AtEthPortTxSerdesBridge(ethPort, NULL); //Remove if any bridge before
        ret = AtEthPortRxSerdesSelect(ethPort, serdesControler);
        if (ret != cAtOk) {
            printf("ERROR: Cannot set port interface  \r\n");
            return ret;
        }
    }
//    AtCliExecute("show eth port serdes 1-2");

    for (liuId = 0; liuId < 48; liuId++) {
        debug_print(".");
        uint16 pwId = liuId;
        AtPdhDe1 de1 = AtModulePdhDe1Get(PdhModule(DeviceGet()), liuId);
        if (de1 == NULL) {
            printf("WARNING: de1 does not exist\r\n");
            return cAtErrorNullPointer;
        }

        ret = AtPdhChannelFrameTypeSet(de1, d1.frameType); // pdh de1 framing 1-48 e1_unframed
        if (ret != cAtOk) {
            printf("Error - Cannot set frame type %d\n", liuId);
            return ret;
        }

        ret = AtChannelLoopbackSet(de1, d1.pdhLoopbackMode); // pdh de1 loopback 1-48 release
        if (ret != cAtOk) {
            printf("Error - Cannot set frame type %d\n", liuId);
            return ret;
        }

        pw = (AtPw) AtModulePwSAToPCreate(pwModule, liuId);  //  pw create satop 1
        if (pw == NULL) {
            printf("WARNING: pw does not exist %d\n", liuId);
            return cAtErrorNullPointer;
        }

        ret = AtPwCircuitBind(pw, (AtChannel) de1); // pw circuit bind 1 de1.1
        if (ret != cAtOk) {
            printf("ERROR: Cannot Bind Circuit %d\n", liuId);
            return ret;
        }

        if (AtChannelIsEnabled((AtChannel) pw)) {
            AtChannelEnable((AtChannel) pw, cAtFalse);   // pw disable 1
            AtPwEthPortSet(pw, NULL); // pw ethport 1 none
        }

        ret = AtPwEthPortSet(pw, ethPort); // pw ethport 1 1
        if (ret != cAtOk) {
            printf("ERROR: Cannot set Ethernet port %d\n", liuId);
            return ret;
        }

        ret = AtPwRtpEnable(pw, cAtFalse);
        if (ret != cAtOk) {
            printf("ERROR: Cannot Disable RTP %d\n", liuId);
            return ret;
        }
#if 0

        /* Set payload size DS1 = 193, E1 = 256 */
        if (d1.frameType == cAtPdhE1UnFrm) {
//            printf("E1 Mode %d\n", liuId);
            ret = AtPwPayloadSizeSet(pw, 256);
            if (ret != cAtOk) {
                printf("ERROR: Cannot set pw payload size %d\n", liuId);
                return ret;
            }
        } else {
//            printf("DS1 Mode %d\n", liuId);
            ret = AtPwPayloadSizeSet(pw, 193);
            if (ret != cAtOk) {
                printf("ERROR: Cannot set pw payload size %d\n", liuId);
                return ret;
            }
        }

        ret = AtPwJitterBufferSizeInPacketSet(pw, 20);
        if (ret != cAtOk) {
            printf("ERROR: Cannot set jitterbuffer size %d\n", liuId);
            return ret;
        }
        ret = AtPwJitterBufferDelayInPacketSet(pw, 10);
        if (ret != cAtOk) {
            printf("ERROR: Cannot set jitterbufferdelay %d\n", liuId);
            return ret;
        }

        ret = AtPwLopsSetThresholdSet(pw, AtPwLopsThresholdMin(pw));
        if (ret != cAtOk) {
            printf("ERROR: Cannot set pw lop threshold to %d \r\n", AtPwLopsThresholdMin(pw));
            return ret;
        }
#endif
#if 0
        ret = AtPwRemotePacketLossDefectThresholdSet(pw, 1);
        if (ret != cAtOk)
        {
            printf("ERROR: Cannot set pw remote threshold to 1 %d\n", liuId);
        }
#endif

        //    pw psn 1 mpls
        //    pw mpls innerlabel 1 1.1.1
        //    pw mpls expectedlabel 1 1
        //    pw ethheader 1 c0.ca.c0.ca.c0.ca none none
        SetupPw(pw);

        /* Create MPLS PSN */
        AtChannelEnable((AtChannel) pw, cAtTrue);   // pw enable 1
    }

    if(IS_BOARD_TYPE_ASR907_RSP3_400())
    {
        /* Serdes tuning */
        if(d1.ethPortInterface == cAtEthPortInterfaceXGMii)
        {
            if (rsp2_get_slot_no() == RSP_1) {
                serdesControler = AtModuleEthSerdesController(EthModule(device), 1);
            }else if (rsp2_get_slot_no() == RSP_0) {
                serdesControler = AtModuleEthSerdesController(EthModule(device), 0);
            } else {
                printf("ERROR: Wrong RSP slot \n");
                return cAtError;
            }
            AtSerdesControllerPhysicalParamSet(serdesControler, cAtSerdesParamTxDiffCtrl, 0xf);
            AtSerdesControllerPhysicalParamSet(serdesControler, cAtSerdesParamTxPostCursor, 0x14);
        }
    }
    return ret;
}

static eBool IsDS3(uint32 frameType) {
    if (frameType == cAtPdhE3Unfrm)
        return cAtFalse;
    return cAtTrue;
}

eAtRet SetupLiuSAToPMplsDe3(tLiuSatopDe3 d3) {
    uint16 liuId;
    eAtRet ret;
    eAtModulePwRet returnValue;
    AtPw pw;
    AtPwPsn psn;
    char buffer[100];
    AtSerdesController serdesControler = NULL;
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);

    printf("\nPre Execution\n");

    //sprintf(buffer,"device diag check disable");
    //AtCliExecute(buffer);

    AtDevice device = DeviceGet();
    eBool isDs3 = IsDS3(d3.frameType);

    ret = AtDeviceDiagnosticModeEnable(device, cAtFalse);
    if (ret != cAtOk) {
        printf("ERROR: Device Disable Diagnostic err code : %d  \r\n", ret);
        return cAtError;
    }

    if (!m_inited[slotId]) {
        //m_inited[slotId] = TRUE;
        prverbose("device init\r\n");
        ret = AtDeviceInit(device);  // device init
        if (ret != cAtOk)
        {
            printf("ERROR: Device Init Failed err code : %d  \r\n",ret);
            return cAtError;
        }
    }

    AtModulePw pwModule = (AtModulePw) AtDeviceModuleGet(device, cAtModulePw);
    if (pwModule == NULL) {
        printf("WARNING: module does not exist\r\n");
        return cAtErrorNullPointer;
    }

    /* Delete PW's (if any) before creating */
    for (liuId = 0; liuId < 48; liuId++) {
        AtPw pw = AtModulePwGetPw(pwModule, liuId);
        if (pw == NULL)
            continue;
        AtChannelEnable((AtChannel) pw, cAtFalse); // pw disable 1
        ret = AtPwCircuitUnbind(pw); // pw circuit unbind 1
        if (ret != cAtOk) {
            printf("ERROR: Cannot Un-Bind Circuit %d\n", liuId);
            return ret;
        }
        ret = AtPwEthPortSet(pw, NULL); // pw ethport 1 none
        if (ret != cAtOk) {
            printf("ERROR: Cannot Un-Bind Circuit %d\n", liuId);
            return ret;
        }
        ret = AtModulePwDeletePw(pwModule, liuId);
        if (ret != cAtOk) {
            printf("ERROR: Cannot Delete-Pw %d\n", liuId);
            return ret;
        }
    }

    /*disable jitterbuffer automatically center after init 1 minute*/
    returnValue = AtModulePwJitterBufferCenteringEnable(pwModule, cAtFalse);
    if (returnValue != cAtOk) {
        printf("ERROR: Cannot Delete-Pw %d\n", liuId);
        return ret;
    }

    if (AtModulePwJitterBufferCenteringIsEnabled(pwModule) != cAtFalse) {
        printf("ERROR: Cannot Delete-Pw %d\n", liuId);
        return ret;
    }

    uint8 ethPortId = 0; // Try 0 also, DefaultEthernetPortId();
    AtEthPort ethPort = AtModuleEthPortGet(EthModule(device), ethPortId);

    if (ethPort == NULL) {
        printf("WARNING: Port %u does not exist\r\n", ethPortId);
        return cAtErrorNullPointer;
    }

    ret = AtEthPortInterfaceSet(ethPort, d3.ethPortInterface); //eth port interface 1 xgmii
    if (ret != cAtOk) {
        printf("ERROR: Cannot set port interface  \r\n");
        return ret;
    }
    ret = AtChannelLoopbackSet((AtChannel) ethPort, d3.loopbackMode); // eth port loopback 1 release
    if (ret != cAtOk) {
        printf("ERROR: Cannot set loopback\r\n");
        return ret;
    }

    ret = AtEthPortSourceMacAddressSet(ethPort, DefaultEthPortSourceMac(ethPortId)); //eth port srcmac 1 C0.CA.C0.CA.C0.CA
    if (ret != cAtOk) {
        printf("ERROR: Cannot set source mac address \r\n");
        return ret;
    }

    ret = AtEthPortIpV4AddressSet(ethPort, DefaultEthPortSourceIpv4(ethPortId)); //eth port ipv4 1 172.33.34.35
    if (ret != cAtOk) {
        printf("ERROR: Cannot set IPV4 address \r\n");
        return ret;
    }

    if (rsp2_get_slot_no() == RSP_1) {
        serdesControler = AtModuleEthSerdesController(EthModule(device), 1);
        AtEthPortTxSerdesBridge(ethPort, NULL); //Remove if any bridge before
        AtEthPortTxSerdesBridge(ethPort, serdesControler);
        ret = AtEthPortRxSerdesSelect(ethPort, serdesControler);
        if (ret != cAtOk) {
            printf("ERROR: Cannot set port interface  \r\n");
            return ret;
        }
    } else if (rsp2_get_slot_no() == RSP_0) {
        serdesControler = AtModuleEthSerdesController(EthModule(device), 0);
        AtEthPortTxSerdesBridge(ethPort, NULL); //Remove if any bridge before
        ret = AtEthPortRxSerdesSelect(ethPort, serdesControler);
        if (ret != cAtOk) {
            printf("ERROR: Cannot set port interface  \r\n");
            return ret;
        }
    }
//    AtCliExecute("show eth port serdes 1-2");

    for (liuId = 47; ((liuId >= 0) && (liuId <= 47)); liuId--) {
        debug_print(".");
        uint16 pwId = liuId;
        AtPdhSerialLine serialLine;
        AtPdhDe1 de1 = AtModulePdhDe3Get(PdhModule(DeviceGet()), liuId);
        if (de1 == NULL) {
            printf("WARNING: de1 does not exist\r\n");
            return cAtErrorNullPointer;
        }

        serialLine = AtModulePdhDe3SerialLineGet(PdhModule(DeviceGet()), liuId);
        ret = AtPdhSerialLineModeSet(serialLine,
                isDs3 ? cAtPdhDe3SerialLineModeDs3 : cAtPdhDe3SerialLineModeE3);
        if (ret != cAtOk) {
            printf("ERROR: AtPdhSerialLineModeSet : %d\r\n", (int) ret);
            return ret;
        }

        ret = AtPdhChannelFrameTypeSet(de1, d3.frameType); // pdh de3 framing 1-48 e1_unframed
        if (ret != cAtOk) {
            printf("Error - Cannot set frame type %d\n", liuId);
            return ret;
        }

        pw = (AtPw) AtModulePwSAToPCreate(pwModule, liuId);  //  pw create satop 1
        if (pw == NULL) {
            printf("WARNING: pw does not exist %d\n", liuId);
            return cAtErrorNullPointer;
        }

        ret = AtPwCircuitBind(pw, (AtChannel) de1); // pw circuit bind 1 de1.1
        if (ret != cAtOk) {
            printf("ERROR: Cannot Bind Circuit %d\n", liuId);
            return ret;
        }

        if (AtChannelIsEnabled((AtChannel) pw)) {
            AtChannelEnable((AtChannel) pw, cAtFalse);   // pw disable 1
            AtPwEthPortSet(pw, NULL); // pw ethport 1 none
        }

        if (d3.ethPortInterface == cAtEthPortInterfaceQSgmii) {
            if (liuId <= 11) {
                ret = AtPwEthPortQueueSet(pw, 0);
            } else if (liuId >= 12 && liuId <= 23) {
                ret = AtPwEthPortQueueSet(pw, 1);
            } else if (liuId >= 24 && liuId <= 35) {
                ret = AtPwEthPortQueueSet(pw, 2);
            } else if (liuId >= 36 && liuId <= 47) {
                ret = AtPwEthPortQueueSet(pw, 3);
            }
            if (ret != cAtOk) {
                printf("ERROR: Cannot set EthPort Queue %d\n", liuId);
                return ret;
            }
        }

        ret = AtPwEthPortSet(pw, ethPort); // pw ethport 1 1
        if (ret != cAtOk) {
            printf("ERROR: Cannot set Ethernet port %d\n", liuId);
            return ret;
        }

        ret = AtPwRtpEnable(pw, cAtFalse);
        if (ret != cAtOk) {
            printf("ERROR: Cannot Disable RTP %d\n", liuId);
            return ret;
        }
#if 0
        ret = AtPwJitterBufferSizeInPacketSet(pw, 20);
        if (ret != cAtOk) {
            printf("ERROR: Cannot set jitterbuffer size %d\n", liuId);
            return ret;
        }
        ret = AtPwJitterBufferDelayInPacketSet(pw, 10);
        if (ret != cAtOk) {
            printf("ERROR: Cannot set jitterbufferdelay %d\n", liuId);
            return ret;
        }
        ret = AtPwLopsSetThresholdSet(pw, AtPwLopsThresholdMin(pw));
        if (ret != cAtOk) {
            printf("ERROR: Cannot set pw lop threshold to %d \r\n", AtPwLopsThresholdMin(pw));
            return ret;
        }
#if 0
        ret = AtPwRemotePacketLossDefectThresholdSet(pw, 1);
        if (ret != cAtOk)
        {
            printf("ERROR: Cannot set pw remote threshold to 1 \r\n");
        }
#endif
        ret = AtPwPayloadSizeSet(pw, 702);
        if (ret != cAtOk) {
            printf("ERROR: Cannot set pw payload size %d\n", liuId);
            return ret;
        }
#endif

        //    pw psn 1 mpls
        //    pw mpls innerlabel 1 1.1.1
        //    pw mpls expectedlabel 1 1
        //    pw ethheader 1 c0.ca.c0.ca.c0.ca none none
        SetupPw(pw);
        /* Create MPLS PSN */
        AtChannelEnable((AtChannel) pw, cAtTrue);   // pw enable 1
    }

    /* Serdes tuning */
    if(IS_BOARD_TYPE_ASR907_RSP3_400())
    {
        if(d3.ethPortInterface == cAtEthPortInterfaceXGMii)
        {
            if (rsp2_get_slot_no() == RSP_1) {
                serdesControler = AtModuleEthSerdesController(EthModule(device), 1);
            }else if (rsp2_get_slot_no() == RSP_0) {
                serdesControler = AtModuleEthSerdesController(EthModule(device), 0);
            } else {
                printf("ERROR: Wrong RSP slot \n");
                return cAtError;
            }
            AtSerdesControllerPhysicalParamSet(serdesControler, cAtSerdesParamTxDiffCtrl, 0xf);
            AtSerdesControllerPhysicalParamSet(serdesControler, cAtSerdesParamTxPostCursor, 0x14);
        }
    }
    return ret;
}

static eBool IsProduct10G(AtDevice device) {
    uint32 productCode = AtDeviceProductCodeGet(device);
    if ((productCode & cBit23_0) == (0x60210051 & cBit23_0))
        return cAtTrue;
    return cAtFalse;
}

eAtRet IM_Ocn_10G_Configure_Xfi_Group(uint8 xfiGroup) {
    AtDevice device;
    AtModuleEth ethModule;
    AtEthPort ethPort0, ethPort1;
    AtSerdesController rsp0Serdes0, rsp0Serdes1, rsp1Serdes0, rsp1Serdes1 ;
    uint8 slotId = (uint8) im_get_current_slot_no();

    EnterSlot(slotId);
    device = DeviceGet();

    ethModule = (AtModuleEth) AtDeviceModuleGet(device, cAtModuleEth);
    if (ethModule == NULL) {
        printf("WARNING: module ETH does not exist\r\n");
        return cAtErrorNullPointer;
    }

    eBool isProduct10G = IsProduct10G(device);
    if (isProduct10G) {

        AtAssert(AtCiscoModuleEthXfiGroupSet(ethModule, xfiGroup) == cAtOk);  //xfiGroup
    }

    /* Common control Serdes for both 10G and 20G product */
    ethPort0 = AtModuleEthPortGet(ethModule, 0);
    ethPort1 = AtModuleEthPortGet(ethModule, 1);

    rsp0Serdes0 = AtModuleEthSerdesController(ethModule,   0 + (xfiGroup *2 ));
    rsp0Serdes1 = AtModuleEthSerdesController(ethModule,   1 + (xfiGroup *2 ));
    rsp1Serdes0 = AtModuleEthSerdesController(ethModule,   4 + (xfiGroup *2 ));
    rsp1Serdes1 = AtModuleEthSerdesController(ethModule,   5 + (xfiGroup *2 ));

    if (rsp0Serdes0 == NULL) {
        printf("ERROR: rsp0Serdes is NULL!!\r\n");
    }
    if (rsp1Serdes0 == NULL) {
        printf("ERROR: rsp1Serdes is NULL!!\r\n");
    }

    if (rsp2_get_slot_no() == RSP_1) {
    	prverbose("in RSP 1\n");
        AtEthPortRxSerdesSelect(ethPort0, rsp1Serdes0);
        prverbose("INFO: set serdes selection is %d\r\n", AtSerdesControllerIdGet(rsp1Serdes0));
        prverbose("INFO: get serdes selection is %d\r\n",
                AtSerdesControllerIdGet(AtEthPortRxSelectedSerdes(ethPort0)));

        AtEthPortRxSerdesSelect(ethPort1, rsp1Serdes1);
        prverbose("INFO: set serdes selection is %d\r\n", AtSerdesControllerIdGet(rsp1Serdes1));
        prverbose("INFO: get serdes selection is %d\r\n",
                AtSerdesControllerIdGet(AtEthPortRxSelectedSerdes(ethPort1)));
    }
    if (rsp2_get_slot_no() == RSP_0) {
    	prverbose("in RSP 0\n");
        AtEthPortRxSerdesSelect(ethPort0, rsp0Serdes0);
        prverbose("INFO: set serdes selection is %d\r\n", AtSerdesControllerIdGet(rsp0Serdes0));
        prverbose("INFO: get serdes selection is %d\r\n",
                AtSerdesControllerIdGet(AtEthPortRxSelectedSerdes(ethPort0)));

        AtEthPortRxSerdesSelect(ethPort1, rsp0Serdes1);
        prverbose("INFO: set serdes selection is %d\r\n", AtSerdesControllerIdGet(rsp0Serdes1));
        prverbose("INFO: get serdes selection is %d\r\n",
                AtSerdesControllerIdGet(AtEthPortRxSelectedSerdes(ethPort1)));
    }
    return cAtOk;
}

int Im_Ocn_10G_Arrive_Device_Init(void) {
    eAtRet ret;
    AtDevice device;
    uint8 slotId = (uint8) im_get_current_slot_no();

    EnterSlot(slotId);

    printf("\nArrive Device Init ..\n");

    device = DeviceGet();

    ret = AtDeviceDiagnosticModeEnable(device, cAtFalse);
    if (ret != cAtOk) {
        printf("ERROR: Device Disable Diagnostic err code : %d  \r\n", ret);
        return FAILED;
    }

    ret = AtDeviceInit(device);  // device init
    if (ret != cAtOk) {
        err("ERROR: Device Init Failed err code : %d  \r\n", ret);
        return FAILED;
    }
    return (PASSED);
}

eAtRet SetupOcnSAToPMplsDe3(tCep cep) {
    eAtRet ret;
    AtDevice device;
    AtPw pw;
    AtPwPsn psn;
    AtModulePw pwModule;
    AtModuleEth ethModule;
    AtEthPort ethPort0, ethPort1;
    AtModuleSdh sdhModule;
    uint8 oc48LineId;
    AtSerdesController bSerdes;
    const uint8 numSupportedLine = 4;
    uint8 slotId = (uint8) im_get_current_slot_no();

    EnterSlot(slotId);

    printf("\nPre Execution\n");

    eAtEthPortInterface portInterface = cep.ethPortInterface;
    eAtLoopbackMode loopback = cep.loopbackMode;
    device = DeviceGet();

    //if(PASSED != Im_Ocn_10G_Arrive_Device_Init()) {
    	//err('f',0,"Arrive Device Init Failed");
   // }

    pwModule = (AtModulePw) AtDeviceModuleGet(device, cAtModulePw);
    if (pwModule == NULL) {
        printf("WARNING: module PW does not exist\r\n");
        return cAtErrorNullPointer;
    }

    ethModule = (AtModuleEth) AtDeviceModuleGet(device, cAtModuleEth);
    if (ethModule == NULL) {
        printf("WARNING: module ETH does not exist\r\n");
        return cAtErrorNullPointer;
    }

    eBool isProduct10G = IsProduct10G(device);
    if (isProduct10G) {

        AtAssert(AtCiscoModuleEthXfiGroupSet(ethModule, cep.xfiGroup) == cAtOk);  //xfiGroup
    }

    /* Common control Serdes for both 10G and 20G product */
    ethPort0 = AtModuleEthPortGet(ethModule, 0);
    ethPort1 = AtModuleEthPortGet(ethModule, 1);

    IM_Ocn_10G_Configure_Xfi_Group(cep.xfiGroup);

    //Set ETH PORT Interferface
    ret = AtEthPortInterfaceSet(ethPort0, portInterface); //eth port interface 1 xgmii
    if (ret != cAtOk) {
        printf("ERROR: Cannot set port interface  %d\r\n", portInterface);
        return ret;
    }
    ret = AtEthPortInterfaceSet(ethPort1, portInterface); //eth port interface 1 xgmii
    if (ret != cAtOk) {
        printf("ERROR: Cannot set port interface  %d\r\n", portInterface);
        return ret;
    }
    ret = AtChannelLoopbackSet((AtChannel) ethPort0, loopback); // eth port loopback 1 release
    if (ret != cAtOk) {
        printf("ERROR: Cannot set loopback\r\n");
        return ret;
    }
    ret = AtChannelLoopbackSet((AtChannel) ethPort1, loopback); // eth port loopback 1 release
    if (ret != cAtOk) {
        printf("ERROR: Cannot set loopback\r\n");
        return ret;
    }

    ret = AtEthPortSourceMacAddressSet(ethPort0, DefaultEthPortSourceMac(0)); //eth port srcmac 1 C0.CA.C0.CA.C0.CA
    if (ret != cAtOk) {
        printf("ERROR: Cannot set source mac address \r\n");
        return ret;
    }
    ret = AtEthPortSourceMacAddressSet(ethPort1, DefaultEthPortSourceMac(0)); //eth port srcmac 1 C0.CA.C0.CA.C0.CA
    if (ret != cAtOk) {
        printf("ERROR: Cannot set source mac address \r\n");
        return ret;
    }

    /* After initializing:
     * - At TX direction, there is always a bridge made by hardware */
    //AtAssert(AtEthPortTxBridgedSerdes(ethPort) == rsp1Serdes);
    bSerdes = AtEthPortTxBridgedSerdes(ethPort0);
    if (bSerdes == NULL) {
        printf("ERROR: bSerdes is NULL!!\r\n");
    } else {
    	prverbose("INFO: bSerdes is %d\r\n", AtSerdesControllerIdGet(bSerdes));
    }
    bSerdes = AtEthPortTxBridgedSerdes(ethPort1);
    if (bSerdes == NULL) {
        printf("ERROR: bSerdes is NULL!!\r\n");
    } else {
    	prverbose("INFO: bSerdes is %d\r\n", AtSerdesControllerIdGet(bSerdes));
    }

    sdhModule = (AtModuleSdh) AtDeviceModuleGet(device, cAtModuleSdh);
    if (sdhModule == NULL) {
        printf("WARNING: module SDH does not exist\r\n");
        return cAtErrorNullPointer;
    }

    for (oc48LineId = 0; oc48LineId < numSupportedLine; oc48LineId++)
	{
		//oc48LineId = cep.sdhLineNum;
    	prverbose(".");
		uint16 pwId = oc48LineId;
		AtSdhAu au3;
		AtSdhVc vc3;
		AtPdhDe3 de3;
		AtSdhLine line = AtModuleSdhLineGet(sdhModule, oc48LineId);
		if (line == NULL) {
			printf("WARNING: line %d does not exist\r\n", oc48LineId);
			return cAtErrorNullPointer;
		}

		/* Map to AU3-VC3 */
		ret = AtSdhChannelMapTypeSet((AtSdhChannel) AtSdhLineAug1Get(line, 0), cAtSdhAugMapTypeAug1Map3xVc3s);
		if (ret != cAtOk) {
			printf("ERROR: Cannot map AUG1 to VC3\r\n");
			return ret;
		}

		vc3 = AtSdhLineVc3Get(line, 0, 0);
		if (vc3 == NULL) {
			printf("WARNING: VC3 does not exist\r\n");
			return cAtErrorNullPointer;
		}

		au3 = AtSdhLineAu3Get(line, 0, 0);
		if (au3 == NULL) {
			printf("WARNING: AU3 does not exist\r\n");
			return cAtErrorNullPointer;
		}

		/* Disable SSbit AtEthPortInterfaceSetcheck because we don't know HYPHY modify SSbit or not */
		ret = AtSdhPathSsCompareEnable(au3, cAtFalse); // sdh path ss expect au3.1.1.1 dis 0
		if (ret != cAtOk) {
			printf("ERROR: Cannot disable SSbit check for AU3\r\n");
			return ret;
		}

		ret = AtSdhPathExpectedSsSet(au3, 0);
		if (ret != cAtOk) {
			printf("ERROR: Cannot disable Expected SSbit check for AU3\r\n");
			return ret;
		}

		ret = AtSdhChannelMapTypeSet((AtSdhChannel) vc3, cAtSdhVcMapTypeVc3MapDe3);
		if (ret != cAtOk) {
			printf("ERROR: Cannot map VC3 to DS3\r\n");
			return ret;
		}

		/* Disable SSbit check because we don't know HYPHY modify SSbit or not */
		ret = AtSdhPathSsCompareEnable(au3, cAtFalse); // sdh path ss expect au3.1.1.1 dis 0
		if (ret != cAtOk) {
			printf("ERROR: Cannot disable SSbit check for AU3\r\n");
			return ret;
		}

		ret = AtSdhPathExpectedSsSet(au3, 0);
		if (ret != cAtOk) {
			printf("ERROR: Cannot disable Expected SSbit check for AU3\r\n");
			return ret;
		}

		de3 = AtSdhChannelMapChannelGet((AtSdhChannel) vc3);
		if (de3 == NULL) {
			printf("WARNING: de3 does not exist\r\n");
			return cAtErrorNullPointer;
		}

		ret = AtPdhChannelFrameTypeSet(de3, cAtPdhDs3Unfrm); // pdh de3 framing 1-48 ds3_unframed
		if (ret != cAtOk) {
			printf("Error: Cannot set frame type\n");
			return ret;
		}

		pw = (AtPw) AtModulePwSAToPCreate(pwModule, oc48LineId);  //  pw create satop 1
		if (pw == NULL) {
			printf("WARNING: pw does not exist\r\n");
			return cAtErrorNullPointer;
		}

		ret = AtPwCircuitBind(pw, (AtChannel) de3); // pw circuit bind 1 de3.1
		if (ret != cAtOk) {
			printf("ERROR: Cannot Bind Circuit \r\n");
			return ret;
		}

		if (AtChannelIsEnabled((AtChannel) pw)) {
			AtChannelEnable((AtChannel) pw, cAtFalse);   // pw disable 1
			AtPwEthPortSet(pw, NULL); // pw ethport 1 none
		}

        if(oc48LineId < 2) {
        	prverbose("A: Eth port: %s, ID : %u\r\n", AtObjectToString((AtObject)ethPort0), AtChannelIdGet((AtChannel)ethPort0));
		    ret = AtPwEthPortSet(pw, ethPort0); // pw ethport 1 1
        } else {
        	prverbose("B: Eth port: %s, ID : %u\r\n", AtObjectToString((AtObject)ethPort1), AtChannelIdGet((AtChannel)ethPort1));
		    ret = AtPwEthPortSet(pw, ethPort1); // pw ethport 1 1
        }

		if (ret != cAtOk) {
		    printf("ERROR: Cannot set Ethernet port  \r\n");
		     return ret;
		}

		ret = AtPwRtpEnable(pw, cAtFalse);
		if (ret != cAtOk) {
			printf("ERROR: Cannot Disable RTP  \r\n");
			return ret;
		}
#if 0
		ret = AtPwJitterBufferSizeInPacketSet(pw, 28/*200*/);
		if (ret != cAtOk) {
			printf("ERROR: Cannot set jitterbuffer size \r\n");
			return ret;
		}

		ret = AtPwJitterBufferDelayInPacketSet(pw, 14/*100*/);
		if (ret != cAtOk) {
			printf("ERROR: Cannot set jitterbufferdelay \r\n");
			return ret;
		}
		ret = AtPwLopsSetThresholdSet(pw, AtPwLopsThresholdMin(pw));
		if (ret != cAtOk) {
			printf("ERROR: Cannot set pw lop threshold to %d \r\n", AtPwLopsThresholdMin(pw));
			return ret;
		}

		ret = AtPwPayloadSizeSet(pw, 783/*1024*/);
		if (ret != cAtOk) {
			printf("ERROR: Cannot set pw payload size \r\n");
			return ret;
		}
#endif
		SetupPw(pw);
		AtChannelEnable((AtChannel) pw, cAtTrue);   // pw enable 1
		}
    return ret;
}

eAtRet EnablePw(uint16 ChannelId, eBool enable) {
    eAtRet ret;
    AtPw pw;
    AtPwCounters counters;
    tAtEthPortCounters ethcounters;
    char buffer[100];
    static int printOnetime = 0;

    AtEthPort ethPort = AtModuleEthPortGet(EthModule(DeviceGet()), 0);

    AtModulePw pwModule = (AtModulePw) AtDeviceModuleGet(Device(), cAtModulePw);

    pw = (AtPw) AtModulePwGetPw(pwModule, ChannelId);

    if (enable == cAtTrue) {
        if (AtChannelAllCountersClear((AtChannel) pw, &counters)) {
            err('f', 0, "%s: can not clear counters for  pw : %d", __FUNCTION__, ChannelId);
            return (FAILED);
        }
        __asm__("sync");
        if (AtChannelAllCountersClear((AtChannel) ethPort, &ethcounters)) {
            err('f', 0, "%s: can not clear counters for  pw : %d", __FUNCTION__, ChannelId);
            return (FAILED);
        }
        if (printOnetime) {
            printOnetime = 0;
            sprintf(buffer, "show eth port counters 1 r2c");
            AtCliExecute(buffer);
            sprintf(buffer, "show pw counters %d r2c", (ChannelId + 1));
            AtCliExecute(buffer);
            sprintf(buffer, "show pw %d ", (ChannelId + 1));
            AtCliExecute(buffer);
            sprintf(buffer, "show pw jitterbuffer  %d ", (ChannelId + 1));
            AtCliExecute(buffer);
        }

        ret = AtChannelEnable((AtChannel) pw, cAtTrue);   // pw enable 1
        __asm__("sync");

    } else {
        ret = AtChannelEnable((AtChannel) pw, cAtFalse);
    }
    return ret;
}

eAtRet VzAtSetJitrDelay(uint16 ChannelId, uint16 numPackets) {
    eAtRet ret;
    AtPw pw;
    AtModulePw pwModule = (AtModulePw) AtDeviceModuleGet(Device(), cAtModulePw);

    pw = (AtPw) AtModulePwGetPw(pwModule, ChannelId);

    ret = AtPwJitterBufferDelayInPacketSet(pw, numPackets);

    return ret;
}

eAtRet EnableLiuSAToPMplsPwChannel(uint16 ChannelId) {

    AtPw pw;
    AtModulePw pwModule = (AtModulePw) AtDeviceModuleGet(Device(), cAtModulePw);

    pw = (AtPw) AtModulePwGetPw(pwModule, ChannelId);

    AtChannelEnable((AtChannel) pw, cAtTrue);

    return cAtOk;
}

eAtRet DisableLiuSAToPMplsPwChannel(uint16 ChannelId) {

    AtPw pw;
    AtModulePw pwModule = (AtModulePw) AtDeviceModuleGet(Device(), cAtModulePw);

    pw = (AtPw) AtModulePwGetPw(pwModule, ChannelId);

    AtChannelEnable((AtChannel) pw, cAtFalse);

    return cAtOk;
}

eAtRet SetupCepUdp(uint16 sdhLineId, uint16 ethPortId) {

    uint16 aug4Id;
    uint16 aug1Id;
    AtPwPsn psn;
    AtDevice device = DeviceGet();
    AtModulePw pwModule = PwModule(device);
    AtEthPort ethPort = AtModuleEthPortGet(EthModule(device), ethPortId);
    AtSdhLine line = AtModuleSdhLineGet(SdhModule(device), sdhLineId);
    AtSdhAug aug16 = AtSdhLineAug16Get(line, 0);

    mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel )(aug16), cAtSdhAugMapTypeAug16Map4xAug4s));

    /* Make AUG-4 contains AUG-1s */
    for (aug4Id = 0; aug4Id < cNumAug4InAug16; aug4Id++) {
        AtSdhAug aug4 = AtSdhLineAug4Get(line, aug4Id);
        mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel )(aug4), cAtSdhAugMapTypeAug4Map4xAug1s));
    }

    /* Make each AUG-1 contains AU-3 <--> VC-3 */
    for (aug1Id = 0; aug1Id < cNumAug1InAug16; aug1Id++) {
        uint16 vc3Id;
        AtSdhAug aug1 = AtSdhLineAug1Get(line, aug1Id);
        mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel )(aug1), cAtSdhAugMapTypeAug1Map3xVc3s));

        /* Make each VC-3 contains TUG-2s */
        for (vc3Id = 0; vc3Id < cNumVc3InAug1; vc3Id++) {
            AtSdhVc vc3 = AtSdhLineVc3Get(line, aug1Id, vc3Id);
            uint16 tug2Id;
            mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel )(vc3), cAtSdhVcMapTypeVc3Map7xTug2s));

            /* Make each TUG-2 contains Tu12s */
            for (tug2Id = 0; tug2Id < cNumTug2InVc3; tug2Id++) {
                uint16 pwId;
                uint16 vc1xId;
                AtSdhTug tug2 = AtSdhLineTug2Get(line, aug1Id, vc3Id, tug2Id);
                mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel )(tug2), cAtSdhTugMapTypeTug2Map3xTu12s));

                /* For each VC12, emulate over PSN */
                for (vc1xId = 0; vc1xId < cNumVc12InTug2; vc1xId++) {
                    AtPw pw;
                    AtSdhVc vc12 = AtSdhLineVc1xGet(line, aug1Id, vc3Id, tug2Id, vc1xId);
                    mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel )(vc12), cAtSdhVcMapTypeVc1xMapC1x));

                    pwId = PwIdFromSdhChannelId(aug1Id, vc3Id, tug2Id, vc1xId, sdhLineId);
                    pw = (AtPw) AtModulePwCepCreate(pwModule, pwId, cAtPwCepModeBasic);

                    mSuccessAssert(AtPwPayloadSizeSet(pw, DefaultPwPayloadSize(pwId)));
                    mSuccessAssert(AtPwCircuitBind(pw, (AtChannel )vc12));
                    mSuccessAssert(AtPwEthPortSet(pw, ethPort));

                    /* Configure Ethernet header with default MAC and no VLAN */
                    mSuccessAssert(AtPwEthHeaderSet(pw, DefaultPwDestMac(ethPortId), NULL, NULL));

                    /* Create UDP over IPv4 PSN */
                    psn = UdpIpv4PsnCreate(pwId, ethPortId);
                    mSuccessAssert(AtPwPsnSet(pw, psn));
                    /* Masking enabling channel as we wan to give control to use */
                    /* mSuccessAssert(AtChannelEnable((AtChannel)(pw), cAtTrue));*/

                    /* Clean objects */
                    AtObjectDelete((AtObject) (psn));
                }
            }
        }
    }

    /* Set Source MAC and IP for Ethernet Port */
    mSuccessAssert(AtEthPortSourceMacAddressSet(ethPort, DefaultEthPortSourceMac(ethPortId)));
    mSuccessAssert(AtEthPortIpV4AddressSet(ethPort, DefaultEthPortSourceIpv4(ethPortId)));
    mSuccessAssert(AtChannelLoopbackSet((AtChannel )(ethPort), cAtLoopbackModeRelease));

    return cAtOk;
}

eAtRet SetupCepMpls(uint16 sdhLineId, uint16 ethPortId) {

    uint16 aug4Id;
    uint16 aug1Id;
    AtPwPsn psn;
    AtDevice device = DeviceGet();
    AtModulePw pwModule = PwModule(device);
    AtEthPort ethPort = AtModuleEthPortGet(EthModule(device), ethPortId);
    AtSdhLine line = AtModuleSdhLineGet(SdhModule(device), sdhLineId);
    AtSdhAug aug16 = AtSdhLineAug16Get(line, 0);

    mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel )(aug16), cAtSdhAugMapTypeAug16Map4xAug4s));

    /* Make AUG-4 contains AUG-1s */
    for (aug4Id = 0; aug4Id < cNumAug4InAug16; aug4Id++) {
        AtSdhAug aug4 = AtSdhLineAug4Get(line, aug4Id);
        mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel )(aug4), cAtSdhAugMapTypeAug4Map4xAug1s));
    }

    /* Make each AUG-1 contains AU-3 <--> VC-3 */
    for (aug1Id = 0; aug1Id < cNumAug1InAug16; aug1Id++) {
        uint16 vc3Id;
        AtSdhAug aug1 = AtSdhLineAug1Get(line, aug1Id);
        mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel )(aug1), cAtSdhAugMapTypeAug1Map3xVc3s));

        /* Make each VC-3 contains TUG-2s */
        for (vc3Id = 0; vc3Id < cNumVc3InAug1; vc3Id++) {
            AtSdhVc vc3 = AtSdhLineVc3Get(line, aug1Id, vc3Id);
            uint16 tug2Id;
            mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel )(vc3), cAtSdhVcMapTypeVc3Map7xTug2s));

            /* Make each TUG-2 contains Tu12s */
            for (tug2Id = 0; tug2Id < cNumTug2InVc3; tug2Id++) {
                uint16 pwId;
                uint16 vc1xId;
                AtSdhTug tug2 = AtSdhLineTug2Get(line, aug1Id, vc3Id, tug2Id);
                mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel )(tug2), cAtSdhTugMapTypeTug2Map3xTu12s));

                /* For each VC12, emulate over PSN */
                for (vc1xId = 0; vc1xId < cNumVc12InTug2; vc1xId++) {
                    AtPw pw;
                    AtSdhVc vc12 = AtSdhLineVc1xGet(line, aug1Id, vc3Id, tug2Id, vc1xId);
                    mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel )(vc12), cAtSdhVcMapTypeVc1xMapC1x));

                    pwId = PwIdFromSdhChannelId(aug1Id, vc3Id, tug2Id, vc1xId, sdhLineId);
                    pw = (AtPw) AtModulePwCepCreate(pwModule, pwId, cAtPwCepModeBasic);

                    mSuccessAssert(AtPwPayloadSizeSet(pw, DefaultPwPayloadSize(pwId)));
                    mSuccessAssert(AtPwCircuitBind(pw, (AtChannel )vc12));
                    mSuccessAssert(AtPwEthPortSet(pw, ethPort));

                    /* Configure Ethernet header with default MAC and no VLAN */
                    mSuccessAssert(AtPwEthHeaderSet(pw, DefaultPwDestMac(ethPortId), NULL, NULL));

                    /* Create UDP over IPv4 PSN */
                    psn = MplsPsnCreate(pwId, ethPortId);
                    mSuccessAssert(AtPwPsnSet(pw, psn));
                    /* Masking enabling channel as we wan to give control to use */
                    /* mSuccessAssert(AtChannelEnable((AtChannel)(pw), cAtTrue));*/

                    /* Clean objects */
                    AtObjectDelete((AtObject) (psn));
                }
            }
        }
    }

    /* Set Source MAC and IP for Ethernet Port */
    mSuccessAssert(AtEthPortSourceMacAddressSet(ethPort, DefaultEthPortSourceMac(ethPortId)));
    mSuccessAssert(AtEthPortIpV4AddressSet(ethPort, DefaultEthPortSourceIpv4(ethPortId)));
    mSuccessAssert(AtChannelLoopbackSet((AtChannel )(ethPort), cAtLoopbackModeRelease));

    return cAtOk;

}

static char *AlarmString(uint32 alarm) {
    static char buf[32];

    if (alarm == 0) {
        AtSprintf(buf, "none");
        return buf;
    }

    AtOsalMemInit(buf, 0, sizeof(buf));
    if (alarm & cAtPrbsEngineAlarmTypeError) {
        AtStrcat(buf, "|");
        AtStrcat(buf, "error");
    }
    if (alarm & cAtPrbsEngineAlarmTypeLossSync) {
        AtStrcat(buf, "|");
        AtStrcat(buf, "lossSync");
    }

    return &buf[1]; /* Get rid of the first '|' */
}

void Tfi5Prbs(AtSdhLine line, eAtPrbsMode mode) {
    uint32 alarm;
    AtSerdesController serdesController = AtSdhLineSerdesController(line);
    AtPrbsEngine prbsEngine = AtSerdesControllerPrbsEngine(serdesController);

    /* Configure the PRBS mode */
    AtAssert(AtPrbsEngineTxModeSet(prbsEngine, mode) == cAtOk);
    AtAssert(AtPrbsEngineTxModeGet(prbsEngine) == mode);
    AtAssert(AtPrbsEngineRxModeSet(prbsEngine, mode) == cAtOk);
    AtAssert(AtPrbsEngineRxModeGet(prbsEngine) == mode);

    /* Then enable the prbsEngine */
    AtAssert(AtPrbsEngineRxEnable(prbsEngine, cAtTrue));
    AtAssert(AtPrbsEngineRxIsEnabled(prbsEngine) == cAtTrue);
    AtAssert(AtPrbsEngineTxEnable(prbsEngine, cAtTrue));
    AtAssert(AtPrbsEngineTxIsEnabled(prbsEngine) == cAtTrue);

    //alarm = StickyGet(prbsEngine, 1);
    sleep(1);

    alarm = AtPrbsEngineAlarmGet(prbsEngine);
    if (alarm) {
        printf("\nError : PRBS Error\n");
        AllCountersShow(prbsEngine, 1); /* read2clear*/
    }

    /* Then disable the prbsEngine */
    AtAssert(AtPrbsEngineRxEnable(prbsEngine, cAtFalse));
    AtAssert(AtPrbsEngineRxIsEnabled(prbsEngine) == cAtFalse);
    AtAssert(AtPrbsEngineTxEnable(prbsEngine, cAtFalse));
    AtAssert(AtPrbsEngineTxIsEnabled(prbsEngine) == cAtFalse);

}

eAtRet De1Prbs() {
    uint32 sticky;
    uint32 alarm;
    uint32 line;
    //uint32 engineId;
    eAtRet ret = cAtOk;
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);
    AtDevice device = DeviceGet();
    AtModulePdh pdhModule = (AtModulePdh) AtDeviceModuleGet(device, cAtModulePdh);
    AtChannel channel;
    AtPrbsEngine engine_0;

    for (line = 0; line < 48; line++) {
        channel = (AtChannel) AtModulePdhDe1Get(pdhModule, line);
        engine_0 = AtPdhChannelLinePrbsEngineGet((AtPdhChannel) channel);

        /* Configure the PRBS mode */
        if (AtPrbsEngineTxModeSet(engine_0, cAtPrbsModePrbs23) == cAtOk) {
            printf("Error TxModeSet");
        }
        if (AtPrbsEngineTxModeGet(engine_0) == cAtPrbsModePrbs23) {
            printf("Error TxModeGet");
        }
        if (AtPrbsEngineRxModeSet(engine_0, cAtPrbsModePrbs23) == cAtOk) {
            printf("Error TxModeSet");
        }
        if (AtPrbsEngineRxModeGet(engine_0) == cAtPrbsModePrbs23) {
            printf("Error TxModeGet");
        }

        ret = AtPrbsEngineEnable(engine_0, cAtTrue);
        if (ret != cAtOk) {
            printf("Error not able to generate PRBS\n");
        }

        debug_print("Rx Enable status %d\n", AtPrbsEngineRxIsEnabled(engine_0));
        debug_print("Tx Enable status %d\n", AtPrbsEngineTxIsEnabled(engine_0));
    }

    if (ret == cAtOk) {
        sleep(3);
        for (line = 0; line < 48; line++) {
            channel = (AtChannel) AtModulePdhDe1Get(pdhModule, line);
            engine_0 = AtPdhChannelLinePrbsEngineGet((AtPdhChannel) channel);

            alarm = AtPrbsEngineAlarmGet(engine_0);
            sticky = AtPrbsEngineAlarmHistoryClear(engine_0);
            debug_print("Alarm = %d, Sticky = %d\n", alarm, sticky);
            if (sticky) {
                debug_print("\n Clearing PRBS sticky for line %d\n", line);
            }
        }

        sleep(3);

        for (line = 0; line < 48; line++) {
            channel = (AtChannel) AtModulePdhDe1Get(pdhModule, line);
            engine_0 = AtPdhChannelLinePrbsEngineGet((AtPdhChannel) channel);

            alarm = AtPrbsEngineAlarmGet(engine_0);
            sticky = AtPrbsEngineAlarmHistoryClear(engine_0);
            debug_print("Alarm = %d, Sticky = %d\n", alarm, sticky);
            if (sticky) {
                printf("\n Error : PRBS sticky set in line %d \n", line);
            }

            ret = AtPrbsEngineEnable(engine_0, cAtFalse);
            if (ret != cAtOk)
                printf("Error: not able to stop PRBS\n");
        }
    }
    return ret;
}

/*
 cAtLoopbackModeRelease, /**< Release loopback * /
 cAtLoopbackModeLocal,   /**< Local loopback * /
 cAtLoopbackModeRemote   /**< Remote loopback */

void atTf5LineLoopback(uint16 sdhLineId, eAtLoopbackMode lpbkMode) {
    AtDevice device = DeviceGet();
    AtModulePw pwModule = PwModule(device);
    AtSdhLine line = AtModuleSdhLineGet(SdhModule(device), sdhLineId);

    AtChannel lineChannel = (AtChannel) line;
    AtSerdesController serdesController = AtSdhLineSerdesController(line);

    /* The following lines show how to perform each loopback mode on line */
    AtAssert(AtChannelLoopbackSet(lineChannel, lpbkMode) == cAtOk);
    AtAssert(AtChannelLoopbackGet(lineChannel) == lpbkMode);

    /* And the following lines show how to perform each loopback mode on line's SERDES */
    AtAssert(AtSerdesControllerLoopbackSet(serdesController, lpbkMode) == cAtOk);
    AtAssert(AtSerdesControllerLoopbackGet(serdesController) == lpbkMode);
}

/*
 cAtLoopbackModeRelease, /**< Release loopback * /
 cAtLoopbackModeLocal,   /**< Local loopback * /
 cAtLoopbackModeRemote   /**< Remote loopback */

void atEthLoopback(uint8 ethPortId, eAtLoopbackMode lpbkMode) {
    AtDevice device = DeviceGet();
    AtModulePw pwModule = PwModule(device);
    AtEthPort ethPort = AtModuleEthPortGet(EthModule(device), ethPortId);

    AtSerdesController serdesController = AtEthPortSerdesController(ethPort);

    /* The following lines show how to perform each loopback mode on line */
    /*
     AtAssert(AtChannelLoopbackSet(lineChannel, lpbkMode)   == cAtOk);
     AtAssert(AtChannelLoopbackSet(lineChannel, cAtLoopbackModeRemote)  == cAtOk);
     AtAssert(AtChannelLoopbackSet(lineChannel, cAtLoopbackModeRelease) == cAtOk);
     */

    /* And the following lines show how to perform each loopback mode on line's SERDES */
    AtAssert(AtSerdesControllerLoopbackSet(serdesController, cAtLoopbackModeLocal) == cAtOk);
    AtAssert(AtSerdesControllerLoopbackSet(serdesController, cAtLoopbackModeRemote) == cAtOk);
    AtAssert(AtSerdesControllerLoopbackSet(serdesController, cAtLoopbackModeRelease) == cAtOk);
}

int atAllDdrsTest(void) {
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);

    AtDevice device = DeviceGet();
    AtModuleRam ramModule = (AtModuleRam) AtDeviceModuleGet(device, cAtModuleRam);
    uint8 ddr_i;
    int retCode = PASSED;
    eAtRet ret;

    AtDeviceDiagnosticModeEnable(device, cAtTrue);
    debug_print("\n%s %d  AtModuleRamNumDdrGet : %d\n", __FUNCTION__, __LINE__,
            AtModuleRamNumDdrGet(ramModule));

    /* Test all of DDRs */
    for (ddr_i = 0; ddr_i < AtModuleRamNumDdrGet(ramModule); ddr_i++) {
        debug_print("\n%s %d\n", __FUNCTION__, __LINE__);
        static const uint32 cAnyAddress = 0x2345;
        static const uint32 cStartAddress = 0;
        AtRam ram = AtModuleRamDdrGet(ramModule, ddr_i);
        uint32 endAddress = AtRamMaxAddressGet(ram);
        uint32 firstErrorAddress;
        if (AtRamAddressBusTest(ram) != cAtOk)
            err('f', 0, "AtRamAddressBusTest Failed");
        if (AtRamDataBusTest(ram, cAnyAddress) != cAtOk)
            err('f', 0, "AtRamDataBusTest Failed");
        /* Memory testing is longer and the result may need to be examined */
        ret = AtRamMemoryTest(ram, cStartAddress, endAddress, &firstErrorAddress);
        if (ret != cAtOk) {
            retCode = FAILED;
            AtPrintf("memory : %d, First error address: 0x%08x\r\n", ddr_i, firstErrorAddress);
        }
    }
    if (ret != cAtOk)
        return (FAILED);
    else
        printf("\nDDR BIST Passed\r\n");

    AtDeviceDiagnosticModeEnable(device, cAtFalse);


    ret = AtDeviceInit(device);  // device init
    if (ret != cAtOk) {
        printf("\nERROR: Device Init Failed err code : %d  \r\n", ret);
        retCode = FAILED;
    }

    sleep(1);


    return (retCode);
}

int atAllQdrTest(void) {
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);

    AtDevice device = DeviceGet();
    AtModuleRam ramModule = (AtModuleRam) AtDeviceModuleGet(device, cAtModuleRam);
    uint8 qdr_i;
    int retCode = PASSED;
    eAtRet ret = cAtOk;

    AtDeviceDiagnosticModeEnable(device, cAtTrue);
    prverbose("\n%s %d  AtModuleRamNumQdrGet : %d\n", __FUNCTION__, __LINE__,
            AtModuleRamNumQdrGet(ramModule));

    if (AtModuleRamNumQdrGet(ramModule) == 0) {
        printf("\nThis test is not valid for DS3/DS1 ..skipped\n");
        AtDeviceDiagnosticModeEnable(device, cAtFalse);
        return (retCode);
    }

    /* Test all of QDRs */
    for (qdr_i = 0; qdr_i < AtModuleRamNumQdrGet(ramModule); qdr_i++) {
        debug_print("\n%s %d\n", __FUNCTION__, __LINE__);
        static const uint32 cAnyAddress = 0x2345;
        static const uint32 cStartAddress = 0;
        eAtRet ret;
        AtRam ram = AtModuleRamQdrGet(ramModule, qdr_i);
        uint32 endAddress = AtRamMaxAddressGet(ram);
        uint32 firstErrorAddress;
        if (AtRamAddressBusTest(ram) != cAtOk)
            err('f', 0, "AtRamAddressBusTest Failed");
        if (AtRamDataBusTest(ram, cAnyAddress) != cAtOk)
            err('f', 0, "AtRamDataBusTest Failed");
        /* Memory testing is longer and the result may need to be examined */
        ret = AtRamMemoryTest(ram, cStartAddress, endAddress, &firstErrorAddress);
        if (ret != cAtOk) {
            AtPrintf("First error address: 0x%08x\r\n", firstErrorAddress);
            retCode = FAILED;
        }
    }

    if (ret != cAtOk)
        return (FAILED);
    else
        printf("\nQDR BIST Passed\r\n");

    AtDeviceDiagnosticModeEnable(device, cAtFalse);

    ret = AtDeviceInit(device);  // device init
    if (ret != cAtOk) {
        printf("\nERROR: Device Init Failed err code : %d  \r\n", ret);
        retCode = FAILED;
    }

    sleep(1);

    return (retCode);
}

static uint8 sliceOfPw(uint32 pwId) {
    AtModulePw pwModule = (AtModulePw) AtDeviceModuleGet(CliDevice(), cAtModulePw);
    AtPw pw = AtModulePwGetPw(pwModule, (uint16) (pwId));
    AtSdhChannel sdhChannel = NULL;
    AtPdhChannel pdhChannel = NULL;
    uint8 slice, sts;

    if (AtPwTypeGet(pw) == cAtPwTypeCEP)
        sdhChannel = (AtSdhChannel) AtPwBoundCircuitGet(pw);

    else if (AtPwTypeGet(pw) == cAtPwTypeSAToP)
        pdhChannel = (AtPdhChannel) AtPwBoundCircuitGet(pw);

    else if (AtPwTypeGet(pw) == cAtPwTypeCESoP)
        pdhChannel = (AtPdhChannel) AtPdhNxDS0De1Get((AtPdhNxDS0) AtPwBoundCircuitGet(pw));

    if (sdhChannel) {
        ThaSdhChannel2HwMasterStsId(sdhChannel, cAtModuleSdh, &slice, &sts);
    } else if (pdhChannel) {
        ThaPdhChannelHwIdGet(pdhChannel, cAtModulePdh, &slice, &sts);
    } else
        return 0;

    return (uint8) (slice + 1);
}

static uint32 PwPrbsMonAddress(uint32 slice) {
    AtDevice device = DeviceGet();
    uint32 productCode = AtDeviceProductCodeGet(device);
    if ((productCode & cBit23_0) == (0x60210031 & cBit23_0))
        return (0x247800UL + (uint32) (slice - 1) * 0x8000UL);
    else
        return (0x246800UL + (uint32) (slice - 1) * 0x1000UL);
}

static uint32 PwPrbsGenAddress(uint32 slice) {
    AtDevice device = DeviceGet();
    uint32 productCode = AtDeviceProductCodeGet(device);
    if ((productCode & cBit23_0) == (0x60210031 & cBit23_0))
        return (0x247000UL + (uint32) (slice - 1) * 0x8000UL);
    else
        return (0x246400UL + (uint32) (slice - 1) * 0x1000UL);
}

int PwPrbsEnable(int enable) {
    uint32 numPws;
    uint32 pw_i;
    uint8 slice;
    AtDevice device = DeviceGet();
    AtHal hal = AtIpCoreHalGet(AtDeviceIpCoreGet(device, 0));
    AtModulePw pwModule;

    /* Get the shared ID buffer */
    //uint32* idBuf = CliSharedIdBufferGet(&bufferSize);

    pwModule = (AtModulePw) AtDeviceModuleGet(CliDevice(), cAtModulePw);
    for (pw_i = 0; pw_i < 48; pw_i++) {
        AtPw pw = AtModulePwGetPw(pwModule, (uint16) (pw_i));
        uint32 address;

        slice = sliceOfPw(pw_i);
        if (slice == 0)
            continue;

        address = PwPrbsGenAddress(slice) + pw_i;
        AtHalWrite(hal, address, (enable) ? 1 : 0);

        if (AtPwTypeGet(pw) == cAtPwTypeCEP) {
            AtSdhChannel sdhChannel = (AtSdhChannel) AtPwBoundCircuitGet(pw);
            ThaOcnVtPohInsertEnable(sdhChannel, AtSdhChannelSts1Get(sdhChannel),
                    AtSdhChannelTug2Get(sdhChannel), AtSdhChannelTu1xGet(sdhChannel), enable);
        }
    }

    DisplayAndClearCounters(1, 0, 47, 0);

    return (PASSED);
}

// Set the stickyClear to clear all the status
int PwPrbsCheck(int stickyClear) {
    uint32 numPws;
    uint32 pw_i;
    uint8 slice;
    AtDevice device = DeviceGet();
    AtHal hal = AtIpCoreHalGet(AtDeviceIpCoreGet(device, 0));
    eBool success = cAtTrue;
    eBool prbsIsRunning;

    uint8 slotNumber = (uint8) im_get_current_slot_no();

    for (pw_i = 0; pw_i < 48; pw_i++) {
        uint32 firstTimeVal, secondTimeVal;
        uint32 address;
        uint8 checkTime;

        slice = sliceOfPw(pw_i);
        if (slice == 0)
            continue;

        address = PwPrbsMonAddress(slice) + pw_i;

        /* Check if prbs is running */
        prbsIsRunning = cAtFalse;
        firstTimeVal = AtHalRead(hal, address);
        for (checkTime = 0; checkTime < 10; checkTime++) {
            AtOsalUSleep(100);
            secondTimeVal = AtHalRead(hal, address);

            if (firstTimeVal != secondTimeVal) {
                prbsIsRunning = cAtTrue;
                break;
            }
        }

        if (!prbsIsRunning) {
            if (!stickyClear) {
                printf("\r\nPrbs for pw %u is stop, address 0x%08x\r\n", pw_i, address);
                DisplayAndClearCounters(0, pw_i, pw_i, 1);
                checkSerdesPll();
            }
            success = cAtFalse;
            continue;
        }

        if (((firstTimeVal & cBit17_16) >> 16) != 1) {
            if (!stickyClear) {
                printf("\r\nPrbs for pw %u is fail: read 0x%08x = 0x%08x\r\n", pw_i, address, firstTimeVal);
                DisplayAndClearCounters(0, pw_i, pw_i, 1);
                checkSerdesPll();
            }
            success = cAtFalse;
            continue;
        }

        AtFileFlush(AtStdStandardOutput(AtStdSharedStdGet()));
    }

    if (success) {
        if (!stickyClear)
            debug_print("\r\nPrbs for all pws are good\r\n");
        return (PASSED);
    }

    if (!stickyClear) {
        if (im_type[slotNumber] == IM_DS1)
            AtCliExecute("show pdh de1 alarm 1-48");
        else if (im_type[slotNumber] == IM_DS3)
            AtCliExecute("show pdh de3 alarm 1-48");
    }
    return FAILED;
}

eAtRet ethPrbs() {
    uint32 sticky;
    uint32 alarm;
    uint32 line;
    eAtRet ret = cAtOk;
    AtPrbsEngine engine_0;

    AtDevice device = DeviceGet();

    /* ARAD remote loopback must be set before we execute following sequence */
    // if RSP in slot 0
    if (rsp2_get_slot_no() == RSP_0) {

        for (line = 0; line < 4; line++) {
            config_arad_remote_lpbk(line, TRUE);

            engine_0 = AtSerdesControllerPrbsEngine(AtModuleEthSerdesController(EthModule(device), line));

            ret = AtPrbsEngineEnable(engine_0, cAtTrue);
            if (ret != cAtOk) {
                printf("Error not able to generate PRBS\n");
            }

            debug_print("Rx Enable status %d\n", AtPrbsEngineRxIsEnabled(engine_0));
            debug_print("Tx Enable status %d\n", AtPrbsEngineTxIsEnabled(engine_0));

            AtPrbsEngineErrorForce(engine_0, cAtTrue);

            sticky = AtPrbsEngineAlarmHistoryClear(engine_0);
            msleep(1);
            sticky = AtPrbsEngineAlarmHistoryClear(engine_0);
            debug_print("%s %d Alarm = %d, Sticky = %d\n", __FUNCTION__, __LINE__, alarm, sticky);
            if (!sticky) {
                ret = cAtError;
                printf("\n Error: Force error on PRBS Failed to set for line %d\n", line);
            }

            AtPrbsEngineErrorForce(engine_0, cAtFalse);

            sticky = AtPrbsEngineAlarmHistoryClear(engine_0);
            msleep(1);
            sticky = AtPrbsEngineAlarmHistoryClear(engine_0);
            if (sticky) {
                ret = cAtError;
                printf("\n Error: Force error on PRBS Failed to clear for line %d\n", line);
            }
        }
    } else {
        for (line = 0; line < 4; line++) {
            config_arad_remote_lpbk(line, TRUE);

            engine_0 = AtSerdesControllerPrbsEngine(AtModuleEthSerdesController(EthModule(device), line));

            ret = AtPrbsEngineEnable(engine_0, cAtTrue);
            if (ret != cAtOk) {
                printf("Error not able to generate PRBS\n");
            }

            debug_print("Rx Enable status %d\n", AtPrbsEngineRxIsEnabled(engine_0));
            debug_print("Tx Enable status %d\n", AtPrbsEngineTxIsEnabled(engine_0));

            AtPrbsEngineErrorForce(engine_0, cAtTrue);

            sticky = AtPrbsEngineAlarmHistoryClear(engine_0);
            msleep(1);
            sticky = AtPrbsEngineAlarmHistoryClear(engine_0);
            debug_print("%s %d Alarm = %d, Sticky = %d\n", __FUNCTION__, __LINE__, alarm, sticky);
            if (!sticky) {
                ret = cAtError;
                printf("\n Error: Force error on PRBS Failed to set for line %d\n", line);
            }

            AtPrbsEngineErrorForce(engine_0, cAtFalse);

            sticky = AtPrbsEngineAlarmHistoryClear(engine_0);
            msleep(1);
            sticky = AtPrbsEngineAlarmHistoryClear(engine_0);
            if (sticky) {
                ret = cAtError;
                printf("\n Error: Force error on PRBS Failed to clear for line %d\n", line);
            }
        }

    }

    return ret;

}

/*--------------------------- Implementation ---------------------------------*/
static CiscoIm ImAtSlot(uint8 slotIndex) {
    return &(m_ims[slotIndex]);
}

AtDevice DeviceAtSlot(uint8 slotId) {
    return ImAtSlot(slotId)->device;
}

#if 0
static void PCIeWriteFunc(uint32 realAddress, uint32 value)
{
    volatile unsigned int *address;
    address = (volatile unsigned int *) (realAddress);
    *address = value;
    (void) (*address);
}

static uint32 PCIeReadFunc(uint32 realAddress)
{
    volatile unsigned int *address;
    address = (volatile unsigned int *) (realAddress);
    return (*address);
}
#endif

static AtTextUI TinyTextUI(void) {
    if (m_textUI == NULL)
        m_textUI = AtDefaultTinyTextUINew();
    return m_textUI;
}

static eAtRet DriverInit(void) {
    AtDriverCreate(cMaxNumSlots, AtOsalLinux());

    if (AtDriverSharedDriverGet() == NULL)
        AtPrintc(cSevCritical, "ERROR: Cannot create driver\r\n");

    AtCliSharedTextUISet(TinyTextUI());

    return cAtOk;
}

static eAtRet DeviceCleanUp(AtDevice device) {
    AtHal hal;

    if (device == NULL)
        return cAtOk;

    hal = AtDeviceIpCoreHalGet(device, 0);
    AtDriverDeviceDelete(AtDriverSharedDriverGet(), device);
    AtHalDelete(hal);

    return cAtOk;
}

static eAtRet DriverCleanup(AtDriver driver) {
    uint8 numDevices, i;

    if (driver == NULL)
        return cAtOk;

    /* Delete all devices */
    AtDriverAddedDevicesGet(driver, &numDevices);
    for (i = 0; i < numDevices; i++) {
        AtDevice device = AtDriverDeviceGet(driver, 0);
        DeviceCleanUp(device);
    }

    /* Delete driver then delete all saved HALs */
    AtDriverDelete(driver);

    return cAtOk;
}

int arrive_fpag_init(void) {
    eAtRet ret;

    /* Initialize driver */
    ret = DriverInit();
    if (ret != cAtOk) {
        AtPrintc(cSevCritical, "Cannot initialize driver, ret = %s\r\n", AtRet2String(ret));
        return FAILED;
    }

    return PASSED;
}

int arrive_fpag_exit(void) {
    DriverCleanup(AtDriverSharedDriverGet());
    AtTextUIDelete(TinyTextUI());
    return PASSED;
}

int arrive_fpag_invoke_cli(void) {
    uint8 slotNumber = (uint8) im_get_current_slot_no();

    if (slotNumber >= cMaxNumSlots) {
        AtPrintc(cSevCritical, "ERROR: Slot number must be from 0 to %u\r\n", cMaxNumSlots - 1);
        return FAILED;
    }

    EnterSlot(slotNumber);
    AtCliStart();

    return PASSED;
}

int setup_cep(tCep cep) {

    eAtRet ret;

    if (cep.psnType == cPsnUdpIp) {
        ret = SetupCepUdp(cep.sdhLineNum, 1);
    } else if (cep.psnType == cPsnMpls) {
        ret = SetupCepMpls(cep.sdhLineNum, 1);
    } else {
        printf("\n Cep setup failed : Invalid PSN type : %d \n", (uint32) cep.psnType);
    }

    if (ret != cAtOk) {
        printf("\n Cep udp setup failed : error code : %d \n", (uint32) ret);
        return FAILED;
    }

    return PASSED;
}

int setup_liu_de1(tLiuSatopDe1 de1) {
    eAtRet ret = cAtOk;
    uint32 data;
    uint8 slotNumber = (uint8) im_get_current_slot_no();

    im_fpga_reg_read(slotNumber, IM_DATA_FPGA_CTL_STATUS, &data);
    if (!(data & IM_DATA_PROG_DONE)) {
        printf("CEM FPGA Not Programmed");
        return FAILED;
    }

    if (slotNumber >= cMaxNumSlots) {
        AtPrintc(cSevCritical, "ERROR: Slot number must be from 0 to %u\r\n", cMaxNumSlots - 1);
        return FAILED;
    }

    EnterSlot(slotNumber);

    if (de1.psnType == cPsnUdpIp) {
        ret = SetupLiuSAToPMplsDe1(de1); // TODO: Add UdpIP API
    } else if (de1.psnType == cPsnMpls) {
        ret = SetupLiuSAToPMplsDe1(de1);
    } else {
        printf("\n CES DE1  setup failed : Invalid PSN type : %d \n", (uint32) de1.psnType);
    }

    if (ret != cAtOk) {
        printf("\n CES DE1  setup failed : error code : %d \n", (uint32) ret);
        return FAILED;
    }

    return PASSED;
}

int setup_liu_de3(tLiuSatopDe3 de3) {

    eAtRet ret;
    uint32 data;
    uint8 slotNumber = (uint8) im_get_current_slot_no();

    im_fpga_reg_read(slotNumber, IM_DATA_FPGA_CTL_STATUS, &data);
    if (!(data & IM_DATA_PROG_DONE)) {
        printf("CEM FPGA Not Programmed");
        return FAILED;
    }

    EnterSlot(slotNumber);

    if (de3.psnType == cPsnUdpIp) {
        ret = SetupLiuSAToPMplsDe3(de3);
    } else if (de3.psnType == cPsnMpls) {
        ret = SetupLiuSAToPMplsDe3(de3);
    } else {
        printf("\n CES DE3  setup failed : Invalid PSN type : %d \n", (uint32) de3.psnType);
    }

    if (ret != cAtOk) {
        printf("\n CES DE3  setup failed : error code : %d \n", (uint32) ret);
        return FAILED;
    }

    return PASSED;
}

int diag_cem_fpga_device_id_test(int show_menu) {
    if (show_menu == FOX_TREE_MAGIC_WORD) {
        return (PASSED);
    }

    return (PASSED);
}

uint32 setup_liu_default(int loopback) {
    tLiuSatopDe1 de1 = { 0, cAtPdhDs1J1UnFrm, cPsnMpls, cAtEthPortInterfaceXGMii, cAtLoopbackModeRelease,
            cAtPdhLoopbackModeRelease };

    if (loopback == 1)
        de1.pdhLoopbackMode = cAtPdhLoopbackModeRemoteLine;

    printf("API based testing\n");
    if (setup_liu_de1(de1) == FAILED) {
        printf("CEM setup failed\n");
        return (FAILED);
    }
    return (PASSED);
}

uint32 setup_dsx_with_eth_loopback(int dsx) {
    tLiuSatopDe1 de1 = { 0, cAtPdhDs1J1UnFrm, cPsnMpls, cAtEthPortInterfaceXGMii, cAtLoopbackModeLocal,
            cAtPdhLoopbackModeRelease };
    tLiuSatopDe3 de3 = { 0, cAtPdhDs3Unfrm, cPsnMpls, cAtEthPortInterfaceXGMii, cAtLoopbackModeLocal, };

    prverbose("Setting up DSx CEM = 0x%d\n", dsx);
    if (dsx == 0) {
        if (setup_liu_de1(de1) == FAILED) {
            printf("CEM setup DS1 failed\n");
            return (FAILED);
        }
    } else if (dsx == 1) {
        if (setup_liu_de3(de3) == FAILED) {
            printf("CEM setup DS3 failed\n");
            return (FAILED);
        }
    }

    return (PASSED);
}

eAtRet De3Prbs_DS1(uint32 engId, uint8 oc48LineId) {
    uint32 alarm;
    eAtRet ret = cAtOk;
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);
    AtDevice device = DeviceGet();
    time_t start_time;
    time_t current_time;
    unsigned long wait_time = 0;
    unsigned long ittr = 0;
    uint16 liuId;
    uint16 engineId;

    if (diag_global_flag & DIAG_FLAG_EDVT_ON)
        wait_time = 60;
    else
        wait_time = getdec_answer("Enter time in seconds for test to run ", 6, 6, 4294967294);

    AtModulePrbs prbsModule = (AtModulePrbs) AtDeviceModuleGet(device, cAtModulePrbs);

    for (liuId = oc48LineId, engineId = 0; liuId < (oc48LineId + 16); liuId++, engineId++) {
        AtPdhDe1 de3 = AtModulePdhDe1Get(PdhModule(DeviceGet()), liuId);
        if (de3 == NULL) {
            printf("WARNING: de3 does not exist\r\n");
            return cAtErrorNullPointer;
        }

        /* Create engine */

        //uint32 engineId = 0; /* Assume that this engine has not been used yet */
        AtPrbsEngine engine = AtModulePrbsDe1PrbsEngineCreate(prbsModule, engineId, de3);
        /* Can access this engine after creating as following */
        if (engine == NULL)
            err('f', 0, "engine NULL for engineId %d", engineId);
        if (AtPrbsEngineIdGet(engine) != engineId)
            err('f', 0, " Failed");
        if (AtModulePrbsEngineGet(prbsModule, engineId) != engine)
            err('f', 0, "AtPrbsEngineIdGet Failed");
        /* Both these objects have reference to each other */
        if (AtPrbsEngineChannelGet(engine) != (AtChannel) de3)
            err('f', 0, "AtPrbsEngineChannelGet Failed");
        if (AtChannelPrbsEngineGet((AtChannel) de3) != engine)
            err('f', 0, "AtChannelPrbsEngineGet Failed");
        /* Configure the PRBS mode */
        if (AtPrbsEngineTxModeSet(engine, cAtPrbsModePrbs23) != cAtOk)
            err('f', 0, "AtPrbsEngineTxModeSet Failed");
        if (AtPrbsEngineTxModeGet(engine) != cAtPrbsModePrbs23)
            err('f', 0, "AtPrbsEngineTxModeGet Failed");
        if (AtPrbsEngineRxModeSet(engine, cAtPrbsModePrbs23) != cAtOk)
            err('f', 0, "AtPrbsEngineRxModeSet Failed");
        if (AtPrbsEngineRxModeGet(engine) != cAtPrbsModePrbs23)
            err('f', 0, "AtPrbsEngineRxModeGet Failed");
        /* Then enable the engine */
        if (AtPrbsEngineRxEnable(engine, cAtTrue) != cAtOk)
            err('f', 0, "AtPrbsEngineRxEnable Failed");
        if (AtPrbsEngineRxIsEnabled(engine) != cAtTrue)
            err('f', 0, "AtPrbsEngineRxIsEnabled Failed");
        if (AtPrbsEngineTxEnable(engine, cAtTrue) != cAtOk)
            err('f', 0, "AtPrbsEngineTxEnable Failed");
        if (AtPrbsEngineTxIsEnabled(engine) != cAtTrue)
            err('f', 0, "AtPrbsEngineTxIsEnabled Failed");
        AtPrbsEngineMonitoringSideSet(engine, cAtPrbsSideTdm);
    }

    printf("Side to be monitored is TDM..\r\n");
    //printf("Waiting 1 Min ....\r\n");
    sleep(5);
    printf("Start \r\n");
    time(&start_time);
    current_time = start_time;
    time_t last_print_count = 0;

    for (liuId = oc48LineId, engineId = 0; liuId < (oc48LineId + 16); liuId++, engineId++) {
        StickyGet(engineId, 1);
    }
    sleep(1);

    /* Clear Eth Counters */
    clearCountersAndDisaplay(0);

    change_text_colour(Green);
    do {
        for (liuId = oc48LineId, engineId = 0; liuId < (oc48LineId + 16); liuId++, engineId++) {
            alarm = StickyGet(engineId, 1);
            if (alarm) {
                change_text_colour(Red);
                err('f', 0, "\033[1;31m%s: TDM PRBS check failed for line %d , ittr : %d\033[1;m",
                        __FUNCTION__, liuId, ittr);
                change_text_colour(Black);
                return (cAtError);
            }
        }
        ittr++;
        if (last_print_count != current_time) {
            printf("\nTime : %d        ", (start_time + wait_time) - current_time);
            last_print_count = current_time;
        }

    } while ((start_time + wait_time) > time(&current_time));
    printf("\33[2K\rOK");
    change_text_colour(Black);

    printf("\nSide to be monitored is PSN\r\n");
    for (liuId = oc48LineId, engineId = 0; liuId < (oc48LineId + 16); liuId++, engineId++) {
        AtPrbsEngine engine = AtModulePrbsEngineGet(prbsModule, engineId);
        /* change the sides to be monitored*/
        AtPrbsEngineMonitoringSideSet(engine, cAtPrbsSidePsn);
        if (AtPrbsEngineRxEnable(engine, cAtTrue) != cAtOk)
            err('f', 0, "AtPrbsEngineRxEnable Failed");
        if (AtPrbsEngineRxIsEnabled(engine) != cAtTrue)
            err('f', 0, "AtPrbsEngineRxIsEnabled Failed");
        if (AtPrbsEngineTxEnable(engine, cAtTrue) != cAtOk)
            err('f', 0, "AtPrbsEngineTxEnable Failed");
        if (AtPrbsEngineTxIsEnabled(engine) != cAtTrue)
            err('f', 0, "AtPrbsEngineTxIsEnabled Failed");
    }
    //printf("Waiting 1 Min ....\r\n");
    sleep(5);

    for (liuId = oc48LineId, engineId = 0; liuId < (oc48LineId + 16); liuId++, engineId++) {
        StickyGet(engineId, 1);
    }
    sleep(1);
    /* Clear Eth Counters */
    clearCountersAndDisaplay(0);

    time(&start_time);
    current_time = start_time;
    last_print_count = 0;
    ittr = 0;
    change_text_colour(Green);
    do {
        for (liuId = oc48LineId, engineId = 0; liuId < (oc48LineId + 16); liuId++, engineId++) {
            alarm = StickyGet(engineId, 1);
            if (alarm) {
                change_text_colour(Red);
                err('f', 0, "\033[1;31m%s: TDM PRBS check failed for line %d, itr : %d \033[1;m",
                        __FUNCTION__, liuId, ittr);
                change_text_colour(Black);
                return (cAtError);
            }
        }
        ittr++;
        if (last_print_count != current_time) {
            printf("\nTime : %d        ", (start_time + wait_time) - current_time);
            last_print_count = current_time;
        }
    } while ((start_time + wait_time) > time(&current_time));
    printf("\33[2K\rOK");
    change_text_colour(Black);
    printf("\n");

    for (liuId = oc48LineId, engineId = 0; liuId < (oc48LineId + 16); liuId++, engineId++) {
        AtPrbsEngine engine = AtModulePrbsEngineGet(prbsModule, engineId);
        /* When PRBS is not used any more, application may want to disable and destroy
         this engine to save resource as following */
        if (AtPrbsEngineEnable(engine, cAtFalse) != cAtOk)
            err('f', 0, "AtPrbsEngineEnable Failed");
        if (AtModulePrbsEngineDelete(prbsModule, engineId) != cAtOk)
            err('f', 0, "AtModulePrbsEngineDelete Failed");
        engine = AtModulePrbsEngineGet(prbsModule, engineId);
        if (engine != NULL)
            err('f', 0, "engine not NULL");

        /* All of associations would be invalid after this */
        if (AtPrbsEngineChannelGet(engine) != NULL)
            err('f', 0, "AtPrbsEngineChannelGet Failed");
    }
    return ret;
}

eAtRet De3Prbs_DS3(uint32 engineId, uint8 oc48LineId) {
    uint32 alarm;
    eAtRet ret = cAtOk;
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);
    AtDevice device = DeviceGet();
    time_t start_time;
    time_t current_time;
    unsigned long wait_time = 0;
    unsigned long ittr = 0;
    uint16 liuId;

    if (diag_global_flag & DIAG_FLAG_EDVT_ON)
        wait_time = 60;
    else
        wait_time = getdec_answer("Enter time in seconds for test to run ", 6, 6, 4294967294);

    AtModulePrbs prbsModule = (AtModulePrbs) AtDeviceModuleGet(device, cAtModulePrbs);
    for (liuId = oc48LineId, engineId = 0; liuId < (oc48LineId + 16); liuId++, engineId++) {
        AtPdhDe3 de3 = AtModulePdhDe3Get(PdhModule(DeviceGet()), liuId);
        if (de3 == NULL) {
            printf("WARNING: de3 does not exist\r\n");
            return cAtErrorNullPointer;
        }

        /* Create engine */

        AtPrbsEngine engine = AtModulePrbsDe3PrbsEngineCreate(prbsModule, engineId, de3);
        /* Can access this engine after creating as following */
        if (engine == NULL)
            err('f', 0, "engine NULL");
        if (AtPrbsEngineIdGet(engine) != engineId)
            err('f', 0, "AtPrbsEngineIdGet Failed");
        if (AtModulePrbsEngineGet(prbsModule, engineId) != engine)
            err('f', 0, "AtModulePrbsEngineGet Failed");
        /* Both these objects have reference to each other */
        if (AtPrbsEngineChannelGet(engine) != (AtChannel) de3)
            err('f', 0, "AtPrbsEngineChannelGet Failed");
        if (AtChannelPrbsEngineGet((AtChannel) de3) != engine)
            err('f', 0, "AtChannelPrbsEngineGet Failed");
        /* Configure the PRBS mode */
        if (AtPrbsEngineTxModeSet(engine, cAtPrbsModePrbs23) != cAtOk)
            err('f', 0, "AtPrbsEngineTxModeSet Failed");
        if (AtPrbsEngineTxModeGet(engine) != cAtPrbsModePrbs23)
            err('f', 0, "AtPrbsEngineTxModeGet Failed");
        if (AtPrbsEngineRxModeSet(engine, cAtPrbsModePrbs23) != cAtOk)
            err('f', 0, "AtPrbsEngineRxModeSet Failed");
        if (AtPrbsEngineRxModeGet(engine) != cAtPrbsModePrbs23)
            err('f', 0, "AtPrbsEngineRxModeGet Failed");
        /* Then enable the engine */
        if (AtPrbsEngineRxEnable(engine, cAtTrue) != cAtOk)
            err('f', 0, "AtPrbsEngineRxEnable Failed");
        if (AtPrbsEngineRxIsEnabled(engine) != cAtTrue)
            err('f', 0, "AtPrbsEngineRxIsEnabled Failed");
        if (AtPrbsEngineTxEnable(engine, cAtTrue) != cAtOk)
            err('f', 0, "AtPrbsEngineTxEnable Failed");
        if (AtPrbsEngineTxIsEnabled(engine) != cAtTrue)
            err('f', 0, "AtPrbsEngineTxIsEnabled Failed");
        AtPrbsEngineMonitoringSideSet(engine, cAtPrbsSideTdm);
    }

    printf("Side to be monitored is TDM..\r\n");
    //printf("Waiting 1 Min ....\r\n");
    sleep(5);
    printf("Start \r\n");
    time(&start_time);
    current_time = start_time;
    time_t last_print_count = 0;

    for (liuId = oc48LineId, engineId = 0; liuId < (oc48LineId + 16); liuId++, engineId++) {
        StickyGet(engineId, 1);
    }
    sleep(1);
    /* Clear Eth Counters */
    clearCountersAndDisaplay(0);

    change_text_colour(Green);
    do {
        for (liuId = oc48LineId, engineId = 0; liuId < (oc48LineId + 16); liuId++, engineId++) {
            alarm = StickyGet(engineId, 1);
            if (alarm) {
                change_text_colour(Red);
                err('f', 0, "\033[1;31m%s: TDM PRBS check failed for line %d, ittr : %d \033[1;m",
                        __FUNCTION__, liuId, ittr);
                change_text_colour(Black);
                return (cAtError);
            }
        }

        ittr++;
        if (last_print_count != current_time) {
            printf("\nTime : %d        ", (start_time + wait_time) - current_time);
            last_print_count = current_time;
        }
    } while ((start_time + wait_time) > time(&current_time));

    printf("\33[2K\rOK");
    change_text_colour(Black);

    printf("\nSide to be monitored is PSN\r\n");
    for (liuId = oc48LineId, engineId = 0; liuId < (oc48LineId + 16); liuId++, engineId++) {
        AtPrbsEngine engine = AtModulePrbsEngineGet(prbsModule, engineId);
        /* change the sides to be monitored*/
        AtPrbsEngineMonitoringSideSet(engine, cAtPrbsSidePsn);
        if (AtPrbsEngineRxEnable(engine, cAtTrue) != cAtOk)
            err('f', 0, "AtPrbsEngineRxEnable Failed");
        if (AtPrbsEngineRxIsEnabled(engine) != cAtTrue)
            err('f', 0, "AtPrbsEngineRxIsEnabled Failed");
        if (AtPrbsEngineTxEnable(engine, cAtTrue) != cAtOk)
            err('f', 0, "AtPrbsEngineTxEnable Failed");
        if (AtPrbsEngineTxIsEnabled(engine) != cAtTrue)
            err('f', 0, "AtPrbsEngineTxIsEnabled Failed");
    }
    //printf("Waiting 1 Min ....\r\n");
    sleep(5);

    for (liuId = oc48LineId, engineId = 0; liuId < (oc48LineId + 16); liuId++, engineId++) {
        StickyGet(engineId, 1);
    }
    sleep(1);
    /* Clear Eth Counters */
    clearCountersAndDisaplay(0);

    time(&start_time);
    current_time = start_time;
    last_print_count = 0;

    ittr = 0;
    change_text_colour(Green);
    do {
        for (liuId = oc48LineId, engineId = 0; liuId < (oc48LineId + 16); liuId++, engineId++) {
            alarm = StickyGet(engineId, 1);
            if (alarm) {
                change_text_colour(Red);
                err('f', 0, "\033[1;31m%s: PSN PRBS check failed for line %d, itr: %d \033[1;m", __FUNCTION__,
                        liuId, ittr);
                change_text_colour(Black);
                return (cAtError);
            }
        }

        ittr++;
        if (last_print_count != current_time) {
            printf("\nTime : %d        ", (start_time + wait_time) - current_time);
            last_print_count = current_time;
        }
    } while ((start_time + wait_time) > time(&current_time));

    printf("\33[2K\rOK");
    change_text_colour(Black);
    printf("\n");

    for (liuId = oc48LineId, engineId = 0; liuId < (oc48LineId + 16); liuId++, engineId++) {
        AtPrbsEngine engine = AtModulePrbsEngineGet(prbsModule, engineId);
        /* When PRBS is not used any more, application may want to disable and destroy
         this engine to save resource as following */
        if (AtPrbsEngineEnable(engine, cAtFalse) != cAtOk)
            err('f', 0, "AtPrbsEngineEnable Failed");
        if (AtModulePrbsEngineDelete(prbsModule, engineId) != cAtOk)
            err('f', 0, "AtModulePrbsEngineDelete Failed");
        engine = AtModulePrbsEngineGet(prbsModule, engineId);
        if (engine != NULL)
            err('f', 0, "engine not NULL");

        /* All of associations would be invalid after this */
        if (AtPrbsEngineChannelGet(engine) != NULL)
            err('f', 0, "AtPrbsEngineChannelGet Failed");
    }
    return ret;
}

eAtRet De3Prbs(uint32 engineId, uint8 oc48LineId) {
    uint32 alarm;
    eAtRet ret = cAtOk;
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);
    AtDevice device = DeviceGet();
    time_t start_time;
    time_t current_time;
    unsigned long wait_time = 0;
    unsigned long ittr = 0;
    int errCnt = 1;
    char buffer[100];

    AtModulePw pwModule = (AtModulePw) AtDeviceModuleGet(DeviceGet(), cAtModulePw);

    AtModuleSdh sdhModule = (AtModuleSdh) AtDeviceModuleGet(device, cAtModuleSdh);
    if (sdhModule == NULL)
        err('f', 0, "engine NULL");
    AtSdhLine line = AtModuleSdhLineGet(sdhModule, oc48LineId);
    if (line == NULL)
        err('f', 0, "line NULL");
    AtSdhVc vc3 = AtSdhLineVc3Get(line, 0, 0);
    if (vc3 == NULL)
        err('f', 0, "vc3 NULL");
    AtPdhDe3 de3 = AtSdhChannelMapChannelGet((AtSdhChannel) vc3);
    if (de3 == NULL)
        err('f', 0, "de3 NULL");
    /* Create engine */
    AtModulePrbs prbsModule = (AtModulePrbs) AtDeviceModuleGet(device, cAtModulePrbs);
    //uint32 engineId = 0; /* Assume that this engine has not been used yet */
    AtPrbsEngine engine = AtModulePrbsDe3PrbsEngineCreate(prbsModule, engineId, de3);
    /* Can access this engine after creating as following */
    if (engine == NULL)
        err('f', 0, "engine NULL");
    if (AtPrbsEngineIdGet(engine) != engineId)
        err('f', 0, "prbs engine get failed");
    if (AtModulePrbsEngineGet(prbsModule, engineId) != engine)
        err('f', 0, "AtModulePrbsEngineGet Failed");
    /* Both these objects have reference to each other */
    if (AtPrbsEngineChannelGet(engine) != (AtChannel) de3)
        err('f', 0, " Failed");
    if (AtChannelPrbsEngineGet((AtChannel) de3) != engine)
        err('f', 0, " AtPrbsEngineChannelGet Failed");
    /* Configure the PRBS mode */
    if (AtPrbsEngineTxModeSet(engine, cAtPrbsModePrbs23) != cAtOk)
        err('f', 0, " Failed");
    if (AtPrbsEngineTxModeGet(engine) != cAtPrbsModePrbs23)
        err('f', 0, " AtPrbsEngineTxModeSet Failed");
    if (AtPrbsEngineRxModeSet(engine, cAtPrbsModePrbs23) != cAtOk)
        err('f', 0, " AtPrbsEngineRxModeSet Failed");
    if (AtPrbsEngineRxModeGet(engine) != cAtPrbsModePrbs23)
        err('f', 0, "AtPrbsEngineRxModeGet Failed");
    /* Then enable the engine */
    if (AtPrbsEngineRxEnable(engine, cAtTrue) != cAtOk)
        err('f', 0, "AtPrbsEngineRxEnable Failed");
    if (AtPrbsEngineRxIsEnabled(engine) != cAtTrue)
        err('f', 0, "AtPrbsEngineRxIsEnabled Failed");
    if (AtPrbsEngineTxEnable(engine, cAtTrue) != cAtOk)
        err('f', 0, "AtPrbsEngineTxEnable Failed");
    if (AtPrbsEngineTxIsEnabled(engine) != cAtTrue)
        err('f', 0, "AtPrbsEngineTxIsEnabled Failed");
    printf("Waiting 5 secs ....\r\n");
    sleep(5);
    printf("Start ....\r\n");

    printf("Side to be monitored is TDM..\r\n");
    AtPrbsEngineMonitoringSideSet(engine, cAtPrbsSideTdm);

    if (diag_global_flag & DIAG_FLAG_EDVT_ON)
        wait_time = 60;
    else
        wait_time = getdec_answer("Enter time in seconds for test to run ", 6, 6, 4294967294);

    sleep(1);
    alarm = StickyGet(engineId, 1);
    sleep(1);
    alarm = StickyGet(engineId, 1);

    change_text_colour(Green);
    PrbsEngineAllCountersClear(engineId);
    time(&start_time);
    current_time = start_time;
    time_t last_print_count = 0;
    do {
        alarm = StickyGet(engineId, 1);
        if (alarm) {
            change_text_colour(Red);
            err('f', 0, "%s: TDM PRBS check failed, itr : %d", __FUNCTION__, ittr);
            change_text_colour(Black);
            ret = cAtErrorBerEngineFail;
            break;
        }
        ittr++;
        if (last_print_count != current_time) {
            printf("\nTime : %d        ", (start_time + wait_time) - current_time);
            last_print_count = current_time;
        }
    } while ((start_time + wait_time) > time(&current_time));

    if (ret == cAtOk)
        printf("\33[2K\rOK");
    change_text_colour(Black);

    printf("\nChanging side to be monitored to PSN\r\n");

    if (!(diag_global_flag & DIAG_FLAG_EDVT_ON))
        wait_time = getdec_answer("Enter time in seconds for test to run ", 6, 6, 4294967294);

    /* change the sides to be monitored*/
    AtPrbsEngineMonitoringSideSet(engine, cAtPrbsSidePsn);
    if (AtPrbsEngineRxEnable(engine, cAtTrue) != cAtOk)
        err('f', 0, "AtPrbsEngineRxEnable Failed");
    if (AtPrbsEngineRxIsEnabled(engine) != cAtTrue)
        err('f', 0, "AtPrbsEngineRxIsEnabled Failed");
    if (AtPrbsEngineTxEnable(engine, cAtTrue) != cAtOk)
        err('f', 0, "AtPrbsEngineTxEnable Failed");
    if (AtPrbsEngineTxIsEnabled(engine) != cAtTrue)
        err('f', 0, "AtPrbsEngineTxIsEnabled Failed");
    sleep(1);
    alarm = StickyGet(engineId, 1);
    sleep(1);
    alarm = StickyGet(engineId, 1);

    ittr = 0;
    change_text_colour(Green);

    PrbsEngineAllCountersClear(engineId);
    time(&start_time);
    current_time = start_time;
    last_print_count = 0;
    DisplayAndClearCounters(1, 0, 0, 0);
    do {
        alarm = StickyGet(engineId, 1);
        if (alarm) {
            change_text_colour(Red);
            err('f', 0, "%s: PSN PRBS check failed , errCnt %d, time left %d", __FUNCTION__, errCnt,
                    (start_time + wait_time) - current_time);
            change_text_colour(Black);
            errCnt++;
            ret = cAtErrorBerEngineFail;
            sprintf(buffer, "show eth port counters 1 r2c");
            DisplayAndClearCounters(1, 0, 0, 1);
            break;
        }

        ittr++;
        if (last_print_count != current_time) {
            printf("\nTime : %d        ", (start_time + wait_time) - current_time);
            last_print_count = current_time;
        }
    } while ((start_time + wait_time) > time(&current_time));

    if (ret == cAtOk)
        printf("\33[2K\rOK");

    change_text_colour(Black);

    printf("\n");
#if 1
    /* When PRBS is not used any more, application may want to disable and destroy
     this engine to save resource as following */
    if (AtPrbsEngineEnable(engine, cAtFalse) != cAtOk)
        err('f', 0, "AtPrbsEngineEnable Failed");
    if (AtModulePrbsEngineDelete(prbsModule, engineId) != cAtOk)
        err('f', 0, "AtModulePrbsEngineDelete Failed");
    engine = AtModulePrbsEngineGet(prbsModule, engineId);
    if (engine != NULL)
        err('f', 0, "engine not NULL");

    /* All of associations would be invalid after this */
    if (AtPrbsEngineChannelGet(engine) != NULL)
        err('f', 0, "AtPrbsEngineChannelGet Failed");
    if (AtChannelPrbsEngineGet((AtChannel) de3) != NULL)
        err('f', 0, "AtChannelPrbsEngineGet Failed");
#endif
    return ret;
}

eAtRet diag_ocn_bert_prbs_start(uint32 engineId, uint8 oc48LineId, uint8 monitoringSide) {
    eAtRet ret = cAtOk;
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);
    AtDevice device = DeviceGet();
    char buffer[100];

    AtModulePw pwModule = (AtModulePw) AtDeviceModuleGet(device, cAtModulePw);

    AtModuleSdh sdhModule = (AtModuleSdh) AtDeviceModuleGet(device, cAtModuleSdh);
    if (sdhModule == NULL)
        err('f', 0, "sdhModule NULL");
    AtSdhLine line = AtModuleSdhLineGet(sdhModule, oc48LineId);
    if (line == NULL)
        err('f', 0, "line NULL");
    AtSdhVc vc3 = AtSdhLineVc3Get(line, 0, 0);
    if (vc3 == NULL)
        err('f', 0, "vc3 NULL");
    AtPdhDe3 de3 = AtSdhChannelMapChannelGet((AtSdhChannel) vc3);
    if (de3 == NULL)
        err('f', 0, "de3 NULL");
    /* Create engine */
    AtModulePrbs prbsModule = (AtModulePrbs) AtDeviceModuleGet(device, cAtModulePrbs);
    //uint32 engineId = 0; /* Assume that this engine has not been used yet */
    AtPrbsEngine engine = AtModulePrbsDe3PrbsEngineCreate(prbsModule, engineId, de3);
    /* Can access this engine after creating as following */
    if (engine == NULL)
        err('f', 0, "engine NULL");
    if (AtPrbsEngineIdGet(engine) != engineId)
        err('f', 0, "prbs engine get failed");
    if (AtModulePrbsEngineGet(prbsModule, engineId) != engine)
        err('f', 0, "AtModulePrbsEngineGet Failed");
    /* Both these objects have reference to each other */
    if (AtPrbsEngineChannelGet(engine) != (AtChannel) de3)
        err('f', 0, " Failed");
    if (AtChannelPrbsEngineGet((AtChannel) de3) != engine)
        err('f', 0, " AtPrbsEngineChannelGet Failed");
    /* Configure the PRBS mode */
    if (AtPrbsEngineTxModeSet(engine, cAtPrbsModePrbs23) != cAtOk)
        err('f', 0, " Failed");
    if (AtPrbsEngineTxModeGet(engine) != cAtPrbsModePrbs23)
        err('f', 0, " AtPrbsEngineTxModeSet Failed");
    if (AtPrbsEngineRxModeSet(engine, cAtPrbsModePrbs23) != cAtOk)
        err('f', 0, " AtPrbsEngineRxModeSet Failed");
    if (AtPrbsEngineRxModeGet(engine) != cAtPrbsModePrbs23)
        err('f', 0, "AtPrbsEngineRxModeGet Failed");
    /* Then enable the engine */
    if (AtPrbsEngineRxEnable(engine, cAtTrue) != cAtOk)
        err('f', 0, "AtPrbsEngineRxEnable Failed");
    if (AtPrbsEngineRxIsEnabled(engine) != cAtTrue)
        err('f', 0, "AtPrbsEngineRxIsEnabled Failed");
    if (AtPrbsEngineTxEnable(engine, cAtTrue) != cAtOk)
        err('f', 0, "AtPrbsEngineTxEnable Failed");
    if (AtPrbsEngineTxIsEnabled(engine) != cAtTrue)
        err('f', 0, "AtPrbsEngineTxIsEnabled Failed");

    if(monitoringSide == 0) {
        printf("Side to be monitored is PSN..\r\n");
        AtPrbsEngineMonitoringSideSet(engine, cAtPrbsSidePsn);
    } else {
        printf("Side to be monitored is TDM..\r\n");
        AtPrbsEngineMonitoringSideSet(engine, cAtPrbsSideTdm);
    }
    return ret;
}

eAtRet Im_Ocn_10g_Prbs_Chg_Monitoring_side(uint32 engineId, uint8 monitoringSide) {
    eAtRet ret = cAtOk;
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);
    AtDevice device = DeviceGet();

    AtModulePrbs prbsModule = (AtModulePrbs) AtDeviceModuleGet(device, cAtModulePrbs);

    AtPrbsEngine engine = AtModulePrbsEngineGet(prbsModule, engineId);
    if (engine == NULL)
        err('f', 0, "engine NULL for id : %d",engineId);

    if(monitoringSide == 0) {
        printf("Side to be monitored is PSN..\r\n");
        ret = AtPrbsEngineMonitoringSideSet(engine, cAtPrbsSidePsn);
    } else {
        printf("Side to be monitored is TDM..\r\n");
        ret = AtPrbsEngineMonitoringSideSet(engine, cAtPrbsSideTdm);
    }
#if 0
    StickyGet(engineId, 1);
    uint32 timeout = 60; /* 10 seconds to wait*/

    while(--timeout) {
    	if(StickyGet(engineId, 1)) {
    		sleep(1);
    	}else {
    		break;
    	}
    }
    if(timeout == 0)
    	ret = cAtError;

    printf("\ntimeout : %d\n",timeout);
#endif
    return ret;
}


eAtRet diag_ocn_bert_prbs_stop(uint32 engId) {
    eAtRet ret = cAtOk;
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);
    AtDevice device = DeviceGet();

    AtModulePrbs prbsModule = (AtModulePrbs) AtDeviceModuleGet(device, cAtModulePrbs);

    AtPrbsEngine engine = AtModulePrbsEngineGet(prbsModule, engId);
    /* When PRBS is not used any more, application may want to disable and destroy
     this engine to save resource as following */
    if ((ret = AtPrbsEngineEnable(engine, cAtFalse)) != cAtOk)
        printf("\nAtPrbsEngineTxIsEnabled Failed");
    if ((ret = AtModulePrbsEngineDelete(prbsModule, engId)) != cAtOk)
    	printf("\nAtPrbsEngineTxIsEnabled Failed");
    engine = AtModulePrbsEngineGet(prbsModule, engId);
    if (engine != NULL) {
    	ret = cAtError;
    	printf("\nAtPrbsEngineTxIsEnabled Failed");
    }
    /* All of associations would be invalid after this */
    if (AtPrbsEngineChannelGet(engine) != NULL) {
    	ret = cAtError;
    	printf("AtPrbsEngineTxIsEnabled Failed");
    }

    return ret;
}



uint8 diag_ocn_bert_prbs_check(uint32 engId, eBool read2Clear) {

    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);

    if(StickyGet(engId, read2Clear))
        return FAILED;
    else
    	return PASSED;
}

void ethCountersClears(void) {
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);
    AtModuleEth ethModule = (AtModuleEth) AtDeviceModuleGet(DeviceGet(), cAtModuleEth);
    tAtEthPortCounters ethCounters;
    AtChannelAllCountersClear((AtChannel) AtModuleEthPortGet(ethModule, 0), &ethCounters);
    return;
}

void hyphyTfiArriveLoopbackConf(dev_object_t *dev_obj, uint8_t enable) {

    char buffer[100];
    uint8 slotId = (uint8) im_get_current_slot_no();
    if (enable) {
        EnterSlot(slotId);
        sprintf(buffer, "sdh line serdes loopback 1-8 remote");
        AtCliExecute(buffer);
    } else {
        sprintf(buffer, "sdh line serdes loopback 1-8 release");
        AtCliExecute(buffer);
    }
}
;

void DisplayAndClearCounters(int clearFlag, int StartPw, int EndPw, int Display) {
    int pwCount = 0;
    AtDevice device = DeviceGet();
    AtModulePw pwModule = (AtModulePw) AtDeviceModuleGet(device, cAtModulePw);
    AtModulePdh pdhModule = (AtModulePdh) AtDeviceModuleGet(device, cAtModulePdh);
    AtPwCounters counters;
    AtPw pw;
    if (device == NULL || pwModule == NULL || pdhModule == NULL) {
        printf("WARNING: module does not exist\r\n");
        return;
    }

    if (EndPw < StartPw)
        return;

    for (pwCount = StartPw; pwCount <= EndPw; pwCount++) {
        pw = AtModulePwGetPw(pwModule, pwCount);

        if (clearFlag)
            AtChannelAllCountersClear((AtChannel) pw, &counters);
        else
            AtChannelAllCountersGet((AtChannel) pw, &counters);

        if (Display) {
            printf("----- PW %d -----\n", pwCount);
            printf("TxPackets = %d\n", AtPwCountersTxPacketsGet(counters));
            printf("TxPayloadBytes = %d\n", AtPwCountersTxPayloadBytesGet(counters));
            printf("RxPackets = %d\n", AtPwCountersRxPacketsGet(counters));
            printf("RxPayloadBytes = %d\n", AtPwCountersRxPayloadBytesGet(counters));
            printf("RxDiscardedPackets = %d\n", AtPwCountersRxDiscardedPacketsGet(counters));
            printf("RxMalformedPackets = %d\n", AtPwCountersRxMalformedPacketsGet(counters));
            printf("RxReorderedPackets = %d\n", AtPwCountersRxReorderedPacketsGet(counters));
            printf("RxLostPackets = %d\n", AtPwCountersRxLostPacketsGet(counters));
            printf("RxOutOfSeqDropPackets = %d\n", AtPwCountersRxOutOfSeqDropPacketsGet(counters));
            printf("RxStrayPackets = %d\n", AtPwCountersRxStrayPacketsGet(counters));
            printf("RxOamPackets = %d\n", AtPwCountersRxOamPacketsGet(counters));
            printf("TxLbitPackets = %d\n", AtPwTdmCountersTxLbitPacketsGet((AtPwTdmCounters) counters));
            printf("TxRbitPackets = %d\n", AtPwTdmCountersTxRbitPacketsGet((AtPwTdmCounters) counters));
            printf("RxLbitPackets = %d\n", AtPwTdmCountersRxLbitPacketsGet((AtPwTdmCounters) counters));
            printf("RxRbitPackets = %d\n", AtPwTdmCountersRxRbitPacketsGet((AtPwTdmCounters) counters));
            printf("RxJitBufOverrunEvents = %d\n",
                    AtPwTdmCountersRxJitBufOverrunGet((AtPwTdmCounters) counters));
            printf("RxJitBufUnderrunEvents = %d\n",
                    AtPwTdmCountersRxJitBufUnderrunGet((AtPwTdmCounters) counters));
            printf("RxLops = %d\n", AtPwTdmCountersRxLopsGet((AtPwTdmCounters) counters));
            printf("NumPktsInJitterBuffer = %u + %u\n", AtPwNumCurrentAdditionalBytesInJitterBuffer(pw),
                    AtPwNumCurrentPacketsInJitterBuffer(pw));
        }
    }

}

uint32 checkSerdesPll(void) {
    AtDevice device = DeviceGet();
    AtModuleEth ethModule = (AtModuleEth) AtDeviceModuleGet(device, cAtModuleEth);
    uint8 ethPortId = 0;
    AtSerdesController controller = AtModuleEthSerdesController(ethModule, ethPortId);
    eBool pllLocked = AtSerdesControllerPllIsLocked(controller);

    if (device == NULL || ethModule == NULL || controller == NULL) {
        printf("WARNING: module does not exist\r\n");
        return cAtErrorNullPointer;
    }

    if (pllLocked) {
        printf("Serdes PLL is LOCKED\n");
        return PASSED;
    } else {
        printf("SErdes PLL is NOT LOCKED\n");
        return FAILED;
    }
}

static int At4EthPortSerdesPrbsEnable(AtDevice self, eBool enable) {
    uint32 serdes_i;
    uint32 numberOfSerdes = 4;
    eBool success = cAtTrue;
    AtModuleEth ethModule = (AtModuleEth) AtDeviceModuleGet(self, cAtModuleEth);
    for (serdes_i = 0; serdes_i < numberOfSerdes; serdes_i++) {
        AtSerdesController serdesControler = AtModuleEthSerdesController(ethModule, serdes_i);
        AtPrbsEngine engine = AtSerdesControllerPrbsEngine(serdesControler);
        if (AtPrbsEngineEnable(engine, enable) != cAtOk) {
            AtPrintc(cSevCritical, "Error: Can not enable for Eth_Serdes#%d\r\n", (serdes_i + 1));
            success = cAtFalse;
        }
    }
    return success;
}

static eBool At4EthPortSerdesPrbsCheck(AtDevice self, eBool enableCheck, eBool enableDisplay) {
    uint32 serdes_i;
    uint32 numberOfSerdes = 4;
    eBool success = cAtTrue;
    AtModuleEth ethModule = (AtModuleEth) AtDeviceModuleGet(self, cAtModuleEth);
    for (serdes_i = 0; serdes_i < numberOfSerdes; serdes_i++) {
        uint32 sticky = 0;
        AtSerdesController serdesControler = AtModuleEthSerdesController(ethModule, serdes_i);
        AtPrbsEngine engine = AtSerdesControllerPrbsEngine(serdesControler);
        sticky = AtPrbsEngineAlarmHistoryClear(engine);
        if ((enableCheck) && (sticky != 0)) {
            success = cAtFalse;
            if (enableDisplay) {
                AtPrintc(cSevCritical, "Error: PRBS has error sticky at Eth_Serdes#%d\r\n", (serdes_i + 1));
            }
        }
    }
    return success;
}

int Diag4EthSerdes(void) {
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);
    AtDevice device = DeviceGet();
    time_t start_time;
    time_t current_time;
    unsigned long wait_time = 0;
    unsigned long ittr = 0;
    int rc = PASSED;

    AtDeviceDiagnosticModeEnable(device, cAtTrue);

    if (!At4EthPortSerdesPrbsEnable(device, cAtTrue)) {
        err('f', 0, "At4EthPortSerdesPrbsEnable Failed");
    }

    printf("Waiting 3 secs ....\r\n");
    sleep(3);

    /* clear all sticky before check */
    At4EthPortSerdesPrbsCheck(device, cAtFalse, cAtFalse);

    if (diag_global_flag & DIAG_FLAG_EDVT_ON)
        wait_time = 60;
    else
        wait_time = getdec_answer("Enter time in seconds for test to run ", 6, 6, 4294967294);

    change_text_colour(Green);
    time(&start_time);
    do {
        if (At4EthPortSerdesPrbsCheck(device, cAtTrue, cAtTrue) == cAtFalse) {
            change_text_colour(Red);
            err('f', 0, "%s: SERDES ETH PRBS check failed, itr : %d", __FUNCTION__, ittr);
            change_text_colour(Black);
            rc = FAILED;
            break;
        }
        ittr++;
        if (ittr % 10)
            printf("\33[2K\r");

        printf("Time : %d\r", (start_time + wait_time) - current_time);

    } while ((start_time + wait_time) > time(&current_time));

    if (rc == PASSED)
        printf("\33[2K\rOK");
    change_text_colour(Black);
    printf("\n");

    /* Disable SERDES PRBS when PASSED, Otherwise, keep this enable for debug purpose */
    if (rc == PASSED) {
        At4EthPortSerdesPrbsEnable(device, cAtFalse);
        AtDeviceDiagnosticModeEnable(device, cAtFalse);
    }

    return rc;
}

eAtRet simpleBertPrbsDSx_Create() {
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);
    AtDevice device = DeviceGet();
    uint32 pw_i = 0;
    AtPrbsEngine engine = NULL;
    AtPw pw = NULL;

    uint32 liu_i = 0;
    for (liu_i = 0; liu_i < 48; liu_i++) {
        pw_i = liu_i;
        pw = AtModulePwGetPw(PwModule(device), pw_i);
        if (pw == NULL) {
            printf("WARNING: module does not exist\r\n");
            return cAtErrorNullPointer;
        }
        engine = AtChannelPrbsEngineGet((AtChannel) pw);
        if (engine == NULL) {
            printf("PRBS is null for pw%d\n", liu_i);
            return cAtError;
        }
        if (AtPrbsEngineEnable(engine, cAtTrue) != cAtOk) {
            printf("prbs#%d can not enable\r\n", liu_i);
            return cAtError;
        }
        if (AtPrbsEngineIsEnabled(engine) != cAtTrue) {
            printf("prbs#%d can not get enabled\r\n", liu_i);
            return cAtError;
        }
        if (AtPrbsEngineModeSet(engine, cAtPrbsModePrbs15) != cAtOk) {
            printf("prbs#%d can not set prbs15\r\n", liu_i + 1);
            return cAtError;
        }
//        if (AtPrbsEngineModeSet(engine, cAtPrbsModePrbs31) != cAtOk) {
//            printf("prbs#%d can not set prbs31\r\n",liu_i + 1);
//            return cAtError;
//        }
        if (AtPrbsEngineModeGet(engine) != cAtPrbsModePrbs15) {
            printf("prbs#%d is not set to prbs15\r\n", liu_i);
            return cAtError;
        }

        debug_print("prbs#%d Enabled\r\n", liu_i);
    }

    printf("Sleeping for 5 Sec\r\n");
    sleep(5);
    printf("Side to be monitored is PSN..\r\n");

    for (liu_i = 0; liu_i < 48; liu_i++) {
        pw_i = liu_i;
        pw = AtModulePwGetPw(PwModule(device), pw_i);
        if (pw == NULL) {
            printf("WARNING: module does not exist\r\n");
            return cAtErrorNullPointer;
        }
        engine = AtChannelPrbsEngineGet((AtChannel) pw);
        if (engine == NULL) {
            printf("PRBS%d is NULL with \r\n", liu_i);
            return cAtErrorNullPointer;
        }

        if (AtPrbsEngineIsEnabled(engine) == cAtFalse) {
            printf("prbs#%d is not enabled\r\n", liu_i);
            return cAtError;
        }

        if (AtPrbsEngineAlarmGet(engine) != 0) {
            debug_print("BEFORE CLEAR: prbs#%d got Alarm\r\n", liu_i);
        }
        if (AtPrbsEngineAlarmHistoryClear(engine) != 0) {
            debug_print("BEFORE CLEAR: prbs#%d got Error-Sticky\r\n", liu_i);
        }
    }
    printf("PRBS Enabled\r\n");
    return cAtOk;
}

eAtRet simpleBertPrbsDSx_Check() {
    uint8 slotId = (uint8) im_get_current_slot_no(), error = FALSE;
    EnterSlot(slotId);
    AtDevice device = DeviceGet();
    uint32 liu_i = 0;
    AtPrbsEngine engine = NULL;
    uint32 pw_i = 0;
    AtPw pw = NULL;
    //unsigned char sticky = 0;
    //unsigned char alarm = 0;

    for (liu_i = 0; liu_i < 48; liu_i++) {
        pw_i = liu_i;
        pw = AtModulePwGetPw(PwModule(device), pw_i);
        engine = AtChannelPrbsEngineGet((AtChannel) pw);
        //alarm = AtPrbsEngineAlarmGet(engine);
        //sticky = AtPrbsEngineAlarmHistoryClear(engine);
        //debug_print("Alarm = %d, Sticky = %d\n",alarm, sticky);
        //if((alarm != 0)  || (sticky != 0)){
        if ((AtPrbsEngineAlarmHistoryClear(engine) != 0) || (AtPrbsEngineAlarmGet(engine) != 0)) {
            printf("%s PSN PRBS check failed for line %d\n", __FUNCTION__, liu_i);
            error = TRUE;
        }
    }
    if (error == TRUE) {
        err('f', 0, "%s PSN PRBS check failed", __FUNCTION__);
        return cAtError;
    }
    return cAtOk;
}

eAtRet simpleBertPrbsDSx_Clean() {
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);
    AtDevice device = DeviceGet();

    uint32 liu_i = 0;
    for (liu_i = 0; liu_i < 48; liu_i++) {
        AtPrbsEngine engine;
        uint32 pw_i = liu_i;
        AtPw pw = AtModulePwGetPw(PwModule(device), pw_i);
        if (pw == NULL) {
            printf("WARNING: module does not exist\r\n");
            return cAtErrorNullPointer;
        }
        engine = AtChannelPrbsEngineGet((AtChannel) pw);
        if (engine == NULL) {
            printf("PRBS is null for pw%d\n", liu_i);
            return cAtError;
        }
        if (AtPrbsEngineEnable(engine, cAtFalse) != cAtOk) {
            printf("prbs#%d can not disable\r\n", liu_i);
            return cAtError;
        }
        if (AtPrbsEngineIsEnabled(engine) != cAtFalse) {
            printf("prbs#%d can not get disabled\r\n", liu_i);
            return cAtError;
        }

        debug_print("prbs#%d Disabled\r\n", liu_i);
    }
    printf("PRBS Disabled\r\n");
    return cAtOk;
}

static eAtRet DeviceReset(AtDevice device) {
    const uint32 cRetryTimes = 10;
    uint32 retry_i;
    eAtRet ret = cAtOk;

    for (retry_i = 0; retry_i < cRetryTimes; retry_i++) {
        ret = AtDeviceReset(device);
        if (ret == cAtOk)
            break;
    }

    return ret;
}

int cemDeviceReset() {
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);
    AtDevice device = DeviceGet();
    eAtRet ret = DeviceReset(device);
    if (ret != cAtOk) {
        err('f', "%s : %s ", __FUNCTION__, AtRet2String(ret));
        return FAILED;
    } else {
        printf("\n %s %s\n", __FUNCTION__, AtRet2String(ret));
        return PASSED;
    }
}

int clearCountersAndDisaplay(uint8 display)
{
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);
    AtDevice device = DeviceGet();
    AtModuleEth ethModule;
    AtEthPort port;
    uint8 ethPortId = 0;
    tAtEthPortCounters ethcounters;

    /* Get Ethernet Module */
    ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    if(ethModule == NULL)
    {
        printf("clearCounters: ethModule returned NULL\n");
    }
    port = AtModuleEthPortGet(ethModule, (uint8)ethPortId);
    if(port == NULL)
    {
        printf("clearCounters: port returned NULL\n");
    }
    AtChannelAllCountersClear((AtChannel)port, &ethcounters);

    if(display)
    {
        printf("txPackets = %d\n", ethcounters.txPackets);
        printf("txBytes = %d\n", ethcounters.txBytes);
        printf("txOamPackets = %d\n", ethcounters.txOamPackets);
        printf("txPeriodOamPackets = %d\n", ethcounters.txPeriodOamPackets);
        printf("txPwPackets = %d\n", ethcounters.txPwPackets);
        printf("txPacketsLen0_64 = %d\n", ethcounters.txPacketsLen0_64);
        printf("txPacketsLen65_127 = %d\n", ethcounters.txPacketsLen65_127);
        printf("txPacketsLen128_255 = %d\n", ethcounters.txPacketsLen128_255);
        printf("txPacketsLen256_511 = %d\n", ethcounters.txPacketsLen256_511);
        printf("txPacketsLen512_1024 = %d\n", ethcounters.txPacketsLen512_1024);
        printf("txPacketsLen1025_1528 = %d\n", ethcounters.txPacketsLen1025_1528);
        printf("txPacketsJumbo = %d\n", ethcounters.txPacketsJumbo);
        printf("txTopPackets = %d\n", ethcounters.txTopPackets);
    //    printf("txPauFramePackets = %d\n", ethcounters.txPauFramePackets);
        printf("rxPackets = %d\n", ethcounters.rxPackets);
        printf("rxBytes = %d\n", ethcounters.rxBytes);
        printf("rxErrEthHdrPackets = %d\n", ethcounters.rxErrEthHdrPackets);
        printf("rxErrBusPackets = %d\n", ethcounters.rxErrBusPackets);
        printf("rxErrFcsPackets = %d\n", ethcounters.rxErrFcsPackets);
        printf("rxOversizePackets = %d\n", ethcounters.rxOversizePackets);
        printf("rxUndersizePackets = %d\n", ethcounters.rxUndersizePackets);
        printf("rxPacketsLen0_64 = %d\n", ethcounters.rxPacketsLen0_64);
        printf("rxPacketsLen65_127 = %d\n", ethcounters.rxPacketsLen65_127);
        printf("rxPacketsLen128_255 = %d\n", ethcounters.rxPacketsLen128_255);
        printf("rxPacketsLen256_511 = %d\n", ethcounters.rxPacketsLen256_511);
        printf("rxPacketsLen512_1024 = %d\n", ethcounters.rxPacketsLen512_1024);
        printf("rxPacketsLen1025_1528 = %d\n", ethcounters.rxPacketsLen1025_1528);
        printf("rxPacketsJumbo = %d\n", ethcounters.rxPacketsJumbo);
        printf("rxPwUnsupportedPackets = %d\n", ethcounters.rxPwUnsupportedPackets);
        printf("rxErrPwLabelPackets = %d\n", ethcounters.rxErrPwLabelPackets);
        printf("rxDiscardedPackets = %d\n", ethcounters.rxDiscardedPackets);
        printf("rxPausePackets = %d\n", ethcounters.rxPausePackets);
        printf("rxErrPausePackets = %d\n", ethcounters.rxErrPausePackets);
        printf("rxBrdCastPackets = %d\n", ethcounters.rxBrdCastPackets);
        printf("rxMultCastPackets = %d\n", ethcounters.rxMultCastPackets);
        //printf("rxUniCastPackets = %d\n", ethcounters.rxUniCastPackets);
        printf("rxArpPackets = %d\n", ethcounters.rxArpPackets);
        printf("rxOamPackets = %d\n", ethcounters.rxOamPackets);
        printf("rxEfmOamPackets = %d\n", ethcounters.rxEfmOamPackets);
        printf("rxErrEfmOamPackets = %d\n", ethcounters.rxErrEfmOamPackets);
        printf("rxEthOamType1Packets = %d\n", ethcounters.rxEthOamType1Packets);
        printf("rxEthOamType2Packets = %d\n", ethcounters.rxEthOamType2Packets);
        printf("rxIpv4Packets = %d\n", ethcounters.rxIpv4Packets);
        printf("rxErrIpv4Packets = %d\n", ethcounters.rxErrIpv4Packets);
        printf("rxIcmpIpv4Packets = %d\n", ethcounters.rxIcmpIpv4Packets);
        printf("rxIpv6Packets = %d\n", ethcounters.rxIpv6Packets);
        printf("rxErrIpv6Packets = %d\n", ethcounters.rxErrIpv6Packets);
        printf("rxIcmpIpv6Packets = %d\n", ethcounters.rxIcmpIpv6Packets);
        printf("rxMefPackets = %d\n", ethcounters.rxMefPackets);
        printf("rxErrMefPackets = %d\n", ethcounters.rxErrMefPackets);
        printf("rxMplsPackets = %d\n", ethcounters.rxMplsPackets);
        printf("rxErrMplsPackets = %d\n", ethcounters.rxErrMplsPackets);
        printf("rxMplsErrOuterLblPackets = %d\n", ethcounters.rxMplsErrOuterLblPackets);
        printf("rxMplsDataPackets = %d\n", ethcounters.rxMplsDataPackets);
        printf("rxLdpIpv4Packets = %d\n", ethcounters.rxLdpIpv4Packets);
        printf("rxLdpIpv6Packets = %d\n", ethcounters.rxLdpIpv6Packets);
        printf("rxMplsIpv4Packets = %d\n", ethcounters.rxMplsIpv4Packets);
        printf("rxMplsIpv6Packets = %d\n", ethcounters.rxMplsIpv6Packets);
        printf("rxErrL2tpv3Packets = %d\n", ethcounters.rxErrL2tpv3Packets);
        printf("rxL2tpv3Packets = %d\n", ethcounters.rxL2tpv3Packets);
        printf("rxL2tpv3Ipv4Packets = %d\n", ethcounters.rxL2tpv3Ipv4Packets);
        printf("rxL2tpv3Ipv6Packets = %d\n", ethcounters.rxL2tpv3Ipv6Packets);
        printf("rxUdpPackets = %d\n", ethcounters.rxUdpPackets);
        printf("rxErrUdpPackets = %d\n", ethcounters.rxErrUdpPackets);
        printf("rxUdpIpv4Packets = %d\n", ethcounters.rxUdpIpv4Packets);
        printf("rxUdpIpv6Packets = %d\n", ethcounters.rxUdpIpv6Packets);
        printf("rxErrPsnPackets = %d\n", ethcounters.rxErrPsnPackets);
        printf("rxPacketsSendToCpu = %d\n", ethcounters.rxPacketsSendToCpu);
        printf("rxPacketsSendToPw = %d\n", ethcounters.rxPacketsSendToPw);
        printf("rxTopPackets = %d\n", ethcounters.rxTopPackets);
        printf("rxPacketDaMis = %d\n", ethcounters.rxPacketDaMis);
    //    printf("rxPauFramePackets = %d\n", ethcounters.rxPauFramePackets);
    }

    return PASSED;
}

int Im_Ocn_10g_Cem_ClearCountersAndDisaplay(uint8 display, uint8 eth)
{
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);
    AtDevice device = DeviceGet();
    AtModuleEth ethModule;
    AtEthPort port;
    uint8 ethPortId = eth;
    tAtEthPortCounters ethcounters;

    /* Get Ethernet Module */
    ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    if(ethModule == NULL)
    {
        printf("clearCounters: ethModule returned NULL\n");
    }
    port = AtModuleEthPortGet(ethModule, (uint8)ethPortId);
    if(port == NULL)
    {
        printf("clearCounters: port returned NULL\n");
    }
    AtChannelAllCountersClear((AtChannel)port, &ethcounters);

    if(display)
    {
        printf("txPackets = %d\n", ethcounters.txPackets);
        printf("txBytes = %d\n", ethcounters.txBytes);
        printf("txOamPackets = %d\n", ethcounters.txOamPackets);
        printf("txPeriodOamPackets = %d\n", ethcounters.txPeriodOamPackets);
        printf("txPwPackets = %d\n", ethcounters.txPwPackets);
        printf("txPacketsLen0_64 = %d\n", ethcounters.txPacketsLen0_64);
        printf("txPacketsLen65_127 = %d\n", ethcounters.txPacketsLen65_127);
        printf("txPacketsLen128_255 = %d\n", ethcounters.txPacketsLen128_255);
        printf("txPacketsLen256_511 = %d\n", ethcounters.txPacketsLen256_511);
        printf("txPacketsLen512_1024 = %d\n", ethcounters.txPacketsLen512_1024);
        printf("txPacketsLen1025_1528 = %d\n", ethcounters.txPacketsLen1025_1528);
        printf("txPacketsJumbo = %d\n", ethcounters.txPacketsJumbo);
        printf("txTopPackets = %d\n", ethcounters.txTopPackets);
    //    printf("txPauFramePackets = %d\n", ethcounters.txPauFramePackets);
        printf("rxPackets = %d\n", ethcounters.rxPackets);
        printf("rxBytes = %d\n", ethcounters.rxBytes);
        printf("rxErrEthHdrPackets = %d\n", ethcounters.rxErrEthHdrPackets);
        printf("rxErrBusPackets = %d\n", ethcounters.rxErrBusPackets);
        printf("rxErrFcsPackets = %d\n", ethcounters.rxErrFcsPackets);
        printf("rxOversizePackets = %d\n", ethcounters.rxOversizePackets);
        printf("rxUndersizePackets = %d\n", ethcounters.rxUndersizePackets);
        printf("rxPacketsLen0_64 = %d\n", ethcounters.rxPacketsLen0_64);
        printf("rxPacketsLen65_127 = %d\n", ethcounters.rxPacketsLen65_127);
        printf("rxPacketsLen128_255 = %d\n", ethcounters.rxPacketsLen128_255);
        printf("rxPacketsLen256_511 = %d\n", ethcounters.rxPacketsLen256_511);
        printf("rxPacketsLen512_1024 = %d\n", ethcounters.rxPacketsLen512_1024);
        printf("rxPacketsLen1025_1528 = %d\n", ethcounters.rxPacketsLen1025_1528);
        printf("rxPacketsJumbo = %d\n", ethcounters.rxPacketsJumbo);
        printf("rxPwUnsupportedPackets = %d\n", ethcounters.rxPwUnsupportedPackets);
        printf("rxErrPwLabelPackets = %d\n", ethcounters.rxErrPwLabelPackets);
        printf("rxDiscardedPackets = %d\n", ethcounters.rxDiscardedPackets);
        printf("rxPausePackets = %d\n", ethcounters.rxPausePackets);
        printf("rxErrPausePackets = %d\n", ethcounters.rxErrPausePackets);
        printf("rxBrdCastPackets = %d\n", ethcounters.rxBrdCastPackets);
        printf("rxMultCastPackets = %d\n", ethcounters.rxMultCastPackets);
        //printf("rxUniCastPackets = %d\n", ethcounters.rxUniCastPackets);
        printf("rxArpPackets = %d\n", ethcounters.rxArpPackets);
        printf("rxOamPackets = %d\n", ethcounters.rxOamPackets);
        printf("rxEfmOamPackets = %d\n", ethcounters.rxEfmOamPackets);
        printf("rxErrEfmOamPackets = %d\n", ethcounters.rxErrEfmOamPackets);
        printf("rxEthOamType1Packets = %d\n", ethcounters.rxEthOamType1Packets);
        printf("rxEthOamType2Packets = %d\n", ethcounters.rxEthOamType2Packets);
        printf("rxIpv4Packets = %d\n", ethcounters.rxIpv4Packets);
        printf("rxErrIpv4Packets = %d\n", ethcounters.rxErrIpv4Packets);
        printf("rxIcmpIpv4Packets = %d\n", ethcounters.rxIcmpIpv4Packets);
        printf("rxIpv6Packets = %d\n", ethcounters.rxIpv6Packets);
        printf("rxErrIpv6Packets = %d\n", ethcounters.rxErrIpv6Packets);
        printf("rxIcmpIpv6Packets = %d\n", ethcounters.rxIcmpIpv6Packets);
        printf("rxMefPackets = %d\n", ethcounters.rxMefPackets);
        printf("rxErrMefPackets = %d\n", ethcounters.rxErrMefPackets);
        printf("rxMplsPackets = %d\n", ethcounters.rxMplsPackets);
        printf("rxErrMplsPackets = %d\n", ethcounters.rxErrMplsPackets);
        printf("rxMplsErrOuterLblPackets = %d\n", ethcounters.rxMplsErrOuterLblPackets);
        printf("rxMplsDataPackets = %d\n", ethcounters.rxMplsDataPackets);
        printf("rxLdpIpv4Packets = %d\n", ethcounters.rxLdpIpv4Packets);
        printf("rxLdpIpv6Packets = %d\n", ethcounters.rxLdpIpv6Packets);
        printf("rxMplsIpv4Packets = %d\n", ethcounters.rxMplsIpv4Packets);
        printf("rxMplsIpv6Packets = %d\n", ethcounters.rxMplsIpv6Packets);
        printf("rxErrL2tpv3Packets = %d\n", ethcounters.rxErrL2tpv3Packets);
        printf("rxL2tpv3Packets = %d\n", ethcounters.rxL2tpv3Packets);
        printf("rxL2tpv3Ipv4Packets = %d\n", ethcounters.rxL2tpv3Ipv4Packets);
        printf("rxL2tpv3Ipv6Packets = %d\n", ethcounters.rxL2tpv3Ipv6Packets);
        printf("rxUdpPackets = %d\n", ethcounters.rxUdpPackets);
        printf("rxErrUdpPackets = %d\n", ethcounters.rxErrUdpPackets);
        printf("rxUdpIpv4Packets = %d\n", ethcounters.rxUdpIpv4Packets);
        printf("rxUdpIpv6Packets = %d\n", ethcounters.rxUdpIpv6Packets);
        printf("rxErrPsnPackets = %d\n", ethcounters.rxErrPsnPackets);
        printf("rxPacketsSendToCpu = %d\n", ethcounters.rxPacketsSendToCpu);
        printf("rxPacketsSendToPw = %d\n", ethcounters.rxPacketsSendToPw);
        printf("rxTopPackets = %d\n", ethcounters.rxTopPackets);
        printf("rxPacketDaMis = %d\n", ethcounters.rxPacketDaMis);
    //    printf("rxPauFramePackets = %d\n", ethcounters.rxPauFramePackets);
    }

    return PASSED;
}


int cemEthLoopback(uint8 ethPortId, eAtLoopbackMode lpbkMode)
{
    AtDevice device = DeviceGet();
    AtEthPort ethPort = AtModuleEthPortGet(EthModule(device), ethPortId);

    if(AtChannelLoopbackSet((AtChannel)ethPort, lpbkMode) != cAtOk)
    {
        return (FAILED);
    }
    return (PASSED);
}

int cemForceSetAis(uint8 liuId, uint8 enable)
{
    uint8 slotNumber = (uint8) im_get_current_slot_no();
    AtDevice device = DeviceGet();
    AtModulePdh modulePdh =  PdhModule(device);
    if(enable)
    {
        if (im_type[slotNumber] == IM_DS1)
        {
            AtPdhDe1 de1 = AtModulePdhDe1Get(modulePdh, liuId);
            AtChannelRxAlarmForce((AtChannel)de1, cAtPdhDe1AlarmAis);
        }
        else if (im_type[slotNumber] == IM_DS3)
        {
            AtPdhDe3 de3 = AtModulePdhDe3Get(modulePdh, liuId);
            AtChannelRxAlarmForce((AtChannel)de3, cAtPdhDe3AlarmAis);
        }
    }
    else
    {
        if (im_type[slotNumber] == IM_DS1)
        {
            AtPdhDe1 de1 = AtModulePdhDe1Get(modulePdh, liuId);
            AtChannelRxAlarmUnForce((AtChannel)de1, cAtPdhDe1AlarmAis);
        }
        else if (im_type[slotNumber] == IM_DS3)
        {
            AtPdhDe3 de3 = AtModulePdhDe3Get(modulePdh, liuId);
            AtChannelRxAlarmUnForce((AtChannel)de3, cAtPdhDe3AlarmAis);
        }
    }
    return (PASSED);
}

int cemEnablePdhInterrupt(uint8 enable)
{
    AtDevice device = DeviceGet();
    uint32 liuId;
    uint8 slotNumber = (uint8) im_get_current_slot_no();
    uint8 core_i = 0;

    if(enable)
    {
        //device interrupt enable
        AtIterator coreIterator = AtDeviceCoreIteratorCreate(device);
        for (core_i = 0; core_i < AtDeviceNumIpCoresGet(device); core_i++)
            AtIpCoreInterruptEnable(AtDeviceIpCoreGet(device, core_i), cAtTrue);

        AtObjectDelete((AtObject)coreIterator);

        // pdh interrupt enable
        AtModulePdh modulePdh =  PdhModule(device);
        AtModuleInterruptEnable((AtModule)modulePdh, cAtTrue);

        // pdh de1 interruptmast 1-48 ais en
        // pdh de3 interruptmast 1-48 ais en
        for(liuId=0; liuId<48; liuId++)
        {
            if (im_type[slotNumber] == IM_DS1)
            {
                AtPdhDe1 de1 = AtModulePdhDe1Get(modulePdh, liuId);
                AtChannelInterruptMaskSet((AtChannel)de1, (uint32)(cAtPdhDe1AlarmAis),
                        (uint32)(cAtPdhDe1AlarmAis));
                /* Clear old interrupts, which can keep the line low
                 * after interrupt enable
                 */
                AtChannelAlarmInterruptClear((AtChannel)de1);
            }
            if (im_type[slotNumber] == IM_DS3)
            {
                AtPdhDe3 de3 = AtModulePdhDe3Get(modulePdh, liuId);
                AtChannelInterruptMaskSet((AtChannel)de3, (uint32)(cAtPdhDe1AlarmAis),
                        (uint32)(cAtPdhDe1AlarmAis));
                AtChannelAlarmInterruptClear((AtChannel)de3);
            }
        }
    }
    else
    {
        //device interrupt disable
        AtIterator coreIterator = AtDeviceCoreIteratorCreate(device);
        for (core_i = 0; core_i < AtDeviceNumIpCoresGet(device); core_i++)
            AtIpCoreInterruptEnable(AtDeviceIpCoreGet(device, core_i), cAtFalse);

        AtObjectDelete((AtObject)coreIterator);

        // pdh interrupt disable
        AtModulePdh modulePdh =  PdhModule(device);
        AtModuleInterruptEnable((AtModule)modulePdh, cAtFalse);

        // pdh de1 interruptmast 1-48 ais dis
        // pdh de3 interruptmast 1-48 ais dis
        for(liuId=0; liuId<48; liuId++)
        {
            if (im_type[slotNumber] == IM_DS1)
            {
                AtPdhDe1 de1 = AtModulePdhDe1Get(modulePdh, liuId);
                AtChannelInterruptMaskSet((AtChannel)de1, (uint32)(cAtPdhDe1AlarmAis),
                        (uint32)(0x0));
            }
            if (im_type[slotNumber] == IM_DS3)
            {
                AtPdhDe3 de3 = AtModulePdhDe3Get(modulePdh, liuId);
                AtChannelInterruptMaskSet((AtChannel)de3, (uint32)(cAtPdhDe1AlarmAis),
                        (uint32)(0x0));
            }
        }
    }
    return PASSED;
}

int im_ocn_10g_ds3_config ()
{
    int rc = PASSED;

    tCep cep =
        { 0, cPsnMpls, cAtEthPortInterfaceXGMii, cAtLoopbackModeRelease,
          0,0
    };

    cep.sdhLineNum = 0;
    if (cAtOk != SetupOcnSAToPMplsDe3(cep)) {
    	printf("\nSetupOcnSAToPMplsDe3 Failed for line : %d",0);
        rc = FAILED;
    }
    return rc;
}

int im_ocn_10g_ds3_normal_intr_enable ()
{
    int rc = PASSED;
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);

    Au3InterruptEnable( 0, 0, 0);
    /* Au3TriggerInterrupt( 0, 0, 0); */

    return rc;
}

int im_ocn_10g_ds3_normal_intr_trigger ()
{
    int rc = PASSED;
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);

    Au3TriggerInterrupt( 0, 0, 0);
    return rc;
}

void im_ocn_10g_ds3_sdsf_intr_enable ()
{
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);
    AtDevice device = DeviceGet();

    Au3SdSfInterruptEnable( device, 0, 0, 0);
}

void im_ocn_10g_ds3_sdsf_intr_trigger ()
{
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);
    AtDevice device = DeviceGet();

    Au3SdSfTriggerInterrupt( device, 0, 0, 0);
}

void Im_Ocn_10g_sdsf_Interrupt_Process(void)
    {
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);
    AtDevice device = DeviceGet();

    AtCiscoUpsrInterruptProcess(SdSfBaseAddress(device));
    }

static void Au3InterruptEnable(uint8 lineId, uint8 aug1Id, uint8 au3Id)
    {
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);
    AtDevice device = DeviceGet();
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtSdhLine line = AtModuleSdhLineGet(sdhModule, lineId);
    AtChannel au3 = (AtChannel)AtSdhLineAu3Get(line, aug1Id, au3Id);

    /* Device and module layer need to enabled */
    AtIpCoreInterruptEnable(AtDeviceIpCoreGet(device, 0), cAtTrue);
    AtModuleInterruptEnable((AtModule)sdhModule, cAtTrue);

    /* Make a loopback it it does not exist */
    if (AtChannelLoopbackGet((AtChannel)line) != cAtLoopbackModeLocal)
        AtChannelLoopbackSet((AtChannel)line, cAtLoopbackModeLocal);

    /* Enable AIS, just for example */
    AtChannelInterruptMaskSet(au3, cAtSdhPathAlarmAis, cAtSdhPathAlarmAis);
    }

/* As HyPhy may terminate Pointer, it maybe not possible to detect Pointer and
 * Overhead alarm. Just for testing the interrupt PIN purpose, let's make a
 * TFI-5 local loopback.
 *
 * This function will trigger interrupt by force/unforce AIS alarm */
static void Au3TriggerInterrupt(uint8 lineId, uint8 aug1Id, uint8 au3Id)
    {
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);
    AtDevice device = DeviceGet();
	AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtChannel line = (AtChannel)AtModuleSdhLineGet(sdhModule, lineId);
    AtChannel au3 = (AtChannel)AtSdhLineAu3Get((AtSdhLine)line, aug1Id, au3Id);

    /* Make a loopback it it does not exist */
    if (AtChannelLoopbackGet(line) != cAtLoopbackModeLocal)
        AtChannelLoopbackSet(line, cAtLoopbackModeLocal);

    /* Toggle alarm forcing and interrupt PIN is expected to be raised */
    if (AtChannelTxForcedAlarmGet(au3) & cAtSdhPathAlarmAis) /* If AIS is being forced */
        AtChannelTxAlarmUnForce(au3, cAtSdhPathAlarmAis);    /* Unforce it */
    else /* AIS is not being forced */
        AtChannelTxAlarmForce(au3, cAtSdhPathAlarmAis);      /* Force it */
    }



/* When interrupt happen, it needs to be processed, otherwise, the interrupt
 * PIN will raise forever */
void Im_Ocn_10g_Interrupt_Process(void)
    {
    uint8 slotId = (uint8) im_get_current_slot_no();
    EnterSlot(slotId);
    AtDevice device = DeviceGet();
	AtDeviceInterruptProcess(device);
    }
static uint32 SdSfBaseAddress(AtDevice device)
    {
    return (uint32_t)(AtHalBaseAddressGet(AtDeviceIpCoreHalGet(device, 0)) + (AtCiscoUpsrDwordStartOffset() << 2));
    }

static uint32 FlatStsId(AtDevice device, uint8 lineId, uint8 aug1Id, uint8 au3Id)
    {
    return (lineId * cNumStsInOc48) + (aug1Id * cNumStsInAug1) + au3Id;
    }

static void Au3SdSfInterruptEnable(AtDevice device, uint8 lineId, uint8 aug1Id, uint8 au3Id)
    {
    uint32 baseAddress = SdSfBaseAddress(device);
    uint32 sts1Id = FlatStsId(device, lineId, aug1Id, au3Id);
    uint32 rowId    = sts1Id / cNumBitsInDword;
    uint32 bitIndex = sts1Id % cNumBitsInDword;
    uint32 regVal;

    /* Enable global interrupt. */
    AtCiscoUpsrMASRMaskWrite(baseAddress, 0);

    /* Enable STS SD interrupt */
    regVal  = AtCiscoUpsrStsMaskRead(baseAddress, cAtCiscoUpsrAlarmSd, rowId);
    regVal |= (1 << bitIndex);
    AtCiscoUpsrStsMaskWrite(baseAddress, cAtCiscoUpsrAlarmSd, rowId, regVal);

    /* Enable STS SF interrupt */
    regVal  = AtCiscoUpsrStsMaskRead(baseAddress, cAtCiscoUpsrAlarmSf, rowId);
    regVal |= (1 << bitIndex);
    AtCiscoUpsrStsMaskWrite(baseAddress, cAtCiscoUpsrAlarmSf, rowId, regVal);
    }

/* As HyPhy may terminate Pointer, it maybe not possible to detect Pointer and
 * Overhead alarm. Just for testing the interrupt PIN purpose, let's make a
 * TFI-5 local loopback.
 *
 * This function will trigger interrupt by force/unforce AIS alarm */
static void Au3SdSfTriggerInterrupt(AtDevice device, uint8 lineId, uint8 aug1Id, uint8 au3Id)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtChannel line = (AtChannel)AtModuleSdhLineGet(sdhModule, lineId);
    AtChannel au3 = (AtChannel)AtSdhLineAu3Get((AtSdhLine)line, aug1Id, au3Id);

    /* Make a loopback it it does not exist */
    if (AtChannelLoopbackGet(line) != cAtLoopbackModeLocal)
        AtChannelLoopbackSet(line, cAtLoopbackModeLocal);

    if (AtChannelTxForcedAlarmGet(au3) & cAtSdhPathAlarmAis)
        AtChannelTxAlarmUnForce(au3, cAtSdhPathAlarmAis);
    else
        AtChannelTxAlarmForce(au3, cAtSdhPathAlarmAis);
    }

 void Im_Ocn_10g_Disable_Line_Local_Lpbk(uint8 lineId, uint8 aug1Id, uint8 au3Id)
    {
	    uint8 slotId = (uint8) im_get_current_slot_no();
	    EnterSlot(slotId);
	    AtDevice device = DeviceGet();
        AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
        AtChannel line = (AtChannel)AtModuleSdhLineGet(sdhModule, lineId);

        AtChannelLoopbackSet(line, cAtLoopbackModeRelease);
    }


 void Im_Ocn_10g_Remove_Forced_Ais_Alarm_Au3(uint8 lineId, uint8 aug1Id, uint8 au3Id)
    {
	    uint8 slotId = (uint8) im_get_current_slot_no();
	    EnterSlot(slotId);
	    AtDevice device = DeviceGet();
        AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
        AtChannel line = (AtChannel)AtModuleSdhLineGet(sdhModule, lineId);
        AtChannel au3 = (AtChannel)AtSdhLineAu3Get((AtSdhLine)line, aug1Id, au3Id);

        AtChannelTxAlarmUnForce(au3, cAtSdhPathAlarmAis);
    }

 static void show_prbs_engine_status(uint8 engineId)
 {
     char buffer[100];
     sprintf(buffer, "show prbs engine %d",engineId);
     AtCliExecute(buffer);
 }

static void ClearAradCounters(){
     uint8 ix;
     for (ix = 0; ix < gNoAradDetected; ix++) {
         bshell(ix, "clear c");
     }
 }
static int Im_Ocn_10g_Check_All_Prbs_Engines_Sync(){
	uint8 ix;
    for(ix = 0;ix < IM_OCN_10G_TFI_LINES; ix++) {
    	if(diag_ocn_bert_prbs_check(ix, 1) == FAILED)
    		return FAILED;
    }
    return PASSED;
}


 static void Im_Ocn_10g_Arad_Loopback_Set(eBool set){
     uint8 iy;
     uint8 slotNumber = (uint8) im_get_current_slot_no();
     arad_nif_t arad_port_info;
     char buffer[100];

     for(iy = 0; iy < IM_OCN_10G_BKPLANE_SERDES_COUNT; iy++) {

         im_get_arad_port(slotNumber, iy, &arad_port_info);

         if(set)
             sprintf(buffer, "phy diag xe%d loopback mode=r", arad_port_info.nif_no);
         else
             sprintf(buffer, "phy diag xe%d loopback mode=none", arad_port_info.nif_no);

         bshell(arad_port_info.unit, buffer);
         printf(buffer, "\n");
     }

 }

 static int Im_Ocn_10g_Prbs_start(int show_menu, uint8 tfiPort, uint8 monitoringSide) {

         if (cAtOk != diag_ocn_bert_prbs_start(tfiPort, tfiPort, monitoringSide)) {
             return FAILED;
         }
         else {
             return PASSED;
         }
 }

 static int Im_Ocn_10g_Prbs_re_start(int show_menu, uint8 tfiPort, uint8 monitoringSide) {

         if (cAtOk != diag_ocn_bert_prbs_stop(tfiPort)) {
     	    printf("\nocn bert prbs Stop Failed");
      	     return FAILED;
          }

         return(Im_Ocn_10g_Prbs_start(show_menu, tfiPort, monitoringSide));
 }

 static void Show_Arad_Counters(){
     uint iy;
     for (iy = 0; iy < gNoAradDetected; iy++) {
         bshell(iy, "show c");
         bshell(iy, "g rfcs");
         printf("\n");
     }
 }

 static void Im_Ocn_10G_HyPhy_config(int show_menu) {

     uint8 OCx_config ;
     int rc = PASSED;

     OCx_config = (show_menu & 0xf0);

     if ( OCx_config != OC192_CONFIG ) {
     if (!(diag_global_flag & DIAG_FLAG_EXT_LOOPBACK)) {
         hyphy_param.slice =
             getdec_answer
             ("HAL_PMC_OCn_to_ESSI_CONFIG :Enter the SFP LINK ( Default ALL)\n"
              "    LINK 0 - 7 = 0 - 7 \n" "    LINK ALL   = 8 : ", 8, 0, 8);
         hyphy_param.sonet_mode =
             getdec_answer
             ("HAL_PMC_OCn_to_ESSI_CONFIG :Enter the SONET MODE: \n"
              "  SONET OC3    = 0 \n" "  SONET OC12    = 1 \n" "  SONET OC48   = 2 \n", 2, 0, 2);
         hyphy_param.sfp_group =
             getdec_answer
             ("HAL_PMC_OCn_to_ESSI_CONFIG :Enter the SFP GROUP: \n"
              "  SFP GROUP 1 ( SFP 0~3 <--> TFI 0~3) = 1 \n"
              "  SFP GROUP 2 ( SFP 4~7 <--> TFI 0~3) = 2 \n", 1, 1, 2);
     } else {
           hyphy_param.slice = 8;
         hyphy_param.sonet_mode = 2;
         hyphy_param.sfp_group = (show_menu & 1 ?1:0) +1;
     }
     }
     hyphy_param.enable = 1;

     dev_hyphy_object_t *hyphy_obj;

     hyphy_obj = (dev_hyphy_object_t *) hyphy_get_obj();

     if ( OCx_config != OC192_CONFIG ) {
     if (hyphy_param.slice < 8) {
         rc = hyphy_OCn_ESSI_config(hyphy_obj->fileHndl, hyphy_param.slice,
                                        hyphy_param.sonet_mode,
                                        hyphy_param.enable,
                                        hyphy_param.sfp_group, hyphy_obj->hyphy_param->Lpbk);
     } else {
         rc |=
             hyphy_OCn_ESSI_config(hyphy_obj->fileHndl, hyphy_param.slice,
                                       hyphy_param.sonet_mode,
                                       hyphy_param.enable,
                                       hyphy_param.sfp_group, hyphy_obj->hyphy_param->Lpbk);
     }
     } else {
         rc = hyphy_OC192_ESSI_config(hyphy_obj->fileHndl, 0, hyphy_param.enable, hyphy_obj->hyphy_param->Lpbk);
     }
 }


 static uint8 Im_Ocn_10g_slotToxfi_group(slotNumber) {
     if (IS_BOARD_TYPE_ASR907_RSP3_400()) {
         switch(slotNumber) {
         case 3:
         case 4:
         case 7:
         case 8:
             return 0;
         case 11:
         case 12:
             return 1;
         default :
     	    err('f',0,"unsupported slotNumber %d",slotNumber);
     	    break;
         }
         return 0;
     }
     else {
     	return 0;
     }
 }

 static int Stop_All_Prbs_Engines()
 {
	 uint8 slotId = (uint8) im_get_current_slot_no();
	 EnterSlot(slotId);
	 AtDevice device = DeviceGet();

	 AtModulePrbs prbsModule = (AtModulePrbs) AtDeviceModuleGet(device, cAtModulePrbs);
	 AtPrbsEngine engine;


	 int ix, rc = PASSED;

     for(ix = 0;ix < IM_OCN_10G_TFI_LINES; ix++) {
 	     engine = AtModulePrbsEngineGet(prbsModule, ix);
         if (AtPrbsEngineChannelGet(engine) != NULL) {
   	         if (cAtOk != diag_ocn_bert_prbs_stop(ix)) {
                 err('f', 0, "prbs stop Failed for engine : %d", ix);
                 rc = FAILED;
             }
         }
     }
     return rc;
 }


int Im_Ocn_10g_prbs_test(int show_menu) {

    int ix;
    uint8 xfiGrp;
    uint32 wait_time = 0;
    uint8 sync_failed = 0;

    if (show_menu == FOX_TREE_MAGIC_WORD) {
        return (PASSED);
    }

    int rc = PASSED;

    uint8 slotNumber = (uint8) im_get_current_slot_no();

    if (FALSE == g_arad_init_flag) {
        printf("Warning: Arad in not initialized.First initialize Arad.\n");
        return (FAILED);
    }

    assert(slotNumber < 16);
    assert(im_type[slotNumber] == IM_OCn);


    if (!(diag_global_flag & DIAG_FLAG_EXT_LOOPBACK)) {
        /* Do not run test if EXT_LOOPBACK flag is OFF */
        printf("\nExt Loopback OFF. External Loopback Test skipped.\n");
        return (PASSED);
    }
    START_AGAIN:

    /*which xfi group 0/1*/
    xfiGrp = Im_Ocn_10g_slotToxfi_group(slotNumber);
    /*configure HyPhy for oc48 / 192 mode */
    Im_Ocn_10G_HyPhy_config(show_menu);
    /*configure arrive fpga xfi group*/
    if( IM_Ocn_10G_Configure_Xfi_Group(xfiGrp) != cAtOk) {
        err('f',0,"Error configuring XFI group %d" , xfiGrp);
        return(FAILED);
    }
    /*set arad loopback*/
    Im_Ocn_10g_Arad_Loopback_Set(1);

    if (diag_global_flag & DIAG_FLAG_EDVT_ON)
        wait_time = 60;
    else
        wait_time = getdec_answer("Enter time in seconds for test to run ", 6, 6, 4294967294u);

    /*Start prbs engines*/
    for(ix = 0;ix < IM_OCN_10G_TFI_LINES; ix++) {
        /* Monitoring side TDM*/
        if(FAILED == Im_Ocn_10g_Prbs_start(ix, ix, 1));
    }
    prpass("Waiting 5 sec .......");
    sleep(5);
    /*Remove any sticky during setup*/
    for(ix = 0;ix < IM_OCN_10G_TFI_LINES; ix++)
        diag_ocn_bert_prbs_check(ix, 1);
    sleep(1);
    prpass("Checking pre sync  .......");
    /*Check if sync is achieved if not try to restart and again try*/
    for(ix = 0;ix < IM_OCN_10G_TFI_LINES; ix++) {
        if(diag_ocn_bert_prbs_check(ix, 1) == FAILED) {
            printf("prbs sync Failed for engine : %d..iteration 1", ix);
            if(sync_failed){
                Stop_All_Prbs_Engines();
                err('f',0,"PRBS FAILED for engine : %d",ix);
                rc = FAILED;
                goto EXIT;
            }
            else{
                Stop_All_Prbs_Engines();
                printf("\nTry Again\n");
                ++sync_failed;
                goto START_AGAIN;
            }
        }
    }

    ClearAradCounters();
    DisplayAndClearCounters(1, 0, 3, 0);
    for(ix = 0;ix < 2; ix++) {
        Im_Ocn_10g_Cem_ClearCountersAndDisaplay(0, ix);
    }
    printf("\n");
    prpass("Checking sync  .......");
    /*TDM PRBS checking Start*/
    do {
        for(ix = 0;ix < IM_OCN_10G_TFI_LINES; ix++) {
            if (diag_ocn_bert_prbs_check(ix, 1) == FAILED) {
                err('f', 0, "%s: TDM PRBS check failed for Line : %d, ", __FUNCTION__, ix);
                rc = FAILED;
                Show_Arad_Counters();
                Im_Ocn_10g_Cem_ClearCountersAndDisaplay(1,(ix<2) ? 0:1);
                show_prbs_engine_status(ix+1);
                DisplayAndClearCounters(1, ix, ix, 1);
                rc = FAILED;
                goto EXIT;
            }
        }
        prpass("Time :%5d", wait_time);
        sleep(1);
    } while (wait_time--);
        printf("\n");
    /*PSN PRBS starts*/
        for(ix = 0;ix < IM_OCN_10G_TFI_LINES; ix++) {
            /* Monitoring side PSN*/
            //if (cAtOk != Im_Ocn_10g_Prbs_Chg_Monitoring_side (ix, 0)) {
            if (FAILED == Im_Ocn_10g_Prbs_re_start(ix, ix, 0)) {
                err('f', 0, "Monitoring side change Failed for engine : %d", ix);
                rc = FAILED;
                goto EXIT;
            }
        }
        sleep(5);
        /*Remove any sticky during setup*/
        for(ix = 0;ix < IM_OCN_10G_TFI_LINES; ix++)
            diag_ocn_bert_prbs_check(ix, 1);

        ClearAradCounters();
        DisplayAndClearCounters(1, 0, 3, 0);
        for(ix = 0;ix < 2; ix++) {
            Im_Ocn_10g_Cem_ClearCountersAndDisaplay(0, ix);
        }
        if (diag_global_flag & DIAG_FLAG_EDVT_ON)
            wait_time = 60;
        else
            wait_time = getdec_answer("Enter time in seconds for test to run - PSN Side ", 6, 6, 4294967294u);
        printf("\n");
        /*PSN PRBS checking Start*/
        do {
            for(ix = 0;ix < IM_OCN_10G_TFI_LINES; ix++) {
                if (diag_ocn_bert_prbs_check(ix, 1) == FAILED) {
                    err('f', 0, "%s: PSN PRBS check failed for Line : %d, ", __FUNCTION__, ix);
                    rc = FAILED;
                    Im_Ocn_10g_Cem_ClearCountersAndDisaplay(1,(ix<2)?0:1);
                    show_prbs_engine_status(ix+1);
                    DisplayAndClearCounters(1, ix, ix, 1);
                    Show_Arad_Counters();
                    rc = FAILED;
                    goto EXIT;
                }
            }
            prpass("Time :%5d", wait_time);
            sleep(1);
        } while (wait_time--);

    printf("\n");

    EXIT:
    prpass("Cleaninig mess  .......");
    Im_Ocn_10g_Arad_Loopback_Set(0);

    for(ix = 0;ix < IM_OCN_10G_TFI_LINES; ix++) {
        Stop_All_Prbs_Engines();
    }

    return (rc);
}

int Im_Ocn_10g_prbs_continuous_test_start(int show_menu) {

    int ix = 0;
    uint8 OCx_config;
    uint lockCheck = 0;
    uint8 monitoringSide;
    uint8 xfiGrp, sfpGrp;
    uint8 timeout;

    if (show_menu == FOX_TREE_MAGIC_WORD) {
        return (PASSED);
    }

    xfiGrp = gethex_answer("Enter Eth serdes group no , 0:1 -> 0 2:3 -> 1 : ", 0, 0, 1);
    sfpGrp = gethex_answer("Enter SFP group , 0:3 -> 0 4:7 -> 1 : ", 0, 0, 1);
    OCx_config = gethex_answer("OC192 Mode, 0-> No 1 -> Yes", 0, 0, 1)?(1<<4):0;
    monitoringSide = gethex_answer("Select Monitoring Side 0-PSN  1- TDM ", 0, 0, 1);

    /*int liu_mode, loopback_mode, value; */

    uint8 slotNumber = (uint8) im_get_current_slot_no();

    assert(slotNumber < 16);
    assert(im_type[slotNumber] == IM_OCn);

    if (!(diag_global_flag & DIAG_FLAG_EXT_LOOPBACK)) {
        printf("\nExt Loopback OFF. External Loopback Test skipped.\n");
        return (PASSED);
    }

    if (FALSE == g_arad_init_flag) {
        printf("Warning: Arad in not initialized.First initialize Arad.\n");
        return (FAILED);
    }

    ClearAradCounters();

    show_menu |= sfpGrp;
    show_menu |= OCx_config;

    Im_Ocn_10G_HyPhy_config(show_menu);

#if 0
    if (rc) {
    	printf("\nHyPhy ocn essi config Failed ..instance 1");
        goto CLEANUP;
     }
#endif

    IM_Ocn_10G_Configure_Xfi_Group(xfiGrp);

    Im_Ocn_10G_HyPhy_config(show_menu);

    do {
        if (checkSerdesPll() == PASSED)
            break;
        lockCheck++;
    } while (lockCheck < 100);

    if (lockCheck >= 100) {
    	printf("\n%s: CEM Eth SERDES PLL not locked", __FUNCTION__);
        return FAILED;
    }

    //arad_serdes_link_reset(im_slot);
    Im_Ocn_10g_Arad_Loopback_Set(1);

    /* Reset FPGA XFI serdes */
    for(ix = 0;ix < 4; ix++) {
            //DiagSerdesControllerReset(ix);

            if (cAtOk != diag_ocn_bert_prbs_start(ix, ix, monitoringSide)) {
                Show_Arad_Counters();
                err('f',0,"\nprbs start Failed for engine : %d", ix);
                goto CLEANUP;
            }
    }

    printf("waiting 5 sec...\r\n");
    sleep(5);

    timeout = 60;
    /*check 1 more min*/
    while(--timeout) {
    	sleep(1);
    	if(Im_Ocn_10g_Check_All_Prbs_Engines_Sync() == FAILED)
    		continue;
    	else
    		break;
    }
    if(!timeout)
    	printf("WARNING !!! PRBS sync still not achieved !!!");

    /* Set start to 1 */
    continuousStart[im_slot] = 1;
    return (PASSED);

  CLEANUP:

  Im_Ocn_10g_Arad_Loopback_Set(0);

  return (FAILED);


}

int Im_Ocn_10g_prbs_continuous_test_stop (int param)
{
    char buffer[100];
    arad_nif_t arad_port_info = aradPortIinfoContinuousTest[im_slot];
    uint8 ix;
    int ret = PASSED;

    if (param == FOX_TREE_MAGIC_WORD) {
        return (PASSED);
    }
    printf("\n");

    if (continuousStart[im_slot] == 1) {
    	 for(ix = 0;ix < 4; ix++) {
    	     if (cAtOk != diag_ocn_bert_prbs_stop(ix)) {
    	    	 printf("\nocn bert prbs Stop Failed");
    	    	 ret = FAILED;
             }
    	 }
        for (ix = 0; ix < 2; ix++) {
            im_get_arad_port(im_slot, ix , &arad_port_info);
            sprintf(buffer, "phy diag xe%d loopback mode=none",
                    arad_port_info.nif_no);
            bshell(arad_port_info.unit, buffer);
            sleep(1);
        }

        continuousStart[im_slot] = 0;
    } else {
        printf("Continuous test not running!!\n");
    }
    return (ret);
}

int Im_Ocn_10g_prbs_continuous_test_Check (int param)
{
    uint8 ix;

    if (param == FOX_TREE_MAGIC_WORD) {
        return (PASSED);
    }
    printf("\n");

    if (continuousStart[im_slot] == 1) {
    	for(ix = 0;ix < 4; ix++) {
            if (PASSED != diag_ocn_bert_prbs_check(ix, 1)) {
            	err('f', 0, "%s PSN  PRBS check failed for engine %d", __FUNCTION__, ix);
            	return (FAILED);
            }
    	}
        printf("No alarms\n");
    } else {
        printf("Continuous test not running\n");
    }
    return (PASSED);
}

/*------------------------------------------------------------------
 * diag_arrive.c :  C file for Arrive CEM/CES FPGA support.
 *
 * May 2015, Yasdixit
 *
 * Copyright (c) 2015-2016 by Cisco Systems, Inc.
 *
 * Description : Cisco sample code implementation
 *               There are two main functions in this file:
 *               - DiagnosticSetup: to emulate all of VC-12s of one line over
 *                 UDP/IP via a specified ETH Port.
 *               - DiagnosticTearDown: To tear down all of emulated circuits
 *                 setup by the DiagnosticSetup()
 *------------------------------------------------------------------
 */
 
/*--------------------------- Include files ----------------------------------*/
#include <AtDriver.h>
#include <AtEthPort.h>
#include <AtSdhLine.h>
#include <AtSdhAug.h>
#include <AtSdhVc.h>
#include <AtSdhTug.h>
#include <AtPw.h>
#include <AtPdhDe1.h>
#include <AtPdhDe3.h>
#include "AtModuleSdh.h"
#include "AtClasses.h"
#include "diag_arrive.h"
#include "AtObject.h"
#include "AtPdhSerialLine.h"

#include "commacro.h"
#include "AtCisco.h"
#include "AtChannel.h"

#include "AtModuleRam.h"
#include "AtRam.h"
#include "commacro.h"


/*--------------------------- Define -----------------------------------------*/
#define cNumAug4InAug16 4
#define cNumAug1InAug16 16
#define cNumVc3InAug1   3
#define cNumTug2InVc3   7
#define cNumVc12InTug2  3


/*--------------------------- Macros -----------------------------------------*/
#define mSuccessAssert(ret)                                                    \
    do                                                                         \
        {                                                                      \
        eAtRet ret__ = ret;                                                    \
        if (ret__ != cAtOk)                                                    \
            return ret__;                                                      \
        }while(0)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

extern tCiscoIm m_ims[];
/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice DeviceGet(void)    {

    return AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
}

static AtModuleSdh SdhModule(AtDevice device)    {
	
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
}

static AtModuleEth EthModule(AtDevice device)    {
	
    return (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
}

static AtModulePw PwModule(AtDevice device)    {
	
    return (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
}

static uint16 PwIdFromSdhChannelId(uint16 aug1Id, uint8 vc3Id, uint8 tug2Id, uint8 vc1xId, uint16 lineId)    {
	
    /* TODO: just an example, user can replace it by another formula */
    return (aug1Id * 63) + (vc3Id * 21) + (tug2Id * 3)  + vc1xId + lineId;
}

static uint8 *DefaultEthPortSourceMac(uint32 ethPortId)    {
	
    /* TODO: just an example, user can replace it by another value */
    static uint8 buf[6] = {0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA};
    return buf;
}

static uint8 *DefaultEthPortSourceIpv4(uint32 ethPortId)    {
	
    /* TODO: just an example, user can replace it by another value */
    static uint8 buf[4] = {172, 0, 0, 0};
    return buf;
}

static uint8 *DefaultPwDestMac(uint32 ethPortId)    {
	
    /* TODO: just an example, user can replace it by another value */
    static uint8 buf[6] = {0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA};
    return buf;
}

static uint8 *DefaultPwSourceIpv4(uint32 ethPortId)    {
	
    /* TODO: just an example, user can replace it by another value */
    static uint8 buf[4] = {172, 0, 0, 0};
    return buf;
}

static uint8 *DefaultPwDestIpv4(uint32 ethPortId)    {
	
    /* TODO: just an example, user can replace it by another value */
    static uint8 buf[4] = {172, 0, 0, 0};
    return buf;
}

static uint32 DefaultPwUdpSourcePort(uint32 pwId)    {
	
    /* TODO: just an example, user can replace it by another value */
    return pwId;
}

static uint32 DefaultPwUdpDestPort(uint32 pwId)    {
	
    /* TODO: just an example, user can replace it by another value */
    return pwId;
}

static uint32 DefaultPwUdpExpectedPort(uint32 pwId)    {
	
    /* TODO: just an example, user can replace it by another value */
    return pwId;
}

static uint32 DefaultPwPayloadSize(uint32 pwId)    {
	
    /* TODO: just an example, user can replace it by another value */
    return 140;
}

static AtPwPsn UdpIpv4PsnCreate(uint16 pwId, uint16 ethPortId) {
	
    AtPwUdpPsn udpPsn   = AtPwUdpPsnNew();
    AtPwIpV4Psn ipv4Psn = AtPwIpV4PsnNew();

    /* Setup UDP */
    AtPwUdpPsnSourcePortSet(udpPsn, DefaultPwUdpSourcePort(pwId));
    AtPwUdpPsnDestPortSet(udpPsn, DefaultPwUdpDestPort(pwId));
    AtPwUdpPsnExpectedPortSet(udpPsn, DefaultPwUdpExpectedPort(pwId));

    /* Setup IP */
    AtPwIpPsnSourceAddressSet((AtPwIpPsn)ipv4Psn, DefaultPwSourceIpv4(ethPortId));
    AtPwIpPsnDestAddressSet((AtPwIpPsn)ipv4Psn, DefaultPwDestIpv4(ethPortId));

    /* TODO: can set other attributes of each PSN here ... */

    /* Build PSN hierarchy */
    AtPwPsnLowerPsnSet((AtPwPsn)udpPsn, (AtPwPsn)ipv4Psn);

    return (AtPwPsn)udpPsn;
}

static AtPwPsn MplsPsnCreate(uint16 pwId, uint16 ethPortId) {
	
    AtPwMplsPsn mplsPsn = AtPwMplsPsnNew();
    tAtPwMplsLabel label;

    /* Make default label content, just for simple, the PW ID is used as label,
     * experimental field is set to 0 and time-to-live to 255 */
     /* CISCO modified , pw was undefined */
    AtPwMplsLabelMake((AtChannel)pwId, 0, 255, &label);

    /* Setup label stack:
     * - Inner label: is the label has S bit set to 1
     * - Outter labels: is labels have S bit set to 0
     * These labels are for MPLS packets transmitted to Ethernet side. Application
     * can flexible configure any labels.
     */
    AtPwMplsPsnInnerLabelSet(mplsPsn, &label);
    AtPwMplsPsnOuterLabelAdd(mplsPsn, &label);

    /* For direction from ETH to TDM, when MPLS packets are received, their
     * inner label (S = 1) are used to look for Pseudowire that they belong to.
     * The following line is to setup */
     /* CISCO modified , pw was undefined */
    AtPwMplsPsnExpectedLabelSet(mplsPsn, AtChannelIdGet((AtChannel)pwId));

    /* Apply this MPLS for this PW. Note, since PSN object is allocated in this
     * context, so it should also be deleted in this context. The internal
     * implementation already clones it. */
/*    AtPwPsnSet(pw, (AtPwPsn)mplsPsn); */
/*    AtObjectDelete((AtObject)mplsPsn); */
    return (AtPwPsn)mplsPsn;
}


/*
 * Example function to emulate all of VC-12s of one line over UDP/IP via a
 * specified ETH Port
 *
 * @param sdhLineId SDH Line ID
 * @param ethPortId ETH Port ID
 *
 * @return AT return code
 */
eAtRet diagnosticSetup(uint16 sdhLineId, uint16 ethPortId) {

    uint16 aug4Id;
    uint16 aug1Id;
    AtPwPsn psn;
    AtDevice device = DeviceGet();
    AtModulePw  pwModule  = PwModule(device);
    AtEthPort ethPort = AtModuleEthPortGet(EthModule(device), ethPortId);
    AtSdhLine line = AtModuleSdhLineGet(SdhModule(device), sdhLineId);
    AtSdhAug aug16 = AtSdhLineAug16Get(line, 0);

    mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel)(aug16), cAtSdhAugMapTypeAug16Map4xAug4s));

    /* Make AUG-4 contains AUG-1s */
    for (aug4Id = 0; aug4Id < cNumAug4InAug16; aug4Id++)
        {
        AtSdhAug aug4 = AtSdhLineAug4Get(line, aug4Id);
        mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel)(aug4), cAtSdhAugMapTypeAug4Map4xAug1s));
        }

    /* Make each AUG-1 contains AU-3 <--> VC-3 */
    for (aug1Id = 0; aug1Id < cNumAug1InAug16; aug1Id++)
        {
        uint16 vc3Id;
        AtSdhAug aug1 = AtSdhLineAug1Get(line, aug1Id);
        mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel)(aug1), cAtSdhAugMapTypeAug1Map3xVc3s));

        /* Make each VC-3 contains TUG-2s */
        for (vc3Id = 0; vc3Id < cNumVc3InAug1; vc3Id++)
            {
            AtSdhVc vc3 = AtSdhLineVc3Get(line, aug1Id, vc3Id);
            uint16 tug2Id;
            mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel)(vc3), cAtSdhVcMapTypeVc3Map7xTug2s));

            /* Make each TUG-2 contains Tu12s */
            for (tug2Id = 0; tug2Id < cNumTug2InVc3; tug2Id++)
                {
                uint16 pwId;
                uint16 vc1xId;
                AtSdhTug tug2 = AtSdhLineTug2Get(line, aug1Id, vc3Id, tug2Id);
                mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel)(tug2), cAtSdhTugMapTypeTug2Map3xTu12s));

                /* For each VC12, emulate over PSN */
                for (vc1xId = 0; vc1xId < cNumVc12InTug2; vc1xId++)
                    {
                    AtPw pw;
                    AtSdhVc vc12 = AtSdhLineVc1xGet(line, aug1Id, vc3Id, tug2Id, vc1xId);
                    mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel)(vc12), cAtSdhVcMapTypeVc1xMapC1x));

                    pwId = PwIdFromSdhChannelId(aug1Id, vc3Id, tug2Id, vc1xId, sdhLineId);
                    pw   = (AtPw)AtModulePwCepCreate(pwModule, pwId, cAtPwCepModeBasic);

                    mSuccessAssert(AtPwPayloadSizeSet(pw, DefaultPwPayloadSize(pwId)));
                    mSuccessAssert(AtPwCircuitBind(pw, (AtChannel)vc12));
                    mSuccessAssert(AtPwEthPortSet(pw, ethPort));

                    /* Configure Ethernet header with default MAC and no VLAN */
                    mSuccessAssert(AtPwEthHeaderSet(pw, DefaultPwDestMac(ethPortId), NULL, NULL));

                    /* Create UDP over IPv4 PSN */
                    psn = UdpIpv4PsnCreate(pwId, ethPortId);
                    mSuccessAssert(AtPwPsnSet(pw, psn));

                    mSuccessAssert(AtChannelEnable((AtChannel)(pw), cAtTrue));

                    /* Clean objects */
                    AtObjectDelete((AtObject)(psn));
                    }
                }
            }
        }

    /* Set Source MAC and IP for Ethernet Port */
    mSuccessAssert(AtEthPortSourceMacAddressSet(ethPort, DefaultEthPortSourceMac(ethPortId)));
    mSuccessAssert(AtEthPortIpV4AddressSet(ethPort, DefaultEthPortSourceIpv4(ethPortId)));
    mSuccessAssert(AtChannelLoopbackSet((AtChannel)(ethPort), cAtLoopbackModeRelease));

    return cAtOk;
}

/*
 * To tear down all of emulated circuits setup by the DiagnosticSetup()
 *
 * @param sdhLineId SDH Line ID
 * @param ethPortId ETh Port ID
 *
 * @return AT return code
 */
eAtRet diagnosticTearDown(uint16 sdhLineId, uint16 ethPortId) {

    uint16 aug1Id;
    AtModulePw pwModule = PwModule(DeviceGet());

    for (aug1Id = 0; aug1Id < cNumAug1InAug16; aug1Id++)
        {
        uint16 vc3Id;

        for (vc3Id = 0; vc3Id < cNumVc3InAug1; vc3Id++)
            {
            uint16 tug2Id;

            for (tug2Id = 0; tug2Id < cNumTug2InVc3; tug2Id++)
                {
                uint16 vc1xId;

                for (vc1xId = 0; vc1xId < cNumVc12InTug2; vc1xId++)
                    {
                    uint16 pwId = PwIdFromSdhChannelId(aug1Id, vc3Id, tug2Id, vc1xId, sdhLineId);
                    AtPw pw = (AtPw)AtModulePwGetPw(pwModule, pwId);
                    if (pw == NULL)
                        continue;

                    mSuccessAssert(AtPwCircuitUnbind(pw));
                    mSuccessAssert(AtModulePwDeletePw(pwModule, pwId));
                    }
                }
            }
        }

    return cAtOk;
}

/*------------------------------------------------------------------------------
 *
 *
 * Description : This source file is to show how a CEP PW is setup to emulate
 *               SDH VC (VC-4/VC-3/VC-1x) to Packet Switch Network
 *
 *               For the sake of simplicity, this source file makes a default
 *               mapping on SDH layer to carry all kinds of VC mentioned above.
 *               And Line#1 is used.
 *
 *               There are 16 AUG-1s in STM-16 and 3 AUG-1s are used in this
 *               example. There mappings are as following:
 *               - AUG-1#1: VC-4
 *               - AUG-1#2: AU-4 <--> VC-4 <--> TUG-3 <--> VC-3
 *               - AUG-1#3: AU-4 <--> VC-4 <--> TUG-3 <--> TUG-2 <--> VC-12
 *
 *               And this source file will emulate 3 VCs onto 3 CEP PW, they are:
 *               - VC-4: which is contained in AUG-1#1
 *               - The first VC-3  in AUG-1#2
 *               - The first VC-12 in AUG-1#3
 *
 *               All of these PWs will use MPLS PSN and Ethernet Port 1 is used
 *               as Ethernet physical.
 *
 * Notes       : CLIs to configure this flow are added at the end of this file
 *----------------------------------------------------------------------------*/


/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(void) {

    return AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
}

static AtSdhLine Line1(void) {
	
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(Device(), cAtModuleSdh);
    return AtModuleSdhLineGet(sdhModule, 0);
}

/*
 * Mapping detail for line 1
 * - AUG-1#1: VC-4
 * - AUG-1#2: AU-4 <--> VC-4 <--> TUG-3 <--> VC-3
 * - AUG-1#3: AU-4 <--> VC-4 <--> TUG-3 <--> TUG-2 <--> VC-12
 */
static void SetupSdhMapping(void) {

    AtSdhLine line1 = Line1();
    uint8 aug1Id;
    AtSdhChannel aug1, vc4, vc3, vc12, tug3, tug2;

    /* Setup mapping for AUG-1#1 to map VC-4 */
    aug1Id = 0;
    aug1 = (AtSdhChannel)AtSdhLineAug1Get(line1, aug1Id);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1MapVc4);
    vc4 = (AtSdhChannel)AtSdhLineVc4Get(line1, aug1Id);
    AtSdhChannelMapTypeSet(vc4, cAtSdhVcMapTypeVc4MapC4);

    /* Setup mapping for AUG-1#2 to map AU-4 <--> VC-4 <--> TUG-3 <--> VC-3 */
    aug1Id = 1;
    aug1 = (AtSdhChannel)AtSdhLineAug1Get(line1, aug1Id);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1MapVc4);
    vc4 = (AtSdhChannel)AtSdhLineVc4Get(line1, aug1Id);
    AtSdhChannelMapTypeSet(vc4, cAtSdhVcMapTypeVc4Map3xTug3s);
    tug3 = (AtSdhChannel)AtSdhLineTug3Get(line1, aug1Id, 0); /* Use the first one for this example */
    AtSdhChannelMapTypeSet(tug3, cAtSdhTugMapTypeTug3MapVc3);
    vc3 = (AtSdhChannel)AtSdhLineVc3Get(line1, aug1Id, 0);
    AtSdhChannelMapTypeSet(vc3, cAtSdhVcMapTypeVc3MapC3);

    /* Setup mapping for AUG-1#3 to map AU-4 <--> VC-4 <--> TUG-3 <--> TUG-2 <--> VC-12 */
    aug1Id = 2;
    aug1 = (AtSdhChannel)AtSdhLineAug1Get(line1, aug1Id);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1MapVc4);
    vc4 = (AtSdhChannel)AtSdhLineVc4Get(line1, aug1Id);
    AtSdhChannelMapTypeSet(vc4, cAtSdhVcMapTypeVc4Map3xTug3s);
    tug3 = (AtSdhChannel)AtSdhLineTug3Get(line1, aug1Id, 0); /* Use the first one for this example */
    AtSdhChannelMapTypeSet(tug3, cAtSdhTugMapTypeTug3Map7xTug2s);
    tug2 = (AtSdhChannel)AtSdhLineTug2Get(line1, aug1Id, 0, 0); /* Use the first one for this example */
    AtSdhChannelMapTypeSet(tug2, cAtSdhTugMapTypeTug2Map3xTu12s);
    vc12 = (AtSdhChannel)AtSdhLineVc1xGet(line1, aug1Id, 0, 0, 0); /* Use the first one for this example */
    AtSdhChannelMapTypeSet(vc12, cAtSdhVcMapTypeVc1xMapC1x);
}

static void SetupPwMpls(AtPw pw) {
	
    AtPwMplsPsn mplsPsn = AtPwMplsPsnNew();
    tAtPwMplsLabel label;

    /* Make default label content, just for simple, the PW ID is used as label,
     * experimental field is set to 0 and time-to-live to 255 */
    AtPwMplsLabelMake(AtChannelIdGet((AtChannel)pw), 0, 255, &label);

    /* Setup label stack:
     * - Inner label: is the label has S bit set to 1
     * - Outter labels: is labels have S bit set to 0
     * These labels are for MPLS packets transmitted to Ethernet side. Application
     * can flexible configure any labels.
     */
    AtPwMplsPsnInnerLabelSet(mplsPsn, &label);
    AtPwMplsPsnOuterLabelAdd(mplsPsn, &label);

    /* For direction from ETH to TDM, when MPLS packets are received, their
     * inner label (S = 1) are used to look for Pseudowire that they belong to.
     * The following line is to setup */
    AtPwMplsPsnExpectedLabelSet(mplsPsn, AtChannelIdGet((AtChannel)pw));

    /* Apply this MPLS for this PW. Note, since PSN object is allocated in this
     * context, so it should also be deleted in this context. The internal
     * implementation already clones it. */
    AtPwPsnSet(pw, (AtPwPsn)mplsPsn);
    AtObjectDelete((AtObject)mplsPsn);
}

/* This function is to setup DMAC and VLANs for Ethernet frame transmitted to
 * Ethernet port. In this example, one VLAN is used. Ethernet port 1 is used for
 * physical layer */
static void SetupPwMac(AtPw pw) {

    static uint8 mac[] = {0xCA, 0xFE, 0xCA, 0xFE, 0xCA, 0xFE}; /* Application has to know */
    static tAtEthVlanTag cVlan;
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(Device(), cAtModuleEth);

    AtEthVlanTagConstruct(0, 0, AtChannelIdGet((AtChannel)pw), &cVlan);

    AtPwEthPortSet(pw, AtModuleEthPortGet(ethModule, 0));
    AtPwEthHeaderSet(pw, mac, &cVlan, NULL);
}

/* This function make a default setup for PW:
 * - MPLS information
 * - MAC information
 */
static void SetupPw(AtPw pw) {

    SetupPwMpls(pw);
    SetupPwMac(pw);
}

static uint32 De1Id(void) {
	
    /* Use first DE1 for demonstration purpose */
    return 0;
}

static uint32 De3Id(void) {
	
    /* Use first DE1 for demonstration purpose */
    return 0;
}
static AtModulePdh PdhModule(void) {
	
    return (AtModulePdh)AtDeviceModuleGet(Device(), cAtModulePdh);
}

static AtPdhDe1 PdhDe1Get(uint32 de1Id) {
	
    return AtModulePdhDe1Get(PdhModule(), de1Id);
}

static AtPdhDe3 PdhDe3Get(uint32 de3Id){
    return AtModulePdhDe3Get(PdhModule(), de3Id);
}

static eAtPdhDe3FrameType DefaultDe3FrameType(void) {
    return cAtPdhE3Unfrm;
}

static eAtPdhDe3SerialLineMode DefaultSerialLineMode(void) {
    /* Use this mode for LIU - DE3 */
    return cAtPdhDe3SerialLineModeE3;
}


static eAtPdhDe1FrameType DefaultDe1FrameType(void) {
    return cAtPdhE1UnFrm;
}



void SetupLiuSAToPMplsDe1(tLiuSatopDe1 d1   ) {
    AtPw pw;
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(Device(), cAtModulePw);
    AtPdhChannel de1 = (AtPdhChannel)PdhDe1Get(De1Id());

    /* Set default frame type for DE1 */
    AtPdhChannelFrameTypeSet(de1, DefaultDe1FrameType());

    /* Create a SAToP to emulate this E1 */
	/* FIXME : Cisco modified putting pwid as 1 */
    pw = (AtPw)AtModulePwSAToPCreate(pwModule, 1);
    AtPwCircuitBind(pw, (AtChannel)de1);
    /* TODO : Fix pw setup with proper ethernet and port*/
    SetupPw(pw);
    AtChannelEnable((AtChannel)pw, cAtTrue);
}

void SetupLiuSAToPMplsDe3(tLiuSatopDe3 d3   ) {
    AtPw pw;
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(Device(), cAtModulePw);
    AtPdhSerialLine serialLine = AtModulePdhDe3SerialLineGet(PdhModule(), De3Id());
    AtPdhChannel de3 = (AtPdhChannel)PdhDe3Get(De1Id());

    /* Set default serial line mode */
    AtPdhSerialLineModeSet(serialLine, DefaultSerialLineMode());

    /* Set default frame type for DE1 */
    AtPdhChannelFrameTypeSet(de3, DefaultDe3FrameType());

    /* Create a SAToP to emulate this E1 */
	/* FIXME : Cisco modified putting pwid as 1 */
    pw = (AtPw)AtModulePwSAToPCreate(pwModule, 1);
    AtPwCircuitBind(pw, (AtChannel)de3);
    /* TODO : Fix pw setup with proper ethernet and port*/
    SetupPw(pw);
    AtChannelEnable((AtChannel)pw, cAtTrue);
}

eAtRet EnableLiuSAToPMplsPwChannel(uint16 ChannelId) {

    AtPw pw;
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(Device(), cAtModulePw);

    pw = (AtPw)AtModulePwGetPw(pwModule, ChannelId);

    AtChannelEnable((AtChannel)pw, cAtTrue);

    return cAtOk;
}

eAtRet DisableLiuSAToPMplsPwChannel(uint16 ChannelId) {

    AtPw pw;
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(Device(), cAtModulePw);

    pw = (AtPw)AtModulePwGetPw(pwModule, ChannelId);

    AtChannelEnable((AtChannel)pw, cAtFalse);

    return cAtOk;
}


eAtRet SetupCepUdp(uint16 sdhLineId, uint16 ethPortId) {

    uint16 aug4Id;
    uint16 aug1Id;
    AtPwPsn psn;
    AtDevice device = DeviceGet();
    AtModulePw  pwModule  = PwModule(device);
    AtEthPort ethPort = AtModuleEthPortGet(EthModule(device), ethPortId);
    AtSdhLine line = AtModuleSdhLineGet(SdhModule(device), sdhLineId);
    AtSdhAug aug16 = AtSdhLineAug16Get(line, 0);

    mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel)(aug16), cAtSdhAugMapTypeAug16Map4xAug4s));

    /* Make AUG-4 contains AUG-1s */
    for (aug4Id = 0; aug4Id < cNumAug4InAug16; aug4Id++)        {
        AtSdhAug aug4 = AtSdhLineAug4Get(line, aug4Id);
        mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel)(aug4), cAtSdhAugMapTypeAug4Map4xAug1s));
        }

    /* Make each AUG-1 contains AU-3 <--> VC-3 */
    for (aug1Id = 0; aug1Id < cNumAug1InAug16; aug1Id++)
        {
        uint16 vc3Id;
        AtSdhAug aug1 = AtSdhLineAug1Get(line, aug1Id);
        mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel)(aug1), cAtSdhAugMapTypeAug1Map3xVc3s));

        /* Make each VC-3 contains TUG-2s */
        for (vc3Id = 0; vc3Id < cNumVc3InAug1; vc3Id++)
            {
            AtSdhVc vc3 = AtSdhLineVc3Get(line, aug1Id, vc3Id);
            uint16 tug2Id;
            mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel)(vc3), cAtSdhVcMapTypeVc3Map7xTug2s));

            /* Make each TUG-2 contains Tu12s */
            for (tug2Id = 0; tug2Id < cNumTug2InVc3; tug2Id++)
                {
                uint16 pwId;
                uint16 vc1xId;
                AtSdhTug tug2 = AtSdhLineTug2Get(line, aug1Id, vc3Id, tug2Id);
                mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel)(tug2), cAtSdhTugMapTypeTug2Map3xTu12s));

                /* For each VC12, emulate over PSN */
                for (vc1xId = 0; vc1xId < cNumVc12InTug2; vc1xId++)
                    {
                    AtPw pw;
                    AtSdhVc vc12 = AtSdhLineVc1xGet(line, aug1Id, vc3Id, tug2Id, vc1xId);
                    mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel)(vc12), cAtSdhVcMapTypeVc1xMapC1x));

                    pwId = PwIdFromSdhChannelId(aug1Id, vc3Id, tug2Id, vc1xId, sdhLineId);
                    pw   = (AtPw)AtModulePwCepCreate(pwModule, pwId, cAtPwCepModeBasic);

                    mSuccessAssert(AtPwPayloadSizeSet(pw, DefaultPwPayloadSize(pwId)));
                    mSuccessAssert(AtPwCircuitBind(pw, (AtChannel)vc12));
                    mSuccessAssert(AtPwEthPortSet(pw, ethPort));

                    /* Configure Ethernet header with default MAC and no VLAN */
                    mSuccessAssert(AtPwEthHeaderSet(pw, DefaultPwDestMac(ethPortId), NULL, NULL));

                    /* Create UDP over IPv4 PSN */
                    psn = UdpIpv4PsnCreate(pwId, ethPortId);
                    mSuccessAssert(AtPwPsnSet(pw, psn));
                    /* Masking enabling channel as we wan to give control to use */
                    /* mSuccessAssert(AtChannelEnable((AtChannel)(pw), cAtTrue));*/

                    /* Clean objects */
                    AtObjectDelete((AtObject)(psn));
                    }
                }
            }
        }

    /* Set Source MAC and IP for Ethernet Port */
    mSuccessAssert(AtEthPortSourceMacAddressSet(ethPort, DefaultEthPortSourceMac(ethPortId)));
    mSuccessAssert(AtEthPortIpV4AddressSet(ethPort, DefaultEthPortSourceIpv4(ethPortId)));
    mSuccessAssert(AtChannelLoopbackSet((AtChannel)(ethPort), cAtLoopbackModeRelease));

    return cAtOk;
}

eAtRet SetupCepMpls(uint16 sdhLineId, uint16 ethPortId) {

    uint16 aug4Id;
    uint16 aug1Id;
    AtPwPsn psn;
    AtDevice device = DeviceGet();
    AtModulePw  pwModule  = PwModule(device);
    AtEthPort ethPort = AtModuleEthPortGet(EthModule(device), ethPortId);
    AtSdhLine line = AtModuleSdhLineGet(SdhModule(device), sdhLineId);
    AtSdhAug aug16 = AtSdhLineAug16Get(line, 0);

    mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel)(aug16), cAtSdhAugMapTypeAug16Map4xAug4s));

    /* Make AUG-4 contains AUG-1s */
    for (aug4Id = 0; aug4Id < cNumAug4InAug16; aug4Id++)        {
        AtSdhAug aug4 = AtSdhLineAug4Get(line, aug4Id);
        mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel)(aug4), cAtSdhAugMapTypeAug4Map4xAug1s));
        }

    /* Make each AUG-1 contains AU-3 <--> VC-3 */
    for (aug1Id = 0; aug1Id < cNumAug1InAug16; aug1Id++)
        {
        uint16 vc3Id;
        AtSdhAug aug1 = AtSdhLineAug1Get(line, aug1Id);
        mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel)(aug1), cAtSdhAugMapTypeAug1Map3xVc3s));

        /* Make each VC-3 contains TUG-2s */
        for (vc3Id = 0; vc3Id < cNumVc3InAug1; vc3Id++)
            {
            AtSdhVc vc3 = AtSdhLineVc3Get(line, aug1Id, vc3Id);
            uint16 tug2Id;
            mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel)(vc3), cAtSdhVcMapTypeVc3Map7xTug2s));

            /* Make each TUG-2 contains Tu12s */
            for (tug2Id = 0; tug2Id < cNumTug2InVc3; tug2Id++)
                {
                uint16 pwId;
                uint16 vc1xId;
                AtSdhTug tug2 = AtSdhLineTug2Get(line, aug1Id, vc3Id, tug2Id);
                mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel)(tug2), cAtSdhTugMapTypeTug2Map3xTu12s));

                /* For each VC12, emulate over PSN */
                for (vc1xId = 0; vc1xId < cNumVc12InTug2; vc1xId++)
                    {
                    AtPw pw;
                    AtSdhVc vc12 = AtSdhLineVc1xGet(line, aug1Id, vc3Id, tug2Id, vc1xId);
                    mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel)(vc12), cAtSdhVcMapTypeVc1xMapC1x));

                    pwId = PwIdFromSdhChannelId(aug1Id, vc3Id, tug2Id, vc1xId, sdhLineId);
                    pw   = (AtPw)AtModulePwCepCreate(pwModule, pwId, cAtPwCepModeBasic);

                    mSuccessAssert(AtPwPayloadSizeSet(pw, DefaultPwPayloadSize(pwId)));
                    mSuccessAssert(AtPwCircuitBind(pw, (AtChannel)vc12));
                    mSuccessAssert(AtPwEthPortSet(pw, ethPort));

                    /* Configure Ethernet header with default MAC and no VLAN */
                    mSuccessAssert(AtPwEthHeaderSet(pw, DefaultPwDestMac(ethPortId), NULL, NULL));

                    /* Create UDP over IPv4 PSN */
                    psn = MplsPsnCreate(pwId, ethPortId);
                    mSuccessAssert(AtPwPsnSet(pw, psn));
                    /* Masking enabling channel as we wan to give control to use */
                    /* mSuccessAssert(AtChannelEnable((AtChannel)(pw), cAtTrue));*/

                    /* Clean objects */
                    AtObjectDelete((AtObject)(psn));
                    }
                }
            }
        }

    /* Set Source MAC and IP for Ethernet Port */
    mSuccessAssert(AtEthPortSourceMacAddressSet(ethPort, DefaultEthPortSourceMac(ethPortId)));
    mSuccessAssert(AtEthPortIpV4AddressSet(ethPort, DefaultEthPortSourceIpv4(ethPortId)));
    mSuccessAssert(AtChannelLoopbackSet((AtChannel)(ethPort), cAtLoopbackModeRelease));

    return cAtOk;


}


eAtRet EnableCepPwChannel(uint16 sdhLineId, uint16 ethPortId) {

    uint16 aug1Id;
    AtModulePw pwModule = PwModule(DeviceGet());

    for (aug1Id = 0; aug1Id < cNumAug1InAug16; aug1Id++)
		{
        uint16 vc3Id;

        for (vc3Id = 0; vc3Id < cNumVc3InAug1; vc3Id++){
			
            uint16 tug2Id;

            for (tug2Id = 0; tug2Id < cNumTug2InVc3; tug2Id++) {
				
                uint16 vc1xId;

                for (vc1xId = 0; vc1xId < cNumVc12InTug2; vc1xId++) {
					
                    uint16 pwId = PwIdFromSdhChannelId(aug1Id, vc3Id, tug2Id, vc1xId, sdhLineId);
                    AtPw pw = (AtPw)AtModulePwGetPw(pwModule, pwId);
                    if (pw == NULL)
                        continue;
					
                    if( (aug1Id == 0) && (vc3Id == 0) && (tug2Id == 0) && (vc1xId == 0)) {
                        mSuccessAssert(AtChannelEnable((AtChannel)(pw), cAtTrue));
                    }
                }
            }
        }
    }

    return cAtOk;
}

eAtRet DisableCepPwChannel(uint16 sdhLineId, uint16 ethPortId) {

    uint16 aug1Id;
    AtModulePw pwModule = PwModule(DeviceGet());

    for (aug1Id = 0; aug1Id < cNumAug1InAug16; aug1Id++)
		{
        uint16 vc3Id;

        for (vc3Id = 0; vc3Id < cNumVc3InAug1; vc3Id++){
			
            uint16 tug2Id;

            for (tug2Id = 0; tug2Id < cNumTug2InVc3; tug2Id++) {
				
                uint16 vc1xId;

                for (vc1xId = 0; vc1xId < cNumVc12InTug2; vc1xId++) {
					
                    uint16 pwId = PwIdFromSdhChannelId(aug1Id, vc3Id, tug2Id, vc1xId, sdhLineId);
                    AtPw pw = (AtPw)AtModulePwGetPw(pwModule, pwId);
                    if (pw == NULL)
                        continue;
					
                    if( (aug1Id == 0) && (vc3Id == 0) && (tug2Id == 0) && (vc1xId == 0)) {
                        mSuccessAssert(AtChannelEnable((AtChannel)(pw), cAtFalse));
                    }
                }
            }
        }
    }

    return cAtOk;
}

static char *AlarmString(uint32 alarm)
    {
    static char buf[32];

    if (alarm == 0)
        {
        AtSprintf(buf, "none");
        return buf;
        }

    AtOsalMemInit(buf, 0, sizeof(buf));
    if (alarm & cAtPrbsEngineAlarmTypeError)
        {
        AtStrcat(buf, "|");
        AtStrcat(buf, "error");
        }
    if (alarm & cAtPrbsEngineAlarmTypeLossSync)
        {
        AtStrcat(buf, "|");
        AtStrcat(buf, "lossSync");
        }

    return &buf[1]; /* Get rid of the first '|' */
   }



void Tfi5Prbs(AtSdhLine line,  eAtPrbsMode mode)    {
    uint32 alarm;
    AtSerdesController serdesController = AtSdhLineSerdesController(line);
    AtPrbsEngine prbsEngine = AtSerdesControllerPrbsEngine(serdesController);

    /* Configure the PRBS mode */
    AtAssert(AtPrbsEngineTxModeSet(prbsEngine, mode) == cAtOk);
    AtAssert(AtPrbsEngineTxModeGet(prbsEngine) == mode);
    AtAssert(AtPrbsEngineRxModeSet(prbsEngine, mode) == cAtOk);
    AtAssert(AtPrbsEngineRxModeGet(prbsEngine) == mode);

    /* Then enable the prbsEngine */
    AtAssert(AtPrbsEngineRxEnable(prbsEngine, cAtTrue));
    AtAssert(AtPrbsEngineRxIsEnabled(prbsEngine) == cAtTrue);
    AtAssert(AtPrbsEngineTxEnable(prbsEngine, cAtTrue));
    AtAssert(AtPrbsEngineTxIsEnabled(prbsEngine) == cAtTrue);

    alarm = StickyGet(prbsEngine, 1);
    sleep(1);

    alarm = AtPrbsEngineAlarmGet(prbsEngine);    
    if(alarm){
		printf("\nError : PRBS Error\n");
        AllCountersShow(prbsEngine, 1); /* read2clear*/		 
    }

    /* Then disable the prbsEngine */
    AtAssert(AtPrbsEngineRxEnable(prbsEngine, cAtFalse));
    AtAssert(AtPrbsEngineRxIsEnabled(prbsEngine) == cAtFalse);
    AtAssert(AtPrbsEngineTxEnable(prbsEngine, cAtFalse));
    AtAssert(AtPrbsEngineTxIsEnabled(prbsEngine) == cAtFalse);

}

eAtRet De1Prbs(AtPdhDe1 de1)
    {
    uint32 alarm;
    /* Create engine */
    AtDevice device = AtChannelDeviceGet((AtChannel)de1);
    AtModulePrbs prbsModule = (AtModulePrbs)AtDeviceModuleGet(device, cAtModulePrbs);
    uint32 engineId = 0; /* Assume that this engine has not been used yet */
    AtPrbsEngine engine = AtModulePrbsDe1PrbsEngineCreate(prbsModule, engineId, de1);
    /* Can access this engine after creating as following */
    AtAssert(engine != NULL);
    AtAssert(AtPrbsEngineIdGet(engine) == engineId);
    AtAssert(AtModulePrbsEngineGet(prbsModule, engineId) == engine);
    /* Both these objects have reference to each other */
    AtAssert(AtPrbsEngineChannelGet(engine) == (AtChannel)de1);
    AtAssert(AtChannelPrbsEngineGet((AtChannel)de1) == engine);
    /* Configure the PRBS mode */
    AtAssert(AtPrbsEngineTxModeSet(engine, cAtPrbsModePrbs23) == cAtOk);
    AtAssert(AtPrbsEngineTxModeGet(engine) == cAtPrbsModePrbs23);
    AtAssert(AtPrbsEngineRxModeSet(engine, cAtPrbsModePrbs23) == cAtOk);
    AtAssert(AtPrbsEngineRxModeGet(engine) == cAtPrbsModePrbs23);
    /* Then enable the engine */
    AtAssert(AtPrbsEngineRxEnable(engine, cAtTrue));
    AtAssert(AtPrbsEngineRxIsEnabled(engine) == cAtTrue);
    AtAssert(AtPrbsEngineTxEnable(engine, cAtTrue));
    AtAssert(AtPrbsEngineTxIsEnabled(engine) == cAtTrue);

    alarm = StickyGet(engine, 1);
    sleep(1);

    alarm = AtPrbsEngineAlarmGet(engine);    
    if(alarm){
		printf("\nError : PRBS Error\n");
        AllCountersShow(engine, 1); /* read2clear*/		 
    }

    /* When PRBS is not used any more, application may want to disable and destroy
     * this engine to save resource as following */
    AtAssert(AtPrbsEngineEnable(engine, cAtFalse) == cAtOk);
    AtAssert(AtModulePrbsEngineDelete(prbsModule, engineId) == cAtOk);
    AtAssert(AtModulePrbsEngineGet(prbsModule, engineId) == NULL);
    /* All of associations would be invalid after this */
    AtAssert(AtPrbsEngineChannelGet(engine) == NULL);
    AtAssert(AtChannelPrbsEngineGet((AtChannel)de1) == NULL);    
}

eAtRet De3Prbs(AtPdhDe3 de3)
    {
    uint32 alarm;
    /* Create engine */
    AtDevice device = AtChannelDeviceGet((AtChannel)de3);
    AtModulePrbs prbsModule = (AtModulePrbs)AtDeviceModuleGet(device, cAtModulePrbs);
    uint32 engineId = 0; /* Assume that this engine has not been used yet */
    AtPrbsEngine engine = AtModulePrbsDe1PrbsEngineCreate(prbsModule, engineId, de3);
    /* Can access this engine after creating as following */
    AtAssert(engine != NULL);
    AtAssert(AtPrbsEngineIdGet(engine) == engineId);
    AtAssert(AtModulePrbsEngineGet(prbsModule, engineId) == engine);
    /* Both these objects have reference to each other */
    AtAssert(AtPrbsEngineChannelGet(engine) == (AtChannel)de3);
    AtAssert(AtChannelPrbsEngineGet((AtChannel)de3) == engine);
    /* Configure the PRBS mode */
    AtAssert(AtPrbsEngineTxModeSet(engine, cAtPrbsModePrbs23) == cAtOk);
    AtAssert(AtPrbsEngineTxModeGet(engine) == cAtPrbsModePrbs23);
    AtAssert(AtPrbsEngineRxModeSet(engine, cAtPrbsModePrbs23) == cAtOk);
    AtAssert(AtPrbsEngineRxModeGet(engine) == cAtPrbsModePrbs23);
    /* Then enable the engine */
    AtAssert(AtPrbsEngineRxEnable(engine, cAtTrue));
    AtAssert(AtPrbsEngineRxIsEnabled(engine) == cAtTrue);
    AtAssert(AtPrbsEngineTxEnable(engine, cAtTrue));
    AtAssert(AtPrbsEngineTxIsEnabled(engine) == cAtTrue);

    alarm = StickyGet(engine, 1);
    sleep(1);

    alarm = AtPrbsEngineAlarmGet(engine);    
    if(alarm){
		printf("\nError : PRBS Error\n");
        AllCountersShow(engine, 1); /* read2clear*/		 
    }

    /* When PRBS is not used any more, application may want to disable and destroy
     * this engine to save resource as following */
    AtAssert(AtPrbsEngineEnable(engine, cAtFalse) == cAtOk);
    AtAssert(AtModulePrbsEngineDelete(prbsModule, engineId) == cAtOk);
    AtAssert(AtModulePrbsEngineGet(prbsModule, engineId) == NULL);
    /* All of associations would be invalid after this */
    AtAssert(AtPrbsEngineChannelGet(engine) == NULL);
    AtAssert(AtChannelPrbsEngineGet((AtChannel)de3) == NULL);    
}

void Oc48LineAccess(AtModuleSdh sdhModule) {
    AtSdhLine tfi5Line, hoLine, loLine;
    /* This product support 22 lines with:
     * - 8 TFI-5 Lines
     * - 8 HO Lines
     * - 6 LO Lines
     */
    AtAssert(AtModuleSdhMaxLinesGet(sdhModule)  == 22);
    AtAssert(AtModuleSdhNumTfi5Lines(sdhModule) == 8);
    AtAssert(AtModuleSdhNumHoLines(sdhModule)   == 8);
    AtAssert(AtModuleSdhNumLoLines(sdhModule)   == 6);
    /* Access the first TFI-5 Line, first HO Line and first LO Line */
    AtAssert((tfi5Line = AtModuleSdhTfi5LineGet(sdhModule, 0)));
    AtAssert((hoLine   = AtModuleSdhHoLineGet(sdhModule, 0)));
    AtAssert((loLine   = AtModuleSdhLoLineGet(sdhModule, 0)));
    /* There assigned ID is as following */
    AtAssert(AtChannelIdGet((AtChannel)tfi5Line) == 0);
    AtAssert(AtChannelIdGet((AtChannel)hoLine)   == 8);
    AtAssert(AtChannelIdGet((AtChannel)loLine)   == 16);
    /* And these lines can be also access by its assigned flat ID */
    AtAssert(tfi5Line == AtModuleSdhLineGet(sdhModule, 0));
    AtAssert(hoLine   == AtModuleSdhLineGet(sdhModule, 8));
    AtAssert(loLine   == AtModuleSdhLineGet(sdhModule, 16));
}

    /* 
     cAtLoopbackModeRelease, /**< Release loopback * /
     cAtLoopbackModeLocal,   /**< Local loopback * /
    cAtLoopbackModeRemote   /**< Remote loopback */
    
void atTf5LineLoopback(uint16 sdhLineId, eAtLoopbackMode lpbkMode)
{
    AtDevice device = DeviceGet();
    AtModulePw  pwModule  = PwModule(device);
    AtSdhLine line = AtModuleSdhLineGet(SdhModule(device), sdhLineId);


    AtChannel lineChannel = (AtChannel)line;
    AtSerdesController serdesController = AtSdhLineSerdesController(line);

    /* The following lines show how to perform each loopback mode on line */
    AtAssert(AtChannelLoopbackSet(lineChannel, lpbkMode)   == cAtOk);
    AtAssert(AtChannelLoopbackGet(lineChannel)  == lpbkMode);


    /* And the following lines show how to perform each loopback mode on line's SERDES */
    AtAssert(AtSerdesControllerLoopbackSet(serdesController, lpbkMode)   == cAtOk);
    AtAssert(AtSerdesControllerLoopbackGet(serdesController)  == lpbkMode);
}

    /* 
     cAtLoopbackModeRelease, /**< Release loopback * /
     cAtLoopbackModeLocal,   /**< Local loopback * /
    cAtLoopbackModeRemote   /**< Remote loopback */
    
void atEthLoopback(uint8 ethPortId, eAtLoopbackMode lpbkMode)
{
    AtDevice device = DeviceGet();
    AtModulePw  pwModule  = PwModule(device);
    AtEthPort ethPort = AtModuleEthPortGet(EthModule(device), ethPortId);

    AtSerdesController serdesController = AtEthPortSerdesController(ethPort);

    /* The following lines show how to perform each loopback mode on line */
/*	
    AtAssert(AtChannelLoopbackSet(lineChannel, lpbkMode)   == cAtOk);
    AtAssert(AtChannelLoopbackSet(lineChannel, cAtLoopbackModeRemote)  == cAtOk);
    AtAssert(AtChannelLoopbackSet(lineChannel, cAtLoopbackModeRelease) == cAtOk);
*/

    /* And the following lines show how to perform each loopback mode on line's SERDES */
    AtAssert(AtSerdesControllerLoopbackSet(serdesController, cAtLoopbackModeLocal)   == cAtOk);
    AtAssert(AtSerdesControllerLoopbackSet(serdesController, cAtLoopbackModeRemote)  == cAtOk);
    AtAssert(AtSerdesControllerLoopbackSet(serdesController, cAtLoopbackModeRelease) == cAtOk);
}



int atAllDdrsTest(void)
{
    uint8 slotNumber = (uint8)im_get_current_slot_no();
	CiscoIm im = &(m_ims[slotNumber]);
    AtDevice device = im->device ;
    //AtDevice device = AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
    AtModuleRam ramModule = (AtModuleRam)AtDeviceModuleGet(device, cAtModuleRam);
    uint8 ddr_i;
    int retCode = PASSED;

    debug_print("\n%s %d\n",__FUNCTION__,__LINE__);
    AtDeviceDiagnosticModeEnable(device, cAtTrue);
    debug_print("\n%s %d\n",__FUNCTION__,__LINE__);

    /* Test all of DDRs */
    for (ddr_i = 0; ddr_i < AtModuleRamNumDdrGet(ramModule); ddr_i++)
        {
        debug_print("\n%s %d\n",__FUNCTION__,__LINE__);
        static const uint32 cAnyAddress   = 0x2345;
        static const uint32 cStartAddress = 0;
        eAtRet ret;
        AtRam ram = AtModuleRamDdrGet(ramModule, ddr_i);
        uint32 endAddress = AtRamMaxAddressGet(ram);
        uint32 firstErrorAddress;
        AtAssert(AtRamAddressBusTest(ram) == cAtOk);
        AtAssert(AtRamDataBusTest(ram, cAnyAddress) == cAtOk);
        /* Memory testing is longer and the result may need to be examined */
        ret = AtRamMemoryTest(ram, cStartAddress, endAddress, &firstErrorAddress);
        if (ret != cAtOk) {
			retCode = FAILED;
            AtPrintf("First error address: 0x%08x\r\n", firstErrorAddress);
        }
        AtAssert(ret == cAtOk);
        }
    AtDeviceDiagnosticModeEnable(device, cAtFalse);
    return (retCode);	
}

int atAllQdrTest(void)
{
    uint8 slotNumber = (uint8)im_get_current_slot_no();
	CiscoIm im = &(m_ims[slotNumber]);
    AtDevice device = im->device ;
    AtModuleRam ramModule = (AtModuleRam)AtDeviceModuleGet(device, cAtModuleRam);
    uint8 qdr_i;
    int retCode = PASSED;

    debug_print("\n%s %d\n",__FUNCTION__,__LINE__);
    AtDeviceDiagnosticModeEnable(device, cAtTrue);
    debug_print("\n%s %d\n",__FUNCTION__,__LINE__);

    /* Test all of QDRs */
    for (qdr_i = 0; qdr_i < AtModuleRamNumQdrGet(ramModule); qdr_i++)
        {
        debug_print("\n%s %d\n",__FUNCTION__,__LINE__);
        static const uint32 cAnyAddress   = 0x2345;
        static const uint32 cStartAddress = 0;
        eAtRet ret;
        AtRam ram = AtModuleRamQdrGet(ramModule, qdr_i);
        uint32 endAddress = AtRamMaxAddressGet(ram);
        uint32 firstErrorAddress;
        AtAssert(AtRamAddressBusTest(ram) == cAtOk);
        AtAssert(AtRamDataBusTest(ram, cAnyAddress) == cAtOk);
        /* Memory testing is longer and the result may need to be examined */
        ret = AtRamMemoryTest(ram, cStartAddress, endAddress, &firstErrorAddress);
        if (ret != cAtOk) {
            AtPrintf("First error address: 0x%08x\r\n", firstErrorAddress);
			retCode = FAILED;
        }
        AtAssert(ret == cAtOk);
        }
    AtDeviceDiagnosticModeEnable(device, cAtFalse);
    return (retCode);	
}



/*
 *------------------------------------------------------------------
 * uea_lotr_im_cem_fpga.c - dev_obj for Arrive SDK operations
 *
 * March 2015, Raghu Nandan A
 *
 * Copyright (c) 2015 by cisco Systems, Inc.
 * All rights reserved.
 *------------------------------------------------------------------
 */
#define BTRACE_APP_MACROS

#include <btrace/btrace.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <binos/common.h>
#include <sys/mman.h>
#include <iomd_infra/iomd_infra.h>
#include <errmsg/errmsg.h>
#include <devobj/dev_object.h>
#include "../../../cc/src/iomd_log.h" 
#include "../../../cc/src/iomd.h"
#include <infra/debugging.h>
#include <flash/flashlib.h>

#include <arrive/AtClasses.h>
#include <arrive/AtDevice.h>
#include <arrive/AtDriver.h>
#include <arrive/AtHal.h>
#include <arrive/AtOsal.h>
#include <arrive/AtHalTcp.h>
#include <arrive/AtList.h>
#include <arrive/AtModuleSdh.h>
#include <arrive/AtSdhLine.h>
#include <arrive/AtSdhAug.h>
#include <arrive/AtSdhVc.h>
#include <arrive/AtSdhTug.h>
#include <arrive/AtPw.h>
#include <arrive/AtPdhDe1.h>
#include <arrive/AtPdhDe3.h>
#include <arrive/AtPwCounters.h>
#include <arrive/AtModulePdh.h>
#include <arrive/AtCliService.h>
#include <arrive/AtHalCisco.h>
#include <arrive/AtChannel.h>
#include <arrive/AtSurEngine.h>
#include "uea_lotr_im_cem_fpga.h"
#include <uea_chocx_sonet_private_ds.h>
#include <uea_lotr_common.h>
#include <im_fpga/dev_im_fpga.h>
#include <binos/sw_wdog.h>
#include <alarm_stats_lib/uea_lotr_im_chocx_alarm.h>

#define NUM_PATHS_IN_AUG1 3
#define EGRESS_ETH_PORT 0
#define CEM_VLAN_ID 4095

static uint8_t cem_fpga_simulation;

dev_cem_fpga_object_t *g_dev_cem_fpga;

static void
cem_fpga_alarm_change_state_register(AtChannel channel,
                                     alarm_task_type_e alarm_type,
                                     eBool enable);

void cem_fpga_path_alarm_change_state(AtChannel channel,
                                      uint32 changed_alarms,
                                      uint32 current_status);
void cem_fpga_vt_alarm_change_state(AtChannel channel,
                                    uint32 changed_alarms,
                                    uint32 current_status);
void cem_fpga_t1e1_alarm_change_state(AtChannel channel,
                                      uint32 changed_alarms,
                                      uint32 current_status);
void cem_fpga_t3e3_alarm_change_state(AtChannel channel,
                                      uint32 changed_alarms,
                                      uint32 current_status);
void cem_fpga_path_get_channel_to_essi(AtChannel channel, 
                                       essi_ckt_t *ckt);
void cem_fpga_vt_get_channel_to_essi(AtChannel channel, 
                                     essi_ckt_t *ckt);
void cem_fpga_t3e3_get_channel_to_essi(AtChannel channel, 
                                       essi_ckt_t *ckt);
void cem_fpga_t1e1_get_channel_to_essi(AtChannel channel, 
                                       essi_ckt_t *ckt);
void cem_fpga_t3e3_get_channel_to_t3e3(AtChannel channel, 
                                           t3e3_ckt_t *ckt);
void cem_fpga_t1e1_get_channel_to_t3e3(AtChannel channel, 
                                           t3e3_ckt_t *ckt);
void cem_fpga_t1e1_get_channel_to_t1e1(AtChannel channel, 
                                           t1e1_ckt_t *ckt);

static boolean 
cem_fpga_mem_map_init (dev_cem_fpga_object_t *cem_fpga) 
{
    /*This function stores the MEM MAP for all the CEM FPGA Memory Area */
    char device_name[IOMD_CEM_FPGA_DEV_NAME_MAX];
    uint8_t bay;
    off_t mmap_offset = 0x00;
    size_t mmap_size = 0x20000000;  /* 512MB */

    if (!cem_fpga) {
        return (FALSE);
    }
    bay = cem_fpga->bay;
    snprintf(device_name, IOMD_CEM_FPGA_DEV_NAME_MAX, 
             "/dev/cem_fpga%d", bay);

    /* Assuming mmap to virtual address will not fail*/
    cem_fpga->fd = open(device_name, O_RDWR);
    if (0 > cem_fpga->fd) {
        ERR("CEM_FPGA:%s open failed",device_name);
        return (DEV_STATUS_FAILURE_MALLOC);
    }
    cem_fpga->im_bar_mmap = mmap(NULL, mmap_size, 
                            PROT_READ | PROT_WRITE, MAP_SHARED | O_SYNC,
                            cem_fpga->fd, mmap_offset); 

    if (cem_fpga->im_bar_mmap == NULL) {
        ERR("\nCEM_FPGA: MMAP Failed for %s\n", device_name);
        return (FALSE);
    }
    INFO("CEM_FPGA: MMAP Addr: %x\n", (unsigned int)cem_fpga->im_bar_mmap);
    return (TRUE);
}

static void PCIeWrite (uint32 address, uint32 value) 
{
    *((volatile uint32 *)address) = value;
    __asm__("sync");
    (void)*((volatile uint32 *)address);
    __asm__("sync");
}

static uint32 PCIeRead (uint32 address)
{
    uint32 regVal = *((volatile uint32 *)address);
    __asm__("sync");
    return regVal;
}

static AtHal CreateHal (uint32 baseAddress) 
{
    return AtHalCiscoNew(baseAddress, PCIeWrite, PCIeRead);
}

static AtEthPort get_eth_port_object (AtDevice device, uint8 portId)
{
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    return AtModuleEthPortGet(ethModule, portId);
}

static dev_status
cem_fpga_driver_init (dev_object_t * dev)
{
if (cem_fpga_simulation) {
    dev_cem_fpga_object_t *cem_fpga = 
                        (dev_cem_fpga_object_t *)dev;
    AtDriver driver = AtDriverCreate(1, AtOsalLinux());
    AtHal hal = CreateHal((uint32_t)cem_fpga->im_bar_mmap);
    //uint32 productCode = AtHalRead(hal, 0x0);
    uint32 productCode = AtProductCodeGetByHal(hal);
    AtDevice newDevice = AtDriverDeviceCreate(driver, productCode);
    AtIpCore newIpCore = AtDeviceIpCoreGet(newDevice, 0);
    AtModule newSdhModule = AtDeviceModuleGet(newDevice, cAtModuleSdh);
    AtModule newPdhModule = AtDeviceModuleGet(newDevice, cAtModulePdh);
    eAtRet ret;
    uint16_t atcli_port;

    if (newDevice == NULL) {
        ERR("cem_fpga_driver_init failed\n");
        return(DEV_CEM_FPGA_FAILURE);
    }

    /* Use this HAL for the IP Core */
    AtDeviceIpCoreHalSet(newDevice, 0, hal);
    ret = AtDeviceInit(newDevice);
    ERR("AtDeviceInit: %s\r\n", AtRet2String(ret));

    atcli_port = CEM_FPGA_ATCLI_START_PORT + 
            ((cem_fpga->slot * MAX_CEM_FPGA_PER_CHASIS) + (cem_fpga->bay));
    AtCliServerStart("127.0.0.1", atcli_port);
    ERR("The atcli port is %d", atcli_port);

    cem_fpga->cem_fpga_driver = driver;
    INFO("cem_fpga_product_code :0x%x", productCode);

    if ((ret = AtIpCoreInterruptEnable(newIpCore, TRUE)) != cAtOk) {
        ERR("\n Failed to enable interrupt for IpCore, ret %d", ret);
    }
    if ((ret = AtModuleInterruptEnable(newSdhModule, TRUE)) != cAtOk) {
        ERR("\n Failed to enable interrupt for SdhModule, ret %d", ret);
    }
    if ((ret = AtModuleInterruptEnable(newPdhModule, TRUE)) != cAtOk) {
        ERR("\n Failed to enable interrupt for PdhModule, ret %d", ret);
    }
    /*
     * TODO: Simulation CEM-FPGA for OCx has multiple product ids,
     * but for final FPGA only single product code,so proudct check is 
     * not valid for simulation 
     */
#if 0
    if(fpga_product_code != cem_fpga->product_code) {
       ERR("cem_fpga_product_code configured:%d  expected :%d \n",
                fpga_product_code,cem_fpga->product_code);
       return (DEV_CEM_FPGA_FAILURE);
    }
#endif
    if ((productCode == DS1_CEM_FPGA_PRODUCT_CODE)  ||
        (productCode == DS3_CEM_FPGA_PRODUCT_CODE)) {
        ret = AtEthPortInterfaceSet(get_eth_port_object(
                    newDevice, EGRESS_ETH_PORT), cAtEthPortInterfaceXGMii);
        ERR("AtEthPortInterfaceSet: %s\r\n", AtRet2String(ret));          
        }
    }

    return (DEV_CEM_FPGA_SUCCESS);
}

static int cem_fpga_driver_init_wd_disabled (const char *cause __UNUSED,
                const u_int64_t buffer_size, void *opaque2)
{
    dev_object_t *dev = (dev_object_t *)opaque2;

    if (!dev) {
        ERR("%s() dev obj is NULL", __func__);
        return (DEV_CEM_FPGA_FAILURE);
    }

    return (cem_fpga_driver_init(dev));
}

static dev_status 
dev_cem_fpga_init (dev_object_t * dev)
{
    dev_cem_fpga_object_t *cem_fpga = (dev_cem_fpga_object_t *)dev;

    if (cem_fpga_simulation) {
    INFO("dev_cem_fpga_init()");
    if (!dev) {
        return(DEV_CEM_FPGA_FAILURE);
    }

    cem_fpga_mem_map_init(cem_fpga);
    /* setup few system variables the Mgmt intf should be 
     * in Arrive eval board subnet */
    system("ifconfig eth1 172.33.44.1/24");
    if (!sw_wdog_disable_block(cem_fpga_driver_init_wd_disabled, 0, dev,
                "CEM Arrive SDK Init")) {
        dev->dev_state = DEV_STATE_INIT;
        return (DEV_CEM_FPGA_SUCCESS);
    } else {
        ERR("CEM device setup failed");
        return(DEV_CEM_FPGA_FAILURE);
    }
}
    dev->dev_state = DEV_STATE_INIT;
    return (DEV_CEM_FPGA_SUCCESS);
}

static AtList cem_fpga_device_hals(AtDevice device)
{   
    uint8 coreId;
    AtList hals;

    if (device == NULL) 
    return NULL;

    hals = AtListCreate(0);
    for (coreId = 0; coreId < AtDeviceNumIpCoresGet(device); coreId++) {
        AtObject hal = 
            (AtObject)AtIpCoreHalGet(AtDeviceIpCoreGet(device, coreId));
        if (!AtListContainsObject(hals, hal)) {
            AtListObjectAdd(hals, hal); 
        }
    }
    return hals;
}

static dev_status
cem_fpga_driver_deinit (dev_cem_fpga_object_t *cem_fpga)
{
if (cem_fpga_simulation) {
    uint8 num_devices = 0, i;
    AtList hals;
    AtDriver driver = cem_fpga->cem_fpga_driver;

    if (driver == NULL) {
        return(DEV_CEM_FPGA_FAILURE);
    }

    /* Delete all devices */
    AtDriverAddedDevicesGet(driver, &num_devices);
    for (i = 0; i < num_devices; i++) {
        AtDevice device = AtDriverDeviceGet(driver, 0);
        if (device == NULL) {
            return (DEV_CEM_FPGA_SUCCESS);
        }

        /* Save all HALs that are installed so far */
        hals = cem_fpga_device_hals(device);
        AtDriverDeviceDelete(AtDriverSharedDriverGet(), device);

        /* Delete all HALs */
        while (AtListLengthGet(hals) > 0) {
            AtHalDelete((AtHal)AtListObjectRemoveAtIndex(hals, 0));
        }
        AtObjectDelete((AtObject)hals);
        return (DEV_CEM_FPGA_SUCCESS);
    }

    /* Delete driver then delete all saved HALs */
    AtDriverDelete(driver);
}
    return(DEV_CEM_FPGA_SUCCESS);
}

static void
dev_cem_fpga_destroy (dev_object_t **device_ptr)
{
    if (device_ptr != NULL) {
        dev_cem_fpga_object_t *dev = 
                (dev_cem_fpga_object_t*)*device_ptr;

        if (cem_fpga_driver_deinit(dev)) {
            return;
        }
        dev_base_deinit(&dev->base);
        dev_free(*device_ptr);
        *device_ptr = NULL;
    }
}

/*
 * Enable device operation. 
 */
static dev_status
dev_cem_fpga_oper_enable (dev_object_t * dev)
{
if (cem_fpga_simulation) {
    INFO("dev_cem_fpga_oper_enable()");

    if (!dev) {
        return(DEV_CEM_FPGA_FAILURE);
    }
     
    dev->dev_state = DEV_STATE_OPER_ENABLE;
}

    return (DEV_CEM_FPGA_SUCCESS);
}

/*
 * Disable device operation. 
 */
static dev_status
dev_cem_fpga_oper_disable (dev_object_t * dev)
{
if (cem_fpga_simulation) {
    if (!dev) {
        return (DEV_CEM_FPGA_FAILURE);
    }
    ERR("dev_cem_fpga_oper_disable()");

    dev->dev_state = DEV_STATE_OPER_DISABLE;
}

    return (DEV_CEM_FPGA_SUCCESS);
}

static dev_status 
dev_cem_fpga_intr_enable (dev_object_t * dev)
{
    INFO("dev_cem_fpga_intr_enable()");

    return (DEV_CEM_FPGA_SUCCESS);
}

static dev_status 
dev_cem_fpga_intr_disable (dev_object_t * dev)
{
    INFO("dev_cem_fpga_intr_disable()");

    return (DEV_CEM_FPGA_SUCCESS);
}

static dev_status_isr 
dev_cem_fpga_isr (dev_object_t * dev)
{
    ERR("dev_cem_fpga_isr()");
    return (DEV_CEM_FPGA_ISR_ROUTINE);
}

static void
dev_cem_fpga_show_brief (dev_object_t * dev, print_fn_t print_fn)
{
    print_fn("\n(0x%08x), name %s state %d\n",
             dev, dev->dev_object_fvt->dev_name, dev->dev_state);
}

static void
dev_cem_fpga_show_config (dev_object_t * dev, print_fn_t print_fn)
{
    dev_cem_fpga_object_t *cem_fpga = 
                (dev_cem_fpga_object_t *)dev;

    print_fn("\n\n%s (0x%08x) device configuration:",
               dev->dev_object_fvt->dev_name, cem_fpga);
}

static void
dev_cem_fpga_show_error (dev_object_t * dev, print_fn_t print_fn)
{
    dev_cem_fpga_object_t *cem_fpga = 
                (dev_cem_fpga_object_t *)dev;

    print_fn("\n\n%s (0x%08x) device errors:",
               dev->dev_object_fvt->dev_name, cem_fpga);

}

static void
dev_cem_fpga_show_status (dev_object_t * dev, print_fn_t print_fn)
{
    dev_cem_fpga_object_t *cem_fpga = 
                (dev_cem_fpga_object_t *)dev;

    print_fn("\n\n%s (0x%08x) device status:",
               dev->dev_object_fvt->dev_name, cem_fpga);
}

/*
 * Device show command. 
 */
static dev_status
dev_cem_fpga_show (
        dev_object_t * dev, print_fn_t print_fn, dev_show_cmd show)
{
if (cem_fpga_simulation) {
    ERR("\ndev_cem_fpga_show()");
    
    if (!dev || !print_fn) {
        return(DEV_CEM_FPGA_FAILURE);
    }

    print_fn("\n%s: ", ((dev_cem_fpga_object_t *)dev)->dev_if_name);
    switch (show) {
    case DEV_SHOW_ALL:
        dev_cem_fpga_show_brief(dev, print_fn);
        dev_cem_fpga_show_config(dev, print_fn);
        dev_cem_fpga_show_status(dev, print_fn);
        dev_cem_fpga_show_error(dev, print_fn);
        break;
    case DEV_SHOW_BRIEF:
        dev_cem_fpga_show_brief(dev, print_fn);
        break;
    case DEV_SHOW_CONFIG:
        dev_cem_fpga_show_config(dev, print_fn);
        break;
    case DEV_SHOW_ERRORS:
        dev_cem_fpga_show_error(dev, print_fn);
        break;
    case DEV_SHOW_REGISTERS:
        dev_cem_fpga_show_config(dev, print_fn);
        dev_cem_fpga_show_status(dev, print_fn);
        dev_cem_fpga_show_error(dev, print_fn);
        break;
    case DEV_SHOW_STATUS:
        dev_cem_fpga_show_status(dev, print_fn);
        break;
    case DEV_SHOW_COUNTERS_ALL:
        break;
    default:
        break;
    }
}

    return (DEV_STATUS_SUCCESS);
}

static AtSdhLine cem_fpga_get_line (dev_cem_fpga_object_t *cem_fpga, 
        uint8_t line_id)
{
    AtDevice device =
        AtDriverDeviceGet(cem_fpga->cem_fpga_driver, CEM_FPGA_CORE_ID);
    AtModuleSdh sdhModule = 
        (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    return (AtModuleSdhLineGet(sdhModule, line_id));
}

void cem_fpga_path_get_channel_to_essi (AtChannel channel, essi_ckt_t *ckt)
{
    /* In this context, the channel must be SDH channel */
    AtSdhChannel sdhChannel = (AtSdhChannel)channel;
    uint8_t line_id = AtSdhChannelLineGet(sdhChannel);
    uint8_t sts1_id = AtSdhChannelSts1Get(sdhChannel);

    ckt->line_id = line_id;
    ckt->sts1_id = sts1_id;
    ckt->tug2_id = 0;
    ckt->tu11_id = 0;
}

void cem_fpga_vt_get_channel_to_essi (AtChannel channel, essi_ckt_t *ckt)
{
    /* In this context, the channel must be SDH channel */
    AtSdhChannel sdhChannel = (AtSdhChannel)channel;
    uint8_t line_id = AtSdhChannelLineGet(sdhChannel);
    uint8_t sts1_id = AtSdhChannelSts1Get(sdhChannel);
    uint8_t tug2_id = AtSdhChannelTug2Get(sdhChannel);
    uint8_t tu11_id = AtSdhChannelTu1xGet(sdhChannel);

    ckt->line_id = line_id;
    ckt->sts1_id = sts1_id;
    ckt->tug2_id = tug2_id;
    ckt->tu11_id = tu11_id;
}

void cem_fpga_t3e3_get_channel_to_essi (AtChannel channel, 
                                        essi_ckt_t *ckt) 
{
    AtPdhChannel de3 = (AtPdhChannel)channel;
    AtSdhChannel vc3 = (AtSdhChannel)AtPdhChannelVcGet(de3);

    /* This DS3/E3 may be mapped directly to VC3 */
    if (vc3) {
        ckt->line_id = AtSdhChannelLineGet(vc3);
        ckt->sts1_id = AtSdhChannelSts1Get(vc3);
    }
}

void cem_fpga_t3e3_get_channel_to_t3e3 (AtChannel channel, 
                                            t3e3_ckt_t *ckt) 
{
    /* This DS3/E3 from ds3 card LIU */
    ckt->de3_id = AtChannelIdGet((AtChannel)channel);
    ckt->de2_id = 0;
    ckt->de1_id = 0;
}

void cem_fpga_t1e1_get_channel_to_essi (AtChannel channel, 
                                        essi_ckt_t *ckt) 
{
    AtPdhChannel de1  = (AtPdhChannel)channel;
    AtPdhChannel de2, de3;
    AtSdhChannel vc3;

    /* This is ds1 from vc1x channel */
    AtSdhChannel vc1x = (AtSdhChannel)AtPdhChannelVcGet(de1);

    /* And this DS3/E3 may also be mapped to VC3 incase of OCx card */
    de2 = AtPdhChannelParentChannelGet(de1);
    de3 = AtPdhChannelParentChannelGet(de2);
    vc3 = (AtSdhChannel)AtPdhChannelVcGet(de3);

    if (vc1x) {
        /* VC1x does exist, this is t1e1 part of VC11 */
        ckt->line_id = AtSdhChannelLineGet(vc1x);
        ckt->sts1_id = AtSdhChannelSts1Get(vc1x);
        ckt->tug2_id = AtSdhChannelTug2Get(vc1x);
        ckt->tu11_id = AtSdhChannelTu1xGet(vc1x);
    } else if (vc3) {
        /* VC-3 does exist, this is t1e1 part of T3E3 within STS1 */
        ckt->line_id = AtSdhChannelLineGet(vc3);
        ckt->sts1_id = AtSdhChannelSts1Get(vc3);
        ckt->tug2_id  = AtChannelIdGet((AtChannel)de2);
        ckt->tu11_id  = AtChannelIdGet((AtChannel)de1);
    } 
}

void cem_fpga_t1e1_get_channel_to_t3e3 (AtChannel channel, 
                                            t3e3_ckt_t *ckt) 
{
    AtPdhChannel de1  = (AtPdhChannel)channel;
    AtPdhChannel de2, de3;

    de2 = AtPdhChannelParentChannelGet(de1);

    /* This is ds1 from DS3 card */
    if (de2) {
        de3 = AtPdhChannelParentChannelGet(de2);
        ckt->de3_id = AtChannelIdGet((AtChannel)de3);
        ckt->de2_id = AtChannelIdGet((AtChannel)de2);
        ckt->de1_id = AtChannelIdGet((AtChannel)de1);
    }
}

void cem_fpga_t1e1_get_channel_to_t1e1 (AtChannel channel, 
                                            t1e1_ckt_t *ckt) 
{
    /* This is ds1 from DS1 card */
    ckt->de1_id = AtChannelIdGet((AtChannel)channel);
}


void cem_fpga_path_alarm_change_state (AtChannel channel, 
                                       uint32 changed_alarms, 
                                       uint32 current_status)
{
    sonetsdh_path_alarm_map_e alarm_type = 0;
    boolean alarm_status = FALSE;
    essi_ckt_t ckt_info; 
    essi_ckt_t *p_ckt_info = &ckt_info;
    cem_fpga_alarm_change_state_fn_t cem_fpga_alarm_change_state;
    cem_fpga_alarm_change_state = 
               g_dev_cem_fpga->callout->alarm_change_state;

    cem_fpga_path_get_channel_to_essi((AtChannel)channel, p_ckt_info);
    ERR("\n Alarm changed on STS. ckt_info = %d.%d", ckt_info.line_id,
                                                     ckt_info.sts1_id);

    if (changed_alarms & cAtSdhPathAlarmLop) {
        alarm_status = (current_status & cAtSdhPathAlarmLop);
        alarm_type = SONETSDH_PATH_STSLOP;
        ERR(" LOP: %s\n", (alarm_status) ? "Raise" : "Clear");
        cem_fpga_alarm_change_state(p_ckt_info, PATH_ALARM_TYPE,
                                    alarm_type, alarm_status);
    }
    if (changed_alarms & cAtSdhPathAlarmAis) {
        alarm_status = (current_status & cAtSdhPathAlarmAis);
        alarm_type = SONETSDH_PATH_STSAIS;
        ERR(" AIS: %s\n", (alarm_status) ? "Raise" : "Clear");
        cem_fpga_alarm_change_state(p_ckt_info, PATH_ALARM_TYPE,
                                    alarm_type, alarm_status);
    }
    if (changed_alarms & cAtSdhPathAlarmUneq) {
        alarm_status = (current_status & cAtSdhPathAlarmUneq);
        alarm_type = SONETSDH_PATH_UNEQUIPPED;
        ERR(" UNEQ: %s\n", (alarm_status) ? "Raise" : "Clear");
        cem_fpga_alarm_change_state(p_ckt_info, PATH_ALARM_TYPE,
                                    alarm_type, alarm_status);
    }
    if (changed_alarms & cAtSdhPathAlarmRdi) {
        alarm_status = (current_status & cAtSdhPathAlarmRdi);
        alarm_type = SONETSDH_PATH_STSRDI;
        ERR(" RDI: %s\n", (alarm_status) ? "Raise" : "Clear");
        cem_fpga_alarm_change_state(p_ckt_info, PATH_ALARM_TYPE,
                                    alarm_type, alarm_status);
    }
    return;
}

void cem_fpga_vt_alarm_change_state (AtChannel channel, 
                                     uint32 changed_alarms, 
                                     uint32 current_status)
{
    sonetsdh_path_alarm_map_e alarm_type = 0;
    boolean alarm_status = FALSE;
    essi_ckt_t ckt_info; 
    essi_ckt_t *p_ckt_info = &ckt_info;
    cem_fpga_alarm_change_state_fn_t cem_fpga_alarm_change_state;
    cem_fpga_alarm_change_state = 
               g_dev_cem_fpga->callout->alarm_change_state;

    ERR("\n Alarm changed on VT. ckt_info = %d.%d.%d.%d", ckt_info.line_id,
                                                          ckt_info.sts1_id,
                                                          ckt_info.tug2_id,
                                                          ckt_info.tu11_id);
    if (changed_alarms & cAtSdhPathAlarmLop) {
        alarm_status = (current_status & cAtSdhPathAlarmLop);
        alarm_type = SONETSDH_VT_PATHLOP;
        ERR(" LOP: %s\n", (alarm_status) ? "Raise" : "Clear");
        cem_fpga_alarm_change_state(p_ckt_info, VT_ALARM_TYPE,
                                    alarm_type, alarm_status);
    }
    if (changed_alarms & cAtSdhPathAlarmAis) {
        alarm_status = (current_status & cAtSdhPathAlarmAis);
        alarm_type = SONETSDH_VT_PATHAIS;
        ERR(" AIS: %s\n", (alarm_status) ? "Raise" : "Clear");
        cem_fpga_alarm_change_state(p_ckt_info, VT_ALARM_TYPE,
                                    alarm_type, alarm_status);
    }
    if (changed_alarms & cAtSdhPathAlarmUneq) {
        alarm_status = (current_status & cAtSdhPathAlarmUneq);
        alarm_type = SONETSDH_VT_UNEQUIPPED;
        ERR(" UNEQ: %s\n", (alarm_status) ? "Raise" : "Clear");
        cem_fpga_alarm_change_state(p_ckt_info, VT_ALARM_TYPE,
                                    alarm_type, alarm_status);
    }
    if (changed_alarms & cAtSdhPathAlarmRdi) {
        alarm_status = (current_status & cAtSdhPathAlarmRdi);
        alarm_type = SONETSDH_VT_PATHRDI;
        ERR(" RDI: %s\n", (alarm_status) ? "Raise" : "Clear");
        cem_fpga_alarm_change_state(p_ckt_info, VT_ALARM_TYPE,
                                    alarm_type, alarm_status);
    }
    if (changed_alarms & cAtSdhPathAlarmRfi) {
        alarm_status = (current_status & cAtSdhPathAlarmRfi);
        alarm_type = SONETSDH_VT_PATHRFI;
        ERR(" RFI: %s\n", (alarm_status) ? "Raise" : "Clear");
        cem_fpga_alarm_change_state(p_ckt_info, VT_ALARM_TYPE,
                                    alarm_type, alarm_status);
    }
    return;
}

void cem_fpga_t3e3_alarm_change_state (AtChannel channel, 
                                       uint32 changed_alarms, 
                                       uint32 current_status)
{
    sonetsdh_dsx3_alarm_map_e alarm_type = 0;
    alarm_task_type_e ckt_type;
    boolean alarm_status = FALSE;
    AtDevice device = AtChannelDeviceGet((AtChannel)channel);
    uint32 product_code = AtDeviceProductCodeGet(device);
    cem_fpga_alarm_change_state_fn_t cem_fpga_alarm_change_state = NULL;
    void *p_ckt_info = NULL; 
    essi_ckt_t essi_info;
    t3e3_ckt_t t3e3_info; 

    if (product_code == OCX_CEM_FPGA_PRODUCT_CODE) {
        p_ckt_info = (essi_ckt_t *)&essi_info; 
        cem_fpga_t3e3_get_channel_to_essi((AtChannel)channel, p_ckt_info);
        ckt_type = T3E3_ALARM_TYPE;
        cem_fpga_alarm_change_state = 
                   g_dev_cem_fpga->callout->alarm_change_state;
        ERR("\n Alarm changed on T3E3. ckt_info = %d.%d", essi_info.line_id,
                                                          essi_info.sts1_id);
    }
    if (product_code == DS3_CEM_FPGA_PRODUCT_CODE) {
        p_ckt_info = (t3e3_ckt_t *)&t3e3_info; 
        cem_fpga_t3e3_get_channel_to_t3e3((AtChannel)channel, p_ckt_info);
        ckt_type = PORT_ALARM_TYPE;
        cem_fpga_alarm_change_state = 
                   g_dev_cem_fpga->callout->alarm_change_state;
        ERR("\n Alarm changed on T3E3. ckt_info = %d", t3e3_info.de3_id);
    } 

    if (cem_fpga_alarm_change_state == NULL) {
        return;
    }

    if (changed_alarms & cAtPdhDe3AlarmLos) {
        alarm_status = (current_status & cAtPdhDe3AlarmLos);
        alarm_type = SONETSDH_DSX3_RX_LOS;
        ERR(" LOS: %s\n", (alarm_status) ? "Raise" : "Clear");
        cem_fpga_alarm_change_state(p_ckt_info, ckt_type,
                                    alarm_type, alarm_status);
    }
    if (changed_alarms & cAtPdhDe3AlarmLof) {
        alarm_status = (current_status & cAtPdhDe3AlarmLof);
        alarm_type = SONETSDH_DSX3_RX_LOF;
        ERR(" LOF: %s\n", (alarm_status) ? "Raise" : "Clear");
        cem_fpga_alarm_change_state(p_ckt_info, ckt_type,
                                    alarm_type, alarm_status);
    }
    if (changed_alarms & cAtPdhDe3AlarmAis) {
        alarm_status = (current_status & cAtPdhDe3AlarmAis);
        alarm_type = SONETSDH_DSX3_RX_AIS;
        ERR(" AIS: %s\n", (alarm_status) ? "Raise" : "Clear");
        cem_fpga_alarm_change_state(p_ckt_info, ckt_type,
                                    alarm_type, alarm_status);
    }
    if (changed_alarms & cAtPdhDe3AlarmRai) {
        alarm_status = (current_status & cAtPdhDe3AlarmRai);
        alarm_type = SONETSDH_DSX3_RX_RAI;
        ERR(" RAI: %s\n", (alarm_status) ? "Raise" : "Clear");
        cem_fpga_alarm_change_state(p_ckt_info, ckt_type,
                                    alarm_type, alarm_status);
    }
    return;
}

void cem_fpga_t1e1_alarm_change_state (AtChannel channel, 
                                       uint32 changed_alarms, 
                                       uint32 current_status)
{
    sonetsdh_dsx1_alarm_map_e alarm_type = 0;
    alarm_task_type_e ckt_type = PORT_ALARM_TYPE;
    boolean alarm_status = FALSE;
    AtDevice device = AtChannelDeviceGet((AtChannel)channel);
    uint32 product_code = AtDeviceProductCodeGet(device);
    cem_fpga_alarm_change_state_fn_t cem_fpga_alarm_change_state = NULL;
    essi_ckt_t essi_info; 
    t3e3_ckt_t t3e3_info; 
    t1e1_ckt_t t1e1_info; 
    void *p_ckt_info = NULL; 

    if (product_code == OCX_CEM_FPGA_PRODUCT_CODE) {
        p_ckt_info = (essi_ckt_t *)&essi_info; 
        cem_fpga_t1e1_get_channel_to_essi((AtChannel)channel, p_ckt_info);
        ckt_type = T1E1_ALARM_TYPE;
        cem_fpga_alarm_change_state = 
                    g_dev_cem_fpga->callout->alarm_change_state;
        ERR("\n Alarm changed on T1E1. ckt_info = %d.%d.%d.%d", essi_info.line_id,
                                                              essi_info.sts1_id,
                                                              essi_info.tug2_id,
                                                              essi_info.tu11_id);
    }
    if (product_code == DS3_CEM_FPGA_PRODUCT_CODE) {
        p_ckt_info = (t3e3_ckt_t *)&t3e3_info; 
        cem_fpga_t1e1_get_channel_to_t3e3((AtChannel)channel, p_ckt_info);
        ckt_type = T1E1_ALARM_TYPE;
        cem_fpga_alarm_change_state = 
                     g_dev_cem_fpga->callout->alarm_change_state;
        ERR("\n Alarm changed on T1E1. ckt_info = %d.%d", t3e3_info.de3_id,
                                                          t3e3_info.de1_id);
    } 
    if (product_code == DS1_CEM_FPGA_PRODUCT_CODE) {
        p_ckt_info = (t1e1_ckt_t *)&t1e1_info; 
        cem_fpga_t1e1_get_channel_to_t1e1((AtChannel)channel, p_ckt_info);
        ckt_type = PORT_ALARM_TYPE;
        cem_fpga_alarm_change_state = 
                     g_dev_cem_fpga->callout->alarm_change_state;
        ERR("\n Alarm changed on T1E1. ckt_info = %d", t1e1_info.de1_id);
    } 

    if (cem_fpga_alarm_change_state == NULL) {
        return;
    }

    if (changed_alarms & cAtPdhDe1AlarmLos) {
        alarm_status = (current_status & cAtPdhDe1AlarmLos);
        alarm_type = SONETSDH_DSX1_RX_LOS;
        ERR(" LOS: %s\n", (alarm_status) ? "Raise" : "Clear");
        cem_fpga_alarm_change_state(p_ckt_info, ckt_type,
                                    alarm_type, alarm_status);
    }
    if (changed_alarms & cAtPdhDe1AlarmLof) {
        alarm_status = (current_status & cAtPdhDe1AlarmLof);
        alarm_type = SONETSDH_DSX1_RX_LOF;
        ERR(" LOF: %s\n", (alarm_status) ? "Raise" : "Clear");
        cem_fpga_alarm_change_state(p_ckt_info, ckt_type,
                                    alarm_type, alarm_status);
    }
    if (changed_alarms & cAtPdhDe1AlarmAis) {
        alarm_status = (current_status & cAtPdhDe1AlarmAis);
        alarm_type = SONETSDH_DSX1_RX_AIS;
        ERR(" AIS: %s\n", (alarm_status) ? "Raise" : "Clear");
        cem_fpga_alarm_change_state(p_ckt_info, ckt_type,
                                    alarm_type, alarm_status);
    }
    if (changed_alarms & cAtPdhDe1AlarmRai) {
        alarm_status = (current_status & cAtPdhDe1AlarmRai);
        alarm_type = SONETSDH_DSX1_RX_RAI;
        ERR(" RAI: %s\n", (alarm_status) ? "Raise" : "Clear");
        cem_fpga_alarm_change_state(p_ckt_info, ckt_type,
                                    alarm_type, alarm_status);
    }
    if (changed_alarms & cAtPdhDe1AlarmLomf) {
        alarm_status = (current_status & cAtPdhDe1AlarmLomf);
        alarm_type = SONETSDH_DSX1_RX_LOMF;
        ERR(" LOMF: %s\n", (alarm_status) ? "Raise" : "Clear");
        cem_fpga_alarm_change_state(p_ckt_info, ckt_type,
                                    alarm_type, alarm_status);
    }
    return;
}
//////
static boolean cem_fpga_sts1_path_provision (dev_cem_fpga_object_t *cem_fpga,
        uint16_t port, uint16_t path_num, path_mode_e path_mode)
{
if (cem_fpga_simulation) {
    AtSdhLine line_inst = cem_fpga_get_line(cem_fpga, port);
    AtSdhChannel aug1, vc3;
    AtChannel vc3_sts;
    INFO("Path number is %d path_mode is %d", path_num, path_mode);
    uint8_t aug_id = (path_num / NUM_PATHS_IN_AUG1);
    uint8_t vc3_id = (path_num % NUM_PATHS_IN_AUG1);
    /* Setup mapping for AUG-1#aug_id to map VC-3 */
    aug1 = (AtSdhChannel)AtSdhLineAug1Get(line_inst, aug_id);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1Map3xVc3s);
    vc3 = (AtSdhChannel)AtSdhLineVc3Get(line_inst, aug_id, vc3_id);
    vc3_sts = (AtChannel)AtSdhLineVc3Get(line_inst, aug_id, vc3_id);

    switch (path_mode) {
    case PATH_MODE_VT15:
    case PATH_MODE_C12:
    case PATH_MODE_C11:
    case PATH_MODE_VT20:
        AtSdhChannelMapTypeSet(vc3, cAtSdhVcMapTypeVc3Map7xTug2s);
        cem_fpga_alarm_change_state_register(vc3_sts, PATH_ALARM_TYPE, TRUE);
        cem_fpga_alarm_change_state_register((AtChannel)
                                             AtSdhLineAu3Get(line_inst, aug_id,
                                             vc3_id), PATH_ALARM_TYPE, TRUE);
        return (TRUE);
    case PATH_MODE_CT3:
    case PATH_MODE_T3:
    case PATH_MODE_E3:
    case PATH_MODE_CT3_E1:
        AtSdhChannelMapTypeSet(vc3, cAtSdhVcMapTypeVc3MapDe3);
        cem_fpga_alarm_change_state_register(vc3_sts, PATH_ALARM_TYPE, TRUE);
        cem_fpga_alarm_change_state_register((AtChannel)
                                             AtSdhLineTu3Get(line_inst, aug_id,
                                             vc3_id), PATH_ALARM_TYPE, TRUE);
        return (TRUE);
    case PATH_MODE_STS1: 
        AtSdhChannelMapTypeSet(vc3, cAtSdhVcMapTypeVc3MapC3);
        cem_fpga_alarm_change_state_register(vc3_sts, PATH_ALARM_TYPE, TRUE);
        cem_fpga_alarm_change_state_register((AtChannel)
                                             AtSdhLineTu3Get(line_inst, aug_id,
                                             vc3_id), PATH_ALARM_TYPE, TRUE);
        return (TRUE);
    default:
        ERR("path_mode not set correctly");
        return (FALSE);
    }
}
    /* Shouldnt hit here */
    return (TRUE);
}

static boolean cem_fpga_sts3c_path_provision (dev_cem_fpga_object_t *cem_fpga,
        uint16_t port, uint16_t path_num)
{
if (cem_fpga_simulation) {
    AtSdhLine line_inst = cem_fpga_get_line(cem_fpga, port);
    AtSdhChannel aug1, vc4;
    AtChannel vc4_sts3;
    uint8_t aug_id = (path_num / NUM_PATHS_IN_AUG1);

    aug1 = (AtSdhChannel)AtSdhLineAug1Get(line_inst, aug_id);
    AtSdhChannelMapTypeSet(aug1, cAtSdhAugMapTypeAug1MapVc4);
    vc4 = (AtSdhChannel)AtSdhLineVc4Get(line_inst, aug_id);
    AtSdhChannelMapTypeSet(vc4, cAtSdhVcMapTypeVc4MapC4);

    vc4_sts3 = (AtChannel)AtSdhLineVc4Get(line_inst, aug_id);
    cem_fpga_alarm_change_state_register(vc4_sts3, PATH_ALARM_TYPE, TRUE);
    cem_fpga_alarm_change_state_register((AtChannel)
                                         AtSdhLineAu4Get(line_inst, aug_id),
                                         PATH_ALARM_TYPE, TRUE);
}
    return (TRUE);
}

static boolean cem_fpga_t3e3_intf_provision (dev_cem_fpga_object_t *cem_fpga,
        uint16_t port_num, uint16_t path_num, uint16_t t3e3_num)
{
if (cem_fpga_simulation) {
    AtSdhLine line_inst = cem_fpga_get_line(cem_fpga, port_num);
    uint8_t vc3_id = (path_num % NUM_PATHS_IN_AUG1);
    uint8_t aug_id = (path_num / NUM_PATHS_IN_AUG1);
    AtSdhChannel vc3_inst;
    AtPdhChannel de3;

    /* AU-3 <--> VC-3 <--> DS3 */
    vc3_inst = (AtSdhChannel)AtSdhLineVc3Get(line_inst, aug_id, vc3_id);
    AtSdhChannelMapTypeSet(vc3_inst, cAtSdhVcMapTypeVc3MapDe3);

    de3  = (AtPdhChannel)AtSdhChannelMapChannelGet(vc3_inst);
}
    return (TRUE);
}

static boolean cem_fpga_t1e1_intf_provision (dev_cem_fpga_object_t *cem_fpga,
        uint16_t port_num, uint16_t path_num, uint16_t tug2_num, 
        uint16_t t1e1_num)
{
if (cem_fpga_simulation) {
    AtSdhLine line_inst = cem_fpga_get_line(cem_fpga, port_num);
    uint8_t aug_id = (path_num / NUM_PATHS_IN_AUG1);
    uint8_t vc3_id = (path_num % NUM_PATHS_IN_AUG1);
    AtSdhChannel tug2, vc11;
    AtPdhChannel de1;

    /* AU-3 <--> VC-3 <--> TUG-2 <--> VC-11 <--> DS1 */
    tug2 = (AtSdhChannel)AtSdhLineTug2Get(line_inst, aug_id, 
            vc3_id, tug2_num);
    AtSdhChannelMapTypeSet(tug2, cAtSdhTugMapTypeTug2Map4xTu11s);
    vc11 = (AtSdhChannel)AtSdhLineVc1xGet(line_inst, aug_id, 
            vc3_id, tug2_num, t1e1_num);
    INFO("port_num = %d, t1e1 prov path:%d, tug2: %d, t1e1_num: %d", 
            port_num, path_num, tug2_num, t1e1_num);
    AtSdhChannelMapTypeSet(vc11, cAtSdhVcMapTypeVc1xMapDe1);
    de1 = (AtPdhChannel)AtSdhChannelMapChannelGet(vc11);
    /* set the framing type here as of now */
    if(de1){
        AtPdhChannelFrameTypeSet(de1, cAtPdhDs1J1UnFrm);
    }
}
    return (TRUE);
}

static boolean cem_fpga_vt_intf_provision (dev_cem_fpga_object_t *cem_fpga,
        uint16_t port_num, uint16_t path_num, uint16_t tug2_num, 
        uint16_t t1e1_num)
{
if (cem_fpga_simulation) {
    AtSdhLine line_inst = cem_fpga_get_line(cem_fpga, port_num);
    uint8_t aug_id = (path_num / NUM_PATHS_IN_AUG1);
    uint8_t vc3_id = (path_num % NUM_PATHS_IN_AUG1);
    AtSdhChannel vc11, tug2;
    AtChannel vc11_cep;

    /* Setup mapping for AUG-1 to map AU-3 <--> 
     * <--> TUG-2 <--> VC-11 */
    tug2 = (AtSdhChannel)AtSdhLineTug2Get(line_inst, aug_id, 
            vc3_id, tug2_num);
    AtSdhChannelMapTypeSet(tug2, cAtSdhTugMapTypeTug2Map4xTu11s);
    vc11 = (AtSdhChannel)AtSdhLineVc1xGet(line_inst, aug_id, 
            vc3_id, tug2_num, t1e1_num);
    AtSdhChannelMapTypeSet(vc11, cAtSdhVcMapTypeVc1xMapC1x);

    vc11_cep = AtSdhChannelMapChannelGet(vc11);
    cem_fpga_alarm_change_state_register(vc11_cep, VT_ALARM_TYPE, TRUE);
    cem_fpga_alarm_change_state_register((AtChannel)
                                         AtSdhLineTu1xGet(line_inst, aug_id,
                                         vc3_id, tug2_num, t1e1_num), 
                                         PATH_ALARM_TYPE, TRUE);
}
    return (TRUE);
}
static AtDevice Device(void )
{
    return AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
}

static AtModulePdh PdhModule(void )
{
    return (AtModulePdh)AtDeviceModuleGet(Device(), cAtModulePdh);
}

static AtPdhDe1 PdhDe1Get(uint32 de1Id)
{
   return AtModulePdhDe1Get(PdhModule(), de1Id);
}

static AtPdhDe3 PdhDe3Get(uint32 de3Id)
{
    return AtModulePdhDe3Get(PdhModule(), de3Id);
}

static void
cem_fpga_alarm_change_state_register (AtChannel channel, 
                                      alarm_task_type_e alarm_type,
                                      eBool enable)
{
    uint32_t enable_mask = 0, defect_mask = 0;
    static tAtChannelEventListener m_listener_path = 
                         {.AlarmChangeState = NULL,
                          .OamReceived = NULL};
    static tAtChannelEventListener m_listener_vt = 
                         {.AlarmChangeState = NULL,
                          .OamReceived = NULL};
    static tAtChannelEventListener m_listener_t1e1 = 
                         {.AlarmChangeState = NULL,
                          .OamReceived = NULL};
    static tAtChannelEventListener m_listener_t3e3 = 
                         {.AlarmChangeState = NULL,
                          .OamReceived = NULL};
    tAtChannelEventListener *p_listener = NULL;

    switch (alarm_type) {
        case PATH_ALARM_TYPE:
        /* Enable these alarms at PATH level */
            enable_mask = cAtSdhPathAlarmAis | cAtSdhPathAlarmLop | 
                          cAtSdhPathAlarmUneq | cAtSdhPathAlarmPlm | 
                          cAtSdhPathAlarmRdi | cAtSdhPathAlarmErdiP |
                          cAtSdhPathAlarmErdiC |
                          cAtSdhPathAlarmBerSd | cAtSdhPathAlarmBerSf;
            m_listener_path.AlarmChangeState = cem_fpga_path_alarm_change_state;
            p_listener = &m_listener_path;
            break;

        case VT_ALARM_TYPE:
            /* Enable these alarms at VT level */
            enable_mask = cAtSdhPathAlarmAis | cAtSdhPathAlarmLop | 
                          cAtSdhPathAlarmUneq | cAtSdhPathAlarmRdi | 
                          cAtSdhPathAlarmRfi;
            m_listener_vt.AlarmChangeState = cem_fpga_vt_alarm_change_state;
            p_listener = &m_listener_vt;
            break;

        case T1E1_ALARM_TYPE:
            /* Enable these alarms at T1E1 level */
            enable_mask = cAtPdhDe1AlarmLof | 
                          cAtPdhDe1AlarmAis | cAtPdhDe1AlarmRai;
            m_listener_t1e1.AlarmChangeState = cem_fpga_t1e1_alarm_change_state;
            p_listener = &m_listener_t1e1;
            break;

        case T3E3_ALARM_TYPE:
            /* Enable these alarms at T3E3 level */
            enable_mask = cAtPdhDe3AlarmLof | 
                          cAtPdhDe3AlarmAis | cAtPdhDe3AlarmRai | 
                          cAtPdhDe3AlarmSfBer | cAtPdhDe3AlarmSdBer;
            m_listener_t3e3.AlarmChangeState = cem_fpga_t3e3_alarm_change_state;
            p_listener = &m_listener_t3e3;
            break;

        default:
            break;

    }
    if (p_listener) {
        if (enable) {
            defect_mask = enable_mask;
            AtChannelEventListenerAdd(channel, p_listener);
            AtChannelInterruptMaskSet(channel, defect_mask, enable_mask);
        } else {
            defect_mask = enable_mask;
            enable_mask = 0;
            AtChannelEventListenerRemove(channel, p_listener);
            AtChannelInterruptMaskSet(channel, defect_mask, enable_mask);
        }
        ERR("%s interrupt for %s.%s, defect_mask = 0x%08x, enable_mask = 0x%08x\r\n", 
            (enable) ? "Enable" : "Disable",
            AtChannelTypeString((AtChannel)channel),
            AtChannelIdString((AtChannel)channel),
            defect_mask, enable_mask);
    }
}

static boolean cem_fpga_cem_intf_config (dev_cem_fpga_object_t *cem_fpga,
        uint16_t port_num, uint16_t path_num, uint16_t vtg_num, 
        uint16_t t1e1_num, pw_info_t *pw_info, path_mode_e cem_intf_mode,
        uint32_t timeslot_bitmap)
{
if (cem_fpga_simulation) {
    AtPw pw;
    eAtRet  ret;
    AtModulePw pwModule;
    AtSdhChannel vc3, vc11;
    AtChannel de1, de3, vc3_sts, vc4_sts3, vc11_cep;
    AtPdhNxDS0 nxDs0;
    AtPdhDe1 nxde1;
    AtDevice device =
        AtDriverDeviceGet(cem_fpga->cem_fpga_driver, CEM_FPGA_CORE_ID);
    uint8_t aug_id = (path_num / NUM_PATHS_IN_AUG1);
    uint8_t vc3_id = (path_num % NUM_PATHS_IN_AUG1);

    pwModule = 
        (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
   
    INFO("cem_intf_config : mode is %d; aug_id: %d," 
            "path_num %d; vtg %d  2807:17:15; "
            "t1e1 %d; pw-type is %d id is %d port is %d", 
            cem_intf_mode, aug_id, path_num, vtg_num,
            t1e1_num, pw_info->pw_type, pw_info->cem_id, port_num);

    /*TODO: 
     * Simulation CEM-FPGA for OCx has multiple product ids,
     * but for final FPGA only single product code,so shall modify 
     * the code to use switch case 
     */
    if ((cem_fpga->product_code == DS1_CEM_FPGA_PRODUCT_CODE_EP6_C1) 
        || (cem_fpga->product_code == DS1_CEM_FPGA_PRODUCT_CODE)) {
        INFO("in t1e1 board");
        /* Create a SAToP to emulate this E1 */
        AtPdhChannel pdhDe1 = (AtPdhChannel)PdhDe1Get((uint32_t)port_num);
        AtPdhChannelFrameTypeSet(pdhDe1, cAtPdhDs1J1UnFrm);
        pw = (AtPw)AtModulePwSAToPCreate(pwModule, pw_info->cem_id);
        ret = AtPwCircuitBind(pw,(AtChannel)pdhDe1);
        ERR(" AtPwCircuitBind : cem_intf : ret :%ss\n", AtRet2String(ret));
    } else if (cem_fpga->product_code == DS3_CEM_FPGA_PRODUCT_CODE) {
       AtPdhDe3 de3_1 = (AtPdhDe3)PdhDe3Get((uint32_t)port_num);
       ret = AtPdhChannelFrameTypeSet((AtPdhChannel)de3_1, cAtPdhDs3Unfrm);
       if (ret != cAtOk) {
        ERR("cem_fpga_t3e3_framing_config not successful:  ret :0x%x",cAtOk );
        return (FALSE);
       }

        ERR("T3e3 board framing initialized ");
        AtPdhChannel pdhDe3 = (AtPdhChannel)PdhDe3Get((uint32_t)port_num);
        pw = (AtPw)AtModulePwSAToPCreate(pwModule, pw_info->cem_id);
        INFO("T3e3 board SAToP created port_num = %d pw-id:%d\n ", port_num, 
                pw_info->cem_id);
        AtPwCircuitBind(pw,(AtChannel)pdhDe3);
        ERR("T3e3 board  pwcir bind passed ");
    } else {
        INFO("in oc48 board");
        AtSdhLine line_inst = cem_fpga_get_line(cem_fpga, port_num);
        switch (cem_intf_mode) {
        case PATH_MODE_VT15:
            switch (pw_info->pw_type) {
            case PW_TYPE_SATOP:
                vc11 = (AtSdhChannel)AtSdhLineVc1xGet(line_inst, aug_id,
                            vc3_id, vtg_num, t1e1_num);
                de1 = AtSdhChannelMapChannelGet(vc11);
                pw = (AtPw)AtModulePwSAToPCreate(pwModule, 
                        pw_info->cem_id);
                AtPwCircuitBind(pw, de1);
                break;
            case PW_TYPE_CESOP:
                vc11 = (AtSdhChannel)AtSdhLineVc1xGet(line_inst, aug_id,
                            path_num, vtg_num, t1e1_num);
                nxde1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc11);
                nxDs0 = AtPdhDe1NxDs0Create(nxde1, timeslot_bitmap);
                pw = (AtPw)AtModulePwCESoPCreate(pwModule, 
                            pw_info->cem_id, cAtPwCESoPModeBasic);
                AtPwCircuitBind(pw, (AtChannel)nxDs0);
                break;
            case PW_TYPE_CEP:
                vc11_cep = (AtChannel)AtSdhLineVc1xGet(line_inst, aug_id,
                                path_num, vtg_num, t1e1_num);
                pw = (AtPw)AtModulePwCepCreate(pwModule, pw_info->cem_id,
                                cAtPwCepModeBasic);
                AtPwCircuitBind(pw, vc11_cep);
                break;
            default:
                ERR("Wrong pw_circuit_type !");
                break;
            }
            break;
        case PATH_MODE_CT3:
            vc3 = 
               (AtSdhChannel)AtSdhLineVc3Get(line_inst, aug_id, vc3_id);
            de3  = AtSdhChannelMapChannelGet(vc3);
            if (pw_info->pw_type == PW_TYPE_SATOP) {
                pw   = (AtPw)AtModulePwSAToPCreate(pwModule, pw_info->cem_id);
                AtPwCircuitBind(pw, de3);
            } else {
                ERR("Wrong pw_circuit_type !");
            }
            break;
        case PATH_MODE_STS1: 
            vc3_sts = (AtChannel)AtSdhLineVc3Get(line_inst, aug_id, vc3_id);
            if (pw_info->pw_type == PW_TYPE_CEP) {
                pw  = (AtPw)AtModulePwCepCreate(pwModule, pw_info->cem_id, 
                              cAtPwCepModeBasic);
                AtPwCircuitBind(pw, vc3_sts);
            } else {
                ERR("Wrong pw_circuit_type !");
            }
            break;
    	case PATH_MODE_STS3C: 
            vc4_sts3 = (AtChannel)AtSdhLineVc4Get(line_inst, aug_id);
            if (pw_info->pw_type == PW_TYPE_CEP) {
                pw  = (AtPw)AtModulePwCepCreate(pwModule, pw_info->cem_id, 
                         cAtPwCepModeBasic);
                AtPwCircuitBind(pw, vc4_sts3);
            } else {
                ERR("Wrong pw_circuit_type !");
            }
            break;
    	default:
            break;
     	}	
    }
}
    return (TRUE);
}

static boolean cem_fpga_cem_connect_config (dev_cem_fpga_object_t *cem_fpga,
        pw_info_t *pw_info)
{
if (cem_fpga_simulation) {
    AtModulePw pwModule;
    AtPw pw;
    AtPwMplsPsn mplsPsn = AtPwMplsPsnNew();
    tAtEthVlanTag cVlan;
    tAtPwMplsLabel label;
    AtDevice device =
        AtDriverDeviceGet(cem_fpga->cem_fpga_driver, CEM_FPGA_CORE_ID);
    AtModuleEth ethModule = 
        (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    
    ERR("cem_conn_config  pw-type is %d id is %d ",
            pw_info->pw_type, pw_info->cem_id);

    /* Get the pw module */
    pwModule = 
        (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    INFO("cem_conn_config  pw-type is %d id is %d \n",  pw_info->pw_type, 
            pw_info->cem_id);
    /* Get the Pw */
    pw = AtModulePwGetPw(pwModule, (uint16_t)pw_info->cem_id);
    INFO(" local_mpls_label : %d, pw-id :%d \n", pw_info->local_mpls_label, 
            pw_info->cem_id);

    /* Fill the vlan fields */
    cVlan.cfi = 1;
    cVlan.priority = 1;
    cVlan.vlanId = CEM_VLAN_ID;

    /* Fill Ethernet header */
    AtPwEthHeaderSet(pw, pw_info->dst_mac, &cVlan, NULL);
    AtEthPortMacCheckingEnable(AtModuleEthPortGet(ethModule,EGRESS_ETH_PORT),
                                FALSE);

    /* Set mpls header */
    label.label = (pw_info->local_mpls_label);
    label.timeToLive = 255; /* Check with Anand */
    label.experimental = 0;
    AtPwMplsPsnInnerLabelSet(mplsPsn, &label);

    /* Set mpls expected label as inner label */
    AtPwMplsPsnExpectedLabelSet(mplsPsn, pw_info->local_mpls_label); 

    /* bind the mpls info with pw */
    AtPwPsnSet(pw, (AtPwPsn)mplsPsn);

    /* Get the egress Ethernet port */
    AtPwEthPortSet(pw, AtModuleEthPortGet(ethModule, 
                EGRESS_ETH_PORT));

    ERR("cem connect done");

    /* delete the mplspsn obj in tis context */
    AtObjectDelete((AtObject)mplsPsn);

}
    return (TRUE);
}

static boolean cem_fpga_cem_channel_enable (dev_cem_fpga_object_t *cem_fpga,
        uint16_t cem_id)
{
if (cem_fpga_simulation) {
    AtModulePw pwModule;
    AtPw pw;
    AtSurEngine engine;
    AtDevice device =
        AtDriverDeviceGet(cem_fpga->cem_fpga_driver, CEM_FPGA_CORE_ID);

    /* Get the pw module */
    pwModule = 
        (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);

    /* Get the pw for the given cem_id */
    pw = AtModulePwGetPw(pwModule, cem_id);

    /* Enable the pw */
    AtChannelEnable((AtChannel)pw, cAtTrue);
    /* Enable the pw Engine*/
    engine = AtChannelSurEngineGet((AtChannel)pw);
    AtSurEngineEnable(engine, cAtTrue);
}
    return (TRUE);
}

static boolean cem_fpga_cem_connect_unconfig (dev_cem_fpga_object_t *cem_fpga,
        uint16_t cem_id)
{
if (cem_fpga_simulation) {
    AtModulePw pwModule;
    AtPw pw;
    eAtRet ret = cAtOk;
    AtSurEngine engine;
    AtDevice device =
        AtDriverDeviceGet(cem_fpga->cem_fpga_driver, CEM_FPGA_CORE_ID);

    INFO("the cem-id i got is %d", cem_id);
    /* Get the pw module */
    pwModule = 
        (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    pw = AtModulePwGetPw(pwModule, cem_id);
    /*Get the pw engine */
    engine = AtChannelSurEngineGet((AtChannel)pw);
    if (!pw) {
        ERR("PW is not valied ");
        return (FALSE);
    }

    AtChannelEnable((AtChannel)pw, cAtFalse);
    ret = AtPwCircuitUnbind(pw);
    if (ret != cAtOk) {
        ERR("PW unbind didnt happen properly");
        return (FALSE);
    }

    /* Delete the pw for the given cem_id */
    ret = AtModulePwDeletePw(pwModule, cem_id);
    if (ret != cAtOk) {
        ERR("PW delete didnt happen properly");
        return (FALSE);
    } else {
        AtSurEngineEnable(engine, cAtFalse);
        return (TRUE);
    }
}
    return (TRUE);
}

static boolean cem_fpga_cem_intf_unconfig (dev_cem_fpga_object_t *cem_fpga,
        uint32_t port_num, uint16_t t3e3, uint16_t t1e1_num)
{
    /* Delete the mapping done by SDK */
    return(TRUE);
}

static boolean cem_fpga_t1e1_config (dev_cem_fpga_object_t *cem_fpga)
{
    return (TRUE);
}

static boolean cem_fpga_t3e3_config (dev_cem_fpga_object_t *cem_fpga)
{
    return (TRUE);
}

static boolean cem_fpga_line_config (dev_cem_fpga_object_t *cem_fpga,
        uint16_t port_num, ctrlr_framing_e framing, port_rate_e rate)
{
if (cem_fpga_simulation) {
    AtSdhLine line_inst = cem_fpga_get_line(cem_fpga, port_num);

    INFO("Entering line config. Framing is %d, rate is %d", framing, rate);

    switch (framing) {
    case FRAMING_SONET:
        AtSdhLineModeSet(line_inst, cAtSdhLineModeSonet);
        break;
    case FRAMING_SDH:
        AtSdhLineModeSet(line_inst, cAtSdhLineModeSdh);
        break;
    default:
        ERR("Wrong framing type");
        return (FALSE);
    }

    switch (rate) {
    case OC3:
        AtSdhLineRateSet(line_inst, cAtSdhLineRateStm1);
        break;
    case OC12:
        AtSdhLineRateSet(line_inst, cAtSdhLineRateStm4);
        break;
    case OC48:
        AtSdhLineRateSet(line_inst, cAtSdhLineRateStm16);
        break;
    case OC192:
    default:
        ERR("Wrong rate type");
        return (FALSE);
    }
}
    return (TRUE);
}

static boolean cem_fpga_path_unprovision (dev_cem_fpga_object_t *cem_fpga)
{
    return (TRUE);
}

static boolean 
cem_fpga_path_loopback_config (dev_cem_fpga_object_t *cem_fpga,
                               uint16_t port_num, uint8_t path_num,
                               loopback_e loopback)
{
if (cem_fpga_simulation) {
    AtSdhLine line_inst = cem_fpga_get_line(cem_fpga, port_num);
    AtChannel channel;
    eAtRet ret_val = cAtError;
    uint8_t aug_id = (path_num / NUM_PATHS_IN_AUG1);
    uint8_t vc3_id = (path_num % NUM_PATHS_IN_AUG1);

    channel = (AtChannel)AtSdhLineVc3Get(line_inst, aug_id, vc3_id);

    switch (loopback) {
    case LOOPBACK_NONE:
        ret_val = AtChannelLoopbackSet(channel, cAtLoopbackModeRelease);
        break;
    case LOOPBACK_LOCAL_LINE:
        ret_val = AtChannelLoopbackSet(channel, cAtLoopbackModeLocal);
        break;
    case LOOPBACK_NETWORK_PAYLOAD:
        ret_val = AtChannelLoopbackSet(channel, cAtLoopbackModeRemote);
        break;
    default:
        break;
    }

    if (ret_val == cAtOk) {
        return (TRUE);
    }
    return (FALSE);
}
    return TRUE;
}

static eAtBerRate get_at_ber_rate (uint8_t value)
{
    eAtBerRate ber_rate;
    switch (value) {
        case BER_1_IN_1K:
            ber_rate = cAtBerRate1E3;
            break;
        case BER_1_IN_10K:
            ber_rate = cAtBerRate1E4;
            break;
        case BER_1_IN_100K:
            ber_rate = cAtBerRate1E5;
            break;
        case BER_1_IN_1M:
            ber_rate = cAtBerRate1E6;
            break;
        case BER_1_IN_10M:
            ber_rate = cAtBerRate1E7;
            break;
        case BER_1_IN_100M:
            ber_rate = cAtBerRate1E8;
            break;
        case BER_1_IN_1B:
            ber_rate = cAtBerRate1E9;
            break;
        case BER_NULL:
        case BER_DISABLE:
        case BER_INVALID:
        default:
            ber_rate = cAtBerRateUnknown;
            break;
    }
    return (ber_rate);
}

static boolean 
cem_fpga_poh_config (dev_cem_fpga_object_t *cem_fpga, uint16_t port_num, 
                         uint8_t path_num, uint8_t path_mode,
                         path_poh_type_e type, void *data, uint8_t plength)
{
if (cem_fpga_simulation) {
    AtSdhLine line_inst = cem_fpga_get_line(cem_fpga, port_num); 
    AtSdhChannel channel = 0; 
    AtBerController berController;
    eAtModuleSdhRet ret_val = cAtError; 
    uint8_t value;
    uint8_t *msg = NULL;
    tAtSdhTti tti;
    eAtSdhTtiMode tti_mode;
    eAtBerRate ber_rate;

    uint8_t aug1_id = (path_num / NUM_PATHS_IN_AUG1);
    uint8_t vc3_id = (path_num % NUM_PATHS_IN_AUG1);
    uint8_t aug4_id = (path_num / 12);

    if (path_mode == PATH_MODE_STS1) { 
        channel = (AtSdhChannel)AtSdhLineVc3Get(line_inst, aug1_id, vc3_id);
    } else if (path_mode == PATH_MODE_STS3C) { 
        channel = (AtSdhChannel)AtSdhLineVc4Get(line_inst, aug1_id);
    } else if (path_mode == PATH_MODE_STS12C) { 
        channel = (AtSdhChannel)AtSdhLineVc4_4cGet(line_inst, aug4_id);
    } else if (path_mode == PATH_MODE_STS48C) { 
        /* This is temp code as we don't have AtSdhLineVc4_16cGet  api yet */
        AtSdhChannel aug16   = (AtSdhChannel)AtSdhLineAug16Get(line_inst, 0);
        AtSdhChannel au4_16c = AtSdhChannelSubChannelGet(aug16, 0);
        AtSdhChannel vc4_16c = AtSdhChannelSubChannelGet(au4_16c, 0);
        channel = vc4_16c;
    }
    switch (type) {
        case PATH_POH_TYPE_TX_C2_BYTE:
            value = *(uint8_t *)data;
            AtSdhChannelTxOverheadByteSet(channel, 
                    cAtSdhPathOverheadByteC2, value);
            break;
        case PATH_POH_TYPE_TX_PATH_TRACE_BUFFER:
            msg = (uint8_t *)data;
            strncpy((char *)&tti.message, (char *)msg, strlen((char *)msg));
            if (plength == 16) {
                tti_mode = cAtSdhTtiMode16Byte;
            } else if (plength == 64) {
                tti_mode = cAtSdhTtiMode64Byte;
            } else {
                tti_mode = cAtSdhTtiMode1Byte;
            }
            if (AtSdhTtiMake(tti_mode, msg, plength, &tti) == &tti) {
                ret_val = AtSdhChannelTxTtiSet(channel, &tti);
            }
            break;
        case PATH_POH_TYPE_EXPECTED_J1_TRACE:
            msg = (uint8_t *)data;
            strncpy((char *)&tti.message, (char *)msg, strlen((char *)msg));
            if (plength == 16) {
                tti_mode = cAtSdhTtiMode16Byte;
            } else if (plength == 64) {
                tti_mode = cAtSdhTtiMode64Byte;
            } else {
                tti_mode = cAtSdhTtiMode1Byte;
            }
            if (AtSdhTtiMake(tti_mode, msg, plength, &tti) == &tti) {
                ret_val = AtSdhChannelExpectedTtiSet(channel, &tti);
            }
            break;
            /* TODO API not yet available */
        case PATH_POH_TYPE_B3_TCA_THRESHOLD:
            value = *(uint8_t *)data;
            ber_rate = get_at_ber_rate(value); //to test threshold
            berController = AtSdhChannelBerControllerGet(channel);
            AtBerControllerTcaThresholdSet(berController, ber_rate);
            AtBerControllerEnable(berController, cAtTrue);
            break;
        case PATH_POH_TYPE_B3_SD_THRESHOLD:
            value = *(uint8_t *)data;
            ber_rate = get_at_ber_rate(value);
            berController = AtSdhChannelBerControllerGet(channel);
            AtBerControllerSdThresholdSet(berController, ber_rate);
            AtBerControllerEnable(berController, cAtTrue);
            break;
        case PATH_POH_TYPE_B3_SF_THRESHOLD:
            value = *(uint8_t *)data;
            ber_rate = get_at_ber_rate(value);
            berController = AtSdhChannelBerControllerGet(channel);
            AtBerControllerSfThresholdSet(berController, ber_rate);
            AtBerControllerEnable(berController, cAtTrue);
            break;
        default:
            break;
        if (ret_val == cAtOk) {
            return (TRUE);
        }
    }
}
    return (TRUE);
}

static boolean 
cem_fpga_vt_oh_config (dev_cem_fpga_object_t *cem_fpga, uint16_t port_num, 
                         uint16_t path_num, uint16_t vtg_num, uint16_t t1e1_num,
                         vt_oh_type_e oh_type, void *data, uint8_t length)
{
    #ifdef CEM_FPGA_SIMULATION
    AtSdhLine line_inst = cem_fpga_get_line(cem_fpga, port_num); 
    AtSdhChannel channel = 0; 
    eAtModuleSdhRet ret_val = cAtError; 
    uint8_t value;
    uint8_t *msg = NULL;
    tAtSdhTti tti;
    eAtSdhTtiMode tti_mode;

    uint8_t aug1_id = (path_num / NUM_PATHS_IN_AUG1);
    //uint8_t vc3_id = (path_num % NUM_PATHS_IN_AUG1);
    //uint8_t aug4_id = (path_num / 12);
    uint8_t au3tug3_id = (path_num % NUM_PATHS_IN_AUG1);
    uint8_t tug2_id = vtg_num;
    uint8_t tu_id = t1e1_num;

    channel = (AtSdhChannel)AtSdhLineVc1xGet(line_inst, aug1_id, au3tug3_id, 
              tug2_id, tu_id);

    switch(oh_type) {
        case VT_POH_TYPE_TX_J2_TRACE_BUFFER:
            msg = (char *)data;
            strncpy((char *)&tti.message, (char *)msg, strlen((char *)msg));
            if (length == 16) {
                tti_mode = cAtSdhTtiMode16Byte;
            } else if (length == 64) {
                tti_mode = cAtSdhTtiMode64Byte;
            } else {
                tti_mode = cAtSdhTtiMode1Byte;
            }
            if (AtSdhTtiMake(tti_mode, msg, length, &tti) == &tti) {
                ret_val = AtSdhChannelTxTtiSet(channel, &tti);
            }
            break;
        case VT_POH_TYPE_RX_J2_TRACE_BUFFER:
            msg = (char *)data;
            strncpy((char *)&tti.message, (char *)msg, strlen((char *)msg));
            if (length == 16) {
                tti_mode = cAtSdhTtiMode16Byte;
            } else if (length == 64) {
                tti_mode = cAtSdhTtiMode64Byte;
            } else {
                tti_mode = cAtSdhTtiMode1Byte;
            }
            if (AtSdhTtiMake(tti_mode, msg, length, &tti) == &tti) {
                ret_val = AtSdhChannelExpectedTtiSet(channel, &tti);
            }
            break;
        case VT_POH_TYPE_TX_V5_BYTE:
            value = *(uint8_t *)data;
            ret_val = AtSdhPathTxPslSet((AtSdhPath)channel, value);
            break;
        default:
            if (ret_val == cAtOk) {
            return (TRUE);
        }
    }
#endif
    return(TRUE);
}

static boolean cem_fpga_bert_config (void *bert)
{
    return (TRUE);
}

    static boolean
cem_fpga_stats(dev_cem_fpga_object_t *cem_fpga,
               uint16_t cem_id,               
               pw_stats_t *pw_stats)
{
    if (cem_fpga_simulation) {
        AtModulePw pwModule;
        AtPw pw;

        /* Variable to hold Pseudowire counter object */
        AtPwCounters counter;

        AtDevice device =
            AtDriverDeviceGet(cem_fpga->cem_fpga_driver, CEM_FPGA_CORE_ID);
        INFO("the cem-id  got is %d", cem_id); 
        /* Get the pw module */
        pwModule =
            (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
        pw = AtModulePwGetPw(pwModule, cem_id);
        if (!pw) {
            ERR("PW is not valied ");
            return (FALSE);
        }

        AtChannelAllCountersClear((AtChannel)pw, &counter);
        /*Code to get PwPmon counters */
        AtSurEngine engine = AtChannelSurEngineGet((AtChannel)pw); 
        /* And get the built-in Surveillance engine */
        /* Start running */
        AtPmParam Param;
        AtPmRegister PmReg;
        Param  =  AtSurEnginePmParamGet(engine, cAtSurEnginePwPmParamEs);
        PmReg = AtPmParamCurrentSecondRegister(Param);
        pw_stats->num_error_seconds =  AtPmRegisterReset(PmReg);

        Param  =  AtSurEnginePmParamGet(engine, cAtSurEnginePwPmParamSes);
        PmReg = AtPmParamCurrentSecondRegister(Param);
        pw_stats->num_severly_error_seconds = AtPmRegisterReset(PmReg);

        Param  =  AtSurEnginePmParamGet(engine,cAtSurEnginePwPmParamUas);
        PmReg = AtPmParamCurrentSecondRegister(Param);
        pw_stats->unavailable_seconds = AtPmRegisterReset(PmReg);

        AtPwTdmCounters tdmPwCounter = (AtPwTdmCounters)counter;
        pw_stats->num_input_bytes  = AtPwCountersRxPayloadBytesGet(counter);
        pw_stats->num_output_bytes = AtPwCountersTxPayloadBytesGet(counter);
        pw_stats->num_output_packets = AtPwCountersTxPacketsGet(counter);
        pw_stats->num_input_packets = AtPwCountersRxPacketsGet(counter);
        pw_stats->malformed_packets = 
            AtPwCountersRxMalformedPacketsGet(counter);
        pw_stats->misorder_dropped = 
            AtPwCountersRxOutOfSeqDropPacketsGet(counter);
        pw_stats->num_input_pkt_drop = AtPwCountersRxLostPacketsGet(counter);
        /*FIX_ME: Once Arrive supports TxLost packets need to collect the same
         * pw_stats->num_output_pkt_drop = AtPwCountersTxLostPacketsGet(counter);
         */
        pw_stats->num_output_pkt_drop = 0;
        pw_stats->reorder_packets = AtPwCountersRxReorderedPacketsGet(counter);
        pw_stats->jitter_buffer_underruns = 
            AtPwTdmCountersRxJitBufUnderrunGet(tdmPwCounter);
        pw_stats->jitter_buffer_overruns = 
            AtPwTdmCountersRxJitBufOverrunGet(tdmPwCounter);
    }
    return (TRUE);
}

static eAtPdhLoopbackMode loopback_enum_ios_to_cem_fpga
					(loopback_e_dsx loopback)
{
    switch(loopback) {
        case DSX_LOOP_NONE:
            return cAtPdhLoopbackModeRelease;
        case DSX_LOOP_NETWK_PAYLOAD:
            return cAtPdhLoopbackModeLocalPayload;
        case DSX_LOOP_NETWK_LINE:
            return cAtPdhLoopbackModeLocalLine;
        default:
            ERR("Error: Invalid loopback %d", loopback);
            return cAtPdhLoopbackModeRelease;
    }           
}
static boolean cem_fpga_t1e1_config_loopback(dev_cem_fpga_object_t *cem_fpga, 
        t1e1_t *t1e1_info)
{
    eAtRet  ret;
    eAtPdhLoopbackMode loopback;
    AtPdhChannel de1 = (AtPdhChannel)PdhDe1Get(t1e1_info->t1e1_num);

    loopback = loopback_enum_ios_to_cem_fpga(t1e1_info->loopback);
    INFO("loopback for t1e1_num:%d recv_loopback:%d fpga_loopback:%d\n",
            t1e1_info->t1e1_num,t1e1_info->loopback, loopback);
    ret = AtChannelLoopbackSet((AtChannel)de1, loopback);
    if (ret != cAtOk) {
        ERR("cem_fpga_t1e1_loopback_config not successful:  ret :0x%x",cAtOk );
        return (FALSE);
    }

    return (TRUE);
}

static eAtPdhLoopbackMode 
loopback_t3e3_enum_ios_to_cem_fpga (loopback_e loopback) {
    switch(loopback) {
        case LOOPBACK_NONE:
            return cAtPdhLoopbackModeRelease;
        case LOOPBACK_NETWORK_PAYLOAD:
            return cAtPdhLoopbackModeRemotePayload;
        case LOOPBACK_LOCAL_LINE:
            return cAtPdhLoopbackModeLocalLine;
        default:
            ERR("Error: Invalid loopback %d", loopback);
            return cAtPdhLoopbackModeRelease;
    }
}

static boolean 
cem_fpga_t3e3_config_loopback(dev_cem_fpga_object_t *cem_fpga,
        pdh_t3e3_t *t3e3_info) {
    eAtRet  ret ;
    eAtPdhLoopbackMode loopback;
    AtPdhChannel de3 = (AtPdhChannel)PdhDe3Get(t3e3_info->t3e3_num);

    loopback = loopback_t3e3_enum_ios_to_cem_fpga(t3e3_info->loopback);
    INFO("loopback for t3e3_num:%d recv_loopback:%d fpga_loopback:%d\n",
            t3e3_info->t3e3_num,t3e3_info->loopback, loopback);
    ret = AtChannelLoopbackSet((AtChannel)de3, loopback);

    if (ret != cAtOk) {
        ERR("cem_fpga_t3e3_loopback_config not successful: %s\r\n", 
                AtRet2String(ret));
        return (FALSE);
    }


    return (TRUE);
}

static boolean cem_fpga_t3e3_config_mode(dev_cem_fpga_object_t *cem_fpga,
        pdh_t3e3_t *t3e3_info)
{
    return (TRUE);
}

static boolean cem_fpga_t1e1_config_mode(dev_cem_fpga_object_t *cem_fpga, 
		t1e1_t *t1e1_info)
{
    return (TRUE);
}

static eBool  if_state_enum_ios_to_cem_fpga (if_state_e if_state)
{
    switch(if_state){ 
        case STATE_DOWN:
        case ADMIN_STATE_DOWN:
            return cAtFalse;
        case STATE_UP:
        case ADMIN_STATE_UP:
            return cAtTrue;
        default:
            ERR("Error: Invalid state %d", if_state);
            return cAtFalse;
    }           
}
static boolean cem_fpga_t1e1_enable_if_state(dev_cem_fpga_object_t *cem_fpga, 
        t1e1_t *t1e1_info)
{
    eAtRet  ret;
    eBool if_state;
    AtPdhChannel de1 = (AtPdhChannel)PdhDe1Get(t1e1_info->t1e1_num);

    INFO("%s: Entry", __FUNCTION__);
    if_state  = if_state_enum_ios_to_cem_fpga(t1e1_info->if_state);
    INFO("Enable if state for t1e1_num:%d recv_if_state:%d fpga_if_state:%d\n",
            t1e1_info->t1e1_num,t1e1_info->if_state, if_state);
    ret = AtChannelEnable((AtChannel)de1, if_state);
    if (ret != cAtOk) {
        ERR("cem_fpga_t1e1_if_state_config not successful:  ret :0x%x",cAtOk );
        return (FALSE);
    }
    ERR("\n calling alarm register for t1e1 num %d", t1e1_info->t1e1_num);
    cem_fpga_alarm_change_state_register((AtChannel)de1, T1E1_ALARM_TYPE,
                                         if_state);
    return (TRUE);
}

static eAtPdhDe1FrameType frame_type_enum_ios_to_cem_fpga
					(t1e1_framing_e framing)
{
     switch(framing) {
        case T1E1_UNFRAMED:
             return cAtPdhDs1J1UnFrm;
        case T1E1_FRM_ESF:
             return cAtPdhDs1FrmEsf;
        case T1E1_FRM_SF:
            return cAtPdhDs1FrmSf;
        case T1E1_FRM_E1:
            return cAtPdhE1Frm;
        case T1E1_FRM_E1_CRC:
        case T1E1_FRM_E1_MF:
        case T1E1_FRM_E1_CRC_MF:
             return cAtPdhE1MFCrc;
        case T1E1_FRM_J1_SF:
             return cAtPdhJ1FrmSf;
        case T1E1_FRM_J1_ESF:
             return cAtPdhJ1FrmEsf;
        default:
            return cAtPdhDs1J1UnFrm;
       }
 
}

static boolean cem_fpga_t1e1_config_framing(dev_cem_fpga_object_t *cem_fpga, 
		t1e1_t *t1e1_info)
{
    eAtRet  ret;
    eAtPdhDe1FrameType frameType;
    AtPdhChannel de1 = (AtPdhChannel)PdhDe1Get(t1e1_info->t1e1_num);
   
    INFO("%s: Entry", __FUNCTION__);
    frameType = frame_type_enum_ios_to_cem_fpga(t1e1_info->framing);
    INFO("Framing for t1e1_num:%d recv_frameType:%d fpga_frameType:%d\n",
		 t1e1_info->t1e1_num,t1e1_info->framing, frameType);
   ret = AtPdhChannelFrameTypeSet(de1, cAtPdhDs1J1UnFrm);
   if (ret != cAtOk) {
       ERR("cem_fpga_t1e1_framing_config not successful:  ret :0x%x",cAtOk );
       return (FALSE);
    }
    return (TRUE);
}


static eAtPdhDe3FrameType  frame_type_enum_ds3_ios_to_cem_fpga
(t3e3_framing_e framing)
{

    switch (framing) {
        case TDL_T3_FRAMING_M23:
            return (cAtPdhDs3FrmM23);
        case TDL_T3_FRAMING_CBIT:
            return (cAtPdhDs3FrmCbitUnChn); /* TDB chnl cbit framing */
        case TDL_E3_FRAMING_G751:
            return (cAtPdhE3FrmG751);
        case TDL_E3_FRAMING_G832:
            return (cAtPdhE3Frmg832);
        case TDL_T3_UNFRAMED:
            return (cAtPdhDs3Unfrm);
        case TDL_E3_UNFRAMED:
            return(cAtPdhE3Unfrm);
        case TDL_T3_FRAMING_AUTO_DETECT:
        default:
            ERR("\n Invalid framing type = %d", framing);
            return (cAtPdhDs3Unfrm);
    }
}

static boolean cem_fpga_t3e3_config_framing(dev_cem_fpga_object_t *cem_fpga,
        pdh_t3e3_t *t3e3_info)
{
   
    eAtRet  ret;
    eAtPdhDe3FrameType frameType;

    AtPdhDe3 de3 = (AtPdhDe3)PdhDe3Get(t3e3_info->t3e3_num);

    frameType = frame_type_enum_ds3_ios_to_cem_fpga(t3e3_info->framing);
    INFO("T3E3_framing: t3e3_num:%d recv_frameType:%d fpga_frameType:%d\n",
            t3e3_info->t3e3_num,t3e3_info->framing, frameType);
    ret = AtPdhChannelFrameTypeSet((AtPdhChannel)de3, frameType);
    if (ret != cAtOk) {

        ERR("cem_fpga_t3e3_framing_config not successful:  ret :0x%x",cAtOk );
        return (FALSE);
    }
    ERR("\n calling alarm register for t3e3 num %d", t3e3_info->t3e3_num);
    cem_fpga_alarm_change_state_register((AtChannel)de3, T3E3_ALARM_TYPE,
                                         TRUE);
    return (TRUE);

}

static eAtTimingMode clock_enum_ios_to_cem_fpga(path_clock_e clock_type)
{
     switch(clock_type) {
        case PATH_CLOCK_LINE:
             return cAtTimingModeSys;
         case PATH_CLOCK_INTERNAL:
             return cAtTimingModeSys;
         case PATH_CLOCK_RECOVERED:
             return cAtTimingModeSys;
             //return cAtTimingModeAcr;
         default:
              return cAtTimingModeSys;
       }
}
static boolean cem_fpga_t1e1_config_clock(dev_cem_fpga_object_t *cem_fpga, 
		t1e1_t *t1e1_info)
{

    eAtRet  ret;
    eAtTimingMode clock_type = cAtTimingModeSys;

    INFO("%s: Entry", __FUNCTION__);
    //Channel number needs to pass from caller.. 
    AtPdhChannel de1 = (AtPdhChannel)PdhDe1Get(t1e1_info->t1e1_num);
    INFO("Clock  for t1e1_num:%d received_clock_Type:%d fpga_clockType:%d\n",
		 t1e1_info->t1e1_num,t1e1_info->framing, clock_type);
  
    clock_type = clock_enum_ios_to_cem_fpga(t1e1_info->clock);
   
    /* Set default system type for DE1 */
    ret = AtChannelTimingSet((AtChannel)de1, clock_type, NULL);
    if (ret != cAtOk) {
       ERR("cem_fpga_t1e1_config_clock not successful");
    }
    return (TRUE);
}

static boolean cem_fpga_t3e3_config_clock(dev_cem_fpga_object_t *cem_fpga,
        pdh_t3e3_t *t3e3_info)

{

    return (TRUE);
}

static void cem_fpga_device_isr (dev_cem_fpga_object_t *cem_fpga)
{
    AtDevice device = NULL;
    device = AtDriverDeviceGet(cem_fpga->cem_fpga_driver,
                               CEM_FPGA_CORE_ID);
    AtDeviceInterruptProcess(device);
}

static dev_cem_fpga_callin_fvt_t cem_fpga_callin_fvt =
{
    cem_fpga_sts1_path_provision,
    cem_fpga_sts3c_path_provision,
    cem_fpga_t3e3_intf_provision,
    cem_fpga_vt_intf_provision,
    cem_fpga_t1e1_intf_provision,
    cem_fpga_t1e1_config,
    cem_fpga_t3e3_config,
    cem_fpga_cem_intf_config,
    cem_fpga_cem_connect_config,
    cem_fpga_cem_channel_enable,
    cem_fpga_line_config,
    cem_fpga_cem_connect_unconfig,
    cem_fpga_cem_intf_unconfig,
    cem_fpga_path_unprovision,
    cem_fpga_path_loopback_config,
    cem_fpga_poh_config,
    cem_fpga_bert_config,
    cem_fpga_t1e1_config_framing,
    cem_fpga_t1e1_enable_if_state,
    cem_fpga_t1e1_config_mode,
    cem_fpga_t1e1_config_loopback,
    cem_fpga_t1e1_config_clock,
    cem_fpga_t3e3_config_loopback,
    cem_fpga_t3e3_config_mode,
    cem_fpga_t3e3_config_framing,
    cem_fpga_t3e3_config_clock,
    cem_fpga_stats,
    cem_fpga_vt_oh_config,
    cem_fpga_device_isr
};

static dev_cem_fpga_callout_fvt_t cem_fpga_callout_fvt = 
{
};

static dev_cem_fpga_callin_fvt_t cem_fpga_do_nothing_callin_fvt =
{
    (cem_fpga_sts1_path_provision_fn_t)    dev_do_nothing,
    (cem_fpga_sts3c_path_provision_fn_t)   dev_do_nothing, 
    (cem_fpga_t3e3_intf_provision_fn_t)    dev_do_nothing, 
    (cem_fpga_vt_intf_provision_fn_t)      dev_do_nothing, 
    (cem_fpga_t1e1_intf_provision_fn_t)    dev_do_nothing, 
    (cem_fpga_t1e1_config_fn_t)            dev_do_nothing,
    (cem_fpga_t3e3_config_fn_t)            dev_do_nothing,
    (cem_fpga_cem_intf_config_fn_t)        dev_do_nothing,
    (cem_fpga_cem_connect_config_fn_t)     dev_do_nothing,
    (cem_fpga_cem_channel_enable_fn_t)     dev_do_nothing,
    (cem_fpga_line_config_fn_t)            dev_do_nothing,
    (cem_fpga_cem_connect_unconfig_fn_t)   dev_do_nothing,
    (cem_fpga_cem_intf_unconfig_fn_t)      dev_do_nothing,
    (cem_fpga_path_unprovision_fn_t)       dev_do_nothing,
    (cem_fpga_path_loopback_conf_fn_t)     dev_do_nothing,
    (cem_fpga_poh_conf_fn_t)               dev_do_nothing,
    (cem_fpga_bert_conf_fn_t)              dev_do_nothing,
    (cem_fpga_t1e1_config_framing_fn_t)    dev_do_nothing,
    (cem_fpga_t1e1_enable_if_state_fn_t)   dev_do_nothing,
    (cem_fpga_t1e1_config_mode_fn_t)       dev_do_nothing,
    (cem_fpga_t1e1_config_loopback_fn_t)   dev_do_nothing,
    (cem_fpga_t1e1_config_clock_fn_t)      dev_do_nothing,
    (cem_fpga_t3e3_config_framing_fn_t)    dev_do_nothing,
    (cem_fpga_t3e3_config_mode_fn_t)       dev_do_nothing,
    (cem_fpga_t3e3_config_loopback_fn_t)   dev_do_nothing,
    (cem_fpga_t3e3_config_clock_fn_t)      dev_do_nothing,
    (cem_fpga_stats_t)                     dev_do_nothing, 
    (cem_fpga_vt_oh_fn_t)                  dev_do_nothing,
    (cem_fpga_device_isr_fn_t)             dev_do_nothing
};

static char dev_cem_fpga_name[] = "CEM FPGA";

static dev_status
dev_cem_fpga_restart (dev_object_t * dev)
{
    return (DEV_STATUS_SUCCESS);
}

/*
 * Enable the pluggable optics device common callin vector
 */
static dev_status
dev_cem_fpga_attach (dev_object_t * dev)
{
    dev_cem_fpga_object_t *cem_fpga = 
                         (dev_cem_fpga_object_t *)dev;

    INFO("dev_cem_fpga_attach()");
    if (!dev) {
        return(DEV_CEM_FPGA_FAILURE);
    }    
    dev_base_attach(dev); /* Also sets device state */

    cem_fpga->callin = &cem_fpga_callin_fvt;
    cem_fpga->callout = &cem_fpga_callout_fvt;

    return (DEV_CEM_FPGA_SUCCESS);
}

static dev_status
dev_cem_fpga_detach (dev_object_t * dev)
{
    dev_cem_fpga_object_t *cem_fpga = 
                         (dev_cem_fpga_object_t *)dev;

    INFO("in dev_cem_fpga_detach()");
    if (!dev) {
        return(DEV_CEM_FPGA_FAILURE);
    }    
    dev_base_detach(dev); /* Also sets device state */

    cem_fpga->callin = &cem_fpga_do_nothing_callin_fvt;

    return (DEV_CEM_FPGA_SUCCESS);
}

dev_object_t *
dev_cem_fpga_create (dev_cem_fpga_create_info_t *args)
{
    dev_object_t *dev;
    dev_object_fvt_t *dev_fvt_attached;
    dev_cem_fpga_object_t *cem_fpga_dev;
#ifdef CEM_FPGA_SIMULATION
    char string[6];
#endif

    INFO("dev_cem_fpga_create: creating cem_fpga object");

    cem_fpga_dev = (dev_cem_fpga_object_t *)
                dev_malloc(sizeof(dev_cem_fpga_object_t));
    if (!cem_fpga_dev) {
        ERR("dev_cem_fpga_create: Malloc failed");
        return (NULL);
    }

    dev_memset(cem_fpga_dev, 0, sizeof(dev_cem_fpga_object_t));
    dev = (dev_object_t *)&cem_fpga_dev->base;
    if (!dev) {
        ERR("dev is NULL in dev_cem_fpga_create");
        return (NULL);
    }

    dev->dev_addr = args->dev_addr;
    dev->client_context = args->context;
    dev_fvt_attached = &dev->dev_fvt_attached;

    /* Perform standard initialization of base object */
    dev_base_init(dev); /* Also sets state */

    /* Set function vectors that have been implemented for this device.*/
    dev_fvt_attached->dev_attach = dev_cem_fpga_attach;
    dev_fvt_attached->dev_detach = dev_cem_fpga_detach;
    dev_fvt_attached->dev_restart = dev_cem_fpga_restart;
    dev_fvt_attached->dev_init = dev_cem_fpga_init;
    dev_fvt_attached->dev_oper_enable = 
                                dev_cem_fpga_oper_enable;
    dev_fvt_attached->dev_oper_disable =
                                dev_cem_fpga_oper_disable;
    dev_fvt_attached->dev_intr_enable = 
                                dev_cem_fpga_intr_enable;
    dev_fvt_attached->dev_intr_disable =
                                dev_cem_fpga_intr_disable;
    dev_fvt_attached->dev_isr = dev_cem_fpga_isr;
    dev_fvt_attached->dev_show = dev_cem_fpga_show;
    dev_fvt_attached->dev_destroy = dev_cem_fpga_destroy;
    dev_fvt_attached->dev_name = dev_cem_fpga_name;

    /* Make sure attach is valid in base object detached FVT */
    dev->dev_fvt_detached.dev_attach = dev_cem_fpga_attach;
    dev->dev_fvt_detached.dev_destroy = dev_cem_fpga_destroy;
    cem_fpga_dev->base.dev_flag = args->dev_flag;
    cem_fpga_dev->product_code  = args->product_code;
#ifdef CEM_FPGA_SIMULATION
    if (mcp_getmonvar("LOTR_IM_SIM_TYPE", string, sizeof(string)) > 0) {
        cem_fpga_simulation = (strtol(string, NULL, 0)) & 0x1;
        ERR(" cem_fpga_simulation %x ", cem_fpga_simulation);
    }
#endif
    return (dev);
}


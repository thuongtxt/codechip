//

/*--------------------------- Include files ----------------------------------*/
#define BTRACE_APP_MACROS
#define LOCAL_TRACE_MODULE INFRA
#include "AtCiscoUpsr.h"
#include "protection_ds.h"
#include "upsr_hal.h"
#include <btrace/btrace.h>
#include <binos/berror.h>



/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cAtCiscoUpsrCellBitMask(cell)   (1U << (cell))

/*--------------------------- Local Typedefs ---------------------------------*/

extern void upsr_alarm_interrupt_processing(uint8_t bay, uint32_t base_address);

protection_callout_fvt upsr_alarm_callout_fvt;

static uint32_t StsStatusChange (uint8_t bay,
        uint32_t current, uint32_t rowIndex,
        eAtCiscoUpsrAlarm table)
{
    uint32_t change;
    if (table == cAtCiscoUpsrAlarmSf)
    {
        change = upsr_alarm_callout_fvt.get_sts_sf_history_table(
                bay, rowIndex) ^ current;
        upsr_alarm_callout_fvt.set_sts_sf_history_table(bay,
                rowIndex, current);
        return (change);
    }

    if (table == cAtCiscoUpsrAlarmSd)
    {
        change = upsr_alarm_callout_fvt.get_sts_sd_history_table(
               bay, rowIndex) ^ current;
        upsr_alarm_callout_fvt.set_sts_sd_history_table(bay,
                rowIndex, current);
        return (change);
    }
    printf("PRAFULL %s\n", __FUNCTION__);
    ERR("PRAFULL %s\n", __FUNCTION__);

    return (0);
}

static uint32_t VtStatusChange (uint8_t bay, uint32_t current, uint32_t rowIndex,
        eAtCiscoUpsrAlarm table)
{
    uint32_t change;
    if (table == cAtCiscoUpsrAlarmSf)
    {
        change = upsr_alarm_callout_fvt.get_vt_sf_history_table(
                bay, rowIndex) ^ current;
        upsr_alarm_callout_fvt.set_vt_sf_history_table(bay,
               rowIndex, current);

        return (change);
    }

    if (table == cAtCiscoUpsrAlarmSd)
    {
        change = upsr_alarm_callout_fvt.get_vt_sd_history_table(bay,
               rowIndex) ^ current;
        upsr_alarm_callout_fvt.set_vt_sd_history_table(bay,
               rowIndex, current);
        return (change);
    }
    printf("PRAFULL %s\n", __FUNCTION__);
    ERR("PRAFULL %s\n", __FUNCTION__);
    return (0);
}


static void InterruptProcess (uint8_t bay, uint32_t baseAddress,
        uint32_t MASRStatus, eAtCiscoUpsrAlarm table)
{
    uint32_t stsRow;

    for (stsRow = 0; stsRow < 12; stsRow++)
    {

        if (cAtCiscoUpsrCellBitMask(stsRow) & MASRStatus)
        {
            uint32_t regVal = AtCiscoUpsrStsStatusRead(baseAddress, table,
                    stsRow);
            uint32_t intrVal = StsStatusChange(bay, regVal, stsRow, table);
            uint32_t column;

            for (column = 0; column < 32; column++)
            {
                if (cAtCiscoUpsrCellBitMask(column) & intrVal)
                {
                    uint32_t stsIndex = stsRow * 32 + column;
                    uint32_t defect = 0;
                    uint32_t event = table;
                    if (cAtCiscoUpsrCellBitMask(column) & regVal)
                        defect = table;

                    uea_upsr_processStsPathUpdate(bay, stsIndex, event, defect);

                }
            }
        }
    }
    for (stsRow = 0; stsRow < 12; stsRow++)
    {
        if (cAtCiscoUpsrCellBitMask(stsRow + 12) & MASRStatus)
        {
            uint32_t regVal = AtCiscoUpsrSASRVtRead(baseAddress, table, stsRow);
            uint32_t sasr;

            for (sasr = 0; sasr < 32; sasr++)
            {
                if (cAtCiscoUpsrCellBitMask(sasr) & regVal)
                {
                    uint32_t vtRow = stsRow * 32 + sasr;
                    uint32_t rowVal = AtCiscoUpsrVtStatusRead(
                            baseAddress, table, vtRow);
                    uint32_t intrVal = VtStatusChange(bay, rowVal, vtRow, table);
                    uint32_t column;

                    for (column = 0; column < 28; column++)
                    {
                        if (cAtCiscoUpsrCellBitMask(column) & intrVal)
                        {
                            uint32_t defect = 0;
                            uint32_t event = table;
                            if (cAtCiscoUpsrCellBitMask(column) & rowVal)
                                defect = table;

                            uea_upsr_processVtPathUpdateBulk(bay, vtRow,
                                    column, event, defect);
                        }
                    }
                }
            }
        }
    }
}

void upsr_alarm_interrupt_processing (uint8_t bay, uint32_t base_address)
{
    eAtCiscoUpsrAlarm table = cAtCiscoUpsrAlarmSf;

    /* Loop through SF and SD tables */
    for (; table <= cAtCiscoUpsrAlarmSd; table <<= 1) {
        uint32_t MASRVal = AtCiscoUpsrMASRStatusRead(base_address, table);
        if (MASRVal) {
            printf("%s %d %x\n",__FUNCTION__,bay,MASRVal);
            InterruptProcess(bay, base_address, MASRVal, table);
        }
        //printf("%s %d %x\n",__FUNCTION__,bay,base_address);
    }
}

void upsr_alarm_fvt_register (protection_callout_fvt fvt)
{
    upsr_alarm_callout_fvt = fvt;
}

void uea_alarm_interrupt_enable (uint8_t bay, uint32_t base_address) {
    uint32_t row;
    printf("\n PRE1 PRAFULL %s %d \n", __FUNCTION__, bay);
    ERR("\n PRE1 PRAFULL %s %d \n", __FUNCTION__, bay);

    AtCiscoUpsrMASRMaskWrite(base_address,0);
    printf("\n PRE2 PRAFULL %s %d \n", __FUNCTION__, bay);
    ERR("\n PRE2 PRAFULL %s %d \n", __FUNCTION__, bay);
    for (row=0; row < 12;row++) {
        AtCiscoUpsrStsMaskWrite(base_address,cAtCiscoUpsrAlarmSf,row,0xFFFFFFFF);
        AtCiscoUpsrStsMaskWrite(base_address,cAtCiscoUpsrAlarmSd,row,0xFFFFFFFF);
    }
    printf("\n POST PRAFULL %s %d \n", __FUNCTION__, bay);
    ERR("\n POST PRAFULL %s %d \n", __FUNCTION__, bay);
}

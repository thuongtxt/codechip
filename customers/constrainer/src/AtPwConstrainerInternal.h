/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Application
 * 
 * File        : AtPwConstrainerInternal.h
 * 
 * Created Date: Feb 15, 2016
 *
 * Description : Pw constrainer internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPWCONSTRAINERINTERNAL_H_
#define _ATPWCONSTRAINERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPwConstrainer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mMax(a,b) (a > b ? a : b)
#define mMin(a,b) (a > b ? b : a)
#define AtUnused(x) (void)(x)

#define mMethodsGet(object) (object)->methods
#define mMethodsSet(object, methods) mMethodsGet(object) = methods
#define mMethodOverride(methods, methodName) (methods).methodName = methodName

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPwCache
    {
    uint8_t  pwType;
    uint8_t  rtpIsEnabled;
    uint8_t  numTimeslot;
    uint32_t rateInBytesPer125Us;
    uint32_t payloadSize;
    uint32_t bufferSizeInUs;
    uint32_t ethHeaderLengthInByte;
    uint32_t psnLengthInByte;
    uint32_t ethPortId;
    uint32_t queueId;
    uint32_t lopsThreshold;
    uint32_t numBufferBlock;
    uint32_t bandwidth;
    }tAtPwCache;

typedef struct tEthPortBandwidth
    {
    uint32_t bandWidthInKbps;
    uint32_t bandWidthInBps;
    }tEthPortBandwidth;

typedef struct tAtEthPortCache
    {
    uint8_t portType;
    uint8_t portTxGap;
    tEthPortBandwidth provisionedBandWidth;
    }tAtEthPortCache;

typedef struct tAtPwConstrainerMethods
    {
    uint32_t (*MaxNumPw)(AtPwConstrainer self);
    uint32_t (*MaxNumEthPort)(AtPwConstrainer self);
    uint32_t (*UpperBoundOfPacketsForJitterBufferSize)(AtPwConstrainer self, uint32_t pwId);
    uint32_t (*LowerBoundOfPacketsForJitterBufferSize)(AtPwConstrainer self);
    uint32_t (*MaxJitterBufferSizeInMicroSecond)(AtPwConstrainer self, uint32_t pwId);
    uint32_t (*MinJitterBufferSizeInMicroSecond)(AtPwConstrainer self, uint32_t pwId);
    uint16_t (*MaxPayloadSizeByPwType)(AtPwConstrainer self, uint32_t pwId);
    uint16_t (*MinPayloadSizeByPwType)(AtPwConstrainer self, uint32_t pwId);
    uint8_t  (*LopsThresholdHasLimitOneSecond)(AtPwConstrainer self);
    uint32_t (*MaxNumJitterBufferBlocks)(AtPwConstrainer self);
    uint32_t (*JitterBufferBlockSizeInByte)(AtPwConstrainer self);
    void     (*PwBandwidthUpdate)(AtPwConstrainer self, uint32_t pwId, uint32_t newPwBandwidth);
    uint32_t (*RemainingBwForPw)(AtPwConstrainer self, uint32_t pwId);
    uint32_t (*MaxBandwidthInKbps)(AtPwConstrainer self, uint32_t portId);
    uint32_t (*RemainingBpsOfMaxBandwidth)(AtPwConstrainer self, uint32_t portId);
    uint8_t  (*PwWasSetDefault)(AtPwConstrainer self, uint32_t pwId);
    uint8_t  (*DefaultEthPortTxIpg)(AtPwConstrainer self, uint32_t portId);
    uint8_t  (*DefaultEthPortType)(AtPwConstrainer self, uint32_t portId);
    uint32_t (*EthPortQueueBandwidthGet)(AtPwConstrainer self, uint32_t portId, uint32_t queueId);
    void     (*PwQueueSet)(AtPwConstrainer self, uint32_t pwId, uint32_t queueId);
    uint32_t (*PwQueueGet)(AtPwConstrainer self, uint32_t pwId);
    uint32_t (*LocalIdFromUserId)(AtPwConstrainer self, uint32_t pwId);
    void (*PwReset)(AtPwConstrainer self, uint32_t pwId);
    void (*Delete)(AtPwConstrainer self);
    uint32_t (*MinNumPacketsForJitterDelay)(AtPwConstrainer self);
    }tAtPwConstrainerMethods;

typedef struct tAtPwConstrainer
    {
    const tAtPwConstrainerMethods *methods;

    tAtPwCache *allPwsCache;
    tAtEthPortCache *allEthPortsCache;
    uint32_t numOccupiedBufferBlocks;
    }tAtPwConstrainer;

typedef struct tAtPwConstrainerOcnCard
    {
    tAtPwConstrainer super;
    }tAtPwConstrainerOcnCard;

typedef struct tAtPwConstrainer5GMsCard
    {
    tAtPwConstrainerOcnCard super;
    }tAtPwConstrainer5GMsCard;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPwConstrainer AtPwConstrainerObjectInit(AtPwConstrainer self);
AtPwConstrainer AtPwConstrainerOcnCardObjectInit(AtPwConstrainer self);
AtPwConstrainer AtPwConstrainer5GMsCardObjectInit(AtPwConstrainer self);

uint32_t AtPwConstrainerPwCurrentBandwidthGet(AtPwConstrainer self, uint32_t pwId);

#ifdef __cplusplus
}
#endif
#endif /* _ATPWCONSTRAINERINTERNAL_H_ */


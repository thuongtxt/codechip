/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : AtPwConstrainerOcnCard.c
 *
 * Created Date: Feb 17, 2016
 *
 * Description : PW constrains for OCN card
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwConstrainerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8_t m_methodsInit = 0;

/* Override */
static tAtPwConstrainerMethods m_AtPwConstrainerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32_t MaxNumPw(AtPwConstrainer self)
    {
    AtUnused(self);
    return 5376;
    }

static uint32_t MaxNumEthPort(AtPwConstrainer self)
    {
    AtUnused(self);
    return 2;
    }

static uint32_t UpperBoundOfPacketsForJitterBufferSize(AtPwConstrainer self, uint32_t pwId)
    {
    eAtPwConstrainerPwType pwType = AtPwConstrainerPwTypeGet(self, pwId);
    uint32_t circuitRate;

    if (pwType != cAtPwConstrainerPwTypeCEP)
        return 2048;

    circuitRate = AtPwConstrainerCircuitRateGet(self, pwId);
    if ((circuitRate == cAtCircuitRateVc12) || (circuitRate == cAtCircuitRateVc11))
        return 2048;

    return 8191;
    }

static uint32_t LowerBoundOfPacketsForJitterBufferSize(AtPwConstrainer self)
    {
    AtUnused(self);
    return 6;
    }

static uint32_t MaxNumJitterBufferBlocks(AtPwConstrainer self)
    {
    AtUnused(self);
    return 524288;
    }

static uint32_t JitterBufferBlockSizeInByte(AtPwConstrainer self)
    {
    AtUnused(self);
    return 1024;
    }

static uint32_t MaxBandwidthInKbps(AtPwConstrainer self, uint32_t portId)
    {
    AtUnused(self);
    AtUnused(portId);
    return 10000000; /* 10G */
    }

static uint8_t LopsThresholdHasLimitOneSecond(AtPwConstrainer self)
    {
    AtUnused(self);
    return 1;
    }

static uint8_t DefaultEthPortTxIpg(AtPwConstrainer self, uint32_t portId)
    {
    AtUnused(self);
    AtUnused(portId);
    return 8;
    }

static uint32_t MinNumPacketsForJitterDelay(AtPwConstrainer self)
    {
    AtUnused(self);
    return 3;
    }

static void OverrideAtPwConstrainer(AtPwConstrainer self)
    {
    if (!m_methodsInit)
        {
        memcpy(&m_AtPwConstrainerOverride, mMethodsGet(self), sizeof(tAtPwConstrainerMethods));
        mMethodOverride(m_AtPwConstrainerOverride, MaxNumPw);
        mMethodOverride(m_AtPwConstrainerOverride, MaxNumEthPort);
        mMethodOverride(m_AtPwConstrainerOverride, UpperBoundOfPacketsForJitterBufferSize);
        mMethodOverride(m_AtPwConstrainerOverride, LowerBoundOfPacketsForJitterBufferSize);
        mMethodOverride(m_AtPwConstrainerOverride, MaxNumJitterBufferBlocks);
        mMethodOverride(m_AtPwConstrainerOverride, JitterBufferBlockSizeInByte);
        mMethodOverride(m_AtPwConstrainerOverride, MaxBandwidthInKbps);
        mMethodOverride(m_AtPwConstrainerOverride, LopsThresholdHasLimitOneSecond);
        mMethodOverride(m_AtPwConstrainerOverride, DefaultEthPortTxIpg);
        mMethodOverride(m_AtPwConstrainerOverride, MinNumPacketsForJitterDelay);
        }

    mMethodsSet(self, &m_AtPwConstrainerOverride);
    }

static uint32_t ObjectSize(void)
    {
    return sizeof(tAtPwConstrainerOcnCard);
    }

AtPwConstrainer AtPwConstrainerOcnCardObjectInit(AtPwConstrainer self)
    {
    /* Clear memory */
    memset(self, 0, ObjectSize());

    /* Super constructor */
    if (AtPwConstrainerObjectInit(self) == NULL)
        return NULL;

    /* Override */
    OverrideAtPwConstrainer(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtPwConstrainer
 * @{
 */

/**
 * Create PW constrainer for OCN card
 *
 * @return PW constrainer
 */
AtPwConstrainer AtPwConstrainerOcnCardNew(void)
    {
    /* Allocate memory */
    AtPwConstrainer newConstrainer = malloc(ObjectSize());
    if (newConstrainer == NULL)
        return NULL;

    /* Construct it */
    return AtPwConstrainerOcnCardObjectInit(newConstrainer);
    }
/**
 * @}
 */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : AtPwConstrainer.c
 *
 * Created Date: Feb 1, 2016
 *
 * Description : PW constrains
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwConstrainerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8_t m_methodsInit = 0;
static tAtPwConstrainerMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32_t MaxNumPw(AtPwConstrainer self)
    {
    AtUnused(self);
    /* Let concrete class decide */
    return 0;
    }

static uint32_t MaxNumEthPort(AtPwConstrainer self)
    {
    AtUnused(self);
    /* Let concrete class decide */
    return 0;
    }

static uint32_t UpperBoundOfPacketsForJitterBufferSize(AtPwConstrainer self, uint32_t pwId)
    {
    AtUnused(self);
    AtUnused(pwId);
    /* Let concrete class decide */
    return 0;
    }

static uint32_t LowerBoundOfPacketsForJitterBufferSize(AtPwConstrainer self)
    {
    AtUnused(self);
    return 4;
    }

static uint32_t PwCwLengthGet(AtPwConstrainer self, uint32_t pwId)
    {
    const uint8_t cCwLength = 4;
    uint32_t cwLength = 0;

    if (AtPwConstrainerPwRtpIsEnabled(self, pwId))
        cwLength += 12;

    if (AtPwConstrainerPwTypeGet(self, pwId) == cAtPwConstrainerPwTypeCEP)
        cwLength += 4;

    return cwLength + cCwLength;
    }

static uint32_t PwTotalHeaderLengthInByte(AtPwConstrainer self, uint32_t pwId)
    {
    uint32_t ethHeaderLength = AtPwConstrainerPwEthHeaderLengthGet(self, pwId);
    uint32_t psnLength       = AtPwConstrainerPwPsnLengthGet(self, pwId);
    uint32_t cwLength        = PwCwLengthGet(self, pwId);

    return (uint32_t)(ethHeaderLength + psnLength + cwLength);
    }

static uint16_t MtuByPwType(AtPwConstrainer self, uint32_t pwId)
    {
    eAtPwConstrainerPwType pwType = AtPwConstrainerPwTypeGet(self, pwId);
    if ((pwType == cAtPwConstrainerPwTypeSAToP) || (pwType == cAtPwConstrainerPwTypeCESoP))
        return 2048;

    if (pwType == cAtPwConstrainerPwTypeCEP)
        {
        if (AtPwConstrainerCircuitRateGet(self, pwId) > cAtCircuitRateSts1)
            return 4092;

        return 2044;
        }

    return 0;
    }

static uint16_t MaxPayloadSizeByPwType(AtPwConstrainer self, uint32_t pwId)
    {
    uint16_t mtu = MtuByPwType(self, pwId);
    uint32_t totalLengthInByte = PwTotalHeaderLengthInByte(self, pwId);

    if (mtu < totalLengthInByte)
        return 0;

    return (uint16_t)(mtu - totalLengthInByte);
    }

static uint32_t PwCESoPNumTimeslotGet(AtPwConstrainer self, uint32_t pwId)
    {
    /* Rate in byte/125us equal to number of slot */
    return AtPwConstrainerCircuitRateGet(self, pwId);
    }

static uint16_t CESoPMinPayloadSizeInByte(AtPwConstrainer self, uint32_t pwId)
    {
    static const uint8_t minNumFrames = 4;
    uint32_t payloadWithMinFrames = (uint32_t)(minNumFrames * PwCESoPNumTimeslotGet(self, pwId));

    /* Payload size of CESoP must >= 8 bytes and >= 4 frames */
    return (uint16_t)mMax(8UL, payloadWithMinFrames);
    }

static uint16_t CEPMinPayloadSize(AtPwConstrainer self, uint32_t pwId)
    {
    uint32_t circuitRate = AtPwConstrainerCircuitRateGet(self, pwId);

    if (circuitRate <= cAtCircuitRateVc11)
        return 104;

    if (circuitRate == cAtCircuitRateVc12)
        return 140;

    if (circuitRate == cAtCircuitRateSts1)
        return 256;

    return 783;
    }

static uint16_t MinPayloadSizeByPwType(AtPwConstrainer self, uint32_t pwId)
    {
    eAtPwConstrainerPwType pwType = AtPwConstrainerPwTypeGet(self, pwId);

    if (pwType == cAtPwConstrainerPwTypeSAToP)
        {
        uint32_t rateByCircuit = AtPwConstrainerCircuitRateGet(self, pwId);

        if (rateByCircuit == cAtCircuitRateE1)
            return 128;

        if ((rateByCircuit == cAtCircuitRateE3) ||
            (rateByCircuit == cAtCircuitRateDs3))
            return 256;

        return 96;
        }

    if (pwType == cAtPwConstrainerPwTypeCESoP)
        return CESoPMinPayloadSizeInByte(self, pwId);

    if (pwType == cAtPwConstrainerPwTypeCEP)
        return CEPMinPayloadSize(self, pwId);

    return 0;
    }

static uint8_t LopsThresholdHasLimitOneSecond(AtPwConstrainer self)
    {
    AtUnused(self);

    /* Let concrete class decide */
    return 0;
    }

static uint32_t MaxJitterBufferSizeInMicroSecond(AtPwConstrainer self, uint32_t pwId)
    {
    uint32_t circuitRate = AtPwConstrainerCircuitRateGet(self, pwId);
    if (circuitRate == cAtCircuitRateStsNc(12))
        return 128000;

    if (circuitRate == cAtCircuitRateStsNc(48))
        return 64000;

    return 256000;
    }

static uint32_t MinJitterBufferSizeInMicroSecond(AtPwConstrainer self, uint32_t pwId)
    {
    AtUnused(pwId);
    AtUnused(self);
    return 250;
    }

static uint32_t MaxNumJitterBufferBlocks(AtPwConstrainer self)
    {
    AtUnused(self);

    /* Let concrete class decide */
    return 0;
    }

static uint32_t JitterBufferBlockSizeInByte(AtPwConstrainer self)
    {
    AtUnused(self);

    /* Let concrete class decide */
    return 0;
    }

static uint32_t MaxBandwidthInKbps(AtPwConstrainer self, uint32_t portId)
    {
    AtUnused(self);
    AtUnused(portId);
    /* Let concrete class decide */
    return 0;
    }

static uint32_t RemainingBpsOfMaxBandwidth(AtPwConstrainer self, uint32_t portId)
    {
    AtUnused(self);
    AtUnused(portId);
    /* Let concrete class decide */
    return 0;
    }

static uint8_t PwIdIsValid(AtPwConstrainer self, uint32_t pwId)
    {
    if (pwId < mMethodsGet(self)->MaxNumPw(self))
        return 1;

    return 0;
    }

static void PwCacheInit(AtPwConstrainer self, tAtPwCache* cache)
    {
    AtUnused(self);
    memset(cache, 0, sizeof(tAtPwCache));
    cache->ethPortId = cAtInvalidUint32;
    cache->queueId = cAtInvalidUint32;
    }

static tAtPwCache* AllPwsCacheAllocate(AtPwConstrainer self)
    {
    uint32_t pwId;
    uint32_t numPw = mMethodsGet(self)->MaxNumPw(self);
    uint32_t memSize = numPw * sizeof(tAtPwCache);
    tAtPwCache* allPwsCache = malloc(memSize);
    if (allPwsCache == NULL)
        return NULL;

    /* Init pw eth port and queue ID to invalid value */
    for (pwId = 0; pwId < numPw; pwId++)
        PwCacheInit(self, &allPwsCache[pwId]);

    return allPwsCache;
    }

static tAtPwCache* PwCacheByIdGet(AtPwConstrainer self, uint32_t pwId)
    {
    uint32_t localPwId = mMethodsGet(self)->LocalIdFromUserId(self, pwId);
    if ((self == NULL) || !PwIdIsValid(self, localPwId))
        return NULL;

    if (self->allPwsCache == NULL)
        self->allPwsCache = AllPwsCacheAllocate(self);

    if (self->allPwsCache)
        return self->allPwsCache + localPwId;

    return NULL;
    }

static void PwCurrentBandwidthSet(AtPwConstrainer self, uint32_t pwId, uint32_t bandwidth)
    {
    tAtPwCache* pwCache = PwCacheByIdGet(self, pwId);
    if (pwCache)
        pwCache->bandwidth = bandwidth;
    }

/* Return true if increase */
static uint8_t AdjustmentCalculate(uint32_t oldBwInBps, uint32_t newBwInBps, tEthPortBandwidth* offset)
    {
    uint32_t offsetInBps;
    uint8_t isIncrease;

    if (newBwInBps > oldBwInBps)
        {
        isIncrease = 1;
        offsetInBps = newBwInBps - oldBwInBps;
        }
    else
        {
        isIncrease = 0;
        offsetInBps = oldBwInBps - newBwInBps;
        }

    offset->bandWidthInKbps = offsetInBps / 1000;
    offset->bandWidthInBps  = offsetInBps % 1000;
    return isIncrease;
    }

static tEthPortBandwidth* BandwidthPlus(tEthPortBandwidth bw1, const tEthPortBandwidth* bw2, tEthPortBandwidth* sum)
    {
    sum->bandWidthInKbps = bw1.bandWidthInKbps + bw2->bandWidthInKbps;
    sum->bandWidthInBps  = bw1.bandWidthInBps  + bw2->bandWidthInBps;

    if (sum->bandWidthInBps >= 1000)
        {
        sum->bandWidthInBps = sum->bandWidthInBps % 1000;
        sum->bandWidthInKbps += 1;
        }

    return sum;
    }

static tEthPortBandwidth* BandwidthMinus(tEthPortBandwidth minuend, const tEthPortBandwidth* subtrahend, tEthPortBandwidth* difference)
    {
    if (minuend.bandWidthInKbps < subtrahend->bandWidthInKbps)
        {
        difference->bandWidthInKbps = 0;
        difference->bandWidthInBps = 0;
        return difference;
        }

    difference->bandWidthInKbps = minuend.bandWidthInKbps - subtrahend->bandWidthInKbps;
    if (minuend.bandWidthInBps < subtrahend->bandWidthInBps)
        {
        if (difference->bandWidthInKbps == 0)
            difference->bandWidthInBps = 0;
        else
            {
            difference->bandWidthInBps = minuend.bandWidthInBps + 1000 - subtrahend->bandWidthInBps;
            difference->bandWidthInKbps -= 1;
            }
        }
    else
        difference->bandWidthInBps = minuend.bandWidthInBps - subtrahend->bandWidthInBps;

    return difference;
    }

static tAtEthPortCache* AllEthPortsCacheAllocate(AtPwConstrainer self)
    {
    uint32_t port_i;
    uint32_t memSize = mMethodsGet(self)->MaxNumEthPort(self) * sizeof(tAtEthPortCache);
    tAtEthPortCache* allEthPortsCache = malloc(memSize);
    if (allEthPortsCache == NULL)
        return NULL;

    memset(allEthPortsCache, 0, memSize);
    for (port_i = 0; port_i < mMethodsGet(self)->MaxNumEthPort(self); port_i++)
        {
        allEthPortsCache[port_i].portTxGap = mMethodsGet(self)->DefaultEthPortTxIpg(self, port_i);
        allEthPortsCache[port_i].portType = mMethodsGet(self)->DefaultEthPortType(self, port_i);
        }

    return allEthPortsCache;
    }

static uint8_t EthPortIdIsValid(AtPwConstrainer self, uint32_t portId)
    {
    if (portId < mMethodsGet(self)->MaxNumEthPort(self))
        return 1;

    return 0;
    }

static tAtEthPortCache* EthPortCacheByIdGet(AtPwConstrainer self, uint32_t portId)
    {
    if ((self == NULL) || !EthPortIdIsValid(self, portId))
        return NULL;

    if (self->allEthPortsCache == NULL)
        self->allEthPortsCache = AllEthPortsCacheAllocate(self);

    if (self->allEthPortsCache)
        return self->allEthPortsCache + portId;

    return NULL;
    }

static void EthPortBandwidthGet(AtPwConstrainer self, uint32_t portId, tEthPortBandwidth* portBw)
    {
    tAtEthPortCache *ethPortCache = EthPortCacheByIdGet(self, portId);
    if (ethPortCache == NULL)
        return;

    portBw->bandWidthInBps = ethPortCache->provisionedBandWidth.bandWidthInBps;
    portBw->bandWidthInKbps = ethPortCache->provisionedBandWidth.bandWidthInKbps;
    }

static void EthPortBandwidthSet(AtPwConstrainer self, uint32_t portId, tEthPortBandwidth* portBw)
    {
    tAtEthPortCache *ethPortCache = EthPortCacheByIdGet(self, portId);
    if (ethPortCache == NULL)
        return;

    ethPortCache->provisionedBandWidth.bandWidthInBps = portBw->bandWidthInBps;
    ethPortCache->provisionedBandWidth.bandWidthInKbps = portBw->bandWidthInKbps;
    }

static void EthPortInUsedBandwidthUpdate(AtPwConstrainer self, uint32_t portId, uint32_t removeBwBps, uint32_t addBwBps)
    {
    uint8_t isIncrease;
    tEthPortBandwidth offset;
    tEthPortBandwidth portBw;

    if ((portId == cAtInvalidUint32) || (removeBwBps == addBwBps))
        return;

    memset(&portBw, 0, sizeof(portBw));

    isIncrease = AdjustmentCalculate(removeBwBps, addBwBps, &offset);
    EthPortBandwidthGet(self, portId, &portBw);

    if (isIncrease)
        BandwidthPlus(portBw, &offset, &portBw);

    else
        BandwidthMinus(portBw, &offset, &portBw);

    EthPortBandwidthSet(self, portId, &portBw);
    }

static void PwBandwidthUpdate(AtPwConstrainer self, uint32_t pwId, uint32_t newPwBandwidth)
    {
    uint32_t currentPwBw;
    uint32_t pwEthPort = AtPwConstrainerPwEthPortGet(self, pwId);

    currentPwBw = AtPwConstrainerPwCurrentBandwidthGet(self, pwId);
    EthPortInUsedBandwidthUpdate(self, pwEthPort, currentPwBw, newPwBandwidth);
    PwCurrentBandwidthSet(self, pwId, newPwBandwidth);
    }

/* NOTE: If remaining BW in bit per second over 32bit number (uint32), cBit31_0 will be returned */
static uint32_t EthPortPwRemainingBandwidthInBpsGet(AtPwConstrainer self, uint32_t portId, uint32_t removeBwInBps)
    {
    const uint32_t c32BitMaxBwInKbps = 4294967;
    tEthPortBandwidth bwAfterRemove;
    tEthPortBandwidth bwRemoving;
    tEthPortBandwidth maxBandwidth;
    tEthPortBandwidth remainingBandwidth;
    tEthPortBandwidth portBandwidth;

    if (self == NULL)
        return 0;

    memset(&portBandwidth, 0, sizeof(portBandwidth));
    bwRemoving.bandWidthInKbps = removeBwInBps / 1000;
    bwRemoving.bandWidthInBps  = removeBwInBps % 1000;

    EthPortBandwidthGet(self, portId, &portBandwidth);
    /* Total provisioned bandwidth without pw bandwidth */
    BandwidthMinus(portBandwidth, &bwRemoving, &bwAfterRemove);

    maxBandwidth.bandWidthInKbps = mMethodsGet(self)->MaxBandwidthInKbps(self, portId);
    maxBandwidth.bandWidthInBps = mMethodsGet(self)->RemainingBpsOfMaxBandwidth(self, portId);

    /* Remaining bandwidth */
    BandwidthMinus(maxBandwidth, &bwAfterRemove, &remainingBandwidth);

    if (remainingBandwidth.bandWidthInKbps > c32BitMaxBwInKbps)
        return 0xFFFFFFFF;

    return remainingBandwidth.bandWidthInKbps * 1000 + remainingBandwidth.bandWidthInBps;
    }

static uint32_t RemainingBwForPw(AtPwConstrainer self, uint32_t pwId)
    {
    uint32_t ethPort = AtPwConstrainerPwEthPortGet(self, pwId);
    if (ethPort == cAtInvalidUint32)
        return 0;

    return EthPortPwRemainingBandwidthInBpsGet(self, ethPort, AtPwConstrainerPwCurrentBandwidthGet(self, pwId));
    }

static uint32_t NumBytesForTimeInUs(uint32_t microsecond, uint32_t rateInBytePer125Us)
    {
    return (rateInBytePer125Us * microsecond) / 125;
    }

static uint32_t TimeInUsForNumBytes(uint32_t numBytes, uint32_t rateInBytePer125Us)
    {
    uint32_t us;

    if (rateInBytePer125Us == 0)
        return 0;

    us = (numBytes * 125) / rateInBytePer125Us;
    if ((numBytes * 125) % rateInBytePer125Us)
        us = us + 1;

    return us;
    }

static uint32_t NumUnUsedBlock(AtPwConstrainer self)
    {
    return mMethodsGet(self)->MaxNumJitterBufferBlocks(self) - self->numOccupiedBufferBlocks;
    }

static uint32_t NumBlockPerPacket(AtPwConstrainer self, uint32_t payloadSizeInByte)
    {
    uint32_t jitterBufferBlockSizeInByte = mMethodsGet(self)->JitterBufferBlockSizeInByte(self);
    uint32_t numBlockForPayload;

    if (jitterBufferBlockSizeInByte == 0)
        return 0;

    numBlockForPayload = payloadSizeInByte / jitterBufferBlockSizeInByte;
    if ((payloadSizeInByte % jitterBufferBlockSizeInByte) != 0)
        numBlockForPayload = numBlockForPayload + 1;

    return numBlockForPayload;
    }

static uint32_t MaxNumPacketByUnUsedBlocks(AtPwConstrainer self, uint32_t payloadSizeInByte)
    {
    uint32_t numBlockPerPacket;

    if (self == NULL)
        return cAtInvalidUint32;

    numBlockPerPacket = NumBlockPerPacket(self, payloadSizeInByte);
    if (numBlockPerPacket)
        return NumUnUsedBlock(self) / numBlockPerPacket;

    return cAtInvalidUint32;
    }

static uint32_t PayloadSizeInBytes(AtPwConstrainer self, uint32_t pwId, uint32_t payloadSize)
    {
    /* In case of CESoP, payload size is number of frames. But the following formula expects payload size in bytes */
    if (AtPwConstrainerPwTypeGet(self, pwId) == cAtPwConstrainerPwTypeCESoP)
        return payloadSize * PwCESoPNumTimeslotGet(self, pwId);

    return payloadSize;
    }

static uint32_t MaxNumPacketsForJitterBufferSize(AtPwConstrainer self, uint32_t pwId)
    {
    uint32_t maxNumPacketsBufferSize = mMethodsGet(self)->UpperBoundOfPacketsForJitterBufferSize(self, pwId);
    return mMin(maxNumPacketsBufferSize, MaxNumPacketByUnUsedBlocks(self, PayloadSizeInBytes(self, pwId, AtPwConstrainerPayloadSizeGet(self, pwId))));
    }

static uint16_t PdaMinPayloadSizeByBuffer(AtPwConstrainer self, uint32_t pwId, uint32_t jitterBufferSize)
    {
    uint32_t numBytes = NumBytesForTimeInUs(jitterBufferSize, AtPwConstrainerCircuitRateGet(self, pwId));
    uint32_t maxNumPackets = MaxNumPacketsForJitterBufferSize(self, pwId);
    uint32_t payloadSize = (numBytes / maxNumPackets);

    if (numBytes % maxNumPackets)
        payloadSize = payloadSize + 1;

    return (uint16_t)((payloadSize == 0) ? 1 : payloadSize);
    }

static uint16_t MinPayloadSizeByBuffer(AtPwConstrainer self, uint32_t pwId, uint32_t jitterBufferSize)
    {
    uint16_t pdaMinPldSizeByBuffer = PdaMinPayloadSizeByBuffer(self, pwId, jitterBufferSize);
    uint16_t pdaMinAcceptedPldSize = mMethodsGet(self)->MinPayloadSizeByPwType(self, pwId);

    return mMax(pdaMinAcceptedPldSize, pdaMinPldSizeByBuffer);
    }

static uint32_t PwEthPortGapPreembleFcsLengthGet(AtPwConstrainer self, uint32_t ethPortId)
    {
    const uint8_t cEthPreembleLength = 8;
    const uint8_t cNumByteFcs        = 4;
    uint8_t portTxGap = 0;

    if (ethPortId != cAtInvalidUint32)
        portTxGap = AtPwConstrainerEthPortTxIpgGet(self, ethPortId);

    return (uint32_t)(portTxGap + cEthPreembleLength + cNumByteFcs);
    }

static uint16_t PayloadSizeInByteFromBwCalculate(AtPwConstrainer self, uint32_t pwId, uint32_t bandwidth)
    {
    uint32_t circuitDataRateInbps = AtPwConstrainerCircuitRateGet(self, pwId) * 64;
    uint32_t ethHeaderLength;
    uint32_t gapAndPreemble;
    uint32_t cwLength;
    uint32_t paddingLength;
    uint32_t ethAdditionalBytes;
    uint32_t psnLength;

    if (bandwidth <= circuitDataRateInbps)
        return 0;

    ethHeaderLength    = AtPwConstrainerPwEthHeaderLengthGet(self, pwId);
    psnLength          = AtPwConstrainerPwPsnLengthGet(self, pwId);
    gapAndPreemble     = PwEthPortGapPreembleFcsLengthGet(self, AtPwConstrainerPwEthPortGet(self, pwId));
    cwLength           = PwCwLengthGet(self, pwId);
    paddingLength      = 0;
    ethAdditionalBytes = gapAndPreemble + ethHeaderLength + psnLength + cwLength + paddingLength;

    return (uint16_t)(((ethAdditionalBytes * circuitDataRateInbps) / (bandwidth - circuitDataRateInbps)) + 1);
    }

static uint16_t MinPayloadSizeByBandwidth(AtPwConstrainer self, uint32_t pwId)
    {
    uint32_t remainingBwForPw = mMethodsGet(self)->RemainingBwForPw(self, pwId);
    return PayloadSizeInByteFromBwCalculate(self, pwId, remainingBwForPw);
    }

static uint32_t MaxPayloadSizeByBuffer(AtPwConstrainer self, uint32_t pwId, uint32_t jitterBufferSize)
    {
    const uint32_t cMaxLopsInUs = 1000000;
    uint32_t circuitRate = AtPwConstrainerCircuitRateGet(self, pwId);
    uint32_t numBytesForTimeInUs = NumBytesForTimeInUs(jitterBufferSize, circuitRate);
    uint32_t payloadSizeWithBufferLimit = numBytesForTimeInUs / mMethodsGet(self)->LowerBoundOfPacketsForJitterBufferSize(self);
    uint32_t payloadSizeWithLopsLimit;
    uint32_t lopsThreshold;
    uint32_t maxPayloadSize;

    if (mMethodsGet(self)->LopsThresholdHasLimitOneSecond(self) == 0)
        return (payloadSizeWithBufferLimit == 0) ? 1 : payloadSizeWithBufferLimit;

    lopsThreshold = AtPwConstrainerPwLopsThresholdGet(self, pwId);
    if (lopsThreshold == 0)
        return payloadSizeWithBufferLimit;

    numBytesForTimeInUs = NumBytesForTimeInUs(cMaxLopsInUs, circuitRate);
    payloadSizeWithLopsLimit = numBytesForTimeInUs / lopsThreshold;
    maxPayloadSize = mMin(payloadSizeWithLopsLimit, payloadSizeWithBufferLimit);

    return (maxPayloadSize == 0) ? 1 : maxPayloadSize;
    }

static uint32_t NumPacketsFromUs(uint32_t microsecond, uint32_t rateInBytePer125Us, uint32_t payloadSize)
    {
    uint32_t numBytes, numPackets, remainingBytes;

    if (payloadSize == 0)
        return 0;

    numBytes       = NumBytesForTimeInUs(microsecond, rateInBytePer125Us);
    numPackets     = numBytes / payloadSize;
    remainingBytes = numBytes % payloadSize;

    /* Just for safe, plus one packet if remaining bytes are greater
     * than 30% of payload size. */
    if ((remainingBytes != 0) && (remainingBytes >= (payloadSize / 3)))
        numPackets = numPackets + 1;

    return numPackets;
    }

static uint32_t PwOccupiedBlock(AtPwConstrainer self, uint32_t pwId)
    {
    uint32_t payloadSizeInByte = PayloadSizeInBytes(self, pwId, AtPwConstrainerPayloadSizeGet(self, pwId));
    uint32_t bufferSizeInUs = AtPwConstrainerJitterBufferSizeGet(self, pwId);
    uint32_t bufferSizeInPkt = NumPacketsFromUs(bufferSizeInUs, AtPwConstrainerCircuitRateGet(self, pwId), payloadSizeInByte);

    return bufferSizeInPkt * NumBlockPerPacket(self, payloadSizeInByte);
    }

static void PwNumBlockForBufferSet(AtPwConstrainer self, uint32_t pwId, uint32_t numBlock)
    {
    tAtPwCache* pwCache = PwCacheByIdGet(self, pwId);
    if (pwCache)
        pwCache->numBufferBlock = numBlock;
    }

static uint32_t PwNumBlockForBufferGet(AtPwConstrainer self, uint32_t pwId)
    {
    tAtPwCache* pwCache = PwCacheByIdGet(self, pwId);
    if (pwCache)
        return pwCache->numBufferBlock;

    return 0;
    }

static void TotalInUsedRamBlockUpdate(AtPwConstrainer self, uint32_t pwId, uint32_t newOccupiedBlock)
    {
    uint32_t pwNumBlock = PwNumBlockForBufferGet(self, pwId);
    uint32_t newTotalOccupiedBlocks = self->numOccupiedBufferBlocks + newOccupiedBlock;

    if (newTotalOccupiedBlocks < pwNumBlock)
        self->numOccupiedBufferBlocks = 0;

    self->numOccupiedBufferBlocks = newTotalOccupiedBlocks - pwNumBlock;
    PwNumBlockForBufferSet(self, pwId, newOccupiedBlock);
    }

static uint32_t PwPaddingLengthGet(AtPwConstrainer self, uint32_t pwId, uint32_t controlWordLength)
    {
    const uint8_t cMinimumPayloadSize = 64;
    uint32_t pwPldSizeInByte = PayloadSizeInBytes(self, pwId, AtPwConstrainerPayloadSizeGet(self, pwId)) + controlWordLength;
    return ((pwPldSizeInByte >= cMinimumPayloadSize) ? 0 : (cMinimumPayloadSize - pwPldSizeInByte));
    }

static uint32_t BandwidthCalculate(uint32_t ethAdditionalBytes, uint32_t circuitDataRateInbps, uint32_t pwPldSizeInByte)
    {
    return ((ethAdditionalBytes * (circuitDataRateInbps / pwPldSizeInByte)) + circuitDataRateInbps);
    }

static uint32_t PwBwCalculate(AtPwConstrainer self, uint32_t pwId)
    {
    uint32_t circuitDataRateInbps = AtPwConstrainerCircuitRateGet(self, pwId) * 64000;
    uint32_t totalHeaderLength    = PwTotalHeaderLengthInByte(self, pwId);
    uint32_t gapAndPreemble       = PwEthPortGapPreembleFcsLengthGet(self, AtPwConstrainerPwEthPortGet(self, pwId));
    uint32_t paddingLength        = PwPaddingLengthGet(self, pwId, PwCwLengthGet(self, pwId));
    uint32_t ethAdditionalBytes   = (gapAndPreemble + totalHeaderLength + paddingLength);
    uint32_t pwPldSizeInByte      = PayloadSizeInBytes(self, pwId, AtPwConstrainerPayloadSizeGet(self, pwId));

    if (pwPldSizeInByte == 0)
        return 0;

    return BandwidthCalculate(ethAdditionalBytes, circuitDataRateInbps, pwPldSizeInByte);
    }

static void AllCacheDelete(AtPwConstrainer self)
    {
    if (self->allEthPortsCache)
        free(self->allEthPortsCache);

    if (self->allPwsCache)
        free(self->allPwsCache);
    }

static uint16_t CESoPPdaMaxNumFrame(AtPwConstrainer self, uint32_t pwId, uint32_t maxNumByte)
    {
    /* Super return maximum num byte */
    const uint16_t cDefaultCESoPPayloadSize = 8;
    uint32_t numSlots = PwCESoPNumTimeslotGet(self, pwId);
    uint16_t numFrames;

    if (numSlots == 0)
        return cDefaultCESoPPayloadSize;

    numFrames = (uint16_t)(maxNumByte / numSlots);
    return (uint16_t)((numFrames == 0) ? 1 : numFrames);
    }

static uint16_t CESoPPweMaxNumFrame(void)
    {
    return 512;
    }

static uint16_t CESopMaxPayloadSize(AtPwConstrainer self, uint32_t pwId, uint32_t maxPayloadInByte)
    {
    /* Maximum number of frame */
    uint16_t pdaCESoPMaxNumFrame = CESoPPdaMaxNumFrame(self, pwId, maxPayloadInByte);
    uint16_t pweCESoPMaxNumFrame = CESoPPweMaxNumFrame();

    return mMin(pdaCESoPMaxNumFrame, pweCESoPMaxNumFrame);
    }

static uint16_t CESoPDefaultMinPayloadSize(AtPwConstrainer self, uint32_t pwId)
    {
    return (PwCESoPNumTimeslotGet(self, pwId) >= 2) ? 1 : 2; /* Frames */
    }

static uint16_t CESoPPdaMinNumFrame(AtPwConstrainer self, uint32_t pwId)
    {
    uint32_t numBytes = mMethodsGet(self)->MinPayloadSizeByPwType(self, pwId);
    uint32_t numSlots = PwCESoPNumTimeslotGet(self, pwId);

    if (numSlots == 0)
        return 0;

    return (uint16_t)(numBytes / numSlots);
    }

static uint16_t CESoPMinPayloadSizeByBuffer(AtPwConstrainer self, uint32_t pwId, uint32_t jitterBufferSize)
    {
    AtUnused(jitterBufferSize);
    return (uint16_t)mMax(CESoPPdaMinNumFrame(self, pwId), CESoPDefaultMinPayloadSize(self, pwId));
    }

static uint16_t CESoPMinPayloadSizeByBandwidth(AtPwConstrainer self, uint32_t pwId)
    {
    uint32_t remainingBwForPw = mMethodsGet(self)->RemainingBwForPw(self, pwId);
    uint32_t minPayloadSizeByBw = PayloadSizeInByteFromBwCalculate(self, pwId, remainingBwForPw);
    uint32_t numSlots = PwCESoPNumTimeslotGet(self, pwId);
    return (uint16_t)((numSlots > 0) ? minPayloadSizeByBw / numSlots : 1);
    }

static uint16_t CESoPMinPayloadSize(AtPwConstrainer self, uint32_t pwId, uint32_t jitterBufferSize)
    {
    uint32_t minNumFrameByJitterBuffer = CESoPMinPayloadSizeByBuffer(self, pwId, jitterBufferSize);
    uint32_t minPayloadSizeByBw = CESoPMinPayloadSizeByBandwidth(self, pwId);

    return (uint16_t)mMax(minNumFrameByJitterBuffer, minPayloadSizeByBw);
    }

static uint32_t MinJitterDelay(AtPwConstrainer self, uint32_t pwId, uint32_t payloadSize)
    {
    uint32_t payloadSizeInBytes = PayloadSizeInBytes(self, pwId, payloadSize);
    uint32_t minJitterDelayByPayload = TimeInUsForNumBytes(mMethodsGet(self)->MinNumPacketsForJitterDelay(self) * payloadSizeInBytes,
                                                           AtPwConstrainerCircuitRateGet(self, pwId));

    return minJitterDelayByPayload;
    }

static uint32_t DefaultBufferSize(AtPwConstrainer self, uint32_t pwId)
    {
    uint32_t minJitterBufferUs = mMethodsGet(self)->MinJitterBufferSizeInMicroSecond(self, pwId);
    return mMax(MinJitterDelay(self, pwId, AtPwConstrainerPayloadSizeGet(self, pwId)) * 2, minJitterBufferUs);
    }

static uint32_t CEPDefaultPayloadSize(AtPwConstrainer self, uint32_t pwId)
    {
    uint32_t circuitRate = AtPwConstrainerCircuitRateGet(self, pwId);
    uint32_t num3c;
    uint32_t numSts = circuitRate / cAtCircuitRateSts1;

    if ((circuitRate == cAtCircuitRateStsNc(3)) ||
        (circuitRate == cAtCircuitRateSts1)     ||
        (circuitRate == cAtCircuitRateStsNc(12))||
        (circuitRate == cAtCircuitRateStsNc(48)))
        return 783;

    if (circuitRate == cAtCircuitRateVc12)
        return 140;

    if (circuitRate == cAtCircuitRateVc11)
        return 104;

    num3c = numSts / 3UL;
    if (num3c == 0)
        return 0;

    return 783UL + num3c - (783UL % num3c);
    }

static uint32_t SAToPDefaultPayloadSize(AtPwConstrainer self, uint32_t pwId)
    {
    uint32_t circuitRate = AtPwConstrainerCircuitRateGet(self, pwId);

    if (circuitRate == cAtCircuitRateE1)
        return 256;

    if (circuitRate == cAtCircuitRateDs1)
        return 193;

    return 1024;
    }

static uint32_t DefaultPayloadSizeByType(AtPwConstrainer self, uint32_t pwId)
    {
    eAtPwConstrainerPwType pwType = AtPwConstrainerPwTypeGet(self, pwId);
    if (pwType == cAtPwConstrainerPwTypeSAToP)
        return SAToPDefaultPayloadSize(self, pwId);

    if (pwType == cAtPwConstrainerPwTypeCESoP)
        return 8;

    if (pwType == cAtPwConstrainerPwTypeCEP)
        return CEPDefaultPayloadSize(self, pwId);

    return 0;
    }

static uint32_t ObjectSize(void)
    {
    return sizeof(tAtPwConstrainer);
    }

static uint8_t PwWasSetDefault(AtPwConstrainer self, uint32_t pwId)
    {
    if ((AtPwConstrainerCircuitRateGet(self, pwId) != 0) &&
        (AtPwConstrainerPwEthPortGet(self, pwId) != cAtInvalidUint32))
        return 1;

    return 0;
    }

static uint8_t DefaultEthPortTxIpg(AtPwConstrainer self, uint32_t portId)
    {
    AtUnused(self);
    AtUnused(portId);
    return 0;
    }

static uint32_t EthPortQueueBandwidthGet(AtPwConstrainer self, uint32_t portId, uint32_t queueId)
    {
    AtUnused(self);
    AtUnused(portId);
    AtUnused(queueId);

    return 0;
    }

static void PwQueueSet(AtPwConstrainer self, uint32_t pwId, uint32_t queueId)
    {
    tAtPwCache *pwCache = PwCacheByIdGet(self, pwId);
    if (pwCache)
        pwCache->queueId = queueId;
    }

static uint32_t PwQueueGet(AtPwConstrainer self, uint32_t pwId)
    {
    tAtPwCache *pwCache = PwCacheByIdGet(self, pwId);
    if (pwCache)
        return pwCache->queueId;

    return cAtInvalidUint32;
    }

static uint8_t DefaultEthPortType(AtPwConstrainer self, uint32_t portId)
    {
    AtUnused(self);
    AtUnused(portId);

    return cAtPwConstrainerEthPortTypeXfi;
    }

static uint32_t LocalIdFromUserId(AtPwConstrainer self, uint32_t pwId)
    {
    AtUnused(self);
    return pwId;
    }

static void PwReset(AtPwConstrainer self, uint32_t pwId)
    {
    AtPwConstrainerPwEthPortSet(self, pwId, cAtInvalidUint32);
    AtPwConstrainerPwQueueSet(self, pwId, cAtInvalidUint32);
    AtPwConstrainerPayloadSizeSet(self, pwId, 0);
    AtPwConstrainerJitterBufferSizeSet(self, pwId, 0);
    AtPwConstrainerCircuitRateSet(self, pwId, 0);
    AtPwConstrainerPwEthHeaderLengthSet(self, pwId, 0);
    AtPwConstrainerPwPsnLengthSet(self, pwId, 0);
    AtPwConstrainerPwRtpEnable(self, pwId, 0);
    AtPwConstrainerPwTypeSet(self, pwId, cAtPwConstrainerPwTypeInvalid);
    }

static void Delete(AtPwConstrainer self)
    {
    if (self == NULL)
        return;

    AllCacheDelete(self);
    free(self);
    }

static uint32_t MinNumPacketsForJitterDelay(AtPwConstrainer self)
    {
    AtUnused(self);
    return 2;
    }

static void MethodsInit(AtPwConstrainer self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        memset(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, MaxNumPw);
        mMethodOverride(m_methods, MaxNumEthPort);
        mMethodOverride(m_methods, UpperBoundOfPacketsForJitterBufferSize);
        mMethodOverride(m_methods, LowerBoundOfPacketsForJitterBufferSize);
        mMethodOverride(m_methods, MaxPayloadSizeByPwType);
        mMethodOverride(m_methods, MinPayloadSizeByPwType);
        mMethodOverride(m_methods, LopsThresholdHasLimitOneSecond);
        mMethodOverride(m_methods, PwBandwidthUpdate);
        mMethodOverride(m_methods, MaxJitterBufferSizeInMicroSecond);
        mMethodOverride(m_methods, MinJitterBufferSizeInMicroSecond);
        mMethodOverride(m_methods, MaxNumJitterBufferBlocks);
        mMethodOverride(m_methods, JitterBufferBlockSizeInByte);
        mMethodOverride(m_methods, RemainingBwForPw);
        mMethodOverride(m_methods, MaxBandwidthInKbps);
        mMethodOverride(m_methods, RemainingBpsOfMaxBandwidth);
        mMethodOverride(m_methods, PwWasSetDefault);
        mMethodOverride(m_methods, DefaultEthPortTxIpg);
        mMethodOverride(m_methods, DefaultEthPortType);
        mMethodOverride(m_methods, EthPortQueueBandwidthGet);
        mMethodOverride(m_methods, PwQueueSet);
        mMethodOverride(m_methods, PwQueueGet);
        mMethodOverride(m_methods, LocalIdFromUserId);
        mMethodOverride(m_methods, PwReset);
        mMethodOverride(m_methods, Delete);
        mMethodOverride(m_methods, MinNumPacketsForJitterDelay);
        }

    mMethodsSet(self, &m_methods);
    }

AtPwConstrainer AtPwConstrainerObjectInit(AtPwConstrainer self)
    {
    /* Clear memory */
    memset(self, 0, ObjectSize());

    /* Initialize implementation */
    MethodsInit(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

uint32_t AtPwConstrainerPwCurrentBandwidthGet(AtPwConstrainer self, uint32_t pwId)
    {
    tAtPwCache* pwCache = PwCacheByIdGet(self, pwId);
    if (pwCache)
        return pwCache->bandwidth;

    return 0;
    }

uint32_t AtPwConstrainerEthPortBandwidthInKbpsGet(AtPwConstrainer self, uint32_t portId)
    {
    tAtEthPortCache* ethPortCache = EthPortCacheByIdGet(self, portId);
    if (ethPortCache == NULL)
        return 0;

    return ethPortCache->provisionedBandWidth.bandWidthInKbps;
    }

uint32_t AtPwConstrainerEthPortQueueBandwidthInBpsGet(AtPwConstrainer self, uint32_t portId, uint32_t queueId)
    {
    if (self)
        return mMethodsGet(self)->EthPortQueueBandwidthGet(self, portId, queueId);
    return 0;
    }

/**
 * @addtogroup AtPwConstrainer
 * @{
 */

/**
 * Set PW type
 *
 * @param self PW constrainer
 * @param pwId PW ID
 * @param pwType Type of PW
 */
void AtPwConstrainerPwTypeSet(AtPwConstrainer self, uint32_t pwId, eAtPwConstrainerPwType pwType)
    {
    tAtPwCache* pwCache = PwCacheByIdGet(self, pwId);
    if (pwCache)
        pwCache->pwType = pwType;
    }

/**
 * Get PW type
 *
 * @param self PW constrainer
 * @param pwId PW ID
 *
 * @return Type of PW
 */
eAtPwConstrainerPwType AtPwConstrainerPwTypeGet(AtPwConstrainer self, uint32_t pwId)
    {
    tAtPwCache* pwCache = PwCacheByIdGet(self, pwId);
    if (pwCache)
        return pwCache->pwType;

    return cAtPwConstrainerPwTypeInvalid;
    }

/**
 * Set rate of PW's circuit
 *
 * @param self PW constrainer
 * @param pwId PW ID
 * @param rateInBytesPer125Us Rate of circuit
 */
void AtPwConstrainerCircuitRateSet(AtPwConstrainer self, uint32_t pwId, uint32_t rateInBytesPer125Us)
    {
    tAtPwCache* pwCache = PwCacheByIdGet(self, pwId);
    if (pwCache)
        pwCache->rateInBytesPer125Us = rateInBytesPer125Us;

    TotalInUsedRamBlockUpdate(self, pwId, PwOccupiedBlock(self, pwId));
    mMethodsGet(self)->PwBandwidthUpdate(self, pwId, PwBwCalculate(self, pwId));
    }

/**
 * Get rate of PW's circuit
 *
 * @param self PW constrainer
 * @param pwId PW ID
 *
 * @return Rate of PW's circuit
 */
uint32_t AtPwConstrainerCircuitRateGet(AtPwConstrainer self, uint32_t pwId)
    {
    tAtPwCache* pwCache = PwCacheByIdGet(self, pwId);
    if (pwCache)
        return pwCache->rateInBytesPer125Us;

    return 0;
    }

/**
 * Get minimum payload size corresponding to buffer size
 *
 * @param self PW constrainer
 * @param pwId PW ID
 * @param jitterBufferSize Buffer size in microsecond
 *
 * @return Minimum payload size
 */
uint16_t AtPwConstrainerMinPayloadSize(AtPwConstrainer self, uint32_t pwId, uint32_t jitterBufferSize)
    {
    if (AtPwConstrainerPwTypeGet(self, pwId) != cAtPwConstrainerPwTypeCESoP)
        {
        uint16_t minPayloadSizeByJitterBuffer = MinPayloadSizeByBuffer(self, pwId, jitterBufferSize);
        uint16_t minPayloadSizeByBandwidth = MinPayloadSizeByBandwidth(self, pwId);
        return mMax(minPayloadSizeByJitterBuffer, minPayloadSizeByBandwidth);
        }

    return CESoPMinPayloadSize(self, pwId, jitterBufferSize);
    }

/**
 * Get maximum payload size corresponding to buffer size
 *
 * @param self PW constrainer
 * @param pwId PW ID
 * @param jitterBufferSize Buffer size in microsecond
 * @return Maximum payload size
 */
uint16_t AtPwConstrainerMaxPayloadSize(AtPwConstrainer self, uint32_t pwId, uint32_t jitterBufferSize)
    {
    uint32_t maxPayloadSizeByBuffer = MaxPayloadSizeByBuffer(self, pwId, jitterBufferSize);
    uint16_t maxPayloadSizeByType = mMethodsGet(self)->MaxPayloadSizeByPwType(self, pwId);
    uint16_t maxPayloadInByte = (uint16_t)mMin(maxPayloadSizeByBuffer, maxPayloadSizeByType);

    if (AtPwConstrainerPwTypeGet(self, pwId) != cAtPwConstrainerPwTypeCESoP)
        return maxPayloadInByte;

    return CESopMaxPayloadSize(self, pwId, maxPayloadInByte);
    }

/**
 * Set PW payload size
 *
 * @param self PW constrainer
 * @param pwId PW ID
 * @param payloadSize PW payload size
 */
void AtPwConstrainerPayloadSizeSet(AtPwConstrainer self, uint32_t pwId, uint32_t payloadSize)
    {
    tAtPwCache* pwCache = PwCacheByIdGet(self, pwId);
    if (pwCache)
        pwCache->payloadSize = payloadSize;

    TotalInUsedRamBlockUpdate(self, pwId, PwOccupiedBlock(self, pwId));
    mMethodsGet(self)->PwBandwidthUpdate(self, pwId, PwBwCalculate(self, pwId));
    }

/**
 * Get PW payload size
 *
 * @param self PW constrainer
 * @param pwId PW ID
 *
 * @return PW payload size
 */
uint32_t AtPwConstrainerPayloadSizeGet(AtPwConstrainer self, uint32_t pwId)
    {
    tAtPwCache* pwCache = PwCacheByIdGet(self, pwId);
    if (pwCache == NULL)
        return 0;

    if (pwCache->payloadSize)
        return pwCache->payloadSize;

    if (mMethodsGet(self)->PwWasSetDefault(self, pwId))
        return DefaultPayloadSizeByType(self, pwId);

    return 0;
    }

/**
 * Get minimum jitter buffer size corresponding with payload size
 *
 * @param self PW constrainer
 * @param pwId PW ID
 * @param payloadSize Payload size
 *
 * @return Minimum jitter buffer
 */
uint32_t AtPwConstrainerMinJitterBufferSize(AtPwConstrainer self, uint32_t pwId, uint32_t payloadSize)
    {
    uint32_t payloadSizeInBytes = PayloadSizeInBytes(self, pwId, payloadSize);
    uint32_t minJitterBufferByPayload = TimeInUsForNumBytes(mMethodsGet(self)->LowerBoundOfPacketsForJitterBufferSize(self) * payloadSizeInBytes,
                                                            AtPwConstrainerCircuitRateGet(self, pwId));
    uint32_t minJitterBufferUs = mMethodsGet(self)->MinJitterBufferSizeInMicroSecond(self, pwId);
    return mMax(minJitterBufferByPayload, minJitterBufferUs);
    }

/**
 * Get maximum jitter buffer size corresponding with payload size
 *
 * @param self PW constrainer
 * @param pwId PW ID
 * @param payloadSize Payload size
 * @return Maximum jitter buffer
 */
uint32_t AtPwConstrainerMaxJitterBufferSize(AtPwConstrainer self, uint32_t pwId, uint32_t payloadSize)
    {
    uint32_t payloadSizeInBytes = PayloadSizeInBytes(self, pwId, payloadSize);
    uint32_t maxJitterBufferByPayload = TimeInUsForNumBytes(mMethodsGet(self)->UpperBoundOfPacketsForJitterBufferSize(self, pwId) * payloadSizeInBytes,
                                                            AtPwConstrainerCircuitRateGet(self, pwId));
    uint32_t maxBufferSizeUs = mMethodsGet(self)->MaxJitterBufferSizeInMicroSecond(self, pwId);
    return mMin(maxJitterBufferByPayload, maxBufferSizeUs);
    }

/**
 * Set PW jitter buffer size
 *
 * @param self PW constrainer
 * @param pwId PW ID
 * @param microseconds Jitter buffer size
 */
void AtPwConstrainerJitterBufferSizeSet(AtPwConstrainer self, uint32_t pwId, uint32_t microseconds)
    {
    tAtPwCache* pwCache = PwCacheByIdGet(self, pwId);
    if (pwCache)
        pwCache->bufferSizeInUs = microseconds;

    TotalInUsedRamBlockUpdate(self, pwId, PwOccupiedBlock(self, pwId));
    }

/**
 * Get PW jitter buffer size
 *
 * @param self PW constrainer
 * @param pwId PW ID
 * @return Jitter buffer size in microsecond
 */
uint32_t AtPwConstrainerJitterBufferSizeGet(AtPwConstrainer self, uint32_t pwId)
    {
    tAtPwCache* pwCache = PwCacheByIdGet(self, pwId);
    if (pwCache == NULL)
        return 0;

    if (pwCache->bufferSizeInUs)
        return pwCache->bufferSizeInUs;

    if (mMethodsGet(self)->PwWasSetDefault(self, pwId))
        return DefaultBufferSize(self, pwId);

    return 0;
    }

/**
 * To apply valid combination of payload size and jitter buffer size
 *
 * @param self PW constrainer
 * @param pwId PW ID
 * @param microseconds Jitter buffer size
 * @param payloadSize Payload size
 */
void AtPwConstrainerJitterBufferAndPayloadSizeSet(AtPwConstrainer self, uint32_t pwId, uint32_t microseconds, uint32_t payloadSize)
    {
    tAtPwCache* pwCache = PwCacheByIdGet(self, pwId);
    if (pwCache == NULL)
        return;

    pwCache->bufferSizeInUs = microseconds;
    pwCache->payloadSize = payloadSize;

    TotalInUsedRamBlockUpdate(self, pwId, PwOccupiedBlock(self, pwId));
    mMethodsGet(self)->PwBandwidthUpdate(self, pwId, PwBwCalculate(self, pwId));
    }

/**
 * Set Ethernet port type
 *
 * @param self PW constrainer
 * @param ethPortId Port ID
 * @param portType Port type
 */
void AtPwConstrainerPwEthPortTypeSet(AtPwConstrainer self, uint32_t ethPortId, eAtPwConstrainerEthPortType portType)
    {
    tAtEthPortCache * ethPortCache = EthPortCacheByIdGet(self, ethPortId);
    if (ethPortCache)
        ethPortCache->portType = portType;
    }

/**
 * Get Ethernet port type
 *
 * @param self PW constrainer
 * @param ethPortId Port ID
 * @return Port type
 */
eAtPwConstrainerEthPortType AtPwConstrainerPwEthPortTypeGet(AtPwConstrainer self, uint32_t ethPortId)
    {
    tAtEthPortCache * ethPortCache = EthPortCacheByIdGet(self, ethPortId);
    if (ethPortCache == NULL)
        return cAtPwConstrainerEthPortTypeUnknown;

    if (ethPortCache->portType != cAtPwConstrainerEthPortTypeUnknown)
        return ethPortCache->portType;

    return mMethodsGet(self)->DefaultEthPortType(self, ethPortId);
    }

/**
 * Set Ethernet port Tx inter-packet gap
 *
 * @param self PW constrainer
 * @param ethPortId Port ID
 * @param portTxGap Tx inter-packet gap
 */
void AtPwConstrainerEthPortTxIpgSet(AtPwConstrainer self, uint32_t ethPortId, uint8_t portTxGap)
    {
    tAtEthPortCache *ethPortCache = EthPortCacheByIdGet(self, ethPortId);
    if (ethPortCache)
        ethPortCache->portTxGap = portTxGap;
    }

/**
 * Get Ethernet port Tx inter-packet gap
 *
 * @param self PW constrainer
 * @param ethPortId Port ID
 * @return Tx inter-packet gap
 */
uint8_t AtPwConstrainerEthPortTxIpgGet(AtPwConstrainer self, uint32_t ethPortId)
    {
    tAtEthPortCache *ethPortCache = EthPortCacheByIdGet(self, ethPortId);
    if (ethPortCache == NULL)
        return 0;

    if (ethPortCache->portTxGap)
        return ethPortCache->portTxGap;

    return mMethodsGet(self)->DefaultEthPortTxIpg(self, ethPortId);
    }

/**
 * Set PW Ethernet header length
 *
 * @param self PW constrainer
 * @param pwId PW ID
 * @param numBytes Ethernet header length in byte, including DA, SA, Ethernet type, VLANs (if have)
 */
void AtPwConstrainerPwEthHeaderLengthSet(AtPwConstrainer self, uint32_t pwId, uint32_t numBytes)
    {
    tAtPwCache* pwCache = PwCacheByIdGet(self, pwId);
    if (pwCache)
        pwCache->ethHeaderLengthInByte = numBytes;

    mMethodsGet(self)->PwBandwidthUpdate(self, pwId, PwBwCalculate(self, pwId));
    }

/**
 * Get PW Ethernet header length
 *
 * @param self PW constrainer
 * @param pwId PW ID
 * @return Ethernet header length in byte
 */
uint32_t AtPwConstrainerPwEthHeaderLengthGet(AtPwConstrainer self, uint32_t pwId)
    {
    tAtPwCache* pwCache = PwCacheByIdGet(self, pwId);
    if (pwCache)
        return pwCache->ethHeaderLengthInByte;

    return 0;
    }

/**
 * Set PW PSN length
 *
 * @param self PW constrainer
 * @param pwId PW ID
 * @param numBytes PSN length in byte
 */
void AtPwConstrainerPwPsnLengthSet(AtPwConstrainer self, uint32_t pwId, uint32_t numBytes)
    {
    tAtPwCache* pwCache = PwCacheByIdGet(self, pwId);
    if (pwCache)
        pwCache->psnLengthInByte = numBytes;

    mMethodsGet(self)->PwBandwidthUpdate(self, pwId, PwBwCalculate(self, pwId));
    }

/**
 * Get PW PSN length
 *
 * @param self PW constrainer
 * @param pwId PW ID
 *
 * @return PSN length in byte
 */
uint32_t AtPwConstrainerPwPsnLengthGet(AtPwConstrainer self, uint32_t pwId)
    {
    tAtPwCache* pwCache = PwCacheByIdGet(self, pwId);
    if (pwCache)
        return pwCache->psnLengthInByte;

    return 0;
    }

/**
 * Assign PW to Ethernet port
 *
 * @param self PW constrainer
 * @param pwId PW ID
 * @param ethPortId Ethernet port ID
 */
void AtPwConstrainerPwEthPortSet(AtPwConstrainer self, uint32_t pwId, uint32_t ethPortId)
    {
    tAtPwCache* pwCache = PwCacheByIdGet(self, pwId);
    uint32_t pwBw;
    if (pwCache == NULL)
        return;

    EthPortInUsedBandwidthUpdate(self, pwCache->ethPortId, AtPwConstrainerPwCurrentBandwidthGet(self, pwId), 0);
    pwCache->ethPortId = ethPortId;

    pwBw = PwBwCalculate(self, pwId);
    EthPortInUsedBandwidthUpdate(self, ethPortId, 0, pwBw);
    PwCurrentBandwidthSet(self, pwId, pwBw);
    }

/**
 * Get PW Ethernet port ID
 *
 * @param self PW constrainer
 * @param pwId PW ID
 *
 * @return Ethernet port ID
 */
uint32_t AtPwConstrainerPwEthPortGet(AtPwConstrainer self, uint32_t pwId)
    {
    tAtPwCache* pwCache = PwCacheByIdGet(self, pwId);
    if (pwCache)
        return pwCache->ethPortId;

    return cAtInvalidUint32;
    }

/**
 * Enable PW RTP
 *
 * @param self PW constrainer
 * @param pwId PW ID
 * @param enable
 *               - 1 to enable PW RTP
 *               - 0 to disable PW RTP
 */
void AtPwConstrainerPwRtpEnable(AtPwConstrainer self, uint32_t pwId, uint8_t enable)
    {
    tAtPwCache* pwCache = PwCacheByIdGet(self, pwId);
    if (pwCache)
        pwCache->rtpIsEnabled = enable;

    mMethodsGet(self)->PwBandwidthUpdate(self, pwId, PwBwCalculate(self, pwId));
    }

/**
 * Check if PW RTP is enabled
 *
 * @param self PW constrainer
 * @param pwId PW ID
 * @return
 *               - 1 if PW RTP is enabled
 *               - 0 if PW RTP is disabled
 */
uint8_t AtPwConstrainerPwRtpIsEnabled(AtPwConstrainer self, uint32_t pwId)
    {
    tAtPwCache* pwCache = PwCacheByIdGet(self, pwId);
    if (pwCache)
        return pwCache->rtpIsEnabled;

    return 0;
    }

/**
 * Assign PW to Ethernet port queue, this API is necessary for DS3 card only, there is no effect on other cards
 *
 * @param self PW constrainer
 * @param pwId PW ID
 * @param queueId Queue ID
 */
void AtPwConstrainerPwQueueSet(AtPwConstrainer self, uint32_t pwId, uint32_t queueId)
    {
    if (self)
        mMethodsGet(self)->PwQueueSet(self, pwId, queueId);
    }

/**
 * Get PW Ethernet port Queue
 *
 * @param self PW constrainer
 * @param pwId PW ID
 * @return Queue ID
 */
uint32_t AtPwConstrainerPwQueueGet(AtPwConstrainer self, uint32_t pwId)
    {
    if (self)
        return mMethodsGet(self)->PwQueueGet(self, pwId);

    return cAtInvalidUint32;
    }

/**
 * Set LOPS threshold, this API is necessary for OCN and DS3 card only, there is no effect on DS1 card
 *
 * @param self PW constrainer
 * @param pwId PW ID
 * @param lopsThreshold LOPS threshold
 */
void AtPwConstrainerPwLopsThresholdSet(AtPwConstrainer self, uint32_t pwId, uint32_t lopsThreshold)
    {
    tAtPwCache* pwCache = PwCacheByIdGet(self, pwId);
    if (pwCache)
        pwCache->lopsThreshold = lopsThreshold;
    }

/**
 * Get LOPS threshold
 *
 * @param self PW constrainer
 * @param pwId PW ID
 * @return LOPS threshold
 */
uint32_t AtPwConstrainerPwLopsThresholdGet(AtPwConstrainer self, uint32_t pwId)
    {
    const uint32_t cDefaultLopsSetThreshold = 30;
    tAtPwCache* pwCache = PwCacheByIdGet(self, pwId);
    if (pwCache == NULL)
        return 0;

    if (pwCache->lopsThreshold)
        return pwCache->lopsThreshold;

    return cDefaultLopsSetThreshold;
    }

/**
 * Delete PW constrainer
 *
 * @param self PW constrainer
 */
void AtPwConstrainerDelete(AtPwConstrainer self)
    {
    if (self)
        mMethodsGet(self)->Delete(self);
    }

/**
 * Get version of PW constrainer
 *
 * @param self PW constrainer
 * @return version
 */
const char *AtPwConstrainerVersion(AtPwConstrainer self)
    {
    AtUnused(self);
    return "1.0";
    }
    
/**
 * Reset Pw constrainer, PW constrainer should be reset after PW is deleted
 *
 * @param self PW constrainer
 * @param pwId PW ID
 */
void AtPwConstrainerPwReset(AtPwConstrainer self, uint32_t pwId)
    {
    if (self)
        mMethodsGet(self)->PwReset(self, pwId);
    }

/**
 * Get number of ethernet ports
 *
 * @param self PW constrainer
 * @return number of ethernet ports
 */
uint32_t AtPwConstrainerMaxNumEthPort(AtPwConstrainer self)
    {
    if (self == NULL)
        return 0;

    return mMethodsGet(self)->MaxNumEthPort(self);
    }

/**
 * @}
 */

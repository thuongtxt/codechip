/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : AtPwConstrainerDs3Card.c
 *
 * Created Date: Feb 17, 2016
 *
 * Description : PW constrains for DS3 card
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwConstrainerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cMaxNumQsgmiiLanes 5
#define mThis(self) ((tAtPwConstrainerDs3Card*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPwConstrainerDs3Card
    {
    tAtPwConstrainer super;
    uint32_t laneBandwidthInBps[cMaxNumQsgmiiLanes];
    }tAtPwConstrainerDs3Card;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8_t m_methodsInit = 0;

/* Override */
static tAtPwConstrainerMethods m_AtPwConstrainerOverride;
static const tAtPwConstrainerMethods * m_AtPwConstrainerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32_t MaxNumPw(AtPwConstrainer self)
    {
    AtUnused(self);
    return 1344;
    }

static uint32_t MaxNumEthPort(AtPwConstrainer self)
    {
    AtUnused(self);
    return 1;
    }

static uint32_t UpperBoundOfPacketsForJitterBufferSize(AtPwConstrainer self, uint32_t pwId)
    {
    AtUnused(self);
    AtUnused(pwId);
    return 511;
    }

static uint32_t MaxNumJitterBufferBlocks(AtPwConstrainer self)
    {
    AtUnused(self);
    return 262144;
    }

static uint32_t JitterBufferBlockSizeInByte(AtPwConstrainer self)
    {
    AtUnused(self);
    return 256;
    }

static uint32_t MaxBandwidthInKbps(AtPwConstrainer self, uint32_t portId)
    {
    AtUnused(self);
    AtUnused(portId);
    return 4500000; /* 4.5G */
    }

static uint32_t QueueMaxBandwidthInBps(void)
    {
    return 900000000; /* 900M */
    }

static uint8_t MultipleLanesIsSupported(AtPwConstrainer self, uint32_t portId)
    {
    if (AtPwConstrainerPwEthPortTypeGet(self, portId) == cAtPwConstrainerEthPortTypeQsgmii)
        return 1;

    return 0;
    }

static uint8_t QueueIdIsValid(uint32_t queueId)
    {
    if (queueId >= cMaxNumQsgmiiLanes)
        return 0;

    return 1;
    }

static void QueueBandwidthAdd(AtPwConstrainer self, uint32_t queueId, uint32_t bandwidth)
    {
    mThis(self)->laneBandwidthInBps[queueId] += bandwidth;
    }

static void QueueBandwidthRemove(AtPwConstrainer self, uint32_t queueId, uint32_t bandwidth)
    {
    if (mThis(self)->laneBandwidthInBps[queueId] < bandwidth)
        mThis(self)->laneBandwidthInBps[queueId] = 0;
    else
        mThis(self)->laneBandwidthInBps[queueId] -= bandwidth;
    }

static void QueueInUsedBandwidthUpdate(AtPwConstrainer self, uint32_t queueId, uint32_t removeBw, uint32_t addBw)
    {
    if (!QueueIdIsValid(queueId))
        return;

    if (removeBw == addBw)
        return;

    QueueBandwidthAdd(self, queueId, addBw);
    QueueBandwidthRemove(self, queueId, removeBw);
    }

static void PwBandwidthUpdate(AtPwConstrainer self, uint32_t pwId, uint32_t newPwBandwidth)
    {
    uint32_t pwEthPort = AtPwConstrainerPwEthPortGet(self, pwId);
    uint32_t queueId = AtPwConstrainerPwQueueGet(self, pwId);
    uint32_t currentPwBw;

    if (!QueueIdIsValid(queueId) || !MultipleLanesIsSupported(self, pwEthPort))
        {
        m_AtPwConstrainerMethods->PwBandwidthUpdate(self, pwId, newPwBandwidth);
        return;
        }

    currentPwBw = AtPwConstrainerPwCurrentBandwidthGet(self, pwId);
    QueueInUsedBandwidthUpdate(self, queueId, currentPwBw, newPwBandwidth);
    m_AtPwConstrainerMethods->PwBandwidthUpdate(self, pwId, newPwBandwidth);
    }

static uint32_t QueueBandwidthTryRemoving(AtPwConstrainer self, uint32_t queueId, uint32_t bandwidth)
    {
    if (mThis(self)->laneBandwidthInBps[queueId] < bandwidth)
        return 0;

    return mThis(self)->laneBandwidthInBps[queueId] - bandwidth;
    }

static uint32_t QueuePwRemainingBandwidthInBps(AtPwConstrainer self, uint32_t queueId, uint32_t removeBwInBps)
    {
    uint32_t queueBwAfterRemoving = QueueBandwidthTryRemoving(self, queueId, removeBwInBps);
    return QueueMaxBandwidthInBps() - queueBwAfterRemoving;
    }

static uint32_t QueuesMaxRemainingBandwidthInBpsGet(AtPwConstrainer self, uint32_t queueId, uint32_t removeBwInBps)
    {
    uint32_t maxRemaining = 0;
    uint32_t queue_i;

    AtUnused(queueId);
    AtUnused(removeBwInBps);
    for (queue_i = 0; queue_i < cMaxNumQsgmiiLanes; queue_i++)
        {
        uint32_t queueRemaining = QueueMaxBandwidthInBps() - mThis(self)->laneBandwidthInBps[queue_i];
        if (queueRemaining > maxRemaining)
            maxRemaining = queueRemaining;
        }

    return maxRemaining;
    }

static uint32_t QueueRemainingBandwidthInBpsGet(AtPwConstrainer self, uint32_t pwId)
    {
    uint32_t queueId = AtPwConstrainerPwQueueGet(self, pwId);

    /* User select queue, return on that selected queue */
    if (QueueIdIsValid(queueId))
        return QueuePwRemainingBandwidthInBps(self, queueId, AtPwConstrainerPwCurrentBandwidthGet(self, pwId));

    /* Return max bandwidth of all queues */
    return QueuesMaxRemainingBandwidthInBpsGet(self, queueId, AtPwConstrainerPwCurrentBandwidthGet(self, pwId));
    }

static uint32_t RemainingBwForPw(AtPwConstrainer self, uint32_t pwId)
    {
    uint32_t ethPort = AtPwConstrainerPwEthPortGet(self, pwId);
    if ((ethPort == cAtInvalidUint32) || !MultipleLanesIsSupported(self, ethPort))
        return m_AtPwConstrainerMethods->RemainingBwForPw(self, pwId);

    return QueueRemainingBandwidthInBpsGet(self, pwId);
    }

static uint8_t LopsThresholdHasLimitOneSecond(AtPwConstrainer self)
    {
    AtUnused(self);
    return 1;
    }

static uint32_t EthPortQueueBandwidthGet(AtPwConstrainer self, uint32_t portId, uint32_t queueId)
    {
    if (!QueueIdIsValid(queueId) || !MultipleLanesIsSupported(self, portId))
        return 0;

    return mThis(self)->laneBandwidthInBps[queueId];
    }

static void PwQueueSet(AtPwConstrainer self, uint32_t pwId, uint32_t queueId)
    {
    uint32_t pwBandwidth;
    if (!QueueIdIsValid(queueId) || !MultipleLanesIsSupported(self, AtPwConstrainerPwEthPortGet(self, pwId)))
        return;

    pwBandwidth = AtPwConstrainerPwCurrentBandwidthGet(self, pwId);
    QueueInUsedBandwidthUpdate(self, AtPwConstrainerPwQueueGet(self, pwId), pwBandwidth, 0);
    m_AtPwConstrainerMethods->PwQueueSet(self, pwId, queueId);
    QueueInUsedBandwidthUpdate(self, queueId, 0, pwBandwidth);
    }

static uint8_t PwWasSetDefault(AtPwConstrainer self, uint32_t pwId)
    {
    if (AtPwConstrainerCircuitRateGet(self, pwId) != 0)
        return 1;

    return 0;
    }

static uint8_t DefaultEthPortTxIpg(AtPwConstrainer self, uint32_t portId)
    {
    AtUnused(self);
    AtUnused(portId);
    return 12;
    }

static void OverrideAtPwConstrainer(AtPwConstrainer self)
    {
    if (!m_methodsInit)
        {
        m_AtPwConstrainerMethods = mMethodsGet(self);
        memcpy(&m_AtPwConstrainerOverride, m_AtPwConstrainerMethods, sizeof(tAtPwConstrainerMethods));
        mMethodOverride(m_AtPwConstrainerOverride, MaxNumPw);
        mMethodOverride(m_AtPwConstrainerOverride, MaxNumEthPort);
        mMethodOverride(m_AtPwConstrainerOverride, UpperBoundOfPacketsForJitterBufferSize);
        mMethodOverride(m_AtPwConstrainerOverride, MaxNumJitterBufferBlocks);
        mMethodOverride(m_AtPwConstrainerOverride, JitterBufferBlockSizeInByte);
        mMethodOverride(m_AtPwConstrainerOverride, MaxBandwidthInKbps);
        mMethodOverride(m_AtPwConstrainerOverride, PwBandwidthUpdate);
        mMethodOverride(m_AtPwConstrainerOverride, RemainingBwForPw);
        mMethodOverride(m_AtPwConstrainerOverride, LopsThresholdHasLimitOneSecond);
        mMethodOverride(m_AtPwConstrainerOverride, EthPortQueueBandwidthGet);
        mMethodOverride(m_AtPwConstrainerOverride, PwQueueSet);
        mMethodOverride(m_AtPwConstrainerOverride, PwWasSetDefault);
        mMethodOverride(m_AtPwConstrainerOverride, DefaultEthPortTxIpg);
        }

    mMethodsSet(self, &m_AtPwConstrainerOverride);
    }

static uint32_t ObjectSize(void)
    {
    return sizeof(tAtPwConstrainerDs3Card);
    }

static AtPwConstrainer ObjectInit(AtPwConstrainer self)
    {
    /* Clear memory */
    memset(self, 0, ObjectSize());

    /* Super constructor */
    if (AtPwConstrainerObjectInit(self) == NULL)
        return NULL;

    /* Override */
    OverrideAtPwConstrainer(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtPwConstrainer
 * @{
 */

/**
 * Create PW constrainer for DS3 card
 *
 * @return PW constrainer
 */
AtPwConstrainer AtPwConstrainerDs3CardNew(void)
    {
    /* Allocate memory */
    AtPwConstrainer newConstrainer = malloc(ObjectSize());
    if (newConstrainer == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newConstrainer);
    }

/**
 * @}
 */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Pseudowire
 *
 * File        : AtPwConstrainer5GMsCard.c
 *
 * Created Date: May 16, 2016
 *
 * Description : CES/CEP Pseudowire constrainer for 5G MS card
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwConstrainerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8_t m_methodsInit = 0;

/* Override */
static tAtPwConstrainerMethods m_AtPwConstrainerOverride;
static const tAtPwConstrainerMethods * m_AtPwConstrainerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32_t MaxNumPw(AtPwConstrainer self)
    {
    AtUnused(self);
    return 3072;
    }

static uint32_t MaxNumEthPort(AtPwConstrainer self)
    {
    AtUnused(self);
    return 1;
    }

static void OverrideAtPwConstrainer(AtPwConstrainer self)
    {
    if (!m_methodsInit)
        {
        m_AtPwConstrainerMethods = mMethodsGet(self);
        memcpy(&m_AtPwConstrainerOverride, m_AtPwConstrainerMethods, sizeof(tAtPwConstrainerMethods));
        mMethodOverride(m_AtPwConstrainerOverride, MaxNumPw);
        mMethodOverride(m_AtPwConstrainerOverride, MaxNumEthPort);
        }

    mMethodsSet(self, &m_AtPwConstrainerOverride);
    }

static uint32_t ObjectSize(void)
    {
    return sizeof(tAtPwConstrainer5GMsCard);
    }

AtPwConstrainer AtPwConstrainer5GMsCardObjectInit(AtPwConstrainer self)
    {
    /* Clear memory */
    memset(self, 0, ObjectSize());

    /* Super constructor */
    if (AtPwConstrainerOcnCardObjectInit(self) == NULL)
        return NULL;

    /* Override */
    OverrideAtPwConstrainer(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtPwConstrainer
 * @{
 */

/**
 * Create PW constrainer for 5G MS card
 *
 * @return PW constrainer
 */
AtPwConstrainer AtPwConstrainer5GMsCardNew(void)
    {
    /* Allocate memory */
    AtPwConstrainer newConstrainer = malloc(ObjectSize());
    if (newConstrainer == NULL)
        return NULL;

    /* Construct it */
    return AtPwConstrainer5GMsCardObjectInit(newConstrainer);
    }
/**
 * @}
 */

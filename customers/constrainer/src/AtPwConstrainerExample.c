/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO
 *
 * File        : AtPwConstrainerExample.c
 *
 * Created Date: Feb 5, 2016
 *
 * Description : TODO
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <assert.h>
#include "AtPwConstrainer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
void CepConstrains(AtPwConstrainer constrainer);
void SAToPConstrains(AtPwConstrainer constrainer);
void SAToPConstrainsWithBandwidth(AtPwConstrainer constrainer);

/*--------------------------- Implementation ---------------------------------*/
void CepConstrains(AtPwConstrainer constrainer)
    {
    uint32_t pwId;
    uint32_t minPayloadSize, maxPayloadSize;
    uint32_t minJitterBufferSize, maxJitterBufferSize;
    uint32_t payloadSize, jitterBufferSize;

    /* VC11 constrains, let's use PW#0 in this example. Assume that application
     * need to configure jitter buffer size 4ms and payload size 256 bytes.
     * Let's take jitter buffer size and check if payload size satisfies
     * constrains */
    pwId = 0; jitterBufferSize = 4000; payloadSize = 256;
    AtPwConstrainerPwTypeSet(constrainer, pwId, cAtPwConstrainerPwTypeCEP);
    AtPwConstrainerCircuitRateSet(constrainer, pwId, cAtCircuitRateVc11);
    minPayloadSize = AtPwConstrainerMinPayloadSize(constrainer, pwId, jitterBufferSize);
    maxPayloadSize = AtPwConstrainerMaxPayloadSize(constrainer, pwId, jitterBufferSize);
    assert((payloadSize >= minPayloadSize) && (payloadSize <= maxPayloadSize));
    AtPwConstrainerJitterBufferAndPayloadSizeSet(constrainer, pwId, jitterBufferSize, payloadSize);

    /* STS-1 constrains, let's use PW#1 in this example. Assume that application
     * need to configure jitter buffer size 4ms and payload size 783 bytes.
     * Let's take payload size and check if jitter buffer size satisfies
     * constrains */
    pwId = 1; jitterBufferSize = 4000; payloadSize = 783;
    AtPwConstrainerPwTypeSet(constrainer, pwId, cAtPwConstrainerPwTypeCEP);
    AtPwConstrainerCircuitRateSet(constrainer, pwId, cAtCircuitRateSts1);
    minJitterBufferSize = AtPwConstrainerMinJitterBufferSize(constrainer, pwId, payloadSize);
    maxJitterBufferSize = AtPwConstrainerMaxJitterBufferSize(constrainer, pwId, payloadSize);
    assert((jitterBufferSize >= minJitterBufferSize) && (jitterBufferSize <= maxJitterBufferSize));
    AtPwConstrainerJitterBufferAndPayloadSizeSet(constrainer, pwId, jitterBufferSize, payloadSize);
    }

void SAToPConstrains(AtPwConstrainer constrainer)
    {
    uint32_t pwId;
    uint32_t minPayloadSize, maxPayloadSize;
    uint32_t minJitterBufferSize, maxJitterBufferSize;
    uint32_t payloadSize, jitterBufferSize;

    /* DS1 constrains, let's use PW#0 in this example. Assume that application
     * need to configure jitter buffer size 4ms and payload size 256 bytes.
     * Let's take jitter buffer size and check if payload size satisfies
     * constrains */
    pwId = 0; jitterBufferSize = 4000; payloadSize = 256;
    AtPwConstrainerPwTypeSet(constrainer, pwId, cAtPwConstrainerPwTypeSAToP);
    AtPwConstrainerCircuitRateSet(constrainer, pwId, cAtCircuitRateDs1);
    minPayloadSize = AtPwConstrainerMinPayloadSize(constrainer, pwId, jitterBufferSize);
    maxPayloadSize = AtPwConstrainerMaxPayloadSize(constrainer, pwId, jitterBufferSize);
    assert((payloadSize >= minPayloadSize) && (payloadSize <= maxPayloadSize));
    AtPwConstrainerJitterBufferAndPayloadSizeSet(constrainer, pwId, jitterBufferSize, payloadSize);

    /* DS3 constrains, let's use PW#1 in this example. Assume that application
     * need to configure jitter buffer size 10ms and payload size 1024 bytes.
     * Let's take payload size and check if jitter buffer size satisfies
     * constrains */
    pwId = 1; jitterBufferSize = 10000; payloadSize = 1024;
    AtPwConstrainerPwTypeSet(constrainer, pwId, cAtPwConstrainerPwTypeSAToP);
    AtPwConstrainerCircuitRateSet(constrainer, pwId, cAtCircuitRateDs3);
    minJitterBufferSize = AtPwConstrainerMinJitterBufferSize(constrainer, pwId, payloadSize);
    maxJitterBufferSize = AtPwConstrainerMaxJitterBufferSize(constrainer, pwId, payloadSize);
    assert((jitterBufferSize >= minJitterBufferSize) && (jitterBufferSize <= maxJitterBufferSize));
    AtPwConstrainerJitterBufferAndPayloadSizeSet(constrainer, pwId, jitterBufferSize, payloadSize);
    }

void SAToPConstrainsWithBandwidth(AtPwConstrainer constrainer)
    {
    uint32_t pwId;
    uint32_t minPayloadSize, maxPayloadSize;
    uint32_t payloadSize, jitterBufferSize;
    uint32_t ethPortId = 0; /* Let's use port 0 for example */

    /* DS1 constrains, let's use PW#0 in this example. Assume that application
     * need to configure jitter buffer size 4ms and payload size 256 bytes.
     * Let's take jitter buffer size and check if payload size satisfies
     * constrains */
    pwId = 0; jitterBufferSize = 4000; payloadSize = 256;
    AtPwConstrainerPwTypeSet(constrainer, pwId, cAtPwConstrainerPwTypeSAToP);
    AtPwConstrainerCircuitRateSet(constrainer, pwId, cAtCircuitRateDs1);

    /* Let the constrainer know about ETH side:
     * - Port 0 is used with QSGMII interface
     * - length(ETH Header: DMAC/SMAC/1VLAN/ETHTYPE) = 18 bytes
     * - length(MPLS Header: 1 label) = 4 bytes
     *
     * For DS3 card, queue needs to be specified and queue 0 is used in this example
     */
    AtPwConstrainerPwEthPortTypeSet(constrainer, ethPortId, cAtPwConstrainerEthPortTypeQsgmii);
    AtPwConstrainerPwEthHeaderLengthSet(constrainer, pwId, mAtEthHeaderLengthWithNumberOfVlans(1));
    AtPwConstrainerPwPsnLengthSet(constrainer, pwId, mAtMplsHeaderLengthWithNumberOfLabels(1));
    AtPwConstrainerPwQueueSet(constrainer, pwId, 0);

    /* Now, payload size can be constrained base on current in-used bandwidth */
    minPayloadSize = AtPwConstrainerMinPayloadSize(constrainer, pwId, jitterBufferSize);
    maxPayloadSize = AtPwConstrainerMaxPayloadSize(constrainer, pwId, jitterBufferSize);
    assert((payloadSize >= minPayloadSize) && (payloadSize <= maxPayloadSize));
    AtPwConstrainerJitterBufferAndPayloadSizeSet(constrainer, pwId, jitterBufferSize, payloadSize);
    }

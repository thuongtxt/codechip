/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : AtPwConstrainerDs1Card.c
 *
 * Created Date: Feb 17, 2016
 *
 * Description : PW constrains for DS1 card
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwConstrainerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPwConstrainerDs1Card
    {
    tAtPwConstrainer super;
    }tAtPwConstrainerDs1Card;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8_t m_methodsInit = 0;

/* Override */
static tAtPwConstrainerMethods m_AtPwConstrainerOverride;
static const tAtPwConstrainerMethods *m_AtPwConstrainerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32_t MaxNumPw(AtPwConstrainer self)
    {
    AtUnused(self);
    return 192;
    }

static uint32_t MaxNumEthPort(AtPwConstrainer self)
    {
    AtUnused(self);
    return 1;
    }

static uint32_t UpperBoundOfPacketsForJitterBufferSize(AtPwConstrainer self, uint32_t pwId)
    {
    AtUnused(self);
    AtUnused(pwId);
    return 512;
    }

static uint32_t MaxNumJitterBufferBlocks(AtPwConstrainer self)
    {
    AtUnused(self);
    return 16384;
    }

static uint32_t JitterBufferBlockSizeInByte(AtPwConstrainer self)
    {
    AtUnused(self);
    return 64;
    }

static uint32_t MaxBandwidthInKbps(AtPwConstrainer self, uint32_t portId)
    {
    AtUnused(self);
    AtUnused(portId);
    return 1000000; /* 1G */
    }

static uint16_t CEPMinPayloadSize(AtPwConstrainer self, uint32_t pwId)
    {
    uint32_t circuitRate = AtPwConstrainerCircuitRateGet(self, pwId);
    if ((circuitRate == cAtCircuitRateVc11) || (circuitRate == cAtCircuitRateVc12))
        return 16;

    if ((circuitRate == cAtCircuitRateSts1) || (circuitRate == cAtCircuitRateStsNc(3)))
        return 32;

    /* Default */
    return 64;
    }

static uint16_t MinPayloadSizeByPwType(AtPwConstrainer self, uint32_t pwId)
    {
    if (AtPwConstrainerPwTypeGet(self, pwId) == cAtPwConstrainerPwTypeCEP)
        return CEPMinPayloadSize(self, pwId);

    return m_AtPwConstrainerMethods->MinPayloadSizeByPwType(self, pwId);
    }

static uint8_t PwWasSetDefault(AtPwConstrainer self, uint32_t pwId)
    {
    if (AtPwConstrainerCircuitRateGet(self, pwId) != 0)
        return 1;

    return 0;
    }

static uint8_t DefaultEthPortTxIpg(AtPwConstrainer self, uint32_t portId)
    {
    AtUnused(self);
    AtUnused(portId);
    return 12;
    }

static uint32_t PwQueueGet(AtPwConstrainer self, uint32_t pwId)
    {
    uint32_t ethPortId = AtPwConstrainerPwEthPortGet(self, pwId);
    if (AtPwConstrainerPwEthPortTypeGet(self, ethPortId) == cAtPwConstrainerEthPortTypeQsgmii)
        return 0;

    return m_AtPwConstrainerMethods->PwQueueGet(self, pwId);
    }

static uint8_t DefaultEthPortType(AtPwConstrainer self, uint32_t portId)
    {
    AtUnused(self);
    AtUnused(portId);
    return cAtPwConstrainerEthPortTypeQsgmii;
    }

static void OverrideAtPwConstrainer(AtPwConstrainer self)
    {
    if (!m_methodsInit)
        {
        m_AtPwConstrainerMethods = mMethodsGet(self);
        memcpy(&m_AtPwConstrainerOverride, m_AtPwConstrainerMethods, sizeof(tAtPwConstrainerMethods));
        mMethodOverride(m_AtPwConstrainerOverride, MaxNumPw);
        mMethodOverride(m_AtPwConstrainerOverride, MaxNumEthPort);
        mMethodOverride(m_AtPwConstrainerOverride, UpperBoundOfPacketsForJitterBufferSize);
        mMethodOverride(m_AtPwConstrainerOverride, MaxNumJitterBufferBlocks);
        mMethodOverride(m_AtPwConstrainerOverride, JitterBufferBlockSizeInByte);
        mMethodOverride(m_AtPwConstrainerOverride, MaxBandwidthInKbps);
        mMethodOverride(m_AtPwConstrainerOverride, MinPayloadSizeByPwType);
        mMethodOverride(m_AtPwConstrainerOverride, PwWasSetDefault);
        mMethodOverride(m_AtPwConstrainerOverride, DefaultEthPortTxIpg);
        mMethodOverride(m_AtPwConstrainerOverride, PwQueueGet);
        mMethodOverride(m_AtPwConstrainerOverride, DefaultEthPortType);
        }

    mMethodsSet(self, &m_AtPwConstrainerOverride);
    }

static uint32_t ObjectSize(void)
    {
    return sizeof(tAtPwConstrainerDs1Card);
    }

static AtPwConstrainer ObjectInit(AtPwConstrainer self)
    {
    /* Clear memory */
    memset(self, 0, ObjectSize());

    /* Super constructor */
    if (AtPwConstrainerObjectInit(self) == NULL)
        return NULL;

    /* Override */
    OverrideAtPwConstrainer(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtPwConstrainer
 * @{
 */

/**
 * Create PW constrainer for DS1 card
 *
 * @return PW constrainer
 */
AtPwConstrainer AtPwConstrainerDs1CardNew(void)
    {
    /* Allocate memory */
    AtPwConstrainer newConstrainer = malloc(ObjectSize());
    if (newConstrainer == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newConstrainer);
    }
/**
 * @}
 */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Application
 * 
 * File        : AtPwConstrainer.h
 * 
 * Created Date: Feb 1, 2016
 *
 * Description : PW constrainer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPWCONSTRAINER_H_
#define _ATPWCONSTRAINER_H_

/*--------------------------- Includes ---------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/* Circuit rate in bytes/125us */
#define cAtCircuitRateVc11     26
#define cAtCircuitRateVc12     35
#define cAtCircuitRateDs1      24
#define cAtCircuitRateE1       32
#define cAtCircuitRateDs3      699
#define cAtCircuitRateE3       537
#define cAtCircuitRateSts1     783
#define cAtCircuitRateStsNc(N) (cAtCircuitRateSts1 * (N))
#define cAtCircuitRateNxDs0(N) (N)

/*--------------------------- Macros -----------------------------------------*/
/* Helpers for calculating header length */
#define mAtEthHeaderLengthWithNumberOfVlans(numVlans)   (6 + 6 + ((numVlans) * 4) + 2)
#define mAtMplsHeaderLengthWithNumberOfLabels(numLabels) ((numLabels) * 4)

#define cAtInvalidUint32 0xFFFFFFFF

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtPwConstrainer
 * @{
 */

/** @brief PW constrainer abstract class */
typedef struct tAtPwConstrainer * AtPwConstrainer;

/** @brief PW type. As each kind of PW has different constrains */
typedef enum eAtPwConstrainerPwType
    {
    cAtPwConstrainerPwTypeInvalid, /**< Invalid Pseudowire type */
    cAtPwConstrainerPwTypeSAToP,   /**< SAToP Pseudowire */
    cAtPwConstrainerPwTypeCESoP,   /**< CESoPSN Pseudowire */
    cAtPwConstrainerPwTypeCEP      /**< CEP Pseudowire */
    }eAtPwConstrainerPwType;

/** @brief Ethernet port type. */
typedef enum eAtPwConstrainerEthPortType
    {
    cAtPwConstrainerEthPortTypeUnknown, /**< Invalid port type */
    cAtPwConstrainerEthPortTypeXfi,     /**< XFI */
    cAtPwConstrainerEthPortTypeQsgmii   /**< QSGMII */
    }eAtPwConstrainerEthPortType;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete constrainers */
AtPwConstrainer AtPwConstrainerDs1CardNew(void);
AtPwConstrainer AtPwConstrainerDs3CardNew(void);
AtPwConstrainer AtPwConstrainerOcnCardNew(void);
AtPwConstrainer AtPwConstrainer5GMsCardNew(void);
AtPwConstrainer AtPwConstrainer3GMsCardNew(void);

const char *AtPwConstrainerVersion(AtPwConstrainer self);
void AtPwConstrainerDelete(AtPwConstrainer self);
void AtPwConstrainerPwReset(AtPwConstrainer self, uint32_t pwId);

/* PW type to calculate */
void AtPwConstrainerPwTypeSet(AtPwConstrainer self, uint32_t pwId, eAtPwConstrainerPwType pwType);
eAtPwConstrainerPwType AtPwConstrainerPwTypeGet(AtPwConstrainer self, uint32_t pwId);

/* Circuit rate */
void AtPwConstrainerCircuitRateSet(AtPwConstrainer self, uint32_t pwId, uint32_t rateInBytesPer125Us);
uint32_t AtPwConstrainerCircuitRateGet(AtPwConstrainer self, uint32_t pwId);

/* Payload size */
uint16_t AtPwConstrainerMinPayloadSize(AtPwConstrainer self, uint32_t pwId, uint32_t jitterBufferSize);
uint16_t AtPwConstrainerMaxPayloadSize(AtPwConstrainer self, uint32_t pwId, uint32_t jitterBufferSize);
void AtPwConstrainerPayloadSizeSet(AtPwConstrainer self, uint32_t pwId, uint32_t payloadSize);
uint32_t AtPwConstrainerPayloadSizeGet(AtPwConstrainer self, uint32_t pwId);

/* Jitter buffer size */
uint32_t AtPwConstrainerMinJitterBufferSize(AtPwConstrainer self, uint32_t pwId, uint32_t payloadSize);
uint32_t AtPwConstrainerMaxJitterBufferSize(AtPwConstrainer self, uint32_t pwId, uint32_t payloadSize);
void AtPwConstrainerJitterBufferSizeSet(AtPwConstrainer self, uint32_t pwId, uint32_t microseconds);
uint32_t AtPwConstrainerJitterBufferSizeGet(AtPwConstrainer self, uint32_t pwId);

/* To apply valid combination of payload size and jitter buffer size */
void AtPwConstrainerJitterBufferAndPayloadSizeSet(AtPwConstrainer self, uint32_t pwId, uint32_t microseconds, uint32_t payloadSize);

/* =============================================================================
 * Small payload size can occupy more bandwidth because of PSN/MAC headers. To
 * prevent bandwidth overflow on a port, the following APIs are used to
 * constrain base on port in-used bandwidth. All of following APIs will affect
 * to Min/Max payload size.
 * =============================================================================*/

/* Need to know port speed */
void AtPwConstrainerPwEthPortTypeSet(AtPwConstrainer self, uint32_t ethPortId, eAtPwConstrainerEthPortType portType);
eAtPwConstrainerEthPortType AtPwConstrainerPwEthPortTypeGet(AtPwConstrainer self, uint32_t ethPortId);

/* Need to know port gap */
void AtPwConstrainerEthPortTxIpgSet(AtPwConstrainer self, uint32_t ethPortId, uint8_t portTxGap);
uint8_t AtPwConstrainerEthPortTxIpgGet(AtPwConstrainer self, uint32_t ethPortId);

/* Need to know MAC/PSN header size to constrain the MTU */
void AtPwConstrainerPwEthHeaderLengthSet(AtPwConstrainer self, uint32_t pwId, uint32_t numBytes);
uint32_t AtPwConstrainerPwEthHeaderLengthGet(AtPwConstrainer self, uint32_t pwId);
void AtPwConstrainerPwPsnLengthSet(AtPwConstrainer self, uint32_t pwId, uint32_t numBytes);
uint32_t AtPwConstrainerPwPsnLengthGet(AtPwConstrainer self, uint32_t pwId);

/* Assign PW to Ethernet Port */
void AtPwConstrainerPwEthPortSet(AtPwConstrainer self, uint32_t pwId, uint32_t ethPortId);
uint32_t AtPwConstrainerPwEthPortGet(AtPwConstrainer self, uint32_t pwId);

/* Need to know if RTP is enabled */
void AtPwConstrainerPwRtpEnable(AtPwConstrainer self, uint32_t pwId, uint8_t enable);
uint8_t AtPwConstrainerPwRtpIsEnabled(AtPwConstrainer self, uint32_t pwId);

/* In DS3 card, queue is specified */
void AtPwConstrainerPwQueueSet(AtPwConstrainer self, uint32_t pwId, uint32_t queueId); /* DS3 only */
uint32_t AtPwConstrainerPwQueueGet(AtPwConstrainer self, uint32_t pwId);               /* DS3 only */

/* OCN/DS3 only */
void AtPwConstrainerPwLopsThresholdSet(AtPwConstrainer self, uint32_t pwId, uint32_t lopsThreshold);
uint32_t AtPwConstrainerPwLopsThresholdGet(AtPwConstrainer self, uint32_t pwId);

/* For testing only */
uint32_t AtPwConstrainerEthPortBandwidthInKbpsGet(AtPwConstrainer self, uint32_t portId);
uint32_t AtPwConstrainerEthPortQueueBandwidthInBpsGet(AtPwConstrainer self, uint32_t portId, uint32_t queueId);
uint32_t AtPwConstrainerMaxNumEthPort(AtPwConstrainer self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPWCONSTRAINER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APP
 *
 * File        : AtZtePtn.c
 *
 * Created Date: Jul 8, 2013
 *
 * Description : To work with ZTE PTN
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtZtePtnInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool SdhLineSerdesControllersAreAllGood(AtDevice self)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(self, cAtModuleSdh);
    uint8 line_i;

    for (line_i = 0; line_i < AtModuleSdhMaxLinesGet(sdhModule); line_i++)
        {
        AtSdhLine line = AtModuleSdhLineGet(sdhModule, line_i);
        AtSerdesController serdes = AtSdhLineSerdesController(line);
        if (serdes && !AtSerdesControllerPllIsLocked(serdes))
            return cAtFalse;
        }

    return cAtTrue;
    }

static eBool EthPortSerdesControllersAreAllGood(AtDevice self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(self, cAtModuleEth);
    uint8 port_i;

    for (port_i = 0; port_i < AtModuleEthMaxPortsGet(ethModule); port_i++)
        {
        AtEthPort port = AtModuleEthPortGet(ethModule, port_i);
        AtSerdesController serdes = AtEthPortSerdesController(port);

        if (serdes && !AtSerdesControllerPllIsLocked(serdes))
            return cAtFalse;
        }

    return cAtTrue;
    }

eBool AtZtePtnTag2IsValid(const tAtZtePtnTag2 *tag2)
    {
    if (tag2 == NULL)
        return cAtFalse;

    if ((tag2->stmPortId > 7) || (tag2->zteChannelId > 255))
        return cAtFalse;

    return cAtTrue;
    }

eBool AtZtePtnTag1IsValid(const tAtZtePtnTag1 *tag1)
    {
    /* May be checked in the future if request */
    return tag1 ? cAtTrue : cAtFalse;
    }

tAtEthVlanTag *AtZtePtnCVlanFromTag2(const tAtZtePtnTag2 *tag2, tAtEthVlanTag *cVlan)
    {
    if ((cVlan == NULL) || (tag2 == NULL))
        return NULL;

    if (!AtZtePtnTag2IsValid(tag2))
        return NULL;

    cVlan->cfi      =  tag2->cfi;
    cVlan->priority =  tag2->priority;
    cVlan->vlanId   = (uint16)((tag2->stmPortId << 9) | ((tag2->isMlpppIma ? 1 : 0) << 8) | tag2->zteChannelId);

    return cVlan;
    }

tAtZtePtnTag1 *AtZtePtnTag1FromSVlan(const tAtEthVlanTag *sVlan, tAtZtePtnTag1 *tag1)
    {
    if ((sVlan == NULL) || (tag1 == NULL))
        return NULL;

    if (!AtZtePtnTag1IsValid(tag1))
        return NULL;

    tag1->cfi             =  sVlan->cfi;
    tag1->priority        =  sVlan->priority;
    tag1->cpuPktIndicator = (sVlan->vlanId >> 11) & cBit0;
    tag1->encapType       = (sVlan->vlanId >> 6)  & cBit4_0;
    tag1->packetLength    = (sVlan->vlanId)       & cBit5_0;

    return tag1;
    }

tAtZtePtnTag2 *AtZtePtnTag2FromCVlan(const tAtEthVlanTag *cVlan, tAtZtePtnTag2 *tag2)
    {
    if ((cVlan == NULL) || (tag2 == NULL))
        return NULL;

    tag2->cfi          = cVlan->cfi;
    tag2->priority     = cVlan->priority;
    tag2->stmPortId    = (uint8)((cVlan->vlanId & cBit11_9) >> 9);
    tag2->isMlpppIma   = (cVlan->vlanId & cBit8) ? cAtTrue : cAtFalse;
    tag2->zteChannelId = cVlan->vlanId & cBit7_0;

    return tag2;
    }

tAtEthVlanTag *AtZtePtnSVlanFromTag1(const tAtZtePtnTag1 *zteTag1, tAtEthVlanTag *vlanTag)
    {
    if ((vlanTag != NULL) && (zteTag1 != NULL))
        {
        vlanTag->cfi      = zteTag1->cfi;
        vlanTag->priority = zteTag1->priority;
        vlanTag->vlanId   = (uint16)((zteTag1->packetLength & cBit5_0)     |
                                     ((zteTag1->encapType & cBit4_0) << 6) |
                                     ((zteTag1->cpuPktIndicator & cBit0) << 11));
        return vlanTag;
        }
    else
        return NULL;
    }

eAtZtePtnClockStatus AtZtePtnClockCheck(AtDevice self)
    {
    AtModuleClock clockModule = (AtModuleClock)AtDeviceModuleGet(self, cAtModuleClock);
    return AtModuleClockAllClockCheck(clockModule);
    }

eBool AtZtePtnRamIsGood(AtDevice self)
    {
    AtModuleRam ramModule = (AtModuleRam)AtDeviceModuleGet(self, cAtModuleRam);
    uint8 ddrId;

    for (ddrId = 0; ddrId < AtModuleRamNumDdrGet(ramModule); ddrId++)
        {
        AtRam ddr = AtModuleRamDdrGet(ramModule, ddrId);
        if (!AtRamIsGood(ddr))
            return cAtFalse;
        }

    return cAtTrue;
    }

eBool AtZtePtnRamTested(AtDevice self)
    {
    AtModuleRam ramModule = (AtModuleRam)AtDeviceModuleGet(self, cAtModuleRam);
    uint8 ddrId;

    for (ddrId = 0; ddrId < AtModuleRamNumDdrGet(ramModule); ddrId++)
        {
        AtRam ddr = AtModuleRamDdrGet(ramModule, ddrId);
        if (!AtRamIsTested(ddr))
            return cAtFalse;
        }

    return cAtTrue;
    }

eBool AtZtePtnInterfaceIsGood(AtDevice self)
    {
    return SdhLineSerdesControllersAreAllGood(self) && EthPortSerdesControllersAreAllGood(self);
    }

eBool AtZtePtnFpgaLogicIsGood(AtDevice self)
    {
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    uint32 productCode;
    uint32 predefinedProductCodes[] = {0x60031031,
                                       0x60031032,
                                       0x60031035,
                                       0x60031021,
                                       0x60011023,
                                       0x60091023,
                                       0x60091132,
                                       0x65031032};
    uint32 valuesToTry[] = {0xAAAAAAAA, 0x55555555};
    uint32 value_i, code_i;
    uint32 cProductCodeReg = 0x0;

    /* Try to change product code register, if FPGA logic works well, this
     * register must not be changed */
    for (value_i = 0; value_i < mCount(valuesToTry); value_i++)
        {
        eBool isValidCode = cAtFalse;

        /* Write any value to product code register */
        AtHalWrite(hal, cProductCodeReg, valuesToTry[value_i]);

        /* Make sure that it will never be changed */
        isValidCode = cAtFalse;
        productCode = AtHalRead(hal, cProductCodeReg);
        for (code_i = 0; code_i < mCount(predefinedProductCodes); code_i++)
            {
            if (productCode == predefinedProductCodes[code_i])
                {
                isValidCode = cAtTrue;
                break;
                }
            }

        /* Logic does not work, so this register is affected by read/write operation */
        if (!isValidCode)
            return cAtFalse;
        }

    return cAtTrue;
    }

eAtRet AtZtePtnEthPortSwitch(AtEthPort self, AtEthPort toPort)
    {
    /* Need release history switch of all port */
    AtEthPortSwitchRelease(self);
    AtEthPortSwitchRelease(toPort);

    /* switch it */
    return AtEthPortSwitch(self, toPort);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ALU
 *
 * File        : AtAluSem.c
 *
 * Created Date: Mar 17, 2015
 *
 * Description : ALU Soft Error Mitigation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtAlu.h"
#include "AtDevice.h"
#include "commacro.h"
#include "AtSemController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/**
 * @addtogroup AtCustomer
 * @{
 */

static eAtSemValidateRet SemValidateReturnCode(eAtRet ret)
    {
    if (ret == cAtOk)                    return cAtSemValidateCorrected;
    if (ret == cAtErrorSemNotActive)     return cAtSemValidateNotActive;
    if (ret == cAtErrorSemFsmFailed)     return cAtSemValidateFsmFailed;
    if (ret == cAtErrorSemFatalError)    return cAtSemValidateFatalError;
    if (ret == cAtErrorSemUncorrectable) return cAtSemValidateUncorrectable;

    return cAtSemValidateFatalError;
    }

static AtSemController SemController(AtDevice device)
    {
    return AtDeviceSemControllerGet(device);
    }

/**
 * Validate Soft Error Mitigation function (SEM)
 *
 * @param device Device
 * @return SEM validation result. If returned value is cAtSemValidateCorrected, it means SEM function is working normally,
 * inserted error has been corrected. Otherwise, depend on returned value, SEM may have one of following several malfunction,
 * an FPGA download may be required
 *      cAtSemValidateNotActive     : SEM function is not active
 *      cAtSemValidateFsmFailed     : SEM state machine does not work correctly
 *      cAtSemValidateFatalError    : SEM function has fatal error and can not work properly
 *      cAtSemValidateUncorrectable : SEM can not correct inserted CRC error
 */
eAtSemValidateRet AtAluDeviceSemValidate(AtDevice device)
    {
    return SemValidateReturnCode(AtSemControllerValidate(SemController(device)));
    }

/**
 * Get Soft Error Mitigation error counter
 *
 * @param device Device
 * @return SEM error counter
 */
uint8 AtAluDeviceSemErrorCounterGet(AtDevice device)
    {
    return AtSemControllerCorrectedErrorCounterGet(SemController(device));
    }

/**
 * Get Soft Error Mitigation status
 *
 * @param device Device
 * @return SEM status. If returned value is 0, it means SEM function is not supported. If return value is greater than 0
 * it can be a bit mask of several status.
 * - @ref eAtSemStatus "SEM status"
 */
uint8 AtAluDeviceSemStatusGet(AtDevice device)
    {
    return (uint8)AtSemControllerStatusGet(SemController(device));
    }

/**
 * Get Soft Error Mitigation alarm
 *
 * @param device Device
 * @return SEM status. If returned value is 0, it means SEM function has no alarm. If return value is greater than 0
 * it can be a bit mask of several alarm.
 * - @ref eAtSemAlarm "SEM alarm type"
 */
uint8 AtAluDeviceSemAlarmGet(AtDevice device)
    {
    return (uint8)AtSemControllerAlarmHistoryGet(SemController(device));
    }

/**
* Clear Soft Error Mitigation alarm
*
* @param device Device
* @return SEM status. If returned value is 0, it means SEM function has no alarm. If return value is greater than 0
* it can be a bit mask of several alarm.
* - @ref eAtSemAlarm "SEM alarm type"
*/
uint8 AtAluDeviceSemAlarmClear(AtDevice device)
    {
    return (uint8)AtSemControllerAlarmHistoryClear(SemController(device));
    }

/**
 * @}
 */



/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : AtCienaKbyteChannel.c
 *
 * Created Date: Jun 11, 2018
 *
 * Description : K-Byte channel over SGMII
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../driver/src/generic/man/AtDriverInternal.h"
#include "AtPwKbyteChannel.h"
#include "AtCienaInternal.h"
#include "AtCienaKbyteChannel.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 LabelValidInRange(uint16 label)
    {
    return (label <= cBit11_0) ? cAtTrue : cAtFalse;
    }

static eAtRet KByteChannelExpectedLabelSet(AtChannel self, uint16 label)
    {
    if (LabelValidInRange(label))
        return AtPwKbyteChannelExpectedLabelSet((AtPwKbyteChannel)self, label);
    return cAtErrorOutOfRangParm;
    }

static eAtRet KByteChannelTxLabelSet(AtChannel self, uint16 label)
    {
    if (LabelValidInRange(label))
        return AtPwKbyteChannelTxLabelSet((AtPwKbyteChannel)self, label);
    return cAtErrorOutOfRangParm;
    }

/**
 * @addtogroup AtCiena
 * @{
 */

/**
 * Get Faceplate SDH line
 *
 * @param self The K-Byte channel
 *
 * @return Associated faceplate SDH line
 */
AtSdhLine AtCienaKbyteChannelLineGet(AtChannel self)
    {
    return AtPwKbyteChannelSdhLineGet((AtPwKbyteChannel)self);
    }

/**
 * Get K-byte PW
 *
 * @param self The K-Byte channel
 *
 * @return K-byte PW
 */
AtPw AtCieanKbyteChannelPwGet(AtChannel self)
    {
    return AtPwKbyteChannelKbytePwGet((AtPwKbyteChannel)self);
    }

/**
 * Enable/disable K-Byte validation to transmit frame immediately upon newly
 * validated K bytes or Extended K-bytes
 *
 * @param self The K-Byte channel
 * @param enable Enabling
 *
 * @return AT return code
 */
eAtRet AtCienaKbyteChannelValidationEnable(AtChannel self, eBool enable)
    {
    mOneParamWrapCall(enable, eAtRet, AtPwKbyteChannelValidationEnable((AtPwKbyteChannel)self, enable));
    }

/**
 * Check if K-Byte validation is enabled
 *
 * @param self The K-Byte channel
 *
 * @retval cAtTrue if validation is enabled
 * @retval cAtFalse if validation if disabled
 */
eBool AtCienaKbyteChannelValidationIsEnabled(AtChannel self)
    {
    return AtPwKbyteChannelValidationIsEnabled((AtPwKbyteChannel)self);
    }

/**
 * Enable/disable K-Byte Alarm-Suppression
 *
 * @param self The K-Byte channel
 * @param enable Enabling. When disable K-Byte Insertion not filter RDL-I
 *               and AIS-L with receiving from ETH pkt.
 *
 * @return AT return code
 */
eAtRet AtCienaKbyteChannelSuppressionEnable(AtChannel self, eBool enable)
    {
    mOneParamWrapCall(enable, eAtRet, AtPwKbyteChannelAlarmSuppressionEnable((AtPwKbyteChannel)self, enable));
    }

/**
 * Check if K-Byte Alarm-Suppression is enabled or not.
 *
 * @param self The K-Byte channel
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtCienaKbyteChannelSuppressionIsEnabled(AtChannel self)
    {
    return AtPwKbyteChannelAlarmSuppressionIsEnabled((AtPwKbyteChannel)self);
    }

/**
 * Enable/disable K-Byte overriding
 *
 * @param self The K-Byte channel
 * @param enable Enabling. If enable K-Byte on ETH frame will get from CPU
 *
 * @return AT return code
 * @see AtCienaKBytePwChannelOverrideK1Set()/AtCienaKBytePwChannelOverrideK2Set()
 */
eAtRet AtCienaKbyteChannelOverrideEnable(AtChannel self, eBool enable)
    {
    mOneParamWrapCall(enable, eAtRet, AtPwKbyteChannelOverrideEnable((AtPwKbyteChannel)self, enable));
    }

/**
 * Check if K-Byte Value is overridden.
 *
 * @param self The K-Byte channel
 *
 * @retval cAtTrue  if overriding is enabled
 * @retval cAtFalse if overriding is disabled
 */
eBool AtCienaKbyteChannelOverrideIsEnabled(AtChannel self)
    {
    return AtPwKbyteChannelOverrideIsEnabled((AtPwKbyteChannel)self);
    }

/**
 * Set K1 overridden value
 *
 * @param self The K-Byte channel
 * @param value K1 overridden value.
 *
 * @return AT return code
 */
eAtRet AtCienaKbyteChannelTxK1Set(AtChannel self, uint8 value)
    {
    mOneParamWrapCall(value, eAtRet, AtPwKbyteChannelTxK1Set((AtPwKbyteChannel)self, value));
    }

/**
 * Get K1 overridden value
 *
 * @param self The K-Byte channel
 *
 * @return K1 overridden value.
 */
uint8 AtCienaKbyteChannelTxK1Get(AtChannel self)
    {
    return AtPwKbyteChannelTxK1Get((AtPwKbyteChannel)self);
    }

/**
 * Set K2 overridden value
 *
 * @param self The K-Byte channel
 * @param value K2 overridden value
 *
 * @return AT return code
 */
eAtRet AtCienaKbyteChannelTxK2Set(AtChannel self, uint8 value)
    {
    mOneParamWrapCall(value, eAtRet, AtPwKbyteChannelTxK2Set((AtPwKbyteChannel)self, value));
    }

/**
 * Get K2 overridden value
 *
 * @param self The K-Byte PW
 *
 * @return K2 overridden value
 */
uint8 AtCienaKbyteChannelTxK2Get(AtChannel self)
    {
    return AtPwKbyteChannelTxK2Get((AtPwKbyteChannel)self);
    }

/**
 * Set EK1 overridden value
 *
 * @param self The K-Byte channel
 * @param value EK1 overridden value
 *
 * @return AT return code
 */
eAtRet AtCienaKbyteChannelTxEK1Set(AtChannel self, uint8 value)
    {
    mOneParamWrapCall(value, eAtRet, AtPwKbyteChannelTxEK1Set((AtPwKbyteChannel)self, value));
    }

/**
 * Get EK1 overridden value
 *
 * @param self The K-Byte channel
 *
 * @return EK1 overridden value
 */
uint8 AtCienaKbyteChannelTxEK1Get(AtChannel self)
    {
    return AtPwKbyteChannelTxEK1Get((AtPwKbyteChannel)self);
    }

/**
 * Set EK2 overridden value
 *
 * @param self The K-Byte channel
 * @param value EK2 overridden value
 *
 * @return AT return code
 */
eAtRet AtCienaKbyteChannelTxEK2Set(AtChannel self, uint8 value)
    {
    mOneParamWrapCall(value, eAtRet, AtPwKbyteChannelTxEK2Set((AtPwKbyteChannel)self, value));
    }

/**
 * Get EK2 overridden value
 *
 * @param self The K-Byte channel
 *
 * @return EK2 overridden value
 */
uint8 AtCienaKbyteChannelTxEK2Get(AtChannel self)
    {
    return AtPwKbyteChannelTxEK2Get((AtPwKbyteChannel)self);
    }

/**
 * Set expected value of the upper 12 bits in the received channel ID of the APS relay message.
 * Mismatches are not accepted and flag an interrupt.
 *
 * @param self The K-Byte channel
 * @param label the 12Bits value of channel Id[15:4]
 *
 * @return AT return code
 */
eAtRet AtCienaKbyteChannelExpectedLabelSet(AtChannel self, uint16 label)
    {
    mOneParamWrapCall(label, eAtRet, KByteChannelExpectedLabelSet(self, label));
    }

/**
 * Get expected value of the upper 12 bits in the received channel ID of the APS relay message.
 *
 * @param self The K-Byte channel
 *
 * @return the 12Bits value of channel Id[15:4]
 */
uint16 AtCienaKbyteChannelExpectedLabelGet(AtChannel self)
    {
    return AtPwKbyteChannelExpectedLabelGet((AtPwKbyteChannel)self);
    }

/**
 * Set the value of the upper 12 bits for transmit the APS relay message.
 *
 * @param self The K-Byte channel
 * @param label the 12Bits value of channel Id[15:4]
 *
 * @return AT return code
 */
eAtRet AtCienaKbyteChannelTxLabelSet(AtChannel self, uint16 label)
    {
    mOneParamWrapCall(label, eAtRet, KByteChannelTxLabelSet(self, label));
    }

/**
 * Get the transmitted value of the upper 12 bits in the transmitting APS relay message.
 *
 * @param self The K-Byte channel
 *
 * @return the 12Bits value of channel Id[15:4]
 */
uint16 AtCienaKbyteChannelTxLabelGet(AtChannel self)
    {
    return AtPwKbyteChannelTxLabelGet((AtPwKbyteChannel)self);
    }

/**
 * Get received K1 value in K-Byte channel
 *
 * @param self The K-Byte channel
 *
 * @return Received K1 value
 */
uint8 AtCienaKbyteChannelRxK1Get(AtChannel self)
    {
    return AtPwKbyteChannelRxK1Get((AtPwKbyteChannel)self);
    }

/**
 * Get received K2 value in K-Byte channel
 *
 * @param self The K-Byte channel
 *
 * @return Received K2 value
 */
uint8 AtCienaKbyteChannelRxK2Get(AtChannel self)
    {
    return AtPwKbyteChannelRxK2Get((AtPwKbyteChannel)self);
    }

/**
 * Get received Extended K1 value in K-Byte channel
 *
 * @param self The K-Byte channel
 *
 * @return Extended K1 value
 */
uint8 AtCienaKbyteChannelRxEK1Get(AtChannel self)
    {
    return AtPwKbyteChannelRxEK1Get((AtPwKbyteChannel)self);
    }

/**
 * Get received Extended K2 value in K-Byte channel
 *
 * @param self The K-Byte channel
 *
 * @return Extended K2 value
 */
uint8 AtCienaKbyteChannelRxEK2Get(AtChannel self)
    {
    return AtPwKbyteChannelRxEK2Get((AtPwKbyteChannel)self);
    }

/**
 * @}
 */

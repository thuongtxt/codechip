/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Customer
 * 
 * File        : AtCienaInternal.h
 * 
 * Created Date: Jan 23, 2017
 *
 * Description : Ciena specific internal declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCIENAINTERNAL_H_
#define _ATCIENAINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCiena.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtModuleEthRet AtCienaEthPortSgmiiInterruptMaskSet(AtEthPort self, uint32 defectMask, uint32 enableMask);
uint32 AtCienaEthPortSgmiiInterruptMaskGet(AtEthPort self);

/* Excessive error ratio */
eAtRet AtCienaEthPortExcessiveErrorRatioUpperThresholdSet(AtEthPort self, uint32 threshold);
uint32 AtCienaEthPortExcessiveErrorRatioUpperThresholdGet(AtEthPort self);
uint32 AtCienaEthPortExcessiveErrorRatioUpperThresholdMax(AtEthPort self);
eAtRet AtCienaEthPortExcessiveErrorRatioLowerThresholdSet(AtEthPort self, uint32 threshold);
uint32 AtCienaEthPortExcessiveErrorRatioLowerThresholdGet(AtEthPort self);
uint32 AtCienaEthPortExcessiveErrorRatioLowerThresholdMax(AtEthPort self);
eBool  AtCienaEthPortExcessiveErrorRatioThresholdSupported(AtEthPort self);
uint16 AtCienaDccKByteSgmiiCounterTypeToThaCounterType(eAtCienaSgmiiEthPortCounterType counterType);

#ifdef __cplusplus
}
#endif
#endif /* _ATCIENAINTERNAL_H_ */


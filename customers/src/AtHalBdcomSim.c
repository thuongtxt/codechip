/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APP
 *
 * File        : AtHalBdcomSim.c
 *
 * Created Date: Dec 10, 2013
 *
 * Description : BDCOM HAL simulation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "AtHalBdcom.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_memory[256];

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 BdcomFpgaRead(uint8 slot, uint16 reg)
    {
    AtUnused(slot);
    return m_memory[reg & cBit7_0];
    }

static uint8 BdcomFpgaWrite(uint8 slot, uint16 reg, uint8 value)
    {
    AtUnused(slot);
    m_memory[reg & cBit7_0] = value;
    return value;
    }

static void ClearMemory(void)
    {
    AtOsalMemInit(m_memory, 0, sizeof(m_memory));
    }

AtHal AtHalBdcomSimNew(uint8 slot, uint8 page)
    {
    ClearMemory();
    return AtHalBdcomNew(slot, page, BdcomFpgaRead, BdcomFpgaWrite);
    }

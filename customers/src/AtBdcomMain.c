/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APP
 *
 * File        : main.c
 *
 * Created Date: Oct 9, 2012
 *
 * Description : Sample source code to bring up driver. For the sake of simplicity,
 *               and make it be easy to understand, this sample code is to
 *               handle driver with one device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtDriver.h"
#include "AtCliModule.h"
#include "AtBdcom.h"
#include "AtHalBdcom.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* For simulation. The m_isSimulate must be set to cAtTrue in order to run in
 * simulation mode, the m_simulateProductCode variable must also be set to
 * correct product code of the device want to simulate */
static eBool m_isSimulate          = cAtTrue;
static const uint32 m_simulateProductCode = 0x60060011;

/* Just for demo purpose */
static const uint8 m_maxNumDevices = 2;

/*--------------------------- Forward declarations ---------------------------*/
/* BDCOM specific functions */
extern uint8 fpgaex_read_fpga(int slot, uint16 reg);
extern int fpgaex_write_fpga(int slot, uint16 reg, uint8 value);

extern AtHal AtHalBdcomSimNew(uint8 slot, uint8 page);

/*--------------------------- Implementation ---------------------------------*/
static AtHal CreateHal(uint8 slotId, uint8 pageId)
    {
    if (m_isSimulate)
        return AtHalBdcomSimNew(slotId, pageId);

    return AtHalBdcomNew(slotId, pageId, (FpgaRead)fpgaex_read_fpga, (FpgaWrite)fpgaex_write_fpga);
    }

static uint32 ReadProductCode(uint8 slotId, uint8 pageId)
    {
    uint32 productCode;
    AtHal hal;

    if (m_isSimulate)
        return m_simulateProductCode;

    hal = CreateHal(slotId, pageId);
    productCode = AtProductCodeGetByHal(hal);
    AtHalDelete(hal);

    if (!AtDriverProductCodeIsValid(AtDriverSharedDriverGet(), productCode))
        AtPrintc(cSevCritical, "ERROR: product code 0x%08x is invalid\r\n", productCode);

    return productCode;
    }

static eAtRet AddDevice(uint8 slot, uint8 page)
    {
    AtDevice newDevice;
    uint32 productCode;
    eAtRet ret = cAtOk;

    /* Create driver if it has not been */
    if (AtDriverSharedDriverGet() == NULL)
        AtDriverCreate(m_maxNumDevices, AtOsalBdcom());

    /* Create and setup this device */
    productCode = ReadProductCode(slot, page);
    newDevice = AtDriverDeviceCreate(AtDriverSharedDriverGet(), productCode);
    if (newDevice == NULL)
        {
        AtPrintc(cSevCritical, "ERROR: Add device with product code 0x%08x fail. This code may be invalid\r\n", productCode);
        return cAtErrorDevFail;
        }

    /* And setup it */
    AtIpCoreHalSet(AtDeviceIpCoreGet(newDevice, 0), CreateHal(slot, page));

    /* Optional: initialize device */
    ret = AtDeviceInit(newDevice);

    return ret;
    }

static eAtRet DeleteDevice(AtDevice device)
    {
    AtHal hal;

    if (device == NULL)
        return cAtOk;

    hal = AtDeviceIpCoreHalGet(device, 0);
    AtDriverDeviceDelete(AtDriverSharedDriverGet(), device);
    AtHalDelete(hal);

    return cAtOk;
    }

static eAtRet DriverCleanup(AtDriver driver)
    {
    AtDevice device;

    if (driver == NULL)
        return cAtOk;

    /* Delete all devices */
    while ((device = AtDriverDeviceGet(driver, 0)) != NULL)
        DeleteDevice(device);

    return cAtOk;
    }

int AtBdcomAddDevice(uint8 slotId, uint8 pageId)
    {
    eAtRet ret = AddDevice(slotId, pageId);
    if (ret != cAtOk)
        {
        AtPrintc(cSevCritical, "Cannot add device, ret = %s\r\n", AtRet2String(ret));
        return 1;
        }

    return 0;
    }

int AtBdcomCleanup(void)
    {
    eAtRet ret;

    ret = DriverCleanup(AtDriverSharedDriverGet());
    AtCliStop();
    AtDriverDelete(AtDriverSharedDriverGet());

    return (ret == cAtOk) ? 0 : 1;
    }

int AtBdComCliExecute(const char *cli)
    {
    return AtCliExecute(cli);
    }

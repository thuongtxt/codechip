/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : AtCisco.c
 *
 * Created Date: Mar 26, 2015
 *
 * Description : Specific APIs for Cisco projects
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCisco.h"

#include <AtCpld.h>

#include "../../driver/src/implement/codechip/Tha60210011/sdh/Tha60210011ModuleSdh.h"
#include "../../driver/src/implement/codechip/Tha60210012/eth/Tha60210012EXaui.h"
#include "../../driver/src/implement/codechip/Tha60210012/eth/Tha60210012ModuleEth.h"
#include "../../driver/src/implement/default/eth/ThaModuleEth.h"
#include "../../driver/src/implement/codechip/Tha60210061/pdh/Tha60210061ModulePdh.h"
#include "../../driver/src/implement/codechip/Tha60210061/physical/Tha60210061SerdesManager.h"
#include "../../driver/src/implement/codechip/Tha60210061/eth/Tha60210061ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/**
 * @addtogroup AtCisco
 * @{
 */

/**
 * Set mapping type for AU-VC with selected hardware STSs in low-order space.
 * FPGA provides 6 blocks OC-48 for low-order space, each block contains 48 STSs
 *
 * @param vc An AU-VC
 * @param mapType Mapping type. @ref eAtSdhVcMapType "VC mapping type"
 * @param loSts Selected hardware STSs in low-oder space with flat ID from 0-287.
 *        This parameter can be NULL, if it is NULL, low-order STSs will be dynamic allocated
 * @param numLoSts Number of selected STSs, if loSts is NULL, this parameter will be ignored
 * @return At return code
 */
eAtRet AtCiscoSdhAuVcMapTypeSet(AtSdhVc vc, uint8 mapType, uint16* loSts, uint8 numLoSts)
    {
    return Tha60210011Tfi5LineAuVcMapTypeSet((AtSdhChannel)vc, mapType, loSts, numLoSts);
    }

/**
 * Set active group for XFI SERDES in 10G OCN card XFI.
 * There are 2 groups:0 and 1 group#0 include SERDES 0,1,4,5; group#1 include
 * SERDES 2,3,6,7
 *
 * @param ethModule Ethernet module
 * @param groupId XFI group identifier, value 0 or 1
 *
 * @return At return code
 */
eAtRet AtCiscoModuleEthXfiGroupSet(AtModuleEth ethModule, uint32 groupId)
    {
    return Tha60210051ModuleEthXfiGroupSet(ethModule, groupId);
    }

/**
 * Get current active group of XFI SERDES in 10G OCN card XFI.
 * There are 2 groups:0 and 1 group#0 include SERDES 0,2,4,6; group#1 include
 * SERDES 1,3,5,7
 *
 * @param self Ethernet module module
 *
 * @return Current active group
 */
uint32 AtCiscoModuleEthXfiGroupGet(AtModuleEth self)
    {
    return Tha60210051ModuleEthXfiGroupGet(self);
    }

/**
 * To restore driver database after switching role from standby to active. This
 * API is used when driver sync mode is cAtDriverSyncModeNone. After switching
 * the driver role to active, application will keep calling this API until it
 * return 0 to rebuild necessary database by reading registers from hardware.
 *
 * @param self This driver
 *
 * @retval None zero if database has not been completely rebuilt
 * @retval Zero if database has been completely rebuilt
 */
uint32 AtDriverHaRestore(AtDriver self)
    {
    return AtDriverRestore(self);
    }

/**
 * Set VLAN to be inserted on direction from eXAUI to XFI.
 *
 * @param self This port
 * @param channelId [0..255] EoS channel
 * @param vlan @ref tAtVlan VLAN
 *
 * @return AT return code
 *
 * @note This API is only applicable for eXAUI#1.
 */
eAtRet AtExauiIngressVlanSet(AtEthPort self, uint32 channelId, const tAtVlan *vlan)
    {
    return Tha60210012EXauiIngressVlanSet(self, channelId, vlan);
    }

/**
 * Get VLAN that is inserted on direction from eXAUI to XFI.
 *
 * @param self This port
 * @param channelId [0..255] EoS channel
 * @param vlan [out] @ref tAtVlan VLAN
 *
 * @return AT return code
 *
 * @note This API is only applicable for eXAUI#1.
 */
eAtRet AtExauiIngressVlanGet(AtEthPort self, uint32 channelId, tAtVlan *vlan)
    {
    return Tha60210012EXauiIngressVlanGet(self, channelId, vlan);
    }

/**
 * Set expected VLAN ID used to lookup the input EoS channel ID
 *
 * @param self This port
 * @param channelId EoS channel
 * @param expectedVlanId Expected VLAN ID
 *
 * @return AT return code
 * @note This API is only applicable for eXAUI#1.
 */
eAtRet AtExauiEgressExpectedVlanIdSet(AtEthPort self, uint32 channelId, uint16 expectedVlanId)
    {
    return Tha60210012EXauiEgressExpectedVlanIdSet(self, channelId, expectedVlanId);
    }

/**
 * Get expected VLAN ID used to lookup the input EoS channel ID
 *
 * @param self This port
 * @param channelId EoS channel
 *
 * @return Expected VLAN ID used to lookup the input EoS channel ID
 */
uint16 AtExauiEgressExpectedVlanIdGet(AtEthPort self, uint32 channelId)
    {
    return Tha60210012EXauiEgressExpectedVlanIdGet(self, channelId);
    }

/**
 * Get number of eXAUIs
 *
 * @param self Module Ethernet
 *
 * @return Number of eXAUIs
 */
uint32 AtModuleEthNumExauiGet(AtModuleEth self)
    {
    return Tha60210012ModuleEthNumExauiGet(self);
    }

/**
 * Get eXAUI
 *
 * @param self Module Ethernet
 * @param exauiId eXAUI ID
 *
 * @return eXAUI if ID is valid. Otherwise, NULL is returned.
 */
AtEthPort AtModuleEthExauiGet(AtModuleEth self, uint32 exauiId)
    {
    return Tha60210012ModuleEthExauiGet(self, exauiId);
    }

/**
 * Get service type instance
 *
 * @param self Module Ethernet
 * @param serviceType Service type
 *
 * @return AtPtchService if Service type is valid. Otherwise, NULL is returned.
 */
AtPtchService AtModuleEthPtchServiceGet(AtModuleEth self, eAtPtchServiceType serviceType)
    {
    return Tha60210012ModuleEthPtchServiceGet(self, serviceType);
    }

/**
 * Get CPLD version
 *
 * @param self Module PDH
 * @return CPLD version
 */
uint32 AtCiscoModulePdhCpldVersion(AtModulePdh self)
    {
    AtCpld cpld = Tha60210061ModulePdhCpld(self);
    if (cpld == NULL)
        return 0;

    return AtCpldVersion(cpld);
    }

/**
 * Check if CPLD is downloaded or not
 *
 * @param self Module PDH
 * @return cAtTrue if CPLD has been downloaded
 *         cAtFalse if CPLD has not been downloaded
 */
eBool AtCiscoModulePdhCpldIsDownloaded(AtModulePdh self)
    {
    AtCpld cpld = Tha60210061ModulePdhCpld(self);
    if (cpld == NULL)
        return cAtFalse;

    return AtCpldIsDownloaded(cpld);
    }

/**
 * Configure DS1 loopback at CPLD
 *
 * @param self This module
 * @param loopbackMode Loopback mode
 * @return At return code
 */
eAtRet AtCiscoModulePdhCpldDs1LoopbackSet(AtModulePdh self, eAtLoopbackMode loopbackMode)
    {
    AtCpld cpld = Tha60210061ModulePdhCpld(self);
    if (cpld == NULL)
        return cAtErrorNullPointer;

    return AtCpldDs1LoopbackSet(cpld, loopbackMode);
    }

/**
 * Configure Destination MAC Address of frame for PRBS engine
 *
 * @param self PRBS engine object
 * @param mac DMAC Address
 *
 * @return At return code
 */
eAtRet AtCiscoPrbsEngineDestMacSet(AtPrbsEngine self, uint8* mac)
    {
    return Tha60210061SerdesPrbsEngineDestMacSet(self, mac);
    }

/**
 * Configure Source MAC Address of frame for PRBS engine
 *
 * @param self PRBS engine object
 * @param mac SMAC Address
 * @return At return code
 */
eAtRet AtCiscoPrbsEngineSrcMacSet(AtPrbsEngine self, uint8* mac)
    {
    return Tha60210061SerdesPrbsEngineSrcMacSet(self, mac);
    }

/**
 * Configure Ethernet type of frame for PRBS engine
 *
 * @param self PRBS engine object
 * @param ethType Ethernet type
 * @return At return code
 */
eAtRet AtCiscoPrbsEngineEthernetTypeSet(AtPrbsEngine self, uint16 ethType)
    {
    return Tha60210061SerdesPrbsEngineEthernetTypeSet(self, ethType);
    }

/**
 * Set egress destination MAC address
 *
 * @param module Ethernet module
 * @param destMac Pointer to input MAC address array
 *
 * @return AT return code
 */
eAtRet AtCiscoControlFlowEgressDestMacSet(AtModuleEth module, const uint8 *destMac)
    {
    return Tha60210012ModuleEthOamFlowEgressDestMacSet(module, destMac);
    }

/**
 * Get egress destination MAC address
 *
 * @param module Ethernet module
 * @param destMac Pointer to output MAC address array
 *
 * @return AT return code
 */
eAtRet AtCiscoControlFlowEgressDestMacGet(AtModuleEth module, uint8 *destMac)
    {
    return Tha60210012ModuleEthOamFlowEgressDestMacGet(module, destMac);
    }

/**
 * Set egress source MAC address
 *
 * @param module This Ethernet module
 * @param srcMac Pointer to input MAC address array
 *
 * @return AT return code
 */
eAtRet AtCiscoControlFlowEgressSrcMacSet(AtModuleEth module, const uint8 *srcMac)
    {
    return Tha60210012ModuleEthOamFlowEgressSrcMacSet(module, srcMac);
    }

/**
 * Get egress source MAC address
 *
 * @param module This Ethernet module
 * @param srcMac Pointer to output MAC address array
 *
 * @return AT return code
 */
eAtRet AtCiscoControlFlowEgressSrcMacGet(AtModuleEth module, uint8 *srcMac)
    {
    return Tha60210012ModuleEthOamFlowEgressSrcMacGet(module, srcMac);
    }

/**
 * Set egress ethernet type
 *
 * @param module This Ethernet module
 * @param ethType Ethernet type value
 *
 * @return AT return code
 */
eAtRet AtCiscoControlFlowEgressEthTypeSet(AtModuleEth module, uint16 ethType)
    {
    return Tha60210012ModuleEthOamFlowEgressEthTypeSet(module, ethType);
    }

/**
 * Get egress Ethernet type
 *
 * @param module This Ethernet module
 *
 * @return Ethernet type value
 */
uint16 AtCiscoControlFlowEgressEthTypeGet(AtModuleEth module)
    {
    return Tha60210012ModuleEthOamFlowEgressEthTypeGet(module);
    }

/**
 * Set Control-VLAN at Egress direction
 *
 * @param module This Ethernet module
 * @param vlan Control-VLAN
 *
 * @return AT return code
 */
eAtRet AtCiscoControlFlowEgressControlVlanSet(AtModuleEth module, const tAtVlan* vlan)
    {
    return Tha60210012ModuleEthOamFlowEgressControlVlanSet(module, vlan);
    }

/**
 * Get Egress Control-VLAN
 *
 * @param module This Ethernet module
 * @param vlan Control-VLAN
 *
 * @return AT return code
 */
eAtRet AtCiscoControlFlowEgressControlVlanGet(AtModuleEth module, tAtVlan* vlan)
    {
    return Tha60210012ModuleEthOamFlowEgressControlVlanGet(module, vlan);
    }

/**
 * Set Control-VLAN at Ingress direction
 *
 * @param module This Ethernet module
 * @param vlan Control-VLAN
 *
 * @return AT return code
 */
eAtRet AtCiscoControlFlowIngressControlVlanSet(AtModuleEth module, const tAtVlan* vlan)
    {
    return Tha60210012ModuleEthOamFlowIngressControlVlanSet(module, vlan);
    }

/**
 * Get Ingress Control-VLAN
 *
 * @param module This Ethernet module
 * @param vlan Control-VLAN
 *
 * @return AT return code
 */
eAtRet AtCiscoControlFlowIngressControlVlanGet(AtModuleEth module, tAtVlan* vlan)
    {
    return Tha60210012ModuleEthOamFlowIngressControlVlanGet(module, vlan);
    }

/**
 * Set TPID for Circuit Vlan at Egress direction
 *
 * @param module This Ethernet module
 * @param tpid TPID for Circuit Vlan
 *
 * @return AT return code
 */
eAtRet AtCiscoControlFlowEgressCircuitVlanTPIDSet(AtModuleEth module, uint16 tpid)
    {
    return Tha60210012ModuleEthOamFlowEgressCircuitVlanTPIDSet(module, tpid);
    }

/**
 * Get configure TPID of Circuit Vlan at Egress direction
 *
 * @param module This Ethernet module
 *
 * @return TPID of Circuit Vlan
 */
uint16 AtCiscoControlFlowEgressCircuitVlanTPIDGet(AtModuleEth module)
    {
    return Tha60210012ModuleEthOamFlowEgressCircuitVlanTPIDGet(module);
    }

/**
 * Get GE bypass PTCH service
 *
 * @param module Module Ethernet
 * @param gePort GE port
 * @return PTCH service
 */
AtPtchService AtModuleEthGeBypassPtchServiceGet(AtModuleEth module, AtEthPort gePort)
    {
    return Tha60210061ModuleEthGeBypassPtchServiceGet(module, gePort);
    }

/**
 * Set bypass port for GE port
 *
 * @param gePort GE port
 * @param bypassPort Ethernet port bypass
 * @return AT return code
 */
eAtRet AtCiscoGePortBypassPortSet(AtEthPort gePort, AtEthPort bypassPort)
    {
    return Tha60210061GePortBypassPortSet(gePort, bypassPort);
    }

/**
 * Get bypass port of GE port
 * @param gePort GE port
 * @return Port bypass
 */
AtEthPort AtCiscoGePortBypassPortGet(AtEthPort gePort)
    {
    return Tha60210061GePortBypassPortGet(gePort);
    }

/**
 * Enable Ethernet bypass traffic
 *
 * @param gePort GE port
 * @param enable bypass traffic enabling
 *               - cAtTrue: Enable
 *               - cAtFalse: Disable
 * @return AT return code
 */
eAtRet AtCiscoGePortBypassEnable(AtEthPort gePort, eBool enable)
    {
    return Tha60210061GePortBypassEnable(gePort, enable);
    }

/**
 * Check if Ethernet bypass traffic is enabled
 *
 * @param gePort GE port
 * @return cAtTrue if it is enabled or cAtFalse if it is disabled
 */
eBool AtCiscoGePortBypassIsEnabled(AtEthPort gePort)
    {
    return Tha60210061GePortBypassIsEnabled(gePort);
    }

/**
 * Set active group for CiscoIp XFI SERDES in 10G OCN card XFI.
 * There are 2 groups:2 and 3 group#2 include SERDES 8,9,12,13; group#3 include
 * SERDES 10,11,14,15
 *
 * @param ethModule Ethernet module
 * @param groupId XFI group identifier, value 2 or 3
 *
 * @return At return code
 */
eAtRet AtCiscoIpModuleEthXfiGroupSet(AtModuleEth ethModule, uint32 groupId)
    {
    return Tha60210051ModuleEthExtraXfiGroupSet(ethModule, groupId);
    }

/**
 * Get current active group of CiscoIp XFI SERDES in 10G OCN card XFI.
 * There are 2 groups:2 and 3 group#2 include SERDES 8,9,12,13; group#3 include
 * SERDES 10,11,14,15
 *
 * @param self Ethernet module module
 *
 * @return Current active group
 */
uint32 AtCiscoIpModuleEthXfiGroupGet(AtModuleEth self)
    {
    return Tha60210051ModuleEthExtraXfiGroupGet(self);
    }

/**
 * Set SONET/SDH path alarm mask that maps to SONET/SDH UPSR-SF
 *
 * @param self SONET/SDH module
 * @param alarmMask @ref eAtSdhPathAlarmType "SONET/SDH path alarm type"
 *
 * @return At return code
 */
eAtRet AtCiscoModuleSdhUpsrSfAlarmMaskSet(AtModuleSdh self, uint32 alarmMask)
    {
    return Tha60210011ModuleSdhUpsrSfAlarmMaskSet(self, alarmMask);
    }

/**
 * Get SONET/SDH path alarm mask that maps from SONET/SDH UPSR-SF
 *
 * @param self SONET/SDH module
 *
 * @return Path alarm mask @ref eAtSdhPathAlarmType "SONET/SDH path alarm type"
 */
uint32 AtCiscoModuleSdhUpsrSfAlarmMaskGet(AtModuleSdh self)
    {
    return Tha60210011ModuleSdhUpsrSfAlarmMaskGet(self);
    }

/**
 * Set SONET/SDH path alarm mask that maps to SONET/SDH UPSR-SD
 *
 * @param self SONET/SDH module
 * @param alarmMask @ref eAtSdhPathAlarmType "SONET/SDH path alarm type"
 *
 * @return At return code
 */
eAtRet AtCiscoModuleSdhUpsrSdAlarmMaskSet(AtModuleSdh self, uint32 alarmMask)
    {
    return Tha60210011ModuleSdhUpsrSdAlarmMaskSet(self, alarmMask);
    }

/**
 * Get SONET/SDH path alarm mask that maps from SONET/SDH UPSR-SD
 *
 * @param self SONET/SDH module
 *
 * @return Path alarm mask @ref eAtSdhPathAlarmType "SONET/SDH path alarm type"
 */
uint32 AtCiscoModuleSdhUpsrSdAlarmMaskGet(AtModuleSdh self)
    {
    return Tha60210011ModuleSdhUpsrSdAlarmMaskGet(self);
    }

/**
 * @}
 */

/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : UPSR
 *
 * File        : AtCiscoUpsrExample.c
 *
 * Created Date: Sep 22, 2015
 *
 * Description : UPSR interrupt process example.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCiscoUpsr.h"

/*--------------------------- Define -----------------------------------------*/
#ifndef NULL
#define NULL '\0'
#endif

/*--------------------------- Macros -----------------------------------------*/
#define cAtCiscoUpsrCellBitMask(cell)   (1U << (cell))

/*--------------------------- Local Typedefs ---------------------------------*/
typedef struct tCiscoUpsrListener
    {
    tAtCiscoUpsrListener callback;
    void* listener;
    } tCiscoUpsrListener;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static tCiscoUpsrListener upsrCallback = {{NULL, NULL}, NULL};

static at_uint32 stsSDHistoryTable[12] = {0};
static at_uint32 vtSDHistoryTable[384] = {0};
static at_uint32 stsSFHistoryTable[12] = {0};
static at_uint32 vtSFHistoryTable[384] = {0};

/*--------------------------- Forward declaration ----------------------------*/
void AtCiscoUpsrHistoryTableFlush(void);

/*--------------------------- Implementation ---------------------------------*/

static at_uint32 StsStatusChange(at_uint32 current, at_uint32 rowIndex, eAtCiscoUpsrAlarm table)
    {
    at_uint32 change;
    if (table == cAtCiscoUpsrAlarmSf)
        {
        change = stsSFHistoryTable[rowIndex] ^ current;
        stsSFHistoryTable[rowIndex] = current;
        return change;
        }

    if (table == cAtCiscoUpsrAlarmSd)
        {
        change = stsSDHistoryTable[rowIndex] ^ current;
        stsSDHistoryTable[rowIndex] = current;
        return change;
        }

    return 0;
    }

static at_uint32 VtStatusChange(at_uint32 current, at_uint32 rowIndex, eAtCiscoUpsrAlarm table)
    {
    at_uint32 change;
    if (table == cAtCiscoUpsrAlarmSf)
        {
        change = vtSFHistoryTable[rowIndex] ^ current;
        vtSFHistoryTable[rowIndex] = current;
        return change;
        }

    if (table == cAtCiscoUpsrAlarmSd)
        {
        change = vtSDHistoryTable[rowIndex] ^ current;
        vtSDHistoryTable[rowIndex] = current;
        return change;
        }
    return 0;
    }

static void InterruptProcess(AtCiscoUpsr self, at_uint32 MASRStatus, eAtCiscoUpsrAlarm table)
    {
    at_uint32 stsRow;

    for (stsRow = 0; stsRow < 12; stsRow++)
        {
        if (cAtCiscoUpsrCellBitMask(stsRow) & MASRStatus)
            {
            at_uint32 regVal = AtCiscoUpsrStsStatusRead(self, table, stsRow);
            at_uint32 intrVal = StsStatusChange(regVal, stsRow, table);
            at_uint32 column;

            for (column = 0; column < 32; column++)
                {
                if (cAtCiscoUpsrCellBitMask(column) & intrVal)
                    {
                    at_uint32 stsIndex = stsRow * 32 + column;
                    at_uint32 defect = 0;
                    at_uint32 event = table;
                    if (cAtCiscoUpsrCellBitMask(column) & regVal)
                        defect = table;

                    if (upsrCallback.callback.StsInterruptChanged)
                        upsrCallback.callback.StsInterruptChanged(AtCiscoUpsrBaseAddress(self), upsrCallback.listener, stsIndex, event, defect);
                    }
                }
            }
        }

    for (stsRow = 0; stsRow < 12; stsRow++)
        {
        if (cAtCiscoUpsrCellBitMask(stsRow + 12) & MASRStatus)
            {
            at_uint32 regVal = AtCiscoUpsrSASRVtRead(self, table, stsRow);
            at_uint32 sasr;

            for (sasr = 0; sasr < 32; sasr++)
                {
                if (cAtCiscoUpsrCellBitMask(sasr) & regVal)
                    {
                    at_uint32 vtRow = stsRow * 32 + sasr;
                    at_uint32 rowVal = AtCiscoUpsrVtStatusRead(self, table, vtRow);
                    at_uint32 intrVal = VtStatusChange(rowVal, vtRow, table);
                    at_uint32 column;

                    for (column = 0; column < 28; column++)
                        {
                        if (cAtCiscoUpsrCellBitMask(column) & intrVal)
                            {
                            at_uint32 defect = 0;
                            at_uint32 event = table;
                            if (cAtCiscoUpsrCellBitMask(column) & rowVal)
                                defect = table;

                            if (upsrCallback.callback.VtInterruptChanged)
                                upsrCallback.callback.VtInterruptChanged(AtCiscoUpsrBaseAddress(self), upsrCallback.listener, vtRow, column, event, defect);
                            }
                        }
                    }
                }
            }
        }
    }

void AtCiscoUpsrInterruptProcess(AtCiscoUpsr self)
    {
    eAtCiscoUpsrAlarm table = cAtCiscoUpsrAlarmSf;

    for (; table <= cAtCiscoUpsrAlarmSd; table <<= 1)
        {
        at_uint32 MASRVal = AtCiscoUpsrMASRStatusRead(self, table);
        if (MASRVal)
            InterruptProcess(self, MASRVal, table);
        }
    }

void AtCiscoUpsrListenerAdd(AtCiscoUpsr self, void *data, const tAtCiscoUpsrListener *callbacks)
    {
    (void)self;
    if (!upsrCallback.callback.StsInterruptChanged)
        upsrCallback.callback.StsInterruptChanged = callbacks->StsInterruptChanged;
    if (!upsrCallback.callback.VtInterruptChanged)
        upsrCallback.callback.VtInterruptChanged = callbacks->VtInterruptChanged;
    upsrCallback.listener = data;
    }

void AtCiscoUpsrListenerRemove(AtCiscoUpsr self, void *data, const tAtCiscoUpsrListener *callbacks)
    {
    (void)self;
    if (upsrCallback.callback.StsInterruptChanged == callbacks->StsInterruptChanged)
        upsrCallback.callback.StsInterruptChanged = NULL;

    if (upsrCallback.callback.VtInterruptChanged == callbacks->VtInterruptChanged)
        upsrCallback.callback.VtInterruptChanged = NULL;

    if (upsrCallback.listener == data)
        upsrCallback.listener = NULL;
    }

void AtCiscoUpsrHistoryTableFlush(void)
    {
    at_uint32 row;
    for (row = 0; row < 12; row++)
        {
        stsSDHistoryTable[row] = 0;
        stsSFHistoryTable[row] = 0;
        }
    for (row = 0; row < 384; row++)
        {
        vtSDHistoryTable[row] = 0;
        vtSFHistoryTable[row] = 0;
        }
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : AtZteRouter.c
 *
 * Created Date: Jul 8, 2014
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../include/AtZteRouter.h"
#include "AtEthFlow.h"
#include "AtPw.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool AtZteRouterTagIsValid(const tAtZteRouterTag *tag)
    {
    /* May be checked in the future if request */
    return tag ? cAtTrue : cAtFalse;
    }

/* C-VLAN contains ZTE tag */
static tAtEthVlanTag *AtZteRouterCVlanFromTag(const tAtZteRouterTag *tag, tAtEthVlanTag *cVlan)
    {
    if ((cVlan == NULL) || (tag == NULL))
        return NULL;

    if (!AtZteRouterTagIsValid(tag))
        return NULL;

    cVlan->priority = (uint8)(tag->portNumber | (tag->cpuPktIndicator << 4));
    cVlan->cfi      = (uint8)(tag->slotNumber | (tag->encapType << 4));
    cVlan->vlanId   = (uint16)((uint16)(tag->channelNumber << 6) | (uint16)(tag->packetLength & cBit5_0));

    return cVlan;
    }

static tAtZteRouterTag* AtZteRouterTagFromCVlan(const tAtEthVlanTag* cVlan, tAtZteRouterTag *tag)
    {
    if ((tag == NULL) || (cVlan == NULL))
        return NULL;

    tag->portNumber = cVlan->priority & cBit3_0;
    tag->cpuPktIndicator = (cVlan->priority >> 4) & cBit0;
    tag->slotNumber = cVlan->cfi & cBit3_0;
    tag->encapType = (cVlan->cfi >> 4) & cBit3_0;
    tag->packetLength = cVlan->vlanId & cBit5_0;
    tag->channelNumber = (cVlan->vlanId >> 6) & cBit9_0;

    return tag;
    }

/* S-VLAN contains ethernet type */
static tAtEthVlanTag *AtZteRouterSVlanFromEthType(uint16 ethType, tAtEthVlanTag *sVlan)
    {
    if (sVlan == NULL)
        return NULL;

    sVlan->vlanId = ethType;
    return sVlan;
    }

static uint16 AtZteRouterEthTypeFromSVlan(const tAtEthVlanTag *sVlan)
    {
    if (sVlan == NULL)
        return 0;

    return sVlan->vlanId;
    }

tAtZteRouterTag* AtZteRouterPwTagGet(AtPw self, tAtZteRouterTag *zteTag)
    {
    tAtEthVlanTag cVlan;

    if (AtPwEthCVlanGet(self, &cVlan) == NULL)
        return NULL;

    return AtZteRouterTagFromCVlan(&cVlan, zteTag);
    }

uint16 AtZteRouterPwEthTypeGet(AtPw self)
    {
    tAtEthVlanTag sVlan;

    if (AtPwEthSVlanGet(self, &sVlan) == NULL)
        return 0;

    return AtZteRouterEthTypeFromSVlan(&sVlan);
    }

eAtRet AtZteRouterPwHeaderSet(AtPw self, uint8 *destMac, uint16 ethType, const tAtZteRouterTag *zteTag)
    {
    tAtEthVlanTag cVlan, sVlan;

    if ((AtZteRouterCVlanFromTag(zteTag, &cVlan) == NULL) ||
        (AtZteRouterSVlanFromEthType(ethType, &sVlan) == NULL))
        return cAtErrorInvlParm;

    return AtPwEthHeaderSet(self, destMac, &cVlan, &sVlan);
    }

eAtRet AtZteRouterPwExpectedTagSet(AtPw self, tAtZteRouterTag *tag)
    {
    tAtEthVlanTag cVlan;

    if (AtZteRouterCVlanFromTag(tag, &cVlan) == NULL)
        return cAtErrorInvlParm;

    return AtPwEthExpectedCVlanSet(self, &cVlan);
    }

tAtZteRouterTag *AtZteRouterPwExpectedTagGet(AtPw self, tAtZteRouterTag *tag)
    {
    tAtEthVlanTag cVlan;
    return AtZteRouterTagFromCVlan(AtPwEthExpectedCVlanGet(self, &cVlan), tag);
    }

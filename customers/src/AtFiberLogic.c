/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Fiber Logic
 *
 * File        : AtFiberLogic.c
 *
 * Created Date: May 29, 2015
 *
 * Description : Fiber Logic implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <stdlib.h>
#include "atclib.h"
#include "AtCommon.h"
#include "AtFiberLogic.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eAtRet AtFiberLogicLiuInit(eAtFiberLogicLiuMode liuMode)
    {
    if (liuMode == cAtFiberLogicLiuModeE1)
        return (system("/root/liu_setup_E1") != 0) ? cAtErrorDevFail : cAtOk;

    if (liuMode == cAtFiberLogicLiuModeDs1)
        return (system("/root/liu_setup_T1") != 0) ? cAtErrorDevFail : cAtOk;

    return cAtErrorModeNotSupport;
    }

eAtRet AtFiberLogicLiuLoopbackSet(uint16 liuId, eAtLoopbackMode loopMode)
    {
    char cmdString[64];

    if (loopMode == cAtLoopbackModeLocal)
        return cAtErrorModeNotSupport;

    if (loopMode == cAtLoopbackModeRemote)
        {
        AtSprintf(cmdString, "/root/liu_write -s %u -r 0x022 -v 0xe", liuId);
        return (system(cmdString) != 0) ? cAtErrorDevFail : cAtOk;
        }

    AtSprintf(cmdString, "/root/liu_write -s %u -r 0x022 -v 0x8", liuId);
    return (system(cmdString) != 0) ? cAtErrorDevFail : cAtOk;
    }

uint32 AtFiberLogicLiuRead(uint16 liuId, uint32 address)
    {
    char cmdString[64];
    AtSprintf(cmdString, "/root/liu_read -s %u -r 0x%x", liuId, address);
    return (system(cmdString) != 0) ? cAtErrorDevFail : cAtOk;
    }

void AtFiberLogicLiuWrite(uint16 liuId, uint32 address, uint32 value)
    {
    char cmdString[64];
    AtSprintf(cmdString, "/root/liu_write -s %u -r 0x%x -v 0x%x", liuId, address, value);
    system(cmdString);
    }







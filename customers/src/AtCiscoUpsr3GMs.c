/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : UPSR
 *
 * File        : AtCiscoUpsr3GMs.c
 *
 * Created Date: Mar 13, 2017
 *
 * Description : UPSR handler for 3G MS card
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCiscoUpsrInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static at_uint32 RxHsGroupPrimaryOffset(AtCiscoUpsr self, at_uint32 hsGroupId)
    {
    AtUnused(self);
    return (at_uint32)(hsGroupId + 0x400 + cHspwRxOffset);
    }

static at_uint32 MaxHsGroupsGet(AtCiscoUpsr self)
    {
    AtUnused(self);
    return 1024;
    }

static at_uint32 MaxApsGroupsGet(AtCiscoUpsr self)
    {
    AtUnused(self);
    return 2048;
    }

static at_uint32 MaxStsGet(AtCiscoUpsr self)
    {
    AtUnused(self);
    return 48;
    }

static void OverrideAtCiscoUpsr(AtCiscoUpsr self)
    {
    mMethodOverride(self->methods, MaxApsGroupsGet);
    mMethodOverride(self->methods, MaxHsGroupsGet);
    mMethodOverride(self->methods, MaxStsGet);
    mMethodOverride(self->methods, RxHsGroupPrimaryOffset);
    }

static void Override(AtCiscoUpsr self)
    {
    OverrideAtCiscoUpsr(self);
    }

static at_uint32 ObjectSize(void)
    {
    return sizeof (tAtCiscoUpsr);
    }

static AtCiscoUpsr ObjectInit(AtCiscoUpsr self, at_uint32 metaBaseAddress, at_uint32 productCode)
    {
    /* Clear memory */
    memset(self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtCisco5GMsCardUpsrObjectInit(self, metaBaseAddress, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);

    return self;
    }

AtCiscoUpsr AtCisco3GMsCardUpsrNew(at_uint32 metaBaseAddress, at_uint32 productCode)
    {
    AtCiscoUpsr newUpsr = malloc(ObjectSize());
    if (newUpsr == NULL)
        return NULL;

    return ObjectInit(newUpsr, metaBaseAddress, productCode);
    }

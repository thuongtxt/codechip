/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : AtCiena.c
 *
 * Created Date: Jul 1, 2016
 *
 * Description : Ciena specific product
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCiena.h"
#include "../../driver/src/generic/util/AtUtil.h"
#include "../../driver/src/implement/codechip/Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "../../driver/src/implement/default/sdh/ThaSdhLine.h"
#include "../../driver/src/implement/default/clock/ThaModuleClock.h"
#include "../../driver/src/implement/codechip/Tha60290021/common/Tha6029DccKbyte.h"
#include "../../driver/src/implement/codechip/Tha60290021/eth/Tha60290021ModuleEth.h"
#include "../../driver/src/implement/codechip/Tha60290021/eth/Tha60290021FaceplateSerdesEthPort.h"
#include "../../driver/src/implement/codechip/Tha60290011/pdh/Tha60290011ModulePdh.h"
#include "../../driver/src/implement/codechip/Tha60290021/pw/Tha60290021ModulePw.h"
#include "../../driver/src/implement/default/pw/ThaModulePw.h"
#include "../../driver/src/generic/pw/AtPwInternal.h"
#include "../../driver/src/implement/codechip/Tha60290021/physical/Tha6029SerdesManager.h"
#include "../../driver/src/generic/eth/AtEthPortInternal.h"
#include "../../driver/src/implement/codechip/Tha60290022/eth/Tha60290022ModuleEth.h"
#include "AtCienaInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mVlanParamApiLogStart(vlan)                                            \
do                                                                             \
    {                                                                          \
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))           \
        {                                                                      \
        uint32 bufferSize;                                                     \
        char *buffer, *vlanString;                                             \
                                                                               \
        AtDriverLogLock();                                                     \
        buffer = AtDriverLogSharedBufferGet(AtDriverSharedDriverGet(), &bufferSize);   \
        vlanString = AtUtilVlan2String(vlan, buffer, bufferSize);              \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,    \
                                AtSourceLocationNone, "API: %s([%s], %s)\r\n", \
                                AtFunction, AtObjectToString((AtObject)self), vlanString); \
        AtDriverLogUnLock();                                                   \
        }                                                                      \
    }while(0)

#define mVlanWrapCall(call, vlan)                                              \
do                                                                             \
    {                                                                          \
    eAtRet ret;                                                                \
                                                                               \
    mVlanParamApiLogStart(vlan);                                               \
    ret = call(self, vlan);                                                    \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return ret;                                                                \
    }while(0)

/*--------------------------- Local typedefs ---------------------------------*/
typedef enum eAtCienaDeviceType
    {
    cAtCienaDeviceTypeUnknown,
    cAtCienaDeviceType10GMro,
    cAtCienaDeviceType20GMro,
    cAtCienaDeviceType10GMroCas,
    cAtCienaDeviceTypePdh
    }eAtCienaDeviceType;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsTdmPw(eAtPwType pwType)
    {
    return ((pwType == cAtPwTypeSAToP) || (pwType == cAtPwTypeCESoP) || (pwType == cAtPwTypeCEP));
    }

static eBool IsHdlcPw(eAtPwType pwType)
    {
    return ((pwType == cAtPwTypeHdlc) || (pwType == cAtPwTypePpp) || (pwType == cAtPwTypeFr));
    }

static eBool IsKbytePw(eAtPwType pwType)
    {
    return (pwType == cAtPwTypeKbyte);
    }

static eBool IsCESoPPw(eAtPwType pwType)
    {
    return (pwType == cAtPwTypeCESoP);
    }

static eBool IsCEPPw(eAtPwType pwType)
    {
    return (pwType == cAtPwTypeCEP);
    }

static void CountersPwCommonGet(AtChannel channel, tAtCienaPwCounters *counter, AtPwCounters hwCounters, eAtPwType pwType)
    {
    eBool countersApplicable= (IsHdlcPw(pwType)||IsKbytePw(pwType)) ? cAtFalse : cAtTrue;
    eBool counterIsSupported = AtChannelCounterIsSupported(channel, cAtPwCounterTypeRxDuplicatedPackets);

    /* Common counters */
    counter->txPackets = AtPwCountersTxPacketsGet(hwCounters);
    counter->txPayloadBytes = AtPwCountersTxPayloadBytesGet(hwCounters);
    counter->rxPackets = AtPwCountersRxPacketsGet(hwCounters);
    counter->rxPayloadBytes = AtPwCountersRxPayloadBytesGet(hwCounters);
    counter->rxDiscardedPackets = AtPwCountersRxDiscardedPacketsGet(hwCounters);
    if (countersApplicable)
        {
        counter->rxMalformedPackets = AtPwCountersRxMalformedPacketsGet(hwCounters);
        counter->rxReorderedPackets = AtPwCountersRxReorderedPacketsGet(hwCounters);
        counter->rxLostPackets = AtPwCountersRxLostPacketsGet(hwCounters);
        counter->rxOutOfSeqDropedPackets = AtPwCountersRxOutOfSeqDropPacketsGet(hwCounters);
        counter->rxStrayPackets = AtPwCountersRxStrayPacketsGet(hwCounters);
        }

    if (counterIsSupported)
        counter->rxDuplicatedPackets = AtPwCountersRxDuplicatedPacketsGet(hwCounters);
    }

static void CountersPwTdmGet(AtChannel channel, tAtCienaPwCounters *counter, AtPwCounters hwCounters, eAtPwType pwType)
    {
    eBool countersApplicable = IsTdmPw(pwType);
    AtPwTdmCounters tdmCounters = (AtPwTdmCounters)hwCounters;

    AtUnused(channel);

    if (countersApplicable)
        {
        counter->txLbitPackets = AtPwTdmCountersTxLbitPacketsGet(tdmCounters);
        counter->txRbitPackets = AtPwTdmCountersTxRbitPacketsGet(tdmCounters);
        counter->rxLbitPackets = AtPwTdmCountersRxLbitPacketsGet(tdmCounters);
        counter->rxRbitPackets = AtPwTdmCountersRxRbitPacketsGet(tdmCounters);
        counter->rxJitBufOverrun = AtPwTdmCountersRxJitBufOverrunGet(tdmCounters);
        counter->rxJitBufUnderrun = AtPwTdmCountersRxJitBufUnderrunGet(tdmCounters);
        counter->rxLops = AtPwTdmCountersRxLopsGet(tdmCounters);
        }
    }

static void CountersPwCESoPGet(tAtCienaPwCounters *counter, AtPwCounters hwCounters, eAtPwType pwType)
    {
    eBool countersApplicable = IsCESoPPw(pwType);
    AtPwCESoPCounters cesopCounters = (AtPwCESoPCounters)hwCounters;

    if (countersApplicable)
        {
        counter->txMbitPackets = AtPwCESoPCountersTxMbitPacketsGet(cesopCounters);
        counter->rxMbitPackets = AtPwCESoPCountersRxMbitPacketsGet(cesopCounters);
        }
    }

static void CountersPwCEPGet(tAtCienaPwCounters *counter, AtPwCounters hwCounters, eAtPwType pwType)
    {
    eBool countersApplicable = IsCEPPw(pwType);
    AtPwCepCounters cepCounters = (AtPwCepCounters)hwCounters;

    if (countersApplicable)
        {
        counter->txNbitPackets = AtPwCepCountersTxNbitPacketsGet(cepCounters);
        counter->txPbitPackets = AtPwCepCountersTxPbitPacketsGet(cepCounters);
        counter->rxNbitPackets = AtPwCepCountersRxNbitPacketsGet(cepCounters);
        counter->rxPbitPackets = AtPwCepCountersRxPbitPacketsGet(cepCounters);
        }
    }

static void CountersPwOtherGet(AtPw pw, tAtCienaPwCounters *counter, eAtPwType pwType)
    {
    if (!IsHdlcPw(pwType))
        {
        counter->numPacketsInJitterBuffer = AtPwNumCurrentPacketsInJitterBuffer(pw);
        counter->numAdditionBytesInJitterBuffer = AtPwNumCurrentAdditionalBytesInJitterBuffer(pw);
        }
    }

static eAtRet CountersPwRead2Clear(AtChannel channel, void* counters, eBool isClear)
    {
    eAtPwType pwType = AtPwTypeGet((AtPw)channel);
    AtPwCounters hwCounters = NULL;
    eAtRet ret = isClear ? AtChannelAllCountersClear(channel, &hwCounters)
                         : AtChannelAllCountersGet(channel, &hwCounters);
    if (ret != cAtOk)
        return ret;

    /* Common counters */
    CountersPwCommonGet(channel, counters, hwCounters, pwType);

    /* TDM counters */
    CountersPwTdmGet(channel, counters, hwCounters, pwType);

    /* CESoP counters */
    CountersPwCESoPGet(counters, hwCounters, pwType);

    /* CEP counters */
    CountersPwCEPGet(counters, hwCounters, pwType);

    /* Other */
    CountersPwOtherGet((AtPw)channel, counters, pwType);

    return cAtOk;
    }

static eTha60290011PdhInterfaceType PdhInterfaceTypeApp2Internal(eAtCienaPdhInterfaceType type)
    {
    if (type == cAtCienaPdhInterfaceTypeDe1)
        return cTha60290011PdhInterfaceTypeDe1;

    return cTha60290011PdhInterfaceTypeDe3;
    }

static eAtCienaPdhInterfaceType PdhInterfaceType2Internal2App(eTha60290011PdhInterfaceType internalType)
    {
    if (internalType == cTha60290011PdhInterfaceTypeDe1)
        return cAtCienaPdhInterfaceTypeDe1;

    return cAtCienaPdhInterfaceTypeDe3;
    }

static eThaClockMonitorOutput RefClockSourceApps2Tha(eAtCienaClockMonitorOutput refClockSource)
    {
    switch ((uint16)refClockSource)
        {
        case cAtCienaClockMonitorOutputSpareSerdesDiv2:
            return cThaClockMonitorOutputSpareSerdesDiv2;
        case cAtCienaClockMonitorOutputOverheadDiv2:
            return cThaClockMonitorOutputOverheadDiv2;
        case cAtCienaClockMonitorOutputXfi155_52M_0Div2:
            return cThaClockMonitorOutputXfi155_52M_0Div2;
        case cAtCienaClockMonitorOutputXfi155_52M_1Div2:
            return cThaClockMonitorOutputXfi155_52M_1Div2;
        case cAtCienaClockMonitorOutputXfi156_25M_0Div2:
            return cThaClockMonitorOutputXfi156_25M_0Div2;
        case cAtCienaClockMonitorOutputXfi156_25M_1Div2:
            return cThaClockMonitorOutputXfi156_25M_1Div2;
        case cAtCienaClockMonitorOutputEth40G_0Div2:
            return cThaClockMonitorOutputEth40G_0Div2;
        case cAtCienaClockMonitorOutputEth40G_1Div2:
            return cThaClockMonitorOutputEth40G_1Div2;
        case cAtCienaClockMonitorOutputBaseX2500Div2:
            return cThaClockMonitorOutputBaseX2500Div2;
        case cAtCienaClockMonitorOutputQdrDiv2:
            return cThaClockMonitorOutputQdrDiv2;
        case cAtCienaClockMonitorOutputDdr_1Div2:
            return cThaClockMonitorOutputDdr_1Div2;
        case cAtCienaClockMonitorOutputDdr_2Div2:
            return cThaClockMonitorOutputDdr_2Div2;
        case cAtCienaClockMonitorOutputDdr_3Div2:
            return cThaClockMonitorOutputDdr_3Div2;
        case cAtCienaClockMonitorOutputDdr_4Div2:
            return cThaClockMonitorOutputDdr_4Div2;
        case cAtCienaClockMonitorOutputPcieDiv2:
            return cThaClockMonitorOutputPcieDiv2;
        case cAtCienaClockMonitorOutputPrc:
            return cThaClockMonitorOutputPrc;
        case cAtCienaClockMonitorOutputExt:
            return cThaClockMonitorOutputExt;
        case cAtCienaClockMonitorOutputSystem:
            return cThaClockMonitorOutputSystem;
        default:
            return cThaClockMonitorOutputNone;
        }
    }

static eAtCienaClockMonitorOutput RefClockSourceTha2Apps(eThaClockMonitorOutput refClockSource)
    {
    switch ((uint16)refClockSource)
        {
        case cThaClockMonitorOutputSpareSerdesDiv2:
            return cAtCienaClockMonitorOutputSpareSerdesDiv2;
        case cThaClockMonitorOutputOverheadDiv2:
            return cAtCienaClockMonitorOutputOverheadDiv2;
        case cThaClockMonitorOutputXfi155_52M_0Div2:
            return cAtCienaClockMonitorOutputXfi155_52M_0Div2;
        case cThaClockMonitorOutputXfi155_52M_1Div2:
            return cAtCienaClockMonitorOutputXfi155_52M_1Div2;
        case cThaClockMonitorOutputXfi156_25M_0Div2:
            return cAtCienaClockMonitorOutputXfi156_25M_0Div2;
        case cThaClockMonitorOutputXfi156_25M_1Div2:
            return cAtCienaClockMonitorOutputXfi156_25M_1Div2;
        case cThaClockMonitorOutputEth40G_0Div2:
            return cAtCienaClockMonitorOutputEth40G_0Div2;
        case cThaClockMonitorOutputEth40G_1Div2:
            return cAtCienaClockMonitorOutputEth40G_1Div2;
        case cThaClockMonitorOutputBaseX2500Div2:
            return cAtCienaClockMonitorOutputBaseX2500Div2;
        case cThaClockMonitorOutputQdrDiv2:
            return cAtCienaClockMonitorOutputQdrDiv2;
        case cThaClockMonitorOutputDdr_1Div2:
            return cAtCienaClockMonitorOutputDdr_1Div2;
        case cThaClockMonitorOutputDdr_2Div2:
            return cAtCienaClockMonitorOutputDdr_2Div2;
        case cThaClockMonitorOutputDdr_3Div2:
            return cAtCienaClockMonitorOutputDdr_3Div2;
        case cThaClockMonitorOutputDdr_4Div2:
            return cAtCienaClockMonitorOutputDdr_4Div2;
        case cThaClockMonitorOutputPcieDiv2:
            return cAtCienaClockMonitorOutputPcieDiv2;
        case cThaClockMonitorOutputPrc:
            return cAtCienaClockMonitorOutputPrc;
        case cThaClockMonitorOutputExt:
            return cAtCienaClockMonitorOutputExt;
        case cThaClockMonitorOutputSystem:
            return cAtCienaClockMonitorOutputSystem;
        default:
            return cAtCienaClockMonitorOutputNone;
        }
    }

static uint32 CorrectProductCode(AtDevice device)
    {
    uint32 correctProductCode = AtDeviceProductCodeGet(device);
    mFieldIns(&correctProductCode, cBit31_28, 28, 0x6);
    return correctProductCode;
    }

static eAtCienaDeviceType DeviceType(AtDevice device)
    {
    uint32 productCode = CorrectProductCode(device);

    switch (productCode)
        {
        case 0x60290061:
        case 0x60291011:
        case 0x60290011: return cAtCienaDeviceTypePdh;
        case 0x60290021: return cAtCienaDeviceType10GMro;
        case 0x60290022: return cAtCienaDeviceType20GMro;
        case 0x6A290011: return cAtCienaDeviceTypePdh;
        case 0x6A290021: return cAtCienaDeviceType10GMro;
        case 0x6A290E21: return cAtCienaDeviceType10GMro;
        case 0x6A290022: return cAtCienaDeviceType20GMro;
        case 0x60291022: return cAtCienaDeviceType10GMroCas;
        default: return cAtCienaDeviceTypeUnknown;
        }
    }

static eBool IsPdhCemDevice(AtDevice self)
    {
    return (DeviceType(self) == cAtCienaDeviceTypePdh) ? cAtTrue : cAtFalse;
    }

static eBool IsMroDevice(AtDevice self)
    {
    eAtCienaDeviceType deviceType = DeviceType(self);

    if ((deviceType == cAtCienaDeviceType20GMro) ||
        (deviceType == cAtCienaDeviceType10GMro) ||
        (deviceType == cAtCienaDeviceType10GMroCas))
        return cAtTrue;

    return cAtFalse;
    }

static eBool ExcessiveErrorRatioThresholdSupported(AtEthPort self)
    {
    if (!IsMroDevice(AtChannelDeviceGet((AtChannel)self)))
        return cAtFalse;

    if (!Tha60290021ModuleEthIsFaceplatePort(self))
        return cAtFalse;

    return Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioIsSupported(self);
    }

static eAtRet DoPdhCemModulePdhMuxTxEthEnable(AtModulePdh self, eBool enabled)
    {
    if (IsPdhCemDevice(AtModuleDeviceGet((AtModule)self)))
        return Tha60290011ModulePdhMuxTxEthEnable(self, enabled);

    return cAtErrorModeNotSupport;
    }

uint16 AtCienaDccKByteSgmiiCounterTypeToThaCounterType(eAtCienaSgmiiEthPortCounterType counterType)
    {
    switch (counterType)
        {
        case cAtCienaSgmiiEthPortCounterTypeDccTxFrames:
            return cTha602SgmiiPortCounterTypeDccTxFrames;
        case cAtCienaSgmiiEthPortCounterTypeDccTxBytes:
            return cTha602SgmiiPortCounterTypeDccTxBytes;
        case cAtCienaSgmiiEthPortCounterTypeDccTxErrors:
            return cTha602SgmiiPortCounterTypeDccTxErrors;
        case cAtCienaSgmiiEthPortCounterTypeDccRxFrames:
            return cTha602SgmiiPortCounterTypeDccRxFrames;
        case cAtCienaSgmiiEthPortCounterTypeDccRxBytes:
            return cTha602SgmiiPortCounterTypeDccRxBytes;
        case cAtCienaSgmiiEthPortCounterTypeDccRxErrors:
            return cTha602SgmiiPortCounterTypeDccRxErrors;
        case cAtCienaSgmiiEthPortCounterTypeKByteTxFrames:
            return cTha602SgmiiPortCounterTypeKByteTxFrames;
        case cAtCienaSgmiiEthPortCounterTypeKByteTxBytes:
            return cTha602SgmiiPortCounterTypeKByteTxBytes;
        case cAtCienaSgmiiEthPortCounterTypeKByteRxFrames:
            return cTha602SgmiiPortCounterTypeKByteRxFrames;
        case cAtCienaSgmiiEthPortCounterTypeDccRxUnrecognizedFrames:
            return cTha602SgmiiPortCounterTypeDccRxUnrecognizedFrames;
        case cAtCienaSgmiiEthPortCounterTypeKbyteRxUnrecognizedFrames:
            return cTha602SgmiiPortCounterTypeKbyteRxUnrecognizedFrames;
        case cAtCienaSgmiiEthPortCounterTypeDccRxGoodFrames:
            return cTha602SgmiiPortCounterTypeDccRxGoodFrames;
        case cAtCienaSgmiiEthPortCounterTypeDccRxDiscardFrames:
            return cTha602SgmiiPortCounterTypeDccRxDiscardFrames;
        case cAtCienaSgmiiEthPortCounterTypeKbyteRxGoodFrames:
            return cTha602SgmiiPortCounterTypeKbyteRxGoodFrames;
        case cAtCienaSgmiiEthPortCounterTypeKbyteRxDiscardFrames:
            return cTha602SgmiiPortCounterTypeKbyteRxDiscardFrames;
        default: return cInvalidUint16;
        }
    }

/*
 * Set Excessive Error Ratio upper threshold
 *
 * @param self Ethernet port
 * @param threshold Threshold value
 *
 * @return AT return code
 */
eAtRet AtCienaEthPortExcessiveErrorRatioUpperThresholdSet(AtEthPort self, uint32 threshold)
    {
    if (ExcessiveErrorRatioThresholdSupported(self))
        return Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioUpperThresholdSet(self, threshold);
    return cAtErrorModeNotSupport;
    }

/*
 * Get Excessive Error Ratio upper threshold
 *
 * @param self Ethernet port
 *
 * @return Excessive error ratio upper threshold value
 */
uint32 AtCienaEthPortExcessiveErrorRatioUpperThresholdGet(AtEthPort self)
    {
    return Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioUpperThresholdGet(self);
    }

/*
 * Get Excessive Error Ratio upper threshold max value
 *
 * @param self Ethernet port
 *
 * @return Excessive error ratio upper threshold max value
 */
uint32 AtCienaEthPortExcessiveErrorRatioUpperThresholdMax(AtEthPort self)
    {
    return Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioUpperThresholdMax(self);
    }

/*
 * Set Excessive Error Ratio lower threshold
 *
 * @param self Ethernet port
 * @param threshold Threshold value
 *
 * @return AT return code
 */
eAtRet AtCienaEthPortExcessiveErrorRatioLowerThresholdSet(AtEthPort self, uint32 threshold)
    {
    if (ExcessiveErrorRatioThresholdSupported(self))
        return Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioLowerThresholdSet(self, threshold);
    return cAtErrorModeNotSupport;
    }

/*
 * Get Excessive Error Ratio lower threshold
 *
 * @param self Ethernet port
 *
 * @return Excessive error ratio lower threshold value
 */
uint32 AtCienaEthPortExcessiveErrorRatioLowerThresholdGet(AtEthPort self)
    {
    return Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioLowerThresholdGet(self);
    }

/*
 * Get Excessive Error Ratio lower threshold max value
 *
 * @param self Ethernet port
 *
 * @return Excessive error ratio lower threshold max value
 */
uint32 AtCienaEthPortExcessiveErrorRatioLowerThresholdMax(AtEthPort self)
    {
    return Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioLowerThresholdMax(self);
    }

/*
 * Check if Excessive Error Ratio thresholds are configurable
 *
 * @param self Ethernet port
 *
 * @retval cAtTrue if configurable
 * @retval cAtFalse if not configurable
 */
eBool AtCienaEthPortExcessiveErrorRatioThresholdSupported(AtEthPort self)
    {
    if (self)
        return ExcessiveErrorRatioThresholdSupported(self);
    return cAtFalse;
    }

/**
 * @addtogroup AtCiena
 * @{
 */

/**
 * Get number of face-plate lines
 *
 * @param self SDH module
 * @return Number of face-plate lines
 */
uint8 AtModuleSdhNumFaceplateLines(AtModuleSdh self)
    {
    return Tha60290021ModuleSdhNumFaceplateLines(self);
    }

/**
 * Get face-plate line instance.
 *
 * @param self SDH module
 * @param localLineId Local line ID which start from 0 to (AtModuleSdhNumFaceplateLines() - 1)
 *
 * @return Line instance if exists. Otherwise, NULL is returned
 */
AtSdhLine AtModuleSdhFaceplateLineGet(AtModuleSdh self, uint8 localLineId)
    {
    return Tha60290021ModuleSdhFaceplateLineGet(self, localLineId);
    }

/**
 * Get number of MATE lines
 *
 * @param self SDH module
 *
 * @return Number of MATE lines
 */
uint8 AtModuleSdhNumMateLines(AtModuleSdh self)
    {
    return Tha60290021ModuleSdhNumMateLines(self);
    }

/**
 * Get MATE line instance
 *
 * @param self SDH module
 * @param localLineId Local line ID which starts from 0 to (AtModuleSdhNumMateLines() - 1)
 *
 * @return MATE line instance if exists. Otherwise, NULL is returned.
 */
AtSdhLine AtModuleSdhMateLineGet(AtModuleSdh self, uint8 localLineId)
    {
    return Tha60290021ModuleSdhMateLineGet(self, localLineId);
    }

/**
 * Get number of terminated lines
 *
 * @param self SDH module
 *
 * @return Number of terminated lines
 */
uint8 AtModuleSdhNumTerminatedLines(AtModuleSdh self)
    {
    return Tha60290021ModuleSdhNumTerminatedLines(self);
    }

/**
 * Get terminated line instance
 *
 * @param self SDH module
 * @param localLineId Local ID starts from 0 to (AtModuleSdhNumTerminatedLines() - 1)
 *
 * @return Terminated line instance
 */
AtSdhLine AtModuleSdhTerminatedLineGet(AtModuleSdh self, uint8 localLineId)
    {
    return Tha60290021ModuleSdhTerminatedLineGet(self, localLineId);
    }

/**
 * Enable/disable face-plate ETH Port bypassing
 *
 * @param self Face-plate ETH Port
 * @param enabled Enabling. cAtTrue to enable and cAtFalse to disable
 *
 * @return AT return code
 */
eAtRet AtCienaEthPortBypassEnable(AtEthPort self, eBool enabled)
    {
    mOneParamWrapCall(enabled, eAtRet, Tha60290021ModuleEthPortBypassEnable(self, enabled));
    }

/**
 * Check if specified face-plate ETH Port is bypassed
 *
 * @param self Face-plate ETH Port
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtCienaEthPortBypassIsEnabled(AtEthPort self)
    {
    return Tha60290021ModuleEthPortBypassIsEnabled(self);
    }

/**
 * Get number of supported VLAN TPID that the module can support
 *
 * @param self ETH module
 *
 * @return Number of VLAN TPID
 */
uint8 AtCienaModuleEthNumSubPortVlanTpids(AtModuleEth self)
	{
	return Tha60290021ModuleEthNumSubPortVlanTpids(self);
	}

/**
 * Set TX VLAN TPID
 *
 * @param self ETH module
 * @param tpidIndex TPID index which starts from 0 to (AtCienaModuleEthNumSubPortVlanTpids() - 1)
 * @param tpid TPID value
 *
 * @return AT return code
 */
eAtRet AtCienaModuleEthSubPortVlanTxTpidSet(AtModuleEth self, uint8 tpidIndex, uint16 tpid)
	{
    mTwoParamsWrapCall(tpidIndex, tpid, eAtRet, Tha60290021ModuleEthSubPortVlanTxTpidSet(self, tpidIndex, tpid));
	}

/**
 * Get TX VLAN TPID
 *
 * @param self ETH module
 * @param tpidIndex TPID index which starts from 0 to (AtCienaModuleEthNumSubPortVlanTpids() - 1)
 *
 * @return TX VLAN TPID
 */
uint16 AtCienaModuleEthSubPortVlanTxTpidGet(AtModuleEth self, uint8 tpidIndex)
	{
	return Tha60290021ModuleEthSubPortVlanTransmitTpidGet(self, tpidIndex);
	}

/**
 * Set expected VLAN TPID
 *
 * @param self ETH module
 * @param tpidIndex TPID index which starts from 0 to (AtCienaModuleEthNumSubPortVlanTpids() - 1)
 * @param tpid TPID value
 *
 * @return AT return code
 */
eAtRet AtCienaModuleEthSubPortVlanExpectedTpidSet(AtModuleEth self, uint8 tpidIndex, uint16 tpid)
	{
    mTwoParamsWrapCall(tpidIndex, tpid, eAtRet, Tha60290021ModuleEthSubPortVlanExpectedTpidSet(self, tpidIndex, tpid));
	}

/**
 * Get expected VLAN TPID
 *
 * @param self ETH module
 * @param tpidIndex TPID index which starts from 0 to (AtCienaModuleEthNumSubPortVlanTpids() - 1)
 *
 * @return Expected VLAN TPID
 */
uint16 AtCienaModuleEthSubPortVlanExpectedTpidGet(AtModuleEth self, uint8 tpidIndex)
	{
	return Tha60290021ModuleEthSubPortVlanExpectedTpidGet(self, tpidIndex);
	}

/**
 * Set sub-port VLAN for frames bypass from face-plate ETH Port to 40G port.
 *
 * @param self Face-plate ETH Port
 * @param vlan Subport VLAN. Pass NULL to disable sub port VLAN insertion.
 *
 * @return AT return code
 */
eAtRet AtCienaEthPortSubportTxVlanSet(AtEthPort self, const tAtVlan *vlan)
    {
    mVlanWrapCall(Tha60290021ModuleEthPortSubportTxVlanSet, vlan);
    }

/**
 * Get sub-port VLAN for frames bypass from face-plate ETH Port to 40G port.
 *
 * @param self Face-plate ETH Port
 * @param [out] vlan Subport VLAN
 *
 * @retval cAtOk if subport VLAN has been specified
 * @retval cAtModuleEthErrorVlanNotExist if subport VLAN has not been specified
 */
eAtRet AtCienaEthPortSubportTxVlanGet(AtEthPort self, tAtVlan *vlan)
    {
    return Tha60290021ModuleEthPortSubportTxVlanGet(self, vlan);
    }

/**
 * Set expected sub-port VLAN to lookup face-plate ETH Port when receiving
 * frames from 40G port.
 *
 * @param self Face-plate ETH Port
 * @param expectedVlan Expected subport VLAN. Pass NULL to disable sub port VLAN
 *                     comparision.
 *
 * @return AT return code
 */
eAtRet AtCienaEthPortSubportExpectedVlanSet(AtEthPort self, const tAtVlan *expectedVlan)
    {
    mVlanWrapCall(Tha60290021ModuleEthPortSubportExpectedVlanSet, expectedVlan);
    }

/**
 * Get expected sub-port VLAN used to lookup face-plate ETH Port when receiving
 * frames from 40G port.
 *
 * @param self Face-plate ETH Port
 * @param [out] expectedVlan Expected subport VLAN
 *
 * @return AT return code
 */
eAtRet AtCienaEthPortSubportExpectedVlanGet(AtEthPort self, tAtVlan *expectedVlan)
    {
    return Tha60290021ModuleEthPortSubportExpectedVlanGet(self, expectedVlan);
    }

/**
 * Set TX sub-port VLAN at specified index
 *
 * @param self The PW Module
 * @param subPortVlanIndex Subport VLAN index to specify VLAN
 * @param vlan Subport VLAN
 *
 * @return AT return code
 */
eAtRet AtCienaModulePwTxSubportVlanSet(AtModulePw self, uint32 subPortVlanIndex, const tAtVlan *vlan)
    {
    eAtRet ret;

    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))
        {
        AtDriverLogLock();
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,
                                AtSourceLocationNone, "API: %s([%s], %u, %u.%u.%u.%u)\r\n",
                                AtFunction,
                                AtObjectToString((AtObject)self),
                                subPortVlanIndex,
                                vlan->tpid, vlan->priority, vlan->cfi, vlan->vlanId);
        AtDriverLogUnLock();
        }

    ret = ThaModulePwTxSubportVlanSet((ThaModulePw)self, subPortVlanIndex, vlan);
    AtDriverApiLogStop();

    return ret;
    }

/**
 * Get TX sub-port VLAN at specified index
 *
 * @param self The PW Module
 * @param subPortVlanIndex Subport VLAN index
 * @param [out] vlan Subport VLAN
 *
 * @return AT return code
 */
eAtRet AtCienaModulePwTxSubportVlanGet(AtModulePw self, uint32 subPortVlanIndex, tAtVlan *vlan)
    {
    return ThaModulePwTxSubportVlanGet((ThaModulePw)self, subPortVlanIndex, vlan);
    }

/**
 * Set expected sub-port VLAN at specified index
 *
 * @param self The PW Module
 * @param subPortVlanIndex Subport VLAN index to specify VLAN
 * @param expectedVlan Expected subport VLAN.
 *
 * @return AT return code
 */
eAtRet AtCienaModulePwExpectedSubportVlanSet(AtModulePw self, uint32 subPortVlanIndex, const tAtVlan *expectedVlan)
    {
    eAtRet ret;

    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))
        {
        AtDriverLogLock();
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,
                                AtSourceLocationNone, "API: %s([%s], %u, %u.%u.%u.%u)\r\n",
                                AtFunction,
                                AtObjectToString((AtObject)self),
                                subPortVlanIndex,
                                expectedVlan->tpid, expectedVlan->priority, expectedVlan->cfi, expectedVlan->vlanId);
        AtDriverLogUnLock();
        }

    ret = ThaModulePwExpectedSubportVlanSet((ThaModulePw)self, subPortVlanIndex, expectedVlan);
    AtDriverApiLogStop();

    return ret;
    }

/**
 * Get expected sub-port VLAN at specified index
 *
 * @param self The PW Module
 * @param subPortVlanIndex Subport VLAN index
 * @param [out] expectedVlan Expected subport VLAN
 *
 * @return AT return code
 */
eAtRet AtCienaModulePwExpectedSubportVlanGet(AtModulePw self, uint32 subPortVlanIndex, tAtVlan *expectedVlan)
    {
    return ThaModulePwExpectedSubportVlanGet((ThaModulePw)self, subPortVlanIndex, expectedVlan);
    }

/**
 * Get number of sub port VLANs that module can support
 *
 * @param self PW module
 *
 * @return Number of sub port VLANs that module can support
 */
uint32 AtCienaModulePwNumSubPortVlans(AtModulePw self)
    {
    return ThaModulePwNumSubPortVlans((ThaModulePw)self);
    }

/**
 * Set subport VLAN index PW will use to encapsulate packets to transmit to PSN
 *
 * @param self This pseudowire
 * @param subPortVlanIndex Subport VLAN index
 *
 * @return AT return code
 */
eAtRet AtCienaPwSubPortVlanIndexSet(AtPw self, uint32 subPortVlanIndex)
    {
    mOneParamWrapCall(subPortVlanIndex, eAtRet, AtPwSubPortVlanIndexSet(self, subPortVlanIndex));
    }

/**
 * Get subport VLAN Index.
 *
 * @param self This pseudowire
 *
 * @return Index selects subport VLAN from PW module
 */
uint32 AtCienaPwSubPortVlanIndexGet(AtPw self)
    {
    return AtPwSubPortVlanIndexGet(self);
    }

/**
 * Specify PDH interface type (24 DS3/E3/EC1 or 84 DS1/E1) for PDH card.
 *
 * @param self PDH module
 * @param type @ref eAtCienaPdhInterfaceType "PDH interface type"
 *
 * @return AT return code
 */
eAtRet AtCienaModulePdhInterfaceTypeSet(AtModulePdh self, eAtCienaPdhInterfaceType type)
    {
    eTha60290011PdhInterfaceType internalType = PdhInterfaceTypeApp2Internal(type);
    mOneParamWrapCall(type, eAtRet, Tha60290011ModulePdhInterfaceTypeSet(self, internalType));
    }

/**
 * Get PDH interface type (24 DS3/E3/EC1 or 84 DS1/E1) on PDH card.
 *
 * @param self PDH module
 *
 * @return @ref eAtCienaPdhInterfaceType "PDH interface type"
 */
eAtCienaPdhInterfaceType AtCienaModulePdhInterfaceTypeGet(AtModulePdh self)
    {
    return PdhInterfaceType2Internal2App(Tha60290011ModulePdhInterfaceTypeGet(self));
    }

/**
 * PW active force/unforce
 *
 * @param self This pseudowire
 * @param active PW force active
 *               - cAtTrue to force
 *               - cAtFalse to unforce
 *
 * @retval AT return code
 */
eAtModulePwRet AtCienaPwTxActiveForce(AtPw self, eBool active)
    {
    mOneParamWrapCall(active, eAtModulePwRet, AtPwTxActiveForce(self, active));
    }

/**
 * Check if PW active is forced or unforced
 *
 * @param self   This pseudowire
 *
 * @retval cAtTrue if PW active is forced
 * @retval cAtFalse if PW active is unforced
 */
eBool AtCienaPwTxActiveIsForced(AtPw self)
    {
    return AtPwTxActiveIsForced(self);
    }

/**
 * Check if specified face-plate ETH Port is supported
 *
 * @param self Face-plate ETH Port
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtCienaEthPortBypassIsSupported(AtEthPort self)
    {
    return Tha60290021ModuleEthPortBypassIsSupported(self);
    }

/**
 * Set high threshold value of ETH Flow Control
 *
 * @param self Face-plate ETH Port
 *
 * @param threshold value of high threshold
 *
 * @return At return code
 */
eAtModuleEthRet AtCienaEthPortFlowControlHighThresholdSet(AtEthPort self, uint32 threshold)
    {
    mOneParamWrapCall(threshold, eAtModuleEthRet, Tha60290021FaceplateEthPortFlowControlHighThresholdSet(self, threshold));
    }

/**
 * Get high threshold value of ETH Flow Control
 *
 * @param self Face-plate ETH Port
 *
 * @return value of high threshold
 */
uint32 AtCienaEthPortFlowControlHighThresholdGet(AtEthPort self)
    {
    return Tha60290021FaceplateEthPortFlowControlHighThresholdGet(self);
    }

/**
 * Set low threshold value of ETH Flow Control
 *
 * @param self Face-plate ETH Port
 *
 * @param threshold value of low threshold
 *
 * @return At return code
 */
eAtModuleEthRet AtCienaEthPortFlowControlLowThresholdSet(AtEthPort self, uint32 threshold)
    {
    mOneParamWrapCall(threshold, eAtModuleEthRet, Tha60290021FaceplateEthPortFlowControlLowThresholdSet(self, threshold));
    }

/**
 * Get low threshold value of ETH Flow Control
 *
 * @param self Face-plate ETH Port
 *
 * @return value of low threshold
 *
 */
uint32 AtCienaEthPortFlowControlLowThresholdGet(AtEthPort self)
    {
    return Tha60290021FaceplateEthPortFlowControlLowThresholdGet(self);
    }

/**
 * Get max watermark (Kbyte unit) value of ETH Flow Control
 *
 * @param self Face-plate ETH Port
 *
 *
 * @return Number of blocks of 1kbyte
 */
uint32 AtCienaEthPortFlowControlTxWaterMarkMaxGet(AtEthPort self)
    {
    return Tha60290021FaceplateEthPortFlowControlTxWaterMarkMaxGet(self);
    }

/**
 * Get max watermark (Kbyte unit) value of ETH Flow Control and clear this watermark
 *
 * @param self Face-plate ETH Port
 *
 *
 * @return Number of blocks of 1kbyte
 */
uint32 AtCienaEthPortFlowControlTxWaterMarkMaxClear(AtEthPort self)
    {
    return Tha60290021FaceplateEthPortFlowControlTxWaterMarkMaxClear(self);
    }

/**
 * Get min watermark (Kbyte unit) value of ETH Flow Control
 *
 * @param self Face-plate ETH Port
 *
 *
 * @return Number of blocks of 1kbyte
 */
uint32 AtCienaEthPortFlowControlTxWaterMarkMinGet(AtEthPort self)
    {
    return Tha60290021FaceplateEthPortFlowControlTxWaterMarkMinGet(self);
    }

/**
 * Get min watermark (Kbyte unit) value of ETH Flow Control and clear this watermark
 *
 * @param self Face-plate ETH Port
 *
 *
 * @return Number of blocks of 1kbyte
 */
uint32 AtCienaEthPortFlowControlTxWaterMarkMinClear(AtEthPort self)
    {
    return Tha60290021FaceplateEthPortFlowControlTxWaterMarkMinClear(self);
    }

/**
 * Get current OOB fill level (Kbyte unit) of ETH Flow Control
 *
 * @param self Face-plate ETH Port
 *
 * @return Number of blocks of 1kbyte
 */
uint32 AtCienaEthPortFlowControlOOBBufferLevelGet(AtEthPort self)
    {
    return Tha60290021FaceplateEthPortFlowControlOOBBufferLevelGet(self);
    }

/**
 * Get current buffer fill level (Kbyte unit) of ETH Flow Control
 *
 * @param self Face-plate ETH Port
 *
 * @return Number of blocks of 1kbyte
 */
uint32 AtCienaEthPortFlowControlTxBufferLevelGet(AtEthPort self)
    {
    return Tha60290021FaceplateEthPortFlowControlTxBufferLevelGet(self);
    }

/**
 * Set ETH Type field DCC PW
 *
 * @param self This PW
 * @param ethType ETH Type
 *
 * @return AT return code
 */
eAtRet AtCienaDccPwEthTypeSet(AtPw self, uint16 ethType)
    {
    mOneParamWrapCall(ethType, eAtRet, Tha6029PwDccPwEthTypeSet((Tha60290021PwHdlc)self, ethType));
    }

/**
 * Get ETH Type field of DCC PW
 *
 * @param self This PW
 *
 * @return ETH Type
 */
uint16 AtCienaDccPwEthTypeGet(AtPw self)
    {
    return Tha6029PwDccPwEthTypeGet((Tha60290021PwHdlc)self);
    }

/**
 * Set Type field for DCC PW
 *
 * @param self This PW
 * @param type Type field
 *
 * @return AT return code
 */
eAtRet AtCienaDccPwTypeSet(AtPw self, uint8 type)
    {
    mOneParamWrapCall(type, eAtRet, Tha6029PwDccPwTypeSet((Tha60290021PwHdlc)self, type));
    }

/**
 * Get Type field of DCC PW
 *
 * @param self This PW
 *
 * @return Type field
 */
uint8 AtCienaDccPwTypeGet(AtPw self)
    {
    return Tha6029PwDccPwTypeGet((Tha60290021PwHdlc)self);
    }

/**
 * Set Version field for DCC PW
 *
 * @param self This PW
 * @param version Version value
 *
 * @return AT return code
 */
eAtRet AtCienaDccPwVersionSet(AtPw self, uint8 version)
    {
    mOneParamWrapCall(version, eAtRet, Tha6029PwDccPwVersionSet((Tha60290021PwHdlc)self, version));
    }

/**
 * Get Version field of DCC PW
 *
 * @param self This PW
 *
 * @return Version
 */
uint8 AtCienaDccPwVersionGet(AtPw self)
    {
    return Tha6029PwDccPwVersionGet((Tha60290021PwHdlc)self);
    }

/**
 * Set expected global MAC
 *
 * @param self SGMII ETH Port that transport DCC Packets
 * @param mac Global upper 43 bits value of expected DA-MAC on Rx-DCC-PW packet.
 * The lower [5:0] will be not set to hardware
 *
 * @return AT return code
 */
eAtRet AtCienaDccExpectGlobalMacSet(AtEthPort self, const uint8 *mac)
    {
    mMacAttributeWrapCall(mac, AtEthPortExpectedDestMacSet(self, mac));
    }

/**
 * Get expected global MAC
 *
 * @param self SGMII ETH Port that transport DCC Packets
 * @param mac Global upper 43 bits value of expected DA-MAC on Rx-DCC-PW packet
 *
 * @return AT return code
 */
eAtRet AtCienaDccExpectGlobalMacGet(AtEthPort self, uint8 *mac)
    {
    return AtEthPortExpectedDestMacGet(self, mac);
    }

/**
 * Enable MAC checking for DCC PW
 *
 * @param self The DCC PW
 * @param enable Enabling
 *               - cAtTrue: to enable MAC checking
 *               - cAtFalse: to disable MAC checking
 *
 * @return AT return code
 */
eAtRet AtCienaDccPwDestMacCheckEnable(AtPw self, eBool enable)
    {
    mOneParamWrapCall(enable, eAtRet, Tha6029PwDccMacCheckEnable((Tha60290021PwHdlc)self, enable));
    }

/**
 * Check if MAC checking is enabled
 *
 * @param self The DCC PW
 *
 * @retval cAtTrue if MAC checking is enabled
 * @retval cAtFalse if MAC checking is disabled
 */
eBool AtCienaDccPwDestMacCheckIsEnabled(AtPw self)
    {
    return Tha6029PwDccMacCheckIsEnabled((Tha60290021PwHdlc)self);
    }

/**
 * Enable C-VLAN checking for DCC PW
 *
 * @param self The DCC PW
 * @param enable Enabling
 *               - cAtTrue: to enable VLAN checking
 *               - cAtFalse: to disable VLAN checking
 *
 * @return AT return code
 */
eAtRet AtCienaDccPwCVLanCheckEnable(AtPw self, eBool enable)
    {
    mOneParamWrapCall(enable, eAtRet, Tha6029PwDccCVlanCheckEnable((Tha60290021PwHdlc)self, enable));
    }

/**
 * Check if C-VLAN checking is enabled
 *
 * @param self The DCC PW
 *
 * @retval cAtTrue if C-Vlan checking is enabled
 * @retval cAtFalse if C-Vlan checking is disabled
 */
eBool AtCienaDccPwCVLanCheckIsEnabled(AtPw self)
    {
    return Tha6029PwDccCVlanCheckIsEnabled((Tha60290021PwHdlc)self);
    }

/**
 * Set expected SGMII-DCC C-VLAN
 *
 * @param self ETH Port
 * @param vlanId value of C-Vlan
 *
 * @return At return code
 */
eAtModuleEthRet AtCienaEthPortExpectedCVlanSet(AtEthPort self, uint16 vlanId)
    {
    mOneParamWrapCall(vlanId, eAtModuleEthRet, AtEthPortExpectedCVlanIdSet(self, vlanId));
    }

/**
 * Get Expected SGMII-DCC C-VLAN
 *
 * @param self Face-plate ETH Port
 *
 * @return value of C-Vlan
 */
uint16 AtCienaEthPortExpectedCVlanGet(AtEthPort self)
    {
    return AtEthPortExpectedCVlanIdGet(self);
    }

/**
 * Set Expected Label for DCC PW
 *
 * @param self The DCC PW
 * @param label Expected label. Value of lower GLobal MAC. 5bits for MRO and 6
 *        bits for PDH
 *
 * @return At return code
 */
eAtRet AtCienaDccPwExpectedLabelSet(AtPw self, uint8 label)
    {
    mOneParamWrapCall(label, eAtRet, Tha6029PwDccPwExpectedLabelSet((Tha60290021PwHdlc)self, label));
    }

/**
 * Get Expected Label for DCC PW
 *
 * @param self The DCC PW
 *
 * @return Expected label. Value of lower GLobal MAC. 5bits for MRO and 6 bits
 *         for PDH
 */
uint8 AtCienaDccPwExpectedLabelGet(AtPw self)
    {
    return Tha6029PwDccPwExpectedLabelGet((Tha60290021PwHdlc)self);
    }

/**
 * To select what SERDES group to be used for 10G MRO
 * - Group 0: SERDES from 0-7
 * - Group 1: SERDES from 8-15
 *
 * @param self Device
 * @param groupId Group ID
 *
 * @return AT return code
 */
eAtRet AtCienaFaceplateSerdesGroupSet(AtDevice self, uint8 groupId)
    {
    Tha6029SerdesManager manager = (Tha6029SerdesManager)AtDeviceSerdesManagerGet(self);
    mOneParamWrapCall(groupId, eAtRet, Tha6029SerdesManagerFaceplateGroupSet(manager, groupId));
    }

/**
 * To get current selected faceplate SERDES group
 *
 * @param device Device
 *
 * @return Selected group ID
 */
uint8 AtCienaFaceplateSerdesGroupGet(AtDevice device)
    {
    Tha6029SerdesManager manager = (Tha6029SerdesManager)AtDeviceSerdesManagerGet(device);
    return Tha6029SerdesManagerFaceplateGroupGet(manager);
    }

/**
 * To set clock monitor output source
 *
 * @param self Clock module
 * @param clockOutputId Clock output ID
 * @param refClockSource Reference clock source
 *
 * @return AT return code
 */
eAtRet AtCienaModuleClockMonitorOutputSourceSet(AtModuleClock self, uint8 clockOutputId, eAtCienaClockMonitorOutput refClockSource)
    {
    mTwoParamsWrapCall(clockOutputId, refClockSource, eAtRet, ThaModuleClockMonitorOutputSet((ThaModuleClock)self, clockOutputId, RefClockSourceApps2Tha(refClockSource)));
    }

/**
 * To get clock monitor output source
 *
 * @param self Clock module
 * @param clockOutputId Clock output ID
 *
 * @return Source to output monitored clock
 */
eAtCienaClockMonitorOutput AtCienaModuleClockMonitorOutputSourceGet(AtModuleClock self, uint8 clockOutputId)
    {
    return RefClockSourceTha2Apps(ThaModuleClockMonitorOutputGet((ThaModuleClock)self, clockOutputId));
    }

/**
 * To get number of clock monitor output sources
 *
 * @param self Clock module
 *
 * @return number of clock monitor outputs
 */
uint8 AtCienaModuleClockMonitorNumOutputs(AtModuleClock self)
    {
    return ThaModuleClockMonitorNumOutputs((ThaModuleClock)self);
    }

/**
 * Get Counters for SGMII Port.
 *
 * @param self SGMII DCC/KBYTE Port
 * @param counterType DCC/KByte over SGMII ETH counter type.
 *
 * @return Counter value.
 *
 * @note Refer:
 *       - @ref eAtCienaSgmiiEthPortCounterType "DCC/KByte over SGMII ETH counters"
 */
uint32 AtCienaDccKByteSgmiiCounterGet(AtEthPort self, eAtCienaSgmiiEthPortCounterType counterType)
    {
    if (AtEthPortSohTransparentIsSupported(self))
        return AtChannelCounterGet((AtChannel) self, AtCienaDccKByteSgmiiCounterTypeToThaCounterType(counterType));
    return 0x0;
    }

/**
 * Get and Clear Counters for SGMII Port.
 *
 * @param self SGMII DCC/KBYTE Port
 * @param counterType DCC/KByte over SGMII ETH counter type.
 *
 * @return Counter value.
 *
 * @note Refer:
 *       - @ref eAtCienaSgmiiEthPortCounterType "DCC/KByte over SGMII ETH counters"
 */
uint32 AtCienaDccKByteSgmiiCounterClear(AtEthPort self, eAtCienaSgmiiEthPortCounterType counterType)
    {
	return AtChannelCounterClear((AtChannel) self, AtCienaDccKByteSgmiiCounterTypeToThaCounterType(counterType));
    }

/**
 * Enable/disable PDH traffic to 2.5G MAC port for the PDH CEM.
 *
 * @param self PDH module of the PDH CEM device
 * @param enabled Enable/disable the PDH traffic to 2.5G MAC port.
 *
 * @return Refer:
 *         - @ref eAtRet "At Return code".
 */
eAtRet AtCienaPdhCemModulePdhMuxTxEthEnable(AtModulePdh self, eBool enabled)
    {
    mOneParamWrapCall(enabled, eAtRet, DoPdhCemModulePdhMuxTxEthEnable(self, enabled));
    }

/**
 * Get configure of that the PDH traffic to 2.5G MAC port for the PDH CEM is enable/disable.
 *
 * @param self PDH module of the PDH CEM device
 * @return cAtTrue: the PDH traffic to 2.5G MAC port is enabled.
 *         cAtFalse: the PDH traffic to 2.5G MAC port is disabled.
 */
eBool AtCienaPdhCemModulePdhMuxTxEthIsEnabled(AtModulePdh self)
    {
    if (IsPdhCemDevice(AtModuleDeviceGet((AtModule)self)))
        return Tha60290011ModulePdhMuxTxEthIsEnabled(self);

    return cAtFalse;
    }

/**
 * Get Counters for PW.
 *
 * @param self PW Channel
 * @param counters PW counters.
 *
 * @return Refer:
 *         - @ref eAtRet "At Return code".
 *
 * @note Refer:
 *       - @ref tAtCienaPwCounters "PW counters"
 */
eAtRet AtCienaPwAllCountersGet(AtChannel self, void *counters)
    {
    return CountersPwRead2Clear(self, counters, cAtFalse);
    }

/**
 * Get and Clear Counters for PW.
 *
 * @param self PW Channel
 * @param counters PW counters.
 *
 * @return Refer:
 *         - @ref eAtRet "At Return code".
 *
 * @note Refer:
 *       - @ref tAtCienaPwCounters "PW counters"
 */
eAtRet AtCienaPwAllCountersClear(AtChannel self, void *counters)
    {
    return CountersPwRead2Clear(self, counters, cAtTrue);
    }
/**
 * @}
 */


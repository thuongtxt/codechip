/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : UPSR
 *
 * File        : AtCiscoUpsr.c
 *
 * Created Date: Dec 19, 2016
 *
 * Description : UPSR handler for CEM OCN 10G
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCiscoUpsrInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cMetaStartOffset              0x0
#define cMetaMomerySizeInBytes        16
#define cDwordStartOffset             0x2F0000
#define cUprsMemorySizeInBytes        (15 * 1024 * 1024) /* 15 Mbytes */

#define cAf6ProductCodeRegister       0x0
#define cAf6FpgaVersionRegister       0x2
#define cAf6FpgaBuiltNumberRegister   0x3

/*--------------------------- Macros -----------------------------------------*/
#ifdef __ARCH_POWERPC__
#define mAsm(asmCmd) __asm__(asmCmd)
#else
#define mAsm(asmCmd)
#endif

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* For testing purpose */
static AtCiscoUpsrDirectReadHandler  m_directReadHandler  = NULL;
static AtCiscoUpsrDirectWriteHandler m_directWriteHandler = NULL;
static AtCiscoUpsrDirectReadHandler  m_directDeviceInfoReadHandler  = NULL;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static at_uint32 DirectMetaRead(at_uint32 address)
    {
    if (m_directDeviceInfoReadHandler)
        return m_directDeviceInfoReadHandler(address);

    return (*(at_uint32*)(address));
    }

static at_uint32 DirectRead(at_uint32 address)
    {
    if (m_directReadHandler)
        return m_directReadHandler(address);

    return (*(at_uint32*)address);
    }

static void DirectWrite(at_uint32 address, at_uint32 value)
    {
    if (m_directWriteHandler)
        {
        m_directWriteHandler(address, value);
        return;
        }

    (*(at_uint32*)address) = value;
    mAsm("sync");

    /* Read back to sync */
    (void)(*(at_uint32*)address);
    mAsm("sync");
    }

static at_uint32 DeviceMetaRead(at_uint32 metaBase, at_uint32 address, const tAtCiscoUpsrDelegate *delegate, void *delegateData)
    {
    at_uint32 value;
    if (delegate)
        {
        if ((delegate->CanReadRegister != NULL) && (delegate->CanReadRegister(metaBase, address, delegateData) == 0))
            return 0;
        }

    value = DirectMetaRead((metaBase + (address << 2)));
    mAsm("sync");

    return value;
    }

static at_uint32 ProductCode(at_uint32 metaBaseAddress, const tAtCiscoUpsrDelegate *delegate, void *delegateData)
    {
    return DeviceMetaRead(metaBaseAddress, cAf6ProductCodeRegister, delegate, delegateData);
    }

static at_uint32 MaxApsGroupsGet(AtCiscoUpsr self)
    {
    AtUnused(self);
    return 8192;
    }

static at_uint32 MaxHsGroupsGet(AtCiscoUpsr self)
    {
    AtUnused(self);
    return 4096;
    }

static at_uint32 MaxStsGet(AtCiscoUpsr self)
    {
    AtUnused(self);
    return 384;
    }

static void HwTxApsGroupEnable(AtCiscoUpsr self, at_uint32 apsGroupId, at_uint32 enable)
    {
    at_uint32 group16 = apsGroupId / 16;
    at_uint32 idInGroup16 = apsGroupId % 16;
    at_uint32 regAddr = cHspwTxOffset + cAf6Reg_pla_out_upsr_ctrl(group16);
    at_uint32 regVal = AtCiscoUpsrHwRead(self, regAddr);

    mRegFieldSet(regVal,
                cAf6_pla_out_upsr_ctrl_PlaOutUpsrEnCtr_Mask(idInGroup16),
                cAf6_pla_out_upsr_ctrl_PlaOutUpsrEnCtr_Shift(idInGroup16),
                enable);
    AtCiscoUpsrHwWrite(self, regAddr, regVal);
    }

static at_uint32 HwTxApsGroupIsEnabled(AtCiscoUpsr self, at_uint32 apsGroupId)
    {
    at_uint32 group16 = apsGroupId / 16;
    at_uint32 idInGroup16 = apsGroupId % 16;
    at_uint32 regAddr = cHspwTxOffset + cAf6Reg_pla_out_upsr_ctrl(group16);
    at_uint32 regVal = AtCiscoUpsrHwRead(self, regAddr);

    return mRegField(regVal,
                     cAf6_pla_out_upsr_ctrl_PlaOutUpsrEnCtr_Mask(idInGroup16),
                     cAf6_pla_out_upsr_ctrl_PlaOutUpsrEnCtr_Shift(idInGroup16));
    }

static at_uint32 RxHsGroupPrimaryOffset(AtCiscoUpsr self, at_uint32 hsGroupId)
    {
    AtUnused(self);
    return (at_uint32)(hsGroupId + 0x1000 + cHspwRxOffset);
    }

static void HsGroupTxLabelSetSelect(AtCiscoUpsr self, at_uint32 hsGroupId, eAtCiscoPwGroupLabelSet labelSet)
    {
    at_uint32 regVal = 0;
    at_uint32 hwLabelSet = (labelSet == cAtCiscoPwGroupLabelSetPrimary) ? 0 : 1;

    mRegFieldSet(regVal,
                 cAf6_pla_out_hspw_ctrl_PlaOutHspwEnCtr_Mask,
                 cAf6_pla_out_hspw_ctrl_PlaOutHspwEnCtr_Shift,
                 1);
    mRegFieldSet(regVal,
                 cAf6_pla_out_hspw_ctrl_PlaOutHspwPsnCtr_Mask,
                 cAf6_pla_out_hspw_ctrl_PlaOutHspwPsnCtr_Shift,
                 hwLabelSet);
    AtCiscoUpsrHwWrite(self, cHspwTxOffset + cAf6Reg_pla_out_hspw_ctrl(hsGroupId), regVal);
    }

static eAtCiscoPwGroupLabelSet HsGroupTxSelectedLabelGet(AtCiscoUpsr self, at_uint32 hsGroupId)
    {
    at_uint32 regVal = AtCiscoUpsrHwRead(self, cHspwTxOffset + cAf6Reg_pla_out_hspw_ctrl(hsGroupId));
    at_uint32 labelSet = mRegField(regVal,
                                   cAf6_pla_out_hspw_ctrl_PlaOutHspwPsnCtr_Mask,
                                   cAf6_pla_out_hspw_ctrl_PlaOutHspwPsnCtr_Shift);
    return (labelSet == 0) ? cAtCiscoPwGroupLabelSetPrimary : cAtCiscoPwGroupLabelSetBackup;
    }

static at_uint32 UpsrStartOffset(AtCiscoUpsr self)
    {
    AtUnused(self);
    return cDwordStartOffset;
    }

static at_uint32 UpsrMemSizeInBytes(AtCiscoUpsr self)
    {
    AtUnused(self);
    return cUprsMemorySizeInBytes;
    }

static char StsRowIdIsValid(AtCiscoUpsr self, at_uint32 rowId)
    {
    AtUnused(self);
    return ((rowId < 12) ? 1 : 0);
    }

static char VtRowIdIsValid(AtCiscoUpsr self, at_uint32 rowId)
    {
    AtUnused(self);
    return ((rowId < 384) ? 1 : 0);
    }

static char ApsGroupIdIsValid(AtCiscoUpsr self, at_uint32 apsGroupId)
    {
    return (apsGroupId < mMethodsGet(self).MaxApsGroupsGet(self)) ? 1 : 0;
    }

static char HsGroupIdIsValid(AtCiscoUpsr self, at_uint32 hsGroupId)
    {
    return (hsGroupId < mMethodsGet(self).MaxHsGroupsGet(self)) ? 1 : 0;
    }

static AtCiscoUpsr UpsrCreateByProductCode(at_uint32 metaBaseAddress, at_uint32 productCode)
    {
    switch (productCode)
        {
        case 0x60210051: return AtCiscoOcnCardUpsrNew(metaBaseAddress, productCode);
        case 0x60210012: return AtCisco5GMsCardUpsrNew(metaBaseAddress, productCode);
        case 0x60210061: return AtCisco3GMsCardUpsrNew(metaBaseAddress, productCode);
        default:
            break;
        }

    return NULL;
    }

static void MethodsInit(AtCiscoUpsr self)
    {
    memset(&self->methods, 0, sizeof(self->methods));
    mMethodOverride(self->methods, MaxApsGroupsGet);
    mMethodOverride(self->methods, MaxHsGroupsGet);
    mMethodOverride(self->methods, MaxStsGet);
    mMethodOverride(self->methods, HwTxApsGroupEnable);
    mMethodOverride(self->methods, HwTxApsGroupIsEnabled);
    mMethodOverride(self->methods, RxHsGroupPrimaryOffset);
    mMethodOverride(self->methods, HsGroupTxLabelSetSelect);
    mMethodOverride(self->methods, HsGroupTxSelectedLabelGet);
    mMethodOverride(self->methods, UpsrStartOffset);
    mMethodOverride(self->methods, UpsrMemSizeInBytes);
    }

static at_uint32 ObjectSize(void)
    {
    return sizeof (tAtCiscoUpsr);
    }

AtCiscoUpsr AtCiscoOcnCardUpsrObjectInit(AtCiscoUpsr self, at_uint32 metaBaseAddress, at_uint32 productCode)
    {
    memset(self, 0, ObjectSize());

    /* Initialize implementation */
    MethodsInit(self);

    /* Initialize private data */
    self->metaBaseAddress = metaBaseAddress;
    self->productCode     = productCode;

    return self;
    }

AtCiscoUpsr AtCiscoOcnCardUpsrNew(at_uint32 metaBaseAddress, at_uint32 productCode)
    {
    AtCiscoUpsr newUpsr = malloc(ObjectSize());
    if (newUpsr == NULL)
        return NULL;

    return AtCiscoOcnCardUpsrObjectInit(newUpsr, metaBaseAddress, productCode);
    }

at_uint32 AtCiscoUpsrHwRead(AtCiscoUpsr self, at_uint32 address)
    {
    at_uint32 value;

    if ((self->delegate.CanReadRegister != NULL) &&
        (self->delegate.CanReadRegister(mBaseAddress(self), address, self->delegateData) == 0))
        {
        self->latchedError = cAtCiscoUpsrRetReadFail;
        return 0;
        }

    value = DirectRead(mRealAddress(mBaseAddress(self), address));
    mAsm("sync");

    self->latchedError = cAtCiscoUpsrRetOk;
    return value;
    }

void AtCiscoUpsrHwWrite(AtCiscoUpsr self, at_uint32 address, at_uint32 value)
    {
    size_t realAddress = mRealAddress(mBaseAddress(self), address);

    if ((self->delegate.CanWriteRegister != NULL) &&
        (self->delegate.CanWriteRegister(mBaseAddress(self), address, value, self->delegateData) == 0))
        {
        self->latchedError = cAtCiscoUpsrRetWriteFail;
        return;
        }

    DirectWrite(realAddress, value);

    self->latchedError = cAtCiscoUpsrRetOk;
    }

void AtCiscoUpsrDirectReadHandlerSet(AtCiscoUpsrDirectReadHandler readHandler)
    {
    m_directReadHandler = readHandler;
    }

void AtCiscoUpsrDirectWriteHandlerSet(AtCiscoUpsrDirectWriteHandler writeHandler)
    {
    m_directWriteHandler = writeHandler;
    }

void AtCiscoUpsrDeviceMetaDirectReadHandlerSet(AtCiscoUpsrDirectReadHandler readHandler)
    {
    m_directDeviceInfoReadHandler = readHandler;
    }

/**
 * @addtogroup AtCiscoUpsr
 * @{
 */

/**
 * Start offset of device meta information
 *
 * @return offset
 */
at_uint32 AtCiscoUpsrMetaStartOffset(void)
    {
    return cMetaStartOffset;
    }

/**
 * Size of device meta information memory
 *
 * @return Memory size
 */
at_uint32 AtCiscoUpsrMetaSizeInBytes(void)
    {
    return cMetaMomerySizeInBytes;
    }

/**
 * Create UPSR handler
 *
 * @param metaBaseAddress Device meta information base address
 * @param delegate Access delegate to check if read/write can be done
 * @param userData Application opaque data
 *
 * @return UPSR handler instance
 */
AtCiscoUpsr AtCiscoUpsrCreate(at_uint32 metaBaseAddress, const tAtCiscoUpsrDelegate *delegate, void *userData)
    {
    at_uint32 productCode = ProductCode(metaBaseAddress, delegate, userData);
    AtCiscoUpsr upsrHandler = UpsrCreateByProductCode(metaBaseAddress, productCode);

    if (upsrHandler)
        {
        AtCiscoUpsrDelegateSet(upsrHandler, delegate, userData);
        upsrHandler->version     = DeviceMetaRead(metaBaseAddress, cAf6FpgaVersionRegister, delegate, userData);
        upsrHandler->builtNumber = DeviceMetaRead(metaBaseAddress, cAf6FpgaBuiltNumberRegister, delegate, userData);
        }

    return upsrHandler;
    }

/**
 * Delete UPSR handler.
 *
 * @param self UPSR handler
 *
 * @return None
 */
void AtCiscoUpsrDelete(AtCiscoUpsr self)
    {
    if (self)
        free(self);
    }

/**
 * Get start offset in dword alignment that application will
 * used for mmap calling
 *
 * @param self UPRS handler
 * @return Start offset in dword alignment
 */
at_uint32 AtCiscoUpsrDwordStartOffset(AtCiscoUpsr self)
    {
    mAssert(self);
    return mMethodsGet(self).UpsrStartOffset(self);
    }

/**
 * Get memory size in bytes to do mmap
 *
 * @param self UPRS handler
 * @return Memory size in bytes to do mmap
 */
at_uint32 AtCiscoUpsrMemorySizeInBytes(AtCiscoUpsr self)
    {
    mAssert(self);
    return mMethodsGet(self).UpsrMemSizeInBytes(self);
    }

/**
 * Set UPSR base address
 *
 * @param self UPSR handler
 * @param upsrBaseAddress
 */
void AtCiscoUpsrBaseAddressSet(AtCiscoUpsr self, at_uint32 upsrBaseAddress)
    {
    mAssert(self);
    mBaseAddress(self) = upsrBaseAddress;
    }

/**
 * Get the base address of UPSR table
 *
 * @param self UPSR table instance
 *
 * @return Base address
 */
at_uint32 AtCiscoUpsrBaseAddress(AtCiscoUpsr self)
    {
    mAssert(self);
    return mBaseAddress(self);
    }

/**
 * Get version of UPSR handler
 *
 * @return Version of UPSR handler
 */
const char *AtCiscoUpsrVersion(AtCiscoUpsr self)
    {
    AtUnused(self);
    return "2.2.0";
    }

/**
 * Get maximum number of PW APS groups
 *
 * @param self UPSR handler
 * @return Maximum number of PW APS groups
 */
at_uint32 AtCiscoUpsrMaxApsGroupsGet(AtCiscoUpsr self)
    {
    mAssert(self);
    return mMethodsGet(self).MaxApsGroupsGet(self);
    }

/**
 * Get maximum number of PW HS groups
 *
 * @param self UPSR handler
 * @return Maximum number of PW HS groups
 */
at_uint32 AtCiscoUpsrMaxHsGroupsGet(AtCiscoUpsr self)
    {
    mAssert(self);
    return mMethodsGet(self).MaxHsGroupsGet(self);
    }

/**
 * Get maximum number of STS
 *
 * @param self UPSR handler
 * @return Maximum number of STS
 */
at_uint32 AtCiscoUpsrMaxStsGet(AtCiscoUpsr self)
    {
    mAssert(self);
    return mMethodsGet(self).MaxStsGet(self);
    }

/**
 * Get maximum number of VTs in one STS
 *
 * @param self UPSR handler
 * @return Maximum number of VTs in one STS
 */
at_uint32 AtCiscoUpsrMaxVtInOneStsGet(AtCiscoUpsr self)
    {
    AtUnused(self);
    return 28;
    }

/**
 * Install delegate methodology to access hardware UPSR table.
 *
 * @param self UPSR table instance
 * @param delegate Delegate method to access hardware UPSR table. It should be
 *                 NULL to prevent hardware access.
 * @param userData User data passing on delegate processing.
 *
 * @return None
 */
void AtCiscoUpsrDelegateSet(AtCiscoUpsr self, const tAtCiscoUpsrDelegate *delegate, void *userData)
    {
    mAssert(self);

    self->delegate.CanReadRegister  = delegate ? delegate->CanReadRegister  : NULL;
    self->delegate.CanWriteRegister = delegate ? delegate->CanWriteRegister : NULL;
    self->delegateData = userData;
    }

/**
 * Get the installed delegate methodology.
 *
 * @param self UPSR table instance
 *
 * @return The local container of delegate methods.
 */
const tAtCiscoUpsrDelegate *AtCiscoUpsrDelegateGet(AtCiscoUpsr self)
    {
    mAssert(self);
    return &(self->delegate);
    }

/**
 * Restore UPSR interrupt.
 *
 * @param self UPSR table instance
 */
void AtCiscoUpsrInterruptRestore(AtCiscoUpsr self)
    {
    if (self)
        {
        at_uint32 regAddr = cUpsrOffset + cAf6Reg_upen_amectl;
        at_uint32 regVal;

        regVal = AtCiscoUpsrHwRead(self, regAddr);
        AtCiscoUpsrHwWrite(self, regAddr, regVal | cAf6_upen_amectl_AMECtlFIRT_Mask);
        AtCiscoUpsrHwWrite(self, regAddr, regVal & ~cAf6_upen_amectl_AMECtlFIRT_Mask);
        }
    }

/**
 * Read MASR status register
 *
 * @param self UPSR table instance
 * @param table @ref eAtCiscoUpsrAlarm "SD/SF table"
 *
 * @return Register value
 */
at_uint32 AtCiscoUpsrMASRStatusRead(AtCiscoUpsr self, eAtCiscoUpsrAlarm table)
    {
    mAssert(self);

    if (table == cAtCiscoUpsrAlarmSf)
        return AtCiscoUpsrHwRead(self, cUpsrOffset + cAf6Reg_upen_sfrowsts);

    if (table == cAtCiscoUpsrAlarmSd)
        return AtCiscoUpsrHwRead(self, cUpsrOffset + cAf6Reg_upen_sdrowsts);

    return 0;
    }

/**
 * Read MASR interrupt mask register
 *
 * @param self UPSR table instance
 *
 * @return Register value
 */
at_uint32 AtCiscoUpsrMASRMaskRead(AtCiscoUpsr self)
    {
    mAssert(self);
    return AtCiscoUpsrHwRead(self, cUpsrOffset + cAf6Reg_upen_ameintdi);
    }

/**
 * Write MASR status register
 *
 * @param self UPSR table instance
 * @param value Value to write
 */
void AtCiscoUpsrMASRMaskWrite(AtCiscoUpsr self, at_uint32 value)
    {
    mAssert(self);
    AtCiscoUpsrHwWrite(self, cUpsrOffset + cAf6Reg_upen_ameintdi, value);
    }

/**
 * Read STS mask register
 *
 * @param self UPSR table instance
 * @param table @ref eAtCiscoUpsrAlarm "SD/SF table"
 * @param rowId Row index
 *
 * @return Register value
 */
at_uint32 AtCiscoUpsrStsMaskRead(AtCiscoUpsr self, eAtCiscoUpsrAlarm table, at_uint32 rowId)
    {
    mAssert(self);
    if (!StsRowIdIsValid(self, rowId))
        return 0;

    if (table == cAtCiscoUpsrAlarmSf)
        return AtCiscoUpsrHwRead(self, cUpsrOffset + cAf6Reg_upen_xxcfgsts(0, rowId));

    if (table == cAtCiscoUpsrAlarmSd)
        return AtCiscoUpsrHwRead(self, cUpsrOffset + cAf6Reg_upen_xxcfgsts(1, rowId));

    return 0;
    }

/**
 * Write STS mask register
 *
 * @param self UPSR table instance
 * @param table @ref eAtCiscoUpsrAlarm "SD/SF table"
 * @param rowId Row index
 * @param value Value to write
 */
void AtCiscoUpsrStsMaskWrite(AtCiscoUpsr self, eAtCiscoUpsrAlarm table, at_uint32 rowId, at_uint32 value)
    {
    mAssert(self);
    if (!StsRowIdIsValid(self, rowId))
        return;

    if (table == cAtCiscoUpsrAlarmSf)
        AtCiscoUpsrHwWrite(self, cUpsrOffset + cAf6Reg_upen_xxcfgsts(0, rowId), value);

    if (table == cAtCiscoUpsrAlarmSd)
        AtCiscoUpsrHwWrite(self, cUpsrOffset + cAf6Reg_upen_xxcfgsts(1, rowId), value);
    }

/**
 * Read STS current status register
 *
 * @param self UPSR table instance
 * @param table @ref eAtCiscoUpsrAlarm "SD/SF table"
 * @param rowId Row index
 *
 * @return Register value
 */
at_uint32 AtCiscoUpsrStsStatusRead(AtCiscoUpsr self, eAtCiscoUpsrAlarm table, at_uint32 rowId)
    {
    mAssert(self);
    if (!StsRowIdIsValid(self, rowId))
        return 0;

    if (table == cAtCiscoUpsrAlarmSf)
        return AtCiscoUpsrHwRead(self, cUpsrOffset + cAf6Reg_upen_xxstasts(0, rowId));

    if (table == cAtCiscoUpsrAlarmSd)
        return AtCiscoUpsrHwRead(self, cUpsrOffset + cAf6Reg_upen_xxstasts(1, rowId));

    return 0;
    }

/**
 * Read VT interrupt mask register
 *
 * @param self UPSR table instance
 * @param table @ref eAtCiscoUpsrAlarm "SD/SF table"
 * @param rowId Row index
 *
 * @return Register value
 */
at_uint32 AtCiscoUpsrVtMaskRead(AtCiscoUpsr self, eAtCiscoUpsrAlarm table, at_uint32 rowId)
    {
    mAssert(self);
    if (!VtRowIdIsValid(self, rowId))
        return 0;

    if (table == cAtCiscoUpsrAlarmSf)
        return AtCiscoUpsrHwRead(self, cUpsrOffset + cAf6Reg_upen_xxcfgvt(0, rowId));

    if (table == cAtCiscoUpsrAlarmSd)
        return AtCiscoUpsrHwRead(self, cUpsrOffset + cAf6Reg_upen_xxcfgvt(1, rowId));

    return 0;
    }

/**
 * Write VT interrupt mask register
 *
 * @param self UPSR table instance
 * @param table @ref eAtCiscoUpsrAlarm "SD/SF table"
 * @param rowId Row index
 * @param value Value to write
 */
void AtCiscoUpsrVtMaskWrite(AtCiscoUpsr self, eAtCiscoUpsrAlarm table, at_uint32 rowId, at_uint32 value)
    {
    mAssert(self);
    if (!VtRowIdIsValid(self, rowId))
        return;

    if (table == cAtCiscoUpsrAlarmSf)
        AtCiscoUpsrHwWrite(self, cUpsrOffset + cAf6Reg_upen_xxcfgvt(0, rowId), value);

    if (table == cAtCiscoUpsrAlarmSd)
        AtCiscoUpsrHwWrite(self, cUpsrOffset + cAf6Reg_upen_xxcfgvt(1, rowId), value);
    }

/**
 * Read VT current status register
 *
 * @param self UPSR table instance
 * @param table @ref eAtCiscoUpsrAlarm "SD/SF table"
 * @param rowId Row index
 *
 * @return Register value
 */
at_uint32 AtCiscoUpsrVtStatusRead(AtCiscoUpsr self, eAtCiscoUpsrAlarm table, at_uint32 rowId)
    {
    mAssert(self);
    if (!VtRowIdIsValid(self, rowId))
        return 0;

    if (table == cAtCiscoUpsrAlarmSf)
        return AtCiscoUpsrHwRead(self, cUpsrOffset + cAf6Reg_upen_xxstavt(0, rowId));

    if (table == cAtCiscoUpsrAlarmSd)
        return AtCiscoUpsrHwRead(self, cUpsrOffset + cAf6Reg_upen_xxstavt(1, rowId));

    return 0;
    }

/**
 * Read STS SASR status change register
 *
 * @param self UPSR table instance
 * @param table @ref eAtCiscoUpsrAlarm "SD/SF table"
 * @param rowId Row index
 *
 * @return Register value
 */
at_uint32 AtCiscoUpsrSASRStsRead(AtCiscoUpsr self, eAtCiscoUpsrAlarm table, at_uint32 rowId)
    {
    mAssert(self);
    if (rowId != 0)
        return 0;

    if (table == cAtCiscoUpsrAlarmSf)
        return AtCiscoUpsrHwRead(self, cUpsrOffset + cAf6Reg_upen_sfrowsts) & 0xFFF;

    if (table == cAtCiscoUpsrAlarmSd)
        return AtCiscoUpsrHwRead(self, cUpsrOffset + cAf6Reg_upen_sdrowsts) & 0xFFF;

    return 0;
    }

/**
 * Read VT SASR status change register
 *
 * @param self UPSR table instance
 * @param table @ref eAtCiscoUpsrAlarm "SD/SF table"
 * @param rowId Row index
 *
 * @return Register value
 */
at_uint32 AtCiscoUpsrSASRVtRead(AtCiscoUpsr self, eAtCiscoUpsrAlarm table, at_uint32 rowId)
    {
    mAssert(self);
    if (!StsRowIdIsValid(self, rowId))
        return 0;

    if (table == cAtCiscoUpsrAlarmSf)
        return AtCiscoUpsrHwRead(self, cUpsrOffset + cAf6Reg_upen_xxrowvt(0, rowId));

    if (table == cAtCiscoUpsrAlarmSd)
        return AtCiscoUpsrHwRead(self, cUpsrOffset + cAf6Reg_upen_xxrowvt(1, rowId));

    return 0;
    }

/**
 * Set SONET/SDH path alarm mask to UPSR-SF/SD alarm
 *
 * @param self UPSR table instance
 * @param table @ref eAtCiscoUpsrAlarm "SD/SF table"
 * @param alarmMask @ref eAtCiscoUpsrPathAlarm "SONET/SDH UPSR Path alarm types"
 */
void AtCiscoUpsrAlarmMaskWrite(AtCiscoUpsr self, eAtCiscoUpsrAlarm table, eAtCiscoUpsrPathAlarm alarmMask)
    {
    at_uint32 value = AtCiscoUpsrHwRead(self, cUpsrOffset + cAf6Reg_upen_amemask);

    if (table == cAtCiscoUpsrAlarmSf)
        mRegFieldSet(value, cAf6_upen_amemask_AMESFAlmMask_Mask, cAf6_upen_amemask_AMESFAlmMask_Shift, alarmMask);

    if (table == cAtCiscoUpsrAlarmSd)
        mRegFieldSet(value, cAf6_upen_amemask_AMESDAlmMask_Mask, cAf6_upen_amemask_AMESDAlmMask_Shift, alarmMask);

    AtCiscoUpsrHwWrite(self, cUpsrOffset + cAf6Reg_upen_amemask, value);
    }

/**
 * Get SONET/SDH path alarm mask from UPSR-SF/SD alarm
 *
 * @param self UPSR table instance
 * @param table @ref eAtCiscoUpsrAlarm "SD/SF table"
 *
 * @return alarmMask @ref eAtCiscoUpsrPathAlarm "SONET/SDH UPSR Path alarm types"
 */
eAtCiscoUpsrPathAlarm AtCiscoUpsrAlarmMaskRead(AtCiscoUpsr self, eAtCiscoUpsrAlarm table)
    {
    at_uint32 value = AtCiscoUpsrHwRead(self, cUpsrOffset + cAf6Reg_upen_amemask);

    if (table == cAtCiscoUpsrAlarmSf)
        return (eAtCiscoUpsrPathAlarm)mRegField(value, cAf6_upen_amemask_AMESFAlmMask_Mask, cAf6_upen_amemask_AMESFAlmMask_Shift);

    if (table == cAtCiscoUpsrAlarmSd)
        return (eAtCiscoUpsrPathAlarm)mRegField(value, cAf6_upen_amemask_AMESDAlmMask_Mask, cAf6_upen_amemask_AMESDAlmMask_Shift);

    return 0;
    }

/**
 * Enable/disable APS group
 *
 * @param self UPSR table instance
 * @param apsGroupId APS group ID
 * @param enable Enabling
 *               - cAtTrue to enable
 *               - cAtFalse to disable
 */
void AtCiscoApsGroupEnable(AtCiscoUpsr self, at_uint32 apsGroupId, at_uint32 enable)
    {
    mAssert(self);
    if (!ApsGroupIdIsValid(self, apsGroupId))
        return;

    mMethodsGet(self).HwTxApsGroupEnable(self, apsGroupId, enable);
    }

/**
 * Check if APS group is enabled/disabled
 *
 * @param self UPSR table instance
 * @param apsGroupId APS group ID
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
at_uint32 AtCiscoApsGroupIsEnabled(AtCiscoUpsr self, at_uint32 apsGroupId)
    {
    mAssert(self);
    if (!ApsGroupIdIsValid(self, apsGroupId))
        return 0;

    return mMethodsGet(self).HwTxApsGroupIsEnabled(self, apsGroupId);
    }

/**
 * Select label set of a group in Tx direction
 *
 * @param self UPSR table instance
 * @param hsGroupId HSPW group ID
 * @param labelSet @ref eAtCiscoPwGroupLabelSet "Group label set"
 */
void AtCiscoHsGroupTxLabelSetSelect(AtCiscoUpsr self, at_uint32 hsGroupId, eAtCiscoPwGroupLabelSet labelSet)
    {
    mAssert(self);
    if (!HsGroupIdIsValid(self, hsGroupId))
        return;

    mMethodsGet(self).HsGroupTxLabelSetSelect(self, hsGroupId, labelSet);
    }

/**
 * Get label set of a group in Tx direction
 *
 * @param self UPSR table instance
 * @param hsGroupId HSPW group ID
 *
 * @return @ref eAtCiscoPwGroupLabelSet "Group label set"
 */
eAtCiscoPwGroupLabelSet AtCiscoHsGroupTxSelectedLabelGet(AtCiscoUpsr self, at_uint32 hsGroupId)
    {
    mAssert(self);
    if (!HsGroupIdIsValid(self, hsGroupId))
        return cAtCiscoPwGroupLabelSetUnknown;

    return mMethodsGet(self).HsGroupTxSelectedLabelGet(self, hsGroupId);
    }

/**
 * Select label set of a group in Rx direction
 *
 * @param self UPSR table instance
 * @param hsGroupId HSPW group ID
 * @param labelSet @ref eAtCiscoPwGroupLabelSet "Group label set"
 */
void AtCiscoHsGroupRxLabelSetSelect(AtCiscoUpsr self, at_uint32 hsGroupId, eAtCiscoPwGroupLabelSet labelSet)
    {
    at_uint32 regAddr = cAf6Reg_cla_per_grp_enb_Base + hsGroupId + cHspwRxOffset;
    mAssert(self);

    if (!HsGroupIdIsValid(self, hsGroupId))
        return;

    AtCiscoUpsrHwWrite(self, regAddr, (labelSet == cAtCiscoPwGroupLabelSetPrimary) ? 0 : 1);
    regAddr = cAf6Reg_cla_per_grp_enb_Base + mMethodsGet(self).RxHsGroupPrimaryOffset(self, hsGroupId);
    AtCiscoUpsrHwWrite(self, regAddr, (labelSet == cAtCiscoPwGroupLabelSetPrimary) ? 1 : 0);
    }

/**
 * Get label set of a group in Rx direction
 *
 * @param self UPSR table instance
 * @param hsGroupId HSPW group ID
 *
 * @return @ref eAtCiscoPwGroupLabelSet "Group label set"
 */
eAtCiscoPwGroupLabelSet AtCiscoHsGroupRxSelectedLabelGet(AtCiscoUpsr self, at_uint32 hsGroupId)
    {
    at_uint32 regVal;
    at_uint32 regAddr = cAf6Reg_cla_per_grp_enb_Base + hsGroupId + cHspwRxOffset;
    mAssert(self);

    if (!HsGroupIdIsValid(self, hsGroupId))
        return cAtCiscoPwGroupLabelSetUnknown;

    regVal = AtCiscoUpsrHwRead(self, regAddr);
    return (regVal == 0) ? cAtCiscoPwGroupLabelSetPrimary : cAtCiscoPwGroupLabelSetBackup;
    }

/**
 * Get the latest error when UPSR methods were executed.
 *
 * @param self UPSR table instance
 *
 * @return @ref eAtCiscoUpsrRet "Error code"
 */
eAtCiscoUpsrRet AtCiscoUpsrErrorCode(AtCiscoUpsr self)
    {
    mAssert(self);
    return self->latchedError;
    }

/**
 * Get product code
 *
 * @param self UPSR table instance
 *
 * @return product code
 */
at_uint32 AtCiscoUpsrProductCodeGet(AtCiscoUpsr self)
    {
    mAssert(self);
    return self->productCode;
    }

/**
 * Get FPGA version
 *
 * @param self UPSR table instance
 *
 * @return FPGA version
 */
at_uint32 AtCiscoUpsrFpgaVersion(AtCiscoUpsr self)
    {
    mAssert(self);
    return self->version;
    }

/**
 * Get FPGA built number
 *
 * @param self UPSR table instance
 *
 * @return FPGA built number
 */
at_uint32 AtCiscoUpsrFpgaBuiltNumber(AtCiscoUpsr self)
    {
    mAssert(self);
    return self->builtNumber;
    }

/**
 * @}
 */


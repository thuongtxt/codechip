/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS example
 *
 * File        : AtCiscoPrbsExample.c
 *
 * Created Date: Apr 23, 2015
 *
 * Description : Cisco PRBS sample code implementation
 *               There are five main functions in this file:
 *               - De1PrbsEngineSetup: To set up a PRBS engine for a specific DE1 channel
 *               - De3PrbsEngineSetup: To set up a PRBS engine for a specific DE3 channel
 *               - StickyGet: To get PRBS sticky
 *               - AllCountersShow: To print all supported counters
 *               - ForceError: To force error
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDriver.h"
#include "AtDevice.h"
#include "AtPrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mSuccessAssert(ret)                                                    \
    do                                                                         \
        {                                                                      \
        eAtRet ret__ = ret;                                                    \
        if (ret__ != cAtOk)                                                    \
            return ret__;                                                      \
        }while(0)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
eAtRet De1PrbsEngineSetup(uint32 engineId, AtPdhDe1 de1);
eAtRet De3PrbsEngineSetup(uint32 engineId, AtPdhDe3 de3);
eAtRet PrbsEngineDelete(uint32 engineId);
uint32 StickyGet(uint32 engineId, eBool read2Clear);
void AllCountersShow(uint32 engineId, eBool read2Clear);
void ForceError(uint32 engineId);

/*--------------------------- Implementation ---------------------------------*/
static AtDevice DeviceGet(void)
    {
    return AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
    }

static AtModulePrbs PrbsModule(AtDevice device)
    {
    return (AtModulePrbs)AtDeviceModuleGet(device, cAtModulePrbs);
    }

static eAtRet CustomizePrbsConfiguration(AtPrbsEngine engine)
    {
    /* All of attributes can configure on each direction separately.
     * See AtPrbsEngine.h for more detail */
    mSuccessAssert(AtPrbsEngineModeSet(engine, cAtPrbsModePrbs15));
    mSuccessAssert(AtPrbsEngineInvert(engine, cAtFalse));
    mSuccessAssert(AtPrbsEngineBitOrderSet(engine, cAtPrbsBitOrderMsb));

    /* Default PRBS monitoring side is TDM side, mean monitor at Rx TDM side.
     * There is another option to monitor at Rx PSN direction. Call the following
     * API with cAtPrbsSidePsn option to change monitoring side */
    mSuccessAssert(AtPrbsEngineMonitoringSideSet(engine, cAtPrbsSideTdm));
    return cAtOk;
    }

static AtPrbsEngine PrbsEngine(uint32 engineId)
    {
    AtModulePrbs prbsModule = PrbsModule(DeviceGet());
    return AtModulePrbsEngineGet(prbsModule, engineId);
    }

/*
 * Example function to set up a PRBS engine for a specific DE1 channel
 *
 * @param engineId PRBS engine ID
 * @param de1 DE1 object
 *
 * @return AT return code
 */
eAtRet De1PrbsEngineSetup(uint32 engineId, AtPdhDe1 de1)
    {
    AtModulePrbs prbsModule = PrbsModule(DeviceGet());

    /* After create PRBS engine, this engine is configured default attributes (PRBS side, PRBS mode, invert mode,..) */
    AtPrbsEngine engine = AtModulePrbsDe1PrbsEngineCreate(prbsModule, engineId, de1);
    if (engine == NULL)
        return cAtErrorNullPointer;

    /* Customize configuration if need */
    mSuccessAssert(CustomizePrbsConfiguration(engine));

    /* Can enable each direction separately by AtPrbsEngineTxEnable and AtPrbsEngineRxEnable */
    mSuccessAssert(AtPrbsEngineEnable(engine, cAtTrue));

    return cAtOk;
    }

/*
 * Example function to set up a PRBS engine for a specific DE3 channel
 *
 * @param engineId PRBS engine ID
 * @param de3 DE3 object
 *
 * @return AT return code
 */
eAtRet De3PrbsEngineSetup(uint32 engineId, AtPdhDe3 de3)
    {
    AtModulePrbs prbsModule = PrbsModule(DeviceGet());

    /* After create PRBS engine, this engine is configured default attributes (PRBS side, PRBS mode, invert mode,..) */
    AtPrbsEngine engine = AtModulePrbsDe3PrbsEngineCreate(prbsModule, engineId, de3);
    if (engine == NULL)
        return cAtErrorNullPointer;

    /* Customize configuration if need */
    mSuccessAssert(CustomizePrbsConfiguration(engine));

    /* Can enable each direction separately by AtPrbsEngineTxEnable and AtPrbsEngineRxEnable */
    mSuccessAssert(AtPrbsEngineEnable(engine, cAtTrue));

    return cAtOk;
    }

/*
 * Example function to delete a PRBS engine
 *
 * @param engineId PRBS engine ID
 *
 * @return AT return code
 */
eAtRet PrbsEngineDelete(uint32 engineId)
    {
    AtModulePrbs prbsModule = PrbsModule(DeviceGet());
    return AtModulePrbsEngineDelete(prbsModule, engineId);
    }

/*
 * Example function to get PRBS sticky
 *
 * @param engineId PRBS engine ID
 * @param read2Clear Read to clear mode
 *                   + cAtTrue: read to clear
 *                   + cAtFalse: read only
 *
 * @return sticky
 */
uint32 StickyGet(uint32 engineId, eBool read2Clear)
    {
    AtPrbsEngine prbsEngine = PrbsEngine(engineId);
    return (read2Clear) ? AtPrbsEngineAlarmHistoryClear(prbsEngine) : AtPrbsEngineAlarmHistoryGet(prbsEngine);
    }

/*
 * Example function to print all supported counters
 *
 * @param engineId PRBS engine ID
 * @param read2Clear Read to clear mode
 *                   + cAtTrue: read to clear
 *                   + cAtFalse: read only
 *
 * @return None
 */
void AllCountersShow(uint32 engineId, eBool read2Clear)
    {
    AtPrbsEngine engine = PrbsEngine(engineId);
    uint32 (*CounterGet)(AtPrbsEngine self, uint16 counterType);

    CounterGet = (read2Clear) ? AtPrbsEngineCounterClear : AtPrbsEngineCounterGet;

    /* Latch all counters */
    AtPrbsEngineAllCountersLatch(engine);

    /* Not all counter types in eAtPrbsEngineCounterType are supported.
     * Call AtPrbsEngineCounterIsSupported to check whether one counter type is applicable */
    AtPrintc(cSevInfo, "Tx bit      : %d", CounterGet(engine, cAtPrbsEngineCounterTxBit));
    AtPrintc(cSevInfo, "Rx bit      : %d", CounterGet(engine, cAtPrbsEngineCounterRxBit));
    AtPrintc(cSevInfo, "Rx sync bit : %d", CounterGet(engine, cAtPrbsEngineCounterRxSync));
    AtPrintc(cSevInfo, "Rx error bit: %d", CounterGet(engine, cAtPrbsEngineCounterRxBitError));
    }

/*
 * Example function to force error
 *
 * @param engineId PRBS engine ID
 *
 * @return None
 */
void ForceError(uint32 engineId)
    {
    AtPrbsEngine engine = PrbsEngine(engineId);

    /* Set error rate */
    AtPrbsEngineTxErrorRateSet(engine, cAtBerRate1E3);

    /* Force error with above rate */
    AtPrbsEngineErrorForce(engine, cAtTrue);

    /* Disable forcing error */
    AtPrbsEngineErrorForce(engine, cAtFalse);

    /* Force one single error */
    AtPrbsEngineTxErrorInject(engine, 1);
    }

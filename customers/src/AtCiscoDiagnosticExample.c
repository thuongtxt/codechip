/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Cisco sample code
 *
 * File        : AtCiscoDiagnosticExample.c
 *
 * Created Date: Mar 9, 2015
 *
 * Description : Cisco sample code implementation
 *               There are two main functions in this file:
 *               - DiagnosticSetup: to emulate all of VC-12s of one line over
 *                 UDP/IP via a specified ETH Port.
 *               - DiagnosticTearDown: To tear down all of emulated circuits
 *                 setup by the DiagnosticSetup()
 *
 * Notes       : Developers may refer TODO tags in this source file to change
 *               configuration on PSN header or PW resource allocation.
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDriver.h"
#include "AtEthPort.h"
#include "AtSdhLine.h"
#include "AtSdhAug.h"
#include "AtSdhVc.h"
#include "AtSdhTug.h"
#include "AtPw.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumAug4InAug16 4
#define cNumAug1InAug16 16
#define cNumVc3InAug1   3
#define cNumTug2InVc3   7
#define cNumVc12InTug2  3

/*--------------------------- Macros -----------------------------------------*/
#define mSuccessAssert(ret)                                                    \
    do                                                                         \
        {                                                                      \
        eAtRet ret__ = ret;                                                    \
        if (ret__ != cAtOk)                                                    \
            return ret__;                                                      \
        }while(0)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
eAtRet DiagnosticTearDown(uint8 sdhLineId, uint8 ethPortId);
eAtRet DiagnosticSetup(uint8 sdhLineId, uint8 ethPortId);

/*--------------------------- Implementation ---------------------------------*/
static AtDevice DeviceGet(void)
    {
    return AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
    }

static AtModuleSdh SdhModule(AtDevice device)
    {
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    }

static AtModuleEth EthModule(AtDevice device)
    {
    return (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    }

static AtModulePw PwModule(AtDevice device)
    {
    return (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static uint16 PwIdFromSdhChannelId(uint16 aug1Id, uint8 vc3Id, uint8 tug2Id, uint8 vc1xId)
    {
    /* TODO: just an example, user can replace it by another formula */
    return (uint16)((aug1Id * 63) + (vc3Id * 21) + (tug2Id * 3)  + vc1xId);
    }

static uint8 *DefaultEthPortSourceMac(uint32 ethPortId)
    {
    /* TODO: just an example, user can replace it by another value */
    static uint8 buf[6] = {0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA};
    AtUnused(ethPortId);
    return buf;
    }

static uint8 *DefaultEthPortSourceIpv4(uint32 ethPortId)
    {
    /* TODO: just an example, user can replace it by another value */
    static uint8 buf[4] = {172, 0, 0, 0};
    AtUnused(ethPortId);
    return buf;
    }

static uint8 *DefaultPwDestMac(uint32 ethPortId)
    {
    /* TODO: just an example, user can replace it by another value */
    static uint8 buf[6] = {0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA};
    AtUnused(ethPortId);
    return buf;
    }

static uint8 *DefaultPwSourceIpv4(uint32 ethPortId)
    {
    /* TODO: just an example, user can replace it by another value */
    static uint8 buf[4] = {172, 0, 0, 0};
    AtUnused(ethPortId);
    return buf;
    }

static uint8 *DefaultPwDestIpv4(uint32 ethPortId)
    {
    /* TODO: just an example, user can replace it by another value */
    static uint8 buf[4] = {172, 0, 0, 0};
    AtUnused(ethPortId);
    return buf;
    }

static uint16 DefaultPwUdpSourcePort(uint32 pwId)
    {
    /* TODO: just an example, user can replace it by another value */
    return (uint16)pwId;
    }

static uint16 DefaultPwUdpDestPort(uint32 pwId)
    {
    /* TODO: just an example, user can replace it by another value */
    return (uint16)pwId;
    }

static uint16 DefaultPwUdpExpectedPort(uint32 pwId)
    {
    /* TODO: just an example, user can replace it by another value */
    return (uint16)pwId;
    }

static uint16 DefaultPwPayloadSize(uint32 pwId)
    {
    AtUnused(pwId);
    /* TODO: just an example, user can replace it by another value */
    return 140;
    }

static AtPwPsn UdpIpv4PsnCreate(uint32 pwId, uint8 ethPortId)
    {
    AtPwUdpPsn udpPsn   = AtPwUdpPsnNew();
    AtPwIpV4Psn ipv4Psn = AtPwIpV4PsnNew();

    /* Setup UDP */
    AtPwUdpPsnSourcePortSet(udpPsn, DefaultPwUdpSourcePort(pwId));
    AtPwUdpPsnDestPortSet(udpPsn, DefaultPwUdpDestPort(pwId));
    AtPwUdpPsnExpectedPortSet(udpPsn, DefaultPwUdpExpectedPort(pwId));

    /* Setup IP */
    AtPwIpPsnSourceAddressSet((AtPwIpPsn)ipv4Psn, DefaultPwSourceIpv4(ethPortId));
    AtPwIpPsnDestAddressSet((AtPwIpPsn)ipv4Psn, DefaultPwDestIpv4(ethPortId));

    /* TODO: can set other attributes of each PSN here ... */

    /* Build PSN hierarchy */
    AtPwPsnLowerPsnSet((AtPwPsn)udpPsn, (AtPwPsn)ipv4Psn);

    return (AtPwPsn)udpPsn;
    }

/*
 * Example function to emulate all of VC-12s of one line over UDP/IP via a
 * specified ETH Port
 *
 * @param sdhLineId SDH Line ID
 * @param ethPortId ETH Port ID
 *
 * @return AT return code
 */
eAtRet DiagnosticSetup(uint8 sdhLineId, uint8 ethPortId)
    {
    uint8 aug4Id;
    uint8 aug1Id;
    AtPwPsn psn;
    AtDevice device = DeviceGet();
    AtModulePw  pwModule  = PwModule(device);
    AtEthPort ethPort = AtModuleEthPortGet(EthModule(device), ethPortId);
    AtSdhLine line = AtModuleSdhLineGet(SdhModule(device), sdhLineId);
    AtSdhAug aug16 = AtSdhLineAug16Get(line, 0);

    mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel)aug16, cAtSdhAugMapTypeAug16Map4xAug4s));

    /* Make AUG-4 contains AUG-1s */
    for (aug4Id = 0; aug4Id < cNumAug4InAug16; aug4Id++)
        {
        AtSdhAug aug4 = AtSdhLineAug4Get(line, aug4Id);
        mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel)aug4, cAtSdhAugMapTypeAug4Map4xAug1s));
        }

    /* Make each AUG-1 contains AU-3 <--> VC-3 */
    for (aug1Id = 0; aug1Id < cNumAug1InAug16; aug1Id++)
        {
        uint8 vc3Id;
        AtSdhAug aug1 = AtSdhLineAug1Get(line, aug1Id);
        mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel)aug1, cAtSdhAugMapTypeAug1Map3xVc3s));

        /* Make each VC-3 contains TUG-2s */
        for (vc3Id = 0; vc3Id < cNumVc3InAug1; vc3Id++)
            {
            AtSdhVc vc3 = AtSdhLineVc3Get(line, aug1Id, vc3Id);
            uint8 tug2Id;
            mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel)vc3, cAtSdhVcMapTypeVc3Map7xTug2s));

            /* Make each TUG-2 contains Tu12s */
            for (tug2Id = 0; tug2Id < cNumTug2InVc3; tug2Id++)
                {
                uint32 pwId;
                uint8 vc1xId;
                AtSdhTug tug2 = AtSdhLineTug2Get(line, aug1Id, vc3Id, tug2Id);
                mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel)tug2, cAtSdhTugMapTypeTug2Map3xTu12s));

                /* For each VC12, emulate over PSN */
                for (vc1xId = 0; vc1xId < cNumVc12InTug2; vc1xId++)
                    {
                    AtPw pw;
                    AtSdhVc vc12 = AtSdhLineVc1xGet(line, aug1Id, vc3Id, tug2Id, vc1xId);
                    mSuccessAssert(AtSdhChannelMapTypeSet((AtSdhChannel)vc12, cAtSdhVcMapTypeVc1xMapC1x));

                    pwId = PwIdFromSdhChannelId(aug1Id, vc3Id, tug2Id, vc1xId);
                    pw   = (AtPw)AtModulePwCepCreate(pwModule, pwId, cAtPwCepModeBasic);

                    mSuccessAssert(AtPwPayloadSizeSet(pw, DefaultPwPayloadSize(pwId)));
                    mSuccessAssert(AtPwCircuitBind(pw, (AtChannel)vc12));
                    mSuccessAssert(AtPwEthPortSet(pw, ethPort));

                    /* Configure Ethernet header with default MAC and no VLAN */
                    mSuccessAssert(AtPwEthHeaderSet(pw, DefaultPwDestMac(ethPortId), NULL, NULL));

                    /* Create UDP over IPv4 PSN */
                    psn = UdpIpv4PsnCreate(pwId, ethPortId);
                    mSuccessAssert(AtPwPsnSet(pw, psn));

                    mSuccessAssert(AtChannelEnable((AtChannel)pw, cAtTrue));

                    /* Clean objects */
                    AtObjectDelete((AtObject)psn);
                    }
                }
            }
        }

    /* Set Source MAC and IP for Ethernet Port */
    mSuccessAssert(AtEthPortSourceMacAddressSet(ethPort, DefaultEthPortSourceMac(ethPortId)));
    mSuccessAssert(AtEthPortIpV4AddressSet(ethPort, DefaultEthPortSourceIpv4(ethPortId)));
    mSuccessAssert(AtChannelLoopbackSet((AtChannel)ethPort, cAtLoopbackModeRelease));

    return cAtOk;
    }

/*
 * To tear down all of emulated circuits setup by the DiagnosticSetup()
 *
 * @param sdhLineId SDH Line ID
 * @param ethPortId ETh Port ID
 *
 * @return AT return code
 */
eAtRet DiagnosticTearDown(uint8 sdhLineId, uint8 ethPortId)
    {
    uint8 aug1Id;
    AtModulePw pwModule = PwModule(DeviceGet());
    AtUnused(ethPortId);
    AtUnused(sdhLineId);

    for (aug1Id = 0; aug1Id < cNumAug1InAug16; aug1Id++)
        {
        uint8 vc3Id;

        for (vc3Id = 0; vc3Id < cNumVc3InAug1; vc3Id++)
            {
            uint8 tug2Id;

            for (tug2Id = 0; tug2Id < cNumTug2InVc3; tug2Id++)
                {
                uint8 vc1xId;

                for (vc1xId = 0; vc1xId < cNumVc12InTug2; vc1xId++)
                    {
                    uint16 pwId = PwIdFromSdhChannelId(aug1Id, vc3Id, tug2Id, vc1xId);
                    AtPw pw = (AtPw)AtModulePwGetPw(pwModule, pwId);
                    if (pw == NULL)
                        continue;

                    mSuccessAssert(AtPwCircuitUnbind(pw));
                    mSuccessAssert(AtModulePwDeletePw(pwModule, pwId));
                    }
                }
            }
        }

    return cAtOk;
    }

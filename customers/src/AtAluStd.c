/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ALU
 *
 * File        : AtAluStd.c
 *
 * Created Date: Dec 30, 2014
 *
 * Description : ALU STD
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCommon.h"
#include "../../util/std/AtStdDefaultInternal.h"
#include "AtOsal.h"
#include "AtStd.h"
#include "AtCliModule.h"
#include "AtAlu.h"

/*--------------------------- Define -----------------------------------------*/
#define cMsgBufferLength     1024
#define cInvalidShellHandler -1

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtAluStd * AtAluStd;

typedef struct tAtAluStd
    {
    tAtStdDefault super;

    int shellHandler;
    int (*printFunc)(int shellHandler, const char *aString);
    char sharedMsgBuffer[cMsgBufferLength];
    }tAtAluStd;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtStdMethods m_AtStdOverride;

static const tAtStdMethods * m_AtStdMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static char* SharedMessageBufferGet(AtAluStd self, uint32* bufferLength)
    {
    if (bufferLength)
        *bufferLength = cMsgBufferLength;

    AtOsalMemInit(self->sharedMsgBuffer, 0, cMsgBufferLength);
    return self->sharedMsgBuffer;
    }

static eBool ShouldRedirectPrintf(AtAluStd self)
    {
    if ((self->printFunc == NULL) || (self->shellHandler == cInvalidShellHandler))
        return cAtFalse;
    return cAtTrue;
    }

AtAttributePrintf(2, 0)
static uint32 Printf(AtStd self, const char *format, va_list ap)
    {
    AtAluStd aluStd = (AtAluStd)self;
    char* buffer;
    uint32 bufferSize;

    if (!ShouldRedirectPrintf(aluStd))
        return m_AtStdMethods->Printf(self, format, ap);

    buffer = SharedMessageBufferGet(aluStd, &bufferSize);

    /* Build message buffer */
    AtStdSnprintf(self, buffer, bufferSize - 1, format, ap);
    buffer[bufferSize - 1] = '\0';

    return (uint32)(aluStd->printFunc(aluStd->shellHandler, (const char*)buffer));
    }

static void OverrideAtStd(AtStd self)
    {
    if (!m_methodsInit)
        {
        m_AtStdMethods = mMethodsGet(self);
        AtOsalMemCpy(&m_AtStdOverride, m_AtStdMethods, sizeof(m_AtStdOverride));

        mMethodOverride(m_AtStdOverride, Printf);
        }

    mMethodsSet(self, &m_AtStdOverride);
    }

static void Override(AtStd self)
    {
    OverrideAtStd(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtAluStd);
    }

static AtStd ObjectInit(AtStd self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtStdDefaultObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    ((AtAluStd)self)->shellHandler = cInvalidShellHandler;

    return self;
    }

static AtStd SharedStd(void)
    {
    static tAtAluStd m_std;
    static AtStd m_sharedStd = NULL;

    if (m_sharedStd == NULL)
        m_sharedStd = ObjectInit((AtStd)&m_std);

    return m_sharedStd;
    }

/**
 * @addtogroup AtCustomer
 * @{
 */

/**
 * Set print function
 *
 * @param printFunc Print function
 * @return AT return code
 */
eAtRet AtAluPrintFuncSet(int (*printFunc)(int shellHandler, const char *aString))
    {
    AtAluStd aluStd = (AtAluStd)SharedStd();
    if (aluStd == NULL)
        return cAtError;

    aluStd->printFunc = printFunc;
    return cAtOk;
    }

/**
 * Execute command string
 *
 * @param shellHandler Shell handler
 * @param cmdString Command string
 *
 * @retval  0: success
 * @retval -1: fail
 */
int AtAluCliExecute(int shellHandler, const char *cmdString)
    {
    AtAluStd aluStd = (AtAluStd)SharedStd();

    if (aluStd)
        {
        aluStd->shellHandler = shellHandler;
        AtStdSharedStdSet((AtStd)aluStd);
        }

    return AtCliExecute(cmdString);
    }

/**
 * @}
 */

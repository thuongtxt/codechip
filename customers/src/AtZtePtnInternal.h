/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APP
 * 
 * File        : AtZtePtnInternal.h
 * 
 * Created Date: Oct 28, 2013
 *
 * Description : Common include
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATZTEPTNINTERNAL_H_
#define _ATZTEPTNINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "atclib.h"

#include "AtDevice.h"
#include "AtZtePtn.h"
#include "AtPw.h"
#include "AtModuleSdh.h"
#include "AtSdhLine.h"
#include "AtModuleEth.h"
#include "AtEthPort.h"
#include "AtModuleClock.h"
#include "AtModuleRam.h"
#include "AtRam.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool AtZtePtnTag1IsValid(const tAtZtePtnTag1 *tag1);
eBool AtZtePtnTag2IsValid(const tAtZtePtnTag2 *tag2);

tAtEthVlanTag *AtZtePtnSVlanFromTag1(const tAtZtePtnTag1 *zteTag1, tAtEthVlanTag *vlanTag);
tAtEthVlanTag *AtZtePtnCVlanFromTag2(const tAtZtePtnTag2 *tag2, tAtEthVlanTag *cVlan);
tAtZtePtnTag1 *AtZtePtnTag1FromSVlan(const tAtEthVlanTag *sVlan, tAtZtePtnTag1 *tag1);
tAtZtePtnTag2 *AtZtePtnTag2FromCVlan(const tAtEthVlanTag *cVlan, tAtZtePtnTag2 *tag2);

#ifdef __cplusplus
}
#endif
#endif /* _ATZTEPTNINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APP
 *
 * File        : AtZtePtnPpp.c
 *
 * Created Date: Oct 28, 2013
 *
 * Description : To implement lookup method for ZTE PTN PPP/MLPPP products
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPidTable.h"
#include "AtModulePpp.h"
#include "AtZtePtnInternal.h"
#include "../../driver/src/implement/default/eth/ThaModuleEth.h"
#include "../../driver/src/implement/codechip/Tha60031032/ppp/Tha60031032PidTable.h"
#include "../../driver/src/implement/codechip/Tha60091132/eth/Tha60091132ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet IngressTagsAdd(AtEthFlow self, uint8 portId, const tAtZtePtnTag1 *zteTag1, const tAtZtePtnTag2 *zteTag2)
    {
    tAtEthVlanDesc desc;
    tAtEthVlanTag cVlan, sVlan;

    AtOsalMemInit(&desc, 0x0, sizeof(desc));
    if ((AtZtePtnSVlanFromTag1(zteTag1, &sVlan) == NULL) ||
        (AtZtePtnCVlanFromTag2(zteTag2, &cVlan) == NULL))
        return cAtErrorInvlParm;
    AtEthFlowVlanDescConstruct(portId, &sVlan, &cVlan, &desc);

    return AtEthFlowIngressVlanAdd(self, &desc);
    }

static eAtRet IngressTagsRemove(AtEthFlow self, uint8 portId, const tAtZtePtnTag1 *zteTag1, const tAtZtePtnTag2 *zteTag2)
    {
    tAtEthVlanTag cVlan, sVlan;
    tAtEthVlanDesc desc;

    if ((AtZtePtnSVlanFromTag1(zteTag1, &sVlan) == NULL) ||
        (AtZtePtnCVlanFromTag2(zteTag2, &cVlan) == NULL))
        return cAtErrorInvlParm;

    AtEthFlowVlanDescConstruct(portId, &sVlan, &cVlan, &desc);

    return AtEthFlowIngressVlanRemove(self, &desc);
    }

static AtPidTable PidTable(AtDevice self)
    {
    AtModulePpp pppModule = (AtModulePpp)AtDeviceModuleGet(self, cAtModulePpp);
    return AtModulePppPidTableGet(pppModule);
    }

static AtPidTableEntry PidEntry(AtDevice self, uint16 entryIndex)
    {
    return AtPidTableEntryGet(PidTable(self), entryIndex);
    }

static eBool PktTypeHasSupport(eAtZtePtnCHdlcPktType typeValue)
    {
    if ((typeValue == cAtZtePtnDataType) ||
        (typeValue == cAtZtePtnISISType) ||
        (typeValue == cAtZtePtnOamType))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet FrameRelayAttributeSet(AtDevice self,
                                     uint8 value,
                                     eAtRet (*attributeFunc)(AtPidTable self, uint8 value))
    {
    AtPidTable pidTable = PidTable(self);

    if (pidTable == NULL)
        return cAtErrorNullPointer;

    return attributeFunc(pidTable, value);
    }

static uint8 FrameRelayAttributeGet(AtDevice self, uint8 (*attributeFunc)(AtPidTable self))
    {
    AtPidTable pidTable = PidTable(self);

    if (pidTable == NULL)
        return cAtErrorNullPointer;

    return attributeFunc(pidTable);
    }

static eBool ZteTagIsValid(const tAtZtePtnTag1 *zteTag1, const tAtZtePtnTag2 *zteTag2)
    {
    if (!AtZtePtnTag1IsValid(zteTag1) || !AtZtePtnTag2IsValid(zteTag2))
        return cAtFalse;
    return cAtTrue;
    }

static eBool Eth10GIsSupported(AtDevice device)
    {
    return ThaModuleEthPort10GbSupported((ThaModuleEth)AtDeviceModuleGet(device, cAtModuleEth));
    }

eAtRet AtZteEthFlowTxHeaderSet(AtEthFlow self, uint8 portId, const tAtZtePtnTag1 *zteTag1, const tAtZtePtnTag2 *zteTag2)
    {
    tAtEthVlanDesc desc;
    tAtEthVlanTag cVlan, sVlan;

    /* Init value */
    AtOsalMemInit(&desc, 0x0, sizeof(desc));

    if ((AtZtePtnSVlanFromTag1(zteTag1, &sVlan) == NULL) ||
        (AtZtePtnCVlanFromTag2(zteTag2, &cVlan) == NULL))
        return cAtErrorInvlParm;

    AtEthFlowVlanDescConstruct(portId, &sVlan, &cVlan, &desc);
    return AtEthFlowEgressVlanSet(self, &desc);
    }

eAtRet AtZteEthFlowExpectedHeaderSet(AtEthFlow self, uint8 portId, const tAtZtePtnTag1 *zteTag1, const tAtZtePtnTag2 *zteTag2)
    {
    tAtZtePtnTag1 oldTag1, *pOldTag1;
    tAtZtePtnTag2 oldTag2, *pOldTag2;
    eAtRet ret = cAtOk;

    if (!ZteTagIsValid(zteTag1, zteTag2))
        return cAtErrorInvlParm;

    /* Backup first */
    pOldTag1 = AtZtePtnEthFlowExpectedTag1Get(self, &oldTag1);
    pOldTag2 = AtZtePtnEthFlowExpectedTag2Get(self, &oldTag2);

    /* Remove current configuration */
    if (pOldTag1 && pOldTag2)
        {
        ret = IngressTagsRemove(self, portId, pOldTag1, pOldTag2);
        if (ret != cAtOk)
            return ret;
        }

    /* Add the new one */
    ret = IngressTagsAdd(self, portId, zteTag1, zteTag2);

    /* Revert on failure */
    if ((ret != cAtOk) && pOldTag1 && pOldTag2)
        ret |= IngressTagsAdd(self, portId, pOldTag1, pOldTag2);

    return ret;
    }

tAtZtePtnTag1 *AtZtePtnEthFlowTag1Get(AtEthFlow self, tAtZtePtnTag1 *zteTag1)
    {
    tAtEthVlanDesc desc;

    if (AtEthFlowEgressVlanGet(self, &desc) != cAtOk)
        return NULL;

    return AtZtePtnTag1FromSVlan(AtEthVlanDescSVlan(&desc), zteTag1);
    }

tAtZtePtnTag2 *AtZtePtnEthFlowTag2Get(AtEthFlow self, tAtZtePtnTag2 *zteTag2)
    {
    tAtEthVlanDesc desc;

    if (AtEthFlowEgressVlanGet(self, &desc) != cAtOk)
        return NULL;

    return AtZtePtnTag2FromCVlan(AtEthVlanDescCVlan(&desc), zteTag2);
    }

tAtZtePtnTag1 *AtZtePtnEthFlowExpectedTag1Get(AtEthFlow self, tAtZtePtnTag1 *tag1)
    {
    tAtEthVlanDesc desc;

    if (AtEthFlowIngressNumVlansGet(self) == 0)
        return NULL;

    AtEthFlowIngressAllVlansGet(self, &desc);
    return AtZtePtnTag1FromSVlan(AtEthVlanDescSVlan(&desc), tag1);
    }

tAtZtePtnTag2 *AtZtePtnEthFlowExpectedTag2Get(AtEthFlow self, tAtZtePtnTag2 *tag2)
    {
    tAtEthVlanDesc desc;

    if (AtEthFlowIngressNumVlansGet(self) == 0)
        return NULL;

    AtEthFlowIngressAllVlansGet(self, &desc);
    return AtZtePtnTag2FromCVlan(AtEthVlanDescCVlan(&desc), tag2);
    }

uint16 AtZtePtnPppNumLookupEntries(AtDevice self)
    {
    return (uint16)AtPidTableMaxNumEntries(PidTable(self));
    }

eAtRet AtZtePtnPppLookupEntrySet(AtDevice self, uint16 lookupIndex, uint16 pidValue, uint16 ethType, uint8 zteEncapType)
    {
    eAtRet ret = cAtOk;
    AtPidTableEntry entry;

    /* Have the entry */
    entry = AtPidTableEntryGet(PidTable(self), lookupIndex);
    if (entry == NULL)
        return cAtErrorInvlParm;

    if (!Tha60031032PidTableEntryIsLcp(PidTable(self), lookupIndex))
        {
        ret |= AtPidTableEntryPidSet(entry, pidValue);

        if (AtZtePtnPppLookupEntryHasEthType(self, lookupIndex))
           ret |= AtPidTableEntryEthTypeSet(entry, ethType);
        }

    ret |= Tha60031032PppEncapTypeSet(PidTable(self), (uint8)lookupIndex, zteEncapType);
    return ret;
    }

eAtRet AtZtePtnPppLookupEntryEnable(AtDevice self, uint16 lookupIndex, eBool enable)
    {
    eAtRet ret = cAtOk;

    ret |= AtPidTableEntryPsnToTdmEnable(PidEntry(self, lookupIndex), enable);
    ret |= AtPidTableEntryTdmToPsnEnable(PidEntry(self, lookupIndex), enable);

    return ret;
    }

eBool  AtZtePtnPppLookupEntryIsEnabled(AtDevice self, uint16 lookupIndex)
    {
    return AtPidTableEntryTdmToPsnIsEnabled(PidEntry(self, lookupIndex));
    }

uint16 AtZtePtnPppLookupEntryPidGet(AtDevice self, uint16 lookupIndex)
    {
    return (uint16)AtPidTableEntryTdmToPsnPidGet(PidEntry(self, lookupIndex));
    }

uint16 AtZtePtnPppLookupEntryEthTypeGet(AtDevice self, uint16 lookupIndex)
    {
    return (uint16)AtPidTableEntryTdmToPsnEthTypeGet(PidEntry(self, lookupIndex));
    }

uint16 AtZtePtnPppLookupEntryEncapTypeGet(AtDevice self, uint16 lookupIndex)
    {
    if (PidEntry(self, lookupIndex) == NULL)
        return cInvalidUint8;

    return Tha60031032PidTableEncapTypeGet(PidTable(self), (uint8)lookupIndex);
    }

eAtRet AtZtePtnPppLookupEntryCfiSet(AtDevice self, uint16 lookupIndex, uint8 cfi)
    {
    return Tha60031032PidTableCfiSet(PidTable(self), (uint8)lookupIndex, cfi);
    }

uint8 AtZtePtnPppLookupEntryCfiGet(AtDevice self, uint16 lookupIndex)
    {
    return Tha60031032PidTableCfiGet(PidTable(self), lookupIndex);
    }

eAtRet AtZtePtnPppLookupEntryPrioritySet(AtDevice self, uint16 lookupIndex, uint8 priority)
    {
    return Tha60031032PidTablePrioritySet(PidTable(self), lookupIndex, priority);
    }

uint8 AtZtePtnPppLookupEntryPriorityGet(AtDevice self, uint16 lookupIndex)
    {
    return Tha60031032PidTablePriorityGet(PidTable(self), lookupIndex);
    }

eBool AtZtePtnPppLookupEntryHasEthType(AtDevice self, uint16 lookupIndex)
    {
    return Tha60031032PidTableEntryHasEthType(PidTable(self), lookupIndex);
    }

eAtRet AtZtePtnMlpppLookupEntryEncapTypeSet(AtDevice self, uint16 lookupIndex, uint8 zteEncapType)
    {
    return Tha60031032MlpppEncapTypeSet(PidTable(self), (uint8)lookupIndex, zteEncapType);
    }

uint16 AtZtePtnMlpppLookupEntryEncapTypeGet(AtDevice self, uint16 lookupIndex)
    {
    if (PidEntry(self, lookupIndex) == NULL)
        return cInvalidUint8;

    return Tha60031032MlpppEncapTypeGet(PidTable(self), (uint8)lookupIndex);
    }

eAtRet AtZtePtnCHdlcLookupEntryEncapTypeSet(AtDevice self, uint8 zteEncapType)
    {
    return Tha60031032CHdlcEncapTypeSet(PidTable(self), zteEncapType);
    }

uint16 AtZtePtnCHdlcLookupEntryEncapTypeGet(AtDevice self)
    {
    return Tha60031032CHdlcEncapTypeGet(PidTable(self));
    }

eAtRet AtZtePtnCHdlcLookupEntryCfiSet(AtDevice self, eAtZtePtnCHdlcPktType type, uint8 cfi)
    {
    if (!PktTypeHasSupport(type))
        {
        AtPrintc(cSevCritical, "ERROR: Packet type invalid, expected 0x0/0x4/0xF\r\n");
        return cAtErrorModeNotSupport;
        }

    return Tha60031032CHdlcPidTableCfiSet(PidTable(self), (uint16)type, cfi);
    }

uint8 AtZtePtnCHdlcLookupEntryCfiGet(AtDevice self, eAtZtePtnCHdlcPktType type)
    {
    return Tha60031032CHdlcPidTableCfiGet(PidTable(self), (uint16)type);
    }

eAtRet AtZtePtnCHdlcLookupEntryPrioritySet(AtDevice self, eAtZtePtnCHdlcPktType type, uint8 priority)
    {
    if (!PktTypeHasSupport(type))
        {
        AtPrintc(cSevCritical, "ERROR: Packet type invalid, expected 0x0/0x4/0xF\r\n");
        return cAtErrorModeNotSupport;
        }

    return Tha60031032CHdlcPidTablePrioritySet(PidTable(self), (uint16)type, priority);
    }

uint8 AtZtePtnCHdlcLookupEntryPriorityGet(AtDevice self, eAtZtePtnCHdlcPktType type)
    {
    return Tha60031032CHdlcPidTablePriorityGet(PidTable(self), (uint16)type);
    }

eAtRet AtZtePtnFrLookupEntryEncapTypeSet(AtDevice self, uint8 zteEncapType)
    {
    return FrameRelayAttributeSet(self, zteEncapType, Tha60031032FrEncapTypeSet);
    }

uint8 AtZtePtnFrLookupEntryEncapTypeGet(AtDevice self)
    {
    return FrameRelayAttributeGet(self, Tha60031032FrEncapTypeGet);
    }

eAtRet AtZtePtnFrLookupEntryPrioritySet(AtDevice self, uint8 priority)
    {
    return FrameRelayAttributeSet(self, priority, Tha60031032FrPidTablePrioritySet);
    }

uint8 AtZtePtnFrLookupEntryPriorityGet(AtDevice self)
    {
    return FrameRelayAttributeGet(self, Tha60031032FrPidTablePriorityGet);
    }

eAtRet AtZtePtnFrLookupEntryCfiSet(AtDevice self, uint8 cfi)
    {
    return FrameRelayAttributeSet(self, cfi, Tha60031032FrPidTableCfiSet);
    }

uint8 AtZtePtnFrLookupEntryCfiGet(AtDevice self)
    {
    return FrameRelayAttributeGet(self, Tha60031032FrPidTableCfiGet);
    }

eAtRet AtZtePtnEthFlowHigigTxHeaderSet(AtEthFlow self,
                                    uint8 ethPortId,
                                    uint8 stmPortId,
                                    const tAtZtePtnTag1 *zteTag1,
                                    const tAtZtePtnTag2 *zteTag2)
    {
    eAtRet ret;

    if (!Eth10GIsSupported(AtChannelDeviceGet((AtChannel)self)))
        return cAtOk;

    ret = Tha60091132EthFlowTxStmPortSet(self, stmPortId);
    if (ret != cAtOk)
        return ret;

    return AtZteEthFlowTxHeaderSet(self, ethPortId, zteTag1, zteTag2);
    }

uint8 AtZtePtnEthFlowHigigTxStmPortGet(AtEthFlow self)
    {
    if (!Eth10GIsSupported(AtChannelDeviceGet((AtChannel)self)))
        return cBit7_0;

    return Tha60091132EthFlowTxStmPortGet(self);
    }

eAtRet AtZtePtnEthFlowHigigExpectedHeaderSet(AtEthFlow self,
                                             uint8 ethPortId,
                                             uint8 stmPortId,
                                             const tAtZtePtnTag1 *zteTag1,
                                             const tAtZtePtnTag2 *zteTag2)
    {
    tAtZtePtnTag1 oldTag1, *pOldTag1;
    tAtZtePtnTag2 oldTag2, *pOldTag2;
    eAtRet ret;
    uint8 oldStmPortId;

    if (!Eth10GIsSupported(AtChannelDeviceGet((AtChannel)self)))
        return cAtOk;

    if (!ZteTagIsValid(zteTag1, zteTag2))
        return cAtErrorInvlParm;

    /* Backup first */
    oldStmPortId = AtZtePtnEthFlowHigigExpectedStmPortGet(self);
    pOldTag1     = AtZtePtnEthFlowExpectedTag1Get(self, &oldTag1);
    pOldTag2     = AtZtePtnEthFlowExpectedTag2Get(self, &oldTag2);

    /* Remove current configuration */
    if (pOldTag1 && pOldTag2)
        {
        ret = IngressTagsRemove(self, ethPortId, pOldTag1, pOldTag2);
        ret |= Tha60091132EthFlowExpectedStmPortClear(self, oldStmPortId, pOldTag2->isMlpppIma, pOldTag2->zteChannelId);
        if (ret != cAtOk)
            return ret;
        }

    ret = Tha60091132EthFlowExpectedStmPortSet(self, stmPortId, zteTag2->isMlpppIma, zteTag2->zteChannelId);
    if (ret != cAtOk)
        return ret;

    /* Add the new one */
    ret = IngressTagsAdd(self, ethPortId, zteTag1, zteTag2);

    /* Revert on failure */
    if ((ret != cAtOk) && pOldTag1 && pOldTag2)
        {
        ret |= Tha60091132EthFlowExpectedStmPortSet(self, oldStmPortId, pOldTag2->isMlpppIma, pOldTag2->zteChannelId);
        ret |= IngressTagsAdd(self, ethPortId, pOldTag1, pOldTag2);
        }

    return ret;
    }

uint8 AtZtePtnEthFlowHigigExpectedStmPortGet(AtEthFlow self)
    {
    if (!Eth10GIsSupported(AtChannelDeviceGet((AtChannel)self)))
        return cBit7_0;

    return Tha60091132EthFlowExpectedStmPortGet(self);
    }

/* $Id: free.c,v 1.1.1.1 2012-04-25 05:27:23 krreddy Exp $
 * $Source: /svn/sandbox/prajpai_dump_area/CVS/Repo1/diag/common/src/free.c,v $
 *------------------------------------------------------------------
 * Memory pool functions (malloc, free, etc.)
 *------------------------------------------------------------------
 */
#ifdef AT_NONE_OS
#include "strings.h"
#include "proto.h"
#else
#include <stdlib.h>
#include <string.h>
#endif

#include "mempool.h"
#include "common.h"
#include "pcmap.h"
#undef   DEBUG_MEMPOOLS_TABLE
#undef   DEBUG_MALLOC_FREE

#ifdef AT_NONE_OS
extern void free (void *ptr);
#endif

typedef unsigned char       uchar;  /* System V */
typedef unsigned short      ushort; /* System V */
typedef unsigned int        uint;   /* System V */
typedef unsigned long       ulong;  /* System V */
typedef int         boolean;

cisco_mempools_table_t fsl_mempool_tbl;

/*
 * Maintain a mempool struct corresponding to each type of memory
 */
cisco_mempool_t cisco_mempool[CISCO_MEMPOOL_CLASS_MAX];

/*
 * Descriptor that specifies the mempools table in use, if any, 
 * on the platform.  Initialized by a platform call to
 * init_mempools_from_table, the pointer to this descriptor is
 * global.
 */
static cisco_mempools_tbl_desc_t mempools_tbl_desc;
cisco_mempools_tbl_desc_t *cisco_mempools_tbl_desc_p = NULL;  /* optionally assigned */
static unsigned long memsize;
static unsigned long sizemainmem (void)
{

    if (memsize) { /* Memory Size */
        return (memsize);
    }

    return ((long)0);
}
static void dismem (unsigned char *addr, int length,
                    unsigned long disaddr, int fldsize)
    {
    (void)addr;
    (void)length;
    (void)disaddr;
    (void)fldsize;
    }
static void display_buf_info(void *);
static void
cisco_init_mempool (char *memname,
          cisco_mempool_class pool,
          void *membase,
          unsigned long nbytes);
void
cisco_add_region_to_mempool (cisco_mempool_class pool,
                       void *membase,
                       unsigned long nbytes);
void
cisco_display_mempool (cisco_mempool_class pool, char *header);
static void
cisco_reset_mempools (void);
void
cisco_restore_mempools (void);
static void *
cisco_malloc_pool (cisco_mempool_class class, unsigned long nbytes);
static void
cisco_init_mempools_from_table (cisco_mempools_table_t *tbl, unsigned long tbl_size,
                         unsigned long unavail_bytes);
void
cisco_show_mempools_table (void);
void *
cisco_malloc_all (unsigned long nbytes);
/*
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 * Function: cisco_init_mempool
 *
 * Store the arguments into the global mempool array corresponding to
 * to the given pool.  Set that memory pool to its initial state
 * according to the arguments.  Make the total extent of the
 * corresponding memory into a single free block pointing to itself.
 *
 * The size of a free block is recorded in the number of header-size units.
 * The given length of this pool, nbytes, is rounded down, if necessary,
 * to the next lower multiple of the number of bytes in a header (this
 * keeps balanced the allocated/free accounting shown in display_mempool).
 *
 * N.B.  For the present, initialize the optional *moremem function to
 *       NULL.  This field in the structure is a hook that could be
 *       used to add another region of memory to the original pool.
 */
void
cisco_init_mempool (char *memname,
	      cisco_mempool_class pool,
	      void *membase,
	      unsigned long nbytes)
{
    cisco_mempool_t *m;
    mempool_free_header_t *ptr;

#ifdef DEBUG_MALLOC_FREE
    char debug_title[80];
#endif /* DEBUG_MALLOC_FREE */

    if (nbytes == 0) {
	puts("\n*** init_mempool: ERROR -- given length is 0\n");
    }
    else if (pool > CISCO_MEMPOOL_CLASS_UNKNOWN && pool < CISCO_MEMPOOL_CLASS_MAX) {
	m = &cisco_mempool[pool];
	m->name = memname;
	m->class = pool;
	m->membase = membase;
	m->moremem = (void (*)())0;

	m->freep = ptr = (mempool_free_header_t*)membase;
	ptr->s.size = nbytes/sizeof(mempool_free_header_t);
	m->memlen = ptr->s.size * sizeof(mempool_free_header_t);
	ptr->s.freeptr = ptr;  /* now one big block pointing at itself */
	ptr->s.pool_id = (unsigned short)pool;

#ifdef DEBUG_MALLOC_FREE
	sprintf(debug_title, "init_mempool of %s (length = %d bytes)",
		memname, nbytes);
	cisco_display_mempool(pool, debug_title);
#endif /* DEBUG_MALLOC_FREE */

    } else {
	puts("\n*** init_mempool: ERROR -- unknown mempool class\n");
    }
}

/*
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 * Function: cisco_display_mempool
 *
 * Display in order of increasing addresses the current structure of a
 * specified mempool, heading the display with the given title.
 * Check the integrity of each block.
 */
void
cisco_display_mempool (cisco_mempool_class pool, char *header)
{
    int i;
    cisco_mempool_t *m;
    mempool_free_header_t *p, *freep;
    mempool_free_header_t *min_addr = (mempool_free_header_t *)0xFFFFFFFF;  /* init to max */
    unsigned long total_free = 0;

    if (!(pool > CISCO_MEMPOOL_CLASS_UNKNOWN && pool < CISCO_MEMPOOL_CLASS_MAX)) {
	printf("\ndisplay_mempool: ERROR -- given pool #%d is out of bounds\n",
	       pool);
	return;
    }
    m = &cisco_mempool[pool];
    if (m->memlen == 0) {
	printf("\ndisplay_mempool: ERROR -- pool #%d is uninitialized "
	       "(length is zero)\n", pool);
	return;
    }
    
    /*
     * Starting with the block pointed to by this pool's freep, 
     * find the block with the lowest address in the pool.
     */
    p = freep = m->freep;
    do {
        if (p < min_addr) {
            min_addr = p;
        }
        p = p->s.freeptr;
    } while (p != freep);

    printf("\n\nMempool %s: %s", m->name, header);
    printf("\n[base = %#.8x; len = %d bytes]", (unsigned int)(m->membase), (unsigned int)(m->memlen));
    puts("\n block    address        bytes");
    printf("   (freeptr = %#.8x)", (unsigned int)freep);
    for (p = min_addr, i = 1; ; p = p->s.freeptr, i++) {
	printf("\n  %2.d   %#.8x     %8d", i, (unsigned int)p,
	       (unsigned int)(p->s.size*sizeof(mempool_free_header_t)));

#ifdef DEBUG_MALLOC_FREE
	puts("\n");
	dismem((unsigned char *)p, sizeof(mempool_free_header_t), (unsigned)p, 4);
#endif /* DEBUG_MALLOC_FREE */
	
	if (p->s.pool_id != pool) {
	    printf("\nBLOCK DAMAGED -- padding/ID = %#.4x%.4x",
		   p->s.padding, p->s.pool_id);
	    return;
	}
	total_free += p->s.size;
	if (p->s.freeptr == min_addr) {
	    /*
	     * back to the beginning
	     */
	    break;
	}
    }
    total_free *= sizeof(mempool_free_header_t);
    printf("\nTotal bytes allocated = %d; free = %d\n",
	   (unsigned int)(m->memlen - total_free), (unsigned int)total_free);
}


/*
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 * Function: cisco_add_region_to_mempool
 *
 * Into the given existent pool (->memlen != 0), add the specified
 * region as if it were a newly freed malloc buffer, and increase
 * memlen accordingly.  This function could be called as part of the
 * execution of (*moremem)(), or directly, for example during the
 * platform's malloc init in the case where a pool type contains
 * regions that are not in consecutive memory locations.
 *
 * N.B.  Once this function is called to provide non-contiguous regions
 *       in any mempool, restore_mempools cannot be used for reinit of
 *       the platform's mempools.  In this case, the original malloc
 *       init function should be recalled instead.
 */
void
cisco_add_region_to_mempool (cisco_mempool_class pool,
                       void *membase,
                       unsigned long nbytes)
{
    cisco_mempool_t *m;
    mempool_free_header_t *ptr;

#ifdef DEBUG_MALLOC_FREE
    char debug_title[80];
#endif /* DEBUG_MALLOC_FREE */

    if (nbytes == 0) {
	puts("\n*** add_region_to_mempool: ERROR -- given length is 0\n");
    }
    else if (!(pool > CISCO_MEMPOOL_CLASS_UNKNOWN && pool < CISCO_MEMPOOL_CLASS_MAX)) {
        printf("\nadd_region_to_mempool: ERROR -- unknown mempool class = %d\n",
               pool);
    } else {
	m = &cisco_mempool[pool];
	if (m->memlen > 0) {
            ptr = membase;
            ptr->s.size = nbytes / sizeof(mempool_free_header_t);
            ptr->s.pool_id = (unsigned short)pool;
            ptr->s.freeptr = ptr;  /* header of malloc buffer now emulated */
            m->memlen += ptr->s.size * sizeof(mempool_free_header_t);
            free(ptr+1);   /* as if to return a malloc buffer */
#ifdef DEBUG_MALLOC_FREE
            sprintf(debug_title, "add_region_to_mempool (length = %d bytes)",
                    nbytes);
            cisco_display_mempool(pool, debug_title);
#endif /* DEBUG_MALLOC_FREE */
        } else {
            puts("\n*** add_region_to_mempool: ERROR -- given pool has "
                 "zero length (i.e., uninitialized)\n");
        }
    }
}


/*
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 * Function: cisco_reset_mempools
 *
 * Zero the memlen field in each of the structs in the global mempool
 * array.  This field tells whether a particular pool is in use by the
 * platform.
 */
void
cisco_reset_mempools (void)
{
    int i;
    cisco_mempool_t *m;

    for (i = CISCO_MEMPOOL_CLASS_UNKNOWN + 1; i < CISCO_MEMPOOL_CLASS_MAX; i++) {
	m = &cisco_mempool[i];
	m->memlen = 0;
    }
	
}

/*
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 * Function: cisco_restore_mempools
 *
 * For each mempool in use (its memlen field is not zero), reset the
 * free pointer to the pool's membase, reform the corresponding
 * memory into a single free block pointing to itself, and restore
 * the pool ID.
 *
 * N.B.  If any of the active mempools have multiple (non-contiguous)
 *       blocks (init involves call(s) to add_region_to_mempool), this
 *       function CANNOT be used.  In that case, it's appropriate to
 *       recall the init function instead of restore_mempools.
 */
void
cisco_restore_mempools (void)
{
    unsigned short  i;
    cisco_mempool_t *m;
    mempool_free_header_t *ptr;

    for (i = CISCO_MEMPOOL_CLASS_UNKNOWN + 1; i < CISCO_MEMPOOL_CLASS_MAX; i++) {
        /*
         * Common device driver memory pools need to keep their memory that
         * they allocated at dev create time.
         */
        if (i != CISCO_MEMPOOL_CLASS_DRAM_DEV) {
	    m = &cisco_mempool[i];
	    if (m->memlen != 0) {
	        m->freep = ptr = m->membase;
	        ptr->s.size = m->memlen/sizeof(mempool_free_header_t);
	        ptr->s.freeptr = ptr;  /* now one big block pointing at itself */
                ptr->s.pool_id = i;
            }
	}
    }
	
}


/*
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 * Function: cisco_malloc_pool
 *
 * Check the given mempool class and get its latest free list pointer.
 * Convert the given number of bytes to the equivalent number of header-
 * sized units and round it up (thus ensuring that headers and data in
 * the free list remain Mempool_Aligned according to MALLOC_BYTE_Mempool_AlignMENT built
 * in to mempool_free_header_t).  Find the first fit on the free list, and save
 * that pointer in the mempool struct for the next search in this pool.
 *
 * If the block found is greater than the requested size, allocate its
 * head end, leaving the tail part adjusted on the free list.  Return
 * the pointer to the allocated block, whose address is a multiple of
 * MALLOC_BYTE_Mempool_AlignMENT bytes.
 *
 * NOTE: In diagmon, originally this malloc_pool was defined with
 * function name malloc and the same two arguments.  To accommodate
 * the RSA object code (from Michael Reilly) used in the Quack
 * project, it was necessary to convert malloc to have a single size 
 * argument and implicitly use the pool CISCO_MEMPOOL_CLASS_DRAM.
 */
void *
cisco_malloc_pool (cisco_mempool_class class, unsigned long nbytes)
{
    mempool_free_header_t *p, *prevp, *freep;
    unsigned long nunits;
    cisco_mempool_t *m;

    if (class > CISCO_MEMPOOL_CLASS_UNKNOWN && class < CISCO_MEMPOOL_CLASS_MAX) {
	m = &cisco_mempool[class];
	if (m->memlen == 0) {
	    puts("\n*** malloc_pool: ERROR -- uninitialized pool\n");
	    return(NULL);
	}
        prevp = freep = m->freep;
    } else {
	puts("\n*** malloc_pool: ERROR -- unknown mempool class\n");
	return(NULL);
    }
    
    nunits = (nbytes + sizeof(mempool_free_header_t) - 1)/sizeof(mempool_free_header_t) + 1;
    for (p = prevp->s.freeptr; ; prevp = p, p = p->s.freeptr) {
#ifdef DEBUG_MALLOC_FREE
	    printf("\nmalloc_pool: next free chunk = ");
	    dismem((unsigned char *)p, sizeof(mempool_free_header_t), (unsigned)p, 4);
#endif /* DEBUG_MALLOC_FREE */
	if (p->s.size < nunits) {
	    if (p == freep) {
		/*
		 * wrapped around free list
		 */
		if (m->moremem) {
		    (*m->moremem)(m->moremem_param);
		} else {
		    /*
		     * Out of memory. Return NULL and let the caller
		     * print out error message. Do not print here because
		     * malloc_all will search multiple blocks of memory
		     * before reporting error. If we print here, it will
		     * print out these messages multiple times during the
		     * search even when it will eventually found a block of
		     * available memory and run the test successfully.
		     * This will lead to confusion where we report 
		     * "out of memory" message but still run and pass the
		     * test.
		     */
		    return(NULL);
		}
	    }
	    continue;  /* try the next one */
	}
	if (p->s.size <= nunits + 1) {
	    /*
	     * Either an exact fit, or just a header left over.
	     * In either case, allocate its entirety.
	     */
	    prevp = (prevp->s.freeptr = p->s.freeptr);

#ifdef DEBUG_MALLOC_FREE
	    printf("\nmalloc_pool: nbytes = %d; nunits = %d; matched p->size = %d\n",
		   nbytes, nunits, p->s.size);
#endif /* DEBUG_MALLOC_FREE */

	} else {
	    /*
	     * Chop the indicated size off the front
	     */
	    prevp = (prevp->s.freeptr = p + nunits);  /* point at new header */
	    prevp->s.freeptr = p->s.freeptr;
	    prevp->s.size = p->s.size - nunits;
	    prevp->s.pool_id = (unsigned short)class;

#ifdef DEBUG_MALLOC_FREE
	    printf("\nmalloc_pool: nbytes = %d; nunits = %d;"
		   "unchopped units = %d; chopped units = %d\n",
		   nbytes, nunits, p->s.size, prevp->s.size);
	    printf("\nmalloc_pool: new chunk = ");
	    dismem((unsigned char *)prevp, sizeof(mempool_free_header_t),
		   (unsigned)prevp, 4);
#endif /* DEBUG_MALLOC_FREE */

	    p->s.size = nunits;
	}
	m->freep = prevp;
	p->s.freeptr = p;   /* point header at itself for auditing in free() */
        p++;                /* user's block starts one beyond the header */
	return (void *)(p);
    }
}

/*
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 * Function: malloc
 *
 * Obtain the given number of bytes from CISCO_MEMPOOL_CLASS_DRAM by a call
 * to malloc_pool.
 *
 * NOTE: In diagmon, originally this malloc was defined as the current
 * malloc_pool (see above).  To accommodate the RSA object code (from
 * Michael Reilly) used in the Quack project, it was necessary to
 * convert this malloc to have a single size argument and implicitly
 * use the pool CISCO_MEMPOOL_CLASS_DRAM.
 */
void *
cisco_malloc (unsigned long nbytes)
{
    void *ptr;

    ptr = cisco_malloc_pool(CISCO_MEMPOOL_CLASS_DRAM, (unsigned long)nbytes);
    if (ptr == NULL) {
	puts("\n*** malloc: cisco_mempool_class DRAM is OUT OF MEMORY\n");
    }
    return(ptr);
}

/*
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 * Function: realloc
 *
 * Reallocate a block of memory at a different size from the pool
 * CISCO_MEMPOOL_CLASS_DRAM by calling malloc_pool.  If the request is
 * satisfied, call free to release the buffer of the given ptr (if
 * ptr != NULL), and return the new pointer from malloc_pool.
 * If the return from malloc_pool is NULL, simply return it without
 * freeing the given ptr.  
 *
 * (Called by Quack RSA object code from Michael Reilly.)
 */
void *
cisco_realloc (void *ptr, unsigned long size)
{
    void *new_ptr;

    new_ptr = cisco_malloc_pool(CISCO_MEMPOOL_CLASS_DRAM, size);

    if (new_ptr == NULL) {
	puts("\n*** realloc: OUT OF MEMORY\n");
	return(NULL);
    }

    if (ptr != NULL) {
	free(ptr);
    }
    return(new_ptr);
}

/*
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 * Function: display_buf_info
 *
 * Display the header of a malloc buffer and some bytes of the buffer
 * itself.
 */
void
display_buf_info (void *buf)
{
    mempool_free_header_t *ptr;
    uint showbytes, nbytes;

    ptr = (mempool_free_header_t *)buf - 1;
    nbytes = ptr->s.size*sizeof(mempool_free_header_t);
    printf("\n      buf @ %#.8x; bytes = %d\n header->", (unsigned int)buf,
	   nbytes);
    dismem((unsigned char *)ptr, sizeof(mempool_free_header_t), (unsigned)ptr,
           sizeof(long));
    showbytes = (nbytes < MALLOC_SHOW_BUF_BYTES) ? nbytes :
        MALLOC_SHOW_BUF_BYTES;
    dismem((unsigned char *)buf, (int)showbytes, (unsigned)buf, sizeof(long));
}

/*
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 * Function: free
 *
 * Check the returned block's header for integrity, and extract the
 * associated pool's ID.  Scan the pool's free list starting at freep
 * to find the place for insertion.  This is either between two ex-
 * isting blocks or at one end of the list.  In any case, combine
 * adjacent blocks if the freed block is adjacent to either neighbor.
 */
void
cisco_free (void *buf)
{
    mempool_free_header_t *ptr, *p, *freep;
    cisco_mempool_class class;
    cisco_mempool_t *m;

    if (!buf) {
#ifdef DEBUG_MALLOC_FREE
	puts("\n*** free: ptr to returned malloc block is NULL!");
#endif /* DEBUG_MALLOC_FREE */
	return;   /* no thanks */
    }

#ifdef DEBUG_MALLOC_FREE
    ptr = (mempool_free_header_t *)buf - 1;
    printf("\nfree: buf @ %#.8x; bytes = %8d\n  header->", buf,
	   ptr->s.size*sizeof(mempool_free_header_t));
    dismem((unsigned char *)ptr, sizeof(mempool_free_header_t), (unsigned)ptr, 4);
    dismem((unsigned char *)buf, 32, (unsigned)buf, 4);
    cisco_display_mempool(CISCO_MEMPOOL_CLASS_DRAM, "in free() before execution");
#endif /* DEBUG_MALLOC_FREE */    

    /*
     * Point back to block header.  Check that it was gotten from a malloc
     * by verifying that its next free pointer points at its own header,
     * and that its mempool ID is within bounds.
     */
    ptr = (mempool_free_header_t *)buf - 1;
    if (ptr->s.freeptr != ptr) {
	puts("\n*** free: header ptr of returned malloc block overwritten; "
	     "block discarded");
        display_buf_info(buf);
	return;
    }
    class = ptr->s.pool_id;
    if (!(class > CISCO_MEMPOOL_CLASS_UNKNOWN && class < CISCO_MEMPOOL_CLASS_MAX)) {
	puts("\n*** free: header pool ID of returned malloc block "
	     "overwritten; block discarded");
        display_buf_info(buf);
        printf("Debug: Pool_Id = %d\r\n", class);
	return;
    }
    m = &cisco_mempool[class];
    freep = m->freep;

    for (p = freep; !(ptr > p && ptr < p->s.freeptr); p = p->s.freeptr) {
        if (p >= p->s.freeptr && ((ptr > p || ptr < p->s.freeptr) || (ptr == p->s.freeptr))) {
	    break; /* freed block at start or end of pool's memory */
	}
    }

    freep = p;   /* unless returned buf is joined to front of single chunk */
    if (ptr + ptr->s.size == p->s.freeptr) {
	/*
	 * Join to upper neighbor.  If this chunk points at itself (solo),
	 * change to point at its new self.
	 */
	ptr->s.size += p->s.freeptr->s.size;
	ptr->s.freeptr = (p->s.freeptr == p) ? ptr : p->s.freeptr->s.freeptr;
	freep = ptr;
    } else {
	ptr->s.freeptr = p->s.freeptr;
    }
    if (p + p->s.size == ptr) {
	p->s.size += ptr->s.size;             /* join to lower neighbor */
	p->s.freeptr = ptr->s.freeptr;
	freep = p;
    } else {
	p->s.freeptr = ptr;
    }
    
    m->freep = freep;

#ifdef DEBUG_MALLOC_FREE
    puts("\np->header: ");
    dismem((unsigned char *)p, sizeof(mempool_free_header_t), (unsigned)p, 4);
    puts("\nptr->header: ");
    dismem((unsigned char *)ptr, sizeof(mempool_free_header_t), (unsigned)ptr, 4);
    cisco_display_mempool(CISCO_MEMPOOL_CLASS_DRAM, "free() executed");
#endif /* DEBUG_MALLOC_FREE */    
    
}

/*
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * Function: cisco_init_mempools_from_table
 *
 * Get the total memory on the box and deduct the given unavail_bytes,
 * which represents the space already taken by code, data (including
 * ERRLOG), kernel mempool, etc.  Operate on the given table spec to
 * init as many mempools specified until the allocated space for each
 * entry exhausts the actual (remaining) memory.  Then init the global
 * table descriptor and pointer to it for use by the macro POOL_TO_PHY
 * and function malloc_all (which obtains the given bytes from the
 * next round-robin mempool).
 */
void
cisco_init_mempools_from_table (cisco_mempools_table_t *tbl, unsigned long tbl_size,
                          unsigned long unavail_bytes)
{
    unsigned int tx;
    boolean mem_gone;
    cisco_mempools_table_t *tp = tbl;
    unsigned long rem_mem = sizemainmem() - unavail_bytes;
    cisco_mempools_tbl_desc_t *dp = &mempools_tbl_desc;  /* global */

    for (tx = 0; tx < tbl_size; tx++, tp++) {
#ifdef DEBUG_MEMPOOLS_TABLE
        printf("\ninit_mempools_: [%d] rem_mem = %.8x", tx, rem_mem);
#endif /* DEBUG_MEMPOOLS_TABLE */
        mem_gone = tp->max_size > rem_mem;
        if (tp->act_size == 0) {
            tp->act_size = (mem_gone) ? rem_mem : tp->max_size;
        }
        if (tp->act_size) {
            cisco_init_mempool(tp->name, tp->class, tp->mem,
                         tp->act_size);
        } else {
            break;
        }
        rem_mem -= tp->act_size;
#ifdef DEBUG_MEMPOOLS_TABLE
        printf("\ninit_mempools_: [%d] rem_mem- = %#.8x; act_size = %#.8x;"
               " mem_gone = %s", tx, rem_mem, tp->act_size, (mem_gone) ?
               "TRUE" : "FALSE");
#endif /* DEBUG_MEMPOOLS_TABLE */
    }
    dp->table = tbl;
    dp->rnd_rbn = 0;
    dp->num_pools = tx;
    cisco_mempools_tbl_desc_p = dp;  /* Otherwise left NULL */
}    

/*
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * Function: cisco_show_mempools_table
 *
 * Display the mempools table initialized by the platform call to
 * init_mempools_from_table, which fills in the global mempools_tbl_desc
 * and its pointer.
 */
void
cisco_show_mempools_table (void)
{
    unsigned int n;
    cisco_mempools_table_t *tp;
    cisco_mempools_tbl_desc_t *dp = cisco_mempools_tbl_desc_p;

    /*assert(dp);*/  /* initialized by init_mempools_from_table */
    if (!dp) {
        return;
    }
    tp = dp->table;
    printf("\n Mempools initialized for DRAM in this box\n"
           "  Pool       Size      Virt addr base  Phy addr base");
    for (n = 0; n < dp->num_pools; n++, tp++) {
        printf("\n %s  %#.8x     %#.8x     %#.8x",
               tp->name, (unsigned int)(tp->act_size), (unsigned int)(tp->vir_base), (unsigned int)(tp->phy_base));
    }
}

/*
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * Function: cisco_malloc_all
 *
 * Malloc the given number of bytes from the platform's mempool that
 * is indexed by the round-robin index in the mempools_tbl_desc, and
 * bump the index. However, if the descriptor has not been initialized
 * on the platform, revert to a straight malloc call.
 *
 * If the malloc from the indicated mempool returns NULL, bump the
 * round-robin index and try again.  Return NULL only if all mempools
 * on this platform have less than nbytes free.
 */
void *
cisco_malloc_all (unsigned long nbytes)
{
    unsigned int i, n;
    void *p;
    ulong l;
    cisco_mempools_table_t *tp;
    cisco_mempools_tbl_desc_t *dp = cisco_mempools_tbl_desc_p;

    if (!dp) {
        return(cisco_malloc(nbytes));
    }
    p = 0;
    n = dp->num_pools;
    for (i = 0; i < n; i++) {
        tp = dp->table + dp->rnd_rbn;
        p = cisco_malloc_pool(tp->class, nbytes);
        dp->rnd_rbn++;
        if (dp->rnd_rbn == n) {
            dp->rnd_rbn = 0;
        }
        if (p) {
            l = (ulong)p;
            /*assert(l == PHY_TO_POOL(POOL_TO_PHY(l)));*/
            return(p);
        }
    }
    puts("\n*** malloc_all: OUT OF MEMORY\n");
    return(p);  /* No pool can satisfy the request */
}


/* END of module */

/******** History ******** 
$Log: not supported by cvs2svn $
Revision 1.1.1.1  2010/08/19 13:04:55  jackie
initial check-in for Rudy and new projects

$Endlog$
*/

void cisco_init_for_malloc_use(void *mem, unsigned long sizeofmem)
{
    ulong size, unavail, num_tbl_pools;
    cisco_reset_mempools();

    memsize = sizeofmem;

    /*
     * unavail -
     * This variable is used to calculate remaining total memory
     * that can be used for malloc purposes.
     * It is only used inside init_mempools_from_table() in free.c
     * On this platform unavail has no effect, since total memory is
     * alot larger than unavail size
     */
    unavail = 0; /* Set this much aside for mempools */

    /*
     * fsl_mempool_tbl[0] is mem pool for general malloc
     */
    fsl_mempool_tbl.mem = mem;
    fsl_mempool_tbl.class = CISCO_MEMPOOL_CLASS_DRAM;
    fsl_mempool_tbl.phy_base = (unsigned long)mem;
    fsl_mempool_tbl.vir_base = (unsigned long)mem;
    fsl_mempool_tbl.act_size = 0;

    size = sizemainmem();
    size -= (unsigned long)mem;
    fsl_mempool_tbl.max_size = size;

    num_tbl_pools = sizeof(fsl_mempool_tbl) / sizeof(cisco_mempools_table_t);
    cisco_init_mempools_from_table(&fsl_mempool_tbl, num_tbl_pools, unavail);
}

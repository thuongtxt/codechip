/* $Id: proto.h,v 1.1.1.1 2012-04-25 05:28:29 krreddy Exp $
 * $Source: /svn/sandbox/prajpai_dump_area/CVS/Repo1/diag/common/include/proto.h,v $
 *------------------------------------------------------------------
 */

#ifndef __PROTO_H__
#define __PROTO_H__

#include "setjmps.h"

extern int  get_line(char *buffer, int bufsiz);
extern int  getopt(int argc, char *argv[], char *optstr);
extern int  printf(char *fmtptr, ...);
extern void longjmp(jmp_buf *, int);
extern int  setjmp(jmp_buf);
extern int  getnum(char *cptr, int base, unsigned int *longret);
extern int  getnnum(char *cptr, int base, unsigned int *longret, int maxchars);
/*extern int  getnum64(char *, int, unsigned long long *);*/
#endif /* __PROTO_H__ */
/* End of File */

/******** History ******** 
$Log: not supported by cvs2svn $
Revision 1.1.1.1.2.1  2011/03/30 23:57:49  jackie
support 64bit memory access/test utilities

Revision 1.1.1.1  2010-08-19 13:04:55  jackie
initial check-in for Rudy and new projects

$Endlog$
*/


/* $Id: setjmps.h,v 1.1.1.1 2012-04-25 05:28:29 krreddy Exp $ 
 * $Source: /svn/sandbox/prajpai_dump_area/CVS/Repo1/diag/common/include/setjmps.h,v $ 
 *------------------------------------------------------------------
 */

#ifndef __SETJMP_H__
#define __SETJMP_H__

typedef int jmp_buf[22];
extern jmp_buf monjmpbuf, *monjmpptr;
#endif  /* __SETJMP_H__ */

/* end of module */

/******** History ******** 
$Log: not supported by cvs2svn $
Revision 1.1.1.1  2010/08/19 13:04:55  jackie
initial check-in for Rudy and new projects

$Endlog$
*/


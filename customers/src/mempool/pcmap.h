/* $Id: pcmap.h,v 1.3 2013-03-01 10:07:48 yasdixit Exp $
 * $Source: /svn/sandbox/prajpai_dump_area/CVS/Repo1/diag/plat/rudy/include/pcmap.h,v $
 *------------------------------------------------------------------
 * Description: 
 *    Memory map defines for P2020 (PowerQUICC III) RUDY
 *------------------------------------------------------------------
 */

#ifndef __RUDY_PCMAP_H__
#define __RUDY_PCMAP_H__

#ifdef ASMINCLUDE
#include "types.h"
#endif /* ASMINCLUDE */

/*
 * For a description of the physical/virtual memory space and 
 * chip-select lines assignments, see the design document for this platform.
 */

/*
 * Macro to map DRAM virtual space to physical space and 
 * vice versa. 
 */

#define PHYSICAL_ADDR_START    0x00000000

#ifdef VIR_IS_PHY
#define DRAM_VIR_TO_PHY(x)     (x)
#define DRAM_PHY_TO_VIR(x)     (x)
#else
#define DRAM_VIR_TO_PHY(x)     ((ulong)(x) & (~PHYSICAL_ADDR_START))
#define DRAM_PHY_TO_VIR(x)     ((ulong)(x) | PHYSICAL_ADDR_START)
#endif /* #ifdef VIR_IS_PHY */

#define PHY_ADDR(x)         DRAM_VIR_TO_PHY(x)
#define VIR_ADDR(x)         DRAM_PHY_TO_VIR(x)

#define PHY_TO_KSEG1        DRAM_PHY_TO_VIR

#define PHY_TO_CACHE(x)     DRAM_PHY_TO_VIR(PHY_ADDR((ulong)(x)))
#define MEM_TO_PCI(x)       DRAM_VIR_TO_PHY(x)
#define PCI_TO_MEM(x)       PHY_TO_KSEG1(x)


/*
 * Physical DRAM definitions.
 */
#define ADRSPC_RAM          PHYSICAL_ADDR_START    /* start of RAM */
#define PHY_ADRSPC_RAM      PHYSICAL_ADDR_START    /* Start of RAM */
#define PHY_ADRSPC_RAM_TLB1 PHYSICAL_ADDR_START    /* 0x00000000 */
#define PHY_ADRSPC_RAM_TLB2 (0x40000000)  
#define PHY_ADRSPC_RAM_TLB3 (0x80000000)
#define PHY_ADRSPC_RAM_TLB4 (0xc0000000)
#define PHY_ADRSPC_RAM_TLB5 (0xd0000000)
#define ADRSPC_RAM_END      (0xe0000000)
#define ADRSPC_RAM_SIZE     (ADRSPC_RAM_END - ADRSPC_RAM)      /* 3.5GB */

/*
 * PowerQUICC Address definitions.
 */

#ifdef RUDY

#define ADRSPC_PQUICC_IMEMB 0xFF700000       /* POR value: 0xFF70_0000     */
#define ADRSPC_L2SRAM_BASE  0xFC000000       /* L2 SRAM Base address     */
#define ADRSPC_DIAG_STACK_BASE 0x008FFFF0

/*
 * Boot ROM (ROMMON, NVRAM)
 *
 * boot rom base = 0xFF00 0000  Unused                    (4MB)
 *                 0xFF80 0000  BootRom 1 (upgrade image) (2MB)
 *                 0xFFC0 0000  BootRom 0 (Golden image)  (2MB)
 */

#define ADRSPC_MAINBOARD_FLASH    0xFF800000
#define ADRSPC_MAINBOARD_FLASH_CACHED 0xFFC00000  /* readonly ROMMON area */

/*
 * Local Bus Controller
 * for TLB/LAW assignment purpose
 */
#define ADRSPC_LOCAL_BUS    0x80000000

#else /* Sunridges */

#define ADRSPC_PQUICC_IMEMB 0xDF800000       /* POR value: 0xFF70_0000     */
#define ADRSPC_L2SRAM_BASE  0xDF700000       /* L2 SRAM Base address     */
#define PCI_ADRSPC          0xA0000000
#define ADRSPC_MAINBOARD_FLASH    0xF8000000

/*
 * Local Bus Controller
 * for TLB/LAW assignment purpose
 */
#define ADRSPC_LOCAL_BUS    0xDF000000

#endif

#define ADRSPC_PQUICC_REGB      ADRSPC_PQUICC_IMEMB
#define ADRSPC_PQUICC_IMEMB_SZ  0x100000

#define KERN_MEMPOOL_SIZE       0x00100000   /* 1MB */

/*
 * PCIe devices
 */

/* PCIe Address Map, according to Rudy Common Control HFS */
/*
 * IM0:     0x4_0000_0000 ~ 0x4_7FFF_FFFF   (2GB)
 * IM1:     0x4_8000_0000 ~ 0x4_FFFF_FFFF   (2GB)
 * IM2:     0x5_0000_0000 ~ 0x5_7FFF_FFFF   (2GB)
 * IM3:     0x5_8000_0000 ~ 0x5_FFFF_FFFF   (2GB)
 * IM4:     0x6_0000_0000 ~ 0x6_7FFF_FFFF   (2GB)
 * IM5:     0x6_8000_0000 ~ 0x6_FFFF_FFFF   (2GB)
 * Nile0:   0xE000_0000   ~ 0xE7FF_FFFF     (128MB)
 * Nile1:   0xE800_0000   ~ 0xEFFF_FFFF     (128MB)
 * Handoff: 0xF000_0000   ~ 0xF7FF_FFFF     (128MB)
 */

/*
 * P2020 CPU Virtual (32-bit) Address Map for PCIe devices
 */ 
#define ADRSPC_VIRT_IM_BAR0_1    0xF8000000     /* 512MB (1M used, 511M reserved) */
#define ADRSPC_VIRT_IM_BAR2_3    0xC0000000     /* 512MB */
#define ADRSPC_VIRT_IM_BAR4_5    0x80000000     /* 1G */
#define ADRSPC_VIRT_NILE_0_BA    0xE0000000     /* 128M */
#define ADRSPC_VIRT_NILE_1_BA    0xE8000000     /* 128M */
#define ADRSPC_VIRT_HOFF_BA      0xF0000000     /* 128M */
#define ADRSPC_SIZE_64MB         0x04000000
#define ADRSPC_SIZE_256MB        0x10000000

/*
 * P2020 CPU Physical (36-bit) Address Map for PCIe devices
 */
/* IM0: 4_0000_0000 ~ 4_7FFF_FFFF */
#define ADRSPC_PHY_IM0_BAR0_1_H   0x00000004 /* 512MB (1M used, 511M reserved) */
#define ADRSPC_PHY_IM0_BAR0_1_L   0x00000000
#define ADRSPC_PHY_IM0_BAR2_3_H   0x00000004 /* 512MB (1M used, 511M reserved) */
#define ADRSPC_PHY_IM0_BAR2_3_L   0x20000000
#define ADRSPC_PHY_IM0_BAR4_5_H   0x00000004 /* 512MB (1M used, 511M reserved) */
#define ADRSPC_PHY_IM0_BAR4_5_L   0x40000000

#ifdef ASR902
/* IM2: 4_8000_0000 ~ 4_FFFF_FFFF */
#define ADRSPC_PHY_IM2_BAR0_1_H   0x00000004 /* 512MB (1M used, 511M reserved) */
#define ADRSPC_PHY_IM2_BAR0_1_L   0x80000000
#define ADRSPC_PHY_IM2_BAR2_3_H   0x00000004 /* 512MB (1M used, 511M reserved) */
#define ADRSPC_PHY_IM2_BAR2_3_L   0xA0000000
#define ADRSPC_PHY_IM2_BAR4_5_H   0x00000004 /* 512MB (1M used, 511M reserved) */
#define ADRSPC_PHY_IM2_BAR4_5_L   0xC0000000

/* IM1: 5_0000_0000 ~ 5_7FFF_FFFF */
#define ADRSPC_PHY_IM1_BAR0_1_H   0x00000005 /* 512MB (1M used, 511M reserved) */
#define ADRSPC_PHY_IM1_BAR0_1_L   0x00000000
#define ADRSPC_PHY_IM1_BAR2_3_H   0x00000005 /* 512MB (1M used, 511M reserved) */
#define ADRSPC_PHY_IM1_BAR2_3_L   0x20000000
#define ADRSPC_PHY_IM1_BAR4_5_H   0x00000005 /* 512MB (1M used, 511M reserved) */
#define ADRSPC_PHY_IM1_BAR4_5_L   0x40000000

#else

/* IM1: 4_8000_0000 ~ 4_FFFF_FFFF */
#define ADRSPC_PHY_IM1_BAR0_1_H   0x00000004 /* 512MB (1M used, 511M reserved) */
#define ADRSPC_PHY_IM1_BAR0_1_L   0x80000000
#define ADRSPC_PHY_IM1_BAR2_3_H   0x00000004 /* 512MB (1M used, 511M reserved) */
#define ADRSPC_PHY_IM1_BAR2_3_L   0xA0000000
#define ADRSPC_PHY_IM1_BAR4_5_H   0x00000004 /* 512MB (1M used, 511M reserved) */
#define ADRSPC_PHY_IM1_BAR4_5_L   0xC0000000

/* IM2: 5_0000_0000 ~ 5_7FFF_FFFF */
#define ADRSPC_PHY_IM2_BAR0_1_H   0x00000005 /* 512MB (1M used, 511M reserved) */
#define ADRSPC_PHY_IM2_BAR0_1_L   0x00000000
#define ADRSPC_PHY_IM2_BAR2_3_H   0x00000005 /* 512MB (1M used, 511M reserved) */
#define ADRSPC_PHY_IM2_BAR2_3_L   0x20000000
#define ADRSPC_PHY_IM2_BAR4_5_H   0x00000005 /* 512MB (1M used, 511M reserved) */
#define ADRSPC_PHY_IM2_BAR4_5_L   0x40000000

#endif

/* IM3: 5_8000_0000 ~ 5_FFFF_FFFF */
#define ADRSPC_PHY_IM3_BAR0_1_H   0x00000005 /* 512MB (1M used, 511M reserved) */
#define ADRSPC_PHY_IM3_BAR0_1_L   0x80000000
#define ADRSPC_PHY_IM3_BAR2_3_H   0x00000005 /* 512MB (1M used, 511M reserved) */
#define ADRSPC_PHY_IM3_BAR2_3_L   0xA0000000
#define ADRSPC_PHY_IM3_BAR4_5_H   0x00000005 /* 512MB (1M used, 511M reserved) */
#define ADRSPC_PHY_IM3_BAR4_5_L   0xC0000000

/* IM4: 6_0000_0000 ~ 6_7FFF_FFFF */
#define ADRSPC_PHY_IM4_BAR0_1_H   0x00000006 /* 512MB (1M used, 511M reserved) */
#define ADRSPC_PHY_IM4_BAR0_1_L   0x00000000
#define ADRSPC_PHY_IM4_BAR2_3_H   0x00000006 /* 512MB (1M used, 511M reserved) */
#define ADRSPC_PHY_IM4_BAR2_3_L   0x20000000
#define ADRSPC_PHY_IM4_BAR4_5_H   0x00000006 /* 512MB (1M used, 511M reserved) */
#define ADRSPC_PHY_IM4_BAR4_5_L   0x40000000

/* IM5: 6_8000_0000 ~ 6_FFFF_FFFF */
#define ADRSPC_PHY_IM5_BAR0_1_H   0x00000006 /* 512MB (1M used, 511M reserved) */
#define ADRSPC_PHY_IM5_BAR0_1_L   0x80000000
#define ADRSPC_PHY_IM5_BAR2_3_H   0x00000006 /* 512MB (1M used, 511M reserved) */
#define ADRSPC_PHY_IM5_BAR2_3_L   0xA0000000
#define ADRSPC_PHY_IM5_BAR4_5_H   0x00000006 /* 512MB (1M used, 511M reserved) */
#define ADRSPC_PHY_IM5_BAR4_5_L   0xC0000000


#define ADRSPC_PHY_NILE_0_BA     0xE0000000     /* 128M */
#define ADRSPC_PHY_NILE_1_BA     0xE8000000     /* 128M */
#define ADRSPC_PHY_HOFF_BA       0xF0000000     /* 128M */

/*
 * P2020 32bit PCIe Address Map
 */
#define ADRSPC_PCI_CPU_BAR0_1  ADRSPC_VIRT_IM_BAR0_1
#define ADRSPC_PCI_CPU_END     0xFBFFFFFF

/*
 * P2020 64bit PCIe Address Map : 4_0000_0000 ~ 7_FFFF_FFFF
 */ 
#define ADRSPC_PCIE_CPU_BAR0_1_H ADRSPC_PHY_IM0_BAR0_1_H
#define ADRSPC_PCIE_CPU_BAR0_1_L ADRSPC_PHY_IM0_BAR0_1_L
#define ADRSPC_PCIE_CPU_END_H    0x00000007
#define ADRSPC_PCIE_CPU_END_L    0xFFFFFFFF

/*
 * IMs PCIe 32-bit Address Map
 */
#define ADRSPC_PCI_IM_SIZE     0x100000  /* 1MB */

#define ADRSPC_PCI_IM_0_BAR0_1 0xF8000000
#define ADRSPC_PCI_IM_0_END    (ADRSPC_PCI_IM_0_BAR0_1 + (ADRSPC_PCI_IM_SIZE - 1))
#ifdef ASR902
#define ADRSPC_PCI_IM_2_BAR0_1 (ADRSPC_PCI_IM_0_BAR0_1 + ADRSPC_PCI_IM_SIZE)
#define ADRSPC_PCI_IM_2_END    (ADRSPC_PCI_IM_2_BAR0_1 + (ADRSPC_PCI_IM_SIZE - 1))

#define ADRSPC_PCI_IM_1_BAR0_1 (ADRSPC_PCI_IM_2_BAR0_1 + ADRSPC_PCI_IM_SIZE)
#define ADRSPC_PCI_IM_1_END    (ADRSPC_PCI_IM_1_BAR0_1 + (ADRSPC_PCI_IM_SIZE - 1))

#define ADRSPC_PCI_IM_3_BAR0_1 (ADRSPC_PCI_IM_1_BAR0_1 + ADRSPC_PCI_IM_SIZE)
#define ADRSPC_PCI_IM_3_END    (ADRSPC_PCI_IM_3_BAR0_1 + (ADRSPC_PCI_IM_SIZE - 1))

#else

#define ADRSPC_PCI_IM_1_BAR0_1 (ADRSPC_PCI_IM_0_BAR0_1 + ADRSPC_PCI_IM_SIZE)
#define ADRSPC_PCI_IM_1_END    (ADRSPC_PCI_IM_1_BAR0_1 + (ADRSPC_PCI_IM_SIZE - 1))

#define ADRSPC_PCI_IM_2_BAR0_1 (ADRSPC_PCI_IM_1_BAR0_1 + ADRSPC_PCI_IM_SIZE)
#define ADRSPC_PCI_IM_2_END    (ADRSPC_PCI_IM_2_BAR0_1 + (ADRSPC_PCI_IM_SIZE - 1))

#define ADRSPC_PCI_IM_3_BAR0_1 (ADRSPC_PCI_IM_2_BAR0_1 + ADRSPC_PCI_IM_SIZE)
#define ADRSPC_PCI_IM_3_END    (ADRSPC_PCI_IM_3_BAR0_1 + (ADRSPC_PCI_IM_SIZE - 1))
#endif

#define ADRSPC_PCI_IM_4_BAR0_1 (ADRSPC_PCI_IM_3_BAR0_1 + ADRSPC_PCI_IM_SIZE)
#define ADRSPC_PCI_IM_4_END    (ADRSPC_PCI_IM_4_BAR0_1 + (ADRSPC_PCI_IM_SIZE - 1))

#define ADRSPC_PCI_IM_5_BAR0_1 (ADRSPC_PCI_IM_4_BAR0_1 + ADRSPC_PCI_IM_SIZE)
#define ADRSPC_PCI_IM_5_END    (ADRSPC_PCI_IM_5_BAR0_1 + (ADRSPC_PCI_IM_SIZE - 1))

/*
 * IMs PCIe 64-bit Address Map
 */
#define ADRSPC_PCIE_IM_0_BAR0_1_H       ADRSPC_PHY_IM0_BAR0_1_H
#define ADRSPC_PCIE_IM_0_BAR0_1_L       ADRSPC_PHY_IM0_BAR0_1_L
#define ADRSPC_PCIE_IM_0_BAR2_3_H       ADRSPC_PHY_IM0_BAR2_3_H
#define ADRSPC_PCIE_IM_0_BAR2_3_L       ADRSPC_PHY_IM0_BAR2_3_L
#define ADRSPC_PCIE_IM_0_BAR4_5_H       ADRSPC_PHY_IM0_BAR4_5_H
#define ADRSPC_PCIE_IM_0_BAR4_5_L       ADRSPC_PHY_IM0_BAR4_5_L
#define ADRSPC_PCIE_IM_0_END_H          ADRSPC_PHY_IM0_BAR4_5_H
#define ADRSPC_PCIE_IM_0_END_L          0x7FFFFFFF
#ifdef ASR902
#define ADRSPC_PCIE_IM_1_BAR0_1_H       ADRSPC_PHY_IM1_BAR0_1_H
#define ADRSPC_PCIE_IM_1_BAR0_1_L       ADRSPC_PHY_IM1_BAR0_1_L
#define ADRSPC_PCIE_IM_1_BAR2_3_H       ADRSPC_PHY_IM1_BAR2_3_H
#define ADRSPC_PCIE_IM_1_BAR2_3_L       ADRSPC_PHY_IM1_BAR2_3_L
#define ADRSPC_PCIE_IM_1_BAR4_5_H       ADRSPC_PHY_IM1_BAR4_5_H
#define ADRSPC_PCIE_IM_1_BAR4_5_L       ADRSPC_PHY_IM1_BAR4_5_L
#define ADRSPC_PCIE_IM_1_END_H          ADRSPC_PHY_IM1_BAR4_5_H
#define ADRSPC_PCIE_IM_1_END_L          0x7FFFFFFF

#define ADRSPC_PCIE_IM_2_BAR0_1_H       ADRSPC_PHY_IM2_BAR0_1_H
#define ADRSPC_PCIE_IM_2_BAR0_1_L       ADRSPC_PHY_IM2_BAR0_1_L
#define ADRSPC_PCIE_IM_2_BAR2_3_H       ADRSPC_PHY_IM2_BAR2_3_H
#define ADRSPC_PCIE_IM_2_BAR2_3_L       ADRSPC_PHY_IM2_BAR2_3_L
#define ADRSPC_PCIE_IM_2_BAR4_5_H       ADRSPC_PHY_IM2_BAR4_5_H
#define ADRSPC_PCIE_IM_2_BAR4_5_L       ADRSPC_PHY_IM2_BAR4_5_L
#define ADRSPC_PCIE_IM_2_END_H          ADRSPC_PHY_IM2_BAR4_5_H
#define ADRSPC_PCIE_IM_2_END_L          0xfFFFFFFF
#else
#define ADRSPC_PCIE_IM_1_BAR0_1_H       ADRSPC_PHY_IM1_BAR0_1_H
#define ADRSPC_PCIE_IM_1_BAR0_1_L       ADRSPC_PHY_IM1_BAR0_1_L
#define ADRSPC_PCIE_IM_1_BAR2_3_H       ADRSPC_PHY_IM1_BAR2_3_H
#define ADRSPC_PCIE_IM_1_BAR2_3_L       ADRSPC_PHY_IM1_BAR2_3_L
#define ADRSPC_PCIE_IM_1_BAR4_5_H       ADRSPC_PHY_IM1_BAR4_5_H
#define ADRSPC_PCIE_IM_1_BAR4_5_L       ADRSPC_PHY_IM1_BAR4_5_L
#define ADRSPC_PCIE_IM_1_END_H          ADRSPC_PHY_IM1_BAR4_5_H
#define ADRSPC_PCIE_IM_1_END_L          0xFFFFFFFF

#define ADRSPC_PCIE_IM_2_BAR0_1_H       ADRSPC_PHY_IM2_BAR0_1_H
#define ADRSPC_PCIE_IM_2_BAR0_1_L       ADRSPC_PHY_IM2_BAR0_1_L
#define ADRSPC_PCIE_IM_2_BAR2_3_H       ADRSPC_PHY_IM2_BAR2_3_H
#define ADRSPC_PCIE_IM_2_BAR2_3_L       ADRSPC_PHY_IM2_BAR2_3_L
#define ADRSPC_PCIE_IM_2_BAR4_5_H       ADRSPC_PHY_IM2_BAR4_5_H
#define ADRSPC_PCIE_IM_2_BAR4_5_L       ADRSPC_PHY_IM2_BAR4_5_L
#define ADRSPC_PCIE_IM_2_END_H          ADRSPC_PHY_IM2_BAR4_5_H
#define ADRSPC_PCIE_IM_2_END_L          0x7FFFFFFF
#endif

#define ADRSPC_PCIE_IM_3_BAR0_1_H       ADRSPC_PHY_IM3_BAR0_1_H
#define ADRSPC_PCIE_IM_3_BAR0_1_L       ADRSPC_PHY_IM3_BAR0_1_L
#define ADRSPC_PCIE_IM_3_BAR2_3_H       ADRSPC_PHY_IM3_BAR2_3_H
#define ADRSPC_PCIE_IM_3_BAR2_3_L       ADRSPC_PHY_IM3_BAR2_3_L
#define ADRSPC_PCIE_IM_3_BAR4_5_H       ADRSPC_PHY_IM3_BAR4_5_H
#define ADRSPC_PCIE_IM_3_BAR4_5_L       ADRSPC_PHY_IM3_BAR4_5_L
#define ADRSPC_PCIE_IM_3_END_H          ADRSPC_PHY_IM3_BAR4_5_H
#define ADRSPC_PCIE_IM_3_END_L          0xFFFFFFFF

#define ADRSPC_PCIE_IM_4_BAR0_1_H       ADRSPC_PHY_IM4_BAR0_1_H
#define ADRSPC_PCIE_IM_4_BAR0_1_L       ADRSPC_PHY_IM4_BAR0_1_L
#define ADRSPC_PCIE_IM_4_BAR2_3_H       ADRSPC_PHY_IM4_BAR2_3_H
#define ADRSPC_PCIE_IM_4_BAR2_3_L       ADRSPC_PHY_IM4_BAR2_3_L
#define ADRSPC_PCIE_IM_4_BAR4_5_H       ADRSPC_PHY_IM4_BAR4_5_H
#define ADRSPC_PCIE_IM_4_BAR4_5_L       ADRSPC_PHY_IM4_BAR4_5_L
#define ADRSPC_PCIE_IM_4_END_H          ADRSPC_PHY_IM4_BAR4_5_H
#define ADRSPC_PCIE_IM_4_END_L          0x7FFFFFFF

#define ADRSPC_PCIE_IM_5_BAR0_1_H       ADRSPC_PHY_IM5_BAR0_1_H
#define ADRSPC_PCIE_IM_5_BAR0_1_L       ADRSPC_PHY_IM5_BAR0_1_L
#define ADRSPC_PCIE_IM_5_BAR2_3_H       ADRSPC_PHY_IM5_BAR2_3_H
#define ADRSPC_PCIE_IM_5_BAR2_3_L       ADRSPC_PHY_IM5_BAR2_3_L
#define ADRSPC_PCIE_IM_5_BAR4_5_H       ADRSPC_PHY_IM5_BAR4_5_H
#define ADRSPC_PCIE_IM_5_BAR4_5_L       ADRSPC_PHY_IM5_BAR4_5_L
#define ADRSPC_PCIE_IM_5_END_H          ADRSPC_PHY_IM5_BAR4_5_H
#define ADRSPC_PCIE_IM_5_END_L          0xFFFFFFFF




/*
 * Nile PCIe Address Map
 */
#define ADRSPC_PCIE_NILE_SIZE   0x08000000  /* 128 MB */

#define ADRSPC_PCIE_NILE_0_BA   ADRSPC_PHY_NILE_0_BA
#define ADRSPC_PCIE_NILE_0_END  \
       (ADRSPC_PCIE_NILE_0_BA + (ADRSPC_PCIE_NILE_SIZE - 1))

#define ADRSPC_PCIE_NILE_1_BA   ADRSPC_PHY_NILE_1_BA
#define ADRSPC_PCIE_NILE_1_END  \
       (ADRSPC_PCIE_NILE_1_BA + (ADRSPC_PCIE_NILE_SIZE - 1))       

/*
 * Handoff FPGA PCIe Address Map
 */
#define ADRSPC_PCIE_HOFF_SIZE   0x08000000  /* 128 MB */
#define ADRSPC_PCIE_HOFF_BA     ADRSPC_PHY_HOFF_BA
#define ADRSPC_PCIE_HOFF_END    \
        (ADRSPC_PCIE_HOFF_BA + (ADRSPC_PCIE_HOFF_SIZE - 1))     

/*
 * Handoff FPGA QDR Address Map
 */
#define ADRSPC_PCIE_HOFF_QDR_SIZE (0x1000000) /* 16MB */
#define ADRSPC_PCIE_HOFF_QDR_TEST_SIZE (0x400000) /* 4MB */
#define ADRSPC_PCIE_HOFF_QDR_BA   (0xF1000000)
#define ADRSPC_PCIE_HOFF_QDR_END  \
        (ADRSPC_PCIE_HOFF_QDR_BA + (ADRSPC_PCIE_HOFF_QDR_SIZE - 1))
#ifdef RSP2_BOARD

#define ADRSPC_PCIESW1_UP_CYLON_BA   ADRSPC_PCIE_NILE_0_BA
#define ADRSPC_PCIESW1_UP_CYLON_END  ADRSPC_PCIE_NILE_1_END
/*
 * PCIe Switch Downstream Port 0 Address Map
 */
#define ADRSPC_PCIESW1_DP_0_BA   ADRSPC_PCIE_NILE_0_BA
#define ADRSPC_PCIESW1_DP_0_END  ADRSPC_PCIE_NILE_0_END

/*
 * PCIe Switch Downstream Port 1 Address Map
 */
#define ADRSPC_PCIESW1_DP_1_BA   ADRSPC_PCIE_NILE_1_BA
#define ADRSPC_PCIESW1_DP_1_END  ADRSPC_PCIE_NILE_1_END
/*
 * Cylon 0 Address Map
 */
#define ADRSPC_PCI_CYLON_0_BA   ADRSPC_PCIE_NILE_0_BA
#define ADRSPC_PCI_CYLON_0_END  ADRSPC_PCIE_NILE_0_END

/*
 * Cylon 1 Address Map
 */
#define ADRSPC_PCI_CYLON_1_BA   ADRSPC_PCIE_NILE_1_BA
#define ADRSPC_PCI_CYLON_1_END  ADRSPC_PCIE_NILE_1_END

/*
 * PCIe Switch Upstream Port Address Map
 */
#define ADRSPC_PCIESW2_UP_BA_H   ADRSPC_PCIE_CPU_BAR0_1_H
#define ADRSPC_PCIESW2_UP_BA_L   ADRSPC_PCIE_CPU_BAR0_1_L
#define ADRSPC_PCIESW2_UP_END_H  0x00000007
#define ADRSPC_PCIESW2_UP_END_L  0xFFFFFFFF

/*
 * PCIe Switch Downstream Port 0 Address Map
 */
#define ADRSPC_PCIESW2_DP_0_BA_H   ADRSPC_PCIE_IM_0_BAR0_1_H
#define ADRSPC_PCIESW2_DP_0_BA_L   ADRSPC_PCIE_IM_0_BAR0_1_L
#define ADRSPC_PCIESW2_DP_0_END_H  ADRSPC_PCIE_IM_0_END_H
#define ADRSPC_PCIESW2_DP_0_END_L  ADRSPC_PCIE_IM_0_END_L

/*
 * PCIe Switch Downstream Port 1 Address Map
 */
#define ADRSPC_PCIESW2_DP_1_BA_H   ADRSPC_PCIE_IM_1_BAR0_1_H
#define ADRSPC_PCIESW2_DP_1_BA_L   ADRSPC_PCIE_IM_1_BAR0_1_L
#define ADRSPC_PCIESW2_DP_1_END_H  ADRSPC_PCIE_IM_1_END_H
#define ADRSPC_PCIESW2_DP_1_END_L  ADRSPC_PCIE_IM_1_END_L

/*
 * PCIe Switch Downstream Port 2 Address Map
 */
#define ADRSPC_PCIESW2_DP_2_BA_H   ADRSPC_PCIE_IM_2_BAR0_1_H
#define ADRSPC_PCIESW2_DP_2_BA_L   ADRSPC_PCIE_IM_2_BAR0_1_L
#define ADRSPC_PCIESW2_DP_2_END_H  ADRSPC_PCIE_IM_2_END_H
#define ADRSPC_PCIESW2_DP_2_END_L  ADRSPC_PCIE_IM_2_END_L

/*
 * PCIe Switch Downstream Port 3 Address Map
 */
#define ADRSPC_PCIESW2_DP_3_BA_H   ADRSPC_PCIE_IM_3_BAR0_1_H
#define ADRSPC_PCIESW2_DP_3_BA_L   ADRSPC_PCIE_IM_3_BAR0_1_L
#define ADRSPC_PCIESW2_DP_3_END_H  ADRSPC_PCIE_IM_3_END_H
#define ADRSPC_PCIESW2_DP_3_END_L  ADRSPC_PCIE_IM_3_END_L

/*
 * PCIe Switch Downstream Port 4 Address Map
 */
#define ADRSPC_PCIESW2_DP_4_BA_H   ADRSPC_PCIE_IM_4_BAR0_1_H
#define ADRSPC_PCIESW2_DP_4_BA_L   ADRSPC_PCIE_IM_4_BAR0_1_L
#define ADRSPC_PCIESW2_DP_4_END_H  ADRSPC_PCIE_IM_4_END_H
#define ADRSPC_PCIESW2_DP_4_END_L  ADRSPC_PCIE_IM_4_END_L

/*
 * PCIe Switch Downstream Port 5 Address Map
 */
#define ADRSPC_PCIESW2_DP_5_BA_H   ADRSPC_PCIE_IM_5_BAR0_1_H
#define ADRSPC_PCIESW2_DP_5_BA_L   ADRSPC_PCIE_IM_5_BAR0_1_L
#define ADRSPC_PCIESW2_DP_5_END_H  ADRSPC_PCIE_IM_5_END_H
#define ADRSPC_PCIESW2_DP_5_END_L  ADRSPC_PCIE_IM_5_END_L

/*
 * PCIe Switch Downstream Port 6 Address Map
 */
#define ADRSPC_PCIESW2_DP_6_BA   ADRSPC_PCIE_HOFF_BA
#define ADRSPC_PCIESW2_DP_6_END  ADRSPC_PCIE_HOFF_END

#endif


/*
 * PCIe Switch Upstream Port Address Map
 */ 
#define ADRSPC_PCIESW_UP_BA_H   ADRSPC_PCIE_CPU_BAR0_1_H
#define ADRSPC_PCIESW_UP_BA_L   ADRSPC_PCIE_CPU_BAR0_1_L
#define ADRSPC_PCIESW_UP_END_H  0x00000007
#define ADRSPC_PCIESW_UP_END_L  0xFFFFFFFF
/* Non-prefetchable memory base, 32-bit addressing */
#define ADRSPC_PCIESW_UP_NP_BA  0xE0000000
#define ADRSPC_PCIESW_UP_NP_END 0xFFFFFFFF

/*
 * PCIe Switch Downstream Port 0 Address Map
 */ 
#define ADRSPC_PCIESW_DP_0_BA   ADRSPC_PCIE_NILE_0_BA
#define ADRSPC_PCIESW_DP_0_END  ADRSPC_PCIE_NILE_1_END

/*
 * PCIe Switch Downstream Port 1 Address Map
 */ 
#define ADRSPC_PCIESW_DP_1_BA   ADRSPC_PCIE_HOFF_BA
#define ADRSPC_PCIESW_DP_1_END  ADRSPC_PCIE_HOFF_END

/*
 * PCIe Switch Downstream Port 2 Address Map
 */ 
#define ADRSPC_PCIESW_DP_2_BA_H   ADRSPC_PCIE_IM_0_BAR0_1_H
#define ADRSPC_PCIESW_DP_2_BA_L   ADRSPC_PCIE_IM_0_BAR0_1_L
#define ADRSPC_PCIESW_DP_2_END_H  ADRSPC_PCIE_IM_0_END_H
#define ADRSPC_PCIESW_DP_2_END_L  ADRSPC_PCIE_IM_0_END_L

/*
 * PCIe Switch Downstream Port 3 Address Map
 */ 
#define ADRSPC_PCIESW_DP_3_BA_H   ADRSPC_PCIE_IM_1_BAR0_1_H
#define ADRSPC_PCIESW_DP_3_BA_L   ADRSPC_PCIE_IM_1_BAR0_1_L
#define ADRSPC_PCIESW_DP_3_END_H  ADRSPC_PCIE_IM_1_END_H
#define ADRSPC_PCIESW_DP_3_END_L  ADRSPC_PCIE_IM_1_END_L

/*
 * PCIe Switch Downstream Port 4 Address Map
 */ 
#define ADRSPC_PCIESW_DP_4_BA_H   ADRSPC_PCIE_IM_2_BAR0_1_H
#define ADRSPC_PCIESW_DP_4_BA_L   ADRSPC_PCIE_IM_2_BAR0_1_L
#define ADRSPC_PCIESW_DP_4_END_H  ADRSPC_PCIE_IM_2_END_H
#define ADRSPC_PCIESW_DP_4_END_L  ADRSPC_PCIE_IM_2_END_L

/*
 * PCIe Switch Downstream Port 5 Address Map
 */ 
#define ADRSPC_PCIESW_DP_5_BA_H   ADRSPC_PCIE_IM_3_BAR0_1_H
#define ADRSPC_PCIESW_DP_5_BA_L   ADRSPC_PCIE_IM_3_BAR0_1_L
#define ADRSPC_PCIESW_DP_5_END_H  ADRSPC_PCIE_IM_3_END_H
#define ADRSPC_PCIESW_DP_5_END_L  ADRSPC_PCIE_IM_3_END_L

/*
 * PCIe Switch Downstream Port 6 Address Map
 */ 
#define ADRSPC_PCIESW_DP_6_BA_H   ADRSPC_PCIE_IM_4_BAR0_1_H
#define ADRSPC_PCIESW_DP_6_BA_L   ADRSPC_PCIE_IM_4_BAR0_1_L
#define ADRSPC_PCIESW_DP_6_END_H  ADRSPC_PCIE_IM_4_END_H
#define ADRSPC_PCIESW_DP_6_END_L  ADRSPC_PCIE_IM_4_END_L

/*
 * PCIe Switch Downstream Port 7 Address Map
 */ 
#define ADRSPC_PCIESW_DP_7_BA_H   ADRSPC_PCIE_IM_5_BAR0_1_H
#define ADRSPC_PCIESW_DP_7_BA_L   ADRSPC_PCIE_IM_5_BAR0_1_L
#define ADRSPC_PCIESW_DP_7_END_H  ADRSPC_PCIE_IM_5_END_H
#define ADRSPC_PCIESW_DP_7_END_L  ADRSPC_PCIE_IM_5_END_L

/*
 * PCIe-PCI Bridge Address Map
 */ 
#define ADRSPC_PCIE_BRG_BA      ADRSPC_PCIE_NILE_0_BA
#define ADRSPC_PCIE_BRG_END     ADRSPC_PCIE_NILE_1_END

/*
 * Nile PCI Base Address (32-bit address)
 * This base address needs to be configured to BAR0 on Arsenic iface
 */ 
#define ADRSPC_PCI_NILE_SIZE    ADRSPC_PCIE_NILE_SIZE 
#define ADRSPC_PCI_NILE_0_BA    ADRSPC_PCIE_NILE_0_BA
#define ADRSPC_PCI_NILE_0_END   \
       (ADRSPC_PCI_NILE_0_BA + (ADRSPC_PCI_NILE_SIZE - 1))
#define ADRSPC_PCI_NILE_1_BA    ADRSPC_PCIE_NILE_1_BA      
#define ADRSPC_PCI_NILE_1_END   \
       (ADRSPC_PCI_NILE_1_BA + (ADRSPC_PCI_NILE_SIZE - 1))

/* PCIe Inbound Base Address */
#define ADRSPC_PCI_CSSR_BA      0x0A0000000ULL


/*
 * Address used for memory pools.
 */
#define PHY_DRAM_MAP_POOL   0x10000000    /* DRAM_MAP phy starts at 256MB */
#define VIR_DRAM_MAP_POOL   0x10000000    /* DRAM_MAP vir starts at 256MB */

/*
 * Definition for ROMMON used for bus error testing. On this platform with
 * PowerQUICC there is no real KSEG space but these definitions
 * help facilitate porting the existing code from MARs which is
 * a MIPs based platform.
 */ 
#define ADRSPC_BAD_ADDR         0xbad0add0    /* for stack.c */
#define KSEG1_ADRSPC_BAD_ADDR   0xbad0add0    /* for bus error test */
#define ADRSPC_K1BASE           0x0           /* for bus error test */

/*
 * USB
 */
#define ADRSPC_USB0             (ADRSPC_PQUICC_IMEMB + 0x00022000)

/*
 * CPLD
 */
#ifndef RUDY /* Sunridges */
#define ADRSPC_PLD              0xDF000000
#endif 

#ifndef RSP2_BOARD
#define ADRSPC_IO_CPLD          0xFC000000
#else
#define ADRSPC_IO_CPLD          ADRSPC_PHY_HOFF_BA
#endif

/*
 * DUART
 */
#define ADRSPC_DUART0   (ADRSPC_PQUICC_IMEMB + 0x00004500)   /* P2020 DUART0 */
#define ADRSPC_DUART1   (ADRSPC_PQUICC_IMEMB + 0x00004600)   /* P2020 DUART1 */
#define ADRSPC_DUART_SPACE_SZ   0x1000    /* use 16 bytes, allow 4k space */
#define KSEG1_ADRSPC_DUART      ADRSPC_DUART0  /* Used by ROMMON */

/*
 * BITS Framer
 */ 
#define ADRSPC_BITS         0xFC200000  /* CS4 BITS controller */
#define ADRSPC_BITS_SZ      0x00010000  /* 64 KB */

/*
 * Start address of the physical device
 */
#define ADRSPC_PROM                     ADRSPC_MAINBOARD_FLASH
#define ADRSPC_FLASH_START              ADRSPC_MAINBOARD_FLASH
#define ADRSPC_MAINBOARD_FLASH_BASE     ADRSPC_FLASH_START
#define ADRSPC_BOOTROM                  ADRSPC_PROM
#define ADRSPC_MAINBOARD_FLASH_SIZE     0x800000   /* 8MB */

#define FLASH_SECTOR_SIZE               (128*1024)  /* One sector 128K */

/* kody: From Perfect Strom (TBD) */
#ifdef PSMB
#define ADRSPC_NVRAM            0xE2000000
#else
#define ADRSPC_NVRAM            0xFF800000
#endif


/*
 * Macros for PCI fixed constants
 */
#define PCI_BUS_0      0
#define PCI_BUS_1      1
#define PCI_BUS_2      2
#define PCI_BUS_3      3
#define PCI_BUS_4      4
#define PCI_BUS_5      5
#define PCI_BUS_6      6
#define PCI_BUS_7      7
#define PCI_BUS_8      8
#define PCI_BUS_9      9
#define PCI_BUS_10     10

#define PCI_DEV_0      0
#define PCI_DEV_1      1
#define PCI_DEV_2      2
#define PCI_DEV_3      3
#define PCI_DEV_4      4

#define PCI_FUN_0      0
#define PCI_FUN_1      1
#define PCI_FUN_2      2
#define PCI_FUN_3      3

/* Platform specific ESDHC definitions
 *
 */
#define ESDHC_SYSCTL_DTOCV_DEFAULT ESDHC_SYSCTL_DTOCV_26

/* safe settings, could be pushed higher for better performance */
#define ESDHC_SYSCTL_CLK_HI (ESDHC_SYSCTL_SDCLKFS_2 | ESDHC_SYSCTL_DVS_5)
#define ESDHC_SYSCTL_CLK_LO (ESDHC_SYSCTL_SDCLKFS_256 | ESDHC_SYSCTL_DVS_4)


#ifdef RUDY

/* 
 * CS0 - Boot PROM
 *
 * Base Addr: 0xFF800000
 * Size:      8 MB
 * BR0:       0xFF800801
 * OR0:       0xFF800852
 */
#define INIT_PQUICC_BR0 (ADRSPC_MAINBOARD_FLASH | \
                         P2020_BR_8_BIT | \
                         P2020_BR_NO_PARITY | \
                         P2020_BR_READ_WRITE | \
                         P2020_BR_MACH_GPCM | \
                         P2020_BR_VALID) 
#define INIT_PQUICC_BR0_RAW 0xFF800801
#define INIT_PQUICC_OR0 (P2020_OR_8M_MASK | \
                         P2020_OR_LBCTL_ASSERTED | \
                         P2020_OR_CS_NEG_NORM | \
                         P2020_OR_CS_NORM | \
                         P2020_OR_CS_NO_ADD_EXTRA | \
                         P2020_OR_WAIT_STATES(5) | \
                         P2020_OR_INTERNAL_ADDR_TERM | \
                         P2020_OR_TIMING_NORMAL | \
                         P2020_OR_NORMAL_HOLD_TIME | \
                         P2020_OR_NORMAL_LATCH_DELAY)
#define INIT_PQUICC_OR0_RAW 0xFF800050

/* 
 * CS1 - IO-CPLD
 *
 * Base Addr: 0x80000000
 * Size:      32KB
 */
#define INIT_PQUICC_BR1 (ADRSPC_IO_CPLD | \
                         P2020_BR_32_BIT | \
                         P2020_BR_NO_PARITY | \
                         P2020_BR_READ_WRITE | \
                         P2020_BR_MACH_GPCM | \
                         P2020_BR_VALID) 
#define INIT_PQUICC_OR1 (P2020_OR_32K_MASK | \
                         P2020_OR_LBCTL_ASSERTED | \
                         P2020_OR_CS_NEG_QTR_EARLY | \
                         P2020_OR_CS_NORM | \
                         P2020_OR_CS_NO_ADD_EXTRA | \
                         P2020_OR_WAIT_STATES(8) | \
                         P2020_OR_INTERNAL_ADDR_TERM | \
                         P2020_OR_TIMING_NORMAL | \
                         P2020_OR_NORMAL_HOLD_TIME | \
                         P2020_OR_NORMAL_LATCH_DELAY)
                            
/* 
 * CS4 - BITS (TBD, need to confirm w/ Rudy HW team)
 *
 * Base Addr: 0x80308000
 * Size:      32KB
 */
#define INIT_PQUICC_BR4 (ADRSPC_BITS | \
                         P2020_BR_8_BIT | \
                         P2020_BR_NO_PARITY | \
                         P2020_BR_READ_WRITE | \
                         P2020_BR_VALID) 
#define INIT_PQUICC_OR4 (P2020_OR_32K_MASK | \
                         P2020_OR_LBCTL_NOT_ASSERTED | \
                         P2020_OR_CS_NEG_QTR_EARLY | \
                         P2020_OR_CS_NORM | \
                         P2020_OR_TIMING_NORMAL | \
                         P2020_OR_WAIT_STATES(2) | \
                         P2020_OR_TIMING_RELAXED | \
                         P2020_OR_EXTEND_LATCH_DELAY)


#else /* Sunridges */
/* 
 * CS0 - Boot PROM
 *
 * Base Addr: 0xFF000000
 * Size:      16 MB
 * BR0:       0xFF001001
 * OR0:       0xFF000852
 */
#define INIT_PQUICC_BR0 (0xFF000000 | \
                         P2020_BR_16_BIT | \
                         P2020_BR_NO_PARITY | \
                         P2020_BR_READ_WRITE | \
                         P2020_BR_MACH_GPCM | \
                         P2020_BR_VALID) 
#define INIT_PQUICC_BR0_RAW 0xFF001001
#define INIT_PQUICC_OR0 (P2020_OR_16M_MASK | \
                         P2020_OR_LBCTL_ASSERTED | \
                         P2020_OR_CS_NEG_QTR_EARLY | \
                         P2020_OR_CS_NORM | \
                         P2020_OR_CS_NO_ADD_EXTRA | \
                         P2020_OR_WAIT_STATES(5) | \
                         P2020_OR_INTERNAL_ADDR_TERM | \
                         P2020_OR_TIMING_NORMAL | \
                         P2020_OR_EXTEND_HOLD_TIME | \
                         P2020_OR_NORMAL_LATCH_DELAY)
#define INIT_PQUICC_OR0_RAW 0xFF000852

/* 
 * CS1 - CPLD (SUNRIDGES)
 *
 * Base Addr: 0xDF000000
 * Size:      1M
 */
#define INIT_PQUICC_BR1 (0xDF000000 | \
                            P2020_BR_8_BIT | \
                            P2020_BR_NO_PARITY | \
                            P2020_BR_READ_WRITE | \
                            P2020_BR_MACH_GPCM | \
                            P2020_BR_VALID) 
#define INIT_PQUICC_OR1 (P2020_OR_1M_MASK | \
                            P2020_OR_LBCTL_NOT_ASSERTED | \
                            P2020_OR_CS_NEG_QTR_EARLY | \
                            P2020_OR_CS_NORM | \
                            P2020_OR_CS_ADD_EXTRA_LATE | \
                            P2020_OR_WAIT_STATES(0x2) | \
                            P2020_OR_INTERNAL_ADDR_TERM | \
                            P2020_OR_TIMING_NORMAL | \
                            P2020_OR_NORMAL_HOLD_TIME | \
                            P2020_OR_NORMAL_LATCH_DELAY)

/*
 * CS4 - 
 *
 * Base Addr: 0xDF500000
 * Size:      1 MB
 * BR1:       0xDF0018C1
 * OR1:       0xFFF00810
 */
#define INIT_PQUICC_BR4 (0xDF500000 | \
                         P2020_BR_32_BIT | \
                         P2020_BR_NO_PARITY | \
                         P2020_BR_READ_WRITE | \
                         P2020_BR_MACH_UPMC | \
                         P2020_BR_VALID)
#define INIT_PQUICC_OR4 (P2020_OR_1M_MASK | \
                         P2020_OR_LBCTL_ASSERTED | \
                         P2020_OR_CS_NEG_QTR_EARLY | \
                         P2020_OR_CS_NORM | \
                         P2020_OR_CS_NO_ADD_EXTRA | \
                         P2020_OR_WAIT_STATES(1) | \
                         P2020_OR_INTERNAL_ADDR_TERM | \
                         P2020_OR_TIMING_NORMAL | \
                         P2020_OR_NORMAL_HOLD_TIME | \
                         P2020_OR_NORMAL_LATCH_DELAY)


#endif

#endif /* __RUDY_PCMAP_H__ */

/******** History ******** 
$Log: not supported by cvs2svn $
Revision 1.2  2012/06/07 06:21:38  yasdixit
Updated address macro's for RSP2 Board

Revision 1.1.1.1  2012/04/25 05:27:23  krreddy
--------------------------------------------------------------------------------
-----krreddy 04/25/2012------
Initial code import for RSP2 diags based on rudy_diag_V0.4.8.tar.gz code base
received from Foxconn
--------------------------------------------------------------------------------

Revision 1.2.2.20  2011/10/05 04:29:17  jackie
support RSP+ init and 3.5G memory test

Revision 1.2.2.19  2011/07/15 08:24:51  jackie
move diag stack to 0x008FFFF0

Revision 1.2.2.18  2011/03/31 00:07:16  jackie
add QDR memory test support

Revision 1.2.2.17  2011-01-28 00:03:16  jackie
add for dcache flush

Revision 1.2.2.16  2011-01-26 22:15:45  jackie
Add Diag Stack base

Revision 1.2.2.15  2011-01-24 19:37:49  kuangik
Update the PCIe memory address and PCIe switch bringup

Revision 1.2.2.14  2011-01-19 21:39:08  jackie
update BITS(CS4) OR/BR setting

Revision 1.2.2.13  2011-01-18 19:25:36  kuangik
Update some register definition and modify PCIe addressing scheme

Revision 1.2.2.12  2011-01-14 23:54:03  jackie
update mmap for PCIe

Revision 1.2.2.11  2011-01-13 22:17:51  jackie
update pcmap per rev11 Rudy common spec

Revision 1.2.2.9  2010-11-03 06:39:58  kuangik
Modify PCIe switch and Snake Utility according to review comments

Revision 1.2.2.8  2010/10/22 08:20:52  kuangik
Add Nile initialization and Snake Utility

Revision 1.2.2.7  2010/10/11 08:32:06  kody
Modify for IOCPLD memory map address

Revision 1.2.2.6  2010/10/11 06:42:13  kuangik
Update the code for PCIe enumeration

Revision 1.2.2.4  2010/09/29 09:13:26  kuangik
Add Nile Base Address

Revision 1.2.2.3  2010/09/23 05:38:22  jackie
code clean-up

Revision 1.2.2.2  2010/09/14 09:49:19  jackie
initial check-in to support SD Card tests

Revision 1.2.2.1  2010/09/13 08:52:38  james
IM menu initial check-in

Revision 1.2  2010/08/27 03:13:34  kody
Porting Boot Flash code from PerfectStorm

Revision 1.1.1.1  2010/08/19 13:04:55  jackie
initial check-in for Rudy and new projects

$Endlog$
*/


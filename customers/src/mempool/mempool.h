/* $Id: free.h,v 1.1.1.1 2012-04-25 05:28:30 krreddy Exp $
 * $Source: /svn/sandbox/prajpai_dump_area/CVS/Repo1/diag/common/include/free.h,v $
 *------------------------------------------------------------------
 */

#define MALLOC_SHOW_BUF_BYTES  32   /* # bytes to display from malloc buf */

#define MALLOC_BYTE_Mempool_AlignMENT  64   /* Power of two only */

typedef char Mempool_Align[MALLOC_BYTE_Mempool_AlignMENT];

typedef union mempool_free_header_t_ {
    struct {
	union mempool_free_header_t_  *freeptr; /* next free block */
	unsigned long         size;     /* # header-sized units of this block */
	unsigned short        padding;  /* fill struct to long word */
	unsigned short        pool_id;
    } s;
    Mempool_Align a;
} mempool_free_header_t;

typedef enum cisco_mempool_class_ {
    CISCO_MEMPOOL_CLASS_UNKNOWN  = 0,
    CISCO_MEMPOOL_CLASS_DRAM,
    CISCO_MEMPOOL_CLASS_DRAM_DEV,
    CISCO_MEMPOOL_CLASS_PCIMEM,
    CISCO_MEMPOOL_CLASS_IOMEM,
    CISCO_MEMPOOL_CLASS_SHMEM,
    CISCO_MEMPOOL_CLASS_SPACE_1,
    CISCO_MEMPOOL_CLASS_SPACE_2,
    CISCO_MEMPOOL_CLASS_SPACE_3,
    CISCO_MEMPOOL_CLASS_SPACE_4,
    CISCO_MEMPOOL_CLASS_SPACE_5,
    CISCO_MEMPOOL_CLASS_SPACE_6,
    CISCO_MEMPOOL_CLASS_SPACE_7,
    CISCO_MEMPOOL_CLASS_SPACE_8,
    CISCO_MEMPOOL_CLASS_SPACE_9,
    CISCO_MEMPOOL_CLASS_SPACE_10,
    CISCO_MEMPOOL_CLASS_MAPPED_1,
    CISCO_MEMPOOL_CLASS_MAPPED_2,
    CISCO_MEMPOOL_CLASS_MAPPED_3,
    CISCO_MEMPOOL_CLASS_MAPPED_4,
    CISCO_MEMPOOL_CLASS_MAPPED_5,
    CISCO_MEMPOOL_CLASS_MAPPED_6,
    CISCO_MEMPOOL_CLASS_MAPPED_7,
    CISCO_MEMPOOL_CLASS_MAPPED_8,
    CISCO_MEMPOOL_CLASS_KERNEL,   /* Used exclusively by the kernel */
    CISCO_MEMPOOL_CLASS_MAX
} cisco_mempool_class;

typedef struct cisco_mempool_t_ {
    char              *name;              /* name of memory pool */
    cisco_mempool_class     class;              /* memory category */
    void              *membase;           /* first location of this memory */ 
    unsigned long     memlen;             /* length of this pool in bytes */
    mempool_free_header_t     *freep;             /* next available block */
    void              (*moremem)(void *); /* if !NULL, for more if needed */
    void              *moremem_param;     /* param for moremem call */
} cisco_mempool_t;

/*
 * Entry for table that specifies mempools to be initialized on
 * the platform.
 */
typedef struct cisco_mempools_table_t_ {
    char              *name;              /* name of memory pool */
    cisco_mempool_class     class;              /* pool class */
    unsigned long     phy_base;           /* physical address of pool */
    unsigned long     vir_base;           /* virtual address of pool */
    unsigned long     max_size;           /* max length in bytes */
    unsigned long     act_size;           /* actual length (<= max_size) */
    void* mem;
} cisco_mempools_table_t;

/*
 * Descriptor that specifies the mempools table initialized on the
 * platform.
 */
typedef struct cisco_mempools_tbl_desc_t_ {
    cisco_mempools_table_t   *table;             /* initialized table */
    unsigned long      num_pools;          /* # that span actual memory on mb */
    unsigned long      rnd_rbn;            /* next index of pool for access */
} cisco_mempools_tbl_desc_t;


/*
 * Prototypes
 */
void * cisco_malloc (unsigned long nbytes);
void cisco_free (void *buf);
void * cisco_realloc (void *ptr, unsigned long size);
void cisco_init_for_malloc_use(void *mem, unsigned long size);

#define  POOL_TO_PHY(vaddr)  (map_pool_to_phy(vaddr))
#define  PHY_TO_POOL(paddr)  (map_phy_to_pool(paddr))

/******** History ******** 
$Log: not supported by cvs2svn $
Revision 1.1.1.1  2010/08/19 13:04:55  jackie
initial check-in for Rudy and new projects

$Endlog$
*/


/* $Id: types.h,v 1.2 2013-05-22 09:02:21 shvashis Exp $
 * $Source: /svn/sandbox/prajpai_dump_area/CVS/Repo1/diag/common/include/types.h,v $
 *------------------------------------------------------------------
 */
#ifndef __TYPES_H
#define __TYPES_H

typedef int  (*PFI)();
typedef long  (*PFL)();
typedef void (*PFV)();

typedef	unsigned char	    u_char;
typedef	unsigned short	    u_short;
typedef	unsigned int	    u_int;
typedef	unsigned long	    u_long;
typedef unsigned char	    uchar;	/* System V */
typedef	unsigned short	    ushort;	/* System V */
typedef	unsigned int	    uint;	/* System V */
typedef unsigned long	    ulong;	/* System V */
typedef int		    boolean;
typedef long long           llong;
typedef unsigned long long  ullong;

#ifndef AT_SYSTEM_TYPE_H
typedef unsigned char       uint8;	/* 8-bit */
typedef unsigned short      uint16;	/* 16-bit*/
typedef unsigned int        uint32;	/* 32-bit */
typedef unsigned long long  uint64;
typedef unsigned long long  u64;
#endif

typedef unsigned short      uint16_t;	/* 16-bit */
typedef unsigned char       uint8_t;	/* 8-bit */
typedef unsigned int        uint32_t;	/* 32-bit */
typedef short               int16_t;
typedef int                 int32_t;

typedef unsigned char       tinybool;

/*
 * charint data structure
 * Used for manipulating ints, shorts, and bytes.
 */

typedef struct charint_ {
	union {
	    uchar byte[4];
	    uint  lword;
	    ushort sword[2];
	} d;
} charint;

#define PFT PFI
typedef unsigned int        utype_t;
typedef int                 type_t;
#define get_line(ptr, size) getline(ptr, size)
#define REG_EXT &reg_ext
typedef char                int8_t;	/* 8-bit */
typedef long long           int64_t;
#ifndef _SIZE_T_
#define _SIZE_T_ __typeof(sizeof(int))
typedef _SIZE_T_ size_t;
#endif

//typedef unsigned int        size_t;
typedef unsigned long long  uint64_t;
typedef unsigned int *      uintptr_t;

#endif /* TYPES.H */

/* end of module */

/******** History ******** 
$Log: not supported by cvs2svn $
Revision 1.1.1.1  2012/04/25 05:28:29  krreddy
--------------------------------------------------------------------------------
-----krreddy 04/25/2012------
Initial code import for RSP2 diags based on rudy_diag_V0.4.8.tar.gz code base
received from Foxconn
--------------------------------------------------------------------------------

Revision 1.1.1.1  2010/08/19 13:04:55  jackie
initial check-in for Rudy and new projects

$Endlog$
*/


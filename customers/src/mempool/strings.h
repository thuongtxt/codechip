/* $Id: strings.h,v 1.1.1.1 2012-04-25 05:28:29 krreddy Exp $
 * $Source: /svn/sandbox/prajpai_dump_area/CVS/Repo1/diag/common/include/strings.h,v $
 *------------------------------------------------------------------
 */

#ifndef __STRINGS_H__
#define __STRINGS_H__
/*
** strings prototypes
*/

extern char *strchr(char *sp, char c);
extern char *strcat(char *s1, char *s2);
extern int strcmp(char *s1, char *s2);
extern char *strcpy(char *s1, char *s2);
extern int strlen(char *s);
extern char *strncat(char *s1, char *s2, int n);
extern int strncmp(char *s1, char *s2, int n);
extern char *strncpy(char *s1, char *s2, int n);
extern char *strstr ( char *long_string, char *sub_string); /* find first occurance 
							     *of sub_string in long_string */
extern unsigned long strtoul (const char *s, char **ptr, int base);
extern int strcasecmp (const char *s1, const char *s2);
extern void bzero (void *dummy, int count);
extern int  bcopy(char *source, char *destation, int bytes);

extern char *memcpy(char *dst, const char *src, int nbytes);
extern char *memset (char *ptr, int val, int len);
extern void puts(char *cptr);
extern void putchar(char c);
extern char getchar(void);

#endif /* __STRINGS_H__ */

/******** History ******** 
$Log: not supported by cvs2svn $
Revision 1.1.1.1  2010/08/19 13:04:55  jackie
initial check-in for Rudy and new projects

$Endlog$
*/


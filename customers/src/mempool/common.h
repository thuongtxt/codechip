/* $Id: common.h,v 1.5 2013-11-13 05:06:25 ramakatt Exp $
 * $Source: /svn/sandbox/prajpai_dump_area/CVS/Repo1/diag/common/include/common.h,v $
 *------------------------------------------------------------------
 */

#ifndef __COMMON_H__
#define __COMMON_H__
/*
 * The purpose of this file is for generic C defines only. It is not
 * being used for any platform related defines.
 *
 * If the defines is cross platform, then please use cross_platform.h
 */

#define EQUAL      1
#define NOT_EQUAL  0
#define EQ	   EQUAL
#define NEQ        NOT_EQUAL
#define NOTEQUAL   NOT_EQUAL

#define OK	   PASSED
#define NOT_OK     FAILED
#define NOTOK      FAILED
#define PASS	   PASSED
#define FAIL	   FAILED
#define FAIL_RETRY 0x88

#define ENABLE	   1
#define DISABLE	   0

#define FULL_TEST  1
#define IFACE_TEST 2

#ifndef NULL
#define NULL	   0
#endif
#define NULL_PTR   (void *)0

#define POLL_MODE        0
#define INTERRUPT_MODE   1
#define INTR_MODE        INTERRUPT_MODE

#define PATTERN   0x5ADBA56C

/*
 * Basic memory sizes
 */
#define ONE_K            0x00000400
#define TWO_K            0x00000800
#define FOUR_K           0x00001000
#define EIGHT_K          0x00002000
#define SIXTEEN_K        0x00004000
#define THIRTYTWO_K      0x00008000
#define SIXTYFOUR_K      0x00010000
#define ONE28_K          0x00020000
#define TWO56_K          0x00040000
#define FIVE12_K         0x00080000
 
#define HALF_MEG         0x00080000
#define ONE_MEG          0x00100000
#define TWO_MEG          0x00200000
#define THREE_MEG        0x00300000 
#define FOUR_MEG         0x00400000
#define EIGHT_MEG        0x00800000
#define SIXTEEN_MEG      0x01000000
#define THIRTYTWO_MEG    0x02000000
#define SIXTYFOUR_MEG    0x04000000
#define ONE28_MEG        0x08000000
#define TWO56_MEG        0x10000000
#define FIVE12_MEG       0x20000000
#define MEG(x)           ((x) * 1024 * 1024)
 
#define ONE_GIG          0x40000000
#define TWO_GIG          0x80000000
#define THREE_GIG        0xC0000000

#define MASK_2B          0x00000001      /* 2byte - 16-bit aligned */
#define MASK_4B          0x00000003
#define MASK_8B          0x00000007
#define MASK_16B         0x0000000f
#define MASK_32B         0x0000001f
#define MASK_64B         0x0000003f
#define MASK_128B        0x0000007f
#define MASK_4K          0x00000FFF      /* 4Kbyte aligned */
#define MASK_8K          0x00001FFF
#define MASK_16K         0x00003FFF
#define MASK_32K         0x00007FFF
#define MASK_64K         0x0000FFFF

#define ALLIGN_8B(x)   ((void *)((uchar *)((ulong)(x + MASK_8B) & ~MASK_8B)))
#define ALLIGN_16B(x)  ((void *)((uchar *)((ulong)(x + MASK_16B) & ~MASK_16B)))
#define ALLIGN_32B(x)  ((void *)((uchar *)((ulong)(x + MASK_32B) & ~MASK_32B)))
#define ALLIGN_64B(x)  ((void *)((uchar *)((ulong)(x + MASK_64B) & ~MASK_64B)))
#define ALLIGN_128B(x)  ((void *)((uchar *)((ulong)(x + MASK_128B) & ~MASK_128B)))
#define ALLIGN_4K(x)   ((void *)((uchar *)((ulong)(x + MASK_4K) & ~MASK_4K)))
#define ALLIGN_64K(x)  ((void *)((uchar *)((ulong)(x + MASK_64K) & ~MASK_64K)))

#define ALIGN(ptr, sz) (((ulong)(ptr) + (sz - 1)) & ~(sz - 1))

#define LONG_SWAP(x)   ((((x)&0xff)<<24) + (((x)&0xff00)<<8) + \
                         (((x)&0xff0000)>>8) + (((x)&0xff000000)>>24))

#define BIT_0                        0x00000001
#define BIT_1                        0x00000002
#define BIT_2                        0x00000004
#define BIT_3                        0x00000008
#define BIT_4                        0x00000010
#define BIT_5                        0x00000020
#define BIT_6                        0x00000040
#define BIT_7                        0x00000080
#define BIT_8                        0x00000100
#define BIT_9                        0x00000200
#define BIT_10                       0x00000400
#define BIT_11                       0x00000800
#define BIT_12                       0x00001000
#define BIT_13                       0x00002000
#define BIT_14                       0x00004000
#define BIT_15                       0x00008000
#define BIT_16                       0x00010000
#define BIT_17                       0x00020000
#define BIT_18                       0x00040000
#define BIT_19                       0x00080000
#define BIT_20                       0x00100000
#define BIT_21                       0x00200000
#define BIT_22                       0x00400000
#define BIT_23                       0x00800000
#define BIT_24                       0x01000000
#define BIT_25                       0x02000000
#define BIT_26                       0x04000000
#define BIT_27                       0x08000000
#define BIT_28                       0x10000000
#define BIT_29                       0x20000000
#define BIT_30                       0x40000000
#define BIT_31                       0x80000000

/* Definitions for memory flag */
#define MEM_INCFILL                  0x01
#define MEM_BUSERR                   0x02

typedef enum {
    MODE_T1 = 0,
    MODE_E1,
    MODE_CHAN_T1,
    MODE_CHAN_E1,
    MODE_8M,
    MODE_8M_INT,         /* 8M Interleave */
    MODE_8M_INT_T1,
    MODE_UNF_T1,
    MODE_UNF_E1,
    MODE_T3,
    MODE_E3,
    MODE_CHAN_T3,
    MODE_CHAN_E3
#ifdef RSP2_BOARD
    MODE_ESF_T1,
    MODE_CRC4_E1,
    MODE_J1,
    MODE_ESF_J1,
    MODE_64K_8K,
    MODE_64K_400HZ,
#endif
}mode_t_;

/*
 * Macros
 */
#ifdef NDEBUG
#define assert(pred) { }
#else
#define assert(pred) { if(!(pred)) \
                     { printf("\n\nASSERTION FAILED: (%s)\n" \
                              "file %s, line %d\n\n", \
                              #pred, __FILE__, __LINE__); \
                              err('w', 0, "Assertion Failed"); } }
#endif /* NDEBUG */

#endif /* __COMMON_H__ */
/* End of module */

/******** History ******** 
$Log: not supported by cvs2svn $
Revision 1.4  2013/10/08 05:59:12  ramakatt
J1 configuration loop back test added

Revision 1.3  2012/07/17 12:29:24  trgch
*** empty log message ***

Revision 1.2  2012/07/13 12:03:52  yasdixit
Added modes for Bits

Revision 1.1.1.1  2012/04/25 05:28:29  krreddy
--------------------------------------------------------------------------------
-----krreddy 04/25/2012------
Initial code import for RSP2 diags based on rudy_diag_V0.4.8.tar.gz code base
received from Foxconn
--------------------------------------------------------------------------------

Revision 1.3.2.1  2010/09/13 09:01:56  james

Add the enum.

Revision 1.3  2010/09/02 07:44:55  kody
Add and modify for GE PHY test

Revision 1.2  2010/09/01 11:29:17  jackie
fix compile error in common_utils.c

Revision 1.1.1.1  2010/08/19 13:04:55  jackie
initial check-in for Rudy and new projects

$Endlog$
*/


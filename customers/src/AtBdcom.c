/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BDCOM
 *
 * File        : AtBdcom.c
 *
 * Created Date: Oct 5, 2013
 *
 * Description : BDCOM specific implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtBdcom.h"
#include "AtEthFlow.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern eAtRet Tha60060011PppLinkPidTableTdmToPsnPppHeaderSet(AtPppLink self, uint8 entryId, uint32 pppHeader, uint8 headerLength);
extern eAtRet Tha60060011PppLinkPidTableTdmToPsnEthTypeSet(AtPppLink self, uint8 entryId, uint32 ethType);
extern eAtRet Tha60060011PppLinkPidTablePsnToTdmPppHeaderSet(AtPppLink self, uint8 entryId, uint32 pppHeader, uint8 headerLength);
extern eAtRet Tha60060011PppLinkPidTablePsnToTdmEthTypeSet(AtPppLink self, uint8 entryId, uint32 ethType);
extern uint32 Tha60060011PppLinkPidTableTdmToPsnPppHeaderGet(AtPppLink self, uint8 entryId, uint8 *headerLength);
extern uint32 Tha60060011PppLinkPidTableTdmToPsnEthTypeGet(AtPppLink self, uint8 entryId);
extern uint32 Tha60060011PppLinkPidTablePsnToTdmPppHeaderGet(AtPppLink self, uint8 entryId, uint8 *headerLength);
extern uint32 Tha60060011PppLinkPidTablePsnToTdmEthTypeGet(AtPppLink self, uint8 entryId);

/*--------------------------- Implementation ---------------------------------*/
static tAtEthVlanTag *SVlanFromBdcomTag(const tAtBdcomTag *tag, tAtEthVlanTag *sVlan)
    {
    sVlan->cfi      = tag->cpuPktIndicator;
    sVlan->priority = tag->portNumber;
    sVlan->vlanId   = (uint16)((uint16)(tag->slotNumber & cBit3_0) | (uint16)((tag->encapType & cBit3_0) << 4));
    return sVlan;
    }

static tAtEthVlanTag *CVlanFromBdcomTag(const tAtBdcomTag *tag, tAtEthVlanTag *cVlan)
    {
    cVlan->cfi      = tag->channelNumber & cBit7_0;
    cVlan->priority = (tag->channelNumber >> 8) & cBit1_0;
    cVlan->vlanId   = tag->pktLen;

    return cVlan;
    }

static tAtBdcomTag *BdcomTagFromSVlan(const tAtEthVlanTag *sVlan, tAtBdcomTag *tag)
    {
    tag->cpuPktIndicator =  sVlan->cfi;
    tag->portNumber      =  sVlan->priority;
    tag->encapType       =  (sVlan->vlanId >> 4) & cBit3_0;
    tag->slotNumber      =  sVlan->vlanId & cBit3_0;

    return tag;
    }

static tAtBdcomTag *BdcomTagFromCVlan(const tAtEthVlanTag *cVlan, tAtBdcomTag *tag)
    {
    tag->channelNumber = (uint16)((uint16)(cVlan->cfi & cBit7_0) | (uint16)((cVlan->priority & cBit1_0) << 8));
    tag->pktLen  = (uint8)cVlan->vlanId;

    return tag;
    }

static void VlanDescriptionFromBdcomTag(const tAtBdcomTag *tag, uint8 portId, tAtEthVlanDesc *desc)
    {
    tAtEthVlanTag cVlan, sVlan;

    AtOsalMemInit(desc, 0x0, sizeof(tAtEthVlanDesc));
    AtEthFlowVlanDescConstruct(portId, SVlanFromBdcomTag(tag, &sVlan), CVlanFromBdcomTag(tag, &cVlan), desc);
    }

eAtRet AtBdcomEthFlowTxHeaderSet(AtEthFlow self, uint8 portId, const tAtBdcomTag *tag)
    {
    tAtEthVlanDesc desc;

    if ((self == NULL) || (tag == NULL))
        return cAtErrorNullPointer;

    VlanDescriptionFromBdcomTag(tag, portId, &desc);
    return AtEthFlowEgressVlanSet(self, &desc);
    }

eAtRet AtBdcomEthFlowExpectedHeaderSet(AtEthFlow self, uint8 portId, const tAtBdcomTag *tag)
    {
    tAtEthVlanDesc desc;
    tAtEthVlanTag cVlan, sVlan;
    tAtEthVlanDesc *descs;
    uint32 vlanNum;
    uint32 vlanIndex;

    if ((self == NULL) || (tag == NULL))
        return cAtErrorNullPointer;

    AtOsalMemInit(&desc, 0x0, sizeof(desc));

    /* Need to remove all VLANs before adding a new one */
    vlanNum = AtEthFlowIngressAllVlansGet(self, NULL);
    if (vlanNum != 0)
        {
        descs = AtOsalMemAlloc(vlanNum * sizeof(tAtEthVlanDesc));
        if (descs == NULL)
            return cAtErrorRsrcNoAvail;

        AtEthFlowIngressAllVlansGet(self, descs);
        for (vlanIndex = 0; vlanIndex < vlanNum; vlanIndex++)
            AtEthFlowIngressVlanRemove(self, &descs[vlanIndex]);

        AtOsalMemFree(descs);
        }

    AtEthFlowVlanDescConstruct(portId, SVlanFromBdcomTag(tag, &sVlan), CVlanFromBdcomTag(tag, &cVlan), &desc);
    return AtEthFlowIngressVlanAdd(self, &desc);
    }

eAtRet AtBdcomEthFlowTagGet(AtEthFlow self, tAtBdcomTag *tag)
    {
    tAtEthVlanDesc desc;

    if ((self == NULL) || (tag == NULL))
        return cAtErrorNullPointer;

    AtOsalMemInit(tag, 0, sizeof(tAtBdcomTag));
    if (AtEthFlowEgressVlanGet(self, &desc) != cAtOk)
        return cAtError;

    if (desc.numberOfVlans == 0)
        return cAtOk;

    BdcomTagFromCVlan(&desc.vlans[0], tag);
    BdcomTagFromSVlan(&desc.vlans[1], tag);
    return cAtOk;
    }

eAtRet AtBdcomEthFlowExpectedTagGet(AtEthFlow self, tAtBdcomTag *tag)
    {
    tAtEthVlanDesc desc;
    uint32 numTrafficDescr;

    if ((self == NULL) || (tag == NULL))
        return cAtErrorNullPointer;

    AtOsalMemInit(tag, 0, sizeof(tAtBdcomTag));
    numTrafficDescr = AtEthFlowIngressAllVlansGet(self, NULL);
    if (numTrafficDescr == 0)
        return cAtOk;

    if (numTrafficDescr != 1)
        return cAtError;

    AtEthFlowIngressAllVlansGet(self, &desc);
    BdcomTagFromCVlan(&desc.vlans[0], tag);
    BdcomTagFromSVlan(&desc.vlans[1], tag);
    return cAtOk;
    }

eAtRet AtBdcomEthFlowTxFullHeaderSet(AtEthFlow self, uint8 *egressSourceMac, uint8 *egressDestMac, uint8 portId, const tAtBdcomTag *tag)
    {
    tAtEthVlanDesc desc;

    if ((self == NULL) || (tag == NULL))
        return cAtErrorNullPointer;

    VlanDescriptionFromBdcomTag(tag, portId, &desc);
    return AtEthFlowEgressHeaderSet(self, egressSourceMac, egressDestMac, &desc);
    }

eAtRet AtBdcomTdmToPsnPppHeaderSet(AtPppLink self, uint8 entryId, uint32 pppHeader, uint8 headerLength)
    {
    return Tha60060011PppLinkPidTableTdmToPsnPppHeaderSet(self, entryId, pppHeader, headerLength);
    }

eAtRet AtBdcomTdmToPsnEthTypeSet(AtPppLink self, uint8 entryId, uint32 ethType)
    {
    return Tha60060011PppLinkPidTableTdmToPsnEthTypeSet(self, entryId, ethType);
    }

eAtRet AtBdcomPsnToTdmPppHeaderSet(AtPppLink self, uint8 entryId, uint32 pppHeader, uint8 headerLength)
    {
    return Tha60060011PppLinkPidTablePsnToTdmPppHeaderSet(self, entryId, pppHeader, headerLength);
    }

eAtRet AtBdcomPsnToTdmEthTypeSet(AtPppLink self, uint8 entryId, uint32 ethType)
    {
    return Tha60060011PppLinkPidTablePsnToTdmEthTypeSet(self, entryId, ethType);
    }

uint32 AtBdcomTdmToPsnPppHeaderGet(AtPppLink self, uint8 entryId, uint8 *headerLength)
    {
    return Tha60060011PppLinkPidTableTdmToPsnPppHeaderGet(self, entryId, headerLength);
    }

uint32 AtBdcomTdmToPsnEthTypeGet(AtPppLink self, uint8 entryId)
    {
    return Tha60060011PppLinkPidTableTdmToPsnEthTypeGet(self, entryId);
    }

uint32 AtBdcomPsnToTdmPppHeaderGet(AtPppLink self, uint8 entryId, uint8 *headerLength)
    {
    return Tha60060011PppLinkPidTablePsnToTdmPppHeaderGet(self, entryId, headerLength);
    }

uint32 AtBdcomPsnToTdmEthTypeGet(AtPppLink self, uint8 entryId)
    {
    return Tha60060011PppLinkPidTablePsnToTdmEthTypeGet(self, entryId);
    }

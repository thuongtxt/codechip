/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APP
 *
 * File        : AtZtePtnPdh.c
 *
 * Created Date: Mar 7, 2014
 *
 * Description : Implement specific APIs for ZTE PTN projects
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtZtePtn.h"
#include "../../driver/src/implement/default/pdh/ThaPdhDe1.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eAtRet AtZtePtnPdhDe1AutoTxAisEnable(AtPdhDe1 de1, eBool enable)
    {
    return AtPdhDe1AutoTxAisEnable(de1, enable);
    }

eBool AtZtePtnPdhDe1AutoTxAisIsEnabled(AtPdhDe1 de1)
    {
    return AtPdhDe1AutoTxAisIsEnabled(de1);
    }

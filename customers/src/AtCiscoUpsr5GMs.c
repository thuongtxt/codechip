/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : UPSR
 *
 * File        : AtCiscoUpsr5GMs.c
 *
 * Created Date: Dec 19, 2016
 *
 * Description : UPSR handler for 5G MS card
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCiscoUpsrInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static at_uint32 TxApsGroupRegister(at_uint32 apsGroupId)
    {
    return (at_uint32)(0x34000 + apsGroupId);
    }

static void HwTxApsGroupEnable(AtCiscoUpsr self, at_uint32 apsGroupId, at_uint32 enable)
    {
    at_uint32 regAddr = cHspwTxOffset + TxApsGroupRegister(apsGroupId);
    at_uint32 regVal = AtCiscoUpsrHwRead(self, regAddr);

    mRegFieldSet(regVal,
                cAf6_pla_out_upsr_ctrl_PlaOutUpsrEnCtr_Mask(0),
                cAf6_pla_out_upsr_ctrl_PlaOutUpsrEnCtr_Shift(0),
                enable);
    AtCiscoUpsrHwWrite(self, regAddr, regVal);
    }

static at_uint32 HwTxApsGroupIsEnabled(AtCiscoUpsr self, at_uint32 apsGroupId)
    {
    at_uint32 regAddr = cHspwTxOffset + TxApsGroupRegister(apsGroupId);
    at_uint32 regVal = AtCiscoUpsrHwRead(self, regAddr);

    return mRegField(regVal,
                     cAf6_pla_out_upsr_ctrl_PlaOutUpsrEnCtr_Mask(0),
                     cAf6_pla_out_upsr_ctrl_PlaOutUpsrEnCtr_Shift(0));
    }

static at_uint32 TxHsGroupLabelSelectRegister(AtCiscoUpsr self, at_uint32 hsGroupId)
    {
    AtUnused(self);
    return (at_uint32)(0x38000 + hsGroupId);
    }

static at_uint32 RxHsGroupPrimaryOffset(AtCiscoUpsr self, at_uint32 hsGroupId)
    {
    AtUnused(self);
    return (at_uint32)(hsGroupId + 0x800 + cHspwRxOffset);
    }

static at_uint32 TxHsGroupLabelSelectMask(AtCiscoUpsr self)
    {
    AtUnused(self);
    return cBit0;
    }

static at_uint32 TxHsGroupLabelSelectShift(AtCiscoUpsr self)
    {
    AtUnused(self);
    return 0;
    }

static void HsGroupTxLabelSetSelect(AtCiscoUpsr self, at_uint32 hsGroupId, eAtCiscoPwGroupLabelSet labelSet)
    {
    at_uint32 regVal = 0;
    at_uint32 hwLabelSet = (labelSet == cAtCiscoPwGroupLabelSetPrimary) ? 0 : 1;
    at_uint32 address = TxHsGroupLabelSelectRegister(self, hsGroupId);

    mRegFieldSet(regVal,
                 TxHsGroupLabelSelectMask(self),
                 TxHsGroupLabelSelectShift(self),
                 hwLabelSet);
    AtCiscoUpsrHwWrite(self, cHspwTxOffset + address, regVal);
    }

static eAtCiscoPwGroupLabelSet HsGroupTxSelectedLabelGet(AtCiscoUpsr self, at_uint32 hsGroupId)
    {
    at_uint32 address  = TxHsGroupLabelSelectRegister(self, hsGroupId);
    at_uint32 regVal   = AtCiscoUpsrHwRead(self, cHspwTxOffset + address);
    at_uint32 labelSet = mRegField(regVal,
                         TxHsGroupLabelSelectMask(self),
                         TxHsGroupLabelSelectShift(self));
    return (labelSet == 0) ? cAtCiscoPwGroupLabelSetPrimary : cAtCiscoPwGroupLabelSetBackup;
    }

static at_uint32 MaxHsGroupsGet(AtCiscoUpsr self)
    {
    AtUnused(self);
    return 2048;
    }

static at_uint32 MaxApsGroupsGet(AtCiscoUpsr self)
    {
    AtUnused(self);
    return 4096;
    }

static at_uint32 MaxStsGet(AtCiscoUpsr self)
    {
    AtUnused(self);
    return 96;
    }

static void OverrideAtCiscoUpsr(AtCiscoUpsr self)
    {
    mMethodOverride(self->methods, MaxApsGroupsGet);
    mMethodOverride(self->methods, HwTxApsGroupEnable);
    mMethodOverride(self->methods, HwTxApsGroupIsEnabled);
    mMethodOverride(self->methods, RxHsGroupPrimaryOffset);
    mMethodOverride(self->methods, HsGroupTxLabelSetSelect);
    mMethodOverride(self->methods, HsGroupTxSelectedLabelGet);
    mMethodOverride(self->methods, MaxHsGroupsGet);
    mMethodOverride(self->methods, MaxStsGet);
    }

static void Override(AtCiscoUpsr self)
    {
    OverrideAtCiscoUpsr(self);
    }

static at_uint32 ObjectSize(void)
    {
    return sizeof (tAtCiscoUpsr);
    }

AtCiscoUpsr AtCisco5GMsCardUpsrObjectInit(AtCiscoUpsr self, at_uint32 metaBaseAddress, at_uint32 productCode)
    {
    /* Clear memory */
    memset(self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtCiscoOcnCardUpsrObjectInit(self, metaBaseAddress, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);

    return self;
    }

AtCiscoUpsr AtCisco5GMsCardUpsrNew(at_uint32 metaBaseAddress, at_uint32 productCode)
    {
    AtCiscoUpsr newUpsr = malloc(ObjectSize());
    if (newUpsr == NULL)
        return NULL;

    return AtCisco5GMsCardUpsrObjectInit(newUpsr, metaBaseAddress, productCode);
    }

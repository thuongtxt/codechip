/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : AtCiscoOsalNone.c
 *
 * Created Date: Jul 10, 2016
 *
 * Description : OSAL for environment that does not have OS
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "AtCiscoOsalNone.h"
#include "AtDriver.h"
#include "mempool/mempool.h"

/*--------------------------- Define -----------------------------------------*/
#define cAtOsalMemPoolSize (100 * 1048576) /* 100 MB per device */
#define cMaxDevices 6
#define cBit63_0 0xFFFFFFFFFFFFFFFFULL

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtCiscoOsalNone*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtCiscoOsalNone
    {
    tAtOsal super;
    /* Private data */
    uint8* mempool;
    }tAtCiscoOsalNone;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static eBool m_methodsInit = 0;

/* Override */
static tAtOsalMethods m_AtOsalOverride;

/* This OSAL */
static tAtCiscoOsalNone m_osal;
static AtOsal           m_sharedOsal = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void* MemAlloc(AtOsal self, uint32 size)
    {
    AtUnused(self);
    return cisco_malloc(size);
    }

static void MemFree(AtOsal self, void* pMem)
    {
    AtUnused(self);
    if (pMem)
        cisco_free(pMem);
    }

static void* MemInit(AtOsal self, void* pMem, uint32 val, uint32 size)
    {
    AtUnused(self);
    return memset(pMem, (int)val, size);
    }

static void* MemCpy(AtOsal self, void *pDest, const void *pSrc, uint32 size)
    {
    AtUnused(self);
    return memcpy(pDest, pSrc, size);
    }

static int MemCmp(AtOsal self, const void *pMem1, const void *pMem2, uint32 size)
    {
    AtUnused(self);
    return memcmp(pMem1, pMem2, size);
    }

static AtOsalSem SemCreate(AtOsal self, eBool procEn, uint32 initVal)
    {
    AtUnused(self);
    AtUnused(procEn);
    AtUnused(initVal);
    return NULL;
    }

static AtOsalMutex MutexCreate(AtOsal self)
    {
    AtUnused(self);
    return NULL;
    }

static void Sleep(AtOsal self, uint32 timeToSleep)
    {
    AtUnused(self);
    wastetime(timeToSleep * 1000000);
    }

static void USleep(AtOsal self, uint32 timeToSleep)
    {
    AtUnused(self);
    wastetime(timeToSleep);
    }

static uint32 DifferenceTimeInMs(AtOsal self, const tAtOsalCurTime *currentTime, const tAtOsalCurTime *previousTime)
    {
    unsigned long long curTimeUs = ((unsigned long long)currentTime->sec << 32)  | (unsigned long long)(currentTime->usec);
    unsigned long long preTimeUs = ((unsigned long long)previousTime->sec << 32) | (unsigned long long)(previousTime->usec);
    unsigned long long diffTimeUs;

    if (curTimeUs < preTimeUs)
        {
        /*
         * By Cisco's formula to compute us from system stick counter, system clock is 600MHz
         * us64 = (unsigned long long)(time_temp * TB_FREQ_DENOMINATOR) / (unsigned long long)(system_clock(FALSE)/1000000);
         */
        unsigned long long maxUs = (cBit63_0 * 8) / 600; /* Accept this overflow to match with formula */
        diffTimeUs = (maxUs - preTimeUs) + curTimeUs;
        }
    else
        diffTimeUs = curTimeUs - preTimeUs;

    return (uint32)(diffTimeUs / 1000);
    }

static void Panic(AtOsal self)
    {
    AtUnused(self);
    }

static AtTaskManager TaskManagerGet(AtOsal self)
    {
    AtUnused(self);
    return NULL;
    }

static void CurTimeGet(AtOsal self, tAtOsalCurTime *currentTime)
    {
    unsigned long long us64 = current_time_usec_64bit();

    /* Save 32MSB to sec field and 32LSB to usec field.
     * Now, sec/usec fields do not mean second and microsecond anymore.
     */
    currentTime->sec  = (uint32)(us64 >> 32);
    currentTime->usec = (uint32)(us64);
    }

static const char* DateTimeGet(AtOsal self)
    {
    /* Will implement if necessary */
    AtUnused(self);
    return "[NA]";
    }

static const char* DateTimeInUsGet(AtOsal self)
    {
    /* Will implement if necessary */
    AtUnused(self);
    return "[NA]";
    }

static AtStd Std(AtOsal self)
    {
    AtUnused(self);
    return AtStdTinySharedStd();
    }

static eBool IsMultitasking(AtOsal self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideAtOsal(AtOsal self)
    {
    if (!m_methodsInit)
        {
        /* Memory operations */
        m_AtOsalOverride.MemAlloc = MemAlloc;
        m_AtOsalOverride.MemFree  = MemFree;
        m_AtOsalOverride.MemInit  = MemInit;
        m_AtOsalOverride.MemCpy   = MemCpy;
        m_AtOsalOverride.MemCmp   = MemCmp;

        /* Semaphore */
        m_AtOsalOverride.SemCreate = SemCreate;

        /* Mutex */
        m_AtOsalOverride.MutexCreate = MutexCreate;

        /* Delay functions */
        m_AtOsalOverride.Sleep              = Sleep;
        m_AtOsalOverride.USleep             = USleep;
        m_AtOsalOverride.CurTimeGet         = CurTimeGet;
        m_AtOsalOverride.DifferenceTimeInMs = DifferenceTimeInMs;

        /* Utils */
        m_AtOsalOverride.Panic = Panic;

        /* Managers */
        m_AtOsalOverride.TaskManagerGet = TaskManagerGet;
        m_AtOsalOverride.IsMultitasking = IsMultitasking;

        /* Time */
        m_AtOsalOverride.DateTimeGet = DateTimeGet;
        m_AtOsalOverride.DateTimeInUsGet = DateTimeInUsGet;

        m_AtOsalOverride.Std = Std;
        }

    self->methods = &m_AtOsalOverride;
    }

AtOsal AtCiscoOsalNoneObjectInit(AtOsal self)
    {
    uint32 memPoolSize;

    OverrideAtOsal(self);
    m_methodsInit = 1;

    memPoolSize = cMaxDevices * cAtOsalMemPoolSize;

    /* Create Memory pool for SDK: 50MB */
    mThis(self)->mempool = malloc(memPoolSize);
    cisco_init_for_malloc_use((void*)(mThis(self)->mempool), memPoolSize);
    return self;
    }

void AtCiscoOsalArriveMempoolCleanUp(AtOsal self)
    {
    free(mThis(self)->mempool);
    }
    
/**
 * Get Linux implementation of OSAL
 *
 * @return Linux OSAL
 */
AtOsal AtCiscoOsalNone(void)
    {
    if (m_sharedOsal)
        return m_sharedOsal;

    m_sharedOsal = AtCiscoOsalNoneObjectInit((AtOsal)&m_osal);
    return m_sharedOsal;
    }


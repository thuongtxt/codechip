/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : LOOP
 *
 * File        : AtLoop.c
 *
 * Created Date: Mar 14, 2015
 *
 * Description : LOOP implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCommon.h"
#include "AtLoop.h"
#include "../../driver/src/implement/codechip/Tha60070041/man/Tha60070041Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/**
 * Enable reboot system mode of FPGA when reaching to red alarm
 *
 * @param self This device
 * @param enable Enable/Disable
 * @return AT return code
 *
 */
eAtRet AtLoopTemperatureRebootWhenRedAlarmEnable(AtDevice self, eBool enable)
    {
    return Tha60070041DeviceTemperatureRebootEnableWhenRedAlarmSet(self, enable);
    }

/**
 * Get reboot system mode of FPGA when reaching to red alarm
 *
 * @param self This device
 * @return eBool return code
 *
 */
eBool AtLoopTemperatureRebootWhenRedAlarmIsEnabled(AtDevice self)
    {
    return Tha60070041DeviceTemperatureRebootEnableWhenRedAlarmGet(self);
    }

/**
 * Set SET threshold of FPGA temperature red alarm
 *
 * @param self This device
 * @param celsiusValue SET threshold value (oC)
 * @return AT return code
 *
 */
eAtRet AtLoopTemperatureRedAlarmSetThresholdSet(AtDevice self, uint32 celsiusValue)
    {
    return Tha60070041DeviceTemperatureRedAlarmSetThresholdSet(self, celsiusValue);
    }

/**
 * Get SET threshold of FPGA temperature red alarm
 *
 * @param self This device
 * @return SET threshold value (oC)
 *
 */
uint32 AtLoopTemperatureRedAlarmSetThresholdGet(AtDevice self)
    {
    return Tha60070041DeviceTemperatureRedAlarmSetThresholdGet(self);
    }

/**
 * Set CLEAR threshold of FPGA temperature red alarm
 *
 * @param self This device
 * @param celsiusValue CLEAR threshold value (oC)
 * @return AT return code
 *
 */
eAtRet AtLoopTemperatureRedAlarmClearThresholdSet(AtDevice self, uint32 celsiusValue)
    {
    return Tha60070041DeviceTemperatureRedAlarmClearThresholdSet(self, celsiusValue);
    }

/**
 * Get CLEAR threshold of FPGA temperature red alarm
 *
 * @param self This device
 * @return CLEAR threshold value (oC)
 *
 */
uint32 AtLoopTemperatureRedAlarmClearThresholdGet(AtDevice self)
    {
    return Tha60070041DeviceTemperatureRedAlarmClearThresholdGet(self);
    }

/**
 * Set SET threshold of FPGA temperature yellow alarm
 *
 * @param self This device
 * @param celsiusValue SET threshold value (oC)
 * @return AT return code
 *
 */
eAtRet AtLoopTemperatureYellowAlarmSetThresholdSet(AtDevice self, uint32 celsiusValue)
    {
    return Tha60070041DeviceTemperatureYellowAlarmSetThresholdSet(self, celsiusValue);
    }

/**
 * Get SET threshold of FPGA temperature yellow alarm
 *
 * @param self This device
 * @return SET threshold value (oC)
 *
 */
uint32 AtLoopTemperatureYellowAlarmSetThresholdGet(AtDevice self)
    {
    return Tha60070041DeviceTemperatureYellowAlarmSetThresholdGet(self);
    }

/**
 * Set CLEAR threshold of FPGA temperature yellow alarm
 *
 * @param self This device
 * @param celsiusValue CLEAR threshold value (oC)
 * @return AT return code
 *
 */
eAtRet AtLoopTemperatureYellowAlarmClearThresholdSet(AtDevice self, uint32 celsiusValue)
    {
    return Tha60070041DeviceTemperatureYellowAlarmClearThresholdSet(self, celsiusValue);
    }

/**
 * Get CLEAR threshold of FPGA temperature yellow alarm
 *
 * @param self This device
 * @return CLEAR threshold value (oC)
 *
 */
uint32 AtLoopTemperatureYellowAlarmClearThresholdGet(AtDevice self)
    {
    return Tha60070041DeviceTemperatureYellowAlarmClearThresholdGet(self);
    }

/**
 * Get FPGA temperature of device
 *
 * @param self This device
 * @return temperature value (oC)
 *
 */
uint32 AtLoopTemperatureGet(AtDevice self)
    {
    return Tha60070041DeviceTemperatureGet(self);
    }

/**
 * Get FPGA temperature alarm of device
 *
 * @param self This device
 * @return temperature alarm mask
 *
 */
uint32 AtLoopTemperatureAlarmGet(AtDevice self)
    {
    uint32 hwMask = Tha60070041DeviceTemperatureAlarmGet(self);
    const uint32 cTemperatureAlarmRed    = cBit3;
    const uint32 cTemperatureAlarmYellow = cBit0;

    if (hwMask & cTemperatureAlarmRed)
        return cAtLoopTemperatureAlarmRed;

    if (hwMask & cTemperatureAlarmYellow)
        return cAtLoopTemperatureAlarmYellow;

    return cAtLoopTemperatureAlarmNormal;
    }

/**
 * Set LED state for device
 *
 * @param self This device
 * @param ledType @ref eAtLoopLedType "LED type"
 * @param ledState @ref eAtLedState "LED states"
 * @return AT return code
 */
eAtRet AtLoopDeviceStatusLedStateSet(AtDevice self, eAtLoopLedType ledType, eAtLedState ledState)
    {
    if (ledType == cAtLoopLedTypeGreen)
        return Tha60070061DeviceGreenLedStateSet(self, ledState);

    if (ledType == cAtLoopLedTypeRed)
        return Tha60070061DeviceRedLedStateSet(self, ledState);

    return cAtErrorInvlParm;
    }

/**
 * Get LED state for device
 *
 * @param self This device
 * @param ledType @ref eAtLoopLedType "LED type"
 * @return @ref eAtLedState "LED state"
 */
eAtLedState AtLoopDeviceStatusLedStateGet(AtDevice self, eAtLoopLedType ledType)
    {
    if (ledType == cAtLoopLedTypeGreen)
        return Tha60070061DeviceGreenLedStateGet(self);

    if (ledType == cAtLoopLedTypeRed)
        return Tha60070061DeviceRedLedStateGet(self);

    return cAtLedStateOff;
    }

/**
 * Set LED activation state for device
 *
 * @param self This device
 * @param ledType @ref eAtLoopLedType "LED type"
 * @param ledState @ref eAtLedState "LED states"
 * @return AT return code
 */
eAtRet AtLoopDeviceActiveLedStateSet(AtDevice self, eAtLoopLedType ledType, eAtLedState ledState)
    {
    if (ledType == cAtLoopLedTypeGreen)
        return Tha60070061DeviceGreenLedActivationSet(self, ledState);

    if (ledType == cAtLoopLedTypeRed)
        return Tha60070061DeviceRedLedActivationSet(self, ledState);

    return cAtErrorInvlParm;
    }

/**
 * Get LED activation state for device
 *
 * @param self This device
 * @param ledType @ref eAtLoopLedType "LED type"
 * @return @ref eAtLedState "LED state"
 */
eAtLedState AtLoopDeviceActiveLedStateGet(AtDevice self, eAtLoopLedType ledType)
    {
    if (ledType == cAtLoopLedTypeGreen)
        return Tha60070061DeviceGreenLedActivationGet(self);

    if (ledType == cAtLoopLedTypeRed)
        return Tha60070061DeviceRedLedActivationGet(self);

    return cAtLedStateOff;
    }

/**
 * Set LED state for device
 *
 * @param self SDH line
 * @param ledType @ref eAtLoopLedType "LED type"
 * @param ledState @ref eAtLedState "LED states"
 * @return AT return code
 */
eAtRet AtLoopSdhLineLedStateSet(AtSdhLine self, eAtLoopLedType ledType, eAtLedState ledState)
    {
    if (ledType == cAtLoopLedTypeGreen)
        return Tha60070061SdhLineGreenLedStateSet(self, ledState);

    if (ledType == cAtLoopLedTypeRed)
        return Tha60070061SdhLineRedLedStateSet(self, ledState);

    return cAtErrorInvlParm;

    }

/**
 * Get LED state for device
 *
 * @param self SDH line
 * @param ledType @ref eAtLoopLedType "LED type"
 * @return @ref eAtLedState "LED state"
 */
eAtLedState AtLoopSdhLineLedStateGet(AtSdhLine self, eAtLoopLedType ledType)
    {
    if (ledType == cAtLoopLedTypeGreen)
        return Tha60070061SdhLineGreenLedStateGet(self);

    if (ledType == cAtLoopLedTypeRed)
        return Tha60070061SdhLineRedLedStateGet(self);

    return cAtLedStateOff;
    }

/**
 * Get GFP is existed
 *
 * @param self SDH line
 * @return @ref eBool "SFP is existed"
 */
eBool AtLoopSdhLineSfpExisted(AtSdhLine self)
    {
    return Tha60070061SdhLineSfpIsExisted(self);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ALU
 *
 * File        : AtAlu.c
 *
 * Created Date: Aug 27, 2014
 *
 * Description : Implement ALU specific APIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtAlu.h"
#include "AtXauiMdio.h"
#include "AtDevice.h"
#include "AtChannel.h"
#include "commacro.h"
#include "../../driver/src/implement/codechip/Tha60150011/man/Tha60150011Device.h"
#include "../../driver/src/implement/codechip/Tha60150011/pw/Tha60150011ModulePw.h"
#include "../../driver/src/generic/physical/AtSemControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
/* FPGA sticky */
#define cFpgaSticky                                         0x00F00050
#define cThaFpgaStickyQDRNo2ParityErrorMask                 cBit14
#define cThaFpgaStickyQDRNo2ParityErrorShift                14

#define cThaFpgaStickyQDRNo1ParityErrorMask                 cBit12
#define cThaFpgaStickyQDRNo1ParityErrorShift                12

#define cFpgaStickyPllStatusMask                            cBit5_0
#define cFpgaStickyPllStatusShift                           0

/* FPGA sticky 1 */
#define cFpgaSticky1                                        0x00500009
#define cFpgaSticky1TxPktDataValidCmdFifoFullMask           cBit31
#define cFpgaSticky1TxPktDataValidCmdFifoFullShift          31

#define cFpgaSticky1TxPktInfoValidCmdFifoFullMask           cBit30
#define cFpgaSticky1TxPktInfoValidCmdFifoFullShift          30

#define cFpgaSticky1TxPktInfoMemoryBusErrorMask             cBit29_24
#define cFpgaSticky1TxPktInfoMemoryBusErrorShift            24

#define cFpgaSticky1RxPktInfoEccErrorMask                   cBit23
#define cFpgaSticky1RxPktInfoEccErrorShift                  23

#define cFpgaSticky1RxPktInfoValidCmdFifoFullMask           cBit22
#define cFpgaSticky1RxPktInfoValidCmdFifoFullShift          22

#define cFpgaSticky1RxPktInfoMemoryBusErrorMask             cBit21_16
#define cFpgaSticky1RxPktInfoMemoryBusErrorShift            16

#define cFpgaSticky1RxPktDataReorderCmdErrorMask            cBit15
#define cFpgaSticky1RxPktDataReorderCmdErrorShift           15

#define cFpgaSticky1RxPktDataValidCmdFifoFullMask           cBit14
#define cFpgaSticky1RxPktDataValidCmdFifoFullShift          14

#define cFpgaSticky1RxPktDataMemoryBusErrorMask             cBit13_8
#define cFpgaSticky1RxPktDataMemoryBusErrorShift            8

#define cFpgaSticky1RxPktDataDdrCrcErrorMask                cBit7
#define cFpgaSticky1RxPktDataDdrCrcErrorShift               7

#define cFpgaSticky1TxPweWrCacheDuplicationMask             cBit6
#define cFpgaSticky1TxPweWrCacheDuplicationShift            6

#define cFpgaSticky1TxPweWrCacheEmptyMask                   cBit5
#define cFpgaSticky1TxPweWrCacheEmptyShift                  5

#define cFpgaSticky1TxPweDataBlockDuplicationMask           cBit4
#define cFpgaSticky1TxPweDataBlockDuplicationShift          4

#define cFpgaSticky1TxPweDataFifoEmptyMask                  cBit3
#define cFpgaSticky1TxPweDataFifoEmptyShift                 3

#define cFpgaSticky1TxPweWrPacketFifoFullMask               cBit2
#define cFpgaSticky1TxPweWrPacketFifoFullShift               2

#define cFpgaSticky1TxPweRdPacketFifoFullMask               cBit1
#define cFpgaSticky1TxPweRdPacketFifoFullShift              1

#define cFpgaSticky1TxPweRdCacheDuplicationMask             cBit0
#define cFpgaSticky1TxPweRdCacheDuplicationShift            0

/* FPGA sticky 2 */
#define cFpgaSticky2                                        0x0050000A
#define cFpgaSticky2RxPweWrPktOversizeMask                  cBit31
#define cFpgaSticky2RxPweWrPktOversizeShift                 31

#define cFpgaSticky2RxPweWrDataBufferPauseMask              cBit30
#define cFpgaSticky2RxPweWrDataBufferPauseShift             30

#define cFpgaSticky2RxPweWrCacheFifoFullMask                cBit29
#define cFpgaSticky2RxPweWrCacheFifoFullShift               29

#define cFpgaSticky2RxPweWrCacheDisableMask                 cBit19
#define cFpgaSticky2RxPweWrCacheDisableShift                19

#define cFpgaSticky2RxPweDataBufferDisableMask              cBit18
#define cFpgaSticky2RxPweDataBufferDisableShift             18

#define cFpgaSticky2RxPweRdCacheLostMask                    cBit17
#define cFpgaSticky2RxPweRdCacheLostShift                   17

#define cFpgaSticky2RxPweTdmValidSegmentFifoFullMask        cBit16
#define cFpgaSticky2RxPweTdmValidSegmentFifoFullShift       16

#define cFpgaSticky2RxPweRdSegmentFifoFullMask              cBit15
#define cFpgaSticky2RxPweRdSegmentFifoFullShift             15

#define cFpgaSticky2RxPweLinkListValidCmdFifoFullMask       cBit12
#define cFpgaSticky2RxPweLinkListValidCmdFifoFullShift      12

#define cFpgaSticky2RxPweLinkListRequestCmdFifoFullMask     cBit11
#define cFpgaSticky2RxPweLinkListRequestCmdFifoFullShift    11

#define cFpgaSticky2RxPweLostStartOfPktMask                 cBit10
#define cFpgaSticky2RxPweLostStartOfPktShift                10

#define cFpgaSticky2RxPweLostEndOfPktMask                   cBit9
#define cFpgaSticky2RxPweLostEndOfPktShift                  9

#define cFpgaSticky2RxPweRdCacheErrorMask                   cBit8
#define cFpgaSticky2RxPweRdCacheErrorShift                  8

#define cFpgaSticky2RxPweWrCacheEmptyMask                   cBit7
#define cFpgaSticky2RxPweWrCacheEmptyShift                  7

#define cFpgaSticky2RxPweWrCacheDuplicationMask             cBit6
#define cFpgaSticky2RxPweWrCacheDuplicationShift            6

#define cFpgaSticky2RxPweRdCacheEmptyMask                   cBit5
#define cFpgaSticky2RxPweRdCacheEmptyShift                  5

#define cFpgaSticky2RxPweRdCacheDuplicationMask             cBit4
#define cFpgaSticky2RxPweRdCacheDuplicationShift            4

#define cFpgaSticky2RxPweOverrunMask                        cBit3
#define cFpgaSticky2RxPweOverrunShift                       3

#define cFpgaSticky2RxPweUnderrunMask                       cBit2
#define cFpgaSticky2RxPweUnderrunShift                      2

#define cFpgaSticky2RxPweDataBufferEmptyMask                cBit1
#define cFpgaSticky2RxPweDataBufferEmptyShift               1

#define cFpgaSticky2RxPweDataBlockDuplicationMask           cBit0
#define cFpgaSticky2RxPweDataBlockDuplicationShift          0

/* FPGA clock */
#define cFPGAXGMIIClock156_25MhzValueStatus           0xF00061
#define cFPGAOCNTxClock155_52MhzValueStatus           0xF00062
#define cFPGAOCNRxClock155_52MhzValueStatus           0xF00063
#define cFPGACore1Clock100MhzValueStatus              0xF00064
#define cFPGACore2Clock100MhzValueStatus              0xF00065
#define cFPGADDR3UserClock100MhzValueStatus           0xF00066
#define cFPGAPCIEUserClock62_5MhzValueStatus          0xF00067
#define cFPGAQDRNo1FeedBackClock155_52MhzValueStatus  0xF00068
#define cFPGAQDRNo2FeedBackClock155_52MhzValueStatus  0xF00069

#define cFpgaGeFifoState    0x3c1048
#define cFpgaGeFifoFullMask cBit9

#define cFpgaGeFifoLevel      0x3c104B
#define cFpgaGeFifoLevelMask  cBit15_0
#define cFpgaGeFifoLevelShift 0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtAluFpgaFifoSticky RxGeFifoFullStickyGet(AtDevice device)
    {
    AtHal hal;
    uint32 regVal;

    if (device == NULL)
        return 0;

    hal = AtDeviceIpCoreHalGet(device, 0);
    regVal = AtHalRead(hal, cFpgaGeFifoState);
    if (regVal & cFpgaGeFifoFullMask)
        return cAtAluFpgaFifoStickyRxGeFifoFull;

    return 0;
    }

static void RxGeFifoFullStickyClear(AtDevice device)
    {
    AtHal hal;
    uint32 regVal;

    if (device == NULL)
        return;

    hal = AtDeviceIpCoreHalGet(device, 0);
    regVal = AtHalRead(hal, cFpgaGeFifoState);
    regVal |= cFpgaGeFifoFullMask;
    AtHalWrite(hal, cFpgaGeFifoState, regVal);
    }

uint32 AtAluSemControllerMaxElapseTimeToInsertError(AtDevice self)
    {
    return AtSemControllerMaxElapseTimeToInsertError(AtDeviceSemControllerGet(self));
    }

uint32 AtAluSemControllerMaxElapseTimeToChangeFsm(AtDevice self)
    {
    return AtSemControllerMaxElapseTimeToMoveFsm(AtDeviceSemControllerGet(self));
    }

/**
 * @addtogroup AtCustomer
 * @{
 */

/**
 * Get Near-End performance monitoring status
 *
 * @param self This PW
 * @return Performance monitoring status in format
 *        - bit[13:0] Near-End Errored blocks
 *        - bit[14]   Suspect second
 *        - bit[15]   Near-End Defect second
 */
uint32 AtAluPwNEPerformanceMornitoringGet(AtPw self)
    {
    return Tha60150011PwNEPerformanceMornitoringGet(self);
    }

/**
 * Get Far-End performance monitoring status
 *
 * @param self This PW
 *
 * @return Performance monitoring status in format
 *        - bit[13:0] Far-End Errored blocks
 *        - bit[14]   Suspect second
 *        - bit[15]   Far-End Defect second
 */
uint32 AtAluPwFEPerformanceMornitoringGet(AtPw self)
    {
    return Tha60150011PwFEPerformanceMornitoringGet(self);
    }

/**
 * Reset XAUI
 *
 * @param hal Hal for read/write
 * @param reset
 *         - cAtTrue Start reset
 *         - cAtFalse Stop reset
 *
 * @return At return code
 */
eAtRet AtAluXauiReset(AtHal hal, eBool reset)
    {
    return AtXauiReset(hal, reset);
    }

/**
 * Get XAUI status
 *
 * @param hal Hal for read/write
 * @param clear Reading mode
 *         - cAtTrue Read then clear
 *         - cAtFalse Read only
 *
 * @return XAUI status, return 0 when everything is good. If return value is larger than 0
 *         - bit 0 : Tx not ready
 *         - bit 1 : Tx local fault
 *         - bit 2 : Rx local fault
 *         - bit 3 : Rx not SYN
 *         - bit 4 : RX not align
 *         - bit 5 : Link down
 */
uint32 AtAluXauiStatusRead2Clear(AtHal hal, eBool clear)
    {
    return AtXauiStatusRead2Clear(hal, clear);
    }

/**
 * Test QDR
 *
 * @param device Device
 * @param qdrId QDR ID [0..1]
 *
 * @return At return code
 */
eAtRet AtAluQdrTest(AtDevice device, uint8 qdrId)
    {
    return Tha60150011DeviceQdrTest(device, qdrId);
    }

/**
 * Detect DDR3 all margins
 *
 * @param device Device
 *
 * @return At return code
 */
eAtRet AtAluDdrMarginDetect(AtDevice device)
    {
    return Tha60150011DeviceDdrMarginDetect(device);
    }

/**
 * Get DDR3 read left margin
 *
 * @param device Device
 *
 * @return Read left margin in picosecond unit
 */
uint32 AtAluDdrReadLeftMargin(AtDevice device)
    {
    return Tha60150011DeviceDdrReadLeftMargin(device);
    }

/**
 * Get DDR3 read right margin
 *
 * @param device Device
 *
 * @return Read right margin in picosecond unit
 */
uint32 AtAluDdrReadRightMargin(AtDevice device)
    {
    return Tha60150011DeviceDdrReadRightMargin(device);
    }

/**
 * Get DDR3 write left margin
 *
 * @param device Device
 *
 * @return Write left margin in picosecond unit
 */
uint32 AtAluDdrWriteLeftMargin(AtDevice device)
    {
    return Tha60150011DeviceDdrWriteLeftMargin(device);
    }

/**
 * Get DDR3 write right margin
 *
 * @param device Device
 *
 * @return Write right margin in picosecond unit
 */
uint32 AtAluDdrWriteRightMargin(AtDevice device)
    {
    return Tha60150011DeviceDdrWriteRightMargin(device);
    }

/**
 * Check status of DDR3 calib
 *
 * @param device Device
 *
 * @return cAtTrue  calib status is good
 *         cAtFalse calib status is bad
 */
eBool AtAluDdrCalibStatusIsGood(AtDevice device)
    {
    return Tha60150011DeviceDdrCalibIsGood(device);
    }

/**
 * Detect QDR left/right margin
 *
 * @param device Device
 * @param qdrId QDR ID [0..1]

 * @return At return code
 */
eAtRet AtAluQdrMarginDetect(AtDevice device, uint8 qdrId)
    {
    return Tha60150011DeviceQdrMarginDetect(device, qdrId);
    }

/**
 * Get QDR read left margin
 *
 * @param device Device
 * @param qdrId QDR ID
 *
 * @return Read left margin in picosecond unit
 */
uint32 AtAluQdrReadLeftMargin(AtDevice device, uint8 qdrId)
    {
    return Tha60150011DeviceQdrReadLeftMargin(device, qdrId);
    }

/**
 * Get QDR read right margin
 *
 * @param device Device
 * @param qdrId QDR ID
 *
 * @return Read right margin in picosecond unit
 */
uint32 AtAluQdrReadRightMargin(AtDevice device, uint8 qdrId)
    {
    return Tha60150011DeviceQdrReadRightMargin(device, qdrId);
    }

/**
 * Get QDR write left margin
 *
 * @param device Device
 * @param qdrId QDR ID
 *
 * @return Write left margin in picosecond unit
 */
uint32 AtAluQdrWriteLeftMargin(AtDevice device, uint8 qdrId)
    {
    return Tha60150011DeviceQdrWriteLeftMargin(device, qdrId);
    }

/**
 * Get QDR write right margin
 *
 * @param device Device
 * @param qdrId QDR ID [0..1]
 *
 * @return Write right margin in picosecond unit
 */
uint32 AtAluQdrWriteRightMargin(AtDevice device, uint8 qdrId)
    {
    return Tha60150011DeviceQdrWriteRightMargin(device, qdrId);
    }

/**
 * Get PLL alarm interrupt event
 *
 * @param device Device

 * @return Alarm interrupt events. Refer:
 *         - @ref eAtAluFpgaPllSticky
 */
uint32 AtAluPllAlarmStickyGet(AtDevice device)
    {
    AtHal hal;

    if (device == NULL)
        return 0;

    hal = AtDeviceIpCoreHalGet(device, 0);
    return AtHalRead(hal, cFpgaSticky) & cFpgaStickyPllStatusMask;
    }

/**
 * Clear PLL alarm interrupt events
 *
 * @param device Device

 * @return Alarm interrupt events. Refer:
 *         - @ref eAtAluFpgaPllSticky
 */
uint32 AtAluPllAlarmStickyClear(AtDevice device)
    {
    AtHal hal;
    uint32 interrupts;

    if (device == NULL)
        return 0;

    interrupts =  AtAluPllAlarmStickyGet(device);

    /* Clear sticky */
    hal = AtDeviceIpCoreHalGet(device, 0);
    AtHalWrite(hal, cFpgaSticky, cFpgaStickyPllStatusMask);

    return interrupts;
    }

/**
 * Get Fifo alarm interrupt events
 *
 * @param device Device

 * @return Alarm interrupt events. Refer:
 *         - @ref eAtAluFpgaFifoSticky
 */
uint32 AtAluFifoAlarmStickyGet(AtDevice device)
    {
    uint32 interrupts = 0;
    AtHal hal;
    uint32 regVal;

    if (device == NULL)
        return 0;

    hal = AtDeviceIpCoreHalGet(device, 0);
    regVal = AtHalRead(hal, cFpgaSticky1);

    if (regVal & cFpgaSticky1TxPktDataValidCmdFifoFullMask)
        interrupts |= cAtAluFpgaFifoStickyTxPktDataValidCmdFifoFull;

    if (regVal & cFpgaSticky1TxPktInfoValidCmdFifoFullMask)
        interrupts |= cAtAluFpgaFifoStickyTxPktInfoValidCmdFifoFull;

    if (regVal & cFpgaSticky1RxPktInfoValidCmdFifoFullMask)
        interrupts |= cAtAluFpgaFifoStickyRxPktInfoValidCmdFifoFull;

    if (regVal & cFpgaSticky1RxPktDataValidCmdFifoFullMask)
        interrupts |= cAtAluFpgaFifoStickyRxPktDataValidCmdFifoFull;

    if (regVal & cFpgaSticky1TxPweDataFifoEmptyMask)
        interrupts |= cAtAluFpgaFifoStickyTxPweDataFifoEmpty;

    if (regVal & cFpgaSticky1TxPweWrPacketFifoFullMask)
        interrupts |= cAtAluFpgaFifoStickyTxPweWrPacketFifoFull;

    if (regVal & cFpgaSticky1TxPweRdPacketFifoFullMask)
        interrupts |= cAtAluFpgaFifoStickyTxPweRdPacketFifoFull;

    regVal = AtHalRead(hal, cFpgaSticky2);
    if (regVal & cFpgaSticky2RxPweWrCacheFifoFullMask)
        interrupts |= cAtAluFpgaFifoStickyRxPweWrCacheFifoFull;

    if (regVal & cFpgaSticky2RxPweTdmValidSegmentFifoFullMask)
        interrupts |= cAtAluFpgaFifoStickyRxPweTdmValidSegmentFifoFull;

    if (regVal & cFpgaSticky2RxPweRdSegmentFifoFullMask)
        interrupts |= cAtAluFpgaFifoStickyRxPweRdSegmentFifoFull;

    if (regVal & cFpgaSticky2RxPweLinkListValidCmdFifoFullMask)
        interrupts |= cAtAluFpgaFifoStickyRxPweLinkListValidCmdFifoFull;

    if (regVal & cFpgaSticky2RxPweLinkListRequestCmdFifoFullMask)
        interrupts |= cAtAluFpgaFifoStickyRxPweLinkListRequestCmdFifoFull;

    if (regVal & cFpgaSticky2RxPweOverrunMask)
        interrupts |= cAtAluFpgaFifoStickyRxPweTdmFifoFull;

    if (regVal & cFpgaSticky2RxPweUnderrunMask)
        interrupts |= cAtAluFpgaFifoStickyRxPweTdmFifoEmpty;

    interrupts |= RxGeFifoFullStickyGet(device);

    return interrupts;
    }

/**
 * Clear Fifo alarm interrupt events
 *
 * @param device Device

 * @return Alarm interrupt events. Refer:
 *         - @ref eAtAluFpgaFifoSticky
 */
uint32 AtAluFifoAlarmStickyClear(AtDevice device)
    {
    uint32 interrupts;
    AtHal hal;
    uint32 mask;

    if (device == NULL)
        return 0;

    interrupts = AtAluFifoAlarmStickyGet(device);
    hal = AtDeviceIpCoreHalGet(device, 0);

    /* Clear sticky */
    mask = cFpgaSticky1TxPktDataValidCmdFifoFullMask       |
           cFpgaSticky1TxPktInfoValidCmdFifoFullMask       |
           cFpgaSticky1RxPktInfoValidCmdFifoFullMask       |
           cFpgaSticky1RxPktDataValidCmdFifoFullMask       |
           cFpgaSticky1TxPweDataFifoEmptyMask              |
           cFpgaSticky1TxPweWrPacketFifoFullMask           |
           cFpgaSticky1TxPweRdPacketFifoFullMask;
    AtHalWrite(hal, cFpgaSticky1, mask);

    mask =  cFpgaSticky2RxPweWrCacheFifoFullMask            |
            cFpgaSticky2RxPweTdmValidSegmentFifoFullMask    |
            cFpgaSticky2RxPweRdSegmentFifoFullMask          |
            cFpgaSticky2RxPweLinkListValidCmdFifoFullMask   |
            cFpgaSticky2RxPweLinkListRequestCmdFifoFullMask |
            cFpgaSticky2RxPweOverrunMask                    |
            cFpgaSticky2RxPweUnderrunMask;
    AtHalWrite(hal, cFpgaSticky2, mask);

    RxGeFifoFullStickyClear(device);

    return interrupts;
    }

/**
 * Get general alarm interrupt events
 *
 * @param device Device

 * @return Alarm interrupt events. Refer:
 *         - @ref eAtAluFpgaGeneralSticky
 */
uint32 AtAluGeneralAlarmStickyGet(AtDevice device)
    {
    uint32 interrupts = 0;
    AtHal hal;
    uint32 regVal;

    if (device == NULL)
        return 0;

    hal = AtDeviceIpCoreHalGet(device, 0);
    regVal = AtHalRead(hal, cFpgaSticky1);

    if (regVal & cFpgaSticky1TxPktInfoMemoryBusErrorMask)
        interrupts |= cAtAluFpgaGeneralStickyTxPktInfoMemoryBusError;

    if (regVal & cFpgaSticky1RxPktInfoEccErrorMask)
        interrupts |= cAtAluFpgaGeneralStickyRxPktInfoEccError;

    if (regVal & cFpgaSticky1RxPktInfoMemoryBusErrorMask)
        interrupts |= cAtAluFpgaGeneralStickyRxPktInfoMemoryBusError;

    if (regVal & cFpgaSticky1RxPktDataReorderCmdErrorMask)
        interrupts |= cAtAluFpgaGeneralStickyRxPktDataReorderCmdError;

    if (regVal & cFpgaSticky1RxPktDataMemoryBusErrorMask)
        interrupts |= cAtAluFpgaGeneralStickyRxPktDataMemoryBusError;

    if (regVal & cFpgaSticky1RxPktDataDdrCrcErrorMask)
        interrupts |= cAtAluFpgaGeneralStickyRxPktDataDdrCrcError;

    if (regVal & cFpgaSticky1TxPweWrCacheDuplicationMask)
        interrupts |= cAtAluFpgaGeneralStickyTxPweWrCacheDuplication;

    if (regVal & cFpgaSticky1TxPweWrCacheEmptyMask)
        interrupts |= cAtAluFpgaGeneralStickyTxPweWrCacheEmpty;

    if (regVal & cFpgaSticky1TxPweDataBlockDuplicationMask)
        interrupts |= cAtAluFpgaGeneralStickyTxPweDataBlockDuplication;

    if (regVal & cFpgaSticky1TxPweRdCacheDuplicationMask)
        interrupts |= cAtAluFpgaGeneralStickyTxPweRdCacheDuplication;

    regVal = AtHalRead(hal, cFpgaSticky2);
    if (regVal & cFpgaSticky2RxPweWrPktOversizeMask)
        interrupts |= cAtAluFpgaGeneralStickyRxPweWrPktOversize;

    if (regVal & cFpgaSticky2RxPweWrDataBufferPauseMask)
        interrupts |= cAtAluFpgaGeneralStickyRxPweWrDataBufferPause;

    if (regVal & cFpgaSticky2RxPweWrCacheDisableMask)
        interrupts |= cAtAluFpgaGeneralStickyRxPweWrCacheDisable;

    if (regVal & cFpgaSticky2RxPweDataBufferDisableMask)
        interrupts |= cAtAluFpgaGeneralStickyRxPweDataBufferDisable;

    if (regVal & cFpgaSticky2RxPweRdCacheLostMask)
        interrupts |= cAtAluFpgaGeneralStickyRxPweRdCacheLost;

    if (regVal & cFpgaSticky2RxPweLostStartOfPktMask)
        interrupts |= cAtAluFpgaGeneralStickyRxPweLostStartOfPkt;

    if (regVal & cFpgaSticky2RxPweLostEndOfPktMask)
        interrupts |= cAtAluFpgaGeneralStickyRxPweLostEndOfPkt;

    if (regVal & cFpgaSticky2RxPweRdCacheErrorMask)
        interrupts |= cAtAluFpgaGeneralStickyRxPweRdCacheError;

    if (regVal & cFpgaSticky2RxPweWrCacheEmptyMask)
        interrupts |= cAtAluFpgaGeneralStickyRxPweWrCacheEmpty;

    if (regVal & cFpgaSticky2RxPweWrCacheDuplicationMask)
        interrupts |= cAtAluFpgaGeneralStickyRxPweWrCacheDuplication;

    if (regVal & cFpgaSticky2RxPweRdCacheEmptyMask)
        interrupts |= cAtAluFpgaGeneralStickyRxPweRdCacheEmpty;

    if (regVal & cFpgaSticky2RxPweRdCacheDuplicationMask)
        interrupts |= cAtAluFpgaGeneralStickyRxPweRdCacheDuplication;

    if (regVal & cFpgaSticky2RxPweDataBufferEmptyMask)
        interrupts |= cAtAluFpgaGeneralStickyRxPweDataBufferEmpty;

    if (regVal & cFpgaSticky2RxPweDataBlockDuplicationMask)
        interrupts |= cAtAluFpgaGeneralStickyRxPweDataBlockDuplication;

    return interrupts;
    }

/**
 * Clear general alarm interrupt events
 *
 * @param device Device

 * @return Alarm interrupt events. Refer:
 *         - @ref eAtAluFpgaGeneralSticky
 */
uint32 AtAluGeneralAlarmStickyClear(AtDevice device)
    {
    uint32 interrupts;
    AtHal hal;
    uint32 mask;

    if (device == NULL)
        return 0;

    interrupts = AtAluGeneralAlarmStickyGet(device);
    hal = AtDeviceIpCoreHalGet(device, 0);

    mask = cFpgaSticky1TxPktInfoMemoryBusErrorMask   |
           cFpgaSticky1RxPktInfoEccErrorMask         |
           cFpgaSticky1RxPktInfoMemoryBusErrorMask   |
           cFpgaSticky1RxPktDataReorderCmdErrorMask  |
           cFpgaSticky1RxPktDataMemoryBusErrorMask   |
           cFpgaSticky1RxPktDataDdrCrcErrorMask      |
           cFpgaSticky1TxPweWrCacheDuplicationMask   |
           cFpgaSticky1TxPweWrCacheEmptyMask         |
           cFpgaSticky1TxPweDataBlockDuplicationMask |
           cFpgaSticky1TxPweRdCacheDuplicationMask;
    AtHalWrite(hal, cFpgaSticky1, mask);


    mask = cFpgaSticky2RxPweWrPktOversizeMask       |
           cFpgaSticky2RxPweWrDataBufferPauseMask   |
           cFpgaSticky2RxPweWrCacheDisableMask      |
           cFpgaSticky2RxPweDataBufferDisableMask   |
           cFpgaSticky2RxPweRdCacheLostMask         |
           cFpgaSticky2RxPweLostStartOfPktMask      |
           cFpgaSticky2RxPweLostEndOfPktMask        |
           cFpgaSticky2RxPweRdCacheErrorMask        |
           cFpgaSticky2RxPweWrCacheEmptyMask        |
           cFpgaSticky2RxPweWrCacheDuplicationMask  |
           cFpgaSticky2RxPweRdCacheEmptyMask        |
           cFpgaSticky2RxPweRdCacheDuplicationMask  |
           cFpgaSticky2RxPweDataBufferEmptyMask     |
           cFpgaSticky2RxPweDataBlockDuplicationMask;
    AtHalWrite(hal, cFpgaSticky2, mask);

    return interrupts;
    }

/**
 * Get QDR parity error sticky
 *
 * @param device Device
 * @param qdrId QDR ID [0..1]

 * @return cAtTrue  QDR has parity error event
 *         cAtFalse QDR does not have parity error event
 */
eBool AtAluQdrParityErrorGet(AtDevice device, uint8 qdrId)
    {
    AtHal hal;
    uint32 regVal;

    if (device == NULL)
        return cAtFalse;

    hal = AtDeviceIpCoreHalGet(device, 0);
    regVal = AtHalRead(hal, cFpgaSticky);

    if (qdrId == 0)
        return (eBool)mRegField(regVal, cThaFpgaStickyQDRNo1ParityError);

    if (qdrId == 1)
        return (eBool)mRegField(regVal, cThaFpgaStickyQDRNo2ParityError);

    return cAtFalse;
    }

/**
 * Clear QDR parity error sticky
 *
 * @param device Device
 * @param qdrId QDR ID [0..1]

 * @return cAtTrue  QDR has parity error event
 *         cAtFalse QDR does not have parity error event
 */
eBool AtAluQdrParityErrorClear(AtDevice device, uint8 qdrId)
    {
    AtHal hal;
    eBool parityError;

    if (device == NULL)
        return 0;

    parityError = AtAluQdrParityErrorGet(device, qdrId);

    /* Clear sticky */
    hal = AtDeviceIpCoreHalGet(device, 0);
    if (qdrId == 0)
        AtHalWrite(hal, cFpgaSticky, cThaFpgaStickyQDRNo1ParityErrorMask);

    if (qdrId == 1)
        AtHalWrite(hal, cFpgaSticky, cThaFpgaStickyQDRNo2ParityErrorMask);

    return parityError;
    }

/**
 * Get XGMII clock 156.25 MHz current frequency
 *
 * @param device Device

 * @return Clock frequency in Hz unit
 */
uint32 AtAluXGMIIClock156_25MHzCurrentFrequencyGet(AtDevice device)
    {
    AtHal hal = AtDeviceIpCoreHalGet(device, 0);
    return AtHalRead(hal, cFPGAXGMIIClock156_25MhzValueStatus);
    }

/**
 * Get OCN Tx clock 155.52 MHz current frequency
 *
 * @param device Device

 * @return Clock frequency in Hz unit
 */
uint32 AtAluOcnTxClock155_52MHzCurrentFrequencyGet(AtDevice device)
    {
    AtHal hal = AtDeviceIpCoreHalGet(device, 0);
    return AtHalRead(hal, cFPGAOCNTxClock155_52MhzValueStatus);
    }

/**
 * Get OCN Rx clock 155.52 MHz current frequency
 *
 * @param device Device

 * @return Clock frequency in Hz unit
 */
uint32 AtAluOcnRxClock155_52MHzCurrentFrequencyGet(AtDevice device)
    {
    AtHal hal = AtDeviceIpCoreHalGet(device, 0);
    return AtHalRead(hal, cFPGAOCNRxClock155_52MhzValueStatus);
    }

/**
 * Get core 1 clock 100 MHz current frequency
 *
 * @param device Device

 * @return Clock frequency in Hz unit
 */
uint32 AtAluCore1Clock100MHzCurrentFrequencyGet(AtDevice device)
    {
    AtHal hal = AtDeviceIpCoreHalGet(device, 0);
    return AtHalRead(hal, cFPGACore1Clock100MhzValueStatus);
    }

/**
 * Get core 2 clock 100 MHz current frequency
 *
 * @param device Device

 * @return Clock frequency in Hz unit
 */
uint32 AtAluCore2Clock100MHzCurrentFrequencyGet(AtDevice device)
    {
    AtHal hal = AtDeviceIpCoreHalGet(device, 0);
    return AtHalRead(hal, cFPGACore2Clock100MhzValueStatus);
    }

/**
 * Get DDR3 user clock 100 MHz current frequency
 *
 * @param device Device

 * @return Clock frequency in Hz unit
 */
uint32 AtAluDdr3UserClock100MHzCurrentFrequencyGet(AtDevice device)
    {
    AtHal hal = AtDeviceIpCoreHalGet(device, 0);
    return AtHalRead(hal, cFPGADDR3UserClock100MhzValueStatus);
    }

/**
 * Get PCIE user clock 100 MHz current frequency
 *
 * @param device Device

 * @return Clock frequency in Hz unit
 */
uint32 AtAluPcieUserClock62_5MHzCurrentFrequencyGet(AtDevice device)
    {
    AtHal hal = AtDeviceIpCoreHalGet(device, 0);
    return AtHalRead(hal, cFPGAPCIEUserClock62_5MhzValueStatus);
    }

/**
 * Get QDR#1 user clock 100 MHz current frequency
 *
 * @param device Device

 * @return Clock frequency in Hz unit
 */
uint32 AtAluQdrNo1FeedbackClock155_52MHzCurrentFrequencyGet(AtDevice device)
    {
    AtHal hal = AtDeviceIpCoreHalGet(device, 0);
    return AtHalRead(hal, cFPGAQDRNo1FeedBackClock155_52MhzValueStatus);
    }

/**
 * Get QDR#2 user clock 100 MHz current frequency
 *
 * @param device Device

 * @return Clock frequency in Hz unit
 */
uint32 AtAluQdrNo2FeedbackClock155_52MHzCurrentFrequencyGet(AtDevice device)
    {
    AtHal hal = AtDeviceIpCoreHalGet(device, 0);
    return AtHalRead(hal, cFPGAQDRNo2FeedBackClock155_52MhzValueStatus);
    }

/**
 * Get max level of GE FIFO
 *
 * @param device Device
 * @return Max level in byte unit
 */
uint32 AtAluGeFifoMaxLevelGet(AtDevice device)
    {
    uint32 maxLevelInNumBlock8Bytes;
    AtHal hal = AtDeviceIpCoreHalGet(device, 0);
    uint32 regVal = AtHalRead(hal, cFpgaGeFifoLevel);
    maxLevelInNumBlock8Bytes = mRegField(regVal, cFpgaGeFifoLevel);

    return maxLevelInNumBlock8Bytes * 8;
    }

/**
 * Get and clear max level status of GE FIFO
 *
 * @param device Device
 * @return Max level in byte unit
 */
uint32 AtAluGeFifoMaxLevelClear(AtDevice device)
    {
    uint32 maxLevelInNumBlock8Bytes;
    AtHal hal = AtDeviceIpCoreHalGet(device, 0);
    uint32 regVal = AtHalRead(hal, cFpgaGeFifoLevel);
    maxLevelInNumBlock8Bytes = mRegField(regVal, cFpgaGeFifoLevel);

    /* Clear for next time */
    AtHalWrite(hal, cFpgaGeFifoLevel, regVal & (~cFpgaGeFifoLevelMask));

    return maxLevelInNumBlock8Bytes * 8;
    }

/**
 * @}
 */

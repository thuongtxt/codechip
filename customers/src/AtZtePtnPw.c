/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APP
 *
 * File        : AtZtePtnPw.c
 *
 * Created Date: Oct 28, 2013
 *
 * Description : To implement specific interfaces between Arrive and ZTE PTN
 *               layers, for PW products
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtZtePtnInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern eAtRet ThaModulePwSendLbitPacketsToCdrEnable(AtModulePw self, eBool enable);
extern eBool ThaModulePwSendLbitPacketsToCdrIsEnabled(AtModulePw self);

/*--------------------------- Implementation ---------------------------------*/
tAtZtePtnTag1 *AtZtePtnPwTag1Get(AtPw self, tAtZtePtnTag1 *zteTag1)
    {
    tAtEthVlanTag sVlan;

    if (AtPwEthSVlanGet(self, &sVlan) == NULL)
        return NULL;

    return AtZtePtnTag1FromSVlan(&sVlan, zteTag1);
    }

tAtZtePtnTag2 *AtZtePtnPwTag2Get(AtPw self, tAtZtePtnTag2 *zteTag2)
    {
    tAtEthVlanTag  cVlan;

    if (AtPwEthCVlanGet(self, &cVlan) == NULL)
        return NULL;

    return AtZtePtnTag2FromCVlan(&cVlan, zteTag2);
    }

eAtRet AtZtePwPtnHeaderSet(AtPw self, uint8 *destMac, const tAtZtePtnTag1 *zteTag1, const tAtZtePtnTag2 *zteTag2)
    {
    tAtEthVlanTag cVlan, sVlan;

    if ((AtZtePtnSVlanFromTag1(zteTag1, &sVlan) == NULL) ||
        (AtZtePtnCVlanFromTag2(zteTag2, &cVlan) == NULL))
        return cAtErrorInvlParm;

    return AtPwEthHeaderSet(self, destMac, &cVlan, &sVlan);
    }

eAtRet AtZtePtnPwExpectedTag2Set(AtPw self, tAtZtePtnTag2 *tag2)
    {
    tAtEthVlanTag cVlan;

    if (AtZtePtnCVlanFromTag2(tag2, &cVlan) == NULL)
        return cAtErrorInvlParm;

    return AtPwEthExpectedCVlanSet(self, &cVlan);
    }

tAtZtePtnTag2 *AtZtePtnPwExpectedTag2Get(AtPw self, tAtZtePtnTag2 *tag2)
    {
    tAtEthVlanTag cVlan;
    return AtZtePtnTag2FromCVlan(AtPwEthExpectedCVlanGet(self, &cVlan), tag2);
    }

eAtRet AtZtePtnPwExpectedTag1Set(AtPw self, tAtZtePtnTag1 *tag1)
    {
    tAtEthVlanTag sVlan;

    if (AtZtePtnSVlanFromTag1(tag1, &sVlan) == NULL)
        return cAtErrorInvlParm;

    return AtPwEthExpectedSVlanSet(self, &sVlan);
    }

tAtZtePtnTag1 *AtZtePtnPwExpectedTag1Get(AtPw self, tAtZtePtnTag1 *tag1)
    {
    tAtEthVlanTag sVlan;
    return AtZtePtnTag1FromSVlan(AtPwEthExpectedSVlanGet(self, &sVlan), tag1);
    }

eAtRet AtZtePtnPwSendLbitPacketsToCdrEnable(AtModulePw self, eBool enable)
    {
    return ThaModulePwSendLbitPacketsToCdrEnable(self, enable);
    }

eBool AtZtePtnPwSendLbitPacketsToCdrIsEnabled(AtModulePw self)
    {
    return ThaModulePwSendLbitPacketsToCdrIsEnabled(self);
    }

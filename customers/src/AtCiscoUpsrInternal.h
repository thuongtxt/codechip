/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : UPRS
 * 
 * File        : AtCiscoUpsrInternal.h
 * 
 * Created Date: Dec 19, 2016
 *
 * Description : UPRS internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCISCOUPSRINTERNAL_H_
#define _ATCISCOUPSRINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#ifndef AT_SYSTEM_TYPE_H
#define AT_SYSTEM_TYPE_H <stddef.h>
#endif
#include AT_SYSTEM_TYPE_H

#ifndef AT_NONE_OS
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#endif

#include "AtCiscoUpsr.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define AtUnused(x) (void)(x)
#define mBaseAddress(self) (self)->baseAddress

#define mMethodsGet(object) (object)->methods
#define mMethodOverride(methods, methodName) (methods).methodName = methodName

#define mRealAddress(baseAddress, address) (size_t)(baseAddress + (((address) - cDwordStartOffset) << 2))
#define mRegField(regVal, mask, shift) ((at_uint32)(((regVal) & mask) >> (shift)))
#define mRegFieldSet(regVal, mask, shift, fieldValue)                            \
    (regVal) = (at_uint32)((at_uint32)((regVal) & (~(mask))) | (((at_uint32)(fieldValue) << ((at_uint32)(shift))) & (mask)))

#ifdef AT_NONE_OS
#define mAssert(condition)
#else
#define mAssert(condition) assert(condition)
#endif

/* Bit mask definitions */
#define cBit0     0x00000001UL
#define cBit1     0x00000002UL
#define cBit6     0x00000040UL
#define cBit7_0   0x000000FFUL
#define cBit15_8  0x0000FF00UL

#define cUpsrOffset   0x200000U
#define cHspwTxOffset 0x400000U
#define cHspwRxOffset 0x600000U

/* AME Control register */
#define cAf6Reg_upen_amectl                                                                           0x0F0002
/* Status Interrupt restore */
#define cAf6_upen_amectl_AMEStaFIRT_Mask                                                                 cBit7
#define cAf6_upen_amectl_AMEStaFIRT_Shift                                                                    7
/* Force Interrupt restore */
#define cAf6_upen_amectl_AMECtlFIRT_Mask                                                                 cBit6
#define cAf6_upen_amectl_AMECtlFIRT_Shift                                                                    6

/* AME SF/SD Alarm Mask */
#define cAf6Reg_upen_ameintdi                                                                         0x0F0010
#define cAf6_upen_ameintdi_AMESDDis_Mask                                                                 cBit2
#define cAf6_upen_ameintdi_AMESDDis_Shift                                                                    2
#define cAf6_upen_ameintdi_AMESFDis_Mask                                                                 cBit1
#define cAf6_upen_ameintdi_AMESFDis_Shift                                                                    1
#define cAf6_upen_ameintdi_AMEIntDis_Mask                                                                cBit0
#define cAf6_upen_ameintdi_AMEIntDis_Shift                                                                   0

/* AME SF MASR register */
#define cAf6Reg_upen_sfrowsts                                                                         0x0F0011
#define cAf6_upen_sfrowsts_AMESFrowVT_Mask                                                           cBit23_12
#define cAf6_upen_sfrowsts_AMESFrowVT_Shift                                                                 12
#define cAf6_upen_sfrowsts_AMESFrowSTS_Mask                                                           cBit11_0
#define cAf6_upen_sfrowsts_AMESFrowSTS_Shift                                                                 0

/* AME SD MASR register */
#define cAf6Reg_upen_sdrowsts                                                                         0x0F0012
#define cAf6_upen_sdrowsts_AMESDrowVT_Mask                                                           cBit23_12
#define cAf6_upen_sdrowsts_AMESDrowVT_Shift                                                                 12
#define cAf6_upen_sdrowsts_AMESDrowSTS_Mask                                                           cBit11_0
#define cAf6_upen_sdrowsts_AMESDrowSTS_Shift                                                                 0

/* AME STS Status table */
#define cAf6Reg_upen_xxstasts(sdtable, stsgroup)                        (0x0F2000+(sdtable)*0x8000+(stsgroup))
#define cAf6_upen_xxstasts_AMEStaSTS_Mask                                                             cBit31_0
#define cAf6_upen_xxstasts_AMEStaSTS_Shift                                                                   0

/* AME STS Enable table */
#define cAf6Reg_upen_xxcfgsts(sdtable, stsgroup)                        (0x0F3000+(sdtable)*0x8000+(stsgroup))
#define cAf6_upen_xxcfgsts_AMEEnaSTS_Mask                                                             cBit31_0
#define cAf6_upen_xxcfgsts_AMEEnaSTS_Shift                                                                   0

/* AME SASR VT Status table */
#define cAf6Reg_upen_xxrowvt(sdtable, stsgroup)                         (0x0F2100+(sdtable)*0x8000+(stsgroup))
#define cAf6_upen_xxrowvt_AMEStaRowVT_Mask                                                            cBit31_0
#define cAf6_upen_xxrowvt_AMEStaRowVT_Shift                                                                  0

/* AME VT Status table */
#define cAf6Reg_upen_xxstavt(sdtable, stsid)                               (0x0F2400+(sdtable)*0x8000+(stsid))
#define cAf6_upen_xxstavt_AMEStaVT_Mask                                                               cBit27_0
#define cAf6_upen_xxstavt_AMEStaVT_Shift                                                                     0

/* AME VT Enable table */
#define cAf6Reg_upen_xxcfgvt(sdtable, stsid)                               (0x0F3400+(sdtable)*0x8000+(stsid))
#define cAf6_upen_xxcfgvt_AMEEnaVT_Mask                                                               cBit27_0
#define cAf6_upen_xxcfgvt_AMEEnaVT_Shift                                                                     0

/* Payload Assembler Output HSPW Control */
#define cAf6Reg_pla_out_hspw_ctrl(hspwid)                                                   (0x98000+(hspwid))
#define cAf6_pla_out_hspw_ctrl_PlaOutHspwPsnCtr_Mask                                                     cBit1
#define cAf6_pla_out_hspw_ctrl_PlaOutHspwPsnCtr_Shift                                                        1
#define cAf6_pla_out_hspw_ctrl_PlaOutHspwEnCtr_Mask                                                      cBit0
#define cAf6_pla_out_hspw_ctrl_PlaOutHspwEnCtr_Shift                                                         0

/* Payload Assembler Output UPSR Control */
#define cAf6Reg_pla_out_upsr_ctrl(group16)                                                 (0x94000+(group16))
#define cAf6_pla_out_upsr_ctrl_PlaOutUpsrEnCtr_Mask(idInGroup16)                           (1UL << (idInGroup16))
#define cAf6_pla_out_upsr_ctrl_PlaOutUpsrEnCtr_Shift(idInGroup16)                          (idInGroup16)

#define cAf6Reg_cla_per_grp_enb_Base                                                        (0x70000)

/* AME SF/SD Alarm Mask register */
#define cAf6Reg_upen_amemask                                                                          0x0F0004
#define cAf6_upen_amemask_AMESDAlmMask_Mask                                                           cBit15_8
#define cAf6_upen_amemask_AMESDAlmMask_Shift                                                                 8
#define cAf6_upen_amemask_AMESFAlmMask_Mask                                                            cBit7_0
#define cAf6_upen_amemask_AMESFAlmMask_Shift                                                                 0

#ifdef AT_NONE_OS
extern void * malloc (unsigned int size);
extern void free (void *ptr);
extern void * memset (void *block, int c, unsigned int size);
extern void *memcpy(void *dest, const void *src, size_t n);
#endif

#ifndef NULL
#define NULL ((void*)0)
#endif

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtCiscoUpsrMethods
    {
    at_uint32 (*MaxHsGroupsGet)(AtCiscoUpsr self);
    at_uint32 (*MaxApsGroupsGet)(AtCiscoUpsr self);
    at_uint32 (*MaxStsGet)(AtCiscoUpsr self);

    at_uint32 (*UpsrStartOffset)(AtCiscoUpsr self);
    at_uint32 (*UpsrMemSizeInBytes)(AtCiscoUpsr self);

    void (*HwTxApsGroupEnable)(AtCiscoUpsr self, at_uint32 apsGroupId, at_uint32 enable);
    at_uint32 (*HwTxApsGroupIsEnabled)(AtCiscoUpsr self, at_uint32 apsGroupId);
    at_uint32 (*RxHsGroupPrimaryOffset)(AtCiscoUpsr self, at_uint32 hsGroupId);
    void (*HsGroupTxLabelSetSelect)(AtCiscoUpsr self, at_uint32 hsGroupId, eAtCiscoPwGroupLabelSet labelSet);
    eAtCiscoPwGroupLabelSet (*HsGroupTxSelectedLabelGet)(AtCiscoUpsr self, at_uint32 hsGroupId);

    }tAtCiscoUpsrMethods;

typedef struct tAtCiscoUpsr
    {
    tAtCiscoUpsrMethods methods;

    at_uint32 baseAddress;
    eAtCiscoUpsrRet latchedError;

    tAtCiscoUpsrDelegate delegate;
    void *delegateData;

    at_uint32 version;
    at_uint32 builtNumber;
    at_uint32 productCode;
    at_uint32 metaBaseAddress;
    }tAtCiscoUpsr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
at_uint32 AtCiscoUpsrHwRead(AtCiscoUpsr self, at_uint32 address);
void AtCiscoUpsrHwWrite(AtCiscoUpsr self, at_uint32 address, at_uint32 value);
AtCiscoUpsr AtCiscoOcnCardUpsrObjectInit(AtCiscoUpsr self, at_uint32 metaBaseAddress, at_uint32 productCode);
AtCiscoUpsr AtCisco5GMsCardUpsrObjectInit(AtCiscoUpsr self, at_uint32 metaBaseAddress, at_uint32 productCode);

AtCiscoUpsr AtCiscoOcnCardUpsrNew(at_uint32 metaBaseAddress, at_uint32 productCode);
AtCiscoUpsr AtCisco5GMsCardUpsrNew(at_uint32 metaBaseAddress, at_uint32 productCode);
AtCiscoUpsr AtCisco3GMsCardUpsrNew(at_uint32 metaBaseAddress, at_uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _ATCISCOUPSRINTERNAL_H_ */


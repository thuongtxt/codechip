/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ALU
 *
 * File        : AtAluCli.c
 *
 * Created Date: Nov 13, 2014
 *
 * Description : To startup CLI layer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDriver.h"
#include "AtDevice.h"
#include "AtCliModule.h"
#include "AtAlu.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mAtSuccessAssert(ret)                                                 \
    do                                                                         \
        {                                                                      \
        eAtRet _ret = ret;                                                     \
        if (_ret != cAtOk)                                                     \
            {                                                                  \
            AtPrintc(cSevCritical, "%s, %d, ret = %s\r\n", __FILE__, __LINE__, AtRet2String(_ret)); \
            return _ret;                                                       \
            }                                                                  \
        }while(0)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static AtTextUI m_textUI = NULL; /* To avoid recreate it again */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet CliStart(void)
    {
    if (m_textUI == NULL)
        m_textUI = AtDefaultTinyTextUINew();

    AtCliStartWithTextUI(m_textUI);
    return cAtOk;
    }

/**
 * @addtogroup AtCustomer
 * @{
 */

/**
 * This function creates new driver instance, create new device and restore
 * configuration database from hardware.
 *
 * @param hal HAL to recover configuration database from hardware.
 *
 * @return AT return code.
 * @note This function create driver, device along with Text-UI so memories are
 * allocated. To cleanup memory, the function AtAluCliCleanup() should be called.
 */
eAtRet AtAluCliStart(AtHal hal)
    {
    AtDriver driver = AtDriverSharedDriverGet();
    AtDevice device;

    /* Create driver instance if necessary */
    if (driver == NULL)
        driver = AtDriverCreate(1, AtOsalLinux());

    /* Recreate device because HAL may be changed and to have a clean state */
    device = AtDriverDeviceGet(driver, 0);
    if (device)
        AtDriverDeviceDelete(driver, device);

    /* Create new device with the input HAL, then warm restore it */
    device = AtDriverDeviceCreate(driver, AtProductCodeGetByHal(hal));
    mAtSuccessAssert(AtDeviceIpCoreHalSet(device, 0, hal));
    mAtSuccessAssert(AtDeviceWarmRestore(device));

    /* Start CLI to control this device */
    return CliStart();
    }

/**
 * To clean up all of resources allocated by AtAluCliStart()
 *
 * @return AT return code
 */
eAtRet AtAluCliCleanup(void)
    {
    /* Delete Text-UI */
    AtTextUIDelete(m_textUI);
    m_textUI = NULL;

    /* Then the driver */
    AtDriverDelete(AtDriverSharedDriverGet());

    return cAtOk;
    }

/**
 * @}
 */

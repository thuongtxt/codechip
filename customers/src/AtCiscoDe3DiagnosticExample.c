/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Cisco sample code
 *
 * File        : AtCiscoDe3DiagnosticExample.c
 *
 * Created Date: Mar 27, 2015
 *
 * Description : Cisco sample code implementation
 *               There are two main functions in this file:
 *               - De3DiagnosticSetup: to emulate all of DE3s of all lines over
 *                 UDP/IP via ETH Port.
 *               - De3DiagnosticTearDown: To tear down all of emulated circuits
 *                 setup by the De3DiagnosticSetup()
 *
 * Notes       : Developers may refer TODO tags in this source file to change
 *               configuration on PSN header or PW resource allocation.
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDriver.h"
#include "AtEthPort.h"
#include "AtSdhLine.h"
#include "AtSdhVc.h"
#include "AtPw.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mSuccessAssert(ret)                                                    \
    do                                                                         \
        {                                                                      \
        eAtRet ret__ = ret;                                                    \
        if (ret__ != cAtOk)                                                    \
            return ret__;                                                      \
        }while(0)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
eAtRet De3DiagnosticSetup(void);
eAtRet De3DiagnosticTearDown(void);

/*--------------------------- Implementation ---------------------------------*/
static AtDevice DeviceGet(void)
    {
    return AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
    }

static AtModuleSdh SdhModule(AtDevice device)
    {
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    }

static AtModuleEth EthModule(AtDevice device)
    {
    return (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    }

static AtModulePw PwModule(AtDevice device)
    {
    return (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static uint8 *DefaultEthPortSourceMac(uint32 ethPortId)
    {
    /* TODO: just an example, user can replace it by another value */
    static uint8 buf[6] = {0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA};
    AtUnused(ethPortId);
    return buf;
    }

static uint8 *DefaultEthPortSourceIpv4(uint32 ethPortId)
    {
    /* TODO: just an example, user can replace it by another value */
    static uint8 buf[4] = {172, 0, 0, 0};
    AtUnused(ethPortId);
    return buf;
    }

static uint8 *DefaultPwDestMac(uint32 ethPortId)
    {
    /* TODO: just an example, user can replace it by another value */
    static uint8 buf[6] = {0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA};
    AtUnused(ethPortId);
    return buf;
    }

static uint8 *DefaultPwSourceIpv4(uint32 ethPortId)
    {
    /* TODO: just an example, user can replace it by another value */
    static uint8 buf[4] = {172, 0, 0, 0};
    AtUnused(ethPortId);
    return buf;
    }

static uint8 *DefaultPwDestIpv4(uint32 ethPortId)
    {
    /* TODO: just an example, user can replace it by another value */
    static uint8 buf[4] = {172, 0, 0, 0};
    AtUnused(ethPortId);
    return buf;
    }

static uint16 DefaultPwUdpSourcePort(uint16 pwId)
    {
    /* TODO: just an example, user can replace it by another value */
    return pwId;
    }

static uint16 DefaultPwUdpDestPort(uint16 pwId)
    {
    /* TODO: just an example, user can replace it by another value */
    return pwId;
    }

static uint16 DefaultPwUdpExpectedPort(uint16 pwId)
    {
    /* TODO: just an example, user can replace it by another value */
    return pwId;
    }

static uint16 DefaultPwPayloadSize(uint16 pwId)
    {
    /* TODO: just an example, user can replace it by another value */
    AtUnused(pwId);
    return 1024;
    }

static AtPwPsn UdpIpv4PsnCreate(uint16 pwId, uint16 ethPortId)
    {
    AtPwUdpPsn udpPsn   = AtPwUdpPsnNew();
    AtPwIpV4Psn ipv4Psn = AtPwIpV4PsnNew();

    /* Setup UDP */
    AtPwUdpPsnSourcePortSet(udpPsn, DefaultPwUdpSourcePort(pwId));
    AtPwUdpPsnDestPortSet(udpPsn, DefaultPwUdpDestPort(pwId));
    AtPwUdpPsnExpectedPortSet(udpPsn, DefaultPwUdpExpectedPort(pwId));

    /* Setup IP */
    AtPwIpPsnSourceAddressSet((AtPwIpPsn)ipv4Psn, DefaultPwSourceIpv4(ethPortId));
    AtPwIpPsnDestAddressSet((AtPwIpPsn)ipv4Psn, DefaultPwDestIpv4(ethPortId));

    /* TODO: can set other attributes of each PSN here ... */

    /* Build PSN hierarchy */
    AtPwPsnLowerPsnSet((AtPwPsn)udpPsn, (AtPwPsn)ipv4Psn);

    return (AtPwPsn)udpPsn;
    }

static uint8 NumberOfLines(void)
    {
    return 48;
    }

static uint8 DefaultEthernetPortId(void)
    {
    return 0;
    }

/*
 * Example function to emulate all DE3s of all lines over UDP/IP via ETH Port
 *
 * @param None
 *
 * @return AT return code
 */
eAtRet De3DiagnosticSetup(void)
    {
    uint8 sdhLineId;
    AtPw pw;
    AtPwPsn psn;
    AtDevice device = DeviceGet();
    AtModulePw pwModule = PwModule(device);
    uint8 ethPortId = DefaultEthernetPortId();
    AtEthPort ethPort = AtModuleEthPortGet(EthModule(device), ethPortId);

    /* Each line contains one PW, set up all PWs for all lines.
     * VC3 and DE3 object are created after initialize device,
     * so just need to get the object to use here */
    for (sdhLineId = 0; sdhLineId < NumberOfLines(); sdhLineId++)
        {
        uint16 pwId = sdhLineId;
        AtSdhLine line = AtModuleSdhLineGet(SdhModule(device), sdhLineId);
        AtSdhVc vc3 = AtSdhLineVc3Get(line, 0, 0);
        AtPdhDe3 de3 = (AtPdhDe3)AtSdhChannelMapChannelGet((AtSdhChannel)vc3);
        pw = (AtPw)AtModulePwSAToPCreate(pwModule, pwId);

        mSuccessAssert(AtPwPayloadSizeSet(pw, DefaultPwPayloadSize(pwId)));
        mSuccessAssert(AtPwCircuitBind(pw, (AtChannel)de3));
        mSuccessAssert(AtPwEthPortSet(pw, ethPort));

        /* Configure Ethernet header with default MAC and no VLAN */
        mSuccessAssert(AtPwEthHeaderSet(pw, DefaultPwDestMac(ethPortId), NULL, NULL));

        /* Create UDP over IPv4 PSN */
        psn = UdpIpv4PsnCreate(pwId, ethPortId);
        mSuccessAssert(AtPwPsnSet(pw, psn));

        mSuccessAssert(AtChannelEnable((AtChannel)pw, cAtTrue));

        /* Clean objects */
        AtObjectDelete((AtObject)psn);
        }

    /* Set Source MAC and IP for Ethernet Port */
    mSuccessAssert(AtEthPortSourceMacAddressSet(ethPort, DefaultEthPortSourceMac(ethPortId)));
    mSuccessAssert(AtEthPortIpV4AddressSet(ethPort, DefaultEthPortSourceIpv4(ethPortId)));
    mSuccessAssert(AtChannelLoopbackSet((AtChannel)ethPort, cAtLoopbackModeRelease));

    return cAtOk;
    }

/*
 * To tear down all of emulated circuits setup by the De3DiagnosticSetup()
 *
 * @param None
 *
 * @return AT return code
 */
eAtRet De3DiagnosticTearDown(void)
    {
    uint16 sdhLineId;
    AtModulePw pwModule = PwModule(DeviceGet());

    for (sdhLineId = 0; sdhLineId < NumberOfLines(); sdhLineId++)
        {
        uint16 pwId = sdhLineId;
        AtPw pw = (AtPw)AtModulePwGetPw(pwModule, pwId);
        if (pw == NULL)
            continue;

        mSuccessAssert(AtPwCircuitUnbind(pw));
        mSuccessAssert(AtModulePwDeletePw(pwModule, pwId));
        }

    return cAtOk;
    }

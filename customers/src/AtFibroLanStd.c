/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : STD
 *
 * File        : AtFibroLanStd.c
 *
 * Created Date: Feb 5, 2015
 *
 * Description : Fibrolan STD
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCommon.h"
#include "../../util/std/AtStdDefaultInternal.h"
#include "AtOsal.h"
#include "AtStd.h"
#include "AtFibroLan.h"

/*--------------------------- Define -----------------------------------------*/
#define cMsgBufferLength     1024

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtFibroLanStd * AtFibroLanStd;

typedef struct tAtFibroLanStd
    {
    tAtStdDefault super;

    int (*printFunc)(const char *aString);
    char sharedMsgBuffer[cMsgBufferLength];
    }tAtFibroLanStd;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtStdMethods m_AtStdOverride;

static const tAtStdMethods * m_AtStdMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static char* SharedMessageBufferGet(AtFibroLanStd self, uint32* bufferLength)
    {
    if (bufferLength)
        *bufferLength = cMsgBufferLength;

    AtOsalMemInit(self->sharedMsgBuffer, 0, cMsgBufferLength);
    return self->sharedMsgBuffer;
    }

static eBool ShouldRedirectPrintf(AtFibroLanStd self)
    {
    if (self->printFunc == NULL)
        return cAtFalse;
    return cAtTrue;
    }

AtAttributePrintf(2, 0)
static uint32 Printf(AtStd self, const char *format, va_list ap)
    {
    AtFibroLanStd fibroLanStd = (AtFibroLanStd)self;
    char* buffer;
    uint32 bufferSize;

    if (!ShouldRedirectPrintf(fibroLanStd))
        return m_AtStdMethods->Printf(self, format, ap);

    buffer = SharedMessageBufferGet(fibroLanStd, &bufferSize);

    /* Build message buffer */
    AtStdSnprintf(self, buffer, bufferSize - 1, format, ap);
    buffer[bufferSize - 1] = '\0';

    return (uint32)(fibroLanStd->printFunc((const char*)buffer));
    }

static void OverrideAtStd(AtStd self)
    {
    if (!m_methodsInit)
        {
        m_AtStdMethods = mMethodsGet(self);
        AtOsalMemCpy(&m_AtStdOverride, m_AtStdMethods, sizeof(m_AtStdOverride));

        mMethodOverride(m_AtStdOverride, Printf);
        }

    mMethodsSet(self, &m_AtStdOverride);
    }

static void Override(AtStd self)
    {
    OverrideAtStd(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtFibroLanStd);
    }

static AtStd AtFibroLanStdObjectInit(AtStd self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtStdDefaultObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

static AtStd FibroLanSharedStd(void)
    {
    static tAtFibroLanStd m_std;
    static AtStd m_sharedStd = NULL;

    if (m_sharedStd == NULL)
        {
        m_sharedStd = AtFibroLanStdObjectInit((AtStd)&m_std);
        if (m_sharedStd)
            AtStdSharedStdSet(m_sharedStd);
        }

    return m_sharedStd;
    }

/**
 * Set print function
 *
 * @param printFunc Print function
 * @return AT return code
 */
eAtRet AtFibroLanPrintFuncSet(int (*printFunc)(const char *aString))
    {
    AtFibroLanStd fibroLanStd = (AtFibroLanStd)FibroLanSharedStd();
    if (fibroLanStd == NULL)
        return cAtError;

    fibroLanStd->printFunc = printFunc;
    return cAtOk;
    }


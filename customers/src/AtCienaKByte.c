/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : AtCienaKByte.c
 *
 * Created Date: Nov 3, 2016
 *
 * Description : Handle K-Byte over SGMII
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../driver/src/generic/util/AtUtil.h"
#include "../../driver/src/implement/codechip/Tha60290021/pw/Tha60290021ModulePw.h"
#include "../../driver/src/implement/default/sdh/ThaSdhLine.h"
#include "AtCienaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mKbytePw(modulePw) (ThaModulePwKBytePwGet((ThaModulePw)modulePw))

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eThaSdhLineExtendedKBytePosition SdhLineExtendedKBytePositionApp2Internal(eAtCienaSdhLineExtendedKBytePosition position)
    {
    switch (position)
        {
        case cAtCienaSdhLineExtendedKBytePositionD1Sts4:
            return cThaSdhLineExtendedKBytePositionD1Sts4;
        case cAtCienaSdhLineExtendedKBytePositionD1Sts10:
            return cThaSdhLineExtendedKBytePositionD1Sts10;
        case cAtCienaSdhLineExtendedKBytePositionInvalid:
        default:
            return cThaSdhLineExtendedKBytePositionInvalid;
        }
    }

static eAtCienaSdhLineExtendedKBytePosition SdhLineExtendedKBytePositionInternal2App(eThaSdhLineExtendedKBytePosition internalPosition)
    {
    switch (internalPosition)
        {
        case cThaSdhLineExtendedKBytePositionD1Sts4:
            return cAtCienaSdhLineExtendedKBytePositionD1Sts4;
        case cThaSdhLineExtendedKBytePositionD1Sts10:
            return cAtCienaSdhLineExtendedKBytePositionD1Sts10;
        case cThaSdhLineExtendedKBytePositionInvalid:
        default:
            return cAtCienaSdhLineExtendedKBytePositionInvalid;
        }
    }

static eAtCienaSdhLineKByteSource KbyteSourceInternal2App(eThaSdhLineKByteSource internalSource)
    {
    switch (internalSource)
        {
        case cThaSdhLineKByteSourceCpu:
            return cAtCienaSdhLineKByteSourceCpu;
        case cThaSdhLineKByteSourceEth:
            return cAtCienaSdhLineKByteSourceSgmii;
        case cThaSdhLineKByteSourceUnknown:
        default:
            return cAtCienaSdhLineKByteSourceUnknown;
        }
    }

static eThaSdhLineKByteSource KbyteSourceApp2Internal(eAtCienaSdhLineKByteSource appSource)
    {
    switch (appSource)
        {
        case cAtCienaSdhLineKByteSourceCpu:
            return cThaSdhLineKByteSourceCpu;
        case cAtCienaSdhLineKByteSourceSgmii:
            return cThaSdhLineKByteSourceEth;
        case cAtCienaSdhLineKByteSourceUnknown:
        default:
            return cThaSdhLineKByteSourceUnknown;
        }
    }

static uint32 LabelValidInRange(uint16 label)
    {
    return (label <= cBit11_0) ? cAtTrue : cAtFalse;
    }

static AtPwKbyteChannel KbyteChannelGet(AtPw self, uint8 channelId)
    {
    return Tha60290021PwKbyteChannelGet(self, channelId);
    }

static eAtRet KBytePwChannelExpectedLabelSet(AtPw self, uint8 channelId, uint16 label)
    {
    if (LabelValidInRange(label))
        return AtPwKbyteChannelExpectedLabelSet(KbyteChannelGet(self, channelId), label);
    return cAtErrorOutOfRangParm;
    }

static eAtRet KBytePwChannelTxLabelSet(AtPw self, uint8 channelId, uint16 label)
    {
    if (LabelValidInRange(label))
        return AtPwKbyteChannelTxLabelSet(KbyteChannelGet(self, channelId), label);
    return cAtErrorOutOfRangParm;
    }

/**
 * @addtogroup AtCiena
 * @{
 */

/**
 * Set ETH Type field of KByte PW
 *
 * @param self The PW Module
 * @param ethType ETH Type
 *
 * @return AT return code
 */
eAtRet AtCienaKBytePwEthTypeSet(AtModulePw self, uint16 ethType)
    {
    AtPw pw = mKbytePw(self);
    eAtRet ret = cAtOk;

    mOneParamApiLogStart(ethType);
    ret |= Tha6029PwKbytePwTxEthTypeSet(pw, ethType);
    ret |= Tha6029PwKbytePwExpectedEthTypeSet(pw, ethType);
    AtDriverApiLogStop();

    return ret;
    }

/**
 * Get ETH Type field of KByte PW
 *
 * @param self The PW Module
 *
 * @return ETH Type
 */
uint16 AtCienaKBytePwEthTypeGet(AtModulePw self)
    {
    return Tha6029PwKbyteTxEthTypeGet(mKbytePw(self));
    }

/**
 * Set Type field for KByte PW
 *
 * @param self The PW Module
 * @param type Type value
 *
 * @return AT return code
 */
eAtRet AtCienaKBytePwTypeSet(AtModulePw self, uint8 type)
    {
    eAtRet ret = cAtOk;
    AtPw pw = mKbytePw(self);

    mOneParamApiLogStart(type);
    ret |= Tha6029PwKbyteTxTypeSet(pw, type);
    ret |= Tha6029PwKbyteExpectedTypeSet(pw, type);
    AtDriverApiLogStop();

    return ret;
    }

/**
 * Get Type field of KByte PW
 *
 * @param self The PW Module
 *
 * @return Type field
 */
uint8 AtCienaKBytePwTypeGet(AtModulePw self)
    {
    return Tha6029PwKbyteTxTypeGet(mKbytePw(self));
    }

/**
 * Set Version field for KByte PW
 *
 * @param self The PW Module
 * @param version Version value
 *
 * @return AT return code
 */
eAtRet AtCienaKBytePwVersionSet(AtModulePw self, uint8 version)
    {
    eAtRet ret = cAtOk;
    AtPw pw = mKbytePw(self);

    mOneParamApiLogStart(version);
    ret |= Tha6029PwKbyteTxVersionSet(pw, version);
    ret |= Tha6029PwKbyteExpectedVersionSet(pw, version);
    AtDriverApiLogStop();

    return ret;
    }

/**
 * Get Version field of KByte PW
 *
 * @param self The PW Module
 *
 * @return Version field value
 */
uint8 AtCienaKBytePwVersionGet(AtModulePw self)
    {
    return Tha6029PwKbyteTxVersionGet(mKbytePw(self));
    }

/**
 * Get KByte PW
 *
 * @param self The PW Module
 *
 * @return KByte PW if not NULL
 */
AtPw AtCienaKBytePwGet(AtModulePw self)
    {
    return ThaModulePwKBytePwGet((ThaModulePw)self);
    }

/**
 * Enable/disable extended K-Byte
 *
 * @param self Faceplate line
 * @param enable Enabling.
 *               - cAtTrue to enable extended K-Byte
 *               - cAtFalse to disable extended K-Byte
 *
 * @return AT return code
 * @note Only faceplate line can support extended K-Byte. Calling this API on
 *       other lines will have error code returned.
 */
eAtRet AtCienaSdhLineExtendedKByteEnable(AtSdhLine self, eBool enable)
    {
    mOneParamWrapCall(enable, eAtRet, ThaSdhLineExtendedKByteEnable((ThaSdhLine)self, enable));
    }

/**
 * Check if extended K-Byte is enabled or not.
 *
 * @param self Faceplate line
 *
 * @retval cAtTrue if extended K-Byte is enabled
 * @retval cAtFalse if extended K-Byte is disabled
 */
eBool AtCienaSdhLineExtendedKByteIsEnabled(AtSdhLine self)
    {
    return ThaSdhLineExtendedKByteIsEnabled((ThaSdhLine)self);
    }

/**
 * @}
 */

/*
 * Enable/disable K-Byte validation to transmit frame immediately upon newly
 * validated K bytes or Extended K-bytes
 *
 * @param self The K-Byte PW
 * @param channelId The local channel Id in K-Byte PW
 * @param enable Enabling
 *
 * @return AT return code
 */
eAtRet AtCienaKBytePwChannelValidationEnable(AtPw self, uint8 channelId, eBool enable)
    {
    mTwoParamsWrapCall(channelId, enable, eAtRet, AtPwKbyteChannelValidationEnable(KbyteChannelGet(self, channelId), enable));
    }

/*
 * Check if K-Byte validation is enabled
 *
 * @param self The K-Byte PW
 * @param channelId The local channel Id in K-Byte PW
 *
 * @retval cAtTrue if validation is enabled
 * @retval cAtFalse if validation if disabled
 */
eBool AtCienaKBytePwChannelValidationIsEnabled(AtPw self, uint8 channelId)
    {
    return AtPwKbyteChannelValidationIsEnabled(KbyteChannelGet(self, channelId));
    }

/*
 * Set Enable K-Byte Alarm-Suppression
 *
 * @param self The K-Byte PW
 * @param channelId The local channel Id in K-Byte PW
 * @param enable Enabling. When disable K-Byte Insertion not filter RDL-I
 *               and AIS-L with receiving from ETH pkt.
 *
 * @return AT return code
 */
eAtRet AtCienaKBytePwChannelSuppress(AtPw self, uint8 channelId, eBool enable)
    {
    mTwoParamsWrapCall(channelId, enable, eAtRet, AtPwKbyteChannelAlarmSuppressionEnable(KbyteChannelGet(self, channelId), enable));
    }

/*
 * Check if K-Byte Alarm-Suppression is enabled or not.
 *
 * @param self The K-Byte PW
 * @param channelId The local channel Id in K-Byte PW
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtCienaKBytePwChannelIsSuppressed(AtPw self, uint8 channelId)
    {
    return AtPwKbyteChannelAlarmSuppressionIsEnabled(KbyteChannelGet(self, channelId));
    }

/*
 * Enable/disable K-Byte overriding
 *
 * @param self The K-Byte PW
 * @param channelId The local channel Id in K-Byte PW
 * @param enable Enabling. If enable K-Byte on ETH frame will get from CPU
 *
 * @return AT return code
 * @see AtCienaKBytePwChannelOverrideK1Set()/AtCienaKBytePwChannelOverrideK2Set()
 */
eAtRet AtCienaKBytePwChannelOverride(AtPw self, uint8 channelId, eBool enable)
    {
    mTwoParamsWrapCall(channelId, enable, eAtRet, AtPwKbyteChannelOverrideEnable(KbyteChannelGet(self, channelId), enable));
    }

/*
 * Check if K-Byte Value is overridden.
 *
 * @param self The K-Byte PW
 * @param channelId The local channel Id in K-Byte PW
 *
 * @retval cAtTrue  if overriding is enabled
 * @retval cAtFalse if overriding is disabled
 */
eBool AtCienaKBytePwChannelIsOverridden(AtPw self, uint8 channelId)
    {
    return AtPwKbyteChannelOverrideIsEnabled(KbyteChannelGet(self, channelId));
    }

/*
 * Set K1 overridden value
 *
 * @param self The K-Byte PW
 * @param channelId The local channel Id in K-Byte PW
 * @param value K1 overridden value.
 *
 * @return AT return code
 */
eAtRet AtCienaKBytePwChannelOverrideK1Set(AtPw self, uint8 channelId, uint8 value)
    {
    mTwoParamsWrapCall(channelId, value, eAtRet, AtPwKbyteChannelTxK1Set(KbyteChannelGet(self, channelId), value));
    }

/*
 * Get K1 overridden value
 *
 * @param self The K-Byte PW
 * @param channelId The local channel Id in K-Byte PW
 *
 * @return K1 overridden value.
 */
uint8 AtCienaKBytePwChannelOverrideK1Get(AtPw self, uint8 channelId)
    {
    return AtPwKbyteChannelTxK1Get(KbyteChannelGet(self, channelId));
    }

/*
 * Set K2 overridden value
 *
 * @param self The K-Byte PW
 * @param channelId The local channel Id in K-Byte PW
 * @param value K2 overridden value
 *
 * @return AT return code
 */
eAtRet AtCienaKBytePwChannelOverrideK2Set(AtPw self, uint8 channelId, uint8 value)
    {
    mTwoParamsWrapCall(channelId, value, eAtRet, AtPwKbyteChannelTxK2Set(KbyteChannelGet(self, channelId), value));
    }

/*
 * Get K2 overridden value
 *
 * @param self The K-Byte PW
 * @param channelId The local channel Id in K-Byte PW
 *
 * @return K2 overridden value
 */
uint8 AtCienaKBytePwChannelOverrideK2Get(AtPw self, uint8 channelId)
    {
    return AtPwKbyteChannelTxK2Get(KbyteChannelGet(self, channelId));
    }

/*
 * Set EK1 overridden value
 *
 * @param self The K-Byte PW
 * @param channelId The local channel Id in K-Byte PW
 * @param value EK1 overridden value
 *
 * @return AT return code
 */
eAtRet AtCienaKBytePwChannelOverrideEK1Set(AtPw self, uint8 channelId, uint8 value)
    {
    mTwoParamsWrapCall(channelId, value, eAtRet, AtPwKbyteChannelTxEK1Set(KbyteChannelGet(self, channelId), value));
    }

/*
 * Get EK1 overridden value
 *
 * @param self The K-Byte PW
 * @param channelId The local channel Id in K-Byte PW
 *
 * @return EK1 overridden value
 */
uint8 AtCienaKBytePwChannelOverrideEK1Get(AtPw self, uint8 channelId)
    {
    return AtPwKbyteChannelTxEK1Get(KbyteChannelGet(self, channelId));
    }

/*
 * Set EK2 overridden value
 *
 * @param self The K-Byte PW
 * @param channelId The local channel Id in K-Byte PW
 * @param value EK2 overridden value
 *
 * @return AT return code
 */
eAtRet AtCienaKBytePwChannelOverrideEK2Set(AtPw self, uint8 channelId, uint8 value)
    {
    mTwoParamsWrapCall(channelId, value, eAtRet, AtPwKbyteChannelTxEK2Set(KbyteChannelGet(self, channelId), value));
    }

/*
 * Get EK2 overridden value
 *
 * @param self The K-Byte PW
 * @param channelId The local channel Id in K-Byte PW
 *
 * @return EK2 overridden value
 */
uint8 AtCienaKBytePwChannelOverrideEK2Get(AtPw self, uint8 channelId)
    {
    return AtPwKbyteChannelTxEK2Get(KbyteChannelGet(self, channelId));
    }

/**
 * @addtogroup AtCiena
 * @{
 */

/**
 * Set Watchdog timer
 *
 * @param self The K-Byte PW
 * @param timerInUs timer in microseconds
 *
 * @return AT return code
 */
eAtRet AtCienaKBytePwWatchdogTimerSet(AtPw self, uint32 timerInUs)
    {
    mOneParamWrapCall(timerInUs, eAtRet, Tha6029PwKbyteWatchdogSet(self, timerInUs));
    }

/**
 * Get Watch-dog timer
 *
 * @param self The K-Byte PW
 *
 * @return timer in microseconds
 */
uint32 AtCienaKBytePwWatchdogTimerGet(AtPw self)
    {
    return Tha6029PwKbyteWatchdogGet(self);
    }

/**
 * Set transmit frame at refresh interval
 *
 * @param self The K-Byte PW
 * @param timerInUs Transmit frame at refresh interval.
 *                  - 125us to 8ms
 *                  - 0: infinite (disabled)
 *
 * @return AT return code
 */
eAtRet AtCienaKBytePwIntervalSet(AtPw self, uint32 timerInUs)
    {
    mOneParamWrapCall(timerInUs, eAtRet, Tha6029PwKbyteIntervalSet(self, timerInUs));
    }

/**
 * Get refresh interval frames are transmitted
 *
 * @param self The K-Byte PW
 *
 * @return Refresh interval in microseconds
 */
uint32 AtCienaKBytePwIntervalGet(AtPw self)
    {
    return Tha6029PwKbyteIntervalGet(self);
    }

/**
 * Trigger to send a K-Byte/APS ETH packet
 *
 * @param self The K-Byte PW
 *
 * @return AT return code
 */
eAtRet AtCienaKBytePwTrigger(AtPw self)
    {
    mNoParamWrapCall(eAtRet, Tha6029PwKbyteTrigger(self));
    }

/**
 * Set position where Extended K-Byte is placed.
 *
 * @param self Faceplate line
 * @param position @ref eAtCienaSdhLineExtendedKBytePosition "Extended K-Byte position"
 *
 * @return AT return code
 */
eAtRet AtCienaSdhLineExtendedKBytePositionSet(AtSdhLine self, eAtCienaSdhLineExtendedKBytePosition position)
    {
    mOneParamWrapCall(position, eAtRet, ThaSdhLineExtendedKBytePositionSet((ThaSdhLine)self, SdhLineExtendedKBytePositionApp2Internal(position)));
    }

/**
 * Get Position where Extended K-Byte is placed
 *
 * @param self Faceplate line
 *
 * @return @ref eAtCienaSdhLineExtendedKBytePosition "Extended K-Byte position"
 */
eAtCienaSdhLineExtendedKBytePosition AtCienaSdhLineExtendedKBytePositionGet(AtSdhLine self)
    {
    return SdhLineExtendedKBytePositionInternal2App(ThaSdhLineExtendedKBytePositionGet((ThaSdhLine)self));
    }

/**
 * @}
 */

/*
 * Set expected value of the upper 12 bits in the received channel ID of the APS relay message.
 * Mismatches are not accepted and flag an interrupt.
 *
 * @param self The K-Byte PW
 * @param channelId The local channel Id in K-Byte PW
 * @param label the 12Bits value of channel Id[15:4]
 *
 * @return AT return code
 */
eAtRet AtCienaKBytePwChannelExpectedLabelSet(AtPw self, uint8 channelId, uint16 label)
	{
    mTwoParamsWrapCall(channelId, label, eAtRet, KBytePwChannelExpectedLabelSet(self, channelId, label));
	}

/*
 * Get expected value of the upper 12 bits in the received channel ID of the APS relay message.
 *
 * @param self The K-Byte PW
 * @param channelId The local channel Id in K-Byte PW
 *
 * @return the 12Bits value of channel Id[15:4]
 */
uint16 AtCienaKBytePwChannelExpectedLabelGet(AtPw self, uint8 channelId)
	{
	return AtPwKbyteChannelExpectedLabelGet(KbyteChannelGet(self, channelId));
	}

/*
 * Set the value of the upper 12 bits for transmit the APS relay message.
 *
 * @param self The K-Byte PW
 * @param channelId The local channel Id in K-Byte PW
 * @param label the 12Bits value of channel Id[15:4]
 *
 * @return AT return code
 */
eAtRet AtCienaKBytePwChannelTxLabelSet(AtPw self, uint8 channelId, uint16 label)
	{
    mTwoParamsWrapCall(channelId, label, eAtRet, KBytePwChannelTxLabelSet(self, channelId, label));
	}

/*
 * Get the transmitted value of the upper 12 bits in the transmitting APS relay message.
 *
 * @param self The K-Byte PW
 * @param channelId The local channel Id in K-Byte PW
 *
 * @return the 12Bits value of channel Id[15:4]
 */
uint16 AtCienaKBytePwChannelTxLabelGet(AtPw self, uint8 channelId)
	{
	return AtPwKbyteChannelTxLabelGet(KbyteChannelGet(self, channelId));
	}
	
/**
 * @addtogroup AtCiena
 * @{
 */

/**
 * Set K-Byte PW Remote MAC Address
 *
 * @param self The K-Byte PW
 * @param mac The Remote MAC address
 *
 * @return AT return code
 */
eAtRet AtCienaKBytePwRemoteMacSet(AtPw self, uint8 *mac)
	{
    mMacAttributeWrapCall(mac, (uint32)AtPwEthSrcMacSet(self, mac));
	}

/**
 * Get K-Byte PW Remote MAC Address
 *
 * @param self The K-Byte PW
 * @param address The Remote MAC address
 *
 * @return AT return code
 */
eAtRet AtCienaKBytePwRemoteMacGet(AtPw self, uint8 *address)
	{
    return (uint32)AtPwEthSrcMacGet(self, address);
	}

/**
 * Set K-Byte PW Central MAC Address
 *
 * @param self The K-Byte PW
 * @param mac The Central MAC address
 *
 * @return AT return code
 */
eAtRet AtCienaKBytePwCentralMacSet(AtPw self, uint8 *mac)
	{
    mMacAttributeWrapCall(mac, (uint32)AtPwEthDestMacSet(self, mac));
	}

/**
 * Get K-Byte PW Central MAC Address
 *
 * @param self The K-Byte PW
 * @param address The Central MAC address
 *
 * @return AT return code
 */
eAtRet AtCienaKBytePwCentralMacGet(AtPw self, uint8 *address)
	{
    return (uint32)AtPwEthDestMacGet(self, address);
	}

/**
 * @}
 */


/*
 * Get K1 value in K-Byte PW
 *
 * @param self The K-Byte PW
 * @param channelId The local channel Id in K-Byte PW
 *
 * @return K1 value
 */
uint8 AtCienaKBytePwChannelRxK1Get(AtPw self, uint8 channelId)
	{
	return AtPwKbyteChannelRxK1Get(KbyteChannelGet(self, channelId));
	}

/*
 * Get K2 value in K-Byte PW
 *
 * @param self The K-Byte PW
 * @param channelId The local channel Id in K-Byte PW
 *
 * @return K2 value
 */
uint8 AtCienaKBytePwChannelRxK2Get(AtPw self, uint8 channelId)
	{
	return AtPwKbyteChannelRxK2Get(KbyteChannelGet(self, channelId));
	}

/*
 * Get Extended K1 value in K-Byte PW
 *
 * @param self The K-Byte PW
 * @param channelId The local channel Id in K-Byte PW
 *
 * @return Extended K1 value
 */
uint8 AtCienaKBytePwChannelRxEK1Get(AtPw self, uint8 channelId)
	{
	return AtPwKbyteChannelRxEK1Get(KbyteChannelGet(self, channelId));
	}

/*
 * Get Extended K2 value in K-Byte PW
 *
 * @param self The K-Byte PW
 * @param channelId The local channel Id in K-Byte PW
 *
 * @return Extended K2 value
 */
uint8 AtCienaKBytePwChannelRxEK2Get(AtPw self, uint8 channelId)
	{
	return AtPwKbyteChannelRxEK2Get(KbyteChannelGet(self, channelId));
	}

/**
 * @addtogroup AtCiena
 * @{
 */

/**
 * Set K-Byte Source
 *
 * @param self The SONET/SDH line
 * @param source @ref eAtCienaSdhLineKByteSource "K-Byte source"
 *
 * @return AT return code
 */
eAtRet AtCienaSdhLineKByteSourceSet(AtSdhLine self, eAtCienaSdhLineKByteSource source)
	{
    mOneParamWrapCall(source, eAtRet, ThaSdhLineKByteSourceSet((ThaSdhLine)self, KbyteSourceApp2Internal(source)));
	}

/**
 * Get K-Byte Source
 *
 * @param self The SONET/SDH line
 *
 * @return @ref eAtCienaSdhLineKByteSource "K-Byte source"
 */
eAtCienaSdhLineKByteSource AtCienaSdhLineKByteSourceGet(AtSdhLine self)
	{
	return KbyteSourceInternal2App(ThaSdhLineKByteSourceGet((ThaSdhLine)self));
	}

/**
 * Set transmitted EK1 when K-Byte source is selected from CPU
 *
 * @param self The SONET/SDH line
 * @param eK1  Value can be 0 or 1
 *
 * @return AT return code
 */
eAtRet AtCienaSdhLineTxEK1Set(AtSdhLine self, uint8 eK1)
	{
    mOneParamWrapCall(eK1, eAtRet, ThaSdhLineEK1TxSet((ThaSdhLine)self, eK1));
	}

/**
 * Get transmitted EK1 when K-Byte source is selected from CPU
 *
 * @param self The SONET/SDH line
 *
 * @return Value can be 0 or 1. cInvalidUnint8 is invalid value.
 */
uint8 AtCienaSdhLineTxEK1Get(AtSdhLine self)
	{
	return ThaSdhLineEK1TxGet((ThaSdhLine)self);
	}

/**
 * Set transmitted EK2 when K-Byte source is selected from CPU
 *
 * @param self The SONET/SDH line
 * @param eK2  Value can be 0 or 1
 *
 * @return AT return code
 */
eAtRet AtCienaSdhLineTxEK2Set(AtSdhLine self, uint8 eK2)
	{
    mOneParamWrapCall(eK2, eAtRet, ThaSdhLineEK2TxSet((ThaSdhLine)self, eK2));
	}

/**
 * Get transmitted EK2 when K-Byte source is selected from CPU
 *
 * @param self The SONET/SDH line
 *
 * @return Value can be 0 or 1. cInvalidUnint8 is invalid value.
 */
uint8 AtCienaSdhLineTxEK2Get(AtSdhLine self)
	{
	return ThaSdhLineEK2TxGet((ThaSdhLine)self);
	}

/**
 * Get received EK1
 *
 * @param self The SONET/SDH line
 *
 * @return Value can be 0 or 1. cInvalidUnint8 is invalid value.
 */
uint8 AtCienaSdhLineRxEK1Get(AtSdhLine self)
    {
    return ThaSdhLineEK1RxGet((ThaSdhLine)self);
    }

/**
 * Get received EK2
 *
 * @param self The SONET/SDH line
 *
 * @return Value can be 0 or 1. cInvalidUnint8 is invalid value.
 */
uint8 AtCienaSdhLineRxEK2Get(AtSdhLine self)
    {
    return ThaSdhLineEK2RxGet((ThaSdhLine)self);
    }

/**
 * To get number of channels K-Byte PW has. In general, number of channels will
 * be the same of number of faceplate lines.
 *
 * @param self KByte PW
 *
 * @return Number of channels.
 */
uint32 AtCienaKBytePwNumChannels(AtPw self)
    {
    return Tha60290021PwKbyteNumKbyteChannelsGet(self);
    }

/**
 * To get a K-byte channel from K-Byte PW.
 *
 * @param self KByte PW
 * @param channelId KByte channel ID
 *
 * @return K-byte channel.
 */
AtChannel AtCienaKbytePwChannelGet(AtPw self, uint8 channelId)
    {
    return (AtChannel)Tha60290021PwKbyteChannelGet(self, channelId);
    }

/**
 * @}
 */

/*
 * Get channel current defect status
 *
 * @param self This PW
 * @param channelId KByte channel ID
 *
 * @return @ref eAtCienaKbyteApsPwAlarmType "Current defect status"
 */
uint32 AtCienaKBytePwChannelDefectGet(AtPw self, uint8 channelId)
    {
    return AtChannelDefectGet((AtChannel)KbyteChannelGet(self, channelId));
    }

/*
 * Get channel defect interrupt change status
 *
 * @param self This PW
 * @param channelId KByte channel ID
 *
 * @return @ref eAtCienaKbyteApsPwAlarmType "Defect interrupt change status"
 */
uint32 AtCienaKBytePwChannelDefectInterruptGet(AtPw self, uint8 channelId)
    {
    return AtChannelDefectInterruptGet((AtChannel)KbyteChannelGet(self, channelId));
    }

/*
 * Clear channel defect interrupt change status
 *
 * @param self This PW
 * @param channelId KByte channel ID
 *
 * @return @ref eAtCienaKbyteApsPwAlarmType "Defect interrupt change status" before clearing
 */
uint32 AtCienaKBytePwChannelDefectInterruptClear(AtPw self, uint8 channelId)
    {
    return AtChannelDefectInterruptClear((AtChannel)KbyteChannelGet(self, channelId));
    }

/*
 * Set KByte channel interrupt mask
 *
 * @param self This PW
 * @param channelId KByte channel ID
 * @param defectMask @ref eAtCienaKbyteApsPwAlarmType "Defect mask".
 *                   Note, only cAtCienaKbyteApsPwAlarmTypeChannelMisMatch is
 *                   supported
 * @param enableMask @ref eAtCienaKbyteApsPwAlarmType "Enable mask".
 *
 * @return AT return code
 */
eAtRet AtCienaKBytePwChannelInterruptMaskSet(AtPw self, uint8 channelId, uint32 defectMask, uint32 enableMask)
    {
    mThreeParamsWrapCall(channelId, defectMask, enableMask, eAtRet, AtChannelInterruptMaskSet((AtChannel)KbyteChannelGet(self, channelId), defectMask, enableMask));
    }

/*
 * Get KByte channel interrupt mask
 *
 * @param self This PW
 * @param channelId KByte channel ID
 *
 * @return Interrupt mask
 */
uint32 AtCienaKBytePwChannelInterruptMaskGet(AtPw self, uint8 channelId)
    {
    return AtChannelInterruptMaskGet((AtChannel)KbyteChannelGet(self, channelId));
    }

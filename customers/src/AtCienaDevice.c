/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : AtCienaDeviceManage.c
 *
 * Created Date: Sep 21, 2016
 *
 * Description : Ciena device management
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtStd.h"
#include "atclib.h"
#include "AtCienaDevice.h"
#include "../../driver/src/implement/codechip/Tha60290021/common/Tha6029CommonDevice.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/**
 * @addtogroup AtCiena
 * @{
 */

/**
 * Get JTAG ID
 *
 * @param self This device
 *
 * @return JTAG ID
 */
uint32 AtCienaDeviceJtagId(AtDevice self)
	{
	return Tha6029CommonDeviceJtagId(self);
	}

/**
 * Get Device Version
 *
 * @param self
 *
 * @return VERSION
 */
uint32 AtCienaDeviceVersion(AtDevice self)
	{
	return Tha6029CommonDeviceVersion(self);
	}

/**
 * Get the build year of FPGA image
 *
 * @param self
 *
 * @return YEAR
 */
uint32 AtCienaDeviceFpgaYear(AtDevice self)
	{
	return Tha6029CommonDeviceFpgaYear(self);
	}

/**
 * Get the build week of FPGA image
 *
 * @param self
 *
 * @return WEEK
 */
uint32 AtCienaDeviceFpgaWeek(AtDevice self)
	{
	return Tha6029CommonDeviceFpgaWeek(self);
	}

/**
 * Get the build ID of FPGA image
 *
 * @param self
 *
 * @return BUILD ID
 */
uint32 AtCienaDeviceFpgaBuildId(AtDevice self)
	{
	return Tha6029CommonDeviceFpgaBuildId(self);
	}

/**
 * Get the build time stamp of FPGA image
 *
 * @param self
 * @param buffer buffer to contain the result
 * @param bufferLen buffer length
 *
 * @return String of time stamp
 */
char *AtCienaBuildTimeStamp(AtDevice self, char *buffer, uint32 bufferLen)
    {
    return Tha6029CommonBuildTimeStamp(self, buffer, bufferLen);
    }

/**
 * Get type of device ( ASIC or FPGA )
 *
 * @param self
 * @param buffer buffer to contain the result
 * @param bufferLen buffer length
 *
 * @return String
 */
char *AtCienaDeviceType(AtDevice self, char *buffer, uint32 bufferLen)
	{
	return Tha6029CommonDeviceType(self, buffer, bufferLen);
	}

/**
 * Get family of device ( CEM or DIM )
 *
 * @param self
 * @param buffer buffer to contain the result
 * @param bufferLen buffer length
 *
 * @return String
 */
char *AtCienaDeviceFamily(AtDevice self, char *buffer, uint32 bufferLen)
	{
	return Tha6029CommonDeviceFamily(self, buffer, bufferLen);
	}

/**
 * Get architecture of device ( MRO or PDH )
 *
 * @param self
 * @param buffer buffer to contain the result
 * @param bufferLen buffer length
 *
 * @return String
 */
char *AtCienaDeviceArch(AtDevice self, char *buffer, uint32 bufferLen)
	{
	return Tha6029CommonDeviceArch(self, buffer, bufferLen);
	}

/**
 * Get author of device ( Arrive )
 *
 * @param self
 * @param buffer buffer to contain the result
 * @param bufferLen buffer length
 *
 * @return String
 */
char *AtCienaDeviceAuthor(AtDevice self, char *buffer, uint32 bufferLen)
	{
	return Tha6029CommonDeviceAuthor(self, buffer, bufferLen);
	}

/**
 * Show Device informations
 *
 * @param self
 */
void AtCienaShowVersion(AtDevice self)
    {
    uint32 jtagId = AtCienaDeviceJtagId(self);
    char buffer[32];

    AtPrintc (cSevNormal, "JtagId           : 0x%x\n", jtagId);
    AtPrintc (cSevNormal, "   Jversion      : 0x%x\n", (uint32)((jtagId >> 28) & cBit3_0));
    AtPrintc (cSevNormal, "   Jdevice       : 0x%x\n", (uint32)((jtagId >> 12) & cBit15_0));
    AtPrintc (cSevNormal, "   Jvendor       : 0x%x\n", (uint32)(jtagId & cBit11_0));
    AtPrintc (cSevNormal, "Year             : %d\n", AtCienaDeviceFpgaYear(self));
    AtPrintc (cSevNormal, "Week             : %d\n", AtCienaDeviceFpgaWeek(self));
    AtPrintc (cSevNormal, "Build-Id         : 0x%x\n", AtCienaDeviceFpgaBuildId(self));
    AtPrintc (cSevNormal, "Meta-data version: 0x%x\n", AtCienaDeviceVersion(self));
    AtPrintc (cSevNormal, "Build Time       : %s\n", AtCienaBuildTimeStamp(self, buffer, sizeof(buffer)));
    AtPrintc (cSevNormal, "Device Type      : %s\n", AtCienaDeviceType(self, buffer, sizeof(buffer)));
    AtPrintc (cSevNormal, "Device Family    : %s\n", AtCienaDeviceFamily(self, buffer, sizeof(buffer)));
    AtPrintc (cSevNormal, "Device Arch      : %s\n", AtCienaDeviceArch(self, buffer, sizeof(buffer)));
    AtPrintc (cSevNormal, "Device Author    : %s\n", AtCienaDeviceAuthor(self, buffer, sizeof(buffer)));
    return;
    }

/**
 * @}
 */

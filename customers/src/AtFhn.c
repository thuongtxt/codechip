/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APP
 *
 * File        : AtFhn.c
 *
 * Created Date: Jan 13, 2015
 *
 * Description : To work with FHN VxWorks platform
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtHalFhnSpi.h"
#include "AtHalFhnE1.h"
#include "AtDriver.h"
#include "AtCliModule.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint32 m_baseAddress = 0x70000000;

/* For command processing */
static AtDriver m_driver = NULL;
static AtTextUI m_tinyTextUI  = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern int spi_read_stm1(uint8 addr,uint16 *buf);
extern int spi_write_stm1(uint8 addr,uint16 data);

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddressOfFpga(uint8 fpgaId)
    {
    return m_baseAddress;
    }

static AtHal CreateHal(uint32 baseAddress, uint32 productCode)
    {
    if (productCode == 0x60001031)
        return AtHalFhnSpiNew(spi_read_stm1, spi_write_stm1);
    if (productCode == 0x60031071)
        return AtHalFhnE1New(baseAddress);
    return NULL;
    }

static void DeviceSetup(AtDevice device, uint32 productCode)
    {
    uint8 fpga_i;

    for (fpga_i = 0; fpga_i < AtDeviceNumIpCoresGet(device); fpga_i++)
        {
        AtHal hal = CreateHal(BaseAddressOfFpga(fpga_i), productCode);

        if (hal == NULL)
            {
            AtPrintc(cSevCritical, "Cannot create HAL with product code %d\r\n", productCode);
            return;
            }

        AtIpCoreHalSet(AtDeviceIpCoreGet(device, 0), hal);
        }
    }

static AtDriver DriverInit(uint32 productCode)
    {
    AtDevice device;
    AtDriver driver;

    /* Create driver with one device */
    driver = AtDriverCreate(1, AtOsalSharedGet());
    if (!AtDriverProductCodeIsValid(AtDriverSharedDriverGet(), productCode))
        AtPrintc(cSevCritical, "ERROR: product code 0x%08x is invalid\r\n", productCode);

    device = AtDriverDeviceCreate(AtDriverSharedDriverGet(), productCode);
    if (device == NULL)
        return NULL;

    DeviceSetup(device, productCode);

    return driver;
    }

/* Save all HALs installed to this device */
static AtList AllDeviceHals(AtDevice device)
    {
    uint8 coreId;
    AtList hals;

    if (device == NULL)
        return NULL;

    hals = AtListCreate(0);
    for (coreId = 0; coreId < AtDeviceNumIpCoresGet(device); coreId++)
        {
        AtObject hal = (AtObject)AtIpCoreHalGet(AtDeviceIpCoreGet(device, coreId));
        if (!AtListContainsObject(hals, hal))
            AtListObjectAdd(hals, hal);
        }

    return hals;
    }

static eAtRet DeviceCleanUp(AtDevice device)
    {
    AtList hals;

    if (device == NULL)
        return cAtOk;

    /* Save all HALs that are installed so far */
    hals = AllDeviceHals(device);
    AtDriverDeviceDelete(AtDriverSharedDriverGet(), device);

    /* Delete all HALs */
    while (AtListLengthGet(hals) > 0)
        AtHalDelete((AtHal)AtListObjectRemoveAtIndex(hals, 0));
    AtObjectDelete((AtObject)hals);

    return cAtOk;
    }

eAtRet DriverCleanup(void)
    {
    uint8 numDevices, i;

    if (m_driver == NULL)
        return cAtOk;

    /* Delete all devices */
    AtDriverAddedDevicesGet(m_driver, &numDevices);
    for (i = 0; i < numDevices; i++)
        {
        AtDevice device = AtDriverDeviceGet(m_driver, 0);
        DeviceCleanUp(device);
        }

    /* Delete driver then delete all saved HALs */
    AtDriverDelete(m_driver);
    m_driver = NULL;

    return cAtOk;
    }

static void CliStart(void)
    {
    extern int AtCliStartWithTextUI(AtTextUI textUI);
    m_tinyTextUI  = AtDefaultTinyTextUINew();
    AtCliStartWithTextUI(m_tinyTextUI);
    }

static atbool CliCleanup(void)
    {
    AtTextUIDelete(m_tinyTextUI);
    return cAtTrue;
    }

void atsdk(uint32 productCode, eBool cleanupDriverOnExit)
    {
    if (m_driver == NULL)
        m_driver = DriverInit(productCode);

    if (m_driver == NULL)
        {
        AtPrintc(cSevCritical, "Cannot initialize driver\r\n");
        return;
        }

    CliStart();

    /* Cleanup */
    if (cleanupDriverOnExit)
        DriverCleanup();
    CliCleanup();
    }

void BaseAddressSet(uint32 baseAddress)
    {
    m_baseAddress = baseAddress;
    }

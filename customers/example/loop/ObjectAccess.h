/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Application
 * 
 * File        : ObjectAccess.h
 * 
 * Created Date: Jun 17, 2013
 *
 * Description : Object accessing for LOOP project
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATLOOPOBJECTACCESS_H_
#define _ATLOOPOBJECTACCESS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDevice.h"
#include "AtModuleEth.h"
#include "AtModulePdh.h"
#include "AtModulePw.h"
#include "AtPw.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Access devices */
AtDevice AtLoopDeviceGet(void);

/* Access modules */
AtModuleEth AtLoopEthModule(void);
AtModulePdh AtLoopPdhModule(void);
AtModulePw  AtLoopPwModule(void);

/* Access channels */
AtEthPort AtLoopEthPortGet(uint32 portId);
AtPdhDe1 AtLoopDe1Get(uint32 de1Id);
AtPw AtLoopPwGet(uint32 pwId);

/* Create channels */
AtPwSAToP AtLoopPwSatopCreate(uint32 pwId);
AtPwCESoP AtLoopPwCesopCreate(uint32 pwId, eAtPwCESoPMode mode);

/* Delete channels */
eAtRet AtLoopPwDelete(uint32 pwId);

#endif /* _ATLOOPOBJECTACCESS_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : ObjectAccess.c
 *
 * Created Date: Jun 17, 2013
 *
 * Description : Object accessing example
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "ObjectAccess.h"
#include "AtDriver.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(void)
    {
    return AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
    }

AtModuleEth AtLoopEthModule(void)
    {
    return (AtModuleEth)AtDeviceModuleGet(Device(), cAtModuleEth);
    }

AtModulePdh AtLoopPdhModule(void)
    {
    return (AtModulePdh)AtDeviceModuleGet(Device(), cAtModulePdh);
    }

AtModulePw AtLoopPwModule(void)
    {
    return (AtModulePw)AtDeviceModuleGet(Device(), cAtModulePw);
    }

AtEthPort AtLoopEthPortGet(uint32 portId)
    {
    return AtModuleEthPortGet(AtLoopEthModule(), (uint8)(portId - 1));
    }

AtPdhDe1 AtLoopDe1Get(uint32 de1Id)
    {
    return AtModulePdhDe1Get(AtLoopPdhModule(), de1Id - 1);
    }

/* Note: if want to get a PW, need to create it first */
AtPw AtLoopPwGet(uint32 pwId)
    {
    return AtModulePwGetPw(AtLoopPwModule(), pwId - 1);
    }

AtPwSAToP AtLoopPwSatopCreate(uint32 pwId)
    {
    return AtModulePwSAToPCreate(AtLoopPwModule(), pwId - 1);
    }

AtPwCESoP AtLoopPwCesopCreate(uint32 pwId, eAtPwCESoPMode mode)
    {
    return AtModulePwCESoPCreate(AtLoopPwModule(), pwId - 1, mode);
    }

eAtRet AtLoopPwDelete(uint32 pwId)
    {
    return AtModulePwDeletePw(AtLoopPwModule(), pwId - 1);
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      :
 *
 * File        : datapath.c
 *
 * Created Date: March 14, 2014
 *
 * Description : Sample source code to bring up driver. For the sake of simplicity,
 *               and make it be easy to understand, this sample code is to
 *               handle driver with one device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ObjectAccess.h"
#include "AtPwPsn.h"
#include "AtPdhChannel.h"
#include "AtPdhDe1.h"

/*--------------------------- Define -----------------------------------------*/
#define cMacLengthInByte        6
#define cDefaultEthernetPortId  1

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
eAtRet ConfigurePsnForPw(AtPw pw);
eAtRet CommonConfigure(AtPdhDe1 de1, AtPw pw, AtEthPort port);
eAtRet CreateSatopDatapath(uint8 liuPortId, uint16 pwId);
eAtRet CreateCesopDatapath(uint8 liuPortId, uint16 pwId);
eAtRet EthernetPortLoopback(AtEthPort port, eAtLoopbackMode mode);

/*--------------------------- Implementation ---------------------------------*/
eAtRet ConfigurePsnForPw(AtPw pw)
    {
    eAtRet ret;
    tAtPwMplsLabel mplsLabel;
    AtPwMplsPsn pwPsn;

    /* Create MPLS PSN */
    pwPsn = AtPwMplsPsnNew();

    /* Build a default label and set it to PW PSN object*/
    AtPwMplsLabelMake(1, 1, 1, &mplsLabel);
    ret = AtPwMplsPsnInnerLabelSet(pwPsn, &mplsLabel);
    if (ret != cAtOk)
        return ret;

    ret = AtPwMplsPsnExpectedLabelSet(pwPsn, 1);
    if (ret != cAtOk)
        return ret;

    /* Set PW PSN object to PW */
    ret = AtPwPsnSet(pw, (AtPwPsn)pwPsn);
    if (ret != cAtOk)
        return ret;

    return cAtOk;
    }

eAtRet CommonConfigure(AtPdhDe1 de1, AtPw pw, AtEthPort port)
    {
    eAtRet ret;
    uint8 i;
    uint8 mac[cMacLengthInByte];

    /* Set timing, some types of timing can be only set after binding PW */
    ret = AtChannelTimingSet((AtChannel)de1, cAtTimingModeAcr, (AtChannel)pw);
    if (ret != cAtOk)
        return ret;

    /* Set Ethernet port to PW */
    ret = AtPwEthPortSet(pw, port);
    if (ret != cAtOk)
        return ret;

    /* Configure PSN */
    ret = ConfigurePsnForPw(pw);
    if (ret != cAtOk)
        return ret;

    /* Build MAC address */
    for (i = 0; i < cMacLengthInByte; i++)
        mac[i] = i;

    /* Set SMAC 0.1.2.3.4.5 for Ethernet port */
    ret = AtEthPortSourceMacAddressSet(port, (void*)mac);
    if (ret != cAtOk)
        return ret;

    /* Enable PW */
    ret = AtChannelEnable((AtChannel)pw, cAtTrue);
    if (ret != cAtOk)
        return ret;

    /* Enable reordering */
    ret = AtPwReorderingEnable(pw, cAtTrue);
    if (ret != cAtOk)
        return ret;

    return cAtOk;
    }

eAtRet CreateSatopDatapath(uint8 liuPortId, uint16 pwId)
    {
    eAtRet ret;
    AtPdhDe1 de1;
    AtPw pw;
    AtEthPort port;

    /* Get object E1 */
    de1 = AtLoopDe1Get(liuPortId);

    /* Get object PW with ID is 1 */
    pw = (AtPw)AtLoopPwSatopCreate(pwId);
    if (pw == NULL)
        return cAtErrorNullPointer;

    /* Get object Ethernet port */
    port = AtLoopEthPortGet(cDefaultEthernetPortId);

    /* Set frame type */
    ret = AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhE1Frm);
    if (ret != cAtOk)
        return ret;

    /* Bind this PW to E1 */
    ret = AtPwCircuitBind(pw, (AtChannel)de1);
    if (ret != cAtOk)
        return ret;

    ret = CommonConfigure(de1, pw, port);
    if (ret != cAtOk)
        return ret;

    return cAtOk;
    }

eAtRet CreateCesopDatapath(uint8 liuPortId, uint16 pwId)
    {
    eAtRet ret;
    AtPdhDe1 de1;
    AtPdhNxDS0 nxDs0;
    AtPw pw;
    AtEthPort port;

    /* Get object E1 */
    de1 = AtLoopDe1Get(liuPortId);

    /* Get object PW with ID is 1 */
    pw = (AtPw)AtLoopPwCesopCreate(pwId, cAtPwCESoPModeBasic);
    if (pw == NULL)
        return cAtErrorNullPointer;

    /* Get object Ethernet port */
    port = AtLoopEthPortGet(cDefaultEthernetPortId);

    /* Set frame type */
    ret = AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhE1Frm);
    if (ret != cAtOk)
        return ret;

    /* Create a NxDS0 with 31 timeslot from 1 to 31. Timeslot 0 is not carried in PW CESoP */
    nxDs0 = AtPdhDe1NxDs0Create(de1, 0xFFFFFFFE);

    /* Bind this PW to NxDS0 */
    ret = AtPwCircuitBind(pw, (AtChannel)nxDs0);
    if (ret != cAtOk)
        return ret;

    ret = CommonConfigure(de1, pw, port);
    if (ret != cAtOk)
        return ret;

    return cAtOk;
    }

eAtRet EthernetPortLoopback(AtEthPort port, eAtLoopbackMode mode)
    {
    return AtChannelLoopbackSet((AtChannel)port, mode);
    }

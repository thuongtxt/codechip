/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APP
 *
 * File        : sample_code.c
 *
 * Created Date: March 14, 2014
 *
 * Description : Driver management for Loop project
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtDriver.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef int (*FpgaRead) (unsigned long address, unsigned long *value);
typedef int (*FpgaWrite)(unsigned long address, unsigned long value);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
extern int arrive_e1_fpga_read(unsigned long address, unsigned long *value);
extern int arrive_e1_fpga_write(unsigned long address, unsigned long value);
extern AtHal AtHalLoopNew(FpgaRead readFunc, FpgaWrite writeFunc);

/*--------------------------- Implementation ---------------------------------*/
static AtDevice AddedDevice(void)
    {
    uint8 numAddedDevices;
    AtDevice *addedDevices = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), &numAddedDevices);
    return (numAddedDevices == 0) ? NULL : addedDevices[0];
    }

static AtHal CreateHal(void)
    {
    return AtHalLoopNew(arrive_e1_fpga_read, arrive_e1_fpga_write);
    }

static uint32 ReadProductCode(void)
    {
    uint32 productCode;
    AtHal hal = CreateHal();
    productCode = AtProductCodeGetByHal(hal);
    AtHalDelete(hal);

    if (!AtDriverProductCodeIsValid(AtDriverSharedDriverGet(), productCode))
        AtPrintc(cSevCritical, "ERROR: product code 0x%08x is invalid\r\n", productCode);

    return productCode;
    }

/* To setup HALs for all of IP Cores */
static void DeviceSetup(AtDevice device)
    {
    uint8 fpga_i;

    for (fpga_i = 0; fpga_i < AtDeviceNumIpCoresGet(device); fpga_i++)
        AtIpCoreHalSet(AtDeviceIpCoreGet(AddedDevice(), 0), CreateHal());
    }

static eAtRet DriverInit(void)
    {
    /* Create driver with one device */
    AtDriverCreate(1, AtOsalLinux());
    AtDriverDeviceCreate(AtDriverSharedDriverGet(), ReadProductCode());
    if (AddedDevice() == NULL)
        return cAtErrorRsrcNoAvail;

    /* And setup it */
    DeviceSetup(AddedDevice());

    /* Optional: initialize device */
    return AtDeviceInit(AddedDevice());
    }

/* Save all HALs installed to this device */
static AtList AllDeviceHals(AtDevice device)
    {
    uint8 coreId;
    AtList hals;

    if (device == NULL)
        return NULL;

    hals = AtListCreate(0);
    for (coreId = 0; coreId < AtDeviceNumIpCoresGet(device); coreId++)
        {
        AtObject hal = (AtObject)AtIpCoreHalGet(AtDeviceIpCoreGet(device, coreId));
        if (!AtListContainsObject(hals, hal))
            AtListObjectAdd(hals, hal);
        }

    return hals;
    }

/*
 * Sample device clean up function.
 *
 * Application create HALs and install them to AtDevice, so application must be
 * responsible for deleting these HALs. But, during device deleting, driver may
 * use HAL objects for special purposes, so HAL should be deleted after device
 * is deleted.
 *
 * The following sample code will backup all HALs that installed to device,
 * after deleting device, all of them are deleted
 */
static eAtRet DeviceCleanUp(AtDevice device)
    {
    AtList hals;

    if (device == NULL)
        return cAtOk;

    /* Save all HALs that are installed so far */
    hals = AllDeviceHals(device);
    AtDriverDeviceDelete(AtDriverSharedDriverGet(), device);

    /* Delete all HALs */
    while (AtListLengthGet(hals) > 0)
        AtHalDelete((AtHal)AtListObjectRemoveAtIndex(hals, 0));
    AtObjectDelete((AtObject)hals);

    return cAtOk;
    }

static eAtRet DriverCleanup(AtDriver driver)
    {
    uint8 numDevices, i;

    if (driver == NULL)
        return cAtOk;

    /* Delete all devices */
    AtDriverAddedDevicesGet(driver, &numDevices);
    for (i = 0; i < numDevices; i++)
        {
        AtDevice device = AtDriverDeviceGet(driver, 0);
        DeviceCleanUp(device);
        }

    /* Delete driver then delete all saved HALs */
    AtDriverDelete(driver);

    return cAtOk;
    }

int main(int argc, char* argv[])
    {
    eAtRet ret;

    AtUnused(argc);
    AtUnused(argv);

    /* Initialize driver */
    ret = DriverInit();
    if (ret != cAtOk)
        AtPrintc(cSevCritical, "Cannot initialize driver, ret = %s\r\n", AtRet2String(ret));

    /* Configure data-path here. See datapath.c for detail */
    /*CreateCesopDatapath(uint8 liuPortId, uint16 pwId)*/
    /*CreateSatopDatapath(uint8 liuPortId, uint16 pwId)*/

    /* Cleanup driver when stop using it */
    DriverCleanup(AtDriverSharedDriverGet());

    return 0;
    }

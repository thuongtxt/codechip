/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Customer
 * 
 * File        : AtCiscoVersion.h
 * 
 * Created Date: May 24, 2016
 *
 * Description : To control
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCISCOVERSION_H_
#define _ATCISCOVERSION_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Product code */
uint32 AtCiscoDs1CardProductCode(void);
uint32 AtCiscoDs3CardProductCode(void);
uint32 AtCiscoOcnCardProductCode(void);
uint32 AtCiscoOcn5GMsCardProductCode(void);

/* Hardware version, format:
 * - bit[31:24]: Year
 * - bit[23:16]: Month
 * - bit[15:8] : Day
 * - bit[7:4]  : Major version
 * - bit[3:0]  : Minor version
 * Example: 0x16052470: Firmware version 7.0 which is released on May-24-2016.
 */
uint32 AtCiscoDs1CardVersionNumber(void);
uint32 AtCiscoDs3CardVersionNumber(void);
uint32 AtCiscoOcnCardVersionNumber(void);
uint32 AtCiscoOcn5GMsCardVersionNumber(void);

/* Built number: just a flat number which can be reset to 0 for every new
 * firmware version */
uint32 AtCiscoDs1CardBuiltNumber(void);
uint32 AtCiscoDs3CardBuiltNumber(void);
uint32 AtCiscoOcnCardBuiltNumber(void);
uint32 AtCiscoOcn5GMsCardBuiltNumber(void);

#ifdef __cplusplus
}
#endif
#endif /* _ATCISCOVERSION_H_ */


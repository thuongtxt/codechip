There is an issue that makes router silently reboot because of PCIe operations on 
the standby RSP. The SDK when runs on the standby, it needs to know:
- Product code to create correct device instance
- Hardware version to handle new features and also support old firmware version 
  correctly.

To do so, the SDK will issue HA reads on standby. For each HA read operation, it 
uses 7 PCIe transactions.

To comletely remove the PCIe read on standby, application needs to tell the SDK 
know about the product code along with hardware version and this module is for 
this purpose.

The SDK itself will not depend on this library, but application will. There are 
some files:
- include/AtCiscoVersion.h: application will include this header file.
- src/*.c: application will compile these files as they are not part of the SDK.

For each card, there are 3 APIs to:

- Return product code
  + AtCiscoDs1CardProductCode()
  + AtCiscoDs3CardProductCode()
  + AtCiscoOcnCardProductCode()
  
- Return device version number:
  + AtCiscoDs1CardVersionNumber()
  + AtCiscoDs3CardVersionNumber()
  + AtCiscoOcnCardVersionNumber()

- Return device internal built number:
  + AtCiscoDs1CardBuiltNumber()
  + AtCiscoDs3CardBuiltNumber()
  + AtCiscoOcnCardBuiltNumber()

Whenever there is a new firmware is released, corresponding .c file will be updated 
to define corresponding hardware version.

The hardware version is managed by the SDK as following:
- Hardware version number:
  + bit[31:24]: Year
  + bit[23:16]: Month
  + bit[15:8] : Day
  + bit[7:4]  : Major version
  + bit[3:0]  : Minor version
  Example: 0x16052470: Firmware version 7.0 which is released on May-24-2016.
- Hardware internal built number: this is only used for LAB testing firmware and 
  it is just a flat number increased everytime new firmware is synthesis.
  
The SDK also supports new APIs at AtDevice class to specify the hardware version:
- AtDeviceVersionNumberSet(): to specify hardware version number
- AtDeviceBuiltNumberSet(): to specify hardware internal built number

So, let's have one example to setup the DS1 card on the standby RSP:

#include "AtCiscoVersion.h"

/* Create driver object as usually */
...

/* Create device with product code which is not read from hardware */
uint32 productCode = AtCiscoDs1CardProductCode();
AtDevice device = AtDriverDeviceCreate(AtDriverSharedDriverGet(), productCode);

/* Tell it hardware version */
AtDeviceVersionNumberSet(device, AtCiscoDs1CardVersionNumber());
AtDeviceBuiltNumberSet(device, AtCiscoDs1CardBuiltNumber());

/* Other things would remain the same including AtDeviceInit() */
...

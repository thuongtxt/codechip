/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : AtCiscoDs3CardVersion.c
 *
 * Created Date: May 24, 2016
 *
 * Description : DS3 card version
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCiscoCardVersionInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
uint32 AtCiscoDs3CardProductCode(void)
    {
    return 0x60210031;
    }

uint32 AtCiscoDs3CardVersionNumber(void)
    {
    return mVersionNumberBuild(0x17, 0x01, 0x05, 0x4, 0x6);
    }

uint32 AtCiscoDs3CardBuiltNumber(void)
    {
    return 0x4632;
    }

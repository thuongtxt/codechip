/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : AtCisco3GMsCardVersion.c
 *
 * Created Date: Sep 7, 2016
 *
 * Description : OCN 3G MS card version
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCiscoCardVersionInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
uint32 AtCiscoOcn3GMsCardProductCode(void)
    {
    return 0x60210061;
    }

uint32 AtCiscoOcn3GMsCardVersionNumber(void)
    {
    return mVersionNumberBuild(0x17, 0x03, 0x22, 0x2, 0x8);
    }

uint32 AtCiscoOcn3GMsCardBuiltNumber(void)
    {
    return 0x1043;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : AtCiscoDs1CardVersion.c
 *
 * Created Date: May 24, 2016
 *
 * Description : DS1 card version
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCiscoCardVersionInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
uint32 AtCiscoDs1CardProductCode(void)
    {
    return 0x60210021;
    }

uint32 AtCiscoDs1CardVersionNumber(void)
    {
    return mVersionNumberBuild(0x16, 0x12, 0x05, 0x4, 0x6);
    }

uint32 AtCiscoDs1CardBuiltNumber(void)
    {
    return 0x4631;
    }

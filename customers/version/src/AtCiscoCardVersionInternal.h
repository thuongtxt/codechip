/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Customer
 * 
 * File        : AtCiscoCardVersionInternal.h
 * 
 * Created Date: May 24, 2016
 *
 * Description : Common utils to control firmware version
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCISCOCARDVERSIONINTERNAL_H_
#define _ATCISCOCARDVERSIONINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCiscoVersion.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define cVersionMajorMask  cBit7_4
#define cVersionMajorShift 4
#define cVersionMinorMask  cBit3_0
#define cVersionMinorShift 0
#define cVersionDDMask     cBit15_8
#define cVersionDDShift    8
#define cVersionMMMask     cBit23_16
#define cVersionMMShift    16
#define cVersionYYMask     cBit31_24
#define cVersionYYShift    24

#define mVersionNumberBuild(year, month, day, major, minor)                    \
        ((((uint32)(year)  << cVersionYYShift)    & cVersionYYMask)    |     \
         (((uint32)(month) << cVersionMMShift)    & cVersionMMMask)    |     \
         (((uint32)(day)   << cVersionDDShift)    & cVersionDDMask)    |     \
         (((uint32)(major) << cVersionMajorShift) & cVersionMajorMask) |     \
         (((uint32)(minor) << cVersionMinorShift) & cVersionMinorMask))

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _ATCISCOCARDVERSIONINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : AtCiscoOcn5GMsCardVersion.c
 *
 * Created Date: Jun 6, 2016
 *
 * Description : OCN 5G MS card version
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCiscoCardVersionInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
uint32 AtCiscoOcn5GMsCardProductCode(void)
    {
    return 0x60210012;
    }

uint32 AtCiscoOcn5GMsCardVersionNumber(void)
    {
    return mVersionNumberBuild(0x17, 0x03, 0x30, 0x5, 0x9);
    }

uint32 AtCiscoOcn5GMsCardBuiltNumber(void)
    {
    return 0x1018;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Customer
 *
 * File        : AtCiscoOcnCardVersion.c
 *
 * Created Date: May 24, 2016
 *
 * Description : OCN card version
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCiscoCardVersionInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
uint32 AtCiscoOcnCardProductCode(void)
    {
    return 0x60210051;
    }

uint32 AtCiscoOcnCardVersionNumber(void)
    {
    return mVersionNumberBuild(0x17, 0x03, 0x20, 0x7, 0x4);
    }

uint32 AtCiscoOcnCardBuiltNumber(void)
    {
    return 0x1042;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : AtCienaKbyteDeprecated.h
 * 
 * Created Date: Jun 4, 2018
 *
 * Description : Deprecated APS/K-byte PW
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCIENAKBYTEDEPRECATED_H_
#define _ATCIENAKBYTEDEPRECATED_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/* Label which is in CHANNEL ID[15:4], now are deprecated by AtCienaKByteChannelTxLabelSet(), etc. @ref AtCienaKbyteChannel.h for more details.
 * the parameter 'channelId' shall be abstracted by AtChannel object, which is equal to AtChannelIdGet(). */
eAtRet AtCienaKBytePwChannelTxLabelSet(AtPw self, uint8 channelId, uint16 label);
uint16 AtCienaKBytePwChannelTxLabelGet(AtPw self, uint8 channelId);
eAtRet AtCienaKBytePwChannelExpectedLabelSet(AtPw self, uint8 channelId, uint16 label);
uint16 AtCienaKBytePwChannelExpectedLabelGet(AtPw self, uint8 channelId);

/* Validation, now are deprecated by AtCienaKbyteChannelValidationEnable()/AtCienaKbyteChannelValidationIsEnabled() */
eAtRet AtCienaKBytePwChannelValidationEnable(AtPw self, uint8 channelId, eBool enabled);
eBool AtCienaKBytePwChannelValidationIsEnabled(AtPw self, uint8 channelId);

/* AIS and RDI propagation suppression, now are deprecated by AtCienaKbyteChannelSuppression()/AtCienaKbyteChannelSuppressionIsEnabled() */
eAtRet AtCienaKBytePwChannelSuppress(AtPw self, uint8 channelId, eBool enable);
eBool AtCienaKBytePwChannelIsSuppressed(AtPw self, uint8 channelId);

/* Received K-Bytes on SGMII, now are deprecated by AtCienaKbyteChannelRxK1Get(), AtCienaKbyteChannelRxK2Get, etc. */
uint8 AtCienaKBytePwChannelRxK1Get(AtPw self, uint8 channelId);
uint8 AtCienaKBytePwChannelRxK2Get(AtPw self, uint8 channelId);
uint8 AtCienaKBytePwChannelRxEK1Get(AtPw self, uint8 channelId);
uint8 AtCienaKBytePwChannelRxEK2Get(AtPw self, uint8 channelId);

/* Channel alarm, deprecated by AtChannelDefectGet(), AtChannelDefectInterruptGet(),
 * AtChannelDefectInterruptClear(), AtChannelChannelInterruptMaskSet() and AtChannelInterruptMaskGet() respectively. */
uint32 AtCienaKBytePwChannelDefectGet(AtPw self, uint8 channelId);
uint32 AtCienaKBytePwChannelDefectInterruptGet(AtPw self, uint8 channelId);
uint32 AtCienaKBytePwChannelDefectInterruptClear(AtPw self, uint8 channelId);
eAtRet AtCienaKBytePwChannelInterruptMaskSet(AtPw self, uint8 channelId, uint32 defectMask, uint32 enableMask);
uint32 AtCienaKBytePwChannelInterruptMaskGet(AtPw self, uint8 channelId);

/* =============================================================================
 * Testing APIs
 * ============================================================================= */
/* Enable/disable K-Byte overriding, deprecated by AtCienaKbyteChannelOverrideEnable()/AtCienaKbyteChannelOverrideIsEnabled() */
eAtRet AtCienaKBytePwChannelOverride(AtPw self, uint8 channelId, eBool enable);
eBool AtCienaKBytePwChannelIsOverridden(AtPw self, uint8 channelId);

/* Overridden K1/EK1 value, deprecated by AtCienaKbyteChannelTxK1Set()/AtCienaKbyteChannelTxK1Get(), etc. */
eAtRet AtCienaKBytePwChannelOverrideK1Set(AtPw self, uint8 channelId, uint8 value);
uint8 AtCienaKBytePwChannelOverrideK1Get(AtPw self, uint8 channelId);
eAtRet AtCienaKBytePwChannelOverrideEK1Set(AtPw self, uint8 channelId, uint8 value);
uint8 AtCienaKBytePwChannelOverrideEK1Get(AtPw self, uint8 channelId);

/* Overridden K2/EK2 value, deprecated by AtCienaKbyteChannelTxK2Set()/AtCienaKbyteChannelTxK2Get(), etc. */
eAtRet AtCienaKBytePwChannelOverrideK2Set(AtPw self, uint8 channelId, uint8 value);
uint8 AtCienaKBytePwChannelOverrideK2Get(AtPw self, uint8 channelId);
eAtRet AtCienaKBytePwChannelOverrideEK2Set(AtPw self, uint8 channelId, uint8 value);
uint8 AtCienaKBytePwChannelOverrideEK2Get(AtPw self, uint8 channelId);


#ifdef __cplusplus
}
#endif
#endif /* _ATCIENAKBYTEDEPRECATED_H_ */


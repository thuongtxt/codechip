/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Customer
 * 
 * File        : AtCienaVlan.h
 * 
 * Created Date: Dec 31, 2016
 *
 * Description : VLAN APIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCIENAVLAN_H_
#define _ATCIENAVLAN_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* ETH pass-through sub port VLAN: two global TPIDs at TX/RX directions */
uint8 AtCienaModuleEthNumSubPortVlanTpids(AtModuleEth self);
eAtRet AtCienaModuleEthSubPortVlanTxTpidSet(AtModuleEth self, uint8 tpidIndex, uint16 tpid);
uint16 AtCienaModuleEthSubPortVlanTxTpidGet(AtModuleEth self, uint8 tpidIndex);
eAtRet AtCienaModuleEthSubPortVlanExpectedTpidSet(AtModuleEth self, uint8 tpidIndex, uint16 tpid);
uint16 AtCienaModuleEthSubPortVlanExpectedTpidGet(AtModuleEth self, uint8 tpidIndex);

/* Faceplate ETH Port sub port VLAN */
eAtRet AtCienaEthPortSubportTxVlanSet(AtEthPort self, const tAtVlan *vlan);
eAtRet AtCienaEthPortSubportTxVlanGet(AtEthPort self, tAtVlan *vlan);
eAtRet AtCienaEthPortSubportExpectedVlanSet(AtEthPort self, const tAtVlan *expectedVlan);
eAtRet AtCienaEthPortSubportExpectedVlanGet(AtEthPort self, tAtVlan *expectedVlan);

/* PW module sub port VLANs: two global VLANs at TX/RX directions */
eAtRet AtCienaModulePwTxSubportVlanSet(AtModulePw self, uint32 subPortVlanIndex, const tAtVlan *vlan);
eAtRet AtCienaModulePwTxSubportVlanGet(AtModulePw self, uint32 subPortVlanIndex, tAtVlan *vlan);
eAtRet AtCienaModulePwExpectedSubportVlanSet(AtModulePw self, uint32 subPortVlanIndex, const tAtVlan *expectedVlan);
eAtRet AtCienaModulePwExpectedSubportVlanGet(AtModulePw self, uint32 subPortVlanIndex, tAtVlan *expectedVlan);
uint32 AtCienaModulePwNumSubPortVlans(AtModulePw self);

/* Sub port VLAN per PW */
eAtRet AtCienaPwSubPortVlanIndexSet(AtPw self, uint32 subPortVlanIndex);
uint32 AtCienaPwSubPortVlanIndexGet(AtPw self);

#ifdef __cplusplus
}
#endif
#endif /* _ATCIENAVLAN_H_ */


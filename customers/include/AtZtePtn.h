/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APP
 * 
 * File        : AtZtePtn.h
 * 
 * Created Date: Jul 8, 2013
 *
 * Description : To work with ZTE PTN
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATZTEPTN_H_
#define _ATZTEPTN_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtModulePw.h"
#include "AtEthFlow.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
/* Clock checking status */
typedef enum eAtZtePtnClockStatus
    {
    cAtZtePtnClockStatusOk                  = 0,
    cAtZtePtnClockStatusSystemClockFail     = cBit0,
    cAtZtePtnClockStatusLocalBusClockFail   = cBit1,
    cAtZtePtnClockStatusGeClockFail         = cBit2,
    cAtZtePtnClockStatusOcnClock1Fail       = cBit3,
    cAtZtePtnClockStatusOcnClock2Fail       = cBit4,
    cAtZtePtnClockStatusDdr1ClockFail       = cBit5,
    cAtZtePtnClockStatusDdr2ClockFail       = cBit6,
    cAtZtePtnClockStatusDdr3ClockFail       = cBit7,
    cAtZtePtnClockStatusReference1ClockFail = cBit8,
    cAtZtePtnClockStatusReference2ClockFail = cBit9
    }eAtZtePtnClockStatus;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/*
 * @brief ZTE Tag 1 (S-VLAN tag)
 */
typedef struct tAtZtePtnTag1
    {
    uint8 priority;         /* [0..7] Vlan priority, can be used to in QoS. The field
                               indicates the frame priority level which can be used for
                               the priority of traffic. The field can represent
                               8 levels */
    uint8 cfi;              /* [0..1] Canonical Format Indicator. If the value of this
                               field is 1, the MAC address is in noncanonical format.
                               If the value is 0, the MAC address is in canonical format */
    uint8 cpuPktIndicator;  /* [0..1] CPU packet indicator */
    uint8 encapType;        /* [0..31] Encap type. Identifies the packet type of business
                               configuration set */
    uint8 packetLength;     /* [0..63] Packet Length. If this field is 0, indicating that
                               the message length is greater than 60 bytes (the length of
                               the DA, SA, L / T, Line Header, payload); Otherwise,
                               the actual length of the message padding field for packets
                               filled to fill */
    }tAtZtePtnTag1;

/*
 * @brief ZTE Tag 2 (C-VLAN tag)
 */
typedef struct tAtZtePtnTag2
    {
    uint8  priority;         /* [0..7] Vlan priority, can be used to in QoS. The field
                                indicates the frame priority level which can be used for
                                the priority of traffic. The field can represent
                                8 levels */
    uint8  cfi;              /* [0..1] Canonical Format Indicator. If the value of this
                                field is 1, the MAC address is in noncanonical format.
                                If the value is 0, the MAC address is in canonical format */
    uint8  stmPortId;        /* [0..7] STM port ID */
    eBool  isMlpppIma;       /* cAtTrue for MLPPP/IMA */
    uint16 zteChannelId;     /* [0..255] ZTE channel ID */
    }tAtZtePtnTag2;

/*
 * @brief C-HDLC Packet Type
 */
typedef enum eAtZtePtnCHdlcPktType
    {
    cAtZtePtnDataType = 0x0, /* Data Type */
    cAtZtePtnISISType = 0x4, /* IS-IS Type */
    cAtZtePtnOamType  = 0xF   /* IS-IS Type */
    }eAtZtePtnCHdlcPktType;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* FPGA status */
eAtZtePtnClockStatus AtZtePtnClockCheck(AtDevice self);
eBool AtZtePtnRamIsGood(AtDevice self);
eBool AtZtePtnRamTested(AtDevice self);
eBool AtZtePtnInterfaceIsGood(AtDevice self);
eBool AtZtePtnFpgaLogicIsGood(AtDevice self);

/* PW Ethernet header from TDM to ETH direction */
tAtZtePtnTag1 *AtZtePtnPwTag1Get(AtPw self, tAtZtePtnTag1 *tag1);
tAtZtePtnTag2 *AtZtePtnPwTag2Get(AtPw self, tAtZtePtnTag2 *tag2);
eAtRet AtZtePwPtnHeaderSet(AtPw self, uint8 *destMac, const tAtZtePtnTag1 *tag1, const tAtZtePtnTag2 *tag2);

/* PW Lookup */
eAtRet AtZtePtnPwExpectedTag1Set(AtPw self, tAtZtePtnTag1 *tag1);
tAtZtePtnTag1 *AtZtePtnPwExpectedTag1Get(AtPw self, tAtZtePtnTag1 *tag1);
eAtRet AtZtePtnPwExpectedTag2Set(AtPw self, tAtZtePtnTag2 *tag2);
tAtZtePtnTag2 *AtZtePtnPwExpectedTag2Get(AtPw self, tAtZtePtnTag2 *tag2);

/* ETH flow header */
eAtRet AtZteEthFlowTxHeaderSet(AtEthFlow self, uint8 portId, const tAtZtePtnTag1 *zteTag1, const tAtZtePtnTag2 *zteTag2);
eAtRet AtZteEthFlowExpectedHeaderSet(AtEthFlow self, uint8 portId, const tAtZtePtnTag1 *zteTag1, const tAtZtePtnTag2 *zteTag2);
tAtZtePtnTag1 *AtZtePtnEthFlowTag1Get(AtEthFlow self, tAtZtePtnTag1 *zteTag1);
tAtZtePtnTag2 *AtZtePtnEthFlowTag2Get(AtEthFlow self, tAtZtePtnTag2 *zteTag2);
tAtZtePtnTag1 *AtZtePtnEthFlowExpectedTag1Get(AtEthFlow self, tAtZtePtnTag1 *tag1);
tAtZtePtnTag2 *AtZtePtnEthFlowExpectedTag2Get(AtEthFlow self, tAtZtePtnTag2 *tag2);

/* PPP table */
uint16 AtZtePtnPppNumLookupEntries(AtDevice self);
eAtRet AtZtePtnPppLookupEntrySet(AtDevice self, uint16 lookupIndex, uint16 pidValue, uint16 ethType, uint8 zteEncapType);
eAtRet AtZtePtnPppLookupEntryEnable(AtDevice self, uint16 lookupIndex, eBool enable);
eBool  AtZtePtnPppLookupEntryIsEnabled(AtDevice self, uint16 lookupIndex);
uint16 AtZtePtnPppLookupEntryPidGet(AtDevice self, uint16 lookupIndex);
uint16 AtZtePtnPppLookupEntryEthTypeGet(AtDevice self, uint16 lookupIndex);
uint16 AtZtePtnPppLookupEntryEncapTypeGet(AtDevice self, uint16 lookupIndex);
eBool AtZtePtnPppLookupEntryHasEthType(AtDevice self, uint16 lookupIndex);

eAtRet AtZtePtnPppLookupEntryCfiSet(AtDevice self, uint16 lookupIndex, uint8 cfi);
uint8 AtZtePtnPppLookupEntryCfiGet(AtDevice self, uint16 lookupIndex);
eAtRet AtZtePtnPppLookupEntryPrioritySet(AtDevice self, uint16 lookupIndex, uint8 priority);
uint8 AtZtePtnPppLookupEntryPriorityGet(AtDevice self, uint16 lookupIndex);

/* Mlppp lookup */
eAtRet AtZtePtnMlpppLookupEntryEncapTypeSet(AtDevice self, uint16 lookupIndex, uint8 zteEncapType);
uint16 AtZtePtnMlpppLookupEntryEncapTypeGet(AtDevice self, uint16 lookupIndex);

/* DS1/E1 specific APIs */
eAtRet AtZtePtnPdhDe1AutoTxAisEnable(AtPdhDe1 de1, eBool enable);
eBool AtZtePtnPdhDe1AutoTxAisIsEnabled(AtPdhDe1 de1);

/* C-Hdlc */
eAtRet AtZtePtnCHdlcLookupEntryEncapTypeSet(AtDevice self, uint8 zteEncapType);
uint16 AtZtePtnCHdlcLookupEntryEncapTypeGet(AtDevice self);
eAtRet AtZtePtnCHdlcLookupEntryCfiSet(AtDevice self, eAtZtePtnCHdlcPktType type, uint8 cfi);
uint8 AtZtePtnCHdlcLookupEntryCfiGet(AtDevice self, eAtZtePtnCHdlcPktType type);
eAtRet AtZtePtnCHdlcLookupEntryPrioritySet(AtDevice self, eAtZtePtnCHdlcPktType type, uint8 priority);
uint8 AtZtePtnCHdlcLookupEntryPriorityGet(AtDevice self, eAtZtePtnCHdlcPktType type);

/* Frame-Relay */
eAtRet AtZtePtnFrLookupEntryEncapTypeSet(AtDevice self, uint8 zteEncapType);
uint8 AtZtePtnFrLookupEntryEncapTypeGet(AtDevice self);
eAtRet AtZtePtnFrLookupEntryPrioritySet(AtDevice self, uint8 priority);
uint8 AtZtePtnFrLookupEntryPriorityGet(AtDevice self);
eAtRet AtZtePtnFrLookupEntryCfiSet(AtDevice self, uint8 cfi);
uint8 AtZtePtnFrLookupEntryCfiGet(AtDevice self);

/* Switch ethernet port */
eAtRet AtZtePtnEthPortSwitch(AtEthPort self, AtEthPort toPort);

eAtRet AtZtePtnPwSendLbitPacketsToCdrEnable(AtModulePw self, eBool enable);
eBool AtZtePtnPwSendLbitPacketsToCdrIsEnabled(AtModulePw self);

/* Higig ethernet flow header */
eAtRet AtZtePtnEthFlowHigigTxHeaderSet(AtEthFlow self,
                                       uint8 ethPort,
                                       uint8 stmPortId,
                                       const tAtZtePtnTag1 *zteTag1,
                                       const tAtZtePtnTag2 *zteTag2);
uint8 AtZtePtnEthFlowHigigTxStmPortGet(AtEthFlow self);
eAtRet AtZtePtnEthFlowHigigExpectedHeaderSet(AtEthFlow self,
                                             uint8 ethPort,
                                             uint8 stmPortId,
                                             const tAtZtePtnTag1 *zteTag1,
                                             const tAtZtePtnTag2 *zteTag2);
uint8 AtZtePtnEthFlowHigigExpectedStmPortGet(AtEthFlow self);

#ifdef __cplusplus
}
#endif
#endif /* _ATZTEPTN_H_ */

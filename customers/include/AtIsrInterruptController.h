/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ISR
 * 
 * File        : AtIsrInterruptController.h
 * 
 * Created Date: Sep 12, 2017
 *
 * Description : To support functions that are ISR safe
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATISRINTERRUPTCONTROLLER_H_
#define _ATISRINTERRUPTCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtIsrInterruptController * AtIsrInterruptController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Create interrupt controller for specific product code */
AtIsrInterruptController AtIsrInterruptControllerCreate(AtHal hal, uint32 productCode);
void AtIsrInterruptControllerDelete(AtIsrInterruptController self);

/* Read-only */
uint32 AtIsrInterruptControllerProductCode(AtIsrInterruptController self);
AtHal AtIsrInterruptControllerHal(AtIsrInterruptController self);

/* Cores */
uint8 AtIsrInterruptControllerNumCores(AtIsrInterruptController self);
eAtRet AtIsrInterruptControllerCoreEnable(AtIsrInterruptController self, uint8 coreId, eBool enabled);
eBool AtIsrInterruptControllerCoreIsEnabled(AtIsrInterruptController self, uint8 coreId);

#ifdef __cplusplus
}
#endif
#endif /* _ATISRINTERRUPTCONTROLLER_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Fiber Logic
 * 
 * File        : AtFiberLogic.h
 * 
 * Created Date: May 29, 2015
 *
 * Description : Fiber Logic declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATFIBERLOGIC_H_
#define _ATFIBERLOGIC_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eAtFiberLogicLiuMode
    {
    cAtFiberLogicLiuModeE1,
    cAtFiberLogicLiuModeDs1
    }eAtFiberLogicLiuMode;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet AtFiberLogicLiuInit(eAtFiberLogicLiuMode liuMode);
eAtRet AtFiberLogicLiuLoopbackSet(uint16 liuId, eAtLoopbackMode loopMode);
uint32 AtFiberLogicLiuRead(uint16 liuId, uint32 address);
void AtFiberLogicLiuWrite(uint16 liuId, uint32 address, uint32 value);

#ifdef __cplusplus
}
#endif
#endif /* _ATFIBERLOGIC_H_ */


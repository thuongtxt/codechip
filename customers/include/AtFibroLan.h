/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : FIBROLAN
 * 
 * File        : AtFibroLan.h
 * 
 * Created Date: Feb 10, 2015
 *
 * Description : FIBROLAN specific APIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATFIBROLAN_H_
#define _ATFIBROLAN_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet AtFibroLanPrintFuncSet(int (*printFunc)(const char *aString));

#ifdef __cplusplus
}
#endif
#endif /* _ATFIBROLAN_H_ */


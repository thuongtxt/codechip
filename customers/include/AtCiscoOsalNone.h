/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Customer
 * 
 * File        : AtCiscoOsalNone.h
 * 
 * Created Date: Jul 10, 2016
 *
 * Description : OSAL for environment that does not have OS
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCISCOOSALNONE_H_
#define _ATCISCOOSALNONE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtOsal.h"

#ifndef AT_NONE_OS
#include <stdlib.h>
#include <string.h>
#endif

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
#ifdef AT_NONE_OS
extern void * malloc (unsigned int size);
extern void free (void *ptr);
extern void * memset (void *block, int c, unsigned int size);
extern void * memcpy (void *to, const void *from, unsigned int size);
extern int memcmp (const void *a1, const void *a2, unsigned int size);
extern void wastetime (long us);
extern unsigned long current_time_usec(void);
extern unsigned long long current_time_usec_64bit(void);
#endif

/*--------------------------- Entries ----------------------------------------*/
AtOsal AtCiscoOsalNone(void);
void AtCiscoOsalArriveMempoolCleanUp(AtOsal self);

#ifdef __cplusplus
}
#endif
#endif /* _ATCISCOOSALNONE_H_ */


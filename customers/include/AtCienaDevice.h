/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Customer
 * 
 * File        : AtCienaDeviceManage.h
 * 
 * Created Date: Sep 21, 2016
 *
 * Description : Ciena device management
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCIENADEVICE_H_
#define _ATCIENADEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDevice.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 AtCienaDeviceJtagId(AtDevice self);
uint32 AtCienaDeviceVersion(AtDevice self);
uint32 AtCienaDeviceFpgaYear(AtDevice self);
uint32 AtCienaDeviceFpgaWeek(AtDevice self);
uint32 AtCienaDeviceFpgaBuildId(AtDevice self);
char *AtCienaBuildTimeStamp(AtDevice self, char *buffer, uint32 bufferLen);
char *AtCienaDeviceType(AtDevice self, char *buffer, uint32 bufferLen);
char *AtCienaDeviceFamily(AtDevice self, char *buffer, uint32 bufferLen);
char *AtCienaDeviceArch(AtDevice self, char *buffer, uint32 bufferLen);
char *AtCienaDeviceAuthor(AtDevice self, char *buffer, uint32 bufferLen);

void AtCienaShowVersion(AtDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _ATCIENADEVICE_H_ */


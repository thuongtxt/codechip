/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : AtZteRouter.h
 * 
 * Created Date: Jul 8, 2014
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATZTEROUTER_H_
#define _ATZTEROUTER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtPw.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/*
 * @brief ZTE Tag
 */
typedef struct tAtZteRouterTag
    {
    uint8 cpuPktIndicator;  /* [0..1] CPU packet indicator */
    uint8 portNumber;       /* [0..15] Port number */
    uint8 encapType;        /* [0..15] Encap type. Identifies the packet type of business
                               configuration set */
    uint8 slotNumber;       /* [0..15] Slot number */
    uint16 channelNumber;   /* [0..1023] ZTE channel ID */
    uint8 packetLength;     /* [0..63] Packet Length. If this field is 0, indicating that
                               the message length is greater than 60 bytes (the length of
                               the DA, SA, L / T, Line Header, payload); Otherwise,
                               the actual length of the message padding field for packets
                               filled to fill */
    }tAtZteRouterTag;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet AtZteRouterPwExpectedTagSet(AtPw self, tAtZteRouterTag *tag);
eAtRet AtZteRouterPwHeaderSet(AtPw self, uint8 *destMac, uint16 ethType, const tAtZteRouterTag *zteTag);
uint16 AtZteRouterPwEthTypeGet(AtPw self);
tAtZteRouterTag* AtZteRouterPwTagGet(AtPw self, tAtZteRouterTag *zteTag);
tAtZteRouterTag *AtZteRouterPwExpectedTagGet(AtPw self, tAtZteRouterTag *tag);

#endif /* _ATZTEROUTER_H_ */


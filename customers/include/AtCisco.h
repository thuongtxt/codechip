/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Customer
 * 
 * File        : AtCisco.h
 * 
 * Created Date: Mar 26, 2015
 *
 * Description : Specific APIs for Cisco projects
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCISCO_H_
#define _ATCISCO_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleSdh.h"
#include "AtSdhLine.h"
#include "AtModulePw.h"
#include "AtPwPsn.h"
#include "AtModuleEth.h"
#include "AtPtchService.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 AtDriverHaRestore(AtDriver self);
eAtRet AtCiscoSdhAuVcMapTypeSet(AtSdhVc vc, uint8 mapType, uint16* loSts, uint8 numLoSts);

/* XFI grouping */
eAtRet AtCiscoModuleEthXfiGroupSet(AtModuleEth ethModule, uint32 groupId);
uint32 AtCiscoModuleEthXfiGroupGet(AtModuleEth ethModule);
eAtRet AtCiscoIpModuleEthXfiGroupSet(AtModuleEth ethModule, uint32 groupId);
uint32 AtCiscoIpModuleEthXfiGroupGet(AtModuleEth ethModule);

eAtRet AtCiscoModuleSdhUpsrSfAlarmMaskSet(AtModuleSdh self, uint32 alarmMask);
uint32 AtCiscoModuleSdhUpsrSfAlarmMaskGet(AtModuleSdh self);
eAtRet AtCiscoModuleSdhUpsrSdAlarmMaskSet(AtModuleSdh self, uint32 alarmMask);
uint32 AtCiscoModuleSdhUpsrSdAlarmMaskGet(AtModuleSdh self);

/* VLAN management for eXAUI#1 */
eAtRet AtExauiIngressVlanSet(AtEthPort self, uint32 channelId, const tAtVlan *vlan);
eAtRet AtExauiIngressVlanGet(AtEthPort self, uint32 channelId, tAtVlan *vlan);
eAtRet AtExauiEgressExpectedVlanIdSet(AtEthPort self, uint32 channelId, uint16 expectedVlanId);
uint16 AtExauiEgressExpectedVlanIdGet(AtEthPort self, uint32 channelId);

/* eXAUIs */
uint32 AtModuleEthNumExauiGet(AtModuleEth self);
AtEthPort AtModuleEthExauiGet(AtModuleEth self, uint32 exauiId);

/* PTCH service */
AtPtchService AtModuleEthPtchServiceGet(AtModuleEth self, eAtPtchServiceType serviceType);
AtPtchService AtModuleEthGeBypassPtchServiceGet(AtModuleEth module, AtEthPort gePort);

/* CPLD */
uint32 AtCiscoModulePdhCpldVersion(AtModulePdh self);
eBool AtCiscoModulePdhCpldIsDownloaded(AtModulePdh self);
eAtRet AtCiscoModulePdhCpldDs1LoopbackSet(AtModulePdh self, eAtLoopbackMode loopbackMode);

/* Set Dest MAC, Src MAC, Ethernet Type for prbs engine */
eAtRet AtCiscoPrbsEngineDestMacSet(AtPrbsEngine self, uint8* mac);
eAtRet AtCiscoPrbsEngineSrcMacSet(AtPrbsEngine self, uint8* mac);
eAtRet AtCiscoPrbsEngineEthernetTypeSet(AtPrbsEngine self, uint16 ethType);

/* OAM flows */
eAtRet AtCiscoControlFlowEgressDestMacSet(AtModuleEth module, const uint8* destMac);
eAtRet AtCiscoControlFlowEgressDestMacGet(AtModuleEth module, uint8* destMac);
eAtRet AtCiscoControlFlowEgressSrcMacSet(AtModuleEth module, const uint8* srcMac);
eAtRet AtCiscoControlFlowEgressSrcMacGet(AtModuleEth module, uint8* srcMac);
eAtRet AtCiscoControlFlowEgressEthTypeSet(AtModuleEth module, uint16 ethType);
uint16 AtCiscoControlFlowEgressEthTypeGet(AtModuleEth module);
eAtRet AtCiscoControlFlowEgressControlVlanSet(AtModuleEth module, const tAtVlan* vlan);
eAtRet AtCiscoControlFlowEgressControlVlanGet(AtModuleEth module, tAtVlan* vlan);
eAtRet AtCiscoControlFlowEgressCircuitVlanTPIDSet(AtModuleEth module, uint16 tpid);
uint16 AtCiscoControlFlowEgressCircuitVlanTPIDGet(AtModuleEth module);

eAtRet AtCiscoControlFlowIngressControlVlanSet(AtModuleEth module, const tAtVlan* vlan);
eAtRet AtCiscoControlFlowIngressControlVlanGet(AtModuleEth module, tAtVlan* vlan);

/* GE bypass */
eAtRet AtCiscoGePortBypassPortSet(AtEthPort gePort, AtEthPort bypassPort);
AtEthPort AtCiscoGePortBypassPortGet(AtEthPort gePort);
eAtRet AtCiscoGePortBypassEnable(AtEthPort gePort, eBool enable);
eBool AtCiscoGePortBypassIsEnabled(AtEthPort gePort);

eAtRet AtCiscoModuleSdhUpsrSfAlarmMaskSet(AtModuleSdh self, uint32 alarmMask);
uint32 AtCiscoModuleSdhUpsrSfAlarmMaskGet(AtModuleSdh self);
eAtRet AtCiscoModuleSdhUpsrSdAlarmMaskSet(AtModuleSdh self, uint32 alarmMask);
uint32 AtCiscoModuleSdhUpsrSdAlarmMaskGet(AtModuleSdh self);

#ifdef __cplusplus
}
#endif
#endif /* _ATCISCO_H_ */


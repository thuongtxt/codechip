/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Customer
 * 
 * File        : AtCienaDcc.h
 * 
 * Created Date: Nov 23, 2016
 *
 * Description : DCC specific APIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCIENADCC_H_
#define _ATCIENADCC_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtCiena
 * @{
 */

/**
 * @brief DCC-PW-Alarm
 */
typedef enum eAtCienaDccPwAlarmType
    {
	cAtCienaDccPwAlarmTypeRxPktDiscards = cBit0 /**< Rx packet discard if corresponding enable MAC is not set, with discard flag per channel. */
    }eAtCienaDccPwAlarmType;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* ETH Type */
eAtRet AtCienaDccPwEthTypeSet(AtPw self, uint16 ethType);
uint16 AtCienaDccPwEthTypeGet(AtPw self);

/* Type */
eAtRet AtCienaDccPwTypeSet(AtPw self, uint8 type);
uint8 AtCienaDccPwTypeGet(AtPw self);

/* Version */
eAtRet AtCienaDccPwVersionSet(AtPw self, uint8 version);
uint8 AtCienaDccPwVersionGet(AtPw self);

/* MAC checking */
eAtRet AtCienaDccPwDestMacCheckEnable(AtPw self, eBool enable);
eBool AtCienaDccPwDestMacCheckIsEnabled(AtPw self);

/* Lower MAC for RxPW */
eAtRet AtCienaDccPwExpectedLabelSet(AtPw self, uint8 label);
uint8 AtCienaDccPwExpectedLabelGet(AtPw self);

/* Set gobal mac MAC DA for  Rx Header Processing
 * The mac[5:0] will be not set to hardware.
 * */
eAtRet AtCienaDccExpectGlobalMacSet(AtEthPort self, const uint8 *destMac);
eAtRet AtCienaDccExpectGlobalMacGet(AtEthPort self, uint8 *destMac);

/* VLAN checking */
eAtRet AtCienaDccPwCVLanCheckEnable(AtPw self, eBool enable);
eBool AtCienaDccPwCVLanCheckIsEnabled(AtPw self);
eAtModuleEthRet AtCienaEthPortExpectedCVlanSet(AtEthPort self, uint16 vlanId);
uint16 AtCienaEthPortExpectedCVlanGet(AtEthPort self);

#ifdef __cplusplus
}
#endif
#endif /* _ATCIENADCC_H_ */


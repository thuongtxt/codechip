/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ALU
 * 
 * File        : AtAlu.h
 * 
 * Created Date: Aug 27, 2014
 *
 * Description : ALU specific APIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATALU_H_
#define _ATALU_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtPw.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtCustomer
 * @{
 */

/** @brief PLL sticky */
typedef enum eAtAluFpgaPllSticky
    {
    cAtAluFpgaPllStickyXAUI156MHzPllNotLocked     = cBit5, /**<  */
    cAtAluFpgaPllStickyQdrNo2FeedbackPllNotLocked = cBit4, /**<  */
    cAtAluFpgaPllStickyQdrNo1FeedbackPllNotLocked = cBit3, /**<  */
    cAtAluFpgaPllStickyRef100MHzCore2PllNotLocked = cBit2, /**<  */
    cAtAluFpgaPllStickyRef100MHzCore1PllNotLocked = cBit1, /**<  */
    cAtAluFpgaPllStickySys155MHzPllNotLocked      = cBit0  /**<  */
    }eAtAluFpgaPllSticky;

/** @brief FIFO sticky. Note, PWE stands for PW encapsulation */
typedef enum eAtAluFpgaFifoSticky
    {
    cAtAluFpgaFifoStickyTxPktDataValidCmdFifoFull       = cBit0,   /**<  */
    cAtAluFpgaFifoStickyTxPktInfoValidCmdFifoFull       = cBit1,   /**<  */
    cAtAluFpgaFifoStickyRxPktInfoValidCmdFifoFull       = cBit2,   /**<  */
    cAtAluFpgaFifoStickyRxPktDataValidCmdFifoFull       = cBit3,   /**<  */
    cAtAluFpgaFifoStickyTxPweDataFifoEmpty              = cBit4,   /**<  */
    cAtAluFpgaFifoStickyTxPweWrPacketFifoFull           = cBit5,   /**<  */
    cAtAluFpgaFifoStickyTxPweRdPacketFifoFull           = cBit6,   /**<  */
    cAtAluFpgaFifoStickyRxPweWrCacheFifoFull            = cBit7,   /**<  */
    cAtAluFpgaFifoStickyRxPweTdmValidSegmentFifoFull    = cBit8,   /**<  */
    cAtAluFpgaFifoStickyRxPweRdSegmentFifoFull          = cBit9,   /**<  */
    cAtAluFpgaFifoStickyRxPweLinkListValidCmdFifoFull   = cBit10,  /**<  */
    cAtAluFpgaFifoStickyRxPweLinkListRequestCmdFifoFull = cBit11,  /**<  */
    cAtAluFpgaFifoStickyRxPweTdmFifoFull                = cBit12,  /**<  */
    cAtAluFpgaFifoStickyRxPweTdmFifoEmpty               = cBit13,  /**<  */
    cAtAluFpgaFifoStickyRxGeFifoFull                    = cBit14   /**<  */
    }eAtAluFpgaFifoSticky;

/** @brief General sticky. Note, PWE stands for PW encapsulation */
typedef enum eAtAluFpgaGeneralSticky
    {
    cAtAluFpgaGeneralStickyTxPktInfoMemoryBusError    = cBit0,  /**<  */
    cAtAluFpgaGeneralStickyRxPktInfoEccError          = cBit1,  /**<  */
    cAtAluFpgaGeneralStickyRxPktInfoMemoryBusError    = cBit2,  /**<  */
    cAtAluFpgaGeneralStickyRxPktDataReorderCmdError   = cBit3,  /**<  */
    cAtAluFpgaGeneralStickyRxPktDataMemoryBusError    = cBit4,  /**<  */
    cAtAluFpgaGeneralStickyRxPktDataDdrCrcError       = cBit5,  /**<  */
    cAtAluFpgaGeneralStickyTxPweWrCacheDuplication    = cBit6,  /**<  */
    cAtAluFpgaGeneralStickyTxPweWrCacheEmpty          = cBit7,  /**<  */
    cAtAluFpgaGeneralStickyTxPweDataBlockDuplication  = cBit8,  /**<  */
    cAtAluFpgaGeneralStickyTxPweRdCacheDuplication    = cBit9,  /**<  */
    cAtAluFpgaGeneralStickyRxPweWrPktOversize         = cBit10, /**<  */
    cAtAluFpgaGeneralStickyRxPweWrDataBufferPause     = cBit11, /**<  */
    cAtAluFpgaGeneralStickyRxPweWrCacheDisable        = cBit12, /**<  */
    cAtAluFpgaGeneralStickyRxPweDataBufferDisable     = cBit13, /**<  */
    cAtAluFpgaGeneralStickyRxPweRdCacheLost           = cBit14, /**<  */
    cAtAluFpgaGeneralStickyRxPweLostStartOfPkt        = cBit15, /**<  */
    cAtAluFpgaGeneralStickyRxPweLostEndOfPkt          = cBit16, /**<  */
    cAtAluFpgaGeneralStickyRxPweRdCacheError          = cBit17, /**<  */
    cAtAluFpgaGeneralStickyRxPweWrCacheEmpty          = cBit18, /**<  */
    cAtAluFpgaGeneralStickyRxPweWrCacheDuplication    = cBit19, /**<  */
    cAtAluFpgaGeneralStickyRxPweRdCacheEmpty          = cBit20, /**<  */
    cAtAluFpgaGeneralStickyRxPweRdCacheDuplication    = cBit21, /**<  */
    cAtAluFpgaGeneralStickyRxPweDataBufferEmpty       = cBit22, /**<  */
    cAtAluFpgaGeneralStickyRxPweDataBlockDuplication  = cBit23  /**<  */
    }eAtAluFpgaGeneralSticky;

/**
 * @brief Soft Error Mitigation (SEM) Alarm types
 */
typedef enum eAtSemAlarm
    {
    cAtSemAlarmNone           = 0,      /**< SEM IP has no alarm */
    cAtSemAlarmInitialization = cBit0,  /**< SEM IP got Initialization State event */
    cAtSemAlarmFatalError     = cBit1,  /**< SEM IP has fatal error */
    cAtSemAlarmCorrection     = cBit2   /**< SEM IP got Correction State event */
    }eAtSemAlarm;

/**
 * @brief Soft Error Mitigation (SEM) Status types
 */
typedef enum eAtSemStatus
    {
    cAtSemStatusNotSupport     = 0,      /**< SEM IP has not been supported */
    cAtSemStatusActive         = cBit0,  /**< FSM of SEM IP is active */
    cAtSemStatusIdle           = cBit1,  /**< FMS of SEM IP currently in IDLE */
    cAtSemStatusInitialization = cBit2,  /**< FSM of SEM IP is INITIALIZATION */
    cAtSemStatusObservation    = cBit3,  /**< FSM of SEM IP is OSERVATION */
    cAtSemStatusUncorrectable  = cBit4   /**< Error uncorrectable */
    }eAtSemStatus;

/**
 * @brief Soft Error Mitigation (SEM) validation return code
 */
typedef enum eAtSemValidateRet
    {
    cAtSemValidateCorrected,    /**<  SEM function is working normally, inserted error has been corrected */
    cAtSemValidateNotActive,    /**<  SEM function is not active */
    cAtSemValidateFsmFailed,    /**<  SEM state machine does not work correctly */
    cAtSemValidateFatalError,   /**<  SEM function has fatal error and can not work properly */
    cAtSemValidateUncorrectable /**<  SEM can not correct inserted CRC error */
    }eAtSemValidateRet;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Access PM registers */
uint32 AtAluPwNEPerformanceMornitoringGet(AtPw self);
uint32 AtAluPwFEPerformanceMornitoringGet(AtPw self);

/* To customize printf action */
int AtAluCliExecute(int shellHandler, const char *cmdString);
eAtRet AtAluPrintFuncSet(int (*printFunc)(int shellHandler, const char *aString));

/* For CLI application */
eAtRet AtAluCliStart(AtHal hal);
eAtRet AtAluCliCleanup(void);

/* To control XAUI initializing */
eAtRet AtAluXauiReset(AtHal hal, eBool reset);
uint32 AtAluXauiStatusRead2Clear(AtHal hal, eBool clear);

/* QDR */
eAtRet AtAluQdrTest(AtDevice device, uint8 qdrId);
eAtRet AtAluQdrMarginDetect(AtDevice device, uint8 qdrId);
uint32 AtAluQdrReadLeftMargin(AtDevice device, uint8 qdrId);
uint32 AtAluQdrReadRightMargin(AtDevice device, uint8 qdrId);
uint32 AtAluQdrWriteLeftMargin(AtDevice device, uint8 qdrId);
uint32 AtAluQdrWriteRightMargin(AtDevice device, uint8 qdrId);
eBool AtAluQdrParityErrorGet(AtDevice device, uint8 qdrId);
eBool AtAluQdrParityErrorClear(AtDevice device, uint8 qdrId);

/* DDR */
eAtRet AtAluDdrMarginDetect(AtDevice device);
uint32 AtAluDdrReadLeftMargin(AtDevice device);
uint32 AtAluDdrReadRightMargin(AtDevice device);
uint32 AtAluDdrWriteLeftMargin(AtDevice device);
uint32 AtAluDdrWriteRightMargin(AtDevice device);
eBool AtAluDdrCalibStatusIsGood(AtDevice device);

/* Alarm sticky */
uint32 AtAluPllAlarmStickyGet(AtDevice device);
uint32 AtAluPllAlarmStickyClear(AtDevice device);
uint32 AtAluFifoAlarmStickyGet(AtDevice device);
uint32 AtAluFifoAlarmStickyClear(AtDevice device);
uint32 AtAluGeneralAlarmStickyGet(AtDevice device);
uint32 AtAluGeneralAlarmStickyClear(AtDevice device);

/* Clock status */
uint32 AtAluXGMIIClock156_25MHzCurrentFrequencyGet(AtDevice device);
uint32 AtAluOcnTxClock155_52MHzCurrentFrequencyGet(AtDevice device);
uint32 AtAluOcnRxClock155_52MHzCurrentFrequencyGet(AtDevice device);
uint32 AtAluCore1Clock100MHzCurrentFrequencyGet(AtDevice device);
uint32 AtAluCore2Clock100MHzCurrentFrequencyGet(AtDevice device);
uint32 AtAluDdr3UserClock100MHzCurrentFrequencyGet(AtDevice device);
uint32 AtAluPcieUserClock62_5MHzCurrentFrequencyGet(AtDevice device);
uint32 AtAluQdrNo1FeedbackClock155_52MHzCurrentFrequencyGet(AtDevice device);
uint32 AtAluQdrNo2FeedbackClock155_52MHzCurrentFrequencyGet(AtDevice device);

/* Soft Error Mitigation (SEM) IP */
eAtSemValidateRet AtAluDeviceSemValidate(AtDevice device);
uint8 AtAluDeviceSemErrorCounterGet(AtDevice device);
uint8 AtAluDeviceSemStatusGet(AtDevice device);
uint8 AtAluDeviceSemAlarmGet(AtDevice device);
uint8 AtAluDeviceSemAlarmClear(AtDevice device);
uint32 AtAluSemControllerMaxElapseTimeToInsertError(AtDevice self);
uint32 AtAluSemControllerMaxElapseTimeToChangeFsm(AtDevice self);

/* GE FIFO max reached level */
uint32 AtAluGeFifoMaxLevelGet(AtDevice device);
uint32 AtAluGeFifoMaxLevelClear(AtDevice device);

#endif /* _ATALU_H_ */


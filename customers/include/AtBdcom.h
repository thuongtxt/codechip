/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BDCOM
 * 
 * File        : AtBdcom.h
 * 
 * Created Date: Oct 5, 2013
 *
 * Description : BDCOM specific APIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATBDCOM_H_
#define _ATBDCOM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtEthFlow.h"
#include "AtPppLink.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/*
 * @brief BDCOM Tag (C-VLAN tag)
 */
typedef struct tAtBdcomTag
    {
    uint8 cpuPktIndicator;   /* [0..1] CPU packet indicator */
    uint8 portNumber;        /* [1..15] Input port number */
    uint8 encapType;         /* [1..15] Encap type. Identifies the packet type of business
                               configuration set */
    uint8 slotNumber;        /* [1..15] Input slot number */
    uint16 channelNumber;    /* [1..1024] Input channel number */
    uint8 pktLen;            /* [0..63] Packet Length. If this field is 0, indicating that
                               the message length is greater than 60 bytes (the length of
                               the DA, SA, L / T, Line Header, payload); Otherwise,
                               the actual length of the message padding field for packets
                               filled to fill */
    }tAtBdcomTag;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* ETH flow header */
eAtRet AtBdcomEthFlowTxFullHeaderSet(AtEthFlow self, uint8 *egressSourceMac, uint8 *egressDestMac, uint8 portId, const tAtBdcomTag *tag);
eAtRet AtBdcomEthFlowTxHeaderSet(AtEthFlow self, uint8 portId, const tAtBdcomTag *tag);
eAtRet AtBdcomEthFlowExpectedHeaderSet(AtEthFlow self, uint8 portId, const tAtBdcomTag *tag);
eAtRet AtBdcomEthFlowTagGet(AtEthFlow self, tAtBdcomTag *tag);
eAtRet AtBdcomEthFlowExpectedTagGet(AtEthFlow self, tAtBdcomTag *tag);

/* PID table for BDCOM product */
eAtRet AtBdcomTdmToPsnPppHeaderSet(AtPppLink self, uint8 entryId, uint32 pppHeader, uint8 headerLength);
eAtRet AtBdcomTdmToPsnEthTypeSet(AtPppLink self, uint8 entryId, uint32 ethType);
eAtRet AtBdcomPsnToTdmPppHeaderSet(AtPppLink self, uint8 entryId, uint32 pppHeader, uint8 headerLength);
eAtRet AtBdcomPsnToTdmEthTypeSet(AtPppLink self, uint8 entryId, uint32 ethType);
uint32 AtBdcomTdmToPsnPppHeaderGet(AtPppLink self, uint8 entryId, uint8 *headerLength);
uint32 AtBdcomTdmToPsnEthTypeGet(AtPppLink self, uint8 entryId);
uint32 AtBdcomPsnToTdmPppHeaderGet(AtPppLink self, uint8 entryId, uint8 *headerLength);
uint32 AtBdcomPsnToTdmEthTypeGet(AtPppLink self, uint8 entryId);

/* Device management */
int AtBdcomAddDevice(uint8 slotId, uint8 pageId);
int AtBdcomCleanup(void);
int AtBdComCliExecute(const char *cli);

/* Platform */
AtOsal AtOsalBdcom(void);

#ifdef __cplusplus
}
#endif
#endif /* _ATBDCOM_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Application
 *
 * File        : AtCiscoUpsr.h
 *
 * Created Date: Sep 10, 2015
 *
 * Description : UPSR SD/SF tables
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _ATCISCOUPSR_H_
#define _ATCISCOUPSR_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtCiscoUpsr
 * @{
 */

/** @brief 32-bit unsigned integer */
typedef unsigned int at_uint32;

/** @brief Abstract data structure to encapsulate UPSR table */
typedef struct tAtCiscoUpsr * AtCiscoUpsr;

/**
 * @brief UPSR alarm type
 */
typedef enum eAtCiscoUpsrAlarm
    {
    cAtCiscoUpsrAlarmNone = 0,      /**< No alarm */
    cAtCiscoUpsrAlarmSf   = 1 << 0, /**< SF */
    cAtCiscoUpsrAlarmSd   = 1 << 1  /**< SD */
    }eAtCiscoUpsrAlarm;

/** @brief Group label set */
typedef enum eAtCiscoPwGroupLabelSet
    {
    cAtCiscoPwGroupLabelSetUnknown, /**< Invalid set */
    cAtCiscoPwGroupLabelSetPrimary, /**< Primary label set */
    cAtCiscoPwGroupLabelSetBackup   /**< Backup label set */
    }eAtCiscoPwGroupLabelSet;

/**
 * @brief Delegate that this module will ask when necessary
 */
typedef struct tAtCiscoUpsrDelegate
    {
    at_uint32 (*CanReadRegister)(at_uint32 baseAddress, at_uint32 localAddress, void *userData); /**<
       Called before reading a register to check if read can be performed */
    at_uint32 (*CanWriteRegister)(at_uint32 baseAddress, at_uint32 localAddress, at_uint32 value, void *userData); /**<
       Call before writing a register to check if write can be performed */
    }tAtCiscoUpsrDelegate;

/**
 * UPSR listener
 */
typedef struct tAtCiscoUpsrListener
    {
    void (*StsInterruptChanged)(at_uint32 baseAddress,
                                void *data,
                                at_uint32 stsId, /**< STS Index from 0 to 383 */
                                eAtCiscoUpsrAlarm changedAlarms,
                                eAtCiscoUpsrAlarm currentStatus); /**< Called when STS has interrupt changed */

    void (*VtInterruptChanged)(at_uint32 baseAddress,
                               void *data,
                               at_uint32 stsId, /**< STS Index from 0 to 383 */
                               at_uint32 vtId, /**< VT Index inside STS from 0 to 27 */
                               eAtCiscoUpsrAlarm changedAlarms,
                               eAtCiscoUpsrAlarm currentStatus); /**< Called when VT has interrupt changed */
    }tAtCiscoUpsrListener;

/** @brief Error code */
typedef enum eAtCiscoUpsrRet
    {
    cAtCiscoUpsrRetOk,        /**< Success */
    cAtCiscoUpsrRetReadFail,  /**< Read fail */
    cAtCiscoUpsrRetWriteFail  /**< Write fail */
    }eAtCiscoUpsrRet;

/**
 * @brief UPSR Path alarm type
 */
typedef enum eAtCiscoUpsrPathAlarm
    {
    cAtCiscoUpsrPathAlarmNone   = 0,
    cAtCiscoUpsrPathAlarmBerSf  = 1 << 0,
    cAtCiscoUpsrPathAlarmBerSd  = 1 << 1,
    cAtCiscoUpsrPathAlarmPlm    = 1 << 2,
    cAtCiscoUpsrPathAlarmLop    = 1 << 3,
    cAtCiscoUpsrPathAlarmAis    = 1 << 4,
    cAtCiscoUpsrPathAlarmUneq   = 1 << 5,
    cAtCiscoUpsrPathAlarmTim    = 1 << 6
    }eAtCiscoUpsrPathAlarm;

typedef at_uint32 (*AtCiscoUpsrDirectReadHandler)(at_uint32 address);
typedef void (*AtCiscoUpsrDirectWriteHandler)(at_uint32 address, at_uint32 value);

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* For memory mapping to get device meta information */
at_uint32 AtCiscoUpsrMetaStartOffset(void);
at_uint32 AtCiscoUpsrMetaSizeInBytes(void);

/* Create instance */
AtCiscoUpsr AtCiscoUpsrCreate(at_uint32 metaBaseAddress, const tAtCiscoUpsrDelegate *delegate, void *userData);
void AtCiscoUpsrDelete(AtCiscoUpsr self);

/* For memory mapping to control UPSR tables and PW protection */
at_uint32 AtCiscoUpsrDwordStartOffset(AtCiscoUpsr self);
at_uint32 AtCiscoUpsrMemorySizeInBytes(AtCiscoUpsr self);

/* UPSR base address */
void AtCiscoUpsrBaseAddressSet(AtCiscoUpsr self, at_uint32 upsrBaseAddress);
at_uint32 AtCiscoUpsrBaseAddress(AtCiscoUpsr self);

/* Version control */
const char *AtCiscoUpsrVersion(AtCiscoUpsr self);

/* Device capacity */
at_uint32 AtCiscoUpsrMaxApsGroupsGet(AtCiscoUpsr self);
at_uint32 AtCiscoUpsrMaxHsGroupsGet(AtCiscoUpsr self);
at_uint32 AtCiscoUpsrMaxStsGet(AtCiscoUpsr self);
at_uint32 AtCiscoUpsrMaxVtInOneStsGet(AtCiscoUpsr self);

/* Delegate */
void AtCiscoUpsrDelegateSet(AtCiscoUpsr self, const tAtCiscoUpsrDelegate *delegate, void *userData);
const tAtCiscoUpsrDelegate *AtCiscoUpsrDelegateGet(AtCiscoUpsr self);

/* Interrupt restore */
void AtCiscoUpsrInterruptRestore(AtCiscoUpsr self);

/* MASR registers */
at_uint32 AtCiscoUpsrMASRStatusRead(AtCiscoUpsr self, eAtCiscoUpsrAlarm table);
at_uint32 AtCiscoUpsrMASRMaskRead(AtCiscoUpsr self);
void AtCiscoUpsrMASRMaskWrite(AtCiscoUpsr self, at_uint32 value);

/* STS registers */
at_uint32 AtCiscoUpsrStsMaskRead(AtCiscoUpsr self, eAtCiscoUpsrAlarm table, at_uint32 rowId);
void AtCiscoUpsrStsMaskWrite(AtCiscoUpsr self, eAtCiscoUpsrAlarm table, at_uint32 rowId, at_uint32 value);
at_uint32 AtCiscoUpsrStsStatusRead(AtCiscoUpsr self, eAtCiscoUpsrAlarm table, at_uint32 rowId);

/* VT registers */
at_uint32 AtCiscoUpsrVtMaskRead(AtCiscoUpsr self, eAtCiscoUpsrAlarm table, at_uint32 rowId);
void AtCiscoUpsrVtMaskWrite(AtCiscoUpsr self, eAtCiscoUpsrAlarm table, at_uint32 rowId, at_uint32 value);
at_uint32 AtCiscoUpsrVtStatusRead(AtCiscoUpsr self, eAtCiscoUpsrAlarm table, at_uint32 rowId);

/* SASR register */
at_uint32 AtCiscoUpsrSASRStsRead(AtCiscoUpsr self, eAtCiscoUpsrAlarm table, at_uint32 rowId);
at_uint32 AtCiscoUpsrSASRVtRead(AtCiscoUpsr self, eAtCiscoUpsrAlarm table, at_uint32 rowId);

/* SF/SD Alarm Mask register */
void AtCiscoUpsrAlarmMaskWrite(AtCiscoUpsr self, eAtCiscoUpsrAlarm table, eAtCiscoUpsrPathAlarm alarmMask);
eAtCiscoUpsrPathAlarm AtCiscoUpsrAlarmMaskRead(AtCiscoUpsr self, eAtCiscoUpsrAlarm table);

/* APS group enable */
void AtCiscoApsGroupEnable(AtCiscoUpsr self, at_uint32 apsGroupId, at_uint32 enable);
at_uint32 AtCiscoApsGroupIsEnabled(AtCiscoUpsr self, at_uint32 apsGroupId);

/* HSPW group control */
void AtCiscoHsGroupTxLabelSetSelect(AtCiscoUpsr self, at_uint32 hsGroupId, eAtCiscoPwGroupLabelSet labelSet);
eAtCiscoPwGroupLabelSet AtCiscoHsGroupTxSelectedLabelGet(AtCiscoUpsr self, at_uint32 hsGroupId);
void AtCiscoHsGroupRxLabelSetSelect(AtCiscoUpsr self, at_uint32 hsGroupId, eAtCiscoPwGroupLabelSet labelSet);
eAtCiscoPwGroupLabelSet AtCiscoHsGroupRxSelectedLabelGet(AtCiscoUpsr self, at_uint32 hsGroupId);

/* Error handling */
eAtCiscoUpsrRet AtCiscoUpsrErrorCode(AtCiscoUpsr self);

/* Auxiliary */
at_uint32 AtCiscoUpsrProductCodeGet(AtCiscoUpsr self);
at_uint32 AtCiscoUpsrFpgaVersion(AtCiscoUpsr self);
at_uint32 AtCiscoUpsrFpgaBuiltNumber(AtCiscoUpsr self);

/* Work with sample implementation */
void AtCiscoUpsrInterruptProcess(AtCiscoUpsr self);
void AtCiscoUpsrListenerAdd(AtCiscoUpsr self, void *data, const tAtCiscoUpsrListener *callbacks);
void AtCiscoUpsrListenerRemove(AtCiscoUpsr self, void *data, const tAtCiscoUpsrListener *callbacks);

/* For testing purpose */
void AtCiscoUpsrDirectReadHandlerSet(AtCiscoUpsrDirectReadHandler readHandler);
void AtCiscoUpsrDirectWriteHandlerSet(AtCiscoUpsrDirectWriteHandler writeHandler);
void AtCiscoUpsrDeviceMetaDirectReadHandlerSet(AtCiscoUpsrDirectReadHandler readHandler);

#ifdef __cplusplus
}
#endif
#endif /* _ATCISCOUPSR_H_ */

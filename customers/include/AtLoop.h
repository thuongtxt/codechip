/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : LOOP
 * 
 * File        : AtLoop.h
 * 
 * Created Date: Mar 14, 2015
 *
 * Description : LOOP declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATLOOP_H_
#define _ATLOOP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCommon.h"
#include "AtSdhLine.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eAtLoopTemperatureAlarm
    {
    cAtLoopTemperatureAlarmNormal,
    cAtLoopTemperatureAlarmYellow,
    cAtLoopTemperatureAlarmRed
    }eAtLoopTemperatureAlarm;

typedef enum eAtLoopLedType
    {
    cAtLoopLedTypeGreen,
    cAtLoopLedTypeRed
    }eAtLoopLedType;

/*--------------------------- Forward declarations ---------------------------*/
/* Configure */
eAtRet AtLoopTemperatureRebootWhenRedAlarmEnable(AtDevice self, eBool enable);
eBool AtLoopTemperatureRebootWhenRedAlarmIsEnabled(AtDevice self);

eAtRet AtLoopTemperatureRedAlarmSetThresholdSet(AtDevice self, uint32 celsiusValue);
uint32 AtLoopTemperatureRedAlarmSetThresholdGet(AtDevice self);
eAtRet AtLoopTemperatureRedAlarmClearThresholdSet(AtDevice self, uint32 celsiusValue);
uint32 AtLoopTemperatureRedAlarmClearThresholdGet(AtDevice self);

eAtRet AtLoopTemperatureYellowAlarmSetThresholdSet(AtDevice self, uint32 celsiusValue);
uint32 AtLoopTemperatureYellowAlarmSetThresholdGet(AtDevice self);
eAtRet AtLoopTemperatureYellowAlarmClearThresholdSet(AtDevice self, uint32 celsiusValue);
uint32 AtLoopTemperatureYellowAlarmClearThresholdGet(AtDevice self);

/* Alarm */
uint32 AtLoopTemperatureGet(AtDevice self);
uint32 AtLoopTemperatureAlarmGet(AtDevice self);

/* Controlling device LEDs */
eAtRet AtLoopDeviceStatusLedStateSet(AtDevice self, eAtLoopLedType ledType, eAtLedState ledState);
eAtLedState AtLoopDeviceStatusLedStateGet(AtDevice self, eAtLoopLedType ledType);
eAtRet AtLoopDeviceActiveLedStateSet(AtDevice self, eAtLoopLedType ledType, eAtLedState ledState);
eAtLedState AtLoopDeviceActiveLedStateGet(AtDevice self, eAtLoopLedType ledType);

/* Controlling STM LEDs */
eAtRet AtLoopSdhLineLedStateSet(AtSdhLine self, eAtLoopLedType ledType, eAtLedState ledState);
eAtLedState AtLoopSdhLineLedStateGet(AtSdhLine self, eAtLoopLedType ledType);

/* SFP */
eBool AtLoopSdhLineSfpExisted(AtSdhLine self);

#ifdef __cplusplus
}
#endif

#endif /* _ATLOOP_H_ */


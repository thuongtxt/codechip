/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Customer
 * 
 * File        : AtCienaKByte.h
 * 
 * Created Date: Nov 2, 2016
 *
 * Description : K-Bytes
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCIENAKBYTE_H_
#define _ATCIENAKBYTE_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtCiena
 * @{
 */

/**
 * @brief Kbyte-PW-Alarm
 */
typedef enum eAtCienaKbyteApsPwAlarmType
    {
    cAtCienaKbyteApsPwAlarmTypeDaMacMisMatch       = cBit0, /**< DA mismatch */
    cAtCienaKbyteApsPwAlarmTypeEthTypeMisMatch     = cBit1, /**< ETH Type mismatch */
    cAtCienaKbyteApsPwAlarmTypeVerMisMatch         = cBit2, /**< Version mismatch */
    cAtCienaKbyteApsPwAlarmTypeTypeMisMatch        = cBit3, /**< Type mismatch */
    cAtCienaKbyteApsPwAlarmTypeLengthFieldMisMatch = cBit4, /**< Length field mismatch */
    cAtCienaKbyteApsPwAlarmTypeLengthCountMisMatch = cBit5, /**< Number of actual bytes are mismatched */
    cAtCienaKbyteApsPwAlarmTypeChannelMisMatch     = cBit6, /**< Channel mismatch */
    cAtCienaKbyteApsPwAlarmTypeWatchdogTimer       = cBit7  /**< Watchdog expires */
    }eAtCienaKbyteApsPwAlarmType;

/**
 * @brief PDH interface type for PDH card
 */
typedef enum eAtCienaSdhLineExtendedKBytePosition
    {
    cAtCienaSdhLineExtendedKBytePositionInvalid, /**< Invalid Position */
    cAtCienaSdhLineExtendedKBytePositionD1Sts4,  /**< D1, STS1#4 */
    cAtCienaSdhLineExtendedKBytePositionD1Sts10  /**< D1, STS1#10 */
    } eAtCienaSdhLineExtendedKBytePosition;

/**
 * @brief K-Byte source to transmit to SONET/SDH side
 */
typedef enum eAtCienaSdhLineKByteSource
    {
    cAtCienaSdhLineKByteSourceUnknown, /**< Unknown source */
    cAtCienaSdhLineKByteSourceCpu,     /**< TX SONET/SDH framer will take K/EK-Bytes from CPU */
    cAtCienaSdhLineKByteSourceSgmii    /**< TX SONET/SDH framer will take K/EK-Bytes from received packets on SGMII */
    }eAtCienaSdhLineKByteSource;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Access the built-in PW */
AtPw AtCienaKBytePwGet(AtModulePw self);

/* Global attributes */
eAtRet AtCienaKBytePwEthTypeSet(AtModulePw self, uint16 ethType);
uint16 AtCienaKBytePwEthTypeGet(AtModulePw self);
eAtRet AtCienaKBytePwTypeSet(AtModulePw self, uint8 type);
uint8 AtCienaKBytePwTypeGet(AtModulePw self);
eAtRet AtCienaKBytePwVersionSet(AtModulePw self, uint8 version);
uint8 AtCienaKBytePwVersionGet(AtModulePw self);

/* Channel managements */
uint32 AtCienaKBytePwNumChannels(AtPw self);
AtChannel AtCienaKbytePwChannelGet(AtPw self, uint8 channelId);

/* Extended K Byte */
eAtRet AtCienaSdhLineExtendedKByteEnable(AtSdhLine self, eBool enable);
eBool AtCienaSdhLineExtendedKByteIsEnabled(AtSdhLine self);
eAtRet AtCienaSdhLineExtendedKBytePositionSet(AtSdhLine self, eAtCienaSdhLineExtendedKBytePosition position);
eAtCienaSdhLineExtendedKBytePosition AtCienaSdhLineExtendedKBytePositionGet(AtSdhLine self);

/* Transmit frame at refresh interval */
eAtRet AtCienaKBytePwIntervalSet(AtPw self, uint32 timerInUs);
uint32 AtCienaKBytePwIntervalGet(AtPw self);

/* Watchdog timer if no frames received for 256us to 16ms */
eAtRet AtCienaKBytePwWatchdogTimerSet(AtPw self, uint32 timerInUs);
uint32 AtCienaKBytePwWatchdogTimerGet(AtPw self);

/* K-Byte source */
eAtRet AtCienaSdhLineKByteSourceSet(AtSdhLine line, eAtCienaSdhLineKByteSource source);
eAtCienaSdhLineKByteSource AtCienaSdhLineKByteSourceGet(AtSdhLine line);

/* EK1/K2 to be inserted from CPU */
eAtRet AtCienaSdhLineTxEK1Set(AtSdhLine line, uint8 eK1);
eAtRet AtCienaSdhLineTxEK2Set(AtSdhLine line, uint8 eK2);
uint8 AtCienaSdhLineTxEK1Get(AtSdhLine line);
uint8 AtCienaSdhLineTxEK2Get(AtSdhLine line);
uint8 AtCienaSdhLineRxEK1Get(AtSdhLine line);
uint8 AtCienaSdhLineRxEK2Get(AtSdhLine line);

/* MACs
 * - Remote MAC
 *   + TX direction (to Phlox)  : Inserted in SA MAC
 *   + RX direction (from Phlox): Compared with DA MAC
 * - Central MAC
 *   + TX direction (to Phlox)  : Inserted in DA MAC
 *   + RX direction (from Phlox): Compared with SA MAC
 */
eAtRet AtCienaKBytePwRemoteMacSet(AtPw self, uint8 *address);
eAtRet AtCienaKBytePwRemoteMacGet(AtPw self, uint8 *address);
eAtRet AtCienaKBytePwCentralMacSet(AtPw self, uint8 *address);
eAtRet AtCienaKBytePwCentralMacGet(AtPw self, uint8 *address);

/* =============================================================================
 * Testing APIs
 * ============================================================================= */
/* Transmit frames on demand via S/W register */
eAtRet AtCienaKBytePwTrigger(AtPw self);

#ifdef __cplusplus
}
#endif
#endif /* _ATCIENAKBYTE_H_ */

#include "AtCienaKbyteDeprecated.h"

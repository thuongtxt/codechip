/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : AtCieanKbyteChannel.h
 * 
 * Created Date: May 28, 2018
 *
 * Description : APS/KByte channel
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCIENAKBYTECHANNEL_H_
#define _ATCIENAKBYTECHANNEL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannel.h" /* Superclass */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtCiena
 * @{
 */

/**
 * @brief APS/K-byte channel alarm type
 */
typedef enum eAtCienaKbyteChannelAlarmType
    {
    cAtCienaKbyteChannelAlarmTypeMismatch  = cBit0 /**< K-byte channel ID mismatched */
    }eAtCienaKbyteChannelAlarmType;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/
AtSdhLine AtCienaKbyteChannelLineGet(AtChannel self);
AtPw AtCieanKbyteChannelPwGet(AtChannel self);

/* Label which is in CHANNEL ID[15:4] */
eAtRet AtCienaKbyteChannelTxLabelSet(AtChannel self, uint16 label);
uint16 AtCienaKbyteChannelTxLabelGet(AtChannel self);
eAtRet AtCienaKbyteChannelExpectedLabelSet(AtChannel self, uint16 label);
uint16 AtCienaKbyteChannelExpectedLabelGet(AtChannel self);

/* Validation */
eAtRet AtCienaKbyteChannelValidationEnable(AtChannel self, eBool enabled);
eBool AtCienaKbyteChannelValidationIsEnabled(AtChannel self);

/* AIS and RDI propagation suppression */
eAtRet AtCienaKbyteChannelSuppressionEnable(AtChannel self, eBool enable);
eBool AtCienaKbyteChannelSuppressionIsEnabled(AtChannel self);

/* Received K-Bytes on SGMII */
uint8 AtCienaKbyteChannelRxK1Get(AtChannel self);
uint8 AtCienaKbyteChannelRxK2Get(AtChannel self);
uint8 AtCienaKbyteChannelRxEK1Get(AtChannel self);
uint8 AtCienaKbyteChannelRxEK2Get(AtChannel self);

/* =============================================================================
 * Testing APIs
 * ============================================================================= */
/* Enable/disable K-Byte overriding. */
eAtRet AtCienaKbyteChannelOverrideEnable(AtChannel self, eBool enable);
eBool AtCienaKbyteChannelOverrideIsEnabled(AtChannel self);

/* Overridden K1/EK1 value */
eAtRet AtCienaKbyteChannelTxK1Set(AtChannel self, uint8 value);
uint8 AtCienaKbyteChannelTxK1Get(AtChannel self);
eAtRet AtCienaKbyteChannelTxEK1Set(AtChannel self, uint8 value);
uint8 AtCienaKbyteChannelTxEK1Get(AtChannel self);

/* Overridden K2/EK2 value */
eAtRet AtCienaKbyteChannelTxK2Set(AtChannel self, uint8 value);
uint8 AtCienaKbyteChannelTxK2Get(AtChannel self);
eAtRet AtCienaKbyteChannelTxEK2Set(AtChannel self, uint8 value);
uint8 AtCienaKbyteChannelTxEK2Get(AtChannel self);

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _ATCIENAKBYTECHANNEL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Customer
 * 
 * File        : AtCiena.h
 * 
 * Created Date: Jun 18, 2016
 *
 * Description : Ciena specific APIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCIENA_H_
#define _ATCIENA_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleSdh.h"
#include "AtModulePdh.h"
#include "AtSdhLine.h"
#include "AtEthPort.h"
#include "AtCienaDevice.h"
#include "AtModuleClock.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtCiena
 * @{
 */

/**
 * @brief PW counters
 */
typedef struct tAtCienaPwCounters
    {
    uint32 txPackets;                      /**< Number of packets transmitted to PSN side. */
    uint32 txPayloadBytes;                 /**< Number of transmitted bytes of PW payload, including PW Control Word, RTP
                                                but not include PSN header. */
    uint32 rxPackets;                      /**< Number of received packets from PSN including good/stray/malformed packets. */
    uint32 rxPayloadBytes;                 /**< Number of received bytes of PW payload, including PW Control Word, RTP
                                                but not include PSN header. Payload bytes of good/stray/malformed packets are
                                                also counted in this counter.*/
    uint32 rxDiscardedPackets;             /**< Number of discarded packets */
    uint32 rxMalformedPackets;             /**< Number of received malformed packets. */
    uint32 rxReorderedPackets;             /**< Number of successfully re-ordered packets. This is not error counter,
                                                just to report there are re-sequence actions with packet stream. */
    uint32 rxLostPackets;                  /**< Number of lost packets - packets with expected sequences do not come.
                                                For example, packets with sequence 1,2,6,7 come but 3,4,5 do not. After
                                                re-sequence timeout, this counter will increase 3 as 3 packets do not come.*/
    uint32 rxOutOfSeqDropedPackets;        /**< Number of received packets are out of sequence but can not be re-ordered.
                                                For detail, packet coming with (sequence - expected sequence > min{PDV, 32})
                                                will be considered as out of sequence dropped packets*/
    uint32 rxStrayPackets;                 /**< Number of received stray packets. Packet is stray if RTP SSRC field
                                                mismatch packets. */
    uint32 rxDuplicatedPackets;            /**< Number of received duplicated packets. */

    /* CEP counters */
    uint32 txNbitPackets;                  /**< Number of transmitted N-bit packets. */
    uint32 txPbitPackets;                  /**< Number of transmitted P-bit packets */
    uint32 rxNbitPackets;                  /**< Number of received N-bit packets */
    uint32 rxPbitPackets;                  /**< Number of received P-bit packets */

    /* CESoP counters */
    uint32 txMbitPackets;                  /**< Number of transmitted M Bit packets when circuit detects RDI. */
    uint32 rxMbitPackets;                  /**< Number of received M Bit packets. This will include good /stray/malformed packets. */

    /* TDM counters */
    uint32 txLbitPackets;                  /**< Number of transmitted L-Bit packets. This happens when TDM has critical alarm (LOF/AIS/...) */
    uint32 txRbitPackets;                  /**< Number of transmitted R-Bit packets. When PW is in lost-of-packet state
                                                (SAToP term) or lost of packet synchronization (CEP term), RBit packets will
                                                be transmitted. */
    uint32 rxLbitPackets;                  /**< Number of received L-bit packets */
    uint32 rxRbitPackets;                  /**< Number of received R-bit packets */
    uint32 rxJitBufOverrun;                /**< Number of PW packets come when jitter buffer is in OVERRUN state. */
    uint32 rxJitBufUnderrun;               /**< Number of empty packets sent to TDM during UNDERRUN state */
    uint32 rxLops;                         /**< Number of transitions from normal to lost of packet synchronization condition */

    /* Other */
    uint32 numPacketsInJitterBuffer;       /**< Number of PW packets in jitter buffer */
    uint32 numAdditionBytesInJitterBuffer; /**< Number of PW addition bytes in jitter buffer */
    }tAtCienaPwCounters;

/**
 * @brief PDH interface type for PDH card
 */
typedef enum eAtCienaPdhInterfaceType
    {
    cAtCienaPdhInterfaceTypeUnknown, /**< Unknown interface type */
    cAtCienaPdhInterfaceTypeDe1,     /**< 84xDS1/E1 work with LIU */
    cAtCienaPdhInterfaceTypeDe3      /**< 24xDS3/E3/EC1 work with LIU */
    }eAtCienaPdhInterfaceType;

/**
 * @brief Monitoring clock output source types
 */
typedef enum eAtCienaClockMonitorOutput
    {
    cAtCienaClockMonitorOutputNone,
    cAtCienaClockMonitorOutputSpareSerdesDiv2,  /**< Monitor clock output of Spare SERDES reference clock source divide 2 */
    cAtCienaClockMonitorOutputOverheadDiv2,     /**< Monitor clock output of Overhead reference clock source divide 2 */
    cAtCienaClockMonitorOutputXfi155_52M_0Div2, /**< Monitor clock output of XFI serdes 155.52M reference clock source#0 divide 2 */
    cAtCienaClockMonitorOutputXfi155_52M_1Div2, /**< Monitor clock output of XFI serdes 155.52M reference clock source#1 divide 2 */
    cAtCienaClockMonitorOutputXfi156_25M_0Div2, /**< Monitor clock output of XFI serdes 156.25M reference clock source#0 divide 2 */
    cAtCienaClockMonitorOutputXfi156_25M_1Div2, /**< Monitor clock output of XFI serdes 156.25M reference clock source#1 divide 2 */
    cAtCienaClockMonitorOutputEth40G_0Div2,     /**< Monitor clock output of ETH 40Ghz reference clock source#0 divide 2 */
    cAtCienaClockMonitorOutputEth40G_1Div2,     /**< Monitor clock output of ETH 40Ghz reference clock source#1 divide 2 */
    cAtCienaClockMonitorOutputBaseX2500Div2,    /**< Monitor clock output of BaseX2500 reference clock source divide 2 */
    cAtCienaClockMonitorOutputQdrDiv2,          /**< Monitor clock output of QDR reference clock source divide 2 */
    cAtCienaClockMonitorOutputDdr_1Div2,        /**< Monitor clock output of DDR reference clock source#1 divide 2 */
    cAtCienaClockMonitorOutputDdr_2Div2,        /**< Monitor clock output of DDR reference clock source#2 divide 2 */
    cAtCienaClockMonitorOutputDdr_3Div2,        /**< Monitor clock output of DDR reference clock source#3 divide 2 */
    cAtCienaClockMonitorOutputDdr_4Div2,        /**< Monitor clock output of DDR reference clock source#4 divide 2 */
    cAtCienaClockMonitorOutputPcieDiv2,         /**< Monitor clock output of PCIE reference clock source divide 2 */
    cAtCienaClockMonitorOutputPrc,              /**< Monitor clock output of PRC reference clock source */
    cAtCienaClockMonitorOutputExt,              /**< Monitor clock output of External reference clock source */
    cAtCienaClockMonitorOutputSystem            /**< Monitor clock output of System PLL 311.04Mhz reference clock source */
    }eAtCienaClockMonitorOutput;

/** @brief DCC/KByte over SGMII ETH counters */
typedef enum eAtCienaSgmiiEthPortCounterType
    {
    cAtCienaSgmiiEthPortCounterTypeDccTxFrames   = cBit15,
	cAtCienaSgmiiEthPortCounterTypeDccTxBytes,
	cAtCienaSgmiiEthPortCounterTypeDccTxErrors,
    cAtCienaSgmiiEthPortCounterTypeDccRxFrames,
	cAtCienaSgmiiEthPortCounterTypeDccRxBytes,
	cAtCienaSgmiiEthPortCounterTypeDccRxErrors,
    cAtCienaSgmiiEthPortCounterTypeKByteTxFrames,
    cAtCienaSgmiiEthPortCounterTypeKByteTxBytes,
    cAtCienaSgmiiEthPortCounterTypeKByteRxFrames,
    cAtCienaSgmiiEthPortCounterTypeDccRxUnrecognizedFrames,
    cAtCienaSgmiiEthPortCounterTypeKbyteRxUnrecognizedFrames,
    cAtCienaSgmiiEthPortCounterTypeDccRxGoodFrames,
    cAtCienaSgmiiEthPortCounterTypeDccRxDiscardFrames,
    cAtCienaSgmiiEthPortCounterTypeKbyteRxGoodFrames,
    cAtCienaSgmiiEthPortCounterTypeKbyteRxDiscardFrames
    }eAtCienaSgmiiEthPortCounterType;

/** @brief DCC/KByte over SGMII ETH counters */
typedef enum eAtCienaSgmiiEthPortAlarmType
	{
	cAtCienaSgmiiEthPortAlarmTypeDccDaMacMisMatch = cBit30, /**< Received DA value of APS frame different from configured DA */
	cAtCienaSgmiiEthPortAlarmTypeDccCVlanMisMatch = cBit29, /**< Received 12b CVLAN ID value of DCC frame different from global provisioned CVID  */
	cAtCienaSgmiiEthPortAlarmTypeDccLenOverSize   = cAtEthPortAlarmOversized, /**< Received DCC Length is over-size */
	cAtCienaSgmiiEthPortAlarmTypeFcsError         = cAtEthPortAlarmFcsError,  /**< Received packet from SGMII port has FCS error */
	cAtCienaSgmiiEthPortAlarmTypeEthTypeMismatch  = cBit28  /**< ETH Type mismatch */
	}eAtCienaSgmiiEthPortAlarmType;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* SERDES group */
eAtRet AtCienaFaceplateSerdesGroupSet(AtDevice device, uint8 groupId);
uint8 AtCienaFaceplateSerdesGroupGet(AtDevice device);

/* Face-plate lines */
uint8 AtModuleSdhNumFaceplateLines(AtModuleSdh self);
AtSdhLine AtModuleSdhFaceplateLineGet(AtModuleSdh self, uint8 localLineId);

/* MATE lines */
uint8 AtModuleSdhNumMateLines(AtModuleSdh self);
AtSdhLine AtModuleSdhMateLineGet(AtModuleSdh self, uint8 localLineId);

/* Terminated lines */
uint8 AtModuleSdhNumTerminatedLines(AtModuleSdh self);
AtSdhLine AtModuleSdhTerminatedLineGet(AtModuleSdh self, uint8 localLineId);

/* ETH bypassing */
eAtRet AtCienaEthPortBypassEnable(AtEthPort self, eBool enabled);
eBool AtCienaEthPortBypassIsEnabled(AtEthPort self);
eBool AtCienaEthPortBypassIsSupported(AtEthPort self);

/* ETH Flow Control Threshold */
eAtModuleEthRet AtCienaEthPortFlowControlHighThresholdSet(AtEthPort self, uint32 threshold);
uint32 AtCienaEthPortFlowControlHighThresholdGet(AtEthPort self);
eAtModuleEthRet AtCienaEthPortFlowControlLowThresholdSet(AtEthPort self, uint32 threshold);
uint32 AtCienaEthPortFlowControlLowThresholdGet(AtEthPort self);
uint32 AtCienaEthPortFlowControlTxWaterMarkMaxGet(AtEthPort self);
uint32 AtCienaEthPortFlowControlTxWaterMarkMaxClear(AtEthPort self);
uint32 AtCienaEthPortFlowControlTxWaterMarkMinGet(AtEthPort self);
uint32 AtCienaEthPortFlowControlTxWaterMarkMinClear(AtEthPort self);
uint32 AtCienaEthPortFlowControlOOBBufferLevelGet(AtEthPort self);
uint32 AtCienaEthPortFlowControlTxBufferLevelGet(AtEthPort self);

/* PDH interface type */
eAtRet AtCienaModulePdhInterfaceTypeSet(AtModulePdh self, eAtCienaPdhInterfaceType type);
eAtCienaPdhInterfaceType AtCienaModulePdhInterfaceTypeGet(AtModulePdh self);
eAtRet AtCienaPdhCemModulePdhMuxTxEthEnable(AtModulePdh self, eBool enabled);
eBool AtCienaPdhCemModulePdhMuxTxEthIsEnabled(AtModulePdh self);

/* PW */
eAtModulePwRet AtCienaPwTxActiveForce(AtPw self, eBool active);
eBool AtCienaPwTxActiveIsForced(AtPw self);
eAtRet AtCienaPwAllCountersGet(AtChannel self, void *counters);
eAtRet AtCienaPwAllCountersClear(AtChannel self, void *counters);

/* Monitoring clock outputs */
eAtRet AtCienaModuleClockMonitorOutputSourceSet(AtModuleClock self, uint8 clockOutputId, eAtCienaClockMonitorOutput refClockSource);
eAtCienaClockMonitorOutput AtCienaModuleClockMonitorOutputSourceGet(AtModuleClock self, uint8 clockOutputId);
uint8 AtCienaModuleClockMonitorNumOutputs(AtModuleClock self);

/* DCC/KByte SGMII port counters */
uint32 AtCienaDccKByteSgmiiCounterGet(AtEthPort self, eAtCienaSgmiiEthPortCounterType counterType);
uint32 AtCienaDccKByteSgmiiCounterClear(AtEthPort self, eAtCienaSgmiiEthPortCounterType counterType);

#ifdef __cplusplus
}
#endif
#endif /* _ATCIENA_H_ */

#include "AtCienaKByte.h"
#include "AtCienaKbyteChannel.h"
#include "AtCienaDcc.h"
#include "AtCienaVlan.h"

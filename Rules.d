#-----------------------------------------------------------------------------
#
# COPYRIGHT (C) 2012 Arrive Technologies Inc.
#
# The information contained herein is confidential property of Arrive Tecnologies. 
# The use, copying, transfer or disclosure of such information 
# is prohibited except by express written agreement with Arrive Technologies.
#
# File        : Rules.d
#
# Created Date: Nov-05-2012
#
# Description : Dependencies
# 
#----------------------------------------------------------------------------
include ${CLI_DIR}/Rules.d
include ${ATSDK_HOME}/debugutil/Rules.d
include ${ATSDK_HOME}/customers/Rules.d 

COMPONENT_LIB = $(wildcard $(AT_COMPONENTS_OBJ))

.PHONY: all atsdk clean cleanatsdk atsdkall atsdkallso atsdkallpo cleanatsdkall

all: atsdk atsdkall

ifeq ($(AT_OS), linux)
all: atsdkallso
endif

ifeq ($(AT_OS), vxworks)
all: atsdkallpo
endif

AT_SDK_ALL_OBJ = $(call GetObjectsFromSource, $(DRIVER_SRC) $(PLATFORM_SRC) $(UTIL_SRC))

atsdk: ${AT_SDK_LIB}
${AT_SDK_LIB}: $(AT_SDK_ALL_OBJ)
	$(AT_LD) $(LDFLAGS) $@ $^

atsdkall: atsdk atcli debugutil atcustomers
atsdkall: ${AT_SDK_LIB_ALL}
${AT_SDK_LIB_ALL}: ${AT_CLI_LIB} ${AT_CUSTOMERS_LIB_O} ${AT_DEBUGUTIL_LIB} ${AT_SDK_LIB} ${COMPONENT_LIB}
	$(AT_AR) cvr $@ $^
	$(AT_RANLIB) $@

atsdkallpo: atsdk atcli debugutil atcustomers
atsdkallpo: ${AT_SDK_LIB_ALL_PO}
${AT_SDK_LIB_ALL_PO}: ${AT_CLI_LIB} ${AT_CUSTOMERS_LIB_O} ${AT_DEBUGUTIL_LIB} ${AT_SDK_LIB} ${COMPONENT_LIB}
	$(AT_LD)  -r -X -warn-common $(LDFLAGS) $@ $^

atsdkallso: atsdk atcli debugutil atcustomers
atsdkallso: ${AT_SDK_LIB_ALL_SO}
${AT_SDK_LIB_ALL_SO}: ${AT_CLI_LIB} ${AT_CUSTOMERS_LIB_O} ${AT_DEBUGUTIL_LIB} ${AT_SDK_LIB} ${COMPONENT_LIB}
	$(AT_CC) $(AT_CFLAGS) -shared -o $@ $^

clean: cleanatsdk cleanatsdkall
cleanatsdk: 
	rm -f $(AT_SDK_ALL_OBJ) $(AT_SDK_LIB) ${AT_SDK_LIB_ALL_SO} ${AT_SDK_LIB_ALL_PO}
	rm -f $(patsubst %.o, %.d, $(DRIVER_SRC_OBJ) $(PLATFORM_SRC_OBJ) $(UTIL_SRC_OBJ) $(AT_SDK_LIB))

cleanatsdkall:
	rm -f $(AT_SDK_LIB_ALL)

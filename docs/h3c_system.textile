h1. IP

172.33.44.79

h1. SFP control

*Enable:*
devmem 0xb000000e 16 0x0
devmem 0xb0000010 16 0x0

*Disable:*
devmem 0xb000000e 16 0x1
devmem 0xb0000010 16 0x1

h2. Load FPGAs

* Insert module if it has not been: @insmod /ffs/h3c_main_card_init.ko@. After inserting, PLL, serdes, ... of all cards are initialized. This module is automatically inserted after rebooting. If for some reason, this module should be reload, try these commands to reload:
** @rmmod h3c_main_card_init.ko@
** @insmod /ffs/h3c_main_card_init.ko@ 

After kernel module has been inserted, there may be two devices at: /dev/[E1|STM]CardFpgaloadSlot[1|2]

It will detect which card is plug in and create device file. The naming convention for these files as following:
    
For example: Card E1 is plug to Slot2 it will create file /dev/E1CardFpgaLoadSlot2
NOTE: this file is a input to fpgaload command to load FPGA.

Use command fpgaload to load fpga.
    fpgaload [-s] [-d dev_name] [-b num_bits_per_word] -h <file name to load>
       -s : no wait fpga initing time

For example you want to load FPGA to card E1 on slot1, you run this command:
    fpgaload -d /dev/E1CardFpgaloadSlot1 xxxxxFPGA.rbf   

NOTE:
- We do not support plug and play in the driver to detect a new card is insert/remove from the backplane  now. If you plug a new card, please run command (2) to remove module and re-insert the kernel module by command (1).
We will support this feature soon.
